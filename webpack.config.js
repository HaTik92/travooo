const path    = require('path');
const webpack = require('webpack');

const nodeDir = path.join(__dirname, 'node_modules');

module.exports = {
    entry: [
        './resources/assets/js/core/core.js'
    ],
    output: {
        path: path.resolve(__dirname, 'public/js'),
        filename: 'app.bundle.js',
        // chunkFilename: "[name].chunk.js",
        publicPath: '/js/'
    },
    resolve: {
        alias: {
            'datatables': path.join(nodeDir, '/datatables/media/js/jquery.dataTables.js'),
            'datatables-select': path.join(nodeDir, '/datatables-select/js/dataTables.select.js'),
            'select2': path.join(nodeDir, '/select2/dist/js/select2.full.js'),
            'intlTelInput': path.join(nodeDir, '/intl-tel-input/index.js'),
            'datepicker': path.join(nodeDir, '/jquery-datepicker/index.js'),
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015'],
                    plugins: ['syntax-dynamic-import', 'transform-runtime']
                }
            },
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: 'jquery',
            "window.jQuery": "jquery",
            _: 'lodash',
            axios: "axios"
        })
    ],
    stats: {
        colors: true
    },
    devtool: 'cheap-eval-source-map'
};