<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DeleteAccountRequestNotification extends Notification
{
    use Queueable;
    private $token, $fromWeb;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $fromWeb = true)
    {
        $this->token = $token;
        $this->fromWeb = $fromWeb;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->fromWeb) {
            $url = url('delete-account/' . $this->token);
        } else {
            $url = generateDeepLink(url('/api/v1/delete-account/' . $this->token));
        }

        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', $url)
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
