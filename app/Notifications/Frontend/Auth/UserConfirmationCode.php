<?php

namespace App\Notifications\Frontend\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserConfirmationCode.
 */
class UserConfirmationCode extends Notification
{
    use Queueable;

    /**
     * @var
     */
    protected $confirmation_code;

    /**
     * UserConfirmationCode constructor.
     *
     * @param $confirmation_code
     */
    public function __construct($confirmation_code)
    {
        $this->confirmation_code = $confirmation_code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $url = generateDeepLink((url("/api/v1/email_confirmation") . "/{$this->confirmation_code}"));

        return (new MailMessage())
            ->subject('Travooo : ' . trans('exceptions.frontend.auth.confirmation.confirm'))
            ->greeting('Let’s make sure this is the right email address for you — please confirm this is the right address to use for your new account.')
            ->line('<span>Please enter this verification code to get started on Travooo:</span><br><span style="font-weight: bold;display: inline-block;margin: 10px 0;font-size: 21px;color: #000;">' . $this->confirmation_code . '</span><br><span>Verification codes expire after 10 minute.</span>');
    }
}
