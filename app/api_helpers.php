<?php

/**
 * Global Api helpers file with misc functions.
 */

use App\Http\Constants\CommonConst;
use App\Models\ActivityLog\ActivityLog;
use App\Models\ActivityMedia\MediasLikes;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Place\Place;
use App\Models\Posts\PostsCities;
use App\Models\Posts\PostsCountries;
use App\Models\Posts\PostsPlaces;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsDraft;
use App\Models\TripPlans\TripPlans;
use App\Services\Trips\TripsService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Api\CountryController2;
use App\Http\Controllers\Api\CityController;
use App\Http\Controllers\Api\PlaceController2;
use App\Http\Controllers\Api\ReportsController;
use App\Http\Controllers\Api\DiscussionController;
use App\Http\Controllers\Api\PrimaryPostController;
use App\Http\Controllers\Api\NewsfeedPageController;
use App\Http\Controllers\Api\GlobalTripController;
use App\Models\Discussion\Discussion;
use App\Models\Posts\Posts;
use App\Models\Reports\ReportsInfos;
use App\Models\Reviews\Reviews;
use App\Models\TripPlaces\TripPlaces;
use App\Models\User\UsersContentLanguages;

if (!function_exists('optionalAuthRoutes')) {

    function optionalAuthRoutes($className, $apiRoot = "api/v1")
    {
        switch ($className) {
            case CountryController2::class:
                return [
                    $apiRoot . "/country/{id}",
                    $apiRoot . "/country/{id}/experts",
                    $apiRoot . "/country/{id}/trips",
                    $apiRoot . "/country/{id}/newsfeed",
                ];
                break;
            case CityController::class:
                return [
                    $apiRoot . "/city/{id}",
                    $apiRoot . "/city/{id}/experts",
                    $apiRoot . "/city/{id}/trips",
                    $apiRoot . "/city/{id}/newsfeed",
                ];
                break;
            case PlaceController2::class:
                return [
                    $apiRoot . "/place/{id}",
                    $apiRoot . "/place/{id}/experts",
                    $apiRoot . "/place/{id}/trips",
                    $apiRoot . "/place/{place_id}/reviews",
                    $apiRoot . "/place/{id}/newsfeed",
                ];
                break;
            case ReportsController::class:
                return [
                    $apiRoot . "/reports",
                    $apiRoot . "/reports/{id}",
                    $apiRoot . "/reports/top-places",
                ];
                break;
            case DiscussionController::class:
                return [
                    $apiRoot . "/discussions",
                    $apiRoot . "/discussions/tabs/{tab_name}",
                ];
                break;
            case PrimaryPostController::class:
                return [
                    $apiRoot . "/{type}/{post_id}/comment",
                ];
                break;
            case NewsfeedPageController::class:
                return [
                    $apiRoot . "/newsfeed/{page_id}",
                ];
                break;
            case GlobalTripController::class:
                return [
                    $apiRoot . "/global-trip",
                    $apiRoot . "/global-trip/trending-trips",
                    $apiRoot . "/global-trip/trending-destinations",
                ];
                break;
        }
        return [];
    }
}

if (!function_exists('_getDBQuery')) {

    function _getDBQuery($query)
    {
        $bindings = $query->getBindings();
        $sql = str_replace('?', "'%s'", $query->toSql());
        return sprintf($sql, ...$bindings);
    }
}

if (!function_exists('modifyPagination')) {

    /**
     * @param $paginationResult
     * @return stdClass
     */
    function modifyPagination($paginationResult)
    {
        $newClass = new stdClass();
        $newClass->total        = $paginationResult->total();
        $newClass->per_page     = $paginationResult->perPage();
        $newClass->current_page = $paginationResult->currentPage();
        $newClass->total_pages  = $paginationResult->lastPage();
        $newClass->data         = $paginationResult->getCollection();
        return $newClass;
    }
}

if (!function_exists('_createLocationObjectWithFullDetailsForReport')) {
    /**
     * @param array [collect(City),collect(Country)]
     * @return array
     */
    function _createLocationObjectWithFullDetailsForReport($cities, $countries)
    {
        $cities = collect($cities);
        $countries = collect($countries);
        $result = [];
        $countries->each(function ($country) use (&$result) {
            $result[] = [
                'type' => 'country',
                'data' => preparedReportCountryDetails($country)
            ];
        });
        $cities->each(function ($city) use (&$result) {
            $result[] = [
                'type' => 'city',
                'data' => preparedReportCityDetails($city)
            ];
        });
        return $result;
    }
}

if (!function_exists('preparedReportCityDetails')) {
    function preparedReportCityDetails($city)
    {
        $return = [];
        $return['id'] = $city->id;
        $return['lat'] = $city->lat;
        $return['lng'] = $city->lng;
        $return['city'] = @$city->trans[0]->title;
        $return['country'] = @$city->country->trans[0]->title;
        $return['phone_code'] = $city->code;
        $return['population'] = @$city->trans[0]->population;
        $return['nationality'] = @$city->country->trans[0]->nationality;
        $return['working_days'] = (@$city->trans[0]->working_days != '') ? @$city->trans[0]->working_days : @$city->country->trans[0]->working_days;

        $speedLimit = (@$city->trans[0]->speed_limit != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->speed_limit) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->speed_limit);
        $speedLimits = [];
        foreach ($speedLimit as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $speedLimits[] = [
                    "type" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }
        $return['speed_limit'] = $speedLimits;
        (@$city->trans[0]->metrics != '') ? preg_match_all("/\(([^\]]*)\)/", @$city->trans[0]->metrics, $matches) : preg_match_all("/\(([^\]]*)\)/", @$city->country->trans[0]->metrics, $matches);
        $return['metrics'] = isset($matches[1][0]) ? explode(", ", $matches[1][0]) : [];
        $return['internet'] = (@$city->trans[0]->internet != '') ? @$city->trans[0]->internet : @$city->country->trans[0]->internet;

        $timing = (@$city->trans[0]->best_time != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->best_time) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->best_time);
        $timings = [];
        foreach ($timing as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $timings[] = [
                    "type" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }
        $return['general']['timing'] = $timings;
        $return['general']['transportation'] = (@$city->trans[0]->transportation != '') ? explode(", ", @$city->trans[0]->transportation) : explode(", ", @$city->country->trans[0]->transportation);
        $return['general']['currencies'] = @$city->country->currencies[0]->transsingle->title;

        // $return['medias'] = count(@$city->getMedias) > 0 ? @$city->getMedias : @$city->country->getMedias;

        $socket = (@$city->trans[0]->sockets != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->sockets) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->sockets);
        $sockets = [];
        foreach ($socket as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $sockets[] = [
                    "type" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }
        $return['sockets'] = $sockets;
        $return['cost_of_living'] = (@$city->trans[0]->cost_of_living != '') ? @$city->trans[0]->cost_of_living : @$city->country->trans[0]->cost_of_living;
        $return['crime_rate'] = (@$city->trans[0]->geo_stats != '') ? @$city->trans[0]->geo_stats : @$city->country->trans[0]->geo_stats;
        $return['quality_of_life'] = (@$city->trans[0]->demographics != '') ? @$city->trans[0]->demographics : @$city->country->trans[0]->demographics;

        $economy = (@$city->trans[0]->economy != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->economy) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->economy);
        $economies = [];
        foreach ($economy as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $economies[] = [
                    "type" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }
        $return['restrictions'] = $economies;

        $return['emergency_numbers'] = [];
        @$city_emergency = (count((@$city->emergency ?? [])) > 0) ? @$city->emergency : @$city->country->emergency;
        if (@$city_emergency) {
            $emerg = [];
            foreach (@$city_emergency as $emergency) {
                $emerg[] = $emergency->transsingle->title;
            }

            $return['emergency_numbers'] = $emerg;
        }

        $return['general']['religions'] = [];
        @$city_religions = (count((@$city->religions ?? [])) > 0) ? @$city->religions : @$city->country->religions;
        if (@$city_religions) {
            $rel = [];
            foreach (@$city_religions as $religions) {
                $rel[] = $religions->transsingle->title;
            }
            $return['general']['religions'] = $rel;
        }

        $return['holidays'] = [];
        $city_holiday = (count((@$city->cityHolidays ?? [])) > 0) ? @$city->cityHolidays : @$city->country->countryHolidays;

        if (@$city_holiday) {
            $hol = [];
            foreach (@$city_holiday as $holidays) {
                if (isset($holidays->holiday->transsingle) && !empty($holidays->holiday->transsingle)) {
                    $hol[] = $holidays->holiday->transsingle->title;
                }
            }
            $return['holidays'] = $hol;
        }

        $return['general']['languages_spoken'] = [];
        $lng = [];
        @$languages_spoken = (count((@$city->languages_spoken ?? [])) > 0) ? @$city->languages_spoken : @$city->country->languages;
        if (@$languages_spoken) {
            foreach (@$languages_spoken as $languages) {
                $lng[] = $languages->transsingle->title;
            }
        }

        @$city_additional_languages = (count((@$city->additional_languages ?? [])) > 0) ? @$city->additional_languages : @$city->country->additional_languages;
        if (@$city_additional_languages) {
            foreach (@$city_additional_languages as $additional_languages) {
                $lng[] = $additional_languages->transsingle->title;
            }

            $return['general']['languages_spoken'] = $lng;
        }
        return $return;
    }
}

if (!function_exists('preparedReportCountryDetails')) {
    function preparedReportCountryDetails($country)
    {
        $return = [];
        $return['id'] = $country->id;
        $return['country'] = @$country->trans[0]->title;
        $return['capital'] = @$country->capitals[0]->city->trans[0]->title ?? "";
        $return['phone_code'] = $country->code;
        $return['iso_code'] = $country->iso_code;
        $return['lat'] = $country->lat;
        $return['lng'] = $country->lng;
        $return['population'] = @$country->trans[0]->population;
        $return['nationality'] = @$country->trans[0]->nationality;
        $return['working_days'] = @$country->trans[0]->working_days;

        $speedLimit = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->speed_limit);
        $speedLimits = [];
        foreach ($speedLimit as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $speedLimits[] = [
                    "type" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }
        $return['speed_limit'] = $speedLimits;
        preg_match_all("/\(([^\]]*)\)/", @$country->trans[0]->metrics, $matches);
        $return['metrics'] = isset($matches[1][0]) ? explode(", ", $matches[1][0]) : [];
        $return['internet'] = @$country->trans[0]->internet;
        $timing = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->best_time);
        $timings = [];
        foreach ($timing as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $timings[] = [
                    "type" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }

        $return['general']['timing'] = $timings;
        $return['general']['transportation'] = explode(", ", @$country->trans[0]->transportation);
        $return['general']['currencies'] = @$country->currencies[0]->transsingle->title;

        // $return['medias'] = @$country->getMedias;

        $socket = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->sockets);
        $sockets = [];
        foreach ($socket as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $sockets[] = [
                    "type" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }
        $return['sockets'] = $sockets;
        $return['cost_of_living'] = @$country->trans[0]->cost_of_living;
        $return['crime_rate'] = @$country->trans[0]->geo_stats;
        $return['quality_of_life'] = @$country->trans[0]->demographics;


        $economy = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->economy);
        $economies = [];
        foreach ($economy as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $economies[] = [
                    "type" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }
        $return['restrictions'] = $economies;

        $return['emergency_numbers'] = [];
        if (@$country->emergency) {
            $emerg = [];
            foreach (@$country->emergency as $emergency) {
                $emerg[] = $emergency->transsingle->title;
            }
            $return['emergency_numbers'] = $emerg;
        }

        $return['general']['religions'] = [];
        if (@$country->religions) {
            $rel = [];
            foreach (@$country->religions as $religions) {
                $rel[] = $religions->transsingle->title;
            }
            $return['general']['religions'] = $rel;
        }

        $return['holidays'] = [];

        if (@$country->countryHolidays) {
            $hol = [];
            foreach (@$country->countryHolidays as $holidays) {
                if (isset($holidays->holiday->transsingle) && !empty($holidays->holiday->transsingle)) {
                    $hol[] = $holidays->holiday->transsingle->title;
                }
            }
            $return['holidays'] = $hol;
        }


        $return['general']['languages_spoken'] = [];
        $lng = [];
        if (@$country->languages) {
            foreach (@$country->languages as $languages) {
                $lng[] = $languages->transsingle->title;
            }
        }

        if (@$country->additional_languages) {
            foreach (@$country->additional_languages as $additional_languages) {
                $lng[] = $additional_languages->transsingle->title;
            }

            $return['general']['languages_spoken'] = $lng;
        }
        return $return;
    }
}


if (!function_exists('when')) {
    /**
     * This function same work like switch statement 
     * For Ex:
     * $result = when(1, [
     *              1   => "one",
     *              2   => "two",
     *              3   => "three",
     *          ]);
     */
    function when($condition, $keyValues)
    {
        foreach ($keyValues as $arrCondition => $value) {
            if ($arrCondition == $condition) {
                return $value;
            }
        }
        return null;
    }
}

if (!function_exists('travelogTripObject')) {
    function travelogTripObject($plan_id_or_object)
    {
        $trip_plan = (is_int($plan_id_or_object)) ? TripPlans::find($plan_id_or_object) : $plan_id_or_object;
        if ($trip_plan) {
            $places = [];
            $countries = [];
            $cities = [];
            $total_destination = 0;
            $_totalDestination = ['city_wise' => [], 'country_wise' => []];

            foreach ($trip_plan->published_places as $tripPlace) {
                if (!$tripPlace->place or !$tripPlace->country or !$tripPlace->city) {
                    continue;
                }
                if (!isset($places[$tripPlace->places_id])) {
                    $total_destination++;
                    $_totalDestination['city_wise'][$tripPlace->cities_id][$tripPlace->places_id] = $tripPlace->place;
                    $_totalDestination['country_wise'][$tripPlace->countries_id][$tripPlace->places_id] = $tripPlace->place;
                    $places[$tripPlace->places_id] = [
                        'id' => $tripPlace->places_id,
                        'country_id' => $tripPlace->countries_id,
                        'city_id' => $tripPlace->cities_id,
                        'title' => isset($tripPlace->place->transsingle->title) ? $tripPlace->place->transsingle->title : null,
                        'lat' => $tripPlace->place->lat,
                        'lng' => $tripPlace->place->lng,
                        'image' => check_place_photo($tripPlace->place),
                    ];
                }
                if (!isset($countries[$tripPlace->countries_id])) {
                    $countries[$tripPlace->countries_id] = [
                        'id' => $tripPlace->countries_id,
                        'title' => isset($tripPlace->country->trans[0]->title) ? $tripPlace->country->trans[0]->title : null,
                        'lat' => $tripPlace->country->lat,
                        'lng' => $tripPlace->country->lng,
                        'image' => get_country_flag($tripPlace->country),
                        'total_destination' => 0,
                    ];
                }
                if (!isset($cities[$tripPlace->cities_id])) {
                    $cities[$tripPlace->cities_id] = [
                        'id' => $tripPlace->cities_id,
                        'country_id' => $tripPlace->countries_id,
                        'title' => isset($tripPlace->city->trans[0]->title) ? $tripPlace->city->trans[0]->title : null,
                        'lat' => $tripPlace->city->lat,
                        'lng' => $tripPlace->city->lng,
                        'image' => isset($tripPlace->city->medias[0]->media->path) ?
                            check_city_photo($tripPlace->city->medias[0]->media->path) :
                            url(PATTERN_PLACEHOLDERS),
                        'total_destination' => 0,
                    ];
                }
            }

            foreach ($countries as $country_id => &$country) {
                $country['total_destination'] = isset($_totalDestination['country_wise'][$country_id]) ? count($_totalDestination['country_wise'][$country_id]) : 0;
            }
            foreach ($cities as $city_id =>  &$city) {
                $city['total_destination'] = isset($_totalDestination['city_wise'][$city_id]) ? count($_totalDestination['city_wise'][$city_id]) : 0;
            }
            $country_title =  isset(array_values($countries)[0]['title']) ? array_values($countries)[0]['title'] : null;
            $trip_plan->total_destination = $total_destination;
            $trip_plan->data = [];
            if (count($countries) > 1) {
                $trip_plan->type = TripsService::NEWSFEED_MULTIPLE_COUNTRY;
                $trip_plan->data = array_values($countries);
            } else {
                if (count($cities) > 1) {
                    $trip_plan->type = TripsService::NEWSFEED_MULTIPLE_CITY;
                    $trip_plan->country_title = $country_title;
                    $trip_plan->data = array_values($cities);
                } else {
                    $trip_plan->type = TripsService::NEWSFEED_SINGLE_CITY;
                    $trip_plan->country_title = $country_title;
                    $trip_plan->data = array_values($places);
                }
            }

            unset($trip_plan->published_places);

            if ($trip_plan->author) {
                $trip_plan->author->profile_picture = check_profile_picture($trip_plan->author->profile_picture);
            }

            return $trip_plan;
        }
        return null;
    }
}

if (!function_exists('savePostInLocation')) {
    function savePostInLocation($cityOrPlaceOrCityObject, $postId)
    {
        if (is_null($cityOrPlaceOrCityObject)) return false;
        switch (get_class($cityOrPlaceOrCityObject)) {
            case Place::class:
                $postPlace = new PostsPlaces;
                $postPlace->posts_id = $postId;
                $postPlace->places_id = $cityOrPlaceOrCityObject->id;
                $postPlace->save();

                $postCity = new PostsCities;
                $postCity->posts_id = $postId;
                $postCity->cities_id = $cityOrPlaceOrCityObject->cities_id;
                $postCity->save();

                $postCountry = new PostsCountries;
                $postCountry->posts_id = $postId;
                $postCountry->countries_id = $cityOrPlaceOrCityObject->countries_id;
                $postCountry->save();
                break;
            case Cities::class:
                $postCity = new PostsCities;
                $postCity->posts_id = $postId;
                $postCity->cities_id = $cityOrPlaceOrCityObject->id;
                $postCity->save();

                $postCountry = new PostsCountries;
                $postCountry->posts_id = $postId;
                $postCountry->countries_id = $cityOrPlaceOrCityObject->countries_id;
                $postCountry->save();
                break;
            case Countries::class:
                $postCountry = new PostsCountries;
                $postCountry->posts_id = $postId;
                $postCountry->countries_id = $cityOrPlaceOrCityObject->id;
                $postCountry->save();
                break;
            default:
                return false;
        }
        return true;
    }
}


if (!function_exists('api_get_report_locations_info')) {

    function api_get_report_locations_info($fetchLocations)
    {
        $location_info = [];
        collect($fetchLocations[Reports::LOCATION_COUNTRY])->each(function ($country) use (&$location_info) {
            $location_info[] = [
                'id' => $country->id,
                'type' => 'country',
                'iso_code' => ($country->iso_code) ? $country->iso_code : '',
                'title' => $country->transsingle->title
            ];
        });
        collect($fetchLocations[Reports::LOCATION_CITY])->each(function ($city) use (&$location_info) {
            $location_info[] = [
                'id' => $city->id,
                'type' => 'city',
                'iso_code' => ($city->country->iso_code) ? $city->country->iso_code : '',
                'title' => $city->transsingle->title
            ];
        });
        return $location_info;
    }
}


if (!function_exists('mediaCommentSection')) {
    function mediaCommentSection(&$post)
    {
        $user = auth()->user();
        $post->total_likes = MediasLikes::where('medias_id', $post->id)->count();
        $post->like_flag = $user ? (MediasLikes::where('medias_id', $post->id)->where('users_id', $user->id)->count() ? true : false) : false;
        $post->comment_flag = $user ? ($post->postComments->where('users_id', $user->id)->count() ? true : false) : false;
        $comments = $post->postComments->take(2);
        unset($post->postComments);
        foreach ($comments as $parent_comment) {

            // For Parent Comment
            $parent_comment->text = isset($parent_comment->tags) ? convert_post_text_to_tags(strip_tags($parent_comment->comment), $parent_comment->tags, false) : strip_tags($parent_comment->comment);
            unset($parent_comment->tags, $parent_comment->comment);

            $parent_comment->like_status = $user ? ($parent_comment->likes->where('users_id', $user->id)->first() ? true : false) : false;
            $parent_comment->total_likes = $parent_comment->likes->count();
            if ($parent_comment->medias) {
                foreach ($parent_comment->medias as $media) {
                    $media->media;
                }
            }
            if ($parent_comment->author) {
                $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
            }
            $parent_comment->comment_flag = $user ? ($parent_comment->sub->where('users_id', $user->id)->count() ? true : false) : false;
            unset($parent_comment->likes);

            // For Sub Comment
            if ($parent_comment->sub) {
                foreach ($parent_comment->sub as $sub_comment) {

                    $sub_comment->text = isset($sub_comment->tags) ? convert_post_text_to_tags(strip_tags($sub_comment->comment), $sub_comment->tags, false) : strip_tags($sub_comment->comment);
                    unset($sub_comment->tags, $sub_comment->comment);

                    $sub_comment->like_status = $user ? ($parent_comment->likes->where('users_id', $user->id)->first() ? true : false) : false;
                    $sub_comment->total_likes = $sub_comment->likes->count();
                    if ($sub_comment->medias) {
                        foreach ($sub_comment->medias as $media) {
                            $media->media;
                        }
                    }
                    if ($sub_comment->author) {
                        $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                    }
                    unset($sub_comment->likes);
                }
            }
        }
        $post->post_comments = $comments;
        return $post;
    }
}

/**
 * Countries + Citie + Place | Countries + Citie + Place | Countries + Citie + Place | Countries + Citie + Place 
 */

if (!function_exists('_fieldValueFromTrans')) {
    function _fieldValueFromTrans($obj, $field, $default = null)
    {
        return isset($obj->trans[0]->$field) ? $obj->trans[0]->$field : $default;
    }
}

if (!function_exists('_ccp_weather')) {
    function _ccp_weather($obj)
    {
        try {
            $data = [];
            $resp = weatherApi(1, [
                'lat' => $obj->lat,
                'lng' => $obj->lng,
            ]);

            if ($resp && $resp != "null") {
                $resp = json_decode($resp);
                $place_key = $resp->Key;

                $country_weather = weatherApi(4, [
                    'hours' => 12,
                    'place_key' => $place_key,
                ]);
                $current_weather = weatherApi(2, [
                    'place_key' => $place_key,
                ]);
                $daily_weather = weatherApi(3, [
                    'day' => 10,
                    'place_key' => $place_key,
                ]);

                $data['country_weather'] = json_decode($country_weather);
                $data['current_weather'] = json_decode($current_weather);
                $data['daily_weather'] = $daily_weather ? json_decode($daily_weather)->DailyForecasts : null;
            }
        } catch (\Throwable $th) {
            // throw $th;
        }
        return $data;
    }
}
if (!function_exists('__prepared_ccp_weather')) {
    function __prepared_ccp_weather($obj)
    {
        $info = [];
        try {
            $info['current_weather']['temperature'] = @$obj['current_weather'][0]->Temperature->Metric->Value;
            $info['current_weather']['date'] = @$obj['current_weather'][0]->LocalObservationDateTime;
            $info['current_weather']['text'] = @$obj['current_weather'][0]->WeatherText;
            $info['current_weather']['icon_name'] = @$obj['current_weather'][0]->WeatherIcon . '-s.png';

            $info['country_weather'] =  collect(@$obj['country_weather'] ?? [])->map(function ($cw) {
                $temp = [];
                $temp['date'] = $cw->DateTime;
                $temp['icon_name'] = $cw->WeatherIcon . '-s.png';
                $temp['Icon_phrase'] = $cw->IconPhrase;
                $temp['temperature'] = @$cw->Temperature->Value;
                return $temp;
            });

            $info['daily_weather'] =  collect(@$obj['daily_weather'] ?? [])->map(function ($dw) {
                $temp = [];
                $temp['date'] = $dw->EpochDate;
                $temp['icon_name'] = @$dw->Day->Icon . '-s.png';
                $temp['Icon_phrase'] = @$dw->Day->IconPhrase;
                $temp['temperature'] = @$dw->Temperature->Maximum->Value;
                return $temp;
            });
        } catch (\Throwable $th) {
        }
        return $info;
    }
}

/**
 * Countries + Citie + Place | Countries + Citie + Place | Countries + Citie + Place | Countries + Citie + Place 
 */


if (!function_exists('_ApiRequest')) {
    function _ApiRequest($module = null, $sub = null, $meta = [])
    {
        if (auth()->check()) {
            $meta = collect([
                'auth_id' => auth()->id(),
                'auth_email' => auth()->user()->email,
            ])->merge($meta)->toArray();
        }
        $ApiRequest = new \App\Models\ApiRequest;
        $ApiRequest->module = $module;
        $ApiRequest->sub = $sub;
        $ApiRequest->method = request()->method();
        $ApiRequest->end_point = request()->url();
        $ApiRequest->request_params = json_encode(request()->all());
        $ApiRequest->header = json_encode(getallheaders());
        $ApiRequest->meta = json_encode($meta);
        $ApiRequest->save();
    }
}

if (!function_exists('get_as_bool')) {
    function get_as_bool($val)
    {
        return filter_var($val, FILTER_VALIDATE_BOOLEAN);
    }
}
