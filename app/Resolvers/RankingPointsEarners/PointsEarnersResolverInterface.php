<?php

namespace App\Resolvers\RankingPointsEarners;

interface PointsEarnersResolverInterface
{
    public function resolve($entity);
}