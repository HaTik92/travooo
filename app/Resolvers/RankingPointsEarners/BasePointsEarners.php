<?php

namespace App\Resolvers\RankingPointsEarners;

use App\Models\Ranking\RankingPointsEarners;

class BasePointsEarners implements PointsEarnersResolverInterface
{
    public function resolve($entity)
    {
        return $this->getEarners([$entity->id], get_class($entity));
    }

    protected function getEarners($entitiesIds, $entityClass)
    {
        return RankingPointsEarners::query()
            ->whereIn('entity_id', $entitiesIds)
            ->where('entity_type', $entityClass)
            ->pluck('user_id')
            ->unique();
    }
}