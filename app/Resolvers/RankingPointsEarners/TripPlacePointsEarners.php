<?php

namespace App\Resolvers\RankingPointsEarners;

use App\Models\TripPlaces\TripPlaces;
use App\Services\Trips\TripsSuggestionsService;

class TripPlacePointsEarners extends BasePointsEarners
{
    /**
     * @var TripsSuggestionsService
     */
    private $tripsSuggestionsService;

    public function __construct(TripsSuggestionsService $tripsSuggestionsService)
    {
        $this->tripsSuggestionsService = $tripsSuggestionsService;
    }

    public function resolve($entity)
    {
        $tripPlaceIds = $this->tripsSuggestionsService->getAllSuggestionPackTripPlacesIds($this->tripsSuggestionsService->getLastTripPlace($entity->id));

        return $this->getEarners($tripPlaceIds, TripPlaces::class);
    }
}