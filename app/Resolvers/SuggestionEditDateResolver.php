<?php

namespace App\Resolvers;

use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SuggestionEditDateResolver extends SuggestionEditResolver
{
    public function resolvePlace()
    {
        $this->validateValues();

        $date = $this->values['date'];
        $time = $this->values['time'];

        $date_time = strtotime($date . " " . $time);
        $date_time = date("Y-m-d H:i:s", $date_time);

        $this->place->date = $date_time;
        $this->place->time = $date_time;
        $this->place->without_time = $time === null;

        return $this->place;
    }

    /**
     * @return bool|void
     * @throws AuthorizationException
     */
    public function validateValues()
    {
        if (!array_key_exists('date', $this->values) ||
            !array_key_exists('time', $this->values)
        ) {
            throw new BadRequestHttpException();
        }

        $timeValue = Carbon::parse($this->values['time']);

        if ($this->place->trip->memory && Carbon::parse($timeValue)->greaterThan(now())) {
            throw new AuthorizationException('', 403);
        }
    }

    protected function prepareValues()
    {
        $this->values = [
            'date' => $this->place->time,
            'time' => ''
        ];
    }
}