<?php

namespace App\Resolvers;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SuggestionEditTagsResolver extends SuggestionEditResolver
{
    public function resolvePlace()
    {
        $this->validateValues();

        $this->place->comment = $this->values['tags'];

        return $this->place;
    }

    public function validateValues()
    {
        if (!array_key_exists('tags', $this->values)) {
            throw new BadRequestHttpException();
        }
    }


    protected function prepareValues()
    {
        $this->values = [
            'tags' => $this->place->comment
        ];
    }
}