<?php

namespace App\Resolvers;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SuggestionEditDurationResolver extends SuggestionEditResolver
{
    public function resolvePlace()
    {
        $this->validateValues();

        $days = $this->values['duration_days'];
        $hours = $this->values['duration_hours'];
        $minutes = $this->values['duration_minutes'];

        $duration = ($days * 60 * 24) + ($hours * 60) + $minutes;

        $this->place->duration = $duration;

        return $this->place;
    }

    public function validateValues()
    {
        if (!array_key_exists('duration_days', $this->values) ||
            !array_key_exists('duration_hours', $this->values) ||
            !array_key_exists('duration_minutes', $this->values)
        ) {
            throw new BadRequestHttpException();
        }
    }

    protected function prepareValues()
    {
        $duration = $this->place->duration;
        $days = 0;
        $hours = 0;

        if ($duration > (60 * 24)) {
            $days = (int) ($duration / (60 * 24));
            $duration -= $days * (60 * 24);
        }

        if ($duration > 60) {
            $hours = (int) ($duration / 60);
        }

        $minutes = $duration - ($hours * 60);

        $this->values = [
            'duration_days' => $days,
            'duration_hours' => $hours,
            'duration_minutes' => $minutes
        ];
    }
}