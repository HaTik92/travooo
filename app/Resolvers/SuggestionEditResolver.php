<?php

namespace App\Resolvers;

use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripsSuggestion;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class SuggestionEditResolver implements SuggestionEditResolverInterface
{
    /**
     * @var TripPlaces
     */
    protected $place;
    /**
     * @var array
     */
    protected $values;

    /**
     * @var TripsSuggestion|null
     */
    protected $suggestion;

    /**
     * SuggestionEditResolver constructor.
     * @param TripPlaces $place
     * @param array $values
     * @param TripsSuggestion|null $suggestion
     */
    public function __construct(TripPlaces $place, $values = [], $suggestion = null)
    {
        if ($suggestion) {
            $this->place = $suggestion->suggestedPlace;
        } else {
            $this->place = $place->replicate();
            $this->place->deleted_at = null;
            $this->place->versions_id = 0;
        }

        $this->values = $values;
        $this->suggestion = $suggestion;
    }

    /**
     * @return TripsSuggestion|null
     */
    public function actualizeSuggestion()
    {
        $this->prepareValues();
        $this->actualizePlace();
        $this->resolvePlace();
        $this->place->save();

        $this->suggestion->suggested_trips_places_id = $this->place->id;
        $this->suggestion->save();

        return $this->suggestion;
    }

    /**
     * @return TripPlaces
     */
    public function resolvePlace()
    {
        return $this->place;
    }

    public function validateValues()
    {
        return true;
    }

    protected function actualizePlace()
    {
        if (!$this->suggestion) {
            throw new BadRequestHttpException();
        }

        /** @var TripsSuggestionsService $suggestionsService */
        $suggestionsService = app(TripsSuggestionsService::class);

        $actualPlaceId = $suggestionsService->getLastTripPlace($this->suggestion->active_trips_places_id, true);
        $actualPlace = TripPlaces::query()->where('id', $actualPlaceId)->withTrashed()->first();

        if (!$actualPlace) {
            throw new ModelNotFoundException();
        }

        $this->place = $actualPlace->replicate();
        $this->place->versions_id = 0;
        $this->place->deleted_at = null;

        $this->suggestion = $this->suggestion->replicate();
        $this->suggestion->active_trips_places_id = $actualPlaceId;
        $this->suggestion->type = TripsSuggestionsService::SUGGESTION_EDIT_TYPE;
    }

    protected function prepareValues()
    {
        $this->values = [];
    }
}