<?php

namespace App\Resolvers;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SuggestionEditBudgetResolver extends SuggestionEditResolver
{
    public function resolvePlace()
    {
        $this->validateValues();

        $this->place->budget = $this->values['budget'];

        return $this->place;
    }

    public function validateValues()
    {
        if (!array_key_exists('budget', $this->values)) {
            throw new BadRequestHttpException();
        }
    }

    protected function prepareValues()
    {
        $this->values = [
            'budget' => $this->place->budget
        ];
    }
}
