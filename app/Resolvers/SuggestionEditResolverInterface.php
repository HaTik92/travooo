<?php

namespace App\Resolvers;

use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripsSuggestion;

interface SuggestionEditResolverInterface
{
    public function __construct(TripPlaces $place, $values);

    /**
     * @return TripsSuggestion|null
     */
    public function actualizeSuggestion();
    public function validateValues();
    public function resolvePlace();
}