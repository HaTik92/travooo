<?php

namespace App\Resolvers;

use App\Models\TripMedias\TripMedias;
use App\Models\User\UsersMedias;
use App\Services\Trips\TripsSuggestionsService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SuggestionEditMediaResolver extends SuggestionEditResolver
{
    public function resolvePlace()
    {
        $this->validateValues();

        $medias = array_slice(array_unique($this->values['medias']), 0, 10);

        if ($this->place->save()) {
            $userId = auth()->id();

            if ($this->suggestion && $this->suggestion->users_id) {
                $userId = $this->suggestion->users_id;
            }

            foreach ($medias as $mediaId) {
                $attributes = [
                    'medias_id' => $mediaId,
                    'trips_id' => $this->place->trips_id,
                    'places_id' => $this->place->places_id,
                    'trip_place_id' => $this->place->id
                ];

                TripMedias::query()->firstOrCreate($attributes);
                UsersMedias::query()->firstOrCreate(['users_id' => $userId, 'medias_id' => $mediaId]);
            }
        }

        return $this->place;
    }

    public function validateValues()
    {
        if (!array_key_exists('medias', $this->values)) {
            throw new BadRequestHttpException();
        }
    }

    protected function prepareValues()
    {
        $this->values = [
            'medias' => $this->place->medias->pluck('medias_id')->toArray()
        ];
    }
}