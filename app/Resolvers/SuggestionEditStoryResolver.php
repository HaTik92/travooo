<?php

namespace App\Resolvers;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SuggestionEditStoryResolver extends SuggestionEditResolver
{
    public function resolvePlace()
    {
        $this->validateValues();

        $this->place->story = $this->values['story'];

        return $this->place;
    }

    public function validateValues()
    {
        if (!array_key_exists('story', $this->values)) {
            throw new BadRequestHttpException();
        }
    }

    protected function prepareValues()
    {
        $this->values = [
            'story' => $this->place->story
        ];
    }
}