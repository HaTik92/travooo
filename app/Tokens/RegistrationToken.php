<?php

namespace App\Tokens;

use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use JsonSerializable;

class RegistrationToken implements JsonSerializable
{
    /**
     * @var int
     */
    protected $userId;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var Carbon
     */
    protected $expiredDate;

    /**
     * @var string
     */
    protected $password;


    /**
     * RegistrationToken constructor.
     * @param User $user
     * @param string $password
     */
    public function __construct(User $user, $password)
    {
        $this->userId = $user->getKey();
        $this->expiredDate = Carbon::create()->addHours(2);
        $this->password = $password;
        $this->email = $user->email;
    }

    /**
     * @param User $user
     * @param string $password
     * @return RegistrationToken
     */
    public static function create(User $user, $password)
    {
        return new static($user, $password);
    }

    /**
     * @param string $value
     * @throws DecryptException
     * @return RegistrationToken
     */
    public static function decrypt($value)
    {
        return decrypt($value);
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return encrypt($this);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getValue();
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->getValue();
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return Carbon
     */
    public function getExpiredDate()
    {
        return $this->expiredDate;
    }
}
