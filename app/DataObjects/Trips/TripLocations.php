<?php

namespace App\DataObjects\Trips;

class TripLocations
{
    /**
     * @var array
     */
    protected $cities;

    /**
     * @var array
     */
    protected $countries;

    /** @var array */
    protected $places;

    /**
     * TripLocations constructor.
     * @param array $cities
     * @param array $countries
     * @param array $places
     */
    public function __construct($cities, $countries, $places)
    {
        $this->countries = $countries;
        $this->cities = $cities;
        $this->places = $places;
    }

    /**
     * @return array
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @return array
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @return array
     */
    public function getPlaces()
    {
        return $this->places;
    }
}