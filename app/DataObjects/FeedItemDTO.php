<?php
namespace App\DataObjects;

class FeedItemDTO
{
    public $id;
    public $type;
    public $action;
    public $isFollowContent = false;
    public $isSuggested = false;
    public $withLink = false;
    public $withLikedAndShared = true;
    public $shareData = null;

    public function __construct($id, $type, $action)
    {
        $this->id = $id;
        $this->type = $type;
        $this->action = $action;
    }
}