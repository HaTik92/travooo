<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Http\Responses\ApiResponse;
use ErrorException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $API_DEBUG_MODE = true;
        if ($API_DEBUG_MODE && strpos(url()->current(), '/api/v1/')) {
            try {
                _ApiRequest('debug', '', [
                    'laravel_debugger' => [
                        '__message__'   => $exception->getMessage(),
                        '__class__' => get_class($exception),
                    ]
                ]);
            } catch (\Throwable $e) {
            }
            // return ApiResponse::createServerError($exception);
            if (($exception instanceof TokenMismatchException) || ($exception instanceof AuthenticationException)) {
                return ApiResponse::__createUnAuthorizedResponse();
            } else if (($exception instanceof GeneralException) || ($exception instanceof ErrorException)) {
                return ApiResponse::__createServerError($exception->getMessage());
            } elseif ($exception instanceof PlanPermissionsException) {
                return ApiResponse::__createNoPermissionResponse('This plan is private');
            } else if ($exception instanceof PlanNotExistsException) {
                return ApiResponse::create(["message" => ['This plan is deleted or does not exist']], false, ApiResponse::NOT_FOUND);
            } else if ($exception instanceof NotFoundHttpException) {
                return ApiResponse::create(["message" => ["Page Not Found"]], false, ApiResponse::NOT_FOUND);
            } else if ($exception instanceof MethodNotAllowedException) {
                return ApiResponse::__createServerError("invalid api method");
            } else if ($exception instanceof MaxRejectedTimesException) {
                return ApiResponse::__createServerError("Too many rejected requests");
            } else if ($exception instanceof MethodNotAllowedHttpException) {
                return ApiResponse::__createServerError("invalid api method");
            } elseif ($exception instanceof ModelNotFoundException) {
                $model = explode('\\', $exception->getModel());
                $modelPhrase = ucwords(implode(' ', preg_split('/(?=[A-Z])/', end($model))));
                return ApiResponse::__createServerError(\App::make($exception->getModel())->modelNotFoundMessage ?? "$modelPhrase not found");
            }
            return ApiResponse::createServerError($exception);
        }


        if ($exception instanceof PlanPermissionsException) {
            //return new Response('This plan is private');
            return new RedirectResponse(url('private-plan'));
        }

        if ($exception instanceof PlanNotExistsException) {
            return new RedirectResponse(url('content-deleted'));
        }

        if ($exception instanceof MaxRejectedTimesException) {
            return new JsonResponse([
                'error' => 'Too many rejected requests'
            ]);
        }

        /*
         * Redirect if token mismatch error
         * Usually because user stayed on the same screen too long and their session expired
         */
        if ($exception instanceof TokenMismatchException) {
            return redirect()->route('frontend.auth.login');
        }

        /*
         * All instances of GeneralException redirect back with a flash message to show a bootstrap alert-error
         */
        if ($exception instanceof GeneralException) {
            return redirect()->back()->withInput()->withFlashDanger($exception->getMessage());
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        $currentUrl = url()->current();
        $pos = strpos($currentUrl, '/api/v1/');

        if ($request->expectsJson() or !($pos === false)) {
            return ApiResponse::create(['message' => ["Unauthorized user."]], false, ApiResponse::UNAUTHORIZED);
        }
        $guard = array_get($exception->guards(), 0);
        switch ($guard) {
            case 'user':
                $login = '/login';
                break;
            default:
                $login = '/admin/login';
                break;
        }
        return redirect()->guest($login);
    }
}
