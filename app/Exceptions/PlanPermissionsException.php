<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;

class PlanPermissionsException extends AuthorizationException
{
}