<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;

class PlanNotExistsException extends AuthorizationException
{
}