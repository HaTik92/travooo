<?php

namespace App\Helpers\Backend;

class BestTime
{
    private $raw;
    private $season = [];
    /**
     * @var array
     */
    protected $monthsLongName = [
        "Jan" => 'January',
        "Feb" => 'February',
        "Mar" => 'March',
        "Apr" => 'April',
        "May" => 'May',
        "Jun" => 'June',
        "Jul" => 'July',
        "Aug" => 'August',
        "Sep" => 'September',
        "Oct" => 'October',
        "Nov" => 'November',
        "Dec" => 'December'
    ];

    /**
     * @var array
     */
    protected $colors = [
        'Shoulder' => 'yellow',
        'High Season' => 'red',
        'Low Season' => 'green'
    ];

    /**
     * BestTime constructor.
     * @param $rawData
     */
    public function __construct($rawData)
    {
        $this->raw = $rawData;

        $this->parse();
    }

    /**
     * @return array
     */
    public function getSeason()
    {
        return $this->season;
    }

    protected function parse()
    {
        $this->season = array_flip(array_values($this->monthsLongName));

        $lines = array_values(array_filter(preg_split('/\r\n|\r|\n/', $this->raw)));

        foreach ($lines as $line) {
            list($season, $interval) = explode(':', $line);

            if (isset($this->colors[$season])) {
                $range = $this->parsePeriod($interval);

                foreach ($range as $item) {
                    $this->season[$this->monthsLongName[$item]] = $this->colors[$season];
                }
            }
        }
    }

    /**
     * @param $interval
     *
     * @return array
     */
    protected function parsePeriod($interval)
    {
        $months = [];
        $interval = trim($interval);

        foreach (explode(',', $interval) as $item) {
            if (strpos($item, '-') !== false) {
                list($start, $finish) = explode('-', $item);
                $months = array_merge($months, $this->parseRange($start, $finish));
            } else {
                $months[] = trim($item);
            }
        }

        return $months;
    }

    /**
     * @param $start
     * @param $finish
     *
     * @return array
     */
    protected function parseRange($start, $finish)
    {
        $start = trim($start);
        $finish = trim($finish);
        $map = array_keys($this->monthsLongName);
        $monthIndexMap = array_flip($map);
        $startIndex = $monthIndexMap[$start];
        $finishIndex = $monthIndexMap[$finish];

        if ($startIndex > $finishIndex) {
            return array_merge(
                array_slice($map, $startIndex),
                array_slice($map, 0, $finishIndex + 1)
            );
        } else {
            return array_slice($map, $startIndex, $finishIndex - $startIndex + 1);
        }
    }
}