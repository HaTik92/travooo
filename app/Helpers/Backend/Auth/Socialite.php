<?php

namespace App\Helpers\Backend\Auth;

/**
 * Class Socialite.
 */
class Socialite
{
    /**
     * Generates social login links based on what is enabled.
     *
     * @return string
     */
    public function getSocialLinks()
    {
        $socialiteEnabledProviders = [];
        $socialiteProviders = [
            'BITBUCKET_CLIENT_ID' => [
                'link' => link_to_route(
                    'frontend.auth.social.login',
                    trans('labels.frontend.auth.login_with',
                        ['social_media' => 'Bitbucket']
                    ),
                    'bitbucket'
                )
            ],
            'FACEBOOK_CLIENT_ID' => [
                'link' => link_to_route(
                    'frontend.auth.social.login',
                    trans('labels.frontend.auth.login_with',
                        ['social_media' => 'Facebook']
                    ),
                    'facebook'
                )
            ],
            'GOOGLE_CLIENT_ID' => [
                'link' => link_to_route(
                    'frontend.auth.social.login',
                    trans('labels.frontend.auth.login_with',
                        ['social_media' => 'Google']
                    ),
                    'google'
                )
            ],
            'GITHUB_CLIENT_ID' => [
                'link' => link_to_route(
                    'frontend.auth.social.login',
                    trans('labels.frontend.auth.login_with',
                        ['social_media' => 'GitHub']
                    ),
                    'github'
                )
            ],
            'LINKEDIN_CLIENT_ID' => [
                'link' => link_to_route(
                    'frontend.auth.social.login',
                    trans('labels.frontend.auth.login_with',
                        ['social_media' => 'LinkedIn']
                    ),
                    'linkedin'
                )
            ],
            'TWITTER_CLIENT_ID' => [
                'link' => link_to_route(
                    'frontend.auth.social.login',
                    trans('labels.frontend.auth.login_with',
                        ['social_media' => 'Twitter']
                    ),
                    'twitter'
                )
            ]
        ];

        foreach ($socialiteProviders as $providerID => $socialiteProvider) {
            !getenv($providerID) ?:
                array_push(
                    $socialiteEnabledProviders, $socialiteProvider['link']
                );
        }

        return implode(' | ', $socialiteEnabledProviders);
    }

    /**
     * List of the accepted third party provider types to login with.
     *
     * @return array
     */
    public function getAcceptedProviders()
    {
        return [
            'bitbucket',
            'facebook',
            'google',
            'github',
            'linkedin',
            'twitter',
        ];
    }
}
