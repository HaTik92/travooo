<?php

namespace App\Helpers\Backend\Fractal;

use \League\Fractal\Manager AS FractalManager;

class Manager extends FractalManager
{
    /**
     * Manager constructor.
     * @param ScopeFactoryInterface|null $scopeFactory
     */
    public function __construct(ScopeFactoryInterface $scopeFactory = null)
    {
        parent::__construct(new ScopeFactory());
    }

    /**
     * @param array|string $includes
     * @return $this
     */
    public function parseIncludes($includes)
    {
        // Wipe these before we go again
        $this->requestedIncludes = $this->includeParams = [];

        if (is_string($includes)) {
            $includes = explode(',', $includes);
        }

        if (! is_array($includes)) {
            throw new \InvalidArgumentException(
                'The parseIncludes() method expects a string or an array. '.gettype($includes).' given'
            );
        }

        foreach ($includes as $include) {
            list($includeName, $allModifiersStr) = array_pad(explode(':', $include, 2), 2, null);

            // Trim it down to a cool level of recursion
            $includeName = $this->trimToAcceptableRecursionLevel($includeName);

            if (in_array($includeName, $this->requestedIncludes)) {
                continue;
            }
            $this->requestedIncludes[] = $includeName;

            // No Params? Bored
            if ($allModifiersStr === null) {
                continue;
            }

            $this->includeParams[$includeName] = [
                'only' => explode($this->paramDelimiter, $allModifiersStr)
            ];
        }

        // This should be optional and public someday, but without it includes would never show up
        $this->autoIncludeParents();

        return $this;
    }
}