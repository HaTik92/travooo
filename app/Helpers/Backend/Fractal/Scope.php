<?php

namespace App\Helpers\Backend\Fractal;


class Scope extends \League\Fractal\Scope
{
    /**
     * Convert the current data for this scope to an array.
     *
     * @return array
     */
    public function toArray()
    {
        list($rawData, $rawIncludedData) = $this->executeResourceTransformers();

        $serializer = $this->manager->getSerializer();

        $rawData = $this->dataFilter($rawData);

        $data = $this->serializeResource($serializer, $rawData);

        // If the serializer wants the includes to be side-loaded then we'll
        // serialize the included data and merge it with the data.
        if ($serializer->sideloadIncludes()) {
            //Filter out any relation that wasn't requested
            $rawIncludedData = array_map(array($this, 'filterFieldsets'), $rawIncludedData);

            $includedData = $serializer->includedData($this->resource, $rawIncludedData);

            // If the serializer wants to inject additional information
            // about the included resources, it can do so now.
            $data = $serializer->injectData($data, $rawIncludedData);

            if ($this->isRootScope()) {
                // If the serializer wants to have a final word about all
                // the objects that are sideloaded, it can do so now.
                $includedData = $serializer->filterIncludes(
                    $includedData,
                    $data
                );
            }

            $data = array_merge($data, $includedData);
        }

        if ($this->resource instanceof Collection) {
            if ($this->resource->hasCursor()) {
                $pagination = $serializer->cursor($this->resource->getCursor());
            } elseif ($this->resource->hasPaginator()) {
                $pagination = $serializer->paginator($this->resource->getPaginator());
            }

            if (! empty($pagination)) {
                $this->resource->setMetaValue(key($pagination), current($pagination));
            }
        }

        // Pull out all of OUR metadata and any custom meta data to merge with the main level data
        $meta = $serializer->meta($this->resource->getMeta());

        // in case of returning NullResource we should return null and not to go with array_merge
        if (is_null($data)) {
            if (!empty($meta)) {
                return $meta;
            }
            return null;
        }

        return array_merge($data, $meta);
    }

    /**
     * @param array $data
     *
     * @return array $result
     */
    protected function dataFilter($data)
    {
        $currentInclude = implode('.',
            array_filter(
                array_merge(
                    $this->getParentScopes(),
                    [ $this->getScopeIdentifier() ]
                )
            )
        );
        $dataParams = $this->manager->getIncludeParams($currentInclude);

        if(empty($dataParams['only'])) {
            return $data;
        }

        $only = $dataParams['only'];

        foreach ($this->getManager()->getRequestedIncludes() as $include) {
            if (str_is($currentInclude . '.*', $include)) {
                $only[] = str_replace($currentInclude . '.', '', $include);
            }
        }

        $result = [];

        if (isset($data[0]) && is_array($data[0])) {
            foreach ($data as $index => $item) {
                foreach ($item as $field => $value) {
                    if (in_array($field, $only)) {
                        $result[$index][$field] = $value;
                    }
                }
            }
        } else {
            foreach ($data as $field => $value) {
                if (in_array($field, $only)) {
                    $result[$field] = $value;
                }
            }
        }

        return $result;
    }
}