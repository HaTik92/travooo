<?php

namespace App\Helpers\Backend\Fractal;

use League\Fractal\Resource\ResourceInterface;
use League\Fractal\Manager as FractalManager;
use League\Fractal\Scope as FractalScope;
use League\Fractal\ScopeFactoryInterface as FractalScopeFactoryInterface;

class ScopeFactory implements FractalScopeFactoryInterface
{
    /**
     * @param FractalManager $manager
     * @param ResourceInterface $resource
     * @param string|null $scopeIdentifier
     * @return Scope
     */
    public function createScopeFor(FractalManager $manager, ResourceInterface $resource, $scopeIdentifier = null)
    {
        return new Scope($manager, $resource, $scopeIdentifier);
    }

    /**
     * @param FractalManager $manager
     * @param FractalScope $parentScopeInstance
     * @param ResourceInterface $resource
     * @param string|null $scopeIdentifier
     * @return Scope
     */
    public function createChildScopeFor(FractalManager $manager, FractalScope $parentScopeInstance, ResourceInterface $resource, $scopeIdentifier = null)
    {
        $scopeInstance = $this->createScopeFor($manager, $resource, $scopeIdentifier);

        // This will be the new children list of parents (parents parents, plus the parent)
        $scopeArray = $parentScopeInstance->getParentScopes();
        $scopeArray[] = $parentScopeInstance->getScopeIdentifier();

        $scopeInstance->setParentScopes($scopeArray);

        return $scopeInstance;
    }
}