<?php

namespace App\Helpers\Backend\Fractal;

use League\Fractal\Serializer\ArraySerializer;

class CustomArraySerializer extends ArraySerializer
{
    /**
     * Serialize a collection.
     *
     * @param string $resourceKey
     * @param array  $data
     *
     * @return array
     */
    public function collection($resourceKey = null, array $data)
    {
        if ($resourceKey === false) {
            return $data;
        } else {
            return ['data' => $data];
        }
    }

    /**
     * Serialize an item.
     *
     * @param string $resourceKey
     * @param array  $data
     *
     * @return array
     */
    public function item($resourceKey = null, array $data)
    {
        if ($resourceKey === false) {
            return $data;
        } else {
            return ['data' => $data];
        }
    }

    /**
     * Serialize null resource.
     *
     * @return array
     */
    public function null()
    {
        return ['data' => []];
    }
}