<?php

namespace App\Helpers\Backend\Weather;

use \GuzzleHttp\Client;

class Weather
{
    private $lat;
    private $lng;
    private $language;
    private $locationKey;

    /**
     * Weather constructor.
     * @param $lat
     * @param $lng
     * @param $language
     */
    public function __construct($lat, $lng, $language)
    {
        $this->lat = $lat;
        $this->lng = $lng;
        $this->language = $language;
        $this->locationKey = $this->getLocationKey($this->lat, $this->lng);
    }

    /**
     * @param $lat
     * @param $lng
     *
     * @return mixed
     */
    protected function getLocationKey($lat, $lng)
    {
        $client   = new Client();

        $response = $client->get(
            'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search',
            [
                'query' => [
                    'apikey' => config('weather.apikey'),
                    'q' => $lat . "," . $lng
                ]
            ]
        );

        if ($response->getStatusCode() == 200) {
            $data = json_decode((string)$response->getBody(), true);

            return $data['Key'];
        }
    }

    /**
     * @return bool|string
     */
    public function getDailyForecast()
    {
        $client = new Client();

        try {
            $response = $client->get(
                'http://dataservice.accuweather.com/forecasts/v1/hourly/24hour/'.$this->locationKey,
                [
                    'query' => [
                        'apikey' => config('weather.apikey'),
                        'language' => $this->language
                    ]
                ]
            );

            if ($response->getStatusCode() == 200) {
                $data = json_decode((string)$response->getBody(), true);

                return json_encode($data);
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return bool|string
     */
    public function getUpcomingForecast()
    {
        $client   = new Client();

        try {
            $response = $client->get(
                'http://dataservice.accuweather.com/forecasts/v1/daily/10day/'.$this->locationKey,
                [
                    'query' => [
                        'apikey' => config('weather.apikey'),
                        'language' => $this->language
                    ]
                ]
            );

            if ($response->getStatusCode() == 200) {
                $data = json_decode((string)$response->getBody(), true);

                return json_encode($data);
            }
        } catch (\Exception $e) {
            return false;
        }
    }
}