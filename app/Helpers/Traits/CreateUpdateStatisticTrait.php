<?php
namespace App\Helpers\Traits;

trait CreateUpdateStatisticTrait {

    function updateStatistic($model, $column, $value){
        if(!$model->statistics) {
            $model->statistics()->create([
                $column => $value
            ]);
        } else {
            $model->statistics()->update([
                $column => $value
            ]);
        }
    }
}