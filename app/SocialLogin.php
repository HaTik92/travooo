<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLogin extends Model
{
    protected $fillable = ['user_id', 'provider_id', 'provider', 'token', 'avatar'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
