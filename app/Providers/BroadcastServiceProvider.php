<?php

namespace App\Providers;

use App\Models\User\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;

/**
 * Class BroadcastServiceProvider.
 */
class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (request()->hasHeader('authorization')) {
            Broadcast::routes(['middleware' => 'auth:api']);
        } else {
            Broadcast::routes();
        }

        require base_path('routes/channels.php');

        /*
         * Authenticate the user's personal channel...
         */
        Broadcast::channel('chat-channel.{chatroomId}', function ($user, $chatroomId) {
            return (int)$user->id === (int)$chatroomId;
        });

        //This is used as public channel for typing indicator as whisperer is not working (some kind of bug in laravel-echo-server)
        Broadcast::channel('chat-typing.{chatroomId}', function ($user, $chatroomId) {
            return true;
        });

        Broadcast::channel('notifications-channel.{userId}', function ($user, $userId) {
            return (int)$user->id === (int)$userId;
        });

        Broadcast::channel('plan-logs-channel.{userId}', function ($user, $userId) {
            return (int)$user->id === (int)$userId;
        });

        Broadcast::channel('new-test-channel.{userId}', function (User $user,  $userId) {
            return (int)$user->id === (int)$userId;
        });
    }
}
