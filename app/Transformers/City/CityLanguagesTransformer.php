<?php
namespace App\Transformers\City;

use App\Models\Access\language\Languages;
use League\Fractal\TransformerAbstract;

class CityLanguagesTransformer extends TransformerAbstract
{
    public function transform($languages)
    {
        if (!empty($languages)) {
            return [
                'id'                  => $languages->id,
                'languages_spoken_id' => $languages->languages_spoken_id,
                'languages_id'        => $languages->languages_id,
                'title'               => $languages->title,
                'description'         => $languages->description,
                'type'                => $languages->type
            ];
        } else {
            return [];
        }
    }
}