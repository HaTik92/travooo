<?php
namespace App\Transformers\City;

use App\Models\EmergencyNumbers\EmergencyNumbers;
use League\Fractal\TransformerAbstract;

class CityEmergencyNumbersTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(EmergencyNumbers $cities)
    {
        return $this->collection($cities->trans, new CityEmergencyNumbersTranslateTransformer(), false);
    }

    public function transform(EmergencyNumbers $citiesEmergencyNumbers)
    {
        return array_except($citiesEmergencyNumbers->toArray(), []);
    }
}