<?php
namespace App\Transformers\City;

use App\Models\Currencies\CurrenciesTranslations;
use League\Fractal\TransformerAbstract;

class CityCurrenciesTranslationTransformer extends TransformerAbstract
{
    public function transform(CurrenciesTranslations $citiesCurrencies)
    {
        return array_except($citiesCurrencies->toArray(), []);
    }
}