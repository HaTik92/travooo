<?php
namespace App\Transformers\City;

use App\Models\EmergencyNumbers\EmergencyNumbersTranslations;
use League\Fractal\TransformerAbstract;
use Mews\Purifier\Facades\Purifier;

class CityEmergencyNumbersTranslateTransformer extends TransformerAbstract
{
    public function transform(EmergencyNumbersTranslations $citiesEmergencyNumbers)
    {
        $citiesEmergencyNumbers->description = Purifier::clean($citiesEmergencyNumbers->description, ['HTML.Allowed' => '']);
        return array_except($citiesEmergencyNumbers->toArray(), []);
    }
}