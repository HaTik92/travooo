<?php
/**
 * Created by PhpStorm.
 * User: dvin
 * Date: 26/06/2018
 * Time: 11:12
 */

namespace App\Transformers\City;

use App\Models\City\CitiesWeather;
use League\Fractal\TransformerAbstract;

class CityWeatherTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(CitiesWeather $weather)
    {
        return $this->collection($weather->trans, new CityWeatherTranslateTransformer(), false);
    }

    public function transform(CitiesWeather $citiesWeather)
    {
        return array_except($citiesWeather->toArray(), ['trans']);
    }
}