<?php
namespace App\Transformers\City;

use App\Models\City\Cities;
use App\Models\City\CitiesTimezones;
use App\Models\City\CitiesWeather;
use App\Models\Country\Countries;
use App\Transformers\Country\CountryCurrenciesTransformer;
use App\Transformers\Country\CountryEmergencyNumbersTransformer;
use App\Transformers\Country\CountryHolidaysTransformer;
use App\Transformers\Country\CountryReligionsTransformer;
use App\Transformers\Country\CountryTransformer;
use App\Transformers\Media\MediaTransformer;
use App\Transformers\Place\PlacesCityTransformer;
use League\Fractal\TransformerAbstract;

class CityTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans',
        'medias',
        'places',
        'discussions',
        'country',
        'languages',
        'currencies',
        'religions',
        'holidays',
        'emergency',
        'timezone'
    ];

    public function includeTrans(Cities $cities)
    {
        return $this->collection($cities->trans, new CityTranslateTransformer(), false);
    }

    public function includeMedias(Cities $cities)
    {
        return $this->collection($cities->getMedias, new MediaTransformer(), false);
    }

    public function includePlaces(Cities $cities)
    {
        return $this->collection($cities->places, new PlacesCityTransformer(), false);
    }

    public function includeDiscussions(Cities $cities)
    {
        return $this->collection($cities->discussions, new CityDiscussionsTransformer(), false);
    }

    public function includeCountry(Cities $cities)
    {
        return $this->item($cities->country, new CountryTransformer(), false);
    }

    public function includeLanguages(Cities $cities)
    {
        $CitiesLang = [];
        $CountyLang = [];
        foreach ($cities->languages as $item) {
            if ($item->type == 'city') {
                array_push($CitiesLang, $item);
            } else {
                array_push($CountyLang, $item);
            }
        }

        return $this->collection(
            !empty($CitiesLang) ? $CitiesLang : $CountyLang,
            new CityLanguagesTransformer(),
            false
        );
    }

    public function includeCurrencies(Cities $cities)
    {
        $citiesCurrencies = $this->collection($cities->currencies, new CityCurrenciesTransformer(), false);
        if (!empty($citiesCurrencies->getData()->all())) {
            return $citiesCurrencies;
        } else {
            $country = Countries::with(['currencies.trans'])
                ->where('id', $cities->countries_id)
                ->where('active', 1)
                ->first();
            return $this->collection($country->currencies, new CountryCurrenciesTransformer(), false);
        }
    }

    public function includeReligions(Cities $cities)
    {
        $citiesReligions = $this->collection($cities->religions, new CityReligionsTransformer(), false);

        if (!empty($citiesReligions->getData()->all())) {
            return $citiesReligions;
        } else {
            $country = Countries::with(['religions.trans'])
                ->where('id', $cities->countries_id)
                ->where('active', 1)
                ->first();
            return $this->collection($country->religions, new CountryReligionsTransformer(), false);
        }
    }

    public function includeHolidays(Cities $cities)
    {
        $country = Countries::with(['holidays.trans'])
            ->where('id', $cities->countries_id)
            ->where('active', 1)
            ->first();
        return $this->collection($country->holidays, new CountryHolidaysTransformer(), false);
    }

    public function includeEmergency(Cities $cities)
    {
        $citiesEmergency = $this->collection($cities->emergency, new CityEmergencyNumbersTransformer(), false);

        if (!empty($citiesEmergency->getData()->all())) {
            return $citiesEmergency;
        } else {
            $country = Countries::with(['emergency.trans'])
                ->where('id', $cities->countries_id)
                ->where('active', 1)
                ->first();
            return $this->collection($country->emergency, new CountryEmergencyNumbersTransformer(), false);
        }
    }

    public function includeTimezone(Cities $cities)
    {
        return $this->item(!empty($cities->timezone) ? $cities->timezone : new CitiesTimezones(), new CityTimezonesTransformer(), false);
    }

    public function includeWeather(Cities $cities)
    {
        return $this->item(!empty($cities->weather) ? $cities->weather : new CitiesWeather(), new CityWeatherTransformer(), false);
    }

    public function transform(Cities $cities)
    {
        $country = Countries::where('id', $cities->countries_id)
                            ->where('active', 1)
                            ->first();
        $cities->code = $cities->code ? $cities->code : $country->code;

        return array_except($cities->toArray(), ['trans', 'get_medias', 'country']);
    }
}