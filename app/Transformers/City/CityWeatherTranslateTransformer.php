<?php

namespace App\Transformers\City;

use App\Models\City\CitiesWeatherTranslations;
use League\Fractal\TransformerAbstract;

class CityWeatherTranslateTransformer extends TransformerAbstract
{
    public function transform(CitiesWeatherTranslations $citiesWeather)
    {
        return [
            'daily' => json_decode($citiesWeather->daily_forecast),
            'upcoming' => json_decode($citiesWeather->upcoming_forecast)
        ];
    }
}