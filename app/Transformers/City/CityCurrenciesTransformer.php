<?php
namespace App\Transformers\City;

use App\Models\Currencies\Currencies;
use League\Fractal\TransformerAbstract;

class CityCurrenciesTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(Currencies $cities)
    {
        return $this->collection($cities->trans, new CityCurrenciesTranslationTransformer(), false);
    }

    public function transform(Currencies $citiesCurrencies)
    {
        return array_except($citiesCurrencies->toArray(), []);
    }
}