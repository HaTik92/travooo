<?php
namespace App\Transformers\City;

use App\Models\Holidays\HolidaysTranslations;
use League\Fractal\TransformerAbstract;

class CityHolidaysTranslationTransformer extends TransformerAbstract
{
    public function transform(HolidaysTranslations $citiesHolidaysTrans)
    {
        return array_except($citiesHolidaysTrans->toArray(), []);
    }
}