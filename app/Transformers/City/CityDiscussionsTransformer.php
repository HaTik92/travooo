<?php
namespace App\Transformers\City;

use App\Models\City\CitiesDiscussions;
use App\Transformers\User\UserTransformer;
use League\Fractal\TransformerAbstract;

class CityDiscussionsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'UpVotes',
        'DownVotes',
        'users'
    ];

    public function includeUpVotes(CitiesDiscussions $citiesDiscussions)
    {
        return $this->collection($citiesDiscussions->upVotes, new CityDiscussionsUpvotesTransformer(), false);
    }

    public function includeDownVotes(CitiesDiscussions $citiesDiscussions)
    {
        return $this->collection($citiesDiscussions->downVotes, new CityDiscussionsDownvotesTransformer(), false);
    }

    public function includeUsers(CitiesDiscussions $citiesDiscussions)
    {
        return $this->item($citiesDiscussions->users, new UserTransformer(), false);
    }

    public function transform(CitiesDiscussions $citiesDiscussions)
    {
        return array_except($citiesDiscussions->toArray(), ['up_votes', 'down_votes']);
    }
}