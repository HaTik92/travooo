<?php
namespace App\Transformers\City;

use App\Models\Holidays\Holidays;
use League\Fractal\TransformerAbstract;

class CityHolidaysTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(Holidays  $cities)
    {
        return $this->collection($cities->trans, new CityHolidaysTranslationTransformer(), false);
    }

    public function transform(Holidays  $citiesHolidays)
    {
        return array_except($citiesHolidays->toArray(), []);
    }
}