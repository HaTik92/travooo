<?php
namespace App\Transformers\City;

use App\Models\Religion\ReligionTranslations;
use League\Fractal\TransformerAbstract;

class CityReligionsTranslationTransformer extends TransformerAbstract
{
    public function transform(ReligionTranslations $citiesReligions)
    {
        return array_except($citiesReligions->toArray(), []);
    }
}