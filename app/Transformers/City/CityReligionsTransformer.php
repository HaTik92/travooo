<?php
namespace App\Transformers\City;

use App\Models\Religion\Religion;
use League\Fractal\TransformerAbstract;

class CityReligionsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(Religion  $cities)
    {
        return $this->collection($cities->trans, new CityReligionsTranslationTransformer(), false);
    }

    public function transform(Religion $citiesReligions)
    {
        return array_except($citiesReligions->toArray(), []);
    }
}