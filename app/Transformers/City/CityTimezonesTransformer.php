<?php
namespace App\Transformers\City;

use App\Models\City\CitiesTimezones;
use League\Fractal\TransformerAbstract;

class CityTimezonesTransformer extends TransformerAbstract
{
    public function transform(CitiesTimezones $citiesTimezones)
    {
        return array_except($citiesTimezones->toArray(), []);
    }
}