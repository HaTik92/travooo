<?php
namespace App\Transformers\City;

use App\Helpers\Backend\BestTime;
use App\Models\City\Cities;
use App\Models\City\CitiesTranslations;
use App\Transformers\Country\CountryTransformer;
use League\Fractal\TransformerAbstract;
use Mews\Purifier\Facades\Purifier;

class CityTranslateTransformer extends TransformerAbstract
{
    public function transform(CitiesTranslations $cityTranslation)
    {
        $languageId = $cityTranslation->languages_id;
        $cityId     = $cityTranslation->cities_id;
        $country    = Cities::with([
            'country.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            }
        ])
            ->where('id', $cityId)
            ->where('active', 1)
            ->first();
        $CountryData        = $this->item($country->country, new CountryTransformer(), false);
        $CountryTranslation = $CountryData->getData()->trans[0];

        $cityTranslation->description = Purifier::clean($cityTranslation->description, ['HTML.Allowed' => '']);
        $bestTime = new BestTime($cityTranslation->best_time);

        return array_except(
            array_merge(
                $cityTranslation->toArray(),
                [
                    'best_time'         => $bestTime->getSeason(),
                    'working_days'      => $cityTranslation->working_days ? $cityTranslation->working_days : $CountryTranslation->working_days,
                    'transportation'    => $cityTranslation->transportation ? $cityTranslation->transportation : $CountryTranslation->transportation,
                    'speed_limit'       => $cityTranslation->speed_limit ? $cityTranslation->speed_limit : $CountryTranslation->speed_limit,
                    'internet'          => $cityTranslation->internet ? $cityTranslation->internet : $CountryTranslation->internet,
                    'etiquette'         => $cityTranslation->etiquette ? $cityTranslation->etiquette : $CountryTranslation->etiquette,
                    'restrictions'      => $cityTranslation->economy ? $cityTranslation->economy : $CountryTranslation->economy,
                    'potential_dangers' => $cityTranslation->potential_dangers ? $cityTranslation->potential_dangers : $CountryTranslation->potential_dangers,
                    'health_notes'      => $cityTranslation->health_notes ? $cityTranslation->health_notes : $CountryTranslation->health_notes,
                    'sockets'           => $cityTranslation->sockets ? $cityTranslation->sockets : $CountryTranslation->sockets,
                    'planning_tips'     => $cityTranslation->planning_tips ? $cityTranslation->planning_tips : $CountryTranslation->planning_tips,
                    'metrics'           => $CountryTranslation->metrics,
                    'indexes'           => [
                        'crime_rate'      => $cityTranslation->geo_stats ? $cityTranslation->geo_stats : $CountryTranslation->geo_stats,
                        'quality_of_life' => $cityTranslation->demographics ? $cityTranslation->demographics : $CountryTranslation->demographics,
                        'pollution'       => $cityTranslation->pollution_index ? $cityTranslation->pollution_index : $CountryTranslation->pollution_index,
                        'daily_costs'     => $cityTranslation->best_time ? $cityTranslation->best_time : $CountryTranslation->best_time
                    ],
                ]
            ),
            [
                 'cities_id',
                 'economy',
                 'geo_stats',
                 'demographics',
                 'pollution_index'
            ]
        );
    }
}