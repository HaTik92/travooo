<?php
namespace App\Transformers\City;

use App\Models\City\CitiesDiscussions;
use League\Fractal\TransformerAbstract;

class CityDiscussionsUpvotesTransformer extends TransformerAbstract
{
    public function transform(CitiesDiscussions $citiesDiscussions)
    {
        return array_except($citiesDiscussions->toArray(), []);
    }
}