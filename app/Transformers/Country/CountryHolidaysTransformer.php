<?php
namespace App\Transformers\Country;

use App\Models\Holidays\Holidays;
use League\Fractal\TransformerAbstract;

class CountryHolidaysTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(Holidays  $countries)
    {
        return $this->collection($countries->trans, new CountryHolidaysTranslationTransformer(), false);
    }

    public function transform(Holidays  $countriesHolidays)
    {
        return array_except($countriesHolidays->toArray(), []);
    }
}