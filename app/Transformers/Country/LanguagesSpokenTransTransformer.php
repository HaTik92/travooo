<?php
namespace App\Transformers\Country;

use App\Models\LanguagesSpoken\LanguagesSpokenTranslation;
use League\Fractal\TransformerAbstract;

class LanguagesSpokenTransTransformer extends TransformerAbstract
{
    public function transform(LanguagesSpokenTranslation $countriesLanguages)
    {
        return array_except($countriesLanguages->toArray(), []);
    }
}