<?php
namespace App\Transformers\Country;

use App\Models\Country\CountriesCapitals;
use App\Transformers\City\CityTransformer;
use League\Fractal\TransformerAbstract;

class CountryCapitalsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'city'
    ];

    public function includeCity(CountriesCapitals  $countries)
    {
        return $this->item($countries->city, new CityTransformer(), false);
    }

    public function transform(CountriesCapitals  $countriesCapitals)
    {
        return array_except($countriesCapitals->toArray(), []);
    }
}