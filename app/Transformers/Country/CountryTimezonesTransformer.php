<?php
namespace App\Transformers\Country;

use App\Models\Country\CountriesTimezones;
use League\Fractal\TransformerAbstract;

class CountryTimezonesTransformer extends TransformerAbstract
{
    public function transform(CountriesTimezones $countriesTimezones)
    {
        return array_except($countriesTimezones->toArray(), []);
    }
}