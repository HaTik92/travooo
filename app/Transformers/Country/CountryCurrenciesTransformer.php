<?php
namespace App\Transformers\Country;

use App\Models\Currencies\Currencies;
use League\Fractal\TransformerAbstract;

class CountryCurrenciesTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(Currencies $countries)
    {
        return $this->collection($countries->trans, new CountryCurrenciesTranslationTransformer(), false);
    }

    public function transform(Currencies $countriesCurrencies)
    {
        return array_except($countriesCurrencies->toArray(), []);
    }
}