<?php
namespace App\Transformers\Country;

use App\Models\Country\CountriesTranslations;
use League\Fractal\TransformerAbstract;
use Mews\Purifier\Facades\Purifier;

class CountryTranslateTransformer extends TransformerAbstract
{

    public function transform(CountriesTranslations $translation)
    {
        $translation->description = Purifier::clean($translation->description, ['HTML.Allowed' => '']);
        return array_except(
            array_merge(
                $translation->toArray(),
                [
                    'crime_rate' => $translation->geo_stats,
                    'quality_of_life' => $translation->demographics,
                    'restrictions' => $translation->economy
                ]
            ),
            [
                'economy',
                'geo_stats',
                'demographics'
            ]
        );
    }
}