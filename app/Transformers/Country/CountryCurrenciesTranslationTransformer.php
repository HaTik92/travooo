<?php
namespace App\Transformers\Country;

use App\Models\Currencies\CurrenciesTranslations;
use League\Fractal\TransformerAbstract;

class CountryCurrenciesTranslationTransformer extends TransformerAbstract
{
    public function transform(CurrenciesTranslations $countriesCurrencies)
    {
        return array_except($countriesCurrencies->toArray(), []);
    }
}