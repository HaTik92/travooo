<?php
namespace App\Transformers\Country;

use App\Models\Religion\ReligionTranslations;
use League\Fractal\TransformerAbstract;

class CountryReligionsTranslationTransformer extends TransformerAbstract
{
    public function transform(ReligionTranslations $countriesReligions)
    {
        return array_except($countriesReligions->toArray(), []);
    }
}