<?php
namespace App\Transformers\Country;

use App\Models\EmergencyNumbers\EmergencyNumbers;
use League\Fractal\TransformerAbstract;

class CountryEmergencyNumbersTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(EmergencyNumbers $countries)
    {
        return $this->collection($countries->trans, new CountryEmergencyNumbersTranslateTransformer(), false);
    }

    public function transform(EmergencyNumbers $countriesEmergencyNumbers)
    {
        return array_except($countriesEmergencyNumbers->toArray(), []);
    }
}