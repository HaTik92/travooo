<?php
namespace App\Transformers\Country;

use App\Models\Country\CountriesDiscussions;
use App\Transformers\User\UserTransformer;
use League\Fractal\TransformerAbstract;

class CountryDiscussionsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'UpVotes',
        'DownVotes',
        'users'
    ];

    public function includeUpVotes(CountriesDiscussions $countriesDiscussions)
    {
        return $this->collection($countriesDiscussions->upVotes, new CountryDiscussionsUpvotesTransformer(), false);
    }

    public function includeDownVotes(CountriesDiscussions $countriesDiscussions)
    {
        return $this->collection($countriesDiscussions->downVotes, new CountryDiscussionsDownvotesTransformer(), false);
    }

    public function includeUsers(CountriesDiscussions $countriesDiscussions)
    {
        return $this->item($countriesDiscussions->users, new UserTransformer(), false);
    }

    public function transform(CountriesDiscussions $countriesDiscussions)
    {
        return array_except($countriesDiscussions->toArray(), ['up_votes', 'down_votes']);
    }
}