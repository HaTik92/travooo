<?php
namespace App\Transformers\Country;

use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\LanguagesSpoken\LanguagesSpokenTranslation;
use League\Fractal\TransformerAbstract;

class CountryLanguagesSpokenTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(LanguagesSpoken $countries)
    {
        return $this->item(empty($countries->trans) ? new LanguagesSpokenTranslation() : $countries->trans,
            new LanguagesSpokenTransTransformer(),
            false
        );
    }

    public function transform($languages)
    {
        if (!empty($languages)) {
            return [
                'id'                  => $languages->id,
                'languages_spoken_id' => $languages->languages_spoken_id,
                'languages_id'        => $languages->languages_id,
                'title'               => $languages->title,
                'description'         => $languages->description
            ];
        } else {
            return [];
        }
    }
}