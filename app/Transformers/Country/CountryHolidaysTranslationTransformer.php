<?php
namespace App\Transformers\Country;

use App\Models\Holidays\HolidaysTranslations;
use League\Fractal\TransformerAbstract;

class CountryHolidaysTranslationTransformer extends TransformerAbstract
{
    public function transform(HolidaysTranslations $countriesHolidaysTrans)
    {
        return array_except($countriesHolidaysTrans->toArray(), []);
    }
}