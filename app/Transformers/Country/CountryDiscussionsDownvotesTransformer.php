<?php
namespace App\Transformers\Country;

use App\Models\Country\CountriesDiscussions;
use League\Fractal\TransformerAbstract;

class CountryDiscussionsDownvotesTransformer extends TransformerAbstract
{
    public function transform(CountriesDiscussions $countriesDiscussions)
    {
        return array_except($countriesDiscussions->toArray(), []);
    }
}