<?php
namespace App\Transformers\Country;

use App\Models\EmergencyNumbers\EmergencyNumbersTranslations;
use League\Fractal\TransformerAbstract;
use Mews\Purifier\Facades\Purifier;

class CountryEmergencyNumbersTranslateTransformer extends TransformerAbstract
{
    public function transform(EmergencyNumbersTranslations $countriesEmergencyNumbers)
    {
        $countriesEmergencyNumbers->description = Purifier::clean($countriesEmergencyNumbers->description, ['HTML.Allowed' => '']);
        return array_except($countriesEmergencyNumbers->toArray(), []);
    }
}