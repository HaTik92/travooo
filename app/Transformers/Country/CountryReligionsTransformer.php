<?php
namespace App\Transformers\Country;

use App\Models\Religion\Religion;
use League\Fractal\TransformerAbstract;

class CountryReligionsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(Religion  $countries)
    {
        return $this->collection($countries->trans, new CountryReligionsTranslationTransformer(), false);
    }

    public function transform(Religion $countriesReligions)
    {
        return array_except($countriesReligions->toArray(), []);
    }
}