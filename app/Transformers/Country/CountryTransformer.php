<?php
namespace App\Transformers\Country;

use App\Models\Country\Countries;
use App\Models\Country\CountriesTimezones;
use App\Transformers\City\CityTransformer;
use App\Transformers\Media\MediaTransformer;
use App\Transformers\Place\PlacesCountryTransformer;
use App\Transformers\Region\RegionTransformer;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans',
        'cities',
        'medias',
        'places',
        'discussions',
        'region',
        'languages',
        'holidays',
        'religions',
        'currencies',
        'capitals',
        'emergency',
        'timezone'
    ];

    public function includeTrans(Countries $countries)
    {
        return $this->collection($countries->trans, new CountryTranslateTransformer(), false);
    }

    public function includeCities(Countries $countries)
    {
        return $this->collection($countries->cities, new CityTransformer(), false);
    }

    public function includeMedias(Countries $countries)
    {
        return $this->collection($countries->getMedias, new MediaTransformer(), false);
    }

    public function includePlaces(Countries $countries)
    {
        return $this->collection($countries->places, new PlacesCountryTransformer(), false);
    }

    public function includeDiscussions(Countries $countries)
    {
        return $this->collection($countries->discussions, new CountryDiscussionsTransformer(), false);
    }

    public function includeRegion(Countries $countries)
    {
        return $this->item($countries->region, new RegionTransformer(), false);
    }

    public function includeLanguages(Countries $countries)
    {
        return $this->collection($countries->languages, new CountryLanguagesSpokenTransformer(),false);
    }

    public function includeHolidays(Countries $countries)
    {
        return $this->collection($countries->holidays, new CountryHolidaysTransformer(), false);
    }

    public function includeReligions(Countries $countries)
    {
        return $this->collection($countries->religions, new CountryReligionsTransformer(), false);
    }

    public function includeCurrencies(Countries $countries)
    {
        return $this->collection($countries->currencies, new CountryCurrenciesTransformer(), false);
    }

    public function includeCapitals(Countries $countries)
    {
        return $this->collection($countries->capitals, new CountryCapitalsTransformer(), false);
    }

    public function includeEmergency(Countries $countries)
    {
        return $this->collection($countries->emergency, new CountryEmergencyNumbersTransformer(), false);
    }

    public function includeTimezone(Countries $countries)
    {
        return $this->item(!empty($countries->timezone) ? $countries->timezone : new CountriesTimezones(), new CountryTimezonesTransformer(), false);
    }

    public function transform(Countries $countries)
    {
        return array_except($countries->toArray(), ['trans', 'get_medias']);
    }
}