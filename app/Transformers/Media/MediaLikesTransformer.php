<?php
namespace App\Transformers\Media;

use App\Models\ActivityMedia\MediasLikes;
use League\Fractal\TransformerAbstract;

class MediaLikesTransformer extends TransformerAbstract
{
    public function transform(MediasLikes $mediasLikes)
    {
        return array_except($mediasLikes->toArray(), []);
    }
}