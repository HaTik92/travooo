<?php
namespace App\Transformers\Media;

use App\Models\Place\Medias;
use App\Transformers\User\UserTransformer;
use League\Fractal\TransformerAbstract;

class MediaTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'users'
    ];

    public function includeUsers(Medias $medias)
    {
        return $this->collection($medias->users, new UserTransformer(), false);
    }

    public function transform(Medias $medias)
    {
        return array_except($medias->toArray(), ['trans', 'pivot']);
    }
}