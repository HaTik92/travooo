<?php
namespace App\Transformers\Media;

use App\Models\ActivityMedia\MediasCommentsLikes;
use League\Fractal\TransformerAbstract;

class MediaCommetnsLikesTransformer extends TransformerAbstract
{
    public function transform(MediasCommentsLikes $mediasCommentsLikes)
    {
        return array_except($mediasCommentsLikes->toArray(), []);
    }
}