<?php
namespace App\Transformers\Media;

use App\Models\ActivityMedia\MediasLikes;
use App\Models\User\UsersMedias;
use League\Fractal\TransformerAbstract;

class MediaUserTransformer extends TransformerAbstract
{
    public function transform(UsersMedias $mediaUser)
    {
        return array_except($mediaUser->toArray(), []);
    }
}