<?php
namespace App\Transformers\Media;

use App\Models\ActivityMedia\Media;
use App\Transformers\User\UserTransformer;
use League\Fractal\TransformerAbstract;

class MediaReactionsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'users',
        'likes',
        'comments',
        'mediaUser'
    ];

    public function includeUsers(Media $media)
    {
        return $this->collection($media->users, new UserTransformer(), false);
    }

    public function includeMediaUser(Media $media)
    {
        return $this->item($media->mediaUser, new MediaUserTransformer(), false);
    }

    public function includeLikes(Media $media)
    {
        return $this->collection($media->likes, new MediaLikesTransformer(), false);
    }

    public function includeComments(Media $media)
    {
        return $this->collection($media->comments, new MediaCommentsTransformer(), false);
    }

    public function transform(Media $media)
    {
        return array_except(
            array_merge(
                $media->toArray(),
                [
                    'num_likes' => count($media->likes),
                    'num_comments' => count($media->comments),
                    'user_id' => $media->mediaUser->users_id
                ]
            ),
            ['media_user']
        );
    }
}