<?php
namespace App\Transformers\Media;

use App\Models\ActivityMedia\MediasComments;
use App\Models\User\User;
use App\Transformers\User\UserTransformer;
use League\Fractal\TransformerAbstract;

class MediaCommentsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'user',
        'likes',
        'reply_to'
    ];

    public function includeUser(MediasComments $mediasComments)
    {
        return $this->item(
            empty($mediasComments->user) ? new User() : $mediasComments->user,
            new UserTransformer(),
            false
        );
    }

    public function includeLikes(MediasComments $mediasComments)
    {
        return $this->item(
            empty($mediasComments->likes) ? new MediasComments() : $mediasComments->likes,
            new MediaCommetnsLikesTransformer(),
            false
        );
    }

    public function includeReplyTo(MediasComments $mediasComments)
    {
        return $this->item(
            empty($mediasComments->replyTo) ? new MediasComments() : $mediasComments->replyTo,
            new static(),
            false
        );
    }

    public function transform(MediasComments $mediasComments)
    {
        return array_except(
            array_merge(
                $mediasComments->toArray(),
                [
                    'num_likes' => empty($mediasComments->likes) ? 0 : count($mediasComments->likes),
                ]
            ),
            ['reply_to']
        );
    }
}