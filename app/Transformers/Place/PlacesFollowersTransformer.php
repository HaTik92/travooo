<?php

namespace App\Transformers\Place;

use App\Models\Place\PlaceFollowers;
use App\Transformers\User\UserTransformer;
use League\Fractal\TransformerAbstract;

class PlacesFollowersTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'followers'
    ];

    public function includeFollowers(PlaceFollowers $place)
    {
        return $this->collection($place->followers, new UserTransformer(), false);
    }

    public function transform(PlaceFollowers $followers)
    {
        return array_except($followers->toArray(), ['id', 'users_id', 'places_id']);
    }
}