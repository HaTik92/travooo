<?php
namespace App\Transformers\Place;

use App\Models\Place\PlacesDiscussions;
use League\Fractal\TransformerAbstract;

class PlacesDiscussionsDownvotesTransformer extends TransformerAbstract
{
    public function transform(PlacesDiscussions $placesDiscussions)
    {
        return array_except($placesDiscussions->toArray(), []);
    }
}