<?php

namespace App\Transformers\Place;

use App\Models\Place\Place;
use App\Transformers\Media\MediaTransformer;
use League\Fractal\TransformerAbstract;

class PlacesCityTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'medias',
        'trans'
    ];

    public function includeTrans(Place $place)
    {
        return $this->collection($place->trans, new PlacesTranslateTransformer(), false);
    }

    public function includeMedias(Place $place)
    {
        return $this->collection($place->getMedias, new MediaTransformer(), false);
    }

    public function transform(Place $place)
    {
        return array_except($place->toArray(), ['get_medias']);
    }
}