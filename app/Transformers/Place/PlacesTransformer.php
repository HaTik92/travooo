<?php

namespace App\Transformers\Place;

use App\Models\Place\Place;
use App\Transformers\City\CityTransformer;
use App\Transformers\Country\CountryTransformer;
use League\Fractal\TransformerAbstract;

class PlacesTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'city',
        'country',
        'trans',
        'discussions'
    ];

    public function includeCity(Place $place)
    {
        return $this->item($place->city, new CityTransformer(), false);
    }

    public function includeTrans(Place $place)
    {
        return $this->collection($place->trans, new PlacesTranslateTransformer(), false);
    }

    public function includeCountry(Place $place)
    {
        return $this->item($place->country, new CountryTransformer(), false);
    }

    public function includeDiscussions(Place $place)
    {
        return $this->collection($place->discussions, new PlaceDiscussionsTransformer(), false);
    }

    public function transform(Place $place)
    {
        return [
            'id' => $place->id,
            'lat' => $place->lat,
            'lng' => $place->lng,
            'pivot' => [
                'date' => $place->pivot->date,
                'time' => $place->pivot->time,
                'order' => $place->pivot->order,
                'duration' => $place->pivot->duration,
                'budget' => $place->pivot->budget,
                'active' => $place->pivot->active
            ],
            'trans' => [
                'title' => count($place->trans) === 1 ? $place->trans[0]->title : $place->trans->pluck('title')
            ]
        ];
    }
}