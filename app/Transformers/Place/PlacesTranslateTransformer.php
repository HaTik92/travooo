<?php

namespace App\Transformers\Place;

use App\Models\Place\PlaceTranslations;
use League\Fractal\TransformerAbstract;

class PlacesTranslateTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function transform(PlaceTranslations $translate)
    {
        return array_except($translate->toArray(), ['places_id', 'languages_id']);
    }
}