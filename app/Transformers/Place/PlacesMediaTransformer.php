<?php

namespace App\Transformers\Place;

use App\Models\ActivityMedia\Media;
use App\Models\Place\Medias;
use App\Models\Place\Place;
use App\Transformers\Media\MediaTransformer;
use League\Fractal\TransformerAbstract;

class PlacesMediaTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'medias',
        'firstmedia',
        'trans'
    ];

    public function includeTrans(Place $place)
    {
        return $this->collection($place->trans, new PlacesTranslateTransformer(), false);
    }

    public function includeMedias(Place $place)
    {
        return $this->collection($place->getMedias, new MediaTransformer(), false);
    }

    public function includeFirstmedia(Place $place)
    {
        return $this->item(empty($place->getMedias[0]) ? new Medias() : $place->getMedias[0], new MediaTransformer(), false);
    }

    public function transform(Place $place)
    {
        return array_except($place->toArray(), ['get_medias']);
    }
}