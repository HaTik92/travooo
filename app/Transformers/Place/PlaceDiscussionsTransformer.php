<?php
namespace App\Transformers\Place;

use App\Models\Place\PlacesDiscussions;
use App\Transformers\User\UserTransformer;
use League\Fractal\TransformerAbstract;

class PlaceDiscussionsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'UpVotes',
        'DownVotes',
        'users'
    ];

    public function includeUpVotes(PlacesDiscussions $placesDiscussions)
    {
        return $this->collection($placesDiscussions->upVotes, new PlacesDiscussionsDownvotesTransformer(), false);
    }

    public function includeDownVotes(PlacesDiscussions $placesDiscussions)
    {
        return $this->collection($placesDiscussions->downVotes, new PlacesDiscussionsDownvotesTransformer(), false);
    }

    public function includeUsers(PlacesDiscussions $placesDiscussions)
    {
        return $this->item($placesDiscussions->users, new UserTransformer(), false);
    }

    public function transform(PlacesDiscussions $placesDiscussions)
    {
        return array_except($placesDiscussions->toArray(), ['up_votes', 'down_votes']);
    }
}