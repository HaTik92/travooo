<?php
namespace App\Transformers\User;

use App\Models\User\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return array_except($user->toArray(), ['trans']);
    }
}