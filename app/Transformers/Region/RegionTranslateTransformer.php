<?php
namespace App\Transformers\Region;

use App\Models\Regions\RegionsTranslation;
use League\Fractal\TransformerAbstract;

class RegionTranslateTransformer extends TransformerAbstract
{
    public function transform(RegionsTranslation $translation)
    {
        return array_except($translation->toArray(), ['regions_id']);
    }
}