<?php
namespace App\Transformers\Region;

use App\Models\Regions\Regions;
use League\Fractal\TransformerAbstract;

class RegionTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'trans'
    ];

    public function includeTrans(Regions $regions)
    {
        return $this->collection($regions->trans, new RegionTranslateTransformer(), false);
    }

    public function transform(Regions $regions)
    {
        return array_except($regions->toArray(), ['trans']);
    }
}