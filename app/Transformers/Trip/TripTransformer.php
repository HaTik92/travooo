<?php

namespace App\Transformers\Trip;

use App\Models\TripPlans\TripPlans;
use App\Transformers\City\CityTransformer;
use App\Transformers\Country\CountryTransformer;
use App\Transformers\Place\PlacesTransformer;
use League\Fractal\TransformerAbstract;

class TripTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'places',
        'cities',
        'countries'
    ];

    public function includePlaces(TripPlans $tripPlans)
    {
        return $this->collection( $tripPlans->places, new PlacesTransformer(), false );
    }

    public function includeCities(TripPlans $tripPlans)
    {
        return $this->collection( $tripPlans->places->city, new CityTransformer(), false );
    }

    public function includeCountries(TripPlans $tripPlans)
    {
        return $this->collection( $tripPlans->places->country, new CountryTransformer(), false );
    }

    public function transform(TripPlans $tripPlans)
    {
        return [];
    }
}