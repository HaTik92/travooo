<?php

namespace App\Models\ExpertsPlaces;

use App\Models\Place\Place;
use Illuminate\Database\Eloquent\Model;

class ExpertsPlaces extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'users_id',
        'places_id'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User\User::class, 'users_id');
    }

      public function places()
    {
        return $this->belongsTo(Place::class,'places_id');
    }
}
