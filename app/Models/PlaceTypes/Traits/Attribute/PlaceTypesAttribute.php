<?php

namespace App\Models\PlaceTypes\Traits\Attribute;

/**
 * Class PlaceTypesAttribute.
 */
trait PlaceTypesAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
