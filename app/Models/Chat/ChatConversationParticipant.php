<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChatConversationParticipant extends Model
{
    //
    use SoftDeletes;

    protected $table = 'chat_conversation_participants';
    protected $fillable = ['chat_conversation_id', 'user_id'];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function conversation()
    {
        return $this->belongsTo('App\Models\Chat\ChatConversation', 'chat_conversation_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'user_id');
    }

    // -----------------------------------------------------------------------------------

}
