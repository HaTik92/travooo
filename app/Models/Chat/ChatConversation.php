<?php

namespace App\Models\Chat;

use App\Events\UpdatePlanChatsEvent;
use App\Services\PlaceChat\PlaceChatsService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class ChatConversation extends Model
{
    //
    use SoftDeletes;

    protected $table = 'chat_conversations';
    protected $fillable = ['created_by_id', 'subject', 'plans_id', 'type'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $appends = ['user_thumb', 'user_id', 'unread_messages_count', 'participant_names_chunk', 'participant_names_chunk_before', 'conversation_title', 'last_updated_at', 'participants_names', 'participants_id', 'has_unread_messages', 'conversation_url', 'participant_images'];


    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'created_by_id', 'id');
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            date_default_timezone_set("UTC");
        });

        self::created(function ($model) {
            date_default_timezone_set("Asia/Riyadh");
        });

        self::updating(function ($model) {
            date_default_timezone_set("UTC");
        });

        self::updated(function ($model) {
            date_default_timezone_set("Asia/Riyadh");
        });

        self::saving(function ($model) {
            date_default_timezone_set("UTC");
        });

        self::saved(function ($model) {
            date_default_timezone_set("Asia/Riyadh");
        });
    }

    public function participants()
    {
        return $this->hasMany('App\Models\Chat\ChatConversationParticipant', 'chat_conversation_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Chat\ChatConversationMessage', 'chat_conversation_id');
    }

    public function fromuser()
    {
        return $this->belongsTo('App\Models\User\User', 'from_id', 'id');
    }

    public function touser()
    {
        return $this->belongsTo('App\Models\User\User', 'to_id', 'id');
    }
    public function myMessages($user_id, $paginate = null)
    {
        $convid = $this->id;
        $query = ChatConversationMessage::where(function ($query) use ($user_id, $convid) {
            $query->where('chat_conversation_id', $convid);
            $query->where('to_id', $user_id);
        })->orWhere(function ($query) use ($user_id, $convid) {
            $query->where('chat_conversation_id', $convid);
            $query->where('from_id', $user_id);
            $query->where('to_id', $user_id);
        });

        if ($paginate) {
            $query->orderByDesc('id')->paginate($paginate);
        } else {
            $query->orderBy('id');
        }

        return $query->with('fromuser', 'touser')->get();
    }

    public function markAllAsRead($user_id)
    {
        $convid = $this->id;

        $result = ChatConversationMessage::where(function ($query) use ($user_id, $convid) {
            $query->where('chat_conversation_id', $convid);
            $query->where('to_id', $user_id);
        })->orWhere(function ($query) use ($user_id, $convid) {
            $query->where('chat_conversation_id', $convid);
            $query->where('from_id', $user_id);
            $query->where('to_id', $user_id);
        })->orderBy('id')->update(['is_read' => true]);

        if ($this->plans_id) {
            try {
                broadcast(new UpdatePlanChatsEvent($user_id, $this->plans_id));
            } catch (\Exception $e) {
            }
        }

        return $result;
    }

    public function is_unread($user_id)
    {
        $unread_messages = $this->unread_count($user_id);

        if ($unread_messages == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getUnreadMessagesCountAttribute()
    {
        $user_id = Auth::id();
        return $this->unread_count($user_id);
    }

    public function unread_count($user_id)
    {
        $unread_messages = $this->messages->where('to_id', $user_id)->where('from_id', '<>', $user_id)->where('is_read', false)->count();

        return $unread_messages;
    }

    public function getConversationUrlAttribute()
    {
        return route('chat.index', ['conversation_id' => $this->id]);
    }

    public function getHasUnreadMessagesAttribute()
    {
        return $this->is_unread(Auth::guard('user')->user()->id);
    }

    //This is used for title line of conversations lists
    public function getConversationTitleAttribute()
    {
        $last_message = ChatConversationMessage::where('chat_conversation_id', $this->id)->whereNotNull('message')->orderByDesc('id')->first();
        if ($last_message !== null) {
            return $last_message->conv_title;
        } else {
            return '';
        }
    }

    // Get group chat images
    public function getParticipantImagesAttribute()
    {
        $user_id = Auth::id();
        $ps = array();
        foreach ($this->participants as $tp) {
            if ($tp->user_id != $user_id || ($this->type === PlaceChatsService::CHAT_TYPE_PLACE || $this->type === PlaceChatsService::CHAT_TYPE_MAIN)) {
                $ps[] = check_profile_picture($tp->user->profile_picture);
            }
        }
        $ps = @join(",", $ps);

        return $ps;
    }

    // Get group chat names only first 4
    public function getParticipantNamesChunkBeforeAttribute()
    {
        $ps = array();
        $user_id = Auth::id();
        if (count($this->participants) > 4) {
            $k = 0;
            foreach ($this->participants as $tp) {
                if ($tp->user_id != $user_id) {
                    $ps[] = $tp->user->name;
                    if ($k == 3) {
                        break;
                    }

                    $k++;
                }
            }
            $ps[] = " + " . (count($this->participants) - 5) . " more";
        }


        return @join(",", $ps);
    }

    // Get group chat names only first 4
    public function getParticipantNamesChunkAttribute()
    {
        $ps = array();
        $user_id = Auth::id();
        if (count($this->participants) > 4) {
            foreach ($this->participants as $k => $tp) {

                if ($tp->user_id != $user_id) {
                    if ($k <= 4) continue;
                    $ps[] = $tp->user->name;
                }
            }
        }


        return @join(",", $ps);
    }



    public function participant_names($user_id, $include_owner = false)
    {
        if (
            $this->type === PlaceChatsService::CHAT_TYPE_MAIN
            || $this->type === PlaceChatsService::CHAT_TYPE_PLACE
        ) {
            /** @var PlaceChatsService $placeChatsService */
            $placeChatsService = app(PlaceChatsService::class);
            $participants = $placeChatsService->getPlaceChatParticipants($this->id, false);
            $result = 'Trip Chat: ';

            foreach ($participants as $key => $participant) {
                if ($key > 0) {
                    $result .= ', ';
                }

                $result .= $participant->user->name;
            }

            return $result;
        }

        $ps = array();
        foreach ($this->participants as $tp) {
            if ($tp->user_id != $user_id) {
                $ps[] = $tp->user->name;
            }
        }
        $ps = @join(", ", $ps);

        return $ps;
    }

    public function getUserIdAttribute()
    {
        $user_id = Auth::guard('user')->user()->id;
        $ps = array();
        foreach ($this->participants as $tp) {
            if ($tp->user_id != $user_id) {
                $ps[] = $tp->user->id;
            }
        }
        $ps = @join(", ", $ps);

        return $ps;
    }

    public function getParticipantsNamesAttribute()
    {
        return $this->participant_names(Auth::guard('user')->user()->id);
    }

    public function getUserThumbAttribute()
    {
        return @check_profile_picture(@$this->participants->where('user_id', '<>', Auth::guard('user')->user()->id)->first()->user->profile_picture);
    }

    public function getLastUpdatedAtAttribute()
    {
        return Carbon::parse($this->updated_at)->format('d/m/Y h:i A');
    }

    public function getParticipantsIdAttribute()
    {
        $ids = '';
        $participants = $this->participants;
        foreach ($participants as $participant) {
            if ($participants->last() === $participant) {
                $ids = $ids . $participant->user_id;
            } else {
                $ids = $ids . $participant->user_id . ';';
            }
        }

        return $ids;
    }
    // -----------------------------------------------------------------------------------


}
