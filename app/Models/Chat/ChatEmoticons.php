<?php
/**
 * Created by PhpStorm.
 * User: belte
 * Date: 13.10.2018
 * Time: 13:02
 */

namespace App\Models\Chat;


class ChatEmoticons
{
    protected $emoticons_map;

    public function __construct()
    {
        $this->emoticons_map = [
            ':smile:' => [
                'shortcode' => ':smile:',
                'src' => url('/assets2/image/smile-smiley.svg')
            ],
            ':eek:' => [
                'shortcode' => ':eek:',
                'src' => url('/assets2/image/smile-eek.svg')
            ],
            ':heart_eyes:' => [
                'shortcode' => ':heart_eyes:',
                'src' => url('/assets2/image/smile-eye-heart.svg')
            ],
            ':joy:' => [
                'shortcode' => ':joy:',
                'src' => url('/assets2/image/smile-happy-cry.svg')
            ],
            ':rage:' => [
                'shortcode' => ':rage:',
                'src' => url('/assets2/image/smile-angry.svg')
            ],
            ':thumbsup:' => [
                'shortcode' => ':thumbsup:',
                'src' => url('/assets2/image/smile-finger-up.svg')
            ],
            ':thumbsdown:' => [
                'shortcode' => ':thumbsdown:',
                'src' => url('/assets2/image/smile-finger-down.svg')
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getEmoticonsMap()
    {
        return $this->emoticons_map;
    }
}