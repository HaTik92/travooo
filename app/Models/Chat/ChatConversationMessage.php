<?php

namespace App\Models\Chat;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class ChatConversationMessage extends Model
{
    //
    use SoftDeletes;

    protected $table = 'chat_conversation_messages';
    protected $fillable = [
        'send_id',
        'chat_conversation_id',
        'from_id',
        'to_id',
        'message',
        'file_path',
        'file_name',
        'file_type',
        'file_size',
        'is_read',
        'updated_at',
        'places_id',
        'plans_id',
        'type',
        'post_type',
        'post_id',
    ];

    protected $casts = [
        'is_read' => 'boolean'
    ];

    protected $appends = ['from_user_name', 'file_link', 'message_parsed', 'conv_title', 'last_updated_at', 'from_user_thumb', 'unread_chat_conversations_count'];
    protected $hidden = ['message', 'file_path', 'file_name', 'file_type', 'file_size', 'deleted_at', 'fromuser', 'touser'];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            date_default_timezone_set("UTC");
        });

        self::created(function($model){
            date_default_timezone_set("Asia/Riyadh");
        });

        self::updating(function($model){
            date_default_timezone_set("UTC");
        });

        self::updated(function($model){
            date_default_timezone_set("Asia/Riyadh");
        });

        self::saving(function($model){
            date_default_timezone_set("UTC");
        });

        self::saved(function($model){
            date_default_timezone_set("Asia/Riyadh");
        });

    }

    public function conversation()
    {
        return $this->belongsTo('App\Models\Chat\ChatConversation', 'chat_conversation_id');
    }

    public function fromuser()
    {
        return $this->belongsTo('App\Models\User\User', 'from_id', 'id');
    }

    public function touser()
    {
        return $this->belongsTo('App\Models\User\User', 'to_id', 'id');
    }

    public function getUnreadChatConversationsCountAttribute(){
        return $this->touser->unread_chat_conversations_count;
    }

    public function getFromUserNameAttribute()
    {
        return $this->fromuser->name;
    }

    public function getFileLinkAttribute()
    {
        if ($this->file_path !== null) {
            $video_types = ['mp4', 'mpeg', 'ogg', 'mov'];
            $image_types = ['bmp', 'gif', 'jpeg', 'png', 'jpg'];
            $other_types = ['css', 'csv', 'csv-schema', 'html', 'plain', 'rtf', 'doc', 'docx', 'xml', 'xls', 'pdf'];

            $file_paths = explode('|',$this->file_path);
            $file_names = explode('|',$this->file_name);
            $file_types = explode('|',$this->file_type);
            $files = '';

            foreach ($file_paths as $key=>$path){
                if(!$path) continue;

                if(in_array(strtolower($file_types[$key]), $video_types)){
                    $files .= "<video src='".url("/download/" . $path)."' width='200' controls style='margin-left:5px;margin-top:5px;' alt='$file_names[$key]'></video>";
                }else if(in_array(strtolower($file_types[$key]), $image_types)){
                    $files .= "<img src='".url("/download/" . $path)."' width='100' height='100' style='margin-left:5px;margin-top:5px;' class='appended-img' alt='$file_names[$key]'>";
                }else if(in_array(strtolower($file_types[$key]), $other_types)){
                    $files .= "<a href='" . url("/download/" . $path) . "' style='margin-left:5px;margin-top:5px;' target='blank'>" . $file_names[$key] . "</a>";
                }

            }
            return $files;
            //return "<a href='" . url("/download/" . $this->file_path) . "' target='blank'>" . $this->file_name . "</a>";

        } else {
            return null;
        }
    }

    public function getFromUserThumbAttribute()
    {
        return url($this->fromuser->profile_picture);
    }

    public function getMessageParsedAttribute()
    {
        if ($this->message !== null && $this->message != '') {
            $emoticons = new \App\Models\Chat\ChatEmoticons();
            $emoticons = $emoticons->getEmoticonsMap();
            $message_parsed = $this->message;
            foreach ($emoticons as $emoticon) {
                $emoticon_html = "<img class='smile' src='" . $emoticon['src'] . "' alt='smile' style='width:16px'>";
                $message_parsed = str_replace($emoticon['shortcode'], $emoticon_html, $message_parsed);
            }
            return nl2br($message_parsed);

        } else {
            return null;
        }
    }

    public function getConvTitleAttribute()
    {
        if ($this->file_path === null) {
            //$msg = substr($this->message, 0, 80);
            $msg = $this->message;
            $emoticons = new \App\Models\Chat\ChatEmoticons();
            $emoticons = $emoticons->getEmoticonsMap();
            $message_parsed = $msg;
            foreach ($emoticons as $emoticon) {
                $emoticon_html = "<img class='smile' src='" . $emoticon['src'] . "' alt='smile' style='width:16px'>";
                $message_parsed = str_replace($emoticon['shortcode'], $emoticon_html, $message_parsed);
            }
            return nl2br($message_parsed);
        } else {
            if ($this->message === null) {
                return $this->file_name;
            } else {
                return substr($this->message, 0, 80);
            }
        }
    }

    public function getLastUpdatedAtAttribute()
    {
        return $this->created_at->format('d/m/Y h:i A');
    }


    // -----------------------------------------------------------------------------------

}
