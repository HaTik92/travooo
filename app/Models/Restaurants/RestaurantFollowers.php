<?php

namespace App\Models\Restaurants;

use Illuminate\Database\Eloquent\Model;

class RestaurantFollowers extends Model
{
    protected $table    = 'restaurants_followers';
    public $timestamps  = false;

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'restaurants_id', 'users_id'];
    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
}
