<?php

namespace App\Models\Restaurants;

use Illuminate\Database\Eloquent\Model;
use App\Models\Restaurants\Traits\Relationship\RestaurantsRelationship;
use App\Models\Restaurants\Traits\Attribute\RestaurantsAttribute;
use App\Models\ActivityMedia\Media;
use Elasticquent\ElasticquentTrait;

class Restaurants extends Model
{
    CONST ACTIVE    = 1;
    CONST DEACTIVE  = 2;

    CONST IS_CAPITAL     = 1;
    CONST IS_NOT_CAPITAL = 2;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'restaurants';
    
    use RestaurantsRelationship,
        RestaurantsAttribute;
    
    use ElasticquentTrait;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['countries_id', 'cities_id' , 'places_id' , 'lat', 'lng', 'active'];
    protected $mappingProperties = array(
        'title' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'type' => [
            'type' => 'text',
            "analyzer" => "standard",
        ]
    );

    /**
     * @return mixed
     **/
    public function deleteTrans(){
        self::deleteModels($this->trans);
    }

    /**
     * @return mixed
     **/
    public function deleteMedias(){
        self::deleteModels($this->medias);
    }

    /**
     * @return mixed
     **/
    public function deleteModels($models){
        if(!empty($models)){
            foreach ($models as $key => $value) {
                $value->delete();
            }
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function medias(){
        return $this->belongsToMany('\App\Models\ActivityMedia\Media', 'restaurants_medias', 'restaurants_id', 'medias_id');
    }
    
    public function followers()
    {
        return $this->hasMany('\App\Models\User\User', 'id', 'users_id');
    }
    
    public function reviews()
    {
        return $this->hasMany('App\Models\Reviews\Reviews', 'restaurants_id', 'id');
    }
    
    public function getMedias()
    {
        return $this->belongsToMany('\App\Models\ActivityMedia\Media', 'restaurants_medias', 'restaurants_id', 'medias_id');
    }
   
    
    function getIndexName() {
        return 'restaurants';
    }
    function getTypeName()
    {
        return 'restaurants';
    }
}
