<?php

namespace App\Models\Holidays\Traits\Attribute;

/**
 * Class HolidaysAttribute.
 */
trait HolidaysAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
