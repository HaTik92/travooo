<?php
/**
 * Created by PhpStorm.
 * User: Syuniq
 * Date: 19-Nov-18
 * Time: 2:39 PM
 */

namespace App\Models\Holidays;
use Illuminate\Database\Eloquent\Model;
use App\Models\Holidays\Traits\Relationship\HolidaysMediaRelationship;


class HolidaysMedias extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'holidays_medias';

    use HolidaysMediaRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'holidays_id' , 'medias_id' ];
}