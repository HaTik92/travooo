<?php

namespace App\Models\Discussion;


use App\Models\Discussion\Traits\Relationship\DiscussionRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\User\UsersContentLanguages;

use Carbon\Carbon;

class Discussion extends Model
{

    protected $table = 'discussions';

    //    use ElasticquentTrait;
    use DiscussionRelationship;
    use SoftDeletes;

    //    protected $mappingProperties = array(
    //        'title' => [
    //            'type' => 'text',
    //            "analyzer" => "standard",
    //        ],
    //        'question' => [
    //            'type' => 'text',
    //            "analyzer" => "standard",
    //        ]
    //    );
    //
    //    protected $elasticSearchRelationMapping = [
    ////        'place_translations' => [
    ////            'class' => PlaceTranslations::class,
    ////            'relation' => 'discussionsingle'
    ////        ],
    //        'city_translations' => [
    //            'class' => CityRelationship::class,
    //            'relation' => 'discussionsingle'
    //        ],
    //        'translations' => [
    //            'class' => CityRelationship::class,
    //            'relation' => 'trans'
    //        ],
    //    ];




    public static function getDiscussions($title, $skip = '', $limit = '',  $destinationType, $destinationId, $order = 'popular', $type = null,  $onlyAuthUser = false, $topic = '', $answer_user_id = '')
    {
        $langugae_id = session('discussion_langugae_id');
        if ($langugae_id == null) {
            $langugae_id = getDesiredLanguage();
            if (isset($langugae_id[0])) {
                $langugae_id = $langugae_id[0];
            } else {
                $langugae_id = UsersContentLanguages::DEFAULT_LANGUAGES;
            }
            session(['discussion_langugae_id' => $langugae_id]);
        }


        $query = Discussion::query()
            ->where('language_id', $langugae_id)
            ->whereNotIn('discussions.users_id', blocked_users_list());

        if ($type && $type == 3) {
            $query->where('discussions.users_id', Auth::user()->id);
        }

        if ($onlyAuthUser && $type == 2) {
            $discussionsIds = DiscussionReplies::query()->select('discussions_id')->byAuthUser()->pluck('discussions_id');
            $query->whereIn('discussions.id', $discussionsIds);
        }

        if ($answer_user_id != '') {
            $discussionsIds = DiscussionReplies::query()->select('discussions_id')->where('users_id', $answer_user_id)->pluck('discussions_id');
            $query->whereIn('discussions.id', $discussionsIds);
        }

        if ($title != '') {
            $query->where('question', 'like', "%" . $title . "%");
        }

        if ($destinationType != '') {
            $query->where('destination_type', $destinationType)->where('destination_id', $destinationId);
        }

        switch ($order) {
            case 'dateasc':
                $query->orderBy('created_at', 'ASC');
                break;
            case 'datedesc':
                $query->orderBy('created_at', 'DESC');
                break;
            default:  /* changed popular algo & now rank calculated based on recent replies and upvotes */
                $query->withCount([
                    'replies'  => function ($q) {
                        $q->where('created_at', '>', Carbon::now()->subDays(60));
                    },
                    'likes'  => function ($q) {
                        $q->where('vote', 1);
                        $q->where('created_at', '>', Carbon::now()->subDays(60));
                    },
                ])->orderByDesc(DB::raw('likes_count + replies_count'));
        }

        if ($topic != '') {
            $query->whereHas('topics', function ($query) use ($topic) {
                $query->where('topics', $topic);
            });
        }

        if ($skip != '') {
            $query->skip($skip);
        }
        if ($limit != '') {
            $query->take($limit);
        }

        $discussions = $query->with([
            'author',
            'medias',
            'media',
            'likes',
            'replies.author',
            'replies.upvotes',
            'replies.downvotes'
        ])->get();

        if (count($discussions) == 0 && $langugae_id != UsersContentLanguages::DEFAULT_LANGUAGES) {
            session(['discussion_langugae_id' => UsersContentLanguages::DEFAULT_LANGUAGES]);
            return self::getDiscussions($title, $skip, $limit,  $destinationType, $destinationId, $order, $type,  $onlyAuthUser, $topic, $answer_user_id);
        }

        return $discussions;
    }
    //
    //
    //    function getIndexName() {
    //        return 'discussions';
    //    }
    //
    //    function getTypeName()
    //    {
    //        return 'discussions';
    //    }



    public function countriesIds()
    {
        if (strtolower($this->destination_type) == 'country') {
            return collect([$this->destination_id]);
        }
        return collect([]);
    }
}
