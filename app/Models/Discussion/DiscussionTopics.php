<?php

namespace App\Models\Discussion;

use Illuminate\Database\Eloquent\Model;

class DiscussionTopics extends Model
{
    protected $table = 'discussions_topics';

    /**
     * Many-to-one relations with Discussion.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function discussion() {
        return $this->hasOne('App\Models\Discussion\Discussion', 'id',  'discussions_id');
    }

}
