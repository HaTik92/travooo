<?php

namespace App\Models\Discussion;

use Illuminate\Database\Eloquent\Model;

class DiscussionRepliesDownvotes extends Model
{
    protected $fillable = ['discussions_id'];
}
