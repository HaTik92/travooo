<?php

namespace App\Models\Discussion;

use Illuminate\Database\Eloquent\Model;

class DiscussionViews extends Model
{
     protected $table = 'discussion_views';
}
