<?php

namespace App\Models\Discussion;

use Illuminate\Database\Eloquent\Model;

class DiscussionLikes extends Model
{

    protected $fillable = ['discussions_id', 'users_id', 'vote'];

    const UP_VOTE = 1;
    const DOWN_VOTE = 0;

    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
}
