<?php

namespace App\Models\Discussion;

use Illuminate\Database\Eloquent\Model;

class DiscussionExperts extends Model
{
    protected $table = 'discussion_experts';
    
    
    public function user()
    {
        return $this->belongsTo(\App\Models\User\User::class, 'expert_id');
    }
}
