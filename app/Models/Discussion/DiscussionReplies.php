<?php

namespace App\Models\Discussion;

use App\Models\Posts\PostsTags;
use Illuminate\Database\Eloquent\Model;

class DiscussionReplies extends Model
{
    const TYPE_RECOMMEND = 1;
    const TYPE_TIP       = 2;
    const TYPE_QUESTION  = 4;

    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
    public function upvotes()
    {
        return $this->hasMany('App\Models\Discussion\DiscussionRepliesUpvotes', 'replies_id');
    }
    public function downvotes()
    {
        return $this->hasMany('App\Models\Discussion\DiscussionRepliesDownvotes', 'replies_id');
    }

    public function discussion()
    {

        return $this->belongsTo('App\Models\Discussion\Discussion', 'discussions_id');
    }

    public function scopeByAuthUser($query)
    {
        return $query->where('users_id', \Auth::id());
    }

    public function sub()
    {
        return $this->hasMany(self::class, 'parents_id');
    }

    public function tags()
    {
        return $this->hasMany('App\Models\Posts\PostsTags', 'posts_id')->where('posts_type', PostsTags::TYPE_DISCUSSION_REPLY);
    }
}
