<?php

namespace App\Models\Discussion;

use Illuminate\Database\Eloquent\Model;

class DiscussionMedias extends Model
{
    protected $table = 'discussions_medias';
}
