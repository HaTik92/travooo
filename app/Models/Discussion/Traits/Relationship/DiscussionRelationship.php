<?php

namespace App\Models\Discussion\Traits\Relationship;

use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionLikes;
use Illuminate\Support\Facades\Auth;

/**
 * Class DiscussionRelationship.
 */
trait DiscussionRelationship {

    /**
     * Belongs-To relations with DiscussionTypes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getType() {
        return $this->belongsTo('App\Models\Discussion\DiscussionTypes', 'type');
    }

    /**
     * Belongs-To relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function author() {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    /**
     * Many-to-Many relations with DiscussionReplies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function replies() {
        return $this->hasMany('App\Models\Discussion\DiscussionReplies', 'discussions_id');
    }

    /**
     * Many-to-Many relations with DiscussionReplies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function main_replies() {
        return $this->hasMany('App\Models\Discussion\DiscussionReplies', 'discussions_id')
            ->where('parents_id', 0);
    }

    /**
     * Many-to-Many relations with DiscussionViews.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function views() {
        return $this->hasMany('App\Models\Discussion\DiscussionViews', 'discussions_id');
    }

    /**
     * Many-to-Many relations with DiscussionFollows.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function follows() {
        return $this->hasMany('App\Models\Discussion\DiscussionFollows', 'discussions_id');
    }

    /**
     * Many-to-Many relations with DiscussionLikes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes()
    {
        return $this->hasMany('App\Models\Discussion\DiscussionLikes', 'discussions_id');
    }

    /**
     * Many-to-Many relations with DiscussionShares.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function shares()
    {
        return $this->hasMany('App\Models\Discussion\DiscussionShares', 'discussions_id');
    }

    
    /**
     * Many-to-Many relations with DiscussionMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function medias()
    {
        return $this->hasMany('App\Models\Discussion\DiscussionMedias', 'discussion_id');
    }
    
     /**
     * Belongs-To relations with DiscussionMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function media()
    {
        return $this->hasMany('App\Models\Discussion\DiscussionMedias', 'discussion_id') 
                ->where(\DB::raw('RIGHT(media_url, 4)'), '!=', '.mp4')->take(1);
    }
    
    
     /**
     * Many-to-Many relations with DiscussionExperts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function experts()
    {
        return $this->hasMany('App\Models\Discussion\DiscussionExperts', 'discussion_id');
    }

    /**
     * Many-to-Many relations with DiscussionExperts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discussion_upvotes()
    {
        return $this->hasMany(DiscussionLikes::class, 'discussions_id')
            ->where('vote', DiscussionLikes::UP_VOTE);
    }

    /**
     * Many-to-Many relations with DiscussionExperts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discussion_downvotes()
    {
        return $this->hasMany(DiscussionLikes::class, 'discussions_id')
            ->where('vote', DiscussionLikes::DOWN_VOTE);
    }

    /**
     * Many-to-Many relations with DiscussionReplies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function topics() {
        return $this->hasMany('App\Models\Discussion\DiscussionTopics', 'discussions_id');
    }

}
