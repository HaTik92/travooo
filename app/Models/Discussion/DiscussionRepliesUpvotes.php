<?php

namespace App\Models\Discussion;

use Illuminate\Database\Eloquent\Model;

class DiscussionRepliesUpvotes extends Model
{
    protected $fillable = ['discussions_id'];
}
