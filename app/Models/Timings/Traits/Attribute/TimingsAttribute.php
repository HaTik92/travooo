<?php

namespace App\Models\Timings\Traits\Attribute;

/**
 * Class TimingsAttribute.
 */
trait TimingsAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
