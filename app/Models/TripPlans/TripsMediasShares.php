<?php

namespace App\Models\TripPlans;

use App\Models\TripMedias\TripMedias;
use Illuminate\Database\Eloquent\Model;
use App\Models\Posts\PostsTags;
use App\Services\Api\ShareService;

class TripsMediasShares extends Model
{
    protected $table = 'trip_medias_shares';

    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'user_id');
    }

    public function trip_media()
    {
        return $this->belongsTo(TripMedias::class, 'trip_media_id');
    }

    public function tags()
    {
        return $this->hasMany('App\Models\Posts\PostsTags', 'posts_id')->where('posts_type', PostsTags::TYPE_TRIP_PLACE_MEDIA_SHARE);
    }

    public function postComments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')
            ->where('parents_id', '=', NULL)
            ->where('type', ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE)
            ->orderby('created_at', 'DESC');
    }
}
