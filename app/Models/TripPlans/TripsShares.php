<?php

namespace App\Models\TripPlans;

use App\Models\Posts\PostsTags;
use App\Models\TripPlans\Traits\Relationship\TripsSharesRelationship;
use App\Services\Api\ShareService;
use Illuminate\Database\Eloquent\Model;

class TripsShares extends Model
{
    protected $table = 'trips_shares';

    use TripsSharesRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'trip_id', 'users_id', 'scope', 'comment', 'created_at'];

    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'user_id');
    }

    public function tags()
    {
        return $this->hasMany('App\Models\Posts\PostsTags', 'posts_id')->where('posts_type', PostsTags::TYPE_TRIP_SHARE);
    }

    public function postComments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')
            ->where('parents_id', '=', NULL)
            ->where('type', ShareService::TYPE_TRIP_SHARE)
            ->orderby('created_at', 'DESC');
    }
}
