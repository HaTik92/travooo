<?php

namespace App\Models\TripPlans;

use Illuminate\Database\Eloquent\Model;

class TripsVersions extends Model
{
    protected $fillable = ['active'];

    protected $dates = ['start_date', 'end_date'];

    public function cities()
    {
        return $this->plan->cities();
    }

    public function places()
    {
        return $this->plan->places();
    }

    public function plan()
    {
        return $this->belongsTo('App\Models\TripPlans\TripPlans', 'plans_id');
    }

    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'authors_id');
    }
}
