<?php

namespace App\Models\TripPlans;

use Illuminate\Database\Eloquent\Model;

class TripContentPostLike extends Model
{
    protected $table = 'trip_content_post_likes';

    public function user()
    {
        return $this->hasOne('App\Models\User\User', 'id', 'users_id');
    }
}
