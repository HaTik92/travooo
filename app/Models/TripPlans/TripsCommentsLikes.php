<?php

namespace App\Models\TripPlans;

use Illuminate\Database\Eloquent\Model;

class TripsCommentsLikes extends Model
{
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
}
