<?php

namespace App\Models\TripPlans;

use Illuminate\Database\Eloquent\Model;

class TripsUsers extends Model
{
    public function user()
    {
        return $this->hasOne('App\Models\User\User', 'id', 'users_id');
    }
}
