<?php

namespace App\Models\TripPlans;

use Illuminate\Database\Eloquent\Model;

class PlanActivityLogUser extends Model
{
    protected $table = 'plan_activity_logs_users';

    public function log()
    {
        return $this->belongsTo(PlanActivityLog::class, 'log_id');
    }
}
