<?php

namespace App\Models\TripPlans;

use Illuminate\Database\Eloquent\Model;

class TripsComments extends Model
{
    public function trip()
    {
        return $this->belongsTo(TripPlans::class, 'trips_id');
    }

    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
    
    public function likes()
    {
        return $this->hasMany('App\Models\TripPlans\TripsCommentsLikes');
    }

    public function medias()
    {
        return $this->hasMany('App\Models\TripPlans\TripsCommentsMedias');
    }

}
