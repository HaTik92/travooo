<?php

namespace App\Models\TripPlans;

use App\Http\Constants\CommonConst;
use App\Models\TripCities\TripCities;
use App\Models\TripCountries\TripCountries;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlaces\TripPlacesComments;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TripPlans extends Model
{
    use SoftDeletes;
    protected $table = 'trips';

    public $timestamps = false;

    protected $fillable = ['active'];


    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function cities()
    {
        return $this->belongsToMany('App\Models\City\Cities', 'trips_cities', 'trips_id', 'cities_id')
            ->withPivot('date', 'order');
    }

    public function countries()
    {
        return $this->belongsToMany('App\Models\Country\Countries', 'trips_countries', 'trips_id', 'countries_id')
            ->withPivot('date', 'order');
    }

    public function places()
    {
        return $this->belongsToMany('App\Models\Place\Place', 'trips_places', 'trips_id', 'places_id')
            ->withPivot('countries_id', 'cities_id', 'date', 'order', 'time', 'duration', 'budget', 'weather', 'comment', 'active')->whereNull('deleted_at');
    }

    public function published_places()
    {
        return $this->hasMany(TripPlaces::class, 'trips_id', 'id')
            ->with('place')
            ->where('versions_id', $this->active_version)
            ->whereNull('deleted_at');
    }

    public function trip_places_by_active_version()
    {
        return $this->hasMany(TripPlaces::class, 'versions_id', 'active_version')->where('versions_id', '!=', 0);
    }

    public function join_requests()
    {
        return $this->hasOne('App\Models\TravelMates\TravelMatesRequests', 'plans_id');
    }

    public function versions()
    {
        return $this->hasMany('App\Models\TripPlans\TripsVersions', 'plans_id');
    }

    public function trips_cities()
    {
        return $this->hasMany(TripCities::class, 'trips_id');
    }

    public function trips_countries()
    {
        return $this->hasMany(TripCountries::class, 'trips_id');
    }

    public function trips_places()
    {
        return $this->hasMany('App\Models\TripPlaces\TripPlaces', 'trips_id');
    }

    public function myversion($user_id = null)
    {
        $id = $user_id ? $user_id : \Illuminate\Support\Facades\Auth::guard('user')->user()->id;
        return $this->hasMany('App\Models\TripPlans\TripsVersions', 'plans_id')
            ->where('authors_id', $id)->get();
    }

    public function contribution_requests()
    {
        return $this->hasMany('App\Models\TripPlans\TripsContributionRequests', 'plans_id');
    }

    public function latest_contribution_request()
    {
        return $this->hasOne('App\Models\TripPlans\TripsContributionRequests', 'plans_id')
            ->select('trips_contribution_requests.*')
            ->join('trips', 'plans_id', '=', 'trips.id')
            ->where(function ($query) {
                $query->where('status', '!=', TripInvitationsService::STATUS_REJECTED);
                $query->orWhere(function ($query) {
                    $query->where('status', TripInvitationsService::STATUS_REJECTED);
                    $query->where('trips.privacy', '!=',  TripsService::PRIVACY_PRIVATE);
                });
            })
            ->latest('updated_at');
    }

    public function users()
    {
        return $this->hasMany('App\Models\TripPlans\TripsUsers', 'trips_id');
        // return $this->belongsTo('App\Models\TripPlans\TripsUsers', 'users_id');
    }

    public function activeversion()
    {
        return $this->hasOne('App\Models\TripPlans\TripsVersions', 'plans_id')
            ->where('id', $this->active_version);
    }

    public function version()
    {
        return $this->hasOne('App\Models\TripPlans\TripsVersions', 'plans_id');
    }

    public function medias()
    {
        return $this->hasMany('App\Models\TripMedias\TripMedias');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\TripPlans\TripsLikes', 'trips_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')
            ->where('parents_id', '=', NULL)
            ->where('type', CommonConst::TYPE_TRIP)
            ->orderby('created_at', 'DESC');
    }

    public function allComments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')
            ->where('type', CommonConst::TYPE_TRIP);
    }

    public function shares()
    {
        return $this->hasMany('App\Models\TripPlans\TripsShares', 'trip_id');
    }

    public function postShares()
    {
        return $this->hasMany('App\Models\Posts\PostsShares', 'posts_type_id')
            ->where('type', CommonConst::TYPE_TRIP);
    }

    public function budget()
    {
        $budget = 0;
        $published_places = $this->published_places;
        if ($published_places)
            foreach ($published_places as $tp) {
                $budget += $tp->budget;
            }

        return $budget;
    }

    public function postComments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')
            ->where('parents_id', '=', NULL)
            ->where('type', CommonConst::TYPE_TRIP)
            ->orderby('created_at', 'DESC');
    }

    const ATTRIBUTE_ALL             = "all";
    const ATTRIBUTE_BUDGET          = "total_budget";
    const ATTRIBUTE_DURATION        = "total_duration";
    const ATTRIBUTE_DISTANCE        = "total_distance";
    const ATTRIBUTE_TOTAL_DESTINATION = "total_destination";
    const ATTRIBUTE_STARTING_DATE   = "start_date";
    const ATTRIBUTE_NUMBER_OF_DAYS  = "number_of_days";

    /**
     * 
     */
    public function _attribute($attribute = self::ATTRIBUTE_ALL)
    {
        $budget = 0;
        $durations = 0;
        $distances = 0;
        $start_date = null;
        $numberOfDays = 0;
        $totalDestination = 0;

        if (in_array($attribute, [self::ATTRIBUTE_ALL, self::ATTRIBUTE_DURATION, self::ATTRIBUTE_DISTANCE, self::ATTRIBUTE_BUDGET, self::ATTRIBUTE_NUMBER_OF_DAYS])) {
            $published_places = $this->published_places;
        }

        /* number of days */
        if (in_array($attribute, [self::ATTRIBUTE_ALL, self::ATTRIBUTE_NUMBER_OF_DAYS])) {
            if ($published_places) {
                $numberOfDays = $published_places->unique('date')->count();
            };
            if ($attribute != self::ATTRIBUTE_ALL) return $numberOfDays;
        }

        /* budget */
        if (in_array($attribute, [self::ATTRIBUTE_ALL, self::ATTRIBUTE_BUDGET])) {
            if ($published_places) {
                foreach ($published_places as $tp) {
                    $budget += $tp->budget;
                }
            };
            if ($attribute != self::ATTRIBUTE_ALL) return $budget;
        }

        /* starting_date */
        if (in_array($attribute, [self::ATTRIBUTE_ALL, self::ATTRIBUTE_STARTING_DATE])) {
            $activeversion = $this->activeversion;
            if ($activeversion && $activeversion->start_date) {
                $start_date = $activeversion->start_date->format("d F Y");
            }
            if ($attribute != self::ATTRIBUTE_ALL)  return $start_date;
        }

        /* duration */
        if (in_array($attribute, [self::ATTRIBUTE_ALL, self::ATTRIBUTE_DURATION])) {
            $tempDurations = 0;
            $trips_cities = $this->trips_cities;

            if ($published_places) {
                foreach ($published_places as $tp) {
                    $tempDurations += $tp->duration * 60;
                    $tempDurations += $tp->travel_duration;
                }
            }
            if ($trips_cities) {
                foreach ($trips_cities as $tc) {
                    $tempDurations += $tc->duration;
                }
            }

            if ($tempDurations > 0 && $tempDurations < 3600) {
                $durations = floor(($tempDurations / 60) % 60) . ' min';
            } elseif ($tempDurations >= 3600) {
                $durations = floor($tempDurations / 3600) . ' h ' . ((floor(($tempDurations / 60) % 60) != 0) ? floor(($tempDurations / 60) % 60) . ' min' : '');
            } else {
                $durations = $tempDurations . ' min';
            }

            if ($attribute != self::ATTRIBUTE_ALL) return $durations;
        }

        /* distance */
        if (in_array($attribute, [self::ATTRIBUTE_ALL, self::ATTRIBUTE_DISTANCE])) {
            $_tempDistances = 0;
            if ($published_places) {
                $beforePlace = null;
                foreach ($published_places as $tp) {
                    if ($beforePlace && $beforePlace->place && $tp->place && $beforePlace->place->id !== $tp->place->id) {
                        $_tempDistances += haversineGreatCircleDistance($beforePlace->place->lat, $beforePlace->place->lng, $tp->place->lat, $tp->place->lng);
                    }

                    $beforePlace = $tp;
                }
            }

            if ($_tempDistances > 0 && $_tempDistances < 1000) {
                $distances =  $_tempDistances . ' m';
            } elseif ($_tempDistances > 1000) {
                $distances =  round($_tempDistances / 1000, 2) . ' km';
            } else {
                $distances =  $_tempDistances;
            }

            if ($attribute != self::ATTRIBUTE_ALL)  return $distances;
        }

        /* total_destination */
        if (in_array($attribute, [self::ATTRIBUTE_ALL, self::ATTRIBUTE_TOTAL_DESTINATION])) {
            if ($published_places) {
                foreach ($published_places as $tp) {
                    if (!$tp->place or !$tp->country or !$tp->city) {
                        continue;
                    }
                    $totalDestination++;
                }
            }

            if ($attribute != self::ATTRIBUTE_ALL)  return $totalDestination;
        }

        /* all */
        return collect([
            self::ATTRIBUTE_BUDGET              => $budget,
            self::ATTRIBUTE_DURATION            => $durations,
            self::ATTRIBUTE_DISTANCE            => $distances,
            self::ATTRIBUTE_STARTING_DATE       => $start_date,
            self::ATTRIBUTE_NUMBER_OF_DAYS      => $numberOfDays,
            self::ATTRIBUTE_TOTAL_DESTINATION   => $totalDestination
        ]);
    }

    public function place_comments()
    {
        return $this->hasManyThrough(TripPlacesComments::class, TripPlaces::class, 'trips_id', 'trip_place_id', 'id', 'id');
    }
}
