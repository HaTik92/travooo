<?php

namespace App\Models\TripPlans;

use App\Models\Posts\PostsTags;
use App\Models\TripPlaces\TripPlaces;
use App\Services\Api\ShareService;
use Illuminate\Database\Eloquent\Model;

class TripsPlacesShares extends Model
{
    protected $table = 'trip_places_shares';

    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'user_id');
    }

    public function trip_place()
    {
        return $this->belongsTo(TripPlaces::class, 'trip_place_id');
    }

    public function tags()
    {
        return $this->hasMany('App\Models\Posts\PostsTags', 'posts_id')->where('posts_type', PostsTags::TYPE_TRIP_PLACE_SHARE);
    }

    public function postComments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')
            ->where('parents_id', '=', NULL)
            ->where('type', ShareService::TYPE_TRIP_PLACE_SHARE)
            ->orderby('created_at', 'DESC');
    }
}
