<?php

namespace App\Models\TripPlans;

use Illuminate\Database\Eloquent\Model;

class TripsCommentsMedias extends Model
{
    public function media()
    {
        return $this->hasOne('App\Models\ActivityMedia\Media', 'id', 'medias_id');
    }
}
