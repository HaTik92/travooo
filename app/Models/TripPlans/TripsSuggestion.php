<?php

namespace App\Models\TripPlans;

use App\Models\TripPlaces\TripPlaces;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class TripsSuggestion extends Model
{
    /**
     * @var string
     */
    protected $table = 'trips_suggestions';

    protected $casts = [
        'meta' => 'array'
    ];

    protected $fillable = [
        'is_saved',
        'is_published',
        'users_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function suggestedPlace()
    {
        return $this->belongsTo(TripPlaces::class, 'suggested_trips_places_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activePlace()
    {
        return $this->belongsTo(TripPlaces::class, 'active_trips_places_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plan()
    {
        return $this->belongsTo(TripPlans::class, 'trips_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function suggestionLogUsers()
    {
        return $this->hasMany(SuggestionUserLog::class, 'suggestion_id');
    }
}
