<?php

namespace App\Models\TripPlans;

use App\Models\TripPlaces\TripPlaces;
use Illuminate\Database\Eloquent\Model;

class TripsLikes extends Model
{
    public function user()
    {
        return $this->hasOne('App\Models\User\User', 'id', 'users_id');
    }

    public function tripPlace()
    {
        return $this->belongsTo(TripPlaces::class, 'places_id');
    }

    public function trip()
    {
        return $this->belongsTo(TripPlans::class, 'trips_id');
    }
}
