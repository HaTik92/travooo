<?php

namespace App\Models\TripPlans;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class SuggestionUserLog extends Model
{
    protected $table = 'suggestions_logs_users';

    protected $fillable = ['user_id', 'suggestion_id', 'unread'];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function suggestion()
    {
        return $this->hasOne(TripsSuggestion::class, 'id', 'suggestion_id');
    }
}
