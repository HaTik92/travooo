<?php

namespace App\Models\TripPlans;

use Illuminate\Database\Eloquent\Model;

class TripsDescriptions extends Model
{
    protected $table = 'trips_descriptions';
     
    protected $fillable = ['description', 'images'];
}
