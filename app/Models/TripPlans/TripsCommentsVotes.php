<?php

namespace App\Models\TripPlans;

use Illuminate\Database\Eloquent\Model;

class TripsCommentsVotes extends Model
{
    protected $table = 'trips_comments_votes';
}
