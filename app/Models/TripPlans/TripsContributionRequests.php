<?php

namespace App\Models\TripPlans;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TripsContributionRequests extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'authors_id');
    }

    public function plan()
    {
        return $this->belongsTo(TripPlans::class, 'plans_id');
    }
}
