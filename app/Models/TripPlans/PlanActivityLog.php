<?php

namespace App\Models\TripPlans;

use Illuminate\Database\Eloquent\Model;

class PlanActivityLog extends Model
{
    const TYPE_PUBLISHED_PLAN = 0;
    const TYPE_CHANGED_PRIVACY_SETTINGS = 1;
    const TYPE_ADDED_PLACE = 2;
    const TYPE_EDITED_PLACE = 3;
    const TYPE_REMOVED_PLACE = 4;
    const TYPE_SUGGESTED_NEW_PLACE = 5;
    const TYPE_SUGGESTED_EDIT_PLACE = 6;
    const TYPE_SUGGESTED_REMOVAL_PLACE = 7;
    const TYPE_APPROVED_SUGGESTION = 8;
    const TYPE_DISAPPROVED_SUGGESTION = 9;
    const TYPE_INVITED_TO_PLAN = 10;
    const TYPE_REMOVED_FROM_PLAN = 11;
    const TYPE_JOINED_THE_PLAN = 12;
    const TYPE_LEFT_THE_PLAN = 13;
    const TYPE_CHANGED_ROLE = 14;
    const TYPE_MEDIA_WAS_REMOVED_DUE_COPYRIGHT = 15;
    const TYPE_MEDIA_WAS_REMOVED_DUE_TERMS = 16;
    const TYPE_PLAN_TYPE_CHANGED = 17;
    const TYPE_PLAN_TYPE_CHANGED_TO_MEMORY = 18;
    const TYPE_CHANGED_PLAN_TITLE = 19;
    const TYPE_CHANGED_PLAN_DESCRIPTION = 20;
    const TYPE_CHANGED_PLAN_COVER = 21;
    const TYPE_REJECT_INVITATION = 22;

    const TYPE_MAPPING = [
        self::TYPE_PUBLISHED_PLAN => 'has published the plan',
        self::TYPE_CHANGED_PRIVACY_SETTINGS => 'has changed the privacy setting of the plan %privacy%',
        self::TYPE_ADDED_PLACE => 'added a place',
        self::TYPE_EDITED_PLACE => 'edited a place',
        self::TYPE_REMOVED_PLACE => 'removed a place',
        self::TYPE_SUGGESTED_NEW_PLACE => 'suggested a new place',
        self::TYPE_SUGGESTED_EDIT_PLACE => 'suggested edits to a place',
        self::TYPE_SUGGESTED_REMOVAL_PLACE => 'suggested removal of a place',
        self::TYPE_APPROVED_SUGGESTION => 'approved %username% suggestion',
        self::TYPE_DISAPPROVED_SUGGESTION => 'disapproved %username% suggestion',
        self::TYPE_INVITED_TO_PLAN => 'invited %username% to the plan as an %role%',
        self::TYPE_REMOVED_FROM_PLAN => 'has removed %username% (%role%) from the plan',
        self::TYPE_JOINED_THE_PLAN => 'has joined the plan as an %role%',
        self::TYPE_LEFT_THE_PLAN => '(%role%) has left the plan',
        self::TYPE_CHANGED_ROLE => 'changed %username%\'s role to %role%',
        self::TYPE_MEDIA_WAS_REMOVED_DUE_COPYRIGHT => 'A photo/video was removed from %event% of your plan due to a **copyright claim** by %username%',
        self::TYPE_MEDIA_WAS_REMOVED_DUE_TERMS => 'A photo/video was removed from %event% of your plan due to violation of our **terms**.',
        self::TYPE_PLAN_TYPE_CHANGED => 'Hope you had fun with the events of Day %X%. You can now attach media and write about your experiences!',
        self::TYPE_PLAN_TYPE_CHANGED_TO_MEMORY => 'This plan has now turned into a Memory Plan. Future events can no longer be added',
        self::TYPE_CHANGED_PLAN_TITLE => 'changed the plan title.',
        self::TYPE_CHANGED_PLAN_DESCRIPTION => 'changed the plan description.',
        self::TYPE_CHANGED_PLAN_COVER => 'changed the plan cover.',
        self::TYPE_REJECT_INVITATION => '(%role%) has rejected the invitation'
    ];

    protected $table = 'plan_activity_logs';

    protected $fillable = [
        'type',
        'trips_id',
        'entity_id',
        'user_id',
        'meta',
        'suggestion_id'
    ];

    protected $casts = [
        'meta' => 'array'
    ];
}
