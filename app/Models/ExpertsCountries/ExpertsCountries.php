<?php

namespace App\Models\ExpertsCountries;

use Illuminate\Database\Eloquent\Model;

class ExpertsCountries extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'users_id',
        'countries_id'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User\User::class, 'users_id');
    }

      public function countries()
    {
        return $this->belongsTo(\App\Models\Country\Countries::class, 'countries_id');
    }
}
