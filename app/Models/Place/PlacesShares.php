<?php

namespace App\Models\Place;

use App\Models\Place\Traits\Relationship\PlacesSharesRelationship;
use Illuminate\Database\Eloquent\Model;

class PlacesShares extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'places_shares';

    use PlacesSharesRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'place_id', 'users_id', 'scope', 'comment', 'created_at'];
}
