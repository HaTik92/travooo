<?php

namespace App\Models\Place\Traits\Attribute;

/**
 * Class SpamReportAttribute.
 */
trait SpamReportAttribute
{
    /**
     * @return string
     */
    public function getIgnoreButtonAttribute()
    {
        return '<a href="'.route('admin.location.spam_reports.ignore', ['spamPostId' => $this->id] ) . '" class="btn btn-xs btn-success">
                    <i class="fa fa-arrow-up" data-toggle="tooltip" data-placement="top" title="Ignore"></i>
                </a> ';
    }

    /**
     * @return string
     */
    public function getResolveButtonAttribute()
    {
        return '<a href="'.route('admin.location.spam_reports.resolve', ['spamPostId' => $this->id] ) . '" class="btn btn-xs btn-danger">
                    <i class="fa fa-arrow-down" data-toggle="tooltip" data-placement="top" title="Resolve"></i>
                </a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return
            $this->getIgnoreButtonAttribute().
            $this->getResolveButtonAttribute();
    }
}
