<?php

namespace App\Models\Place\Traits\Attribute;

/**
 * Class TopPlaceAttribute.
 */
trait TopPlaceAttribute
{

    /**
     * @return string
     */
    public function getUnmarkTopPlaceButtonAttribute()
    {
        return '<a href="'.route('admin.top_places.unmark_top_place' , 'id=' . $this->id ).'"
             data-method="delete"
             data-trans-button-cancel="'.trans('buttons.general.cancel').'"
             data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
             data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
             class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete').'"></i></a> ';

    }

    /**
     * @return string
     */
    public function getUnmarkTopHotelButtonAttribute()
    {
        return '<a href="'.route('admin.top_hotels.unmark_top_hotel' , 'id=' . $this->id ).'"
             data-method="delete"
             data-trans-button-cancel="'.trans('buttons.general.cancel').'"
             data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
             data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
             class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete').'"></i></a> ';

    }


    /**
     * @return string
     */
    public function getUnmarkTopRestaurantButtonAttribute()
    {
        return '<a href="'.route('admin.top_restaurants.unmark_top_restaurant' , 'id=' . $this->id ).'"
             data-method="delete"
             data-trans-button-cancel="'.trans('buttons.general.cancel').'"
             data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
             data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
             class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete').'"></i></a> ';

    }



    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return
            $this->getUnmarkTopPlaceButtonAttribute();
    }

    /**
     * @return string
     */
    public function getHotelActionButtonsAttribute()
    {
        return
            $this->getUnmarkTopHotelButtonAttribute();
    }

    /**
     * @return string
     */
    public function getRestaurantActionButtonsAttribute()
    {
        return
            $this->getUnmarkTopRestaurantButtonAttribute();
    }
}
