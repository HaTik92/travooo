<?php
namespace App\Models\Place\Traits\Relationship;

use App\Models\User\User;

trait PlaceFollowersRelationship
{
    public function followers()
    {
        return $this->hasMany(User::class, 'id', 'users_id');
    }
}