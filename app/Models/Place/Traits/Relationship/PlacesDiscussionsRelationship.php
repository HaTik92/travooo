<?php
namespace App\Models\Place\Traits\Relationship;

use App\Models\Place\PlacesDiscussionsDownvotes;
use App\Models\Place\PlacesDiscussionsUpvotes;
use App\Models\User\User;

trait PlacesDiscussionsRelationship
{
    /**
     * One-to-Many relations with UpVotes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function upVotes()
    {
        return $this->hasMany(PlacesDiscussionsUpvotes::class, 'discussions_id', 'id');
    }

    /**
     * One-to-Many relations with UpVotes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function downVotes()
    {
        return $this->hasMany(PlacesDiscussionsDownvotes::class, 'discussions_id', 'id');
    }

    /**
     * Many-to-One relations with Users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}