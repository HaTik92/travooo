<?php

namespace App\Models\Place\Traits\Relationship;

use App\Models\Place\Medias;
use App\Models\Place\PlaceFollowers;
use App\Models\Place\PlaceReviews;
use App\Models\Place\PlacesDiscussions;
use App\Models\Place\PlacesShares;
use App\Models\Place\PlaceStatistics;
use App\Models\Place\PlaceTranslations;
use App\Models\System\Session;
use App\Models\Access\User\SocialLogin;
use App\Models\SafetyDegree\SafetyDegree;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\PlaceTypes\PlaceTypes;
use App\Models\Place\PlaceMedias;
use App\Models\Discussion\Discussion;

/**
 * Class PlaceRelationship.
 */
trait PlacesRelationship
{
    protected $elasticSearchRelationMapping = [
        'translations' => [
            'class' => PlaceTranslations::class,
            'relation' => 'trans'
        ],
        'medias' => [
            'class' => Medias::class,
            'relation' => 'medias'
        ]
    ];
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('access.role'), config('access.role_user_table'), 'user_id', 'role_id');
    }

    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialLogin::class);
    }

    /**
     * Many-to-Many relations with CountriesTrans.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trans()
    {
        return $this->hasMany(config('locations.place_trans'), 'places_id');
    }

    public function transsingle()
    {
        return $this->hasOne(config('locations.place_trans'), 'places_id')->withDefault(function (){
            return new PlaceTranslations();
        });
    }

    /**
     * @return mixed
     */
    public function country()
    {
        return $this->hasOne(Countries::class , 'id' , 'countries_id');
    }

    /**
     * @return mixed
     */
    public function city()
    {
        return $this->hasOne(Cities::class , 'id' , 'cities_id');
    }

    /**
     * @return mixed
     */
    public function type()
    {
        return $this->hasOne(PlaceTypes::class , 'id' , 'place_type_ids');
    }

    /**
     * @return mixed
     */
    public function degree()
    {
        return $this->hasOne(SafetyDegree::class , 'id' , 'safety_degrees_id');
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }
    
    /**
     * One-to-Many relations with Discussion.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discussionsingle()
    {
        return $this->hasMany(Discussion::class, 'destination_id', 'id');
    }

    /**
     * @return mixed
     */
    public function medias()
    {
        return $this->hasMany(PlaceMedias::class , 'places_id' , 'id');
    }

    /**
     * @return mixed
     */
    public function first_media()
    {
        return $this->hasOne(PlaceMedias::class , 'places_id' , 'id');
    }

    /**
     * Many-to-Many relations with PlacesMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getMedias()
    {
        return $this->belongsToMany(Medias::class, 'places_medias', 'places_id', 'medias_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Reviews\Reviews', 'places_id', 'id');
    }

    public function followers()
    {
        return $this->hasMany(PlaceFollowers::class, 'places_id', 'id');
    }

    public function shares()
    {
        return $this->hasMany(PlacesShares::class, 'place_id', 'id');
    }

    /**
     * One-to-One relations with Statistics.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function statistics()
    {
        return $this->hasOne(PlaceStatistics::class, 'places_id');
    }

    /**
     * One-to-Many relations with Places.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discussions()
    {
        return $this->hasMany(PlacesDiscussions::class, 'places_id', 'id');
    }
    
    public function versions()
    {
        return $this->hasMany('App\Models\TripPlaces\TripPlaces', 'places_id');
    }
    
    public function checkins()
    {
        return $this->hasMany('App\Models\Posts\Checkins', 'place_id');
    }
    
    public function posts()
    {
        return $this->belongsToMany('App\Models\Posts\Posts', 'posts_places', 'places_id');
    }
    
}
