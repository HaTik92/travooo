<?php

namespace App\Models\Place;

use Illuminate\Database\Eloquent\Model;
use App\Models\Place\Traits\Relationship\PlaceReviewRelationship;

class PlaceReviews extends Model
{
    protected $table    = 'places_reviews';
    public $timestamps  = true;

    use PlaceReviewRelationship;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'places_id', 'users_id', 'score', 'comment', 'ip_address'];

}
