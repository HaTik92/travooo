<?php

namespace App\Models\Place;

use App\Models\Place\Traits\Attribute\UnapprovedItemAttribute;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Place\Traits\Relationship\PlacesRelationship;
use Elasticquent\ElasticquentTrait;

/**
 * @property int provider_id
 * @property int|null id
 */
class UnapprovedItem extends Model {

    use ElasticquentTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'places';
    protected $mappingProperties = array(
        'title' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'type' => [
            'type' => 'text',
            "analyzer" => "standard",
        ]
    );

    use PlacesRelationship,
        UnapprovedItemAttribute;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['region_id', 'code', 'lat', 'lng', 'safety_degree', 'active',
        'place_type', 'safety_degrees_id', 'provider_id', 'countries_id', 'cities_id', 'rating', 'media_count'];

    /**
     * @return mixed
     * */
    public function deleteMedias() {
        $this->deleteModals($this->medias);
    }

    /**
     * @return mixed
     * */
    public function deleteModals($models) {
        if (!empty($models)) {
            foreach ($models as $key => $value) {
                $value->delete();
            }
        }
    }

    public function trips() {
        return $this->belongsToMany('App\Models\TripPlans\TripPlans', 'trips_places', 'places_id', 'trips_id');
    }

    public function scopeCloseTo(Builder $query, $latitude, $longitude) {
        return $query->selectRaw("
           *,
           ST_Distance(
                point(lng, lat),
                point(?, ?)
           ) as distance
        ", [
                    $longitude,
                    $latitude,
        ]);
    }

    public function medias() {
        return $this->belongsToMany('\App\Models\ActivityMedia\Media', 'places_medias', 'places_id', 'medias_id');
    }

    function getIndexName() {
        return 'places';
    }
    
    function getTypeName()
    {
        return 'places';
    }

}
