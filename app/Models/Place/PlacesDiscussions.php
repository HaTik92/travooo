<?php

namespace App\Models\Place;

use App\Models\Place\Traits\Relationship\PlacesDiscussionsRelationship;
use Illuminate\Database\Eloquent\Model;

class PlacesDiscussions extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'places_discussions';

    use PlacesDiscussionsRelationship;

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'thread_id' , 'parent_id', 'places_id', 'users_id', 'text', 'upvotes', 'downvotes', 'replies', 'created_at', 'updated_at'];
}
