<?php

namespace App\Models\Place;

use App\Models\Place\Traits\Relationship\PlacesStatisticsRelationship;
use Illuminate\Database\Eloquent\Model;

class PlaceStatistics extends Model
{
    protected $table    = 'places_statistics';
    public $timestamps  = false;

    use PlacesStatisticsRelationship;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['places_id', 'trips', 'followers', 'media', 'likes', 'reviews', 'shares'];
}
