<?php

namespace App\Models\Place;

use App\Models\Place\Traits\Relationship\PlaceFollowersRelationship;
use Illuminate\Database\Eloquent\Model;

class PlaceFollowers extends Model
{
    protected $table    = 'places_followers';
//    public $timestamps  = false;

    use PlaceFollowersRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'places_id', 'users_id'];
    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function place()
    {
        return $this->hasOne('App\Models\Place\Place', 'id',  'places_id');
    }
}
