<?php

namespace App\Models\Place;

use App\Models\Place\Traits\Relationship\PlacesDiscussionsUpvotesRelationship;
use Illuminate\Database\Eloquent\Model;

class PlacesDiscussionsUpvotes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'places_discussions_upvotes';

    use PlacesDiscussionsUpvotesRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'discussions_id', 'users_id', 'ipaddress', 'created_at'];
}
