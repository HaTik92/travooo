<?php

namespace App\Models\Place;

use App\Models\Country\Countries;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Reviews\Reviews;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Place\Traits\Relationship\PlacesRelationship;
use App\Models\Place\Traits\Attribute\PlaceAttribute;
use Elasticquent\ElasticquentTrait;

/**
 * @property int provider_id
 * @property int|null id
 */
class Place extends Model {

    use ElasticquentTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'places';
    protected $mappingProperties = array(
        'title' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'type' => [
            'type' => 'text',
            "analyzer" => "standard",
        ]
    );

    use PlacesRelationship,
        PlaceAttribute;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['auto_import', 'region_id', 'code', 'lat', 'lng', 'safety_degree', 'active',
        'place_type', 'safety_degrees_id', 'provider_id', 'pluscode', 'countries_id', 'cities_id', 'rating', 'media_count'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return mixed
     * */
    public function deleteMedias() {
        $this->deleteModals($this->medias);
    }

    /**
     * @return mixed
     * */
    public function deleteModals($models) {
        if (!empty($models)) {
            foreach ($models as $key => $value) {
                $value->delete();
            }
        }
    }

    public function trips() {
        return $this->belongsToMany('App\Models\TripPlans\TripPlans', 'trips_places', 'places_id', 'trips_id');
    }
    public function cities()
    {
        return $this->hasOne('App\Models\City\Cities', 'id',  'cities_id');
    }

    public function scopeCloseTo(Builder $query, $latitude, $longitude) {
        return $query->selectRaw("
           *,
           ST_Distance(
                point(lng, lat),
                point(?, ?)
           ) as distance
        ", [
                    $longitude,
                    $latitude,
        ]);
    }

    public function medias() {
        return $this->belongsToMany('\App\Models\ActivityMedia\Media', 'places_medias', 'places_id', 'medias_id');
    }

    public function media()
    {
        return $this->medias()->limit(1);
    }

    function getIndexName() {
        return 'places';
    }
    
    function getTypeName()
    {
        return 'places';
    }


    public function topPlace()
    {
        return $this->hasOne(PlacesTop::class, 'places_id');
    }

    public function events()
    {
        return $this->hasMany('App\Models\Events\Events', 'places_id');
    }

    public function isHotel(){
        $is_hotel=false;
        if(strpos($this->place_type, "lodging") === 0) {
            $is_hotel=true;
        }
        return $is_hotel;
    }
    public function trans()
    {
        return $this->hasMany(config('locations.place_trans'), 'places_id');
    }

    public function reviews()
    {
        return $this->hasMany(Reviews::class, 'places_id');
    }


//    China GPS shift problem

    /**
     * transform
     * @param float $x
     * @param float $y
     * @return float[]
     */
    private static function transform($x, $y)
    {
        $xy = $x * $y;
        $absX = sqrt(abs($x));
        $d = (20.0 * sin(6.0 * $x * pi()) + 20.0 * sin(2.0 * $x * pi())) * 2.0 / 3.0;

        $lat = -100.0 + 2.0 * $x + 3.0 * $y + 0.2 * $y * $y + 0.1 * $xy + 0.2 * $absX;
        $lng = 300.0 + $x + 2.0 * $y + 0.1 * $x * $x + 0.1 * $xy + 0.1 * $absX;

        $lat += $d;
        $lng += $d;

        $lat += (20.0 * sin($y * pi()) + 40.0 * sin($y / 3.0 * pi())) * 2.0 / 3.0;
        $lng += (20.0 * sin($x * pi()) + 40.0 * sin($x / 3.0 * pi())) * 2.0 / 3.0;

        $lat += (160.0 * sin($y / 12.0 * pi()) + 320 * sin($y / 30.0 * pi())) * 2.0 / 3.0;
        $lng += (150.0 * sin($x / 12.0 * pi()) + 300.0 * sin($x / 30.0 * pi())) * 2.0 / 3.0;

        return [$lat, $lng];
    }

    /**
     * delta
     * @param float $lat
     * @param float $lng
     * @return float[] [$lat, $lng]
     */
    private static function delta($lat, $lng)
    {
        $a = 6378137.0;
        $ee = 0.00669342162296594323;
        list($dLat, $dLng) = self::transform($lng - 105.0, $lat - 35.0);
        $radLat = $lat / 180.0 * pi();
        $magic = sin($radLat);
        $magic = 1 - $ee * $magic * $magic;
        $sqrtMagic = sqrt($magic);
        $dLat = ($dLat * 180.0) / (($a * (1 - $ee)) / ($magic * $sqrtMagic) * pi());
        $dLng = ($dLng * 180.0) / ($a / $sqrtMagic * cos($radLat) * pi());
        return [$dLat, $dLng];
    }

    public function getLatAttribute()
    {
        if ($this->countries_id !== Countries::CHINA_ID) {
            return $this->attributes['lat'];
        }

        $dLat = self::delta($this->attributes['lat'], $this->attributes['lng'])[0];
        return $this->attributes['lat'] - $dLat;
    }

    public function getLngAttribute()
    {
        if ($this->countries_id !== Countries::CHINA_ID) {
            return $this->attributes['lng'];
        }

        $dLng = self::delta($this->attributes['lat'], $this->attributes['lng'])[1];
        return $this->attributes['lng'] - $dLng;
    }
}
