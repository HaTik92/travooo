<?php

namespace App\Models\Place;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Medias extends Model
{
    protected $fillable = ["title", "image_src", "author_name", "author_link", "source_link", "license_name", "license_link"];
    protected $hidden = ["image_name_field"];

    /**
     * Many-to-Many relations with CitiesTrans.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trans()
    {
        return $this->hasMany('App\Models\ActivityMedia\MediaTranslations', 'medias_id');

    }

    /**
     * Many-to-Many relations with CountriesMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'users_medias', 'medias_id', 'users_id');
    }

}
