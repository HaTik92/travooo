<?php

namespace App\Models\Place;

use Illuminate\Database\Eloquent\Model;
use App\Models\Place\Traits\Relationship\PlaceTransRelationship;

class PlaceTranslations extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'places_trans';

    public $timestamps = false;

    use PlaceTransRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['places_id', 'address', 'phone', 'highlights', 'working_days', 'working_times', 'how_to_go', 'when_to_go', 'num_activities', 'popularity', 'conditions', 'price_level', 'num_checkins', 'history', 'languages_id', 'title', 'description'];

    public function place()
    {
        return $this->hasOne(Place::class, 'id','places_id');
    }
}
