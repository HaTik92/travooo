<?php

namespace App\Models\Reports;

use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Place\Traits\Attribute\SpamReportAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportsDraft extends Model
{


    protected $table = 'reports_drafts';

    public $timestamps = true;

    use SpamReportAttribute,
        SoftDeletes;
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function infos()
    {
        return $this->hasMany('App\Models\Reports\ReportsDraftInfos', 'reports_draft_id');
    }

    public function cover()
    {
        return $this->hasMany('App\Models\Reports\ReportsDraftInfos', 'reports_draft_id')
            ->where('var', '=', 'cover')
            ->take(100);
    }

    public function map()
    {
        return $this->hasMany('App\Models\Reports\ReportsDraftInfos', 'reports_draft_id')
            ->where('var', '=', 'map')
            ->take(100);
    }

    public function photo()
    {
        return $this->hasMany('App\Models\Reports\ReportsDraftInfos', 'reports_draft_id')
            ->where('var', '=', 'photo')
            ->take(100);
    }

    public function video()
    {
        return $this->hasMany('App\Models\Reports\ReportsDraftInfos', 'reports_draft_id')
            ->where('var', '=', 'video')
            ->take(100);
    }


    // don't use more this relationship
    public function countryOrCity()
    {
        if ($this->countries_id) {
            return $this->belongsTo('App\Models\Country\Countries', 'countries_id');
        } else {
            return $this->belongsTo('App\Models\City\Cities', 'cities_id');
        }
    }

    // don't use more this relationship
    public function country()
    {
        return $this->belongsTo('App\Models\Country\Countries', 'countries_id');
    }

    // don't use more this relationship
    public function city()
    {
        return $this->belongsTo('App\Models\City\Cities', 'cities_id');
    }

    /**
     * don't use more countries_id and cities_id please use below relationship
     */
    public function location()
    {
        return $this->hasMany('App\Models\Reports\ReportsDraftsLocation', 'reports_draft_id');
    }

    public function prettyLocation()
    {
        $return = [Reports::LOCATION_COUNTRY => [], Reports::LOCATION_CITY => []];
        $this->location->each(function ($item) use (&$return) {
            if ($item->location_type == Reports::LOCATION_COUNTRY) {
                $return[Reports::LOCATION_COUNTRY][] = $item->location_id;
            } else if ($item->location_type == Reports::LOCATION_CITY) {
                $return[Reports::LOCATION_CITY][] = $item->location_id;
            }
        });

        if (count($return[Reports::LOCATION_CITY])) {
            $return[Reports::LOCATION_CITY] = Cities::with('transsingle', 'country', 'country.transsingle')->whereIn('id', $return[Reports::LOCATION_CITY])->get();
        }
        if (count($return[Reports::LOCATION_COUNTRY])) {
            $return[Reports::LOCATION_COUNTRY] = Countries::with('transsingle')->whereIn('id', $return[Reports::LOCATION_COUNTRY])->get();
        }

        $return['total'] = count($return[Reports::LOCATION_CITY]) + count($return[Reports::LOCATION_COUNTRY]);
        return $return;
    }
}
