<?php

namespace App\Models\Reports;

use App\Http\Constants\CommonConst;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Place\Traits\Attribute\SpamReportAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reports extends Model
{
    public $timestamps = true;

    const LOCATION_COUNTRY = 'country';
    const LOCATION_CITY = 'city';

    const LOCATION_MAPPING = [
        self::LOCATION_COUNTRY,
        self::LOCATION_CITY,
    ];

    use SpamReportAttribute,
        SoftDeletes;

    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function infos()
    {
        return $this->hasMany('App\Models\Reports\ReportsInfos', 'reports_id');
    }

    public function cover()
    {
        return $this->hasMany('App\Models\Reports\ReportsInfos', 'reports_id')
            ->where('var', '=', 'cover')
            ->take(100);
    }

    public function story()
    {
        return $this->hasOne('App\Models\Reports\ReportsInfos', 'reports_id')
            ->where('var', '=', 'text');
    }

    public function map()
    {
        return $this->hasMany('App\Models\Reports\ReportsInfos', 'reports_id')
            ->where('var', '=', 'map')
            ->take(100);
    }

    public function photo()
    {
        return $this->hasMany('App\Models\Reports\ReportsInfos', 'reports_id')
            ->where('var', '=', 'photo')
            ->take(100);
    }

    public function video()
    {
        return $this->hasMany('App\Models\Reports\ReportsInfos', 'reports_id')
            ->where('var', '=', 'video')
            ->take(100);
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Reports\ReportsLikes', 'reports_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')
            ->where('parents_id', '=', NULL)
            ->where('type', CommonConst::TYPE_REPORT)
            ->orderby('created_at', 'DESC');
    }

    public function allComments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')
            ->where('type', CommonConst::TYPE_REPORT);
    }

    public function shares()
    {
        return $this->hasMany('App\Models\Reports\ReportsShares', 'reports_id');
    }

    public function postShares()
    {
        return $this->hasMany('App\Models\Posts\PostsShares', 'posts_type_id')
            ->where('type', CommonConst::TYPE_REPORT);
    }

    public function views()
    {
        return $this->hasMany('App\Models\Reports\ReportsViews', 'reports_id');
    }

    // don't use more this relationship
    public function countryOrCity()
    {
        if ($this->countries_id) {
            return $this->belongsTo('App\Models\Country\Countries', 'countries_id');
        } else {
            return $this->belongsTo('App\Models\City\Cities', 'cities_id');
        }
    }

    // don't use more this relationship
    public function country()
    {
        return $this->belongsTo('App\Models\Country\Countries', 'countries_id');
    }

    // don't use more this relationship
    public function city()
    {
        return $this->belongsTo('App\Models\City\Cities', 'cities_id');
    }

    public function draft_report()
    {
        return $this->hasOne('App\Models\Reports\ReportsDraft', 'reports_id');
    }

    public function postComments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')
            ->where('parents_id', '=', NULL)
            ->where('type', CommonConst::TYPE_REPORT)
            ->orderby('id', 'DESC');
    }

    public function places()
    {
        return $this->hasMany('App\Models\Reports\ReportsPlaces', 'reports_id');
    }

    /**
     * don't use more countries_id and cities_id please use below relationship
     */
    public function location()
    {
        return $this->hasMany('App\Models\Reports\ReportsLocation', 'reports_id');
    }

    public function prettyLocation()
    {
        $return = [self::LOCATION_COUNTRY => [], self::LOCATION_CITY => []];
        $this->location->each(function ($item) use (&$return) {
            if ($item->location_type == self::LOCATION_COUNTRY) {
                $return[self::LOCATION_COUNTRY][] = $item->location_id;
            } else if ($item->location_type == self::LOCATION_CITY) {
                $return[self::LOCATION_CITY][] = $item->location_id;
            }
        });

        if (count($return[self::LOCATION_CITY])) {
            $return[self::LOCATION_CITY] = Cities::with('transsingle', 'country', 'country.transsingle')->whereIn('id', $return[self::LOCATION_CITY])->get();
        }
        if (count($return[self::LOCATION_COUNTRY])) {
            $return[self::LOCATION_COUNTRY] = Countries::with('transsingle')->whereIn('id', $return[self::LOCATION_COUNTRY])->get();
        }

        $return['total'] = count($return[self::LOCATION_CITY]) + count($return[self::LOCATION_COUNTRY]);
        return $return;
    }

    public function countriesIds()
    {
        $prettyLocation = $this->prettyLocation();
        return collect($prettyLocation[self::LOCATION_CITY])->pluck('country.id')
            ->merge(collect($prettyLocation[self::LOCATION_COUNTRY])->pluck('id'))->unique();
    }
}
