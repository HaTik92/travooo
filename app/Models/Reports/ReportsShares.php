<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportsShares extends Model
{
    public $timestamps = true;
    
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
    
}
