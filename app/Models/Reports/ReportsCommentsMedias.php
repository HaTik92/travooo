<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportsCommentsMedias extends Model
{
    public function media()
    {
        return $this->hasOne('App\Models\ActivityMedia\Media', 'id', 'medias_id');
    }
}
