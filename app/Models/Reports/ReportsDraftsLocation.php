<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportsDraftsLocation extends Model
{

    use SoftDeletes;

    protected $table = 'reports_drafts_locations';
    public $timestamps = false;

    protected $guarded = [];

    public function report_draft()
    {
        return $this->belongsTo('App\Models\Reports\ReportsDraft', 'reports_draft_id');
    }

    public function report()
    {
        return $this->belongsTo('App\Models\Reports\Reports', 'reports_id');
    }
}
