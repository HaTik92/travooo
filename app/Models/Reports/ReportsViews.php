<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportsViews extends Model
{
    public $timestamps = true;
    
    public function repots()
    {
        return $this->belongsTo('App\Models\Reports\Reports', 'reports_id', 'id');
    }
}
