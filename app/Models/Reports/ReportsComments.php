<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportsComments extends Model
{
    public $timestamps = true;
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Reports\ReportsCommentsLikes', 'comments_id');
    }
    
    public function medias()
    {
        return $this->hasMany('App\Models\Reports\ReportsCommentsMedias');
    }

    public function report()
    {
        return $this->belongsTo(Reports::class, 'reports_id');
    }

}
