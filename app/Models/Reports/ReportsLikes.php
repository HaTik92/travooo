<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportsLikes extends Model
{
    public $timestamps = true;
    
    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function report()
    {
        return $this->belongsTo(Reports::class, 'reports_id');
    }
}
