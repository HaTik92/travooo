<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportsDraftInfos extends Model
{
    
    protected $table = 'reports_draft_infos';

    protected $fillable = ['reports_draft_id', 'var', 'val', 'order'];
    
    public $timestamps = true;
    
    public function report_draft()
    {
        return $this->belongsTo('App\Models\Reports\ReportsDraft', 'reports_draft_id');
    }
}
