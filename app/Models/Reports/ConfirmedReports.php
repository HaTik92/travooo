<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ConfirmedReports extends Model
{
    public $timestamps = true;

    protected $table = 'confirmed_reports';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'admin_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function spamsPost()
    {
        return $this->belongsTo('App\Models\Posts\SpamsPosts', 'spam_post_id', 'id');
    }
}
