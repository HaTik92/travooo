<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportsLocation extends Model
{

    use SoftDeletes;

    protected $table = 'reports_locations';
    public $timestamps = false;

    protected $guarded = [];

    public function report()
    {
        return $this->belongsTo('App\Models\Reports\Reports', 'reports_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country\Countries', 'location_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City\Cities', 'location_id', 'id');
    }

}
