<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportsInfos extends Model
{
    public $timestamps = true;
    
    public function report()
    {
        return $this->belongsTo('App\Models\Reports\Reports', 'reports_id');
    }
}
