<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportsPlaces extends Model
{
    /*this table only for published reports and used in place module*/
    protected $table = 'reports_places';
    public $timestamps = false;

    protected $fillable = ['reports_id', 'places_id'];

    public function place()
    {
        return $this->hasOne('App\Models\Place\Place', 'id', 'places_id');
    }

    public function report()
    {
        return $this->belongsTo('App\Models\Reports\Reports', 'reports_id');
    }
}
