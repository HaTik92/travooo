<?php

namespace App\Models\States;

use Illuminate\Database\Eloquent\Model;

class StateTranslation extends Model
{
    /**
     * States Model pointing DB table
     * @var string
     */
    protected $table ='states_trans';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['states_id','language_id','title','description'];
}
