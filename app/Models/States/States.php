<?php

namespace App\Models\States;

use Illuminate\Database\Eloquent\Model;
use App\Models\States\StateTranslation;
class States extends Model
{
 
    /**
     * States Model pointing DB table
     * @var string
     */
    protected $table ='states';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function transsingle()
    {
        return $this->hasOne(StateTranslation::class, 'states_id');
    }
    protected $fillable = ['countries_id','lat','lng','is_province','active','cover_media_id'];

}
