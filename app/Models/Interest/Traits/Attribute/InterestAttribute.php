<?php

namespace App\Models\Interest\Traits\Attribute;

/**
 * Class InterestAttribute.
 */
trait InterestAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
