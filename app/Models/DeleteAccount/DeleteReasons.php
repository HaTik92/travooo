<?php

namespace App\Models\DeleteAccount;

use Illuminate\Database\Eloquent\Model;

class DeleteReasons extends Model
{
    protected $table = 'delete_reasons_list';

}
