<?php

namespace App\Models\DeleteAccount;

use Illuminate\Database\Eloquent\Model;

class DeletedUsers extends Model
{
    protected $table = 'deleted_users';

}
