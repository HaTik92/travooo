<?php

namespace App\Models\TripPlaces;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class TripPlacesCommentsLikes extends Model
{
    protected $table = 'trips_places_comments_likes';

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
