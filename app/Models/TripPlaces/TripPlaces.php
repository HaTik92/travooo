<?php

namespace App\Models\TripPlaces;

use App\Models\TripMedias\TripMedias;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TripPlaces extends Model
{
    use SoftDeletes;

    protected $table = 'trips_places';
   // protected $appends = ['transportation'];
    protected $fillable = ['travel_duration', 'distance'];
    
    public $timestamps = false;
    
    public function city()
    {
        return $this->belongsTo('App\Models\City\Cities', 'cities_id');
    }
    
    public function country()
    {
        return $this->belongsTo('App\Models\Country\Countries', 'countries_id');
    }
    
    public function trip()
    {
        return $this->belongsTo('App\Models\TripPlans\TripPlans', 'trips_id');
    }

    public function active_trip()
    {
        return $this->belongsTo('App\Models\TripPlans\TripPlans', 'versions_id', 'active_version');
    }
    
    public function place()
    {
        return $this->belongsTo('App\Models\Place\Place', 'places_id');
    }
    
    public function trip_description()
    {
        return $this->hasOne('App\Models\TripPlans\TripsDescriptions', 'trips_id', 'trips_id') 
                ->where('places_id', $this->places_id);
    }
    
    public function trip_comments()
    {
        return $this->hasMany('App\Models\TripPlans\TripsComments', 'trips_id', 'trips_id') 
                ->where('places_id', $this->places_id);
    }
    
    public function trip_description_likes()
    {
        return $this->hasMany('App\Models\TripPlans\TripsLikes', 'trips_id', 'trips_id') 
                ->where('places_id', $this->places_id);
    }

    /**
     * @return mixed
     **/
    public function comments()
    {
        return $this->hasMany(TripPlacesComments::class, 'trip_place_id')->where('reply_to', '=', 0)->orderby('created_at', 'DESC');
    }

    public function medias()
    {
        return $this->hasMany(TripMedias::class, 'trip_place_id');
    }
}
