<?php

namespace App\Models\TripPlaces;

use Illuminate\Database\Eloquent\Model;

class TripPlacesCommentsMedias extends Model
{
    protected $table = 'trips_places_comments_medias';

    public function media()
    {
        return $this->hasOne('App\Models\ActivityMedia\Media', 'id', 'medias_id');
    }
}
