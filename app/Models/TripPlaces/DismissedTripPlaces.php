<?php

namespace App\Models\TripPlaces;

use Illuminate\Database\Eloquent\Model;

class DismissedTripPlaces extends Model
{
    protected $table = 'dismissed_trips_places';
    protected $fillable = ['trip_place_id', 'user_id'];
}
