<?php

namespace App\Models\TripPlaces;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Posts\PostsTags;


class TripPlacesComments extends Model
{
    protected $table = 'trips_places_comments';

    protected $fillable = [
        'comment',
        'trip_place_id',
        'user_id'
    ];

    public function tripPlace()
    {
        return $this->belongsTo(TripPlaces::class, 'trip_place_id');
    }

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function likes()
    {
        return $this->hasMany(TripPlacesCommentsLikes::class, 'trip_place_comment_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'reply_to', 'id');
    }

    public function replyTo()
    {
        return $this->hasOne(static::class, 'id', 'reply_to');
    }

    public function medias()
    {
        return $this->hasMany(TripPlacesCommentsMedias::class, 'trip_place_comment_id', 'id');
    }

    public function sub()
    {
        return $this->hasMany(self::class, 'reply_to');
    }

    public function tags()
    {
        return $this->hasMany('App\Models\Posts\PostsTags', 'posts_id')->where('posts_type', PostsTags::TYPE_TRIP_PLACE_COMMENT);
    }
}
