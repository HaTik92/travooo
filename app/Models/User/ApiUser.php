<?php

/* Folder Location */

namespace App\Models\User;

/* Dependencies */
use Laravel\Passport\HasApiTokens;
use App\Models\System\Session;

/**
 * Class ApiUser.
 */
class ApiUser extends User {

    use HasApiTokens;
    /*
     *   Api Level Functionality Will Be Defined In This Class For Users
     */

    /* Validate Arguments Passed From End Point */

    public static function validateInputParams($post) {

        $error = [];

        /* "username" validation */
        if (!isset($post['username']) || empty($post['username'])) {
            // \App::abort(400 , 'Username cannot be empty');
            array_push($error, 'Username not provided.');
        } else {

            /* Check Length Of Name Argument And Ensure It Is Between ( 8 - 32 ) characters */
            if (strlen($post['username']) < 8 || strlen($post['username']) > 32) {
                array_push($error, 'Length of username should be between (8-32) characters.');
            }

            if (!preg_match('/^[a-zA-Z0-9 ]+$/', $post['username'])) {
                array_push($error, 'Name can only contain alphanumeric characters.');
            }

            /* Check If User Exists For The "username" */
            $user = Self::where(['username' => $post['username']])->first();

            if (!empty($user)) {
                array_push($error, 'Username already taken.');
            }
        }

        /* "name" validation */
        if (!isset($post['name']) || empty($post['name'])) {
            array_push($error, 'Name not provided.');
        } else {
            /* Check Length Of Name Argument And Ensure It Is Between ( 8 - 32 ) characters */
            if (strlen($post['name']) < 8 || strlen($post['name']) > 32) {
                array_push($error, 'Length of name should be between (8-32) characters.');
            }

            if (!preg_match('/^[a-zA-Z0-9 ]+$/', $post['name'])) {
                array_push($error, 'Name can only contain alphanumeric characters.');
            }
        }

        /* "gender" validation */
        if (!isset($post['gender']) || ( empty($post['gender']) && $post['gender'] != 0 ) ) {
            array_push($error, 'Gender not provided.');
        } else {
            $message = 'Gender can only contain following values (' . Self::GENDER_MALE . '-Male, ' . Self::GENDER_FEMALE . '-Female, ' . Self::GENDER_UNSPECIFIED . '-Unspecified).';

            /* Check If Provided Gender Is An Integer */
            if (!preg_match('/^[0-9]+$/', $post['gender'])) {

                array_push($error, $message);
            } else {

                if ($post['gender'] != Self::GENDER_MALE && $post['gender'] != Self::GENDER_FEMALE && $post['gender'] != Self::GENDER_UNSPECIFIED) {

                    array_push($error, $message);
                }
            }
        }

        /* "email" validation */
        if (!isset($post['email']) || empty($post['email'])) {
            array_push($error, 'Email not provided.');
        } else {
            /* Check If Provided Email Argument Is In Email Format */
            if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
                array_push($error, "'" . $post['email'] . "' is not a valid email address.");
            }

            /* Check If User For Provided Email Exists */
            $model = Self::where(['email' => $post['email']])->first();

            if (!empty($model)) {
                array_push($error, 'Email already taken.');
            }
        }

        /* "password" validation */
        if (!isset($post['password']) || empty($post['password'])) {
            array_push($error, 'Password not provided.');
        } else {
            /* Check If Provided Password Matches The Required Format */
            if (!preg_match('/^[a-zA-Z0-9._]+$/', $post['password'])) {
                array_push($error, 'Password can only contain alphanumeric characters.');
            }
            /* Check If Provided Password Length Is Between ( 6-20 ) characters */
            if (strlen($post['password']) < 6 || strlen($post['password']) > 20) {
                array_push($error, 'Length of password should be between (6-20) characters.');
            }
        }

        /* "password_confirmation" validation */
        if (!isset($post['password_confirmation']) || empty($post['password_confirmation'])) {
            array_push($error, 'Confirm password not provided.');
        } else {

            /* Check If Confirm Password Length Is Between ( 6-20 ) characters */
            if (strlen($post['password_confirmation']) < 6 || strlen($post['password_confirmation']) > 20) {
                array_push($error, 'Length of confirm password should be between (6-20) characters.');
            }

            /* Check If Password Confirmation Matches The Required Format */
            if (!preg_match('/^[a-zA-Z0-9._]+$/', $post['password_confirmation'])) {
                array_push($error, 'Confirm password can only contain alphanumeric characters.');
            }
        }

        if (isset($post['password_confirmation']) && isset($post['password'])) {
            /* Check If "Password Confirmation" matches "Password" Argument */
            if ($post['password_confirmation'] != $post['password']) {
                array_push($error, 'Password and confirm password do not match.');
            }
        }

        /* If Mobile Number Doesn't Matches The Required Format, Return Error */
        if (isset($post['mobile_number'])) {

            if (!preg_match('/^[+][0-9 -]+$/', $post['mobile_number'])) {

                array_push($error, 'Invalid Mobile Number Format. Please Provide A Valid Mobile Number Format. E.g,(+000 00000000)');
            }
        }

        if (empty($error)) {
            return false;
        } else {
            return [
                'data' => [
                    'error' => 400,
                    'message' => $error,
                ],
                'status' => false
            ];
        }
    }

    public static function twitter_social_login($post){
        
        $errors = [];

        if(!isset($post['twuid']) || empty($post['twuid'])){
            $errors[] = 'Twitter user id not provided.';
        }

        if(!isset($post['email']) || empty($post['email'])){
            $errors[] = 'Email not provided.';
        }else{
            if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
                $errors[] = "'" . $post['email'] . "' is not a valid email address.";
            }
        }
        
        if(!empty($errors)){
            return Self::generateErrorMessage(false,404,$errors);
        }

        // $user = Self::where(['email' => $post['email'], 'social_key' => $post['twuid'], 'login_type' => Self::FACEBOOK])->first();
        $user = Self::where(['email' => $post['email']])->first();

        if(empty($user)){
            
            $user = new Self;

            $user->username   = 'Tw_' . $post['twuid'];
            $user->email      = $post['email'];
            $user->name       = $post['name'];
            $user->status     = 1;
            $user->password   = sha1('SC_123456');
            $user->login_type = Self::TWITTER;
            $user->social_key = $post['twuid']; 

            if($user->save()){
               
                /* Find Session For The Provided User */
                $session = Session::where([ 'user_id' => $user->id])->first();

                /* If Session Not Found Create New Session */
                if (empty($session)) {
                    $sessionData = [
                        'user_id' => $user->id
                    ];
                    /* Save User Data To Session */
                    session()->put('user', $user->getArrayResponse());
                    session()->save();
                    /* Find Last Entered Record In Session Array To Assign User Id */
                    $session = Session::orderBy('last_activity', 'desc')->first();
                    $session->user_id = $user->id;
                    /* Assign 63 character long string to the id of session */
                    $session->id = Self::generateRandomString();
                    $session->save();
                }

                $user->createActivationEntry();
                $user->sendActivationMessage();

                /* If Session Found, Return Session Token And User Information, with True Status */
                return [
                    'data' => [
                        'user'  => $user->getArrayResponse(),
                        'token' => $session->id,
                        'type'  => 'register'
                    ],
                    'success' => true,
                    'code'    => 200
                ];
            }else{
                return [
                    'success' => false,
                    'code' => 404,
                    'data' => 'Error saving data in DB.'
                ];
            }
        }else{

            $user->username   = 'TW_' . $post['twuid'];
            $user->login_type = Self::TWITTER;
            $user->social_key = $post['twuid'];

            if($user->save()){

                /* Find Session For The Provided User */
                $session = Session::where([ 'user_id' => $user->id])->first();

                /* If Session Not Found Create New Session */
                if (empty($session)) {
                    $sessionData = [
                        'user_id' => $user->id
                    ];
                    /* Save User Data To Session */
                    session()->put('user', $user->getArrayResponse());
                    session()->save();
                    /* Find Last Entered Record In Session Array To Assign User Id */
                    $session = Session::orderBy('last_activity', 'desc')->first();
                    $session->user_id = $user->id;
                    /* Assign 63 character long string to the id of session */
                    $session->id = Self::generateRandomString();
                    $session->save();
                }

                /* If Session Found, Return Session Token And User Information, with True Status */
                return [
                    'data' => [
                        'user'  => $user->getArrayResponse(),
                        'token' => $session->id,
                        'type'  => 'login'
                    ],
                    'success' => true,
                    'code'    => 200
                ];
            }else{
                return [
                    'success' => false,
                    'code' => 404,
                    'data' => 'Error saving data in DB.'
                ];   
            }
        }
    }

    public static function createUser($post) {

        /* New Api User */
        $model = new Self;

        /* Load Values In Model */
        $model->name = $post['name'];
        $model->username = $post['username'];
        $model->email = $post['email'];
        $model->password = bcrypt($post['password']);
        $model->gender = $post['gender'];
        $model->status = Self::STATUS_INACTIVE;

        if (isset($post['mobile_number']) && !empty($post['mobile_number'])) {
            $model->mobile = $post['mobile_number'];
        }

        if ($model->save()) {

            $model->createActivationEntry();
            $model->sendActivationMessage();
            return [
                'status' => true,
                'data' => $model->getArrayResponse()
            ];
        }
    }

    /* Send password reset request */

    public static function forgotPassword($post) {

        if (!isset($post['email']) || empty($post['email'])) {
            return Self::generateErrorMessage(false, 400, 'Email not provided.');
        }

        /* Check If Provided Email Matches The Email Format */
        if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
            return Self::generateErrorMessage(false, 400, "'" . $post['email'] . "' is not a valid email address.");
        }

        $model = Self::where([ 'email' => $post['email']])->first();

        if (empty($model)) {
            return Self::generateErrorMessage(false, 400, 'Email Doesn\'t Exists.');
        }

        $password_reset_token = Self::generateRandomString(20) . time() . Self::generateRandomString(20);

        $model->password_reset_token = $password_reset_token;
        $model->save();
        $model->sendPasswordResetEmail();

        return [
            'success' => true,
            'data' => [
                'message' => 'An email with password reset link is sent to your email account.',
                'token'   => $model->password_reset_token
            ],
            'code'  => 200
        ];
    }

    /* Return User Information In Array Format */

    public function getArrayResponse() {

        return [
            'id' => $this->id,
            'username' => $this->username,
            'name' => $this->name,
            'email' => $this->email,
            'status' => $this->status,
            'mobile_number' => $this->mobile,
            'address' => $this->address,
            'age' => $this->age,
            'nationality' => $this->nationality,
            'login_type' => $this->login_type
        ];
    }

    /* Return User Information In Array Format */

    public static function getArrayFormat($model) {

        return [
            'id' => $model->id,
            'username' => $model->username,
            'name' => $model->name,
            'email' => $model->email,
            'status' => $model->status,
            'mobile_number' => $model->mobile,
            'address' => $model->address,
            'age' => $model->age,
            'nationality' => $model->nationality
        ];
    }

    /* Send Activation Email To User's Email Address */

    public function sendActivationMessage() {
        $activation_code = Activation::where(['user_id' => $this->id])->first();

        $site_url = url('');
        $activation_url = $site_url . '/api/users/activate/' . $activation_code->token;

        $to = $this->email;

        $subject = 'Travoo Account Activation';
        $message = 'Click on the link given below to activate your travoo account.<br />
        <a href="' . $activation_url . '">Activate My Travoo Account</a>';
        $headers = 'From: travoo@abcd.com' . '\r\n' .
                'CC: travoo-test@abcd.com';

        // send email
        $mail_status = mail($to, $subject, $message, $headers);

        return true;
    }

    /* Send Mail To Reset Password With Reset Token */

    public function sendPasswordResetEmail() {

        $password_reset_code = $this->password_reset_token;

        $site_url = url('');
        $new_password_url = 'javascript:void(0);';

        $to = $this->email;

        $subject = 'Travoo Account Password Reset';
        // $message = 'Click on the link given below to reset your travoo account password.<br />
        // <a href="' . $new_password_url . '">Reset My Travoo Account Password</a>';
        // $message = 'Password reset token : <p>' . $this->password_reset_token . '</p>';
        $message = 'Password reset token: ' . $password_reset_code;

        $headers = 'From: travoo@abcd.com' . '\r\n' .
                'CC: travoo-test@abcd.com';

        // send email
        $mail_status = mail($to, $subject, $message, $headers);

        return true;
    }

    public function sendPasswordChangeEmail() {

        header('Access-Control-Allow-Origin: *');
        //if you need cookies or login etc
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 604800');
        //if you need special headers
        header('Access-Control-Allow-Headers: x-requested-with');

        $password_reset_code = $this->password_reset_token;

        $site_url = url('');
        $new_password_url = 'javascript:void(0);';

        $to = $this->email;

        $subject = 'Travoo Account Password Change';
        $message = 'Your Travoo Account Password Has Been Changed Successfully.';
        $headers = 'From: travoo@abcd.com' . '\r\n' .
                'CC: travoo-test@abcd.com';

        // send email
        $mail_status = mail($to, $subject, $message, $headers);

        return true;
    }

    public function sendDeactiveConfirmationEmail() {

        header('Access-Control-Allow-Origin: *');
        //if you need cookies or login etc
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 604800');
        //if you need special headers
        header('Access-Control-Allow-Headers: x-requested-with');

        $to = $this->email;

        $subject = 'Travoo Account Deactivation';
        $message = 'Your Travoo Account Has Been Deactivated Successfully.';
        $headers = 'From: travoo@abcd.com' . '\r\n' .
                'CC: travoo-test@abcd.com';

        // send email
        $mail_status = mail($to, $subject, $message, $headers);

        return true;
    }

    /**
     * @return string
     * */
    /* Generate Error Message With provided "status", "code" and "message" */

    public static function generateErrorMessage($status, $code, $message) {

        $response = [];
        /* If Code == null */
        if ($code) {
            $response = [
                'data'    => [ 'message' => [$message]],
                'code'    => $code,
                'success' => $status
            ];
        } else {
            $response = [
                'data'    =>  $message,
                'code'    => 200,
                'success' => $status
            ];
        }

        return $response;
    }

}
