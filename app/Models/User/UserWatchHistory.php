<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserWatchHistory extends Model
{
    protected $table = 'user_watch_log';
    protected $fillable = ['id','users_id','posts_id','type','watch_flag'];
}
