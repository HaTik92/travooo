<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersPrivacy.
 */
class UsersPrivacy extends Model
{
    const EVERYONE = 0;
    const FRIENDS = 1;
    const FOLLOWERS = 2;

    const COMMON_PRIVACY_MAPPING = [
        self::EVERYONE,
        self::FRIENDS,
        self::FOLLOWERS,
    ];

    const ONLY_PEOPLE_I_FOLLOWING = 1;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'users_privacy';

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'users_id');
    }
}
