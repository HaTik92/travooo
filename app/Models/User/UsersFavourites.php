<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersFavourites.
 */
class UsersFavourites extends Model
{
    public $timestamps = false;

	/**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'users_favourites';

    protected $fillable = [
        'fav_id',
        'fav_type',
        'users_id',
    ];

    /**
     * @return mixed
     */
    public function user(){
        return $this->hasOne( User::class , 'id' , 'users_id');
    }
}
