<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersBlocks.
 */
class SettingUsersBlocks extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'setting_users_block';

    /**
     * @return mixed
     */
    public function user(){
        return $this->hasOne( User::class , 'id' , 'users_id');
    }

    /**
     * @return mixed
     */
    public function blocked_user(){
        return $this->hasOne( User::class , 'id' , 'blocked_users_id');
    }
}
