<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class DeleteAccountRequest extends Model
{
    const TOKEN_LENGTH = 65;

    const STATUS_SEND_MAIL = 0;
    const STATUS_CONFIRM = 1;

    public $table = 'delete_account_requests';
    protected $guarded = [];
}
