<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use App\Models\LanguagesSpoken\LanguagesSpoken;

/**
 * Class UsersBlocks.
 */
class UsersContentLanguages extends Model
{
    const DEFAULT_LANGUAGES = 1;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'user_content_languages';

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'users_id');
    }

    /**
     * @return mixed
     */
    public function languages()
    {
        return $this->hasOne(LanguagesSpoken::class, 'id', 'language_id');
    }
}
