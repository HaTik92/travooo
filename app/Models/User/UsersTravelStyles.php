<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersBlocks.
 */
class UsersTravelStyles extends Model
{
    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'users_travel_styles';

    protected $fillable = [
        'users_id',
        'conf_lifestyles_id'
    ];

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'users_id');
    }

    public function travelstyle()
    {
        return $this->hasOne('App\Models\Travelstyles\Travelstyles', 'id', 'conf_lifestyles_id');
    }

    public function countriesLifestyles()
    {
        return $this->hasMany('\App\Models\Country\CountriesLifestyles', 'lifestyles_id', 'conf_lifestyles_id');
    }

    public function citiesLifestyles()
    {
        return $this->hasMany('\App\Models\City\CitiesLifestyles', 'lifestyles_id', 'conf_lifestyles_id');
    }
}
