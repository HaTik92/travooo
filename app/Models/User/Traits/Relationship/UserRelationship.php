<?php

namespace App\Models\User\Traits\Relationship;

use App\Models\City\Cities;
use App\Models\CopyrightInfringements\CopyrightInfringement;
use App\Models\Country\Countries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsPlaces\ExpertsPlaces;
use App\Models\ExpertsTravelStyles\ExpertsTravelStyles;
use App\Models\InviteExpertLinks\InviteExpertLink;
use App\Models\Notifications\Notifications;
use App\Models\Place\Place;
use App\Models\System\Session;
use App\Models\User\SocialLogin;
use App\Models\User\UsersFriends;
use App\Models\User\UsersBlocks;
use App\Models\User\UsersHiddenContent;
use App\Models\User\UsersFriendRequests;
use App\Models\User\UsersMedias;
use App\Models\User\UsersFavourites;
use App\Models\User\SettingUsersBlocks;
use App\Models\User\UsersContentLanguages;

use App\Models\Travelstyles\Travelstyles;
use App\Models\User\UsersTravelStyles;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('access.role'), config('access.role_user_table'), 'user_id', 'role_id');
    }


    public function travelstyles()
    {
        return $this->hasMany('App\Models\User\UsersTravelStyles', 'users_id', 'id');
    }

    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialLogin::class);
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    /**
     * @return mixed
     */
    public function user_friends()
    {

        return $this->hasMany(UsersFriends::class, 'users_id', 'id');
    }

    /**
     * @return mixed
     */
    public function user_blocks()
    {
        return $this->hasMany(UsersBlocks::class, 'users_id', 'id');
    }

    /**
     * @return mixed
     */
    public function setting_users_block()
    {
        return $this->hasMany(SettingUsersBlocks::class, 'users_id', 'id');
    }

    /**
     * @return mixed
     */
    public function content_languages()
    {
        return $this->hasMany(UsersContentLanguages::class, 'users_id', 'id');
    }

    /**
     * @return mixed
     */
    public function user_hidden_content()
    {

        return $this->hasMany(UsersHiddenContent::class, 'users_id', 'id');
    }

    /**
     * @return mixed
     */
    public function my_friend_requests()
    {
        return $this->hasMany(UsersFriendRequests::class, 'to', 'id');
    }

    /**
     * @return mixed
     */
    public function my_medias()
    {
        return $this->hasMany(UsersMedias::class, 'users_id', 'id');
    }

    /**
     * @return mixed
     */
    public function favourites()
    {
        return $this->hasMany(UsersFavourites::class, 'users_id', 'id');
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Posts\Posts', 'users_id', 'id');
    }


    public function get_followers()
    {
        return $this->hasMany('App\Models\UsersFollowers\UsersFollowers', 'users_id', 'id')->where('follow_type', 1);
    }

    public function privacy()
    {
        return $this->hasOne('App\Models\User\UsersPrivacy', 'users_id', 'id');
    }

    public function following_user()
    {
        return $this->hasMany('App\Models\UsersFollowers\UsersFollowers', 'followers_id', 'id')->where('follow_type', 1);
    }

    public function followingCountries()
    {
        return $this->hasMany('App\Models\Country\CountriesFollowers', 'users_id', 'id');
    }

    public function followingCities()
    {
        return $this->hasMany('App\Models\City\CitiesFollowers', 'users_id', 'id');
    }

    public function following_places()
    {
        return $this->hasMany('App\Models\Place\PlaceFollowers', 'users_id', 'id');
    }

    public function badges()
    {
        return $this->hasMany('App\Models\Ranking\RankingUsersBadges', 'users_id', 'id');
    }

    public function badge()
    {
        return $this->hasOne('App\Models\Ranking\RankingUsersBadges', 'users_id', 'id');
    }

    public function points()
    {
        return $this->hasOne('App\Models\Ranking\RankingUsersPoints', 'users_id', 'id');
    }

    public function reports()
    {
        return $this->hasMany('App\Models\Reports\Reports', 'users_id', 'id');
    }

    public function spam_reports()
    {
        return $this->hasMany('App\Models\Posts\SpamsPosts', 'users_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Reviews', 'by_users_id', 'id');
    }

    public function country_of_nationality()
    {
        return $this->hasOne('App\Models\Country\Countries', 'id', 'nationality');
    }

    public function user_checkIn()
    {
        return $this->hasMany('App\Models\Posts\Checkins', 'users_id', 'id');
    }

    /**
     * @return MorphToMany
     */
    public function favouritesCountries()
    {
        return $this->morphedByMany(Countries::class, 'fav', 'users_favourites', 'users_id');
    }

    /**
     * @return MorphToMany
     */
    public function favouritesPlaces()
    {
        return $this->morphedByMany(Place::class, 'fav', 'users_favourites', 'users_id');
    }

    /**
     * @return MorphToMany
     */
    public function favouritesCities()
    {
        return $this->morphedByMany(Cities::class, 'fav', 'users_favourites', 'users_id');
    }

    public function inviteExpertLink()
    {
        return $this->belongsTo(InviteExpertLink::class);
    }

    public function disapproveMessage()
    {
        return $this->hasMany(Notifications::class, 'users_id', 'id')->where('type', 'disapprove')->latest();
    }

    public function confirmedReports()
    {
        return $this->hasMany('App\Models\Reports\ConfirmedReports', 'owner_id', 'id');
    }

    public function confirmedReportsAuthor()
    {
        return $this->hasMany('App\Models\Reports\ConfirmedReports', 'users_id', 'id');
    }

    public function copyrightInfringement()
    {
        return $this->hasMany(CopyrightInfringement::class, 'owner_id', 'id')->withTrashed();
    }

    public function expertCountries()
    {
        return $this->hasMany(ExpertsCountries::class, 'users_id');
    }

    public function expertCities()
    {
        return $this->hasMany(ExpertsCities::class, 'users_id');
    }

    public function expertPlaces()
    {
        return $this->hasMany(ExpertsPlaces::class, 'users_id');
    }

    public function expertTravelStyles()
    {
        return $this->hasMany(ExpertsTravelStyles::class, 'users_id');
    }
}
