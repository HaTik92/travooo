<?php

namespace App\Models\User\Traits\Scope;

use App\Models\User\User;

/**
 * Class UserScope.
 */
trait UserScope
{
    /**
     * @param $query
     * @param bool $confirmed
     *
     * @return mixed
     */
    public function scopeConfirmed($query, $confirmed = true)
    {
        return $query->where('confirmed', $confirmed);
    }

    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }

    /**
     * @param $query
     * @param bool $isApproved
     * @return mixed
     */
    public function scopeApproved($query, $isApproved = true)
    {
        return $query->where('is_approved', $isApproved)->where('approved_type', '!=', 1);
    }

    /**
     * @param $query
     * @param bool $approvedType
     * @return mixed
     */
    public function scopeApprovedType($query, $approvedType = true)
    {
        return $query->where('is_approved', 1)->where('approved_type', $approvedType);
    }

    public function scopeInvited($query)
    {
        return $query->where('type', User::TYPE_INVITED_EXPERT);
    }
}
