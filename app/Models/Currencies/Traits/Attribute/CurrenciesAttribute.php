<?php

namespace App\Models\Currencies\Traits\Attribute;

/**
 * Class CurrenciesAttribute.
 */
trait CurrenciesAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
