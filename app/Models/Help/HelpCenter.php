<?php

namespace App\Models\Help;

use Illuminate\Database\Eloquent\Model;
use App\Models\Help\HelpCenterSubMenu;
use App\Models\Help\HelpCenterContent;

class HelpCenter extends Model
{
    protected $table = 'help_center_categories';
    protected $fillable = ['name'];

    
    // public $array = ['devices_faq', 'content', 'media'];

    function subMenues() {
    	return $this->hasMany(HelpCenterSubMenu::class, "category_id");
    }
}
