<?php

namespace App\Models\Help;

use Illuminate\Database\Eloquent\Model;

class HelpCenterLIkeDislike extends Model
{
    protected $table = 'help_center_contents_like_dislike';
    protected $fillable = ['like', 'dislike'];

}
