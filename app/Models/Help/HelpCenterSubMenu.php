<?php

namespace App\Models\Help;

use Illuminate\Database\Eloquent\Model;
use App\Models\Help\HelpCenterContent;
use App\Models\Help\HelpCenterLIkeDislike;
use App\Models\Help\HelpCenter;

class HelpCenterSubMenu extends Model
{
    protected $table = 'help_center_sub_menues';
    protected $fillable = ['title', 'position'];

    public function content() {
        return $this->hasMany(HelpCenterContent::class, "sub_menu_id");
    }

    public function LikesDislikesCount() {
        return [
            "likes" => HelpCenterLIkeDislike::where([["like", true], ["sub_menu_id", $this->id]])->count(),
            "dislikes" => HelpCenterLIkeDislike::where([["dislike", true], ["sub_menu_id", $this->id]])->count()
        ];
    }

    public function category() {
        return $this->belongsTo(HelpCenter::class);
    }
}
