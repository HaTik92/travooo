<?php

namespace App\Models\Help;

use Illuminate\Database\Eloquent\Model;

class HelpCenterContent extends Model
{
    protected $table = 'help_center_contents';
    protected $fillable = ['type','data', 'mobile_faq', 'desktop_faq', 'media_url'];
}
