<?php

namespace App\Models\EnlargedViews;

use Illuminate\Database\Eloquent\Model;

class EnlargedView extends Model
{
    protected $fillable = [
        'url_key'
    ];

    public function entity()
    {
        return $this->morphTo();
    }
}
