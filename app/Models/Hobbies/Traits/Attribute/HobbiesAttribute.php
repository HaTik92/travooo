<?php

namespace App\Models\Hobbies\Traits\Attribute;

/**
 * Class HobbiesAttribute.
 */
trait HobbiesAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
