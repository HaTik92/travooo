<?php

namespace App\Models\ActivityMedia;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;
use App\Models\Posts\PostsTags;


/**
 * Class MediasComments.
 */
class MediasComments extends Model
{
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'medias_comments';


    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'users_id');
    }

    public function author() //use in api
    {
        return $this->hasOne(User::class, 'id', 'users_id');
    }

    /**
     * @return mixed
     */
    public function media()
    {
        return $this->hasOne(Media::class, 'id', 'medias_id');
    }

    public  function likes()
    {
        return $this->hasMany(MediasCommentsLikes::class, 'medias_comments_id', 'id');
    }

    public function replyTo()
    {
        return $this->hasOne(static::class, 'id', 'reply_to');
    }

    public function medias()
    {
        return $this->hasMany('App\Models\ActivityMedia\MediascommentsMedias');
    }

    public function sub()
    {
        return $this->hasMany(self::class, 'reply_to');
    }

    public function tags()
    {
        return $this->hasMany('App\Models\Posts\PostsTags', 'posts_id')->where('posts_type', PostsTags::TYPE_TRIP_PLACE_MEDIA_COMMENT);
    }
}
