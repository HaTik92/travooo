<?php

namespace App\Models\ActivityMedia;

use App\Models\Posts\Posts;
use Illuminate\Database\Eloquent\Model;
use App\Models\ActivityMedia\Traits\Relationship\MediaRelationship;
use App\Models\ActivityMedia\Traits\Attribute\MediaAttribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User\User;

/**
 * @property mixed|string url
 * @property string $author_name
 * @property string $title
 * @property string $author_url
 * @property string $source_url
 * @property string $license_name
 * @property string $license_url
 * @property int id
 */
class Media extends Model
{
    const TYPE_IMAGE = 1;
    const TYPE_VIDEO = 2;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medias';

    use MediaRelationship,
        MediaAttribute,
        SoftDeletes;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'url', 'author_name',
        'author_url', 'source_url', 'license_name',
        'license_url', 'title', 'featured'
    ];

    /**
     * @return mixed
     **/
    public function deleteTrans()
    {
        self::deleteModels($this->trans);
    }

    /**
     * @return mixed
     **/
    public function deleteModels($models)
    {
        if (!empty($models)) {
            foreach ($models as $key => $value) {
                $value->delete();
            }
        }
    }

    /**
     * @return mixed
     **/
    public function comments()
    {
        return $this->hasMany('App\Models\ActivityMedia\MediasComments', 'medias_id')->where('reply_to', '=', 0)->orderby('created_at', 'DESC');
    }

    /**
     * @return mixed
     **/
    public function likes()
    {
        return $this->hasMany('App\Models\ActivityMedia\MediasLikes', 'medias_id');
    }

    /**
     * @return mixed
     **/
    public function shares()
    {
        return $this->hasMany('App\Models\ActivityMedia\MediasShares', 'medias_id');
    }

    /**
     * Many-to-Many relations with CountriesMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'users_medias', 'medias_id', 'users_id');
    }

    public function post()
    {
        return $this->belongsToMany(Posts::class, 'posts_medias', 'medias_id', 'posts_id');
    }

    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function postComments()
    {
        return $this->hasMany(MediasComments::class, 'medias_id')
            ->where('reply_to', '=', 0)
            ->orderby('created_at', 'DESC');
    }
}
