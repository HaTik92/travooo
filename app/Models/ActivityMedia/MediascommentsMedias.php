<?php

namespace App\Models\ActivityMedia;

use Illuminate\Database\Eloquent\Model;

class MediascommentsMedias extends Model
{
    public function media()
    {
        return $this->hasOne('App\Models\ActivityMedia\Media', 'id', 'medias_id');
    }
}
