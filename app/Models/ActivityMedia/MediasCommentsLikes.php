<?php

namespace App\Models\ActivityMedia;

use App\Models\ActivityMedia\Traits\Relationship\MediaCommentsLikesRelationship;
use Illuminate\Database\Eloquent\Model;

class MediasCommentsLikes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medias_comments_likes';

    use MediaCommentsLikesRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'medias_comments_id', 'users_id', 'created_at'];
    
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
}
