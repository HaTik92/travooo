<?php

namespace App\Models\ActivityMedia\Traits\Relationship;

use App\Models\System\Session;
use App\Models\Access\User\SocialLogin;
use App\Models\ActivityMedia\MediasLikes;
use App\Models\ActivityMedia\MediasComments;
use App\Models\Place\Place;
use App\Models\User\UsersMedias;

/**
 * Class MediaRelationship.
 */
trait MediaRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('access.role'), config('access.role_user_table'), 'user_id', 'role_id');
    }

    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialLogin::class);
    }

    /**
     * Many-to-Many relations with MediaTrans.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trans()
    {
        return $this->hasMany(config('activitymedia.media_trans'), 'medias_id' , 'id');
    }

    public function transsingle()
    {
        return $this->hasOne(config('activitymedia.media_trans'), 'medias_id' , 'id');
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    /**
     * @return mixed
     */
    public function likes()
    {
        return $this->hasMany(MediasLikes::class, 'medias_id' , 'id');
    }

    /**
     * @return mixed
     */
    public function comments()
    {
        return $this->hasMany(MediasComments::class, 'medias_id' , 'id');
    }

    /**
     * @return mixed
     */
    public function mediaUser(){
        return $this->hasOne( UsersMedias::class , 'medias_id' , 'id');
    }

    /**
     * @return mixed
     */
    public function replyTo(){
        return $this->hasOne( UsersMedias::class , 'medias_id' , 'id');
    }

    /**
     * @return mixed
     */
    public function getPlaces(){
        return $this->belongsToMany( Place::class , 'places_medias', 'medias_id', 'places_id');
    }
    
    public function users()
    {
        return $this->belongsToMany('\App\Models\User\User', 'users_medias', 'medias_id', 'users_id');
    }
}
