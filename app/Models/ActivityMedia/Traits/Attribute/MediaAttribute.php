<?php

namespace App\Models\ActivityMedia\Traits\Attribute;

/**
 * Class MediaAttribute.
 */
trait MediaAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }

    /* This function returns Medias information in Array format */
    public function getArrayResponse(){

        /* Get All Translations In Array Format */
        $translations = [];

        if(!empty($this->trans)){
            foreach ($this->trans as $key => $value) {
                $translations[$value->languages_id] = [
                    'title' => $value->title,
                    'description' => $value->description
                ];
            }
        }

        $media_arr = [
            'id' => $this->id,
            'url' => $this->url,
            'translations' => $translations
        ];

        return $media_arr;
    }
}
