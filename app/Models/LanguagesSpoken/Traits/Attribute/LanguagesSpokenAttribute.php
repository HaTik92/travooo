<?php

namespace App\Models\LanguagesSpoken\Traits\Attribute;

/**
 * Class LanguagesSpokenAttribute.
 */
trait LanguagesSpokenAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
