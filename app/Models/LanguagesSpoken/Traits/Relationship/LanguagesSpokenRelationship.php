<?php

namespace App\Models\LanguagesSpoken\Traits\Relationship;

use App\Models\System\Session;

/**
 * Class LanguagesSpokenRelationship.
 */
trait LanguagesSpokenRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trans()
    {
        return $this->hasOne(config('languages_spoken.languages_spoken_trans'), 'languages_spoken_id');
    }

    public function transsingle()
    {
        return $this->hasOne(config('languages_spoken.languages_spoken_trans'), 'languages_spoken_id');
    }
    /**
     * @return mixed
     */

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }
}
