<?php

namespace App\Models\ActivityTypes\Traits\Attribute;

/**
 * Class CountryAttribute.
 */
trait ActivityTypesAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
