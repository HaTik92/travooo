<?php

namespace App\Models\TopCities;

use Illuminate\Database\Eloquent\Model;

class TopCities extends Model
{
       protected $fillable = ['cities_id','countries_id', 'no_of_reviews'];

}
