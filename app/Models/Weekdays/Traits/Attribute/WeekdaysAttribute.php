<?php

namespace App\Models\Weekdays\Traits\Attribute;

/**
 * Class WeekdaysAttribute.
 */
trait WeekdaysAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
