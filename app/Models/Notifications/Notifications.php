<?php

namespace App\Models\Notifications;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'authors_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
}
