<?php

namespace App\Models\PlacesTop;

use App\Models\City\Cities;
use Illuminate\Database\Eloquent\Model;
use App\Models\Place\Traits\Attribute\TopPlaceAttribute;

class PlacesTop extends Model
{

    protected $fillable = ['places_id', 'country_id', 'city_id', 'rating', 'city', 'country','ciites_id','country_id','travooo_title','title','destination_type','reviews_num','order_by_city'];

    protected $table = 'places_top';

    public $timestamps = false;

    use TopPlaceAttribute;
    
    public function place()
    {
        return $this->belongsTo('App\Models\Place\Place', 'places_id');
    }

    public function city()
    {
        return $this->belongsTo(Cities::class, 'city_id');
    }
}
