<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class PostsCommentsLikes extends Model
{
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
}
