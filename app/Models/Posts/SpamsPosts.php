<?php

namespace App\Models\Posts;

use App\Models\Place\Traits\Attribute\SpamReportAttribute;
use Illuminate\Database\Eloquent\Model;

class SpamsPosts extends Model
{
    use SpamReportAttribute;

    protected $primaryKey = "id";

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id' , 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function confirmedReports()
    {
        return $this->hasMany('App\Models\Reports\ConfirmedReports','spam_post_id', 'id' );
    }
}
