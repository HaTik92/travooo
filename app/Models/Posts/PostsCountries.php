<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class PostsCountries extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts_countries';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['posts_id', 'countries_id', 'places_id', 'cities_id'];

    public function country()
    {
        return $this->hasOne('App\Models\Country\Countries', 'id', 'countries_id');
    }

    public function post()
    {
        return $this->hasOne('App\Models\Posts\Posts', 'id', 'posts_id');
    }
}
