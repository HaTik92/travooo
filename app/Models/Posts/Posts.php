<?php

namespace App\Models\Posts;

use App\Http\Constants\CommonConst;
use App\Models\ActivityMedia\Media;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts extends Model
{
    use SoftDeletes;

    // Post type constants
    const POST_TYPE_POST = 0;
    const POST_TYPE_MEDIAUPLOAD = 1;
    const POST_TYPE_TRIP = 2;
    const POST_TYPE_CHECKIN = 3;
    const POST_TYPE_PLACEIMAGE = 5;
    const POST_TYPE_DISCUSSION = 6;
    const POST_TYPE_REPORT = 7;
    const POST_TYPE_REVIEW = 8;
    const POST_TYPE_EVENT = 9;

    const POST_PERMISSION_PUBLIC = 0;
    const POST_PERMISSION_FRIENDS_ONLY = 1;
    const POST_PERMISSION_PRIVATE = 2;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'users_id', 'text', 'created_at', 'updated_at', 'scope', 'permission'];

    public static function postTypes()
    {
        return [
            self::POST_TYPE_POST => "Post",
            self::POST_TYPE_MEDIAUPLOAD => "Mediaupload",
            self::POST_TYPE_TRIP => "Trip",
            self::POST_TYPE_CHECKIN => "Checkin",
            self::POST_TYPE_PLACEIMAGE => "PlaceImage",
            self::POST_TYPE_DISCUSSION => "Discussion",
            self::POST_TYPE_REPORT => "Report",
            self::POST_TYPE_REVIEW => "Review",
            self::POST_TYPE_EVENT => "Event"
        ];
    }

    public static function reportType()
    {
        return [
            ['type' => 0, 'text' => trans('labels.backend.spam_reports.spam')],
            ['type' => 2, 'text' => trans('labels.backend.spam_reports.fake_news')],
            ['type' => 3, 'text' => trans('labels.backend.spam_reports.harassment')],
            ['type' => 4, 'text' => trans('labels.backend.spam_reports.hate_speech')],
            ['type' => 5, 'text' => trans('labels.backend.spam_reports.nudity')],
            ['type' => 6, 'text' => trans('labels.backend.spam_reports.terrorism')],
            ['type' => 7, 'text' => trans('labels.backend.spam_reports.violence')],
            ['type' => 1, 'text' => trans('labels.backend.spam_reports.other')],
        ];
    }

    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function postCheckin()
    {
        return $this->belongsTo('App\Models\Posts\PostsCheckins', 'posts_id', 'id');
    }

    public function checkin()
    {
        return $this->belongsToMany('App\Models\Posts\Checkins', 'posts_checkins', 'posts_id', 'checkins_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Posts\PostsLikes');
    }

    public function comments()
    {
        return $this->hasMany(PostsComments::class)
            ->where('parents_id', '=', NULL)
            ->where('type', CommonConst::TYPE_POST)
            ->orderby('id', 'DESC');
    }

    public function allComments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments')
            ->where('type', CommonConst::TYPE_POST);
    }

    public function shares()
    {
        return $this->hasMany('App\Models\Posts\PostsShares');
    }

    public function postShares()
    {
        return $this->hasMany('App\Models\Posts\PostsShares', 'posts_type_id')
            ->where('type', CommonConst::TYPE_POST);
    }

    public function views()
    {
        return $this->hasMany('App\Models\Posts\PostsViews');
    }

    public function medias()
    {
        return $this->hasMany('App\Models\Posts\PostsMedia')->whereHas('media');
    }

    public function getMedias()
    {
        return $this->belongsToMany(Media::class, 'posts_medias', 'posts_id', 'medias_id');
    }

    public function spam_reports()
    {
        return $this->hasMany('App\Models\Posts\SpamsPosts', 'posts_id');
    }

    public function posts_place()
    {
        return $this->hasMany('App\Models\Posts\PostsPlaces', 'posts_id');
    }

    public function posts_city()
    {
        return $this->hasMany('App\Models\Posts\PostsCities', 'posts_id');
    }

    public function posts_country()
    {
        return $this->hasMany('App\Models\Posts\PostsCountries', 'posts_id');
    }

    public function tags()
    {
        return $this->hasMany('App\Models\Posts\PostsTags', 'posts_id')->where('posts_type', PostsTags::TYPE_POST);
    }
}
