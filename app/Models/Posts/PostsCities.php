<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class PostsCities extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts_cities';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['posts_id', 'cities_id', 'places_id'];

    public function city()
    {
        return $this->hasOne('App\Models\City\Cities', 'id', 'cities_id');
    }

    public function post()
    {
        return $this->hasOne('App\Models\Posts\Posts', 'id', 'posts_id');
    }
}
