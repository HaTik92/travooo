<?php

namespace App\Models\Posts;

use App\Models\ActivityMedia\Media;
use App\Models\Posts\PostsTags;
use Illuminate\Database\Eloquent\Model;

class PostsComments extends Model
{
    // Post Comment type constants
    const COMMENT_TYPE_CITYFOLLOW      = 0;
    const COMMENT_TYPE_COUNTRYFOLLOW   = 1;
    const COMMENT_TYPE_HOTELFOLLOW     = 2;
    const COMMENT_TYPE_HOTELREVIEW     = 3;
    const COMMENT_TYPE_MEDIACOMMENT    = 5;
    const COMMENT_TYPE_PLACEFOLLOW     = 6;
    const COMMENT_TYPE_PLACEREVIEW     = 7;
    const COMMENT_TYPE_POSTCHECKIN     = 8;
    const COMMENT_TYPE_STATUSCOMMENT   = 9;
    const COMMENT_TYPE_STATUSPUBLISH   = 10;
    const COMMENT_TYPE_TRIPCREATE      = 11;
    const COMMENT_TYPE_UPCOMINGEVENT   = 12;
    const COMMENT_TYPE_UPCOMINGHOLIDAY = 13;
    const COMMENT_TYPE_USERFOLLOW      = 14;
    const COMMENT_TYPE_CHECKINCOMMENT  = 15;
    const COMMENT_TYPE_TRIPCOMMENT     = 16;
    const COMMENT_TYPE_REPORTCOMMENT   = 17;
    const COMMENT_TYPE_EVENTCOMMENT    = 18;

    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Posts\PostsCommentsLikes');
    }

    public function medias()
    {
        return $this->hasMany('App\Models\Posts\PostsCommentsMedias');
    }
    public function media()
    {
        return $this->belongsToMany(Media::class, 'posts_comments_medias', 'posts_comments_id', 'medias_id');
    }

    public function sub()
    {
        return $this->hasMany(self::class, 'parents_id');
    }

    public function tags()
    {
        return $this->hasMany('App\Models\Posts\PostsTags', 'posts_id')->where('posts_type', PostsTags::TYPE_POSTS_COMMENTS);
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Posts\Posts', 'posts_id');
    }

    public static function commentTypes()
    {
        return [
            self::COMMENT_TYPE_CITYFOLLOW      => "Cityfollow",
            self::COMMENT_TYPE_COUNTRYFOLLOW   => "Countryfollow",
            self::COMMENT_TYPE_HOTELFOLLOW     => "Hotelfollow",
            self::COMMENT_TYPE_HOTELREVIEW     => "Hotelreview",
            self::COMMENT_TYPE_MEDIACOMMENT    => "Mediacomment",
            self::COMMENT_TYPE_PLACEFOLLOW     => "Placefollow",
            self::COMMENT_TYPE_PLACEREVIEW     => "Placereview",
            self::COMMENT_TYPE_POSTCHECKIN     => "Postcheckin",
            self::COMMENT_TYPE_STATUSCOMMENT   => "Postcomment",
            self::COMMENT_TYPE_STATUSPUBLISH   => "Statuscomment",
            self::COMMENT_TYPE_TRIPCREATE      => "Statuspublish",
            self::COMMENT_TYPE_UPCOMINGEVENT   => "Tripcreate",
            self::COMMENT_TYPE_UPCOMINGHOLIDAY => "Upcomingevent",
            self::COMMENT_TYPE_USERFOLLOW      => "Upcomingholiday",
            self::COMMENT_TYPE_CHECKINCOMMENT  => "Userfollow",
            self::COMMENT_TYPE_TRIPCOMMENT     => "Tripcomment",
            self::COMMENT_TYPE_REPORTCOMMENT   => "Reportcomment",
            self::COMMENT_TYPE_EVENTCOMMENT    => "Eventcomment",
        ];
    }
}
