<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Checkins extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'checkins';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['place_id', 'city_id', 'country_id', 'location', 'lat_lng'];


    public function place()
    {
        return $this->hasOne('App\Models\Place\Place', 'id', 'place_id');
    }

    public function country()
    {
        return $this->hasOne('App\Models\Country\Countries', 'id',  'country_id');
    }

    public function city()
    {
        return $this->hasOne('App\Models\City\Cities', 'id',  'city_id');
    }

    public function post_checkin()
    {

        return $this->hasOne('App\Models\Posts\PostsCheckins', 'checkins_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User\User', 'id',  'users_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Posts\CheckinsLikes');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Posts\CheckinsComments');
    }

    public function postComments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')->where('type', 'checkin');
    }
}
