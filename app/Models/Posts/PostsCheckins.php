<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class PostsCheckins extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts_checkins';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'checkins_id','posts_id'];
    
    public function checkin()
    {
        
        return $this->belongsTo('App\Models\Posts\Checkins', 'checkins_id');
    }
    
    public function post()
    {
        
        return $this->hasOne('App\Models\Posts\Posts', 'id', 'posts_id');
    }
}
