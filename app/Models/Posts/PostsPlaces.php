<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class PostsPlaces extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts_places';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['posts_id', 'places_id'];

    public function place()
    {
        return $this->hasOne('App\Models\Place\Place', 'id', 'places_id');
    }

    public function post()
    {
        return $this->hasOne('App\Models\Posts\Posts', 'id', 'posts_id');
    }
}
