<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;
use App\Services\Api\ShareService;

class PostsShares extends Model
{
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Posts\Posts', 'posts_id');
    }

    public function reference()
    {
        switch (trim(strtolower($this->type))) {
            case ShareService::TYPE_DISCUSSION:
                return $this->belongsTo('App\Models\Discussion\Discussion', 'posts_type_id');
                break;

            case ShareService::TYPE_EVENT:
                return $this->belongsTo('App\Models\Events\Events', 'posts_type_id');
                break;

            case ShareService::TYPE_REPORT:
                return $this->belongsTo('App\Models\Reports\Reports', 'posts_type_id');
                break;

            case ShareService::TYPE_REVIEW:
                return $this->belongsTo('App\Models\Reviews\Reviews', 'posts_type_id');
                break;

            case ShareService::TYPE_COUNTRY:
                return $this->belongsTo('App\Models\Country\Countries', 'posts_type_id');
                break;

            case ShareService::TYPE_CITY:
                return $this->belongsTo('App\Models\City\Cities', 'posts_type_id');
                break;

            case ShareService::TYPE_PLACE:
                return $this->belongsTo('App\Models\Place\Place', 'posts_type_id');
                break;

            default: // For TYPE_POST
                return $this->belongsTo('App\Models\Posts\Posts', 'posts_type_id');
                break;
        }
    }
}
