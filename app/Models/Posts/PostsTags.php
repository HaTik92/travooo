<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;

class PostsTags extends Model
{
    protected $table = 'post_tags';

    public $timestamps = true;

    const DESTINATION_COUNTRY   = "country";
    const DESTINATION_PLACE     = "place";
    const DESTINATION_CITY      = "city";
    const DESTINATION_USER      = "user";

    const DESTINATION_MAPPING = [
        self::DESTINATION_COUNTRY,
        self::DESTINATION_PLACE,
        self::DESTINATION_CITY,
        self::DESTINATION_USER
    ];

    const TYPE_POST                         = "post"; // share post + General post (text , media , check-in) +  External or internal links(others)
    const TYPE_REPORT                       = "report"; // Travlog
    const TYPE_TRIP                         = "trip";
    const TYPE_EVENT                        = "event";
    const TYPE_DISCUSSION                   = "discussion";
    const TYPE_POSTS_COMMENTS               = "post_comment"; // trip comment, report comment, post comment, event comment
    const TYPE_DISCUSSION_REPLY             = "discussion_replies";
    const TYPE_TRIP_PLACE_COMMENT           = "trips_place_comment"; // for trip place comment
    const TYPE_TRIP_PLACE_MEDIA_COMMENT     = "trips_media_comment"; // for trip place media comment
    const TYPE_TRIP_SHARE                   = "plan_shared"; // for trip place media comment
    const TYPE_TRIP_PLACE_SHARE             = "plan_step_shared"; // for trip place media comment
    const TYPE_TRIP_PLACE_MEDIA_SHARE       = "plan_media_shared"; // for trip place media comment

    public function relatedDestination()
    {
        switch ($this->destination) {
            case self::DESTINATION_COUNTRY:
                return $this->belongsTo('App\Models\Country\Countries', 'destination_id', 'id');
                break;

            case self::DESTINATION_PLACE:
                return $this->belongsTo('App\Models\Place\Place', 'destination_id', 'id');
                break;

            case self::DESTINATION_CITY:
                return $this->belongsTo('App\Models\City\Cities', 'destination_id', 'id');
                break;

            case self::DESTINATION_USER:
                return $this->belongsTo(User::class, 'destination_id', 'id');
                break;
        }
        return null;
    }
}
