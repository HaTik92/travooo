<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class PostsMedia extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts_medias';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['posts_id', 'medias_id'];
    
    public function media()
    {
        return $this->hasOne('App\Models\ActivityMedia\Media', 'id', 'medias_id');
    }

    public function post()
    {
        return $this->hasOne('App\Models\Posts\Posts', 'id', 'posts_id');
    }

    public function checkin()
    {
        return $this->belongsToMany('App\Models\Posts\Checkins', 'posts_checkins', 'posts_id', 'checkins_id', 'posts_id');
    }
}
