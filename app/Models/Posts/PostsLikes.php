<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class PostsLikes extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
}
