<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;


class CheckinsComments extends Model
{
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Posts\CheckinsCommentsLikes');
    }

}
