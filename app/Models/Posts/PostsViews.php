<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class PostsViews extends Model
{
    protected $table = 'posts_views';

    public $timestamps = true;
    
     public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
    
    public function post_view_country()
    {
        return $this->hasOne('App\Models\Country\Countries', 'id', 'nationality');
    }
}
