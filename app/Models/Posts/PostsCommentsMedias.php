<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class PostsCommentsMedias extends Model
{
    public function media()
    {
        return $this->hasOne('App\Models\ActivityMedia\Media', 'id', 'medias_id');
    }
}
