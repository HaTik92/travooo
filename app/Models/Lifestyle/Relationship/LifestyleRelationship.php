<?php

namespace App\Models\Lifestyle\Relationship;

use App\Models\System\Session;
use App\Models\Access\User\SocialLogin;
use App\Models\ActivityMedia\Media;

/**
 * Class LifestyleRelationship.
 */
trait LifestyleRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trans()
    {
        return $this->hasMany(config('lifestyle.lifestyle_trans'), 'lifestyles_id');
    }

    public function transsingle()
    {
        return $this->hasOne(config('lifestyle.lifestyle_trans'), 'lifestyles_id');
    }

    /**
     * Many-to-Many relations with CountriesMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function medias()
    {
        return $this->hasMany(config('lifestyle.lifestyle_medias'), 'lifestyles_id');
    }

    /**
     * One-to-One relations with Medias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToOne
     */
    public function cover()
    {
        return $this->hasOne( Media::class, 'id', 'cover_media_id');
    }
}
