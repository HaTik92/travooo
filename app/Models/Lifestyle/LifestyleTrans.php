<?php

namespace App\Models\Lifestyle;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lifestyle\Relationship\LifestyleTransRelationship;

class LifestyleTrans extends Model
{
    use LifestyleTransRelationship;

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conf_lifestyles_trans';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description'];
}
