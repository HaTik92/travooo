<?php

namespace App\Models\Lifestyle;

class ApiLifestyle extends Lifestyle
{
    public function getResponse(){
        
        $title = $this->title;

        return  [
            'id'                => $this->lId,
            'name'              => $title,
            'cover_image' 		=> ''
        ];
    }
}
