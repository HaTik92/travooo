<?php

namespace App\Models\Lifestyle;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lifestyle\Relationship\LifestyleRelationship;
use App\Models\Lifestyle\Traits\Attribute\LifestyleAttribute;

class Lifestyle extends Model
{
    use LifestyleAttribute,
        LifestyleRelationship;

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conf_lifestyles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
}
