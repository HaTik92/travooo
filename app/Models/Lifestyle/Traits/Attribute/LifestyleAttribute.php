<?php

namespace App\Models\Lifestyle\Traits\Attribute;

/**
 * Class LifestyleAttribute.
 */
trait LifestyleAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
