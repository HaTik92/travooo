<?php

namespace App\Models\Access\Role;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleUser.
 */
class RoleUser extends Model
{
    use RoleUserRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'role_id'];
}
