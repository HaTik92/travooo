<?php

namespace App\Models\Access\Language\Traits\Attribute;

/**
 * Class UserAttribute.
 */
trait LanguageAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
