<?php

namespace App\Models\Access\Language;

use Illuminate\Database\Eloquent\Model;

class ConfTranslationsProgress extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conf_translations_progress';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language_title',
        'language_code',
        'event',
        'total_count',
        'done_count',
        'completed_at',
        'created_at',
        'updated_at'
    ];
}
