<?php

namespace App\Models\Access\User\Traits\Relationship;

use App\Models\CopyrightInfringements\CopyrightInfringement;
use App\Models\System\Session;
use App\Models\Access\User\SocialLogin;
use App\Models\User\UsersFavourites;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('access.role'), config('access.role_user_table'), 'user_id', 'role_id');
    }

    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialLogin::class);
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    public function copyrightInfringement()
    {
        return $this->hasMany(CopyrightInfringement::class,'owner_id', 'id' )->withTrashed();
    }

    /**
     * @return mixed
     */
    public function favourites(){
        return $this->hasMany(UsersFavourites::class, 'users_id', 'id');
    }

    public function travelstyles()
    {
        return $this->hasMany('App\Models\User\UsersTravelStyles', 'users_id', 'id');
    }

    public function expertCountries()
    {
        return $this->hasMany('App\Models\ExpertsCountries\ExpertsCountries', 'users_id', 'id');
    }

    public function expertCities()
    {
        return $this->hasMany('App\Models\ExpertsCities\ExpertsCities', 'users_id', 'id');
    }

    public function expertPlaces()
    {
        return $this->hasMany('App\Models\ExpertsPlaces\ExpertsPlaces', 'users_id', 'id');
    }

    public function expertTravelStyles()
    {
        return $this->hasMany('App\Models\ExpertsTravelStyles\ExpertsTravelStyles', 'users_id', 'id');
    }

}
