<?php

namespace App\Models\TripMedias;

use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use Illuminate\Database\Eloquent\Model;

class TripMedias extends Model
{
    protected $table = 'trips_medias';

    protected $fillable = [
        'medias_id',
        'trips_id',
        'places_id',
        'trip_place_id'
    ];

    public function media()
    {
        return $this->hasOne('App\Models\ActivityMedia\Media', 'id', 'medias_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trip()
    {
        return $this->belongsTo(TripPlans::class, 'trips_id');
    }

    public function tripPlace()
    {
        return $this->belongsTo(TripPlaces::class, 'trip_place_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')->where('parents_id', '=', NULL)->where('type', 'trip-media')->orderby('created_at', 'DESC');
    }
}
