<?php

namespace App\Models\SafetyDegree\Traits\Attribute;

/**
 * Class SafetyDegreeAttribute.
 */
trait SafetyDegreeAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
