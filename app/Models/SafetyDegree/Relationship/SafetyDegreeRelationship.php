<?php

namespace App\Models\SafetyDegree\Relationship;

/**
 * Class RegionsRelationship.
 */
trait SafetyDegreeRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trans()
    {
        return $this->hasMany(config('access.safety_degree_trans'), 'safety_degrees_id');
    }

    public function transsingle()
    {
        return $this->hasOne(config('access.safety_degree_trans'), 'safety_degrees_id');
    }
}
