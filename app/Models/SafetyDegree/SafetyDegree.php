<?php

namespace App\Models\SafetyDegree;

use Illuminate\Database\Eloquent\Model;
use App\Models\SafetyDegree\Relationship\SafetyDegreeRelationship;
use App\Models\SafetyDegree\Traits\Attribute\SafetyDegreeAttribute;

class SafetyDegree extends Model
{
    use SafetyDegreeAttribute,
        SafetyDegreeRelationship;

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conf_safety_degrees';
}
