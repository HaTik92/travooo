<?php

namespace App\Models\Country;

use App\Models\Country\Traits\Relationship\CountriesDiscussionsDownvotesRelationship;
use Illuminate\Database\Eloquent\Model;

class CountriesDiscussionsDownvotes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries_discussions_downvotes';

    use CountriesDiscussionsDownvotesRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'discussions_id' , 'ipaddress', 'users_id','created_at'];
}
