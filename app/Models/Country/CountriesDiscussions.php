<?php

namespace App\Models\Country;

use App\Models\Country\Traits\Relationship\CountriesDiscussionsRelationship;
use Illuminate\Database\Eloquent\Model;

class CountriesDiscussions extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries_discussions';

    use CountriesDiscussionsRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'thread_id' , 'parent_id', 'cities_id', 'users_id', 'text', 'upvotes', 'downvotes', 'replies', 'created_at', 'updated_at'];
}
