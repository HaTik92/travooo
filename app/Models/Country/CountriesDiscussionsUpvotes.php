<?php

namespace App\Models\Country;

use App\Models\Country\Traits\Relationship\CountriesDiscussionsUpvotesRelationship;
use Illuminate\Database\Eloquent\Model;

class CountriesDiscussionsUpvotes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries_discussions_upvotes';

    use CountriesDiscussionsUpvotesRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'discussions_id' , 'ipaddress', 'users_id','created_at'];
}
