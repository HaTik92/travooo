<?php

namespace App\Models\Country;

use App\Models\Reviews\Reviews;
use Illuminate\Database\Eloquent\Model;
use App\Models\Country\Traits\Relationship\CountriesRelationship;
use App\Models\Country\Traits\Attribute\CountryAttribute;
use App\Models\ActivityMedia\Media;
use Illuminate\Support\Facades\DB;

class Countries extends Model
{
    CONST ACTIVE    = 1;
    CONST DEACTIVE  = 2;

    CONST CHINA_ID = 45;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';
    
    use CountriesRelationship,
        CountryAttribute;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['regions_id', 'code', 'iso_code', 'lat', 'lng', 'safety_degree', 'active'];

    /**
     * @return mixed
     **/
    public function deleteTrans(){
        self::deleteModels($this->trans);
    }

    /**
     * @return mixed
     **/
    public function deleteAirports(){
        self::deleteModels($this->airports);
    }

    /**
     * @return mixed
     **/
    public function deleteCurrencies(){
        self::deleteModels($this->currencies);
    }

    /**
     * @return mixed
     **/
    public function deleteCapitals(){
        self::deleteModels($this->capitals);
    }

    /**
     * @return mixed
     **/
    public function deleteEmergencyNumbers(){
        self::deleteModels($this->emergency_numbers);
    }

    /**
     * @return mixed
     **/
    public function deleteHolidays(){
        self::deleteModels($this->holidays);
    }

    /**
     * @return mixed
     **/
    public function deleteLanguagesSpoken(){
        self::deleteModels($this->languages_spoken);
    }

    /**
     * @return mixed
     **/
    public function deleteLifestyles(){
        self::deleteModels($this->lifestyles);
    }

    /**
     * @return mixed
     **/
    public function deleteMedias(){
        if(!$this->getMedias->isEmpty()){
            foreach ($this->getMedias as $value) {
                $medias[] = Media::find($value->id);
            }
            self::deleteModels($medias);
        }
    }

    /**
     * @return mixed
     **/
    public function deleteReligions(){
        self::deleteModels($this->religions);
    }

    /**
     * @return mixed
     **/
    public function deleteModels($models){
        if(!empty($models)){
            foreach ($models as $key => $value) {
                $value->delete();
            }
        }
    }

    public function abouts()
    {
        return $this->hasMany('App\Models\Country\CountriesAbouts');
    }
    
    public function getLanguages()
    {
        $countryId = $this->id;

        return DB::table('conf_languages_spoken_trans AS trans')
            ->select(
                'trans.id',
                'trans.languages_spoken_id',
                'trans.languages_id',
                'trans.title',
                'trans.description'
            )
            ->join('conf_languages_spoken', function($join)
            {
                $join->on('conf_languages_spoken.id', '=', 'trans.languages_spoken_id')
                    ->where('conf_languages_spoken.active', '=', 1);
            })
            ->join('countries_langugaes_spoken', function($join) use($countryId)
            {
                $join->on('countries_langugaes_spoken.languages_spoken_id', '=', 'trans.languages_spoken_id')
                    ->where('countries_langugaes_spoken.countries_id', '=', $countryId);
            })
            ->union(
                DB::table('conf_languages_spoken_trans AS additionalTrans')
                    ->select(
                        'additionalTrans.id',
                        'additionalTrans.languages_spoken_id',
                        'additionalTrans.languages_id',
                        'additionalTrans.title',
                        'additionalTrans.description'
                    )
                    ->join('conf_languages_spoken', function($join)
                    {
                        $join->on('conf_languages_spoken.id', '=', 'additionalTrans.languages_spoken_id')
                            ->where('conf_languages_spoken.active', '=', 1);
                    })
                    ->join('countries_additional_languages', function($join) use($countryId)
                    {
                        $join->on('countries_additional_languages.languages_spoken_id', '=', 'additionalTrans.languages_spoken_id')
                            ->where('countries_additional_languages.countries_id', '=', $countryId);
                    })
            )->get();
    }

    public function isHotel(){
        $is_hotel=false;
        if(strpos($this->place_type, "lodging") === 0) {
            $is_hotel=true;
        }
        return $is_hotel;
    }

    public function reviews()
    {
        return $this->hasMany(Reviews::class, 'countries_id');
    }
}
