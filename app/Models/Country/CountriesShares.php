<?php

namespace App\Models\Country;

use App\Models\Country\Traits\Relationship\CountriesSharesRelationship;
use Illuminate\Database\Eloquent\Model;

class CountriesShares extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries_shares';

    use CountriesSharesRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'country_id', 'user_id', 'scope', 'comment', 'created_at'];
    
    /**
     * @return mixed
     */
    public function user()
    {
        
        return $this->belongsTo('\App\Models\Access\User\User', 'user_id', 'id');
    }
}
