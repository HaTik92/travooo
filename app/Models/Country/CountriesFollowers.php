<?php

namespace App\Models\Country;

use App\Models\Country\Traits\Relationship\CountriesFollowersRelationship;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CountriesFollowers extends Model
{
    CONST ACTIVE    = 1;
    CONST DEACTIVE  = 2;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries_followers';

    use CountriesFollowersRelationship;

//    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['countries_id', 'users_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
    
    public function country()
    {
        return $this->hasOne('App\Models\Country\Countries', 'id',  'countries_id');
    }
}
