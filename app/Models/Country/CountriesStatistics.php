<?php

namespace App\Models\Country;

use App\Models\Country\Traits\Relationship\CountriesStatisticsReletionship;
use Illuminate\Database\Eloquent\Model;

class CountriesStatistics extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries_statistics';

    use CountriesStatisticsReletionship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['countries_id', 'medias', 'followers', 'trips', 'places', 'cities', 'shares'];
}
