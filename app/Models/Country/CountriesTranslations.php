<?php

namespace App\Models\Country;

use Illuminate\Database\Eloquent\Model;
use App\Models\Country\Traits\Relationship\CountriesTransRelationship;

class CountriesTranslations extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries_trans';

    public $timestamps = false;

    use CountriesTransRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'countries_id',
        'languages_id',
        'title',
        'description',
        'nationality',
        'population',
        'best_place',
        'best_time',
        'cost_of_living',
        'geo_stats',
        'demographics',
        'economy',
        'suitable_for',
        'user_rating',
        'num_cities',
        'metrics',
        'sockets',
        'working_days',
        'internet',
        'other',
        'highlights',
        'health_notes',
        'accommodation',
        'potential_dangers',
        'local_poisons',
        'speed_limit',
        'etiquette',
        'pollution_index',
        'transportation',
        'planning_tips'
    ];
}
