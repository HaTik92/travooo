<?php

namespace App\Models\Country;

use Illuminate\Database\Eloquent\Model;
use App\Models\Country\Traits\Relationship\CountriesTransRelationship;

class CountriesAbouts extends Model
{
    const TYPE_RESTRICTIONS       = 'restrictions';
    const TYPE_PLANNING_TIPS      = 'planning_tips';
    const TYPE_HEALTH_NOTES       = 'health_notes';
    const TYPE_POTENTIAL_DANGERS  = 'potential_dangers';
    const TYPE_SPEED_LIMIT        = 'speed_limit';
    const TYPE_ETIQUETTE          = 'etiquette';
    const TYPE_BEST_TIME          = 'best_time';
    const TYPE_DAILY_COSTS        = 'daily_costs';
    const TYPE_TRANSPORTATION     = 'transportation';
    const TYPE_SOCKETS_PLUGS      = 'sockets_plugs';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries_abouts';

    public $timestamps = false;

    use CountriesTransRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'countries_id',
        'type',
        'title',
        'body'
    ];
}
