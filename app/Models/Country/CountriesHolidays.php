<?php

namespace App\Models\Country;

use Illuminate\Database\Eloquent\Model;
use App\Models\Country\Traits\Relationship\CountriesHolidaysRelationship;

class CountriesHolidays extends Model
{
    const ACTIVE    = 1;
    const DEACTIVE  = 2;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries_holidays';

    use CountriesHolidaysRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['countries_id', 'holidays_id', 'date'];

    public function country()
    {
        return $this->belongsTo('App\Models\Country\Countries', 'countries_id');
    }
}
