<?php

namespace App\Models\Country;

/* Api Country Model */
use Mews\Purifier\Facades\Purifier;

class ApiCountry extends Countries
{
    public function getArrayResponse(){

        /* Container For Country Translations */
        $country_translations = [];

        /* If Country Translations Exist, Get Translation Information In Array Format */
        if(!empty($this->trans)){
            foreach ($this->trans as $key => $value) {
                $country_translations[$value->languages_id] = [
                    'title'         => $value->title,
                    'description'   => Purifier::clean($value->description),
                    'nationality'   => $value->nationality,
                    'population'    => $value->population,
                    'best_place'    => $value->best_place,
                    'best_time'     => $value->best_time,
                    'cost_of_living'=> $value->cost_of_living,
                    'geo_stats'     => $value->geo_stats,
                    'demographics'  => $value->demographics,
                    'economy'       => $value->economy,
                    'suitable_for'  => $value->suitable_for,
                    'user_rating'   => $value->user_rating,
                    'num_cities'    => $value->num_cities
                ];
            }
        }

        /* Container For Country Media Information */
        $country_media_arr = [];

        /* If Country Media Exists, Get Media Information In Array Format */
        if(!empty($this->medias)){
            foreach ($this->medias as $key => $value) {
                array_push($country_media_arr,$value->media->getArrayResponse());
            }
        }

        /* Return Array Response */
        return [
            'id'               => $this->id,
            'regions_id'       => $this->id,
            'code'             => $this->code,
            'lat'              => $this->lat,
            'lng'              => $this->lng,
            'safety_degree_id' => $this->safety_degree_id,
            'active'           => $this->active,
            'translations'     => $country_translations,
            'country_medias'   => $country_media_arr
        ];
    }

    /* Generate Error Message With provided "status", "code" and "message" */
    /**
     * @param $status
     * @param $code
     * @param $message
     * @return array
     */
    public static function generateErrorMessage($status, $code, $message){

        $response = [];
        /* If Code == null */
        if( $code ){
            $response = [
                'data' => [
                    'error'     => $code,
                    'message'   => $message,
                ],
                'status'    => $status
            ];
        }else{
            $response = [
                'data' => [
                    'message'   => $message,
                ],
                'status'    => $status
            ];
        }

        return $response;
    }

    /**
     * @return array
     */
    public function getResponse(){

        $title = '';
        $cover_image = '';

        if(!empty($this->title)){
            $title = $this->title;
        }

        if(!empty($this->cover)){
            $cover_image = $this->cover->url;
        }

        return [
            'id'            => $this->cId,
            'active'        => $this->active,
            'lat'           => $this->lat,
            'lng'           => $this->lng,
            'code'          => $this->code,
            'cover_image'   => $cover_image,
            'name'          => $title 
        ];
    }
}
