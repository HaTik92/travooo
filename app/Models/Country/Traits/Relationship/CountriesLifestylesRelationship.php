<?php

namespace App\Models\Country\Traits\Relationship;

use App\Models\Lifestyle\Lifestyle;

/**
 * Class UserRelationship.
 */
trait CountriesLifestylesRelationship
{
    public function lifestyle()
    {
        return $this->hasOne(Lifestyle::class , 'id' , 'lifestyles_id');
    }

}
