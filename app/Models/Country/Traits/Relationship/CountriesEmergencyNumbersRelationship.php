<?php

namespace App\Models\Country\Traits\Relationship;

use App\Models\EmergencyNumbers\EmergencyNumbers;

/**
 * Class CountriesEmergencyNumbersRelationship.
 */
trait CountriesEmergencyNumbersRelationship
{
    public function emergency_number()
    {
        return $this->hasOne(EmergencyNumbers::class , 'id' , 'emergency_numbers_id');
    }

}
