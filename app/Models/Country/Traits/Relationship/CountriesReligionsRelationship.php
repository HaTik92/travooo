<?php

namespace App\Models\Country\Traits\Relationship;

use App\Models\Religion\Religion;

/**
 * Class CountriesReligionsRelationship.
 */
trait CountriesReligionsRelationship
{
    public function religion()
    {
        return $this->hasOne(Religion::class , 'id' , 'religions_id');
    }

}
