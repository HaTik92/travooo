<?php

namespace App\Models\Country\Traits\Relationship;

use App\Models\City\Cities;
use App\Models\Country\CountriesDiscussions;
use App\Models\Country\CountriesFollowers;
use App\Models\Country\CountriesShares;
use App\Models\Country\CountriesStatistics;
use App\Models\Country\CountriesTimezones;
use App\Models\Currencies\Currencies;
use App\Models\EmergencyNumbers\EmergencyNumbers;
use App\Models\Holidays\Holidays;
use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\Place\Medias;
use App\Models\Place\Place;
use App\Models\Religion\Religion;
use App\Models\System\Session;
use App\Models\Access\User\SocialLogin;
use App\Models\Regions\Regions;
use App\Models\SafetyDegree\SafetyDegree;
use App\Models\ActivityMedia\Media;
use App\Models\Hotels\Hotels;
use App\Models\Discussion\Discussion;
use App\Models\Country\CountriesHolidays;

use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\Reports\Reports;

/**
 * Class UserRelationship.
 */
trait CountriesRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('access.role'), config('access.role_user_table'), 'user_id', 'role_id');
    }

    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialLogin::class);
    }

    /**
     * Many-to-Many relations with CountriesTrans.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trans()
    {
        return $this->hasMany(config('locations.country_trans'), 'countries_id');
    }

    public function transsingle()
    {
        return $this->hasOne(config('locations.country_trans'), 'countries_id');
    }

    /**
     * @return mixed
     */
    public function region()
    {
        return $this->hasOne(Regions::class , 'id' , 'regions_id');
    }

    /**
     * @return mixed
     */
    public function degree()
    {
        return $this->hasOne(SafetyDegree::class , 'id' , 'safety_degree_id');
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    /**
     * Many-to-Many relations with CountriesAirports.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function airports()
    {
        return $this->hasMany(config('locations.country_airports_trans'), 'countries_id');
    }

    /**
     * Many-to-Many relations with CountriesCurrencies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function currencies()
    {
        return $this->belongsToMany(
            Currencies::class,
            'countries_currencies',
            'countries_id',
            'currencies_id'
        );
    }

    /**
     * Many-to-Many relations with CountriesCapitals.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function capitals()
    {
        return $this->hasMany(config('locations.country_capitals_trans'), 'countries_id');
    }

    /**
     * Many-to-Many relations with CountriesEmergencyNumbers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function emergency()
    {
        return $this->belongsToMany(
            EmergencyNumbers::class,
            'countries_emergency_numbers',
            'countries_id',
            'emergency_numbers_id'
        );
    }

    /**
     * Many-to-Many relations with CountriesHolidays.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function holidays()
    {
        return $this->belongsToMany(
            Holidays::class,
            'countries_holidays',
            'countries_id',
            'holidays_id'
        );
    }

    /**
     * Many-to-Many relations with CountriesHolidays.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function countryHolidays() {
        return $this->hasMany(CountriesHolidays::class, 'countries_id');
    }

    /**
     * Many-to-Many relations with CountriesLanguagesSpoken.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function languages()
    {
        return $this->belongsToMany(
            LanguagesSpoken::class,
            'countries_langugaes_spoken',
            'countries_id',
            'languages_spoken_id'
        );
    }

    /**
     * Many-to-Many relations with CountriesAdditionalLanguagesSpoken.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function additional_languages_spoken()
    {
        return $this->hasMany(config('locations.country_additional_languages_spoken'), 'countries_id');
    }

    public function additional_languages()
    {
        return $this->belongsToMany(
            LanguagesSpoken::class,
            'countries_additional_languages',
            'countries_id',
            'languages_spoken_id'
        );
    }

    /**
     * Many-to-Many relations with CountriesLifestyles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function lifestyles()
    {
        return $this->hasMany(config('locations.country_lifestyles_trans'), 'countries_id');
    }

    /**
     * Many-to-Many relations with CountriesMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function medias()
    {
        return $this->hasMany(config('locations.country_medias_trans'), 'countries_id');
    }

    /**
     * Many-to-Many relations with CountriesReligions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function religions()
    {
        return $this->belongsToMany(
            Religion::class,
            'countries_religions',
            'countries_id',
            'religions_id'
        );
    }

    /**
     * One-to-One relations with Medias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cover()
    {
        return $this->hasOne( Media::class, 'id', 'cover_media_id');
    }

    /**
     * One-to-Many relations with Followers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->hasMany(CountriesFollowers::class, 'countries_id', 'id');
    }

    /**
     * One-to-Many relations with Places.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function places()
    {
        return $this->hasMany(Place::class, 'countries_id', 'id');
    }

    /**
     * One-to-Many relations with Discussion.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discussionsingle()
    {
        return $this->hasMany(Discussion::class, 'destination_id', 'id');
    }

    /**
     * One-to-Many relations with Places.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function atms()
    {
        return $this->hasMany(Place::class, 'countries_id', 'id');
    }

    /**
     * One-to-Many relations with Places.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function hotels()
    {
        return $this->hasMany(Hotels::class, 'countries_id', 'id');
    }

    /**
     * One-to-Many relations with Cities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cities()
    {
        return $this->hasMany(Cities::class, 'countries_id', 'id');
    }

    /**
     * One-to-One relations with Statistics.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function statistics()
    {
        return $this->hasOne(CountriesStatistics::class, 'countries_id');
    }

    /**
     * Many-to-Many relations with CountriesMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getMedias()
    {
        return $this->belongsToMany(Medias::class, 'countries_medias', 'countries_id', 'medias_id');
    }

    public function country_medias()
    {
        return $this->getMedias();
    }


    /**
     * One-to-Many relations with Discussions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discussions()
    {
        return $this->hasMany(CountriesDiscussions::class, 'countries_id', 'id');
    }

    public function versions()
    {
        return $this->hasMany('App\Models\TripCountries\TripCountries', 'countries_id');
    }

    /**
     * @return mixed
     */
    public function shares()
    {
        return $this->hasMany(CountriesShares::class, 'country_id', 'id');
    }


    /**
     * @return mixed
     */
    public function timezone()
    {
        return $this->hasOne(CountriesTimezones::class, 'countries_id');
    }

    public function timezones()
    {
        return $this->hasMany(CountriesTimezones::class, 'countries_id');
    }

    public function experts()
    {
        return $this->hasMany(ExpertsCountries::class, 'countries_id', 'id');
    }

    public function reports()
    {
        return $this->hasMany(Reports::class, 'countries_id', 'id');
    }
    public function checkins()
    {
        return $this->hasMany('App\Models\Posts\Checkins', 'country_id');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\Posts\Posts', 'posts_countries', 'countries_id');
    }

    public function events()
    {
        return $this->hasMany('App\Models\Events\Events', 'countries_id');
    }
}
