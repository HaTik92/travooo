<?php

namespace App\Models\Country\Traits\Relationship;

use App\Models\Place\Place;

/**
 * Class UserRelationship.
 */
trait CountriesAirportsRelationship
{
    public function place()
    {
        return $this->hasOne(Place::class , 'id' , 'places_id');
    }

}
