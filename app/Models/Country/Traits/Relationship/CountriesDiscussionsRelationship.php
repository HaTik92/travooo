<?php

namespace App\Models\Country\Traits\Relationship;
use App\Models\Country\CountriesDiscussionsDownvotes;
use App\Models\Country\CountriesDiscussionsUpvotes;
use App\Models\User\User;

/**
 * Class CountriesDiscussionsRelationship
 */
trait CountriesDiscussionsRelationship
{
    /**
     * One-to-Many relations with UpVotes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function upVotes()
    {
        return $this->hasMany(CountriesDiscussionsUpvotes::class, 'discussions_id', 'id');
    }

    /**
     * One-to-Many relations with UpVotes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function downVotes()
    {
        return $this->hasMany(CountriesDiscussionsDownvotes::class, 'discussions_id', 'id');
    }

    /**
     * Many-to-One relations with Users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}
