<?php

namespace App\Models\Country\Traits\Relationship;

use App\Models\Currencies\Currencies;

/**
 * Class UserRelationship.
 */
trait CountriesCurrenciesRelationship
{
    public function currency()
    {
        return $this->hasOne(Currencies::class , 'id' , 'currencies_id');
    }

}
