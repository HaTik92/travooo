<?php

namespace App\Models\Country\Traits\Relationship;

use App\Models\ActivityMedia\Media;

/**
 * Class CountriesMediasRelationship.
 */
trait CountriesMediasRelationship
{
    public function media()
    {
        return $this->hasOne(Media::class , 'id' , 'medias_id');
    }

}
