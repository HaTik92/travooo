<?php

namespace App\Models\Country\Traits\Attribute;

/**
 * Class CountryAttribute.
 */
trait CountryAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
