<?php

namespace App\Models\Country;

use Illuminate\Database\Eloquent\Model;

class CountriesTimezones extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries_timezones';


    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'countries_id', 'time_zone_id', 'time_zone_name', 'dst_offset', 'raw_offset'];
}
