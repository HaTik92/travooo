<?php

namespace App\Models\City;

use App\Models\ActivityMedia\Media;
use App\Models\Country\CountriesTranslations;
use App\Models\Reviews\Reviews;
use App\Models\TopCities\TopCities;
use Illuminate\Database\Eloquent\Model;
use App\Models\City\Traits\Relationship\CityRelationship;
use App\Models\City\Traits\Attribute\CityAttribute;
use Illuminate\Support\Facades\DB;
use Mews\Purifier\Facades\Purifier;
use App\Models\States\StateTranslation;

class Cities extends Model
{
    CONST ACTIVE    = 1;
    CONST DEACTIVE  = 2;

    CONST IS_CAPITAL     = 1;
    CONST IS_NOT_CAPITAL = 2;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities';
    
    use CityRelationship,
        CityAttribute;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['countries_id', 'code', 'is_capital' , 'lat', 'lng', 'safety_degree_id', 'active'];

    /**
     * @return mixed
     **/
    public function deleteAirports(){
        self::deleteModels($this->airports);
    }

    /**
     * @return mixed
     **/
    public function deleteCurrencies(){
        self::deleteModels($this->currencies);
    }

    /**
 * @return mixed
 **/
    public function deleteTrans(){
        self::deleteModels($this->trans);
    }

    /**
     * @return mixed
     **/
    public function deleteEmergency_numbers(){
        self::deleteModels($this->emergency_numbers);
    }

    /**
     * @return mixed
     **/
    public function deleteHolidays(){
        self::deleteModels($this->holidays);
    }

    /**
     * @return mixed
     **/
    public function deleteLanguagesSpoken(){
        self::deleteModels($this->languages_spoken);
    }

    /**
     * @return mixed
     **/
    public function deleteLifestyles(){
        self::deleteModels($this->lifestyles);
    }

    /**
     * @return mixed
     **/
    public function deleteMedias(){
        if(!$this->getMedias->isEmpty()){
            foreach ($this->getMedias as $value) {
                $medias[] = Media::find($value->id);
            }
            self::deleteModels($medias);
        }
    }

    /**
     * @return mixed
     **/
    public function deleteReligions(){
        self::deleteModels($this->religions);
    }   

    /**
 * @return mixed
 **/
    public function deleteModels($models){
        if(!empty($models)){
            foreach ($models as $key => $value) {
                $value->delete();
            }
        }
    }
    
    
    public function trans()
    {
        return $this->hasMany('App\Models\City\CitiesTranslations');

    }
    public function abouts()
    {
        return $this->hasMany('App\Models\City\CitiesAbouts');

    }
    public function top()
    {
        return $this->hasOne(TopCities::class, 'cities_id', 'id');

    }
    
    public function trips()
    {
        return $this->belongsToMany('App\Models\TripPlans\TripPlans', 'trips_cities', 'cities_id', 'trips_id');
    }

    public function countryTitle()
    {
        return $this->hasOne(CountriesTranslations::class, 'countries_id', 'countries_id');
    }

    public function getCitiesOrCountryLanguages()
    {
        $cityId    = $this->id;
        $countryId = $this->countries_id;

        return DB::table('conf_languages_spoken_trans AS trans')
            ->select('trans.id',
                'trans.languages_spoken_id',
                'trans.languages_id',
                'trans.title',
                'trans.description',
                DB::raw("'city' as type")
            )
            ->join('conf_languages_spoken', function($join)
            {
                $join->on('conf_languages_spoken.id', '=', 'trans.languages_spoken_id')
                    ->where('conf_languages_spoken.active', '=', 1);
            })
            ->join('cities_languages_spoken', function($join) use($cityId)
            {
                $join->on('cities_languages_spoken.languages_spoken_id', '=', 'trans.languages_spoken_id')
                    ->where('cities_languages_spoken.cities_id', '=', $cityId);
            })
            ->union(
                DB::table('conf_languages_spoken_trans AS addTransCity')
                    ->select('addTransCity.id',
                        'addTransCity.languages_spoken_id',
                        'addTransCity.languages_id',
                        'addTransCity.title',
                        'addTransCity.description',
                        DB::raw("'city' as type")
                    )
                    ->join('conf_languages_spoken', function($join)
                    {
                        $join->on('conf_languages_spoken.id', '=', 'addTransCity.languages_spoken_id')
                            ->where('conf_languages_spoken.active', '=', 1);
                    })
                    ->join('cities_additional_languages', function($join) use($cityId)
                    {
                        $join->on('cities_additional_languages.languages_spoken_id', '=', 'addTransCity.languages_spoken_id')
                            ->where('cities_additional_languages.cities_id', '=', $cityId);
                    })
            )
            ->union(
                DB::table('conf_languages_spoken_trans AS addTransCountry')
                    ->select('addTransCountry.id',
                        'addTransCountry.languages_spoken_id',
                        'addTransCountry.languages_id',
                        'addTransCountry.title',
                        'addTransCountry.description',
                        DB::raw("'country' as type")
                    )
                    ->join('conf_languages_spoken', function($join)
                    {
                        $join->on('conf_languages_spoken.id', '=', 'addTransCountry.languages_spoken_id')
                            ->where('conf_languages_spoken.active', '=', 1);
                    })
                    ->join('countries_langugaes_spoken', function($join) use($countryId)
                    {
                        $join->on('countries_langugaes_spoken.languages_spoken_id', '=', 'addTransCountry.languages_spoken_id')
                            ->where('countries_langugaes_spoken.countries_id', '=', $countryId);
                    })
            )
            ->union(
                DB::table('conf_languages_spoken_trans AS addTransCountryAdditional')
                    ->select('addTransCountryAdditional.id',
                        'addTransCountryAdditional.languages_spoken_id',
                        'addTransCountryAdditional.languages_id',
                        'addTransCountryAdditional.title',
                        'addTransCountryAdditional.description',
                        DB::raw("'country' as type")
                    )
                    ->join('conf_languages_spoken', function($join)
                    {
                        $join->on('conf_languages_spoken.id', '=', 'addTransCountryAdditional.languages_spoken_id')
                            ->where('conf_languages_spoken.active', '=', 1);
                    })
                    ->join('countries_additional_languages', function($join) use($countryId)
                    {
                        $join->on('countries_additional_languages.languages_spoken_id', '=', 'addTransCountryAdditional.languages_spoken_id')
                            ->where('countries_additional_languages.countries_id', '=', $countryId);
                    })
            )
            ->get();
    }

    public function getArrayResponse(){

        /* Container For Country Translations */
        $city_translations = [];

        /* If Country Translations Exist, Get Translation Information In Array Format */
        if(!empty($this->trans)){
            foreach ($this->trans as $key => $value) {
                $city_translations[$value->languages_id] = [
                    'title'         => $value->title,
                    'description'   => Purifier::clean($value->description),
                    'nationality'   => $value->nationality,
                    'population'    => $value->population,
                    'best_place'    => $value->best_place,
                    'best_time'     => $value->best_time,
                    'cost_of_living'=> $value->cost_of_living,
                    'geo_stats'     => $value->geo_stats,
                    'demographics'  => $value->demographics,
                    'economy'       => $value->economy,
                    'suitable_for'  => $value->suitable_for,
                    'user_rating'   => $value->user_rating,
                    'num_cities'    => $value->num_cities
                ];
            }
        }

        /* Container For Country Media Information */
        $city_media_arr = [];

        /* If Country Media Exists, Get Media Information In Array Format */
        if(!empty($this->medias)){
            foreach ($this->medias as $key => $value) {
                array_push($city_media_arr,$value->medias->getArrayResponse());
            }
        }

        /* Return Array Response */
        return [
            'id'               => $this->id,
            'regions_id'       => $this->id,
            'code'             => $this->code,
            'lat'              => $this->lat,
            'lng'              => $this->lng,
            'safety_degree_id' => $this->safety_degree_id,
            'active'           => $this->active,
            'translations'     => $city_translations,
            'city_medias'      => $city_media_arr
        ];
    }
    
    public function getLanguages()
    {
        $cityId = $this->id;

        return DB::table('conf_languages_spoken_trans AS trans')
            ->select(
                'trans.id',
                'trans.languages_spoken_id',
                'trans.languages_id',
                'trans.title',
                'trans.description'
            )
            ->join('conf_languages_spoken', function($join)
            {
                $join->on('conf_languages_spoken.id', '=', 'trans.languages_spoken_id')
                    ->where('conf_languages_spoken.active', '=', 1);
            })
            ->join('cities_languages_spoken', function($join) use($cityId)
            {
                $join->on('cities_languages_spoken.languages_spoken_id', '=', 'trans.languages_spoken_id')
                    ->where('cities_languages_spoken.cities_id', '=', $cityId);
            })
            ->union(
                DB::table('conf_languages_spoken_trans AS additionalTrans')
                    ->select(
                        'additionalTrans.id',
                        'additionalTrans.languages_spoken_id',
                        'additionalTrans.languages_id',
                        'additionalTrans.title',
                        'additionalTrans.description'
                    )
                    ->join('conf_languages_spoken', function($join)
                    {
                        $join->on('conf_languages_spoken.id', '=', 'additionalTrans.languages_spoken_id')
                            ->where('conf_languages_spoken.active', '=', 1);
                    })
                    ->join('cities_additional_languages', function($join) use($cityId)
                    {
                        $join->on('cities_additional_languages.languages_spoken_id', '=', 'additionalTrans.languages_spoken_id')
                            ->where('cities_additional_languages.cities_id', '=', $cityId);
                    })
            )->get();
    }
    public function isHotel(){
        $is_hotel=false;
        if(strpos($this->place_type, "lodging") === 0) {
            $is_hotel=true;
        }
        return $is_hotel;
    }
    public function stateTitle()
    {
        return $this->hasMany(StateTranslation::class, 'states_id', 'states_id');
    }

    public function reviews()
    {
        return $this->hasMany(Reviews::class, 'cities_id');
    }
}
