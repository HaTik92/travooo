<?php

namespace App\Models\City;

use Illuminate\Database\Eloquent\Model;
use App\Models\City\Traits\Relationship\CityTransRelationship;

class CitiesTranslations extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities_trans';

    public $timestamps = false;

    use CityTransRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cities_id',
        'languages_id',
        'title',
        'description',
        'best_place',
        'best_time',
        'cost_of_living',
        'geo_stats',
        'demographics',
        'economy',
        'suitable_for',
        'metrics',
        'health_notes',
        'accommodation',
        'potential_dangers',
        'local_poisons',
        'sockets',
        'working_days',
        'restrictions',
        'planning_tips',
        'other',
        'internet',
        'speed_limit',
        'etiquette',
        'pollution_index',
        'transportation',
        'highlights',
        'population'
    ];
}
