<?php

namespace App\Models\City\Traits\Relationship;
use App\Models\City\CitiesDiscussionsDownvotes;
use App\Models\City\CitiesDiscussionsUpvotes;
use App\Models\User\User;

/**
 * Class CityDiscussionsRelationship
 */
trait CityDiscussionsRelationship
{
    /**
     * One-to-Many relations with UpVotes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function upVotes()
    {
        return $this->hasMany(CitiesDiscussionsUpvotes::class, 'discussions_id', 'id');
    }

    /**
     * One-to-Many relations with UpVotes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function downVotes()
    {
        return $this->hasMany(CitiesDiscussionsDownvotes::class, 'discussions_id', 'id');
    }

    /**
     * Many-to-One relations with Users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}
