<?php

namespace App\Models\City\Traits\Relationship;

use App\Models\City\CitiesDiscussions;
use App\Models\City\CitiesFollowers;
use App\Models\City\CitiesHolidays;
use App\Models\City\CitiesStatistics;
use App\Models\City\CitiesTimezones;
use App\Models\City\CitiesWeather;
use App\Models\Currencies\Currencies;
use App\Models\EmergencyNumbers\EmergencyNumbers;
use App\Models\Holidays\Holidays;
use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\Place\Medias;
use App\Models\Place\Place;
use App\Models\Religion\Religion;
use App\Models\System\Session;
use App\Models\Access\User\SocialLogin;
use App\Models\Country\Countries;
use App\Models\SafetyDegree\SafetyDegree;
use App\Models\ActivityMedia\Media;
use App\Models\Hotels\Hotels;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\Discussion\Discussion;
use Illuminate\Support\Facades\Auth;

use App\Models\Reports\Reports;

/**
 * Class CityRelationship.
 */
trait CityRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('access.role'), config('access.role_user_table'), 'user_id', 'role_id');
    }

    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialLogin::class);
    }

    /**
     * @return mixed
     */
    public function country()
    {
        return $this->hasOne(Countries::class , 'id' , 'countries_id');
    }
    
    /**
     * One-to-Many relations with Discussion.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discussionsingle()
    {
        return $this->hasMany(Discussion::class, 'destination_id', 'id');
    }

    /**
     * Many-to-Many relations with CitiesTrans.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trans()
    {
        // return $this->belongsToMany(config('access.regions'), config('access.regions_trans'), 'id', 'regions_id');
        return $this->hasMany(config('locations.cities_trans'), 'cities_id');
    }

    public function transsingle()
    {

        // return $this->belongsToMany(config('access.regions'), config('access.regions_trans'), 'id', 'regions_id');
        return $this->hasOne(config('locations.cities_trans'), 'cities_id');
    }

    /**
     * Many-to-Many relations with CitiesAirports.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function airports()
    {
        // return $this->belongsToMany(config('access.regions'), config('access.regions_trans'), 'id', 'regions_id');
        return $this->hasMany(config('locations.cities_airports_trans'), 'cities_id');
    }

    /**
     * Many-to-Many relations with CitiesCurrencies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function currencies()
    {
        return $this->belongsToMany(
            Currencies::class,
            'cities_currencies',
            'cities_id',
            'currencies_id'
        );
    }

    /**
     * Many-to-Many relations with CitiesNumbers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function emergency()
    {
        return $this->belongsToMany(
            EmergencyNumbers::class,
            'cities_emergency_numbers',
            'cities_id',
            'emergency_numbers_id'
        );
    }

    /**
     * Many-to-Many relations with CitiesNumbers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function holidays()
    {
        return $this->belongsToMany(
            Holidays::class,
            'cities_holidays',
            'cities_id',
            'holidays_id'
        )->withPivot('date');
    }

    public function cityHolidays() {
        return $this->hasMany(CitiesHolidays::class, 'cities_id');
    }


    /**
     * Many-to-Many relations with CitiesLifestyles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function lifestyles()
    {
        // return $this->belongsToMany(config('access.regions'), config('access.regions_trans'), 'id', 'regions_id');
        return $this->hasMany(config('locations.cities_lifestyles_trans'), 'cities_id');
    }

    /**
     * Many-to-Many relations with CitiesNumbers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function languages_spoken()
    {
        return $this->belongsToMany(
            LanguagesSpoken::class,
            'cities_languages_spoken',
            'cities_id',
            'languages_spoken_id'
        );
    }

    public function additional_languages()
    {
        return $this->belongsToMany(
            LanguagesSpoken::class,
            'cities_additional_languages',
            'cities_id',
            'languages_spoken_id'
        );
    }

    /**
     * Many-to-Many relations with CitiesAdditionalLanguages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function additional_languages_spoken()
    {
        // return $this->belongsToMany(config('access.regions'), config('access.regions_trans'), 'id', 'regions_id');
        return $this->hasMany(config('locations.cities_additional_languages_spoken'), 'cities_id');
    }

    /**
     * Many-to-Many relations with CitiesMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function medias()
    {
        // return $this->belongsToMany(config('access.regions'), config('access.regions_trans'), 'id', 'regions_id');
        return $this->hasMany(config('locations.cities_medias_trans'), 'cities_id');
    }

    public function first_media()
    {
        return $this->hasOne(config('locations.cities_medias_trans'), 'cities_id');
    }

    /**
     * Many-to-Many relations with CitiesReligions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function religions()
    {
        return $this->belongsToMany(
            Religion::class,
            'cities_religions',
            'cities_id',
            'religions_id'
        );
    }

    /**
     * @return mixed
     */
    public function degree()
    {
        return $this->hasOne(SafetyDegree::class , 'id' , 'safety_degree_id');
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    /**
     * One-to-One relations with Medias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToOne
     */
    public function cover()
    {
        // return $this->belongsToMany(config('access.regions'), config('access.regions_trans'), 'id', 'regions_id');
        return $this->hasOne( Media::class, 'id', 'cover_media_id');
    }

    /**
     * One-to-Many relations with Followers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->hasMany(CitiesFollowers::class, 'cities_id', 'id');
    }

    /**
     * One-to-Many relations with Places.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function places()
    {
        return $this->hasMany(Place::class, 'cities_id', 'id');
    }
    
    /**
     * One-to-Many relations with Places.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function atms()
    {
        return $this->hasMany(Place::class, 'cities_id', 'id');
    }
    
    /**
     * One-to-Many relations with Places.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function hotels()
    {
        return $this->hasMany(Hotels::class, 'cities_id', 'id');
    }

    /**
     * One-to-One relations with Weather.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function weather()
    {
        return $this->hasOne(CitiesWeather::class, 'cities_id', 'id');
    }

    /**
     * One-to-Many relations with Discussions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discussions()
    {
        return $this->hasMany(CitiesDiscussions::class, 'cities_id', 'id');
    }

    /**
     * One-to-One relations with Statistics.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function statistics()
    {
        return $this->hasOne(CitiesStatistics::class, 'cities_id');
    }

    /**
     * Many-to-Many relations with CitiesMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getMedias()
    {
         return $this->belongsToMany(Media::class, 'cities_medias', 'cities_id', 'medias_id');
    }

    public function city_medias()
    {
        return $this->getMedias();
    }

    public function first_city_medias()
    {
        return $this->getMedias()->take(1);
    }

    /**
     * @return mixed
     */
    public function timezone()
    {
        return $this->hasOne(CitiesTimezones::class, 'cities_id');
    }
    
    public function versions()
    {
        return $this->hasMany('App\Models\TripCities\TripCities', 'countries_id');
    }
    
    public function experts()
    {
        return $this->hasMany(ExpertsCities::class, 'cities_id', 'id');
    }
    
    public function reports()
    {
        return $this->hasMany(Reports::class, 'cities_id', 'id');
    }
    public function checkins()
    {
        return $this->hasMany('App\Models\Posts\Checkins', 'city_id');
    }
    
    public function posts()
    {
        return $this->belongsToMany('App\Models\Posts\Posts', 'posts_cities', 'cities_id');
    }

    public function events()
    {
        return $this->hasMany('App\Models\Events\Events', 'cities_id');
    }
    
}
