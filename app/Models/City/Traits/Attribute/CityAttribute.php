<?php

namespace App\Models\City\Traits\Attribute;

/**
 * Class CountryAttribute.
 */
trait CityAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
