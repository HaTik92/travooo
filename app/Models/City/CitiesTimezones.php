<?php

namespace App\Models\City;

use Illuminate\Database\Eloquent\Model;

class CitiesTimezones extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities_timezones';


    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'cities_id', 'time_zone_id', 'time_zone_name', 'dst_offset', 'raw_offset'];
}
