<?php

namespace App\Models\City;

use Illuminate\Database\Eloquent\Model;

class CitiesWeatherTranslations extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities_weather_trans';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =
        [
            'id',
            'weather_id',
            'language_id',
            'daily_forecast',
            'upcoming_forecast'
        ];
}
