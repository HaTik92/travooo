<?php

namespace App\Models\City;

use Illuminate\Database\Eloquent\Model;

class CitiesWeather extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'cities_weather';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'cities_id', 'schedule'];

    public function trans()
    {
        return $this->hasMany(CitiesWeatherTranslations::class, 'weather_id', 'id');
    }
}
