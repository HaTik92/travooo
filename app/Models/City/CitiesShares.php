<?php

namespace App\Models\City;

use App\Models\City\Traits\Relationship\CitiesSharesRelationship;
use Illuminate\Database\Eloquent\Model;

class CitiesShares extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities_shares';

    use CitiesSharesRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'city_id', 'users_id', 'scope', 'comment', 'created_at'];
}
