<?php

namespace App\Models\City;

use App\Models\City\Traits\Relationship\CityStatisticsReletionship;
use Illuminate\Database\Eloquent\Model;

class CitiesStatistics extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities_statistics';

    use CityStatisticsReletionship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cities_id', 'medias', 'followers', 'trips', 'places'];
}
