<?php

namespace App\Models\City;

use App\Models\City\Traits\Relationship\CitiesFollowersRelationship;
use Illuminate\Database\Eloquent\Model;

class CitiesFollowers extends Model
{
    CONST ACTIVE    = 1;
    CONST DEACTIVE  = 2;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities_followers';

    use CitiesFollowersRelationship;

//    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cities_id', 'users_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
    
    public function city()
    {
         return $this->hasOne('App\Models\City\Cities', 'id',  'cities_id');
    }
}
