<?php

namespace App\Models\City;

use App\Models\City\Traits\Relationship\CityDiscussionsUpvotesRelationship;
use Illuminate\Database\Eloquent\Model;

class CitiesDiscussionsUpvotes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities_discussions_upvotes';

    use CityDiscussionsUpvotesRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'discussions_id' , 'ipaddress', 'users_id','created_at'];
}
