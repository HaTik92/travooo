<?php

namespace App\Models\City;

use App\Models\City\Traits\Relationship\CityDiscussionsRelationship;
use Illuminate\Database\Eloquent\Model;

class CitiesDiscussions extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities_discussions';

    use CityDiscussionsRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'thread_id' , 'parent_id', 'cities_id', 'users_id', 'text', 'upvotes', 'downvotes', 'replies', 'created_at', 'updated_at'];
}
