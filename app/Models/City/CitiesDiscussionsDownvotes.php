<?php

namespace App\Models\City;

use App\Models\City\Traits\Relationship\CityDiscussionsDownvotesRelationship;
use Illuminate\Database\Eloquent\Model;

class CitiesDiscussionsDownvotes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities_discussions_downvotes';

    use CityDiscussionsDownvotesRelationship;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'discussions_id' , 'ipaddress', 'users_id','created_at'];
}
