<?php

namespace App\Models\Religion\Traits\Attribute;

/**
 * Class ReligionAttribute.
 */
trait ReligionAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
