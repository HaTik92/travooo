<?php

namespace App\Models\Travelstyles\Traits\Attribute;

/**
 * Class TimingsAttribute.
 */
trait TravelstylesAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
