<?php

namespace App\Models\Travelstyles\Traits\Relationship;

use App\Models\System\Session;
use App\Models\Access\User\SocialLogin;

/**
 * Class TimingsRelationship.
 */
trait TravelstylesRelationship
{
    public function trans()
    {
        return $this->hasMany('\App\Models\Travelstyles\TravelstylesTranslations', 'lifestyles_id' , 'id');
    }

    public function transsingle()
    {
        return $this->hasOne('\App\Models\Travelstyles\TravelstylesTranslations', 'lifestyles_id' , 'id');
    }
}
