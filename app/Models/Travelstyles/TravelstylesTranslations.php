<?php

namespace App\Models\Travelstyles;

use Illuminate\Database\Eloquent\Model;
use App\Models\Travelstyles\Traits\Relationship\TravelstylesTransRelationship;

class TravelstylesTranslations extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conf_lifestyles_trans';

    public $timestamps = false;

    use TravelstylesTransRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['lifestyles_id', 'languages_id', 'title' , 'description'];
}
