<?php

namespace App\Models\Travelstyles;

use App\Models\Place\Medias;
use Illuminate\Database\Eloquent\Model;
use App\Models\Travelstyles\Traits\Relationship\TravelstylesRelationship;
use App\Models\Travelstyles\Traits\Attribute\TravelstylesAttribute;

class Travelstyles extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conf_lifestyles';

    use TravelstylesRelationship,
        TravelstylesAttribute;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id'];

    public function trans()
    {

        return $this->hasMany('App\Models\Travelstyles\TravelstylesTranslations', 'lifestyles_id');
    }

    /**
     * Many-to-Many relations with PlacesMedias.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getMedias()
    {
        return $this->belongsToMany(Medias::class, 'conf_lifestyles_medias', 'lifestyles_id', 'medias_id');
    }
}
