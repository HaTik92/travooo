<?php

namespace App\Models\ExpertsTravelStyles;

use App\Models\Activity\Activity;
use App\Models\Place\Place;
use App\Models\Travelstyles\Travelstyles;
use Illuminate\Database\Eloquent\Model;

class ExpertsTravelStyles extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'users_id',
        'travel_style_id'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User\User::class, 'users_id');
    }

      public function travelStyles()
    {
        return $this->belongsTo(Travelstyles::class,'travel_style_id');
    }
}
