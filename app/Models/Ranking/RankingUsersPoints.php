<?php

namespace App\Models\Ranking;

use Illuminate\Database\Eloquent\Model;

class RankingUsersPoints extends Model
{
    protected $table = 'ranking_user_points';

    public $timestamps = true;

    protected $fillable = ['users_id', 'points'];
}
