<?php

namespace App\Models\Ranking;

use Illuminate\Database\Eloquent\Model;

class RankingPointsEarners extends Model
{
    protected $table = 'ranking_points_earners';

    protected $fillable = ['entity_id', 'entity_type', 'user_id'];
}
