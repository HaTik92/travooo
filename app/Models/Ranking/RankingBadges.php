<?php

namespace App\Models\Ranking;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;

class RankingBadges extends Model
{
    protected $table = 'ranking_badges';
    protected $fillable = ['name', 'url', 'order', 'followers', 'points', 'active'];

    public $timestamps = true;
    
    public function users()
    {
        return $this->belongsToMany('App\Models\User\User', 'ranking_user_badges', 'badges_id', 'users_id');
    }

    public function getActionsAttribute()
    {
        return '<a href="'.route('admin.badges.edit', $this).'" class="btn btn-xs btn-info"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i></a>';
    }
}
