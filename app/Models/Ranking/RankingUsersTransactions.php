<?php

namespace App\Models\Ranking;

use Illuminate\Database\Eloquent\Model;

class RankingUsersTransactions extends Model
{
    protected $table = 'ranking_user_transactions';
    public $timestamps = true;

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\Models\User\User', 'id', 'users_id');
    }
}
