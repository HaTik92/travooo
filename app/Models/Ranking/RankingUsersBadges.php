<?php

namespace App\Models\Ranking;

use Illuminate\Database\Eloquent\Model;

class RankingUsersBadges extends Model
{
    protected $table = 'ranking_user_badges';

    public $timestamps = true;

    protected $fillable = ['users_id', 'badges_id', 'is_preset'];
    
    public function badge()
    {
        return $this->hasOne('App\Models\Ranking\RankingBadges', 'id', 'badges_id');
    }
    
     public function user()
    {
        return $this->hasOne('App\Models\User\User', 'id', 'users_id');
    }
}
