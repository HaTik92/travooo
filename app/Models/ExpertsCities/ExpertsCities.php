<?php

namespace App\Models\ExpertsCities;

use Illuminate\Database\Eloquent\Model;

class ExpertsCities extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'users_id',
        'cities_id'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User\User::class, 'users_id');
    }

    public function cities()
    {
        return $this->belongsTo(\App\Models\City\Cities::class, 'cities_id');
    }
}
