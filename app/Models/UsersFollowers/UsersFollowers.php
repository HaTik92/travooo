<?php

namespace App\Models\UsersFollowers;

use Illuminate\Database\Eloquent\Model;

class UsersFollowers extends Model
{
    // protected $fillable = array('users_id');
    protected $guarded = [];
    public function user()
    {
        return $this->hasOne(\App\Models\User\User::class, 'id', 'users_id');
    }

    public function follower()
    {
        return $this->hasOne(\App\Models\User\User::class, 'id', 'followers_id');
    }

    public function following()
    {
        return $this->hasOne(\App\Models\User\User::class, 'id', 'users_id');
    }
}
