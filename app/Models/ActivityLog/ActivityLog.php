<?php

namespace App\Models\ActivityLog;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;

class ActivityLog extends Model
{
    const PLAN_SHARE_TYPE = 'plan_shared';
    const PLAN_SHARE_TYPE_STEP = 'plan_step_shared';
    const PLAN_SHARE_TYPE_MEDIA = 'plan_media_shared';

    protected $table    = 'activity_log';
    public $timestamps  = false;
    protected $fillable = ['users_id', 'type', 'action', 'time', 'permission', 'variable'];

    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function event()
    {
        return $this->hasOne('App\Models\Events\Events', 'id', 'variable');
    }

    public function post()
    {
        return $this->hasOne('App\Models\Posts\Posts', 'id', 'variable');
    }

    public function activityLog()
    {
        return $this->hasOne('App\Models\ActivityLog\ActivityLog', 'id', 'variable');
    }
}
