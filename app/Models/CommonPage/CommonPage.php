<?php

namespace App\Models\CommonPage;

use Illuminate\Database\Eloquent\Model;

class CommonPage extends Model
{
    protected $table = 'common_pages';

    const SLUG_PRIVACY_POLICY = "privacy-policy";
    const SLUG_TERMS_OF_SERVICE = "terms-of-service";

    const SLUG_MAPPING = [
        self::SLUG_PRIVACY_POLICY => 'Privacy Policy',
        self::SLUG_TERMS_OF_SERVICE => 'Terms Of Service',
    ];

    // protected $fillable = ['slug', 'active'];
    protected $guarded = [];

    const ACTIVE = 1;
    const DEACTIVE = 0;

    public function trans()
    {
        return $this->hasMany(CommonPageTranslations::class, 'common_page_id', 'id');
    }

    public function transsingle()
    {
        return $this->hasOne(CommonPageTranslations::class, 'common_page_id', 'id');
    }
}
