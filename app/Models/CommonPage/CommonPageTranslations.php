<?php

namespace App\Models\CommonPage;

use Illuminate\Database\Eloquent\Model;

class CommonPageTranslations extends Model
{
    protected $table = 'common_page_translations';
    public $timestamps = false;

    // protected $fillable = ['common_page_id', 'languages_id', 'title', 'description'];
    protected $guarded = [];

    public function translanguage()
    {
        return $this->belongsTo(Languages::class, 'languages_id');
    }
}
