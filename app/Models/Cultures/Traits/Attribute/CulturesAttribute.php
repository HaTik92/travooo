<?php

namespace App\Models\Cultures\Traits\Attribute;

/**
 * Class CulturesAttribute.
 */
trait CulturesAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
