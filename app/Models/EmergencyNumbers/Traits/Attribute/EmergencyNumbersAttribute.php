<?php

namespace App\Models\EmergencyNumbers\Traits\Attribute;

/**
 * Class EmergencyNumbersAttribute.
 */
trait EmergencyNumbersAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
