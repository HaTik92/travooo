<?php

namespace App\Models\InviteExpertLinks;

use App\Models\Ranking\RankingBadges;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class InviteExpertLink extends Model
{
    protected $table = 'invite_expert_links';

    protected $fillable = [
        'name',
        'points',
        'code',
        'badge_id'
    ];

    public function experts()
    {
        return $this->hasMany(User::class);
    }

    public function badge()
    {
        return $this->belongsTo(RankingBadges::class);
    }
}