<?php

namespace App\Models\TripCountries;

use Illuminate\Database\Eloquent\Model;

class TripCountries extends Model
{
    protected $table = 'trips_countries';
    
    public $timestamps = false;
    
    public function country()
    {
        return $this->belongsTo('App\Models\Country\Countries', 'countries_id');
    }
    public function trip()
    {
        return $this->belongsTo('App\Models\TripPlans\TripPlans', 'trips_id');
    }
}
