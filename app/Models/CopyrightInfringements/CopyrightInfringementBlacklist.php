<?php

namespace App\Models\CopyrightInfringements;

use Illuminate\Database\Eloquent\Model;

class CopyrightInfringementBlacklist extends Model
{

    protected $table = 'copyright_infringement_blacklist';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\Models\User\User', 'id', 'admin_id');
    }

    public function copyright_infringement()
    {
        return $this->hasOne('App\Models\CopyrightInfringement\CopyrightInfringement', 'sender_email', 'sender_email');
    }
}
