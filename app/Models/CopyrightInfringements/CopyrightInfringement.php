<?php

namespace App\Models\CopyrightInfringements;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CopyrightInfringement extends Model
{
    use SoftDeletes;

    protected $table = 'copyright_infringements';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\Models\User\User', 'id', 'owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blacklist()
    {
        return $this->belongsTo('App\Models\CopyrightInfringements\CopyrightInfringementBlacklist', 'sender_email', 'sender_email');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deleted_copyright()
    {
        return $this->belongsTo('App\Models\CopyrightInfringements\CopyrightInfringementBlacklist', 'id', 'copyright_infringement_id');
    }
}
