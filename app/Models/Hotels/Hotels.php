<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;
use App\Models\User\User;

class Hotels extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hotels';

    use ElasticquentTrait;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['countries_id', 'cities_id', 'places_id', 'lat', 'lng', 'active'];
    protected $mappingProperties = array(
        'title' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'type' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'location' => [
            'type' => 'text',
            "fielddata" => true,
        ]
    );

    public function deleteMedias()
    {
        $this->deleteModals($this->medias);
    }

    public function deleteModals($models)
    {
        if (!empty($models)) {
            foreach ($models as $key => $value) {
                $value->delete();
            }
        }
    }

    public function followers()
    {
        return $this->hasMany(User::class, 'id', 'users_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Reviews\Reviews', 'hotels_id', 'id');
    }

    public function getMedias()
    {

        return $this->belongsToMany('\App\Models\ActivityMedia\Media', 'hotels_medias', 'hotels_id', 'medias_id');
    }

    public function medias()
    {
        return $this->belongsToMany('\App\Models\ActivityMedia\Media', 'hotels_medias', 'hotels_id', 'medias_id');
    }

    function getIndexName()
    {
        return 'hotels';
    }

    function getTypeName()
    {
        return 'hotels';
    }
}
