<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

class HotelFollowers extends Model
{
    protected $table    = 'hotels_followers';
    public $timestamps  = false;

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'hotels_id', 'users_id'];
    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
}
