<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Model;

class RankCronLog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rank_cron_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'cron_id',
        'post_type',
        'action',
        'message',
        'post_id',
        'loop_count',
        'rank_type',
        'created_at',
        'updated_at'
    ];

    const ACTION_START = 'start';
    const ACTION_END = 'end';
    const ACTION_FAIL = 'fail';

    const RANK_TYPE_7 = 1;
    const RANK_TYPE_ALL = 2;
    const RANK_TYPE_7_TEXT = '7 Days';
    const RANK_TYPE_ALL_TEXT = 'All Time';
}
