<?php

namespace App\Models\TripCities;

use Illuminate\Database\Eloquent\Model;

class TripCities extends Model
{
    protected $table = 'trips_cities';

    protected $fillable = ['duration', 'distance'];
    protected $dates = ['date'];

    public $timestamps = false;

    public function city()
    {
        return $this->belongsTo('App\Models\City\Cities', 'cities_id');
    }
    public function trip()
    {
        return $this->belongsTo('App\Models\TripPlans\TripPlans', 'trips_id');
    }
}
