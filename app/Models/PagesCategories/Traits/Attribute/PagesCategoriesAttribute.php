<?php

namespace App\Models\PagesCategories\Traits\Attribute;

/**
 * Class PagesCategoriesAttribute.
 */
trait PagesCategoriesAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
