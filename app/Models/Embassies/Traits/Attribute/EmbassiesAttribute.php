<?php

namespace App\Models\Embassies\Traits\Attribute;

/**
 * Class EmbassiesAttribute.
 */
trait EmbassiesAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
