<?php

namespace App\Models\Pages\Traits\Attribute;

/**
 * Class PagesAttribute.
 */
trait PagesAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
