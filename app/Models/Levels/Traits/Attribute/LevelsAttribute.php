<?php

namespace App\Models\Levels\Traits\Attribute;

/**
 * Class LevelsAttribute.
 */
trait LevelsAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
