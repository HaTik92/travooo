<?php

namespace App\Models\Regions\Traits\Relationship;

use App\Models\System\Session;

/**
 * Class RegionsRelationship.
 */
trait RegionsRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trans()
    {
        return $this->hasMany(config('locations.regions_trans'), 'regions_id');
    }

    public function transsingle()
    {
        return $this->hasOne(config('locations.regions_trans'), 'regions_id');
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    public function medias()
    {
        return $this->hasMany(config('locations.regions_medias'), 'regions_id');
    }
}
