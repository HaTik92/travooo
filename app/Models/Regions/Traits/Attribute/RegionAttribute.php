<?php

namespace App\Models\Regions\Traits\Attribute;

/**
 * Class RegionAttribute.
 */
trait RegionAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
