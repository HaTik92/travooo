<?php

namespace App\Models\Destinations;

use Illuminate\Database\Eloquent\Model;

class CitiesByNationality extends Model
{

    protected $table = 'top_cities_by_nationality';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function nationality()
    {
        return $this->hasOne('App\Models\Country\Countries', 'id', 'nationality_id');
    }

    /**
     * Many-to-Many relations with DiscussionReplies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function countries() {
        return $this->hasMany('App\Models\Country\Countries', 'countries_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function country()
    {
        return $this->hasOne('App\Models\Country\Countries', 'id', 'countries_id');
    }

    public function getActionsAttribute()
    {
        return '<a href="'.route('admin.cities-by-nationality.edit', $this->nationality_id).'" class="btn btn-xs btn-info"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i></a>';
    }

}
