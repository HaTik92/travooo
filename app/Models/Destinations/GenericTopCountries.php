<?php

namespace App\Models\Destinations;

use Illuminate\Database\Eloquent\Model;

class GenericTopCountries extends Model
{

    protected $table = 'generic_top_countries';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function country()
    {
        return $this->hasOne('App\Models\Country\Countries', 'id', 'countries_id');
    }

    public function getActionsAttribute()
    {
        return '<a href="'.route('admin.generic-top-countries.edit', $this).'" class="btn btn-xs btn-info">
                <i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>
                </a>
                <a href="'.route('admin.generic-top-countries.destroy', $this) .'"
                            data-method="delete"
                            data-trans-button-cancel="'. __('buttons.general.cancel') . '"
                            data-trans-button-confirm="'. __('buttons.general.crud.delete') . '"
                            data-trans-title="'. __('strings.backend.general.are_you_sure') . '"
                            class="btn btn-xs btn-danger">
                            <i class="fa fa-trash" rel="tooltip" data-toggle="tooltip" data-placement="top" title="'. __('buttons.general.crud.delete') . '"></i>
                </a>';
    }

}
