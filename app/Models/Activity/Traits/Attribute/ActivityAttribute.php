<?php

namespace App\Models\Activity\Traits\Attribute;

/**
 * Class ActivityAttribute.
 */
trait ActivityAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
