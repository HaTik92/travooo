<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;

class EventsLikes extends Model
{
    protected $table = 'events_likes';

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
}
