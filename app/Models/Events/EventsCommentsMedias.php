<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;

class EventsCommentsMedias extends Model
{
    public function media()
    {
        return $this->hasOne('App\Models\ActivityMedia\Media', 'id', 'medias_id');
    }
}
