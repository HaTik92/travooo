<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;

class EventsComments extends Model
{
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Events\EventsCommentsLikes');
    }
    
     public function medias()
    {
        return $this->hasMany('App\Models\Events\EventsCommentsMedias');
    }

}
