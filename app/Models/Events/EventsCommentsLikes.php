<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;

class EventsCommentsLikes extends Model
{
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
}
