<?php

namespace App\Models\Events;
use Illuminate\Database\Eloquent\Model;

class EventMedia extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'event_media';
    
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'event_id' , 'medias_id' ];
}
