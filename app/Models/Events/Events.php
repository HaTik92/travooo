<?php

namespace App\Models\Events;

use App\Models\Place\Medias;
use App\Services\Api\PrimaryPostService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Events extends Model
{
    use SoftDeletes;
    protected $table = 'events';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'city_id',
        'category',
        'description',
        'start',
        'countries_id',
        'end',
        'lat',
        'lng',
        'provider_id',
        'reviews',
        'ratings',
        'variable',
        'category',
        'cities_id'

    ];
    public function likes()
    {
        return $this->hasMany('App\Models\Events\EventsLikes');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')->where('parents_id', '=', NULL)->where('type', 'event')->orderby('created_at', 'DESC');
    }

    public function shares()
    {
        return $this->hasMany('App\Models\Events\EventsShares');
    }

    public function place()
    {
        return $this->hasOne('App\Models\Place\Place', 'id', 'places_id');
    }

    public function country()
    {
        return $this->hasOne('App\Models\Country\Countries', 'id', 'countries_id');
    }

    public function city()
    {
        return $this->hasOne('App\Models\City\Cities', 'id', 'cities_id');
    }

    public function medias()
    {
        return $this->belongsToMany('App\Models\Events\Events', 'event_media', 'event_id', 'medias_id');
    }

    public function getMedias()
    {
        return $this->belongsToMany(Medias::class, 'event_media', 'event_id', 'medias_id');
    }

    public function postComments()
    {
        return $this->hasMany('App\Models\Posts\PostsComments', 'posts_id')
            ->where('parents_id', '=', NULL)
            ->where('type', PrimaryPostService::TYPE_EVENT)
            ->orderby('created_at', 'DESC');
    }
}
