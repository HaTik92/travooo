<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;

class EventCities extends Model
{
    protected $table = 'musement_cities';
    protected $fillable = ['cities_id','countries_id','tp_id','name','country','tp_country_id'];
}
