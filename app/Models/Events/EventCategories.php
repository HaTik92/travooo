<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;

class EventCategories extends Model
{
    protected $table = 'events_categories';
    protected $fillable = ['p_id','tp_id','level','name','code','type'];
}
