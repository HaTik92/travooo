<?php

namespace App\Models\Submissions;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;

/**
 * Class Submissions
 * package App.
 */
class Submissions extends Model
{
    const NEW_SUBMISSIONS = 0;
    const APPROVED_SUBMISSIONS = 1;
    const DISAPPROVED_SUBMISSIONS = 1;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'search_submissions';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'status', 'place_name', 'place_category', 'place_hyperlink', 'place_city', 'id', 'place_country', "iso_code", "place_state", "lat", "lng"];

    public $timestamps = true;

    // public $incrementing = false;

    public function getUser()
    {
        return $this->belongsTo( User::class, 'user_id', 'id')->select(['name', 'email']);
    }

    public function getActionsAttribute()
    {
        return '<a href="'.route('admin.submission.approve', $this).'" class="btn btn-xs btn-info"><i class="fa fa-check-square" data-toggle="tooltip" data-placement="top" title="" data-original-title="Approve"></i></a>
        <a href="'.route('admin.submission.disapprove', $this).'" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Disapprove"></i></a>';
    }

}
