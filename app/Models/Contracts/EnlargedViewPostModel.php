<?php

namespace App\Models\Contracts;

use App\Models\EnlargedViews\EnlargedView;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

abstract class EnlargedViewPostModel extends Model implements EnlargedViewPostContract
{
    /**
     * @var string
     */
    protected $postView = '';

    /**
     * @return string
     */
    public function getPostView()
    {
        return $this->postView;
    }

    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function buildPostView()
    {
        return view($this->getPostView(), []);
    }

    /**
     * @return MorphOne
     */
    public function enlargedView()
    {
        return $this->morphOne(EnlargedView::class, 'entity');
    }
}
