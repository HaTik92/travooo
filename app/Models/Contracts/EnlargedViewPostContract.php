<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Relations\MorphOne;

interface EnlargedViewPostContract
{
    /**
     * @return string
     */
    public function getPostView();

    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function buildPostView();

    /**
     * @return MorphOne
     */
    public function enlargedView();
}
