<?php

namespace App\Models\Reviews;

use App\Models\ActivityMedia\Media;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reviews extends Model
{
    use SoftDeletes;
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'by_users_id');
    }

    public function place()
    {
        return $this->belongsTo('App\Models\Place\Place', 'places_id');
    }

    public function updownvotes()
    {
        return $this->hasMany('App\Models\Reviews\ReviewsVotes', 'review_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Reviews\ReviewsVotes', 'review_id')
            ->where('vote_type', ReviewsVotes::LIKE);
    }

    public function dislikes()
    {
        return $this->hasMany('App\Models\Reviews\ReviewsVotes', 'review_id')
            ->where('vote_type', ReviewsVotes::DISLIKE);
    }

    public function shares()
    {
        return $this->hasMany('App\Models\Reviews\ReviewsShares', 'review_id');
    }

    public function medias()
    {
        return $this->belongsToMany('App\Models\Place\Medias', 'reviews_media', 'reviews_id', 'medias_id');
    }
}
