<?php

namespace App\Models\Reviews;

use Illuminate\Database\Eloquent\Model;

class ReviewsMedia extends Model
{
    protected $table ='reviews_media';
    protected $fillable = ['reviews_id','medias_id'];
}
