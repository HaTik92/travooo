<?php

namespace App\Models\Reviews;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class ReviewsVotes extends Model
{
    CONST LIKE = 1;
    CONST DISLIKE = 2;

    protected $fillable = ['review_id'];

    public function review()
    {
        return $this->belongsTo(Reviews::class, 'review_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function scopeLiked($query)
    {
        return $query->where('vote_type', ReviewsVotes::LIKE);
    }

    public function scopeDisliked($query)
    {
        return $query->where('vote_type', ReviewsVotes::DISLIKE);
    }
}
