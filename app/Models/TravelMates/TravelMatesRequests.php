<?php

namespace App\Models\TravelMates;

use Illuminate\Database\Eloquent\Model;
use App\Models\TripPlans\TripPlans;
use App\Models\TravelMates\TravelMatesRequestPlans;
use App\Models\TravelMates\TravelMatesWithoutPlan;

class TravelMatesRequests extends Model
{
    protected $table = 'travelmate_requests';
    
    public function plan()
    {
        return $this->belongsTo('App\Models\TripPlans\TripPlans', 'plans_id');
    }
    
    
    public function plans()
    {
        return $this->hasMany('App\Models\TripPlans\TripPlans', 'plans_id');
    }
    
    public function plan_country()
    {
        return $this->belongsTo('App\Models\TripCountries\TripCountries', 'plans_id', 'trips_id');
    }
    
    public function plan_city()
    {
        return $this->belongsTo('App\Models\TripCities\TripCities', 'plans_id', 'trips_id');
    }
    
    public function plan_place()
    {
        return $this->belongsTo('App\Models\TripPlaces\TripPlaces', 'plans_id', 'trips_id');
    }
    
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
    
    public function going()
    {
        return $this->hasMany('App\Models\TravelMates\TravelMatesRequestUsers', 'requests_id');
    }
    
    public function join_user()
    {
        return $this->belongsTo('App\Models\TravelMates\TravelMatesRequestUsers', 'requests_id', 'id');
    }
    
    public function versions()
    {
        return $this->hasMany('App\Models\TripPlans\TripsVersions', 'plans_id');
    }
    
    public function without_plan()
    {
        return $this->belongsTo('App\Models\TravelMates\TravelMatesWithoutPlan', 'without_plans_id');
    }
    
    public function without_plans()
    {
        return $this->hasMany('App\Models\TravelMates\TravelMatesWithoutPlan', 'without_plans_id');
    }
}
