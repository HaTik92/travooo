<?php

namespace App\Models\TravelMates;

use Illuminate\Database\Eloquent\Model;
use App\Models\TravelMates\TravelMatesRequests;

class TravelMatesRequestUsers extends Model
{
    protected $table = 'travelmate_request_users';
    public $timestamps = false;
    
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
    
    public function request()
    {
        
        return $this->belongsTo('App\Models\TravelMates\TravelMatesRequests', 'requests_id');
    }
    
    
}
