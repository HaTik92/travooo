<?php

namespace App\Models\TravelMates;

use Illuminate\Database\Eloquent\Model;

class TravelMatesRequestPlans extends Model
{
    protected $table = 'travelmate_request_plans';
    public $timestamps = false;
    
    public function requests()
    {
        return $this->belongsTo('App\Models\TravelMates\TravelMatesRequests', 'requests_id');
    }
}
