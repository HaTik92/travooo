<?php

namespace App\Models\TravelMates;

use Illuminate\Database\Eloquent\Model;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\Country\Countries;
use App\Models\City\Cities;

class TravelMatesWithoutPlan extends Model
{
    protected $table = 'travelmate_without_plans';
    
    public function author()
    {
        return $this->belongsTo('App\Models\User\User', 'users_id');
    }
    
    public function plan_requests()
    {
        return $this->hasOne('App\Models\TravelMates\TravelMatesRequests', 'without_plans_id');
    }
    
    public function author_location()
    {
        return $this->hasOne('App\Models\City\Cities',  'id', 'current_location');
    }
    
    public function plan_invitations()
    {
        return $this->hasMany('App\Models\TripPlans\TripsContributionRequests', 'w_plans_id');
    }
    
    
    public function countries() {
        $countries = [];
        $country_list = explode(',', $this->countries_id);
        $city_list = explode(',', $this->cities_id);
        if ($this->countries_id) {
            foreach ($country_list as $country) {
                $countries[] = [
                    'country_info'=>Countries::find($country),
                    'name'=>Countries::find($country)->transsingle->title
                ];
            }
        }

        if ($this->cities_id) {
            foreach ($city_list as $city) {
                $city_info = Cities::find($city);
                $countries[] = [
                    'country_info'=>@$city_info->country,
                    'name'=>@$city_info->transsingle->title
                ];
            }
        }
        return $countries;
    }

}
