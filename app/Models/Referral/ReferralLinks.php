<?php

namespace App\Models\Referral;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referral\ReferralLinksViews;
use Illuminate\Support\Facades\Auth;

class ReferralLinks extends Model
{
    public function clicks(){
        return $this->hasMany(ReferralLinksViews::class, 'links_id', 'id');
    }
    public function exits(){
        return $this->hasMany(ReferralLinksClicks::class, 'links_id', 'id');
    }
    
    
    public static function getReferralLinks($take, $skip, $order = 'newest'){
        
        $query = ReferralLinks::query();
        
        $query->where('users_id', Auth::user()->id);
        
        if($order == 'newest'){
            $query->orderBy('created_at', 'DESC');
        }else{
             $query->orderBy('created_at', 'ASC');
        }
        
        if ($skip != '') {
            $query->skip($skip);
        }
        
        if ($take != '') {
            $query->take($take);
        }
        
        return $query->get();
    }
}
