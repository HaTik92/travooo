<?php

namespace App\Models\Accommodations\Traits\Attribute;

/**
 * Class AccommodationsAttribute.
 */
trait AccommodationsAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
