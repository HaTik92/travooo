<?php

namespace App\Models\AgeRanges\Traits\Attribute;

/**
 * Class AgeRangesAttribute.
 */
trait AgeRangesAttribute
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
