<?php
namespace App\Fabrics\Feeds;

use App\Models\Events\Events as EventsModel;
use App\Models\Events\EventsLikes;
use App\Services\Api\ShareService;
use Illuminate\Support\Collection;

class Events extends FeedItemAbstract
{
    public function prepare($list)
    {
        $ids = $list->pluck('id');
        $authUserId = auth()->id();
        $result = new Collection;

        $events = EventsModel::query()
            ->whereIn('id', $ids)
            ->with([
                'place.transsingle:id,places_id,title',
                'country.transsingle:id,countries_id,title',
                'city.transsingle:id,cities_id,title',
            ])
            ->withCount('likes')
            ->get();

        $eventTotalShares = $this->getTotalShares($ids, ShareService::TYPE_EVENT);
        $eventsIsLiked = $this->getInteraction(EventsLikes::class, $ids, 'events_id', $authUserId);
        $comments = $this->getPrepareComments($ids, 'event');

        foreach ($events as $event) {
            $variable = json_decode($event->variable);
            $result->push(array_merge([
                'id' => $event->id,
                'type' => 'event',
                'date' => $event->created_at->toDateTimeString(),
                'booking_url' => $variable->url ?? '',
                'category' => $event->category,
                'title' => $event->title,
                'description' => $event->description,
                'img' => eventImg($event),
                'address' => str_replace("\n", ", ", $event->address ?? ''),
                'like_flag' => isset($eventsIsLiked[$event->id]), //is_liked
                'total_likes' => $event->likes_count,
                'total_shares' => $eventTotalShares[$event->id] ?? 0,
                'location' => $this->getLocation($event)
            ], $comments[$event->id] ?? []));

        }

        return $result;
    }
    private function getLocation($event)
    {
        $locationsType = ['place', 'country', 'city'];

        foreach ($locationsType as $value) {
            if ($event->$value) {
                if ($value == 'place') {
                    $event->place->place_type = do_placetype(@$event->place->place_type ?: 'Event');
                }
                $event->$value->title = $event->$value->transsingle->title;
                unset($event->$value->transsingle);
                return [
                    'type' => $value,
                    'data' => $event->$value
                ];
            }
        }
    }
}