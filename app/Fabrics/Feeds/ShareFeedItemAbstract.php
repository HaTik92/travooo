<?php
namespace App\Fabrics\Feeds;


class ShareFeedItemAbstract extends FeedItemAbstract
{
    protected function prepareMultipleShare($class, $share, $columnName, $type, $reference)
    {
        $anotherSharesOfThisMedia = $class::query()
            ->with(['author:id,name,username,profile_picture'])
            ->whereNull('comment')
            ->where($columnName, $share->$columnName)
            ->where('id', '<>', $share->id)
            ->limit(4)->get()->map(function ($item) {
                return $this->prepareUserLight($item->author);
            })->toArray();
        return array_merge([
                'type' => $type,
                'action' => 'multiple_share',
                'header' => array_merge([$this->prepareUserLight($share->author)], $anotherSharesOfThisMedia),
            ], $reference);
    }
}