<?php
namespace App\Fabrics\Feeds;

use App\Models\ActivityLog\ActivityLog;
use App\Models\TripPlans\TripsLikes;
use App\Services\Trips\TripsService;
use Illuminate\Support\Collection;
use App\Models\TripPlans\TripPlans as TripPlansModel;

class TripPlans extends FeedItemAbstract
{
    use TripPlanInfoTrait;

    public function prepare($list)
    {
        $ids = $list->pluck('id');
        $result = new Collection;
        $authUserId = auth()->id();

        $trips = TripPlansModel::whereIn('id', $ids)
            ->has('trip_places_by_active_version')
            ->with([
                'author:id,name,username,profile_picture',
                'trip_places_by_active_version.place.transsingle:id,title,places_id',
                'trip_places_by_active_version.place.first_media.media',
                'trip_places_by_active_version.country.transsingle:id,title,countries_id',
                'trip_places_by_active_version.city.transsingle:id,title,cities_id',
                'trip_places_by_active_version.city.first_media.medias',
            ])
            ->withCount(['likes', 'shares'])
            ->get();

        //TODO we must move this in updated_at field of TripPlans table
        $lastUpdatedLog = ActivityLog::query()->select('variable', 'time')
            ->where('type', 'trip')
            ->whereIn('variable', $ids)
            ->whereIn('action', ['create', 'plan_updated'])
            ->orderBy('time', 'DESC')
            ->groupBy(['variable'])
            ->pluck('time', 'variable');

        $tripsIsLiked = $this->getInteraction(TripsLikes::class, $ids, 'trips_id', $authUserId);

        $comments = $this->getPrepareComments($ids, 'trip');

        foreach ($trips as $trip) {
            $index = $list->search(function ($item) use ($trip) {
                return $item->id > $trip->id;
            });

            $tripPlanInfo = $this->getTripPlanInfo($trip->trip_places_by_active_version);
            $data = array_merge([
                'id' => $trip->id,
                'type' => 'trip',
                'author' => $this->prepareUserLight($trip->author),
                'date' => $trip->created_at,
                'updated_at' => $lastUpdatedLog[$trip->id],
                'like_flag' => isset($tripsIsLiked[$trip->id]),
                'total_likes' => $trip->likes_count,
                'total_shares' => $trip->shares_count,
                'static_map_image' => tripPlanThumb($trip, 615, 400),
                'follow_header' => $list[$index]->isFollowContent ? getTripDestinationDetails($trip) : null,
            ], $comments[$trip->id] ?? [], $tripPlanInfo);

            $result->push($data);
        }
        return $result;
    }


}