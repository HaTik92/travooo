<?php
namespace App\Fabrics\Feeds;

use App\Models\TripPlans\TripContentPostLike;
use App\Models\TripPlans\TripsShares as TripsSharesModel;
use App\Services\Api\ShareService;
use Illuminate\Support\Collection;

class TripsShares extends ShareFeedItemAbstract
{
    public function prepare($list)
    {
        $ids = $list->pluck('id');
        $authUserId = auth()->id();
        $result = new Collection;

        $shares = TripsSharesModel::query()
            ->whereIn('id', $ids)
            ->with([
                'author:id,name,username,profile_picture',
                'tags',
            ])
            ->get();

        $tripPlansFeed = new TripPlans();
        $tripPlans = $tripPlansFeed->prepare($shares->pluck('trip_id'));

        $actionableShares = $shares->filter(function ($share) { return !is_null($share->comment); });

        if ($actionableShares->count()) {
            $actionableSharesIds = $actionableShares->pluck('id')->toArray();
            $totalShares = $this->getCounts(
                TripsSharesModel::class,
                $shares->pluck('trip_id')->toArray(),
                'trip_id');
            $totalLikes = $this->getCounts(
                TripContentPostLike::class,
                $actionableSharesIds,
                'posts_id',
                [['posts_type', ShareService::TYPE_TRIP_SHARE]]
            );
            $comments = $this->getPrepareComments($actionableSharesIds, ShareService::TYPE_TRIP_SHARE);
        }
        foreach ($shares as $share) {
            $reference = $tripPlans;
            if (is_null($share->comment)) {
                $result->push($this->prepareMultipleShare(
                    TripsSharesModel::class,
                    $share,
                    'trip_id',
                    ShareService::TYPE_TRIP_SHARE,
                    $reference
                ));
            } else {
                $result->push(array_merge([
                    'id' => $share->id,
                    'type' => ShareService::TYPE_TRIP_SHARE,
                    'author' => $this->prepareUserLight($share->author),
                    'date' => $share->created_at->toDateTimeString(),
                    'comment' => convert_post_text_to_tags(strip_tags($share->comment), $share->tags, false),
                    'reference' => $reference,
                    'total_likes' => $totalLikes[$share->id] ?? 0,
                    'total_shares' => $totalShares[$share->trip_media_id] ?? 0,
                ], $comments[$share->id] ?? []));
            }
        }

        return $result;
    }

}