<?php
namespace App\Fabrics\Feeds;

use App\Services\Api\ShareService;

class FeedsFabric
{
    static public function get($type)
    {
        switch ($type) {
            case 'other':
            case 'post': return new Posts;
            case 'review': return new Reviews;
            case 'discussion': return new Discussions;
            case 'report': return new Reports;
            case 'trip': return new TripPlans;
            case 'event': return new Events;
            case ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE: return new TripsMediasShares;
            case ShareService::TYPE_TRIP_PLACE_SHARE: return new TripsPlacesShares;
            case ShareService::TYPE_TRIP_SHARE: return new TripsShares;
            case 'share': return new PostsShares;
            default:
                return null;
        }
    }
}