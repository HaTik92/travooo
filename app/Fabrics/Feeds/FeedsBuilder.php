<?php
namespace App\Fabrics\Feeds;

use App\DataObjects\FeedItemDTO;
use Illuminate\Support\Collection;

class FeedsBuilder
{
    private $list;
    private $order;

    public function __construct()
    {
        $this->list = new Collection();
        $this->order = [];
    }

    public function add(FeedItemDTO $item)
    {
        $this->list->push($item);
        $this->order[] = $item->type . '-' . $item->id;
    }

    public function getFeeds()
    {
        $result = array_flip($this->order);

        $gropedList = $this->list->groupBy(function ($item) {
            return $item->type;
        });

        if (isset($gropedList['share'])) {
            $shareFabric = FeedsFabric::get('share');
            $preparedData = $shareFabric->prepare($gropedList['share']);
            foreach ($preparedData as $data) {
                if (empty($gropedList[$data['type']])) {
                    $gropedList[$data['type']] = new Collection();
                }
                $feedItem = new FeedItemDTO($data['id'], $data['type'], $data['action']);
                $feedItem->shareData = $data['data'];
                $feedItem->withLikedAndShared = $data['action'] == 'multiple_share';
                $gropedList[$data['type']]->push($feedItem);
            }
            unset($gropedList['share']);
        }

        foreach ($gropedList as $type => $list) {
            $fabric = FeedsFabric::get($type);
            $preparedData = $fabric ? $fabric->prepare($list) : [];
            foreach ($preparedData as $data) {
                $feedItem = $list->firstWhere('id', $data['share_id'] ?? $data['id']);
                $result[$data['type'] . '-' . $data['id']] = $fabric->prepareMainData($data, $feedItem);
            }
        }

        return array_values(array_filter($result, 'is_array'));
    }
}