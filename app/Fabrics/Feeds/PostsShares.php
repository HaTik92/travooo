<?php
namespace App\Fabrics\Feeds;

use App\DataObjects\FeedItemDTO;
use App\Models\Posts\PostsLikes;
use App\Models\Posts\PostsShares as PostsSharesModel;
use App\Services\Api\ShareService;
use Illuminate\Support\Collection;

class PostsShares extends FeedItemAbstract
{

    public function prepare($list)
    {
        $ids = $list->pluck('id');
        $authUserId = auth()->id();
        $result = new Collection;

        $shares = PostsSharesModel::query()
            ->whereIn('id', $ids)
            ->with([
                'post.author:id,name,username,profile_picture',
                'post.tags'
            ])
            ->get();

        $posts = $shares->filter(function ($item) {
            return !is_null($item->text);
        })->map(function ($share) {
            return $share->post;
        });

        $postsIds = $posts->pluck('posts_id')->toArray();
        $comments = $this->getPrepareComments($postsIds, 'post');
        $postsTotalShares = $this->getTotalShares($postsIds, ShareService::TYPE_POST);
        $postsIsLiked = $this->getInteraction(PostsLikes::class, $postsIds, 'posts_id', $authUserId);
        $postsTotalLiked = $this->getCounts(PostsLikes::class, $postsIds, 'posts_id');
        foreach ($shares as $share) {
            if (is_null($share->text)) {
                $anotherSharesOfThisPost = PostsSharesModel::query()
                    ->with(['author:id,name,username,profile_picture'])
                    ->whereNull('text')
                    ->whereType($share->type)
                    ->wherePostsTypeId($share->posts_type_id)
                    ->where('id', '<>', $share->id)
                    ->limit(4)->get()->map(function ($item) {
                        return $this->prepareUserLight($item->author);
                    })->toArray();
                $action = 'multiple_share';
                $data = [
                    'header' => array_merge([$this->prepareUserLight($share->author)], $anotherSharesOfThisPost),
                    'action' => $action,
                ];
            } else {
                $action = 'share';
                $data = array_merge([
                    'id' => $share->id,
                    'author' => $this->prepareUserLight($share->post->author),
                    'type' => 'share',
                    'like_flag' => isset($postsIsLiked[$share->posts_id]), //is_liked
                    'total_likes' => $postsTotalLiked[$share->posts_id] ?? 0,
                    'total_shares' => $postsTotalShares[$share->posts_id] ?? 0,
                    'comment' => convert_post_text_to_tags(strip_tags($share->text), $share->post->tags, false),
                ], $comments[$share->posts_id] ?? []);
            }

            $result->push([
                'id' => $share->posts_type_id,
                'type' => $share->type,
                'action' => $action,
                'share_id' => $share->id,
                'data' => $data
            ]);
        }

        return $result;
    }
}