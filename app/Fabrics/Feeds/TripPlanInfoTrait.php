<?php
namespace App\Fabrics\Feeds;

use App\Services\Trips\TripsService;

trait TripPlanInfoTrait
{
    private function getTripPlanInfo($tripPlaces, $isShareFor = null)
    {
        $places = [];
        $countries = [];
        $cities = [];
        $total_destination = 0;
        $_totalDestination = ['city_wise' => [], 'country_wise' => []];

        foreach ($tripPlaces as $tripPlace) {
            if (!isset($places[$tripPlace->places_id]) && $tripPlace->place) {
                $total_destination++;
                $_totalDestination['city_wise'][$tripPlace->cities_id][$tripPlace->places_id] = $tripPlace->place;
                $_totalDestination['country_wise'][$tripPlace->countries_id][$tripPlace->places_id] = $tripPlace->place;
                $places[$tripPlace->places_id] = $this->getDestinationData($tripPlace, 'place', $isShareFor);
            }
            if (!isset($countries[$tripPlace->countries_id])) {
                $countries[$tripPlace->countries_id] = $this->getDestinationData($tripPlace, 'country', $isShareFor);
            }
            if (!isset($cities[$tripPlace->cities_id])) {
                $cities[$tripPlace->cities_id] = $this->getDestinationData($tripPlace, 'city', $isShareFor);
            }
        }

        foreach ($countries as $country_id => &$country) {
            $country['total_destination'] = isset($_totalDestination['country_wise'][$country_id]) ? count($_totalDestination['country_wise'][$country_id]) : 0;
        }
        foreach ($cities as $city_id =>  &$city) {
            $city['total_destination'] = isset($_totalDestination['city_wise'][$city_id]) ? count($_totalDestination['city_wise'][$city_id]) : 0;
        }

        if (count($countries) > 1) {
            $result = [
                'trip_type' => TripsService::NEWSFEED_MULTIPLE_COUNTRY,
                'data' => array_values($countries)
            ];
        } else {
            if (count($cities) > 1) {
                $result = [
                    'trip_type' => TripsService::NEWSFEED_MULTIPLE_CITY,
                    'data' => array_values($cities)
                ];
            } else {
                $result = [
                    'trip_type' => TripsService::NEWSFEED_SINGLE_CITY,
                    'data' => array_values($places),
                ];
            }
            $result['country_title'] = array_values($countries)[0]['title'] ?? null;
        }

        $result['total_destination'] = $total_destination;

        return $result;
    }

    private function getDestinationData($tripPlace, $type, $isShareFor = null)
    {
        $result = [
            'id' => $tripPlace->$type->id,
            'title' => $tripPlace->$type->transsingle->title ?? null,
            'lat' => $tripPlace->$type->lat,
            'lng' => $tripPlace->$type->lng,
            'image' => $type == 'country'
                ? check_city_photo($tripPlace->$type->first_media->medias->url ?? null)
                : get_country_flag($tripPlace->country)
        ];

        if ($isShareFor) $result['is_share'] = $isShareFor == $tripPlace->id;

        if ($type == 'place') $result['city_id'] = $tripPlace->cities_id;

        if ($type != 'place') $result['total_destination'] = 0;

        if ($type != 'country') $result['country_id'] = $tripPlace->countries_id;

        return $result;
    }
}