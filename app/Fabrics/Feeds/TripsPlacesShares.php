<?php
namespace App\Fabrics\Feeds;

use App\Models\TripPlans\TripContentPostLike;
use App\Models\TripPlans\TripsPlacesShares as TripsPlacesSharesModel;
use App\Services\Api\ShareService;
use Illuminate\Support\Collection;

class TripsPlacesShares extends ShareFeedItemAbstract
{
    use TripPlanInfoTrait;

    public function prepare($list)
    {
        $ids = $list->pluck('id');
        $authUserId = auth()->id();
        $result = new Collection;

        $shares = TripsPlacesSharesModel::query()
            ->whereIn('id', $ids)
            ->with([
                'author:id,name,username,profile_picture',
                'tags',
                'trip_place.trip.author:id,name,username,profile_picture',
                'trip_place.trip.trip_places_by_active_version'
            ])
            ->get();
        $actionableShares = $shares->filter(function ($share) { return !is_null($share->comment); });

        if ($actionableShares->count()) {
            $actionableSharesIds = $actionableShares->pluck('id')->toArray();
            $totalShares = $this->getCounts(
                TripsPlacesSharesModel::class,
                $actionableShares->pluck('trip_place_id')->toArray(),
                'trip_place_id');
            $totalLikes = $this->getCounts(
                TripContentPostLike::class,
                $actionableSharesIds,
                'posts_id',
                [['posts_type', ShareService::TYPE_TRIP_PLACE_SHARE]]
            );
            $comments = $this->getPrepareComments($actionableSharesIds, ShareService::TYPE_TRIP_PLACE_SHARE);
        }

        foreach ($shares as $share) {
            $tripPlanInfo = $this->getTripPlanInfo($share->trip_place->trip->trip_places_by_active_version, $share->trip_place_id);

            if (is_null($share->comment)) {
                $result->push($this->prepareMultipleShare(
                    TripsPlacesSharesModel::class,
                    $share,
                    'trip_place_id',
                    ShareService::TYPE_TRIP_PLACE_SHARE,
                    $tripPlanInfo
                ));
            } else {
                $result->push(array_merge([
                    'id' => $share->id,
                    'type' => ShareService::TYPE_TRIP_PLACE_SHARE,
                    'author' => $this->prepareUserLight($share->author),
                    'date' => $share->created_at->toDateTimeString(),
                    'comment' => convert_post_text_to_tags(strip_tags($share->comment), $share->tags, false),
                    'reference' => $tripPlanInfo,
                    'total_likes' => $totalLikes[$share->id] ?? 0,
                    'total_shares' => $totalShares[$share->trip_media_id] ?? 0,
                ], $comments[$share->id] ?? []));
            }
        }

        return $result;
    }

}