<?php
namespace App\Fabrics\Feeds;

use App\DataObjects\FeedItemDTO;
use App\Models\ActivityMedia\Media;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCommentsLikes;
use App\Models\Posts\PostsCommentsMedias;
use App\Models\Posts\PostsShares;
use App\Models\User\User;
use Illuminate\Support\Facades\DB;

abstract class FeedItemAbstract
{
    protected function prepareUserLight(User $user = null)
    {
        if (!$user) return null;
        return [
            'id' => $user->id,
            'name' => $user->name,
            'username' => $user->username,
            'profile_picture' => check_profile_picture($user->profile_picture)
        ];
    }

    protected function prepareMedias($medias, $urlColumnName = 'url')
    {
        return $medias->map(function ($media) use ($urlColumnName) {
            $type = $media->type == Media::TYPE_IMAGE || is_null($media->type) ? 'image' : 'video';
            return [
                'type' => $type,
                'url' => $media->type == Media::TYPE_IMAGE
                    ? replace_s3_path_with_cloudfront($media->$urlColumnName, '800x0')
                    : $media->$urlColumnName,
                'thumb' => $media->type == Media::TYPE_IMAGE
                    ? replace_s3_path_with_cloudfront($media->$urlColumnName, '200x0')
                    : $media->video_thumbnail_url,
                'title' => $media->title
            ];
        });
    }

    protected function getInteraction($class, $ids, $column, $userId, $userColumn = 'users_id')
    {
        return $class::whereIn($column, $ids)
            ->where($userColumn, $userId)
            ->pluck('id', $column);
    }

    protected function getCounts($class, $ids, $column, $condition = [])
    {
        return $class::select(DB::raw("count(id) as count, $column"))
            ->whereIn($column, $ids)
            ->when($condition, function ($query) use ($condition) {
                $query->where($condition);
            })
            ->groupBy($column)
            ->pluck('count', $column);
    }

    protected function getTotalShares($ids, $type)
    {
        return PostsShares::query()
            ->selectRaw('count(id) as count, posts_type_id')
            ->whereIn('posts_type_id', $ids)
            ->where('type', $type)
            ->groupBy(['posts_type_id'])
            ->pluck('count', 'posts_type_id');
    }

    protected function getPrepareComments($ids, $type)
    {
        $commentsIds = [];
        $commentsUsersIds = [];
        $comments = [];
        $result = [];

        $commentsCount = PostsComments::query()
            ->selectRaw('count(id) as total, posts_id')
            ->where('type', $type)
            ->whereIn('posts_id', $ids)
            ->groupBy(['posts_id'])
            ->pluck('total', 'posts_id');

        foreach ($ids as $id)
        {
            if (isset($commentsCount[$id])) {
                $comments[$id] = PostsComments::query()
                    ->whereType($type)
                    ->wherePostsId($id)
                    ->limit(2)
                    ->get();
                $commentsIds = array_merge($commentsIds, $comments[$id]->pluck('id')->toArray());
                $commentsUsersIds = array_merge($commentsUsersIds, $comments[$id]->pluck('users_id')->toArray());
            } else {
                $comments[$id] = [];
            }

        }

        $commentsUsers = User::select(['id', 'name', 'username', 'profile_picture'])
            ->whereIn('id', $commentsUsersIds)
            ->get();

        $commentsMedias = PostsCommentsMedias::with('media')
            ->whereIn('posts_comments_id', $commentsIds)
            ->get()->groupBy('posts_comments_id');

        $commentsSubCount = $this->getCounts(PostsComments::class, $commentsIds, 'parents_id');
        $commentsLikesCount = $this->getCounts(PostsCommentsLikes::class, $commentsIds, 'posts_comments_id');
        $commentsIsLiked = $this->getInteraction(PostsCommentsLikes::class, $commentsIds, 'posts_comments_id', auth()->id());

        foreach ($comments as $group => $commentList) {
            $commentsData = [];
            foreach ($commentList as $comment) {
                $commentsData[] = [
                    'author' => $this->prepareUserLight($commentsUsers->firstWhere('id', $comment->users_id)),
                    'medias' => isset($commentsMedias[$comment->id]) ? $this->prepareMedias($commentsMedias[$comment->id]) : [],
                    'text' => $comment->text,
                    'total_likes' => $commentsLikesCount[$comment->id] ?? 0,
                    'like_status' => isset($commentsIsLiked[$comment->id]),
                    'created_at' => $comment->created_at,
                    'total_sub_comments' => $commentsSubCount[$comment->id] ?? 0,
                ];
            }
            $result[$group] = [
                'comment_flag' => isset($postsIsCommented[$group]),
                'total_comments' => $commentsCount[$group] ?? 0,
                'comments' => $commentsData
            ];
        }

        return $result;
    }

    public function prepareMainData($data, FeedItemDTO $feedItem, $pageId = null, $uniqueLink = null)
    {
        if ($feedItem->shareData) {
            $data = array_merge(
                $feedItem->shareData,
                $feedItem->action == 'multiple_share' ? $data : ['reference' => $data]);
        }

        if (!$feedItem->withLink) {
            return $data;
        }

        $link = 'newsfeed/' . $data['author']['username'] . '/' . $data['type'] . '/' . $data['id'];
        return [
            'page_id' => $pageId ?? ($feedItem->action != 'multiple_share' ? $this->link($link) : null),
            'unique_link' => $uniqueLink ?? ($feedItem->action != 'multiple_share' ? url($link) : null),
            'type' => $feedItem->type,
            'action' => $feedItem->action,
            'data' => $data
        ];
    }

    private function link($link)
    {
        $link = url($link);
        if (is_string($link)) {
            return base64_encode($link);
        }
        return $link;
    }
}