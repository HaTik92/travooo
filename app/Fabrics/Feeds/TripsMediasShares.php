<?php
namespace App\Fabrics\Feeds;

use App\Models\TripPlans\TripContentPostLike;
use App\Models\TripPlans\TripsMediasShares as TripsMediasSharesModel;
use App\Services\Api\ShareService;
use Illuminate\Support\Collection;

class TripsMediasShares extends ShareFeedItemAbstract
{
    public function prepare($list): Collection
    {
        $ids = $list->pluck('id');
        $result = new Collection;

        $shares = TripsMediasSharesModel::query()
            ->whereIn('id', $ids)
            ->with([
                'author:id,name,username,profile_picture',
                'tags',
                'trip_media.trip.author:id,name,username,profile_picture',
                'trip_media.tripPlace.place.transsingle:id,places_id,title',
                'trip_media.media'
            ])
            ->get();

        $tripMediaIds = $shares->pluck('trip_media_id')->toArray();

        $actionableShares = $shares->filter(function ($share) { return !is_null($share->comment); });

        if ($actionableShares->count()) {
            $totalShares = $this->getCounts(TripsMediasSharesModel::class, $tripMediaIds, 'trip_media_id');
            $totalLikes = $this->getCounts(
                TripContentPostLike::class,
                $ids,
                'posts_id',
                [['posts_type', ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE]]
            );

            $comments = $this->getPrepareComments($ids, ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE);
        }
        foreach ($shares as $share) {
            $media = $this->prepareMedias(new Collection([$share->trip_media->media]))->first();
            $media->users = $this->prepareUserLight($share->trip_media->trip->author);
            $reference = [
                'place_title' => $share->trip_media->tripPlace->place->transsingle->title ?? null,
                'lat' => $share->trip_media->tripPlace->place->lat,
                'lng' => $share->trip_media->tripPlace->place->lng,
                'media' => $media
            ];
            if (is_null($share->comment)) {
                $result->push($this->prepareMultipleShare(
                    TripsMediasSharesModel::class,
                    $share,
                    'trip_media_id',
                    ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE,
                    $reference
                ));
            } else {
                $result->push(array_merge([
                    'id' => $share->id,
                    'type' => ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE,
                    'author' => $this->prepareUserLight($share->author),
                    'date' => $share->created_at->toDateTimeString(),
                    'comment' => convert_post_text_to_tags(strip_tags($share->comment), $share->tags, false),
                    'reference' => $reference,
                    'total_likes' => $totalLikes[$share->id] ?? 0,
                    'total_shares' => $totalShares[$share->trip_media_id] ?? 0,
                ], $comments[$share->id] ?? []));
            }
        }

        return $result;
    }

}