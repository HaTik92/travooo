<?php
namespace App\Fabrics\Feeds;

use App\Http\Constants\CommonConst;
use App\Models\Posts\Posts as PostModel;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCommentsLikes;
use App\Models\Posts\PostsCommentsMedias;
use App\Models\Posts\PostsLikes;
use App\Models\Posts\PostsShares;
use App\Models\User\User;
use App\Services\Api\ShareService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Posts extends FeedItemAbstract
{
    public function prepare($list)
    {
        $ids = $list->pluck('id');
        $authUserId = auth()->id();
        $result = new Collection;


        $posts = PostModel::whereIn('id', $ids)
            ->with([
                'author:id,name,username,profile_picture',
                'getMedias',
                'checkin',
            ])
            ->withCount(['likes'])
            ->get();

        $postTotalShares = $this->getTotalShares($ids, ShareService::TYPE_POST);
        $postsIsLiked = $this->getInteraction(PostsLikes::class, $ids, 'posts_id', $authUserId);
        $comments = $this->getPrepareComments($ids, 'post');

        foreach ($posts as $post) {
            $index = $list->search(function ($item) use ($post) {
                return $item->id > $post->id;
            });
            $result->push(array_merge([
                'id' => $post->id,
                'type' => 'post',
                'author' => $this->prepareUserLight($post->author),
                'text' => convert_post_text_to_tags(strip_tags($post->text), $post->tags, false),
                'date' => $post->date,
                'medias' => $this->prepareMedias($post->medias),
                'like_flag' => isset($postsIsLiked[$post->id]), //is_liked
                'total_likes' => $post->likes_count,
                'total_shares' => $postTotalShares[$post->id] ?? 0,
                'follow_header' => $list[$index]->isFollowContent ? getPostDestinationDetails($post) : null,
                'check_in' => $this->getCheckIn($post),
            ], $comments[$post->id] ?? []));

            //TODO move it on front
            if ($authUserId) {
                $authUser = auth()->user();
                $pv = new \App\Models\Posts\PostsViews;
                $pv->posts_id = $post->id;
                $pv->users_id = $authUser->id;
                $pv->gender = $authUser->gender;
                $pv->nationality = $authUser->nationality;
                $pv->save();
            }
        }

        return $result;
    }

    private function getCheckIn($post)
    {
        if (isset($post->checkin[0])) {
            $postCheckin = $post->checkin[0];
            $type = null;
            if ($postCheckin->place_id) {
                $type = CommonConst::POST_CHECKIN_PLACE;
            } elseif ($postCheckin->country_id) {
                $type = CommonConst::POST_CHECKIN_COUNTRY;
            } elseif ($postCheckin->city_id) {
                $type = CommonConst::POST_CHECKIN_CITY;
            }

            if (!$type || !$postCheckin->$type) {
                return null;
            }

            return [
                'type' => $type,
                'title' => _fieldValueFromTrans($postCheckin->$type, 'title'),
                'image' => check_place_photo($postCheckin->$type)
            ];
        }
        return null;
    }
}