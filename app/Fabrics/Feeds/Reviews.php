<?php
namespace App\Fabrics\Feeds;

use App\Models\Place\PlaceFollowers;
use App\Models\Posts\PostsShares;
use App\Models\Reviews\Reviews as ReviewsModel;
use App\Models\Reviews\ReviewsVotes;
use App\Services\Api\ShareService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Reviews extends FeedItemAbstract
{
    public function prepare($list)
    {
        $ids = $list->pluck('id');
        $result = new Collection;
        $authUserId = auth()->id();

        $reviews = ReviewsModel::whereIn('id', $ids)
            ->with([
                'medias',
                'place.transsingle',
                'author:id,name,username,profile_picture',
            ])
            ->withCount('likes')
            ->get();

        $placesId = $reviews->pluck('places_id');

        $reviewTotalShares = $this->getTotalShares($ids, ShareService::TYPE_REVIEW);

        $reviewsPlaceTotalAndAvg = ReviewsModel::query()
            ->selectRaw('count(id) as count, avg(score) as avg, places_id')
            ->whereIn('places_id', $placesId)
            ->groupBy(['places_id'])
            ->get()->map(function ($item) {
                return [
                    $item->places_id => [
                        'count' => $item->count,
                        'avg' => $item->avg
                    ]
                ];
            });

        $reviewsIsLiked = ReviewsVotes::whereIn('review_id', $ids)
            ->where('users_id', $authUserId)
            ->liked()->pluck('id', 'review_id');

        $placeIsFollowed = $this->getInteraction(PlaceFollowers::class, $placesId, 'places_id', $authUserId);

        foreach ($reviews as $review) {
            $result->push([
                'id' => $review->id,
                'type' => 'review',
                'author' => $this->prepareUserLight($review->author),
                'text' => $review->comment,
                'date' => $review->created_at->toDateTimeString(),
                'avg_review' => $reviewsPlaceTotalAndAvg[$review->places_id]['avg'] ?? 0,
                'total_review' => $reviewsPlaceTotalAndAvg[$review->places_id]['count'] ?? 0,
                'total_shares' => $reviewTotalShares[$review->id] ?? 0,
                'like_flag' => isset($reviewsIsLiked[$review->id]),//is_liked
                'total_likes' => $review->likes_count,
                'medias' => $this->prepareMedias($review->medias),
                'place' => [
                    'id' => $review->places_id,
                    'name' => @$review->place->transsingle->title ?? '',
                    'image' => @check_place_photo(@$review->place),
                    'follow_status' => isset($placeIsFollowed[$review->places_id])
                ]
            ]);
        }

        return $result;
    }
}