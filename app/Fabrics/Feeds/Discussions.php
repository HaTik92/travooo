<?php
namespace App\Fabrics\Feeds;

use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Discussion\Discussion As DiscussionsModel;
use App\Models\Discussion\DiscussionExperts;
use App\Models\Discussion\DiscussionLikes;
use App\Models\Discussion\DiscussionMedias;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Discussion\DiscussionRepliesDownvotes;
use App\Models\Discussion\DiscussionRepliesUpvotes;
use App\Models\Place\Place;
use App\Models\User\User;
use App\Services\Api\ShareService;
use Illuminate\Support\Collection;

class Discussions extends FeedItemAbstract
{
    public function prepare($list)
    {
        $ids = $list->pluck('id');
        $result = new Collection;
        $authUserId = auth()->id();

        $expertsIds = [];

        $discussions = DiscussionsModel::whereIn('id', $ids)
            ->with([
                'author:id,name,username,profile_picture',
                'medias'
            ])
            ->withCount(['discussion_upvotes', 'discussion_downvotes', 'views', 'experts', 'main_replies'])
            ->get()->map(function ($discussion) use (&$expertsIds) {
                if($discussion->experts_count) {
                    $discussion->experts = $discussion->experts()->take(5)->get()->pluck('expert_id')->toArray();
                    $expertsIds = array_merge($expertsIds, $discussion->experts);
                }
                return $discussion;
            });

        $discussionsExpert = User::select('id', 'profile_picture', 'email', 'name', 'username', 'type', 'expert', 'cover_photo')
            ->whereIn('id', $expertsIds)->get()->map(function ($user) {
                $user->profile_picture = check_profile_picture($user->profile_picture);
                return $user;
            });
        $discussionsTotalShares = $this->getTotalShares($ids, ShareService::TYPE_DISCUSSION);

        $discussionsIsLiked = DiscussionLikes::whereIn('discussions_id', $ids)
            ->where('users_id', $authUserId)
            ->get()->groupBy('discussions_id');

        $discussionsIsTagged = $this->getInteraction(DiscussionExperts::class, $ids, 'discussion_id', $authUserId, 'expert_id');
        $discussionsIsReplies = $this->getInteraction(DiscussionReplies::class, $ids, 'discussions_id', $authUserId);

        $replies = $this->getPrepareReplies($discussions);

        foreach ($discussions as $discussion) {
            $index = $list->search(function ($item, $key) use ($discussion) {
                return $item->id > $discussion->id;
            });

            $result->push([
                'id' => $discussion->id,
                'type' => 'discussion',
                'author' => $this->prepareUserLight($discussion->author),
                'date' => $discussion->created_at->toDateTimeString(),
                'medias' => $this->prepareMedias($discussion->medias, 'media_url'),
                'total_replies' => $discussion->main_replies_count,
                'replies_flag' => isset($discussionsIsReplies[$discussion->id]),
                'latest_experts' => $discussionsExpert->filter(function ($user) use ($discussion) {
                    return in_array($user->id, $discussion->experts_count ? $discussion->experts : []);
                })->values(),
                'is_tagged' => isset($discussionsIsTagged[$discussion->id]),
                'upvote_flag' => isset($discussionsIsLiked[$discussion->id]) && $discussionsIsLiked[$discussion->id][0]['vote'] == DiscussionLikes::UP_VOTE,
                'downvotes_flag' => isset($discussionsIsLiked[$discussion->id]) && $discussionsIsLiked[$discussion->id][0]['vote'] == DiscussionLikes::DOWN_VOTE,
                'total_upvotes' => $discussion->discussion_upvotes_count,
                'total_total_downvotes' => $discussion->discussion_downvotes_count,
                'total_views' => $discussion->views_count,
                'total_experts' => $discussion->experts_count,
                'total_shares' => $discussionsTotalShares[$discussion->id] ?? 0,
                'destination' => $this->getDestination($discussion),
                'follow_header' => $list[$index]->isFollowContent ? getDiscussionDestinationDetails($discussion) : null,
                'replies' => $replies[$discussion->id] ?? []
            ]);
        }
        return $result;
    }

    public function getPrepareReplies($discussions)
    {
        $repliesIds = [];
        $repliesUsersIds = [];
        $replies = [];
        $result = [];

        foreach ($discussions as $discussion)
        {
            $replies[$discussion->id] = $discussion->replies()->take(3)->get();
            $repliesIds = array_merge($repliesIds, $replies[$discussion->id]->pluck('id')->toArray());
            $repliesUsersIds = array_merge($repliesUsersIds, $replies[$discussion->id]->pluck('users_id')->toArray());
        }

        $repliesUsers = User::select(['id', 'name', 'username', 'profile_picture'])
            ->whereIn('id', $repliesUsersIds)
            ->get();

        $repliesMedias = DiscussionMedias::with('media')
            ->whereIn('discussion_id', $repliesIds)
            ->get()->groupBy('discussion_id');

        $commentsSubCount = $this->getCounts(DiscussionReplies::class, $repliesIds, 'parents_id');
        $repliesUpVotesCount = $this->getCounts(DiscussionRepliesUpvotes::class, $repliesIds, 'discussions_id');
        $repliesDownVotesCount = $this->getCounts(DiscussionRepliesDownvotes::class, $repliesIds, 'discussions_id');

        $repliesUpIsVoted = $this->getInteraction(DiscussionRepliesUpvotes::class, $repliesIds, 'discussions_id', auth()->id());
        $repliesDownIsVoted = $this->getInteraction(DiscussionRepliesDownvotes::class, $repliesIds, 'discussions_id', auth()->id());

        foreach ($replies as $group => $repliesList) {
            $repliesData = [];
            foreach ($repliesList as $reply) {
                $repliesData[] = [
                    'author' => $this->prepareUserLight($repliesUsers->firstWhere('id', $reply->users_id)),
                    'medias' => isset($repliesMedias[$reply->id]) ? $this->prepareMedias($repliesMedias[$reply->id]) : [],
                    'text' => $reply->text,
                    'total_upvotes' => $repliesUpVotesCount[$reply->id] ?? 0,
                    'total_downvotes' => $repliesDownVotesCount[$reply->id] ?? 0,
                    'upvote_flag' => isset($repliesUpIsVoted[$reply->id]),
                    'downvotes_flag' => isset($repliesDownIsVoted[$reply->id]),
                    'created_at' => $reply->created_at,
                    'total_sub_replies' => $commentsSubCount[$reply->id] ?? 0,
                ];
            }
            $result[$group] = [
                'replies' => $repliesData
            ];
        }

        return $result;
    }

    private function getDestination($discussion)
    {
        switch (strtolower($discussion->destination_type)) {
            case 'place':
                $class = Place::class;
                break;
            case 'country':
                $class = Countries::class;
                break;
            case 'city':
                $class = Cities::class;
                break;
            default:
                return [];
        }
        $destination = $class::find($discussion->destination_id);
        return [
            'type' => strtolower($discussion->destination_type),
            'title' => $destination->transsingle->title,
            'image' => check_place_photo($destination)
        ];
    }
}