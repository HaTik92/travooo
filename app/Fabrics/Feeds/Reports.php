<?php
namespace App\Fabrics\Feeds;

use App\DataObjects\FeedItemDTO;
use App\Models\Reports\ReportsLikes;
use App\Services\Api\PrimaryPostService;
use App\Services\Api\ShareService;
use Illuminate\Support\Collection;
use App\Models\Reports\Reports as ReportsModel;

class Reports extends FeedItemAbstract
{
    public function prepare($list)
    {
        $ids = $list->pluck('id');
        $result = new Collection;
        $authUserId = auth()->id();

        $reports = ReportsModel::whereIn('id', $ids)
            ->with([
                'author:id,name,username,profile_picture',
                'story:id,reports_id,var,val',
                'cover:id,reports_id,var,val'
            ])
            ->with(['likes'])
            ->get();

        $reportsIsLiked = $this->getInteraction(ReportsLikes::class, $ids, 'reports_id', $authUserId);
        $totalShares = $this->getTotalShares($ids, ShareService::TYPE_REPORT);
        $comments = $this->getPrepareComments($ids, 'report');

        foreach ($reports as $report) {
            $index = $list->search(function ($item) use ($report) {
                return $item->id == $report->id;
            });

            $result->push(array_merge([
                'id' => $report->id,
                'type' => 'report',
                'author' => $this->prepareUserLight($report->author),
                'date' => $report->created_at->toDateTimeString(),
                'cover_url' => isset($report->cover[0]) ? replace_s3_path_with_cloudfront($report->cover[0]->val, '800x0') : null,
                'story' => $report->story->val ?? '',
                'like_flag' => isset($reportsIsLiked[$report->id]), //is_liked
                'total_likes' => $report->likes_count,
                'total_shares' => $totalShares[$report->id] ?? 0,
                'follow_header' => $list[$index]->isFollowContent ? getReportsDestinationDetails($report) : null,
            ], $comments[$report->id] ?? []));
        }
        return $result;
    }

    public function prepareMainData($data, FeedItemDTO $feedItem, $pageId = null, $uniqueLink = null)
    {
        return parent::prepareMainData($data, $feedItem, $data['id'], url('reports/' . $data['id']));
    }
}