<?php
namespace App\Fabrics\Following;

interface FollowingInterface
{
    public function count() : int;

    public function getList(int $skip, int $perPage) : array;
}