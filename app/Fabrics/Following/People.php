<?php
namespace App\Fabrics\Following;

use App\Models\User\User;
use App\Models\UsersFollowers\UsersFollowers;
use Illuminate\Support\Facades\Auth;

class People extends FollowingAbstract
{
    public function __construct(int $userId)
    {
        $this->query = UsersFollowers::select('followers_id as id')->where('users_id', $userId)->where('follow_type', 1);
    }

    public function getList(int $skip, int $perPage): array
    {
        $authUserId = Auth::user()->id;

        $ids = $this->query->skip($skip)->take($perPage)->pluck('id');

        $myFollowerIds = UsersFollowers::select('users_id as id')
            ->where('followers_id', $authUserId)
            ->whereIn('users_id', $ids)
            ->pluck('id')->toArray();

        return User::select('id', 'name', 'profile_picture')
            ->whereIn('id', $ids)->get()
            ->map(function ($user) use ($myFollowerIds) {
                return [
                    'id' => $user->id,
                    'name' => $user->name,
                    'type' => 'user',
                    'is_followed' => in_array($user->id, $myFollowerIds),
                    'picture' => check_profile_picture($user->profile_picture)
                ];
            })->toArray();
    }
}