<?php
namespace App\Fabrics\Following;

use App\Models\City\CitiesFollowers;
use App\Models\City\Cities as CitiesModel;

class Cities extends FollowingAbstract
{
    protected $modelFollowers = CitiesFollowers::class;
    protected $model = CitiesModel::class;
    protected $columnName = 'cities_id';

    public function getList(int $skip, int $perPage): array
    {
        $ids = $this->query->skip($skip)->take($perPage)->pluck('id');
        $myFollowerIds = $this->getMyFollowerIds($ids);

        return $this->model::select('id')->with('transsingle', 'getMedias')
            ->whereIn('id', $ids)->get()
            ->map(function ($city) use ($myFollowerIds) {
                //TODO redo on cover
                $media = $city->getMedias->first();
                return [
                    'id' => $city->id,
                    'name' => $city->transsingle->title,
                    'type' => 'city',
                    'is_followed' => in_array($city->id, $myFollowerIds),
                    'picture' => $media ? check_city_photo($media->url, 180) : ''
                ];
            })->toArray();
    }
}