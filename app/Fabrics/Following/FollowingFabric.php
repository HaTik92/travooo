<?php
namespace App\Fabrics\Following;

class FollowingFabric
{
    CONST FOLLOWING_CLASSES = [
        'people' => People::class,
        'countries' => Countries::class,
        'cities' => Cities::class,
        'places' => Places::class,
    ];
    public static function get(string $type, int $userId) : FollowingInterface
    {
        $class = self::FOLLOWING_CLASSES[$type];
        return new $class($userId);
    }
}