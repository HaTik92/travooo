<?php
namespace App\Fabrics\Following;

use App\Models\Country\CountriesFollowers;
use App\Models\Country\Countries as CountriesModel;

class Countries extends FollowingAbstract
{
    protected $modelFollowers = CountriesFollowers::class;
    protected $model = CountriesModel::class;
    protected $columnName = 'countries_id';

    public function getList(int $skip, int $perPage): array
    {
        $ids = $this->query->skip($skip)->take($perPage)->pluck('id');
        $myFollowerIds = $this->getMyFollowerIds($ids);

        return $this->model::select('id', 'iso_code')->with('trans')
            ->whereIn('id', $ids)->get()
            ->map(function ($country) use ($myFollowerIds) {
                return [
                    'id' => $country->id,
                    'name' => $country->trans[0]->title,
                    'type' => 'country',
                    'is_followed' => in_array($country->id, $myFollowerIds),
                    'picture' => get_country_flag($country, true)
                ];
            })->toArray();
    }
}