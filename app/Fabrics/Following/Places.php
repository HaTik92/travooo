<?php
namespace App\Fabrics\Following;

use App\Models\Place\PlaceFollowers;
use App\Models\Place\Place as PlaceModel;

class Places extends FollowingAbstract
{
    protected $modelFollowers = PlaceFollowers::class;
    protected $model = PlaceModel::class;
    protected $columnName = 'places_id';

    public function getList(int $skip, int $perPage): array
    {
        $ids = $this->query->skip($skip)->take($perPage)->pluck('id');
        $myFollowerIds = $this->getMyFollowerIds($ids);

        return $this->model::select('id')->with('transsingle', 'getMedias')
            ->whereIn('id', $ids)->get()
            ->map(function ($place) use ($myFollowerIds) {
                //TODO redo on cover
                $media = $place->getMedias->first();
                return [
                    'id' => $place->id,
                    'name' => $place->transsingle->title,
                    'type' => 'place',
                    'is_followed' => in_array($place->id, $myFollowerIds),
                    'picture' => $media ? check_place_photo($media->url, true) : ''
                ];
            })->toArray();
    }
}