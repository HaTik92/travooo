<?php
namespace App\Fabrics\Following;

use Illuminate\Support\Facades\Auth;

abstract class FollowingAbstract implements FollowingInterface
{
    protected $query;
    protected $model;
    protected $modelFollowers;
    protected $columnName;

    public function __construct(int $userId)
    {
        $this->query = $this->modelFollowers::select("$this->columnName as id")->where('users_id', $userId);
    }

    public function count(): int
    {
        return $this->query->count();
    }

    protected function getMyFollowerIds($ids)
    {
        $authUserId = Auth::user()->id;
        return $this->modelFollowers::select("$this->columnName as id")
            ->where('users_id', $authUserId)
            ->whereIn($this->columnName, $ids)
            ->pluck('id')->toArray();
    }
}