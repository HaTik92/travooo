<?php

namespace App\Repositories;

use App\Models\ActivityMedia\Media;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;

/**
 * Class BaseRepository.
 */
class BaseRepository {

    /** @var Filesystem */
    protected $storage;

    public function __construct() {
        
    }

    /**
     * @return mixed
     */
    public function getAll() {
        return $this->query()->get();
    }

    /**
     * @return mixed
     */
    public function getCount() {
        return $this->query()->count();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id) {
        return $this->query()->find($id);
    }

    /**
     * @return mixed
     */
    public function query() {
        return call_user_func(static::MODEL . '::query');
    }

    /**
     * Declaring Storage
     */
    private function initS3() {
        $this->storage = Storage::disk('s3');
    }

    /**
     * Uploads file to AWS S3 bucket storage
     *
     * @param UploadedFile $file
     * @param null $path
     * @return bool|false|string|array
     */
    public function saveToS3(UploadedFile $file, $path = null) {
        if (!$path) {
            return false;
        }

        $this->initS3();
        $this->storage->put($path, file_get_contents($file->getRealPath()), 'public');

        Log::info("Uploaded file to : {$this->storage->getMetadata($path)['path']}");

        return $this->storage->getMetadata($path)['path'];
    }

    /**
     * Remove the file from path
     *
     * @param null $path
     * @return bool
     */
    public function deleteFromS3($path = null) {
        if (!$path) {
            return false;
        }

        $this->initS3();

        return $this->storage->delete($path);
    }

    /**
     * Uploading licensed images
     *
     * @param array $filesInfo
     * @param Model $model
     * @param Model $pivot
     * @param null $targetColumn
     * @param null $path
     * @return bool
     */
    protected function uploadLicensedImagesToS3($filesInfo = [], Model $model, Model $pivot, $targetColumn = null, $path = null)
    {
        if ( ! $filesInfo) {
            return false;
        }

        foreach ($filesInfo as $inputs) {
            if(isset($inputs['image'])) {
                $imagePath = "{$path}/{$model->id}/" . sha1(microtime()) . ".{$inputs['image']->getClientOriginalExtension()}";
                $inputs['url'] = $this->saveToS3($inputs['image'], $imagePath);
                unset($inputs['image']);
                $media = Media::create($inputs);

                $pivot::create([
                    $targetColumn => $model->id,
                    'medias_id' => $media->id
                ]);
            }

        }

    }
    protected function saveLicensedImagesToS3($filesInfo = [], Model $model, Model $pivot, $targetColumn = null, $path = null)
    {
        if (!$filesInfo) {
            return false;
        }

        $images = $filesInfo['images'];
        $imagesData = $filesInfo['data'];

        Log::info("Uploading the license images for path `{$path}`. Images amount : " . count($images));

        /**
         * @var int $index
         * @var UploadedFile $image
         */
        foreach ($images as $index => $image) {
            usleep(5);
            $imagePath = "{$path}/{$model->id}/" . sha1(microtime()) . ".{$image->getClientOriginalExtension()}";
            $s3Path = $this->saveToS3($image, $imagePath);
            
            /** @var Media $media */
            $media = Media::create([
                'url' => $s3Path,
                'title' => $imagesData['file_title'][$index],
                'author_name' => $imagesData['author_name'][$index],
                'author_url' => $imagesData['author_link'][$index],
                'source_url' => $imagesData['source_link'][$index],
                'license_name' => $imagesData['license_name'][$index],
                'license_url' => $imagesData['license_link'][$index]
            ]);

            if(is_a($model, 'App\Models\City\Cities')) {
                $pivot::create([
                            'cities_id' => $model->id,
                            'medias_id' => $media->id
                        ]);

            }
            elseif(is_a($model, 'App\Models\Country\Countries')) {
                $pivot::create([
                            'countries_id' => $model->id,
                            'medias_id' => $media->id
                        ]);
            }
            elseif(is_a($model, 'App\Models\Place\Place')) {
                $pivot::create([
                            'places_id' => $model->id,
                            'medias_id' => $media->id
                        ]);
            }

        }

        foreach ($imagesData['file_title'] as $i1 => $d1) {
            usleep(5);

            if($i1>0) {
                Media::find($i1)->update(['title'=>$imagesData['file_title'][$i1]]);
            }
        }
        foreach ($imagesData['author_name'] as $i2 => $d2) {
            usleep(5);

            if($i2>0) {
                Media::find($i2)->update(['author_name'=>$imagesData['author_name'][$i2]]);
            }
        }
        foreach ($imagesData['author_link'] as $i3 => $d3) {
            usleep(5);

            if($i3>0) {
                Media::find($i3)->update(['author_url'=>$imagesData['author_link'][$i3]]);
            }
        }
        foreach ($imagesData['source_link'] as $i4 => $d4) {
            usleep(5);

            if($i4>0) {
                Media::find($i4)->update(['source_url'=>$imagesData['source_link'][$i4]]);
            }
        }
        foreach ($imagesData['license_name'] as $i5 => $d5) {
            usleep(5);

            if($i5>0) {
                Media::find($i5)->update(['license_name'=>$imagesData['license_name'][$i5]]);
            }
        }
        foreach ($imagesData['license_link'] as $i6 => $d6) {
            usleep(5);

            if($i6>0) {
                Media::find($i6)->update(['license_url'=>$imagesData['license_link'][$i6]]);
            }
        }
    }

}
