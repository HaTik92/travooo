<?php

namespace App\Repositories\Backend\Hotels;

use App\Models\Hotels\Hotels;
use App\Models\Hotels\HotelsTranslations;
use App\Models\Place\Place;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Models\Hotels\HotelsMedias;
use App\Models\ActivityMedia\Media;
use App\Models\Access\language\Languages;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\Backend\Hotels\TranslateCreatedHotelsTrans;
use App\Jobs\Backend\Hotels\TranslateUpdatedHotelsTrans;

/**
 * Class HotelsRepository.
 */
class HotelsRepository extends BaseRepository
{
    use DispatchesJobs;

    /**
     * Associated Repository Model.
     */
    const MODEL = Hotels::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    /**
     * @param $permissions
     * @param string $by
     *
     * @return mixed
     */
    public function getByPermission($permissions, $by = 'name')
    {
        if (! is_array($permissions)) {
            $permissions = [$permissions];
        }

        return $this->query()->whereHas('roles.permissions', function ($query) use ($permissions, $by) {
            $query->whereIn('permissions.'.$by, $permissions);
        })->get();
    }

    /**
     * @param $roles
     * @param string $by
     *
     * @return mixed
     */
    public function getByRole($roles, $by = 'name')
    {
        if (! is_array($roles)) {
            $roles = [$roles];
        }

        return $this->query()->whereHas('roles', function ($query) use ($roles, $by) {
            $query->whereIn('roles.'.$by, $roles);
        })->get();
    }

    /**
     * @return mixed
     */
    public function getForDataTable($request)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        foreach ($request->columns as $column) {
            if ($column['data'] == 'address') {
                $matchValue     = @$column['search']['value1']['matchValue'];
                $notMatchValue  = @$column['search']['value2']['notMatchValue'];
            }
            if ($column['data'] == 'pluscode') {
                $matchValue2     = @$column['search']['value1']['matchValue2'];
                $notMatchValue2  = @$column['search']['value2']['notMatchValue2'];
            }
        }

        $dataTableQuery = $this->query()
            ->select([
                config('hotels.hotels_table').'.id',
                config('hotels.hotels_table').'.lat',
                config('hotels.hotels_table').'.lng',
                config('hotels.hotels_table').'.cities_id',
                config('hotels.hotels_table').'.countries_id',
                config('hotels.hotels_table').'.active',
                config('hotels.hotels_table').'.place_type',
                config('hotels.hotels_table').'.pluscode',
                'transsingle.title',
                'transsingle.address',
                'cities_trans.title AS city_title',
                'countries_trans.title AS country_title'
            ])
            ->leftJoin(DB::raw('hotels_trans AS transsingle FORCE INDEX (hotels_id)'), function ($query) {
                $query->on('transsingle.hotels_id', '=', 'hotels.id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->leftJoin('cities_trans', function($query) {
                $query->on('cities_trans.cities_id', 'hotels.cities_id')
                ->where('cities_trans.languages_id', 1);
            })
            ->leftJoin('countries_trans', function($query) {
                $query->on('countries_trans.countries_id', 'hotels.countries_id')
                ->where('countries_trans.languages_id', 1);
            })
            ->when($matchValue, function ($query) use ($matchValue) {
                $this->filtered = true;
                return $query->where('transsingle.address', 'LIKE', "%" . $matchValue . "%");
            })
            ->when($notMatchValue, function ($query) use ($notMatchValue) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue);
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue) {
                        $query->where('transsingle.address', 'NOT LIKE', "%" . $notMatchValue . "%");
                    }
                });
            })
            ->when($matchValue2, function ($query) use ($matchValue2) {
                $this->filtered = true;
                return $query->where(config('hotels.hotels_table').'.pluscode', 'LIKE', "%" . $matchValue2 . "%");
            })
            ->when($notMatchValue2, function ($query) use ($notMatchValue2) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue2) ?: [];
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue2) {
                        $query->where(config('hotels.hotels_table').'.pluscode', 'NOT LIKE', "%" . $notMatchValue2 . "%");
                    }
                });
            });


        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $translations
     * @param $extra
     */
    public function create($translations , $extra)
    {
        $model = new Hotels;
        $model->countries_id  = $extra['country_id'];
        $model->cities_id   = $extra['city_id'];
        $model->places_id   = $extra['place_id'];
        $model->place_type  = Place::find($extra['place_id'])->place_type;
        $model->active      = $extra['active'];
        $model->lat         = $extra['lat'];
        $model->lng         = $extra['lng'];
        
        DB::transaction(function () use ($model, $translations, $extra) {
            $check = 1;
            
            if ($model->save()) {

                $this->uploadLicensedImagesToS3(
                    $extra['license_images'],
                    $model,
                    new HotelsMedias(),
                    'hotels_id',
                    'hotels'
                );

                foreach ($translations as $translation) {
                    $translation['hotels_id']   = $model->id;
                    $language                   = Languages::find($translation['languages_id']);
                    if ($language->code == 'en') {
                        $defaultTranslation = $translation;
                        if (!HotelsTranslations::insert($translation)) {
                            $check = 0;
                        }
                    } else {
                        HotelsTranslations::insert($translation);
//                        $this->dispatch(
//                            (new TranslateCreatedHotelsTrans($defaultTranslation, $translation, $language->code))
//                                ->onQueue('translate')
//                        );
                    }

                }
                if ($check) {
                    return true;
                }
            }
            throw new GeneralException('Unexpected Error Occured!');
        });
    }

    /**
     * @param $model
     * @param $translations
     * @param $extra
     */
    public function update($model , $translations , $extra)
    {
        $model->countries_id  = $extra['country_id'];
        $model->cities_id     = $extra['city_id'];
        $model->places_id     = $extra['place_id'];
        $model->active        = $extra['active'];
        $model->lat           = $extra['lat'];
        $model->lng           = $extra['lng'];

        DB::transaction(function () use ($model, $translations, $extra) {
            $check = 1;
            
            if ($model->save()) {
                if (!empty($extra['license_images']['deleted'])) {
                    foreach ($extra['license_images']['deleted'] as $media_id) {
                        $media = Media::find($media_id);
                        unset($extra['license_images']['images'][$media_id]);
                        $this->deleteFromS3($media->url);
                        HotelsMedias::where(['hotels_id' => $model->id, 'medias_id' => $media_id])->delete();
                        $media->delete();
                    }
                }

                foreach ($extra['license_images']['images'] as $key => $image) {
                    if ( $key > 0 ) {
                        Media::find($key)->update($image);
                        unset($extra['license_images']['images'][$key]);
                    }
                }

                $this->uploadLicensedImagesToS3(
                    $extra['license_images']['images'],
                    $model,
                    new HotelsMedias(),
                    'hotels_id',
                    'hotels'
                );

                foreach ($translations as $translation) {
                    $language = Languages::find($translation['languages_id']);
//                    unset($translation['languages_id']);
                    if ($language->code == 'en') {
                        $defaultTranslation = $translation;
                        if (!HotelsTranslations::find($translation['id'])->update($translation)) {
                            $check = 0;
                        }
                    } else {

                        HotelsTranslations::find($translation['id'])->update($translation);

//                        if (null == $translation->id) {
//                            unset($translation['id']);
//                            $translation['hotels_id'] = $model->id;
//                            $translation['languages_id'] = $language->id;
//                            $translation = HotelsTranslations::create($translation)->toArray();
//                            unset($translation['hotels_id']);
//                            unset($translation['languages_id']);
//                        }
//                        $this->dispatch(
//                            (new TranslateUpdatedHotelsTrans($defaultTranslation, $translation, $language->code))
//                                ->onQueue('translate')
//                        );
                    }
                }

                if($check){
                    return true;
                }
            }
            throw new GeneralException('Unexpected Error Occured!');
        });
    }

    public static function validateUpload($extension) {
        $extension = strtolower($extension);

        switch($extension){
            case 'jpeg':
                return true;
            case 'jpg':
                return true;
                break;
            case 'png':
                return true;
                break;
            case 'gif':
                return true;
                break;
            default:
                return false;
        }
    }
}
