<?php

namespace App\Repositories\Backend\TopPlaces;

use App\Models\PlacesTop\PlacesTop;
use App\Repositories\BaseRepository;


/**
 * Class TopPlaceRepository.
 */
class TopPlaceRepository extends BaseRepository
{

    /**
     * Associated Repository Model.
     */
    const MODEL = PlacesTop::class;

    public function __construct()
    {
    }

    /**
     * @param $extra
     */
    public function create($extra)
    {
        $check = 1;

        if(!PlacesTop::create($extra)) {
            $check = 0;
        }

        if($check){
            return true;
        }
    }

    /**
     * @param $extra
     */
    public function update($id, $extra)
    {

        $top_place = PlacesTop::findOrFail($id);
        $check = 1;

        $top_place->order_by_city = $extra['order_by_city'];

        if(!$top_place->save()) {
            $check = 0;
        }

        if($check){
            return true;
        }
    }

    public function getPlacesById($query, $city_id, $place_type = null){
        $places = $this->_doElSearch($query, null, null, $place_type, $city_id, null, 50);

        return $places['result'];
    }

    protected function _doElSearch($query, $lat = null, $lng = null, $place_type = null, $city_id = null, $country = null, $size = 100)
    {
        $curl = curl_init();
        //$query = urlencode($query);
        $geo_distance = '';
        $el_data = false;

        if (!is_null($place_type) and !is_null($city_id)) {

            $el_data = array(
                'query' => array(
                    'bool' => array(
                        'must' => array(
                            'match' => array(
                                'place_type' => $place_type
                            )
                        ),
                        'filter' => array(
                            'term' => array(
                                'cities_id' => $city_id
                            )
                        )
                    )

                ),
                'size' => $size,
                'sort' => array(
                    'top' => array(
                        'order' => 'desc'
                    ),
                    'rating.keyword' => array(
                        'order' => 'desc'
                    ),
                    '_score' => array(
                        'order' => 'desc'
                    )
                )
            );
        } elseif (is_null($place_type) and !is_null($city_id)) {

            $el_data = array(
                'query' => array(
                    'bool' => array(
                        'must' => array(
                            'match' => array(
                                'title' => array(
                                    'query' => $query,
                                    'operator' => 'and'
                                )
                            )
                        ),
                        'filter' => array(
                            'term' => array(
                                'cities_id' => $city_id
                            )
                        )
                    )

                ),
                'size' => $size,
                'sort' => array(
                    'top' => array(
                        'order' => 'desc'
                    ),
                    'rating.keyword' => array(
                        'order' => 'desc'
                    ),
                    '_score' => array(
                        'order' => 'desc'
                    )
                )
            );
        } elseif (!is_null($place_type) and is_null($city_id)) {
            //dd($place_type);

            $el_data = array(
                'query' => array(
                    'bool' => array(
                        'must' => array(
                            'match' => array(
                                'place_type' => $place_type
                            )
                        )
                    )

                ),
                'size' => $size,
                'sort' => array(

                    '_geo_distance' => array(
                        'location' => array(
                            'lat' => $lat,
                            'lon' => $lng
                        ),
                        'order' => 'asc',
                        'unit' => 'km'
                    ),
                    'rating.keyword' => array(
                        'order' => 'desc'
                    ),
                    '_score' => array(
                        'order' => 'desc'
                    )
                )
            );
        } elseif (!is_null($query)) {
            $el_data = array(
                'query' => array(
                    'bool' => array(
                        'should' => array(
                            'match' => array(
                                'title' => array(
                                    'query' => $query,
                                    'operator' => 'and'
                                )
                            )
                        )
                    )
                ),
                'size' => $size,
                'sort' => array(

                    'top' => array(
                        'order' => 'desc'
                    ),
                    'rating.keyword' => array(
                        'order' => 'desc'
                    ),

                    '_score' => array(
                        'order' => 'desc'
                    )
                )
            );
        }

        // if lat & lng are passed, order results by distance
        if (!is_null($lat) and !is_null($lng)) {
            $el_data['sort'] = array(


                '_geo_distance' => array(
                    'location' => array(
                        'lat' => $lat,
                        'lon' => $lng
                    ),
                    'order' => 'asc',
                    'unit' => 'km'
                ),
                'top' => array(
                    'order' => 'desc'
                ),
                'rating.keyword' => array(
                    'order' => 'desc'
                ),

                '_score' => array(
                    'order' => 'desc'
                )
            );
        }


        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/pois/_search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => json_encode($el_data),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json;charset=utf-8',
                'Connection: Keep-Alive'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        //dd($response);
        curl_close($curl);
        $response_array = [];
        if ($err) {
            return [];
        } else {
            $response = json_decode($response, true);
            //dd($response);
            if (isset($response['hits']) && count($response['hits']['hits']) != 0) {
                foreach ($response['hits']['hits'] as $hits) {
                    $response_array[] = $hits['_source'];
                }
                return [
                    'result' => $response_array,
                    'total' => $response['hits']['total']
                ];
            } else {
                return [
                    'result' => [],
                    'total' => 0
                ];
            }
        }
    }


}
