<?php

namespace App\Repositories\Backend\Events;

use App\Models\ActivityMedia\Media;
use App\Models\Events\EventMedia;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Models\Events\Events;
use App\Repositories\BaseRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;

class EventsRepository extends BaseRepository
{
    use DispatchesJobs;

    /**
     * Associated Repository Model.
     */
    const MODEL = Events::class;

    /**
     * @param $translations
     * @param $extra
     */
    public function create($translations, $extra) {

        $model = new Events();
        $model->fill($extra);

        DB::transaction(function () use ($model, $translations, $extra) {

            if ($model->save()) {
                // model saved
                $this->uploadLicensedImagesToS3(
                    $extra['license_images'],
                    $model,
                    new EventMedia(),
                    'event_id',
                    'events'
                );
                if (!empty($extra['medias'])) {
                    foreach ($extra['medias'] as $key => $value) {
                        EventMedia::create(['event_id' => $model->id, 'medias_id' => $value]);
                    }
                }
                return true;
            }
            throw new GeneralException('Unexpected Error Occured!');
        });
    }

    /**
     * @return mixed
     */
    public function getForDataTable($request)
    {
        foreach ($request->columns as $column) {
            if ($column['data'] == 'title') {
                $searchTitle = $column['search']['value'];
            }
        }
        $dataTableQuery = $this->query()
            ->when($searchTitle, function ($query) use ($searchTitle) {
                return $query->where(config('events_model.events_table').'.title', 'LIKE', "%" . $searchTitle . "%");
            })
            ->whereNull(config('events_model.events_table').'.provider_id')
            ->select([
                config('events_model.events_table').'.title',
                config('events_model.events_table').'.id'
            ]);
        return $dataTableQuery;
    }

    /**
     * @return mixed
     */
    public function getForEventAPIDataTable($request)
    {
        foreach ($request->columns as $column) {
            if ($column['data'] == 'title') {
                $searchTitle = $column['search']['value'];
            }
        }
        $dataTableQuery = $this->query()
            ->when($searchTitle, function ($query) use ($searchTitle) {
                return $query->where(config('events_model.events_table').'.title', 'LIKE', "%" . $searchTitle . "%");
            })
            ->whereNotNull(config('events_model.events_table').'.provider_id')
            ->select([
                config('events_model.events_table').'.title',
                config('events_model.events_table').'.id'
            ]);
        return $dataTableQuery;
    }
    /**
     * @param $id
     * @param $translations
     * @param $extra
     */
    public function update($id, $translations, $extra) {
        $model =  Events::find($id);

        $model->title = $extra['title'];
        $model->category = $extra['category'];
        $model->description = $extra['description'];
        $model->start = $extra['start'];
        $model->end = $extra['end'];
        $model->countries_id = $extra['location_country_id'];
        $model->city_id = $extra['city_id'];
        $model->lat = $extra['lat'];
        $model->lng = $extra['lng'];

        DB::transaction(function () use ($model, $extra) {
            $check = 1;
            if ($model->save()) {
                if (!empty($extra['license_images']['deleted'])) {
                    foreach ($extra['license_images']['deleted'] as $media_id) {
                        $media = Media::find($media_id);
                        unset($extra['license_images']['images'][$media_id]);
                        $this->deleteFromS3($media->url);
                        EventMedia::where(['event_id' => $model->id, 'medias_id' => $media_id])->delete();
                        $media->delete();
                    }
                }

                foreach ($extra['license_images']['images'] as $key => $image) {
                    if ($key > 100) {
                        $media = Media::find($key);
                        $media->update($image);
                        unset($extra['license_images']['images'][$key]);
                    }
                }
                $this->uploadLicensedImagesToS3(
                    $extra['license_images']['images'],
                    $model,
                    new EventMedia(),
                    'event_id',
                    'events'
                );
                if ($check) {
                    return true;
                }
            }
        });
    }
}