<?php
namespace App\Repositories\Backend\CopyrightInfringement;

use App\Mail\PlainTextEmail;
use App\Models\ActivityLog\ActivityLog;
use App\Models\ActivityMedia\Media;
use App\Models\ActivityMedia\MediascommentsMedias;
use App\Models\CopyrightInfringements\CopyrightInfringement;
use App\Models\CopyrightInfringements\CopyrightInfringementBlacklist;
use App\Models\Events\EventMedia;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsMedia;
use App\Models\Reports\ReportsInfos;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlans\TripPlans;
use App\Models\User\User;
use App\Models\User\UsersMedias;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class CopyrightInfringementRepository
{
    const MODEL = CopyrightInfringement::class;

    public function index($itBlacklistPage)
    {
        return view('backend.copyright_infringement.index', ['blacklist' => $itBlacklistPage]);
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table($request)
    {
        $orderData = current($request->order);
        $orderColumn = $request->columnName($orderData['column']);
        $orderDir = $orderData['dir'];
        $blocklist = $request->blacklist;
        session()->put('admin_can_visit_private_pages', true);
        $query = (self::MODEL)::with('user')->where(function($query) use ($blocklist) {
            if ($blocklist) {
                return $query->has('blacklist', '>=', 10);
            } else {
                return $query->has('blacklist', '<', 10);
            }
        });

        $response['recordsTotal'] = count($query->get());
        $response['recordsFiltered'] = $response['recordsTotal'];
        $copyrights = $query->offset($request->start)->limit($request->length)->orderBy($orderColumn, $orderDir)->get();
        foreach ($copyrights as $copyright) {
            $copyright->infringement_location =  trans('labels.backend.copyright_infringement.' . $copyright->infringement_location );
            $copyright->approvedInfractions = (self::MODEL)::onlyTrashed()->where('deleted_at', '>', Carbon::now()->subMonths(12))->where('owner_id', $copyright->owner_id)->count();
            $copyright->forms = (self::MODEL)::where('owner_id', $copyright->owner_id)->pluck('id')->toArray();
            $copyright->original_link = json_encode(json_decode($copyrights->first()->original_link));
        }
        $response['data'] = $copyrights;

        return response()->json($response);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function store($data){
        $reportedUrls = array_unique($data['reported_url']);
        foreach ($reportedUrls as $key => $reportedUrl) {
            $locations = $data['infringement_location'][$key];
            $infringementDescription = $data['infringement_description'][$key];
            $copyright = self::MODEL;
            $copyright = new $copyright;
            $url = $reportedUrl;
            $userReports = $this->getUserReporteForUrl($reportedUrl, $data['sender_email'], $locations );

            if($userReports) {
                continue;
            }

            if ($locations === 'profile_image') {
                $ownerId = (int)$reportedUrl;
                $url = url('profile/' . $ownerId);
            } else {
                $copyrightData = $this->getCopyrightData($reportedUrl, $locations);
                $ownerId = $copyrightData['user_id'] ?: '';
            }

            $copyright->owner_id = $ownerId;
            $copyright->sender_type = $data['sender_type'];
            $copyright->owner_name = $data['owner_name'];
            $copyright->sender_name = $data['sender_name'];
            $copyright->company = $data['company'];
            $copyright->job_title = $data['job_title'];
            $copyright->sender_email = $data['sender_email'];
            $copyright->street = $data['street'];
            $copyright->state = $data['state'];
            $copyright->postal_code = $data['postal_code'];
            $copyright->country = $data['country'];
            $copyright->city = $data['city'];
            $copyright->signature = $data['signature'];
            $copyright->phone = strlen($data['phone']) > 1 ? $data['phone'] : null;
            $copyright->fax = strlen($data['fax']) > 1 ? $data['fax'] : null;
            $copyright->original_link = json_encode($data['original_link']);
            $copyright->description = json_encode($data['description']);
            $copyright->infringement_location = $locations;
            $copyright->reported_url = $url;
            $copyright->infringement_description = $infringementDescription;
            $copyright->authority_act = $data['authority_act'];
            $copyright->save();
        }

        return redirect()->back()->withFlashSuccess('Create successfully.');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirm($id)
    {
        $userId = '';
        try {
            $copyright = (self::MODEL)::where('id', $id)->first();
            $locations = $copyright->infringement_location;
            $description = $copyright->infringement_description;
            $reportedUrl = $copyright->reported_url;
            $copyrightData = $this->getCopyrightData($reportedUrl, $locations);
            foreach ($copyrightData['model'] as $model ) {
                if (!empty($model)) {
                    if ($locations === 'profile_image') {
                        if($description === 'profile_picture') {
                            $model->profile_picture = null;
                            $model->save();
                        } elseif ($description === 'cover_image') {
                            $model->cover_photo = null;
                            $model->save();
                        }
                    } elseif ($locations === 'other') {
                        $amazonefilename = explode("https://s3.amazonaws.com/travooo-images2/", $reportedUrl);
                        Storage::disk('s3')->delete($amazonefilename[1]);
                    } else {
                        $model->delete();
                    }
                }
            }

            foreach ($copyrightData['models'] as $models ) {
                foreach ($models as $model) {
                    if (!empty($model)) {
                        $model->delete();
                    }
                }
            }

            $userId = $copyrightData['user_id'];
            $notes = $reportedUrl . " was removed due to a copyright claim by " . $copyright->sender_name;
            if ($userId !== '') {
                $copyright->owner_id = $userId;
                $copyright->save();
                notify($userId, 'copyright_policy', $copyright->id, $notes);
            }
            $message = 'Your request  is accepted and we have removed the copyright infringement on' . $copyright->original_link;

            $this->deleteSimilarInfringement($copyright->reported_url, $message);
            $acceptCount = (self::MODEL)::onlyTrashed()->where('owner_id', $userId)->where('created_at', '>', Carbon::now()->subMonths(12))->count();

            if ($acceptCount > 5) {
                block_user($userId);
            }
        } catch (\Exception $e) {
            return response()->json([
                'data' => [
                    'success' => false,
                    'message' => $e->getMessage(),
                ]
            ]);
        }
        return response()->json([
            'data' => [
                'success' => true,
                'message' => 'Copyright infringement request confirmed successfully.',
            ]
        ]);
    }

    /**
     * @param $id
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function show($id)
    {
        $copyright = (self::MODEL)::withTrashed()->where('id', $id)->first();
        $copyright->approvedInfractions = (self::MODEL)::onlyTrashed()->where('deleted_at', '>', Carbon::now()->subMonths(12))->where('owner_id', $copyright->owner_id)->count();
        return view('backend.copyright_infringement.view')->withCopyright($copyright);
    }


    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function reply($data)
    {
        try {
            $copyright = (self::MODEL)::where('id', $data['id'])->first();
            $to = $copyright->sender_email;
            //create message session for email file
            session(['copyright_message' => $data['message']]);
            // send email
            Mail::to($to)->send(new PlainTextEmail());
            // delete message session
            session()->forget('copyright_message');
            return response()->json([
                'data' => [
                    'success' => true,
                    'message' => 'Reply successfully.',
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'data' => [
                    'success' => false,
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        try {
            $copyright = (self::MODEL)::where('id', $id)->first();
            $message = 'Your request  is refused as we haven\'t encountered any copyright infringements on&nbsp;&nbsp;'
                . '<a href="' . $copyright->reported_url . '">'. $copyright->reported_url . '</a>';

            $this->addToBlacklist($copyright);

            $this->deleteSimilarInfringement($copyright->reported_url, $message);
            return response()->json([
                'data' => [
                    'success' => true,
                    'message' => 'Copyright infringement request deleted successfully.',
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'data' => [
                    'success' => false,
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    /**
     * @param $id
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getCopyrightPolicy($id)
    {
        $copyright = (self::MODEL)::withTrashed()->where('id', $id)->first();
        $senderName = null;
        $link = '#';
        if ($copyright) {
            $senderName = $copyright->sender_name;
            $link = $copyright->reported_url;
        }
        return view('site.home.partials.copyright_policy',['sender_name' => $senderName, 'link' => $link ]);
    }

    /**
 * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
 */
    public function getSuspendedAccounts()
    {
        return view('backend.copyright_infringement.users');
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSuspendedAccountsTable($request)
    {
        $orderData = current($request->order);
        $suspendedUsers = [];
        $orderColumn = $request->columnName($orderData['column']);
        $orderDir = $orderData['dir'];
        $query = User::whereHas('copyrightInfringement', function ($q) {
            $q->onlyTrashed();
        })->with('copyrightInfringement', 'user_blocks');
        $users = $query->offset($request->start)->limit($request->length)->orderBy($orderColumn, $orderDir)->get();
        foreach ($users as $user) {
            if ($user->copyrightInfringement->count() > 5) {
                $suspendedUsers[] = $user;
            }
        }
        $response['recordsTotal'] = count($suspendedUsers);
        $response['recordsFiltered'] = $response['recordsTotal'];
        $response['data'] = $suspendedUsers;
        return response()->json($response);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restorePost($id)
    {
        $copyright = (self::MODEL)::withTrashed()->where('id', $id)->first();
        if ($copyright ) {
            $locations = $copyright->infringement_location;
            $description = $copyright->infringement_description;
            $reportedUrl = $copyright->reported_url;
            $copyrightData = $this->getCopyrightData($reportedUrl, $locations);
            foreach ($copyrightData['model'] as $model ) {
                if ($locations === 'profile_image' || $locations === 'other' ) {
                    continue;
                } else {
                    $model->delete_at = null;
                    $model->save();
                }
            }

            foreach ($copyrightData['models'] as $models ) {
                foreach ($models as $model) {
                    $model->delete_at = null;
                    $model->save();
                }
            }

            $copyright->deleted_at = null;
            $copyright->save();

            return response()->json([
                'data' => [
                    'success' => true,
                    'message' => 'Copyright infringement post restored successfully.',
                ]
            ]);
        }
        return response()->json([
            'data' => [
                'success' => false,
                'message' => 'Copyright infringement does not exist',
            ]
        ]);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getHistory()
    {
        return view('backend.copyright_infringement.history');
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHistoryTable($request)
    {
        $orderData = current($request->order);
        $orderColumn = $request->columnName($orderData['column']);
        $orderDir = $orderData['dir'];
        $query = (self::MODEL)::whereDoesntHave('deleted_copyright')->onlyTrashed();
        $response['recordsTotal'] = count($query->get());
        $response['recordsFiltered'] = $response['recordsTotal'];
        $copyrights = $query->offset($request->start)->limit($request->length)->orderBy($orderColumn, $orderDir)->get();
        foreach ($copyrights as $copyright) {
            $copyright->infringement_location = trans('labels.backend.copyright_infringement.' . $copyright->infringement_location);
        }
        $response['data'] = $copyrights;
        return response()->json($response);
    }

    /**
     * @param $reportedUrl
     * @param $description
     * @param $locations
     * @return array
     */
    private function getCopyrightData($reportedUrl, $locations)
    {
        $userId = '';
        $model = [];
        $models = [];
        switch ($locations) {
            case 'post':
                if (strpos($reportedUrl, 's3.amazonaws.com') !== false) {
                    $link = $reportedUrl;
                    $postMedia = PostsMedia::with('media')->whereHas('media', function ($q) use ($link) {
                        $q->where('url', 'LIKE', "%$link%");
                    })->first();
                }
                if (isset($postMedia)) {
                    $models[] = $postMedia->media();
                    $model[] = $postMedia;
                    $post = Posts::where('id', $postMedia->posts_id)->first();
                    if ($post) {
                        $userId = $post->users_id;
                    }
                } else {
                    $media = Media::where('url', 'LIKE', "%$link%")->first();
                    if ($media) {
                        $models[] = UsersMedias::where('medias_id', $media->id)->get();
                        $activitylog = ActivityLog::where('type', 'Media')
                            ->where('variable', $media->id)->first();
                        if ($activitylog) {
                            $model[] = $activitylog;
                            $userId = $activitylog->users_id;
                        }
                        $model[] = $media;
                    }
                }
                break;
            case 'post_discussion':
                $media = null;
                if (strpos($reportedUrl, 's3.amazonaws.com') !== false) {
                    $link = $reportedUrl;
                    $mediasComments = MediascommentsMedias::with('media.users')->whereHas('media', function ($q) use ($link) {
                        $q->where('url', 'LIKE', "%$link%");
                    })->first();
                }
                if (isset($mediasComments)) {
                    $models[] = $mediasComments->media();
                    $model[] = $mediasComments;
                    foreach ($mediasComments->media->users() as $user) {
                        $userId = $user->id;
                    }
                } else {

                }
                break;
            case 'profile_image':
                if (strpos($reportedUrl, 's3.amazonaws.com') !== false) {
                    $user = User::where('profile_picture', 'LIKE', "%$reportedUrl%")
                        ->orWhere('cover_photo', 'LIKE', "%$reportedUrl%")->first();
                } else {
                    $link = explode('/', $reportedUrl);
                    $user = User::where('id', (int)end($link))->first();
                }

                if ($user) {
                    $userId = $user->id;
                    $model[] = $user;
                }
                break;
            case 'event':
                if (strpos($reportedUrl, 's3.amazonaws.com') !== false) {
                    $media = substr($reportedUrl, stripos($reportedUrl, 'events/'));
//                        Storage::disk('s3')->delete($media);
                    $medias = Media::with('users')->where('url', 'LIKE', "%$media%")->first();
                    if ($media) {
                        $models[] = EventMedia::where('medias_id', $medias->id)->get();
                        $model[] = $medias;
                    }
                }
                break;
            case 'travelog':
                if (strpos($reportedUrl, 's3.amazonaws.com') !== false) {
                    $reportInfo = ReportsInfos::with('report')->where('val', 'LIKE', "%$reportedUrl%")->first();
//                    $amazonefilename = explode("https://s3.amazonaws.com/travooo-images2/", $reportedUrl);
//                        Storage::disk('s3')->delete($amazonefilename[1]);
                    if ($reportInfo) {
                        if ($reportInfo->report) {
                            $userId = $reportInfo->report->users_id;
                        }
                        $model[] = $reportInfo;
                    }
                } elseif (strpos($reportedUrl, '/reports/') !== false) {
                    $link = explode('/', $reportedUrl);
                    $reportInfo = ReportsInfos::where('reports_id', (int)end($link))->first();
                    if ($reportInfo) {
                        if ($reportInfo->report) {
                            $userId = $reportInfo->report->users_id;
                        }
                        $reports = ReportsInfos::where('reports_id', (int)end($link))->get();
                        $models[] = $reports;
                    }
                }
                break;
            case 'trip_plan':
                if (strpos($reportedUrl, 'trip/plan') !== false) {
                    $link = explode('/', $reportedUrl);
                    $tripMedias = TripMedias::with('media')->where('trips_id', (int)end($link))->get();
                    if (!empty($tripMedias)) {
                        $tripPlan = TripPlans::where('id', $link)->first();
                        $userId = $tripPlan ? $tripPlan->users_id : '';
                        foreach ($tripMedias as $tripMedia) {
                            $models[] = $tripMedia->media();
                            $model[] = $tripMedia;
                        }
                    }

                } elseif (strpos($reportedUrl, 's3.amazonaws.com/travooo-images2/trips/') !== false || strpos($reportedUrl, 's3.amazonaws.com') !== false) {
                    $media = substr($reportedUrl, stripos($reportedUrl, '/trips/'));
                    $tripMedia = TripMedias::with(['media', 'trip.author'])->whereHas('media', function ($query) use ($media) {
                        $query->where('url', 'LIKE', "%$media%");
                    })->first();

                    if ($tripMedia) {
                        $models[] = Media::whereIn('id', $tripMedia)->get();
                        $tripPlans = $tripMedia->trip();
                        foreach ($tripPlans->author() as $author ) {
                            $userId = $author->id;
                        }
                        $models[] = $tripMedia->media();
                        $model[] = $tripMedia;
                    }
                }
                break;
            case 'other':
                break;
            default:
                break;
        }

        return [
            'user_id' => $userId,
            'model' => $model,
            'models' => $models,
        ];
    }

    /**
     * @param $copyright
     */
    private function addToBlacklist($copyright)
    {
        $model = new CopyrightInfringementBlacklist();
        $model->copyright_infringement_id = $copyright->id;
        $model->admin_id = Auth::user()->id;
        $model->sender_email = $copyright->sender_email;
        $model->sender_name = $copyright->sender_name;
        $model->save();
    }

    /**
     * @param $reportUrl
     * @param $message
     */
    private function deleteSimilarInfringement($reportedUrl, $message) {
        $copyrightInfringements = (self::MODEL)::where('reported_url', 'like', '%' . $reportedUrl . '%')->get();
        foreach ($copyrightInfringements as $copyrightInfringement) {
            session(['copyright_message' => $message]);
            // send email
            Mail::to($copyrightInfringement->sender_email)->send(new PlainTextEmail());
            // delete message session
            session()->forget('copyright_message');
            $copyrightInfringement->delete();
        }
    }

    /**
     * @param $reportedUrl
     * @param $senderEmail
     * @param $locations
     * @return mixed
     */
    private function getUserReporteForUrl($reportedUrl, $senderEmail, $locations )
    {
        $copyrightInfringements = (self::MODEL)::where('reported_url', 'like', '%' . $reportedUrl . '%')
        ->where('sender_email', $senderEmail)
        ->where('infringement_location', $locations)->first();
        return $copyrightInfringements;
    }
}