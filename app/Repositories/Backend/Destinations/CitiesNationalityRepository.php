<?php

namespace App\Repositories\Backend\Destinations;


use App\Exceptions\GeneralException;
use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\Destinations\CitiesByNationality;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;


/**
 * Class CountriesNationalityRepository.
 */
class CitiesNationalityRepository extends BaseRepository
{
    use CreateUpdateStatisticTrait;
    /**
     * Associated Repository Model.
     */
    const MODEL = CitiesByNationality::class;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function findByNationalityId($id)
    {
        return $this->query()->where('nationality_id', $id)->get();
    }



    /**
     * @param int $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
//            ->withTrashed()
            ->select([
                'top_cities_by_nationality.nationality_id',
                'transsingle.title',
                DB::raw("(GROUP_CONCAT(trans.title SEPARATOR ', ')) as `cities`")
            ])
            ->leftJoin('countries_trans AS transsingle', function ($query) {
                $query->on('transsingle.countries_id', '=', 'top_cities_by_nationality.nationality_id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->leftJoin('cities_trans AS trans', function ($query) {
                $query->on('trans.cities_id', '=', 'top_cities_by_nationality.cities_id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->groupBy('top_cities_by_nationality.nationality_id');

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $extra
     */
    public function create( $extra)
    {
        $check = 1;


        foreach($extra['cities_id'] as $citiy_id){
            $model = new CitiesByNationality;

            $model->nationality_id = $extra['nationality_id'];
            $model->cities_id = $citiy_id;

            if(!$model->save()) {
                $check = 0;
            }
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }


    /**
     * @param $id
     * @param $extra
     */
    public function update($extra)
    {
        CitiesByNationality::where('nationality_id',  $extra['nationality_id'])->delete();

        return $this->create($extra);
    }

}
