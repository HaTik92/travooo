<?php

namespace App\Repositories\Backend\Destinations;


use App\Exceptions\GeneralException;
use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\Destinations\GenericTopPlaces;
use App\Repositories\BaseRepository;


/**
 * Class GenericTopPlacesRepository.
 */
class GenericTopPlacesRepository extends BaseRepository
{
    use CreateUpdateStatisticTrait;
    /**
     * Associated Repository Model.
     */
    const MODEL = GenericTopPlaces::class;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function findById($id)
    {
        return $this::find($id);
    }

    /**
     * @param int $place_id
     *
     * @return mixed
     */
    public function findByPlaceId($place_id)
    {
        return $this->query()->where('places_id', $place_id)->first();
    }


    /**
     * @param int $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
//            ->withTrashed()
            ->select([
                'generic_top_places.id',
                'transsingle.title',
                'cities_trans.title as cities',
                'places_trans.title as places'
            ])
            ->leftJoin('countries_trans AS transsingle', function ($query) {
                $query->on('transsingle.countries_id', '=', 'generic_top_places.countries_id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->leftJoin('cities_trans', function ($query) {
                $query->on('cities_trans.cities_id', '=', 'generic_top_places.cities_id')
                    ->where('cities_trans.languages_id', '=', 1);
            })
            ->leftJoin('places_trans', function ($query) {
                $query->on('places_trans.places_id', '=', 'generic_top_places.places_id')
                    ->where('places_trans.languages_id', '=', 1);
            });

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $extra
     */
    public function create( $extra)
    {
        $check = 1;

        $model = new GenericTopPlaces;

        $model->cities_id = $extra['city_id'];
        $model->countries_id = $extra['country_id'];
        $model->places_id = $extra['place_id'];

        if(!$model->save()) {
            $check = 0;
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }


    /**
     * @param $id
     * @param $extra
     */
    public function update($id, $extra)
    {
        $check = 1;
        $model = GenericTopPlaces::where('id',  $id)->update([
            'cities_id' => $extra['city_id'],
            'countries_id' => $extra['country_id'],
            'places_id' => $extra['place_id'],
        ]);

        if(!$model) {
            $check = 0;
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $check = 1;
        $model = GenericTopPlaces::where('id',  $id)->delete();

        if(!$model) {
            $check = 0;
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }


}
