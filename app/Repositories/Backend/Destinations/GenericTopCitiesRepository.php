<?php

namespace App\Repositories\Backend\Destinations;


use App\Exceptions\GeneralException;
use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\Destinations\GenericTopCities;
use App\Repositories\BaseRepository;


/**
 * Class GenericTopCitiesRepository.
 */
class GenericTopCitiesRepository extends BaseRepository
{
    use CreateUpdateStatisticTrait;
    /**
     * Associated Repository Model.
     */
    const MODEL = GenericTopCities::class;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function findById($id)
    {
        return $this::find($id);
    }

    /**
     * @param int $city_id
     *
     * @return mixed
     */
    public function findByCityId($city_id)
    {
        return $this->query()->where('cities_id', $city_id)->first();
    }


    /**
     * @param int $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
//            ->withTrashed()
            ->select([
                'generic_top_cities.id',
                'transsingle.title',
                'cities_trans.title as cities',
            ])
            ->leftJoin('countries_trans AS transsingle', function ($query) {
                $query->on('transsingle.countries_id', '=', 'generic_top_cities.countries_id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->leftJoin('cities_trans', function ($query) {
                $query->on('cities_trans.cities_id', '=', 'generic_top_cities.cities_id')
                    ->where('cities_trans.languages_id', '=', 1);
            });

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $extra
     */
    public function create( $extra)
    {
        $check = 1;

        $model = new GenericTopCities;

        $model->cities_id = $extra['city_id'];
        $model->countries_id = $extra['country_id'];

        if(!$model->save()) {
            $check = 0;
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }


    /**
     * @param $id
     * @param $extra
     */
    public function update($id, $extra)
    {
        $check = 1;
        $model = GenericTopCities::where('id',  $id)->update(['cities_id' => $extra['city_id'], 'countries_id' => $extra['country_id']]);

        if(!$model) {
            $check = 0;
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $check = 1;
        $model = GenericTopCities::where('id',  $id)->delete();

        if(!$model) {
            $check = 0;
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }


}
