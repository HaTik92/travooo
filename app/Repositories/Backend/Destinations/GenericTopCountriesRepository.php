<?php

namespace App\Repositories\Backend\Destinations;


use App\Exceptions\GeneralException;
use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\Destinations\GenericTopCountries;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;


/**
 * Class GenericTopCountriesRepository.
 */
class GenericTopCountriesRepository extends BaseRepository
{
    use CreateUpdateStatisticTrait;
    /**
     * Associated Repository Model.
     */
    const MODEL = GenericTopCountries::class;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function findById($id)
    {
        return $this::find($id);
    }

    /**
     * @param int $country_id
     *
     * @return mixed
     */
    public function findByCountryId($country_id)
    {
        return $this->query()->where('countries_id', $country_id)->first();
    }


    /**
     * @param int $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
//            ->withTrashed()
            ->select([
                'generic_top_countries.id',
                'transsingle.title',
            ])
            ->leftJoin('countries_trans AS transsingle', function ($query) {
                $query->on('transsingle.countries_id', '=', 'generic_top_countries.countries_id')
                    ->where('transsingle.languages_id', '=', 1);
            });

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $extra
     */
    public function create( $extra)
    {
        $check = 1;

        $model = new GenericTopCountries;

        $model->countries_id = $extra['country_id'];

        if(!$model->save()) {
            $check = 0;
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }


    /**
     * @param $id
     * @param $extra
     */
    public function update($id, $extra)
    {
        $check = 1;
        $model = GenericTopCountries::where('id',  $id)->update(['countries_id'=> $extra]);

        if(!$model) {
            $check = 0;
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $check = 1;
        $model = GenericTopCountries::where('id',  $id)->delete();

        if(!$model) {
            $check = 0;
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }


}
