<?php

namespace App\Repositories\Backend\Destinations;


use App\Exceptions\GeneralException;
use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\Destinations\CountriesByNationality;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;


/**
 * Class CountriesNationalityRepository.
 */
class CountriesNationalityRepository extends BaseRepository
{
    use CreateUpdateStatisticTrait;
    /**
     * Associated Repository Model.
     */
    const MODEL = CountriesByNationality::class;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function findByNationalityId($id)
    {
        return $this->query()->where('nationality_id', $id)->get();
    }



    /**
     * @param int $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
//            ->withTrashed()
            ->select([
                'top_countries_by_nationality.nationality_id',
                'transsingle.title',
                DB::raw("(GROUP_CONCAT(trans.title SEPARATOR ', ')) as `countries`")
            ])
            ->leftJoin('countries_trans AS transsingle', function ($query) {
                $query->on('transsingle.countries_id', '=', 'top_countries_by_nationality.nationality_id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->leftJoin('countries_trans AS trans', function ($query) {
                $query->on('trans.countries_id', '=', 'top_countries_by_nationality.countries_id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->groupBy('top_countries_by_nationality.nationality_id');

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $extra
     */
    public function create( $extra)
    {
        $check = 1;


        foreach($extra['countries_id'] as $country_id){
            $model = new CountriesByNationality;

            $model->nationality_id = $extra['nationality_id'];
            $model->countries_id = $country_id;

            if(!$model->save()) {
                $check = 0;
            }
        }

        if($check){
            return true;
        }

        throw new GeneralException('Unexpected Error Occured!');
    }


    /**
     * @param $id
     * @param $extra
     */
    public function update($extra)
    {
        CountriesByNationality::where('nationality_id',  $extra['nationality_id'])->delete();

        return $this->create($extra);
    }

}
