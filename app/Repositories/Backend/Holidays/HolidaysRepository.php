<?php

namespace App\Repositories\Backend\Holidays;

use App\Models\Holidays\Holidays;
use App\Models\Holidays\HolidaysTranslations;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Models\Holidays\HolidaysMedias;
use App\Models\ActivityMedia\Media;

/**
 * Class HolidaysRepository.
 */
class HolidaysRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Holidays::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    /**
     * @param        $permissions
     * @param string $by
     *
     * @return mixed
     */
    public function getByPermission($permissions, $by = 'name')
    {
        if (! is_array($permissions)) {
            $permissions = [$permissions];
        }

        return $this->query()->whereHas('roles.permissions', function ($query) use ($permissions, $by) {
            $query->whereIn('permissions.'.$by, $permissions);
        })->get();
    }

    /**
     * @param        $roles
     * @param string $by
     *
     * @return mixed
     */
    public function getByRole($roles, $by = 'name')
    {
        if (! is_array($roles)) {
            $roles = [$roles];
        }

        return $this->query()->whereHas('roles', function ($query) use ($roles, $by) {
            $query->whereIn('roles.'.$by, $roles);
        })->get();
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->with('transsingle')
            ->select([
                config('holidays.holidays_table').'.id'
            ]);

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param array $input
     */
    public function create($input, $image)
    {
        $model = new Holidays;
        
        DB::transaction(function () use ($model, $input, $image) {
            $check = 1;
            
            if ($model->save()) {
                foreach ($input as $key => $value) {
                    $trans = new HolidaysTranslations;
                    $trans->holidays_id  = $model->id;
                    $trans->languages_id = $key;
                    $trans->title        = $value['title_'.$key];
                    $trans->slug         = $value['slug_'.$key];
                    $trans->description  = $value['description_'.$key];

                    if(!$trans->save()) {
                        $check = 0;
                    }
                }

                $this->uploadLicensedImagesToS3(
//                $this->saveLicensedImagesToS3(
                    $image,
                    $model,
                    new HolidaysMedias(),
                    'holidays_id',
                    'holidays'
                );

                if($check){
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }


    /**
     * @param $id
     * @param $model
     * @param $input
     */
    public function update($id , $model , $input, $image)
    {
        $prev = HolidaysTranslations::where(['holidays_id' => $id])->get();
        if(!empty($prev)){
            foreach ($prev as $key => $value) {
                $value->delete();
            }
        }

        DB::transaction(function () use ($model, $input, $image) {
            $check = 1;
            
            if ($model->save()) {
                foreach ($input as $key => $value) {
                    $trans = new HolidaysTranslations;
                    $trans->holidays_id  = $model->id;
                    $trans->languages_id = $key;
                    $trans->title        = $value['title_'.$key];
                    $trans->slug         = $value['slug_'.$key];
                    $trans->description  = $value['description_'.$key];

                    if(!$trans->save()) {
                        $check = 0;
                    }
                }

                if (!empty($image['license_images']['deleted'])) {
                    $media = Media::find($image['license_images']['deleted'])->first();
                    $this->deleteFromS3($media->url);
                    HolidaysMedias::where(['holidays_id' => $model->id, 'medias_id' => $image['license_images']['deleted']])->delete();
                    $media->delete();
                }

                $hol_media = HolidaysMedias::where(['holidays_id' => $model->id])->first();
                if(empty($image['license_images']['deleted']) && isset($hol_media->medias_id)){
                    foreach ($image['license_images']['image'] as $key => $image) {
                        $media = Media::find($key);
                        $media->update($image);
                    }
                }

                if (!empty($image['license_images']['image'])) {

                    $this->uploadLicensedImagesToS3(
                        $image['license_images']['image'],
                        $model,
                        new HolidaysMedias(),
                        'holidays_id',
                        'holidays'
                    );
                }

                if($check){
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }
}
