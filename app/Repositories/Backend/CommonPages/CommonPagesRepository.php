<?php

namespace App\Repositories\Backend\CommonPages;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Models\CommonPage\CommonPage;
use App\Models\CommonPage\CommonPageTranslations;
use App\Repositories\BaseRepository;

/**
 * Class CommonPagesRepository.
 */
class CommonPagesRepository extends BaseRepository
{
    const MODEL = CommonPage::class;

    /**
     * @param $id
     * @param $model
     * @param $input
     * @param $extra
     */
    public function update($id, $model, $input, $extra)
    {
        $model->active  = $extra['active'];

        /* Delete Previous Translations Data */
        $prev = CommonPageTranslations::where(['common_page_id' => $id])->get();
        if (!empty($prev)) {
            foreach ($prev as $key => $value) {
                $value->delete();
            }
        }

        DB::transaction(function () use ($model, $input, $extra) {
            $check = 1;

            if ($model->save()) {
                CommonPage::where('id', $model->id)->update(['active' => $model->active]);

                /* Update Page Translations Data*/
                foreach ($input as $key => $value) {
                    $trans = new CommonPageTranslations;
                    $trans->common_page_id = $model->id;
                    $trans->languages_id = $key;
                    $trans->title        = $value['title_' . $key];
                    $trans->description  = $value['description_' . $key];

                    if (!$trans->save()) {
                        $check = 0;
                    }
                }

                if ($check) {
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }
}
