<?php

namespace App\Repositories\Backend\Place;

use App\Models\Place\Place;
use App\Models\Place\PlaceTranslations;
use App\Models\Place\Traits\PlaceRelationship;
use App\Models\PlacesTop\PlacesTop;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Models\Place\PlaceMedias;

use App\Models\ActivityMedia\Media;
use App\Models\Access\language\Languages;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\Backend\Place\TranslateCreatedPlacesTrans;
use App\Jobs\Backend\Place\TranslateUpdatedPlacesTrans;
use Carbon\Carbon;

/**
 * Class PlaceRepository.
 */
class PlaceRepository extends BaseRepository
{
    use DispatchesJobs;

    /**
     * Associated Repository Model.
     */
    const MODEL = Place::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    /**
     * @param        $permissions
     * @param string $by
     *
     * @return mixed
     */
    public function getByPermission($permissions, $by = 'name')
    {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }

        return $this->query()->whereHas('roles.permissions', function ($query) use ($permissions, $by) {
            $query->whereIn('permissions.' . $by, $permissions);
        })->get();
    }

    /**
     * @param        $roles
     * @param string $by
     *
     * @return mixed
     */
    public function getByRole($roles, $by = 'name')
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }

        return $this->query()->whereHas('roles', function ($query) use ($roles, $by) {
            $query->whereIn('roles.' . $by, $roles);
        })->get();
    }

    /**
     * @param int $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($media = null)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        if ($media == '3') {
            $dataTableQuery = $this->query()
                ->select([
                    config('locations.place_table') . '.id',
                    DB::raw('MAX(' . config('locations.place_table') . '.cities_id)'),
                    DB::raw('MAX(' . config('locations.place_table') . '.countries_id)'),
                    DB::raw('MAX(' . config('locations.place_table') . '.place_type) as place_id_title'),
                    DB::raw('MAX(' . config('locations.place_table') . '.media_done)'),
                    DB::raw('MAX(' . config('locations.place_table') . '.active) as active'),
                    DB::raw('MAX(' . config('locations.place_table') . '.media_count) as media_count'),
                    DB::raw('MAX(transsingle.title) as title'),
                    DB::raw('MAX(transsingle.address) as address'),
                    DB::raw('MAX(cities_trans.title) as city_title'),
                    DB::raw('MAX(countries_trans.title) as country_title')
                ])
                /*replace relation to join for make good search performance*/
                /*get places title and address*/


                    // force index for a way better performance
                ->leftJoin(DB::raw('places_trans AS transsingle FORCE INDEX (places_id)'), function ($query) {
                    $query->on('transsingle.places_id', '=', 'places.id')
                        ->where('transsingle.languages_id', '=', 1);
                })
                ->leftJoin('cities_trans', function ($query) {
                    $query->on('cities_trans.cities_id', '=', 'places.cities_id')
                        ->where('cities_trans.languages_id', '=', 1);
                })
                ->Leftjoin('countries_trans', function ($query) {
                    $query->on('countries_trans.countries_id', '=', 'places.countries_id')
                        ->where('countries_trans.languages_id', '=', 1);
                })
                ->groupBy('places.id');
        } else {
            $dataTableQuery = $this->query()
                ->with('transsingle')
                ->with('city')
                ->with('country')
                ->select([
                    config('locations.place_table') . '.id',
                    config('locations.place_table') . '.cities_id',
                    config('locations.place_table') . '.countries_id',
                    config('locations.place_table') . '.place_type',
                    config('locations.place_table') . '.media_done',
                    config('locations.place_table') . '.active'
                ]);
            $dataTableQuery = $dataTableQuery->where(['media_done' => $media]);
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $translations
     * @param $extra
     */
    public function create($translations, $extra)
    {
        $model = new Place;
        $model->countries_id = $extra['countries_id'];
        $model->cities_id = $extra['cities_id'];
        $model->active = $extra['active'];
        $model->lat = $extra['lat'];
        $model->lng = $extra['lng'];
        $model->provider_id = \Hash::make(Carbon::now());

        DB::transaction(function () use ($model, $translations, $extra) {
            $check = 1;

            if ($model->save()) {

                $this->uploadLicensedImagesToS3(
                    $extra['license_images'],
                    $model,
                    new PlaceMedias(),
                    'places_id',
                    'place'
                );

                foreach ($translations as $translation) {
                    $translation['places_id']   = $model->id;
                    $language                   = Languages::find($translation['languages_id']);
                    if ($language->code == 'en') {
                        $defaultTranslation = $translation;
                        if (!PlaceTranslations::create($translation)) {
                            $check = 0;
                        }
                    } else {
                        $this->dispatch(
                            (new TranslateCreatedPlacesTrans($defaultTranslation, $translation, $language->code))
                                ->onQueue('translate')
                        );
                    }

                }
                if ($check) {
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }


    /**
     * @param $id
     * @param $model
     * @param $translations
     * @param $extra
     */
    public function update($id, $model, $translations, $extra)
    {
        $model = Place::findOrFail($id);
        $model->countries_id = $extra['countries_id'];
        $model->cities_id = $extra['cities_id'];
        //$model->place_type_ids  = $extra['place_types_ids'];
        $model->active = $extra['active'];
        $model->lat = $extra['lat'];
        $model->lng = $extra['lng'];
        $model->auto_import = 0;

        DB::transaction(function () use ($model, $translations, $extra) {
            $check = 1;

            if ($model->save()) {

                if (!empty($extra['license_images']['deleted'])) {
                    foreach ($extra['license_images']['deleted'] as $media_id) {
                        $media = Media::find($media_id);
                        unset($extra['license_images']['images'][$media_id]);
                        $this->deleteFromS3($media->url);
                        PlaceMedias::where(['places_id' => $model->id, 'medias_id' => $media_id])->delete();
                        $media->delete();
                    }
                }

                
                foreach ($extra['license_images']['images'] as $key => $image) {
                    if ( $key > 0 && !is_null($image)) {
                        $image['featured'] = @intval($image['featured']);
                        Media::find($key)->update($image);
                        unset($extra['license_images']['images'][$key]);
                    }
                }

                $this->uploadLicensedImagesToS3(
                    $extra['license_images']['images'],
                    $model,
                    new PlaceMedias(),
                    'places_id',
                    'places'
                );

                foreach ($translations as $translation) {
                    $language = Languages::find($translation['languages_id']);
                    unset($translation['languages_id']);
                    if ($language->code == 'en') {
                        $defaultTranslation = $translation;
                        if (!PlaceTranslations::find($translation['id'])->update($translation)) {
                            $check = 0;
                        }
                    } else {
                        if (null == $translation->id) {
                            unset($translation['id']);
                            $translation['places_id'] = $model->id;
                            $translation['languages_id'] = $language->id;
                            $translation = PlaceTranslations::updateOrCreate(['places_id' => $model->id, 'languages_id' => $language->id],$translation)->toArray();
                            unset($translation['places_id']);
                            unset($translation['languages_id']);
                        }
                        $this->dispatch(
                            (new TranslateUpdatedPlacesTrans($defaultTranslation, $translation, $language->code))
                                ->onQueue('translate')
                        );
                    }

                    $place_title = $translation['title'];
                }

                $topPlaceExists = PlacesTop::where('places_id',$model->id)->first();
                if(is_object($topPlaceExists)){
                    $data = [
                        'city_id'=>$model->cities_id,
                        'country_id'=>$model->countries_id,
                        'title'=>$place_title,
                    ];
                    PlacesTop::whereId($topPlaceExists->id)->update($data);
                }

                if ($check) {
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }

    public static function validateUpload($extension)
    {
        $extension = strtolower($extension);

        switch ($extension) {
            case 'jpeg':
                return true;
            case 'jpg':
                return true;
                break;
            case 'png':
                return true;
                break;
            case 'gif':
                return true;
                break;
            default:
                return false;
        }
    }
}
