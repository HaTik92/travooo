<?php

namespace App\Repositories\Backend\Access\User;

use App\Models\Access\User\User;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsPlaces\ExpertsPlaces;
use App\Models\ExpertsTravelStyles\ExpertsTravelStyles;
use App\Models\User\ApiUser;
use App\Models\User\UsersBlocks;
use App\Models\User\UsersFavourites;
use App\Models\User\UsersTravelStyles;
use App\Services\Experts\ExpertsService;
use App\Services\Ranking\RankingService;
use App\Services\Users\ExpertiseService;
use App\Services\Users\FavouritesService;
use App\Services\Users\TravelStylesService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Events\Backend\Access\User\UserCreated;
use App\Events\Backend\Access\User\UserDeleted;
use App\Events\Backend\Access\User\UserUpdated;
use App\Events\Backend\Access\User\UserRestored;
use App\Events\Backend\Access\User\UserDeactivated;
use App\Events\Backend\Access\User\UserReactivated;
use App\Events\Backend\Access\User\UserPasswordChanged;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Events\Backend\Access\User\UserPermanentlyDeleted;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = User::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    /**
     * @param        $permissions
     * @param string $by
     *
     * @return mixed
     */
    public function getByPermission($permissions, $by = 'name')
    {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }

        return $this->query()->whereHas('roles.permissions', function ($query) use ($permissions, $by) {
            $query->whereIn('permissions.' . $by, $permissions);
        })->get();
    }

    /**
     * @param        $roles
     * @param string $by
     *
     * @return mixed
     */
    public function getByRole($roles, $by = 'name')
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }

        return $this->query()->whereHas('roles', function ($query) use ($roles, $by) {
            $query->whereIn('roles.' . $by, $roles);
        })->get();
    }

    /**
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false, $nonConfirmed = false, $suspended = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        $dataTableQuery = $this->query()
            ->with('roles')
            ->withTrashed()
            ->select([
                config('access.users_table') . '.id',
                config('access.users_table') . '.name',
                config('access.users_table') . '.email',
                config('access.users_table') . '.status',
                config('access.users_table') . '.username',
                config('access.users_table') . '.confirmed',
                config('access.users_table') . '.created_at',
                config('access.users_table') . '.updated_at',
                config('access.users_table') . '.deleted_at',
            ]);

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        if ($suspended == 'true') {
            $users = User::WhereHas('copyrightInfringement', function ($q) {
                $q->onlyTrashed();
            })->with('copyrightInfringement');

            $userIds = [];

            $users->each(function ($user) use (&$userIds) {
                if ($user->copyrightInfringement->count() > 5) {
                    $userIds[] = $user->id;
                }
            });

            return $dataTableQuery->where('status', ApiUser::STATUS_DEACTIVE)
                ->orWhereIn('id', $userIds);
        }

        if ($nonConfirmed == 'true') {
            return $dataTableQuery->where('confirmed', 0);
        }

        if ($status !== 'false') {
            return $dataTableQuery;
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    /**
     * @param array $input
     */
    public function create($input)
    {
        $data = $input['data'];
        $roles = $input['roles'];
        $relations = $input['relations'];

        $user = $this->createUserStub($data);

        DB::transaction(function () use ($user, $data, $roles, $relations) {

            if ($user->save()) {

                //                User Created, Validate Roles
                if (isset($roles['assignees_roles']) && !count($roles['assignees_roles'])) {
                    throw new GeneralException(trans('exceptions.backend.access.users.role_needed_create'));
                }

                //Attach new roles
                if (isset($roles['assignees_roles'])) {
                    $user->attachRoles($roles['assignees_roles']);
                }

                //Send confirmation email if requested
                if (isset($data['confirmation_email']) && $user->confirmed == 0) {
                    $user->notify(new UserNeedsConfirmation($user->confirmation_code));
                }

                event(new UserCreated($user));

                $this->setRelations($user, $relations);

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
        });
    }

    /**
     * @param Model $user
     * @param array $input
     *
     * @return bool
     * @throws GeneralException
     */
    public function update(Model $user, array $input)
    {
        $data = $input['data'];
        $roles = $input['roles'];
        $relations = $input['relations'];

        $this->checkUserByEmail($data, $user);

        $user->name              = $data['name'];
        $user->email             = $data['email'];
        $user->address           = $data['address'];
        $user->single            = $data['single'];
        $user->gender            = $data['gender'];
        $user->children          = $data['children'];
        $user->birth_date        = $data['birth_date'];
        $user->public_profile    = $data['public_profile'];
        $user->mobile            = $data['mobile'];
        $user->nationality       = $data['nationality'];
        $user->notifications     = $data['notifications'];
        $user->messages          = $data['messages'];
        $user->username          = $data['username'];
        $user->status            = $data['status'];
        $user->confirmed         = $data['confirmed'];
        $user->sms               = $data['sms'];
        $user->type              = $data['type'];
        $user->website           = $data['website'];
        $user->twitter           = $data['twitter'];
        $user->facebook          = $data['facebook'];
        $user->instagram         = $data['instagram'];
        $user->pinterest         = $data['pinterest'];
        $user->tumblr            = $data['tumblr'];
        $user->youtube           = $data['youtube'];
        $user->interests         = $data['interests'];

        if ($data['profile_picture']) {
            $user->profile_picture = $data['profile_picture'];
        }

        DB::transaction(function () use ($user, $data, $roles, $relations) {
            if ($user->save()) {
                $this->checkUserRolesCount($roles);
                $this->flushRoles($roles, $user);
                $this->setRelations($user, $relations);

                event(new UserUpdated($user));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
        });
    }

    /**
     * @param \App\Models\User\User $user
     * @param array $relations
     */
    public function setRelations($user, $relations)
    {
        $places = [];
        $cities = [];
        $countries = [];

        foreach ($relations['places'] as $place) {
            $place = explode('-', $place);
            $places[] = $place[1];
        }

        foreach ($relations['cities_countries'] as $location) {
            $pieces = explode('-', $location);

            if ($pieces[0] === 'city') {
                $cities[] = $pieces[1];
            } else {
                $countries[] = $pieces[1];
            }
        }

        if ($user->type == \App\Models\User\User::TYPE_REGULAR) {
            /** @var FavouritesService $favouritesService */
            $favouritesService = app(FavouritesService::class);
            /** @var TravelStylesService $travelStyleService */
            $travelStyleService = app(TravelStylesService::class);

            $favouritesService->deleteFavourites($user->id, 'city');
            $favouritesService->deleteFavourites($user->id, 'country');
            $favouritesService->deleteFavourites($user->id, 'place');
            $favouritesService->setFavouritesCities($user->id, $cities);
            $favouritesService->setFavouritesCountries($user->id, $countries);
            $favouritesService->setFavouritesPlaces($user->id, $places);

            $travelStyleService->deleteTravelStyles($user->id);
            $travelStyleService->setTravelStyles($user->id, [$relations['travel_type']]);
        } else if ($user->type == \App\Models\User\User::TYPE_EXPERT || $user->type == \App\Models\User\User::TYPE_INVITED_EXPERT) {
            /** @var ExpertiseService $expertiseService */
            $expertiseService = app(ExpertiseService::class);

            /** @var RankingService $rankingService */
            $rankingService = app(RankingService::class);

            $expertiseService->deleteExpertiseCities($user->id);
            $expertiseService->deleteExpertiseCountries($user->id);
            $expertiseService->deleteExpertisePlaces($user->id);
            $expertiseService->setExpertiseCities($user->id, $cities);
            $expertiseService->setExpertiseCountries($user->id, $countries);
            $expertiseService->setExpertisePlaces($user->id, $places);

            $expertiseService->deleteExpertiseTravelStyles($user->id);
            $expertiseService->setExpertiseTravelStyles($user->id, [$relations['travel_type']]);
            $rankingService->changeBadge($user->id, $relations['badge']);
        }
    }

    /**
     * @param Model $user
     * @param $input
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function updatePassword(Model $user, $input)
    {
        $user->password = bcrypt($input['password']);

        if ($user->save()) {
            event(new UserPasswordChanged($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.update_password_error'));
    }

    /**
     * @param Model $user
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Model $user)
    {
        if (access()->id() == $user->id) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
        }

        if ($user->id == 1) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_admin'));
        }

        // We will use travoooDeleteUserAccount($user->id) in future for delete user aacount
        if ($user->delete()) {
            event(new UserDeleted($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

    /**
     * @param Model $user
     *
     * @throws GeneralException
     */
    public function forceDelete(Model $user)
    {
        if (is_null($user->deleted_at)) {
            throw new GeneralException(trans('exceptions.backend.access.users.delete_first'));
        }

        DB::transaction(function () use ($user) {
            if ($user->forceDelete()) {
                event(new UserPermanentlyDeleted($user));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
        });
    }

    /**
     * @param Model $user
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function restore(Model $user)
    {
        if (is_null($user->deleted_at)) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_restore'));
        }

        if ($user->restore()) {
            event(new UserRestored($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

    /**
     * @param Model $user
     * @param $status
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function mark(Model $user, $status)
    {
        if (access()->id() == $user->id && $status == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_deactivate_self'));
        }

        $user->status = $status;

        switch ($status) {
            case 0:
                event(new UserDeactivated($user));
                break;

            case 1:
                event(new UserReactivated($user));
                break;
        }

        if ($user->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param  $input
     * @param  $user
     *
     * @throws GeneralException
     */
    protected function checkUserByEmail($input, $user)
    {
        //Figure out if email is not the same
        if ($user->email != $input['email']) {
            //Check to see if email exists
            if ($this->query()->where('email', '=', $input['email'])->first()) {
                throw new GeneralException(trans('exceptions.backend.access.users.email_error'));
            }
        }
    }

    /**
     * @param $roles
     * @param $user
     */
    protected function flushRoles($roles, $user)
    {
        //Flush roles out, then add array of new ones
        $user->detachRoles($user->roles->toArray());
        $user->attachRoles($roles['assignees_roles']);
    }

    /**
     * @param  $roles
     *
     * @throws GeneralException
     */
    protected function checkUserRolesCount($roles)
    {
        //User Updated, Update Roles
        //Validate that there's at least one role chosen
        if (isset($roles['assignees_roles']) && count($roles['assignees_roles']) == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.role_needed'));
        }
    }

    /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createUserStub($input)
    {
        $user = self::MODEL;
        $user = new $user;
        $user->name              = $input['name'];
        $user->email             = $input['email'];
        $user->address           = $input['address'];
        $user->single            = $input['single'];
        $user->gender            = $input['gender'];
        $user->children          = $input['children'];
        $user->birth_date        = $input['birth_date'];
        $user->public_profile    = $input['public_profile'];
        $user->mobile            = $input['mobile'];
        $user->nationality       = $input['nationality'];
        $user->profile_picture   = isset($input['profile_picture']) ? $input['profile_picture'] : "";
        $user->notifications     = $input['notifications'];
        $user->messages          = $input['messages'];
        $user->username          = $input['username'];
        $user->password          = bcrypt($input['password']);
        $user->status            = $input['status'];
        $user->confirmation_code = md5(uniqid(mt_rand(), true));
        $user->confirmed         = $input['confirmed'];
        $user->sms               = $input['sms'];
        $user->type              = $input['type'];
        $user->website           = $input['website'];
        $user->twitter           = $input['twitter'];
        $user->facebook          = $input['facebook'];
        $user->instagram         = $input['instagram'];
        $user->pinterest         = $input['pinterest'];
        $user->tumblr            = $input['tumblr'];
        $user->youtube           = $input['youtube'];
        $user->interests         = $input['interests'];
        $user->is_approved       = $input['is_approved'];
        $user->register_steps       = $input['register_steps'];

        return $user;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function block($id)
    {
        block_user((int)$id);
        return response()->json([
            'data' => [
                'success' => true,
                'message' => 'User blocked successfully.',
            ]
        ]);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function unBlockUser($id)
    {
        UsersBlocks::where('users_id', $id)->delete();
        return response()->json([
            'data' => [
                'success' => true,
                'message' => 'User unblocked successfully.',
            ]
        ]);
    }
}
