<?php

namespace App\Repositories\Backend\Access\Expert;

use App\Models\Ranking\RankingBadges;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository.
 */
class BadgesRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = RankingBadges::class;

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query();
    }
}
