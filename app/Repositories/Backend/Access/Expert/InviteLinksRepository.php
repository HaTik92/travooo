<?php

namespace App\Repositories\Backend\Access\Expert;

use App\Models\InviteExpertLinks\InviteExpertLink;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository.
 */
class InviteLinksRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = InviteExpertLink::class;

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()->with('badge');
    }
}
