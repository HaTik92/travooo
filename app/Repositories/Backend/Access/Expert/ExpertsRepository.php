<?php

namespace App\Repositories\Backend\Access\Expert;

use App\Models\Ranking\RankingBadges;
use App\Models\Ranking\RankingUsersBadges;
use App\Models\User\User;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository.
 */
class ExpertsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = User::class;

    /**
     * @param int $status
     * @param string $approved
     * @param int $invited
     * @param bool $trashed
     *
     * @param array $filters
     * @return mixed
     */
    public function getForDataTable($status = 1, $approved = '', $invited = 0, $trashed = false, $filters = [])
    {
        $type = User::TYPE_EXPERT;

        if ($invited === '1' || (isset($filters['invited']) && $filters['invited'])) {
            $type = User::TYPE_INVITED_EXPERT;
        }

        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()->where('type', $type)
            ->withTrashed()
            ->with([
                'roles',
                'inviteExpertLink' => function ($query) {
                    $query->select(['id', 'name', 'code', 'points']);
                },
                'points' => function ($query) {
                    $query->select(['points', 'users_id']);
                },
                'disapproveMessage',
                'badge', 'badge.badge',
                'expertCountries', 'expertCountries.countries.trans',
                'expertCities', 'expertCities.cities.trans',
                'expertPlaces', 'expertPlaces.places.trans',
                'expertTravelStyles', 'expertTravelStyles.travelStyles.trans',
                'get_followers' => function ($query) {
                    $query->selectRaw('users_id, COUNT(*) as count');
                },
            ])
            ->select([
                config('access.users_table').'.id',
                config('access.users_table').'.name',
                config('access.users_table').'.email',
                config('access.users_table').'.confirmed',
                config('access.users_table').'.website',
                config('access.users_table').'.twitter',
                config('access.users_table').'.facebook',
                config('access.users_table').'.instagram',
                config('access.users_table').'.youtube',
                config('access.users_table').'.pinterest',
                config('access.users_table').'.tumblr',
                config('access.users_table').'.created_at',
                config('access.users_table').'.updated_at',
                config('access.users_table').'.deleted_at',
                config('access.users_table').'.invite_expert_link_id',
                config('access.users_table').'.interests',
                config('access.users_table').'.approved_at',
                config('access.users_table').'.is_approved',
                config('access.users_table').'.disapprove_reason',
                config('access.users_table').'.status',
                config('access.users_table').'.type'
            ]);

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        switch ($approved) {
            case "any":
                break;
            case "pending":
                $dataTableQuery = $dataTableQuery->approved(null);
                break;
            case "special":
                $dataTableQuery = $dataTableQuery->approvedType(1);
                break;
            case "1":
                $dataTableQuery = $dataTableQuery->approved(1);
                break;
            case "0":
                $dataTableQuery = $dataTableQuery->approved(0);
                break;
        }

        foreach ($filters as $filter => $val) {
            switch ($filter) {
                case 'badge':
                    if ($val) {
                        $dataTableQuery->whereHas('badge', function($q) use ($val) {
                            $q->where('badges_id', $val);
                        });
                    } elseif ($val === '0') {
                        $dataTableQuery->whereDoesntHave('badge');
                    }
                    break;
                case 'joined':
                    if ($val) {
                        $dataTableQuery->active(1);
                    } elseif ($val === '0') {
                        $dataTableQuery->active(0);
                    }
                    break;
                case 'applied':
                    if ($val) {
                        $dataTableQuery->where('apply_to_badge', $val);
                    }
                    break;
                case 'next-badge':
                    if ($val) {
                        $badge = RankingBadges::query()->find($val);
                        if (!$badge) {
                            break;
                        }

                        $prevBadge = RankingBadges::query()->where('order', '<', $badge->order)->orderByDesc('order')->first();

                        if ($prevBadge) {
                            $expertsIds = RankingUsersBadges::query()->where('badges_id', $prevBadge->id)->pluck('users_id');
                        } else {
                            $expertsIds = User::query()
                                ->where('expert', 1)
                                ->where('is_approved',1)
                                ->whereDoesntHave('badge')
                                ->pluck('id');
                        }

                        $dataTableQuery->whereIn('id', $expertsIds);
                    }
                    break;
                default:
                    break;
            }
        }

        // active() is a scope on the UserScope trait
        if ($status !== 'any') {
            $dataTableQuery->active($status);
        }

        return $dataTableQuery;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getExpertById($id)
    {
        return (self::MODEL)::find((int)$id);
    }
}
