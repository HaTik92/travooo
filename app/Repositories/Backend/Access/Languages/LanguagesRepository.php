<?php

namespace App\Repositories\Backend\Access\Languages;

use App\Models\Access\language\Languages;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\Backend\Access\Languages\Create\TranslationLevel1\LanguagesCreateTransManager;
use App\Jobs\Backend\Access\Languages\Update\TranslationLevel1\LanguagesUpdateTransManager;

/**
 * Class LanguageRepository.
 */
class LanguagesRepository extends BaseRepository
{
    use DispatchesJobs;

    /**
     * Associated Repository Model.
     */
    const MODEL = Languages::class;

    /**
     * @param $status
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            // ->with('roles')
            ->select([
                config('access.language_table').'.id',
                config('access.language_table').'.title',
                config('access.language_table').'.code',
                config('access.language_table').'.active'
            ]);

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param array $input
     * @return bool
     */
    public function create($input)
    {
        $this->checkUserByCode($input);

        $language = new Languages;
        $language->title  = $input['title'];
        $language->code   = $input['code'];
        $language->active = ($input['active'])? 1 : 2 ;
        if ($language->save()) {
            $this->dispatch(
                (new LanguagesCreateTransManager($language->ToArray()))
                    ->onQueue('translateLevel1')
            );
        }
        return true;
    }

    /**
     * @param Model $language
     * @param array $input
     * @return bool
     */
    public function update(Model $language, array $input)
    {
        $translate = false;
        DB::transaction(function () use ($language, $input) {
            if ($language->code != $input['code']) {
                $translate = true;
            }
            if ($language->update($input)) {
                if ($translate) {
                    $this->dispatch(
                        (new LanguagesUpdateTransManager($language->ToArray()))
                            ->onQueue('translateLevel1')
                    );
                }
                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.language.update_error'));
        });
    }

    /**
     * @param  $input
     *
     * @throws GeneralException
     */
    protected function checkUserByCode($input)
    {
        //Check to see if Code exists
        if ($this->query()->where('code', '=', $input['code'])->first()) {
            throw new GeneralException(trans('exceptions.backend.access.language.code_error'));
        }
    }
}
