<?php

namespace App\Repositories\Backend\City;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\City\Cities;
use App\Models\City\CitiesTranslations;
use App\Models\City\CitiesCurrencies;
use App\Models\City\CitiesEmergencyNumbers;
use App\Models\City\CitiesHolidays;
use App\Models\City\CitiesLanguagesSpoken;
use App\Models\City\CitiesAdditionalLanguages;
use App\Models\City\CitiesLifestyles;
use App\Models\City\CitiesMedias;
use App\Models\City\CitiesReligions;
use App\Models\City\CitiesAbouts;
use App\Models\Country\Countries;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;

use App\Models\ActivityMedia\Media;
use App\Models\Access\language\Languages;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\Backend\City\TranslateCreatedCitiestTrans;
use App\Jobs\Backend\City\TranslateUpdatedCitiestTrans;
use App\Services\Medias\LocationsAddMediaService;

/**
 * Class CityRepository.
 */
class CityRepository extends BaseRepository
{
    use CreateUpdateStatisticTrait;
    use DispatchesJobs;

    /**
     * Associated Repository Model.
     */
    const MODEL = Cities::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @var LocationsAddMediaService
     */
    protected $locationsAddMediaService;


    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role, LocationsAddMediaService $locationsAddMediaService)
    {
        $this->role = $role;
        $this->locationsAddMediaService = $locationsAddMediaService;
    }

    /**
     * @param        $permissions
     * @param string $by
     *
     * @return mixed
     */
    public function getByPermission($permissions, $by = 'name')
    {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }

        return $this->query()->whereHas('roles.permissions', function ($query) use ($permissions, $by) {
            $query->whereIn('permissions.' . $by, $permissions);
        })->get();
    }

    /**
     * @param        $roles
     * @param string $by
     *
     * @return mixed
     */
    public function getByRole($roles, $by = 'name')
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }

        return $this->query()->whereHas('roles', function ($query) use ($roles, $by) {
            $query->whereIn('roles.' . $by, $roles);
        })->get();
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            // ->with('roles')
//            ->with('transsingle')
            ->select([
                config('locations.city_table') . '.id',
                config('locations.city_table') . '.code',
                config('locations.city_table') . '.lat',
                config('locations.city_table') . '.lng',
                config('locations.city_table') . '.countries_id',
                config('locations.city_table') . '.active',
                'transsingle.title',
                'countries_trans.title AS country'
            ])
            ->leftJoin('cities_trans AS transsingle', function ($query) {
                $query->on('transsingle.cities_id', '=', 'cities.id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            /*get country title*/
            ->leftJoin('countries AS country', function ($query) {
                $query->on('country.id', '=', 'cities.countries_id');
            })
            ->Leftjoin('countries_trans', function ($query) {
                $query->on('countries_trans.countries_id', '=', 'country.id')
                    ->where('countries_trans.languages_id', '=', 1);
            });

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $translations
     * @param $extra
     */
    public function create($translations, $extra)
    {
        $model = new Cities;
        $model->countries_id = $extra['countries_id'];
        $model->active = $extra['active'];
        $model->is_capital = $extra['is_capital'];
        $model->code = $extra['code'];
        $model->lat = $extra['lat'];
        $model->lng = $extra['lng'];
        // $model->safety_degree_id = $extra['safety_degree_id'];
        $model->level_of_living_id = $extra['level_of_living_id'];
        DB::transaction(function () use ($model, $translations, $extra) {
            $check = 1;

            if ($model->save()) {

                $country = Countries::find($extra['countries_id']);
                $this->updateStatistic($country, 'cities', count($country->cities));
                $this->uploadLicensedImagesToS3(
//                $this->saveLicensedImagesToS3(
                    $extra['license_images'],
                    $model,
                    new CitiesMedias(),
                    'cities_id',
                    'cities'
                );

                $this->updateStatistic($model, 'medias', count($model->medias));

                /* Entry in LanguagesSpoken table */
                if (!empty($extra['languages_spoken'])) {
                    foreach ($extra['languages_spoken'] as $key => $value) {
                        CitiesLanguagesSpoken::create(['cities_id' => $model->id, 'languages_spoken_id' => $value]);
                    }
                }

                /* Entry in AdditionalLanguagesSpoken table */
                if (!empty($extra['additional_languages_spoken'])) {
                    foreach ($extra['additional_languages_spoken'] as $key => $value) {
                        CitiesAdditionalLanguages::create(['cities_id' => $model->id, 'languages_spoken_id' => $value]);
                    }
                }

                /* Entry in Religions table */
                if (!empty($extra['religions'])) {
                    foreach ($extra['religions'] as $key => $value) {
                        CitiesReligions::create(['cities_id' => $model->id, 'religions_id' => $value]);
                    }
                }

                /* Entry in CitiesLifestyles table */
                if (!empty($extra['lifestyles'])) {
                    foreach ($extra['lifestyles'] as $key => $value) {
                        if ($value) {
                            CitiesLifestyles::create(
                                ['cities_id' => $model->id, 'lifestyles_id' => $value, 'rating' => $extra['lifestyles_rating'][$key]]
                            );
                        }
                    }
                }

                /* Entry in EmergencyNumbers table */
                if (!empty($extra['emergency_numbers'])) {
                    foreach ($extra['emergency_numbers'] as $key => $value) {
                        CitiesEmergencyNumbers::create(['cities_id' => $model->id, 'emergency_numbers_id' => $value]);
                    }
                }

                /* Entry in CitiesHolidays table */
//                if (!empty($extra['holidays'])) {
//                    foreach ($extra['holidays'] as $key => $value) {
//                        CitiesHolidays::create(['cities_id' => $model->id, 'holidays_id' => $value]);
//                    }
//                }

                if (!empty($extra['holidays'])) {
                    foreach ($extra['holidays'] as $key => $value) {
                        if(isset($value)){
                            CitiesHolidays::create(['cities_id' => $model->id, 'holidays_id' => $value, 'date' => $extra['holidays_date'][$key]]);
                        }
                    }
                }

                /* Entry in CitiesCurrencies table */
                if (!empty($extra['currencies'])) {
                    foreach ($extra['currencies'] as $key => $value) {
                        CitiesCurrencies::create(['cities_id' => $model->id, 'currencies_id' => $value]);
                    }
                }
                if(isset($extra['best_time'])) {
                    foreach($extra['best_time'] as $best_time) {
                        if(isset($best_time['low_start']) && $best_time['low_start'] != null) {
                            $start = $best_time['low_start'];
                            $end = $best_time['low_end'];
                            $title = 'low_season';
                        } elseif(isset($best_time['shoulder_start']) && $best_time['shoulder_start'] != null) {
                            $start = $best_time['shoulder_start'];
                            $end = $best_time['shoulder_end'];
                            $title = 'shoulder';
                        } elseif(isset($best_time['high_start']) && $best_time['high_start'] != null) {
                            $start = $best_time['high_start'];
                            $end = $best_time['high_end'];
                            $title = 'high_season';
                        }
                        
                        if(isset($start) && $start != 0 && $end != 0) {
                            $city_season = CitiesAbouts::firstOrNew(
                                ['cities_id' => $model->id, 'title' => $title]
                            );
                            $city_season->cities_id     = $model->id;
                            $city_season->type          = 'best_time';
                            $city_season->title         = $title;
                            $city_season->body          = $start.'-'.$end;
                            $city_season->save();
                        }
                    }
                    if (count($extra['best_time']['low_season']['low_start']) == 1) {
                        if($extra['best_time']['low_season']['low_start'][0] == null || $extra['best_time']['low_season']['low_end'][0] == null){
                            CitiesAbouts::where([['cities_id', $model->id], ['title', 'low_season']])->delete();
                        }
                    }
                    if (count($extra['best_time']['shoulder']['shoulder_start']) == 1) {
                        if($extra['best_time']['shoulder']['shoulder_start'][0] == null || $extra['best_time']['shoulder']['shoulder_end'][0] == null){
                            CitiesAbouts::where([['cities_id', $model->id], ['title', 'shoulder']])->delete();
                        }
                    }
                    if (count($extra['best_time']['high_season']['high_start']) == 1) {
                        if($extra['best_time']['high_season']['high_start'][0] == null || $extra['best_time']['high_season']['high_end'][0] == null){
                            CitiesAbouts::where([['cities_id', $model->id], ['title', 'high_season']])->delete();
                        }
                    }
                }
                foreach ($translations as $translation) {
                    $language = Languages::find($translation['languages_id']);

                    $translation['cities_id'] = $model->id;
                    if ($language->code == 'en') {
                        $defaultTranslation = $translation;
                        if (!CitiesTranslations::create($translation)) {
                            $check = 0;
                        }
                    } else {
                        $this->dispatch(
                            (new TranslateCreatedCitiestTrans($defaultTranslation, $translation, $language->code))
                                ->onQueue('translate')
                        );
                    }

                    $this->locationsAddMediaService->addCityMedia($model);
                }
                if ($check) {
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }


    /**
     * @param $id
     * @param $model
     * @param $translations
     * @param $extra
     */
    public function update($id, $model, $translations, $extra)
    {
        $model->countries_id = $extra['countries_id'];
        $model->active = $extra['active'];
        $model->is_capital = $extra['is_capital'];
        $model->code = $extra['code'];
        $model->lat = $extra['lat'];
        $model->lng = $extra['lng'];

        DB::transaction(function () use ($model, $translations, $extra) {

            $check = 1;

            if ($model->save()) {

                if (!empty($extra['license_images']['deleted'])) {
                    foreach ($extra['license_images']['deleted'] as $media_id) {
                        $media = Media::find($media_id);
                        unset($extra['license_images']['images'][$media_id]);
                        $this->deleteFromS3($media->url);
                        CitiesMedias::where(['cities_id' => $model->id, 'medias_id' => $media_id])->delete();
                        $media->delete();
                    }
                }

                foreach ($extra['license_images']['images'] as $key => $image) {
                    if ( $key > 100 ) {
                        $media = Media::find($key);
                        $media->update($image);
                        unset($extra['license_images']['images'][$key]);
                    }
                }

                $this->uploadLicensedImagesToS3(
                    $extra['license_images']['images'],
                    $model,
                    new CitiesMedias(),
                    'cities_id',
                    'cities'
                );

                $this->updateStatistic($model, 'medias', count($model->medias));

                if(is_array($extra['religions'])) {
                    foreach ($extra['religions'] as $religions_id) {
                        CitiesReligions::firstOrCreate(
                            ['cities_id' => $model->id, 'religions_id' => $religions_id]
                        );
                    }
                    
                }
                foreach (CitiesReligions::where(['cities_id' => $model->id])->get() as $city_religion) {
                        if ( !is_array($extra['religions']) OR  !in_array($city_religion->religions_id, $extra['religions'])) {
                            $city_religion->delete();
                        }
                    }
                
                if(is_array($extra['currencies'])) {
                    foreach ($extra['currencies'] as $id) {
                        CitiesCurrencies::firstOrCreate(
                            ['cities_id' => $model->id, 'currencies_id' => $id]
                        );
                    }
                    
                }
                foreach (CitiesCurrencies::where(['cities_id' => $model->id])->get() as $value) {
                        if (!is_array($extra['currencies']) OR  !in_array($value->currencies_id, $extra['currencies'])) {
                            $value->delete();
                        }
                    }
                
                if(is_array($extra['emergency_numbers'])) {
                    foreach ($extra['emergency_numbers'] as $id) {
                        CitiesEmergencyNumbers::firstOrCreate(
                            ['cities_id' => $model->id, 'emergency_numbers_id' => $id]
                        );
                    }
                    
                }
                foreach (CitiesEmergencyNumbers::where(['cities_id' => $model->id])->get() as $value) {
                        if (!is_array($extra['emergency_numbers']) OR  !in_array($value->emergency_numbers_id, $extra['emergency_numbers'])) {
                            $value->delete();
                        }
                    }

                if(isset($extra['holidays'])) {
                    foreach ($extra['holidays'] as $key => $value) {
                        if(isset($value)){
                          $cityHoliday =  CitiesHolidays::firstOrNew(
                                ['cities_id' => $model->id, 'holidays_id' => $value]
                            );
                          $cityHoliday->date = $extra['holidays_date'][$key];
                          $cityHoliday->save();
                        }
                    }
                    foreach (CitiesHolidays::where(['cities_id' => $model->id])->get() as $value) {
                        if (!in_array($value->holidays_id, $extra['holidays'])) {
                            $value->delete();
                        }
                    }
                } else {
                    foreach (CitiesHolidays::where(['cities_id' => $model->id])->get() as $value) {
                        $value->delete();
                    }
                }


                foreach (CitiesHolidays::where(['cities_id' => $model->id])->get() as $value) {
                        if ( !is_array($extra['holidays']) OR  !in_array($value->holidays_id, $extra['holidays'])) {
                            $value->delete();
                        }
                    }
                
                if(isset($extra['abouts'])) {
                    foreach ($extra['abouts']['title'] as $key => $value) {
                        if(isset($value)){
                            $cityAbout =  CitiesAbouts::firstOrNew([
                                'cities_id' => $model->id,
                                'type' => $extra['abouts']['type'][$key],
                                'title' => $value
                            ]);
                            $cityAbout->cities_id     = $model->id;
                            $cityAbout->type          = $extra['abouts']['type'][$key];;
                            $cityAbout->title         = $value;
                            $cityAbout->body          = $extra['abouts']['body'][$key];
                            $cityAbout->save();
                        }
                    }
                    foreach (CitiesAbouts::where(['cities_id' => $model->id])
                    ->whereIn('type', ['restrictions', 'planning_tips', 'health_notes', 'potential_dangers', 'speed_limit', 'etiquette'])
                    ->get() as $value) {
                        if (!in_array($value->title, $extra['abouts']['title']) || !in_array($value->body, $extra['abouts']['body'])) {
                            $value->delete();
                        }
                    }
                } else {
                    foreach (CitiesAbouts::where(['cities_id' => $model->id])
                    ->whereIn('type', ['restrictions', 'planning_tips', 'health_notes', 'potential_dangers', 'speed_limit', 'etiquette'])
                    ->get() as $value) {
                        $value->delete();
                    }
                }

                foreach (CitiesAbouts::where(['cities_id' => $model->id])
                ->whereIn('type', ['restrictions', 'planning_tips', 'health_notes', 'potential_dangers', 'speed_limit', 'etiquette'])
                ->get() as $value) {
                    if ( !is_array($extra['abouts']['type']) || !in_array($value->title, $extra['abouts']['title']) || !in_array($value->body, $extra['abouts']['body'])) {
                        $value->delete();
                    }
                }
                if(isset($extra['best_time'])) {
                    foreach($extra['best_time'] as $season => $best_time) {
                        if($season == 'low_season') {
                            $start_end = [];
                            foreach($best_time['low_start'] as $key => $value) {
                                if ($value != null && $best_time['low_end'][$key] != null) {
                                    array_push($start_end, $value.'-'.$best_time['low_end'][$key]);
                                }
                            }
                            $title = $season;
                        } elseif($season == 'shoulder') {
                            $start_end = [];
                            foreach($best_time['shoulder_start'] as $key => $value) {
                                if ($value != null && $best_time['shoulder_end'][$key] != null) {
                                    array_push($start_end, $value.'-'.$best_time['shoulder_end'][$key]);
                                }
                            }
                            $title = $season;
                        } elseif($season == 'high_season') {
                            $start_end = [];
                            foreach($best_time['high_start'] as $key => $value) {
                                if ($value != null && $best_time['high_end'][$key] != null) {
                                    array_push($start_end, $value.'-'.$best_time['high_end'][$key]);
                                }
                            }
                            $title = $season;
                        }
                        if(!empty($start_end)) {
                            $city_season = CitiesAbouts::firstOrNew(
                                ['cities_id' => $model->id, 'title' => $title]
                            );
                            $city_season->cities_id     = $model->id;
                            $city_season->type          = 'best_time';
                            $city_season->title         = $title;
                            $city_season->body          = implode(",", $start_end);;
                            $city_season->save();
                        }
                    }
                    if (count($extra['best_time']['low_season']['low_start']) == 1) {
                        if($extra['best_time']['low_season']['low_start'][0] == null || $extra['best_time']['low_season']['low_end'][0] == null){
                            CitiesAbouts::where([['cities_id', $model->id], ['title', 'low_season']])->delete();
                        }
                    }
                    if (count($extra['best_time']['shoulder']['shoulder_start']) == 1) {
                        if($extra['best_time']['shoulder']['shoulder_start'][0] == null || $extra['best_time']['shoulder']['shoulder_end'][0] == null){
                            CitiesAbouts::where([['cities_id', $model->id], ['title', 'shoulder']])->delete();
                        }
                    }
                    if (count($extra['best_time']['high_season']['high_start']) == 1) {
                        if($extra['best_time']['high_season']['high_start'][0] == null || $extra['best_time']['high_season']['high_end'][0] == null){
                            CitiesAbouts::where([['cities_id', $model->id], ['title', 'high_season']])->delete();
                        }
                    }
                }
                if (isset($extra['daily_costs'])) {
                    foreach($extra['daily_costs'] as $key => $value) {
                        $daily_costs = CitiesAbouts::firstOrNew(
                            ['cities_id' => $model->id, 'title' => $key]
                        );
                        // dd($daily_costs && $value == null);
                        if ($daily_costs && $value == null) {
                            CitiesAbouts::where([['cities_id', $model->id], ['title', $key]])->delete();
                        } else {
                            
                            $daily_costs->cities_id     = $model->id;
                            $daily_costs->type          = 'daily_costs';
                            $daily_costs->title         = $key;
                            $daily_costs->body          = $value;
                            $daily_costs->save();
                        }
                    }
                }
                if (isset($extra['transportation'])) {
                    $transportation = CitiesAbouts::firstOrNew(['cities_id' => $model->id, 'type' => 'transportation']);
                    if (count($extra['transportation']['transports']) == 1 && $extra['transportation']['transports'][0] == null) {
                        $transportation->delete();
                    } else {
                        $transportation->cities_id     = $model->id;
                        $transportation->type          = 'transportation';
                        $transportation->title         = 'transport';
                        $transportation->body          = implode(",", $extra['transportation']['transports']);
                        $transportation->save();
                    }
                }
                if (isset($extra['sockets_plugs'])) {
                    $sockets_plugs = CitiesAbouts::firstOrNew(['cities_id' => $model->id, 'type' => 'sockets_plugs']);
                    if (empty($extra['sockets_plugs'])) {
                        $sockets_plugs->delete();
                    } else {
                        $sockets_plugs->cities_id    = $model->id;
                        $sockets_plugs->type         = 'sockets_plugs';
                        $sockets_plugs->body         = implode(",", $extra['sockets_plugs']);
                        $sockets_plugs->save();
                    }
                }
                if(is_array($extra['languages_spoken'])) {
                    foreach ($extra['languages_spoken'] as $id) {
                        CitiesLanguagesSpoken::firstOrCreate(
                            ['cities_id' => $model->id, 'languages_spoken_id' => $id]
                        );
                    }
                    
                }
                foreach (CitiesLanguagesSpoken::where(['cities_id' => $model->id])->get() as $value) {
                        if (!is_array($extra['languages_spoken']) OR   !in_array($value->languages_spoken_id, $extra['languages_spoken'])) {
                            $value->delete();
                        }
                    }
                
                if(is_array($extra['additional_languages_spoken'])) {
                    foreach ($extra['additional_languages_spoken'] as $id) {
                        CitiesAdditionalLanguages::firstOrCreate(
                            ['cities_id' => $model->id, 'languages_spoken_id' => $id]
                        );
                    }
                    
                }
                foreach (CitiesAdditionalLanguages::where(['cities_id' => $model->id])->get() as $value) {
                        if (!is_array($extra['additional_languages_spoken']) OR  !is_array($extra['additional_languages_spoken']) OR !in_array($value->languages_spoken_id, $extra['additional_languages_spoken'])) {
                            $value->delete();
                        }
                    }
                foreach (CitiesLifestyles::where(['cities_id' => $model->id])->get() as $value) {
                    $value->delete();
                }
                if(is_array($extra['lifestyles'])) {
                    foreach ($extra['lifestyles'] as $key => $value) {
                        if ($value) {
                            CitiesLifestyles::firstOrCreate(
                                ['cities_id' => $model->id, 'lifestyles_id' => $value, 'rating' => $extra['lifestyles_rating'][$key]]
                            );
                        }
                    }
                }


                foreach ($translations as $translation) {
                    $language = Languages::find($translation['languages_id']);
                    unset($translation['languages_id']);
                    if ($language->code == 'en') {
                        $defaultTranslation = $translation;
                        if (!CitiesTranslations::find($translation['id'])->update($translation)) {
                            $check = 0;
                        }
                    } else {
                        if (null == $translation['id']) {
                            unset($translation['id']);
                            $translation['cities_id'] = $model->id;
                            $translation['languages_id'] = $language->id;
                            $translation = CitiesTranslations::create($translation)->toArray();
                            unset($translation['cities_id']);
                            unset($translation['languages_id']);
                        }
                        $this->dispatch(
                            (new TranslateUpdatedCitiestTrans($defaultTranslation, $translation, $language->code))
                                ->onQueue('translate')
                        );
                    }
                }

                if ($check) {
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }

    public static function validateUpload($extension)
    {
        $extension = strtolower($extension);

        switch ($extension) {
            case 'jpeg':
                return true;
            case 'jpg':
                return true;
                break;
            case 'png':
                return true;
                break;
            case 'gif':
                return true;
                break;
            default:
                return false;
        }
    }
}
