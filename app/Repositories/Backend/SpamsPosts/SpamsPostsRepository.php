<?php

namespace App\Repositories\Backend\SpamsPosts;

use App\Models\ActivityLog\ActivityLog;
use App\Models\ActivityMedia\Media;
use App\Models\ActivityMedia\MediasComments;
use App\Models\City\CitiesFollowers;
use App\Models\Country\CountriesFollowers;
use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Events\Events;
use App\Models\Events\EventsComments;
use App\Models\Hotels\HotelFollowers;
use App\Models\Place\PlaceFollowers;
use App\Models\Place\PlaceReviews;
use App\Models\Posts\ApiReports;
use App\Models\Posts\Checkins;
use App\Models\Posts\CheckinsComments;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsCheckins;
use App\Models\Posts\PostsComments;
use App\Models\Posts\SpamsPosts;
use App\Models\Reports\ConfirmedReports;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsComments;
use App\Models\Reviews\Reviews;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsComments;
use App\Models\User\User;
use App\Models\User\UsersMedias;
use App\Models\UsersFollowers\UsersFollowers;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SpamsPostsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = SpamsPosts::class;

    /**
     * @return mixed
     */
    public function getForDataTable($filter, $reportType)
    {
        $type = $this->_getPostReportType($reportType);

        $dataTableQuery = $this->query()
            ->with('confirmedReports', 'user.confirmedReports')
            ->whereDoesntHave('confirmedReports')
            ->when($filter, function ($query) use ($filter) {
                if ($filter === 'experts' ) {
                    $query->whereHas('user', function($q) {
                        return $q->where('type', 2)->orWhere('expert', 1);
                    });
                } elseif ($filter === 'flagged_authors' ) {
                    $query->whereHas('user', function($q) {
                        return $q->whereHas('confirmedReportsAuthor', function($query) {
                            return $query->select(DB::raw('count(*) as count'))->where('created_at', '>', Carbon::now()->subMonth())
                                ->where('type', 'mark_as_safe')->groupBy('users_id')->having('count', '>', 5);
                        });
                    });
                }
            })
            ->where('report_type', $type)
            ->select(
                'id',
                'post_type',
                'report_type',
                'posts_id',
                'users_id',
                'report_text'
            );
        return $dataTableQuery;
    }
    /**
     * @param $spamPostID
     * @return User|null
     */
    public function getPostOwnerById($spamPostID){
        $spamPost = (self::MODEL)::find($spamPostID);
        $result = $this->getPostData( $spamPost);
        return $result['user'];
    }

    /**
     * @param $postsID
     * @return mixed
     */
    public function getPostReportUsers($postsID)
    {
        $spamPosts = (self::MODEL)::where('posts_id', $postsID)->groupBy('users_id')->get();
        $data = [];
        foreach ($spamPosts as $spamPost) {
            $user = User::where('id', $spamPost->users_id)->first();
            if ($user) {
                $data[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'date' => Carbon::parse($spamPost->created_at)->format('d/m/Y'),
                    'text' => $spamPost->report_text ?: '-',
                ];
            }
        }
        return $data;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsSafe($id) {
        try {
            $spamPosts = (self::MODEL)::where('id', $id)->first();
            $result = $this->getPostData($spamPosts);
            $ownerId = null;
            if ($result['user']) {
                $ownerId = $result['user']->id;
            }
            $this->confirmReport($spamPosts->id, $spamPosts->users_id,  'mark_as_safe', $ownerId);
            return response()->json([
                'data' => [
                    'success' => true,
                    'message' => 'Report marked safe successfully.',
                ]
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'data' => [
                    'success' => false,
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeReportPost($id) {
        try {
            $removedData = $this->_removePost($id);
            $ownerId = $removedData['owners_id'];
            $postsId = $removedData['posts_id'];
            if ($ownerId) {
                $message = '&nbsp&nbspYour <a target="_blank" href="' . url('/profile-posts/' . $ownerId . '#' . $postsId ) . '">post</a> has been deleted';
                notify($ownerId, 'post_delete', $postsId , $message);
            }

            $this->confirmReport($id, $removedData['users_id'], 'remove_post', $ownerId);
            return response()->json([
                'data' => [
                    'success' => true,
                    'message' => $postsId  ? 'Post deleted successfully.' : 'Post not find'
                ]
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'data' => [
                    'success' => false,
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeReportPostAndBlockUser($id) {
        try {
            $removedData = $this->_removePost($id);
            $ownerId = $removedData['owners_id'];
            if ($ownerId) {
                $message = '&nbsp&nbspYour <a target="_blank" href="' . url('/profile-posts/' . $ownerId . '#' . $removedData['posts_id']) . '">post</a> has been deleted';
                block_user($ownerId);
                notify($ownerId, 'post_delete', $removedData['posts_id'], $message);
            }
            $this->confirmReport($id, $removedData['users_id'],'remove_post_and_block_user', $ownerId);

            return response()->json([
                'data' => [
                    'success' => true,
                    'message' => 'Post delete and user blocked successfully.',
                ]
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'data' => [
                    'success' => false,
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    /**
     * @param $spamPostsId
     * @param $type
     * @param $ownerId
     */
    public function confirmReport($spamPostsId, $userId, $type, $ownerId) {
        $confirmedReports = new ConfirmedReports;
        $confirmedReports->admin_id  = Auth::user()->id;
        $confirmedReports->owner_id  = $ownerId;
        $confirmedReports->type = $type;
        $confirmedReports->spam_post_id = $spamPostsId;
        $confirmedReports->save();
    }

    /**
     * @param $spamPostsId
     * @return \Illuminate\Http\JsonResponse
     */
    public function restoreRemovedPost($spamPostsId) {
        try {
            $spamPosts = (self::MODEL)::where('id', $spamPostsId)->first();
            $result = $this->getPostData($spamPosts);
            if ($result['model']) {
                $result['model']->deleted_at = null;
                $result['model']->save();
            } else {
                return response()->json([
                    'data' => [
                        'success' => false,
                        'message' => 'Post can not be restored.',
                    ]
                ]);
            }

            if ($result['user']) {
                $message = '&nbsp&nbspRestore your removed <a target="_blank" href="' . url('/profile-posts/' . $result['user']->id . '#' . $spamPosts->posts_id) . '">post</a>';
                notify($result['user']->id, 'restore_post', $spamPosts->posts_id, $message);
            }
            return response()->json([
                'data' => [
                    'success' => true,
                    'message' => 'Post  successfully restored.',
                ]
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'data' => [
                    'success' => false,
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }


    /**
     * @param $spamPost
     * @return array|null
     */
    public function getPostData($spamPost)
    {
        $data = [];
        if( is_null($spamPost))
            return null;

        /*
            case 'Post':
            case 'Discussion':
            case 'PlaceImage':
            case 'Checkin':
            case 'Mediaupload':

            case 'Trip':
            case 'Report':
            case 'Review':
            case 'Event':
         */
        switch($spamPost->post_type){
            case 'Statuscomment':
            case 'Statuspublish':
            case 'Post':
                $model = Posts::withTrashed()->find($spamPost->posts_id);

                if( is_null($model) )
                    return null;

                if( is_null($model->users_id) )
                    return null;
                $user = User::with('confirmedReports')->find($model->users_id);
                $data['user'] = $user;
                $data['model'] = $model;
                return $data;

                break;
            case 'Mediaupload':
                $model = UsersMedias::where('medias_id', $spamPost->posts_id)->first();

                if( is_null($model) )
                    return null;

                if( is_null($model->users_id) )
                    return null;
                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = Media::withTrashed()->where('id', $spamPost->posts_id)->first();
                return $data;
            case 'Mediacomment':
                $model = MediasComments::find($spamPost->posts_id);
                if( is_null($model) )
                    return null;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Discussion':
                $model = Discussion::where('id', $spamPost->posts_id)->first();

                if( is_null($model) )
                    return null;

                if( is_null($model->users_id) )
                    return null;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;

            case 'PlaceImage':
                $model = UsersMedias::where('medias_id', $spamPost->posts_id)->first();

                if( is_null($model) )
                    return null;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = Media::withTrashed()->where('id', $spamPost->posts_id)->first();
                return $data;

            case 'Checkin':
                $model = Checkins::find($spamPost->posts_id);

                if( is_null($model) )
                    return null;

                if( is_null($model->users_id) )
                    return null;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;

            case 'Tripcreate':
            case 'Trip':
                $model = TripPlans::find($spamPost->posts_id);

                if( is_null($model) )
                    return null;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;

            case 'Report':
                $model = Reports::find($spamPost->posts_id);

                if( is_null($model) )
                    return null;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Hotelreview':
            case 'Review':
                $model = Reviews::find($spamPost->posts_id);

                if( is_null($model) )
                    return null;

                $user = User::with('confirmedReports')->find($model->by_users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Placefollow':
                //DONE
                $model = ActivityLog::find($spamPost->posts_id);
                if( is_null($model) )
                    return null;

                if( is_null($model->users_id) )
                    return null;
                $data['model'] = PlaceFollowers::where(['users_id' => $model->users_id, 'places_id' => $model->variable])->first();                if( is_null($model) )

                $data['activity']['type'] = $model->type;
                $data['activity']['action'] = $model->action;
                $data['activity']['variable'] =  $model->variable;
                $data['activity']['users_id'] = $model->users_id;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                return $data;
            case 'Placereview':
                $model = PlaceReviews::find($spamPost->posts_id);

                if( is_null($model) )
                    return null;

                if( is_null($model->users_id) )
                    return null;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Userfollow':
                $model = ActivityLog::find($spamPost->posts_id);
                if( is_null($model) )
                    return null;
                $data['model'] = UsersFollowers::where(['users_id' => $model->variable, 'followers_id' => $model->users_id])->first();
                $data['activity']['type'] = $model->type;
                $data['activity']['action'] = $model->action;
                $data['activity']['variable'] =  $model->variable;
                $data['activity']['users_id'] = $model->users_id;
                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                return $data;
            case 'Eventcomment':
                $model = EventsComments::find($spamPost->posts_id);

                if( is_null($model) )
                    return null;

                if( is_null($model->users_id) )
                    return null;

                $data['activity']['type'] = 'Event';
                $data['activity']['action'] = 'commentreply';
                $data['activity']['variable'] =  $model->events_id;
                $data['activity']['users_id'] = $model->users_id;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Reportcomment':
                $model = ReportsComments::find($spamPost->posts_id);

                if( is_null($model) )
                    return null;

                if( is_null($model->users_id) )
                    return null;
                $data['activity']['type'] = 'Report';
                $data['activity']['action'] = 'comment';
                $data['activity']['variable'] =  $model->id;
                $data['activity']['users_id'] = $model->users_id;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Tripcomment':
                $model = TripsComments::find($spamPost->posts_id);
                if( is_null($model) )
                    return null;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Checkincomment':
                $model = CheckinsComments::find($spamPost->posts_id);

                if( is_null($model) )
                    return null;

                if( is_null($model->users_id) )
                    return null;

                $data['activity']['type'] = 'Checkin';
                $data['activity']['action'] = 'comment';
                $data['activity']['variable'] =  $model->checkins_id;
                $data['activity']['users_id'] = $model->users_id;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Postcomment':
                $model = PostsComments::find($spamPost->posts_id);

                if( is_null($model) )
                    return null;

                if( is_null($model->users_id) )
                    return null;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
//            case 'Upcomingholiday':
//                $model = UsersFollowers::find($spamPost->posts_id);
//                if( is_null($model) )
//                    return null;
//
//                $user = User::with('confirmedReports')->find($model->users_id);
//
//                $data['user'] = $user;
//                $data['model'] = $model;
//                return $data;
//            case 'Upcomingevent':
                //MISS data in blade
//                $model = PlaceFollowers::find($spamPost->posts_id);
//
//                if( is_null($model) )
//                    return null;
//
//                if( is_null($model->users_id) )
//                    return null;
//
//                $user = User::with('confirmedReports')->find($model->users_id);
//
//                $data['user'] = $user;
//                $data['model'] = $model;
//                return $data;
            case 'Cityfollow':
                $log = ActivityLog::where('id', $spamPost->posts_id)->first();
                if( is_null($log) )
                    return null;

                $model = CitiesFollowers::where('cities_id', $log->variable)->first();
                if( is_null($model) )
                    return null;

                $data['activity'] = $log;
                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Countryfollow':
                $log = ActivityLog::where('id', $spamPost->posts_id)->first();
                if( is_null($log) )
                    return null;

                $model = CountriesFollowers::where('countries_id', $log->variable)->first();
                if( is_null($model) )
                    return null;

                $data['activity'] = $log;
                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'DiscussionReplies':
                $model = DiscussionReplies::find($spamPost->posts_id);
                if( is_null($model) )
                    return null;

                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Hotelfollow':
                $log = ActivityLog::where('id', $spamPost->posts_id)->first();
                if( is_null($log) )
                    return null;

                $model = HotelFollowers::where('hotels_id', $log->variable)->first();
                if( is_null($model) )
                    return null;

                $data['activity'] = $log;
                $user = User::with('confirmedReports')->find($model->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Postcheckin':
                $model = PostsCheckins::with('checkin')->find($spamPost->posts_id);
                if( is_null($model) )
                    return null;
                if (!$model->checkin->users_id)
                    return null;

                $user = User::with('confirmedReports')->find($model->checkin->users_id);

                $data['user'] = $user;
                $data['model'] = $model;
                return $data;
            case 'Event':
                $model = Events::find($spamPost->posts_id);
                if( is_null($model) )
                    return null;

                $data['user'] = [];
                $data['model'] = $model;
                return $data;
            case 'Profile':
                $model = User::find($spamPost->posts_id);
                if( is_null($model) )
                    return null;

                $data['user'] = $model;
                $data['model'] = $model;
                return $data;
            default:
                return null;
                break;
        }
    }

    /**
     * @param $spamPostID
     * @return mixed
     */
    public function getPostById($spamPostID){

        $spamPost = (self::MODEL)::find($spamPostID);
        $result = $this->getPostData( $spamPost);
        return $result['model'];
    }

    /**
     * @return ApiReports|\Illuminate\Database\Eloquent\Builder
     */
    public function apiReportsTable() {
        $query = ApiReports::with('user')
        ->select(
            'id',
            'post_id',
            'user_id',
            'media_url',
            'text',
            'type',
            'created_at'
        );
        return $query;
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function historyTable($request)
    {
        $filter = $request->filter;
        $query = ConfirmedReports::with(['spamsPost', 'user'])
            ->when($filter, function ($query) use ($filter) {
                return $query->where('type', $filter);
            })
            ->select(
                'id',
                'admin_id',
                'owner_id',
                'type',
                'spam_post_id',
                'created_at'
            );
        return $query;
    }

    /**
     * @param $id
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function restoreApiReportPost($id, $type)
    {
        // restore post
        try {
            if($type === 'Mediaupload') {
                $media = Media::withTrashed()->where('id', $id)->first();
                if ($media) {
                    $media->deleted_at = null;
                    $media->save();
                }
            } else {
                $posts = Posts::withTrashed()->find($id);
                if ($posts) {
                    $posts->deleted_at = null;
                    $posts->save();
                }
            }
            ApiReports::where('post_id', $id)->where('type', $type)->delete();
            return response()->json([
                'data' => [
                    'success' => true,
                    'message' => 'Post  successfully restored.',
                ]
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'data' => [
                    'success' => false,
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    /**
     * @param $id
     * @return array
     */
    private function _removePost($id)
    {
        $spamPosts = (self::MODEL)::where('id', $id)->first();
        $ownerId = null;
        $postId = null;
        if ($spamPosts) {
            $result = $this->getPostData($spamPosts);
            //delete related tables e.g.Activity logs
            if (isset($result['activity']) && $result['activity']) {
                del_user_activity($result['activity']['type'], $result['activity']['action'],
                    $result['activity']['variable'], $result['activity']['users_id']);
            }
            //delete post
            if (isset($result['model']) && $result['model']) {
                $result['model']->delete();
            }
            $postId = $spamPosts->posts_id;
            if (isset($result['user']) && $result['user']) {
                $ownerId = $result['user']->id;
            }
        }
        return [
            'owners_id' => $ownerId,
            'posts_id' => $postId,
            'users_id' => $spamPosts->users_id
        ];
    }

    /**
     * @param $id
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function show($id)
    {
        $spamPosts = (self::MODEL)::with('confirmedReports', 'user.confirmedReports')
            ->where('id', $id)->first();
        $spamPosts->report_users = $this->getPostReportUsers($spamPosts->posts_id);
        $spamPosts->report_type = $this->_getPostReportType($spamPosts->report_type);
        $spamPosts->author = $this->getPostOwnerById($spamPosts->id);

        return view('backend.spam_manager.view')->withSpamPosts($spamPosts);
    }

    /**
     * @param $reportType
     * @return int
     */
    public function _getPostReportType($reportType)
    {

        switch($reportType){
            case 'spam':
                $type = 0;
                break;
            case 'other':
                $type = 1;
                break;
            case 'fake_news':
                $type = 2;
                break;
            case 'harassment':
                $type = 3;
                break;
            case 'hate_speech':
                $type = 4;
                break;
            case 'nudity':
                $type = 5;
                break;
            case 'terrorism':
                $type = 6;
                break;
            case 'violence':
                $type = 7;
                break;
            case 'api_reports':
                $type = 8;
                break;
            case 0:
                $type = 'spam';
                break;
            case 1:
                $type = 'other';
                break;
            case 2:
                $type = 'fake_news';
                break;
            case 3:
                $type = 'harassment';
                break;
            case 4:
                $type = 'hate_speech';
                break;
            case 5:
                $type = 'nudity';
                break;
            case 6:
                $type = 'terrorism';
                break;
            case 7:
                $type = 'violence';
                break;
            case 8:
                $type = 'api_reports';
                break;
            default:
                $type = 1;
                break;
        }

        return $type;
    }
}
