<?php
namespace App\Repositories\Backend\ActivityLog;

use App\Models\ActivityLog\ActivityLog as ActivityLogModel;
use App\Repositories\BaseRepository;
use App\Services\Api\PrimaryPostService;
use App\Services\Api\ShareService;

class ActivityLog extends BaseRepository
{
    const MODEL = ActivityLogModel::class;


    protected function getDataForQuery($type)
    {
        switch ($type) {
            case 'liked':
                return [
                    ['type' => 'post', 'action' => ['like']],
                    ['type' => 'event', 'action' => ['like']],
                    ['type' => 'other', 'action' => ['like']],
                    ['type' => 'review', 'action' => ['upvote']],
                    ['type' => 'trip', 'action' => ['like']],
                    ['type' => 'report', 'action' => ['like']],
//              //      ['type' => 'media', 'action' => ['like']],
                    ['type' => 'discussion', 'action' => ['upvote']],
                    ['type' => 'other', 'action' => ['show']],
                ];
            case 'trip_plans':
                return [
                    ['type' => 'trip', 'action' => ['create', 'plan_updated']],
                ];
            case 'travelogs':
                return [
                    ['type' => 'report', 'action' => ['create', 'update']],
                ];
            case 'discussions':
                return [
                    ['type' => 'discussion', 'action' => ['create']],
                ];
            case 'reviews':
                return [
                    ['type' => 'place', 'action' => ['review']],
                    ['type' => 'hotel', 'action' => ['review']],
                ];
            case 'all_feed':
                return [
                    ['type' => 'post', 'action' => ['publish']],
                    ['type' => 'other', 'action' => ['show']],
                    ['type' => 'place', 'action' => ['review']],
                    ['type' => 'hotel', 'action' => ['review']],
                    ['type' => 'discussion', 'action' => ['create']],
                    ['type' => 'trip', 'action' => ['create', 'plan_updated']],
                    ['type' => 'report', 'action' => ['create', 'update']],
                    ['type' => 'share', 'action' => [
                        'plan_media_shared', // for trip place media  share
                        'plan_step_shared', // for trip place share
                        'plan_shared', // for trip share
                        'review',
                        'event',
                        'discussion',
                        'text',
                        'report'
                    ]]
                ];
            default:
                return [
                    ['type' => 'post', 'action' => ['publish']],
                    ['type' => 'other', 'action' => ['show']],
                    ['type' => 'place', 'action' => ['review']],
                    ['type' => 'hotel', 'action' => ['review']],
                    ['type' => 'discussion', 'action' => ['create']],
                    ['type' => 'trip', 'action' => ['create', 'plan_updated']],
                    ['type' => 'report', 'action' => ['create', 'update']],
                ];
        }
    }

    private function getQueryRaw($activityType, $withDeleted = false)
    {
        $condition_items = [];
        $conditions = $this->getDataForQuery($activityType);
        foreach ($conditions as $condition) {
            foreach ($condition['action'] as $action) {
                $data_exists_condition = '';
                if ($condition['type'] == 'trip') { //TODO must redo on trip publish
                    $data_exists_condition = "AND EXISTS(SELECT * from trips WHERE id=variable and  deleted_at is null AND EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `deleted_at` IS NULL AND `trips_places`.`deleted_at` IS NULL))";
                }
                $condition_items[] = '(type = "' . $condition['type'] . '" and action = "' . $action . '" ' . $data_exists_condition . ') ';
            }
        }
        $where_condition = '';
        if (count($condition_items) > 0) {
            $where_condition .= '(' . implode(' or ', $condition_items) . ')';
            if (!$withDeleted) {
                $where_condition .= ' AND deleted = false ';
            }
        }
        return $where_condition;
    }

    public function get(string $activityType, int $userId = null, int $page = 1, int $perPage = 15)
    {
        $skip = ($page - 1) * $perPage;

        return $this->query()
            ->whereRaw($this->getQueryRaw($activityType))
            ->when($userId, function ($query) use ($userId) {
                return $query->where('users_id', $userId);
            })
            ->orderBy('id', 'desc')
            ->skip($skip)
            ->take($perPage)
            ->get();
    }

    public function count(string $activityType, int $userId = null)
    {
        return $this->query()
            ->whereRaw($this->getQueryRaw($activityType))
            ->when($userId, function ($query) use ($userId) {
                return $query->where('users_id', $userId);
            })
            ->count();
    }

    public function getTransformed(string $activityType, int $userId = null, int $page = 1, int $perPage = 15)
    {
        return $this->get($activityType, $userId, $page, $perPage)->map(function ($item) {
            if (
                ($item->action == 'review' && ($item->type == 'place' || $item->type == 'hotel'))
                || $item->action == ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE
                || $item->action == ShareService::TYPE_TRIP_PLACE_SHARE
                || $item->action == ShareService::TYPE_TRIP_SHARE
            ) {
                $type = $item->type;
                $item->type = $item->action;
                $item->action = $type;
            }

            return $item;
        });
    }

    public function create($type, $action, $variable, $permission = 0, $passedUser = null)
    {
        $user = auth()->user() ?: ($passedUser  ?: null);
        $type = snake_case($type);
        $action = snake_case($action);
        if (is_null($user)) {
            return false;
        }

        if ($action == 'delete') {
            ActivityLogModel::query()->where(['type' => $type, 'variable' => $variable])->update(['deleted' => true]);
        }

        if (in_array($type, [PrimaryPostService::TYPE_TRIP, PrimaryPostService::TYPE_REPORT])) {
            $deleteOldLog = ActivityLogModel::query()->where([
                'type' => $type,
                'variable' => $variable,
                'users_id' => $user->id
            ]);
            switch ($type) {
                case PrimaryPostService::TYPE_TRIP;
                    $deleteOldLog->whereIn('action', ['create', 'update', 'plan_updated'])->delete();
                    break;
                case PrimaryPostService::TYPE_REPORT;
                    $deleteOldLog->whereIn('action', ['create', 'update'])->delete();
                    break;
            }
        }
        $activeLog = new ActivityLogModel;
        $activeLog->users_id = $user->id;
        $activeLog->type = $type;
        $activeLog->action = $action;
        $activeLog->variable = $variable;
        $activeLog->time = date('Y-m-d H:i:s', time());
        if (in_array($type, [PrimaryPostService::TYPE_TRIP, PrimaryPostService::TYPE_POST]))
            $activeLog->permission = $permission;

        return ($activeLog->save()) ? $activeLog : false;
    }
}