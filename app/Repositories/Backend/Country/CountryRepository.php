<?php

namespace App\Repositories\Backend\Country;

/* Country Models Start */
use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\Country\Countries;
use App\Models\Country\CountriesTranslations;
use App\Models\Country\CountriesCurrencies;
use App\Models\Country\CountriesCapitals;
use App\Models\Country\CountriesHolidays;
use App\Models\Country\CountriesEmergencyNumbers;
use App\Models\Country\CountriesLanguagesSpoken;
use App\Models\Country\CountriesAdditionalLanguages;
use App\Models\Country\CountriesLifestyles;
use App\Models\Country\CountriesMedias;
use App\Models\Country\CountriesReligions;
use App\Models\Country\CountriesAbouts;
/* Country Models End */

use App\Models\ActivityMedia\Media;
use App\Models\Access\language\Languages;

use App\Services\Medias\LocationsAddMediaService;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Jobs\Backend\Country\TranslateCreatedCountriesTrans;
use App\Jobs\Backend\Country\TranslateUpdatedCountriesTrans;

/**
 * Class CountryRepository.
 */
class CountryRepository extends BaseRepository
{
    use CreateUpdateStatisticTrait;
    use DispatchesJobs;
    /**
     * Associated Repository Model.
     */
    const MODEL = Countries::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @var LocationsAddMediaService
     */
    protected $locationsAddMediaService;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role, LocationsAddMediaService $locationsAddMediaService)
    {
        parent::__construct();
        $this->role = $role;
        $this->locationsAddMediaService = $locationsAddMediaService;
    }

    /**
     * @param        $permissions
     * @param string $by
     *
     * @return mixed
     */
    public function getByPermission($permissions, $by = 'name')
    {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }

        return $this->query()->whereHas('roles.permissions', function ($query) use ($permissions, $by) {
            $query->whereIn('permissions.' . $by, $permissions);
        })->get();
    }

    /**
     * @param        $roles
     * @param string $by
     *
     * @return mixed
     */
    public function getByRole($roles, $by = 'name')
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }

        return $this->query()->whereHas('roles', function ($query) use ($roles, $by) {
            $query->whereIn('roles.' . $by, $roles);
        })->get();
    }

    /**
     * @param int $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
//            ->withTrashed()
            ->select([
                config('locations.country_table') . '.id',
                config('locations.country_table') . '.code',
                config('locations.country_table') . '.lat',
                config('locations.country_table') . '.lng',
                config('locations.country_table') . '.active',
                'transsingle.title'
            ])
            ->leftJoin('countries_trans AS transsingle', function ($query) {
                $query->on('transsingle.countries_id', '=', 'countries.id')
                    ->where('transsingle.languages_id', '=', 1);
            });

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $translations
     * @param $extra
     */
    public function create($translations, $extra)
    {
        $model = new Countries;
        $model->regions_id = $extra['region_id'];
        $model->active = $extra['active'];
        $model->code = $extra['code'];
        $model->lat = $extra['lat'];
        $model->lng = $extra['lng'];
        $model->iso_code = $extra['iso_code'];

        DB::transaction(function () use ($model, $translations, $extra) {
            $check = 1;

            if ($model->save()) {

                $this->uploadLicensedImagesToS3(
                    $extra['license_images'],
                    $model,
                    new CountriesMedias(),
                    'countries_id',
                    'country'
                );

                /* ADD Religions IN CountriesReligions */
                if (!empty($extra['religions'])) {
                    foreach ($extra['religions'] as $key => $value) {
                        CountriesReligions::create(['countries_id' => $model->id, 'religions_id' => $value]);
                    }
                }

                /* ADD Medias IN CountriesMedias */
                if (!empty($extra['medias'])) {
                    foreach ($extra['medias'] as $key => $value) {
                        CountriesMedias::create(['countries_id' => $model->id, 'medias_id' => $value]);
                    }
                }

                $this->updateStatistic($model, 'medias', count($model->medias));

                /* ADD LIFESTYLEs IN CountriesLifestyles */
                if (!empty($extra['lifestyles'])) {
                    foreach ($extra['lifestyles'] as $key => $value) {
                        if ($value) {
                            CountriesLifestyles::create(
                                ['countries_id' => $model->id, 'lifestyles_id' => $value, 'rating' => $extra['lifestyles_rating'][$key]]
                            );
                        }
                    }
                }

                /* ADD LANGUAGES SPOKEN IN CountriesLanguagesSpoken */
                if (!empty($extra['languages_spoken'])) {
                    foreach ($extra['languages_spoken'] as $key => $value) {
                        CountriesLanguagesSpoken::create(['countries_id' => $model->id, 'languages_spoken_id' => $value]);
                    }
                }

                /* ADD ADDITIONAL LANGUAGES SPOKEN IN CountriesLanguagesSpoken */
                if (!empty($extra['additional_languages_spoken'])) {
                    foreach ($extra['additional_languages_spoken'] as $key => $value) {
                        CountriesAdditionalLanguages::create(['countries_id' => $model->id, 'languages_spoken_id' => $value]);
                    }
                }

                /* Add Holidays In CountriesHolidays */
                if (!empty($extra['holidays'])) {
                    foreach ($extra['holidays'] as $key => $value) {
                        if(isset($value)){
                            CountriesHolidays::create(['countries_id' => $model->id, 'holidays_id' => $value, 'date' => $extra['holidays_date'][$key]]);
                        }
                    }
                }

                /* Add Cities in CountriesCapitals */
                if (isset($extra['cities'])) {
                    foreach ($extra['cities'] as $key => $value) {
                        CountriesCapitals::create(['countries_id' => $model->id, 'cities_id' => $value]);
                    }
                }

                /* Add Numbers in CountriesEmergencyNumbers */
                if (!empty($extra['emergency_numbers'])) {
                    foreach ($extra['emergency_numbers'] as $key => $value) {
                        CountriesEmergencyNumbers::create(['countries_id' => $model->id, 'emergency_numbers_id' => $value]);
                    }
                }

                /* Add Currencies in Countries Currencies */
                if (!empty($extra['currencies'])) {
                    foreach ($extra['currencies'] as $key => $value) {
                        CountriesCurrencies::create(['countries_id' => $model->id, 'currencies_id' => $value]);
                    }
                }
                foreach ($translations as $translation) {
                    $language = Languages::find($translation['languages_id']);

                    $translation['countries_id'] = $model->id;
                    if ($language->code == 'en') {
                        $defaultTranslation = $translation;
                        if (!CountriesTranslations::create($translation)) {
                            $check = 0;
                        }
                    } else {
                        $this->dispatch(
                            (new TranslateCreatedCountriesTrans($defaultTranslation, $translation, $language->code))
                                ->onQueue('translate')
                        );
                    }
                }
                /* Add countries about */
                if(isset($extra['abouts'])) {
                    foreach ($extra['abouts']['type'] as $key => $value) {
                        if(isset($extra['abouts']['title'][$key]) && isset($extra['abouts']['body'][$key])){
                            $countryAbout =  CountriesAbouts::create([
                                'countries_id' => $model->id,
                                'type' => $value,
                                'title' => $extra['abouts']['title'][$key],
                                'body' => $extra['abouts']['body'][$key]
                            ]);
                        }
                    }    
                }

                $this->locationsAddMediaService->addCountryMedia($model);

                if ($check) {
                    return true;
                }

                return false;
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }


    /**
     * @param $id
     * @param $model
     * @param $translations
     * @param $extra
     */
    public function update($id, $model, $translations, $extra)
    {
        $model = Countries::findOrFail($id);
        $model->regions_id = $extra['region_id'];
        $model->active = $extra['active'];
        $model->code = $extra['code'];
        $model->iso_code = $extra['iso_code'];
        $model->lat = $extra['lat'];
        $model->lng = $extra['lng'];

        DB::transaction(function () use ($model, $translations, $extra) {

            $check = 1;

            if ($model->save()) {
                if (!empty($extra['license_images']['deleted'])) {
                    foreach ($extra['license_images']['deleted'] as $media_id) {
                        $media = Media::find($media_id);
                        unset($extra['license_images']['images'][$media_id]);
                        $this->deleteFromS3($media->url);
                        CountriesMedias::where(['countries_id' => $model->id, 'medias_id' => $media_id])->delete();
                        $media->delete();
                    }
                }

                foreach ($extra['license_images']['images'] as $key => $image) {
                    if ( $key > 100 ) {
                        $media = Media::find($key);
                        $media->update($image);
                        unset($extra['license_images']['images'][$key]);
                    }
                }


                $this->uploadLicensedImagesToS3(
                    $extra['license_images']['images'],
                    $model,
                    new CountriesMedias(),
                    'countries_id',
                    'countries'
                );

                /* ADD Religions IN CountriesReligions */
                if(isset($extra['religions'])) {
                    foreach ($extra['religions'] as $religions_id) {
                        CountriesReligions::firstOrCreate(
                            ['countries_id' => $model->id, 'religions_id' => $religions_id]
                        );
                    }

                    foreach (CountriesReligions::where(['countries_id' => $model->id])->get() as $country_religion) {
                        if (!in_array($country_religion->religions_id, $extra['religions'])) {
                            $country_religion->delete();
                        }
                    }
                } else {
                    foreach (CountriesReligions::where(['countries_id' => $model->id])->get() as $country_religion) {
                            $country_religion->delete();
                    }
                }

                $this->updateStatistic($model, 'medias', count($model->medias));

                /* ADD Lifestyles IN CountriesLifestyles */
                if(isset($extra['lifestyles'])) {
                    foreach (CountriesLifestyles::where(['countries_id' => $model->id])->get() as $country_lifestyle) {
                        $country_lifestyle->delete();
                    }
                    foreach ($extra['lifestyles'] as $key => $value) {
                        if ($value) {
                            CountriesLifestyles::firstOrCreate(
                                ['countries_id' => $model->id, 'lifestyles_id' => $value, 'rating' => $extra['lifestyles_rating'][$key]]
                            );
                        }
                    }

                } else {
                    foreach (CountriesLifestyles::where(['countries_id' => $model->id])->get() as $country_lifestyle) {
                            $country_lifestyle->delete();
                    }
                }

                /* ADD LANGUAGES SPOKEN IN CountriesLanguagesSpoken */
                if(isset($extra['languages_spoken'])) {
                    foreach ($extra['languages_spoken'] as $key => $value) {
                        CountriesLanguagesSpoken::firstOrCreate(
                            ['countries_id' => $model->id, 'languages_spoken_id' => $value]
                        );
                    }
                    foreach (CountriesLanguagesSpoken::where(['countries_id' => $model->id])->get() as $country_language_spoken) {
                        if (!in_array($country_language_spoken->languages_spoken_id, $extra['languages_spoken'])) {
                            $country_language_spoken->delete();
                        }
                    }
                } else {
                    foreach (CountriesLanguagesSpoken::where(['countries_id' => $model->id])->get() as $country_language_spoken) {
                            $country_language_spoken->delete();
                    }
                }

                /* ADD ADDITIONAL LANGUAGES SPOKEN IN CountriesLanguagesSpoken */
                if(isset($extra['additional_languages_spoken'])) {
                    foreach ($extra['additional_languages_spoken'] as $key => $value) {
                        CountriesAdditionalLanguages::firstOrCreate(
                            ['countries_id' => $model->id, 'languages_spoken_id' => $value]
                        );
                    }
                    foreach (CountriesAdditionalLanguages::where(['countries_id' => $model->id])->get() as $value) {
                        if (!in_array($value->languages_spoken_id, $extra['additional_languages_spoken'])) {
                            $value->delete();
                        }
                    }
                } else {
                    foreach (CountriesAdditionalLanguages::where(['countries_id' => $model->id])->get() as $value) {
                            $value->delete();
                    }
                }

                /* Add Holidays in CountriesHolidays */
                if(isset($extra['holidays'])) {
                    foreach ($extra['holidays'] as $key => $value) {
                        if(isset($value)){
                            CountriesHolidays::firstOrCreate(
                                ['countries_id' => $model->id, 'holidays_id' => $value, 'date' => $extra['holidays_date'][$key]]
                            );
                        }
                    }
                    foreach (CountriesHolidays::where(['countries_id' => $model->id])->get() as $value) {
                        if (!in_array($value->holidays_id, $extra['holidays'])) {
                            $value->delete();
                        }
                    }
                } else {
                    foreach (CountriesHolidays::where(['countries_id' => $model->id])->get() as $value) {
                            $value->delete();
                    }
                }

                if(isset($extra['abouts'])) {
                    foreach ($extra['abouts']['title'] as $key => $value) {
                        if(isset($value)){
                            $countryAbout =  CountriesAbouts::firstOrNew([
                                'countries_id' => $model->id,
                                'type' => $extra['abouts']['type'][$key],
                                'title' => $value
                            ]);
                            $countryAbout->countries_id  = $model->id;
                            $countryAbout->type          = $extra['abouts']['type'][$key];;
                            $countryAbout->title         = $value;
                            $countryAbout->body          = $extra['abouts']['body'][$key];
                            $countryAbout->save();
                        }
                    }
                    foreach (CountriesAbouts::where(['countries_id' => $model->id])
                    ->whereIn('type', ['restrictions', 'planning_tips', 'health_notes', 'potential_dangers', 'speed_limit', 'etiquette'])
                    ->get() as $value) {
                        if (!in_array($value->title, $extra['abouts']['title']) || !in_array($value->body, $extra['abouts']['body'])) {
                            $value->delete();
                        }
                    }
                } else {
                    foreach (CountriesAbouts::where(['countries_id' => $model->id])
                    ->whereIn('type', ['restrictions', 'planning_tips', 'health_notes', 'potential_dangers', 'speed_limit', 'etiquette'])
                    ->get() as $value) {
                        $value->delete();
                    }
                }
                foreach (CountriesAbouts::where(['countries_id' => $model->id])
                ->whereIn('type', ['restrictions', 'planning_tips', 'health_notes', 'potential_dangers', 'speed_limit', 'etiquette'])
                ->get() as $value) {
                    if ( !is_array($extra['abouts']['type']) || !in_array($value->title, $extra['abouts']['title']) || !in_array($value->body, $extra['abouts']['body'])) {
                        $value->delete();
                    }
                }
                if(isset($extra['best_time'])) {
                    foreach($extra['best_time'] as $season => $best_time) {
                        if($season == 'low_season') {
                            $start_end = [];
                            foreach($best_time['low_start'] as $key => $value) {
                                if ($value != null && $best_time['low_end'][$key] != null) {
                                    array_push($start_end, $value.'-'.$best_time['low_end'][$key]);
                                }
                            }
                            $title = $season;
                        } elseif($season == 'shoulder') {
                            $start_end = [];
                            foreach($best_time['shoulder_start'] as $key => $value) {
                                if ($value != null && $best_time['shoulder_end'][$key] != null) {
                                    array_push($start_end, $value.'-'.$best_time['shoulder_end'][$key]);
                                }
                            }
                            $title = $season;
                        } elseif($season == 'high_season') {
                            $start_end = [];
                            foreach($best_time['high_start'] as $key => $value) {
                                if ($value != null && $best_time['high_end'][$key] != null) {
                                    array_push($start_end, $value.'-'.$best_time['high_end'][$key]);
                                }
                            }
                            $title = $season;
                        }
                        if(!empty($start_end)) {
                            $country_season = CountriesAbouts::firstOrNew(
                                ['countries_id' => $model->id, 'title' => $title]
                            );
                            $country_season->countries_id  = $model->id;
                            $country_season->type          = 'best_time';
                            $country_season->title         = $title;
                            $country_season->body          = implode(",", $start_end);;
                            $country_season->save();
                        }
                    }
                    if (count($extra['best_time']['low_season']['low_start']) == 1) {
                        if($extra['best_time']['low_season']['low_start'][0] == null || $extra['best_time']['low_season']['low_end'][0] == null){
                            CountriesAbouts::where([['countries_id', $model->id], ['title', 'low_season']])->delete();
                        }
                    }
                    if (count($extra['best_time']['shoulder']['shoulder_start']) == 1) {
                        if($extra['best_time']['shoulder']['shoulder_start'][0] == null || $extra['best_time']['shoulder']['shoulder_end'][0] == null){
                            CountriesAbouts::where([['countries_id', $model->id], ['title', 'shoulder']])->delete();
                        }
                    }
                    if (count($extra['best_time']['high_season']['high_start']) == 1) {
                        if($extra['best_time']['high_season']['high_start'][0] == null || $extra['best_time']['high_season']['high_end'][0] == null){
                            CountriesAbouts::where([['countries_id', $model->id], ['title', 'high_season']])->delete();
                        }
                    }
                }
                if (isset($extra['daily_costs'])) {
                    foreach($extra['daily_costs'] as $key => $value) {
                        $daily_costs = CountriesAbouts::firstOrNew(
                            ['countries_id' => $model->id, 'title' => $key]
                        );
                        // dd($daily_costs && $value == null);
                        if ($daily_costs && $value == null) {
                            CountriesAbouts::where([['countries_id', $model->id], ['title', $key]])->delete();
                        } else {
                            
                            $daily_costs->countries_id  = $model->id;
                            $daily_costs->type          = 'daily_costs';
                            $daily_costs->title         = $key;
                            $daily_costs->body          = $value;
                            $daily_costs->save();
                        }
                    }
                }
                if (isset($extra['transportation'])) {
                    $transportation = CountriesAbouts::firstOrNew(['countries_id' => $model->id, 'type' => 'transportation']);
                    if (count($extra['transportation']['transports']) == 1 && $extra['transportation']['transports'][0] == null) {
                        $transportation->delete();
                    } else {
                        $transportation->countries_id  = $model->id;
                        $transportation->type          = 'transportation';
                        $transportation->title         = 'transport';
                        $transportation->body          = implode(",", $extra['transportation']['transports']);
                        $transportation->save();
                    }
                }
                if (isset($extra['sockets_plugs'])) {
                    $sockets_plugs = CountriesAbouts::firstOrNew(['countries_id' => $model->id, 'type' => 'sockets_plugs']);
                    if (empty($extra['sockets_plugs'])) {
                        $sockets_plugs->delete();
                    } else {
                        $sockets_plugs->countries_id = $model->id;
                        $sockets_plugs->type         = 'sockets_plugs';
                        $sockets_plugs->body         = implode(",", $extra['sockets_plugs']);
                        $sockets_plugs->save();
                    }
                }
                /* Add Cities in CountriesCapitals */
                if (isset($extra['cities'])) {
                    foreach ($extra['cities'] as $key => $value) {
                        CountriesCapitals::firstOrCreate(
                            ['countries_id' => $model->id, 'cities_id' => $value]
                        );
                    }
                    foreach (CountriesCapitals::where(['countries_id' => $model->id])->get() as $value) {
                        if (!in_array($value->cities_id, $extra['cities'])) {
                            $value->delete();
                        }
                    }
                } else {
                    foreach (CountriesCapitals::where(['countries_id' => $model->id])->get() as $value) {
                        $value->delete();
                    }
                }

                /* Add Numbers in CountriesEmergencyNumbers */
                if(isset($extra['emergency_numbers'])){
                    foreach ($extra['emergency_numbers'] as $key => $value) {
                        CountriesEmergencyNumbers::firstOrCreate(
                            ['countries_id' => $model->id, 'emergency_numbers_id' => $value]
                        );
                    }
                    foreach (CountriesEmergencyNumbers::where(['countries_id' => $model->id])->get() as $value) {
                        if (!in_array($value->emergency_numbers_id, $extra['emergency_numbers'])) {
                            $value->delete();
                        }
                    }
                } else {
                    foreach (CountriesEmergencyNumbers::where(['countries_id' => $model->id])->get() as $value) {
                            $value->delete();
                    }
                }

                /* Add Currencies in CountriesCurrencies */
                if(isset($extra['currencies'])){
                    foreach ($extra['currencies'] as $key => $value) {
                        CountriesCurrencies::firstOrCreate(
                            ['countries_id' => $model->id, 'currencies_id' => $value]
                        );
                    }

                    foreach (CountriesCurrencies::where(['countries_id' => $model->id])->get() as $value) {
                        if ( !in_array($value->currencies_id, $extra['currencies'])) {
                            $value->delete();
                        }
                    }
                } else {
                    foreach (CountriesCurrencies::where(['countries_id' => $model->id])->get() as $value) {
                            $value->delete();
                    }
                }

                foreach ($translations as $translation) {
                    $language = Languages::find($translation['languages_id']);
                    if ($language->code == 'en') {
                        $countryTranslation = CountriesTranslations::find($translation['id']);
                        unset($translation['id']);
                        $defaultTranslation = $countryTranslation;
                        if (!$countryTranslation->update($translation)) {
                            $check = 0;
                        }
                    } else {
                        if (null == $translation['id']) {
                            unset($translation['id']);
                            $translation['countries_id'] = $model->id;
                            $translation['languages_id'] = $language->id;
                            $translation = CountriesTranslations::create($translation)->toArray();
                            unset($translation['countries_id']);
                            unset($translation['languages_id']);
                        }
                        $languageCode = $language->code;
                        $this->dispatch(
                            (new TranslateUpdatedCountriesTrans($defaultTranslation->toArray(), $translation, $languageCode))
                                ->onQueue('translate')
                        );
                    }
                }

                if ($check) {
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }

    public static function validateUpload($extension)
    {
        $extension = strtolower($extension);

        switch ($extension) {
            case 'jpeg':
                return true;
            case 'jpg':
                return true;
                break;
            case 'png':
                return true;
                break;
            case 'gif':
                return true;
                break;
            default:
                return false;
        }
    }
}
