<?php

namespace App\Repositories\Backend\Restaurants;

use App\Models\Place\Place;
use App\Models\Restaurants\Restaurants;
use App\Models\Restaurants\RestaurantsTranslations;
use App\Models\Restaurants\RestaurantsMedias;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Models\ActivityMedia\Media;
use App\Models\Access\language\Languages;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\Backend\Restaurants\TranslateCreatedRestaurantsTrans;
use App\Jobs\Backend\Restaurants\TranslateUpdatedRestaurantsTrans;

/**
 * Class RestaurantsRepository.
 */
class RestaurantsRepository extends BaseRepository
{
    use DispatchesJobs;

    /**
     * Associated Repository Model.
     */
    const MODEL = Restaurants::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    /**
     * @param        $permissions
     * @param string $by
     *
     * @return mixed
     */
    public function getByPermission($permissions, $by = 'name')
    {
        if (! is_array($permissions)) {
            $permissions = [$permissions];
        }

        return $this->query()->whereHas('roles.permissions', function ($query) use ($permissions, $by) {
            $query->whereIn('permissions.'.$by, $permissions);
        })->get();
    }

    /**
     * @param        $roles
     * @param string $by
     *
     * @return mixed
     */
    public function getByRole($roles, $by = 'name')
    {
        if (! is_array($roles)) {
            $roles = [$roles];
        }

        return $this->query()->whereHas('roles', function ($query) use ($roles, $by) {
            $query->whereIn('roles.'.$by, $roles);
        })->get();
    }

    /**
     * @return mixed
     */
    public function getForDataTable($request)
    {
        foreach ($request->columns as $column) {
            if ($column['data'] == 'address') {
                $matchValue     = @$column['search']['value1']['matchValue'];
                $notMatchValue  = @$column['search']['value2']['notMatchValue'];
            }
            if ($column['data'] == 'pluscode') {
                $matchValue2     = @$column['search']['value1']['matchValue2'];
                $notMatchValue2  = @$column['search']['value2']['notMatchValue2'];
            }
        }

        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->with('place')
            ->select([
                config('restaurants.restaurants_table').'.id',
                config('restaurants.restaurants_table').'.lat',
                config('restaurants.restaurants_table').'.lng',
                config('restaurants.restaurants_table').'.countries_id',
                config('restaurants.restaurants_table').'.cities_id',
                config('restaurants.restaurants_table').'.active',
                config('restaurants.restaurants_table').'.place_type',
                config('restaurants.restaurants_table').'.pluscode',
                'transsingle.title',
                'transsingle.address',
                'countries_trans.title AS country_title',
                'cities_trans.title AS city_title',
            ])
            ->leftJoin(DB::raw('restaurants_trans AS transsingle FORCE INDEX (restaurants_id)'), function ($query) {
                $query->on('transsingle.restaurants_id', '=', 'restaurants.id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->leftJoin('countries_trans', function ($query) {
                $query->on('countries_trans.countries_id', '=', 'restaurants.countries_id')
                    ->where('countries_trans.languages_id', '=', 1);
            })
            ->leftJoin('cities_trans', function ($query) {
                $query->on('cities_trans.cities_id', '=', 'restaurants.cities_id')
                    ->where('cities_trans.languages_id', '=', 1);
            })
            ->when($matchValue, function ($query) use ($matchValue) {
                $this->filtered = true;
                return $query->where('transsingle.address', 'LIKE', "%" . $matchValue . "%");
            })
            ->when($notMatchValue, function ($query) use ($notMatchValue) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue);
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue) {
                        $query->where('transsingle.address', 'NOT LIKE', "%" . $notMatchValue . "%");
                    }
                });
            })
            ->when($matchValue2, function ($query) use ($matchValue2) {
                $this->filtered = true;
                return $query->where(config('restaurants.restaurants_table').'.pluscode', 'LIKE', "%" . $matchValue2 . "%");
            })
            ->when($notMatchValue2, function ($query) use ($notMatchValue2) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue2) ?: [];
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue2) {
                        $query->where(config('restaurants.restaurants_table').'.pluscode', 'NOT LIKE', "%" . $notMatchValue2 . "%");
                    }
                });
            });

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $translations
     * @param $extra
     */
    public function create($translations, $extra)
    {
        $model = new Restaurants;
        $model->countries_id    = $extra['countries_id'];
        $model->active          = $extra['active'];
        $model->cities_id       = $extra['cities_id'];
        $model->places_id       = $extra['places_id'];
        $model->place_type      = Place::find($extra['places_id'])->place_type;
        $model->lat             = $extra['lat'];
        $model->lng             = $extra['lng'];
        $model->provider_id     = \Hash::make(Carbon::now());
        
        DB::transaction(function () use ($model, $translations, $extra) {
            $check = 1;
            
            if ($model->save()) {

                $this->uploadLicensedImagesToS3(
                    $extra['license_images'],
                    $model,
                    new RestaurantsMedias(),
                    'restaurants_id',
                    'restaurants'
                );

                foreach ($translations as $translation) {
                    $translation['restaurants_id']  = $model->id;
                    $language                       = Languages::find($translation['languages_id']);
                    if ($language->code == 'en') {
                        $defaultTranslation = $translation;
                        if (!RestaurantsTranslations::insert($translation)) {
                            $check = 0;
                        }
                    } else {
                        RestaurantsTranslations::insert($translation);
//                        $this->dispatch(
//                            (new TranslateCreatedRestaurantsTrans($defaultTranslation, $translation, $language->code))
//                                ->onQueue('translate')
//                        );
                    }

                }

                if($check){
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }


    /**
     * @param $id
     * @param $model
     * @param $translations
     * @param $extra
     */
    public function update($id , $model , $translations , $extra)
    {
        $model = Restaurants::findOrFail(['id' => $id]);
        $model = $model[0];
        $model->countries_id  = $extra['countries_id'];
        $model->active        = $extra['active'];
        $model->lat           = $extra['lat'];
        $model->lng           = $extra['lng'];
        $model->cities_id     = $extra['cities_id'];
        $model->places_id     = $extra['places_id'];

        DB::transaction(function () use ($model, $translations, $extra) {
            $check = 1;
            
            if ($model->save()) {

                if (!empty($extra['license_images']['deleted'])) {
                    foreach ($extra['license_images']['deleted'] as $media_id) {
                        $media = Media::find($media_id);
                        unset($extra['license_images']['images'][$media_id]);
                        $this->deleteFromS3($media->url);
                        RestaurantsMedias::where(['restaurants_id' => $model->id, 'medias_id' => $media_id])->delete();
                        $media->delete();
                    }
                }

                foreach ($extra['license_images']['images'] as $key => $image) {
                    if ( $key > 0 ) {
                        Media::find($key)->update($image);
                        unset($extra['license_images']['images'][$key]);
                    }
                }

                $this->uploadLicensedImagesToS3(
                    $extra['license_images']['images'],
                    $model,
                    new RestaurantsMedias(),
                    'restaurants_id',
                    'restaurants'
                );

                foreach ($translations as $translation) {
                    $language = Languages::find($translation['languages_id']);
//                    unset($translation['languages_id']);
                    if ($language->code == 'en') {
                        $defaultTranslation = $translation;
                        if (!RestaurantsTranslations::find($translation['id'])->update($translation)) {
                            $check = 0;
                        }
                    } else {

                        RestaurantsTranslations::find($translation['id'])->update($translation);

//                        if (null == $translation->id) {
//                            unset($translation['id']);
//                            $translation['restaurants_id'] = $model->id;
//                            $translation['languages_id'] = $language->id;
//                            $translation = RestaurantsTranslations::create($translation)->toArray();
//                            unset($translation['restaurants_id']);
//                            unset($translation['languages_id']);
//                        }
//                        $this->dispatch(
//                            (new TranslateUpdatedRestaurantsTrans($defaultTranslation, $translation, $language->code))
//                                ->onQueue('translate')
//                        );
                    }
                }

                if($check){
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }

    public static function validateUpload($extension) {
        $extension = strtolower($extension);

        switch($extension){
            case 'jpeg':
                return true;
            case 'jpg':
                return true;
                break;
            case 'png':
                return true;
                break;
            case 'gif':
                return true;
                break;
            default:
                return false;
        }
    }
}
