<?php

namespace App\Repositories\Backend\Embassies;

/* Embassies Models Start */

use App\Models\Embassies\Embassies;
use App\Models\Embassies\EmbassiesTranslations;
/* Embassies Models End */

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Models\Embassies\EmbassiesMedias;
use App\Models\ActivityMedia\Media;
use App\Models\Access\language\Languages;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\Backend\Embassies\TranslateCreatedEmbassiesTrans;
use App\Jobs\Backend\Embassies\TranslateUpdatedEmbassiesTrans;


/**
 * Class EmbassiesRepository.
 */
class EmbassiesRepository extends BaseRepository
{
    use DispatchesJobs;

    /**
     * Associated Repository Model.
     */
    const MODEL = Embassies::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    /**
     * @param        $permissions
     * @param string $by
     *
     * @return mixed
     */
    public function getByPermission($permissions, $by = 'name')
    {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }

        return $this->query()->whereHas('roles.permissions', function ($query) use ($permissions, $by) {
            $query->whereIn('permissions.' . $by, $permissions);
        })->get();
    }

    /**
     * @param        $roles
     * @param string $by
     *
     * @return mixed
     */
    public function getByRole($roles, $by = 'name')
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }

        return $this->query()->whereHas('roles', function ($query) use ($roles, $by) {
            $query->whereIn('roles.' . $by, $roles);
        })->get();
    }

    /**
     * @return mixed
     */
    public function getForDataTable($request)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        foreach ($request->columns as $column) {
            if ($column['data'] == 'address') {
                $matchValue     = @$column['search']['value1']['matchValue'];
                $notMatchValue  = @$column['search']['value2']['notMatchValue'];
            }
            if ($column['data'] == 'pluscode') {
                $matchValue2     = @$column['search']['value1']['matchValue2'];
                $notMatchValue2  = @$column['search']['value2']['notMatchValue2'];
            }
        }

        $dataTableQuery = $this->query()
            ->select([
                config('embassies.embassies_table') . '.id',
                config('embassies.embassies_table') . '.lat',
                config('embassies.embassies_table') . '.lng',
                config('embassies.embassies_table') . '.cities_id',
                config('embassies.embassies_table') . '.location_country_id',
                config('embassies.embassies_table') . '.active',
                config('embassies.embassies_table') . '.place_type',
                config('embassies.embassies_table') . '.pluscode',

                'transsingle.title',
                'transsingle.address',
                'cities_trans.title AS city_title',
                'countries_trans.title AS country_title'
            ])
            ->leftJoin(DB::raw('embassies_trans AS transsingle FORCE INDEX (embassies_id)'), function ($query) {
                $query->on('transsingle.embassies_id', '=', 'embassies.id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->leftJoin('countries AS country', function ($query) {
                $query->on('country.id', '=', 'embassies.location_country_id');
            })
            ->Leftjoin('countries_trans', function ($query) {
                $query->on('countries_trans.countries_id', '=', 'location_country_id')
                    ->where('countries_trans.languages_id', '=', 1);
            })
            ->leftJoin('cities AS city', function ($query) {
                $query->on('city.id', '=', 'embassies.cities_id');
            })
            ->Leftjoin('cities_trans', function ($query) {
                $query->on('cities_trans.cities_id', '=', 'city.id')
                    ->where('cities_trans.languages_id', '=', 1);
            })
            ->when($matchValue, function ($query) use ($matchValue) {
                $this->filtered = true;
                return  $query->where('transsingle.address', 'LIKE', "%".$matchValue."%");

            })
            ->when($notMatchValue, function ($query) use ($notMatchValue) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue);
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue) {
                        $query->where('transsingle.address', 'NOT LIKE', "%" . $notMatchValue . "%");
                    }
                });
            })
            ->when($matchValue2, function ($query) use ($matchValue2) {
                $this->filtered = true;
                return $query->where(config('embassies.embassies_table').'.pluscode', 'LIKE', "%" . $matchValue2 . "%");
            })
            ->when($notMatchValue2, function ($query) use ($notMatchValue2) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue2) ?: [];
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue2) {
                        $query->where(config('embassies.embassies_table').'.pluscode', 'NOT LIKE', "%" . $notMatchValue2 . "%");
                    }
                });
            });

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /**
     * @param $translations
     * @param $extra
     */
    public function create($translations, $extra)
    {
        $model = new Embassies;
        $model->location_country_id = $extra['location_country_id'];
        $model->cities_id = $extra['cities_id'];
        $model->active = $extra['active'];
        $model->lat = $extra['lat'];
        $model->lng = $extra['lng'];
        $model->provider_id = 0;

        DB::transaction(function () use ($model, $translations, $extra) {

            if ($model->save()) {

                $check = 1;

                $this->uploadLicensedImagesToS3(
                    $extra['license_images'],
                    $model,
                    new EmbassiesMedias(),
                    'embassies_id',
                    'embassies'
                );


                foreach ($translations as $translation) {
                    $language = Languages::find($translation['languages_id']);

                    $translation['embassies_id'] = $model->id;
                    if ($language->code == 'en') {
                        $defaultTranslation = $translation;
                        if (!EmbassiesTranslations::create($translation)) {
                            $check = 0;
                        }
                    } else {
                        EmbassiesTranslations::create($translation);
//                        $this->dispatch(
//                            (new TranslateCreatedEmbassiesTrans($defaultTranslation, $translation, $language->code))
//                                ->onQueue('translate')
//                        );
                    }
                }

                if ($check) {
                    return true;
                }
            }
            throw new GeneralException('Unexpected Error Occured!');
        });
    }


    /**
     * @param $id
     * @param $model
     * @param $translations
     * @param $extra
     */
    public function update($id, $model, $translations, $extra)
    {
        $model->origin_country_id = $extra['origin_country_id'];
        $model->location_country_id = $extra['location_country_id'];
        $model->cities_id = $extra['cities_id'];
        $model->active = $extra['active'];
        $model->lat = $extra['lat'];
        $model->lng = $extra['lng'];
        $model->provider_id = 0;

        DB::transaction(function () use ($model, $translations, $extra) {
            $check = 1;

            if ($model->save()) {

                if (!empty($extra['license_images']['deleted'])) {
                    foreach ($extra['license_images']['deleted'] as $media_id) {
                        $media = Media::find($media_id);
                        unset($extra['license_images']['images'][$media_id]);
                        $this->deleteFromS3($media->url);
                        EmbassiesMedias::where(['embassies_id' => $model->id, 'medias_id' => $media_id])->delete();
                        $media->delete();
                    }
                }

                foreach ($extra['license_images']['images'] as $key => $image) {
                    if ($key > 0) {
                        Media::find($key)->update($image);
                        unset($extra['license_images']['images'][$key]);
                    }
                }

                    $this->uploadLicensedImagesToS3(
                    $extra['license_images']['images'],
                    $model,
                    new EmbassiesMedias(),
                    'embassies_id',
                    'embassies'
                );

                foreach ($translations as $translation) {
                    $language = Languages::find($translation['languages_id']);
//                    unset($translation['languages_id']);

                    $defaultTranslation = $translation;

                    if ($language->code == 'en') {
                        if (!EmbassiesTranslations::find($translation['id'])->update($translation)) {
                            $check = 0;
                        }
                    } else {

                        if (null == $translation['id']) {
                            unset($translation['id']);
                            $translation['embassies_id'] = $model->id;
                            $translation['languages_id'] = $language->id;
                            $translation = EmbassiesTranslations::create($translation)->toArray();
                            unset($translation['embassies_id']);
                            unset($translation['languages_id']);
                        } else {
                            EmbassiesTranslations::find($translation['id'])->update($translation);
                        }
//                        $this->dispatch(
//                            (new TranslateUpdatedEmbassiesTrans($defaultTranslation, $translation, $language->code))
//                                ->onQueue('translate')
//                        );
                    }
                }

                if($check){
                    return true;
                }
            }

            throw new GeneralException('Unexpected Error Occured!');
        });
    }

    public static function validateUpload($extension)
    {
        $extension = strtolower($extension);

        switch ($extension) {
            case 'jpeg':
                return true;
            case 'jpg':
                return true;
                break;
            case 'png':
                return true;
                break;
            case 'gif':
                return true;
                break;
            default:
                return false;
        }
    }
}
