<?php

namespace App\Events;

use App\Models\Chat\ChatConversationMessage;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UpdatePlanChatsEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var int
     */
    private $planId;

    /**
     * UpdatePlanChatsEvent constructor.
     * @param $userId
     * @param $planId
     */
    public function __construct($userId, $planId)
    {
        $this->userId = $userId;
        $this->planId = $planId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|arrayx
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat-channel.' . $this->userId);
    }

    public function broadcastWith()
    {
        return [
            'data' => $this->planId,
        ];
    }
}
