<?php

namespace App\Events\PlanMedia;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripMediaLikeEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $mediaId;

    /**
     * TripMediaLikeEvent constructor.
     * @param $mediaId
     */
    public function __construct($mediaId)
    {
        $this->mediaId = $mediaId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('plan-media-like');
    }

    public function broadcastWith()
    {
        return [
            'media_id' => $this->mediaId
        ];
    }
}
