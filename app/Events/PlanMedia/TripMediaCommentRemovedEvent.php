<?php

namespace App\Events\PlanMedia;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripMediaCommentRemovedEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $commentId;

    /**
     * @var int
     */
    private $mediaId;

    /**
     * TripMediaCommentRemovedEvent constructor.
     * @param int $commentId
     * @param int $mediaId
     */
    public function __construct($commentId, $mediaId)
    {
        $this->commentId = $commentId;
        $this->mediaId = $mediaId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('plan-media-comment-removed');
    }

    public function broadcastWith()
    {
        return [
            'comment_id' => $this->commentId,
            'media_id' => $this->mediaId,
        ];
    }
}
