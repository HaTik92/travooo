<?php

namespace App\Events\PlanMedia;

use App\Models\ActivityMedia\MediasComments;
use App\Services\Medias\MediaCommentsService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripMediaCommentChangedEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $commentId;

    /**
     * TripMediaCommentChangedEvent constructor.
     * @param int $commentId
     */
    public function __construct($commentId)
    {
        $this->commentId = $commentId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('plan-media-comment-changed');
    }

    public function broadcastWith()
    {
        $comment = MediasComments::find($this->commentId);

        if (!$comment) {
            return [
                'comment' => ''
            ];
        }

        /** @var MediaCommentsService $mediaCommentsService */
        $mediaCommentsService = app(MediaCommentsService::class);

        return [
            'comment' => $comment,
            'comment_view' => (string) $mediaCommentsService->renderComment($comment),
        ];
    }
}
