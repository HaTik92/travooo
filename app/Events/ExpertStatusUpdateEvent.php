<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ExpertStatusUpdateEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var bool
     */
    public $status;

    /**
     * @var int
     */
    public $points;

    /**
     * Create a new event instance.
     *
     * @param $user_id
     * @param $status
     * @param int $points
     */
    public function __construct($user_id, $status, $points = 0)
    {
        //
        $this->user_id = $user_id;
        $this->status = $status;
        $this->points = $points;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('notifications-channel.' . $this->user_id);
    }

    public function broadcastWith()
    {
        return [
            'data' => [
                'status' => $this->status,
                'points' => $this->points
            ]
        ];
    }
}
