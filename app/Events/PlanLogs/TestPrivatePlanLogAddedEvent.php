<?php

namespace App\Events\PlanLogs;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TestPrivatePlanLogAddedEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('new-test-channel.' . ($this->user_id));
    }

    public function broadcastAs()
    {
        return 'new-test-test';
    }

    public function broadcastWith()
    {
        return [
            'msg' => 'this is private channel only for testing',
            'time' => time()
        ];
    }
}
