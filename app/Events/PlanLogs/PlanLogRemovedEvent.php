<?php

namespace App\Events\PlanLogs;

use App\Models\TripPlans\PlanActivityLog;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class PlanLogRemovedEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    protected $userId;

    /**
     * @var array
     */
    protected $log;

    /**
     * @var string
     */
    protected $time;

    /**
     * @var string
     */
    protected $updatedContent;

    /**
     * PlanLogAddedEvent constructor.
     * @param $userId
     * @param PlanActivityLog $log
     * @param $updatedContent
     */
    public function __construct($userId, $log, $updatedContent)
    {
        $this->userId = $userId;
        $this->log = $log->toArray();
        $this->updatedContent = $updatedContent;
        $this->time = diffForHumans($log->created_at);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('plan-logs-channel.' . $this->userId);
    }

    public function broadcastWith()
    {
        return [
            'log' => $this->log,
            'updated_content' => $this->updatedContent,
            'time' => $this->time
        ];
    }
}
