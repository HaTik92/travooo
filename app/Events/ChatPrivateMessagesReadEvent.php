<?php

namespace App\Events;

use App\Models\Chat\ChatConversationMessage;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ChatPrivateMessagesReadEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $userId;
    public $fromId;
    public $message;

    public function __construct($userId, $fromId, $message)
    {
        //
        $this->userId = $userId;
        $this->fromId = $fromId;
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat-channel.' . $this->userId);
    }

    public function broadcastWith()
    {
        return [
            'data' => $this->message,
            'from_id' => $this->fromId
        ];
    }
}
