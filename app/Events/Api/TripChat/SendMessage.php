<?php

namespace App\Events\Api\TripChat;

use App\Models\Chat\ChatConversationMessage;
use App\Models\User\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Log;

class SendMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $user_id;
    public $message;
    public $select_conversation_id;
    public $fromSrc;

    public function __construct($user_id, ChatConversationMessage $message, $select_conversation_id = null)
    {
        $this->user_id = $user_id;
        $this->message = $message;
        $this->select_conversation_id = $select_conversation_id;
        $this->fromSrc = check_profile_picture(User::find($message->from_id)->profile_picture);

        // Log::useDailyFiles(storage_path() . '/logs/chat_list.log');
        // Log::debug('broadcast send-message : ', [
        //     'data'                      => $this->message,
        //     'from_src'                  => $this->fromSrc,
        //     'select_conversation_id'    => $this->select_conversation_id
        // ], PHP_EOL . PHP_EOL);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|arrayx
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat-channel.' . $this->user_id);
    }

    public function broadcastAs()
    {
        return 'send-message';
    }

    public function broadcastWith()
    {
        return [
            'data'                      => $this->message,
            'from_src'                  => $this->fromSrc,
            'select_conversation_id'    => $this->select_conversation_id
        ];
    }
}
