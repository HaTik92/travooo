<?php

namespace App\Events\Api\TripChat;

use App\Models\Chat\ChatConversation;
use App\Models\Chat\ChatConversationMessage;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Support\Facades\Log;

class ChatNewConversationApiEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $sender_id;
    public $user_id;
    public $conversation;

    public function __construct($sender_id, $user_id, ChatConversation $conversation)
    {
        $this->sender_id = $sender_id;
        $this->user_id = $user_id;

        $conversation->messages = ChatConversationMessage::where('chat_conversation_id', $conversation->id)->where('to_id', $user_id)->get();
        $this->conversation = $conversation;

        // Log::useDailyFiles(storage_path() . '/logs/chat_list.log');
        // Log::debug('broadcast new-chat-conversation : ', [
        //     'from_id' => $this->sender_id,
        //     'to_id' => $this->user_id,
        //     'conversation' => $this->conversation,
        // ], PHP_EOL . PHP_EOL);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat-channel.' . $this->user_id);
    }

    public function broadcastAs()
    {
        return 'new-chat-conversation';
    }

    public function broadcastWith()
    {
        return  [
            'from_id' => $this->sender_id,
            'to_id' => $this->user_id,
            'conversation' => $this->conversation,
        ];
    }
}
