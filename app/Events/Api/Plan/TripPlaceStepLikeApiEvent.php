<?php

namespace App\Events\Api\Plan;

use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripPlaceStepLikeApiEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $tripPlaceId;

    /**
     * TripPlaceStepLikeApiEvent constructor.
     * @param $tripPlaceId
     */
    public function __construct($tripPlaceId)
    {
        $this->tripPlaceId = $tripPlaceId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trip-place');
    }

    public function broadcastAs()
    {
        return 'like-unlike';
    }

    public function broadcastWith()
    {
        $tripsSuggestionsService = app(TripsSuggestionsService::class);

        return [
            'trip_place_id' => $tripsSuggestionsService->getLastTripPlace($this->tripPlaceId)
        ];
    }
}
