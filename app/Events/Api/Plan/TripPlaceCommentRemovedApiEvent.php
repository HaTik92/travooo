<?php

namespace App\Events\Api\Plan;

use App\Models\TripPlaces\TripPlacesComments;
use App\Services\Trips\TripPlaceCommentsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripPlaceCommentRemovedApiEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $commentId;

    /**
     * @var int
     */
    private $tripPlaceId;

    /**
     * @var int
     */
    private $userId;

    /**
     * TripPlaceCommentRemovedApiEvent constructor.
     * @param int $commentId
     * @param int $tripPlaceId
     */
    public function __construct($commentId, $tripPlaceId, $userId)
    {
        $this->commentId = $commentId;
        $this->tripPlaceId = $tripPlaceId;
        $this->userId = $userId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trip-place-comment');
    }

    public function broadcastAs()
    {
        return 'removed-comment';
    }

    public function broadcastWith()
    {
        $tripsSuggestionsService = app(TripsSuggestionsService::class);

        return [
            'comment_id' => $this->commentId,
            'trip_place_id' => $tripsSuggestionsService->getLastTripPlace($this->tripPlaceId),
            'user_id' => $this->userId
        ];
    }
}
