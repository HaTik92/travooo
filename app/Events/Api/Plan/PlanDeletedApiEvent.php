<?php

namespace App\Events\Api\Plan;

use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class PlanDeletedApiEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $planId;

    public function __construct($planId)
    {
        $this->planId = $planId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('plan-deleted');
    }

    public function broadcastAs()
    {
        return 'plan-deleted-by-user';
    }

    public function broadcastWith()
    {
        return [
            'plan_id' => $this->planId
        ];
    }
}
