<?php

namespace App\Events\Api\Plan;

use App\Models\TripPlaces\TripPlacesComments;
use App\Services\Trips\TripPlaceCommentsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripPlaceCommentAddedApiEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $commentId;

    /**
     * TripPlaceCommentAddedApiEvent constructor.
     * @param int $commentId
     */
    public function __construct($commentId)
    {
        $this->commentId = $commentId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trip-place-comment');
    }

    public function broadcastAs()
    {
        return 'comment';
    }

    public function broadcastWith()
    {
        $comment = TripPlacesComments::find($this->commentId);

        if (!$comment) {
            return [
                'comment' => null,
                'trip_place_id' => null
            ];
        }

        /** @var TripPlaceCommentsService $tripPlaceCommentsService */
        $tripPlaceCommentsService = app(TripPlaceCommentsService::class);
        /** @var TripsSuggestionsService $tripsSuggestionsService */
        $tripsSuggestionsService = app(TripsSuggestionsService::class);

        return [
            'comment' => $comment,
            'trip_place_id' => $tripsSuggestionsService->getLastTripPlace($comment->trip_place_id)
        ];
    }
}
