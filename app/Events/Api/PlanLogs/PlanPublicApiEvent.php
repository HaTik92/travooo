<?php

namespace App\Events\Api\PlanLogs;

use App\Models\TripPlans\PlanActivityLog;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class PlanPublicApiEvent implements ShouldBroadcastNow
{
    /**
     * @var array
     */
    protected $log;
    /**
     * @var bool
     */
    private $hardRefresh;

    /**
     * @var string
     */
    protected $time;

    /**
     * PlanPublicApiEvent constructor.
     * @param PlanActivityLog $log
     * @param bool $hardRefresh
     */
    public function __construct($log, $hardRefresh = false)
    {
        $this->log = $log->toArray();
        $this->time = diffForHumans($log->created_at);
        $this->hardRefresh = $hardRefresh;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('plan-logs-public-channel');
    }

    public function broadcastAs()
    {
        return getTripPlanLogEvent($this->log->type);
    }

    public function broadcastWith()
    {
        return [
            'log' => $this->log,
            'hard_refresh' => $this->hardRefresh,
            'time' => $this->time
        ];
    }
}
