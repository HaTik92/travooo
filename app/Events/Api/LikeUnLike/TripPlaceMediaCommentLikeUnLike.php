<?php

namespace App\Events\Api\LikeUnLike;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripPlaceMediaCommentLikeUnLike implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $tripPlaceMediaId;

    public function __construct($tripPlaceMediaId)
    {
        $this->tripPlaceMediaId = $tripPlaceMediaId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trip-place-media');
    }

    public function broadcastAs()
    {
        return 'like-unlike';
    }

    public function broadcastWith()
    {
        return [
            'comment_id' => $this->tripPlaceMediaId
        ];
    }
}
