<?php

namespace App\Events\Api\LikeUnLike;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripPlaceCommentLikeUnlike implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $trip_place_id;

    public function __construct($trip_place_id)
    {
        $this->trip_place_id = $trip_place_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trip-place-comment');
    }

    public function broadcastAs()
    {
        return 'like-unlike';
    }

    public function broadcastWith()
    {
        return [
            'comment_id' => $this->trip_place_id
        ];
    }
}
