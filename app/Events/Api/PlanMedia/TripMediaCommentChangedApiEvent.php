<?php

namespace App\Events\Api\PlanMedia;

use App\Models\ActivityMedia\MediasComments;
use App\Services\Medias\MediaCommentsService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripMediaCommentChangedApiEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $commentId;

    /**
     * TripMediaCommentChangedApiEvent constructor.
     * @param int $commentId
     */
    public function __construct($commentId)
    {
        $this->commentId = $commentId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trip-place-media');
    }

    public function broadcastAs()
    {
        return 'reply-comment-media';
    }

    public function broadcastWith()
    {
        $comment = MediasComments::find($this->commentId);

        if (!$comment) {
            return [
                'comment' => null
            ];
        }
        return [
            'comment' => $comment,
        ];
    }
}
