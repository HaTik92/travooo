<?php

namespace App\Events\Api\PlanMedia;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripMediaCommentRemovedApiEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $commentId;

    /**
     * @var int
     */
    private $mediaId;

    /**
     * TripMediaCommentRemovedApiEvent constructor.
     * @param int $commentId
     * @param int $mediaId
     */
    public function __construct($commentId, $mediaId)
    {
        $this->commentId = $commentId;
        $this->mediaId = $mediaId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trip-place-media');
    }

    public function broadcastAs()
    {
        return 'removed-comment-media';
    }

    public function broadcastWith()
    {
        return [
            'comment_id' => $this->commentId,
            'media_id' => $this->mediaId,
        ];
    }
}
