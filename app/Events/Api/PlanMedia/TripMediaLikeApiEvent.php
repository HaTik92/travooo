<?php

namespace App\Events\Api\PlanMedia;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripMediaLikeApiEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $mediaId;

    /**
     * TripMediaLikeApiEvent constructor.
     * @param $mediaId
     */
    public function __construct($mediaId)
    {
        $this->mediaId = $mediaId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trip-place-media');
    }

    public function broadcastAs()
    {
        return 'like-unlike';
    }

    public function broadcastWith()
    {
        return [
            'media_id' => $this->mediaId
        ];
    }
}
