<?php

namespace App\Events\Plan;

use App\Models\TripPlaces\TripPlacesComments;
use App\Services\Trips\TripPlaceCommentsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripPlaceCommentRemovedEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $commentId;

    /**
     * @var int
     */
    private $tripPlaceId;

    /**
     * @var int
     */
    private $userId;

    /**
     * TripPlaceCommentRemovedEvent constructor.
     * @param int $commentId
     * @param int $tripPlaceId
     */
    public function __construct($commentId, $tripPlaceId)
    {
        $this->commentId = $commentId;
        $this->tripPlaceId = $tripPlaceId;
        $this->userId = auth()->id();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('plan-trip-place-comment-removed');
    }

    public function broadcastWith()
    {
        $tripsSuggestionsService = app(TripsSuggestionsService::class);

        return [
            'comment_id' => $this->commentId,
            'trip_place_id' => $tripsSuggestionsService->getLastTripPlace($this->tripPlaceId),
            'user_id' => $this->userId
        ];
    }
}
