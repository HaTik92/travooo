<?php

namespace App\Events\Plan;

use App\Models\TripPlaces\TripPlacesComments;
use App\Services\Trips\TripPlaceCommentsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class TripPlaceCommentChangedEvent implements ShouldBroadcastNow
{
    /**
     * @var int
     */
    private $commentId;

    /**
     * TripPlaceCommentChangedEvent constructor.
     * @param int $commentId
     */
    public function __construct($commentId)
    {
        $this->commentId = $commentId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('plan-trip-place-comment-changed');
    }

    public function broadcastWith()
    {
        $comment = TripPlacesComments::find($this->commentId);

        if (!$comment) {
            return [
                'comment' => ''
            ];
        }

        /** @var TripPlaceCommentsService $tripPlaceCommentsService */
        $tripPlaceCommentsService = app(TripPlaceCommentsService::class);
        /** @var TripsSuggestionsService $tripsSuggestionsService */
        $tripsSuggestionsService = app(TripsSuggestionsService::class);

        return [
            'comment' => $comment,
            'comment_view' => (string) $tripPlaceCommentsService->renderComment($comment),
            'trip_place_id' => $tripsSuggestionsService->getLastTripPlace($comment->trip_place_id)
        ];
    }
}
