<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoadedFeed extends Model
{
    public $guarded = [];
    public $timestamps = false;
}
