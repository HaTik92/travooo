<?php

namespace App\Services\Api;

use App\Events\Api\Plan\TripPlaceStepLikeApiEvent;
use App\Events\Api\PlanMedia\TripMediaLikeApiEvent;
use App\Events\Plan\TripPlaceStepLikeEvent;
use App\Events\PlanMedia\TripMediaLikeEvent;
use App\Models\ActivityMedia\Media;
use App\Models\ActivityMedia\MediasLikes;
use App\Models\Discussion\Discussion;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsLikes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Reports\ReportsLikes;
use App\Models\TripPlans\TripsLikes;
use App\Models\Events\EventsLikes;
use App\Models\Events\Events;
use App\Models\Reports\Reports;
use App\Models\Reviews\Reviews;
use App\Models\Reviews\ReviewsVotes;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripContentPostLike;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsMediasShares;
use App\Models\TripPlans\TripsPlacesShares;
use App\Models\TripPlans\TripsShares;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\Api\ShareService;
use App\Models\User\User;
use App\Services\Ranking\RankingService;

class PrimaryPostService
{

    private $rankingService;

    public function __construct(RankingService $rankingService)
    {
        $this->rankingService = $rankingService;
    }

    const DEFAULT_PER_PAGE = 10;

    // Type of Comment
    const TYPE_POST         = "post";   // General post (text , media , check-in) +  External or internal links(others) + share
    const TYPE_TRIP         = "trip";
    const TYPE_EVENT        = "event";
    const TYPE_REPORT       = "report"; // Travlog
    const TYPE_DISCUSSION   = "discussion";
    const TYPE_REVIEW       = "review"; // Place

    const TYPE_MAPPING = [
        self::TYPE_POST,
        self::TYPE_TRIP,
        self::TYPE_EVENT,
        self::TYPE_REPORT,
        self::TYPE_DISCUSSION,
        self::TYPE_REVIEW,
    ];

    const LIKE_TYPE_MAPPING = [
        self::TYPE_POST,
        self::TYPE_TRIP,
        self::TYPE_EVENT,
        self::TYPE_REPORT,
        self::TYPE_REVIEW,
    ];

    const PRIVACY_MAPPING = [
        self::TYPE_POST,
        self::TYPE_TRIP
    ];

    const REPORT_MAPPING = [
        self::TYPE_POST,
        self::TYPE_TRIP
    ];

    // Secondary 

    const SECONDARY_VIDEO_YOU_MIGHT_LIKE        = 'video_you_might_like';
    const SECONDARY_TRENDING_DESTINATIONS       = 'trending_destinations';
    const SECONDARY_DISCOVER_NEW_TRAVELLERS     = 'discover_new_travellers';
    const SECONDARY_PLACES_YOU_MIGHT_LIKE       = 'places_you_might_like';
    const SECONDARY_COLLECTIVE_FOLLOWERS_PLACES = 'collective_followers_places';
    const SECONDARY_RECOMMENDED_PLACES          = 'recommended_places';
    const SECONDARY_NEW_FOLLOWER                = 'new_follower';
    const SECONDARY_CHECKIN                     = 'checkin';
    const SECONDARY_WEATHER_FORECAST            = 'weather-forecast';
    const SECONDARY_TOP_PLACES                  = 'top_places';
    const SECONDARY_NEW_PEOPLE                  = 'new_people';
    const SECONDARY_TRENDING_EVENTS             = 'trending_events';
    const SECONDARY_HOLIDAY                     = 'holiday';

    const SECONDARY_MAPPING = [
        self::SECONDARY_VIDEO_YOU_MIGHT_LIKE,
        self::SECONDARY_TRENDING_DESTINATIONS,
        self::SECONDARY_DISCOVER_NEW_TRAVELLERS,
        self::SECONDARY_PLACES_YOU_MIGHT_LIKE,
        self::SECONDARY_COLLECTIVE_FOLLOWERS_PLACES,
        self::SECONDARY_RECOMMENDED_PLACES,
        self::SECONDARY_NEW_FOLLOWER,
        self::SECONDARY_CHECKIN,
        self::SECONDARY_WEATHER_FORECAST,
        self::SECONDARY_TOP_PLACES,
        self::SECONDARY_NEW_PEOPLE,
        self::SECONDARY_TRENDING_EVENTS,
        self::SECONDARY_HOLIDAY,
    ];

    const SECONDARY_SLIDER_MAPPING = [
        self::SECONDARY_VIDEO_YOU_MIGHT_LIKE,
        self::SECONDARY_TRENDING_DESTINATIONS,
        self::SECONDARY_PLACES_YOU_MIGHT_LIKE,
        self::SECONDARY_NEW_PEOPLE,
    ];

    // End Secondary 

    // Find Any Primary Post Like Post, Event etc...
    public function find($type, $id, $withAuth = false)
    {
        $feed = null;
        $user = Auth::user();
        $type = trim(strtolower($type));

        $userFieldName = 'users_id';
        if (in_array($type, ShareService::TYPE_MAPPING_TRIP_SHARE)) {
            $userFieldName = 'user_id';
        }
        switch ($type) {
            case 'profile':
                return User::find($id);
                break;

            case self::TYPE_POST:
                $feed = Posts::where('id', $id);
                break;

            case self::TYPE_TRIP:
                $feed = TripPlans::where('id', $id);
                break;

            case self::TYPE_REPORT:
                $feed = Reports::where('id', $id);
                break;

            case self::TYPE_EVENT:
                $feed = Events::where('id', $id);
                break;

            case self::TYPE_DISCUSSION:
                $feed = Discussion::where('id', $id);
                break;

                // Trips Sharing
            case ShareService::TYPE_TRIP_SHARE:
                $feed = TripsShares::where('id', $id);
                break;

            case ShareService::TYPE_TRIP_PLACE_SHARE:
                $feed = TripsPlacesShares::where('id', $id);
                break;

            case ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE:
                $feed = TripsMediasShares::where('id', $id);
                break;
        }

        if (is_bool($withAuth) && $withAuth == true) {
            $feed = $feed->where($userFieldName, $user->id);
        } else if (is_int($withAuth) && $withAuth != 0) {
            $feed = $feed->where($userFieldName, $withAuth);
        }

        if ($feed != null) {
            return $feed->first();
        } else {
            return null;
        }
    }

    // Load More Likes of Any Primary Post Like Event Likes etc...
    public function loadMoreLikes($type, $id, $perPage = self::DEFAULT_PER_PAGE)
    {
        // checkins_likes
        switch ($type) {
            case self::TYPE_POST:
                return PostsLikes::with(['user'])
                    ->where('posts_id', $id)
                    ->orderBy('created_at', 'DESC')
                    ->paginate($perPage);
                break;

            case self::TYPE_TRIP:
                return TripsLikes::with(['user'])
                    ->where('trips_id', $id)
                    ->orderBy('created_at', 'DESC')
                    ->paginate($perPage);
                break;

            case self::TYPE_EVENT:
                return EventsLikes::with(['user'])
                    ->where('events_id', $id)
                    ->orderBy('created_at', 'DESC')
                    ->paginate($perPage);
                break;

            case self::TYPE_REPORT:
                return ReportsLikes::with(['user', 'report'])
                    ->where('reports_id', $id)
                    ->orderBy('created_at', 'DESC')
                    ->paginate($perPage);
                break;

            case self::TYPE_REVIEW:
                return ReviewsVotes::with(['user'])
                    ->where('review_id', $id)
                    ->where('vote_type', 1)
                    ->orderBy('created_at', 'DESC')
                    ->paginate($perPage);
                break;
        }
        return [];
    }

    public function likeOrUnlike($type, $post_id, $request)
    {
        $likeOrNotFlag = false;
        $user = Auth::user();
        switch ($type) {
            case self::TYPE_POST:
                $post = Posts::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('Post not found');

                $check_like_exists = PostsLikes::where('posts_id', $post->id)->where('users_id', $user->id)->first();
                DB::beginTransaction();
                if ($check_like_exists) {
                    $check_like_exists->delete();
                    log_user_activity('Post', 'unlike', $post->id);
                    unnotify($post->users_id, 'status_like', $post->id);
                } else {
                    $likeOrNotFlag = true;
                    $like = new PostsLikes;
                    $like->posts_id = $post->id;
                    $like->users_id = $user->id;
                    $like->save();
                    log_user_activity('Post', 'like', $post->id);

                    if ($user->id != $post->users_id) {
                        notify($post->users_id, 'status_like', $post->id);
                    }
                }
                DB::commit();
                return [
                    'post_id'           => $post_id,
                    'like_status'       => $likeOrNotFlag,
                    'total_like'        => PostsLikes::where('posts_id', $post->id)->count()
                ];
                break;

            case self::TYPE_TRIP:
                $post = TripPlans::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('Trip not found');

                $check_like_exists = TripsLikes::where('trips_id', $post->id)->where('users_id', $user->id)->first();
                if (is_object($check_like_exists)) {
                    if (isset($check_like_exists->trip)) {
                        $this->rankingService->subPointsFromEarners($check_like_exists->trip);
                    }
                    $check_like_exists->delete();
                    log_user_activity('Trip', 'unlike', $post_id);
                } else {
                    $likeOrNotFlag = true;
                    $like = new TripsLikes;
                    $like->trips_id = $post_id;
                    $like->users_id = $user->id;
                    $like->places_id = null;
                    $like->save();
                    if (isset($like->trip)) {
                        $this->rankingService->addPointsToEarners($like->trip);
                    }
                    log_user_activity('Trip', 'like', $post_id);
                }

                return [
                    'post_id'           => $post_id,
                    'like_status'    => $likeOrNotFlag,
                    'total_like'     => TripsLikes::where('trips_id', $post->id)->whereNull('places_id')->count()
                ];
                break;

            case self::TYPE_REPORT:
                $post = Reports::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('Report not found');

                $check_like_exists = ReportsLikes::where('reports_id', $post_id)->where('users_id', $user->id)->first();
                if ($check_like_exists) {
                    if (isset($check_like_exists->report)) {
                        $this->rankingService->subPointsFromEarners($check_like_exists->report);
                    }
                    $check_like_exists->delete();
                    log_user_activity('Report', 'unlike', $post_id);
                } else {
                    $likeOrNotFlag = true;
                    $like = new ReportsLikes;
                    $like->reports_id = $post_id;
                    $like->users_id = $user->id;
                    $like->save();
                    if (isset($like->report)) {
                        $this->rankingService->addPointsToEarners($like->report);
                    }
                    log_user_activity('Report', 'like', $post_id);
                }
                return [
                    'post_id'           => $post_id,
                    'like_status'   => $likeOrNotFlag,
                    'total_like'    => ReportsLikes::where('reports_id', $post_id)->count()
                ];
                break;


            case self::TYPE_EVENT:
                $post = Events::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('Event not found');

                $check_like_exists = EventsLikes::where('events_id', $post_id)->where('users_id', $user->id)->first();
                if ($check_like_exists) {
                    $check_like_exists->delete();
                    log_user_activity('Event', 'unlike', $post_id);
                } else {
                    $likeOrNotFlag = true;
                    $like = new EventsLikes;
                    $like->events_id = $post_id;
                    $like->users_id = $user->id;
                    $like->save();
                    log_user_activity('Event', 'like', $post_id);
                }
                return [
                    'post_id'           => $post_id,
                    'like_status'   => $likeOrNotFlag,
                    'total_like'    => EventsLikes::where('events_id', $post_id)->count()
                ];
                break;

            case self::TYPE_REVIEW:
                $post = Reviews::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('Review not found');

                $check_like_exists = ReviewsVotes::where('review_id', $post_id)->where('users_id', $user->id)->where('vote_type', 1)->first();
                if ($check_like_exists) {
                    if (isset($check_like_exists->review)) {
                        $this->rankingService->subPointsFromEarners($check_like_exists->review);
                    }
                    $check_like_exists->delete();
                    log_user_activity('Review', 'unlike', $post_id);
                } else {
                    $likeOrNotFlag = true;
                    $like = new ReviewsVotes;
                    $like->review_id = $post_id;
                    $like->vote_type = 1;
                    $like->users_id = $user->id;
                    $like->save();
                    if (isset($like->review)) {
                        $this->rankingService->addPointsToEarners($like->review);
                    }
                    log_user_activity('Review', 'like', $post_id);
                }
                return [
                    'id' => $post_id,
                    'post_id'           => $post_id,
                    'like_status'   => $likeOrNotFlag,
                    'total_like'    => ReviewsVotes::where('review_id', $post_id)->where('vote_type', 1)->count()
                ];
                break;
        }
        throw new ModelNotFoundException('like service not available for ' . $type . ' type');
    }

    public function likeOrUnlikeOfTripsSharing($type, $post_id, $request)
    {
        $likeOrNotFlag = false;
        $user = Auth::user();
        switch ($type) {
            case ShareService::TYPE_TRIP_SHARE:
                $post = TripsShares::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('Trip Shared Post not found');
                break;

            case ShareService::TYPE_TRIP_PLACE_SHARE:
                $post = TripsPlacesShares::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('Trip Place Shared Post not found');
                break;

            case ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE:
                $post = TripsMediasShares::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('Trip Place Media Shared Post not found');
                break;
        }

        $check_like_exists = TripContentPostLike::where('posts_type', $type)
            ->where('posts_id', $post_id)
            ->where('users_id', $user->id)->first();
        DB::beginTransaction();
        if ($check_like_exists) {
            $check_like_exists->delete();
            log_user_activity($type, 'unlike', $post_id);
        } else {
            $likeOrNotFlag = true;
            $like = new TripContentPostLike;
            $like->posts_type = $type;
            $like->posts_id = $post_id;
            $like->users_id = $user->id;
            $like->save();
            log_user_activity($type, 'like', $post_id);
        }
        DB::commit();
        return [
            'post_id'           => $post_id,
            'like_status'   => $likeOrNotFlag,
            'total_like'    => TripContentPostLike::where('posts_type', $type)->where('posts_id', $post_id)->count()
        ];
    }

    public function likeOrUnlikeOfTripPlace($post_id, $trip_place_id)
    {
        $likeOrNotFlag = false;
        $user = Auth::user();

        $post = TripPlans::where('id', $post_id)->first();
        if (!$post) throw new ModelNotFoundException('Trip not found');

        $tripPlaces = TripPlaces::where('id', $trip_place_id)->where('trips_id', $post_id)->first();
        if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');

        $check_like_exists = TripsLikes::where('trips_id', $post->id)
            ->where('places_id', $tripPlaces->id)
            ->where('users_id', $user->id)->first();
        if (is_object($check_like_exists)) {
            if (isset($check_like_exists->tripPlace)) {
                $this->rankingService->subPointsFromEarners($check_like_exists->tripPlace);
            }
            $check_like_exists->delete();
            log_user_activity('TripPlace', 'unlike', $post->id);
        } else {
            $likeOrNotFlag = true;
            $like = new TripsLikes;
            $like->trips_id = $post->id;
            $like->users_id = $user->id;
            $like->places_id = $tripPlaces->id;
            $like->save();
            log_user_activity('TripPlace', 'like', $post->id);
            if (isset($like->tripPlace)) {
                $this->rankingService->addPointsToEarners($like->tripPlace);
            }
        }

        try {
            broadcast(new TripPlaceStepLikeEvent($trip_place_id));
        } catch (\Throwable $e) {
        }
        try {
            broadcast(new TripPlaceStepLikeApiEvent($trip_place_id));
        } catch (\Throwable $e) {
        }

        return [
            'post_id'           => $post_id,
            'like_status'    => $likeOrNotFlag,
            'total_like'     => TripsLikes::where('trips_id', $post->id)->where('places_id', $tripPlaces->id)->count()
        ];
    }

    public function likeOrUnlikeOfTripPlaceMedia($post_id, $trip_place_id, $trip_place_media_id)
    {
        $likeOrNotFlag = false;
        $user = Auth::user();

        $post = TripPlans::where('id', $post_id)->first();
        if (!$post) throw new ModelNotFoundException('Trip not found');

        $tripPlaces = TripPlaces::where('id', $trip_place_id)->where('trips_id', $post->id)->first();
        if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');

        $tripMedia = TripMedias::where('id', $trip_place_media_id)->where('trips_id', $post->id)->where('trip_place_id', $tripPlaces->id)->first();
        if (!$tripMedia) throw new ModelNotFoundException('Trip Place media not found');

        $check_like_exists = MediasLikes::where('medias_id', $tripMedia->medias_id)->where('users_id', $user->id)->first();
        if (is_object($check_like_exists)) {
            if (isset($check_like_exists->media)) {
                $this->rankingService->subPointsFromEarners($check_like_exists->media);
            }
            $check_like_exists->delete();
            log_user_activity('media', 'unlike', $post->id);
        } else {
            $likeOrNotFlag = true;
            $like = new MediasLikes();
            $like->medias_id = $tripMedia->medias_id;
            $like->users_id = $user->id;
            $like->save();
            log_user_activity('media', 'like', $post->id);
            if (isset($like->media)) {
                $this->rankingService->addPointsToEarners($like->media);
            }
        }

        try {
            broadcast(new TripMediaLikeEvent($tripMedia->medias_id));
        } catch (\Throwable $e) {
        }
        try {
            broadcast(new TripMediaLikeApiEvent($tripMedia->medias_id));
        } catch (\Throwable $e) {
        }

        return [
            'post_id'           => $post_id,
            'like_status'    => $likeOrNotFlag,
            'total_like'     => MediasLikes::where('medias_id', $tripMedia->medias_id)->count()
        ];
    }

    public function likeOrUnlikeOfMedia($post_id)
    {
        $likeOrNotFlag = false;
        $user = Auth::user();

        $post = Media::where('id', $post_id)->first();
        if (!$post) throw new ModelNotFoundException('media not found');

        $check_like_exists = MediasLikes::where('medias_id', $post->id)->where('users_id', $user->id)->first();
        if (is_object($check_like_exists)) {
            $check_like_exists->delete();
            log_user_activity('media', 'unlike', $post->id);
        } else {
            $likeOrNotFlag = true;
            $like = new MediasLikes();
            $like->medias_id = $post->id;
            $like->users_id = $user->id;
            $like->save();
            log_user_activity('media', 'like', $post->id);
        }

        return [
            'post_id'           => $post_id,
            'like_status'    => $likeOrNotFlag,
            'total_like'     => MediasLikes::where('medias_id', $post->id)->count()
        ];
    }

    // privacy
    public function chnagePrivacy($type, $post_id, $privacy)
    {
        $user = Auth::user();
        switch ($type) {
            case self::TYPE_POST:
                $post = Posts::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('Post not found');
                if ($post->users_id != $user->id) throw new ModelNotFoundException('only ' . $post->author->name . ' can change privacy.');
                $post->permission   = $privacy;
                $post->save();
                return $post;
                break;

            case self::TYPE_TRIP:
                $post = TripPlans::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('Trip not found');
                if ($post->users_id != $user->id) throw new ModelNotFoundException('only ' . $post->author->name . ' can change privacy.');
                $post->privacy = $privacy;
                $post->save();
                return $post;
                break;
        }
        throw new ModelNotFoundException('privacy service not available for ' . $type . ' type');
    }
}
