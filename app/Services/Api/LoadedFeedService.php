<?php

namespace App\Services\Api;

use Illuminate\Support\Facades\Auth;
use App\LoadedFeed;
use Carbon\Carbon;

class LoadedFeedService
{
    public function __construct()
    {
    }

    public static function handleError($e)
    {
        // Log::useDailyFiles(storage_path() . '/logs/laravel.log');
        // Log::warning('API_REQUEST : ', [
        //     $e->getTraceAsString()
        // ], PHP_EOL . PHP_EOL);
        return $e->getTraceAsString();
    }

    public function create($attributes)
    {
        try {
            $data = collect([
                'token' => request()->header('authorization'),
                'users_id' => Auth::user()->id,
                'date' => Carbon::now(),
            ])->merge($attributes)->toArray();
            return LoadedFeed::create($data);
        } catch (\Throwable $e) {
            return self::handleError($e);
        }
    }
    public function createTrending($attributes)
    {
        $old = LoadedFeed::where('users_id', Auth::user()->id)
            ->where('token', request()->header('authorization'))
            ->where('type', $attributes['type'])
            ->where('action', $attributes['action'])->first();
        if ($old) {
            $old->ids = $old->ids . "," . $attributes['ids'];
            $old->save();
            return $old;
        } else {
            return $this->create($attributes);
        }
    }

    public function getOtherSecondaryLoadedNewsfeed()
    {
        return LoadedFeed::where('users_id', Auth::user()->id)
            ->where('token', request()->header('authorization'))
            ->whereDate('date', date('Y-m-d'))
            ->where('action', 'secondary')
            ->pluck('type')->toArray();
    }

    public function cleanAuthLoadedFeed()
    {
        try {
            return LoadedFeed::where('users_id', Auth::user()->id)->delete();
        } catch (\Throwable $e) {
            return self::handleError($e);
        }
    }
}
