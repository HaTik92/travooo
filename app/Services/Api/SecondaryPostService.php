<?php

namespace App\Services\Api;

use Illuminate\Support\Facades\Auth;

class SecondaryPostService
{
    public $ip;
    public $authUser;

    public function __construct()
    {
        $this->authUser = Auth::user();
        $this->ip       = $this->get_client_ip();
    }

    private function get_client_ip()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip_address = (request()->ip()) ? request()->ip() : null;
        }
        return $ip_address;
    }
}
