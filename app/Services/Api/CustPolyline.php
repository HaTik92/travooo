<?php

namespace App\Services\Api;


use \Polyline;

class CustPolyline extends Polyline
{
    protected static $precision = 5;
}
