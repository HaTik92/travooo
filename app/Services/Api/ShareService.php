<?php

namespace App\Services\Api;

use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Discussion\Discussion;
use App\Models\Events\Events;
use App\Models\Place\Place;
use App\Models\Posts\Posts;
use App\Services\Api\PrimaryPostService;
use App\Models\Reports\Reports;
use App\Models\Reviews\Reviews;
use App\Models\TripPlans\TripPlans;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ShareService
{
    // Type of Comment
    const TYPE_POST         = "post";   // General post (text , media , check-in) +  External or internal links(others) + share
    const TYPE_TRIP         = "trip";
    const TYPE_EVENT        = "event";
    const TYPE_REPORT       = "report"; // Travlog
    const TYPE_DISCUSSION   = "discussion";
    const TYPE_REVIEW       = "review";
    const TYPE_COUNTRY      = "country";
    const TYPE_CITY         = "city";
    const TYPE_PLACE        = "place";

    const TYPE_TRIP_SHARE                   = "plan_shared"; // for trip place media comment
    const TYPE_TRIP_PLACE_SHARE             = "plan_step_shared"; // for trip place media comment
    const TYPE_TRIP_PLACE_MEDIA_SHARE       = "plan_media_shared"; // for trip place media comment

    const TYPE_MAPPING_TRIP_SHARE = [
        self::TYPE_TRIP_SHARE,
        self::TYPE_TRIP_PLACE_SHARE,
        self::TYPE_TRIP_PLACE_MEDIA_SHARE,
    ];

    const TYPE_MAPPING = [
        self::TYPE_POST,
        self::TYPE_TRIP,
        self::TYPE_EVENT,
        self::TYPE_REPORT,
        self::TYPE_DISCUSSION,
        self::TYPE_REVIEW,
        self::TYPE_COUNTRY,
        self::TYPE_CITY,
        self::TYPE_PLACE
    ];

    private $primaryPostService;

    public function __construct(PrimaryPostService $primaryPostService)
    {
        $this->primaryPostService = $primaryPostService;
    }

    public function __check($type, $id)
    {
        switch (trim(strtolower($type))) {
            case self::TYPE_POST:
                return Posts::find($id);
                break;

            case self::TYPE_TRIP:
                return TripPlans::find($id);
                break;

            case self::TYPE_EVENT:
                return Events::find($id);
                break;

            case self::TYPE_REPORT:
                return Reports::find($id);
                break;

            case self::TYPE_DISCUSSION:
                return Discussion::find($id);
                break;

            case self::TYPE_REVIEW:
                return Reviews::find($id);
                break;

            case self::TYPE_COUNTRY:
                return Countries::find($id);
                break;

            case self::TYPE_CITY:
                return Cities::find($id);
                break;

            case self::TYPE_PLACE:
                return Place::find($id);
                break;
        }
        throw new ModelNotFoundException('type is invalid');
    }
}
