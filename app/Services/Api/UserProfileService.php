<?php

namespace App\Services\Api;

use Illuminate\Http\Request;
use App\Models\Country\Countries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\User\DeleteAccountRequest;
use App\Models\User\SettingUsersBlocks;
use App\Models\User\User;
use App\Models\User\UsersContentLanguages;
use App\Models\User\UsersTravelStyles;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\City\Cities;

class UserProfileService
{
    public function __construct()
    {
    }

    public function find($userId)
    {
        return User::where('id', $userId)->first();
    }

    /**
     * Fetch User Profile With travelstyles & languages & expertise
     * @param int $userId
     */
    public function findProfile($userId)
    {
        $user = User::select('id', 'name', 'username', 'email', 'nationality', 'about', 'mobile', 'interests', 'gender', 'birth_date')
            ->where('id', $userId)->first();
        if (!$user) throw new ModelNotFoundException('User not found');

        // interests
        $user->interests = explode(",", $user->interests);

        // languages
        $user->content_languages = UsersContentLanguages::where('users_id', $user->id)->get()
            ->map(function ($userLanguage) {
                return [
                    'id' => $userLanguage->language_id,
                    'title' => $userLanguage->languages->transsingle->title
                ];
            });

        // travelstyles
        $user->travelstyles = UsersTravelStyles::where('users_id', $user->id)->get()
            ->map(function ($usersTravelStyle) {
                return [
                    'id' => $usersTravelStyle->conf_lifestyles_id,
                    "name" => $usersTravelStyle->travelstyle->transsingle->title,
                    "desc" => "",
                    "url" => @check_lifestyles_photo($usersTravelStyle->travelstyle->getMedias[0]->url)
                ];
            });

        // expertise
        $expertise  = [];
        ExpertsCities::where('users_id', $user->id)
            ->get()->filter(function ($expertsCity) use (&$expertise) {
                $expertise[] = [
                    "type" => "city",
                    "id" => $expertsCity->cities_id,
                    "name" => $expertsCity->cities->transsingle->title ?? null,
                    "flag" => null
                ];
            });
        ExpertsCountries::where('users_id', $user->id)
            ->get()->filter(function ($expertsCountry) use (&$expertise) {
                $expertise[] = [
                    "type" => "country",
                    "id" => $expertsCountry->countries_id,
                    "name" => $expertsCountry->countries->transsingle->title ?? null,
                    "flag" => get_country_flag($expertsCountry->countries)
                ];
            });
        $user->expertise = $expertise;

        return $user;
    }

    /**
     * Save User Profile With travelstyles & languages & expertise
     * @param int $userId
     * @param Request $request
     */
    public function saveProfile($userId, $request)
    {
        $user = $this->find($userId);
        if (!$user) throw new ModelNotFoundException('User not found');

        if (!($request->username && $request->email)) throw new ModelNotFoundException('username or email must be required');

        $name               = $request->get('name', null);
        $username           = $request->get('username');
        $email              = $request->get('email');
        $nationality        = $request->get('nationality');
        $about              = $request->get('about', null);
        $mobile             = $request->get('mobile', null);
        $interests          = $request->get('interests', []);
        $expertise          = $request->get('expertise', []);
        $travelstyles       = $request->get('travelstyles', []);
        $content_languages  = $request->get('content_languages', []);

        $countries = Countries::find($nationality);
        if (!$countries) throw new ModelNotFoundException("Invalid Country");

        $user->name             = $name;
        $user->username         = $username;
        $user->email            = $email;
        $user->nationality      = $nationality;
        $user->about            = $about;
        $user->mobile           = $mobile;
        $user->interests        = (is_array($interests) && count($interests)) ? implode(",", $interests) : "";

        if ($user->save()) {
            // expertise
            ExpertsCities::where('users_id', $user->id)->delete();
            ExpertsCountries::where('users_id', $user->id)->delete();
            if (isset($expertise) && count($expertise) > 0) {
                foreach ($expertise as $ex) {
                    if ($ex['type'] == "country") {
                        if (Countries::find($ex['value'])) {
                            $new_ex = new ExpertsCountries;
                            $new_ex->users_id = $user->id;
                            $new_ex->countries_id = $ex['value'];
                            $new_ex->save();
                        }
                    } elseif ($ex['type'] == "city") {
                        if (Cities::find($ex['value'])) {
                            $new_ex = new ExpertsCities;
                            $new_ex->users_id = $user->id;
                            $new_ex->cities_id = $ex['value'];
                            $new_ex->save();
                        }
                    }
                }
            }

            //travelstyles
            UsersTravelStyles::where('users_id', $user->id)->delete();
            if (isset($travelstyles) && count($travelstyles)) {
                foreach ($travelstyles as $tr) {
                    if (!(is_array($tr) || is_object($tr))) {
                        $new_tr = new UsersTravelStyles;
                        $new_tr->users_id = $user->id;
                        $new_tr->conf_lifestyles_id = $tr;
                        $new_tr->save();
                    }
                }
            }

            //content_languages
            if (count($content_languages)) {
                UsersContentLanguages::where('users_id', $user->id)->delete();
                foreach ($content_languages as $language) {
                    if (!(is_array($language) || is_object($language))) {
                        $lng = new UsersContentLanguages();
                        $lng->users_id = $user->id;
                        $lng->language_id = $language;
                        $lng->save();
                    }
                }
            }

            return true;
        } else {
            new ModelNotFoundException('Your Account Can not Update!');
        }
    }

    /**
     * Fetch User Social Links
     * @param int $userId
     */
    public function findSocialLinks($userId)
    {
        return User::select('facebook', 'twitter', 'instagram', 'pinterest', 'tripadvisor', 'youtube', 'medium', 'website', 'vimeo')
            ->where('id', $userId)
            ->first();
    }

    /**
     * Save User Social Links
     * @param int $userId
     */
    public function saveSocialLinks($userId, $request)
    {
        $user = $this->find($userId);
        if (!$user) throw new ModelNotFoundException('User not found');

        $user->facebook     = $request->get('facebook', null);
        $user->twitter      = $request->get('twitter', null);
        $user->instagram    = $request->get('instagram', null);
        $user->pinterest    = $request->get('pinterest', null);
        $user->tripadvisor  = $request->get('tripadvisor', null);
        $user->youtube      = $request->get('youtube', null);
        $user->medium       = $request->get('medium', null);
        $user->website      = $request->get('website', null);
        $user->vimeo        = $request->get('vimeo', null);

        if ($user->save())
            return true;
        else
            throw new ModelNotFoundException("Social link not saved!");
    }

    public function fetchBlockUsers($userId)
    {
        $user = $this->find($userId);
        if (!$user) throw new ModelNotFoundException('User not found');

        return SettingUsersBlocks::with('blocked_user')->where('blocked_users_id', '!=', $user->id)->where('users_id', $user->id)->get()
            ->map(function ($blocked_user_obj) {
                return [
                    'id'            => $blocked_user_obj->blocked_users_id,
                    'text'          => $blocked_user_obj->blocked_user->name,
                    'image'         => check_profile_picture($blocked_user_obj->blocked_user->profile_picture),
                    'nationality'   => $blocked_user_obj->blocked_user->nationality ? $blocked_user_obj->blocked_user->country_of_nationality->transsingle->title : '',
                ];
            })->toArray();
    }

    public function blockOrUnBlock($authUserId, $userId)
    {
        $alreadyBlock = $this->isAlreadyBlock($authUserId, $userId);
        if ($alreadyBlock) {
            return ($alreadyBlock->delete()) ? true : false;
        } else {
            $settingUsersBlocks = new SettingUsersBlocks;
            $settingUsersBlocks->users_id = $authUserId;
            $settingUsersBlocks->blocked_users_id = $userId;
            return ($settingUsersBlocks->save()) ? true : false;
        }
    }

    public function isAlreadyBlock($authUserId, $userId)
    {
        $authUser = $this->find($authUserId);
        if (!$authUser) throw new ModelNotFoundException('User not found');

        $user = $this->find($userId);
        if (!$user) throw new ModelNotFoundException('User not found');

        $isAlreadyBlock = SettingUsersBlocks::where('users_id', $authUser->id)->where('blocked_users_id', $user->id)->first();
        return $isAlreadyBlock;
    }

    public function deleteAccount($userId)
    {
        $user = $this->find($userId);
        if (!$user) throw new ModelNotFoundException('User not found');

        $deleteAccountRequest = DeleteAccountRequest::where('users_id', $userId)->where('status', DeleteAccountRequest::STATUS_CONFIRM)->first();
        if (!$deleteAccountRequest) throw new ModelNotFoundException('Invalid request');

        return travoooDeleteUserAccount($user->id, true);
    }

    public function getFollowerIds($userId)
    {
        try {
            return $this->find($userId)->get_followers()->pluck('followers_id')->unique();
        } catch (\Throwable $e) {
            return collect([]);
        }
    }

    public function getFriendIds($userId)
    {
        try {
            return $this->find($userId)->findFriends()->pluck('id');
        } catch (\Throwable $e) {
            return collect([]);
        }
    }

    public function getFriendAndFollowerIds($userId)
    {
        try {
            return collect($this->getFriendIds($userId))->merge($this->getFollowerIds($userId))->unique();
        } catch (\Throwable $e) {
            return collect([]);
        }
    }
}
