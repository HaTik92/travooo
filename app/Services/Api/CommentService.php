<?php

namespace App\Services\Api;

use App\Events\Api\LikeUnLike\TripPlaceCommentLikeUnlike;
use App\Events\Api\LikeUnLike\TripPlaceMediaCommentLikeUnLike;
use App\Events\Api\Plan\TripPlaceCommentAddedApiEvent;
use App\Events\Api\Plan\TripPlaceCommentChangedApiEvent;
use App\Events\Api\PlanMedia\TripMediaCommentAddedApiEvent;
use App\Events\Api\PlanMedia\TripMediaCommentChangedApiEvent;
use App\Events\Plan\TripPlaceCommentAddedEvent;
use App\Events\Plan\TripPlaceCommentChangedEvent;
use App\Events\PlanMedia\TripMediaCommentAddedEvent;
use App\Events\PlanMedia\TripMediaCommentChangedEvent;
use App\Http\Controllers\Api\CommonApiController;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCommentsLikes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\Api\PrimaryPostService;
use Illuminate\Support\Facades\Storage;
use App\Models\ActivityMedia\Media;
use App\Models\ActivityMedia\MediasComments;
use App\Models\ActivityMedia\MediasCommentsLikes;
use App\Models\ActivityMedia\MediascommentsMedias;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Posts\PostsCommentsMedias;
use App\Models\Posts\PostsTags;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlaces\TripPlacesComments;
use App\Models\TripPlaces\TripPlacesCommentsLikes;
use App\Models\TripPlaces\TripPlacesCommentsMedias;
use App\Models\User\UsersMedias;
use App\Services\Discussions\DiscussionsService;
use App\Services\Ranking\RankingService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CommentService
{
    const BASE_URL_TRAVOOO_AMAZON_AWS   = 'https://s3.amazonaws.com/travooo-images2/';
    const COMMENT_MEDIA_LOCATION        = 'post-comment-photo';
    const COMMENT_DISCUSSION            = 'replies_media';

    // Type Use In function
    const FUN_TYPE = 'post_comment';

    // Type Of Filter
    const FILTER_NEW         = 1;
    const FILTER_TOP         = 2;

    const FILTER_MAPPING = [
        self::FILTER_NEW,
        self::FILTER_TOP,
    ];

    private $primaryPostService;
    private $rankingService;

    public function __construct(PrimaryPostService $primaryPostService, RankingService $rankingService)
    {
        $this->primaryPostService = $primaryPostService;
        $this->rankingService = $rankingService;
    }

    // Find Comment Post Comment, Event Comment etc...
    public function findComment($type, $postId, $commentId, $withAuth = false)
    {
        $user       = Auth::user();
        $comment    = PostsComments::where('id', $commentId)
            ->where('posts_id', $postId)->where('type', $type);

        if (is_bool($withAuth) && $withAuth) {
            $comment = $comment->where('users_id', $user->id);
        } else if (is_int($withAuth)) {
            $comment = $comment->where('users_id', $withAuth);
        }

        return ($comment != null) ? $comment->first() : null;
    }

    // Find Comment
    public function findCommentLike($type, $id, $withAuth = false)
    {
        $commentLike = null;
        $user = Auth::user();
        switch ($type) {
            case PrimaryPostService::TYPE_POST:
                $commentLike = PostsCommentsLikes::where('posts_comments_id', $id);
                break;
        }
        if ($commentLike != null) {
            if (is_bool($withAuth) && $withAuth == true) {
                $commentLike = $commentLike->where('users_id', $user->id);
            } else if (is_int($withAuth) && $withAuth != 0) {
                $commentLike = $commentLike->where('users_id', $withAuth);
            }
            return $commentLike->first();
        } else {
            return null;
        }
    }

    // Return Comment Like Count Like Post Comment Count, Event Comment Count etc...
    public function getCommentLikeCount($type, $id)
    {
        switch ($type) {
            case PrimaryPostService::TYPE_POST:
            case PrimaryPostService::TYPE_TRIP:
            case PrimaryPostService::TYPE_REPORT:
            case PrimaryPostService::TYPE_EVENT:
                return PostsCommentsLikes::where('posts_comments_id', $id)->count();
                break;
        }
        return 0;
    }

    // commenting

    // Return Comment Count Like Post Comment Count, Event Comment Count etc...
    public function getCommentCount($type, $posts_id)
    {
        return PostsComments::where('posts_id', $posts_id)->where('type', $type)->count();
    }

    // Load More Comments
    public function loadMore($type, $postId, $perPage = 10, $filter = self::FILTER_NEW)
    {
        $post_comment = PostsComments::select('*')
            // ->addSelect(DB::raw('fun_basic_algo(id, type) as total'))
            ->addSelect(DB::raw('(0) as total'))
            ->where('posts_id', $postId)
            ->where('type', $type)
            ->whereNull('parents_id');

        $post_comment = ($filter == self::FILTER_TOP) ?
            $post_comment->orderBy('total', 'desc') :
            $post_comment->orderBy('created_at', 'desc');

        return $post_comment->paginate($perPage);
    }

    // Create Primary Post Comment Like Post, Trip etc...
    public function createCommentOrReply($request, $type, $postId)
    {
        if ($type == PrimaryPostService::TYPE_DISCUSSION) {
            return $this->createDiscussionCommentOrReply($request, $postId);
        }
        $pair = $request->pair ?? '';

        // posts_id
        $posts_comments_medias  = null;
        $user                   = Auth::user();
        $fileLists              = app(CommonApiController::class)->getTempFiles($pair);
        $post_comment           = convert_string($request->text ?? "");
        $tags                   = $request->input("tags", []);

        // Checking Existing Post & Checking Existing Comment
        $post = $this->primaryPostService->find($type, $postId);
        if (!$post) throw new ModelNotFoundException('Post not found');

        if ($request->has('parents_id')) {
            $parent_comment = $this->findComment($type, $postId, $request->parents_id);
            if (!$parent_comment) throw new ModelNotFoundException('Parent Comment not found');
        }

        DB::beginTransaction();
        $new_comment                = new PostsComments;
        $new_comment->users_id      = $user->id;
        $new_comment->posts_id      = $post->id;
        $new_comment->text          = $post_comment;
        $new_comment->type          = $type;
        if ($request->has('parents_id')) {
            $new_comment->parents_id = $request->parents_id;
        }
        $new_comment->save();

        log_user_activity('Status', 'comment', $post->id);

        // $medias = [];
        if (count($fileLists) > 0) {
            foreach ($fileLists as $file) {
                $filename = $user->id . '_' . time() . '_' . $file[1];
                Storage::disk('s3')->put(self::COMMENT_MEDIA_LOCATION . '/' . $filename, fopen($file[0], 'r+'), 'public');

                $media                  = new Media;
                $media->url             = S3_BASE_URL . '/' . self::COMMENT_MEDIA_LOCATION . '/' . $filename;;
                $media->author_name     = $user->name;
                $media->title           = strip_tags($post_comment);
                $media->author_url      = '';
                $media->source_url      = '';
                $media->license_name    = '';
                $media->license_url     = '';
                $media->uploaded_at     = date('Y-m-d H:i:s');
                $media->users_id        = $user->id;
                $media->type  = getMediaTypeByMediaUrl($media->url);
                $media->save();

                $_usersMedias = new UsersMedias();
                $_usersMedias->users_id = $user->id;
                $_usersMedias->medias_id = $media->id;
                $_usersMedias->save();

                switch ($type) {
                    case PrimaryPostService::TYPE_POST:
                    case ShareService::TYPE_TRIP_SHARE:
                    case ShareService::TYPE_TRIP_PLACE_SHARE:
                    case ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE:
                    case PrimaryPostService::TYPE_REPORT:
                    case PrimaryPostService::TYPE_TRIP:
                    case PrimaryPostService::TYPE_EVENT:
                        $posts_comments_medias = new PostsCommentsMedias;
                        $posts_comments_medias->posts_comments_id = $new_comment->id;
                        break;

                        // case PrimaryPostService::TYPE_TRIP:
                        //     $posts_comments_medias = new TripsCommentsMedias;
                        //     $posts_comments_medias->trips_comments_id   = $new_comment->id;
                        //     break;

                        // case PrimaryPostService::TYPE_REPORT:
                        //     $posts_comments_medias = new ReportsCommentsMedias;
                        //     $posts_comments_medias->reports_comments_id = $new_comment->id;
                        //     break;

                        // case PrimaryPostService::TYPE_EVENT:
                        //     $posts_comments_medias = new EventsCommentsMedias;
                        //     $posts_comments_medias->events_comments_id = $new_comment->id;
                        //     break;
                }

                $posts_comments_medias->medias_id = $media->id;
                $posts_comments_medias->save();
                // $medias[] = $posts_comments_medias;
            }
            app(CommonApiController::class)->deleteTempFiles($pair);
        }
        switch ($type) {
            case PrimaryPostService::TYPE_REPORT:
                $this->rankingService->addPointsToEarners($post);
                break;
        }


        // save tags of trip comment, report comment, post comment, event comment
        save_tags($tags, PostsTags::TYPE_POSTS_COMMENTS, $new_comment->id);

        DB::commit();
        if (count($new_comment->medias) > 0) {
            foreach ($new_comment->medias as $media) {
                $media->media;
            }
        }
        // $new_comment->medias = $medias;
        $new_comment->text = convert_post_text_to_tags($new_comment->text, $new_comment->tags, false);
        if ($new_comment->author) {
            $new_comment->author->profile_picture = check_profile_picture($new_comment->author->profile_picture);
        }
        $new_comment->like_status = false;
        $new_comment->total_likes = 0;
        if (!$request->has('parents_id')) {
            $new_comment->sub;
        }

        if (in_array($type, ShareService::TYPE_MAPPING_TRIP_SHARE)) {
            $post->comment = convert_post_text_to_tags($post->comment, $post->tags, false);
        } else {
            $post->text = convert_post_text_to_tags($post->text, $post->tags, false);
        }


        unset($post->tags, $new_comment->tags);

        return [
            'post_id'           => $postId,
            'post'              => $post,
            'comments_total'    => $this->getCommentCount($type, $post->id),
            'comment'           => $new_comment,
        ];
    }

    public function createDiscussionCommentOrReply($request, $discussion_id)
    {
        $discussionsService = app(DiscussionsService::class);
        $user                   = Auth::user();
        $pair                   = $request->pair ?? '';
        $fileLists              = app(CommonApiController::class)->getTempFiles($pair);
        $post_comment           = convert_string($request->text ?? "");
        $tags                   = $request->input("tags", []);
        $finalMediaUrl          = "";
        $mediaType              = "";

        // Checking Existing Post & Checking Existing Comment
        $post = $this->primaryPostService->find(PrimaryPostService::TYPE_DISCUSSION, $discussion_id);
        if (!$post) throw new ModelNotFoundException('Post not found');

        if ($request->has('parents_id')) {
            $parent_comment = DiscussionReplies::where('id', $request->parents_id)->where('discussions_id', $discussion_id)->first();
            if (!$parent_comment) throw new ModelNotFoundException('Parent Comment not found');
        }

        if (count($fileLists) > 0) {
            $fileLists = [$fileLists[count($fileLists) - 1]];
            foreach ($fileLists as $file) {
                $filename = $user->id . '_' . time() . '_' . $file[1];
                Storage::disk('s3')->put(self::COMMENT_DISCUSSION . '/' . $filename, fopen($file[0], 'r+'), 'public');
                $finalMediaUrl  = S3_BASE_URL . self::COMMENT_DISCUSSION . '/' . $filename;
                $info           = getimagesize($finalMediaUrl);
                $mediaType      = (strtolower(substr($info['mime'], 0, 5)) == 'image' ? 'image' : 'video');
            }
            app(CommonApiController::class)->deleteTempFiles($pair);
        }

        DB::beginTransaction();
        $new_comment                    = new DiscussionReplies;
        $new_comment->users_id          = $user->id;
        $new_comment->discussions_id    = $discussion_id;
        $new_comment->reply             = $post_comment;
        $new_comment->num_upvotes       = 0;
        $new_comment->num_downvotes     = 0;
        $new_comment->num_views         = 0;
        $new_comment->num_follows       = 0;
        if (count($fileLists) > 0) {
            $new_comment->medias_url    = $finalMediaUrl;
            $new_comment->medias_type   = $mediaType;
        }
        if ($request->has('parents_id')) {
            $new_comment->parents_id = $request->parents_id;
        }

        $new_comment->save();
        if (isset($new_comment->discussion)) {
            $this->rankingService->addPointsToEarners($new_comment->discussion);
            $this->rankingService->addPointsEarners($new_comment->id, get_class($new_comment), $user->id);
        }
        log_user_activity('Status', 'comment', $post->id);

        // save tags of trip comment, report comment, post comment, event comment
        save_tags($tags, PostsTags::TYPE_DISCUSSION_REPLY, $new_comment->id);

        DB::commit();
        $new_comment->text = convert_post_text_to_tags($new_comment->reply, $new_comment->tags, false);
        if ($new_comment->author) {
            $new_comment->author->profile_picture = check_profile_picture($new_comment->author->profile_picture);
        }
        $new_comment->upvote_flag = false;
        $new_comment->downvotes_flag = false;
        $new_comment->total_upvotes = 0;
        $new_comment->total_downvotes = 0;
        if (!$request->has('parents_id')) {
            $new_comment->sub;
        }

        unset($new_comment->reply, $new_comment->tags);
        $discussionsService->updatePopularCount($discussion_id);
        return [
            'comments_total'    => DiscussionReplies::where('discussions_id', $discussion_id)->where('parents_id',  0)->count(),
            'comment'           => $new_comment,
        ];
    }

    public function createCommentOrReplyOfTripPlace($request, $type, $postId)
    {
        $posts_comments_medias  = null;
        $user                   = Auth::user();
        $pair       = $request->pair ?? '';
        $fileLists              = app(CommonApiController::class)->getTempFiles($pair);
        $post_comment           = convert_string($request->text ?? "");
        $tags                   = $request->input("tags", []);

        // Checking Existing Post & Checking Existing Comment
        $post = $this->primaryPostService->find($type, $postId);
        if (!$post) throw new ModelNotFoundException('Trip not found');

        $tripPlaces = TripPlaces::where('id', $request->trip_place_id)->where('trips_id', $postId)->first();
        if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');

        if ($request->has('parents_id')) {
            $parent_comment = TripPlacesComments::where('trip_place_id', $tripPlaces->id)->where('id', $request->parents_id)->first();
            if (!$parent_comment) throw new ModelNotFoundException('Parent Comment not found');
        }
        DB::beginTransaction();
        $new_comment                    = new TripPlacesComments;
        $new_comment->user_id           = $user->id;
        $new_comment->trip_place_id     = $tripPlaces->id;
        $new_comment->comment          = $post_comment;
        if ($request->has('parents_id')) {
            $new_comment->reply_to = $request->parents_id;
        } else {
            $new_comment->reply_to = 0;
        }
        if ($new_comment->tripPlace) {
            $this->rankingService->addPointsToEarners($new_comment->tripPlace);
        }
        $new_comment->save();
        log_user_activity('Status', 'comment', $post->id);

        $medias = [];
        if (count($fileLists) > 0) {
            foreach ($fileLists as $file) {
                $filename = $user->id . '_' . time() . '_' . $file[1];
                Storage::disk('s3')->put(self::COMMENT_MEDIA_LOCATION . '/' . $filename, fopen($file[0], 'r+'), 'public');

                $media                  = new Media;
                $media->url             = S3_BASE_URL . '/' . self::COMMENT_MEDIA_LOCATION . '/' . $filename;;
                $media->author_name     = $user->name;
                $media->title           = strip_tags($post_comment);
                $media->author_url      = '';
                $media->source_url      = '';
                $media->license_name    = '';
                $media->license_url     = '';
                $media->uploaded_at     = date('Y-m-d H:i:s');
                $media->users_id        = $user->id;
                $media->type  = getMediaTypeByMediaUrl($media->url);
                $media->save();

                $_usersMedias = new UsersMedias();
                $_usersMedias->users_id = $user->id;
                $_usersMedias->medias_id = $media->id;
                $_usersMedias->save();

                $posts_comments_medias = new TripPlacesCommentsMedias;
                $posts_comments_medias->trip_place_comment_id   = $new_comment->id;
                $posts_comments_medias->medias_id = $media->id;
                $posts_comments_medias->save();
                $medias[] = $posts_comments_medias;
            }
            app(CommonApiController::class)->deleteTempFiles($pair);
        }

        // save tags of trip comment, report comment, post comment, event comment
        save_tags($tags, PostsTags::TYPE_TRIP_PLACE_COMMENT, $new_comment->id);

        DB::commit();
        if (count($medias) > 0) {
            foreach ($medias as $media) {
                $media->media;
            }
        }
        $new_comment->medias = $medias;
        $new_comment->text = convert_post_text_to_tags($new_comment->comment, $new_comment->tags, false);
        unset($new_comment->tags, $new_comment->comment);

        $new_comment->like_status = false;
        $new_comment->total_likes = 0;
        if (!$request->has('parents_id')) {
            $new_comment->sub;
        }



        if (!$new_comment->reply_to) {
            try {
                broadcast(new TripPlaceCommentAddedEvent($new_comment->id));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new TripPlaceCommentAddedApiEvent($new_comment->id));
            } catch (\Throwable $e) {
            }
        } else {
            try {
                broadcast(new TripPlaceCommentChangedEvent($new_comment->reply_to));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new TripPlaceCommentChangedApiEvent($new_comment->reply_to));
            } catch (\Throwable $e) {
            }
        }

        $new_comment->author = $new_comment->user;
        unset($new_comment->user);
        if ($new_comment->author) {
            $new_comment->author->profile_picture = check_profile_picture($new_comment->author->profile_picture);
        }

        return [
            'post_id'           => $postId,
            'post'              => $tripPlaces,
            'comments_total'    => TripPlacesComments::where('trip_place_id', $tripPlaces->id)->count(),
            'comment'           => $new_comment,
        ];
    }

    public function createCommentOrReplyOfTripPlaceMedia($request, $type, $postId)
    {
        $posts_comments_medias  = null;
        $user                   = Auth::user();
        $pair                   = $request->pair ?? '';
        $fileLists              = app(CommonApiController::class)->getTempFiles($pair);
        $post_comment           = convert_string($request->text ?? "");
        $tags                   = $request->input("tags", []);

        // Checking Existing Post & Checking Existing Comment
        $post = $this->primaryPostService->find($type, $postId);
        if (!$post) throw new ModelNotFoundException('Trip not found');

        $tripPlaces = TripPlaces::where('id', $request->trip_place_id)->where('trips_id', $postId)->first();
        if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');

        $tripMedia = TripMedias::where('id', $request->trip_place_media_id)
            ->where('trips_id', $post->id)
            ->where('trip_place_id', $request->trip_place_id)->first();
        if (!$tripMedia) throw new ModelNotFoundException('Trip Place media not found');

        if ($request->has('parents_id')) {
            $parent_comment = MediasComments::where('medias_id', $tripMedia->media->id)
                ->where('id', $request->parents_id)->first();
            if (!$parent_comment) throw new ModelNotFoundException('Parent Comment not found');
        }

        DB::beginTransaction();
        $new_comment                    = new MediasComments;
        $new_comment->users_id          = $user->id;
        $new_comment->medias_id         = $tripMedia->media->id;
        $new_comment->comment           = $post_comment;
        if ($request->has('parents_id')) {
            $new_comment->reply_to          = $request->parents_id;
        } else {
            $new_comment->reply_to = 0;
        }
        $new_comment->save();
        log_user_activity('Status', 'comment', $post->id);

        $medias = [];
        if (count($fileLists) > 0) {
            foreach ($fileLists as $file) {
                $filename = $user->id . '_' . time() . '_' . $file[1];
                Storage::disk('s3')->put(self::COMMENT_MEDIA_LOCATION . '/' . $filename, fopen($file[0], 'r+'), 'public');

                $media                  = new Media;
                $media->url             = S3_BASE_URL . '/' . self::COMMENT_MEDIA_LOCATION . '/' . $filename;;
                $media->author_name     = $user->name;
                $media->title           = strip_tags($post_comment);
                $media->author_url      = '';
                $media->source_url      = '';
                $media->license_name    = '';
                $media->license_url     = '';
                $media->users_id        = $user->id;
                $media->uploaded_at     = date('Y-m-d H:i:s');
                $media->type  = getMediaTypeByMediaUrl($media->url);
                $media->save();

                $_usersMedias = new UsersMedias();
                $_usersMedias->users_id = $user->id;
                $_usersMedias->medias_id = $media->id;
                $_usersMedias->save();

                $posts_comments_medias = new MediascommentsMedias;
                $posts_comments_medias->medias_comments_id   = $new_comment->id;
                $posts_comments_medias->medias_id = $media->id;
                $posts_comments_medias->save();
                $medias[] = $posts_comments_medias;
            }
            app(CommonApiController::class)->deleteTempFiles($pair);
        }
        // save tags of trip comment, report comment, post comment, event comment
        save_tags($tags, PostsTags::TYPE_TRIP_PLACE_MEDIA_COMMENT, $new_comment->id);

        DB::commit();
        if (count($medias) > 0) {
            foreach ($medias as $media) {
                $media->media;
            }
        }
        $new_comment->medias = $medias;
        $tripMedia->media;
        $new_comment->text = convert_post_text_to_tags($new_comment->comment, $new_comment->tags, false);
        unset($new_comment->tags, $new_comment->comment);

        $new_comment->like_status = false;
        $new_comment->total_likes = 0;
        if (!$request->has('parents_id')) {
            $new_comment->sub;
        }

        if (!$new_comment->reply_to) {
            try {
                broadcast(new TripMediaCommentAddedEvent($new_comment->id));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new TripMediaCommentAddedApiEvent($new_comment->id));
            } catch (\Throwable $e) {
            }
        } else {
            try {
                broadcast(new TripMediaCommentChangedEvent($new_comment->reply_to));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new TripMediaCommentChangedApiEvent($new_comment->reply_to));
            } catch (\Throwable $e) {
            }
        }

        $new_comment->author = $new_comment->user;
        unset($new_comment->user);
        if ($new_comment->author) {
            $new_comment->author->profile_picture = check_profile_picture($new_comment->author->profile_picture);
        }

        $new_comment->created_at =  Carbon::parse($new_comment->created_at)->format(Carbon::DEFAULT_TO_STRING_FORMAT);

        return [
            'post_id'           => $postId,
            'post'              => $tripMedia,
            'comments_total'    => MediasComments::where('medias_id', $tripMedia->media->id)->where('reply_to', 0)->count(),
            'comment'           => $new_comment,
        ];
    }

    public function createCommentOrReplyOMedia($request, $postId)
    {

        $posts_comments_medias  = null;
        $user                   = Auth::user();
        $pair           = $request->pair ?? '';
        $fileLists              =  app(CommonApiController::class)->getTempFiles($pair);
        $post_comment           = convert_string($request->text ?? "");
        $tags                   = $request->input("tags", []);

        // Checking Existing Post & Checking Existing Comment
        $post = Media::where('id', $postId)->first();
        if (!$post) throw new ModelNotFoundException('media not found');

        if ($request->has('parents_id')) {
            $parent_comment = MediasComments::where('medias_id', $post->id)
                ->where('id', $request->parents_id)->first();
            if (!$parent_comment) throw new ModelNotFoundException('Parent Comment not found');
        }


        DB::beginTransaction();
        $new_comment                    = new MediasComments;
        $new_comment->users_id          = $user->id;
        $new_comment->medias_id         = $post->id;
        $new_comment->comment           = $post_comment;
        if ($request->has('parents_id')) {
            $new_comment->reply_to          = $request->parents_id;
        } else {
            $new_comment->reply_to = 0;
        }
        $new_comment->save();
        if (isset($new_comment->media)) {
            $this->rankingService->addPointsToEarners($new_comment->media);
        }
        log_user_activity('medias', 'comment', $post->id);

        $medias = [];
        if (count($fileLists) > 0) {
            foreach ($fileLists as $file) {
                $filename = $user->id . '_' . time() . '_' . $file[1];
                Storage::disk('s3')->put(self::COMMENT_MEDIA_LOCATION . '/' . $filename, fopen($file[0], 'r+'), 'public');

                $media                  = new Media;
                $media->url             = S3_BASE_URL . '/' . self::COMMENT_MEDIA_LOCATION . '/' . $filename;;
                $media->author_name     = $user->name;
                $media->title           = strip_tags($post_comment);
                $media->author_url      = '';
                $media->source_url      = '';
                $media->license_name    = '';
                $media->license_url     = '';
                $media->users_id        = $user->id;
                $media->uploaded_at     = date('Y-m-d H:i:s');
                $media->type  = getMediaTypeByMediaUrl($media->url);
                $media->save();

                $_usersMedias = new UsersMedias();
                $_usersMedias->users_id = $user->id;
                $_usersMedias->medias_id = $media->id;
                $_usersMedias->save();

                $posts_comments_medias = new MediascommentsMedias;
                $posts_comments_medias->medias_comments_id   = $new_comment->id;
                $posts_comments_medias->medias_id = $media->id;
                $posts_comments_medias->save();
                $medias[] = $posts_comments_medias;
            }
            app(CommonApiController::class)->deleteTempFiles($pair);
        }
        // save tags of trip comment, report comment, post comment, event comment
        save_tags($tags, PostsTags::TYPE_TRIP_PLACE_MEDIA_COMMENT, $new_comment->id);

        DB::commit();
        if (count($medias) > 0) {
            foreach ($medias as $media) {
                $media->media;
            }
        }
        $new_comment->medias = $medias;
        $new_comment->text = convert_post_text_to_tags($new_comment->comment, $new_comment->tags, false);
        unset($new_comment->tags, $new_comment->comment);

        $new_comment->like_status = false;
        $new_comment->total_likes = 0;
        if (!$request->has('parents_id')) {
            $new_comment->sub;
        }

        $new_comment->author = $new_comment->user;
        unset($new_comment->user);
        if ($new_comment->author) {
            $new_comment->author->profile_picture = check_profile_picture($new_comment->author->profile_picture);
        }

        $new_comment->created_at =  Carbon::parse($new_comment->created_at)->format(Carbon::DEFAULT_TO_STRING_FORMAT);

        $post->url = check_media_url($post->url);
        return [
            'post_id'           => $postId,
            'post'              => $post,
            'comments_total'    => MediasComments::where('medias_id', $post->id)->where('reply_to', 0)->count(),
            'comment'           => $new_comment,
        ];
    }

    // delete Primary Post Comment Like Post, Trip etc...
    public function deleteCommentOrReply($type, $postId, $commentId)
    {
        // Checking Existing Post & Checking Existing Comment
        $post = $this->primaryPostService->find($type, $postId);
        if (!$post) throw new ModelNotFoundException('Post not found');

        if ($type == PrimaryPostService::TYPE_DISCUSSION) {
            $comment = DiscussionReplies::where('id', $commentId)->where('discussions_id', $postId)->first();
        } else {
            $comment = $this->findComment($type, $postId, $commentId);
        }

        if (!$comment) throw new ModelNotFoundException('Comment not found');

        if ($comment->users_id == auth()->id()) {
            switch ($type) {
                case PrimaryPostService::TYPE_POST:
                case ShareService::TYPE_TRIP_SHARE:
                case ShareService::TYPE_TRIP_PLACE_SHARE:
                case ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE:
                case PrimaryPostService::TYPE_REPORT:
                case PrimaryPostService::TYPE_TRIP:
                case PrimaryPostService::TYPE_EVENT:
                    $postsCommentsMedias = PostsCommentsMedias::where('posts_comments_id', $comment->id)->get();
                    if ($postsCommentsMedias->count() > 0) {
                        $mediasId = $postsCommentsMedias->pluck('medias_id')->toArray();
                        $medias = Media::whereIn('id', $mediasId)->get();
                        foreach ($medias as $media) {
                            deleteS3Media($media->url);
                            if ($media->type == Media::TYPE_VIDEO) {
                                deleteS3Media($media->video_thumbnail_url);
                            }
                        }
                        Media::whereIn('id', $mediasId)->delete();
                        UsersMedias::whereIn('medias_id', $mediasId)->delete();
                        PostsCommentsMedias::where('posts_comments_id', $comment->id)->delete();
                    }
                    break;

                case PrimaryPostService::TYPE_DISCUSSION:
                    deleteS3Media($comment->medias_url);
                    break;
            }

            $comment->delete();
            return [
                'message'           => ["Delete successfully"],
                'post_id'           => $postId,
                'id'                => $commentId,
            ];
        }

        throw new ModelNotFoundException('Only comment author can delete comment');
    }

    public function deleteCommentOrReplyOfTripPlace($request, $type, $postId, $commentId)
    {
        // Checking Existing Post & Checking Existing Comment
        $post = $this->primaryPostService->find($type, $postId);
        if (!$post) throw new ModelNotFoundException('Trip not found');

        $tripPlaces = TripPlaces::where('id', $request->trip_place_id)->where('trips_id', $postId)->first();
        if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');

        $comment = TripPlacesComments::where('trip_place_id', $tripPlaces->id)->where('id', $commentId)->first();
        if (!$comment) throw new ModelNotFoundException('Comment not found');

        if ($comment->user_id == auth()->id()) {
            $mediascommentsMedias = MediascommentsMedias::where('medias_comments_id', $comment->id)->get();
            if ($mediascommentsMedias->count() > 0) {
                $mediasId = $mediascommentsMedias->pluck('medias_id')->toArray();
                $medias = Media::whereIn('id', $mediasId)->get();
                foreach ($medias as $media) {
                    deleteS3Media($media->url);
                    if ($media->type == Media::TYPE_VIDEO) {
                        deleteS3Media($media->video_thumbnail_url);
                    }
                }
                Media::whereIn('id', $mediasId)->delete();
                UsersMedias::whereIn('medias_id', $mediasId)->delete();
                MediascommentsMedias::where('medias_comments_id', $comment->id)->delete();
            }
            $comment->delete();
            return [
                'message'           => ["Delete successfully"],
                'post_id'           => $postId,
                'id'                => $commentId,
            ];
        }

        throw new ModelNotFoundException('Only comment author can delete comment');
    }

    public function deleteCommentOrReplyOfTripPlaceMedia($request, $type, $postId, $commentId)
    {
        // Checking Existing Post & Checking Existing Comment
        $post = $this->primaryPostService->find($type, $postId);
        if (!$post) throw new ModelNotFoundException('Trip not found');

        $tripPlaces = TripPlaces::where('id', $request->trip_place_id)->where('trips_id', $postId)->first();
        if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');

        $tripMedia = TripMedias::where('id', $request->trip_place_media_id)->where('trips_id', $post->id)
            ->where('trip_place_id', $request->trip_place_id)->first();
        if (!$tripMedia) throw new ModelNotFoundException('Trip Place media not found');

        $comment = MediasComments::where('medias_id', $tripMedia->media->id)->where('id', $commentId)->first();
        if (!$comment) throw new ModelNotFoundException('Comment not found');

        if ($comment->users_id == auth()->id()) {
            $mediascommentsMedias = MediascommentsMedias::where('medias_comments_id', $comment->id)->get();
            if ($mediascommentsMedias->count() > 0) {
                $mediasId = $mediascommentsMedias->pluck('medias_id')->toArray();
                $medias = Media::whereIn('id', $mediasId)->get();
                foreach ($medias as $media) {
                    deleteS3Media($media->url);
                    if ($media->type == Media::TYPE_VIDEO) {
                        deleteS3Media($media->video_thumbnail_url);
                    }
                }
                Media::whereIn('id', $mediasId)->delete();
                UsersMedias::whereIn('medias_id', $mediasId)->delete();
                MediascommentsMedias::where('medias_comments_id', $comment->id)->delete();
            }
            $comment->delete();
            return [
                'message'           => ["Delete successfully"],
                'post_id'           => $postId,
                'id'                => $commentId,
            ];
        }

        throw new ModelNotFoundException('Only comment author can delete comment');
    }

    public function deleteCommentOrReplyOMedia($request, $postId, $commentId)
    {
        // Checking Existing Post & Checking Existing Comment
        $post = Media::where('id', $postId)->first();
        if (!$post) throw new ModelNotFoundException('media not found');

        $comment = MediasComments::where('medias_id', $post->id)->where('id', $commentId)->first();
        if (!$comment) throw new ModelNotFoundException('Comment not found');

        if ($comment->users_id == auth()->id()) {
            $mediascommentsMedias = MediascommentsMedias::where('medias_comments_id', $comment->id)->get();
            if ($mediascommentsMedias->count() > 0) {
                $mediasId = $mediascommentsMedias->pluck('medias_id')->toArray();
                $medias = Media::whereIn('id', $mediasId)->get();
                foreach ($medias as $media) {
                    deleteS3Media($media->url);
                    if ($media->type == Media::TYPE_VIDEO) {
                        deleteS3Media($media->video_thumbnail_url);
                    }
                }
                Media::whereIn('id', $mediasId)->delete();
                UsersMedias::whereIn('medias_id', $mediasId)->delete();
                MediascommentsMedias::where('medias_comments_id', $comment->id)->delete();
            }
            $comment->delete();
            return [
                'message'           => ["Delete successfully"],
                'post_id'           => $postId,
                'id'                => $commentId,
            ];
        }

        throw new ModelNotFoundException('Only comment author can delete comment');
    }


    // Liking // Liking // Liking // Liking // Liking // Liking // Liking // Liking 
    public function likeOrUnlike($type, $post_id, $comment_id)
    {
        $likeOrNotFlag = false;
        $user = Auth::user();

        $post = $this->primaryPostService->find($type, $post_id);
        if (!$post) throw new ModelNotFoundException('Post not found');

        $comment = $this->findComment($type, $post_id, $comment_id);
        if (!$comment) throw new ModelNotFoundException('Comment not found');

        DB::beginTransaction();
        $check_like_exists = PostsCommentsLikes::where('posts_comments_id', $comment_id)->where('users_id', $user->id)->first();
        if ($check_like_exists) {
            $check_like_exists->delete();
            log_user_activity($type, 'commentunlike', $post->id);
        } else {
            $likeOrNotFlag = true;
            $like = new PostsCommentsLikes;
            $like->posts_comments_id = $comment_id;
            $like->users_id = $user->id;
            $like->save();
            log_user_activity($type, 'commentlike', $post->id);
        }
        DB::commit();

        switch ($type) {
            case PrimaryPostService::TYPE_REPORT:
                $this->rankingService->addPointsToEarners($post);
                break;
        }

        return [
            'post_id'    => $post_id,
            'comment_id' => $comment_id,
            'like_status' => $likeOrNotFlag,
            'total_likes'  => $this->getCommentLikeCount($type, $comment_id)
        ];
    }

    public function likeOrUnlikeOfTripPlace($type, $post_id, $comment_id, $request)
    {
        $likeOrNotFlag = false;
        $user = Auth::user();

        $post = $this->primaryPostService->find($type, $post_id);
        if (!$post) throw new ModelNotFoundException('Trip not found');

        $tripPlaces = TripPlaces::where('id', $request->trip_place_id)->where('trips_id', $post_id)->first();
        if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');

        $comment = TripPlacesComments::where('trip_place_id', $tripPlaces->id)->where('id', $comment_id)->first();
        if (!$comment) throw new ModelNotFoundException('Comment not found');

        DB::beginTransaction();
        $check_like_exists = TripPlacesCommentsLikes::where('trip_place_comment_id', $comment_id)->where('user_id', $user->id)->first();
        if ($check_like_exists) {
            $check_like_exists->delete();
            log_user_activity('tripplace', 'commentunlike', $post->id);
        } else {
            $likeOrNotFlag = true;
            $like = new TripPlacesCommentsLikes;
            $like->trip_place_comment_id = $comment_id;
            $like->user_id = $user->id;
            $like->save();
            log_user_activity('tripplace', 'commentlike', $post->id);
        }
        DB::commit();

        try {
            broadcast(new TripPlaceCommentChangedEvent($comment_id));
        } catch (\Throwable $e) {
        }
        try {
            broadcast(new TripPlaceCommentLikeUnlike($comment_id));
        } catch (\Throwable $e) {
        }

        return [
            'post_id'    => $post_id,
            'comment_id' => $comment_id,
            'like_status' => $likeOrNotFlag,
            'total_likes'  => TripPlacesCommentsLikes::where('trip_place_comment_id', $comment_id)->count()
        ];
    }

    public function likeOrUnlikeOfTripPlaceMedia($type, $post_id, $comment_id, $request)
    {

        $likeOrNotFlag = false;
        $user = Auth::user();

        $post = $this->primaryPostService->find($type, $post_id);
        if (!$post) throw new ModelNotFoundException('Trip not found');

        $tripPlaces = TripPlaces::where('id', $request->trip_place_id)->where('trips_id', $post_id)->first();
        if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');

        $tripMedia = TripMedias::where('id', $request->trip_place_media_id)
            ->where('trips_id', $post_id)
            ->where('trip_place_id', $request->trip_place_id)->first();
        if (!$tripMedia) throw new ModelNotFoundException('Trip Place media not found');

        $comment = MediasComments::where('id', $comment_id)->first();
        if (!$comment) throw new ModelNotFoundException('Comment not found');

        DB::beginTransaction();
        $check_like_exists = MediasCommentsLikes::where('medias_comments_id', $comment_id)->where('users_id', $user->id)->first();
        if ($check_like_exists) {
            $check_like_exists->delete();
            log_user_activity('tripplacemedia', 'commentunlike', $post->id);
        } else {
            $likeOrNotFlag = true;
            $like = new MediasCommentsLikes;
            $like->medias_comments_id = $comment_id;
            $like->users_id = $user->id;
            $like->save();
            log_user_activity('tripplacemedia', 'commentlike', $post->id);
        }
        DB::commit();

        try {
            broadcast(new TripMediaCommentChangedEvent($comment_id));
        } catch (\Throwable $e) {
        }
        try {
            broadcast(new TripPlaceMediaCommentLikeUnLike($comment_id));
        } catch (\Throwable $e) {
        }

        return [
            'post_id'    => $post_id,
            'comment_id' => $comment_id,
            'like_status' => $likeOrNotFlag,
            'total_likes'  => MediasCommentsLikes::where('medias_comments_id', $comment_id)->count()
        ];
    }

    public function likeOrUnlikeOfMedia($post_id, $comment_id)
    {
        $likeOrNotFlag = false;
        $user = Auth::user();

        $post = Media::where('id', $post_id)->first();
        if (!$post) throw new ModelNotFoundException('media not found');

        $comment = MediasComments::where('id', $comment_id)->where('medias_id', $post->id)->first();
        if (!$comment) throw new ModelNotFoundException('Comment not found');

        DB::beginTransaction();
        $check_like_exists = MediasCommentsLikes::where('medias_comments_id', $comment_id)->where('users_id', $user->id)->first();
        if ($check_like_exists) {
            $check_like_exists->delete();
            log_user_activity('tripplacemedia', 'commentunlike', $post->id);
        } else {
            $likeOrNotFlag = true;
            $like = new MediasCommentsLikes;
            $like->medias_comments_id = $comment_id;
            $like->users_id = $user->id;
            $like->save();
            log_user_activity('tripplacemedia', 'commentlike', $post->id);
        }
        DB::commit();

        return [
            'post_id'    => $post_id,
            'comment_id' => $comment_id,
            'like_status' => $likeOrNotFlag,
            'total_likes'  => MediasCommentsLikes::where('medias_comments_id', $comment_id)->count()
        ];
    }
}
