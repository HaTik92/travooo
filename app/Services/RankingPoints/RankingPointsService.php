<?php

namespace App\Services\RankingPoints;

use App\Models\Ranking\RankingUsersPoints;

class RankingPointsService
{
    /**
     * @param int $userId
     * @return RankingUsersPoints|\Illuminate\Database\Eloquent\Model|null
     */
    public function findByUserId($userId)
    {
        return RankingUsersPoints::where('users_id', $userId)->first();
    }

    /**
     * @param int $userId
     * @return RankingUsersPoints|\Illuminate\Database\Eloquent\Model
     */
    public function create($userId)
    {
        return RankingUsersPoints::create([
            'users_id' => $userId,
            'points' => 0
        ]);
    }

    public function addPoints($userId, $amount)
    {
        $rankingPoints = $this->findByUserId($userId);

        if (!$rankingPoints) {
            $rankingPoints = $this->create($userId);
        }

        $rankingPoints->points += (int)$amount;
        $rankingPoints->save();
    }

    public function subPoints($userId, $amount)
    {
        $rankingPoints = $this->findByUserId($userId);

        if (!$rankingPoints) {
            return;
        }

        $rankingPoints->points -= (int)$amount;
        $rankingPoints->save();
    }
}
