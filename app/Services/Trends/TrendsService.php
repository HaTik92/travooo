<?php

namespace App\Services\Trends;

use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Place\Place;
use App\Models\PlacesTop\PlacesTop;
use App\Models\TripPlans\TripPlans;
use App\Services\Trips\TripInvitationsService;
use App\Services\Visits\VisitsService;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class TrendsService
{
    const GLOBAL_TYPE = 0;
    const LOCAL_TYPE = 1;
    const NOT_TRENDING = 2;

    const LOCATION_TYPE_PLACE = 0;
    const LOCATION_TYPE_CITY = 1;
    const LOCATION_TYPE_COUNTRY = 2;

    const PLACE_TYPE_TRENDING = 0;
    const PLACE_TYPE_TRENDING_FRIENDS = 1;
    const PLACE_TYPE_FRIENDS = 2;
    const PLACE_TYPE_SUGGESTED = 3;
    const PLACE_TYPE_NEARBY = 4;

    const CACHE_GET_ENABLE = false;
    const CACHE_PUT_ENABLE = false;
    const CACHE_TIME = 60;
    const CACHE_PREFIX = 'TRIP_PLANNER_TRENDS_V5_';
    const CACHE_ALL_PREFIX = 'ALL_00005_';
    const CACHE_SUGGESTED_PREFIX = 'SUGGESTED_00006_';

    const ES_URL = 'https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/';

    const HOTELS_TYPES = [
        'lodging'
    ];

    const REST_TYPES = [
        'restaurant',
        'food',
        'cafe',
        'bar',
        'bakery',
        'meal_delivery',
        'meal_takeaway',
    ];
    const DAYS_RANGE = 90;
    const MIN_USERS_COUNT = 1;

    /**
     * @var TripInvitationsService
     */
    private $tripInvitationsService;
    /**
     * @var VisitsService
     */
    private $visitsService;

    protected $authUser = null;
    protected $local = [];

    public $counter = 0;
    public $nationality = null;
    public $withNotTrendingCities = false;

    public $debugData = [];

    public $forceUpdateEsCache = false;
    public $handledCities = [];

    public function __construct(TripInvitationsService $tripInvitationsService, VisitsService $visitsService)
    {
        $this->tripInvitationsService = $tripInvitationsService;
        $this->visitsService = $visitsService;
        $this->authUser = auth()->user();
        if ($this->authUser) {
            $this->nationality = $this->authUser->nationality;
            $friends = $this->tripInvitationsService->findFriends();
            $this->local = $friends->merge($this->tripInvitationsService->findFollowers())->unique();
        }

        $this->debugData = [
            'friends' => 0,
            'top' => 0,
            'places_sql' => 0,
            'test' => 0,
            'test2' => 0,
            'other' => 0
        ];
    }

    public function getRecursiveTrendingLocations($location = null, $parentId = null, $nearby = false, $except = [], $latlng = [], $distance = 3, $withLimit = true)
    {
        if ($location === null) {
            $location = self::LOCATION_TYPE_COUNTRY;
        }

        $data = $this->getMixTrendingLocations($location, $parentId, $nearby, $except, $latlng, $distance, $withLimit);
        gc_collect_cycles();
        $data = $data->toArray();

        $savedResults = [];

        if ($location > 0) {
            foreach ($data as $key => $locationData) {
                if (!isset($savedResults[$location - 1])) {
                    $savedResults[$location - 1] = [];
                }

                if (!isset($savedResults[$location - 1][$locationData['location']['id']])) {
                    $savedResults[$location - 1][$locationData['location']['id']] = $this->getRecursiveTrendingLocations($location - 1, $locationData['location']['id'], $nearby, $except, $latlng, $distance, $withLimit);
                    gc_collect_cycles();
                }

                $data[$key]['related_locations'] = $savedResults[$location - 1][$locationData['location']['id']];

                if (!count($data[$key]['related_locations'])) {
                    $data[$key] = null;
                    unset($data[$key]);
                }
            }
        }

        $savedResults = null;
        unset($savedResults);

        if ($location === self::LOCATION_TYPE_COUNTRY || $location === self::LOCATION_TYPE_CITY) {
            $slicedData = [];

            $globalCount = 5;
            $localCount = 5;

            foreach ($data as $key => $locationData) {
                $id = $locationData['location']['id'];

                if ($globalCount > 0 && $locationData['type'] === self::GLOBAL_TYPE) {
                    if (isset($slicedData[$id])) {
                        continue;
                    }
                    $slicedData[$id] = $locationData;
                    $globalCount--;
                    continue;
                }

                if ($localCount > 0 && $locationData['type'] === self::LOCAL_TYPE) {
                    if (isset($slicedData[$id])) {
                        continue;
                    }
                    $slicedData[$id] = $locationData;
                    $localCount--;
                }
            }

            if ($location === self::LOCATION_TYPE_CITY && $this->withNotTrendingCities && count($slicedData) < 10) {
                $notTrendingCities = Cities::query()
                    ->with(['trans'])
                    ->whereNotIn('id', array_merge(array_keys($slicedData)))
                    ->where('countries_id', $parentId)
                    ->where(function ($q) {
                        $q->where('lat', '!=', 0);
                        $q->orWhere('lng', '!=', 0);
                    })
                    //->limit(10 - count($slicedData))
                    ->get();

                foreach ($notTrendingCities as $notTrendingCity) {
                    $img = @$notTrendingCity->first_city_medias[0]->url;
                    $src = S3_BASE_URL . 'th700/' . $img;
                    $thumbSrc = S3_BASE_URL . 'th180/' . $img;

                    if (!$img) {
                        $img = url(PLACE_PLACEHOLDERS);
                        $src = $img;
                        $thumbSrc = $img;
                    }

                    $slicedData[$notTrendingCity->id] = [
                        'type' => self::NOT_TRENDING,
                        'location_type' => self::LOCATION_TYPE_CITY,
                        'title' => $notTrendingCity->trans[0]->title ?? null,
                        'src' => $src,
                        'thumb_src' => $thumbSrc,
                        'location' => $notTrendingCity,
                        'local' => '',
                        'related_locations' => [] //$this->getRecursiveTrendingLocations($location - 1, $notTrendingCity->id, $nearby, $except, $latlng, $distance, $withLimit)
                    ];
                }
            }

            return collect($slicedData);
        }

        return collect($data);
    }

    /**
     * @param int $location
     * @param null $parentId
     * @param bool $nearby
     * @param array $except
     * @param array $latlng
     * @param int $distance
     * @param bool $withSuggestedLimit
     * @return Collection
     */
    public function getMixTrendingLocations($location, $parentId = null, $nearby = false, $except = [], $latlng = [], $distance = 3, $withSuggestedLimit = true)
    {
        $data = $this->getTrendingLocations($location, self::LOCAL_TYPE, $parentId, $except, $nearby, $latlng, $distance, $withSuggestedLimit);

        $isCityOrCountry = $location === self::LOCATION_TYPE_COUNTRY || $location === self::LOCATION_TYPE_CITY;
        if ($isCityOrCountry) {
            $allExcept = $except;
        } else {
            $allExcept = $data->pluck('location.id')->merge($except);
        }

        $allData = $data->merge($this->getTrendingLocations($location, self::GLOBAL_TYPE, $parentId, $allExcept, $nearby, $latlng, $distance, $withSuggestedLimit));
        if ($isCityOrCountry) {
            $data = collect();
            $allData->map(function ($item) use (&$data) {
                $data[$item['type'] . '_' . $item['location']['id']] = $item;
            });
            $allData = null;
        } else {
            $data = $allData->keyBy('location.id');

            if ($nearby) {
                if ($parentId) {
                    $cityId = Place::query()->find($parentId)->cities_id;
                } else {
                    $cityId = 0;
                }
                if (!empty($latlng)) {
                    $data = $data->slice(0, 50);
                } else {
                    $data = $data->slice(0, 10);
                }

                $except[] = $parentId;
            } else {
                $cityId = $parentId;
                if ($withSuggestedLimit) {
                    $data = $data->slice(0, 50);
                }
            }

            if ($this->authUser && (!$nearby || !empty($lnglat))) {
                $visitPlaces = $this->visitsService->getRelatedVisits([$cityId], false, 'all', self::LOCATION_TYPE_CITY, $except);
                $data = $this->addVisitedPlacesToData($data, $visitPlaces, $nearby, $latlng, $withSuggestedLimit);
            }
        }

        return $data;
    }

    /**
     * @param int $location
     * @param int $type
     * @param null $parentId
     * @param array $exceptIds
     * @param bool $nearby
     * @param array $latlng
     * @param int $distance
     * @param bool $withSuggestedLimit
     * @return Collection
     */
    public function getTrendingLocations($location, $type, $parentId = null, $exceptIds = [], $nearby = false, $latlng = [], $distance = 3, $withSuggestedLimit = true)
    {
        $nearbyPlaces = null;
        $beforeParentId = null;

        if ($nearby) {
            $nearbyPlaces = collect();

            if (empty($latlng)) {
                $distance = 0.5;
                while (!$nearbyPlaces->where('id', '!=', $parentId)->count() && $distance <= 2) {
                    $nearbyPlaces = $this->getNearbyPlaces($parentId, $exceptIds, $distance, $latlng);
                    $distance += 0.5;
                }

                if (!$nearbyPlaces->where('id', '!=', $parentId)->count()) {
                    return collect();
                }
            } else {
                $nearbyPlaces = $this->getNearbyPlaces($parentId, $exceptIds, $distance, $latlng);
            }

            if ($nearbyPlaces->count()) {
                $beforeParentId = $parentId;
                $parentId = $nearbyPlaces->first()->cities_id;
            }
        }

        $select = [
            'trips.*',
            'trips_places.time',
            'trips_places.id as trip_place_id',
            'trips_places.cities_id',
            'trips_places.countries_id',
            'trips_places.places_id',
            'trips_places.trips_id'
        ];

        $query = TripPlans::query()
            ->select($select)
            ->selectRaw('count(DISTINCT CONCAT(trips_places.countries_id, \' \', trips.id)) as countries_count')
            ->selectRaw('count(DISTINCT CONCAT(trips_places.cities_id, \' \', trips.id)) as cities_count')
            ->selectRaw('count(DISTINCT CONCAT(trips_places.places_id, \' \', trips.id)) as places_count')
            ->selectRaw('count(DISTINCT trips.users_id) as users_count')
            ->join('trips_places', function ($q) use ($parentId, $location, $type, $nearby, $nearbyPlaces) {
                $from = Carbon::now()->subDay(self::DAYS_RANGE);
                $to = Carbon::now()->addDay(self::DAYS_RANGE);

                $q->on('trips.active_version', 'trips_places.versions_id')
                    ->where('trips_places.time', '<=', $to)
                    ->where('trips_places.time', '>=', $from)
                    ->where('trips_places.cities_id', '!=', 2031)
                    ->whereNull('trips_places.deleted_at');

                if (!$nearby || $parentId) {
                    switch ($location) {
                        case self::LOCATION_TYPE_PLACE:
                            $q->where('trips_places.cities_id', $parentId);
                            break;
                        case self::LOCATION_TYPE_CITY:
                            $q->where('trips_places.countries_id', $parentId);
                            break;
                        default:
                            break;
                    }
                }
            })
            ->where('active_version', '!=', 0);


        if ($nearbyPlaces) {
            $query->whereIn('places_id', $nearbyPlaces->pluck('id'));
        }

        switch ($type) {
            case self::LOCAL_TYPE:
                $cacheKey = self::CACHE_PREFIX . self::CACHE_ALL_PREFIX . $location . '_' . $parentId . '_' . $this->nationality;
                $query->whereHas('author', function ($q) {
                    $q->where('nationality', $this->nationality);
                });
                break;
            case self::GLOBAL_TYPE:
                $cacheKey = self::CACHE_PREFIX . self::CACHE_ALL_PREFIX . $location . '_' . $parentId;
                break;
            default:
                break;
        }

        $result = collect();

        switch ($location) {
            case self::LOCATION_TYPE_PLACE:
                $local = $this->local;

                $query->groupBy('places_id');
                $query->orderByDesc('places_count');
                $query->limit(200);
                $friendsQuery = clone $query;
                $friendsQuery->whereIn('users_id', $local);

                $query->having('users_count', '>=', self::MIN_USERS_COUNT);

                if (self::CACHE_GET_ENABLE && !$nearby && Cache::has($cacheKey) && $this->authUser) {
                    $places = Cache::get($cacheKey);
                } else {
                    $places = $query->get();

                    if (!$nearby && self::CACHE_PUT_ENABLE) {
                        if (self::CACHE_PUT_ENABLE) Cache::put($cacheKey, $places, self::CACHE_TIME);
                    }
                }

                $places = $places->whereNotIn('places_id', $exceptIds);
                $time1 = microtime(true);
                $trendingAmongFriends = $friendsQuery->get();
                $this->debugData['friends'] += microtime(true) - $time1;

                $cityRestsAndHotels = null;

                if ($nearby) {
                    if (isset($place)) {
                        $currentType = explode(',', $place->place_type);
                    } else {
                        $currentType = [];
                    }

                    $maxLimit = 12.5;

                    if (!empty($latlng)) {
                        $maxLimit = 50;
                    }

                    $places = $this->getNearbyPlacesByPlaceTypesRatio($places->pluck('places_id'), $currentType, $maxLimit);
                } else {
                    $placesIds = $places->pluck('places_id');

                    if ($placesIds->count()) {
                        $time = microtime(true);
                        $cityRestsAndHotels = $this->getElasticPlaces($parentId, array_merge(self::REST_TYPES, self::HOTELS_TYPES));
                        $placesIds = $cityRestsAndHotels->intersect($placesIds);
                        $this->debugData['other'] += microtime(true) - $time;
                    }

                    $places = $places->whereIn('places_id', $placesIds)->slice(0, 13);
                }

                $suggestedPlaces = collect();

                if ($type === self::GLOBAL_TYPE) {
                    $suggestedPlacesQuery = PlacesTop::query()
                        ->whereNotIn('places_id', $places->pluck('places_id'))
                        ->whereNotIn('places_id', $exceptIds)
                        ->with(['place', 'place.trans']);

                    if (!$nearby) {
                        $time = microtime(true);
                        $suggestedPlacesQuery->where('city_id', $parentId);
                        $cacheKey = self::CACHE_PREFIX . self::CACHE_SUGGESTED_PREFIX . $location . '_' . $parentId;

                        if (self::CACHE_GET_ENABLE && Cache::has($cacheKey) && $this->authUser) {
                            $suggestedPlaces = Cache::get($cacheKey);

                            if ($withSuggestedLimit) {
                                $suggestedPlaces = $suggestedPlaces->slice(0, 25);
                            }
                        } else {
                            $suggestedPlaceIds = $suggestedPlacesQuery->pluck('places_id');
                            if (is_null($cityRestsAndHotels)) {
                                $cityRestsAndHotels = $this->getElasticPlaces($parentId, array_merge(self::REST_TYPES, self::HOTELS_TYPES));
                            }
                            $suggestedPlaceIds = $cityRestsAndHotels->intersect($suggestedPlaceIds);
                            $suggestedPlacesQuery->whereIn('places_id', $suggestedPlaceIds);

                            $suggestedPlaces = $suggestedPlacesQuery->get();

                            if (self::CACHE_PUT_ENABLE) Cache::put($cacheKey, $suggestedPlaces, self::CACHE_TIME);

                            if ($withSuggestedLimit) {
                                $suggestedPlaces = $suggestedPlaces->slice(0, 25);
                            }
                        }
                        $this->debugData['top'] += microtime(true) - $time;
                    } else {
                        $maxLimit = 25;

                        if (!empty($latlng)) {
                            $maxLimit = 50;
                        }

                        $suggestedPlaces = $suggestedPlacesQuery->whereIn('places_id', $nearbyPlaces->pluck('id'))->get();
                        $suggestedPlaces = $this->getNearbyPlacesByPlaceTypesRatio($suggestedPlaces->pluck('places_id'), $currentType, $maxLimit);
                    }

                    $places = $places->merge($suggestedPlaces);
                }

                $leftNearbyPlaces = collect();

                if ($nearby && $type === self::GLOBAL_TYPE) {
                    $alreadyAdded = $places->pluck('places_id');
                    $leftNearbyPlaces = $nearbyPlaces->pluck('id')->reject(function ($item) use ($alreadyAdded) {
                        return in_array($item, $alreadyAdded->toArray());
                    });

                    $leftNearbyPlaces->map(function ($item) use ($places, $latlng) {

                        if (!empty($latlng) && $places->count() >= 25) {
                            return;
                        }

                        $place = Place::query()->find($item);

                        if ((array_intersect(self::REST_TYPES, explode(',', $place->place_type))
                            || array_intersect(self::HOTELS_TYPES, explode(',', $place->place_type)))) {
                            $obj = new \stdClass();
                            $obj->places_id = $item;
                            $places->push($obj);
                        }
                    });

                    if (!$places->where('places_id', '!=', $beforeParentId)->count()) {
                        return collect();
                    }
                }

                $nationality = @$this->authUser->country_of_nationality->transsingle->title;

                if ($places->count()) {
                    $time = microtime(true);

                    $placesQuery =  Place::query()->with(['trans', 'city', 'city.trans', 'city.city_medias'])->whereIn('id', $places->pluck('places_id'));

                    if ($parentId) {
                        $query->where('cities_id', $parentId);
                    }

                    $placesModels = $placesQuery->get()->keyBy('id')->all();
                    $this->debugData['places_sql'] += microtime(true) - $time;
                }

                foreach ($places as $entity) {
                    if (!isset($placesModels[$entity->places_id])) {
                        continue;
                    }

                    $location = $placesModels[$entity->places_id];

                    if (!$location) {
                        continue;
                    }

                    $img = @$location->media[0]->url;
                    $src = S3_BASE_URL . 'th700/' . $img;
                    $thumbSrc = S3_BASE_URL . 'th180/' . $img;

                    if (!$img) {
                        $img = url(PLACE_PLACEHOLDERS);
                        $src = $img;
                        $thumbSrc = $img;
                    }

                    $placeType = self::PLACE_TYPE_TRENDING;

                    if (!$nationality) {
                        $nationality = '';
                    }

                    $isTrendingAmongFriends = $trendingAmongFriends->where('places_id', $entity->places_id)->first();
                    $isSuggested = $suggestedPlaces->where('places_id', $entity->places_id)->first();
                    $isNearby = in_array($entity->places_id, $leftNearbyPlaces->toArray());

                    if ($isTrendingAmongFriends) {
                        if ($isSuggested) {
                            $placeType = self::PLACE_TYPE_FRIENDS;
                        } else {
                            $placeType = self::PLACE_TYPE_TRENDING_FRIENDS;
                        }
                    } elseif ($isSuggested) {
                        $placeType = self::PLACE_TYPE_SUGGESTED;
                    } elseif ($isNearby) {
                        $placeType = self::PLACE_TYPE_SUGGESTED;
                    }

                    $placeCategory = 'P';

                    foreach (explode(',', $location->place_type) as $placeCategoryPart) {
                        if (in_array($placeCategoryPart, self::REST_TYPES)) {
                            $placeCategory = 'R';
                            break;
                        }

                        if (in_array($placeCategoryPart, self::HOTELS_TYPES)) {
                            $placeCategory = 'H';
                            break;
                        }
                    }


                    $cityImg = @$location->city->first_city_medias[0]->url;
                    $citySrc = S3_BASE_URL . 'th180/' . $cityImg;

                    if (!$cityImg) {
                        $citySrc = url(PLACE_PLACEHOLDERS);
                    }
                    $city = $location->city;
                    if ($city) {
                        $city->title = $city->trans[0]->title ?? null;
                    }

                    $result->push([
                        'type' => $type,
                        'location_type' => self::LOCATION_TYPE_PLACE,
                        'title' => $location->trans[0]->title ?? null,
                        'place_type' => $placeType,
                        'place_category' => $placeCategory,
                        'src' => $src,
                        'local' => $type === self::LOCAL_TYPE ? $nationality : '',
                        'thumb_src' => $thumbSrc,
                        'location' => $location,
                        'city' => $city,
                        'city_img' => $citySrc
                    ]);
                }

                $places = null;
                unset($places);
                break;
            case self::LOCATION_TYPE_CITY:
                $query->groupBy('cities_id');
                $query->orderByDesc('cities_count');
                $query->with(['trips_places.city', 'trips_places.city.trans']);
                $query->limit(3);
                $query->having('users_count', '>=', self::MIN_USERS_COUNT);

                if (self::CACHE_GET_ENABLE && Cache::has($cacheKey) && $this->authUser) {
                    $cities = Cache::get($cacheKey);
                } else {
                    $cities = $query->get();
                    if (self::CACHE_PUT_ENABLE) Cache::put($cacheKey, $cities, self::CACHE_TIME);
                }

                $cities = $cities->whereNotIn('cities_id', $exceptIds);

                $citiesModels = Cities::query()->with(['trans'])->whereIn('id', $cities->pluck('cities_id'))->get()->keyBy('id')->all();

                foreach ($cities as $entity) {
                    if (!isset($citiesModels[$entity->cities_id])) {
                        continue;
                    }

                    $location = $citiesModels[$entity->cities_id];

                    $img = @$location->first_city_medias[0]->url;
                    $src = S3_BASE_URL . 'th700/' . $img;
                    $thumbSrc = S3_BASE_URL . 'th180/' . $img;

                    if (!$img) {
                        $img = url(PLACE_PLACEHOLDERS);
                        $src = $img;
                        $thumbSrc = $img;
                    }

                    $nationality = @$this->authUser->country_of_nationality->transsingle->title;

                    if (!$nationality) {
                        $nationality = '';
                    }

                    $result->push([
                        'type' => $type,
                        'location_type' => self::LOCATION_TYPE_CITY,
                        'title' => $location->trans[0]->title ?? null,
                        'src' => $src,
                        'thumb_src' => $thumbSrc,
                        'location' => $location,
                        'local' => $type === self::LOCAL_TYPE ? $nationality : ''
                    ]);
                }

                break;
            case self::LOCATION_TYPE_COUNTRY:
                $query->groupBy('countries_id');
                $query->orderByDesc('countries_count');
                $query->limit(3);
                $query->having('users_count', '>=', self::MIN_USERS_COUNT);

                if (self::CACHE_GET_ENABLE && Cache::has($cacheKey) && $this->authUser) {
                    $countries = Cache::get($cacheKey);
                } else {
                    $countries = $query->get();
                    if (self::CACHE_PUT_ENABLE) Cache::put($cacheKey, $countries, self::CACHE_TIME);
                }

                $countries = $countries->whereNotIn('countries_id', $exceptIds);
                $countriesModels = Countries::query()->with(['trans'])->whereIn('id', $countries->pluck('countries_id'))->get()->keyBy('id')->all();

                foreach ($countries as $entity) {
                    if (!isset($countriesModels[$entity->countries_id])) {
                        continue;
                    }

                    $location = $countriesModels[$entity->countries_id];

                    //$img = @$location->country_medias[0]->url;
                    $img = null;

                    if (!$img) {
                        $img = get_country_flag($location);
                    } else {
                        $img = S3_BASE_URL . 'th700/' . $img;
                    }

                    $nationality = @$this->authUser->country_of_nationality->transsingle->title;

                    if (!$nationality) {
                        $nationality = '';
                    }

                    $result->push([
                        'type' => $type,
                        'location_type' => self::LOCATION_TYPE_COUNTRY,
                        'title' => $location->trans[0]->title ?? null,
                        'src' => $img,
                        'thumb_src' => $img,
                        'location' => $location,
                        'local' => $type === self::LOCAL_TYPE ? $nationality : ''
                    ]);
                }

                break;
            default:
                break;
        }

        return $result;
    }

    protected function getNearbyPlaces($parentId, $exceptIds, $distance, $latlng)
    {
        if (!empty($latlng)) {
            $lat = $latlng[0];
            $lng = $latlng[1];

            $cityId = null;

            //            $closestCityQuery = $query = DB::table('cities')
            //                ->select(DB::raw('id, ((ACOS(SIN('.$lat.' * PI() / 180) * SIN(lat * PI() / 180) + COS('.$lat.' * PI() / 180) * COS(lat * PI() / 180) * COS(('.$lng.' - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance'))
            //                ->orderBy('distance', 'ASC');
            //
            //            $city = $closestCityQuery->first();
            //
            //            if ($city) {
            //                $cityId = $city->id;
            //            }
        } else {
            $place = Place::query()->where('id', $parentId)->first();

            $lat = $place->lat;
            $lng = $place->lng;

            //$cityId = $place->cities_id;
            $cityId = null;
        }

        if (empty($latlng)) {
            $km = $distance;
        } else {
            $km = $distance > 5 ? 5 : $distance;
        }

        $distance = $km * 0.009;

        $range = [];
        $range['min_lat'] = $lat - $distance;
        $range['max_lat'] = $lat + $distance;
        $range['min_lng'] = $lng - ($distance / cos($lat * M_PI / 180));
        $range['max_lng'] = $lng + ($distance / cos($lat * M_PI / 180));

        $distance = $km;

        $cacheKey = self::CACHE_PREFIX . 'nearby_6_' . implode('-', array_values($range));

        if (self::CACHE_GET_ENABLE && Cache::has($cacheKey)) {
            $nearbyPlaces = Cache::get($cacheKey);
        } else {
            $query = DB::table('places')
                ->select(DB::raw('id, cities_id, ((ACOS(SIN(' . $lat . ' * PI() / 180) * SIN(lat * PI() / 180) + COS(' . $lat . ' * PI() / 180) * COS(lat * PI() / 180) * COS((' . $lng . ' - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance'))
                ->havingRaw('distance <= ?', [$distance])
                ->orderBy('distance', 'ASC')
                ->where('lat', '>=', $range['min_lat'])
                ->where('lng', '>=', $range['min_lng'])
                ->where('lat', '<=', $range['max_lat'])
                ->where('lng', '<=', $range['max_lng'])
                ->whereNotIn('id', $exceptIds);

            if ($cityId) {
                $query->where('cities_id', $cityId);
            }

            $nearbyPlaces = $query->get();
        }

        if (self::CACHE_PUT_ENABLE) Cache::put($cacheKey, $nearbyPlaces, self::CACHE_TIME);

        return $nearbyPlaces;
    }

    protected function getNearbyPlacesByPlaceTypesRatio($placesId, $currentType, $maxLimit)
    {
        if (count(array_intersect($currentType, self::HOTELS_TYPES)) > 0) {
            $nearbyLimits = [(int)($maxLimit * 0.2), (int)($maxLimit * 0.3), (int)($maxLimit * 0.5)];
        } elseif (count(array_intersect($currentType, self::REST_TYPES)) > 0) {
            $nearbyLimits = [(int)($maxLimit * 0.3), (int)($maxLimit * 0.2), (int)($maxLimit * 0.5)];
        } else {
            $nearbyLimits = [(int)($maxLimit * 0.5), (int)($maxLimit * 0.3), (int)($maxLimit * 0.2)];
        }

        $hotels = Place::query()
            ->whereIn('id', $placesId);

        $hotels->where(function ($q) {
            foreach (self::HOTELS_TYPES as $hotelType) {
                $q->orWhere('place_type', 'like', '%' . $hotelType . '%');
            }
        });

        $resultHotels = $hotels->limit($nearbyLimits[0])->get();

        $rests = Place::query()
            ->whereIn('id', $placesId)
            ->whereNotIn('id', $resultHotels->pluck('id'));

        $rests->where(function ($q) {
            foreach (self::REST_TYPES as $restType) {
                $q->orWhere('place_type', 'like', '%' . $restType . '%');
            }
        });

        $limit = $nearbyLimits[1];

        if ($resultHotels->count() < ($nearbyLimits[0])) {
            $limit = $limit + $nearbyLimits[0] - $resultHotels->count();
        }

        $resultRests = $rests->limit($limit)->get();

        $allPlaces = Place::query()
            ->whereIn('id', $placesId)
            ->whereNotIn('id', $resultRests->pluck('id'));

        $allPlaces->where(function ($q) {
            $q->where(function ($q) {
                foreach (array_merge(self::REST_TYPES, self::HOTELS_TYPES) as $otherPlaceType) {
                    $q->where('place_type', 'not like', '%' . $otherPlaceType . '%');
                }
            })->orWhereNull('place_type');
        });


        $restsAndHotelsCount = $resultHotels->pluck('id')->merge($resultRests->pluck('id'))->count();

        $limit = $nearbyLimits[2];

        if ($restsAndHotelsCount < ($nearbyLimits[0] + $nearbyLimits[1])) {
            $limit = ceil($maxLimit) - $restsAndHotelsCount;
        }

        $resultAllPlaces = $allPlaces->limit($limit)->get();
        $resultAllPlaces = $resultAllPlaces->pluck('id')->merge($resultRests->pluck('id'))->merge($resultHotels->pluck('id'));

        $result = collect();

        $resultAllPlaces->map(function ($item) use ($result) {
            $obj = new \stdClass();
            $obj->places_id = $item;
            $result->push($obj);
        });

        return $result;
    }

    /**
     * @param Collection $data
     * @param Collection $visitedPlaces
     * @param bool $nearby
     * @param $latlng
     * @param bool $withSuggestedLimit
     * @return Collection
     */
    protected function addVisitedPlacesToData($data, $visitedPlaces, $nearby, $latlng, $withSuggestedLimit = true)
    {
        if ($nearby) {
            $limit = 3;

            if (!empty($latlng)) {
                $dataLimit = 50;
            } else {
                $dataLimit = 10;
            }
        } else {
            $limit = 10;
            $dataLimit = 50;
        }

        if (!$withSuggestedLimit && $data->count() >= $dataLimit) {
            return $data;
        }

        $intersects = $data->keys()->intersect($visitedPlaces);

        $addToData = collect();

        if ($intersects->count() < $limit) {
            $addToData = $visitedPlaces->filter(function ($item) use ($intersects) {
                return !in_array($item, $intersects->toArray());
            })->slice(0, $limit - $intersects->count());
        }

        if ($data->count() + $addToData->count() > $dataLimit) {
            $dataWithoutIntersections = $data->whereNotIn('location.id', $intersects);
            $slicedDataWithoutIntersections = $dataWithoutIntersections->slice(0, $dataWithoutIntersections->count() - ($data->count() + $addToData->count() - $dataLimit));
            $slicedData = $data->whereIn('location.id', $slicedDataWithoutIntersections->keys()->merge($intersects));
            $data = $slicedData;
        }

        $addToData = $addToData->slice(0, $dataLimit - $data->count());

        foreach ($addToData as $placeId) {
            $location = Place::query()->with('trans')->find($placeId);

            if (!$location) {
                continue;
            }

            $img = @$location->media[0]->url;
            $src = S3_BASE_URL . 'th700/' . $img;
            $thumbSrc = S3_BASE_URL . 'th180/' . $img;

            if (!$img) {
                $img = url(PLACE_PLACEHOLDERS);
                $src = $img;
                $thumbSrc = $img;
            }

            $placeCategory = 'P';

            if ($location->place_type)
                foreach (explode(',', $location->place_type) as $placeCategoryPart) {
                    if (in_array($placeCategoryPart, self::REST_TYPES)) {
                        $placeCategory = 'R';
                        break;
                    }

                    if (in_array($placeCategoryPart, self::HOTELS_TYPES)) {
                        $placeCategory = 'H';
                        break;
                    }
                }

            $city = $location->city()->with('trans')->first();
            if ($city) {
                $city->title = $city->trans[0]->title ?? null;
            }

            $place = [
                'type' => self::GLOBAL_TYPE,
                'location_type' => self::LOCATION_TYPE_PLACE,
                'title' => $location->trans[0]->title ?? null,
                'src' => $src,
                'thumb_src' => $thumbSrc,
                'place_type' => self::PLACE_TYPE_FRIENDS,
                'local' => '',
                'location' => $location,
                'place_category' => $placeCategory,
                'city' => $city
            ];

            $data->put($placeId, $place);
        }

        return $data;
    }

    public function getElasticPlaces($cityId = null, $exceptTypes = [])
    {
        $cacheKey = self::CACHE_PREFIX . (string)$cityId . '_' . implode('-', (array)$exceptTypes);

        if (!$this->forceUpdateEsCache && (1 || self::CACHE_GET_ENABLE) && Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $result = [];
        $mustTerms = [];

        if ($cityId) {
            $mustTerms[] = ['terms' => [
                'cities_id' => [$cityId],
            ]];
        }

        $params = [
            'query' => [
                'bool' => [
                    'must_not' => [
                        'terms' => [
                            'place_type' => $exceptTypes
                        ]
                    ],
                    'must' => $mustTerms
                ]
            ]
        ];

        try {
            $response = $this->elasticCurl($params, true);
        } catch (\Exception $e) {
            return collect();
        }

        $response = @json_decode($response);
        $hits = @$response->hits;
        $scrollId = @$response->_scroll_id;

        if (!$hits || !$scrollId) {
            return collect($result);
        }
        $total = $response->hits->total->value;
        $result = array_merge($result, $hits->hits);

        $params = [
            'scroll_id' => $scrollId,
            'scroll' => '1m'
        ];

        while ($hits->hits && $total > count($result)) {
            try {
                $response = $this->elasticCurl($params);
                $response = @json_decode($response);
                $hits = @$response->hits;
                if ($hits) {
                    $result = array_merge($result, $hits->hits);
                }
            } catch (\Exception $e) {
                $result = array_merge($result, []);
            }
        }

        $result = collect($result)->pluck('_source.place_id');
        if (1 || self::CACHE_PUT_ENABLE) Cache::put($cacheKey, $result, self::CACHE_TIME * 24);

        return $result;
    }

    /**
     * @param $params
     * @return bool|string
     * @throws \Exception
     */
    protected function elasticCurl($params, $firstScrollReq = false)
    {
        $url = self::ES_URL;

        if ($firstScrollReq) {
            $url .= 'places/_search?size=10000&scroll=1m';
        } else {
            $url .= '_search/scroll';
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => array(
                'content-type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if ($err) {
            throw new \Exception();
        }

        return $response;
    }
}
