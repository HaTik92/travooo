<?php

namespace App\Services\Trips;

use App\Models\TripPlans\SuggestionUserLog;
use Illuminate\Support\Collection;

class SuggestionUserLogsService
{
    /**
     * @param int $suggestionId
     * @param int $userId
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function create($suggestionId, $userId)
    {
        return SuggestionUserLog::query()->create([
            'user_id' => $userId,
            'suggestion_id' => $suggestionId,
            'unread' => 1
        ]);
    }

    /**
     * @param $suggestionId
     * @param null $userId
     * @return int
     */
    public function isUnread($suggestionId, $userId = null)
    {
        if (!$userId) {
            $userId = auth()->id();
        }

        $log = SuggestionUserLog::query()->where([
            'suggestion_id' => $suggestionId,
            'user_id' => $userId
        ])->first();

        if (!$log) {
            return 0;
        }

        return $log->unread;
    }

    /**
     * @param array|Collection $suggestionsIds
     * @param null $userId
     */
    public function read($suggestionsIds, $userId = null)
    {
        if (!$userId) {
            $userId = auth()->id();
        }

        SuggestionUserLog::query()
            ->where('user_id', $userId)
            ->whereIn('suggestion_id', $suggestionsIds)
            ->update([
                'unread' => 0
            ]);
    }

    /**
     * @param int $suggestionId
     */
    public function remove($suggestionId)
    {
        SuggestionUserLog::query()
            ->where('suggestion_id', $suggestionId)
            ->delete();
    }
}