<?php

namespace App\Services\Trips;

use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlaces\TripPlacesComments;
use Illuminate\Support\Facades\Auth;

class TripPlaceCommentsService
{
    const DEFAULT_COMMENTS_PER_PAGE = 10;

    /**
     * @var TripsSuggestionsService
     */
    private $tripsSuggestionsService;

    public function __construct(TripsSuggestionsService $tripsSuggestionsService)
    {
        $this->tripsSuggestionsService = $tripsSuggestionsService;
    }

    /**
     * @param $tripPlaceId
     * @param $comment
     * @throws \Exception
     */
    public function create($tripPlaceId, $comment)
    {
        $this->validateTripPlace($tripPlaceId);

        if (!$comment) {
            throw new \Exception('Wrong comment ', 400);
        }

        TripPlacesComments::query()->create([
            'trip_place_id' => $tripPlaceId,
            'comment' => $comment,
            'user_id' => auth()->id()
        ]);
    }

    /**
     * @param $tripPlaceId
     * @param int $perPage
     * @param bool $withoutReplies
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws \Exception
     */
    public function getList($tripPlaceId, $perPage, $withoutReplies = false)
    {
        if (!$perPage) {
            $perPage = self::DEFAULT_COMMENTS_PER_PAGE;
        }

        $this->validateTripPlace($tripPlaceId);

        $query = TripPlacesComments::query()
            ->whereIn('trip_place_id', $this->tripsSuggestionsService->getAllSuggestionPackTripPlacesIds($tripPlaceId));

        if ($withoutReplies) {
            $query->where('reply_to', 0);
        }

        return $query->orderByDesc('created_at')->paginate($perPage);
    }

    public function loadMoreComments($page, $tripPlaceId)
    {
        $next = ($page - 1) * 5;

        $comments = TripPlacesComments::query()
            ->whereIn('trip_place_id', $this->tripsSuggestionsService->getAllSuggestionPackTripPlacesIds($tripPlaceId))
            ->orderBy('created_at', 'desc')
            ->skip($next)
            ->take(5)
            ->get();

        $result = '';

        if(count($comments) > 0){
            foreach ($comments as $comment) {
                $result .= $this->renderComment($comment);
            }
        }

        return $result;
    }

    public function renderComment($comment)
    {
        return view('site.trip2.partials.trip_place_comment_block', [
            'comment' => $comment,
            'authUserId' => Auth::id(),
            'tp' => $comment->tripPlace,
            'auth_picture' => check_profile_picture(auth()->user()->profile_picture)
        ]);
    }

    /**
     * @param $tripPlaceId
     * @throws \Exception
     */
    protected function validateTripPlace($tripPlaceId)
    {
        $tripPlace = TripPlaces::query()->find($tripPlaceId);

        if (!$tripPlace) {
            throw new \Exception('Trip place not found', 404);
        }

        return;
    }
}