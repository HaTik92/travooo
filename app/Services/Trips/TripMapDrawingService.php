<?php

namespace App\Services\Trips;

use App\Models\TripPlans\TripPlans;
use Illuminate\Support\Collection;

class TripMapDrawingService
{
    public const VERSION = 'v3';
    protected const BOUNDS_OFFSET = 20;
    protected const MAP_RESOLUTION_WIDTH = '1280';
    protected const MAP_RESOLUTION_HEIGHT = '1280';
    protected const MAPBOX_API_URL = 'https://api.mapbox.com/';
    protected const MAPBOX_MAP_STYLE = 'styles/v1/alex-sh/ckhxt4xlv15o819mljgd7uscn/static/';
    protected const MARKER_WIDTH = 60;

    /**
     * @var string
     */
    protected $accessToken;

    protected $width;
    protected $height;
    protected $k;
    protected $targetHeight;
    protected $targetWidth;

    private $placeholder;

    public function __construct()
    {
        $this->accessToken = config('mapbox.token');
        $this->width = self::MAP_RESOLUTION_WIDTH;
        $this->height = self::MAP_RESOLUTION_HEIGHT;

        $this->setWidthAndHeight(self::MAP_RESOLUTION_WIDTH, self::MAP_RESOLUTION_HEIGHT);

        $this->placeholder = url(PLACE_PLACEHOLDERS);
        if (!isLiveSite()) {
            $this->placeholder = 'http://travooo.com/' . PLACE_PLACEHOLDERS;
        }
    }

    public function setWidthAndHeight($width, $height)
    {
        $ratio = $width / $height;

        if ($ratio > 1) {
            $this->width = self::MAP_RESOLUTION_WIDTH;
            $this->height = (int) (self::MAP_RESOLUTION_HEIGHT / $ratio);
        } else {
            $this->width = (int) (self::MAP_RESOLUTION_WIDTH * $ratio);
            $this->height = self::MAP_RESOLUTION_HEIGHT;
        }

        //scale k by exp to increase markers and lines width on low resolution
        $x = pow($width / 100, 2);
        $this->k = ($x + 0.5)/($x + 5);

        $this->targetHeight = $height;
        $this->targetWidth = $width;
    }

    public function generateTripMapThumbnail($tripId)
    {
        $t1 = microtime(true);
        /** @var TripPlans $plan */
        $plan = TripPlans::query()->findOrFail($tripId);
        $publishedPlaces = $plan->published_places;

        if (!$publishedPlaces) {
            throw new \Exception();
        }
        $t2 = microtime(true);
        $places = $publishedPlaces->pluck('place');
        $t3 = microtime(true);
        $bounds = $this->getMinimumBounds($places);

        $xy1 = $this->getXYCoordsByLatLng($bounds[0]['lat'], $bounds[0]['lng']);
        $xy2 = $this->getXYCoordsByLatLng($bounds[1]['lat'], $bounds[1]['lng']);

        $diffX = abs($xy2[0] - $xy1[0]);
        $diffY = abs($xy1[1] - $xy2[1]);

        $targetRatio = $this->width / $this->height;

        $paddingX = ((self::MARKER_WIDTH / $this->k)/ 2 + self::BOUNDS_OFFSET) * $diffX / (self::MAP_RESOLUTION_WIDTH - 2 * ((self::MARKER_WIDTH / $this->k) / 2 + self::BOUNDS_OFFSET));
        $paddingY = ((self::MARKER_WIDTH / $this->k) / 2 + self::BOUNDS_OFFSET) * $diffY / (self::MAP_RESOLUTION_HEIGHT - 2 * ((self::MARKER_WIDTH / $this->k) / 2 + self::BOUNDS_OFFSET));

        if ($targetRatio > 1) {
            $paddingY *= $targetRatio;
        } else {
            $paddingX /= $targetRatio;
        }

        $xy1[0] = max($xy1[0] - $paddingX, 0);
        $xy2[0] = min($xy2[0] + $paddingX, 1);
        $xy2[1] = max($xy2[1] - $paddingY, 0);
        $xy1[1] = min($xy1[1] + $paddingY, 1);

        $diffX = abs($xy2[0] - $xy1[0]);
        $diffY = abs($xy1[1] - $xy2[1]);

        if ($diffX == 0 || $diffY == 0) {
            $currentRatio = 1;
        } else {
            $currentRatio = $diffX / $diffY;
        }

        if ($targetRatio > $currentRatio) {
            $addX = ($diffY * $targetRatio - $diffX) / 2;

            $newXY1 = [$xy1[0] - $addX, $xy1[1]];
            $newXY2 = [$xy2[0] + $addX, $xy2[1]];
        } else {
            $addY = ($diffX / $targetRatio - $diffY) / 2;

            $newXY1 = [$xy1[0], $xy1[1] + $addY];
            $newXY2 = [$xy2[0], $xy2[1] - $addY];
        }

        if ($newXY1[0] < 0) {
            $newXY2[0] = min($newXY2[0] - $newXY1[0], 1);
            $newXY1[0] = 0;
        }

        if ($newXY1[1] > 1) {
            $newXY2[1] = max($newXY2[1] - $newXY1[1] + 1, 0);
            $newXY1[1] = 1;
        }

        if ($newXY2[0] > 1) {
            $newXY1[0] = max($newXY1[0] - $newXY2[0] + 1, 0);
            $newXY2[0] = 1;
        }

        if ($newXY2[1] < 0) {
            $newXY1[1] = min($newXY1[1] - $newXY2[1], 1);
            $newXY2[1] = 0;
        }

        if ($newXY1 === $newXY2) {
            $newXY1[0] -= 0.01;
            $newXY2[0] += 0.01;
            $newXY1[1] += 0.01;
            $newXY2[1] -= 0.01;
        }

        $latLng1 = $this->getLatLngByXY($newXY1[0], $newXY1[1]);
        $latLng2 = $this->getLatLngByXY($newXY2[0], $newXY2[1]);

        $bounds = [
            ['lat' => $latLng1[0], 'lng' => $latLng1[1]],
            ['lat' => $latLng2[0], 'lng' => $latLng2[1]]
        ];

        $lngDiff = $bounds[1]['lng'] - $bounds[0]['lng'];
        $pixelsPerLng = $this->width / ($lngDiff);

        $scale = $this->width / ($lngDiff / 360);

        $places = $publishedPlaces->pluck('place', 'id');
        $t4 = microtime(true);
        list($clusters, $markersClusters) = $this->calculateClusters($places, $scale);
        $t5 = microtime(true);


        $beforeOffsets = null;
        $beforePlace = null;
        $placesData = [];

        $renderedClusters = [];


        $images = [];
        $mbBg = $this->getMapboxBackground($bounds);
        $t6 = microtime(true);

        $images[] = $this->placeholder;
        $images[] = $mbBg;

        foreach ($places as $id => $place) {
            $img = @$place->media[0]->url;
            $thumbSrc = S3_BASE_URL . 'th180/' . $img;

            if (!$img) {
                $thumbSrc = $this->placeholder;
            }

            $images[] = $thumbSrc;
        }

        $images = $this->getMultipleImages($images);
        $t7 = microtime(true);
        $placeholder = array_shift($images);
        $bg = $this->createImage(array_shift($images), $placeholder);
        $canvas = imagecreatetruecolor($this->width, $this->height);
        imagecopy($canvas, $bg, 0, 0, 0, 0, $this->width, $this->height);
        $t8 = microtime(true);
        $t = 0;
        foreach ($places as $id => $place) {
            $t00 = microtime(true);
            $addPlace = true;

            $img = $this->createImage(array_shift($images), $placeholder);
            $placeImg = $this->imageCreateCorners($img);

            $lat = $place->lat;
            $lng = $place->lng;

            if (isset($markersClusters[$id]) && $markersClusters[$id] !== null) {
                if (isset($renderedClusters[$markersClusters[$id]])) {
                    $addPlace = false;
                }

                $renderedClusters[$markersClusters[$id]] = true;

                $avgLat = 0;
                $avgLng = 0;

                foreach ($clusters[$markersClusters[$id]] as $placeIdInCluster) {
                    $avgLat += $places[$placeIdInCluster]->lat;
                    $avgLng += $places[$placeIdInCluster]->lng;
                }

                $lat = $avgLat / count($clusters[$markersClusters[$id]]);
                $lng = $avgLng / count($clusters[$markersClusters[$id]]);
            }

            if ($diffX > $diffY) {
                $offsetY = ($this->getXYCoordsByLatLng($lat, $lng)[1] - $this->getXYCoordsByLatLng($bounds[1]['lat'], $bounds[1]['lng'])[1]) * $scale - (self::MARKER_WIDTH / $this->k) / 2;
                $offsetX = ($lng - $bounds[0]['lng']) * $pixelsPerLng - (self::MARKER_WIDTH / $this->k) / 2;
            } else {
                $offsetY = ($this->getXYCoordsByLatLng($lat, $lng)[1] - $this->getXYCoordsByLatLng($bounds[1]['lat'], $bounds[1]['lng'])[1]) * $scale - (self::MARKER_WIDTH / $this->k) / 2;
                $offsetX = ($lng - $bounds[0]['lng']) * $pixelsPerLng - (self::MARKER_WIDTH / $this->k) / 2;
            }

            if ($beforeOffsets) {
                $line = 'dotted';

                if ($beforePlace->cities_id === $place->cities_id) {
                    $line = 'solid';
                } elseif ($beforePlace->countries_id === $place->countries_id) {
                    $line = 'dashed';
                }

                $this->createLine(
                    $canvas,
                    $beforeOffsets[0] + (self::MARKER_WIDTH / $this->k) / 2,
                    $beforeOffsets[1] + (self::MARKER_WIDTH / $this->k) / 2,
                    $offsetX + (self::MARKER_WIDTH / $this->k) / 2,
                    $offsetY + (self::MARKER_WIDTH / $this->k) / 2,
                    $line
                );
            }

            $beforeOffsets = [$offsetX, $offsetY];
            $beforePlace = $place;

            if ($addPlace) {
                $placesData[$id] = [
                    'offsets' => $beforeOffsets,
                    'img' => $placeImg
                ];
            }
            $t += (microtime(true) - $t00);
        }
        $t9 = microtime(true);
        foreach ($placesData as $id => $placeData) {
            imagecopy($canvas, $placeData['img'], $placeData['offsets'][0], $placeData['offsets'][1], 0, 0, self::MARKER_WIDTH / $this->k, self::MARKER_WIDTH / $this->k);

            if (isset($markersClusters[$id]) && $markersClusters[$id] !== null) {
                $count = count($clusters[$markersClusters[$id]]);

                $this->createCountImage($canvas, $count, $placeData['offsets']);
            }
        }
        $t10 = microtime(true);

        $canvas = \Intervention\Image\ImageManagerStatic::make($canvas)->orientate()->resize($this->targetWidth, $this->targetHeight, function ($constraint) {
            $constraint->aspectRatio();
        })->encode(null, 100);

        return $canvas->getCore();
    }

    protected function createCountImage($canvas, $count, $offsets)
    {
        $white = imagecolorallocate($canvas, 255, 255, 255);
        $pathToFont = public_path('/assets2/fonts/CircularStd-Medium.ttf');

        $x = $offsets[0] + (self::MARKER_WIDTH / $this->k) - (self::MARKER_WIDTH / $this->k) / 6;
        $y = $offsets[1] + (self::MARKER_WIDTH / $this->k) / 6;

        $color = imagecolorallocate($canvas, 0, 0, 0);
        imagefilledellipse($canvas, $x, $y, (self::MARKER_WIDTH / $this->k) / 2, (self::MARKER_WIDTH / $this->k) / 2, $color);
        $color = imagecolorallocate($canvas, 255, 255, 255);
        imageellipse($canvas, $x, $y, (self::MARKER_WIDTH / $this->k) / 2 + 2, (self::MARKER_WIDTH / $this->k) / 2 + 2, $color);
        imageellipse($canvas, $x, $y, (self::MARKER_WIDTH / $this->k) / 2 + 4, (self::MARKER_WIDTH / $this->k) / 2 + 4, $color);

        $size = (self::MARKER_WIDTH / $this->k) / 3;

        imagettftext(
            $canvas,
            $size - 3,
            0,
            $x - strlen($count) * $size / 3 + 1,
            $y + $size / 2 - 1,
            $white,
            $pathToFont,
            $count
        );
    }

    protected function getXYCoordsByLatLng(float $lat, float $lng, float $scale = 0): array
    {
        $k = 1; //$this->width / $this->height;

        $x = $k * (180 + $lng) / 360;
        $y = $k * (180 - (180 / M_PI * log(tan(M_PI / 4 + $lat * M_PI / 360)))) / 360;

        return [$x, $y];
    }

    protected function getLatLngByXY(float $x, float $y): array
    {
        $k = 1; //$this->width / $this->height;
        $lng = $x * 360 / $k - 180;
        $lat = 360 / M_PI * atan(exp((180 - $y * 360) * M_PI / (180 * $k))) - 90;

        return [$lat, $lng];
    }

    protected function createImage($placeImg, $placeholder = null)
    {
        try {
            if (is_string($placeImg)) {
                return imagecreatefromstring($placeImg);
            }
            switch (strtolower(pathinfo($placeImg, PATHINFO_EXTENSION))) {
                case 'jpeg':
                case 'jpg':
                    return imagecreatefromjpeg($placeImg);
                    break;
                case 'png':
                    return imagecreatefrompng($placeImg);
                    break;
                case 'gif':
                    return imagecreatefromgif($placeImg);
                    break;
                default:
                    return imagecreatefrompng($this->placeholder);
                    break;
            }
        } catch (\Exception $e) {
            if ($placeholder) {
                return imagecreatefromstring($placeholder);
            }

            return imagecreatefrompng($this->placeholder);
        }
    }

    protected function createLine($canvas, $x1, $y1, $x2, $y2, $type = 'solid')
    {
        $color = imagecolorallocate($canvas, 64, 128, 255);

        switch ($type) {
            case 'dotted':
                imagesetthickness($canvas, 3 / $this->k);
                $this->drawDashedLine($canvas, $x1, $y1, $x2, $y2, 10, 10, $color);
                break;
            case 'dashed':
                imagesetthickness($canvas, 3 / $this->k);
                $this->drawDashedLine($canvas, $x1, $y1, $x2, $y2, 20, 20, $color);
                break;
            case 'solid':
            default:
                imagesetthickness($canvas, 4 / $this->k);
                imageline($canvas, $x1, $y1, $x2, $y2, $color);
                break;
        }
    }

    protected function drawDashedLine($canvas, $x1, $y1, $x2, $y2, $dash_length, $dash_space, $color)
    {
        if ($dash_length) {
            $dashes = array_fill(0, $dash_length, $color);
        } else {
            $dashes = array();
        }
        if ($dash_space) {
            $spaces = array_fill(0, $dash_space, IMG_COLOR_TRANSPARENT);
        } else {
            $spaces = array();
        }
        $style = array_merge($dashes, $spaces);
        imagesetstyle($canvas, $style);
        imageline($canvas, $x1, $y1, $x2, $y2, IMG_COLOR_STYLED);
    }

    protected function calculateClusters($coords, $scale)
    {
        $clusters = [];
        $markersClusters = [];

        foreach ($coords as $id1 => $place1) {
            foreach ($coords as $id2 => $place2) {
                if ($id1 === $id2 || (isset($markersClusters[$id1]) && isset($markersClusters[$id2])  && $markersClusters[$id1] !== null && $markersClusters[$id1] === $markersClusters[$id2])) {
                    continue;
                }

                $xy1 = $this->getXYCoordsByLatLng($place1->lat, $place1->lng);
                $xy2 = $this->getXYCoordsByLatLng($place2->lat, $place2->lng);

                $length = sqrt(pow($xy2[0] - $xy1[0], 2) + pow($xy2[1] - $xy1[1], 2)) * $scale;

                if ($length <= (self::MARKER_WIDTH / $this->k)) {
                    if ((!isset($markersClusters[$id1]) || $markersClusters[$id1] === null) && (!isset($markersClusters[$id2]) || $markersClusters[$id2] === null)) {
                        $clusters[] = [$id1, $id2];

                        $markersClusters[$id1] = count($clusters) - 1;
                        $markersClusters[$id2] = count($clusters) - 1;
                    } elseif (isset($markersClusters[$id1]) && $markersClusters[$id1] !== null) {
                        if (!isset($markersClusters[$id2]) || $markersClusters[$id2] === null) {
                            if (!isset($clusters[$markersClusters[$id1]])) {
                                $clusters[$markersClusters[$id1]] = [];
                            }
                            $clusters[$markersClusters[$id1]][] = $id2;
                            $markersClusters[$id2] = $markersClusters[$id1];
                        } else {
                            $clusterToRemove = $markersClusters[$id2];
                            foreach ($clusters[$clusterToRemove] as $markerToRemove) {
                                $markersClusters[$markerToRemove] = $markersClusters[$id1];

                                if (!isset($clusters[$markersClusters[$id1]])) {
                                    $clusters[$markersClusters[$id1]] = [];
                                }

                                $clusters[$markersClusters[$id1]][] = $markerToRemove;
                            }
                            $clusters[$clusterToRemove] = null;
                        }
                    } else {
                        if (!isset($clusters[$markersClusters[$id2]])) {
                            $clusters[$markersClusters[$id2]] = [];
                        }

                        $clusters[$markersClusters[$id2]][] = $id1;
                        $markersClusters[$id1] = $markersClusters[$id2];
                    }
                }
            }
        }

        return [$clusters, $markersClusters];
    }

    protected function getMinimumBounds(Collection $places): array
    {
        $bounds = [
            ['lng' => 99999, 'lat' => 99999],
            ['lng' => -99999, 'lat' => -99999]
        ];

        foreach ($places as $place) {
            if ($bounds[0]['lng'] > $place->lng) {
                $bounds[0]['lng'] = $place->lng;
            }

            if ($bounds[0]['lat'] > $place->lat) {
                $bounds[0]['lat'] = $place->lat;
            }

            if ($bounds[1]['lat'] < $place->lat) {
                $bounds[1]['lat'] = $place->lat;
            }

            if ($bounds[1]['lng'] < $place->lng) {
                $bounds[1]['lng'] = $place->lng;
            }
        }

        return $bounds;
    }

    protected function getMapboxBackground(array $bounds): string
    {
        return self::MAPBOX_API_URL
            . self::MAPBOX_MAP_STYLE
            . $this->boundsToString($bounds)
            . '/' . $this->width
            . 'x' . $this->height
            . '?padding=0'
            . '&access_token='
            . $this->accessToken;
    }

    protected function boundsToString(array $bounds): string
    {
        return '[' . number_format($bounds[0]['lng'], 4) . ',' . number_format($bounds[0]['lat'], 4) . ',' . number_format($bounds[1]['lng'], 4) . ',' . number_format($bounds[1]['lat'], 4) . ']';
    }

    protected function imageCreateCorners($src, $radius = self::MARKER_WIDTH / 2)
    {
        $w = self::MARKER_WIDTH / $this->k;
        $h = self::MARKER_WIDTH / $this->k;
        $radius /= $this->k;

        $q = 8; # change this if you want
        $radius *= $q;

        # find unique color
        do {
            $r = rand(0, 255);
            $g = rand(0, 255);
            $b = rand(0, 255);
        } while (imagecolorexact($src, $r, $g, $b) < 0);

        $nw = $w * $q;
        $nh = $h * $q;

        $img = imagecreatetruecolor($nw, $nh);
        $alphacolor = imagecolorallocatealpha($img, $r, $g, $b, 127);
        $borderColor = imagecolorallocate($img, 64, 128, 255);
        imagealphablending($img, false);
        imagesavealpha($img, true);
        imagefill($img, 0, 0, $alphacolor);

        imagecopyresampled($img, $src, 0, 0, 0, 0, $nw, $nh, imagesx($src), imagesy($src));

        imageellipse($img, $radius, $radius, $radius * 2, $radius * 2, $borderColor);

        imagefilltoborder($img, 0, 0, $borderColor, $alphacolor);
        imagefilltoborder($img, $nw, 0, $borderColor, $alphacolor);
        imagefilltoborder($img, 0, $nh, $borderColor, $alphacolor);
        imagefilltoborder($img, $nw, $nh, $borderColor, $alphacolor);
        imagecolortransparent($img, $alphacolor);

        # resize image down
        $dest = imagecreatetruecolor($w, $h);
        imagealphablending($dest, false);
        imagesavealpha($dest, true);
        imagefilledrectangle($dest, 0, $h, $w, $h, $alphacolor);
        imagecopyresampled($dest, $img, 0, 0, 0, 0, $w, $h, $nw, $nh);

        imageellipse($dest, $w / 2, $h / 2, $w, $h, $borderColor);
        imageellipse($dest, $w / 2, $h / 2, $w - 2, $h - 2, $borderColor);

        # output image
        $res = $dest;
        //imagedestroy($src);
        imagedestroy($img);
        //        header('Content-Type: image/jpeg');
        //        imagejpeg($res);
        //        die();
        return $res;
    }

    protected function getMultipleImages($nodes)
    {
        $node_count = count($nodes);

        $curl_arr = array();
        $master = curl_multi_init();

        for ($i = 0; $i < $node_count; $i++) {
            $url = $nodes[$i];
            $curl_arr[$i] = curl_init($url);
            curl_setopt($curl_arr[$i], CURLOPT_RETURNTRANSFER, true);
            curl_multi_add_handle($master, $curl_arr[$i]);
        }

        do {
            curl_multi_exec($master, $running);
        } while ($running > 0);

        for ($i = 0; $i < $node_count; $i++) {
            $results[] = curl_multi_getcontent($curl_arr[$i]);
        }

        curl_multi_close($master);

        return $results;
    }
}
