<?php

namespace App\Services\Trips;

use App\Events\Api\PlanLogs\PlanLogAddedApiEvent;
use App\Events\PlanLogs\PlanLogAddedEvent;
use App\Events\PlanLogs\PlanLogReadEvent;
use App\Models\ActivityLog\ActivityLog;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\PlanActivityLog;
use App\Models\TripPlans\PlanActivityLogUser;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsContributionRequests;
use App\Models\TripPlans\TripsSuggestion;
use App\Models\User\User;
use Illuminate\Support\Arr;

class PlanActivityLogService
{
    public $apiFlag = false;

    public function setApiFlag(bool $apiFlag)
    {
        $this->apiFlag = $apiFlag;
    }


    /**
     * @param int $type
     * @param int $planId
     * @param int $entityId
     * @param int $userId
     * @param array $meta
     * @param null|int $suggestionId
     * @return PlanActivityLog|\Illuminate\Database\Eloquent\Model
     */
    public function createPlanActivityLog($type, $planId, $entityId, $userId, $meta = [], $suggestionId = null)
    {
        $log = PlanActivityLog::create([
            'type' => $type,
            'trips_id' => $planId,
            'entity_id' => $entityId,
            'user_id' => $userId,
            'suggestion_id' => $suggestionId,
            'meta' => $meta
        ]);

        if (ActivityLog::query()->where([
            'type' => 'Trip',
            'action' => 'create',
            'variable' => $planId
        ])->exists()) {
            log_user_activity('Trip', 'plan_updated', $planId, TripPlans::query()->find($planId)->privacy);
        } else {
            log_user_activity('Trip', 'create', $planId, TripPlans::query()->find($planId)->privacy);
        }

        $this->createForInvited($planId, $log);

        return $log;
    }

    /**
     * @param int $placeId
     * @return array
     */
    public function getPlaceLogUpdatedAttributes($placeId)
    {
        $userLogs = PlanActivityLogUser::with('log')
            ->where([
                'user_id' => auth()->id(),
                'unread' => 1
            ])->whereHas('log', function ($query) use ($placeId) {
                $query->where('entity_id', $placeId)
                    ->whereIn('type', [PlanActivityLog::TYPE_EDITED_PLACE, PlanActivityLog::TYPE_ADDED_PLACE]);
            })->get();

        if (!$userLogs->count()) {
            return [];
        }

        foreach ($userLogs as $userLog) {
            $userLog->unread = 0;
            $userLog->save();
        }

        $meta = $userLogs->first()->log->meta;

        if (!$meta || !isset($meta['updated'])) {
            return [];
        }

        return $meta['updated'];
    }

    /**
     * @param int $planId
     * @return int
     */
    public function getPlanActivityLogUnreadCount($planId)
    {
        return PlanActivityLogUser::where([
            'user_id' => auth()->id(),
            'unread' => 1
        ])->whereHas('log', function ($query) use ($planId) {
            return $query->where('trips_id', $planId)->where('user_id', '!=', auth()->id());
        })->count();
    }

    /**
     * @param int $planId
     * @return array
     */
    public function getPlacesPlanActivityLogUnread($planId)
    {
        return PlanActivityLogUser::with('log')
            ->where([
                'user_id' => auth()->id(),
                'unread' => 1
            ])->whereHas('log', function ($query) use ($planId) {
                $query->where('trips_id', $planId)
                    ->whereIn('type', [PlanActivityLog::TYPE_EDITED_PLACE, PlanActivityLog::TYPE_ADDED_PLACE]);
            })->get()->pluck('log.entity_id')->toArray();
    }

    /**
     * @param int $planId
     * @return PlanActivityLog[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getPlanActivityLogList($planId)
    {
        $logs = PlanActivityLog::where('trips_id', $planId)->orderByDesc('created_at')->get();

        $result = [];

        foreach ($logs as &$log) {
            $date = diffForHumans($log->created_at);
            $result['logs'][$date][] = $this->prepareLogForResponse($log);
        }

        try {
            broadcast(new PlanLogReadEvent(auth()->id()));
        } catch (\Throwable $e) {
        }

        return $result;
    }

    /**
     * @param int $tripId
     * @return PlanActivityLog[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getFlatPlanActivityLogList($tripId)
    {
        $result = [];
        $logs = PlanActivityLog::where('trips_id', $tripId)->orderByDesc('created_at')->get();
        foreach ($logs as &$log) {
            $log->log_date = diffForHumans($log->created_at);
            $result[] = $this->prepareLogForResponse($log);
        }
        return $result;
    }

    /**
     * @param TripPlaces $updatedPlace
     * @param TripPlaces|null $currentPlace
     * @return array
     */
    public function prepareLogMeta($updatedPlace, $currentPlace = null)
    {
        $meta = [];

        foreach ($updatedPlace->getAttributes() as $attribute => $value) {
            if ($currentPlace && $updatedPlace->$attribute === $currentPlace->$attribute) {
                continue;
            }

            $meta['updated'][] = $attribute;
        }

        return $meta;
    }

    /**
     *
     * @param $log
     * @return mixed|string|string[]
     */
    protected function getMappedType($log)
    {
        $mappedType = PlanActivityLog::TYPE_MAPPING[$log->type];

        switch ($log->type) {
            case PlanActivityLog::TYPE_CHANGED_PRIVACY_SETTINGS:
                $replace = '';

                if (isset($log->meta['privacy'])) {
                    $replace = 'to ' . TripsService::PRIVACY_MAPPING[$log->meta['privacy']];
                }

                $mappedType = str_replace('%privacy%', $replace, $mappedType);

                break;
            case PlanActivityLog::TYPE_APPROVED_SUGGESTION:
            case PlanActivityLog::TYPE_DISAPPROVED_SUGGESTION:
                $suggestion = TripsSuggestion::find($log->entity_id); //TripsContributionRequests
                if (!$suggestion) {
                    break;
                }

                if (strpos($mappedType, '%username%') !== false) {
                    $mappedType = str_replace('%username%', $this->getUserLink($suggestion->user), $mappedType);
                }
                break;
            case PlanActivityLog::TYPE_REMOVED_FROM_PLAN:
            case PlanActivityLog::TYPE_LEFT_THE_PLAN:
            case PlanActivityLog::TYPE_REJECT_INVITATION:
            case PlanActivityLog::TYPE_JOINED_THE_PLAN:
            case PlanActivityLog::TYPE_CHANGED_ROLE:
            case PlanActivityLog::TYPE_INVITED_TO_PLAN:
                $usernameReplace = '';
                $roleReplace = '';

                if (strpos($mappedType, '%username%') !== false && isset($log->meta['user_id'])) {
                    $user = User::query()->find($log->meta['user_id']);
                    if ($user) {
                        $usernameReplace = $this->getUserLink($user);
                    }
                }

                if (strpos($mappedType, '%role%') !== false && isset($log->meta['role'])) {
                    $roleReplace = $log->meta['role'];
                }

                $mappedType = str_replace('%username%', $usernameReplace, $mappedType);
                $mappedType = str_replace('%role%', $roleReplace, $mappedType);

                break;
            case PlanActivityLog::TYPE_PLAN_TYPE_CHANGED:
                if (strpos($mappedType, '%X%') !== false && isset($log->meta['day'])) {
                    $mappedType = str_replace('%X%', $log->meta['day'], $mappedType);
                }
                break;
            default:
                break;
        }

        return $mappedType;
    }

    /**
     *
     * @param $log
     * @return mixed|string|string[]
     */
    protected function getMappedTypeApi($log)
    {
        $mappedType = PlanActivityLog::TYPE_MAPPING[$log->type];
        $returnArr['format'] = $mappedType;


        switch ($log->type) {
            case PlanActivityLog::TYPE_CHANGED_PRIVACY_SETTINGS:
                if (isset($log->meta['privacy'])) {
                    $returnArr['privacy'] = TripsService::PRIVACY_MAPPING[$log->meta['privacy']];
                }
                break;
            case PlanActivityLog::TYPE_APPROVED_SUGGESTION:
            case PlanActivityLog::TYPE_DISAPPROVED_SUGGESTION:
                $suggestion = TripsSuggestion::find($log->entity_id);
                if (!$suggestion) {
                    break;
                }

                if (strpos($mappedType, '%username%') !== false) {
                    $returnArr['user'] = $suggestion->user;
                }


                break;
            case PlanActivityLog::TYPE_REMOVED_FROM_PLAN:
            case PlanActivityLog::TYPE_LEFT_THE_PLAN:
            case PlanActivityLog::TYPE_REJECT_INVITATION:
            case PlanActivityLog::TYPE_JOINED_THE_PLAN:
            case PlanActivityLog::TYPE_CHANGED_ROLE:
            case PlanActivityLog::TYPE_INVITED_TO_PLAN:
                if (strpos($mappedType, '%username%') !== false && isset($log->meta['user_id'])) {
                    $user = User::query()->find($log->meta['user_id']);
                    if ($user) {
                        $returnArr['user'] = $user;
                    }
                }

                if (strpos($mappedType, '%role%') !== false && isset($log->meta['role'])) {
                    $returnArr['role'] =  $log->meta['role'];
                }
                break;
            case PlanActivityLog::TYPE_PLAN_TYPE_CHANGED:
                if (strpos($mappedType, '%X%') !== false && isset($log->meta['day'])) {
                    $returnArr['day'] =  $log->meta['day'];
                }
                break;
            default:
                break;
        }

        return $returnArr;
    }

    /**
     * @param $log
     * @return TripPlaces|TripPlaces[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    protected function getTripPlace($log)
    {
        switch ($log->type) {
            case PlanActivityLog::TYPE_APPROVED_SUGGESTION:
            case PlanActivityLog::TYPE_DISAPPROVED_SUGGESTION:
                $suggestion = TripsSuggestion::find($log->entity_id);
                if (!$suggestion) {
                    $tripPlace = null;
                    break;
                }

                $tripPlace = TripPlaces::find($suggestion->suggested_trips_places_id);
                break;
            case PlanActivityLog::TYPE_CHANGED_ROLE:
            case PlanActivityLog::TYPE_INVITED_TO_PLAN:
                return null;
                break;
            default:
                $tripPlace = TripPlaces::find($log->entity_id);
                break;
        }

        return $tripPlace;
    }

    /**
     * @param $user
     * @return string
     */
    protected function getUserLink($user)
    {
        return '<a href="/profile/' . $user->id . '">' . $user->name . '</a>';
    }

    /**
     * @param int $planId
     * @param PlanActivityLog $log
     */
    protected function createForInvited($planId, $log)
    {
        /** @var TripInvitationsService $tripInvitationsService */
        $tripInvitationsService = app(TripInvitationsService::class);

        $invitedPeople = $tripInvitationsService->getInvitedPeopleIds($planId);

        $plan = TripPlans::find($planId);

        $invitedPeople[$plan->users_id] = 'admin';

        foreach ($invitedPeople as $invitedPersonId => $role) {
            $userLog = new PlanActivityLogUser();
            $userLog->user_id = $invitedPersonId;
            $userLog->log_id = $log->getKey();
            $userLog->save();

            if ($invitedPersonId === auth()->id()) {
                //continue;
            }

            $hardRefresh = false;

            switch ($log->type) {
                case PlanActivityLog::TYPE_CHANGED_ROLE:
                    $hardRefresh = $tripInvitationsService->isUserInvitedRequest($log->entity_id, $invitedPersonId);
                    break;
                case PlanActivityLog::TYPE_SUGGESTED_EDIT_PLACE:
                case PlanActivityLog::TYPE_SUGGESTED_NEW_PLACE:
                case PlanActivityLog::TYPE_SUGGESTED_REMOVAL_PLACE:
                    if ($invitedPersonId !== auth()->id() && $role !== TripInvitationsService::ROLE_ADMIN) {
                        continue 2;
                    }
                    break;
                default:
                    break;
            }
            $tempLog = $this->prepareLogForResponse($log);
            try {
                broadcast(new PlanLogAddedEvent($invitedPersonId, $tempLog, $hardRefresh));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new PlanLogAddedApiEvent($invitedPersonId, $tempLog, $hardRefresh));
            } catch (\Throwable $e) {
            }
        }

        switch ($log->type) {
            case PlanActivityLog::TYPE_LEFT_THE_PLAN:
            case PlanActivityLog::TYPE_REJECT_INVITATION:
            case PlanActivityLog::TYPE_REMOVED_FROM_PLAN:
                $request = $tripInvitationsService->getInvitedRequest($log->entity_id, true);
                if (!$request) {
                    break;
                }

                $hardRefresh = true;
                $tempLog = $this->prepareLogForResponse($log);
                try {
                    broadcast(new PlanLogAddedEvent($request->users_id, $tempLog, $hardRefresh));
                } catch (\Throwable $e) {
                }
                try {
                    broadcast(new PlanLogAddedApiEvent($request->users_id, $tempLog, $hardRefresh));
                } catch (\Throwable $e) {
                }
                break;
            default:
                break;
        }
    }

    /**
     * @param PlanActivityLog $log
     * @return PlanActivityLog
     */
    public function prepareLogForResponse($log)
    {
        if ($this->apiFlag) {
            $log->mapped_type = $this->getMappedTypeApi($log);
        } else {
            $log->mapped_type = $this->getMappedType($log);
        }

        $log->user = null;
        $log->entity = null;

        if ($log->user_id) {
            $log->user = User::find($log->user_id);

            /** @var TripInvitationsService $tripInvitationsService */
            $tripInvitationsService = app(TripInvitationsService::class);
            $log->user->role = $tripInvitationsService->getUserRole($log->trips_id, $log->user_id);
        }

        $tripPlace = $this->getTripPlace($log);

        if ($log->entity_id && $tripPlace && $tripPlace->place) {
            $log->entity = [
                'id' => $tripPlace->id,
                'place_id' => $tripPlace->place->id,
                'name' => $tripPlace->place->transsingle->title,
                'city_name' => $tripPlace->city->transsingle->title,
                'country_name' => $tripPlace->country->transsingle->title,
                'place_image' => check_place_photo($tripPlace->place),
                'city_image' => $this->getLocationMedia($tripPlace->city),
                'country_image' => $this->getLocationMedia($tripPlace->country),
            ];
        } else {
            $plan = TripPlans::find($log->trips_id);

            /** @var TripsSuggestionsService $tripsSuggestionsService */
            $tripsSuggestionsService = app(TripsSuggestionsService::class);

            $planSuggestions = $tripsSuggestionsService->getEditPlanSuggestions($log->trips_id);

            foreach ($planSuggestions as $planSuggestion) {
                $meta = $planSuggestion->meta;
                $plan->privacy = Arr::get($meta, 'privacy');
                $plan->description = Arr::get($meta, 'description');
                $plan->title = Arr::get($meta, 'name');
                $plan->cover = Arr::get($meta, 'cover', $plan->cover);
            }

            $log->entity = [
                'id' => $log->trips_id,
                'name' => $plan->title
            ];
        }

        $userLog = PlanActivityLogUser::where([
            'user_id' => auth()->id(),
            'log_id' => $log->getKey()
        ])->first();

        $log->unread = 0;

        if ($userLog) {
            $log->unread = $userLog->unread;

            $userLog->unread = 0;
            $userLog->save();
        }

        return $log;
    }

    function getLocationMedia($obj)
    {
        if (isset($obj->getMedias[0]->url)) {
            if ($obj->getMedias[0]->thumbs_done == 1) {
                return "https://s3.amazonaws.com/travooo-images2/th1100/" . $obj->getMedias[0]->url;
            } else {
                return "https://s3.amazonaws.com/travooo-images2/" . $obj->getMedias[0]->url;
            }
        } else {
            return asset('assets2/image/placeholders/pattern.png');
        }
    }
}
