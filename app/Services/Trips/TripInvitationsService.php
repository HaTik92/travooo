<?php

namespace App\Services\Trips;

use App\DataObjects\Trips\TripLocations;
use App\Models\ActivityLog\ActivityLog;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsTravelStyles\ExpertsTravelStyles;
use App\Models\Notifications\Notifications;
use App\Models\TripPlans\PlanActivityLog;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsContributionRequests;
use App\Models\TripPlans\TripsSuggestion;
use App\Models\User\User;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Checkins\CheckinsService;
use App\Services\PlaceChat\PlaceChatsService;
use App\Services\Ranking\RankingService;
use App\Services\Visits\VisitsService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TripInvitationsService
{
    const MAX_INVITATION_COUNT = 3;
    const REJECT_INVITATION_TIMEOUT = 600;

    const STATUS_PENDING = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_REJECTED = 2;

    const ROLE_ADMIN = 'admin';
    const ROLE_EDITOR = 'editor';
    const ROLE_VIEWER = 'viewer';
    const ROLE_TRAVEL_MATE = 'travel-mate';

    const AVAILABLE_ROLES = [
        self::ROLE_ADMIN,
        self::ROLE_EDITOR,
        self::ROLE_VIEWER,
        self::ROLE_TRAVEL_MATE
    ];

    const ROLES_MAPPER = [
        'editor' => 'Editor',
        'travel-mate' => 'Travel Mate',
        'viewer' => 'Viewer',
        'admin' => 'Admin'
    ];

    const TABS_MAPPER = [
        'experts' => 'Expert',
        'locals' => 'Local',
        'friends' => 'Friend',
        'followers' => 'Follower'
    ];

    /**
     * @var User
     */
    protected $authUser;

    /**
     * @var TripsService
     */
    protected $tripsService;

    /**
     * @var PlanActivityLogService
     */
    protected $planActivityLogService;

    /**
     * TripInvitationsService constructor.
     * @param TripsService $tripsService
     * @param PlanActivityLogService $planActivityLogService
     */
    public function __construct(TripsService $tripsService, PlanActivityLogService $planActivityLogService)
    {
        $this->authUser = Auth::user();
        // dd('TripInvitationsService', $this->authUser);
        $this->tripsService = $tripsService;
        $this->planActivityLogService = $planActivityLogService;
    }

    /**
     * @param int $planId
     * @param int $userId
     * @param string $notes
     * @param string $role
     * @param null $without_plan_id
     * @return bool
     * @throws \Exception
     */
    public function invite($planId, $userId, $notes, $role, $without_plan_id = NULL)
    {
        $invite = TripsContributionRequests::where([
            'plans_id' => $planId,
            'users_id' => $userId
        ])->first();

        if (!$invite) {
            $invite = new TripsContributionRequests();
            $invite->plans_id = $planId;
            $invite->authors_id = Auth::user()->id;
            $invite->users_id = $userId;
        }

        $invite->count++;
        $invite->status = self::STATUS_PENDING;

        if ($role === 'travel-mate') {
            $role = 'viewer';
            $notes = 'travel-mate';
        }

        $invite->role = strtolower($role);
        $invite->notes = $notes;
        $invite->w_plans_id = $without_plan_id;

        $updatedAt = $invite->updated_at ? $invite->updated_at : Carbon::create(1970);

        if ($invite->count <= self::MAX_INVITATION_COUNT && Carbon::now()->diffInSeconds($updatedAt) >= self::REJECT_INVITATION_TIMEOUT && $invite->save()) {
            log_user_activity('Trip', 'invite_friends', $planId);

            Notifications::where('users_id', $userId)
                ->where('data_id', $invite->id)
                ->where('type', 'plan_invitation_received')
                ->delete();

            // send notification to user
            $not = new Notifications;
            $not->authors_id = auth()->id();
            $not->users_id = $userId;
            $not->type = 'plan_invitation_received';
            $not->data_id = $invite->id;
            $not->notes = $notes;
            $not->read = 0;
            $not->save();
        } else {
            return false;
        }

        $logMeta = [
            'user_id' => $userId,
            'role' => self::ROLES_MAPPER[$role]
        ];

        $this->planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_INVITED_TO_PLAN, $planId, $invite->id, auth()->id(), $logMeta);

        return true;
    }

    /**
     * @param int $planId
     * @return array
     */
    public function getInvitedPeopleIds($planId)
    {
        return TripsContributionRequests::query()
            ->where('plans_id', $planId)
            ->where('status', self::STATUS_ACCEPTED)
            ->pluck('role', 'users_id')
            ->toArray();
    }

    /**
     * @param int $requestId
     * @return array
     */
    public function getInvitedRequest($requestId, $withTrashed = false)
    {
        $query = TripsContributionRequests::query();

        if ($withTrashed) {
            $query = $query->withTrashed();
        }

        return $query->find($requestId);
    }

    /**
     * @param int $requestId
     * @param int $userId
     * @return bool
     */
    public function isUserInvitedRequest($requestId, $userId)
    {
        $request = $this->getInvitedRequest($requestId);

        return $request && $request->users_id === $userId;
    }

    /**
     * @param int $planId
     * @param bool $includeOwner
     * @return array
     */
    public function getInvitedPeople($planId, $includeOwner = false, $withCount = false)
    {
        $count = 0;
        $invitedRequests = TripsContributionRequests::query()->select('notes', 'role', 'users_id', 'status', 'count', 'updated_at')->where('plans_id', $planId)->get();

        $result = [
            'admin' => [],
            'editor' => [],
            'viewer' => []
        ];

        $usersId = $invitedRequests->pluck('users_id');

        if ($includeOwner) {
            $usersId[] = @TripPlans::query()->find($planId)->users_id;
        }

        $users = User::query()->whereIn('id', $usersId)->get();

        foreach ($users as $user) {
            $request = $invitedRequests->where('users_id', $user->id)->first();

            if ($request) {
                switch ($request->status) {
                    case self::STATUS_PENDING:
                        $role = 'pending';
                        break;
                    case self::STATUS_REJECTED:
                        continue 2;
                        break;
                    default:
                        $role = strtolower($request->role);
                }

                if ($request->notes === 'travel-mate') {
                    $role = 'travel-mate';
                }

                $maxInvitationsCountAchieved = $request->count >= self::MAX_INVITATION_COUNT;
                $resendTimeout = self::REJECT_INVITATION_TIMEOUT - Carbon::now()->diffInSeconds($request->updated_at);
            } else {
                $role = 'admin';
                $maxInvitationsCountAchieved = false;
                $resendTimeout = 0;
            }


            $resendDisabled = $maxInvitationsCountAchieved || $resendTimeout > 0;

            if ($resendDisabled) {
                if ($maxInvitationsCountAchieved) {
                    $resendTimeout = 99999999;
                }
            } else {
                $resendTimeout = 0;
            }

            $result[$role][] = [
                'id' => $user->id,
                'img' => check_profile_picture($user->profile_picture),
                'name' => $user->name,
                'desc' => $request ? self::ROLES_MAPPER[strtolower($request->role)] : self::ROLES_MAPPER['admin'],
                'role' => $request ? strtolower($request->role) : 'admin',
                'resend_disabled' => $resendDisabled,
                'resend_timeout' => $resendTimeout,
                'is_owner' => $request === null
            ];

            $count++;
        }

        if ($withCount) {
            return [
                'users' => $result,
                'count' => $count
            ];
        }

        return $result;
    }

    /**
     * @param int $planId
     * @return array
     */
    public function getAnotherPlanPeople($planId)
    {
        $planPeople = [];

        $invitedPeople = $this->getInvitedPeople($planId, true);

        foreach ($invitedPeople as $role => $people) {
            foreach ($people as $person) {
                if ($role === 'pending') {
                    continue;
                }

                $planPeople[] = $person;
            }
        }

        return $planPeople;
    }

    /**
     * @param $planId
     * @return array
     */
    public function getPlanInvolvedPeople($planId)
    {
        $plan = $this->tripsService->findPlan($planId);

        if (!$plan) {
            return [];
        }

        $adminAndEditorIds = TripsContributionRequests::query()
            ->where('plans_id', $planId)
            //->whereIn('role', ['admin', 'editor'])
            ->whereIn('role', ['admin'])
            ->where('status', 1)
            ->pluck('users_id')
            ->toArray();

        $adminAndEditorIds[] = $plan->users_id;

        return array_unique($adminAndEditorIds);
    }

    /**
     * @param string $search
     * @param int $planId
     * @param bool $withStyles
     * @return array
     * @throws \Exception
     */
    public function getPeopleToInvite($search, $planId, $withStyles = false)
    {
        $plan = TripPlans::find($planId);
        if (!$plan) {
            return [
                'experts' => [],
                'locals' => [],
                'friends' => [],
                'followers' => []
            ];
        }

        $planAuthorId = $plan->users_id;
        $query = User::query()->where('id', '!=', $planAuthorId)->whereDoesntHave('user_blocks');

        if ($search) {
            $query->where('name', 'like', '%' . $search . '%');
        }

        $tripLocations = $this->tripsService->getTripLocations($planId);

        $users['experts'] = $this->getExpertsToInvite(clone $query, $planId, $tripLocations);
        $users['locals'] = $this->getLocalsToInvite($users['experts']);
        $users['friends'] = $this->getFriendsToInvite(clone $query, $tripLocations);
        $users['followers'] = $this->getFollowersToInvite(clone $query, $users['friends']->pluck('id'), $tripLocations);

        if ($withStyles) {
            $this->filterByStyles($users);
        }

        return $this->collectResult($users, $planId);
    }

    /**
     * @param int $tripId
     * @param int $userId
     * @return bool
     * @throws \Exception
     */
    public function cancelInvitation($tripId, $userId)
    {
        $invite = TripsContributionRequests::where([
            'users_id' => $userId,
            'plans_id' => $tripId
        ])->first();

        if ($invite && $invite->delete()) {
            log_user_activity('Trip', 'cancel_invitation', $invite->id);

            Notifications::where('users_id', $userId)
                ->where('data_id', $invite->id)
                ->where('type', 'plan_invitation_received')
                ->delete();

            $logMeta = [
                'user_id' => $userId,
                'role' => self::ROLES_MAPPER[$invite->role]
            ];

            $this->planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_REMOVED_FROM_PLAN, $tripId, $invite->id, auth()->id(), $logMeta);

            TripsSuggestion::where([
                'status' => TripsSuggestionsService::SUGGESTION_PENDING_STATUS,
                'trips_id' => $tripId,
                'users_id' => $userId
            ])->delete();

            /** @var PlaceChatsService $placeChatsService */
            $placeChatsService = app(PlaceChatsService::class);
            $placeChatsService->removeFromPlanChats($tripId, $userId);

            return true;
        }

        return false;
    }

    /**
     * @param int $tripId
     * @return bool
     * @throws \Exception
     */
    public function leavePlan($tripId)
    {
        $invite = TripsContributionRequests::where([
            'users_id' => auth()->id(),
            'plans_id' => $tripId,
            'status' => TripInvitationsService::STATUS_ACCEPTED
        ])->first();

        if ($invite) {
            $invite->delete();

            /** @var PlaceChatsService $placeChatsService */
            $placeChatsService = app(PlaceChatsService::class);
            $placeChatsService->removeFromPlanChats($tripId, auth()->id());

            $logMeta = [
                'role' => self::ROLES_MAPPER[$invite->role]
            ];

            $this->planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_LEFT_THE_PLAN, $tripId, $invite->id, auth()->id(), $logMeta);

            TripsSuggestion::query()
                ->where([
                    'users_id' => auth()->id(),
                    'trips_id' => $tripId,
                    'status' => TripsSuggestionsService::SUGGESTION_APPROVED_STATUS
                ])
                ->where('type', '!=', TripsSuggestionsService::SUGGESTION_EDIT_BY_EDITOR_TYPE)
                ->update(['users_id' => $invite->plan->author->id]);

            return true;
        }

        return false;
    }

    /**
     * @param int $userId
     * @param string $role
     * @return Collection
     */
    public function getInvitedUserPlanIds($userId, $role)
    {
        return TripsContributionRequests::where([
            'users_id' => $userId,
            'role' => $role,
            'status' => self::STATUS_ACCEPTED
        ])->pluck('plans_id');
    }

    /**
     * @param int $userId
     * @param int $planId
     * @param string $role
     * @throws \Exception
     */
    public function changeRole($userId, $planId, $role)
    {
        $request = TripsContributionRequests::where('users_id', $userId)
            ->where('plans_id', $planId)
            ->first();

        if (!$request) {
            throw new ModelNotFoundException('Invited request not found');
        }

        if (!in_array($role, self::AVAILABLE_ROLES)) {
            throw new \Exception('Wrong role');
        }

        if (auth()->id() == $userId) {
            throw new \Exception('Invited admin can\'t change own role');
        }

        $beforeRole = $request->role;

        $request->role = $role;
        $request->save();

        if ($beforeRole == self::ROLE_EDITOR && $role == self::ROLE_ADMIN) {
            /** @var TripsSuggestionsService $suggestionsService */
            $suggestionsService = app(TripsSuggestionsService::class);
            $suggestionsService->sendSuggestions($planId, $userId);
        }

        $logMeta = [
            'user_id' => $request->users_id,
            'role' => self::ROLES_MAPPER[$role]
        ];

        $this->planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_CHANGED_ROLE, $planId, $request->id, auth()->id(), $logMeta);
    }

    /**
     * @param int $planId
     * @param int $userId
     * @return mixed|string
     */
    public function getUserRole($planId, $userId)
    {
        $plan = $this->tripsService->findPlan($planId);

        if (!$plan) {
            throw new ModelNotFoundException();
        }

        if ($plan->users_id === $userId) {
            return self::ROLES_MAPPER[self::ROLE_ADMIN];
        }

        $request = TripsContributionRequests::where([
            'users_id' => $userId,
            'plans_id' => $planId,
            'status' => self::STATUS_ACCEPTED
        ])->first();

        if ($request) {
            return self::ROLES_MAPPER[$request->role];
        }

        return '';
    }

    /**
     * @return Collection
     */
    public function findFriends()
    {
        if (Auth::check()) {
            return DB::table('users_followers as uf')
                ->select('uf.users_id')
                ->join('users_followers as uf2', function ($join) {
                    $join->on('uf.users_id', '=', 'uf2.followers_id');
                    $join->on('uf2.users_id', '=', 'uf.followers_id');
                    $join->on('uf2.follow_type', '=', 'uf.follow_type');
                })
                ->where('uf.users_id', '!=', 'uf.followers_id')
                ->where('uf.followers_id', '=', $this->authUser->id)
                ->where('uf.follow_type', '=', 1)
                ->pluck('uf.users_id');
        } else {
            return collect([]);
        }
    }

    /**
     * @return Collection
     */
    public function findFollowings()
    {
        if (Auth::check()) {
            return UsersFollowers::query()
                ->where('followers_id', $this->authUser->id)
                ->where('users_id', '!=', $this->authUser->id)
                ->where('follow_type', 1)
                ->pluck('users_id');
        } else {
            return collect([]);
        }
    }

    /**
     * @param null $userId
     * @return Collection
     */
    public function findFollowers($userId = null)
    {
        if (Auth::check()) {
            if (!$userId) {
                $userId = $this->authUser->id;
            }

            return UsersFollowers::query()
                ->where('followers_id', '!=', $userId)
                ->where('users_id', $userId)
                ->where('follow_type', 1)
                ->pluck('followers_id');
        } else {
            return collect([]);
        }
    }

    /**
     * @param array $users
     * @param int $planId
     * @return array
     */
    protected function collectResult($users, $planId)
    {
        $result = [
            'experts' => [],
            'locals' => [],
            'friends' => [],
            'followers' => []
        ];

        $invitedUsers = TripsContributionRequests::query()->where('plans_id', $planId)->get();

        foreach ($users as $group => $groupUsers) {
            foreach ($groupUsers as $user) {
                $invitedUser = $invitedUsers->where('users_id', $user->id)->first();

                if ($invitedUser && $invitedUser->count >= self::MAX_INVITATION_COUNT) {
                    continue;
                }

                $timeout = 0;

                if ($invitedUser && $invitedUser->status === self::STATUS_REJECTED) {
                    $updatedAt = $invitedUser->updated_at ? $invitedUser->updated_at : Carbon::create(1970);

                    $timeout = Carbon::now()->diffInSeconds($updatedAt) <= self::REJECT_INVITATION_TIMEOUT ? self::REJECT_INVITATION_TIMEOUT - Carbon::now()->diffInSeconds($updatedAt) : 0;
                }

                $result[$group][] = [
                    'id' => $user->id,
                    'img' => check_profile_picture($user->profile_picture),
                    'name' => $user->name,
                    'desc' => self::TABS_MAPPER[$group],
                    'timeout' => $timeout,
                    'invited' => ($invitedUser && $invitedUser->status !== self::STATUS_REJECTED) || ($timeout > 0) ? 1 : 0,
                    'role' => $invitedUser ? strtolower($invitedUser->role) : null,
                    'created_at' =>  $invitedUser ? $invitedUser->created_at->format(Carbon::DEFAULT_TO_STRING_FORMAT) : null,
                    'updated_at' =>  $invitedUser ? $invitedUser->updated_at->format(Carbon::DEFAULT_TO_STRING_FORMAT) : null,
                ];
            }
        }

        return $result;
    }

    /**
     * @param Builder $query
     * @param TripLocations $tripLocations
     * @return Collection
     */
    protected function getFriendsToInvite($query, $tripLocations)
    {
        $friends = $query->whereIn('id', $this->findFriends())->get();
        return $this->sortByVisitsAndCheckins($friends, 'friend', $tripLocations);
    }

    /**
     * @param Builder $query
     * @param array $except
     * @param TripLocations $tripLocations
     * @return Collection
     */
    protected function getFollowersToInvite($query, $except, $tripLocations)
    {
        $followers = UsersFollowers::query()
            ->where('users_id', $this->authUser->id)
            ->where('followers_id', '!=', $this->authUser->id)
            ->pluck('followers_id');

        $followers = $query
            ->whereIn('id', $followers)
            ->whereNotIn('id', $except)
            ->get();

        return $this->sortByVisitsAndCheckins($followers, 'follower', $tripLocations);
    }

    /**
     * @param Collection $users
     * @param string $type
     * @param TripLocations $tripLocations
     * @return mixed
     */
    protected function sortByVisitsAndCheckins($users, $type, $tripLocations)
    {
        /** @var VisitsService $visitsService */
        $visitsService = app(VisitsService::class);
        /** @var CheckinsService $checkinsService */
        $checkinsService = app(CheckinsService::class);

        $visits = $visitsService->getRelatedVisits($tripLocations->getPlaces(), false, $type);

        foreach ($visits as $visit) {
            $user = $users->where('id', $visit->users_id)->first();

            if (!$user) {
                continue;
            }

            $user->places_visits += 1;
        }

        $checkins = $checkinsService->getFriendsAndFollowingsCheckins($tripLocations->getPlaces(), [], $type);

        foreach ($checkins as $checkin) {
            $user = $users->where('id', $checkin->users_id)->first();

            if (!$user) {
                continue;
            }

            $user->places_checkins += 1;
        }

        return $users->sortByDesc('places_checkins')->sortByDesc('places_visits');
    }

    /**
     * @param Builder $query
     * @param int $planId
     * @param TripLocations $tripLocations
     * @return null|Collection
     */
    protected function getExpertsToInvite($query, $planId, $tripLocations)
    {
        $locationExperts = $this->getLocationExperts($planId, $tripLocations);

        if (!$locationExperts->count()) {
            return collect();
        }

        $idsOrdered = implode(',', $locationExperts->toArray());

        $experts = $query
            ->where('expert', 1)
            ->whereIn('id', $locationExperts)
            ->orderByRaw(DB::raw("FIELD(id, $idsOrdered)"))
            ->get();

        //$experts = $this->filterByNationality($experts);
        //$experts = $this->filterByStyles($experts);
        $experts = $this->sortByRanking($experts);

        return $experts;
    }

    /**
     * @param Collection $experts
     * @return Collection
     */
    protected function getLocalsToInvite($experts)
    {
        return $this->filterByNationality($experts);
    }

    /**
     * @param Collection $experts
     * @return Collection
     */
    protected function sortByRanking($experts)
    {
        /** @var RankingService $rankingService */
        $rankingService = app(RankingService::class);

        /** @var User $expert */
        foreach ($experts as &$expert) {
            $value = 10000;

            $progress = $rankingService->getProgressData($expert->id);
            $sumPoints = ($progress['interactions'] + $progress['followers']);

            $currentBadge = $progress['current_badge'];

            if ($currentBadge) {
                $currentBadgeSumPoints = $currentBadge->followers + $currentBadge->points;

                if ($sumPoints < $currentBadgeSumPoints) {
                    $sumPoints = $currentBadgeSumPoints;
                }
            }

            $value += $sumPoints;
            $value -= $this->calculateActivity($expert);

            $expert->order_value = $value;
        }

        return $experts->sortByDesc('order_value');
    }

    /**
     * @param User $expert
     * @return int
     */
    protected function calculateActivity(User $expert)
    {
        $lastDaysConditions = [
            Carbon::now()->subDays(7),
            Carbon::now()->subDays(15),
            Carbon::now()->subDays(30)
        ];

        foreach ($lastDaysConditions as $i => $lastDaysCondition) {
            $numberOfSessions = ActivityLog::query()->selectRaw('count(id) as count')->where([
                'users_id' => $expert->id,
                'action' => 'session_start'
            ])->where('time', '>=', $lastDaysCondition)->count();

            if ($numberOfSessions) {
                return $i * 1000;
            }
        }

        return 10000;
    }

    /**
     * @param Collection $experts
     * @return Collection
     */
    protected function filterByNationality($experts)
    {
        if ($this->authUser && $this->authUser->nationality) {
            return $experts->where('nationality', $this->authUser->nationality);
            //            if ($filteredNationality->count()) {
            //                $ids = $filteredNationality->pluck('id')->toArray();
            //
            //                $experts = $experts->sortBy(function ($model) use ($ids) {
            //                    $index = array_search($model->getKey(), $ids);
            //                    return $index === false ? 999999 : $index;
            //                });
            //            }
        }

        return collect();
    }

    /**
     * @param $usersGroups
     * @return void
     */
    protected function filterByStyles(&$usersGroups)
    {
        $styles = $this->authUser->travelstyles()->pluck('conf_lifestyles_id');

        if (!$styles->count()) {
            return;
        }

        foreach ($usersGroups as $group => &$users) {
            /** @var User $user */
            foreach ($users as $key => $user) {
                $count = $user->travelstyles()->whereIn('conf_lifestyles_id', $styles)->count();
                if (!$count) {
                    unset($users[$key]);
                    continue;
                }

                $user->count_of_styles = $count;
            }

            $usersGroups[$group] = $users->sortByDesc('count_of_styles');
        }
    }

    /**
     * @param $planId
     * @param TripLocations $tripLocations
     * @return Collection|null
     */
    protected function getLocationExperts($planId, $tripLocations)
    {
        $locationExperts = $this->getTripCitiesExperts($tripLocations->getCities());

        return $locationExperts->merge($this->getTripCountriesExperts($tripLocations->getCountries()));
    }

    /**
     * @param array $cities
     * @return Collection
     */
    protected function getTripCitiesExperts($cities)
    {
        return $this->getTripExperts(ExpertsCities::query()->whereIn('cities_id', $cities));
    }

    /**
     * @param array $countries
     * @return Collection
     */
    protected function getTripCountriesExperts($countries)
    {
        return $this->getTripExperts(ExpertsCountries::query()->whereIn('countries_id', $countries));
    }

    /**
     * @return Collection
     */
    protected function getTripStylesExperts()
    {
        $styles = $this->authUser->travelstyles()->pluck('conf_lifestyles_id');
        return $this->getTripExperts(ExpertsTravelStyles::query()->whereIn('travel_style_id', $styles));
    }

    /**
     * @param Builder $query
     * @return Collection
     */
    protected function getTripExperts($query)
    {
        $intersections = $query
            ->selectRaw('users_id, count(users_id) as count')
            ->groupBy('users_id')
            ->orderBy('count', 'desc')
            ->get();

        $max = $intersections->max('count');

        return $intersections->where('count', $max)->pluck('users_id');
    }

    public function isPlanMember($tripId, $userId)
    {
        return (TripsContributionRequests::query()
            ->where('plans_id', $tripId)
            ->where('status', 1)
            ->where('users_id', $userId)
            ->first()) ? true : false;
    }
}
