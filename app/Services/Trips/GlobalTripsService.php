<?php

namespace App\Services\Trips;

use App\DataObjects\Trips\TripLocations;
use App\Models\City\Cities;
use App\Models\City\CitiesTranslations;
use App\Models\Country\Countries;
use App\Models\Country\CountriesTranslations;
use App\Models\Place\Place;
use App\Models\States\States;
use App\Models\States\StateTranslation;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class GlobalTripsService
{
    /**
     * Get Trip Plans trending destinations.
     * @param string $type
     * @return collect
     */
    public function getTrendingLocations($type)
    {
        $trip_cities = DB::table('trips')
            ->select('tp.cities_id', DB::raw('count(tp.cities_id) as total_cities'))
            ->join('trips_places as tp', function ($join) {
                $join->on('tp.trips_id', '=', 'trips.id');
            })
            ->where('trips.active', 1)
            ->where('tp.cities_id', '!=', 2031)
            ->where('trips.privacy', 0)
            ->whereNull('trips.deleted_at')
            ->whereNotIn('users_id', blocked_users_list());

        if ($type) {
            $trip_cities = $trip_cities->where('trips.created_at', '>=', Carbon::now()->subDays(30));
        }

        $trip_cities = $trip_cities->groupBy('tp.cities_id')
            ->orderBy('total_cities', 'desc')
            ->take(20)->get();

        return $trip_cities;
    }

    /**
     * Get top Trip Plans.
     * @param string $type
     * @return collect
     */
    public function getTopTripPlans($type)
    {
        $tripInvitationsService = app(TripInvitationsService::class);

        $friends = $tripInvitationsService->findFriends();
        $query = TripPlans::query();
        $authUserId = auth()->id();
        $_where = '';

        if ($type) {
            $_where = ' and created_at  > "' . Carbon::now()->subDays(30) . '"';
        }


        $query->select('*', DB::raw('(COALESCE(comment_count, 0) + COALESCE(steps_likes_count, 0) + COALESCE(steps_shares_count, 0) + COALESCE(medias_likes_count, 0) + COALESCE(medias_comments_count, 0) + COALESCE(medias_shares_count, 0) + COALESCE(sum(like_count), 0) + COALESCE(sum(shares_count), 0))  as ranks'))
            ->leftJoin(DB::raw('(SELECT `trips_id`, sum(c_count) as comment_count FROM `trips_places` LEFT JOIN (SELECT posts_id, count(*) as c_count FROM posts_comments WHERE type= "trip" ' . $_where . ' GROUP by posts_id) as tps_comment ON tps_comment.posts_id=trips_places.id GROUP by trips_id) as tps'), function ($join) {
                $join->on('trips.id', '=', 'tps.trips_id');
            })
            ->leftJoin(DB::raw('(SELECT trips_id, count(*) as like_count FROM trips_likes WHERE 1=1 ' . $_where . ' GROUP by trips_id) as tpl'), function ($join) {
                $join->on('trips.id', '=', 'tpl.trips_id');
            })
            ->leftJoin(DB::raw('(SELECT trip_id, count(*) as shares_count FROM trips_shares WHERE 1=1 ' . $_where . ' GROUP by trip_id) as tsh'), function ($join) {
                $join->on('trips.id', '=', 'tsh.trip_id');
            })
            ->leftJoin(DB::raw('(SELECT `trips_id`, sum(l_count) as steps_likes_count FROM `trips_places` LEFT JOIN (SELECT places_id, count(*) as l_count FROM trips_likes where places_id is not NULL ' . $_where . ' GROUP by places_id) as tps_likes ON tps_likes.places_id=trips_places.id GROUP by trips_id) as tsl'), function ($join) {
                $join->on('trips.id', '=', 'tsl.trips_id');
            })
            ->leftJoin(DB::raw('(SELECT `trips_id`, sum(tpsh_count) as steps_shares_count FROM `trips_places` LEFT JOIN (SELECT posts_type_id, count(*) as tpsh_count FROM posts_shares WHERE type= "trip" ' . $_where . ' GROUP by posts_type_id) as tp_shares ON tp_shares.posts_type_id=trips_places.id GROUP by trips_id) as tssh'), function ($join) {
                $join->on('trips.id', '=', 'tssh.trips_id');
            })
            ->leftJoin(DB::raw('(SELECT `trips_id`, sum(ml_count) as medias_likes_count FROM `trips_medias` LEFT JOIN (SELECT medias_id, count(*) as ml_count FROM medias_likes WHERE 1=1 ' . $_where . ' GROUP by medias_id) as tm_likes ON tm_likes.medias_id=trips_medias.medias_id GROUP by trips_id) as tml'), function ($join) {
                $join->on('trips.id', '=', 'tml.trips_id');
            })
            ->leftJoin(DB::raw('(SELECT `trips_id`, sum(msh_count) as medias_shares_count FROM `trips_medias` LEFT JOIN (SELECT trip_media_id, count(*) as msh_count FROM trip_medias_shares WHERE 1=1 ' . $_where . ' GROUP by trip_media_id) as m_shares ON m_shares.trip_media_id=trips_medias.id GROUP by trips_id) as msh'), function ($join) {
                $join->on('trips.id', '=', 'msh.trips_id');
            })
            ->leftJoin(DB::raw('(SELECT `trips_id`, sum(mc_count) as medias_comments_count FROM `trips_medias` LEFT JOIN (SELECT medias_id, count(*) as mc_count FROM medias_comments WHERE 1=1 ' . $_where . ' GROUP by medias_id) as tm_comments ON tm_comments.medias_id=trips_medias.medias_id GROUP by trips_id) as tmc'), function ($join) {
                $join->on('trips.id', '=', 'tmc.trips_id');
            });



        $query->where('trips.active', 1)
            ->where(function ($q) use ($friends, $authUserId) {
                $q->where(function ($q) use ($friends) {
                    $q->whereIn('trips.users_id', $friends);
                    $q->where('trips.privacy', TripsService::PRIVACY_FRIENDS);
                });
                $q->orWhere('trips.privacy', TripsService::PRIVACY_PUBLIC);
                $q->orWhere('trips.users_id', $authUserId);
            });
        $query->groupBy('trips.id')->orderBy('ranks', 'DESC');
        $query->having('ranks', '>', 0);

        return $query->take(6)->get();
    }
}
