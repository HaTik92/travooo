<?php

namespace App\Services\Trips;

use App\DataObjects\Trips\TripLocations;
use App\Models\City\Cities;
use App\Models\City\CitiesTranslations;
use App\Models\Country\Countries;
use App\Models\Country\CountriesTranslations;
use App\Models\Place\Place;
use App\Models\States\States;
use App\Models\States\StateTranslation;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\File;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class TripsService
{
    const PRIVACY_PUBLIC = 0;
    const PRIVACY_FRIENDS = 1;
    const PRIVACY_PRIVATE = 2;

    const NEWSFEED_SINGLE_CITY      = 'single-city';
    const NEWSFEED_MULTIPLE_CITY    = 'multiple-city';
    const NEWSFEED_MULTIPLE_COUNTRY = 'multiple-country';

    const PRIVACY_MAPPING = [
        self::PRIVACY_PUBLIC => 'Public',
        self::PRIVACY_FRIENDS => 'Friends',
        self::PRIVACY_PRIVATE => 'Private'
    ];

    const THUMB_RES_PREFIX_ORIGINAL = 'original';
    const THUMB_RES_PREFIX_SMALL = 'th180';
    const THUMB_RES_PREFIX_MIDDLE = 'th640';
    /**
     * @var TripMapDrawingService
     */
    private $tripMapDrawingService;


    public function __construct(TripMapDrawingService $tripMapDrawingService)
    {
        $this->tripMapDrawingService = $tripMapDrawingService;
    }

    /**
     * @param $planId
     * @return TripLocations
     * @throws \Exception
     */
    public function getTripLocations($planId)
    {
        $plan = $this->findPlan($planId);

        if (!$plan) {
            throw new ModelNotFoundException();
        }

        $cities = collect();
        $countries = collect();

        /** @var TripsSuggestionsService $tripsSuggestionsService */
        $tripsSuggestionsService = app(TripsSuggestionsService::class);
        $planData = $tripsSuggestionsService->getPlanContentsData(true, $planId, auth()->id());

        $planPlaces = $planData['trip_places_arr'];

        /** @var Place $place */
        foreach ($planPlaces as $place) {
            $cities->push($place->city);
            $countries->push($place->country);
        }

        $cities = array_unique($cities->pluck('id')->toArray());
        $countries = array_unique($countries->pluck('id')->toArray());
        $planPlaces = array_unique(collect($planPlaces)->pluck('id')->toArray());

        return new TripLocations($cities, $countries, $planPlaces);
    }

    /**
     * @param $planId
     * @return TripPlans|TripPlans[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function findPlan($planId)
    {
        return TripPlans::find($planId);
    }

    public function isPlanCreator($planId, $userId = null)
    {
        if (!$userId) {
            $userId = auth()->id();
        }

        return $this->findPlan($planId)->users_id === $userId;
    }

    /**
     * @param int $planId
     * @return bool
     */
    public function isEventsExpired($planId)
    {
        return false;
    }

    public function addNewPlaceByProviderId($providerId)
    {
        $mapboxLink = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . urlencode($providerId) . '.json?limit=10&routing=true&types=poi&access_token=' . config('mapbox.token');

        if (isset($type) && $type) {
            $mapboxLink .= '&types=' . $type;
        }

        $mapboxResult = json_decode(file_get_contents($mapboxLink));

        if (!isset($mapboxResult->features[0])) {
            return false;
        }

        $mapBoxPlace = $mapboxResult->features[0];
        $mapboxLink = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . urlencode(implode(',', $mapBoxPlace->center)) . '.json?language=EN&limit=1&access_token=' . config('mapbox.token');
        $mapboxResult = json_decode(file_get_contents($mapboxLink));

        $geo_city = @$mapboxResult->features[0]->context;
        $geo_city_guess = '';
        $geo_country = '';
        $geo_state = '';
        $country_code = '';

        if (isset($geo_city)) {
            foreach ($geo_city as $gc) {
                $type = @explode('.', $gc->id)[0];

                switch ($type) {
                    case 'region':
                        $geo_city_guess = $gc->text;
                        break;
                    case 'country':
                        $country_code = $gc->short_code;
                        $geo_country = $gc->text;
                        break;
                    default:
                        break;
                }

                //$geo_state = $gc->long_name;
            }
        }

        if (isset($geo_city_guess) && !empty($geo_city_guess)) {
            $get_city = Cities::whereHas('transsingle', function ($query) use ($geo_city_guess) {
                $query->where('title', 'like', '%' . $geo_city_guess . '%');
            })->get()->first();
        }

        $p = new Place();
        $p->place_type = @join(",", @$mapboxResult->features[0]->types);
        $p->safety_degrees_id = 1;
        $p->provider_id = $providerId;

        if (isset($get_city) && is_object($get_city)) {

            $p->countries_id = $get_city->countries_id;
            $p->cities_id = $get_city->id;
            $p->states_id = $get_city->states_id;
        } else {
            $geo_country_info =  Countries::whereHas('transsingle', function ($query) use ($geo_country) {
                $query->where('title', 'like', '%' . $geo_country . '%');
            })->get()->first();

            $country_id = 326;
            // check if country exists or else add new
            if (isset($geo_country_info)) {
                $country_id  = $geo_country_info->id;
            } else {
                $country  = new Countries();
                $country->lat = $mapboxResult->features[0]->center[1];
                $country->lng = $mapboxResult->features[0]->center[0];
                $country->iso_code = $country_code;
                $country->regions_id = 22;
                $country->safety_degree_id = 1;
                $country->active = 1;
                if ($country->save()) {
                    $country_id = $country->id;
                    $country_trans =  new CountriesTranslations();
                    $country_trans->countries_id = $country->id;
                    $country_trans->title = $geo_country;
                    $country_trans->languages_id = 1;
                    $country_trans->save();
                }
            }


            //check if state exists or add new
            if (isset($geo_state) && !empty($geo_state)) {
                $state = States::whereHas('transsingle', function ($query) use ($geo_state) {
                    $query->where('title', 'like', '%' . $geo_state . '%');
                })->get()->first();
            }
            $states_id = 0;
            if (isset($state) && is_object($state)) {
                $states_id =  $state->id;
            } else {
                $state  = new States();
                $state->lat = $mapboxResult->features[0]->center[1];
                $state->lng = $mapboxResult->features[0]->center[0];
                $state->countries_id  = $country_id;
                $state->active = 1;
                if ($country_id != 227) {
                    $state->is_province = 1;
                }
                if ($state->save()) {
                    $states_id = $state->id;
                    $state_trans =  new StateTranslation();
                    $state_trans->states_id = $state->id;
                    $state_trans->title = $geo_state;
                    $state_trans->languages_id = 1;
                    $state_trans->save();
                }
            }
            $city_id = 2031;
            // add City as it wasnt added before

            if (!empty($geo_city_guess)) {
                $city  = new Cities();
                $city->lat = $mapboxResult->features[0]->center[1];
                $city->lng = $mapboxResult->features[0]->center[0];
                $city->countries_id  = $country_id;
                $city->states_id  = $states_id;
                $city->safety_degree_id  = 1;
                $city->level_of_living_id  = 0;
                $city->active  = 1;
                if ($city->save()) {
                    $city_id = $city->id;
                    $city_trans =  new CitiesTranslations();
                    $city_trans->cities_id = $city->id;
                    $city_trans->title = $geo_city_guess;
                    $city_trans->languages_id = 1;
                    $city_trans->save();
                }
            }
            $p->countries_id = $country_id;
            $p->states_id = $states_id;
            $p->cities_id = $city_id;
        }

        $p->lat = $mapboxResult->features[0]->center[1];
        $p->lng = $mapboxResult->features[0]->center[0];
        $p->active = 1;
        $p->auto_import = 1;
        $p->save();

        $pt = new \App\Models\Place\PlaceTranslations();
        $pt->languages_id = 1;
        $pt->places_id = $p->id;
        $pt->title = $mapboxResult->features[0]->text;
        $pt->address = $mapboxResult->features[0]->place_name;
        $pt->save();

        return $p->id;
    }

    /**
     * @param $tripId
     * @param $tripPlace
     * @return bool
     */
    public function recalculateOrders($tripId, $tripPlace, $time = null)
    {
        $tripPlace->refresh();

        if (!$time) {
            $order = $tripPlace->order;

            $tripPlacesWithMoreOrder = TripPlaces::where('trips_id', $tripId)
                ->where('date', $tripPlace->date)
                ->where('order', '>=', $order)
                ->where('id', '!=', $tripPlace->id)
                ->get();

            foreach ($tripPlacesWithMoreOrder as $tripPlaceWithMoreOrder) {
                $tripPlaceWithMoreOrder->order++;
                $tripPlaceWithMoreOrder->save();
            }

            return true;
        }

        $tripPlaces = TripPlaces::where('trips_id', $tripId)
            ->where('date', $tripPlace->date)
            ->orderBy('time', 'ASC')
            ->get();

        $order = 0;

        foreach ($tripPlaces as $tripPlace) {
            $order++;
            $tripPlace->order = $order;
            $tripPlace->save();
        }

        return true;
    }

    /**
     * @param int $placeId
     * @return array
     */
    public function getPlaceDataById($placeId)
    {
        $place = Place::findOrFail($placeId);
        $image = null;

        if (isset($place->getMedias[0]) && is_object($place->getMedias[0])) {
            $image = 'https://s3.amazonaws.com/travooo-images2/' . $place->getMedias[0]->url;
        }

        return [
            'id' => $place->id,
            'place_type' => @explode(",", $place->place_type)[0],
            'address' => str_replace("'", "&quot;", $place->transsingle->address),
            'title' => $place->transsingle->title,
            'lat' => $place->lat,
            'lng' => $place->lng,
            'rating' => @$place->rating,
            'location' => $place->id,
            'image' => $image,
            'city_id' => @$place->cities_id,
            'city_title' => @$place->city->transsingle->title,
            'city_img' => @$place->city->city_medias[0]->url ? 'https://s3.amazonaws.com/travooo-images2/th180/' . $place->city->city_medias[0]->url : asset('assets2/image/placeholders/place.png')
        ];
    }

    /**
     * @param int $cityId
     * @return array
     */
    public function getCityDataById($cityId)
    {
        $city = Cities::findOrFail($cityId);

        return [
            'id' => $city->id,
            'title' => $city->transsingle->title,
            'country' => 'city in ' . $city->country->transsingle->title,
            'lat' => $city->lat,
            'lng' => $city->lng,
            'city_img' => @$city->city_medias[0]->url ? 'https://s3.amazonaws.com/travooo-images2/th180/' . $city->city_medias[0]->url : asset('assets2/image/placeholders/place.png')
        ];
    }

    /**
     * @param int $countryId
     * @return array
     */
    public function getCountryDataById($countryId)
    {
        $country = Countries::findOrFail($countryId);

        return [
            'id' => $country->id,
            'title' => $country->transsingle->title,
            'lat' => $country->lat,
            'lng' => $country->lng
        ];
    }

    /**
     * @param int $planId
     *
     * @return array
     */
    public function getPlanDataById($planId)
    {
        /** @var TripsSuggestionsService $tripsSuggestionsService */
        $tripsSuggestionsService = app(TripsSuggestionsService::class);

        $plan = $this->findPlan($planId);

        if (!$plan) {
            throw new ModelNotFoundException('Plan not found');
        }

        $planSuggestions = $tripsSuggestionsService->getEditPlanSuggestions($planId);

        $planData = [
            'privacy' => $plan->privacy,
            'description' => $plan->description,
            'title' => $plan->title,
            'cover' => $plan->cover
        ];

        $result = [
            'actual' => $planData,
            'draft' => $planData
        ];

        $authUserId = auth()->id();

        $availableEditMode = $plan->users_id == $authUserId;

        if (!$availableEditMode) {
            $invitedRequest = $tripsSuggestionsService->getInvitedUserRequest($planId, $authUserId, false);

            $availableEditMode = $invitedRequest
                && $invitedRequest->status !== TripInvitationsService::STATUS_REJECTED
                && in_array($invitedRequest->role, ['admin', 'editor']);
        }

        if (!$availableEditMode) {
            return $result;
        }

        foreach ($planSuggestions as $planSuggestion) {
            $meta = $planSuggestion->meta;

            $result['draft'] = [
                'privacy' => Arr::get($meta, 'privacy'),
                'description' => Arr::get($meta, 'description'),
                'title' => Arr::get($meta, 'name'),
                'cover' => Arr::get($meta, 'cover')
            ];
        }

        return $result;
    }

    public function getThumb($plan, $width, $height)
    {
        $filename = $plan->id . '_' . $width . 'x' . $height . '.jpg';
        $path = 'trips/' . $plan->id . '/thumbs/' . TripMapDrawingService::VERSION;
        $fullPath = $path . '/' . $filename;
        $url = S3_BASE_URL . $fullPath;

        if (!Storage::disk('s3')->exists($fullPath)) {
            $this->tripMapDrawingService->setWidthAndHeight($width, $height);
            $thumb = $this->tripMapDrawingService->generateTripMapThumbnail($plan->id);

            imagejpeg($thumb, storage_path($filename), 100);
            $file = new File(storage_path($filename));
            Storage::disk('s3')->putFileAs($path, $file, $filename, 'public');
            @unlink(storage_path($filename));
        }

        return $url;
        //        $resolutions = [
        //            self::THUMB_RES_PREFIX_ORIGINAL => null,
        //            self::THUMB_RES_PREFIX_SMALL => 180,
        //            self::THUMB_RES_PREFIX_MIDDLE => 640,
        //        ];
        //
        //        foreach ($resolutions as $prefix => $val) {
        //            if ($prefix !== self::THUMB_RES_PREFIX_ORIGINAL) {
        //                $cropped = \Intervention\Image\ImageManagerStatic::make($image)->orientate()->resize($val, $val, function ($constraint) {
        //                    $constraint->aspectRatio();
        //                })->encode(null, 100);
        //                Storage::put($prefix . '_' . $filename, $cropped);
        //            }
        //
        //
        //        }
        //
        //        $url = S3_BASE_URL . $path . '/' . self::THUMB_RES_PREFIX_ORIGINAL . '/' . $filename;
    }
}
