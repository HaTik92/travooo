<?php

namespace App\Services\Trips;

use App\Events\Api\PlanLogs\PlanLogRemovedApiEvent;
use App\Events\PlanLogs\PlanLogRemovedEvent;
use App\Exceptions\MaxRejectedTimesException;
use App\Models\Notifications\Notifications;
use App\Models\Reviews\Reviews;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlaces\DismissedTripPlaces;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlaces\TripPlacesComments;
use App\Models\TripPlans\PlanActivityLog;
use App\Models\TripPlans\PlanActivityLogUser;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsContributionRequests;
use App\Models\TripPlans\TripsLikes;
use App\Models\TripPlans\TripsSuggestion;
use App\Models\User\User;
use App\Models\Visits\Visit;
use App\Resolvers\SuggestionEditBudgetResolver;
use App\Resolvers\SuggestionEditDateResolver;
use App\Resolvers\SuggestionEditDurationResolver;
use App\Resolvers\SuggestionEditMediaResolver;
use App\Resolvers\SuggestionEditResolver;
use App\Resolvers\SuggestionEditResolverInterface;
use App\Resolvers\SuggestionEditStoryResolver;
use App\Resolvers\SuggestionEditTagsResolver;
use App\Services\Checkins\CheckinsService;
use App\Services\Ranking\RankingService;
use App\Services\Translations\TranslationService;
use App\Services\Visits\VisitsService;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class TripsSuggestionsService
{
    const SUGGESTION_PENDING_STATUS = 0;
    const SUGGESTION_APPROVED_STATUS = 1;
    const SUGGESTION_DISAPPROVED_STATUS = 2;
    const SUGGESTION_REMOVED_STATUS = 3;

    const SUGGESTION_CREATE_TYPE = 0;
    const SUGGESTION_EDIT_TYPE = 1;
    const SUGGESTION_DELETE_TYPE = 2;
    const SUGGESTION_EDIT_PLAN_DATA_TYPE = 3;
    const SUGGESTION_EDIT_BY_EDITOR_TYPE = 4;

    const MAX_REJECTED_SUGGESTIONS_COUNT = 3;

    const SUGGESTION_EDIT_TYPE_DURATION = 'duration';
    const SUGGESTION_EDIT_TYPE_BUDGET = 'budget';
    const SUGGESTION_EDIT_TYPE_DATE = 'date';
    const SUGGESTION_EDIT_TYPE_MEDIA = 'media';
    const SUGGESTION_EDIT_TYPE_STORY = 'story';
    const SUGGESTION_EDIT_TYPE_TAGS = 'tags';

    const SUGGESTION_EDIT_TYPES = [
        self::SUGGESTION_EDIT_TYPE_DURATION,
        self::SUGGESTION_EDIT_TYPE_BUDGET,
        self::SUGGESTION_EDIT_TYPE_DATE,
        self::SUGGESTION_EDIT_TYPE_MEDIA,
        self::SUGGESTION_EDIT_TYPE_STORY,
        self::SUGGESTION_EDIT_TYPE_TAGS
    ];

    const SUGGESTIONS_PACK_CACHE_PREFIX = 'suggestions_pack_';
    const SUGGESTIONS_ROOT_CACHE_PREFIX = 'suggestions_root_';
    const CACHE_TIME = 60;

    /** @var TripInvitationsService */
    protected $invitationsService;

    /**
     * @var PlanActivityLogService
     */
    protected $planActivityLogService;

    /**
     * @var CheckinsService
     */
    private $checkinsService;
    /**
     * @var VisitsService
     */
    private $visitsService;
    /**
     * @var RankingService
     */
    private $rankingService;
    /**
     * @var TranslationService
     */
    private $translationService;
    /**
     * @var SuggestionUserLogsService
     */
    private $suggestionUserLogsService;

    /**
     * TripsSuggestionsService constructor.
     * @param TripInvitationsService $invitationsService
     * @param PlanActivityLogService $planActivityLogService
     * @param CheckinsService $checkinsService
     * @param VisitsService $visitsService
     * @param RankingService $rankingService
     * @param TranslationService $translationService
     * @param SuggestionUserLogsService $suggestionUserLogsService
     */
    public function __construct(
        TripInvitationsService $invitationsService,
        PlanActivityLogService $planActivityLogService,
        CheckinsService $checkinsService,
        VisitsService $visitsService,
        RankingService $rankingService,
        TranslationService $translationService,
        SuggestionUserLogsService $suggestionUserLogsService
    ) {
        $this->invitationsService = $invitationsService;
        $this->planActivityLogService = $planActivityLogService;
        $this->checkinsService = $checkinsService;
        $this->visitsService = $visitsService;
        $this->rankingService = $rankingService;
        $this->translationService = $translationService;
        $this->suggestionUserLogsService = $suggestionUserLogsService;
    }

    /**
     * @param $suggestedPlaceId
     * @return bool
     * @throws \Exception
     */
    public function dismissSuggestedPlace($suggestedPlaceId)
    {
        $suggestion = TripsSuggestion::query()->where('suggested_trips_places_id', $suggestedPlaceId)->first();

        if (
            $suggestion
            && $suggestion->status === self::SUGGESTION_PENDING_STATUS
            && $suggestion->users_id === auth()->id()
        ) {
            $this->suggestionUserLogsService->remove($suggestion->id);
            $suggestion->delete();
            //$this->planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_DISAPPROVED_SUGGESTION, $suggestion->trips_id, $suggestion->id, auth()->id());
            return true;
        }

        DismissedTripPlaces::query()->firstOrCreate([
            'user_id' => auth()->id(),
            'trip_place_id' => $suggestedPlaceId
        ]);

        return true;
    }

    /**
     * @param int $tripPlaceId
     * @param bool $withTrashed
     * @return int
     */
    public function getRootTripPlace($tripPlaceId, $withTrashed = false)
    {
        $cacheKey = self::SUGGESTIONS_ROOT_CACHE_PREFIX . $tripPlaceId;

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $lastSuggestion = TripsSuggestion::query()
            ->where(['suggested_trips_places_id' => $tripPlaceId])
            ->where('type', '!=', self::SUGGESTION_DELETE_TYPE)
            ->where('type', '!=', self::SUGGESTION_EDIT_BY_EDITOR_TYPE)
            ->latest()
            ->first();

        if (!$lastSuggestion || !$lastSuggestion->active_trips_places_id || (!$lastSuggestion->activePlace && !$withTrashed)) {
            Cache::put($cacheKey, $tripPlaceId, self::CACHE_TIME * 240);
            return $tripPlaceId;
        }

        $result = $this->getRootTripPlace($lastSuggestion->active_trips_places_id, $withTrashed);
        Cache::put($cacheKey, $result, self::CACHE_TIME * 240);

        return $result;
    }

    /**
     * @param int $tripPlaceId
     * @param bool $withTrashed
     * @return int
     */
    public function getLastTripPlace($tripPlaceId, $withTrashed = false)
    {
        $lastSuggestion = TripsSuggestion::query()
            ->where(['active_trips_places_id' => $tripPlaceId])
            ->where('type', '!=', self::SUGGESTION_DELETE_TYPE)
            ->where('type', '!=', self::SUGGESTION_EDIT_BY_EDITOR_TYPE)
            ->where('status', self::SUGGESTION_APPROVED_STATUS)
            ->latest()
            ->first();

        if (!$lastSuggestion || !$lastSuggestion->suggested_trips_places_id || (!$lastSuggestion->suggestedPlace && !$withTrashed)) {
            return $tripPlaceId;
        }

        return $this->getLastTripPlace($lastSuggestion->suggested_trips_places_id);
    }

    /**
     * @param $tripPlaceId
     * @return array
     */
    public function getAllSuggestionPackTripPlacesIds($tripPlaceId)
    {
        return self::getAllSuggestionPackTripPlacesIdsStatic($tripPlaceId);
    }

    public static function getAllSuggestionPackTripPlacesIdsStatic($tripPlaceId)
    {
        //        $cacheKey = self::SUGGESTIONS_PACK_CACHE_PREFIX . $tripPlaceId;
        //
        //        if (Cache::has($cacheKey)) {
        //            return Cache::get($cacheKey);
        //        }

        $result = [(int)$tripPlaceId];
        $suggestion = TripsSuggestion::query()->where('suggested_trips_places_id', $tripPlaceId)->where('type', '!=', self::SUGGESTION_DELETE_TYPE)->first();

        while ($suggestion && $suggestion->active_trips_places_id) {
            $result[] = $suggestion->active_trips_places_id;
            $suggestion = TripsSuggestion::query()->where('suggested_trips_places_id', $suggestion->active_trips_places_id)->where('type', '!=', self::SUGGESTION_DELETE_TYPE)->first();
        }

        //        Cache::put($cacheKey, $result, self::CACHE_TIME);
        return $result;
    }

    /**
     * @param int $planId
     * @return bool
     */
    public static function isNotSendingSuggestions($planId)
    {
        $deletedPlaces = TripsSuggestion::query()
            ->where([
                'status' => self::SUGGESTION_APPROVED_STATUS,
                'type' => self::SUGGESTION_DELETE_TYPE,
                'trips_id' => $planId
            ])
            ->pluck('suggested_trips_places_id')
            ->unique();

        $allDeletedPlaces = collect();

        foreach ($deletedPlaces as $deletedPlace) {
            /** @var self $thisService */
            $thisService = app(self::class);
            $allDeletedPlaces = $allDeletedPlaces->merge($thisService->getAllSuggestionPackTripPlacesIds($deletedPlace));
        }

        return TripsSuggestion::where([
            'status' => self::SUGGESTION_PENDING_STATUS,
            'trips_id' => $planId,
            'is_saved' => 0,
            'users_id' => auth()->id()
        ])
            ->where(function ($query) use ($allDeletedPlaces) {
                $query
                    ->whereNotIn('active_trips_places_id', $allDeletedPlaces->unique())
                    ->orWhereNull('active_trips_places_id');
            })
            ->exists();
    }

    /**
     * @param $editable
     * @param $tripId
     * @param $authUserId
     * @param bool $previewMode
     * @return mixed
     * @throws \Exception
     */
    public function getPlanContents($editable, $tripId, $authUserId, $previewMode = false)
    {
        return View::make('site.trip2.partials.plan_contents', $this->getPlanContentsData($editable, $tripId, $authUserId, $previewMode));
    }

    /**
     * @param $editable
     * @param $tripId
     * @return mixed
     */
    public function getPlanContentsApi($editable, $tripId, $authUserId)
    {
        return $this->getPlanContentsData($editable, $tripId, $authUserId);
    }

    public function getBasicPlanContentsApi($editable, $tripId, $authUserId)
    {
        return $this->getPlanContentsData($editable, $tripId, $authUserId);
    }

    /**
     * @param $placeId
     * @param $planId
     * @param $type
     * @param null $activePlaceId
     * @param string $reason
     * @param null $suggestionEditType
     * @param bool $withNotification
     * @return int
     */
    public function createSuggestion($placeId, $planId, $type, $activePlaceId = null, $reason = null, $suggestionEditType = null, $withNotification = true)
    {
        $tripSuggestion = new TripsSuggestion();

        $tripSuggestion->users_id = auth()->id();
        $tripSuggestion->trips_id = $planId;
        $tripSuggestion->type = $type;
        $tripSuggestion->active_trips_places_id = $activePlaceId;
        $tripSuggestion->suggested_trips_places_id = $placeId;
        $tripSuggestion->status = self::SUGGESTION_PENDING_STATUS;
        $tripSuggestion->reason = $reason;

        if ($suggestionEditType) {
            $tripSuggestion->meta = [
                'edit_type' => $suggestionEditType
            ];
        }

        $tripSuggestion->save();

        foreach ($this->invitationsService->getPlanInvolvedPeople($planId) as $involvedPerson) {
            if ($withNotification) {
                $this->newSuggestionNotification($involvedPerson, $tripSuggestion->id);
            }

            $this->suggestionUserLogsService->create($tripSuggestion->id, $involvedPerson);
        }

        return $tripSuggestion->id;
    }

    /**
     * @param int $planId
     * @param string $name
     * @param string $description
     * @param int $privacy
     * @param $cover
     *
     * @return TripsSuggestion
     */
    public function createEditPlanSuggestion($planId, $name, $description, $privacy, $cover)
    {
        $tripSuggestion = new TripsSuggestion();

        $tripSuggestion->users_id = auth()->id();
        $tripSuggestion->trips_id = $planId;
        $tripSuggestion->type = self::SUGGESTION_EDIT_PLAN_DATA_TYPE;
        $tripSuggestion->status = self::SUGGESTION_APPROVED_STATUS;
        $meta = [
            'name' => $name,
            'description' => $description,
            'privacy' => $privacy
        ];

        if ($cover) {
            $filename = time() . '_' . bin2hex(random_bytes(10)) . '.' . $cover->getClientOriginalExtension();

            Storage::disk('s3')->putFileAs(
                'trips/cover/' . $planId . '/',
                $cover,
                $filename,
                'public'
            );

            $meta['cover'] = 'https://s3.amazonaws.com/travooo-images2/trips/cover/' . $planId . '/' . $filename;
        }

        $tripSuggestion->meta = $meta;
        $tripSuggestion->save();

        return $tripSuggestion;
    }

    /**
     * @param int $planId
     * @return TripsSuggestion[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getEditPlanSuggestions($planId)
    {
        return TripsSuggestion::where([
            'type' => self::SUGGESTION_EDIT_PLAN_DATA_TYPE,
            'is_published' => 0,
            'trips_id' => $planId
        ])->orderBy('created_at')->get();
    }

    /**
     * @param int $suggestionId
     * @return TripsSuggestion
     * @throws AuthorizationException
     * @throws \Exception
     */
    public function approveSuggestion($suggestionId)
    {
        /** @var TripsSuggestion $suggestion */
        $suggestion = TripsSuggestion::find($suggestionId);

        if (!$suggestion) {
            throw new \Exception('Wrong suggestion id');
        }

        $authUserId = auth()->id();

        $plan = $suggestion->plan;

        if ($plan->users_id !== $authUserId) {
            $invitedUserRequest = $this->getInvitedUserRequest($plan->id, $authUserId);

            if (!$invitedUserRequest || $invitedUserRequest->role !== 'admin') {
                throw new AuthorizationException('Unauthorized', 403);
            }
        }

        if ($suggestion->type === self::SUGGESTION_EDIT_BY_EDITOR_TYPE && isset($suggestion->meta['edit_type'])) {
            $oldSuggestion = clone $suggestion;

            $resolver = $this->getSuggestionEditResolver($suggestion->meta['edit_type'], $suggestion->suggestedPlace, [], $suggestion);
            $suggestion = $resolver->actualizeSuggestion();

            $oldSuggestion->status = self::SUGGESTION_APPROVED_STATUS;
            $oldSuggestion->save();

            if ($oldSuggestion->meta['edit_type'] !== self::SUGGESTION_EDIT_TYPE_MEDIA) {
                $medias = TripMedias::query()->where([
                    'trips_id' => 344,
                    'trip_place_id' => 1745
                ])->get()->unique('medias_id')->pluck('medias_id');

                foreach ($medias as $mediaId) {
                    $attributes = [
                        'medias_id' => $mediaId,
                        'trips_id' => $plan->id,
                        'places_id' => $suggestion->suggestedPlace->places_id,
                        'trip_place_id' => $suggestion->suggested_trips_places_id
                    ];

                    TripMedias::query()->firstOrCreate($attributes);
                }
            }
        }

        $suggestion->status = self::SUGGESTION_APPROVED_STATUS;
        $suggestion->is_saved = 0;
        $suggestion->save();

        notify($suggestion->users_id, 'approve_suggestion', $suggestionId, '', $authUserId);

        Notifications::where('data_id', $suggestion->id)
            ->where('type', 'plan_suggestion_added')
            ->delete();

        return $suggestion;
    }

    /**
     * @param int $suggestionId
     * @return TripsSuggestion
     * @throws AuthorizationException
     */
    public function disapproveSuggestion($suggestionId)
    {
        /** @var TripsSuggestion $suggestion */
        $suggestion = TripsSuggestion::find($suggestionId);

        if (!$suggestion) {
            throw new \Exception('Wrong suggestion id');
        }

        $authUserId = auth()->id();

        $plan = $suggestion->plan;
        if ($plan->users_id !== $authUserId) { // if login user is not trip creator
            if (($suggestion->users_id == $authUserId) != true) { // if suggestion created by login user then user can disapprove
                $invitedUserRequest = $this->getInvitedUserRequest($plan->id, $authUserId);

                if (!$invitedUserRequest || $invitedUserRequest->role !== 'admin') {
                    throw new AuthorizationException('Unauthorized', 403);
                }
            }
        }

        $suggestion->status = self::SUGGESTION_DISAPPROVED_STATUS;
        $suggestion->is_saved = 0;
        $suggestion->save();

        notify($suggestion->users_id, 'disapprove_suggestion', $suggestionId, '', $authUserId);

        return $suggestion;
    }

    /**
     * only discard by related suggestion editor
     * @param int $tripId
     * @param int $suggestionTripPlaceId
     * @return boolean
     * @throws AuthorizationException
     * @throws \Exception
     */
    public function discardSuggestedPlace($tripId, $suggestionTripPlaceId)
    {
        $authUserId = auth()->id();
        $query = TripsSuggestion::where([
            'status' => TripsSuggestionsService::SUGGESTION_PENDING_STATUS,
            'trips_id' => $tripId,
            'is_saved' => 0,
            'users_id' => $authUserId
        ]);

        // Check suggestionTripPlaceId is new trip place suggestion or not
        $isNewlyTripPlaceSuggestion = (clone $query)->whereNull('meta')->where('suggested_trips_places_id', $suggestionTripPlaceId)->exists();
        if ($isNewlyTripPlaceSuggestion) {
            return $this->dismissSuggestedPlace($suggestionTripPlaceId);
        } else {
            // fetch all suggestion id : from existing trip place suggestion
            $activeTripsPlacesIds = (clone $query)->where('active_trips_places_id', $suggestionTripPlaceId)->pluck('id');
            if ($activeTripsPlacesIds->count()) {
                $activeTripsPlacesIds->each(function ($suggestionId) {
                    $this->disapproveSuggestion($suggestionId);
                });
                return true;
            }
            return false;
        }
    }

    /**
     * @param int $planId
     * @param int $userId
     * @param bool $accepted
     * @return TripsContributionRequests
     */
    public function getInvitedUserRequest($planId, $userId, $accepted = true)
    {
        $query = TripsContributionRequests::where([
            'plans_id' => $planId,
            'users_id' => $userId
        ]);

        if ($accepted) {
            $query->where([
                'status' => TripInvitationsService::STATUS_ACCEPTED
            ]);
        }

        return $query->first();
    }

    /**
     * @param null $userId
     * @return mixed
     */
    public function getDraftPlans($userId = null)
    {
        if (!$userId) {
            $userId = auth()->id();
        }

        $unpublishedPlanIds = TripsSuggestion::where([
            'is_published' => 0,
            'status' => self::SUGGESTION_APPROVED_STATUS,
            'users_id' => $userId
        ])
            ->select('trips_id')
            ->pluck('trips_id');

        return $unpublishedPlanIds;
    }

    /**
     * @param TripPlans $plan
     * @param null $placeId
     * @return bool
     * @throws \Exception
     */
    public function publishPlan($plan, $placeId = null)
    {
        $this->saveChanges($plan);

        $placesIds = [];

        if ($placeId !== null) {
            $placesIds = $this->getAllSuggestionPackTripPlacesIds($placeId);
        }

        $suggestionsQuery = TripsSuggestion::where([
            'trips_id' => $plan->id,
            'is_saved' => 1,
            'is_published' => 0,
            'status' => self::SUGGESTION_APPROVED_STATUS
        ])->orderBy('created_at');

        if ($placesIds) {
            $suggestionsQuery->whereIn('suggested_trips_places_id', $placesIds);
        }

        $suggestions = $suggestionsQuery->get();

        if (!$suggestions->count()) {
            return false;
        }

        foreach ($suggestions as $suggestion) {
            /** @var TripPlaces $suggestedPlace */
            $suggestedPlace = $suggestion->suggestedPlace;

            /** @var TripPlaces $activePlace */
            $activePlace = $suggestion->activePlace;

            switch ($suggestion->type) {
                case self::SUGGESTION_CREATE_TYPE:
                    if (!$suggestedPlace) {
                        continue 2;
                    }

                    $suggestedPlace->versions_id = $plan->active_version;
                    $suggestedPlace->save();
                    $this->rankingService->addPointsEarners($suggestedPlace->id, get_class($suggestedPlace), $suggestion->users_id);
                    break;
                case self::SUGGESTION_EDIT_TYPE:
                    if (!$suggestedPlace) {
                        continue 2;
                    }

                    $suggestedPlace->versions_id = $plan->active_version;
                    $suggestedPlace->save();
                    $this->rankingService->addPointsEarners($suggestedPlace->id, get_class($suggestedPlace), $suggestion->users_id);

                    if ($activePlace) {
                        $activePlace->versions_id = 0;
                        $activePlace->save();
                    }
                    break;
                case self::SUGGESTION_DELETE_TYPE:
                    if ($activePlace) {
                        $activePlace->delete();
                    } elseif ($suggestedPlace) {
                        $suggestedPlace->delete();
                    }
                    break;
                case self::SUGGESTION_EDIT_PLAN_DATA_TYPE:
                    $meta = $suggestion->meta;
                    $plan->privacy = Arr::get($meta, 'privacy');
                    $plan->description = Arr::get($meta, 'description');
                    $plan->title = Arr::get($meta, 'name');
                    $plan->language_id = $this->translationService->getLanguageId($plan->title . ' ' . $plan->description);
                    $plan->save();
                    $this->rankingService->addPointsEarners($plan->id, get_class($plan), $suggestion->users_id);
                    break;
                default:
                    break;
            }
        }

        TripsSuggestion::query()->whereIn('id', $suggestions->pluck('id'))->update([
            'is_published' => 1
        ]);

        $plan->refresh();

        if ($plan->privacy === 0) {
            $userId = auth()->id();

            $tripPlaces = TripPlaces::query()
                ->where('trips_id', $plan->getKey())
                ->where('versions_id', $plan->active_version)
                ->where('date', '<', Carbon::now())
                ->get();

            foreach ($tripPlaces as $tripPlace) {
                if (!Visit::where('user_id', $userId)->where('place_id', $tripPlace->places_id)->exists()) {
                    $visit = new Visit();
                    $visit->place_id = $tripPlace->places_id;
                    $visit->user_id = $userId;
                    $visit->save();
                }
            }
        }

        $path = 'trips/' . $plan->id . '/thumbs/' . TripMapDrawingService::VERSION;

        Storage::disk('s3')->deleteDirectory($path);
        Cache::forget("NEWSFEED_TRIP_" . $plan->id);

        return $suggestions->count() > 0;
    }

    /**
     * @param int $planId
     * @return int
     */
    public function isUnsavedChanges($planId)
    {
        $count = TripsSuggestion::where([
            'trips_id' => $planId,
            'is_saved' => 0,
            //'status' => self::SUGGESTION_APPROVED_STATUS,
            'users_id' => auth()->id()
        ])->count();

        return $count;
    }

    /**
     * @param TripPlans $plan
     */
    public function saveChanges($plan)
    {
        TripsSuggestion::where([
            'trips_id' => $plan->id,
            'is_saved' => 0,
            'status' => self::SUGGESTION_APPROVED_STATUS
        ])->update([
            'is_saved' => 1
        ]);
    }

    /**
     * @param $plan
     * @return bool|int
     * @throws \Exception
     */
    public function undoChanges($plan)
    {
        $suggestionQuery = TripsSuggestion::where([
            'trips_id' => $plan->id,
            'is_saved' => 0,
            //'status' => self::SUGGESTION_APPROVED_STATUS,
            //'users_id' => auth()->id()
        ])->orderByDesc('created_at');

        $suggestion = $suggestionQuery->first();

        if ($suggestion) {
            $logs = PlanActivityLog::where('suggestion_id', $suggestion->getKey())->get();
            $suggestion->suggestionLogUsers()->delete();
            $suggestion->delete();

            foreach ($logs as $log) {
                $log->delete();

                $userLogs = PlanActivityLogUser::where('log_id', $log->getKey())->get();

                foreach ($userLogs as $userLog) {
                    $userLog->delete();
                    $updatedContent = $this->getPlanContents(true, $log->trips_id, $userLog->user_id)->render();
                    try {
                        broadcast(new PlanLogRemovedEvent($userLog->user_id, $log, $updatedContent));
                    } catch (\Throwable $e) {
                    }
                    try {
                        broadcast(new PlanLogRemovedApiEvent($userLog->user_id, $log, $updatedContent));
                    } catch (\Throwable $e) {
                    }
                }
            }

            if ($suggestion->suggested_trips_places_id) {
                $suggestedTripPlace = TripPlaces::find($suggestion->suggested_trips_places_id);

                if ($suggestedTripPlace && $suggestion->type !== self::SUGGESTION_DELETE_TYPE) {
                    $suggestedTripPlace->delete();
                }

                if ($suggestion->active_trips_places_id) {
                    TripsSuggestion::where('active_trips_places_id', $suggestion->suggested_trips_places_id)->update([
                        'active_trips_places_id' => $suggestion->active_trips_places_id
                    ]);
                } else {
                    TripsSuggestion::where('active_trips_places_id', $suggestion->suggested_trips_places_id)->delete();
                }
            }

            return $suggestionQuery->count();
        } else {
            return false;
        }
    }

    /**
     * @param TripPlans $plan
     * @throws \Exception
     */
    public function cancelChanges($plan)
    {
        $suggestions = TripsSuggestion::where([
            'trips_id' => $plan->id,
            'is_saved' => 0,
            'status' => self::SUGGESTION_APPROVED_STATUS
        ])->orderBy('updated_at')->get();

        foreach ($suggestions as $suggestion) {
            $suggestion->delete();
        }
    }


    /**
     * @param int $planId
     * @return TripsSuggestion[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getPlanSuggestions($planId)
    {
        $admins = $this->invitationsService->getPlanInvolvedPeople($planId);

        return TripsSuggestion::where(['trips_id' => $planId])
            ->where('type', '!=', TripsSuggestionsService::SUGGESTION_EDIT_PLAN_DATA_TYPE)
            ->where('status', '!=', TripsSuggestionsService::SUGGESTION_REMOVED_STATUS)
            ->where('is_published', 0)
            ->where(function ($query) use ($planId, $admins) {
                $query->where(function ($query) {
                    $query->where('is_saved', 1)
                        ->where('status', '=', TripsSuggestionsService::SUGGESTION_PENDING_STATUS)
                        ->where('users_id', '=', auth()->id());
                })
                    ->orWhere(function ($query) use ($planId, $admins) {
                        if (in_array(auth()->id(), $admins)) {
                            $query->where('is_saved', 1)
                                ->where('status', '=', TripsSuggestionsService::SUGGESTION_PENDING_STATUS);
                        }
                    })
                    ->orWhere('status', '=', TripsSuggestionsService::SUGGESTION_APPROVED_STATUS)
                    ->orWhere('users_id', '=', auth()->id());
            })
            ->with([
                'suggestedPlace',
                'suggestedPlace.medias',
                'suggestedPlace.place',
                'suggestedPlace.place.medias',
                'suggestedPlace.place.getMedias',
                'suggestedPlace.place.city',
                'suggestedPlace.place.city.trans',
                'suggestedPlace.place.city.country',
                'suggestedPlace.place.city.country.trans',
                'activePlace',
                'activePlace.place',
                'activePlace.place.medias',
                'activePlace.place.getMedias',
                'activePlace.place.city',
                'activePlace.place.city.trans',
                'activePlace.place.city.country',
                'activePlace.place.city.country.trans',
            ])
            ->get();
    }

    /**
     * @param int $tripPlaceId
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getPlaceSuggestions($tripPlaceId, $role)
    {
        $query = TripsSuggestion::query()
            ->where('active_trips_places_id', $tripPlaceId)
            ->where('type', '=', TripsSuggestionsService::SUGGESTION_EDIT_BY_EDITOR_TYPE);

        if ($role == TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN]) {
            $query->where(function ($q) {
                $q->where('is_saved', 1)
                    ->orWhere('status', '!=', self::SUGGESTION_PENDING_STATUS);
            });
        } else {
            $query->where('users_id', auth()->id());
        }

        return $query->orderByDesc('created_at')->get();
    }

    /**
     * @param int $planId
     * @param null $userId
     */
    public function sendSuggestions($planId, $userId = null)
    {
        if (!$userId) {
            $userId = auth()->id();
        }

        /** @var PlanActivityLogService $planActivityLogService */
        $planActivityLogService = app(PlanActivityLogService::class);

        $suggestions = TripsSuggestion::where([
            'status' => self::SUGGESTION_PENDING_STATUS,
            'trips_id' => $planId,
            'is_saved' => 0,
            'users_id' => $userId
        ])->get();

        $suggestionIds = [];

        /** @var TripsSuggestion $suggestion */
        foreach ($suggestions as $suggestion) {
            $suggestion->is_saved = 1;
            $suggestion->save();

            switch ($suggestion->type) {
                case self::SUGGESTION_CREATE_TYPE:
                    $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_SUGGESTED_NEW_PLACE, $planId, $suggestion->suggested_trips_places_id, $userId);
                    break;
                case self::SUGGESTION_EDIT_BY_EDITOR_TYPE:
                case self::SUGGESTION_EDIT_TYPE:
                    $meta = $planActivityLogService->prepareLogMeta($suggestion->suggestedPlace, $suggestion->activePlace);
                    $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_SUGGESTED_EDIT_PLACE, $planId, $suggestion->suggested_trips_places_id, $userId, $meta);
                    break;
                case self::SUGGESTION_DELETE_TYPE:
                    $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_SUGGESTED_REMOVAL_PLACE, $planId, $suggestion->suggested_trips_places_id, $userId);
                    break;
                default:
                    break;
            }

            $suggestionIds[] = $suggestion->id;
        }

        foreach ($this->invitationsService->getPlanInvolvedPeople($planId) as $involvedPerson) {
            foreach ($suggestionIds as $suggestionId) {
                $this->newSuggestionNotification($involvedPerson, $suggestionId);
            }
        }
    }

    /**
     * @param TripPlaces $place
     * @param int|null $tripPlaceId
     * @return bool
     * @throws \Exception
     */
    public function isPlaceOrTimeExistsInPlan(TripPlaces $place, $tripPlaceId = null)
    {
        $planId = $place->trips_id;
        $data = $this->getPlanContentsData(true, $planId, auth()->id());

        $planPlacesDays = $data['trip_places'];

        foreach ($planPlacesDays as $day => $planPlaces) {
            foreach ($planPlaces as $planPlaceArr) {
                $planPlace = $planPlaceArr['trip_place'];

                $samePoint = (string)$planPlace->time === (string)$place->time
                    && (int)$planPlace->id !== (int)$tripPlaceId
                    && $planPlace->without_time !== 1
                    && $place->without_time !== 1;

                if ($samePoint) {
                    return true;
                }
            }
        }

        return false;
    }

    public function isDeletedTripPlace($tripPlaceId)
    {
        $deletedPlaces = TripsSuggestion::query()
            ->where([
                'status' => self::SUGGESTION_APPROVED_STATUS,
                'type' => self::SUGGESTION_DELETE_TYPE,
                'suggested_trips_places_id' => $this->getAllSuggestionPackTripPlacesIds($tripPlaceId)
            ])
            ->pluck('suggested_trips_places_id')
            ->unique();

        return TripsSuggestion::query()->where([
            'status' => self::SUGGESTION_APPROVED_STATUS,
            'type' => self::SUGGESTION_DELETE_TYPE,
            'suggested_trips_places_id' => $tripPlaceId
        ])->whereIn('suggested_trips_places_id', $deletedPlaces)->exists();
    }

    /**
     * @param string $suggestionEditType
     * @param TripPlaces $place
     * @param array $values
     *
     * @param TripsSuggestion|null $suggestion
     * @return SuggestionEditResolverInterface
     */
    public function getSuggestionEditResolver($suggestionEditType, $place, $values = [], $suggestion = null)
    {
        $class = SuggestionEditResolver::class;

        switch ($suggestionEditType) {
            case self::SUGGESTION_EDIT_TYPE_BUDGET:
                $class = SuggestionEditBudgetResolver::class;
                break;
            case self::SUGGESTION_EDIT_TYPE_DURATION:
                $class = SuggestionEditDurationResolver::class;
                break;
            case self::SUGGESTION_EDIT_TYPE_MEDIA:
                $class = SuggestionEditMediaResolver::class;
                break;
            case self::SUGGESTION_EDIT_TYPE_DATE:
                $class = SuggestionEditDateResolver::class;
                break;
            case self::SUGGESTION_EDIT_TYPE_STORY:
                $class = SuggestionEditStoryResolver::class;
                break;
            case self::SUGGESTION_EDIT_TYPE_TAGS:
                $class = SuggestionEditTagsResolver::class;
                break;
            default:
                break;
        }

        return new $class($place, $values, $suggestion);
    }

    /**
     * @param int $planId
     * @param int $suggestionId
     */
    protected function newSuggestionNotification($involvedPerson, $suggestionId)
    {
        $not = new Notifications;
        $not->authors_id = auth()->id();
        $not->users_id = $involvedPerson;
        $not->type = 'plan_suggestion_added';
        $not->data_id = $suggestionId;
        $not->read = 0;
        $not->save();
    }

    /**
     * @param int $planId
     * @return bool
     */
    protected function isSuggestionAvailable($planId)
    {
        $count = TripsSuggestion::where([
            'trips_id' => $planId,
            'users_id' => auth()->id()
        ])
            ->whereIn('status', [
                self::SUGGESTION_DISAPPROVED_STATUS,
                self::SUGGESTION_REMOVED_STATUS
            ])->count();

        if ($count >= self::MAX_REJECTED_SUGGESTIONS_COUNT) {
            return false;
        }

        return true;
    }

    /**
     * @param bool $editable
     * @param int $trip_id
     * @param int $authUserId
     * @param bool $previewMode
     * @return array
     * @throws \Exception
     */
    public function getPlanContentsData($editable, $trip_id, $authUserId, $previewMode = false)
    {
        $t1 = microtime(true);
        $data = [];

        $data['editable'] = $editable;
        $data['preview_mode'] = $previewMode;

        if ($editable) {
            $data['do'] = 'edit';
        }

        if ($previewMode) {
            $data['do'] = 'preview';
        }

        $trip = TripPlans::findOrFail($trip_id);
        $my_version_id = $trip->versions[0]->id;
        $data['my_version_id'] = $my_version_id;
        $data['trip'] = $trip;

        $query = TripPlaces::where('trips_id', $trip_id)
            ->with([
                'place',
                'place.transsingle',
                'place.city',
                'place.city.trans',
                'place.city.country',
                'place.city.country.trans',
                'place.getMedias',
                'city',
                'city.trans',
                'city.country',
                'city.country.trans',
                'medias',
                'trip'
            ])
            ->where('versions_id', $my_version_id)
            ->whereHas('place')
            ->orderBy('order', 'ASC');

        if ($editable) {
            $query->withTrashed();
        }

        $trip_places = $query->get();
        $t2 = microtime(true);
        $ftrip_places = [];

        $invitedUserRequest = $this->getInvitedUserRequest($trip_id, $authUserId, false);

        $data['is_invited'] = $invitedUserRequest && $invitedUserRequest->status === TripInvitationsService::STATUS_PENDING;

        $data['invite_details'] = [];
        if ($data['is_invited']) {
            $data['invite_details']['invite_role'] = $invitedUserRequest->role;
            $data['invite_details']['invite_id'] = $invitedUserRequest->id;
            $data['invite_details']['inviter'] = $invitedUserRequest->author;
        }

        $invitedUserRequest = $invitedUserRequest && $invitedUserRequest->status === TripInvitationsService::STATUS_ACCEPTED ? $invitedUserRequest : null;

        $data['is_admin'] = $trip->users_id === $authUserId || ($invitedUserRequest && $invitedUserRequest->role === 'admin');
        $data['plan_member'] = $trip->users_id === $authUserId || $invitedUserRequest ? true : false;




        $overallBudget = [];
        $isEdited = [];
        $trip_places_arr = [];
        $map_places = [];

        $notApprovedSuggestions = [];
        $t3 = microtime(true);
        //\DB::enableQueryLog();
        $tripPlaceMedia = TripMedias::query()->where([
            'trips_id' => $trip_id
        ])
            ->whereIn('trip_place_id', $trip_places->pluck('id'))
            ->with('media', 'media.likes', 'media.comments', 'media.users')
            ->get()
            ->unique('medias_id')
            ->groupBy('trip_place_id');
        $t31 = microtime(true);
        $reviews = Reviews::whereIn('places_id', $trip_places->pluck('places_id'))->get()->groupBy('places_id');
        $t32 = microtime(true);
        foreach ($trip_places as $tp) {
            if (!$tp->place or !$tp->place->city) {
                continue;
            }

            $tp->published = 1;

            $ftrip_places[$tp->date][$tp->id] = [
                'trip_place' => $tp,
                'suggestion' => null,
                'is_expert'  => 0,
                'role'       => ''
            ];

            if ($tp->deleted_at) {
                continue;
            }

            if (!isset($tripPlaceMedia[$tp->id])) {
                $tripPlaceMedia[$tp->id] = collect();
            }

            $overallBudget[$tp->id] = $tp->budget;

            $tripPlaceId = $this->getRootTripPlace($tp->id);

            $tpReviews = isset($reviews[$tp->places_id]) ? $reviews[$tp->places_id] : collect();

            $trip_places_arr[$tripPlaceId] = $tp->place;
            $trip_places_arr[$tripPlaceId]['image'] = check_place_photo($tp->place);
            $trip_places_arr[$tripPlaceId]['thumb'] = check_place_photo($tp->place, true);
            $trip_places_arr[$tripPlaceId]['time'] = $tp->time;
            $trip_places_arr[$tripPlaceId]['trip_place_id'] = $tp->id;
            $trip_places_arr[$tripPlaceId]['name'] = @$tp->place->transsingle->title;
            $trip_places_arr[$tripPlaceId]['city_name'] = (@$tp->place->city->trans) ? @$tp->place->city->trans->first()->title : '';
            $trip_places_arr[$tripPlaceId]['reviews'] = $tpReviews;
            $trip_places_arr[$tripPlaceId]['reviews_avg'] = $tpReviews->avg('score');
            $trip_places_arr[$tripPlaceId]['status'] = 1;
            $trip_places_arr[$tripPlaceId] = clone $trip_places_arr[$tripPlaceId];

            $map_places[@$tp->city->id . '-place-' . @$tripPlaceId] = array('lat' => @$tp->place->lat, 'lng' => @$tp->place->lng);
            $map_places[@$tp->city->id . '-place-' . @$tripPlaceId]['time'] = $tp->time;
        }

        //dd(\DB::getQueryLog());
        $t4 = microtime(true);
        if (Auth::check() && ($editable || $previewMode)) {
            $planSuggestions = $this->getEditPlanSuggestions($trip_id);

            foreach ($planSuggestions as $planSuggestion) {
                $meta = $planSuggestion->meta;
                $data['trip']->privacy = Arr::get($meta, 'privacy');
                $data['trip']->description = Arr::get($meta, 'description');
                $data['trip']->title = Arr::get($meta, 'name');
                $data['trip']->cover = Arr::get($meta, 'cover', $data['trip']->cover);
            }

            $tripSuggestions = $this->getPlanSuggestions($trip_id);

            /** @var TripsSuggestion $tripSuggestion */
            foreach ($tripSuggestions as $tripSuggestion) {
                $suggestedPlace = $tripSuggestion->suggestedPlace;
                $activePlace = $tripSuggestion->activePlace;

                $invitedUserRequest = $this->getInvitedUserRequest($trip_id, $tripSuggestion->users_id);

                $role = $invitedUserRequest ? $invitedUserRequest->role : 'admin';
                $tripSuggestion->role = $role;

                $suggestionData = [
                    'suggestion' => $tripSuggestion,
                    'is_expert'  => 0,
                    'role'       => $role
                ];

                if ($tripSuggestion->status === TripsSuggestionsService::SUGGESTION_APPROVED_STATUS) {
                    switch ($tripSuggestion->type) {
                        case TripsSuggestionsService::SUGGESTION_CREATE_TYPE:
                            if (!$suggestedPlace || !$suggestedPlace->place || !$suggestedPlace->place->city) {
                                continue 2;
                            }

                            $suggestionData['trip_place'] = $suggestedPlace;
                            $ftrip_places[$suggestedPlace->date][$suggestedPlace->id] = $suggestionData;
                            $tripPlaceMedia[$suggestedPlace->id] = $suggestedPlace->medias;
                            $overallBudget[$suggestedPlace->id] = $suggestedPlace->budget;

                            $tripPlaceId = $this->getRootTripPlace($suggestedPlace->id);

                            $reviews = Reviews::where('places_id', $suggestedPlace->place->id)->get();

                            $trip_places_arr[$tripPlaceId] = $suggestedPlace->place;
                            $trip_places_arr[$tripPlaceId]['image'] = check_place_photo($suggestedPlace->place);
                            $trip_places_arr[$tripPlaceId]['thumb'] = check_place_photo($suggestedPlace->place, true);
                            $trip_places_arr[$tripPlaceId]['time'] = $suggestedPlace->time;
                            $trip_places_arr[$tripPlaceId]['without_time'] = $suggestedPlace->without_time;
                            $trip_places_arr[$tripPlaceId]['trip_place_id'] = $suggestedPlace->id;
                            $trip_places_arr[$tripPlaceId]['name'] = $suggestedPlace->place->transsingle->title;
                            $trip_places_arr[$tripPlaceId]['city_name'] = $suggestedPlace->place->city->trans->first()->title;
                            $trip_places_arr[$tripPlaceId]['reviews'] = $reviews;
                            $trip_places_arr[$tripPlaceId]['reviews_avg'] = $reviews->avg('score');
                            $trip_places_arr[$tripPlaceId]['status'] = $tripSuggestion->status;
                            $trip_places_arr[$tripPlaceId] = clone $trip_places_arr[$tripPlaceId];

                            $map_places[@$suggestedPlace->city->id . '-place-' . @$tripPlaceId] = array('lat' => @$suggestedPlace->place->lat, 'lng' => @$suggestedPlace->place->lng);
                            $map_places[@$suggestedPlace->city->id . '-place-' . @$tripPlaceId]['time'] = $suggestedPlace->time;

                            break;
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE:
                            if (!$suggestedPlace || !$activePlace || !isset($tripPlaceMedia[$activePlace->id]) || !$suggestedPlace->place || !$suggestedPlace->place->city || !$activePlace->place->city) {
                                continue 2;
                            }

                            unset($ftrip_places[$activePlace->date][$activePlace->id]);

                            if (isset($ftrip_places[$activePlace->date]) && count($ftrip_places[$activePlace->date]) === 0) {
                                unset($ftrip_places[$activePlace->date]);
                            }

                            $suggestionData['trip_place'] = $suggestedPlace;
                            $ftrip_places[$suggestedPlace->date][$suggestedPlace->id] = $suggestionData;
                            $tripPlaceMedia[$suggestedPlace->id] = $suggestedPlace->medias;
                            $overallBudget[$suggestedPlace->id] = $suggestedPlace->budget;
                            $isEdited[$suggestedPlace->id] = 1;

                            $tripPlaceId = $this->getRootTripPlace($suggestedPlace->id);

                            $reviews = Reviews::where('places_id', $suggestedPlace->place->id)->get();

                            $trip_places_arr[$tripPlaceId] = $suggestedPlace->place;
                            $trip_places_arr[$tripPlaceId]['image'] = check_place_photo($suggestedPlace->place);
                            $trip_places_arr[$tripPlaceId]['thumb'] = check_place_photo($suggestedPlace->place, true);
                            $trip_places_arr[$tripPlaceId]['time'] = $suggestedPlace->time;
                            $trip_places_arr[$tripPlaceId]['without_time'] = $suggestedPlace->without_time;
                            $trip_places_arr[$tripPlaceId]['trip_place_id'] = $suggestedPlace->id;
                            $trip_places_arr[$tripPlaceId]['name'] = $suggestedPlace->place->transsingle->title;
                            $trip_places_arr[$tripPlaceId]['city_name'] = $suggestedPlace->place->city->trans->first()->title;
                            $trip_places_arr[$tripPlaceId]['reviews'] = $reviews;
                            $trip_places_arr[$tripPlaceId]['reviews_avg'] = $reviews->avg('score');
                            $trip_places_arr[$tripPlaceId]['status'] = $tripSuggestion->status;
                            $trip_places_arr[$tripPlaceId] = clone $trip_places_arr[$tripPlaceId];

                            $map_places[@$activePlace->city->id . '-place-' . @$tripPlaceId] = array('lat' => @$suggestedPlace->place->lat, 'lng' => @$suggestedPlace->place->lng);
                            $map_places[@$activePlace->city->id . '-place-' . @$tripPlaceId]['time'] = $suggestedPlace->time;
                            break;
                        case TripsSuggestionsService::SUGGESTION_DELETE_TYPE:
                            if (!$activePlace || !$activePlace->place->city || !$suggestedPlace->place->city) {
                                continue 2;
                            }

                            $suggestionData['trip_place'] = $activePlace;
                            $ftrip_places[$activePlace->date][$activePlace->id] = $suggestionData;

                            if ($tripSuggestion->users_id === $authUserId || $tripSuggestion->meta['deleted'] === true) {
                                unset($ftrip_places[$activePlace->date][$activePlace->id]);

                                if (count($ftrip_places[$activePlace->date]) === 0) {
                                    unset($ftrip_places[$activePlace->date]);
                                }
                            }

                            unset($overallBudget[$suggestedPlace->id]);

                            $tripPlaceId = $this->getRootTripPlace($suggestedPlace->id);

                            if (isset($trip_places_arr[$tripPlaceId])) {
                                unset($trip_places_arr[$tripPlaceId]);
                                unset($map_places[$suggestedPlace->city->id . '-place-' . @$tripPlaceId]);
                            }

                            break;
                        default:
                            break;
                    }

                    continue;
                }

                switch ($tripSuggestion->type) {
                    case TripsSuggestionsService::SUGGESTION_CREATE_TYPE:
                        if (!$suggestedPlace || !$suggestedPlace->place || !$suggestedPlace->place->city) {
                            continue 2;
                        }

                        $suggestionData['trip_place'] = $suggestedPlace;
                        $ftrip_places[$suggestedPlace->date][$suggestedPlace->id] = $suggestionData;

                        if ($tripSuggestion->status === self::SUGGESTION_DISAPPROVED_STATUS) {
                            continue 2;
                        }

                        $tripPlaceMedia[$suggestedPlace->id] = $suggestedPlace->medias;
                        $overallBudget[$suggestedPlace->id] = $suggestedPlace->budget;

                        $tripPlaceId = $this->getRootTripPlace($suggestedPlace->id);

                        $reviews = Reviews::where('places_id', $suggestedPlace->place->id)->get();

                        $trip_places_arr[$tripPlaceId] = $suggestedPlace->place;
                        $trip_places_arr[$tripPlaceId]['image'] = check_place_photo($suggestedPlace->place);
                        $trip_places_arr[$tripPlaceId]['thumb'] = check_place_photo($suggestedPlace->place, true);
                        $trip_places_arr[$tripPlaceId]['time'] = $suggestedPlace->time;
                        $trip_places_arr[$tripPlaceId]['without_time'] = $suggestedPlace->without_time;
                        $trip_places_arr[$tripPlaceId]['trip_place_id'] = $suggestedPlace->id;
                        $trip_places_arr[$tripPlaceId]['name'] = $suggestedPlace->place->transsingle->title;
                        $trip_places_arr[$tripPlaceId]['city_name'] = $suggestedPlace->place->city->trans->first()->title;
                        $trip_places_arr[$tripPlaceId]['reviews'] = $reviews;
                        $trip_places_arr[$tripPlaceId]['reviews_avg'] = $reviews->avg('score');
                        $trip_places_arr[$tripPlaceId]['status'] = $tripSuggestion->status;
                        $trip_places_arr[$tripPlaceId] = clone $trip_places_arr[$tripPlaceId];
                        $map_places[@$suggestedPlace->city->id . '-place-' . @$tripPlaceId] = array('lat' => @$suggestedPlace->place->lat, 'lng' => @$suggestedPlace->place->lng);
                        $map_places[@$suggestedPlace->city->id . '-place-' . @$tripPlaceId]['time'] = $suggestedPlace->time;
                        break;
                    case TripsSuggestionsService::SUGGESTION_EDIT_TYPE:
                    case TripsSuggestionsService::SUGGESTION_EDIT_BY_EDITOR_TYPE:
                        if (!$activePlace) {
                            continue 2;
                        }

                        switch ($tripSuggestion->status) {
                            case self::SUGGESTION_PENDING_STATUS:
                                $tripPlaceId = $this->getLastTripPlace($activePlace->id, true);
                                if (!$this->isDeletedTripPlace($tripPlaceId)) {
                                    $notApprovedSuggestions[$tripPlaceId][$tripSuggestion->users_id] = $tripSuggestion->user;
                                }

                                break;
                            default:
                                break;
                        }

                        break;
                    case TripsSuggestionsService::SUGGESTION_DELETE_TYPE:
                        if (!$activePlace) {
                            continue 2;
                        }

                        $tripPlaceId = $this->getRootTripPlace($suggestedPlace->id);

                        $suggestionData['trip_place'] = $activePlace;
                        $ftrip_places[$activePlace->date][$activePlace->id] = $suggestionData;

                        unset($overallBudget[$suggestedPlace->id]);

                        if (isset($trip_places_arr[$tripPlaceId])) {
                            unset($trip_places_arr[$tripPlaceId]);
                            unset($map_places[$suggestedPlace->city->id . '-place-' . @$tripPlaceId]);
                        }

                        break;
                    default:
                        break;
                }
            }
        }
        $t5 = microtime(true);
        $trip_places_arr = collect(array_values($trip_places_arr))->sortBy('time')->values()->all();
        $map_places = collect($map_places)->sortBy('time')->toArray();

        $data['trip_places_arr'] = $trip_places_arr;
        $data['trip_arr'] = $map_places;

        $tripSuggestions = TripsSuggestion::where(['trips_id' => $trip_id])
            ->where('status', '!=', TripsSuggestionsService::SUGGESTION_DISAPPROVED_STATUS)
            ->where('status', '!=', TripsSuggestionsService::SUGGESTION_REMOVED_STATUS)
            ->where('type', '!=', TripsSuggestionsService::SUGGESTION_EDIT_PLAN_DATA_TYPE)
            ->with(['user', 'suggestedPlace'])
            ->get();

        $invitedPeopleIds = array_keys($this->invitationsService->getInvitedPeopleIds($trip_id));

        /** @var TripsSuggestion $tripSuggestion */
        foreach ($tripSuggestions as $tripSuggestion) {
            $suggestedPlace = $tripSuggestion->suggestedPlace;
            $isExpert = $tripSuggestion->user->expert;

            if (
                $suggestedPlace &&
                isset($ftrip_places[$suggestedPlace->date][$suggestedPlace->id]) &&
                ($tripSuggestion->type === TripsSuggestionsService::SUGGESTION_EDIT_TYPE || $tripSuggestion->type === TripsSuggestionsService::SUGGESTION_CREATE_TYPE) &&
                ($editable || $tripSuggestion->is_published) &&
                in_array($tripSuggestion->users_id, $invitedPeopleIds)
            ) {
                $ftrip_places[$suggestedPlace->date][$suggestedPlace->id]['is_expert'] = $isExpert;
            }
        }
        $t6 = microtime(true);
        ksort($ftrip_places);

        foreach ($ftrip_places as $date => $places) {
            foreach ($places as $key => &$place) {
                $pack = $this->getAllSuggestionPackTripPlacesIds($place['trip_place']->id);

                $commentsQuery = TripPlacesComments::query()->whereIn('trip_place_id', $pack);
                $place['trip_place']['comments_count'] = $commentsQuery->count();
                $place['trip_place']['likes'] = TripsLikes::query()->whereIn('places_id', $pack)->get();

                if ($place['suggestion']) {
                    $toDismiss = $place['suggestion'] && ($place['suggestion']->status === \App\Services\Trips\TripsSuggestionsService::SUGGESTION_DISAPPROVED_STATUS || $place['suggestion']->type === \App\Services\Trips\TripsSuggestionsService::SUGGESTION_DELETE_TYPE);
                } else {
                    $toDismiss = (bool)$place['trip_place']['deleted_at'];

                    if ($toDismiss) {
                        $place['suggestion'] = TripsSuggestion::where([
                            'status' => self::SUGGESTION_APPROVED_STATUS,
                            'type' => self::SUGGESTION_DELETE_TYPE,
                            'active_trips_places_id' => $place['trip_place']->id,
                            'trips_id' => $trip_id
                        ])->first();
                    }
                }

                $place['trip_place']['to_dismiss'] = $toDismiss;

                if ($toDismiss && (DismissedTripPlaces::where(['user_id' => $authUserId, 'trip_place_id' => $place['trip_place']->id])->first() || !$place['suggestion'])) {
                    unset($places[$key]);
                }
            }

            $places = collect($places);
            $places = $places->sortBy('trip_place.order');

            $ftrip_places[$date] = $places->toArray();

            if (empty($ftrip_places[$date])) {
                unset($ftrip_places[$date]);
            }
        }

        $t7 = microtime(true);
        $data['trip_places'] = $ftrip_places;

        $overallBudgetSum = 0;

        foreach ($overallBudget as $overallBudgetPlace) {
            $overallBudgetSum += $overallBudgetPlace;
        }

        if ($overallBudgetSum > 1000) {
            $overallBudgetSum = $overallBudgetSum / 1000 . 'K';
        }


        $data['overall_budget'] = $overallBudgetSum;
        $data['unread_logs_count'] = $this->planActivityLogService->getPlanActivityLogUnreadCount($trip_id);
        $data['updated_places'] = $this->planActivityLogService->getPlacesPlanActivityLogUnread($trip_id);
        $data['authUserId'] = $authUserId;
        $data['trip_place_medias'] = $tripPlaceMedia;
        $data['is_edited'] = $isEdited;

        $placesId = collect($data['trip_places_arr'])->pluck('trip_place_id');

        $data['destinations'] = $placesId->count();

        $distanceCount = 0;
        $beforePlace = null;

        foreach (TripPlaces::query()->whereIn('id', $placesId)->with('place')->get() as $distancePlace) {
            if ($beforePlace && $beforePlace->place && $beforePlace->place->id !== $distancePlace->place->id) {
                $distanceCount += haversineGreatCircleDistance($beforePlace->place->lat, $beforePlace->place->lng, $distancePlace->place->lat, $distancePlace->place->lng);
            }

            $beforePlace = $distancePlace;
        }

        $data['distance'] = round($distanceCount / 1000);
        $data['budget'] = TripPlaces::query()->whereIn('id', $placesId)->sum('budget');

        $data['duration'] = 0;

        if (isset($data['trip_places']) && is_array($data['trip_places']) && count($data['trip_places']) > 0) {
            $data['duration'] = count($data['trip_places']);
        }

        foreach ($trip_places_arr as &$tripPlace) {
            $tripPlace->checkins = [];
            $tripPlace->visits = [];
            $tripPlace->place_map_preview = get_plan_map($trip, 613, 378, 2, $tripPlace->trip_place_id, null, $tripPlace);
        }

        $data['not_approved_suggestions'] = $notApprovedSuggestions;
        $data['plan_map_preview'] = get_plan_map($trip, 613, 378, 2);
        $data['approved_suggestions'] = TripsSuggestion::query()
            ->where([
                'users_id' => auth()->id(),
                'trips_id' => $trip_id,
                'status' => self::SUGGESTION_APPROVED_STATUS
            ])
            ->where('type', '!=', self::SUGGESTION_EDIT_BY_EDITOR_TYPE)
            ->with('suggestedPlace', 'suggestedPlace.place', 'user')
            ->whereHas('suggestedPlace.place')
            ->get();

        foreach ($data['approved_suggestions'] as &$approvedSuggestion) {
            if (!$approvedSuggestion->suggestedPlace) {
                continue;
            }
            $approvedSuggestion->place_img = check_place_photo($approvedSuggestion->suggestedPlace->place, true);
            $approvedSuggestion->name = $approvedSuggestion->suggestedPlace->place->transsingle->title;
        }

        $data['send_suggestions_available'] = self::isNotSendingSuggestions($trip_id);
        $data['is_undo'] = $this->isUnsavedChanges($trip_id);
        $data['is_editor'] = in_array($this->invitationsService->getUserRole($trip_id, $authUserId), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
        ]);

        if ($authUserId) {
            $data['auth_picture'] = check_profile_picture(auth()->user()->profile_picture);
        }
        $t8 = microtime(true);

        $debugTiming = [
            'time1' => $t2 - $t1,
            'time2' => $t3 - $t2,
            'time3' => $t31 - $t3,
            'time31' => $t32 - $t1,
            'time32' => $t4 - $t32,
            'time4' => $t5 - $t4,
            'time5' => $t6 - $t5,
            'time6' => $t7 - $t6,
            'time7' => $t8 - $t7,
            'all_time' => $t8 - $t1,
        ];
        //dd($debugTiming);
        $data['debug'] = $debugTiming;

        return $data;
    }

    /**
     * don't delete because now using in api
     * @throws \Exception
     */
    public function getApiTripPlace($userId, $trip_id, $trip_place_id)
    {
        $new_trip_place  = TripPlaces::with([
            'medias',
            'medias.media',
            'place',
            'place.transsingle',
            'place.city',
            'place.city.trans',
            'place.city.country',
            'place.city.country.trans',
            'place.getMedias',
            'city',
            'city.trans',
            'city.country',
            'city.country.trans',
        ])->whereKey($trip_place_id)->first();

        if (!$new_trip_place) {
            throw new \Exception('trip place not found');
        }

        $trip = TripPlans::findOrFail($trip_id);

        // format need by FE
        $new_trip_place->tags = $new_trip_place->comment;
        $pack = $this->getAllSuggestionPackTripPlacesIds($new_trip_place->id);

        $commentsQuery = TripPlacesComments::query()->whereIn('trip_place_id', $pack);
        $new_trip_place->comments_count = $commentsQuery->count();
        $new_trip_place->likes = TripsLikes::query()->whereIn('places_id', $pack)->get();

        $users_suggestions_ids = TripsSuggestion::where([
            'status' => TripsSuggestionsService::SUGGESTION_PENDING_STATUS,
            'trips_id' => $trip_id,
            'is_saved' => 0,
        ])->groupBy('active_trips_places_id')->pluck('users_id')->toArray();
        $new_trip_place->users_suggestions = count($users_suggestions_ids) ? User::whereIn('id', $users_suggestions_ids)->get() : [];

        $to_place                   = $new_trip_place['place'];
        $to_place['title']          = isset($to_place['transsingle']['title']) ? $to_place['transsingle']['title'] : null;
        $to_place['description']    = isset($to_place['transsingle']['description']) ? $to_place['transsingle']['description'] : null;
        $to_place['address']        = isset($to_place['transsingle']['address']) ? $to_place['transsingle']['address'] : null;
        $to_place['rating']         = $to_place['rating'] ? $to_place['rating'] : 0;
        $to_place['name']           = $to_place['title'];
        $to_place['image'] = check_place_photo($new_trip_place['place']);

        $to_place_city                      = $new_trip_place['place']['city'];
        $to_place_city['image']             = (isset($to_place_city->medias[0]->media->path)) ? $to_place_city->medias[0]->media->path : url(PATTERN_PLACEHOLDERS);
        $to_place_city['title']             = isset($to_place_city['trans'][0]->title) ? $to_place_city['trans'][0]->title : null;
        $to_place_city['description']       = isset($to_place_city['trans'][0]->description) ? $to_place_city['trans'][0]->description : null;
        $to_place_city['languages_id']      = isset($to_place_city['trans'][0]->languages_id) ? $to_place_city['trans'][0]->languages_id : null;

        $to_country                    = $new_trip_place['place']['city']->country;
        $to_country['image']           = (isset($to_country->medias[0]->media->path)) ? $to_country->medias[0]->media->path : url(PATTERN_PLACEHOLDERS);
        $to_country['title']           = isset($to_country->trans[0]->title) ? $to_country->trans[0]->title : null;
        $to_country['description']     = isset($to_country->trans[0]->description) ? $to_country->trans[0]->description : null;
        $to_country['languages_id']    = isset($to_country->trans[0]->languages_id) ? $to_country->trans[0]->languages_id : null;
        $to_country['flag']            = get_country_flag($to_country);
        $noOfFlags[] = $to_country['flag'];

        $to_city                    = $new_trip_place['city'];
        $to_city['image']           = (isset($to_city->medias[0]->media->path)) ? $to_city->medias[0]->media->path : url(PATTERN_PLACEHOLDERS);
        $to_city['title']           = isset($to_city->trans[0]->title) ? $to_city->trans[0]->title : null;
        $to_city['description']     = isset($to_city->trans[0]->description) ? $to_city->trans[0]->description : null;
        $to_city['languages_id']    = isset($to_city->trans[0]->languages_id) ? $to_city->trans[0]->languages_id : null;

        $toCountry                  = $new_trip_place['city']->country;
        $toCountry['image']         = (isset($toCountry->medias[0]->media->path)) ? $toCountry->medias[0]->media->path : url(PATTERN_PLACEHOLDERS);
        $toCountry['title']         = isset($toCountry->trans[0]->title) ? $toCountry->trans[0]->title : null;
        $toCountry['description']   = isset($toCountry->trans[0]->description) ? $toCountry->trans[0]->description : null;
        $toCountry['languages_id']  = isset($toCountry->trans[0]->languages_id) ? $toCountry->trans[0]->languages_id : null;
        $toCountry['flag']          = get_country_flag($toCountry);

        unset(
            $new_trip_place['place']['medias'],
            $new_trip_place['place']['transsingle'],
            $new_trip_place['place']['city']['trans'],
            $new_trip_place['city']['trans'],
            $new_trip_place['city']->country['trans'],
            $new_trip_place['place']['city']->country['trans'],
            $new_trip_place['city']->country->medias,
            $new_trip_place['place']['city']->country->medias,
            $new_trip_place['place']['city']->medias,
            $new_trip_place['city']->medias
        );

        $invitedUserRequest = $this->getInvitedUserRequest($trip_id, $userId, true);
        $isMyTrip = $userId == $trip->users_id;

        return [
            'date'      => $new_trip_place->date,
            'ob' => [
                "is_expert" => $isMyTrip ? 0 : (($invitedUserRequest && $invitedUserRequest->author) ? $invitedUserRequest->author->expert : 0),
                "role" => $isMyTrip ? 'admin' : ($invitedUserRequest ? $invitedUserRequest->role : ''),
                "suggestion" => TripsSuggestion::with(['user', 'activePlace'])->where(['suggested_trips_places_id' => $new_trip_place->id, 'trips_id' => $new_trip_place->trips_id])->first(),
                "trip_place" => $new_trip_place,
            ]
        ];
    }
}
