<?php
namespace App\Services\Medias;

use App\Models\ActivityMedia\Media;
use App\Models\User\User;

class MediaService
{
    public function getPhotos($userId, $sortBy, $offset, $limit)
    {
        return $this->getMedias($userId, $sortBy, $offset, $limit, Media::TYPE_IMAGE, 'photos');
    }

    public function getVideos($userId, $sortBy, $offset, $limit)
    {
        return $this->getMedias($userId, $sortBy, $offset, $limit, Media::TYPE_VIDEO, 'videos');
    }

    public function getMedias($userId, $sortBy, $offset, $limit, $type = null, $typeName = 'medias')
    {
        $user = User::findOrFail($userId);
        $userMediasIds = $user->my_medias()->pluck('medias_id');
        $userMediasQuery = Media::query()
            ->select('id', 'url', 'title', 'type', 'video_thumbnail_url')
            ->whereIn('id', $userMediasIds);

        if ($type) {
            $userMediasQuery->where('type', $type);
        }
        $data['count'] = (clone $userMediasQuery)->count();

        switch ($sortBy) {
            case 'date_up':
                $userMediasQuery->orderBy('id', 'asc');
                break;
            case 'most_liked':
                $userMediasQuery->withCount('likes')->orderBy('likes_count', 'desc');
                break;
            case 'most_commented':
                $userMediasQuery->withCount('comments')->orderBy('comments_count', 'desc');
                break;
            default:
                $userMediasQuery->orderBy('id', 'desc');
        }
        $data[$typeName] = $userMediasQuery->skip($offset)->take($limit)->get()->map(function($media) {
            return [
                'id' => $media->id,
                'type' => $media->type == Media::TYPE_IMAGE ? 'image' : 'video',
                'title' => $media->title,
                'url' => $media->url,
                'thumbnail' => $media->type == Media::TYPE_VIDEO ? $media->video_thumbnail_url : null
            ];
        });
        return $data;
    }
}