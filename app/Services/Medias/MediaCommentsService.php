<?php

namespace App\Services\Medias;

use App\Models\TripMedias\TripMedias;
use Illuminate\Support\Facades\Auth;

class MediaCommentsService
{
    public function renderComment($comment)
    {
        return view('site.trip2.partials.trip_place_media_comment_block', [
            'comment' => $comment,
            'authUserId' => Auth::id(),
            'photo' => TripMedias::query()->where('medias_id', $comment->medias_id)->first()
        ]);
    }
}