<?php

namespace App\Services\Medias;

use App\Models\ActivityMedia\Media;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Country\CountriesMedias;
use App\Models\City\CitiesMedias;


class LocationsAddMediaService
{

    /**
     * Add country medias
     *
     * @param Model $model
     * @return bool
     */
    public function addCountryMedia($model){
        if($model->transsingle && count($model->getMedias)<5){

                $place_response = $this->getPhotos($model->transsingle->title);

                if($place_response->result && @$place_response->result->photos && count(@$place_response->result->photos) > 0){
                       $this->saveMedia(@$place_response->result->photos, $model, new CountriesMedias(), 'countries', 'countries_id');
                }
        }
    }

    /**
     * Add city medias
     *
     * @param Model $model
     * @return bool
     */

    public function addCityMedia($model){
        if($model->transsingle && count($model->getMedias)<5){

            $place_response = $this->getPhotos($model->transsingle->title. ' in ' . @$model->country->transsingle->title);

            if($place_response && @$place_response->result && @$place_response->result->photos && count(@$place_response->result->photos) > 0){
                $this->saveMedia(@$place_response->result->photos, $model, new CitiesMedias(), 'cities', 'cities_id');
            }
        }
    }

    /**
     * Get google place photos
     *
     * @param string $address
     */
    protected function getPhotos($address) {
        $geo_params = [
            'key' => env('GOOGLE_PLACES_KEY'),
            'address' => $address,
        ];
        $client = new Client();

        $geo_response = json_decode($client->get('https://maps.googleapis.com/maps/api/geocode/json', [
            'query' => $geo_params
        ])->getBody());

        if ($geo_response->results && $geo_response->results[0]) {
            $google_place_id = $geo_response->results[0]->place_id;

            $place_params = [
                'key' => env('GOOGLE_PLACES_KEY'),
                'placeid' => $google_place_id,
            ];

            return  json_decode($client->get('https://maps.googleapis.com/maps/api/place/details/json', [
                                'query' => $place_params
                            ])->getBody());


        }else{
            return false;
        }

    }

    /**
     * Save medias
     *
     * @param array $photos
     * @param Model $location
     * @param Model $location_media
     * @param string $location_type
     * @param null $column
     * @return bool
     */

    protected function saveMedia($photos, Model $location, Model $location_media, $location_type, $column = null){
        foreach($photos as $photo){
            $media =  @file_get_contents('https://maps.googleapis.com/maps/api/place/photo?maxwidth=2500&photoreference='. $photo->photo_reference .'&sensor=true&key='.env('GOOGLE_PLACES_KEY'));;

            $filename = sha1(microtime()) . "_place_added_file.png";
            $img_1100 = Image::make($media)->resize(1100, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img_180 = Image::make($media)->resize(180, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $media_url = $location_type .'/'. $location->id .'/' . $filename;

            Storage::disk('s3')->put(
                $media_url,
                $media,
                'public'
            );

            Storage::disk('s3')->put(
                'th1100/'. $media_url,
                $img_1100->encode(),
                'public'
            );

            Storage::disk('s3')->put(
                'th180/'. $media_url,
                $img_180->encode(),
                'public'
            );


            $media = Media::create([
                'url' => $media_url,
                'uploaded_at' => date('Y-m-d H:i:s')
            ]);

            $location_media::create([
                $column => $location->id,
                'medias_id' => $media->id
            ]);
        }
    }
}
