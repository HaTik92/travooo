<?php

namespace App\Services\Checkins;

use App\Models\Posts\Checkins;
use App\Services\Trips\TripInvitationsService;
use Illuminate\Support\Collection;

class CheckinsService
{
    /**
     * @var TripInvitationsService
     */
    private $tripInvitationsService;

    /**
     * CheckinsService constructor.
     * @param TripInvitationsService $tripInvitationsService
     */
    public function __construct(TripInvitationsService $tripInvitationsService)
    {
        $this->tripInvitationsService = $tripInvitationsService;
    }

    /**
     * @param array|Collection $placesIds
     * @param array $exceptPlacesIds
     * @param null $type
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function getFriendsAndFollowingsCheckins($placesIds, $exceptPlacesIds = [], $type = null)
    {
        $friends = $this->tripInvitationsService->findFriends();
        $followings = $this->tripInvitationsService->findFollowings();
        $followers = $this->tripInvitationsService->findFollowers();

        switch ($type) {
            case 'friend':
                $ids = $friends;
                break;
            case 'following':
                $ids = $followings;
                break;
            case 'follower':
                $ids = $followers;
                break;
            default:
                $ids = $friends->merge($followings);
                break;
        }

        $checkins =  Checkins::query()
            ->whereIn('place_id', $placesIds)
            ->whereNotIn('place_id', $exceptPlacesIds)
            ->limit(10)
            ->whereIn('users_id', $ids)
            ->get()
            ->unique(function ($item) {
                return $item['place_id'] . '_' . $item['users_id'];
            });

        foreach ($checkins as &$checkin) {
            if (in_array($checkin->users_id, $friends->values()->toArray())) {
                $checkin->person_type = 'friend';
            } elseif (in_array($checkin->users_id, $followings->values()->toArray())) {
                $checkin->person_type = 'following';
            }
        }

        return $checkins;
    }
}