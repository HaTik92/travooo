<?php

namespace App\Services\OldSignUpFlow;

use App\Events\ChatSendPrivateMessageEvent;
use App\Events\ExpertStatusUpdateEvent;
use App\Models\Notifications\Notifications;
use App\Models\User\User;
use App\Services\RankingPoints\RankingPointsService;
use App\Services\OldSignUpFlow\UsersService;
use Illuminate\Support\Facades\Auth;

class ExpertsService
{
    /**
     * @var RankingPointsService
     */
    protected $rankingPointsService;

    /**
     * @var UsersService
     */
    protected $usersService;

    /**
     * ExpertsService constructor.
     * @param RankingPointsService $rankingPointsService
     * @param UsersService $usersService
     */
    public function __construct(RankingPointsService $rankingPointsService, UsersService $usersService)
    {
        $this->rankingPointsService = $rankingPointsService;
        $this->usersService = $usersService;
    }

    /**
     * @param int $userId
     * @param int $pointsAmount
     */
    public function approve($userId, $pointsAmount = 0)
    {
        $rankingPoints = $this->rankingPointsService->findByUserId($userId);

        if (!$rankingPoints) {
            $rankingPoints = $this->rankingPointsService->create($userId);
        }

        $this->rankingPointsService->addPoints($rankingPoints, $pointsAmount);
        // ranking_add_points($userId, 5, $pointsAmount, 'expert_approved', $userId, Auth::guard('user')->user()->id);

        $this->usersService->approve($userId);
        $this->usersService->update($userId, [
            'type' => User::TYPE_EXPERT,
            'expert' => 1
        ]);

        notify($userId, 'approve', 'approve', $pointsAmount, $userId);
        //broadcast(new ExpertStatusUpdateEvent($userId, 1, $pointsAmount));
    }

    /**
     * @param int $userId
     * @param string $message
     */
    public function disapprove($userId, $message = '')
    {
        $this->usersService->approve($userId, 0);
        $this->usersService->update($userId, [
            'type' => User::TYPE_REGULAR,
            'expert' => 0
        ]);

        notify($userId, 'disapprove', 'disapprove', $message, $userId);

        //broadcast(new ExpertStatusUpdateEvent($userId, 0));
    }

    /**
     * @param User $user
     * @return int
     */
    public function getPresetPoints($user)
    {
        if (!$user->inviteExpertLink) {
            return 0;
        }

        return $user->inviteExpertLink->points;
    }

    /**
     * @param $userId
     * @return bool
     */
    public function addPresetPoints($userId)
    {
        $user = $this->usersService->find($userId);

        if (!$user) {
            return false;
        }

        $pointsAmount = $this->getPresetPoints($user);

        if (!$pointsAmount) {
            return false;
        }

        $rankingPoints = $this->rankingPointsService->findByUserId($userId);

        if (!$rankingPoints) {
            $rankingPoints = $this->rankingPointsService->create($userId);
        }

        $this->rankingPointsService->addPoints($rankingPoints, $pointsAmount);

        return true;
    }
}
