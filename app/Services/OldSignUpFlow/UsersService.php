<?php

namespace App\Services\OldSignUpFlow;

use App\Models\User\User;
use App\Models\TripPlans\TripPlans;
use App\Models\Posts\Checkins;
use App\Models\Country\CountriesFollowers;
use App\Models\City\CitiesFollowers;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UsersService
{

    /**
     * @param $userId
     * @return User|User[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function find($userId)
    {
        return User::find($userId);
    }

    /**
     * @param string $email
     * @return User|\Illuminate\Database\Eloquent\Model|null
     */
    public function findByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    /**
     * @param string $email
     * @return User|\Illuminate\Database\Eloquent\Model|null
     */
    public function findByUsername($username)
    {
        return User::where('username', $username)->first();
    }

    /**
     * @param array $attributes
     * @param bool $deactivated
     * @return User|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $attributes, $deactivated = true)
    {
        $this->handleAttributes($attributes, $deactivated);

        return User::updateOrCreate(['email' => $attributes['email']], $attributes);
    }

    /**
     * @param $userId
     * @param int $isApproved
     */
    public function approve($userId, $isApproved = 1)
    {
        $this->update($userId, [
            'is_approved' => $isApproved
        ]);
    }

    /**
     * @param $userId
     * @param string $ip
     */
    public function setIpAddress($userId, $ip)
    {
        $this->update($userId, [
            'ip_address' => $ip
        ]);
    }

    /**
     * @param $userId
     * @param string $step
     */
    public function setSteps($userId, $step)
    {
        $this->update($userId, [
            'register_steps' => $step
        ]);
    }


    /**
     * @param int $userId
     * @param array $attributes
     *
     * @return bool
     */
    public function update($userId, array $attributes)
    {
        return User::where('id', $userId)->update($attributes);
    }

    public function getInterests($userId)
    {
        return User::where('id', $userId)->select('interests')->first();
    }

    public function generateConfirmationCode(array &$attributes)
    {
        $attributes['confirmation_code'] = mt_rand(1000000, 9999999);
    }

    /**
     * @param array $attributes
     * @param bool $deactivated
     */
    protected function handleAttributes(array &$attributes, $deactivated = true)
    {
        $this->setStatus($attributes, $deactivated);
        $this->hashPassword($attributes);
        $this->generateConfirmationCode($attributes);
    }

    /**
     * @param array $attributes
     */
    protected function hashPassword(array &$attributes)
    {
        if (array_key_exists('password', $attributes)) {
            $attributes['password'] = Hash::make($attributes['password']);
        }
    }

    /**
     * @param array $attributes
     * @param $deactivated
     */
    protected function setStatus(array &$attributes, $deactivated)
    {
        if ($deactivated) {
            $attributes['status'] = User::STATUS_INACTIVE;
        } else {
            $attributes['status'] = User::STATUS_ACTIVE;
        }
    }

    /**
     * @param string $ip
     * @return array
     */
    public function getUserSuggestionLocations($ip)
    {
        $countries = $cities = $result = [];
        $user_list = User::where('ip_address', $ip)->pluck('id');
        $trip_plans = TripPlans::whereIn('users_id', $user_list)->where('created_at', '>=', Carbon::now()->subDays(1))->take(20)->get();
        $checkins = Checkins::whereIn('users_id', $user_list)->where('created_at', '>=', Carbon::now()->subDays(1))->take(20)->get();
        $country_followers = CountriesFollowers::whereIn('users_id', $user_list)->orderBy('id', 'DESC')->take(10)->get();
        $city_followers = CitiesFollowers::whereIn('users_id', $user_list)->orderBy('id', 'DESC')->take(10)->get();

        foreach ($trip_plans as $plan) {
            foreach ($plan->cities as $city) {
                $cities[$city->id] = $city;
            }

            foreach ($plan->countries as $country) {
                $countries[$country->id] = $country;
            }
        }

        foreach ($checkins as $checkin) {
            if ($checkin->city) {
                $cities[$checkin->city->id] = $checkin->city;
            }

            if ($checkin->country) {
                $countries[$checkin->country->id] = $checkin->country;
            }
        }

        foreach ($country_followers as $country_follow) {
            $countries[$country_follow->country->id] = $country_follow->country;
        }

        foreach ($city_followers as $city_follow) {
            $cities[$city_follow->city->id] = $city_follow->city;
        }

        foreach ($cities as $city_list) {
            $result[] = [
                'id' => 'city-' . $city_list->getKey(),
                'type' => 'location',
                'name' => $city_list->transsingle->title,
                'desc' => $city_list->country->transsingle->title,
                'url' => @check_city_photo($city_list->getMedias[0]->url, 700)
            ];
        }

        foreach ($countries as $country_list) {
            $result[] = [
                'id' => 'country-' . $country_list->getKey(),
                'type' => 'location',
                'name' => $country_list->transsingle->title,
                'desc' => '',
                'url' => @check_country_photo($country_list->getMedias[0]->url, 700)
            ];
        }

        return  $result;
    }
}
