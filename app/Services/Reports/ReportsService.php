<?php

namespace App\Services\Reports;

use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Place\Place;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsShares;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsComments;
use App\Models\Reports\ReportsDraftsLocation;
use App\Models\Reports\ReportsLocation;
use App\Models\Reports\ReportsPlaces;
use App\Models\Reports\ReportsDraft;
use App\Models\Reports\ReportsDraftInfos;
use App\Models\Reports\ReportsInfos;
use App\Models\Reports\ReportsLikes;
use App\Models\Reports\ReportsShares;
use App\Models\TripPlans\TripPlans;
use App\Models\User\UsersContentLanguages;
use App\Services\Api\PrimaryPostService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ReportsService
{
    const VAR_COVER         = 'cover';

    const VAR_PLACE         = 'place';
    const VAR_MAP           = 'map';
    const VAR_PLAN          = 'plan';
    const VAR_TEXT          = 'text';
    const VAR_USER          = 'user';
    const VAR_LOCAL_VIDEO   = 'local_video';
    const VAR_VIDEO         = 'video';
    const VAR_PHOTO         = 'photo';
    const VAR_INFO          = 'info';

    const MAPPING_INFO_VARS = [
        self::VAR_PLACE,
        self::VAR_MAP,
        self::VAR_PLAN,
        self::VAR_TEXT,
        self::VAR_USER,
        self::VAR_LOCAL_VIDEO,
        self::VAR_VIDEO,
        self::VAR_PHOTO,
        self::VAR_INFO,
    ];

    const MAPPING_ALL_VARS = [
        self::VAR_PLACE,
        self::VAR_MAP,
        self::VAR_PLAN,
        self::VAR_TEXT,
        self::VAR_USER,
        self::VAR_LOCAL_VIDEO,
        self::VAR_VIDEO,
        self::VAR_PHOTO,
        self::VAR_INFO,
        self::VAR_COVER,
    ];

    /**
     * Get Traavelos trending Locations.
     * @param bool $type
     * @return array
     */
    public function getTrendingLocations($type)
    {
        $blocked_users_list = blocked_users_list();
        $lang_list = $this->getLanguagesList();

        $reportsCountries = ReportsLocation::query()
            ->whereHas('country', function ($query) {
                $query->whereHas('getMedias');
            })
            ->whereHas('report', function ($q) use ($blocked_users_list, $type, $lang_list) {
                $q->whereNotNull('published_date')
                    ->whereNotIn('users_id', $blocked_users_list)
                    ->whereIn('language_id', $lang_list);
                if ($type) {
                    $q->where('published_date', '>=', Carbon::now()->subDays(30));
                }
            })
            ->selectRaw('location_id, count(reports_id) as `rank`')
            ->where(['location_type' => Reports::LOCATION_COUNTRY])
            ->groupBy('location_id')
            ->orderBy('rank', 'DESC')->take(10)
            ->get()->map(function ($item) {
                $item->type = 'country';
                return $item;
            });
        $location_ids = collect($reportsCountries)
            ->merge(ReportsLocation::query()
                ->whereHas('city', function ($query) {
                    $query->whereHas('getMedias');
                })
                ->whereHas('report', function ($q) use ($blocked_users_list, $type, $lang_list) {
                    $q->whereNotNull('published_date')
                        ->whereNotIn('users_id', $blocked_users_list)
                        ->whereIn('language_id', $lang_list);
                    if ($type) {
                        $q->where('published_date', '>=', Carbon::now()->subDays(30));
                    }
                })
                ->selectRaw('location_id, count(reports_id) as `rank`')
                ->where(['location_type' => Reports::LOCATION_CITY])
                ->groupBy('location_id')->orderBy('rank', 'DESC')->take(20 - count($reportsCountries))->get()->map(function ($item) {
                    $item->type = 'city';
                    return $item;
                }));
        $location_ids = collect($location_ids)->sortByDesc('rank');

        return $location_ids;
    }

    /**
     * Get location report count.
     * @param int $location_id
     * @return integer
     */
    public function getLocationCount($location_id)
    {
        $blocked_users_list = blocked_users_list();
        $lang_list = $this->getLanguagesList();

        $location_count = ReportsLocation::query()
            ->whereHas('report', function ($q) use ($blocked_users_list, $lang_list) {
                $q->whereNotNull('published_date')
                    ->whereNotIn('users_id', $blocked_users_list)
                    ->whereIn('language_id', $lang_list);
            })
            ->where('location_id', $location_id)->get()->count();

        return $location_count;
    }

    /**
     * Get Traavelos trending destinations.
     * @param int $page
     * @return array
     */
    public function getTrendingDestination($page = 1)
    {
        $skip = ($page - 1) * 50;
        $user_loc_list = [];
        $places = [];

        if (Auth::check()) {
            $user_loc_list = get_users_from_my_location(Auth::user()->nationality);
        }

        $trending_destinations = ReportsInfos::selectRaw('*, count(val) as place_count')
            ->whereHas('report', function ($qr) use ($user_loc_list) {
                $qr->whereNotNull('published_date');
                $qr->whereIn('users_id', $user_loc_list);
            })
            ->where('var', 'place')
            ->where('created_at', '>=', Carbon::now()->subDays(60))
            ->groupBy('val');



        if ($trending_destinations->get()->count() < 3) {
            $trending_destinations = ReportsInfos::selectRaw('*, count(val) as place_count')
                ->whereHas('report', function ($qr) {
                    $qr->whereNotNull('published_date');
                })
                ->where('var', 'place')
                ->where('created_at', '>=', Carbon::now()->subDays(60))
                ->groupBy('val');
        }

        if ($trending_destinations->get()->count() >= 3) {
            $trending_destinations->orderBy('place_count', 'DESC')->skip($skip)->take(50)->get()->shuffle()->each(function ($report) use (&$places) {
                $places[] = Place::findOrFail($report->val);
            });
        }

        return $places;
    }


    /**
     * Get Country/City all info.
     * @param collaction $location
     * @param string $type
     * @return array
     */
    public function getLocationAllInfo($location, $type)
    {

        if ($type == 'country') {
            $iso_code = $location->iso_code;
            $location_name =  @$location->trans[0]->title;
            $nationality = @$location->trans[0]->nationality;
            $working_days = @$location->trans[0]->working_days;
            $transportation = explode(',', @$location->trans[0]->transportation);
            $speed_limit = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$location->trans[0]->speed_limit);
            $metrics = @$location->trans[0]->metrics;
            $internet = @$location->trans[0]->internet;
            $timing = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/",  @$location->trans[0]->best_time);
            $sockets = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/",  @$location->trans[0]->sockets);
            $cost_of_living = @$location->trans[0]->cost_of_living;
            $crime_rate = @$location->trans[0]->geo_stats;
            $quality_of_life = @$location->trans[0]->demographics;
            $currencies = @$location->currencies;
            $languages_spoken = @$location->languages;
            $additional_languages_spoken = @$location->additional_languages;
            $holidays = @$location->countryHolidays;
            $religions = @$location->religions;
            $emergency_numbers = @$location->emergency;
        } else {
            $iso_code = $location->country->iso_code;
            $location_name =  $location->country->trans[0]->title . '/' . @$location->trans[0]->title;
            $nationality = $location->country->trans[0]->nationality;
            $working_days = (@$location->trans[0]->working_days != '') ? @$location->trans[0]->working_days : @$location->country->trans[0]->working_days;
            $transportation = (@$location->trans[0]->transportation != '') ? explode(',', @$location->trans[0]->transportation) : explode(',',  @$location->country->trans[0]->transportation);
            $speed_limit = (@$location->trans[0]->speed_limit != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/",  @$location->trans[0]->speed_limit) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$location->country->trans[0]->speed_limit);
            $metrics = (@$location->trans[0]->metrics != '') ?  @$location->trans[0]->metrics : @$location->country->trans[0]->metrics;
            $internet = (@$location->trans[0]->internet != '') ? @$location->trans[0]->internet : @$location->country->trans[0]->internet;
            $timing = (@$location->trans[0]->best_time != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$location->trans[0]->best_time) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$location->country->trans[0]->best_time);
            $sockets = (@$location->trans[0]->sockets != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$location->trans[0]->sockets) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$location->country->trans[0]->sockets);
            $cost_of_living = (@$location->trans[0]->cost_of_living != '') ? @$location->trans[0]->cost_of_living : @$location->country->trans[0]->cost_of_living;
            $crime_rate = (@$location->trans[0]->geo_stats != '') ? @$location->trans[0]->geo_stats : @$location->country->trans[0]->geo_stats;
            $quality_of_life = (@$location->trans[0]->demographics != '') ? @$location->trans[0]->demographics : @$location->country->trans[0]->demographics;
            $currencies = (count(@$location->currencies) > 0) ? @$location->currencies : @$location->country->currencies;
            $languages_spoken = (count(@$location->languages_spoken) > 0) ? @$location->languages_spoken : @$location->country->languages;
            $additional_languages_spoken = (count(@$location->additional_languages) > 0) ? @$location->additional_languages : @$location->country->additional_languages;
            $holidays = (count(@$location->cityHolidays) > 0) ? @$location->cityHolidays : @$location->country->countryHolidays;
            $religions = (count(@$location->religions) > 0) ? @$location->religions : @$location->country->religions;
            $emergency_numbers = (count(@$location->emergency) > 0) ? @$location->emergency : @$location->country->emergency;
        }



        $speed_limit_final = '';
        $speed_limit_count = $speed_limit[0] == '' ? 0 : count($speed_limit);

        array_walk($speed_limit, function ($val, $key) use (&$speed_limit_final) {
            if ($val) {
                list($key, $value) = explode(':', $val);
                $speed_limit_final .= '<div class="sub-list"><span>' . $key . '</span><span>' . $value . '</span></div>';
            }
        });

        $timing_final = '';
        $timing_count = $timing[0] == '' ? 0 : count($timing);


        array_walk($timing, function ($val, $key) use (&$timing_final) {
            if ($val) {
                list($key, $value) = explode(':', $val);
                $timing_final .= '<div class="sub-list"><span>' . $key . '</span><span>' . $value . '</span></div>';
            }
        });

        $socket_final = '';
        $socket_count = $sockets[0] == '' ? 0 : count($sockets);


        array_walk($sockets, function ($val, $key) use (&$socket_final) {
            if ($val) {
                list($key, $value) = explode(':', $val);
                $socket_final .= '<div class="sub-list"><span>' . $key . '</span><span>' . $value . '</span></div>';
            }
        });

        $emerg = '';
        $emerg_count = 0;
        if ($emergency_numbers) {
            foreach ($emergency_numbers as $emergency) {
                $emerg_count++;
                $emerg .= '<div class="sub-list"><span>' . $emergency->transsingle->title . '</span></div>';
            }
        }

        $rel = '';
        $rel_count = 0;
        if ($religions) {
            foreach ($religions as $religions) {
                $rel_count++;
                $rel .= '<div class="sub-list"><span>' . $religions->transsingle->title . '</span></div>';
            }
        }

        $hol = '';
        $num_hol_output = 0;
        if ($holidays) {
            foreach ($holidays as $holidays) {
                if (isset($holidays->holiday->transsingle) && !empty($holidays->holiday->transsingle)) {
                    $num_hol_output++;
                    $hol .= '<div class="sub-list"><span>' . $holidays->holiday->transsingle->title . '</span></div>';
                }
            }
        }

        $lng = '';
        $lng_count = 0;
        if ($languages_spoken) {
            foreach ($languages_spoken as $languages) {
                $lng_count++;
                $lng .= '<div class="sub-list"><span>' . $languages->transsingle->title . '</span></div>';
            }
        }

        if ($additional_languages_spoken) {
            foreach ($additional_languages_spoken as $additional_languages) {
                $lng_count++;
                $lng .= '<div class="sub-list"><span>' . $additional_languages->transsingle->title . '</span></div>';
            }
        }

        $curr = '';
        $curr_count = 0;
        if ($currencies) {
            foreach ($currencies as $currency) {
                $curr_count++;
                $curr .= '<div class="sub-list"><span>' . $currency->transsingle->title . '</span></div>';
            }
        }

        $return = [
            'id' => $location->id,
            'location_name' => $location_name,
            'phone_code' => $location->code,
            'iso_code' => $iso_code,
            'lat' => $location->lat,
            'lng' => $location->lng,
            'population' => @$location->trans[0]->population,
            'nationality' => $nationality,
            'working_days' => $working_days,
            'transportation' => $transportation,
            'speed_limit' => [$speed_limit_count, $speed_limit_final],
            'metrics' => $metrics,
            'internet' => $internet,
            'timing' => [$timing_count, $timing_final],
            'sockets' => [$socket_count, $socket_final],
            'cost_of_living' => $cost_of_living,
            'crime_rate' => $crime_rate,
            'quality_of_life' => $quality_of_life,
            'currencies' => [$curr_count, $curr],
            'languages_spoken' => [$lng_count, $lng],
            'holidays' => [$num_hol_output, $hol],
            'religions' => [$rel_count, $rel],
            'emergency_numbers' => [$emerg_count, $emerg],
        ];

        return $return;
    }

    /**
     * @param int $id
     * @param bool $withDraft
     */
    public function find($id, $withDraft = false)
    {
        if ($withDraft) {
            return Reports::with('draft_report')->where('id', $id)->first();
        }
        return Reports::find($id);
    }

    /**
     * @param Reports $report
     */
    public function delete($report)
    {
        try {
            $report_id = $report->id;
            $draft_report = ReportsDraft::where('reports_id', $report_id)->first();

            // delete countries, cities, places
            ReportsPlaces::where(['reports_id' => $report_id])->delete();
            ReportsLocation::where(['reports_id' => $report_id])->delete();
            ReportsDraftsLocation::where(['reports_id' => $report_id])->delete();

            //Delete report comments
            ReportsComments::where('reports_id', $report_id)->delete();
            PostsComments::where(['type' => PrimaryPostService::TYPE_REPORT, 'posts_id' => $report_id])->delete();

            //Delete report likes & sheres
            ReportsLikes::where('reports_id', $report_id)->delete();
            ReportsShares::where('reports_id', $report_id)->delete();
            PostsShares::where('posts_type_id', $report_id)->where('type', 'report')->delete();

            //Delete report info
            ReportsInfos::where('reports_id', $report_id)->delete();
            ReportsDraftInfos::where('reports_draft_id', $draft_report->id)->delete();
            del_user_activity('Report', 'share', $report_id, $report->users_id);

            //Delete report
            $report->delete();
            $draft_report->delete();
        } catch (\Throwable $e) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * this function only for published report(not include draft mode)
     * @param App\Models\Reports\Reports $report
     */
    public function publishedPlace($report)
    {
        if ($report) {
            $publishedPlaces = $report->infos()->where('var', 'place')->whereNotNull('val')->pluck('val');
            $currentPlaces = ReportsPlaces::where('reports_id', $report->id)->pluck('places_id');
            $newAddedPlaces = $publishedPlaces->diff($currentPlaces);
            $deletedPlaces = $currentPlaces->diff($publishedPlaces);
            if ($newAddedPlaces->count()) {
                $newAddedPlaces->each(function ($places_id) use ($report) {
                    ReportsPlaces::create([
                        'reports_id' => $report->id,
                        'places_id' => $places_id
                    ]);
                });
            }
            if ($deletedPlaces->count()) {
                ReportsPlaces::where('reports_id', $report->id)->whereIn('places_id', $deletedPlaces->toArray())->delete();
            }
        }
    }

    /**
     * @param int $id
     */
    public function publishedLocation($id)
    {
        $tempLocations = [];
        $report = $this->find($id, true);
        ReportsDraftsLocation::query()->select(['location_id', 'location_type'])
            ->where([
                'reports_id' => $report->id,
                'reports_draft_id' =>  $report->draft_report->id,
            ])->get()->each(function ($item) use (&$tempLocations) {
                $tempLocations[] = $item->location_id . "-" . strtolower($item->location_type);
            });
        $this->saveLocations($report, $tempLocations, false);
    }

    /**
     * @param int|Reports $idOrObject
     * @param array $tempLocations
     * @param bool $isDraftMode
     */
    public function saveLocations($idOrObject, $tempLocations, $isDraftMode = true)
    {
        $report = (is_int($idOrObject)) ? $this->find($idOrObject) : $idOrObject;
        $locations = [Reports::LOCATION_CITY => [], Reports::LOCATION_COUNTRY => []];

        collect($tempLocations)->each(function ($tempLocation) use (&$locations) {
            $temp = explode('-', $tempLocation);
            if (Reports::LOCATION_CITY == trim(strtolower($temp[1]))) {
                $locations[Reports::LOCATION_CITY][] = $temp[0];
            } else if (Reports::LOCATION_COUNTRY == trim(strtolower($temp[1]))) {
                $locations[Reports::LOCATION_COUNTRY][] = $temp[0];
            }
        });
        if ($isDraftMode) {
            collect(Reports::LOCATION_MAPPING)
                ->each(function ($location_type) use ($report, $locations) {

                    collect($locations[$location_type])
                        ->each(function ($country_id) use ($report, $location_type) {
                            $data = [
                                'reports_draft_id' => $report->draft_report->id,
                                'reports_id' => $report->id,
                                'location_type' => $location_type,
                                'location_id' => $country_id
                            ];
                            if (!ReportsDraftsLocation::query()->where($data)->exists()) {
                                ReportsDraftsLocation::create($data);
                            }
                        });

                    ReportsDraftsLocation::query()->where([
                        'reports_id' => $report->id,
                        'reports_draft_id' => $report->draft_report->id,
                        'location_type' => $location_type
                    ])->whereNotIn('location_id', $locations[$location_type])->delete();
                });
        } else {
            collect(Reports::LOCATION_MAPPING)
                ->each(function ($location_type) use ($report, $locations) {

                    collect($locations[$location_type])
                        ->each(function ($location_id) use ($report, $location_type) {
                            $data = [
                                'reports_id' => $report->id,
                                'location_type' => $location_type,
                                'location_id' => $location_id
                            ];
                            if (!ReportsLocation::query()->where($data)->exists()) {
                                ReportsLocation::create($data);
                            }
                        });

                    ReportsLocation::query()
                        ->where(['reports_id' => $report->id, 'location_type' => $location_type])
                        ->whereNotIn('location_id', $locations[$location_type])
                        ->delete();
                });
        }
    }

    public function preparedInfoPopup($draft_report)
    {
        $tempLocations = [];
        $draft_report->location->each(function ($location) use (&$tempLocations) {
            if ($location->location_type == Reports::LOCATION_COUNTRY) {
                $obj = Countries::with([
                    'trans',
                    'capitals.city.trans',
                    'currencies.transsingle',
                    'emergency',
                    'religions',
                    'countryHolidays.holiday',
                    'languages.transsingle'
                ])->where('id', $location->location_id)->first();
                if ($obj) $tempLocations[] = $this->_preparedReportCountryDetails($obj);
            } else if ($location->location_type == Reports::LOCATION_CITY) {
                $obj = Cities::with([
                    'trans',
                    'country.trans',
                    'cityHolidays',
                    'country.countryHolidays',
                    'country.currencies.transsingle',
                    'additional_languages',
                    'emergency',
                    'country.emergency',
                    'religions',
                    'country.religions',
                ])->where('id', $location->location_id)->first();
                if ($obj) $tempLocations[] = $this->_preparedReportCityDetails($obj);
            }
        });

        $tempLocations = collect($tempLocations)->filter(function ($location) {
            $location['tabs'] = collect($location['tabs'])->filter(function ($tab) {
                return count($tab['data']);
            });
            return count($location['tabs']);
        });

        return collect($tempLocations)->map(function ($location) use ($draft_report) {
            $location['tabs'] = collect($location['tabs'])->map(function ($tab) use ($location, $draft_report) {
                $tab['data'] = collect($tab['data'])->map(function ($item) use ($location, $draft_report) {
                    $title = $item['title'];
                    $tempItem['html'] = view('react-native.report.info-element.index', [
                        'item' => $item
                    ])->render();
                    $idRender = view('react-native.report.info-element.partials.sub-item', [
                        'item' => $item
                    ])->render();
                    $tempItem['id'] = $title . ':-' . (trim($idRender)) . ":-" . $location['id'] . "-" . $location['type'];
                    $tempItem['added_flag'] = ReportsDraftInfos::query()
                        ->where('reports_draft_id', $draft_report->id)
                        ->where('var', 'info')
                        ->where('val', 'like', "%$title%")->exists();
                    return $tempItem;
                });
                return $tab;
            });
            return $location;
        });
    }

    /**
     * @param Cities $city
     * @return array
     */
    public function _preparedReportCityDetails($city)
    {
        $tabs = [];
        $_other = [];
        $generalFacts = [];
        $other = [];

        $return = ['type' => 'city'];
        $return['id'] = $city->id;
        $return['lat'] = $city->lat;
        $return['lng'] = $city->lng;
        $return['city'] = @$city->trans[0]->title;
        $return['country'] = @$city->country->trans[0]->title;
        $return['image'] = check_city_photo(@$city->medias[0]->media->path);

        $_Nationality = @$city->country->trans[0]->nationality;
        $_PhoneCode = $city->code;
        $_Population = @$city->trans[0]->population;
        $_WorkingDays = (@$city->trans[0]->working_days != '') ? @$city->trans[0]->working_days : @$city->country->trans[0]->working_days;
        $Internet = (@$city->trans[0]->internet != '') ? @$city->trans[0]->internet : @$city->country->trans[0]->internet;

        $speedLimit = (@$city->trans[0]->speed_limit != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->speed_limit) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->speed_limit);
        $speedLimits = [];
        foreach ($speedLimit as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $speedLimits[] = [
                    "key" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }

        (@$city->trans[0]->metrics != '') ? preg_match_all("/\(([^\]]*)\)/", @$city->trans[0]->metrics, $matches) : preg_match_all("/\(([^\]]*)\)/", @$city->country->trans[0]->metrics, $matches);
        // $metrics = isset($matches[1][0]) ? explode(", ", $matches[1][0]) : [];
        $metrics = (@$city->trans[0]->metrics != '') ?  @$city->trans[0]->metrics : @$city->country->trans[0]->metrics;


        $timing = (@$city->trans[0]->best_time != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->best_time) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->best_time);
        $timings = [];
        foreach ($timing as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $timings[] = [
                    "key" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }
        $transportation = (@$city->trans[0]->transportation != '') ? explode(", ", @$city->trans[0]->transportation) : explode(", ", @$city->country->trans[0]->transportation);
        $currencies = @$city->country->currencies[0]->transsingle->title;

        // $return['medias'] = count(@$city->getMedias) > 0 ? @$city->getMedias : @$city->country->getMedias;

        $socket = (@$city->trans[0]->sockets != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->sockets) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->sockets);
        $sockets = [];
        foreach ($socket as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $sockets[] = [
                    "key" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }

        $_QualityOfLife =  (@$city->trans[0]->demographics != '') ? @$city->trans[0]->demographics : @$city->country->trans[0]->demographics;
        $_Crime_Rate = (@$city->trans[0]->geo_stats != '') ? @$city->trans[0]->geo_stats : @$city->country->trans[0]->geo_stats;
        $_Cost_Of_Living = (@$city->trans[0]->cost_of_living != '') ? @$city->trans[0]->cost_of_living : @$city->country->trans[0]->cost_of_living;

        $economy = (@$city->trans[0]->economy != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->economy) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->economy);
        $economies = [];
        foreach ($economy as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $economies[] = [
                    "key" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }

        $emerg = [];
        @$city_emergency = (count((@$city->emergency ?? [])) > 0) ? @$city->emergency : @$city->country->emergency;
        if (@$city_emergency) {
            foreach (@$city_emergency as $emergency) {
                $emerg[] = $emergency->transsingle->title;
            }
        }

        $religions = [];
        @$city_religions = (count((@$city->religions ?? [])) > 0) ? @$city->religions : @$city->country->religions;
        if (@$city_religions) {
            $rel = [];
            foreach (@$city_religions as $religions) {
                $rel[] = $religions->transsingle->title;
            }
            $religions = $rel;
        }

        $holidays = [];
        $city_holiday = (count((@$city->cityHolidays ?? [])) > 0) ? @$city->cityHolidays : @$city->country->countryHolidays;
        if (@$city_holiday) {
            $hol = [];
            foreach (@$city_holiday as $holidays) {
                if (isset($holidays->holiday->transsingle) && !empty($holidays->holiday->transsingle)) {
                    $hol[] = $holidays->holiday->transsingle->title;
                }
            }
            $holidays = $hol;
        }


        $languages_spoken = [];
        $lng = [];
        @$languages_spoken = (count((@$city->languages_spoken ?? [])) > 0) ? @$city->languages_spoken : @$city->country->languages;
        if (@$languages_spoken) {
            foreach (@$languages_spoken as $languages) {
                $lng[] = [
                    'key' => $languages->transsingle->title,
                    'value' => $languages->transsingle->iso_code
                ];
            }
        }

        @$city_additional_languages = (count((@$city->additional_languages ?? [])) > 0) ? @$city->additional_languages : @$city->country->additional_languages;
        if (@$city_additional_languages) {
            foreach (@$city_additional_languages as $additional_languages) {
                $lng[] = [
                    'key' => $additional_languages->transsingle->title,
                    'value' => $additional_languages->transsingle->iso_code
                ];
            }
        }
        $languages_spoken = $lng;

        if ($_Nationality) {
            $generalFacts[] = [
                'title' => 'Nationality',
                'with_key_value' => false,
                'data' => [
                    $_Nationality
                ]
            ];
        }
        if ($_PhoneCode) {
            $generalFacts[] = [
                'title' => 'Phone Code',
                'with_key_value' => false,
                'data' => [
                    $_PhoneCode
                ]
            ];
        }
        if ($_Population) {
            $generalFacts[] = [
                'title' => 'Population',
                'with_key_value' => false,
                'data' => [
                    $_Population
                ]
            ];
        }


        if (!empty($languages_spoken)) {
            $generalFacts[] = [
                'title' => 'Languages Spoken',
                'with_key_value' => true,
                'data' => $languages_spoken
            ];
        }
        if (!empty($currencies)) {
            $generalFacts[] = [
                'title' => 'Currencies',
                'with_key_value' => false,
                'data' => [
                    $currencies
                ]
            ];
        }
        if (!empty($timings)) {
            $generalFacts[] = [
                'title' => 'Timings',
                'with_key_value' => true,
                'data' => $timings
            ];
        }
        if (!empty($transportation)) {
            $generalFacts[] = [
                'title' => 'Transportation Methods',
                'with_key_value' => false,
                'data' => $transportation
            ];
        }
        if (!empty($speedLimits)) {
            $generalFacts[] = [
                'title' => 'Speed Limit',
                'with_key_value' => true,
                'data' => $speedLimits
            ];
        }

        if ($_QualityOfLife) {
            $generalFacts[] = [
                'title' => 'Quality Of Life',
                'with_key_value' => false,
                'data' => [
                    $_QualityOfLife
                ]
            ];
        }
        if ($_Crime_Rate) {
            $generalFacts[] = [
                'title' => 'Crime Rate',
                'with_key_value' => false,
                'data' => [
                    $_Crime_Rate
                ]
            ];
        }
        if ($_Cost_Of_Living) {
            $generalFacts[] = [
                'title' => 'Cost Of Living',
                'with_key_value' => false,
                'data' => [
                    $_Cost_Of_Living
                ]
            ];
        }
        if ($_WorkingDays) {
            $generalFacts[] = [
                'title' => 'Working Days',
                'with_key_value' => false,
                'data' => [
                    $_WorkingDays
                ]
            ];
        }
        if ($Internet) {
            $generalFacts[] = [
                'title' => 'Internet',
                'with_key_value' => false,
                'data' => [
                    $Internet
                ]
            ];
        }

        if (!empty($sockets)) {
            $_other[] = [
                'title' => 'Sockets',
                'with_key_value' => true,
                'data' => $sockets
            ];
        }
        if (!empty($other)) {
            $_other[] = [
                'title' => 'Other',
                'with_key_value' => true,
                'data' => $other
            ];
        }
        if (!empty($emerg)) {
            $generalFacts[] =  [
                'title' => 'Emergency Numbers',
                'with_key_value' => false,
                'data' => $emerg
            ];
        }
        if (!empty($metrics)) {
            $generalFacts[] = [
                'title' => 'Units of measurement',
                'with_key_value' => false,
                'data' => [$metrics]
            ];
        }
        if (!empty($religions)) {
            $generalFacts[] = [
                'title' => 'Religions',
                'with_key_value' => false,
                'data' => $religions
            ];
        }


        // start tabs
        if (!empty($generalFacts)) {
            $tabs[] = [
                'title' => 'General Facts',
                'data' => $generalFacts
            ];
        }
        if (!empty($_other)) {
            $tabs[] = [
                'title' => 'Other',
                'data' => $_other
            ];
        }
        if (!empty($holidays)) {
            $tabs[] = [
                'title' => 'Holidays',
                'data' => [
                    [
                        'title' => 'Holidays',
                        'with_key_value' => false,
                        'data' => $holidays
                    ]
                ]
            ];
        }
        if (!empty($economies)) {
            $tabs[] = [
                'title' => 'Economies',
                'data' => [
                    [
                        'title' => 'Economies',
                        'with_key_value' => true,
                        'data' => $economies
                    ]
                ]
            ];
        }

        $return['tabs'] = $tabs;
        return $return;
    }

    /**
     * @param Countries $country
     * @return array
     */
    public function _preparedReportCountryDetails($country)
    {
        $tabs = [];
        $generalFacts = [];
        $_other = [];
        $other = [];

        $return = ['type' => 'country'];
        $return['id'] = $country->id;
        $return['country'] = @$country->trans[0]->title;
        $return['image'] = get_country_flag($country);
        $return['capital'] = @$country->capitals[0]->city->trans[0]->title ?? "";
        $return['iso_code'] = $country->iso_code;
        $return['lat'] = $country->lat;
        $return['lng'] = $country->lng;

        $speedLimit = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->speed_limit);
        $speedLimits = [];
        foreach ($speedLimit as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $speedLimits[] = [
                    "key" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }

        $timing = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->best_time);
        $timings = [];
        foreach ($timing as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $timings[] = [
                    "key" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }

        $transportation = explode(", ", @$country->trans[0]->transportation);
        $currencies = @$country->currencies[0]->transsingle->title;
        // $return['medias'] = @$country->getMedias;

        $_Nationality = @$country->trans[0]->nationality;
        $_PhoneCode = $country->code;
        $_Population = @$country->trans[0]->population;
        $_WorkingDays =  @$country->trans[0]->working_days;
        $Internet = @$country->trans[0]->internet;

        preg_match_all("/\(([^\]]*)\)/", @$country->trans[0]->metrics, $matches);
        // $metrics = isset($matches[1][0]) ? explode(", ", $matches[1][0]) : [];
        $metrics = @$country->trans[0]->metrics;

        $socket = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->sockets);
        $sockets = [];
        foreach ($socket as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $sockets[] = [
                    "key" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }

        $_QualityOfLife = @$country->trans[0]->demographics;
        $_Crime_Rate = @$country->trans[0]->geo_stat;
        $_Cost_Of_Living = @$country->trans[0]->cost_of_living;

        $economy = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->economy);
        $economies = [];
        foreach ($economy as $value) {
            if (count(explode(": ", $value)) >= 2) {
                $economies[] = [
                    "key" => explode(": ", $value)[0],
                    "value" => explode(": ", $value)[1]
                ];
            }
        }

        $emerg = [];
        if (@$country->emergency) {
            foreach (@$country->emergency as $emergency) {
                $emerg[] = $emergency->transsingle->title;
            }
        }

        $religions = [];
        if (@$country->religions) {
            $rel = [];
            foreach (@$country->religions as $religions) {
                $rel[] = $religions->transsingle->title;
            }
            $religions = $rel;
        }

        $holidays = [];
        if (@$country->countryHolidays) {
            $hol = [];
            foreach (@$country->countryHolidays as $holidays) {
                if (isset($holidays->holiday->transsingle) && !empty($holidays->holiday->transsingle)) {
                    $hol[] = $holidays->holiday->transsingle->title;
                }
            }
            $holidays = $hol;
        }

        $languages_spoken = [];
        $lng = [];
        if (@$country->languages) {
            foreach (@$country->languages as $languages) {
                $lng[] = [
                    'key' => $languages->transsingle->title,
                    'value' => $languages->transsingle->iso_code
                ];
            }
        }
        if (@$country->additional_languages) {
            foreach (@$country->additional_languages as $additional_languages) {
                $lng[] = [
                    'key' => $additional_languages->transsingle->title,
                    'value' => $additional_languages->transsingle->iso_code
                ];
            }
            $languages_spoken = $lng;
        }


        if ($_Nationality) {
            $generalFacts[] = [
                'title' => 'Nationality',
                'with_key_value' => false,
                'data' => [
                    $_Nationality
                ]
            ];
        }
        if ($_PhoneCode) {
            $generalFacts[] = [
                'title' => 'Phone Code',
                'with_key_value' => false,
                'data' => [
                    $_PhoneCode
                ]
            ];
        }
        if ($_Population) {
            $generalFacts[] = [
                'title' => 'Population',
                'with_key_value' => false,
                'data' => [
                    $_Population
                ]
            ];
        }

        if (!empty($languages_spoken)) {
            $generalFacts[] = [
                'title' => 'Languages Spoken',
                'with_key_value' => true,
                'data' => $languages_spoken
            ];
        }
        if (!empty($currencies)) {
            $generalFacts[] = [
                'title' => 'Currencies',
                'with_key_value' => false,
                'data' => [
                    $currencies
                ]
            ];
        }
        if (!empty($timings)) {
            $generalFacts[] = [
                'title' => 'Timings',
                'with_key_value' => true,
                'data' => $timings
            ];
        }
        if (!empty($transportation)) {
            $generalFacts[] = [
                'title' => 'Transportation Methods',
                'with_key_value' => false,
                'data' => $transportation
            ];
        }
        if (!empty($speedLimits)) {
            $generalFacts[] = [
                'title' => 'Speed Limit',
                'with_key_value' => true,
                'data' => $speedLimits
            ];
        }

        if ($_QualityOfLife) {
            $generalFacts[] = [
                'title' => 'Quality Of Life',
                'with_key_value' => false,
                'data' => [
                    $_QualityOfLife
                ]
            ];
        }
        if ($_Crime_Rate) {
            $generalFacts[] = [
                'title' => 'Crime Rate',
                'with_key_value' => false,
                'data' => [
                    $_Crime_Rate
                ]
            ];
        }
        if ($_Cost_Of_Living) {
            $generalFacts[] = [
                'title' => 'Cost Of Living',
                'with_key_value' => false,
                'data' => [
                    $_Cost_Of_Living
                ]
            ];
        }
        if ($_WorkingDays) {
            $generalFacts[] = [
                'title' => 'Working Days',
                'with_key_value' => false,
                'data' => [
                    $_WorkingDays
                ]
            ];
        }
        if ($Internet) {
            $generalFacts[] = [
                'title' => 'Internet',
                'with_key_value' => false,
                'data' => [
                    $Internet
                ]
            ];
        }

        if (!empty($sockets)) {
            $_other[] = [
                'title' => 'Sockets',
                'with_key_value' => true,
                'data' => $sockets
            ];
        }
        if (!empty($other)) {
            $_other[] = [
                'title' => 'Other',
                'with_key_value' => true,
                'data' => $other
            ];
        }
        if (!empty($emerg)) {
            $generalFacts[] =  [
                'title' => 'Emergency Numbers',
                'with_key_value' => false,
                'data' => $emerg
            ];
        }
        if (!empty($metrics)) {
            $generalFacts[] = [
                'title' => 'Units of measurement',
                'with_key_value' => false,
                'data' => [$metrics]
            ];
        }
        if (!empty($religions)) {
            $generalFacts[] = [
                'title' => 'Religions',
                'with_key_value' => false,
                'data' => $religions
            ];
        }


        // start tabs
        if (!empty($generalFacts)) {
            $tabs[] = [
                'title' => 'General Facts',
                'data' => $generalFacts
            ];
        }
        if (!empty($_other)) {
            $tabs[] = [
                'title' => 'Other',
                'data' => $_other
            ];
        }
        if (!empty($holidays)) {
            $tabs[] = [
                'title' => 'Holidays',
                'data' => [
                    [
                        'title' => 'Holidays',
                        'with_key_value' => false,
                        'data' => $holidays
                    ]
                ]
            ];
        }
        if (!empty($economies)) {
            $tabs[] = [
                'title' => 'Economies',
                'data' => [
                    [
                        'title' => 'Economies',
                        'with_key_value' => true,
                        'data' => $economies
                    ]
                ]
            ];
        }

        $return['tabs'] = $tabs;
        return $return;
    }

    /**
     * Get Trip Plan list view with count.
     * @param int $skip
     * @param string $filter
     * @param string $type
     * @param bool $with_count
     * @return array
     */
    public function getTripPlans($skip, $filter, $type, $with_count = false)
    {
        $plan_count = 0;
        $data = '';

        $plan_query = TripPlans::with(['version', 'latest_contribution_request', 'trips_places' => function ($query) {
            $query->where('versions_id', '!=', 0);
        }])->has('author');

        if ($type == 'invited') {
            $plan_query->whereHas('latest_contribution_request', function ($query) {
                $query->where('trips_contribution_requests.users_id', Auth::user()->id);
            });
        } else if ($type == 'mine') {
            $plan_query->where('users_id', Auth::user()->id)->where('active', 1);
        } else {
            $plan_query->where('users_id', Auth::user()->id)->where('active', 1)
                ->orWhereHas('latest_contribution_request', function ($q) {
                    $q->where('trips_contribution_requests.users_id', Auth::user()->id);
                });
        }

        if ($filter != '') {
            $plan_query->where('title', 'like', '%' . $filter . '%');
        }

        $plan_list = $plan_query->skip($skip)->take(10)->get();

        if ($with_count) {
            $plan_count = $plan_query->count();
        }

        if (count($plan_list) > 0) {
            foreach ($plan_list as $plan) {
                $data .= view('site.reports.partials._trip_plan_block', array('plan' => $plan));
            }
        }

        return ['plan_count' => $plan_count, 'view' => $data];
    }


    /**
     * Get current user selected languages list.
     * @return array
     */
    protected function getLanguagesList()
    {
        $langugae_id = getDesiredLanguage();
        if (isset($langugae_id[0])) {
            $langugae_id = $langugae_id;
        } else {
            $langugae_id = [UsersContentLanguages::DEFAULT_LANGUAGES];
        }

        return $langugae_id;
    }
}
