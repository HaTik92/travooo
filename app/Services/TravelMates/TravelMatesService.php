<?php

namespace App\Services\TravelMates;

use App\Models\User\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class TravelMatesService
{
    const DEFAULT_POPULAR_TRAVEL_MATES_LIMIT = 20;
    const DEFAULT_POPULAR_TRAVEL_MATES_OFFSET = 0;

    /**
     * @var TravelMatesRequestsService
     */
    protected $travelMatesRequestsService;

    public function __construct(TravelMatesRequestsService $travelMatesRequestsService)
    {
        $this->travelMatesRequestsService = $travelMatesRequestsService;
    }

    /**
     * Return the most followed users
     *
     * @param int $limit
     * @param int $offset
     *
     * @return Collection
     */
    public function getPopularTravelMates($limit = self::DEFAULT_POPULAR_TRAVEL_MATES_LIMIT, $offset = self::DEFAULT_POPULAR_TRAVEL_MATES_OFFSET)
    {
        $requestsUniqueUserIds = $this->travelMatesRequestsService->getAllUniqueTravelMatesRequestsUsersIds();

        //Get all travel mates ordered by followers number
        $popularUsers = User::leftJoin('users_followers as uf', 'uf.users_id', '=', 'users.id')
            ->selectRaw('users.*, count(uf.id) as cnt')
            ->orderBy('cnt', 'desc')
            ->groupBy(['users.id'])
            ->whereIn('users.id', $requestsUniqueUserIds)
            ->limit($limit)
            ->offset($offset)
            ->get();

        foreach($popularUsers as $user){
            $image_file = explode('/', $user->profile_picture);
           
            if (count($image_file) > 1 && Storage::disk('s3')->exists('users/profile/original/' . $user->id . '/' . end($image_file)) && !Storage::disk('s3')->exists('users/profile/th180/' . $user->id . '/' . end($image_file))){
                 $originalImg = Storage::disk('s3')->get('users/profile/original/' . $user->id . '/' . end($image_file));

                $cropped = Image::make($originalImg)->resize(180, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                
                 Storage::disk('s3')->put(
                        'users/profile/th180/' . $user->id . '/' . end($image_file), $cropped->encode(), 'public'
                    );
            }
        }
       
        return $popularUsers;
    }
}
