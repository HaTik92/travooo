<?php

namespace App\Services\TravelMates;

use App\Helpers\Auth\Auth;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\TravelMates\TravelMatesWithoutPlan;
use Carbon\Carbon;

class TravelMatesRequestsService {

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllUniqueTravelMatesRequestsUsersIds() {
        return TravelMatesWithoutPlan::query()->select('users_id')->distinct('users_id')->pluck('users_id');
    }

    public function getTravelMatesRequests($type, $skip = '', $limit = '', $filters = [], $sort = 'new') {
        $query = TravelMatesRequests::query();

        if (!empty($filters)) {
            foreach ($filters as $filter => $val) {
                if ($val === null) {
                    continue;
                }

                switch ($filter) {
                    case 'dates':
                        if ($type == 1) {
                            if ($val['start_date']) {
                                $startDate = Carbon::parse($val['start_date']);

                                $query->whereHas('plan', function ($planQuery) use ($startDate) {
                                    $planQuery->whereHas('version', function ($versionQuery) use ($startDate) {
                                        $versionQuery->where('start_date', '>=', $startDate);
                                    });
                                });
                            }
                            if ($val['finish_date']) {
                                $finishDate = Carbon::parse($val['finish_date']);

                                $query->whereHas('plan', function ($planQuery) use ($finishDate) {
                                    $planQuery->whereHas('version', function ($versionQuery) use ($finishDate) {
                                        $versionQuery->where('end_date', '<=', $finishDate);
                                    });
                                });
                            }
                        }
                        if ($type == 2) {
                            if ($val['start_date']) {
                                $startDate = Carbon::parse($val['start_date'])->format('Y-m-d');

                                $query->whereHas('without_plan', function ($wPlanQuery) use ($startDate) {
                                    $wPlanQuery->where('start_date', '>=', $startDate);
                                });
                            }
                            if ($val['finish_date']) {
                                $finishDate = Carbon::parse($val['finish_date'])->format('Y-m-d');

                                $query->whereHas('without_plan', function ($wPlanQuery) use ($finishDate) {
                                    $wPlanQuery->where('end_date', '<=', $finishDate);
                                });
                            }
                        }
                        break;
                    case 'destinations':
                        $countries_id = [];
                        $cities_id = [];
                        foreach ($val as $destination) {
                            $destination_type = explode('-', $destination);

                            if ($destination_type[1] == 'country')
                                $countries_id[] = $destination_type[0];

                            if ($destination_type[1] == 'city')
                                $cities_id[] = $destination_type[0];
                        }
                        if ($type == 1) {
                            if (count($cities_id) > 0 && count($countries_id) > 0) {
                                $query->whereHas('plan', function ($planQuery) use ($cities_id) {
                                    $planQuery->whereHas('cities', function ($citiesQuery) use ($cities_id) {
                                        $citiesQuery->whereIn('cities.id', $cities_id);
                                    });
                                });

                                $query->orWhereHas('plan', function ($planQuery) use ($countries_id) {
                                    $planQuery->whereHas('countries', function ($countriesQuery) use ($countries_id) {
                                        $countriesQuery->whereIn('countries.id', $countries_id);
                                    });
                                });
                            } else {
                                if (count($cities_id) > 0) {
                                    $query->whereHas('plan', function ($planQuery) use ($cities_id) {
                                        $planQuery->whereHas('cities', function ($citiesQuery) use ($cities_id) {
                                            $citiesQuery->whereIn('cities.id', $cities_id);
                                        });
                                    });
                                }

                                if (count($countries_id) > 0) {
                                    $query->whereHas('plan', function ($planQuery) use ($countries_id) {
                                        $planQuery->whereHas('countries', function ($countriesQuery) use ($countries_id) {
                                            $countriesQuery->whereIn('countries.id', $countries_id);
                                        });
                                    });
                                }
                            }
                        }

                        if ($type == 2) {
                            if (count($cities_id) > 0 && count($countries_id) > 0) {

                                $query->whereHas('without_plan', function ($wplanQuery) use ($cities_id) {
                                    foreach ($cities_id as $ci => $city) {
                                        if (count($cities_id) > 0 && $ci == 0) {
                                            $wplanQuery->whereRaw('FIND_IN_SET(' . $city . ',cities_id)');
                                        } else {
                                            $wplanQuery->orWhereRaw('FIND_IN_SET(' . $city . ',cities_id)');
                                        }
                                    }
                                });


                                $query->orWhereHas('without_plan', function ($wplanQuery) use ($countries_id) {
                                    foreach ($countries_id as $co => $country) {
                                        if (count($countries_id) > 0 && $co == 0) {
                                            $wplanQuery->whereRaw('FIND_IN_SET(' . $country . ',countries_id)');
                                        } else {
                                            $wplanQuery->orWhereRaw('FIND_IN_SET(' . $country . ',countries_id)');
                                        }
                                    }
                                });
                            } else {
                                if (count($cities_id) > 0) {
                                    $query->whereHas('without_plan', function ($wplanQuery) use ($cities_id) {
                                        foreach ($cities_id as $ci => $city) {
                                            if (count($cities_id) > 0 && $ci == 0) {
                                                $wplanQuery->whereRaw('FIND_IN_SET(' . $city . ',cities_id)');
                                            } else {
                                                $wplanQuery->orWhereRaw('FIND_IN_SET(' . $city . ',cities_id)');
                                            }
                                        }
                                    });
                                }

                                if (count($countries_id) > 0) {
                                    $query->whereHas('without_plan', function ($wplanQuery) use ($countries_id) {
                                        foreach ($countries_id as $co => $country) {
                                            if (count($countries_id) > 0 && $co == 0) {
                                                $wplanQuery->whereRaw('FIND_IN_SET(' . $country . ',countries_id)');
                                            } else {
                                                $wplanQuery->orWhereRaw('FIND_IN_SET(' . $country . ',countries_id)');
                                            }
                                        }
                                    });
                                }
                            }
                        }

                        break;
                    case 'countries':
                        $countriesIds = $val;

                        if (count($countriesIds) > 0) {
                            if ($type == 1) {
                                $query->whereHas('plan', function ($planQuery) use ($countriesIds) {
                                    $planQuery->whereHas('author', function ($authorQuery) use ($countriesIds) {
                                        $authorQuery->whereIn('nationality', $countriesIds);
                                    });
                                });
                            }
                            if ($type == 2) {
                                $query->whereHas('without_plan', function ($planQuery) use ($countriesIds) {
                                    $planQuery->whereHas('author', function ($authorQuery) use ($countriesIds) {
                                        $authorQuery->whereIn('nationality', $countriesIds);
                                    });
                                });
                            }
                        }

                        break;

                    case 'location':
                        if ($type == 2) {
                            $location = $val;
                            $query->whereHas('without_plan', function ($wplanQuery) use ($location) {
                                $wplanQuery->where('current_location', $location);
                            });
                        }
                        break;

                    case 'interests':
                        $interests = collect($val);

                        if ($type == 1) {
                            foreach ($interests as $interest) {
                                $query->whereHas('author', function ($authorQuery) use ($interest) {
                                    $authorQuery->where('interests', 'like', '%' . $interest . '%');
                                });
                            }
                        }
                        if ($type == 2) {
                            foreach ($interests as $interest) {
                                $query->whereHas('without_plan', function ($wplanQuery) use ($interest) {
                                    $wplanQuery->where('interests', 'like', '%' . $interest . '%');
                                });
                            }
                        }

                        break;

                    case 'age':
                       $ageRange = explode('-', $val);
                        $berchDateRange = [Carbon::now()->subYears($ageRange[0])->format('Y-m-d'), Carbon::now()->subYears($ageRange[1])->format('Y-m-d')];
                        
                        $query->whereHas('author', function ($authorQuery) use ($ageRange) {
                            $authorQuery->whereBetween('age', $ageRange);
                            $authorQuery->orWhereBetween('birth_date', [Carbon::now()->subYears($ageRange[1])->format('Y-m-d'), Carbon::now()->subYears($ageRange[0])->format('Y-m-d')]);
                        });
                        break;

                    case 'gender':
                        $query->whereHas('author', function ($authorQuery) use ($val) {
                            $authorQuery->where('gender', $val);
                        });
                        break;
                }
            }
        }


        if ($skip != '') {
            $query->skip($skip);
        }
        if ($limit != '') {
            $query->take($limit);
        }

        if ($type == 1) {
            $query->whereHas('plan');
        }

        if ($type == 2) {
            $query->whereNotNull('without_plans_id');
        }

        $query->where('users_id', '!=', \Auth::user()->id);
        $query->where('status', '!=', -1);

        switch ($sort) {
            case 'relevant':
                break;
            case 'top':
                $query->withCount('going');
                $query->orderBy('going_count', 'desc');
                break;
            case 'upcoming':
                if ($type == 1) {
                    $query->whereHas('plan', function ($planQuery) {
                        $planQuery->whereHas('version', function ($versionQuery) {
                            $versionQuery->where('start_date', '>=', date('Y-m-d'));
                        });
                    });
                }
                if ($type == 2) {
                    $query->whereHas('without_plan', function ($wPlanQuery) {
                        $wPlanQuery->where('start_date', '>=', date('Y-m-d'));
                    });
                }
                break;
            default:
                $query->orderBy('created_at', 'desc');
        }
//        dd($query->toSql());
        $total = $query->count();
        return [
            'total' => $total,
            'items' => $query->get()
        ];
    }

}
