<?php

namespace App\Services\TravelMates;

use App\Models\TripPlans\TripPlans;

class TravelMatesPlansService
{
    const DEFAULT_TRIP_PLANS_LIMIT  = 5;
    const DEFAULT_TRIP_PLANS_OFFSET = 0;

    /**
     * @var TravelMatesRequestsService
     */
    protected $travelMatesRequestsService;

    public function __construct(TravelMatesRequestsService $travelMatesRequestsService)
    {
        $this->travelMatesRequestsService = $travelMatesRequestsService;
    }

    public function getTripPlans($limit = self::DEFAULT_TRIP_PLANS_LIMIT, $offset = self::DEFAULT_TRIP_PLANS_OFFSET)
    {
        return TripPlans::query()->offset(0)->limit($limit)->offset($offset)->get();
    }
}
