<?php

namespace App\Services\EnlargedViews;

use App\Models\Contracts\EnlargedViewPostContract;
use App\Models\EnlargedViews\EnlargedView;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EnlargedViewsService
{
    /**
     * @param EnlargedViewPostContract $post
     */
    public function createEnlargeViewForPost(EnlargedViewPostContract $post)
    {
        $post->enlargedView()->save($this->createEnlargedView());
    }

    /**
     * @param string $urlKey
     *
     * @return EnlargedViewPostContract
     * @throws ModelNotFoundException
     */
    public function getPostByUrlKey($urlKey)
    {
        $enlargedView = $this->getEnlargedViewByUrlKey($urlKey);

        return $enlargedView->entity;
    }

    /**
     * @param string $urlKey
     * @return EnlargedView
     *
     * @throws ModelNotFoundException
     */
    protected function getEnlargedViewByUrlKey($urlKey)
    {
        return EnlargedView::where('url_key', $urlKey)->firstOrFail();
    }

    /**
     * @return EnlargedView
     */
    protected function createEnlargedView()
    {
        return EnlargedView::create([
            'url_key' => $this->generateUniqueEnlargedViewKey()
        ]);
    }

    /**
     * @return string
     */
    protected function generateUniqueEnlargedViewKey()
    {
        return uniqid();
    }
}
