<?php

namespace App\Services\Visits;

use App\Models\TripPlans\TripPlans;
use App\Services\Trends\TrendsService;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsService;
use Carbon\Carbon;

class VisitsService
{
    /**
     * @var TripInvitationsService
     */
    private $tripInvitationsService;

    protected $friends;
    protected $followers;
    protected $authId;

    public function __construct(TripInvitationsService $tripInvitationsService)
    {
        $this->tripInvitationsService = $tripInvitationsService;
        $this->authId = auth()->id();

        if ($this->authId) {
            $this->friends = $this->tripInvitationsService->findFriends();
            $this->followers = $this->tripInvitationsService->findFollowers();
        } else {
            $this->friends = [];
            $this->followers = [];
        }
    }

    public function getRelatedVisits($placesIds, $paginate = false, $type = null, $locationType = 0, $exceptPlaceIds = [])
    {
        $friends = $this->friends;
        $followers = $this->followers;

        $query = TripPlans::query()
            ->select(['trips.*', 'trips_places.time', 'trips_places.id as trip_place_id', 'trips_places.places_id'])
            ->selectRaw('count(DISTINCT CONCAT(trips_places.places_id, \' \', trips.id)) as places_count')
            ->join('trips_places', function($q) use ($placesIds, $locationType) {
                $now = Carbon::now();
                $q->on('trips.active_version', 'trips_places.versions_id')
                    ->where('trips_places.time', '<', $now)
                    ->whereNull('trips_places.deleted_at');

                    switch ($locationType) {
                        case TrendsService::LOCATION_TYPE_PLACE:
                            $q->whereIn('trips_places.places_id', $placesIds);
                            break;
                        case TrendsService::LOCATION_TYPE_CITY:
                            $q->whereIn('trips_places.cities_id', $placesIds);
                            break;
                        case TrendsService::LOCATION_TYPE_COUNTRY:
                            $q->whereIn('trips_places.countries_id', $placesIds);
                            break;
                        default:
                            break;
                    }
            })
            ->where('users_id', '!=', $this->authId)
            ->with(['author'])
            ->orderByDesc('time');

        switch ($type) {
            case 'friend':
                $query->whereIn('users_id', $friends);
                $query->whereIn('privacy', [TripsService::PRIVACY_PUBLIC, TripsService::PRIVACY_FRIENDS]);
                break;
            case 'follower':
                $query->whereIn('users_id', $followers);
                $query->where('privacy', TripsService::PRIVACY_PUBLIC);
                break;
            case 'expert':
                $query->whereHas('author', function($q) {
                    $q->where('expert', 1);
                });
                $query->where('privacy', TripsService::PRIVACY_PUBLIC);
                break;
            case 'other':
                $query->whereNotIn('users_id', $friends);
                $query->whereNotIn('users_id', $followers);
                $query->where('privacy', TripsService::PRIVACY_PUBLIC);
                break;
            case 'all':
                $query->where(function($q) use ($friends, $followers) {
                    $q->where(function($q) use ($friends) {
                        $q->whereIn('users_id', $friends);
                        $q->whereIn('privacy', [TripsService::PRIVACY_PUBLIC, TripsService::PRIVACY_FRIENDS]);
                    });
                    $q->orWhere(function($q) use ($followers) {
                        $q->whereIn('users_id', $followers);
                        $q->where('privacy', TripsService::PRIVACY_PUBLIC);
                    });
                });
                break;
            default:
                $query->where(function($q) use ($friends, $followers) {
                    $q->where(function($q) use ($friends) {
                        $q->whereIn('users_id', $friends);
                        $q->whereIn('privacy', [TripsService::PRIVACY_PUBLIC, TripsService::PRIVACY_FRIENDS]);
                    });
                    $q->orWhere(function($q) use ($followers) {
                        $q->whereIn('users_id', $followers);
                        $q->where('privacy', TripsService::PRIVACY_PUBLIC);
                    });
                    $q->orWhere(function($q) use ($friends, $followers) {
                        $q->whereNotIn('users_id', $friends);
                        $q->whereNotIn('users_id', $followers);
                        $q->where('privacy', TripsService::PRIVACY_PUBLIC);
                    });
                    $q->orWhere(function($q) use ($friends, $followers) {
                        $q->whereHas('author', function($q) {
                            $q->where('expert', 1);
                        });
                        $q->where('privacy', TripsService::PRIVACY_PUBLIC);
                    });
                });
                break;
        }

        if ($paginate) {
            $query->paginate(10);
        }

        if ($type === 'all') {
            $query->groupBy('places_id');
            $query->whereNotIn('places_id', $exceptPlaceIds);
            return $query->pluck('places_id');
        } else {
            $query->groupBy('trips.id');
        }

        $visits = $query->get();

        foreach ($visits as &$visit) {
            if (!$visit->author) {
                continue;
            }

            if (\Auth::check() && in_array($visit->users_id, $friends->values()->toArray())) {
                $visit->person_type = 'friend';
            } elseif (\Auth::check() &&  in_array($visit->users_id, $followers->values()->toArray())) {
                $visit->person_type = 'follower';
            } elseif ($visit->author->expert) {
                $visit->person_type = 'expert';
            } else {
                $visit->person_type = 'other';
            }

            $visit->src = check_profile_picture($visit->author->profile_picture);
        }

        return $visits;
    }
}
