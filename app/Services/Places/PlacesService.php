<?php

namespace App\Services\Places;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PlacesService
{
    /**
     * @param $lat
     * @param $lng
     * @param int $distance
     * @return Collection
     */
    public function getNearbyPlaces($lat, $lng, $distance = 2)
    {
        return DB::table('places')
            ->select(DB::raw('id, ((ACOS(SIN('.$lat.' * PI() / 180) * SIN(lat * PI() / 180) + COS('.$lat.' * PI() / 180) * COS(lat * PI() / 180) * COS(('.$lng.' - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance'))
            ->havingRaw('distance <= ?', [$distance])
            ->orderBy('distance', 'ASC')
            ->get();
    }
}