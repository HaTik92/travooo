<?php

namespace App\Services\Profile;

use App\DataObjects\FeedItemDTO;
use App\Fabrics\Feeds\FeedsBuilder;
use App\Fabrics\Following\FollowingFabric;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\Posts\Checkins;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use App\Models\User\User;
use App\Models\UsersFollowers\UsersFollowers;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Backend\ActivityLog\ActivityLog as ActivityLogRepository;


class ProfileService
{
    /**
     * @var ActivityLogRepository
     */
    private $activityLogRepo;

    public function __construct(ActivityLogRepository $activityLogRepo)
    {
        $this->activityLogRepo = $activityLogRepo;
    }

    public function uploadProfileImage($user_id, $original_file, $croped_image, $type, $save_type, $field)
    {
        $user = User::find($user_id);
        $asw_file_name = explode('/', $user->$field);

        if ($croped_image) {
            list($baseType, $croped_image) = explode(';', $croped_image);
            list(, $croped_image) = explode(',', $croped_image);
            $reized_image = base64_decode($croped_image);

            if ($type == 'profile') {
                $r_image = Image::make($reized_image)->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $croped_image = $r_image->encode();
            } else {
                $croped_image = $reized_image;
            }


            $filename = time() . '_image.png';
            //Move cropped image

            Storage::disk('s3')->put(
                'users/' . $type . '/' . $user_id . '/' . $filename, $croped_image, 'public'
            );

            if ($save_type == 'save') {
                //Move original image
                Storage::disk('s3')->putFileAs(
                    'users/' . $type . '/original/' . $user_id . '/', $original_file, $filename, 'public'
                );

            } else {

                if (Storage::disk('s3')->exists('users/' . $type . '/original/' . $user_id . '/' . end($asw_file_name))) {
                    Storage::disk('s3')->move(
                        'users/' . $type . '/original/' . $user_id . '/' . end($asw_file_name), 'users/' . $type . '/original/' . $user_id . '/' . $filename, 'public');
                } else {
                    if ($user->$field != "" || $user->$field != null) {
                        Storage::disk('s3')->put(
                            'users/' . $type . '/original/' . $user_id . '/' . $filename, fopen(base_path() . '/public/' . $user->$field, 'r+'), 'public');
                    }
                }
            }

            $croped_file_url = 'https://s3.amazonaws.com/travooo-images2/users/' . $type . '/' . $user_id . '/' . $filename;
            $original_file = 'https://s3.amazonaws.com/travooo-images2/users/' . $type . '/original/' . $user_id . '/' . $filename . '?v=' . time();

            if ($user->$field != "" || $user->$field != null) {
                Storage::disk('s3')->delete('users/' . $type . '/' . $user_id . '/' . end($asw_file_name));
                if ($save_type == 'save') {
                    Storage::disk('s3')->delete('users/' . $type . '/original/' . $user_id . '/' . end($asw_file_name));
                }
            }

            $user->$field = $croped_file_url;
            $user->save();

            return ['croped_file' => $croped_file_url, 'original_file' => $original_file];
        }
    }

    public function getPostCount($user_id)
    {
        return $this->activityLogRepo->count('all_feed', $user_id);
    }

    public function getFeed($userId, $type, $page, $perPage, $withLink = false)
    {
        $activityLogs = $this->activityLogRepo->getTransformed($type, $userId, $page, $perPage);
        $feedsBuilder = new FeedsBuilder;
        foreach ($activityLogs as $feed) {
            $feedItem = new FeedItemDTO($feed->variable, $feed->type, $feed->action);
            $feedItem->withLink = $withLink;
            $feedsBuilder->add($feedItem);
        }

        return [
            'total' => $this->activityLogRepo->count($type, $userId),
            'feed' => $feedsBuilder->getFeeds()
        ];
    }

    public function getTripPlans(int $userId, $page, $perPage, $sort, $filter)
    {
        $skip = ($page - 1) * $perPage;

        $all_plans = TripPlans::where('trips.users_id', $userId)->where('trips.active', 1)
            ->with([
                'version' => function ($query) use ($userId) {
                    return $query->where('authors_id', $userId);
                },
                'trips_places' => function ($query) {
                    return $query->where('versions_id', '!=', 0);
                }
            ]);

        if ($filter == 'memory' || $sort == 'creation_date') {
            $all_plans->select('trips.*')
                ->leftJoin('trips_versions', 'trips_versions.plans_id', '=', 'trips.id')
                ->groupBy('trips.id');
        }

        if ($sort == 'creation_date') {
            $all_plans->orderBy('trips_versions.start_date', 'DESC');
        } else {
            $all_plans->orderBy('trips.created_at', $sort);
        }

        if ($filter == 'memory') {
            $all_plans->where(function ($query) {
                $query->where('trips.memory', 1)
                    ->orWhere('trips_versions.end_date', '<=', Carbon::now()->subDays(10));
            });
        } elseif ($filter == 'upcoming') {
            $all_plans->whereHas('versions', function ($q) {
                $q->where('start_date', '>', date('Y-m-d'));
                $q->orWhere('end_date', '>=', Carbon::now()->format('Y-m-d'));
            })
                ->where('trips.users_id', $userId)->where('trips.active', 1);

        }
        $totalTrips = (clone $all_plans)->count();
        $all_plans = $all_plans->skip($skip)->take($perPage)->get()->map(function ($plan) {
            return [
                'id' => $plan->id,
                'trip_image' => get_plan_map($plan, 140, 150),
                'created_at' => $plan->created_at,
                'url' => url_with_locale('trip/plan/'.$plan->id),
                'title' => $plan->title,
                'start_date' => $plan->version->start_date,
                'end_date' => $plan->version->end_date,
                'duration' => calculate_duration($plan->id, 'all'),
                'distance' => calculate_distance($plan->id),
                'destination_count' => $plan->trips_places->count(),
                'destination' => $plan->trips_places->take(6)->map(function ($item) {
                    return @check_place_photo($item->place);
                })
            ];
        });

        return [
            'total' => $totalTrips,
            'trips' => $all_plans
        ];
    }

    public function getFollowers($userId, $page, $perPage)
    {
        $skip = ($page - 1) * $perPage;
        $authUserId = Auth::user()->id;

        $query = UsersFollowers::select('followers_id as id')->where('users_id', $userId)->where('follow_type', 1);

        $total = (clone $query)->count();
        $userIds = $query->skip($skip)->take($perPage)->pluck('id');

        $myFollowerIds = UsersFollowers::select('users_id as id')
            ->where('followers_id', $authUserId)
            ->whereIn('users_id', $userIds)
            ->pluck('id')->toArray();

        $result = User::select('id', 'name', 'profile_picture')->whereIn('id', $userIds)->get()
            ->map(function ($user) use ($myFollowerIds) {
                $user->is_followed = in_array($user->id, $myFollowerIds);
                $user->profile_picture = check_profile_picture($user->profile_picture);
                return $user;
        });

        return [
            'total' => $total,
            'followers' => $result
        ];
    }

    public function getFollowing($type, $userId, $page, $perPage)
    {
        $skip = ($page - 1) * $perPage;

        if ($type != 'all') {
            $following = FollowingFabric::get($type, $userId);

            return [
                'total' => $following->count(),
                'following' => $following->getList($skip, $perPage)
            ];
        }

        $followingTypes = ['people', 'countries', 'cities', 'places'];
        $followingObject = [];
        $followingTotals = ['all' => 0];
        $followingList = [];
        $currentCount = 0;
        foreach ($followingTypes as $key => $followingType) {
            $followingObject[$followingType] = FollowingFabric::get($followingType, $userId);
            $followingTotals[$followingType] = $followingObject[$followingType]->count();
            $followingTotals['all'] += $followingTotals[$followingType];

            if (count($followingList) < $perPage && $skip < ($currentCount + $followingTotals[$followingType])) {
                $followingList = array_merge(
                    $followingList,
                    array_slice(
                        $followingObject[$followingType]->getList($skip - $currentCount, $perPage),
                        0,
                        $perPage - count($followingList))
                );
            }
            $currentCount += $followingTotals[$followingType];
        }

        return [
            'total' => $followingTotals,
            'following' => $followingList
        ];
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getExpertiseList(int $userId) : array
    {
        $cities = ExpertsCities::select('experts_cities.cities_id as id', DB::raw('"city" as type'), 'title')
            ->join('cities_trans', 'cities_trans.cities_id', '=', 'experts_cities.cities_id')
            ->where('users_id', $userId)->get();
        $countries = ExpertsCountries::select('experts_countries.countries_id as id', DB::raw('"country" as type'), 'title')
            ->join('countries_trans', 'countries_trans.countries_id', '=', 'experts_countries.countries_id')
            ->where('users_id', $userId)->get();

        $getExpertiseObject = function ($list) {
            $expertise = [];
            foreach ($list as $item) {
                $expertise[] = [
                    'id' => "$item->type,$item->id",
                    'text' => $item->title
                ];
            }
             return $expertise;
        };

        return array_merge($getExpertiseObject($cities), $getExpertiseObject($countries));
    }

    public function getVisitedPlaceForMap($userId)
    {
        $result = [];

        $checkins = Checkins::query()
            ->where('users_id', $userId)
            ->with([
                'place.transsingle:id,places_id,title',
                'city.transsingle:id,cities_id,title',
                'country.transsingle:id,countries_id,title',
                'post_checkin',
                'user:id,name,profile_picture'
            ])
            ->whereDate('checkin_time', '<', date('Y-m-d') )
            ->orderBy('created_at', 'DESC')
            ->groupBy('id')
            ->take(10)
            ->get();

        foreach ($checkins as $checkin) {
            if (is_object($checkin->place)) {
                $lat = $checkin->place->lat;
                $lng = $checkin->place->lng;
                $place_name = $checkin->place->transsingle->title;
            } elseif (is_object($checkin->city)) {
                $lat = $checkin->city->lat;
                $lng = $checkin->city->lng;
                $place_name = $checkin->city->transsingle->title;
            }
            if (is_object($checkin->country)) {
                $lat = $checkin->country->lat;
                $lng = $checkin->country->lng;
                $place_name = $checkin->country->transsingle->title;
            }

            if(@$checkin->place){
                $media = $checkin->place->getMedias()->first();
                $result[] = [
                    'post_id' => @$checkin->post_checkin->post_id ?: rand(10,100),
                    'place_id' => $checkin->place->id,
                    'lat' => $lat,
                    'lng' => $lng,
                    'photo' => (isset($media->url))
                        ? 'https://s3.amazonaws.com/travooo-images2/th1100/'.$media->url
                        : 'http://s3.amazonaws.com/travooo-images2/placeholders/place.png',
                    'name' => @$checkin->user->name,
                    'id' => @$checkin->user->id,
                    'profile_picture' => check_profile_picture(@$checkin->user->profile_picture),
                    'place_name' => $place_name,
                    'date' => date('d M, Y', strtotime(@$checkin->checkin_time)),
                    'type' =>'checkin',
                    'city_name' => @$checkin->city->transsingle->title,
                    'raw_date' => date('Y-m-d', strtotime(@$checkin->checkin_time))
                ];
            }
        }

        $plans_places = TripPlaces::query()
            ->with([
                'trip.author:id,name,profile_picture',
                'place.transsingle:id,places_id,title',
                'city.transsingle:id,cities_id,title'
            ])
            ->where('time', '<', date('Y-m-d H:i:s'))
            ->whereHas('trip', function ($query) use ($userId){
                $query->where('users_id', $userId);
            })
            ->orderBy('time', 'DESC')
            ->take(20)
            ->get();


        foreach($plans_places as $place){
            if($place->places_id !=0 || !empty($place->places_id)){
                $media = $place->place->getMedias()->first();
                $result[] = [
                    'post_id' => @$place->places_id,
                    'place_id' => @$place->places_id,
                    'lat' => @$place->place->lat,
                    'lng' => @$place->place->lng,
                    'photo' => (isset($media->url))
                        ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $media->url
                        : 'http://s3.amazonaws.com/travooo-images2/placeholders/place.png',
                    'name' => @$place->trip->author->name,
                    'id' => @$place->trip->author->id,
                    'profile_picture' => check_profile_picture(@$place->trip->author->profile_picture),
                    'place_name' => @$place->place->transsingle->title,
                    'date' => date('d M, Y', strtotime(@$place->time)),
                    'type' => 'place',
                    'city_name' => @$place->city->transsingle->title,
                    'raw_date' => date('Y-m-d', strtotime(@$place->time))
                ];
            }
        }

        $user_followers = UsersFollowers::query()
            ->with([
                'follower.country_of_nationality.transsingle:id,countries_id,title'
            ])
            ->where('users_id', $userId)
            ->where('follow_type', 1)
            ->orderBy('id', 'DESC')
            ->take(20)->get();


        if(count($user_followers) > 0){
            foreach($user_followers as $user_follower){
                if($user_follower->follower->country_of_nationality){
                    $media = $user_follower->follower->country_of_nationality->getMedias()->first();
                    $result[] = [
                        'post_id' => @$user_follower->follower->country_of_nationality->id,
                        'place_id' => @$user_follower->follower->country_of_nationality->id,
                        'lat' => @$user_follower->follower->country_of_nationality->lat,
                        'lng' => @$user_follower->follower->country_of_nationality->lng,
                        'photo' => (isset($media->url))
                            ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $media->url
                            : 'http://s3.amazonaws.com/travooo-images2/placeholders/place.png',
                        'name' => @$user_follower->follower->name,
                        'id' => @$user_follower->follower->id,
                        'profile_picture' => check_profile_picture(@$user_follower->follower->profile_picture),
                        'place_name' => @$user_follower->follower->country_of_nationality->transsingle->title,
                        'date' => date('d M, Y', strtotime(@$user_follower->created_at)),
                        'type' => 'followers',
                        'city_name' => @$user_follower->follower->country_of_nationality->transsingle->title,
                        'raw_date' => date('Y-m-d', strtotime(@$user_follower->created_at))
                    ];
                }
            }
        }

        return $result;
    }
}
