<?php
namespace App\Services\Socialite;

use App\Interfaces\SocialiteServiceInterface;
use App\Models\User\User;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use Illuminate\Support\Facades\Auth;

class SocialiteService implements SocialiteServiceInterface
{

    /**
     * @param String $provider
     * @return string
     * @throws Exception
     */
    public function getRedirectUrlByProvider(String $provider)
    {
        try {
            return Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param String $provider
     * @return array[]
     */
    public function loginWithSocialite(String $provider)
    {
        try {
            $userSocial = Socialite::driver($provider)->stateless()->user();
            $user = User::where('email', $userSocial->email)->first();
            if( $user ){
                Auth::guard('user')->login($user);
            }else {
                try{
                    $user = new User();
                    $user->name = $userSocial->getName();
                    $user->email = $userSocial->getEmail();
                    $user->profile_picture = $userSocial->getAvatar();
                    $user->login_type = User::GOOGLE;
                    $user->social_key = $userSocial->getId();
                    $user->save();

                    Auth::guard('user')->login($user);
                }
                catch( Exception $exception ){
                    return $this->prepareErrorResult( $exception->getMessage() );
                }
                
                return $this->prepareErrorResult('This email address is already registered');
            }
            return $this->prepareSuccessResult( $user );
        }catch (Exception $exception){
            throw $exception;
        }
    }

    /**
     * @param String $email
     * @return mixed
     */
    private function searchUserByEmail( String $email )
    {
        return User::with('social')
            ->where(['email'=>$email])
            ->first();
    }

    /**
     * @param User $user
     * @param String|null $url
     * @return mixed
     */
    private function prepareSuccessResult(User $user )
    {
        return $this->makeAuthenticationCookie([
            'user' => $user,
            'redirectUrl' => '/login'
        ]);
    }

    /**
     * @param String $exception
     * @return mixed
     */
    private function prepareErrorResult(String  $exception )
    {
        return $this->makeAuthenticationCookie([
            'error' => $exception,
            'redirectUrl' => '/login',
        ]);
    }

    /**
     * @param $result
     * @return mixed
     */
    private function makeAuthenticationCookie($result)
    {
        return $result;
    }
}