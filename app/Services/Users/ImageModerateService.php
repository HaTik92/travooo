<?php
namespace App\Services\Users;
use App\Models\ActivityMedia\Media;
use App\Models\Posts\ApiReports;
use App\Models\Posts\SpamsPosts;
use App\Models\User\UsersBlocks;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ImageModerateService
{
    /**
     * @param $media
     * @param $post
     * @return bool
     */
    public function getImageData($media, $post) {
        $curl = curl_init();
        $postData = json_encode([
            'requests' => [
                [
                    'image' => [
                        'source' => [
                            'imageUri' => $media->url
                        ]
                    ],
                    'features' => [
                        [
                            'type' => 'SAFE_SEARCH_DETECTION',
                            'maxResults' =>  5
                        ]
                    ]
                ]
            ]
        ]);

        $visionApiKey = config('services.google-vision.key');

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://vision.googleapis.com/v1/images:annotate?key=" . $visionApiKey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: text/plain"
            ),
        ));
        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        $result = true;
        $message = "";
        $likelyMessage = "";
        if (isset($response->responses[0]) && isset($response->responses[0]->safeSearchAnnotation)) {
            $postId = $post->id;
            $postType = 'Post';
            if (!$post->text) {
                $postId = $media->id;
                $postType = 'Mediaupload';
            }
            foreach ($response->responses[0]->safeSearchAnnotation as $key => $data) {
                if ($data == 'VERY_LIKELY' || $data == 'LIKELY') {
                    $likelyMessage .= $key . "-" . strtolower(str_replace("_", " ", $data));
                    break;
                } elseif ($data == 'POSSIBLE') {
                    $message .= $key . "-" . strtolower($data);
                }
            }

            if ($likelyMessage) {
                $result = false;
                //Add data in spam manager api reports
                $apiReports = new ApiReports();
                $apiReports->post_id = $postId;
                $apiReports->user_id = $post->users_id;
                $apiReports->media_url = $media->url;
                $apiReports->text = $likelyMessage;
                $apiReports->type = $postType;
                $apiReports->save();

                $acceptCount = ApiReports::where('user_id', $post->users_id)->where('created_at', '>', Carbon::now()->subMonths(12))->count();

                if ($acceptCount > 5) {
                    $block_record = new UsersBlocks;
                    /* Load Information In UsersBlocks Record */
                    $block_record->users_id = $post->users_id;
                    $block_record->blocks_id = Auth::user()->id;
                    /* Save Block Record */
                    $block_record->save();
                }
            } elseif ($message) {
                $model = new SpamsPosts();
                $model->posts_id = $postId;
                $model->users_id = 0;
                $model->post_type = $postType;
                $model->report_type = 8;
                $model->report_text = $message;
                $model->save();
            }
        }
        return $result;
    }
}