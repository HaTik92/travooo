<?php

namespace App\Services\Users;

use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\User\User;
use App\Models\User\UsersContentLanguages;

class LanguagesService
{

    /**
     * @param $userId
     * @param array $language_list
     */
    public function addContentLanguages($userId, $language_list)
    {
        $this->deleteUserLanguages($userId);

        foreach ($language_list as $language) {
            $lng = new UsersContentLanguages();
            $lng->users_id = $userId;
            $lng->language_id = $language;

            $lng->save();
        }

        return true;
    }


    /**
     * @param $userId
     */
    public function deleteUserLanguages($userId)
    {
        return UsersContentLanguages::where('users_id', $userId)->delete();
    }

    // use in api
    public function __fetchSelectedUserLanguages($userId)
    {
        $selectedLanguageId = UsersContentLanguages::select('language_id')->where('users_id', $userId)->pluck('language_id')->toArray();
        if (count($selectedLanguageId) == 0) return [];
        return LanguagesSpoken::query()->with('transsingle')
            ->whereIn('id', $selectedLanguageId)
            ->get()->map(function ($language) {
                return  [
                    'id' => $language->id,
                    'text' => $language->transsingle->title,
                ];
            });
    }
}
