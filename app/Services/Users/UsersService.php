<?php

namespace App\Services\Users;

use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\User\User;
use App\Models\TripPlans\TripPlans;
use App\Models\Posts\Checkins;
use App\Models\Country\CountriesFollowers;
use App\Models\City\CitiesFollowers;
use App\Models\Destinations\CountriesByNationality;
use App\Models\Destinations\CitiesByNationality;
use App\Models\Destinations\GenericTopCities;
use App\Models\Destinations\GenericTopCountries;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UsersService
{

    /**
     * @param $userId
     * @return User|User[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function find($userId)
    {
        return User::find($userId);
    }

    /**
     * @param string $email
     * @return User|\Illuminate\Database\Eloquent\Model|null
     */
    public function findByEmail($email)
    {
        return User::where('email', $email)->first();
    }
    
    /**
     * @param string $email
     * @return User|\Illuminate\Database\Eloquent\Model|null
     */
    public function findByUsername($username)
    {
        return User::where('username', $username)->first();
    }

    /**
     * @param array $attributes
     * @param bool $deactivated
     * @return User|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $attributes, $deactivated = true)
    {
        $this->handleAttributes($attributes, $deactivated);

        $user = User::query()->where('email', $attributes['email'])->first();

        if ($user) {
            $attributes['type'] = $user->type;
        }

        return User::query()->updateOrCreate(['email' => $attributes['email']], $attributes);
    }

    /**
     * @param $userId
     * @param int $isApproved
     */
    public function approve($userId, $isApproved = 1)
    {

        $attributes = [
            'is_approved' => $isApproved,
            'approved_at' => now()
        ];

        $user = $this->find($userId);

        if(($user->type === User::TYPE_INVITED_EXPERT || $user->approved_type == 2) && $isApproved == 1){
            $attributes['approved_type'] = User::SPECIAL_CASE_EXPERT_APPROVED;
        }else if($user->approved_type === User::SPECIAL_CASE_EXPERT_APPROVED){
            $attributes['approved_type'] = User::SPECIAL_CASE_EXPERT_DISAPPROVED;
        }

        $this->update($userId, $attributes);
    }
    
    /**
     * @param $userId
     * @param string $ip
     */
    public function setIpAddress($userId, $ip)
    {
        $this->update($userId, [
            'ip_address' => $ip
        ]);
    }
    
    /**
     * @param $userId
     * @param string $step
     */
    public function setSteps($userId, $step)
    {
        $this->update($userId, [
            'register_steps' => $step
        ]);
    }


    /**
     * @param int $userId
     * @param array $attributes
     *
     * @return bool
     */
    public function update($userId, array $attributes)
    {
        return User::where('id', $userId)->update($attributes);
    }

    public function getInterests($userId)
    {
        return User::where('id', $userId)->select('interests')->first();
    }

    public function generateConfirmationCode(array &$attributes)
    {
        $attributes['confirmation_code'] = mt_rand(1000000, 9999999);
    }

    /**
     * @param array $attributes
     * @param bool $deactivated
     */
    protected function handleAttributes(array &$attributes, $deactivated = true)
    {
        $this->setStatus($attributes, $deactivated);
        $this->hashPassword($attributes);
        $this->generateConfirmationCode($attributes);
    }

    /**
     * @param array $attributes
     */
    protected function hashPassword(array &$attributes)
    {
        if (array_key_exists('password', $attributes)) {
            $attributes['password'] = Hash::make($attributes['password']);
        }
    }

    /**
     * @param array $attributes
     * @param $deactivated
     */
    protected function setStatus(array &$attributes, $deactivated) {
        if ($deactivated) {
            $attributes['status'] = User::STATUS_INACTIVE;
        } else {
            $attributes['status'] = User::STATUS_PENDING;
        }
    }

    /**
     * @param Model $user
     *
     * @return bool
     */
    public function forceDelete($user)
    {
        if ($user->forceDelete()) {
            return true;
        }
    }
    
    /**
     * @param string $ip
     * @return array
     */
     public function getUserSuggestionLocations($nationality){
         $result = [];
         $country_followers = collect([]);
         $city_followers = collect([]);

         $user_list = User::where('nationality', $nationality)->pluck('id');

        if(count($user_list) > 100) {
            $country_followers_list = CountriesFollowers::select('countries_id', DB::raw('count(*) as total'))
                ->whereIn('users_id', $user_list)
                ->where('created_at', '>=', Carbon::now()->subDays(30))
                ->groupBy('countries_id')->orderBy('id', 'DESC')->pluck('total', 'countries_id');

            foreach ($country_followers_list as $country_id => $total) {
                if ($total >= 100 && count($country_followers) < 10) {
                    $country_followers[] = $country_id;
                }
            }

            $city_followers_list = CitiesFollowers::select('cities_id', DB::raw('count(*) as total'))
                ->whereIn('users_id', $user_list)
                ->where('created_at', '>=', Carbon::now()->subDays(30))
                ->groupBy('cities_id')->orderBy('id', 'DESC')->take(10)->pluck('total', 'cities_id');

            foreach ($city_followers_list as $city_id => $total_count) {
                if ($total_count >= 100 && count($city_followers) < 10) {
                    $city_followers[] = $city_id;
                }
            }
        }


         if(count($country_followers) < 10){
             $country_count = 10 - count($country_followers);
             $countries_by_nationality = CountriesByNationality::where('nationality_id', $nationality)->whereNotIn('countries_id', $country_followers)->take($country_count)->pluck('countries_id');
             $final_by_nationality = $country_followers->concat($countries_by_nationality)->shuffle();

             if(count($final_by_nationality) < 10){
                    $final_country_count = 10 - count($final_by_nationality);
                    $generic_countries = GenericTopCountries::whereNotIn('countries_id', $final_by_nationality)->take($final_country_count)->pluck('countries_id');

                 $countries =  $generic_countries->concat($final_by_nationality)->shuffle();

             }else{
                 $countries = $final_by_nationality;
             }
         }else{
             $countries = $country_followers->shuffle();
         }

         if(count($city_followers) < 10){
             $city_count = 10 - count($city_followers);
             $cities_by_nationality = CitiesByNationality::where('nationality_id', $nationality)->whereNotIn('cities_id', $city_followers)->take($city_count)->pluck('cities_id');
             $final_cities_by_nationality = $city_followers->concat($cities_by_nationality)->shuffle();

             if(count($final_cities_by_nationality) < 10){
                 $final_city_count = 10 - count($final_cities_by_nationality);
                 $generic_cities = GenericTopCities::whereNotIn('cities_id', $final_cities_by_nationality)->take($final_city_count)->pluck('cities_id');

                 $cities =  $generic_cities->concat($final_cities_by_nationality)->shuffle();

             }else{
                 $cities = $final_cities_by_nationality;
             }
         }else{
             $cities = $city_followers->shuffle();
         }


         foreach($countries as $country_id){
            $country_info = Countries::find($country_id);

             $result[] = [
                 'id' => 'country-'.$country_info->id,
                 'type' => 'location',
                 'ob' => $country_info,
                 'name' => $country_info->transsingle->title,
                 'desc' => '',
                 'url' => @check_country_photo($country_info->getMedias[0]->url, 180)
             ];
         }

         
         foreach($cities as $city_id){
             $city_info = Cities::find($city_id);

             $result[] = [
                'id' => 'city-'.$city_info->id,
                'type' => 'location',
                'ob' => $city_info,
                'name' => $city_info->transsingle->title,
                'desc' => $city_info->country->transsingle->title,
                'url' => @check_city_photo( $city_info->getMedias[0]->url, 180)
             ];
         }

         shuffle($result);
         
        return  $result;
     }

}
