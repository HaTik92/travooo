<?php

namespace App\Services\Users;

use App\Models\Travelstyles\Travelstyles;
use App\Models\User\UsersTravelStyles;

class TravelStylesService
{
    /**
     * @param int $userId
     * @param array $travelStyleIds
     */
    public function setTravelStyles($userId, array $travelStyleIds)
    {
        foreach ($travelStyleIds as $travelStyleId) {
            if (Travelstyles::where('id', (int)$travelStyleId)->exists()) {
                $attributes = [
                    'conf_lifestyles_id' => (int)$travelStyleId,
                    'users_id' => (int)$userId
                ];
                UsersTravelStyles::updateOrCreate($attributes);
            }
        }
    }

    public function deleteTravelStyles($userId)
    {
    }

    // use in api
    public function __fetchSelectedTravelStyles($userId)
    {
        $selectedTravelStyles = UsersTravelStyles::select('conf_lifestyles_id')->where('users_id', $userId)->pluck('conf_lifestyles_id')->toArray();
        if (count($selectedTravelStyles) == 0) return [];
        return Travelstyles::query()->with('transsingle')
            ->whereIn('id', $selectedTravelStyles)
            ->get()->map(function ($entity) {
                return  [
                    'id' => $entity->getKey(),
                    'type' => 'travel_style',
                    'name' => $entity->transsingle->title,
                    'desc' => '',
                    'url' => @check_lifestyles_photo($entity->getMedias[0]->url)
                ];
            });
    }
}
