<?php

namespace App\Services\Users;

use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsPlaces\ExpertsPlaces;
use App\Models\Place\Place;
use App\Models\User\UsersFavourites;

class FavouritesService
{
    const TYPE_COUNTRY = 'country';
    const TYPE_PLACE   = 'place';
    const TYPE_CITY    = 'city';

    public static function getTypesMapping()
    {
        return [
            self::TYPE_PLACE   => Place::class,
            self::TYPE_COUNTRY => Countries::class,
            self::TYPE_CITY    => Cities::class
        ];
    }

    /**
     * @param int $userId
     * @param array $countryIds
     */
    public function setFavouritesCountries($userId, array $countryIds)
    {
        $this->setFavourites($userId, $countryIds, self::TYPE_COUNTRY);
    }

    /**
     * @param int $userId
     * @param array $placeIds
     */
    public function setFavouritesPlaces($userId, array $placeIds)
    {
        $this->setFavourites($userId, $placeIds, self::TYPE_PLACE);
    }

    /**
     * @param int $userId
     * @param array $cityIds
     */
    public function setFavouritesCities($userId, array $cityIds)
    {
        $this->setFavourites($userId, $cityIds, self::TYPE_CITY);
    }

    /**
     * @param int $userId
     * @param array $entityIds
     * @param string $type
     */
    protected function setFavourites($userId, array $entityIds, $type)
    {
        foreach ($entityIds as $entityId) {
            $attributes = [
                'fav_type' => $type,
                'fav_id' => $entityId,
                'users_id' => $userId
            ];

            UsersFavourites::updateOrCreate($attributes);
        }
    }

    /**
     * @param int $userId
     * @param string $type
     */
    public function deleteFavourites($userId, $type)
    {
        UsersFavourites::where('users_id', $userId)->where('fav_type', $type)->delete();
    }

    // use in api
    public function __fetchFavouritesCountryCity($userId)
    {
        $result = [];
        $selectedCountries  =  UsersFavourites::select('fav_id')->where('users_id', $userId)->where('fav_type', self::TYPE_COUNTRY)->pluck('fav_id')->toArray();
        $selectedCities     = UsersFavourites::select('fav_id')->where('users_id', $userId)->where('fav_type', self::TYPE_CITY)->pluck('fav_id')->toArray();
        if (count($selectedCountries)) {
            Countries::with('transsingle')->whereIn('id', $selectedCountries)->get()->each(function ($entity) use (&$result) {
                $result[] = [
                    'id' => 'country-' . $entity->getKey(),
                    'type' => 'location',
                    'name' => $entity->transsingle->title,
                    'desc' => '',
                    'url' => @check_country_photo($entity->getMedias[0]->url, 700)
                ];
            });
        }
        if (count($selectedCities)) {
            Cities::with('transsingle', 'country.transsingle')->whereIn('id', $selectedCities)->get()->each(function ($entity) use (&$result) {
                $result[] = [
                    'id' => 'city-' . $entity->getKey(),
                    'type' => 'location',
                    'name' => $entity->transsingle->title,
                    'desc' => $entity->country->transsingle->title,
                    'url' =>  @check_city_photo($entity->getMedias[0]->url, 700)
                ];
            });
        }

        foreach ($result as &$item) {
            if (isset($item['ob'])) unset($item['ob']);
        }
        return $result;
    }

    // use in api
    public function __fetchAreaExpertise($userId)
    {
        $result = [];;
        $cities = Cities::with('transsingle', 'country')->whereIn('id', ExpertsCities::select('cities_id')->where('users_id', $userId)->pluck('cities_id')->toArray());
        $countries = Countries::with('transsingle')->whereIn('id', ExpertsCountries::select('countries_id')->where('users_id', $userId)->pluck('countries_id')->toArray());
        $entities = $cities->get()->concat($countries->get())->shuffle();

        foreach ($entities as $entity) {
            if (get_class($entity) === Cities::class) {
                $result[] = [
                    'id' => 'city-' . $entity->getKey(),
                    'name' => @$entity->transsingle->title,
                    'flag' => @get_country_flag(@$entity->country) ?? null
                ];
            } else if (get_class($entity) === Countries::class) {
                $result[] = [
                    'id' => 'country-' . $entity->getKey(),
                    'name' => @$entity->transsingle->title,
                    'flag' => get_country_flag($entity)
                ];
            }
        }
        return $result;
    }

    // use in api
    public function __fetchFavouritesPlace($userId)
    {
        $result = [];
        $selectedPlaces  =  UsersFavourites::select('fav_id')->where('users_id', $userId)->where('fav_type', self::TYPE_PLACE)->pluck('fav_id')->toArray();
        if (count($selectedPlaces)) {
            Countries::with('transsingle', 'country.transsingle')->whereIn('id', $selectedPlaces)->get()->each(function ($entity) use (&$result) {
                $result[] = [
                    'id' => 'country-' . $entity->getKey(),
                    'type' => 'place',
                    'name' => @$entity->transsingle->title,
                    'desc' => @$entity->country->transsingle->title,
                    'url' => @check_place_photo($entity)
                ];
            });
        }

        foreach ($result as &$item) {
            if (isset($item['ob'])) unset($item['ob']);
        }
        return $result;
    }
}
