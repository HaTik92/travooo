<?php

namespace App\Services\Users;

use App\Models\City\CitiesFollowers;
use App\Models\Country\CountriesFollowers;
use App\Models\Place\PlaceFollowers;

class FollowingService
{

    /**
     * @param int $userId
     * @param array $countryIds
     */
    public function setFollowingCountries($userId, array $countryIds)
    {
        foreach ($countryIds as $countryId) {
            $attributes = [
                'users_id' => $userId,
                'countries_id' => $countryId,
            ];

            CountriesFollowers::updateOrCreate($attributes);
        }
    }

    /**
     * @param int $userId
     * @param array $placeIds
     */
    public function setFollowingPlaces($userId, array $placeIds)
    {
        foreach ($placeIds as $placeId) {
            $attributes = [
                'users_id' => $userId,
                'places_id' => $placeId,
            ];

            PlaceFollowers::updateOrCreate($attributes);
        }
    }

    /**
     * @param int $userId
     * @param array $cityIds
     */
    public function setFollowingCities($userId, array $cityIds)
    {
        foreach ($cityIds as $cityId) {
            $attributes = [
                'users_id' => $userId,
                'cities_id' => $cityId,
            ];

            CitiesFollowers::updateOrCreate($attributes);
        }
    }

    /**
     * @param int $userId
     * @param string $type
     */
    public function deleteFollowing($userId, $type)
    {
        switch ($type){
            case 'country':
                CountriesFollowers::where('users_id', $userId)->delete();
                break;
            case 'city':
                CitiesFollowers::where('users_id', $userId)->delete();
                break;
            case 'place':
                PlaceFollowers::where('users_id', $userId)->delete();
                break;
        }
    }

}
