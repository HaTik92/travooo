<?php

namespace App\Services\Users;

use App\Models\Country\Countries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsPlaces\ExpertsPlaces;
use App\Models\ExpertsTravelStyles\ExpertsTravelStyles;

class ExpertiseService
{
    const TYPE_COUNTRY = 'country';
    const TYPE_PLACE   = 'place';
    const TYPE_CITY    = 'city';

    /**
     * @param int $userId
     * @param array $countryIds
     */
    public function setExpertiseCountries($userId, array $countryIds)
    {
        foreach ($countryIds as $countryId) {
            $attributes = [
                'countries_id' => $countryId,
                'users_id' => $userId
            ];

            ExpertsCountries::updateOrCreate($attributes);
        }
    }

    /**
     * @param int $userId
     * @param array $placeIds
     */
    public function setExpertisePlaces($userId, array $placeIds)
    {
        foreach ($placeIds as $placeId) {
            $attributes = [
                'places_id' => $placeId,
                'users_id' => $userId
            ];

            ExpertsPlaces::updateOrCreate($attributes);
        }
    }

    /**
     * @param int $userId
     * @param array $cityIds
     */
    public function setExpertiseCities($userId, array $cityIds)
    {
        foreach ($cityIds as $cityId) {
            $attributes = [
                'cities_id' => $cityId,
                'users_id' => $userId
            ];

            ExpertsCities::updateOrCreate($attributes);
        }
    }

    /**
     * @param int $userId
     */
    public function getExpertiseCities($userId)
    {
        return  ExpertsCities::with('cities')->where('users_id', $userId)->get();
    }

    /**
     * @param int $userId
     */
    public function getExpertiseCountries($userId)
    {
        return  ExpertsCountries::where('users_id', $userId)->get();
    }

    /**
     * @param int $userId
     */
    public function getExpertiseArray($userId)
    {
        $cities_country = $this->getExpertiseCities($userId)->unique('cities.countries_id')->pluck('cities.countries_id')->toArray();
        $countries = ExpertsCountries::where('users_id', $userId)->pluck('countries_id')->toArray();

        return array_merge($countries, $cities_country);
    }


    public function setExpertiseTravelStyles($userId, array $travelStyleIds)
    {
        foreach ($travelStyleIds as $travelStyleId) {
            $attributes = [
                'travel_style_id' => $travelStyleId,
                'users_id' => $userId
            ];

            ExpertsTravelStyles::updateOrCreate($attributes);
        }
    }

    public function deleteExpertiseCities($userId)
    {
        ExpertsCities::where('users_id', $userId)->delete();
    }

    public function deleteExpertiseCountries($userId)
    {
        ExpertsCountries::where('users_id', $userId)->delete();
    }

    public function deleteExpertisePlaces($userId)
    {
        ExpertsPlaces::where('users_id', $userId)->delete();
    }

    public function deleteExpertiseTravelStyles($userId)
    {
        ExpertsTravelStyles::where('users_id', $userId)->delete();
    }


    public function getExpertise($userId)
    {
        return Countries::query()->select(['id'])
            ->with('trans')
            ->whereIn('id', $this->getExpertiseArray($userId))->get()->map(function ($item) {
                return [
                    'id' => $item->id,
                    'title' => _fieldValueFromTrans($item, 'title'),
                ];
            });
    }
}
