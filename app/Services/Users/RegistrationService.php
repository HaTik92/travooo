<?php

namespace App\Services\Users;

use App\Http\Requests\Frontend\User\Registration\StepRequest;
use App\Http\Requests\Frontend\User\Registration\StepThreeRequest;
use App\Http\Requests\Frontend\User\Registration\StepFourRequest;
use App\Http\Requests\Frontend\User\Registration\StepFiveRequest;
use App\Http\Requests\Frontend\User\Registration\StepSixRequest;
use App\Http\Requests\Frontend\User\Registration\StepSevenRequest;
use App\Http\Requests\Frontend\User\Registration\StepFiveExpertRequest;
use App\Http\Responses\AjaxResponse;
use App\Models\User\User;
use App\Notifications\Frontend\Auth\UserConfirmationCode;
use App\Tokens\RegistrationToken;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\Api\StepRequest as StepRequestApi;
use App\Http\Requests\Api\User\StepFourRequest as StepFourRequestApi;
use App\Http\Requests\Api\User\StepFiveRequest as StepFiveRequestApi;
use App\Http\Requests\Api\User\StepSixRequest as StepSixRequestApi;
use App\Http\Requests\Api\User\StepSevenRequest as StepSevenRequestApi;
use Illuminate\Support\Facades\Log;

class RegistrationService
{
    const MIN_SELECTION_FOR_EACH_STEP = 1;

    const MIN_FAVOURITES_ENTITIES_STEP_4_COUNT = 3;
    const MIN_FAVOURITES_ENTITIES_STEP_5_COUNT = 3;
    const MIN_FAVOURITES_ENTITIES_STEP_7_COUNT = 1;
    const MIN_FAVOURITES_ENTITIES_COUNT = 1;

    /**
     * @var UsersService
     */
    protected $usersService;

    /**
     * RegistrationService constructor.
     * @param UsersService $usersService
     */
    public function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
    }

    /**
     * @param User $user
     * @param string $password
     * @return RegistrationToken
     */
    public function generateRegistrationToken(User $user, $password)
    {
        return RegistrationToken::create($user, $password);
    }

    /**
     * @param string $value
     * @return RegistrationToken|null
     */
    public function getRegistrationToken($value)
    {
        try {
            return RegistrationToken::decrypt($value);
        } catch (DecryptException $e) {
            return null;
        }
    }

    /**
     * @param $userId
     * @param $code
     * @return bool
     */
    public function checkCode($userId, $code)
    {
        $user = $this->usersService->find($userId);

        if ($user && $user->confirmation_code === $code && Carbon::now()->diffInMinutes($user->updated_at) < 10) {
            $user->confirmed = 1;
            $user->save();
            $user->attachRole('3');

            return true;
        }

        return false;
    }

    /**
     * @param StepThreeRequest $request
     * @return bool
     */
    public function validateStepThreeRequest(StepThreeRequest $request)
    {
        if ($this->calculateFavouritesEntitiesCount($request) < self::MIN_FAVOURITES_ENTITIES_COUNT) {
            return false;
        }

        return true;
    }

    /**
     * @param StepFourRequestApi $request
     * @return bool
     */
    public function validateStepFourRequestApi(StepFourRequestApi $request)
    {
        if ($this->calculateFavouritesEntitiesCountApi($request) < self::MIN_FAVOURITES_ENTITIES_STEP_4_COUNT) {
            return false;
        }

        return true;
    }

    /**
     * @param StepFourRequest $request
     * @return bool
     */
    public function validateStepFourRequest(StepFourRequest $request)
    {
        if ($this->calculateFavouritesEntitiesCount($request) < self::MIN_FAVOURITES_ENTITIES_COUNT) {
            return false;
        }

        return true;
    }

    /**
     * @param StepFiveRequestApi $request
     * @return bool
     */
    public function validateStepFiveRequestApi(StepFiveRequestApi $request)
    {
        if ($this->calculateFavouritesEntitiesCountApi($request) < self::MIN_FAVOURITES_ENTITIES_STEP_5_COUNT) {
            return false;
        }

        return true;
    }

    /**
     * @param StepFiveRequest $request
     * @return bool
     */
    public function validateStepFiveRequest(StepFiveRequest $request)
    {
        return true;

        //TODO add real set of keywords
        $setOfKeywords = [];

        foreach ($request->input('interests') as $interest) {
            if (!in_array($interest, $setOfKeywords)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param StepFiveExpertRequest $request
     * @return bool
     */
    public function validateStepFiveExpertRequest(StepFiveExpertRequest $request)
    {
        if ($this->calculateFavouritesEntitiesCount($request) < self::MIN_FAVOURITES_ENTITIES_COUNT) {
            return false;
        }

        return true;
    }

    /**
     * @param StepSixRequestApi $request
     * @return bool
     */
    public function validateStepSixRequestApi(StepSixRequestApi $request)
    {
        return true;

        //TODO add real set of keywords
        $setOfKeywords = [];

        foreach ($request->input('interests') as $interest) {
            if (!in_array($interest, $setOfKeywords)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param StepSixRequest $request
     * @return bool
     */
    public function validateStepSixRequest(StepSixRequest $request)
    {
        if ($this->calculateFavouritesEntitiesCount($request) < self::MIN_FAVOURITES_ENTITIES_COUNT) {
            return false;
        }

        return true;
    }

    /**
     * @param StepSevenRequestApi $request
     * @return bool
     */
    public function validateStepSevenRequestApi(StepSevenRequestApi $request)
    {
        if ($this->calculateFavouritesEntitiesCountApi($request) < self::MIN_FAVOURITES_ENTITIES_STEP_7_COUNT) {
            return false;
        }

        return true;
    }

    /**
     * @param $userId
     * @return bool
     */
    public function activate($userId)
    {
        $attributes = [
            'status' => User::STATUS_ACTIVE
        ];

        $user = $this->usersService->find($userId);

        if ($user && $user->type === User::TYPE_INVITED_EXPERT) {
            $attributes['expert'] = 1;
            $attributes['type'] = 2;
            $attributes['approved_at'] = now();
            $attributes['approved_type'] = User::SPECIAL_CASE_EXPERT_APPROVED;
        }

        return $this->usersService->update($userId, $attributes);
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function loginById($userId)
    {
        $user = Auth::loginUsingId($userId);

        if (!$user) {
            return false;
        }

        return true;
    }

    /**
     * @param User $user
     */
    public function sendConfirmationEmail($user)
    {
        try {
            $user->notify(new UserConfirmationCode($user->confirmation_code));
            return true;
        } catch (\Throwable $e) {
            Log::emergency(json_encode([
                'confirmation_code' => $user->confirmation_code,
                'message'   => $e->getMessage(),
                '__class__' => get_class($e),
            ], JSON_PRETTY_PRINT));
            return false;
        }
    }

    /**
     * @param StepRequest $request
     * @return int
     */
    protected function calculateFavouritesEntitiesCount(StepRequest $request)
    {
        $count = 0;

        foreach ($request->onlyValidated() as $entities) {
            if ($entities) {
                $count += count(array_unique($entities));
            }
        }

        return $count;
    }

    /**
     * @param StepRequestApi $request
     * @return int
     */
    protected function calculateFavouritesEntitiesCountApi(StepRequestApi $request)
    {
        $count = 0;

        foreach ($request->onlyValidated() as $entities) {
            if ($entities) {
                $count += count(array_unique($entities));
            }
        }

        return $count;
    }
}
