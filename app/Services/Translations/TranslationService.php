<?php

namespace App\Services\Translations;

use App\Models\Access\Language\Languages;

use App\Models\LanguagesSpoken\LanguagesSpokenTranslation;
use Google\Cloud\Translate\V2\TranslateClient;

class TranslationService
{
    /** @var TranslateClient */
    protected $translate;

    public function __construct()
    {
        $this->translate = new TranslateClient([
            //'key' => env('GOOGLE_MAPS_KEY')
            'key' => 'AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
        ]);
    }

    /**
     * @param $string
     * @return int|null
     */
    public function getLanguageId($string)
    {
        $code = $this->getLanguageCode($string);
        $language = LanguagesSpokenTranslation::query()->whereRAW('LOWER(iso_code) = "'.$code.'"')->first();
        if ($language) {
            return $language->languages_spoken_id;
        }

        return 1;
    }

    public function getLanguageCode($string)
    {
        try {
            $result = $this->translate->detectLanguage($string);
        } catch (\Exception $e) {
            return '1';
        }

        if ($result['languageCode']) {
            return $result['languageCode'];
        }

        return '1';
    }
}
