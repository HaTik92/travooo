<?php

namespace App\Services\Discussions;

use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionTopics;
use App\Models\Place\Place;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DiscussionsService
{

    /**
     * @param int $discussion_id
     * @return bool
     */
    public function updatePopularCount($discussion_id, $is_add=true)
    {
        $discussion = Discussion::find($discussion_id);

        if($is_add){
            $discussion->popular_count = $discussion->popular_count + 1;
        }else{
            $discussion->popular_count = $discussion->popular_count - 1;
        }

        return $discussion->save();

    }

    /**
     * @return collaction
     */
    public function getTrendingTopic()
    {
        $user_loc_list = [];

        if(Auth::check()){
            $user_loc_list = get_users_from_my_location(Auth::user()->nationality);
        }

        $trending_topics = DiscussionTopics::selectRaw('*, count(topics) as t_count')
            ->whereHas('discussion', function($_q) use($user_loc_list){
                $_q->whereIn('users_id', $user_loc_list);
            })
            ->where('created_at', '>=', Carbon::now()->subDays(7))
            ->groupBy('topics')
            ->having('t_count', '>' , 5)
            ->orderBy('created_at', 'desc')
            ->take(20)->get()->pluck('topics');

        if(count($trending_topics) == 0){
            $trending_topics = DiscussionTopics::where('created_at', '>=', Carbon::now()->subDays(7))
                ->groupBy('topics')
                ->orderBy('created_at', 'desc')
                ->take(20)->get()->pluck('topics');
        }

        if(count($trending_topics) == 0){
            $trending_topics = DiscussionTopics::where('created_at', '>=', Carbon::now()->subDays(30))
                ->groupBy('topics')
                ->orderBy('created_at', 'desc')
                ->take(20)->get()->pluck('topics');
        }

        return $trending_topics;
    }

    /**
     * Get Discussion  trending destinations.
     * @param int $page
     * @return array
     */
    public function getTrendingDiscussions($page = 1)
    {

        $skip = ($page - 1) * 50;
        $blocked_users = blocked_users_list();
        $user_loc_list = [];
        if(Auth::check()){
            $user_loc_list = get_users_from_my_location(Auth::user()->nationality);
        }

        $places = [];

        $trending_discussions = Discussion::selectRaw('destination_type, destination_id, count(*) as total')
            ->where('destination_type', 'place')
            ->whereNotIn('users_id', $blocked_users)
            ->whereIn('users_id', $user_loc_list)
            ->where('created_at', '>=', Carbon::now()->subDays(60))
            ->groupBy('destination_id');

        if($trending_discussions->get()->count() < 3){
            $trending_discussions = Discussion::selectRaw('destination_type, destination_id, count(*) as total')
                ->where('destination_type', 'place')
                ->whereNotIn('users_id', $blocked_users)
                ->where('created_at', '>=', Carbon::now()->subDays(60))
                ->groupBy('destination_id');
        }

        if($trending_discussions->get()->count() >= 3){
            $trending_discussions->skip($skip)->take(50)->get()->each(function($discussion) use (&$places){
                $place = Place::find($discussion->destination_id);

                if($place){
                    $place->total = $discussion->total;
                    $places[] = $place;
                }

            });
        }

        return $places;
    }

}
