<?php

namespace App\Services\Discussions;

use App\Models\Discussion\DiscussionReplies;
use App\Models\Discussion\SpamsReplies;

class SpamsRepliesService
{
    const REPORT_TYPE_SPAM       = 'spam';
    const REPORT_TYPE_OTHER      = 'other';
    const REPORT_TYPE_SPAM_CODE  = 1;
    const REPORT_TYPE_OTHER_CODE = 2;

    /**
     * @param DiscussionReplies $reply
     * @param $type
     * @param null $text
     * @return bool
     * @throws \Exception
     */
    public function report(DiscussionReplies $reply, $type, $text = null)
    {
        $report = new SpamsReplies();

        switch ($type) {
            case self::REPORT_TYPE_SPAM:
                $report->report_type = self::REPORT_TYPE_SPAM_CODE;
                break;
            case self::REPORT_TYPE_OTHER:
                $report->report_type = self::REPORT_TYPE_OTHER_CODE;
                break;
        }

        if (!$report->report_type) {
            throw new \Exception('wrong type');
        }

        $report->users_id    = \Auth::id();
        $report->report_text = $text;
        $report->reply_id    = $reply->getKey();

        $report->save();

        return true;
    }

}
