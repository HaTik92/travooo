<?php

namespace App\Services\Discussions;

use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionLikes;
use App\Services\Discussions\DiscussionsService;

class DiscussionVotesService
{
    const UPVOTE_TYPE  = 1;
    const DOWNVOTE_TYPE = 0;

    /**
     * @var DiscussionsService
     */
    private $discussionsService;

    public function __construct(DiscussionsService $discussionsService)
    {
        $this->discussionsService = $discussionsService;
    }

    /**
     * @param int $userId
     * @return DiscussionLikes|\Illuminate\Database\Eloquent\Model|null
     */
    public function findById($userId, $discussion_id)
    {
        return DiscussionLikes::where('discussions_id', $discussion_id)->where('users_id', $userId)->first();
    }

    /**
     * @param int $discussionId
     * @param int $type
     * @param int $userId
     * @param bool $is_add
     * @return bool
     */
    public function setVotes($discussionId, $type, $userId, $is_add)
    {
        $discussionVotes = $this->findById($userId, $discussionId);

        if($is_add && $discussionVotes && $discussionVotes->vote == $type){
            $discussionVotes->forceDelete();

            $this->discussionsService->updatePopularCount($discussionId, false);

            return true;
        }

        if($type == self::UPVOTE_TYPE){
            log_user_activity('Discussion', 'upvote', $discussionId);
        }

        if(!$discussionVotes){
            DiscussionLikes::create([
                'discussions_id' => $discussionId,
                'users_id' => $userId,
                'vote' => $type
            ]);

            $this->discussionsService->updatePopularCount($discussionId);

            return true;
        }

        switch ($type) {
            case self::UPVOTE_TYPE:
                $discussionVotes->vote = self::UPVOTE_TYPE;
                break;
            case self::DOWNVOTE_TYPE:
                $discussionVotes->vote = self::DOWNVOTE_TYPE;
                break;
        }

        $discussionVotes->save();

        return true;
    }

}
