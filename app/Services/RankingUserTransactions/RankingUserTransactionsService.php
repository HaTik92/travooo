<?php

namespace App\Services\RankingUserTransactions;

use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Ranking\RankingUsersTransactions;
use App\Models\Reports\Reports;
use App\Models\Reviews\Reviews;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use Illuminate\Support\Facades\Log;

class RankingUserTransactionsService
{
    public function addPoints($entity, $userId, $amount)
    {
        try {
            $this->getUserTransaction($entity, $userId)->each(function ($rankingUserTransaction) use ($amount) {
                $rankingUserTransaction->points += (int) $amount;
                $rankingUserTransaction->save();
            });
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function subPoints($entity, $userId, $amount)
    {
        try {
            $this->getUserTransaction($entity, $userId)->each(function ($rankingUserTransaction) use ($amount) {
                $rankingUserTransaction->points -= (int) $amount;
                $rankingUserTransaction->save();
            });
        } catch (\Throwable $th) {
            //throw $th;
        }
    }


    private function getEntity($entity)
    {
        return [get_class($entity), $entity->id];
    }

    private function getCountriesId($entity)
    {
        switch (get_class($entity)) {
            case Reports::class:
            case Discussion::class:
                return $entity->countriesIds();
                break;

            case DiscussionReplies::class:
                return $entity->discussion->countriesIds();
                break;

            case TripPlans::class:
                return $entity->countries()->pluck('id');
                break;

            case TripPlaces::class:
                return collect([$entity->countries_id]);
                break;

            case Reviews::class:
                $place = $entity->place;
                if ($place) {
                    return collect([$place->countries_id]);
                }
                break;
        }
        Log::useDailyFiles(storage_path() . '/logs/missing_ranking_entity.log');
        Log::info('Add : ', $this->getEntity($entity), PHP_EOL . PHP_EOL);
        return null;
    }

    private function getUserTransaction($entity, $userId)
    {
        try {
            $countries_id = $this->getCountriesId($entity);
            if ($countries_id) {
                list($entityType, $entityId) = $this->getEntity($entity);

                $rankingUserTransactions = [];
                collect($countries_id)->each(function ($country_id) use (&$rankingUserTransactions, $userId, $entityType, $entityId) {
                    if ($entityType && $entityId) {
                        $rankingUserTransaction = RankingUsersTransactions::where([
                            'users_id' => $userId,
                            'entity_type' => $entityType,
                            'entity_id' => $entityId,
                            'countries_id' => $country_id,
                        ])->first();

                        if (!$rankingUserTransaction) {
                            $rankingUserTransaction = RankingUsersTransactions::create([
                                'users_id' => $userId,
                                'entity_type' => $entityType,
                                'entity_id' => $entityId,
                                'countries_id' => $country_id,
                                'points' => 0,
                            ]);
                        }

                        $rankingUserTransactions[] = $rankingUserTransaction;
                    }
                });

                return collect($rankingUserTransactions);
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
        return collect([]);
    }
}
