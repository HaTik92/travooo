<?php

namespace App\Services\Experts;

use App\Models\Country\Countries;
use App\Models\Country\CountriesTranslations;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsLikes;
use App\Models\Posts\PostsShares;
use App\Models\Posts\PostsViews;
use App\Models\Ranking\RankingUsersTransactions;
use App\Models\Referral\ReferralLinks;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsComments;
use App\Models\Reports\ReportsLikes;
use App\Models\Reports\ReportsShares;
use App\Models\TripPlans\TripPlans;
use App\Models\User\User;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Ranking\RankingService;
use App\Services\Users\ExpertiseService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardService
{
    public function getOverview($filter)
    {
        $user_followers_type = $image_post_type = $video_post_type = $engagement_types =
        $trip_plan_type = $link_type = $report_type = $referral_type = $overview_countries = $overview_countries_list = [];
        $comment_overview_label = $comment_overview_values = $comment_overview_type = [];
        $userId = Auth::user()->id;
        $filter = $filter - 1;
        $data = [
            'period' => date("M d", strtotime("-$filter day", time())) . ' - ' . date("M d", time()),
            'daysCount' => $filter == 6 ? 'week' : ($filter + 1) . ' days'
        ];


        $posts = Posts::where('users_id', $userId)->pluck('id');

        $countries = PostsViews::whereIn('posts_id', $posts)
            ->with('post_view_country.transsingle')
            ->select('nationality', DB::raw('count(*) as count'))
            ->where('created_at', '>=', Carbon::now()->subDays($filter))
            ->groupBy('nationality')
            ->orderBy('count', 'desc')
            ->get();

        foreach ($countries as $country) {
            if ($country->nationality) {
                $overview_countries[$country->post_view_country->iso_code] = $country->count;
                $overview_countries_list[] = [
                    'url' => get_country_flag_by_iso_code($country->post_view_country->iso_code),
                    'name' => $country->post_view_country->transsingle->title,
                    'count' => $country->count,
                ];
            }
        }


        $overview_countries = array_map(function ($key, $value) {
            return array($this->_convertCountryIsoCode($key), $value);
        }, array_keys($overview_countries), $overview_countries);

        $data['countries'] = [
            'values' => $overview_countries,
            'list' => $overview_countries_list,
        ];

        //Posts view
        $data['post_view'] = $this->_getOverviewByType('post_view', $filter);

        //Repoerts section
        $reports = Reports::where('users_id', $userId)
            ->pluck('id');

        $report_comments_query = ReportsComments::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter));
        $report_comments_count = $report_comments_query->count();
        $report_comments_overviews = $report_comments_query->groupBy('day')->get();

        $report_shares_query = ReportsShares::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter));
        $report_shares_count = $report_shares_query->count();
        $report_shares_overviews = $report_shares_query->groupBy('day')->get();

        $report_likes_query = ReportsLikes::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter));
        $report_likes_count = $report_likes_query->count();
        $report_likes_overviews = $report_likes_query->groupBy('day')->get();

        //Posts comment
        $posts_comment_query = PostsComments::select('*', DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter));
        $comment_overview_count = $posts_comment_query->count();
        $comment_overviews = $posts_comment_query->groupBy('day')->get();

        if ($comment_overview_count > 0) {
            foreach ($comment_overviews as $p_comment) {
                $comment_overview_type[$p_comment->day] = $p_comment->total;
                $engagement_types[$p_comment->day] = $p_comment->total;
            }
        }

        if ($report_comments_count > 0) {
            foreach ($report_comments_overviews as $report_comments_overview) {
                if (array_key_exists($report_comments_overview->day, $engagement_types)) {
                    $engagement_types[$report_comments_overview->day] = $engagement_types[$report_comments_overview->day] + $report_comments_overview->total;
                } else {
                    $engagement_types[$report_comments_overview->day] = $report_comments_overview->total;
                }

                if (array_key_exists($report_comments_overview->day, $comment_overview_type)) {
                    $comment_overview_type[$report_comments_overview->day] = $comment_overview_type[$report_comments_overview->day] + $report_comments_overview->total;
                } else {
                    $comment_overview_type[$report_comments_overview->day] = $report_comments_overview->total;
                }
            }
        }

        if ($report_shares_count > 0) {
            foreach ($report_shares_overviews as $report_shares_overview) {
                if (array_key_exists($report_shares_overview->day, $engagement_types)) {
                    $engagement_types[$report_shares_overview->day] = $engagement_types[$report_shares_overview->day] + $report_shares_overview->total;
                } else {
                    $engagement_types[$report_shares_overview->day] = $report_shares_overview->total;
                }
            }
        }


        if ($report_likes_count > 0) {
            foreach ($report_likes_overviews as $report_likes_overview) {
                if (array_key_exists($report_likes_overview->day, $engagement_types)) {
                    $engagement_types[$report_likes_overview->day] = $engagement_types[$report_likes_overview->day] + $report_likes_overview->total;
                } else {
                    $engagement_types[$report_likes_overview->day] = $report_likes_overview->total;
                }
            }
        }


        for ($day = $filter; $day >= 0; $day--) {
            $comment_overview_label[] = Carbon::now()->subDays($day)->format('d');
            if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $comment_overview_type))
                $comment_overview_values[] = $comment_overview_type[Carbon::now()->subDays($day)->format('Y-m-d')];
            else
                $comment_overview_values[] = 0;
        }

        $data['post_comment'] = [
            'label' => $comment_overview_label,
            'values' => $comment_overview_values,
            'count' => $comment_overview_count + $report_comments_count
        ];

        //Posts engagements

        $posts_like_query = PostsLikes::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter));
        $posts_shere_query = PostsShares::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter));

        $posts_like_count = $posts_like_query->count();
        $posts_shere_count = $posts_shere_query->count();

        $posts_likes = $posts_like_query->groupBy('day')->get();
        $posts_sheres = $posts_shere_query->groupBy('day')->get();

        if ($posts_like_count > 0) {
            foreach ($posts_likes as $posts_like) {
                if (array_key_exists($posts_like->day, $engagement_types)) {
                    $engagement_types[$posts_like->day] = $engagement_types[$posts_like->day] + $posts_like->total;
                } else {
                    $engagement_types[$posts_like->day] = $posts_like->total;
                }
            }
        }
        if ($posts_shere_count > 0) {
            foreach ($posts_sheres as $posts_shere) {
                if (array_key_exists($posts_shere->day, $engagement_types)) {
                    $engagement_types[$posts_like->day] = $engagement_types[$posts_like->day] + $posts_shere->total;
                } else {
                    $engagement_types[$posts_shere->day] = $posts_shere->total;
                }
            }
        }

        for ($day = $filter; $day >= 0; $day--) {
            $engagement_labels[] = "";
            if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $engagement_types))
                $engagement_values[] = $engagement_types[Carbon::now()->subDays($day)->format('Y-m-d')];
            else
                $engagement_values[] = 0;
        }

        $data['post_engagement'] = [
            'label' => $engagement_labels,
            'values' => $engagement_values,
            'count' => $comment_overview_count + $posts_like_count + $posts_shere_count + $report_comments_count + $report_shares_count + $report_likes_count
        ];

        //User followers
        $data['user_followers'] = $this->_getFollowByType('follow', $filter);

        //User unFollowers
        $data['user_unfollowers'] = $this->_getFollowByType('unfollow', $filter);

        //Rescent followers
        $resent_followers = UsersFollowers::whereHas('follower')
            ->with('follower:id,profile_picture')
            ->where('users_id', $userId)
            ->where('follow_type', 1)
            ->where('created_at', '>=', Carbon::now()->subDays($filter));

        $data['resent_followers'] = [
            'count' => (clone $resent_followers)->count(),
            'list' => $resent_followers->orderBy('id', 'DESC')->limit(8)->get()->map(function ($item, $key) {
                $item->follower->profile_picture = check_profile_picture($item->follower->profile_picture);
                return $item->follower;
            })
        ];

        //Texts posts
        $data['post_text'] = $this->_getOverviewByType('post_text', $filter);

        //Image posts
        $data['post_image'] = $this->_getOverviewByType('post_image', $filter);

        //Video posts
        $data['post_video'] = $this->_getOverviewByType('post_video', $filter);

        //Links
        $data['links'] = $this->_getOverviewByType('links', $filter);

        //Trip plan
        $data['trip_plan'] = $this->_getOverviewByType('trip_plan', $filter);

        //Reports
        $data['reports'] = $this->_getOverviewByType('reports', $filter);

        //Referrals achieved
        $referrals_query = ReferralLinks::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->where('users_id', $userId)->where('created_at', '>=', Carbon::now()->subDays($filter));
        $referrals_count = $referrals_query->count();
        $referrals = $referrals_query->groupBy('day')->get();

        if ($referrals_count > 0) {
            foreach ($referrals as $referral) {
                $referral_type[$referral->day] = $referral->total;
            }
        }

        for ($day = $filter; $day >= 0; $day--) {
            $referral_label[] = "";
            if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $referral_type))
                $referral_values[] = $referral_type[Carbon::now()->subDays($day)->format('Y-m-d')];
            else
                $referral_values[] = 0;
        }

        $data['referral'] = [
            'label' => $referral_label,
            'values' => $referral_values,
            'count' => $referrals_count
        ];

        return $data;
    }

    public function getGlobalImpact()
    {
        $expertiseService = app(ExpertiseService::class);
        $rankingService = app(RankingService::class);

        $user = auth()->user();

        $leaderboardLists = [];
        $levelUp = [];

        $expCountriesIds = $expertiseService->getExpertiseArray($user->id);

        $progressData = $rankingService->getProgressData($user->id);

        if ($progressData['next_badge']) {
            $hasNextBadge = true;
        } else {
            $hasNextBadge = false;
        }

        $userAreaExpertise = $rankingService->getUsersByBadgeId($progressData['current_badge']->badges_id)
            ->load('user')
            ->filter(function ($item, $key) use ($user) { return $item->users_id != $user->id; })
            ->all();

        $countriesIdsOrder = RankingUsersTransactions::where('users_id', $user->id)
            ->groupBy('countries_id')
            ->selectRaw('sum(points) as points, countries_id')
            ->orderBy('points', 'desc')
            ->pluck('countries_id')
            ->toArray();

        foreach ($countriesIdsOrder as $value) {
            $leaderboardLists[$value] = [];
        }

        $expLists = CountriesTranslations::whereIn('countries_id', $expCountriesIds)
            ->select('countries_id', 'title')
            ->pluck('title', 'countries_id')
            ->sortBy(function ($item, $key) use ($countriesIdsOrder) { return array_search($key, $countriesIdsOrder); })
            ->toArray();

        $leadersIds = RankingUsersTransactions::whereIn('countries_id', $expCountriesIds)
            ->groupBy('users_id')
            ->where('created_at', '>=', Carbon::now()->subDays(183)) //6 month
            ->pluck('users_id');

        $leadersInteractionsCount = RankingUsersTransactions::whereIn('countries_id', $expCountriesIds)
            ->whereIn('users_id', $leadersIds)
            ->groupBy('users_id', 'countries_id')
            ->with('user:id,name,profile_picture')
            ->selectRaw('sum(points) as points, users_id, countries_id')
            ->get();

        $leadersFollowersCount = UsersFollowers::selectRaw('count(id) as counts, users_id')
            ->whereIn('users_id', $leadersIds)
            ->where('follow_type', 1)
            ->groupBy('users_id')
            ->pluck('counts', 'users_id');

        $maxInteractions = [];
        $maxFollowers = [];

        foreach ($leadersInteractionsCount as $leader) {
            $followersCount = $leadersFollowersCount[$leader->users_id] ?? 0;
            $leaderboardLists[$leader->countries_id][] = [
                'user' => $leader->user,
                'interactions_count' => intval($leader->points),
                'followers_count' =>  intval($followersCount),
                'int_count' => $leader->points + $followersCount,
            ];

            $maxInteractions[$leader->countries_id] = max($maxInteractions[$leader->countries_id] ?? 0, $leader->points);
            $maxFollowers[$leader->countries_id] = max($maxFollowers[$leader->countries_id] ?? 0, $followersCount);
        }

        foreach ($leaderboardLists as $countryId => $list) {
            usort($list, function ($a, $b) {
                return ($a['int_count'] > $b['int_count']) ? -1 : 1;
            });

            foreach ($list as $key => $leader) {
                $leaderboardLists[$countryId][$key]['interactions_progress'] =
                    $leader['interactions_count'] * 100 / ($maxInteractions[$countryId] ?: 1);
                $leaderboardLists[$countryId][$key]['followers_progress'] =
                    $leader['followers_count'] * 100 / ($maxFollowers[$countryId] ?: 1);
                unset($leaderboardLists[$countryId][$key]['int_count']);
                if ($leader['user']['id'] == $user->id) {
                    $levelUp[$countryId] = $key + 1;
                }
            }
        }

        asort($levelUp);

        return [
            'title' => $hasNextBadge ? 'Your next Badge' : 'Your current Badge',
            'badge_url' => $hasNextBadge ? url($progressData['next_badge']->url) : url($progressData['current_badge']->badge->url),
            'badge_name' => $hasNextBadge ? $progressData['next_badge']->name : $progressData['current_badge']->badge->name,
            'overall_progress' => $progressData['overall_progress'],
            'has_next_badge' => $hasNextBadge,
            'users_with_same_badge' => [
                'total' => count($userAreaExpertise),
                'users' => collect($userAreaExpertise)->take(3)->map(function ($item) {
                    return [
                        'id' => $item->user->id,
                        'username' => $item->user->username,
                        'name' => $item->user->name,
                        'profile_picture' => check_profile_picture($item->user->profile_picture)
                    ];
                }),
            ],
            'level' => [
                'rank' => reset($levelUp)?reset($levelUp):1,
                'title' => key($levelUp)?$expLists[key($levelUp)]:reset($expLists)
            ],
            'exp_lists' => $expLists,
            'leaderboard_lists' => $leaderboardLists
        ];
    }

    /**
     * @param string $type
     * @param int $filter
     * @return array
     */
    public function _getOverviewByType($type, $filter, $tab_type = '')
    {
        $overview_type = [];
        switch ($type) {
            case 'post_view':
                //Posts views
                $posts = Posts::where('users_id', Auth::user()->id)->pluck('id');
                $posts_views_query = PostsViews::select('*', DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter));
                $overview_count = $posts_views_query->count();
                $overviews = $posts_views_query->orderBy('created_at', 'DESC')->groupBy('day')->get();
                break;
            case 'post_comment':
                $posts = Posts::where('users_id', Auth::user()->id)->pluck('id');
                $posts_comment_query = PostsComments::select('*', DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter));
                $overview_count = $posts_comment_query->count();
                $overviews = $posts_comment_query->groupBy('day')->get();
                break;
            case 'post_text':
                if ($tab_type != '') {
                    $posts_info = Posts::withCount('views')->withCount('comments')->withCount('shares')->withCount('likes')
                        ->where('users_id', Auth::user()->id)->whereDoesntHave('medias')
                        ->where('created_at', '>=', Carbon::now()->subDays($filter))->orderBy('created_at', 'DESC')->get();
                } else {
                    $texts_posts_query = Posts::select('*', DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->where('users_id', Auth::user()->id)->whereDoesntHave('medias')->where('created_at', '>=', Carbon::now()->subDays($filter));
                    $overview_count = $texts_posts_query->count();
                    $overviews = $texts_posts_query->groupBy('day')->get();
                }
                break;
            case 'post_image':
                if ($tab_type != '') {
                    $posts_info = Posts::withCount('views')->withCount('comments')->withCount('shares')->withCount('likes')
                        ->where('users_id', Auth::user()->id)->where('created_at', '>=', Carbon::now()->subDays($filter))
                        ->whereHas('medias', function ($query) {
                            $query->whereHas('media', function ($mediaQuery) {
                                $mediaQuery->where(\DB::raw('RIGHT(url, 4)'), '!=', '.mp4');
                            });
                        })
                        ->orderBy('created_at', 'DESC')->get();
                } else {
                    $image_posts_query = Posts::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->where('users_id', Auth::user()->id)->where('created_at', '>=', Carbon::now()->subDays($filter))
                        ->whereHas('medias', function ($query) {
                            $query->whereHas('media', function ($mediaQuery) {
                                $mediaQuery->where(\DB::raw('RIGHT(url, 4)'), '!=', '.mp4');
                            });
                        });

                    $overview_count = $image_posts_query->count();
                    $overviews = $image_posts_query->groupBy('day')->get();

                }
                break;
            case 'post_video':
                if ($tab_type != '') {
                    $posts_info = Posts::withCount('views')->withCount('comments')->withCount('shares')->withCount('likes')
                        ->where('users_id', Auth::user()->id)->where('created_at', '>=', Carbon::now()->subDays($filter))
                        ->whereHas('medias', function ($query) {
                            $query->whereHas('media', function ($mediaQuery) {
                                $mediaQuery->where(\DB::raw('RIGHT(url, 4)'), '=', '.mp4');
                            });
                        })
                        ->orderBy('created_at', 'DESC')->get();
                } else {
                    $video_posts_query = Posts::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->where('users_id', Auth::user()->id)->where('created_at', '>=', Carbon::now()->subDays($filter))
                        ->whereHas('medias', function ($query) {
                            $query->whereHas('media', function ($mediaQuery) {
                                $mediaQuery->where(\DB::raw('RIGHT(url, 4)'), '=', '.mp4');
                            });
                        });

                    $overview_count = $video_posts_query->count();
                    $overviews = $video_posts_query->groupBy('day')->get();
                }
                break;
            case 'trip_plan':
                if ($tab_type != '') {
                    $posts_info = TripPlans::withCount('comments')->withCount('shares')->withCount('likes')
                        ->where('users_id', Auth::user()->id)
                        ->where('created_at', '>=', Carbon::now()->subDays($filter))->orderBy('created_at', 'DESC')->get();
                } else {
                    $trip_plans_query = TripPlans::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->where('users_id', Auth::user()->id)->where('created_at', '>=', Carbon::now()->subDays($filter));
                    $overview_count = $trip_plans_query->count();
                    $overviews = $trip_plans_query->groupBy('day')->get();
                }
                break;
            case 'reports':
                if ($tab_type != '') {
                    $posts_info = Reports::withCount('views')->withCount('comments')->withCount('shares')->withCount('likes')
                        ->where('users_id', Auth::user()->id)
                        ->where('created_at', '>=', Carbon::now()->subDays($filter))->orderBy('created_at', 'DESC')->get();
                } else {
                    $reports_query = Reports::select(DB::raw('DATE(created_at) as day'))->withCount('views')->where('users_id', Auth::user()->id)->where('created_at', '>=', Carbon::now()->subDays($filter));
                    $overview_count = $reports_query->count();
                    $overviews = $reports_query->groupBy('day')->get();
                }
                break;
            case 'links':
                if ($tab_type != '') {
                    $posts_info = ReferralLinks::withCount('clicks')->withCount('exits')
                        ->where('users_id', Auth::user()->id)->where('created_at', '>=', Carbon::now()->subDays($filter))->orderBy('created_at', 'DESC')->get();
                } else {
                    $links_query = ReferralLinks::select(DB::raw('count(*) as total'), DB::raw('DATE(referral_links_views.created_at) as day'))
                        ->leftJoin('referral_links_views', 'referral_links_views.links_id', '=', 'referral_links.id')
                        ->where('referral_links.users_id', Auth::user()->id)->where('referral_links_views.created_at', '>=', Carbon::now()->subDays($filter));

                    $overview_count = $links_query->count();
                    $overviews = $links_query->groupBy('day')->get();
                }
                break;
        }


        if ($tab_type == '') {
            if ($type == 'reports') {
                $overview_count = 0;
                foreach ($overviews as $overview) {
                    $overview_type[$overview->day] = $overview->views_count;
                    $overview_count = $overview_count + $overview->views_count;
                }
            } else {
                if ($overview_count > 0) {
                    foreach ($overviews as $overview) {
                        $overview_type[$overview->day] = $overview->total;
                    }
                }
            }

            for ($day = $filter; $day >= 0; $day--) {
                $overview_label[] = Carbon::now()->subDays($day)->format('d');
                if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $overview_type))
                    $overview_values[] = $overview_type[Carbon::now()->subDays($day)->format('Y-m-d')];
                else
                    $overview_values[] = 0;
            }

            $data = [
                'label' => $overview_label,
                'values' => $overview_values,
                'count' => $overview_count,
                'last_update' => count($overviews) > 0 ? diffForHumans($overviews[0]->created_at) : ''
            ];

            return $data;
        } else {
            $data['count'] = count($posts_info);
            $data['post_info'] = $posts_info;
            return $data;
        }
    }

    /**
     * @param string $type
     * @param int $filter
     * @return array
     */
    public function _getFollowByType($type, $filter)
    {
        $follow_type = [];
        $all_count = 0;
        $userFollowType = $type == 'follow' ? 1 : 2;

        $total_count = UsersFollowers::where('users_id', Auth::user()->id)->where('follow_type', $userFollowType)->count();
        $daily_count = UsersFollowers::where('users_id', Auth::user()->id)->where('follow_type', $userFollowType)->whereDate('updated_at', Carbon::today())->count();
        $follows = UsersFollowers::select(DB::raw('count(*) as total'), DB::raw('DATE(updated_at) as day'))->where('users_id', Auth::user()->id)->where('follow_type', $userFollowType)->where('updated_at', '>=', Carbon::now()->subDays($filter))->groupBy('day')->get();
        $followers_list = UsersFollowers::where('users_id', Auth::user()->id)->where('follow_type', $userFollowType)->where('updated_at', '>=', Carbon::now()->subDays($filter))->take(10)->get();


        if (count($follows) > 0) {
            foreach ($follows as $follow) {
                $follow_type[$follow->day] = $follow->total;
                $all_count += $follow->total;
            }
        }

        for ($day = $filter; $day >= 0; $day--) {
            $follow_label[] = Carbon::now()->subDays($day)->format('d');
            $total_values[] = $total_count;
            if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $follow_type)) {
                $daily_values[] = $follow_type[Carbon::now()->subDays($day)->format('Y-m-d')];

            } else {
                $daily_values[] = 0;
            }
        }

        $data['follow_label'] = $follow_label;
        $data['total_values'] = $total_values;
        $data['daily_values'] = $daily_values;
        $data['daily_count'] = $daily_count;
        $data['follow_count'] = $all_count;
        $data['followers_list'] = $followers_list;

        return $data;

    }

    public function _convertCountryIsoCode($iso)
    {
        $iso_code_list = ["BD" => "BGD", "BE" => "BEL", "BF" => "BFA", "BG" => "BGR", "BA" => "BIH", "BB" => "BRB", "WF" => "WLF", "BL" => "BLM", "BM" => "BMU", "BN" => "BRN", "BO" => "BOL", "BH" => "BHR", "BI" => "BDI", "BJ" => "BEN", "BT" => "BTN", "JM" => "JAM", "BV" => "BVT", "BW" => "BWA", "WS" => "WSM", "BQ" => "BES", "BR" => "BRA", "BS" => "BHS", "JE" => "JEY",
            "BY" => "BLR", "BZ" => "BLZ", "RU" => "RUS", "RW" => "RWA", "RS" => "SRB", "TL" => "TLS", "RE" => "REU", "TM" => "TKM", "TJ" => "TJK", "RO" => "ROU", "TK" => "TKL", "GW" => "GNB", "GU" => "GUM", "GT" => "GTM", "GS" => "SGS", "GR" => "GRC", "GQ" => "GNQ", "GP" => "GLP", "JP" => "JPN", "GY" => "GUY", "GG" => "GGY", "GF" => "GUF", "GE" => "GEO",
            "GD" => "GRD", "GB" => "GBR", "GA" => "GAB", "SV" => "SLV", "GN" => "GIN", "GM" => "GMB", "GL" => "GRL", "GI" => "GIB", "GH" => "GHA", "OM" => "OMN", "TN" => "TUN", "JO" => "JOR", "HR" => "HRV", "HT" => "HTI", "HU" => "HUN", "HK" => "HKG", "HN" => "HND", "HM" => "HMD", "VE" => "VEN", "PR" => "PRI", "PS" => "PSE", "PW" => "PLW", "PT" => "PRT",
            "SJ" => "SJM", "PY" => "PRY", "IQ" => "IRQ", "PA" => "PAN", "PF" => "PYF", "PG" => "PNG", "PE" => "PER", "PK" => "PAK", "PH" => "PHL", "PN" => "PCN", "PL" => "POL", "PM" => "SPM", "ZM" => "ZMB", "EH" => "ESH", "EE" => "EST", "EG" => "EGY", "ZA" => "ZAF", "EC" => "ECU", "IT" => "ITA", "VN" => "VNM", "SB" => "SLB", "ET" => "ETH", "SO" => "SOM",
            "ZW" => "ZWE", "SA" => "SAU", "ES" => "ESP", "ER" => "ERI", "ME" => "MNE", "MD" => "MDA", "MG" => "MDG", "MF" => "MAF", "MA" => "MAR", "MC" => "MCO", "UZ" => "UZB", "MM" => "MMR", "ML" => "MLI", "MO" => "MAC", "MN" => "MNG", "MH" => "MHL", "MK" => "MKD", "MU" => "MUS", "MT" => "MLT", "MW" => "MWI", "MV" => "MDV", "MQ" => "MTQ", "MP" => "MNP",
            "MS" => "MSR", "MR" => "MRT", "IM" => "IMN", "UG" => "UGA", "TZ" => "TZA", "MY" => "MYS", "MX" => "MEX", "IL" => "ISR", "FR" => "FRA", "IO" => "IOT", "SH" => "SHN", "FI" => "FIN", "FJ" => "FJI", "FK" => "FLK", "FM" => "FSM", "FO" => "FRO", "NI" => "NIC", "NL" => "NLD", "NO" => "NOR", "NA" => "NAM", "VU" => "VUT", "NC" => "NCL", "NE" => "NER",
            "NF" => "NFK", "NG" => "NGA", "NZ" => "NZL", "NP" => "NPL", "NR" => "NRU", "NU" => "NIU", "CK" => "COK", "XK" => "XKX", "CI" => "CIV", "CH" => "CHE", "CO" => "COL", "CN" => "CHN", "CM" => "CMR", "CL" => "CHL", "CC" => "CCK", "CA" => "CAN", "CG" => "COG", "CF" => "CAF", "CD" => "COD", "CZ" => "CZE", "CY" => "CYP", "CX" => "CXR", "CR" => "CRI",
            "CW" => "CUW", "CV" => "CPV", "CU" => "CUB", "SZ" => "SWZ", "SY" => "SYR", "SX" => "SXM", "KG" => "KGZ", "KE" => "KEN", "SS" => "SSD", "SR" => "SUR", "KI" => "KIR", "KH" => "KHM", "KN" => "KNA", "KM" => "COM", "ST" => "STP", "SK" => "SVK", "KR" => "KOR", "SI" => "SVN", "KP" => "PRK", "KW" => "KWT", "SN" => "SEN", "SM" => "SMR", "SL" => "SLE",
            "SC" => "SYC", "KZ" => "KAZ", "KY" => "CYM", "SG" => "SGP", "SE" => "SWE", "SD" => "SDN", "DO" => "DOM", "DM" => "DMA", "DJ" => "DJI", "DK" => "DNK", "VG" => "VGB", "DE" => "DEU", "YE" => "YEM", "DZ" => "DZA", "US" => "USA", "UY" => "URY", "YT" => "MYT", "UM" => "UMI", "LB" => "LBN", "LC" => "LCA", "LA" => "LAO", "TV" => "TUV", "TW" => "TWN",
            "TT" => "TTO", "TR" => "TUR", "LK" => "LKA", "LI" => "LIE", "LV" => "LVA", "TO" => "TON", "LT" => "LTU", "LU" => "LUX", "LR" => "LBR", "LS" => "LSO", "TH" => "THA", "TF" => "ATF", "TG" => "TGO", "TD" => "TCD", "TC" => "TCA", "LY" => "LBY", "VA" => "VAT", "VC" => "VCT", "AE" => "ARE", "AD" => "AND", "AG" => "ATG", "AF" => "AFG", "AI" => "AIA",
            "VI" => "VIR", "IS" => "ISL", "IR" => "IRN", "AM" => "ARM", "AL" => "ALB", "AO" => "AGO", "AQ" => "ATA", "AS" => "ASM", "AR" => "ARG", "AU" => "AUS", "AT" => "AUT", "AW" => "ABW", "IN" => "IND", "AX" => "ALA", "AZ" => "AZE", "IE" => "IRL", "ID" => "IDN", "UA" => "UKR", "QA" => "QAT", "MZ" => "MOZ"
        ];

        return $iso_code_list[$iso];
    }
}