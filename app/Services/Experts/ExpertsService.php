<?php

namespace App\Services\Experts;

use App\Events\ChatSendPrivateMessageEvent;
use App\Events\ExpertStatusUpdateEvent;
use App\Models\Notifications\Notifications;
use App\Models\User\User;
use App\Notifications\ApproveExpertUser;
use App\Notifications\DisapproveExpertUser;
use App\Services\Ranking\RankingService;
use App\Services\RankingPoints\RankingPointsService;
use App\Services\Users\UsersService;
use Illuminate\Support\Facades\Auth;

class ExpertsService
{
    /**
     * @var RankingPointsService
     */
    protected $rankingPointsService;

    /**
     * @var UsersService
     */
    protected $usersService;
    /**
     * @var RankingService
     */
    private $rankingService;

    /**
     * ExpertsService constructor.
     * @param RankingPointsService $rankingPointsService
     * @param UsersService $usersService
     */
    public function __construct(RankingPointsService $rankingPointsService, UsersService $usersService, RankingService $rankingService)
    {
        $this->rankingPointsService = $rankingPointsService;
        $this->usersService = $usersService;
        $this->rankingService = $rankingService;
    }

    /**
     * @param int $userId
     * @param bool $badge
     */
    public function approve($userId, $badgeId = null)
    {
        if ($badgeId) {
            $this->rankingService->presetBadge($userId, (int)$badgeId);
        }

        $this->usersService->approve($userId);
        $this->usersService->update($userId, [
            'type' => User::TYPE_EXPERT,
            'expert' => 1
        ]);

        notify($userId, 'approve', 'approve', $badgeId, $userId);

        /** @var User $user */
        $user = User::find($userId);
        $user->notify(new ApproveExpertUser());

        //broadcast(new ExpertStatusUpdateEvent($userId, 1, $pointsAmount));
    }

    /**
     * @param int $userId
     * @param string $message
     */
    public function disapprove($userId, $message = '')
    {
        $this->usersService->approve($userId, 0);
        $this->usersService->update($userId, [
            'expert' => 0,
            'disapprove_reason' => $message
        ]);

        notify($userId, 'disapprove', 'disapprove', '', $userId);

        /** @var User $user */
        $user = User::find($userId);
        $user->notify(new DisapproveExpertUser());

        //broadcast(new ExpertStatusUpdateEvent($userId, 0));
    }

    /**
     * @param User $user
     * @return int
     */
    public function getPresetPoints($user)
    {
        if (!$user->inviteExpertLink) {
            return 0;
        }

        return $user->inviteExpertLink->points;
    }

    /**
     * @param User $user
     * @return int
     */
    public function getPresetBadge($user)
    {
        if (!$user->badge) {
            return '';
        }

        return $user->badge->badge->name;
    }

    /**
     * @param $userId
     * @return bool
     */
    public function addPresetPoints($userId)
    {
        $user = $this->usersService->find($userId);

        if (!$user) {
            return false;
        }

        $pointsAmount = $this->getPresetPoints($user);

        if (!$pointsAmount) {
            return false;
        }

        $this->rankingPointsService->addPoints($userId, $pointsAmount);

        return true;
    }

}
