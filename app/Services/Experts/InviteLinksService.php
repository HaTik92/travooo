<?php

namespace App\Services\Experts;

use App\Models\InviteExpertLinks\InviteExpertLink;

class InviteLinksService
{
    public static function getList()
    {
        return InviteExpertLink::all()->pluck('name', 'id');
    }

    /**
     * @param string $name
     * @param int $points
     * @return InviteExpertLink|\Illuminate\Database\Eloquent\Model
     */
    public function create($name, $badgeId)
    {
        return InviteExpertLink::create([
            'name' => $name,
            'code' => $this->generateCode(),
            'badge_id' => $badgeId
        ]);
    }

    /**
     * @return string
     */
    protected function generateCode()
    {
        return bin2hex(openssl_random_pseudo_bytes(16));
    }
}
