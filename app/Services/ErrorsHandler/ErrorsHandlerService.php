<?php

namespace App\Services\ErrorsHandler;

use App\Models\Posts\Posts;
use App\Models\Reports\Reports;
use App\Models\TripPlans\TripPlans;
use App\Models\User\User;
use App\Models\UsersFollowers\UsersFollowers;
use Illuminate\Support\Facades\Auth;

class ErrorsHandlerService
{

    /**
     * Get trending users list.
     * @return collaction
     */
    public function getUsersList(){
        $authUser = (Auth::check()) ? Auth::user() : '';

        $users = $this->userListAlgorithm($authUser);

        $users = $users->each(function ($user) use ($authUser) {
            $user->follow_flag      = (UsersFollowers::where('followers_id', @$authUser->id)->where('users_id', $user->id)->where('follow_type', 1)->first())  ? true : false;
            $user->total_follower   = $user->get_followers->count();
            $user->latest_followers = $user->get_followers->take(3)->map(function ($follower) {
                return [
                    'id' => $follower->followers_id,
                    'name' => @$follower->follower->name,
                ];
            })->toArray();
            unset($user->get_followers);
        });
        return $users->shuffle();
    }


    /**
     * Get trending trip plan list.
     * @return collaction
     */
    public function getPlanList(){
        $authUser = (Auth::check()) ? Auth::user() : '';

        $users = $this->userListAlgorithm($authUser);

        $plan_list = TripPlans::where('privacy', 0)->where('active', 1)->where('users_id', '!=', @$authUser->id)->whereIn('users_id', $users->pluck('id'))->orderBy('created_at', 'DESC')->take(20)->get();

        if(count($plan_list) == 0){
            $plan_list = TripPlans::where('privacy', 0)->where('active', 1)->where('users_id', '!=', @$authUser->id)->orderBy('created_at', 'DESC')->take(20)->get();
        }

        return $plan_list->shuffle();
    }

    /**
     * Get trending reports list.
     * @return collaction
     */
    public function getReportList(){
        $authUser = (Auth::check())?Auth::user():'';

        $users = $this->userListAlgorithm($authUser);

        $report_list = Reports::whereNotNull('published_date')
            ->where('users_id', '!=', @$authUser->id)
            ->whereIn('users_id', $users->pluck('id'))
            ->orderBy('created_at', 'DESC')->take(20)->get();


        if(count($report_list) == 0){
            $report_list = Reports::whereNotNull('published_date')
                ->where('users_id', '!=', @$authUser->id)
                ->orderBy('created_at', 'DESC')->take(20)->get();
        }

        return $report_list->shuffle();
    }


    /**
     * Get All Content mixed.
     * @return collaction
     */
    public function getAllContentList(){
        $index = rand(2, 100);

            if ($index % 2 == 0) {
                $entity = $this->getReportList();
                $type = 'report';
            }else if($index % 3 == 0){
                $entity = $this->getPlanList();
                $type = 'plan';
            }else{
                $entity = $this->getMediaList();
                $type = 'post';
            }

      return ['entity'=>$entity, 'type'=>$type];

    }


    /**
     * Get Media List.
     * @return collaction
     */
    public function getMediaList(){
        $authUser = (Auth::check()) ? Auth::user() : '';

        $users = $this->userListAlgorithm($authUser);

        $media_list = Posts::whereIn('users_id', $users->pluck('id'))->has('getMedias')->orderBy('id', 'DESC')->take(20)->get();

        if(count($media_list) == 0){
            $media_list = Posts::has('getMedias')->orderBy('id', 'DESC')->take(20)->get();
        }

        return $media_list->shuffle();
    }





    public function userListAlgorithm($authUser){
        if($authUser != ''){
            $userLoc = userLoc($authUser, TRUE);
            $ip = get_client_ip();
            $myTravelStyles = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();

            // #1 & #2
            $users = User::whereNotNull('profile_picture')
                ->whereIn('nationality', $userLoc)
                ->orderBy('created_at', 'DESC')
                ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                    $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                        $travelstylesSubQuery->whereIn('conf_lifestyles_id', $myTravelStyles);
                    });
                })->take(50)->get();
            if (count($users) == 0) {
                // #3 & #4
                $otherUser = userLoc($authUser, TRUE);
                $users = User::whereNotNull('profile_picture')
                    ->whereNotIn('nationality', [$authUser->nationality])
                    ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                        $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                            $travelstylesSubQuery->whereIn('conf_lifestyles_id', $myTravelStyles);
                        });
                    })
                    ->when(count($otherUser), function ($q) use ($otherUser) {
                        $q->orWhereIn('nationality', $otherUser);
                    })
                    ->orderBy('created_at', 'DESC')->take(50)->get();
                if (count($users) == 0) {
                    // #3 & #4
                    $users = User::whereNotNull('profile_picture')
                        ->whereIn('nationality', [$authUser->nationality])
                        ->when(count($otherUser), function ($q) use ($otherUser) {
                            $q->orWhereIn('nationality', $otherUser);
                        })
                        ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                            $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                                $travelstylesSubQuery->orWhereIn('conf_lifestyles_id', $myTravelStyles);
                            });
                        })
                        ->orderBy('created_at', 'DESC')->take(50)->get();
                }
            }
        }else{
            $userLoc = userLoc('', TRUE);
            $users = User::whereNotNull('profile_picture')
                ->whereIn('nationality', $userLoc)
                ->orderBy('created_at', 'DESC')->take(50)->get();

            if(count($users) == 0){
                $users = User::whereNotNull('profile_picture')
                    ->orderBy('created_at', 'DESC')->take(50)->get();
            }
        }

        return $users;
    }
}
