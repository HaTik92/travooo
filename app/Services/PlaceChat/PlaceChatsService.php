<?php

namespace App\Services\PlaceChat;

use App\Events\Api\TripChat\ChatNewConversationApiEvent;
use App\Events\Api\TripChat\SendMessage;
use App\Events\ChatNewConversationEvent;
use App\Events\ChatPrivateMessagesReadEvent;
use App\Events\ChatSendPrivateMessageEvent;
use App\Events\UpdatePlanChatsEvent;
use App\Models\Chat\ChatConversation;
use App\Models\Chat\ChatConversationMessage;
use App\Models\Chat\ChatConversationParticipant;
use App\Models\Place\Place;
use App\Models\Posts\Checkins;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use App\Models\User\User;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsService;
use App\Services\Trips\TripsSuggestionsService;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Musonza\Chat\Models\Conversation;
use Musonza\Chat\Services\ConversationService;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PlaceChatsService
{
    const CHAT_TYPE_MAIN = 0;
    const CHAT_TYPE_PLACE = 1;

    const MESSAGE_TYPE_LEFT = 0;
    const MESSAGE_TYPE_ADDED = 1;
    const MESSAGE_TYPE_PAUSED = 2;
    const MESSAGE_TYPE_RESUMED = 3;

    /**
     * @var TripInvitationsService
     */
    protected $tripInvitationsService;

    /**
     * @var TripsService
     */
    protected $tripsService;
    /**
     * @var TripsSuggestionsService
     */
    private $tripsSuggestionsService;

    /**
     * @var boolean $isApi
     */
    private $isApi = false;

    /**
     * PlaceChatsService constructor.
     * @param TripInvitationsService $tripInvitationsService
     * @param TripsService $tripsService
     * @param TripsSuggestionsService $tripsSuggestionsService
     */
    public function __construct(TripInvitationsService $tripInvitationsService, TripsService $tripsService, TripsSuggestionsService $tripsSuggestionsService)
    {
        $this->tripInvitationsService = $tripInvitationsService;
        $this->tripsService = $tripsService;
        $this->tripsSuggestionsService = $tripsSuggestionsService;
    }

    public function getPlaceChatParticipants($chatId, $check_role = true)
    {
        $chatConversation = ChatConversation::find($chatId);

        if (!$chatConversation) {
            return [];
        }

        $chatConversationParticipant = $chatConversation->participants()->where('user_id', auth()->id())->first();

        if (!$chatConversationParticipant) {
            if (!$this->tripsSuggestionsService->getInvitedUserRequest($chatConversation->plans_id, auth()->id()) && !$this->tripsService->isPlanCreator($chatConversation->plans_id)) {
                // todo 403 exception
                return [];
            }
        }

        $participants = $chatConversation->participants()->whereNull('removed_at')->with('user')->get();

        foreach ($participants as &$participant) {
            $participant->src = check_profile_picture($participant->user->profile_picture);
            $participant->role = $check_role?$this->tripInvitationsService->getUserRole($chatConversation->plans_id, $participant->user_id):'';
        }

        return $participants;
    }

    public function setApi($flag)
    {
        $this->isApi = $flag;
    }

    /**
     * @param int $userId
     * @return ChatConversationMessage[]
     */
    public function getPlaceChatMessages($chatId)
    {
        $chatConversation = ChatConversation::find($chatId);

        if (!$chatConversation) {
            // todo 404 exception
            return [];
        }

        $chatConversationParticipant = $chatConversation->participants()->where('user_id', auth()->id())->first();

        if (!$chatConversationParticipant) {
            if (!$this->tripsSuggestionsService->getInvitedUserRequest($chatConversation->plans_id, auth()->id()) && !$this->tripsService->isPlanCreator($chatConversation->plans_id)) {
                // todo 403 exception
                return [];
            }
        }

        $messagesQuery = $chatConversation->messages()->orderBy('created_at');

        if ($chatConversation->removed_at) {
            $messagesQuery->where('created_at', '<', $chatConversation->removed_at);
        }

        $messages = $messagesQuery->get();

        $readIds = $messages->where('to_id', '=', auth()->id())->pluck('id');

        try {
            $query = ChatConversationMessage::whereIn('id', $readIds);
            $query->update(['is_read' => 1]);

            $readMessages = $query->get();

            foreach ($readMessages as $readMessage) {
                broadcast(new ChatPrivateMessagesReadEvent(Auth()->id(), $readMessage->from_id, $readMessage));
            }
        } catch (\Exception $e) {
        }

        $authUserSrc = check_profile_picture(auth()->user()->profile_picture);

        $messages = $messages->unique('send_id')->values();

        foreach ($messages as &$message) {
            if ($message->from_id !== auth()->id()) {
                $userSrc = check_profile_picture(User::find($message->from_id)->profile_picture);
            } else {
                $userSrc = $authUserSrc;
            }

            $message->user_src = $userSrc;
            $message->auth_user_src = $authUserSrc;
        }

        return $messages;
    }

    /**
     * @param int $chatId
     * @param string $message
     * @param int $planId
     * @param int $placeId
     * @return ChatConversationMessage|bool|\Illuminate\Database\Eloquent\Model
     */
    public function createPlaceChatMessage($chatId, $message, $planId, $placeId)
    {
        $conversation = ChatConversation::find($chatId);

        if (!$conversation) {
            return false;
        }

        $chatConversationParticipant = $conversation->participants()->where('user_id', auth()->id())->first();

        if (!$chatConversationParticipant || $chatConversationParticipant->removed_at) {
            return false;
        }

        $this->addAllToPlaceChat($conversation);

        $sendId = ChatConversationMessage::max('send_id') + 1;

        foreach ($conversation->participants()->whereNull('removed_at')->get() as $participant) {
            try {
                if (!$conversation->messages()->where(function ($q) use ($participant) {
                    $q->where('from_id', $participant->user_id)
                        ->orWhere('to_id', $participant->user_id);
                })->count()) {
                    broadcast(new ChatNewConversationEvent($participant->user_id, $conversation));
                    try {
                        broadcast(new ChatNewConversationApiEvent(auth()->id(), $participant->user_id, $conversation));
                    } catch (\Exception $e) {
                    }
                }
            } catch (\Exception $e) {
            }

            $msg = ChatConversationMessage::create([
                'chat_conversation_id' => $conversation->getKey(),
                'to_id' => $participant->user_id,
                'from_id' => Auth()->id(),
                'message' => $message,
                'send_id' => $sendId,
                'places_id' => $placeId,
                'plans_id' => $planId
            ]);

            try {
                broadcast(new ChatSendPrivateMessageEvent($participant->user_id, $msg, $conversation->getKey()));
            } catch (\Exception $e) {
            }

            try {
                broadcast(new SendMessage($participant->user_id, $msg, $conversation->getKey()));
            } catch (\Exception $e) {
            }

            try {
                broadcast(new UpdatePlanChatsEvent($participant->user_id, $planId));
            } catch (\Exception $e) {
            }
        }

        $msg->src = check_profile_picture(auth()->user()->profile_picture);

        return $msg;
    }

    /**
     * @param null|int $userId
     * @param null $placeId
     * @param null $conversationId
     * @return int
     */
    public function getUnreadMessagesCount($userId = null, $placeId = null, $conversationId = null)
    {
        $query = ChatConversationMessage::query()
            ->where('is_read', 0)
            ->where('to_id', auth()->id())
            ->where('from_id', '!=', auth()->id());

        if ($userId) {
            $query->where('from_id', $userId);
        }

        if ($placeId) {
            $query->where('places_id', $placeId);
        }

        if ($conversationId) {
            $query->where('chat_conversation_id', $conversationId);
        }

        return $query->count();
    }

    /**
     * @param int $placeId
     * @return array
     */
    public function getPlaceUsersToChat($planId, $placeId)
    {
        $friends = $this->tripInvitationsService->findFriends();
        $followers = $this->tripInvitationsService->findFollowers();

        $now = Carbon::now();

        $checkins = Checkins::query()
            ->where('place_id', $placeId)
            ->where('checkin_time', '<=', $now)
            ->where('users_id', '!=', auth()->id())
            ->with('user')
            ->get();

        $tripPlacesCheckins = TripPlaces::query()
            ->where('places_id', $placeId)
            ->where('time', '<=', $now)
            ->where('versions_id', '!=', 0)
            ->whereHas('active_trip', function ($q) {
                $q->where('users_id', '!=', auth()->id());
            })
            ->with('trip')
            ->get();

        $planCheckins = [];

        foreach ($tripPlacesCheckins as $tripPlacesCheckin) {
            $tripPlacesCheckin->trip->checkin_time = $tripPlacesCheckin->time;
            $planCheckins[] = $tripPlacesCheckin->trip;
        }

        $checkins = $checkins->merge($planCheckins)->unique('users_id');

        $users = [];

        foreach ($checkins as $checkin) {
            $user = $checkin->user;

            if (!$user) {
                continue;
            }

            $userId = $user->id;

            $role = $this->tripInvitationsService->getUserRole($planId, $userId);

            if ($role) {
                continue;
            }

            $conversation = ChatConversation::select('id')
                ->whereHas('participants', function ($query) use ($userId) {
                    $query->whereIn('user_id', [$userId, auth()->id()])
                        ->whereNull('removed_at');
                }, '=', 2)
                ->where('plans_id', $planId)
                ->where('type', self::CHAT_TYPE_PLACE)
                ->first();

            if (!$conversation) {
                $conversation = $this->createConversation($planId, self::CHAT_TYPE_PLACE, auth()->id(), $userId, $userId);
            }

            $availablePrivacy = [TripsService::PRIVACY_PUBLIC];

            if (in_array($userId, $friends->toArray())) {
                $type = 'friends';
                $availablePrivacy[] = TripsService::PRIVACY_FRIENDS;
            } elseif (in_array($userId, $followers->toArray())) {
                $type = 'followers';
            } else {
                $type = 'available today';
            }

            if ($checkin->privacy !== null && !in_array($checkin->privacy, $availablePrivacy)) {
                continue;
            }

            $time = Carbon::parse($checkin->checkin_time);

            if ($time->endOfDay()->equalTo(now()->endOfDay())) {
                $time = 'today';
            } else {
                $time = $time->diffForHumans();
            }

            $users[$type][] = [
                'id' => $userId,
                'name' => $user->name,
                'src' => check_profile_picture($user->profile_picture),
                'chat_id' => $conversation->id,
                'role' => $role,
                'date' => $time,
                'is_expert' => $user->expert
            ];
        }

        return $users;
    }

    /**
     * @param int $planId
     * @throws ModelNotFoundException
     * @return array
     */
    public function getPlanUsersToChat($planId)
    {
        $plan = $this->tripsService->findPlan($planId);

        if (!$plan) {
            throw new ModelNotFoundException('Plan does not exist');
        }

        $invitedPeople = $this->tripInvitationsService->getInvitedPeopleIds($planId);

        $invitedPeople[$plan->users_id] = 'admin';

        return $this->prepareUsers($invitedPeople);
    }

    public function addToPlanChats($planId, $userId)
    {
        $plan = $this->tripsService->findPlan($planId);

        if (!$plan) {
            throw new ModelNotFoundException('Plan does not exist');
        }

        $conversations = ChatConversation::query()
            ->where('plans_id', $planId)
            ->where('created_by_id', '!=', $userId)
            ->whereHas('participants', function ($q) {
                $q->whereNull('removed_at');
            })
            ->where(function ($q) {
                $q->where('type', self::CHAT_TYPE_MAIN)
                    ->orWhereHas('messages');
            })
            ->get();

        foreach ($conversations as $conversation) {
            $participant = ChatConversationParticipant::firstOrCreate([
                'chat_conversation_id' => $conversation->id,
                'user_id' => $userId
            ]);

            if ($participant->wasRecentlyCreated || $participant->removed_at !== null) {
                $participant->removed_at = null;
                $participant->save();
                $sendId = ChatConversationMessage::max('send_id') + 1;

                foreach ($conversation->participants()->whereNull('removed_at')->get() as $planChatsParticipant) {
                    if ($userId === $planChatsParticipant->user_id) {
                        $message = 'You were added to the chat';
                    } else {
                        $message = User::find($userId)->name . ' was added to the chat';
                    }

                    $msg = ChatConversationMessage::create([
                        'chat_conversation_id' => $conversation->getKey(),
                        'to_id' => $planChatsParticipant->user_id,
                        'from_id' => Auth()->id(),
                        'message' => $message,
                        'send_id' => $sendId,
                        'plans_id' => $planId,
                        'type' => self::MESSAGE_TYPE_ADDED
                    ]);

                    try {
                        broadcast(new ChatSendPrivateMessageEvent($planChatsParticipant->user_id, $msg, $conversation->getKey()));
                    } catch (\Exception $e) {
                    }

                    try {
                        broadcast(new SendMessage($planChatsParticipant->user_id, $msg, $conversation->getKey()));
                    } catch (\Exception $e) {
                    }


                    try {
                        broadcast(new UpdatePlanChatsEvent($planChatsParticipant->user_id, $planId));
                    } catch (\Exception $e) {
                    }
                }
            }
        }

        $this->pauseAllPlaceChatsWithUser($planId, $userId);
    }

    public function removeFromPlanChats($planId, $userId)
    {
        $plan = $this->tripsService->findPlan($planId);

        if (!$plan) {
            throw new ModelNotFoundException('Plan does not exist');
        }

        $query = ChatConversationParticipant::query()
            ->where('user_id', $userId)
            ->whereNull('removed_at')
            ->whereHas('conversation', function ($query) use ($planId, $userId) {
                $query->where('plans_id', $planId);
                $query->where('created_by_id', '!=', $userId);
            });

        $conversations = $query->with('conversation')->get()->pluck('conversation')->unique('id');

        $query->update(['removed_at' => Carbon::now()]);

        foreach ($conversations as $conversation) {
            $sendId = ChatConversationMessage::max('send_id') + 1;

            foreach ($conversation->participants as $participant) {
                if ($userId == $participant->user_id) {
                    $message = 'You were removed from the chat';
                } else {
                    $message = User::find($userId)->name . ' was removed from the chat';
                }

                $msg = ChatConversationMessage::create([
                    'chat_conversation_id' => $conversation->getKey(),
                    'to_id' => $participant->user_id,
                    'from_id' => Auth()->id(),
                    'message' => $message,
                    'send_id' => $sendId,
                    'plans_id' => $planId,
                    'type' => self::MESSAGE_TYPE_LEFT
                ]);

                try {
                    broadcast(new ChatSendPrivateMessageEvent($participant->user_id, $msg, $conversation->getKey()));
                } catch (\Exception $e) {
                }

                try {
                    broadcast(new SendMessage($participant->user_id, $msg, $conversation->getKey()));
                } catch (\Exception $e) {
                }

                try {
                    broadcast(new UpdatePlanChatsEvent($participant->user_id, $planId));
                } catch (\Exception $e) {
                }
            }
        }

        $this->resumeAllPlaceChatsWithUser($planId, $userId);
    }

    /**
     * @param int $planId
     * @throws ModelNotFoundException
     * @return array
     */
    public function getPlanChats($planId)
    {
        $plan = $this->tripsService->findPlan($planId);

        if (!$plan) {
            throw new ModelNotFoundException('Plan does not exist');
        }

        $conversations = ChatConversation::query()->where('plans_id', $planId)->where(function ($query) {
            $query
                ->where('type', self::CHAT_TYPE_MAIN)
                ->orWhereHas('messages', function ($q) {
                    $q->whereNull('type');
                });
        })->with('participants')->get();

        $result = [
            'data' => [],
            'unread_count' => 0
        ];

        foreach ($conversations as $conversation) {
            if ($conversation->type !== self::CHAT_TYPE_MAIN && !$conversation->messages()->where('from_id', $conversation->created_by_id)->withTrashed()->count()) {
                continue;
            }

            $data = $this->prepareConversation($conversation);

            if (count($data['participants'])) {
                $result['data'][] = $data;
                $result['unread_count'] += $data['unread_count'];
            }
        }

        return $result;
    }

    /**
     * Use For Api
     * @param int $planId
     * @throws ModelNotFoundException
     * @return array
     */
    public function getApiPlanChats($tripId)
    {
        $plan = $this->tripsService->findPlan($tripId);
        if (!$plan) throw new ModelNotFoundException('Plan does not exist');

        $items = request('per_page') ?? 10;
        $conversations = ChatConversation::query()
            ->where('plans_id', $tripId)
            ->where(function ($query) {
                $query->where('type', self::CHAT_TYPE_MAIN)
                    ->orWhereHas('messages', function ($q) {
                        $q->whereNull('type');
                    });
            })->with('participants')
            ->paginate($items);

        $result = [
            'unread_count' => 0,
            'current_page' => $conversations->currentPage(),
            'total_pages' => $conversations->lastPage(),
            'per_page' => $conversations->perPage(),
            'total' => $conversations->total(),
            'loginUser' => auth()->user(),
            'data' => [],
        ];

        foreach ($conversations as $conversation) {
            $data = $this->prepareConversation($conversation);
            if (count($data['participants'])) {
                $result['data'][] = $data;
                $result['unread_count'] += $data['unread_count'];
            }
        }
        return $result;
    }

    /**
     * Use For Api
     * @param int $planId
     * @throws ModelNotFoundException
     * @return array
     */
    public function getApiPlanPlaceChats($tripId)
    {
        $items = request('per_page') ?? 10;
        $trip_places_arr = $placeChatList = [];
        $trip = TripPlans::findOrFail($tripId);
        if (!$trip) throw new ModelNotFoundException('Plan does not exist');

        $my_version_id = $trip->versions[0]->id;
        $trip_places = TripPlaces::where('trips_id', $tripId)->where('versions_id', $my_version_id)
            ->orderBy('id', 'ASC')->groupBy('places_id')->paginate($items);

        foreach ($trip_places as $tp) {
            if (!$tp->place or !$tp->place->city) continue;
            $tripPlaceId = $this->tripsSuggestionsService->getRootTripPlace($tp->id);

            $trip_places_arr[$tripPlaceId] = $tp->place;
            $trip_places_arr[$tripPlaceId]['image'] = check_place_photo($tp->place);
            $trip_places_arr[$tripPlaceId]['time'] = $tp->time;
            $trip_places_arr[$tripPlaceId]['trip_place_id'] = $tp->id;
            $trip_places_arr[$tripPlaceId]['name'] = @$tp->place->transsingle->title;
            $trip_places_arr[$tripPlaceId]['city_name'] = (@$tp->place->city->trans) ? @$tp->place->city->trans->first()->title : '';
        }
        $trip_places_arr = collect(array_values($trip_places_arr))->sortBy('time')->values()->all();
        foreach ($trip_places_arr as $tp) {
            $placeChatItem = [];
            $placeChatItem['id'] = $tp->id;
            $placeChatItem['image'] = $tp->image;
            $placeChatItem['name'] = $tp->name;
            $placeChatItem['city_name'] = $tp->city_name;
            $placeChatItem['country'] = $tp->country->transsingle->title;
            $placeChatItem['time']  = $tp->time;
            // get participants
            $participantsResult = $this->getPlaceUsersToChat($tripId, $tp->id);
            $pr = [];
            $cnt = 0;
            foreach ($participantsResult as $chuRoles) :
                foreach ($chuRoles as $role => $chu) :
                    $cnt++;
                    if ($cnt > 3) continue;
                    $pr[] = $chu;
                endforeach;
            endforeach;
            $placeChatItem['participants']  = $pr;
            $placeChatList[] = $placeChatItem;
        }

        return [
            'current_page' => $trip_places->currentPage(),
            'total_pages' => $trip_places->lastPage(),
            'per_page' => $trip_places->perPage(),
            'total' => $trip_places->total(),
            'loginUser' => auth()->user(),
            'data' => $placeChatList,
        ];
    }

    public function getPlanPlaceUsersToChat($planId)
    {
        $plan = $this->tripsService->findPlan($planId);

        if (!$plan) {
            throw new ModelNotFoundException('Plan does not exist');
        }

        $messages = ChatConversationMessage::where(function ($query) use ($planId) {
            $query->where(function ($query) use ($planId) {
                $query->where([
                    'plans_id' => $planId,
                    'to_id' => auth()->id()
                ])->whereNotNull('places_id');
            })->orWhere(function ($query) use ($planId) {
                $query->where([
                    'plans_id' => $planId,
                    'from_id' => auth()->id()
                ])->whereNotNull('places_id');
            });
        })->get()->pluck('places_id')->toArray();

        $placeIds = array_unique($messages);

        $suggestedPlaces = [];

        foreach ($placeIds as $placeId) {
            $place = Place::findOrFail($placeId);

            $suggestedPlaces[] = [
                'place' => $place,
                'unread_count' => $this->getUnreadMessagesCount(null, $placeId),
                'title' => $place->transsingle->title,
                'src' => check_place_photo($place)
            ];
        }

        return $suggestedPlaces;
    }

    /**
     * @param int $planId
     * @param int $type
     * @param int $userFirstId
     * @param int $userSecondId
     * @param int $createdById
     * @return ChatConversation|\Illuminate\Database\Eloquent\Model
     */
    public function createConversation($planId, $type = null, $userFirstId = null, $userSecondId = null, $createdById = null)
    {
        if (!$createdById) {
            $createdById = auth()->id();
        }

        $conversation = ChatConversation::create([
            'created_by_id' => $createdById,
            'subject' => '',
            'plans_id' => $planId,
            'type' => $type
        ]);

        if ($userFirstId) {
            ChatConversationParticipant::create([
                'chat_conversation_id' => $conversation->id,
                'user_id' => $userFirstId
            ]);
        }

        if ($userSecondId) {
            ChatConversationParticipant::create([
                'chat_conversation_id' => $conversation->id,
                'user_id' => $userSecondId
            ]);

            try {
                if ($type !== self::CHAT_TYPE_PLACE) {
                    broadcast(new ChatNewConversationEvent($userFirstId, $conversation));
                    try {
                        broadcast(new ChatNewConversationApiEvent($userSecondId, $userFirstId, $conversation));
                    } catch (\Throwable $e) {
                    }
                }

                broadcast(new UpdatePlanChatsEvent($userSecondId, $planId));
            } catch (\Exception $e) {
            }
        }

        return $conversation;
    }

    /**
     * @param $conversation
     * @return mixed
     */
    public function getLastConversationMessage($conversation)
    {
        return $conversation->myMessages(auth()->id(), 1)->first();
    }

    /**
     * @param int $planId
     * @param int $userId
     */
    public function pauseAllPlaceChatsWithUser($planId, $userId)
    {
        $conversations = ChatConversation::query()
            ->where('created_by_id', $userId)
            ->where('type', self::CHAT_TYPE_PLACE)
            ->where('plans_id', $planId)
            ->get();

        foreach ($conversations as $conversation) {
            $sendId = ChatConversationMessage::max('send_id') + 1;

            $participants = $conversation->participants()->whereNull('removed_at')->get();

            foreach ($participants as $participant) {
                if ($userId == $participant->user_id) {
                    $message = 'Group chat has been closed as you have joined the plan.';
                } else {
                    $message = 'Group chat has been closed as ' . User::find($userId)->name . ' has joined the plan.';
                }

                $msg = ChatConversationMessage::create([
                    'chat_conversation_id' => $conversation->getKey(),
                    'to_id' => $participant->user_id,
                    'from_id' => $userId,
                    'message' => $message,
                    'send_id' => $sendId,
                    'plans_id' => $planId,
                    'type' => self::MESSAGE_TYPE_PAUSED
                ]);

                try {
                    broadcast(new ChatSendPrivateMessageEvent($participant->user_id, $msg, $conversation->getKey()));
                } catch (\Exception $e) {
                }

                try {
                    broadcast(new SendMessage($participant->user_id, $msg, $conversation->getKey()));
                } catch (\Exception $e) {
                }


                try {
                    broadcast(new UpdatePlanChatsEvent($participant->user_id, $planId));
                } catch (\Exception $e) {
                }

                $participant->removed_at = Carbon::now();
                $participant->save();
            }
        }
    }

    /**
     * @param int $planId
     * @param int $userId
     */
    public function resumeAllPlaceChatsWithUser($planId, $userId)
    {
        $conversations = ChatConversation::query()
            ->where('created_by_id', $userId)
            ->where('type', self::CHAT_TYPE_PLACE)
            ->where('plans_id', $planId)
            ->get();

        foreach ($conversations as $conversation) {
            $sendId = ChatConversationMessage::max('send_id') + 1;

            $participants = $conversation->participants()->whereNotNull('removed_at')->get();

            foreach ($participants as $participant) {
                if ($userId == $participant->user_id) {
                    $message = 'Group chat is open as you are no longer part of the trip plan.';
                } else {
                    $message = 'Group chat is open as ' . User::find($userId)->name . ' is no longer part of the trip plan.';
                }

                $msg = ChatConversationMessage::create([
                    'chat_conversation_id' => $conversation->getKey(),
                    'to_id' => $participant->user_id,
                    'from_id' => $userId,
                    'message' => $message,
                    'send_id' => $sendId,
                    'plans_id' => $planId,
                    'type' => self::MESSAGE_TYPE_RESUMED
                ]);

                try {
                    broadcast(new ChatSendPrivateMessageEvent($participant->user_id, $msg, $conversation->getKey()));
                } catch (\Exception $e) {
                }

                try {
                    broadcast(new SendMessage($participant->user_id, $msg, $conversation->getKey()));
                } catch (\Exception $e) {
                }

                try {
                    broadcast(new UpdatePlanChatsEvent($participant->user_id, $planId));
                } catch (\Exception $e) {
                }

                $participant->removed_at = null;
                $participant->save();
            }
        }
    }

    public function renderActualChats($planId, $edit)
    {
        $role = $this->tripInvitationsService->getUserRole($planId, auth()->id());

        if (!$role) {
            throw new AccessDeniedHttpException();
        }

        $data = $this->tripsSuggestionsService->getPlanContentsData($edit, $planId, auth()->id());
        $data['plan_chats'] = $this->getPlanChats($planId);

        foreach ($data['trip_places_arr'] as &$tripPlace) {
            $tripPlace->chat_users = $this->getPlaceUsersToChat($planId, $tripPlace->id);
        }

        $placesArr = $data['trip_places_arr'];
        $data['trip_places_arr'] = [];

        collect($placesArr)->unique('id')->map(function ($e) use (&$data) {
            $data['trip_places_arr'][] = $e;
        });

        return view('site.trip2.partials.plan-chats', $data);
    }

    public function removeAllPlanChats($planId)
    {
        ChatConversation::query()
            ->where('plans_id', $planId)
            ->delete();
    }

    public function read($messageId, $userId = null)
    {
        if (!$userId) {
            $userId = auth()->id();
        }

        /** @var ChatConversationMessage $message */
        $message = ChatConversationMessage::query()->where([
                'id' => $messageId,
                'to_id' => $userId
            ])->first();

        if (!$message) {
            return false;
        }

        $message->is_read = 1;
        $message->save();

        $planId = $message->conversation->plans_id;

        if ($planId) {
            try {
                broadcast(new UpdatePlanChatsEvent($userId, $planId));
            } catch (\Exception $e) {
            }
        }

        return true;
    }

    protected function prepareUsers($planUsers)
    {
        $result = [];

        foreach ($planUsers as $id => $role) {
            $planUser = User::find($id);

            if (!$planUser || $id === auth()->id()) {
                continue;
            }

            $result[$role][] = [
                'id' => $planUser->id,
                'name' => $planUser->name,
                'src' => check_profile_picture($planUser->profile_picture),
                'unread_count' => $this->getUnreadMessagesCount($planUser->id)
            ];
        }

        return $result;
    }

    protected function prepareConversation($conversation)
    {
        $result = [
            'id' => $conversation->id,
            'participants' => [],
            'unread_count' => $this->getUnreadMessagesCount(null, null, $conversation->id),
            'type' => $conversation->type,
            'last_message' => $this->getLastConversationMessage($conversation),
            'created_by_id' => $conversation->created_by_id,
            'author'        => (@$conversation->author) ? [
                'id' => $conversation->author->id,
                'name' => $conversation->author->name,
                'src' => check_profile_picture($conversation->author->profile_picture),
                'is_expert' => ($conversation->author->expert) ? true : false
            ] : null
        ];

        foreach ($conversation->participants()->with('user')->whereNull('removed_at')->get() as $participant) {
            if (auth()->id() === $participant->user_id && $conversation->type === self::CHAT_TYPE_PLACE) {
                //continue;
            }

            $user = $participant->user;

            $result['participants'][] = [
                'id' => $participant->user_id,
                'name' => $user->name,
                'src' => check_profile_picture($user->profile_picture),
                'is_expert' => ($user->expert) ? true : false,
            ];
        }

        return $result;
    }

    protected function addAllToPlaceChat($conversation)
    {
        $plan = $this->tripsService->findPlan($conversation->plans_id);

        if (!$plan) {
            throw new ModelNotFoundException('Plan does not exist');
        }

        $planUsers = array_keys($this->tripInvitationsService->getInvitedPeopleIds($conversation->plans_id));
        $planUsers[] = $plan->users_id;

        $participants = $conversation->participants->pluck('user_id')->toArray();

        foreach ($planUsers as $planUser) {
            if (in_array($planUser, $participants)) {
                continue;
            }

            ChatConversationParticipant::create([
                'chat_conversation_id' => $conversation->id,
                'user_id' => $planUser
            ]);

            try {
                broadcast(new UpdatePlanChatsEvent($planUser, $conversation->plans_id));
            } catch (\Exception $e) {
            }
        }
    }
}
