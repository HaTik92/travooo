<?php

namespace App\Services\Newsfeed;

use App\Exceptions\GeneralException;
use App\Http\Constants\CommonConst;
use App\Http\Responses\ApiResponse;
use App\Models\ActivityLog\ActivityLog;
use App\Models\User\UsersContentLanguages;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\City\Cities;
use App\Models\Place\Place;
use App\Models\Country\Countries;
use App\Models\Discussion\Discussion;
use App\Models\Events\Events;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsCities;
use App\Models\Posts\PostsCountries;
use App\Models\Posts\PostsPlaces;
use App\Models\Reports\Reports;
use App\Models\Reviews\Reviews;
use App\Models\TripPlans\TripPlans;
use App\Services\Api\PrimaryPostService;
use App\Services\Trips\TripsService;
use Illuminate\Support\Collection;
use Illuminate\Database\Query\Expression;

class CCPNewsfeed
{
    public $authUser;
    public $locationObj;
    public $tab, $sub_type;
    public $filter;
    public $page;
    public $per_page;

    public $skip;
    public $type;
    private $hasMore;

    protected $final_language;
    protected $followers;
    protected $friends;

    // newsfeed algo with user language content
    const SUB_TYPES = [
        HomeService::TYPE_COUNTRY => [
            HomeService::TYPE_COUNTRY, // 1 priority // display first country
            HomeService::TYPE_CITY, // 2 priority // after finish country -> display all related children cities
            HomeService::TYPE_PLACE, // 3 priority // after finish cities -> display all related children places
        ],
        HomeService::TYPE_CITY => [
            HomeService::TYPE_CITY, // 1 priority // display first city
            HomeService::TYPE_PLACE, // 2 priority // after finish city -> display all related children places
            HomeService::TYPE_COUNTRY, // 3 priority // after finish places -> display related parent country
        ],
        HomeService::TYPE_PLACE => [
            HomeService::TYPE_PLACE, // 1 priority  // display first place
            HomeService::TYPE_CITY, // 2 priority  // after finish place -> display related parent city
            HomeService::TYPE_COUNTRY, // 3 priority  // after finish city -> display related parent country
        ],
    ];

    public function __construct()
    {
        DB::enableQueryLog();
    }

    /**
     * @param Countries|Cities|Place $obj 
     * @param string $tab ['top-posts'|'new-posts'|'discussions'|'trip-plans'|'reports'|'reviews'|'events','medias']
     * @param array|null $filter
     * @param int $page
     * @param int $per_page
     */
    public function set($locationObj, $tab = CommonConst::CCP_TAB_NEW_POSTS, $sub_type = null, $filter = null, $page = 1, $per_page = 10)
    {
        /* override filter section */ {
            $_filter = [];
            if (isset($filter['people_filter'])) {
                if ($filter['people_filter']  == 'people_you_follow') {
                    $_filter['people'] = 1; // people_you_follow
                }
            }
            if (isset($filter['location_filter'])) {
                if ($filter['location_filter']  == 'from_your_country') {
                    $_filter['location'] = 1; // from_your_country
                }
            }
        }

        $this->authUser             = Auth::user();
        $this->locationObj          = $locationObj;
        $this->tab                  = $tab;
        $this->sub_type             = $sub_type;
        $this->filter               = $_filter;
        $this->page                 = (int) $page;
        $this->per_page             = (int) $per_page;

        $this->followers            = [];
        $this->friends              = [];

        $this->type = when(get_class($this->locationObj), [
            Cities::class       => HomeService::TYPE_CITY,
            Place::class        => HomeService::TYPE_PLACE,
            Countries::class    => HomeService::TYPE_COUNTRY,
        ]);

        $this->skip                 = ($this->page - 1) * $this->per_page;
        $this->hasMore              = true;
        if ($this->authUser) {
            $temp_final_outcome     = getDesiredLanguage();
            $this->final_language   = (isset($temp_final_outcome[0])) ? $temp_final_outcome : UsersContentLanguages::DEFAULT_LANGUAGES;

            $this->followers        = get_followerlist();
            $this->friends          = get_friendlist();
            $this->followers[]      = $this->authUser->id;
            $this->friends[]        = $this->authUser->id;
        } else {
            $this->final_language   = UsersContentLanguages::DEFAULT_LANGUAGES;
        }

        return $this;
    }

    /**
     * @return Collection|array
     * @throws GeneralException
     */
    public function get()
    {
        switch ($this->tab) {
            case CommonConst::CCP_TAB_DISCUSSIONS:
                $posts = $this->showNewsfeedDiscussions();
                break;
            case CommonConst::CCP_TAB_REPORTS:
                $posts = $this->showNewsfeedReports();
                break;
            case CommonConst::CCP_TAB_REVIEWS:
                $posts = $this->showNewsfeedReviews();
                break;
            case CommonConst::CCP_TAB_NEW_POSTS:
                $posts = $this->showNewsfeedAllPosts();
                break;
            case CommonConst::CCP_TAB_TOP_POSTS:
                $posts = $this->showNewsfeedAllPosts(true);
                break;
            case CommonConst::CCP_TAB_TRIP_PLANS:
                $posts = $this->showNewsfeedTripPlans();
                break;
            case CommonConst::CCP_TAB_EVENTS:
                $posts = $this->showNewsfeedEvents();
                break;
            case CommonConst::CCP_TAB_MEDIAS:
                $posts = $this->showNewsfeedNewMedias();
                break;
            default:
                throw new GeneralException('api not found', ApiResponse::NOT_FOUND);
                break;
        }
        // dd(DB::getQueryLog());

        if (request('without_prepared', false) == true) {
            return  $posts;
        }

        $helper = app(HomeService::class);

        $data['type']           = $this->type;
        $data['sub_type']       = $this->sub_type;
        $data['tab']            = $this->tab;
        $data['page']           = $this->page;
        $data['has_more']       = $this->hasMore;
        $data['priorities']       = collect(self::SUB_TYPES[$this->type])->map(function ($key, $index) {
            return ['priority' => $index + 1, 'sub_type' => $key];
        });

        $responsePost = [];
        $collection = json_decode(json_encode(collect($posts['collection'])->toArray()));
        foreach ($collection as $postItem) {
            switch ($postItem->type) {
                case HomeService::TYPE_POST:
                    if (in_array($postItem->action, ['publish'])) {
                        $post = $helper->__preparedPost($postItem->variable);
                        if ($post) {
                            $responsePost[] = [
                                'page_id'   => $helper->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                'type'      => strtolower(HomeService::TYPE_POST),
                                'action'    => $postItem->action,
                                'data'      => $post
                            ];
                        }
                    }
                    break;

                case HomeService::TYPE_EVENT:
                    if (in_array($postItem->action, ['show'])) {
                        $event = $helper->__preparedEvent($postItem->variable);
                        if ($event) {
                            $responsePost[] = [
                                'page_id'   => $helper->link("newsfeed/event/event/" . $event->id),
                                'unique_link'   => url("newsfeed/event/event/" . $event->id),
                                'type'      => strtolower(HomeService::TYPE_EVENT),
                                'action'    => $postItem->action,
                                'data'      => $event
                            ];
                        }
                    }
                    break;

                case "place":
                    if (in_array($postItem->action, [HomeService::TYPE_REVIEW])) {
                        $review = $helper->__preparedPlaceReview($postItem->variable);
                        if ($review) {
                            $responsePost[] = [
                                'page_id'   => $helper->link("newsfeed/" . $review->author->username . "/review/" . $review->id),
                                'unique_link'   => url("newsfeed/" . $review->author->username . "/review/" . $review->id),
                                'type'      => strtolower(HomeService::TYPE_REVIEW),
                                'action'    => strtolower($postItem->type),
                                'data'      => $review
                            ];
                        }
                    }
                    break;

                case HomeService::TYPE_DISCUSSION:
                    if (in_array($postItem->action, ['create'])) {
                        $discussion = $helper->__preparedDiscussion($postItem->variable);
                        if ($discussion) {
                            $responsePost[] =  [
                                'page_id'       => $helper->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                'type'          => strtolower(HomeService::TYPE_DISCUSSION),
                                'action'        => 'create',
                                'data'          => $discussion
                            ];
                        }
                    }
                    break;

                case HomeService::TYPE_TRIP:
                    if (in_array($postItem->action, ['create', 'plan_updated'])) {
                        $trip = $helper->__preparedRegularTrip($postItem->variable);
                        if ($trip) {
                            $responsePost[] = [
                                'page_id'   => $helper->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                'type'      => strtolower(HomeService::TYPE_TRIP),
                                'action'    => 'create',
                                'data'      => $trip
                            ];
                        }
                    }
                    break;


                case HomeService::TYPE_REPORT:
                    if (in_array($postItem->action, ['create', 'update'])) {
                        $report = $helper->__preparedReport($postItem->variable);
                        if ($report) {
                            $responsePost[] = [
                                'page_id'   => $report->id,
                                'unique_link'   => url("reports/" . $report->id),
                                'type'      => strtolower(HomeService::TYPE_REPORT),
                                'action'    => 'create',
                                'data'      => $report
                            ];
                        }
                    }
                    break;
            }
        }

        $data['newsfeeds']      = $responsePost;
        return $data;
    }

    /**
     * Use for CommonConst::CCP_TAB_NEW_POSTS & CommonConst::CCP_TAB_TOP_POSTS
     * @param bool $top 
     * @return Collection
     */
    function showNewsfeedAllPosts($top = false): Collection
    {
        $result  = $this->apiGetPostsIdForAllFeed($top);

        $collection = collect($result['collection'])->map(function ($item) {
            $temp['variable'] = $item['id'];
            if ($item['type'] == 'discussion') {
                $temp['type'] = PrimaryPostService::TYPE_DISCUSSION;
                $temp['action'] = "create";
            } else if ($item['type'] == 'review') {
                $temp['type'] = PrimaryPostService::TYPE_REVIEW;
                $temp['action'] = "place";
            } else if ($item['type'] == 'report' || $item['type'] == 'reports') {
                $temp['type'] = PrimaryPostService::TYPE_REPORT;
                $temp['action'] = "create";
            } else if ($item['type'] == 'plan') {
                $temp['type'] = PrimaryPostService::TYPE_TRIP;
                $temp['action'] = "create";
            } else if ($item['type'] == 'regular') {
                $temp['type'] = PrimaryPostService::TYPE_POST;
                $temp['action'] = "publish";
            }
            return $temp;
        });

        return collect([
            'collection' => $collection
        ]);
    }

    /**
     * @return Collection
     */
    function showNewsfeedNewMedias(): Collection
    {
        $id = $this->queryDetailProvider();
        if ($this->sub_type == HomeService::TYPE_PLACE) {
            $baseQuery = PostsPlaces::query();
            if ($id instanceof Expression) {
                $baseQuery->whereRaw('places_id' . $id);
            } else {
                $method = is_array($id) ? 'whereIn' : 'where';
                $baseQuery->$method('places_id', $id);
            }
        } else if ($this->sub_type == HomeService::TYPE_COUNTRY) {
            $baseQuery = PostsCountries::query();
            if ($id instanceof Expression) {
                $baseQuery->whereRaw('countries_id' . $id);
            } else {
                $method = is_array($id) ? 'whereIn' : 'where';
                $baseQuery->$method('countries_id', $id);
            }
        } else if ($this->sub_type == HomeService::TYPE_CITY) {
            $baseQuery = PostsCities::query();
            if ($id instanceof Expression) {
                $baseQuery->whereRaw('cities_id' . $id);
            } else {
                $method = is_array($id) ? 'whereIn' : 'where';
                $baseQuery->$method('cities_id', $id);
            }
        }

        $baseQuery->whereHas('post.medias')
            ->when($this->authUser, function ($q) {
                $postsList = get_postlist4me($this->locationObj->id, false, $this->filter, HomeService::TYPE_CITY);
                return $q->whereIn('posts_id', $postsList);
            }, function ($q) { // for non login user
                $q->whereHas('post', function ($query) {
                    return $query->where('permission', Posts::POST_PERMISSION_PUBLIC);
                });
            });

        $collection = $baseQuery->orderBy('posts_id', 'desc')
            ->skip($this->skip)
            ->take($this->per_page)
            ->pluck('posts_id')->map(function ($id) {
                return collect([
                    'type' => PrimaryPostService::TYPE_POST,
                    'action' => 'publish',
                    'variable' => $id,
                ]);
            });

        if (count($collection) == 0) {
            $getCurrentSubTypeKey = array_search($this->sub_type, self::SUB_TYPES[$this->type]);
            $getNextSubTypeKey = $getCurrentSubTypeKey + 1;
            if (isset(self::SUB_TYPES[$this->type][$getNextSubTypeKey])) {
                $this->sub_type = self::SUB_TYPES[$this->type][$getNextSubTypeKey];
                return $this->showNewsfeedNewMedias();
            } else {
                $this->hasMore = false;
                // load about post
            }
        }

        // dd(DB::getQueryLog());
        return collect([
            'collection' => $collection
        ]);
    }

    /**
     * @return Collection
     */
    function showNewsfeedReports(): Collection
    {
        $baseQuery = Reports::query();
        $id = $this->queryDetailProvider();

        if ($this->sub_type == HomeService::TYPE_PLACE) {
            $baseQuery->whereHas('places', function ($q) use ($id) {
                $field = 'places_id';
                if ($id instanceof Expression) {
                    $q->whereRaw($field . $id);
                } else {
                    $method = is_array($id) ? 'whereIn' : 'where';
                    $q->$method($field, $id);
                }
            });
        } elseif (in_array($this->sub_type, [Reports::LOCATION_COUNTRY, Reports::LOCATION_CITY])) {
            $baseQuery->whereHas('location', function ($q) use ($id) {
                $q->where('location_type', $this->sub_type);
                $field = 'location_id';
                if ($id instanceof Expression) {
                    $q->whereRaw($field . $id);
                } else {
                    $method = is_array($id) ? 'whereIn' : 'where';
                    $q->$method($field, $id);
                }
            });
        }

        $baseQuery->when(count($this->filter), function ($q) {
            $users = getUsersByFilters($this->filter);
            $q->whereIn('users_id', $users);
        });

        $collection = $baseQuery->orderBy('id', 'desc')
            ->skip($this->skip)
            ->take($this->per_page)
            ->pluck('id')->map(function ($id) {

                $lastUpdatedLog = ActivityLog::query()->where([
                    'type' => PrimaryPostService::TYPE_REPORT,
                    'variable' => $id
                ])->whereIn('action', ['update'])->exists();

                return collect([
                    'type' => PrimaryPostService::TYPE_REPORT,
                    'action' => $lastUpdatedLog ? 'update' : 'create',
                    'variable' => $id,
                ]);
            });

        if (count($collection) == 0) {
            $getCurrentSubTypeKey = array_search($this->sub_type, self::SUB_TYPES[$this->type]);
            $getNextSubTypeKey = $getCurrentSubTypeKey + 1;
            if (isset(self::SUB_TYPES[$this->type][$getNextSubTypeKey])) {
                $this->sub_type = self::SUB_TYPES[$this->type][$getNextSubTypeKey];
                return $this->showNewsfeedReports();
            } else {
                $this->hasMore = false;
                // load about post
            }
        }

        // dd(DB::getQueryLog());
        return collect([
            'collection' => $collection
        ]);
    }

    /**
     * @return Collection
     */
    function showNewsfeedTripPlans(): Collection
    {
        $baseQuery = TripPlans::query()
            ->selectRaw('distinct(trips.id) as id, ((select count(*) from `posts_comments` where `posts_id`=`trips`.`id` and type="trip") 
                + (select count(*) from `trips_likes` where `trips_id`=`trips`.`id`) 
                + (select count(*) from `posts_shares` where `posts_type_id`=`trips`.`id` and type="trip")) as ranks')
            ->leftjoin('trips_places', 'trips_places.trips_id', '=', 'trips.id')
            ->whereRaw("EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `deleted_at` IS NULL AND `trips_places`.`deleted_at` IS NULL)")
            ->whereHas('author')
            ->when(count($this->filter), function ($q) {
                $triplist = get_triplist4me($this->filter);
                $q->whereIn('trips_id', $triplist);
            });

        $field = when($this->sub_type, [
            HomeService::TYPE_CITY    => 'cities_id',
            HomeService::TYPE_PLACE    => 'places_id',
            HomeService::TYPE_COUNTRY    => 'countries_id',
        ]);

        $id = $this->queryDetailProvider();
        if ($id instanceof Expression) {
            $baseQuery->whereRaw($field . $id);
        } else {
            $method = is_array($id) ? 'whereIn' : 'where';
            $baseQuery->$method($field, $id);
        }

        $collection = $baseQuery->orderBy('ranks', 'DESC')
            ->skip($this->skip)->take($this->per_page)
            ->pluck('id')->map(function ($id) {

                $lastUpdatedLog = ActivityLog::query()->where([
                    'type' => PrimaryPostService::TYPE_TRIP,
                    'variable' => $id
                ])->whereIn('action', ['plan_updated', 'update'])->exists();

                return collect([
                    'type' => PrimaryPostService::TYPE_TRIP,
                    'action' => $lastUpdatedLog ? 'plan_updated' : 'create',
                    'variable' => $id,
                ]);
            });

        if (count($collection) == 0) {
            $getCurrentSubTypeKey = array_search($this->sub_type, self::SUB_TYPES[$this->type]);
            $getNextSubTypeKey = $getCurrentSubTypeKey + 1;
            if (isset(self::SUB_TYPES[$this->type][$getNextSubTypeKey])) {
                $this->sub_type = self::SUB_TYPES[$this->type][$getNextSubTypeKey];
                return $this->showNewsfeedTripPlans();
            } else {
                $this->hasMore = false;
                // load about post
            }
        }

        // dd(DB::getQueryLog());
        return collect([
            'collection' => $collection
        ]);
    }

    /**
     * @return Collection
     * @throws GeneralException
     */
    function showNewsfeedReviews(): Collection
    {
        $baseQuery = Reviews::query();

        if ($this->type == HomeService::TYPE_PLACE) {
            $baseQuery->where('places_id', $this->locationObj->id);
        } else {
            throw new GeneralException('only place have review section', ApiResponse::BAD_REQUEST);
        }

        $collection = $baseQuery->orderBy('id', 'desc')
            ->skip($this->skip)
            ->take($this->per_page)
            ->pluck('id')->map(function ($id) {
                return collect([
                    'type' => PrimaryPostService::TYPE_REVIEW,
                    'action' => HomeService::TYPE_PLACE,
                    'variable' => $id,
                ]);
            });

        if (count($collection) == 0) {
            $this->hasMore = false;
            // load about post
        }

        // dd(DB::getQueryLog());
        return collect([
            'collection' => $collection
        ]);
    }

    /**
     * @return Collection
     */
    function showNewsfeedEvents(): Collection
    {
        $baseQuery = Events::query()->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"');

        $field = when($this->sub_type, [
            HomeService::TYPE_CITY    => 'cities_id',
            HomeService::TYPE_PLACE    => 'places_id',
            HomeService::TYPE_COUNTRY    => 'countries_id',
        ]);

        $id = $this->queryDetailProvider();
        if ($id instanceof Expression) {
            $baseQuery->whereRaw($field . $id);
        } else {
            $method = is_array($id) ? 'whereIn' : 'where';
            $baseQuery->$method($field, $id);
        }

        $collection = $baseQuery->orderBy('id', 'desc')
            ->skip($this->skip)
            ->take($this->per_page)
            ->pluck('id')->map(function ($id) {
                return collect([
                    'type' => PrimaryPostService::TYPE_EVENT,
                    'action' => 'show',
                    'variable' => $id,
                ]);
            });

        if (count($collection) == 0) {
            $getCurrentSubTypeKey = array_search($this->sub_type, self::SUB_TYPES[$this->type]);
            $getNextSubTypeKey = $getCurrentSubTypeKey + 1;
            if (isset(self::SUB_TYPES[$this->type][$getNextSubTypeKey])) {
                $this->sub_type = self::SUB_TYPES[$this->type][$getNextSubTypeKey];
                return $this->showNewsfeedEvents();
            } else {
                $this->hasMore = false;
                // load about post
            }
        }

        // dd(DB::getQueryLog());
        return collect([
            'collection' => $collection
        ]);
    }

    /**
     * @return Collection
     */
    function showNewsfeedDiscussions(): Collection
    {
        $baseQuery = Discussion::query()
            ->when(count($this->filter), function ($q) {
                $users_id_array = getUsersByFilters($this->filter);
                $q->whereIn('users_id', $users_id_array);
            });

        $id = $this->queryDetailProvider();
        $baseQuery->where('destination_type', $this->sub_type);

        if ($id instanceof Expression) {
            $baseQuery->whereRaw('destination_id' . $id);
        } else {
            $method = is_array($id) ? 'whereIn' : 'where';
            $baseQuery->$method('destination_id', $id);
        }

        $collection = $baseQuery->orderBy('id', 'desc')
            ->skip($this->skip)
            ->take($this->per_page)
            ->pluck('id')->map(function ($id) {
                return collect([
                    'type' => PrimaryPostService::TYPE_DISCUSSION,
                    'action' => 'create',
                    'variable' => $id,
                ]);
            });

        if (count($collection) == 0) {
            $getCurrentSubTypeKey = array_search($this->sub_type, self::SUB_TYPES[$this->type]);
            $getNextSubTypeKey = $getCurrentSubTypeKey + 1;
            if (isset(self::SUB_TYPES[$this->type][$getNextSubTypeKey])) {
                $this->sub_type = self::SUB_TYPES[$this->type][$getNextSubTypeKey];
                return $this->showNewsfeedDiscussions();
            } else {
                $this->hasMore = false;
                // load about post
            }
        }

        // dd(DB::getQueryLog());
        return collect([
            'collection' => $collection
        ]);
    }

    /**
     * @return int|array|Expression
     */
    private function queryDetailProvider()
    {
        switch ($this->type) {
            case HomeService::TYPE_COUNTRY:
                switch ($this->sub_type) {
                    case HomeService::TYPE_COUNTRY:
                        return $this->locationObj->id;
                    case HomeService::TYPE_CITY:
                        return Cities::query()->where('countries_id', $this->locationObj->id)->pluck('id')->toArray();
                    case HomeService::TYPE_PLACE:
                        // return Place::query()->where('countries_id', $this->locationObj->id)->pluck('id')->toArray();
                        return DB::raw(" in (select id from places where countries_id = {$this->locationObj->id})");
                }
                break;

            case HomeService::TYPE_CITY:
                switch ($this->sub_type) {
                    case HomeService::TYPE_COUNTRY:
                        return $this->locationObj->countries_id;
                    case HomeService::TYPE_CITY:
                        return $this->locationObj->id;
                    case HomeService::TYPE_PLACE:
                        // return Place::query()->where('cities_id', $this->locationObj->id)->pluck('id')->toArray();
                        return DB::raw(" in (select id from places where cities_id = {$this->locationObj->id})");
                }
                break;

            case HomeService::TYPE_PLACE:
                switch ($this->sub_type) {
                    case HomeService::TYPE_COUNTRY:
                        return $this->locationObj->countries_id;
                    case HomeService::TYPE_CITY:
                        return $this->locationObj->cities_id;
                    case HomeService::TYPE_PLACE:
                        return $this->locationObj->id;
                }
                break;
        }
    }

    private function apiGetPostsIdForAllFeed($top = false)
    {
        /** initialize local vars */
        $place_id = $this->locationObj->id;
        $page = $this->page;
        $type =  $this->type;
        $filter = $this->filter;
        $limit = $this->per_page;
        $followers = [];
        $friends = [];
        $filter_users_id = [];
        $uniqueFriendsAndFollowers = [];
        $blocked_users_list = [];
        if (Auth::check()) {
            $my_id = Auth::user()->id;
            $followers  = get_followerlist();
            $friends    = get_friendlist();
            $uniqueFriendsAndFollowers  = collect($followers)->merge($friends)->unique()->toArray();
            $filter_users_id = getUsersByFilters($filter);
            $followers[] = $my_id;
            $friends[] = $my_id;
            $blocked_users_list = blocked_users_list();
        }
        $skip = ($page - 1) * $limit;
        $results = [];
        if ($type  == 'place') {
            $baseQuery = Discussion::query()
                ->select(DB::raw('id as row_id, "discussion" AS rowtype, created_at AS created_date'))
                ->where(['destination_id' => $place_id, 'destination_type' => 'place'])
                ->when($top, function ($q) {
                    $q->selectRaw('(select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`) as rank');
                })->when(Auth::check() && count($filter), function ($q) use ($filter_users_id) {
                    $q->whereIn('users_id', $filter_users_id);
                })->when(Auth::check() && !count($filter), function ($q) use ($uniqueFriendsAndFollowers) {
                    $q->whereIn('users_id', $uniqueFriendsAndFollowers);
                })->when(Auth::check() && count($blocked_users_list), function ($q) use ($blocked_users_list) {
                    $q->whereNotIn('users_id', $blocked_users_list);
                })
                ->UNIONAll(
                    Reports::query()
                        ->whereHas('places', function ($q) use ($place_id) {
                            return $q->where('places_id', $place_id);
                        })
                        ->select(DB::raw('id as row_id, "reports" AS rowtype, created_at AS created_date'))
                        ->when($top, function ($q) {
                            $q->selectRaw('((select count(*) from `posts_comments` where `posts_id`=`reports`.`id` and type ="report" ) 
                                    + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `reports_views` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `posts_shares` where `posts_type_id`=`reports`.`id`  and type ="report" )) as rank');
                        })->when(Auth::check() && count($filter), function ($q) use ($filter_users_id) {
                            $q->whereIn('users_id', $filter_users_id);
                        })->when(Auth::check() && !count($filter), function ($q) use ($uniqueFriendsAndFollowers) {
                            $q->whereIn('users_id', $uniqueFriendsAndFollowers);
                        })->when(Auth::check() && count($blocked_users_list), function ($q) use ($blocked_users_list) {
                            $q->whereNotIn('users_id', $blocked_users_list);
                        })
                )->UNIONAll(
                    TripPlans::query()->where('active', 1)
                        ->select(DB::raw('id as row_id, "plan" AS rowtype, created_at AS created_date'))
                        ->when($top, function ($q) {
                            $q->selectRaw('((select count(*) from `posts_comments` where `posts_id`=`trips`.`id` and type="trip") 
                                + (select count(*) from `trips_likes` where `trips_id`=`trips`.`id`) 
                                + (select count(*) from `posts_shares` where `posts_type_id`=`trips`.`id` and type="trip")) as rank');
                        })->when(Auth::check() && count($filter), function ($q) use ($filter, $place_id) {
                            $q->whereIn('id', get_triplist4me($filter, $place_id, 'place'));
                        })->when(Auth::check() && !count($filter), function ($q) use ($place_id) {
                            $q->whereIn('id', get_triplist4me(false, $place_id, 'place'));
                        })->when(!Auth::check(), function ($q) {
                            $q->where('privacy', TripsService::PRIVACY_PUBLIC)
                                ->whereRaw("EXISTS (SELECT * FROM `users` WHERE `trips`.`users_id` = `users`.`id` AND `users`.`deleted_at` IS NULL) AND EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `deleted_at` IS NULL AND `trips_places`.`deleted_at` IS NULL)");
                        })->when(Auth::check() && count($blocked_users_list), function ($q) use ($blocked_users_list) {
                            $q->whereNotIn('users_id', $blocked_users_list);
                        })
                )->UNIONAll(
                    PostsPlaces::query()
                        ->select(DB::raw('posts.id as row_id, "regular" AS rowtype, posts.created_at AS created_date'))
                        ->when($top, function ($q) {
                            $q->selectRaw('((select count(*) from `posts_comments` where `posts_id`=`posts_places`.`posts_id`) 
                                + (select count(*) from `posts_likes` where `posts_id`=`posts_places`.`posts_id`) 
                                + (select count(*) from `posts_shares` where `posts_id`=`posts_places`.`posts_id`)) as rank');
                        })
                        ->where('places_id', $place_id)
                        ->leftJoin('posts', function ($join) {
                            $join->on('posts_places.posts_id', '=', 'posts.id');
                        })
                        ->whereRaw('posts.deleted_at is null and posts.id is not null')

                        ->when(Auth::check() && count($filter), function ($q) use ($place_id, $top, $filter) {
                            $q->whereIn('posts_id', get_postlist4me($place_id, $top, $filter, 'place'));
                        })->when(Auth::check() && !count($filter), function ($q) use ($place_id, $top) {
                            $q->whereIn('posts_id', get_postlist4me($place_id, $top, false, 'place'));
                        })->when(!Auth::check(), function ($q) {
                            $q->whereRaw('posts.permission=0');
                        })
                );

            if (!$top) {
                $baseQuery->UNIONAll(
                    Reviews::query()
                        ->select(DB::raw('id as row_id, "review" AS rowtype, created_at AS created_date'))
                        ->where('places_id', $place_id)
                        ->when(Auth::check() && count($blocked_users_list), function ($q) use ($blocked_users_list) {
                            $q->whereNotIn('users_id', $blocked_users_list);
                        })
                );
            }
        } else if ($type  == 'country') {
            $baseQuery = Discussion::query()
                ->select(DB::raw('id as row_id, "discussion" AS rowtype, created_at AS created_date'))
                ->where(['destination_id' => $place_id, 'destination_type' => 'country'])
                ->when($top, function ($q) {
                    $q->selectRaw('(select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`) as rank');
                })->when(Auth::check() && count($filter), function ($q) use ($filter_users_id) {
                    $q->whereIn('users_id', $filter_users_id);
                })->when(Auth::check() && !count($filter), function ($q) use ($uniqueFriendsAndFollowers) {
                    $q->whereIn('users_id', $uniqueFriendsAndFollowers);
                })->when(Auth::check() && count($blocked_users_list), function ($q) use ($blocked_users_list) {
                    $q->whereNotIn('users_id', $blocked_users_list);
                })
                ->UNIONAll(
                    Reports::query()
                        ->whereHas('location', function ($q) use ($place_id) {
                            return $q->where(['location_type' => Reports::LOCATION_COUNTRY, 'location_id' => $place_id]);
                        })
                        ->select(DB::raw('id as row_id, "reports" AS rowtype, created_at AS created_date'))
                        ->when($top, function ($q) {
                            $q->selectRaw('((select count(*) from `posts_comments` where `posts_id`=`reports`.`id` and type ="report" ) 
                                    + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `reports_views` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `posts_shares` where `posts_type_id`=`reports`.`id`  and type ="report" )) as rank');
                        })->when(Auth::check() && count($filter), function ($q) use ($filter_users_id) {
                            $q->whereIn('users_id', $filter_users_id);
                        })->when(Auth::check() && !count($filter), function ($q) use ($uniqueFriendsAndFollowers) {
                            $q->whereIn('users_id', $uniqueFriendsAndFollowers);
                        })->when(Auth::check() && count($blocked_users_list), function ($q) use ($blocked_users_list) {
                            $q->whereNotIn('users_id', $blocked_users_list);
                        })
                )->UNIONAll(
                    TripPlans::query()->where('active', 1)
                        ->select(DB::raw('id as row_id, "plan" AS rowtype, created_at AS created_date'))
                        ->when($top, function ($q) {
                            $q->selectRaw('((select count(*) from `posts_comments` where `posts_id`=`trips`.`id` and type="trip") 
                                + (select count(*) from `trips_likes` where `trips_id`=`trips`.`id`) 
                                + (select count(*) from `posts_shares` where `posts_type_id`=`trips`.`id` and type="trip")) as rank');
                        })->when(Auth::check() && count($filter), function ($q) use ($filter, $place_id) {
                            $q->whereIn('id', get_triplist4me($filter, $place_id, 'country'));
                        })->when(Auth::check() && !count($filter), function ($q) use ($place_id) {
                            $q->whereIn('id', get_triplist4me(false, $place_id, 'country'));
                        })->when(!Auth::check(), function ($q) {
                            $q->where('privacy', TripsService::PRIVACY_PUBLIC)
                                ->whereRaw("EXISTS (SELECT * FROM `users` WHERE `trips`.`users_id` = `users`.`id` AND `users`.`deleted_at` IS NULL) AND EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `deleted_at` IS NULL AND `trips_places`.`deleted_at` IS NULL)");
                        })->when(Auth::check() && count($blocked_users_list), function ($q) use ($blocked_users_list) {
                            $q->whereNotIn('users_id', $blocked_users_list);
                        })
                )->UNIONAll(
                    PostsCountries::query()
                        ->select(DB::raw('posts.id as row_id, "regular" AS rowtype, posts.created_at AS created_date'))
                        ->when($top, function ($q) {
                            $q->selectRaw('((select count(*) from `posts_comments` where `posts_id`=`posts_countries`.`posts_id`) 
                            + (select count(*) from `posts_likes` where `posts_id`=`posts_countries`.`posts_id`) 
                            + (select count(*) from `posts_shares` where `posts_id`=`posts_countries`.`posts_id`)) as rank');
                        })
                        ->where('countries_id', $place_id)
                        ->leftJoin('posts', function ($join) {
                            $join->on('posts_countries.posts_id', '=', 'posts.id');
                        })
                        ->whereRaw('posts.deleted_at is null and posts.id is not null')
                        ->when(Auth::check() && count($filter), function ($q) use ($place_id, $top, $filter) {
                            $q->whereIn('posts_id', get_postlist4me($place_id, $top, $filter, 'country'));
                        })->when(Auth::check() && !count($filter), function ($q) use ($place_id, $top) {
                            $q->whereIn('posts_id', get_postlist4me($place_id, $top, false, 'country'));
                        })->when(!Auth::check(), function ($q) {
                            $q->whereRaw('posts.permission=0');
                        })
                );
        } else if ($type == 'city') {

            $baseQuery = Discussion::query()
                ->select(DB::raw('id as row_id, "discussion" AS rowtype, created_at AS created_date'))
                ->where(['destination_id' => $place_id, 'destination_type' => 'city'])
                ->when($top, function ($q) {
                    $q->selectRaw('(select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`) as rank');
                })->when(Auth::check() && count($filter), function ($q) use ($filter_users_id) {
                    $q->whereIn('users_id', $filter_users_id);
                })->when(Auth::check() && !count($filter), function ($q) use ($uniqueFriendsAndFollowers) {
                    $q->whereIn('users_id', $uniqueFriendsAndFollowers);
                })->when(Auth::check() && count($blocked_users_list), function ($q) use ($blocked_users_list) {
                    $q->whereNotIn('users_id', $blocked_users_list);
                })
                ->UNIONAll(
                    Reports::query()
                        ->whereHas('location', function ($q) use ($place_id) {
                            return $q->where(['location_type' => Reports::LOCATION_CITY, 'location_id' => $place_id]);
                        })
                        ->select(DB::raw('id as row_id, "reports" AS rowtype, created_at AS created_date'))
                        ->when($top, function ($q) {
                            $q->selectRaw('((select count(*) from `posts_comments` where `posts_id`=`reports`.`id` and type ="report" ) 
                                    + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `reports_views` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `posts_shares` where `posts_type_id`=`reports`.`id`  and type ="report" )) as rank');
                        })->when(Auth::check() && count($filter), function ($q) use ($filter_users_id) {
                            $q->whereIn('users_id', $filter_users_id);
                        })->when(Auth::check() && !count($filter), function ($q) use ($uniqueFriendsAndFollowers) {
                            $q->whereIn('users_id', $uniqueFriendsAndFollowers);
                        })->when(Auth::check() && count($blocked_users_list), function ($q) use ($blocked_users_list) {
                            $q->whereNotIn('users_id', $blocked_users_list);
                        })
                )->UNIONAll(
                    TripPlans::query()->where('active', 1)
                        ->select(DB::raw('id as row_id, "plan" AS rowtype, created_at AS created_date'))
                        ->when($top, function ($q) {
                            $q->selectRaw('((select count(*) from `posts_comments` where `posts_id`=`trips`.`id` and type="trip") 
                                + (select count(*) from `trips_likes` where `trips_id`=`trips`.`id`) 
                                + (select count(*) from `posts_shares` where `posts_type_id`=`trips`.`id` and type="trip")) as rank');
                        })->when(Auth::check() && count($filter), function ($q) use ($filter, $place_id) {
                            $q->whereIn('id', get_triplist4me($filter, $place_id, 'city'));
                        })->when(Auth::check() && !count($filter), function ($q) use ($place_id) {
                            $q->whereIn('id', get_triplist4me(false, $place_id, 'city'));
                        })->when(!Auth::check(), function ($q) {
                            $q->where('privacy', TripsService::PRIVACY_PUBLIC)
                                ->whereRaw("EXISTS (SELECT * FROM `users` WHERE `trips`.`users_id` = `users`.`id` AND `users`.`deleted_at` IS NULL) AND EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `deleted_at` IS NULL AND `trips_places`.`deleted_at` IS NULL)");
                        })->when(Auth::check() && count($blocked_users_list), function ($q) use ($blocked_users_list) {
                            $q->whereNotIn('users_id', $blocked_users_list);
                        })
                )->UNIONAll(
                    PostsCities::query()
                        ->select(DB::raw('posts.id as row_id, "regular" AS rowtype, posts.created_at AS created_date'))
                        ->when($top, function ($q) {
                            $q->selectRaw('((select count(*) from `posts_comments` where `posts_id`=`posts_cities`.`posts_id`) 
                            + (select count(*) from `posts_likes` where `posts_id`=`posts_cities`.`posts_id`) 
                            + (select count(*) from `posts_shares` where `posts_id`=`posts_cities`.`posts_id`)) as rank');
                        })
                        ->where('cities_id', $place_id)
                        ->leftJoin('posts', function ($join) {
                            $join->on('posts_cities.posts_id', '=', 'posts.id');
                        })
                        ->whereRaw('posts.deleted_at is null and posts.id is not null')
                        ->when(Auth::check() && count($filter), function ($q) use ($place_id, $top, $filter) {
                            $q->whereIn('posts_id', get_postlist4me($place_id, $top, $filter, 'city'));
                        })->when(Auth::check() && !count($filter), function ($q) use ($place_id, $top) {
                            $q->whereIn('posts_id', get_postlist4me($place_id, $top, false, 'city'));
                        })->when(!Auth::check(), function ($q) {
                            $q->whereRaw('posts.permission=0');
                        })
                );
        }

        if (!$top) {
            $baseQuery->orderBy('created_date', 'DESC');
        } else {
            $baseQuery->orderBy('rank', 'DESC');
        }
        $results = $baseQuery->skip($skip)->take($limit)->get();

        // dd(DB::getQueryLog());

        $posts_collection = [];
        if (count($results)) {
            foreach ($results as $gpdc) {
                $posts_collection[] = array(
                    'id' => $gpdc->row_id,
                    'type' =>  $gpdc->rowtype,
                    'place' => isset($gpdc->place) ? $gpdc->place : 0,
                    'city' => isset($gpdc->city) ? $gpdc->city : 0,
                    'timestamp' => $gpdc->created_date
                );
            }
        }
        return collect([
            'collection' => $posts_collection
        ]);
    }
}
