<?php

namespace App\Services\Newsfeed;

use App\Http\Constants\CommonConst;
use App\Models\ActivityLog\ActivityLog;
use App\Models\ActivityMedia\Media;
use App\Models\City\Cities;
use App\Models\City\CitiesFollowers;
use App\Models\City\CitiesHolidays;
use App\Models\City\CitiesTranslations;
use App\Models\Country\Countries;
use App\Models\Country\CountriesFollowers;
use App\Models\Country\CountriesHolidays;
use App\Models\Country\CountriesTranslations;
use App\Models\Discussion\Discussion;
use App\Models\Events\Events;
use App\Models\Holidays\HolidaysMedias;
use App\Models\Holidays\HolidaysTranslations;
use App\Models\Place\Place;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsShares;
use App\Models\Reports\Reports;
use App\Models\Reviews\Reviews;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripContentPostLike;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsLikes;
use App\Models\TripPlans\TripsMediasShares;
use App\Models\TripPlans\TripsPlacesShares;
use App\Models\TripPlans\TripsShares;
use App\Models\User\User;
use App\Models\User\UsersContentLanguages;
use App\Models\User\UserWatchHistory;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Api\LoadedFeedService;
use App\Services\Api\PrimaryPostService;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Services\Api\ShareService;
use App\Services\Trips\TripsService;
use Illuminate\Support\Facades\Auth;

defined("DUMMY_DATA_MODE") or define("DUMMY_DATA_MODE", false);

class HomeService
{
    // Types
    const TYPE_POST         = "post";   // General post (text , media , check-in) +  External or internal links(others) + share
    const TYPE_TRIP         = "trip";
    const TYPE_EVENT        = "event";
    const TYPE_REPORT       = "report"; // Travlog
    const TYPE_DISCUSSION   = "discussion";
    const TYPE_REVIEW       = "review";
    const TYPE_COUNTRY      = "country";
    const TYPE_CITY         = "city";
    const TYPE_PLACE        = "place";


    const SESSION_LOADED_KEYS = [
        self::TYPE_POST => 'loaded_posts_ids',
        self::TYPE_DISCUSSION => 'loaded_discussion_ids',
        self::TYPE_REPORT => 'loaded_reports_ids',
        self::TYPE_TRIP => 'loaded_trips_ids',
    ];

    const SECONDARY_POST = [
        PrimaryPostService::SECONDARY_TOP_PLACES,
        PrimaryPostService::SECONDARY_NEW_FOLLOWER,
        'my_friend_today_trip',
        PrimaryPostService::SECONDARY_CHECKIN,
        PrimaryPostService::SECONDARY_VIDEO_YOU_MIGHT_LIKE,
        PrimaryPostService::SECONDARY_PLACES_YOU_MIGHT_LIKE,
        PrimaryPostService::SECONDARY_RECOMMENDED_PLACES,
        PrimaryPostService::SECONDARY_COLLECTIVE_FOLLOWERS_PLACES,
        PrimaryPostService::SECONDARY_DISCOVER_NEW_TRAVELLERS,
        PrimaryPostService::SECONDARY_TRENDING_DESTINATIONS,
        PrimaryPostService::SECONDARY_NEW_PEOPLE,
        PrimaryPostService::SECONDARY_TRENDING_EVENTS,
        PrimaryPostService::SECONDARY_HOLIDAY,
    ];

    /**
     * @param int $page
     * @param int $type
     */
    public function  getPrimaryPosts($page = 1, $per_page = 15)
    {
        // return collect([]);
        $skip = ($page - 1) * $per_page;;
        $withoutShareWhereRaw   = $this->getWhereRawOfActivityLog();
        $withShareWhereRaw      = $this->getWhereRawOfActivityLog(TRUE);

        $removedSharedPostDuplicationGroupByRaw = "`action`,
            CASE
                WHEN `action` = 'plan_shared' THEN (SELECT trip_id FROM trips_shares WHERE id=variable)
                WHEN `action` = 'plan_step_shared' THEN (SELECT trip_place_id FROM trip_places_shares WHERE id=variable)
                WHEN `action` = 'plan_media_shared' THEN (SELECT trip_media_id FROM trip_medias_shares WHERE id=variable)
                ELSE (SELECT posts_type_id FROM posts_shares WHERE id=variable)
            END"; // ELSE part for post, report, review, event, country, city, place

        if (Auth::check()) { // get login user following
            $myFollowigs            = UsersFollowers::where('followers_id', auth()->id())->where('follow_type', 1)->pluck('users_id')->toArray();
            $finalUsers             =  array_merge($myFollowigs, [auth()->id()]);

            $baseQuery = ActivityLog::query()
                ->whereRaw($withoutShareWhereRaw)
                ->whereIn('users_id', $finalUsers)
                ->whereHas('user', function ($q) {
                    $q->whereDoesntHave('user_blocks');
                })
                ->groupBy(DB::raw('`type`,`action`,`variable`'))
                ->UNIONAll(
                    // get unique shared post
                    ActivityLog::query()
                        ->whereRaw($withShareWhereRaw)
                        ->whereIn('users_id', $finalUsers)
                        ->whereHas('user', function ($q) {
                            $q->whereDoesntHave('user_blocks');
                        })
                        ->where('type', 'share')
                        ->groupBy(DB::raw($removedSharedPostDuplicationGroupByRaw))
                );
        } else {
            $baseQuery = ActivityLog::query()
                ->whereRaw($withoutShareWhereRaw)
                ->groupBy(DB::raw('`type`,`action`,`variable`'))
                ->UNIONAll(
                    // get unique shared post
                    ActivityLog::query()
                        ->whereRaw($withShareWhereRaw)
                        ->where('type', 'share')
                        ->groupBy(DB::raw($removedSharedPostDuplicationGroupByRaw))
                );
        }

        // fetch posts
        // dd($baseQuery->orderBy('id', 'DESC')->toSql());
        $posts = $baseQuery->orderBy('id', 'DESC')->skip($skip)->take($per_page)->get();

        // save in session
        $discussion_id_session = sessionNullSafety(self::SESSION_LOADED_KEYS[self::TYPE_DISCUSSION], []);
        $posts_id_session = sessionNullSafety(self::SESSION_LOADED_KEYS[self::TYPE_POST], []);
        $trips_id_session = sessionNullSafety(self::SESSION_LOADED_KEYS[self::TYPE_TRIP], []);
        $travlog_id_session = sessionNullSafety(self::SESSION_LOADED_KEYS[self::TYPE_REPORT], []);
        foreach ($posts as $_post) {
            if ($_post->type  == self::TYPE_DISCUSSION && in_array($_post->action, ['create'])) {
                $discussion_id_session[] = $_post->variable;
            }
            if ($_post->type  == self::TYPE_POST && in_array($_post->action, ['publish'])) {
                $posts_id_session[] = $_post->variable;
            }
            if ($_post->type  == self::TYPE_TRIP && in_array($_post->action, ['create', 'plan_updated'])) {
                $trips_id_session[] = $_post->variable;
            }
            if ($_post->type  == self::TYPE_REPORT && in_array($_post->action, ['create'])) {
                $travlog_id_session[] = $_post->variable;
            }
        }
        session([self::SESSION_LOADED_KEYS[self::TYPE_DISCUSSION] =>  $discussion_id_session]);
        session([self::SESSION_LOADED_KEYS[self::TYPE_POST] =>  $posts_id_session]);
        session([self::SESSION_LOADED_KEYS[self::TYPE_TRIP] =>  $trips_id_session]);
        session([self::SESSION_LOADED_KEYS[self::TYPE_REPORT] =>  $travlog_id_session]);

        return $posts;
    }

    /**
     * @param int $post_id
     * @param mixed $type
     */
    public function updateTempUserFeed($post_id = 0, $type = false)
    {
        if ($type) {
            UserWatchHistory::create(['posts_id' => $post_id, 'type' => $type, 'users_id' => auth()->id(), 'watch_flag' => 1]);
        }
    }

    /**
     * @param string $type
     * @param int $duration
     * @return array
     */
    public function getWatchedPost($type, $duration = 1)
    {
        return UserWatchHistory::where(['users_id' => auth()->id(), 'type' => $type])->whereDate('created_at', '>', Carbon::now()->subDays($duration))->pluck('posts_id')->toArray();
    }

    /**
     * @return array
     */
    public function userPlacesContent()
    {
        $followedPlaces = auth()->user()->followedPlaces()->pluck('places_id')->toArray();
        return $this->getFollowContent(self::TYPE_PLACE, $followedPlaces);
    }

    /**
     * @return array
     */
    public function userCitiesContent()
    {
        $followedCities = auth()->user()->followedCities()->pluck('cities_id')->toArray();
        return $this->getFollowContent(self::TYPE_CITY, $followedCities);
    }

    /**
     * @return array
     */
    public function userCountriesContent()
    {
        $followedCountries = auth()->user()->followedCountries()->pluck('countries_id')->toArray();
        return $this->getFollowContent(self::TYPE_COUNTRY, $followedCountries);
    }

    /**
     * @param bool $new
     * @param bool $allTime
     */
    public function getTrendingPosts($new = false, $allTime = false)
    {
        $topWeeklyPost = [];
        $already_posted_ids = [];

        $availableTypes = [
            self::TYPE_REPORT => self::TYPE_REPORT,
            self::TYPE_DISCUSSION => self::TYPE_DISCUSSION,
            self::TYPE_POST => self::TYPE_POST,
            self::TYPE_TRIP => self::TYPE_TRIP
        ];

        foreach ($availableTypes as $key => $value) {
            $already_posted_ids[$key] = sessionNullSafety(self::SESSION_LOADED_KEYS[$value], []);
        }

        $language = getDesiredLanguage();
        if (isset($language[0])) {
            $language = $language[0];
        } else {
            $language = UsersContentLanguages::DEFAULT_LANGUAGES;
        }
        if ($new) {
            foreach ($availableTypes as $chosenType) {
                $this->trendingPost($topWeeklyPost, $chosenType, $already_posted_ids, $language, $new, $allTime);
            }
        } else {
            $this->trendingPost($topWeeklyPost, array_rand($availableTypes), $already_posted_ids, $language, $new, $allTime);
        }

        return  $topWeeklyPost;
    }

    private function trendingPost(&$topWeeklyPost, $type, $alreadyPostedIds, $language, $new = false, $allTime = null)
    {
        $postsMinDates = [
            self::TYPE_REPORT => '2019-01-01',
            self::TYPE_DISCUSSION => '2019-01-01',
            self::TYPE_POST => '2019-01-01',
            self::TYPE_TRIP => '2019-01-01',
        ];

        $defaultLanguage = UsersContentLanguages::DEFAULT_LANGUAGES;
        $post = new Collection();
        $watchedDuration = 1;
        $creation = Carbon::now()->subDays(7);
        $likesCount = 5;
        if ($allTime) {
            $watchedDuration = 180;
        }

        $loadedIds = array_merge($alreadyPostedIds[$type], $this->getWatchedPost($type, $watchedDuration));

        if ($type == self::TYPE_POST) {
            $post = Posts::withCount(['likes' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'allComments' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'postShares' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }])
                ->having(DB::raw('likes_count + all_comments_count + post_shares_count'), '>', $likesCount)
                ->where('users_id', '<>', auth()->id())
                ->whereNotIn('id', $loadedIds)
                ->orderByDesc(DB::raw('likes_count + all_comments_count + post_shares_count'));

            if ($allTime) {
                $post->where('created_at', '>', $postsMinDates[$type]);
            };
        } else if ($type == self::TYPE_DISCUSSION) {
            $post = Discussion::withCount(['replies'  => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }])
                ->having('replies_count', '>=', $likesCount)
                ->whereNotIn('id', $loadedIds)
                ->orderByDesc('replies_count');

            if ($allTime) {
                $post->where('created_at', '>', $postsMinDates[$type]);
            };
        } else if ($type == self::TYPE_TRIP) {
            $post = TripPlans::withCount(['likes' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'allComments' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'postShares' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'place_comments' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('trips_places_comments.created_at', '>', $creation);
                }
            }])
                ->having(DB::raw('likes_count + all_comments_count + post_shares_count + place_comments_count'), '>', $likesCount)
                ->where('users_id', '<>', auth()->id())
                ->whereNotIn('id', array_merge(get_triplist4me(), $loadedIds))
                ->orderByDesc(DB::raw('likes_count + all_comments_count + post_shares_count + place_comments_count'));

            if ($allTime) {
                $post->where('created_at', '>', $postsMinDates[$type]);
            };
        } else if ($type == self::TYPE_REPORT) {
            $post = Reports::withCount(['likes' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'allComments' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'postShares' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }])
                ->having(DB::raw('likes_count + all_comments_count + post_shares_count'), '>', $likesCount)
                ->whereHas('cover')
                ->where('users_id', '<>', auth()->id())
                ->where('flag', 1)
                ->whereNotIn('id', $loadedIds)
                ->whereNotNull('published_date')
                ->orderByDesc(DB::raw('likes_count + all_comments_count + post_shares_count'));

            if ($allTime) {
                $post->where('created_at', '>', $postsMinDates[$type]);
            };
        }

        $postQuery = clone $post;
        $topWeeklyPost[$type] = $post->where('language_id',  $language)->first();

        if (!$new && $language != $defaultLanguage && !$topWeeklyPost[$type]) {
            $topWeeklyPost[$type] = $postQuery->where('language_id',  $defaultLanguage)->first();
        }

        if (!empty($topWeeklyPost[$type])) {
            $this->updateTempUserFeed($topWeeklyPost[$type]->id, $type);
        }
    }

    /**
     * @param string $type
     * @param array $followingLocations
     * @return array
     */
    private function getFollowContent($type, $followingLocations)
    {
        $top = [];
        $days = 1;
        $final_attc = [];
        if (!empty($followingLocations)) {
            $loadedPosts = [];
            foreach (self::SESSION_LOADED_KEYS as $type => $key) {
                $loadedPosts[$type] = array_merge(sessionNullSafety($key, []), $this->getWatchedPost($type, $days));
            }

            switch ($type) {
                case self::TYPE_COUNTRY:
                    $top = getFollowedCountriesTopContent($followingLocations, $loadedPosts);
                    break;
                case self::TYPE_CITY:
                    $top = getFollowedCitiesTopContent($followingLocations, $loadedPosts);
                    break;
                case self::TYPE_PLACE:
                    $top = getFollowedPlacesTopContent($followingLocations, $loadedPosts);
                    break;
            }
            foreach (array_keys($loadedPosts) as $key) {
                if (isset($top[$key]) && count($top[$key]) > 0) {
                    $final_attc[$key] =  $top[$key][0];
                }
            }
        }
        if (!empty($final_attc)) {
            $final_attc_rand =  array_rand($final_attc);
        }

        $posts = [];
        if (isset($final_attc_rand) && isset($final_attc[$final_attc_rand])) {
            $posts =  [$final_attc_rand => $final_attc[$final_attc_rand]];
        }

        foreach ($posts as $key => $id) {
            $this->updateTempUserFeed($id, $key);
        }
        return $posts;
    }

    private function getWhereRawOfActivityLog(bool $isSharedRaw = false): string
    {
        // $where_condition    = __preparedHomeNewsfeedWhereCondition();
        $where_condition = '';
        $condition_items = [];

        if (!$isSharedRaw) {
            $conditions = [
                ['type' => self::TYPE_POST, 'action' => ['publish']],
                // ['type' => self::TYPE_EVENT, 'action' => ['show']],
                ['type' => self::TYPE_PLACE, 'action' => ['review']],
                ['type' => 'hotel', 'action' => ['review']],
                ['type' => self::TYPE_DISCUSSION, 'action' => ['create']],
                // ['type' => self::TYPE_TRIP, 'action' => ['create', 'plan_updated']],
                ['type' => self::TYPE_TRIP, 'action' => ['plan_updated']],
                ['type' => self::TYPE_REPORT, 'action' => ['create', 'update']],
                // ['type' =>  self::TYPE_REPORT, 'action' => ['update']],
            ];

            foreach ($conditions as $condition) {
                foreach ($condition['action'] as $action) {
                    $condition['type'] = strtolower($condition['type']);
                    $data_exists_condition = '';
                    if (in_array($condition['type'], [self::TYPE_TRIP, self::TYPE_POST, self::TYPE_REPORT, self::TYPE_PLACE])) {
                        if ($condition['type'] == self::TYPE_TRIP) {
                            if (Auth::check()) {
                                $data_exists_condition = "AND EXISTS(SELECT * from trips WHERE id=variable and  deleted_at is null and EXISTS (SELECT * FROM `users` WHERE `trips`.`users_id` = `users`.`id` AND `users`.`deleted_at` IS NULL) AND EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `deleted_at` IS NULL AND `trips_places`.`deleted_at` IS NULL))";
                            } else { // only public trips for non-login user
                                $data_exists_condition = "AND EXISTS(SELECT * from trips WHERE privacy = " . TripsService::PRIVACY_PUBLIC . " and id=variable and  deleted_at is null and EXISTS (SELECT * FROM `users` WHERE `trips`.`users_id` = `users`.`id` AND `users`.`deleted_at` IS NULL) AND EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `deleted_at` IS NULL AND `trips_places`.`deleted_at` IS NULL))";
                            }
                        } else if ($condition['type'] == self::TYPE_POST) {
                            if (Auth::check()) {
                                $data_exists_condition = "AND EXISTS(SELECT * from posts WHERE id=variable and deleted_at is null)";
                            } else {  // only public posts for non-login user
                                $data_exists_condition = "AND EXISTS(SELECT * from posts WHERE permission = " .  Posts::POST_PERMISSION_PUBLIC . " and id=variable and deleted_at is null)";
                            }
                        } else if ($condition['type'] == self::TYPE_REPORT) {
                            $data_exists_condition = "AND EXISTS(SELECT * from reports WHERE id=variable and deleted_at is null AND flag=1 AND published_date is not null AND EXISTS(SELECT * FROM reports_infos WHERE reports_infos.reports_id=reports.id and var='cover'))";
                        } else if ($condition['type'] == self::TYPE_PLACE) {
                            $data_exists_condition = "AND EXISTS(SELECT * from reviews WHERE id=variable and deleted_at is null AND EXISTS(SELECT * FROM users WHERE users.id=by_users_id) AND EXISTS(SELECT * FROM places WHERE places.id=places_id))";
                        }
                    }
                    $condition_items[] = '(type = "' . $condition['type'] . '" and action = "' . $action . '" ' . $data_exists_condition . ')';
                }
            }

            if (count($condition_items) > 0) {
                $where_condition .= '(' . implode(' or ', $condition_items) . ')';
            }
        } else {
            $conditions = [
                ['type' => 'share', 'action' => [
                    // ShareService::TYPE_REVIEW,
                    ShareService::TYPE_EVENT,
                    ShareService::TYPE_DISCUSSION,
                    // ShareService::TYPE_POST,
                    ShareService::TYPE_REPORT,
                    // ShareService::TYPE_COUNTRY,
                    // ShareService::TYPE_CITY,
                    // ShareService::TYPE_PLACE,
                    ShareService::TYPE_TRIP_SHARE,
                    ShareService::TYPE_TRIP_PLACE_SHARE,
                    // ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE
                ]]
            ];

            foreach ($conditions as $condition) {
                foreach ($condition['action'] as $action) {
                    $data_exists_condition = '';
                    if (in_array($action, [ShareService::TYPE_TRIP_SHARE, ShareService::TYPE_TRIP_PLACE_SHARE, ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE])) {
                        if ($action == ShareService::TYPE_TRIP_SHARE) {
                            $data_exists_condition = "AND EXISTS(SELECT * from trips_shares WHERE id=variable)";
                        } else if ($action == ShareService::TYPE_TRIP_PLACE_SHARE) {
                            $data_exists_condition = "AND EXISTS(SELECT * from trip_places_shares WHERE id=variable)";
                        } else if ($action == ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE) {
                            $data_exists_condition = "AND EXISTS(SELECT * from trip_medias_shares WHERE id=variable)";
                        }
                    } else {
                        $data_exists_condition = "AND EXISTS(SELECT * from posts_shares WHERE id=variable)";
                    }
                    $condition_items[] = '(action = "' . $action . '" ' . $data_exists_condition . ')';
                }
            }

            if (count($condition_items) > 0) {
                $where_condition .= '(' . implode(' or ', $condition_items) . ')';
            }

            $where_condition = "( type='share' and (" . $where_condition . "))";
        }


        return $where_condition;
    }

    public function getNewsFeed($page, $isSuggestion)
    {
        $secondaryNewsfeedService = app(SecondaryNewsfeedService::class);

        $loadedFeedService = app(LoadedFeedService::class);
        // get old session value from DB & save in session
        session([
            self::SESSION_LOADED_KEYS[self::TYPE_POST] => sessionGet(self::SESSION_LOADED_KEYS[self::TYPE_POST], []),
            self::SESSION_LOADED_KEYS[self::TYPE_DISCUSSION] => sessionGet(self::SESSION_LOADED_KEYS[self::TYPE_DISCUSSION], []),
            self::SESSION_LOADED_KEYS[self::TYPE_TRIP] => sessionGet(self::SESSION_LOADED_KEYS[self::TYPE_TRIP], []),
            self::SESSION_LOADED_KEYS[self::TYPE_REPORT] => sessionGet(self::SESSION_LOADED_KEYS[self::TYPE_REPORT], []),
        ]);

        $_primary = true;
        $_secondary = true;
        $responsePost = [];
        $second_suggestion = [];

        $is_suggestion      = get_as_bool($isSuggestion);
        $me                 = Auth::user();

        $per_page           = 15;
        $data               = [];

        //language script
        // $user_lang  = UsersContentLanguages::where('users_id', $me->id)->orderBy('id', 'desc')->get();
        // $getLangids = [];
        // if ($user_lang) {
        //     foreach ($user_lang as $u_lang) {
        //         $getLangids[] = $u_lang->languages->id;
        //     }
        // }
        // session(['ratio_language' =>  $getLangids]); // only need for current request

        // below session used only for current api request
        // $scondary_post_collection = ['new_travellers', 'weather_for_trip', 'weather', 'following_locations', 'collective_following_location', 'video_you_might_like', 'recommended_places', 'people_you_might_know'];
        // shuffle($scondary_post_collection);
        // session()->forget(['secondary_post_style']);
        // $value1 = session(['secondary_post_style' =>  $scondary_post_collection]);


        // prepared where raw
        // get event
        if ($page == 1 && $_primary && !$is_suggestion) {
            $ip = getClientIP();
            $geoLoc = ip_details($ip);

            $getLocCity = Cities::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($geoLoc) {
                    $q->where('title', 'LIKE', "%" . $geoLoc['city'] . "%");
                })
                ->first();

            $_event = isset($getLocCity) ? $this->getEventForUser([$getLocCity->id]) : null;
            if (!$_event) {
                $followed_cities_ids = getUserFollwoingForEvents($me->id);
                $_event = $this->getEventForUser($followed_cities_ids);
            }
        }

        if ($_primary && !$is_suggestion) {
            $posts = $this->getPrimaryPosts($page, $per_page)->shuffle();

            // $tempPosts = [];
            // collect($posts)->each(function ($_item) use (&$tempPosts) {
            //     if (strtolower($_item->type) == "share") {
            //         $tempPosts[$_item->type . "|" . $_item->action] = $_item;
            //     } else {
            //         $tempPosts[$_item->type . "|" . $_item->variable] = $_item;
            //     }
            // });
            // $posts = collect($tempPosts)->values();
        } else {
            $posts = collect([]);
        }

        // radmon
        if (($page == 1 || ($page % 3 == 0 && rand(0, 1))) && $_primary && !$is_suggestion) {
            // my friend on going trip start | end
            $this->onGoingTrip($responsePost);
        }

        if ($_secondary) {
            if ($page == 1 && !$is_suggestion) {
                if (count($posts) == 0) {
                    // new user signup suggestion posts time
                    $final_array = $this->userPlacesContent();
                    if (!empty($final_array)) {
                        if (isset($post_to_print[self::TYPE_POST])) {
                            $post = $this->__preparedPost($post_to_print[self::TYPE_POST], true);
                            if ($post) {
                                $post->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'type'      => strtolower(self::TYPE_POST),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $post
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_TRIP])) {
                            $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP], null, true);
                            if ($trip) {
                                $trip->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'type'      => strtolower(self::TYPE_TRIP),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $trip
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_REPORT])) {
                            $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT], true);
                            if ($report) {
                                $report->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $report->id,
                                    'unique_link'   => url("reports/" . $report->id),
                                    'type'      => strtolower(self::TYPE_REPORT),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $report
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_DISCUSSION])) {
                            $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION], true);
                            if ($discussion) {
                                $discussion->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'type'      => strtolower(self::TYPE_DISCUSSION),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $discussion
                                ];
                            }
                        }
                    }

                    //trending post;
                    $post_to_print = $this->getTrendingPosts(true);
                    $shuffled_array = array();
                    $keys = array_keys($post_to_print);
                    shuffle($keys);
                    foreach ($keys as $key) {
                        $shuffled_array[$key] = $post_to_print[$key];
                    }
                    $post_to_print = $shuffled_array;
                    if (!empty($post_to_print)) {
                        if (isset($post_to_print[self::TYPE_POST]->id)) {
                            $post = $this->__preparedPost($post_to_print[self::TYPE_POST]->id);
                            if ($post) {
                                $post->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'unique_link' => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'type'      => strtolower(self::TYPE_POST),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $post
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_TRIP]->id)) {
                            $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP]->id);
                            if ($trip) {
                                $trip->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'type'      => strtolower(self::TYPE_TRIP),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $trip
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_REPORT]->id)) {
                            $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT]->id);
                            if ($report) {
                                $report->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $report->id,
                                    'unique_link'   => url("reports/" . $report->id),
                                    'type'      => strtolower(self::TYPE_REPORT),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $report
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_DISCUSSION]->id)) {
                            $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION]->id);
                            if ($discussion) {
                                $discussion->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'type'      => strtolower(self::TYPE_DISCUSSION),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $discussion
                                ];
                            }
                        }
                    }


                    //All time trending
                    $post_to_print = $this->getTrendingPosts(true, true);
                    $shuffled_array = array();
                    $keys = array_keys($post_to_print);
                    shuffle($keys);
                    foreach ($keys as $key) {
                        $shuffled_array[$key] = $post_to_print[$key];
                    }
                    $post_to_print = $shuffled_array;
                    if (!empty($post_to_print)) {
                        if (isset($post_to_print[self::TYPE_POST]->id)) {
                            $post = $this->__preparedPost($post_to_print[self::TYPE_POST]->id);
                            if ($post) {
                                $post->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'type'      => strtolower(self::TYPE_POST),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $post
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_TRIP]->id)) {
                            $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP]->id);
                            if ($trip) {
                                $trip->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'type'      => strtolower(self::TYPE_TRIP),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $trip
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_REPORT]->id)) {
                            $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT]->id);
                            if ($report) {
                                $report->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $report->id,
                                    'unique_link'   => url("reports/" . $report->id),
                                    'type'      => strtolower(self::TYPE_REPORT),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $report
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_DISCUSSION]->id)) {
                            $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION]->id);
                            if ($discussion) {
                                $discussion->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'type'      => strtolower(self::TYPE_DISCUSSION),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $discussion
                                ];
                            }
                        }
                    }
                } else {
                    // random 'city', 'place', 'country'
                    $rand_following_suggestion  = collect([self::TYPE_CITY, self::TYPE_PLACE, self::TYPE_COUNTRY])->random();
                    if ($rand_following_suggestion == self::TYPE_CITY) {
                        $post_to_print = $this->userCitiesContent();
                    } else if ($rand_following_suggestion == self::TYPE_COUNTRY) {
                        $post_to_print = $this->userCountriesContent();
                    } else {
                        $post_to_print = $this->userPlacesContent();
                    }
                    if (!empty($post_to_print)) {
                        if (isset($post_to_print[self::TYPE_POST])) {
                            $post = $this->__preparedPost($post_to_print[self::TYPE_POST], true);
                            if ($post) {
                                $post->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'type'      => strtolower(self::TYPE_POST),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $post
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_TRIP])) {
                            $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP], null, true);
                            if ($trip) {
                                $trip->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'type'      => strtolower(self::TYPE_TRIP),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $trip
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_REPORT])) {
                            $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT], true);
                            if ($report) {
                                $report->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $report->id,
                                    'unique_link'   => url("reports/" . $report->id),
                                    'type'      => strtolower(self::TYPE_REPORT),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $report
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_DISCUSSION])) {
                            $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION], true);
                            if ($discussion) {
                                $discussion->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'type'      => strtolower(self::TYPE_DISCUSSION),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $discussion
                                ];
                            }
                        }
                    }

                    //trending post;
                    $post_to_print = $this->getTrendingPosts();
                    if (!empty($post_to_print)) {
                        if (isset($post_to_print[self::TYPE_POST]->id)) {
                            $post = $this->__preparedPost($post_to_print[self::TYPE_POST]->id);
                            if ($post) {
                                $post->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'type'      => strtolower(self::TYPE_POST),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $post
                                ];
                            }
                        } else if (isset($post_to_print[self::TYPE_TRIP]->id)) {
                            $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP]->id);
                            if ($trip) {
                                $trip->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'type'      => strtolower(self::TYPE_TRIP),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $trip
                                ];
                            }
                        } else if (isset($post_to_print[self::TYPE_REPORT]->id)) {
                            $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT]->id);
                            if ($report) {
                                $report->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $report->id,
                                    'unique_link'   => url("reports/" . $report->id),
                                    'type'      => strtolower(self::TYPE_REPORT),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $report
                                ];
                            }
                        } else if (isset($post_to_print[self::TYPE_DISCUSSION]->id)) {
                            $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION]->id);
                            if ($discussion) {
                                $discussion->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'type'      => strtolower(self::TYPE_DISCUSSION),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $discussion
                                ];
                            }
                        }
                    }

                    //All time trending
                    $post_to_print = $this->getTrendingPosts(false, true);
                    if (!empty($post_to_print)) {
                        if (isset($post_to_print[self::TYPE_POST]->id)) {
                            $post = $this->__preparedPost($post_to_print[self::TYPE_POST]->id);
                            if ($post) {
                                $post->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'type'      => strtolower(self::TYPE_POST),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $post
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_TRIP]->id)) {
                            $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP]->id);
                            if ($trip) {
                                $trip->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'type'      => strtolower(self::TYPE_TRIP),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $trip
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_REPORT]->id)) {
                            $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT]->id);
                            if ($report) {
                                $report->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $report->id,
                                    'unique_link'   => url("reports/" . $report->id),
                                    'type'      => strtolower(self::TYPE_REPORT),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $report
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_DISCUSSION]->id)) {
                            $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION]->id);
                            if ($discussion) {
                                $discussion->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'type'      => strtolower(self::TYPE_DISCUSSION),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $discussion
                                ];
                            }
                        }
                    }
                }
            } else {
                if ($is_suggestion) {
                    // user place content
                    $post_to_print = $this->userPlacesContent();
                    if (!empty($post_to_print)) {
                        if (isset($post_to_print[self::TYPE_POST])) {
                            $post = $this->__preparedPost($post_to_print[self::TYPE_POST], true);
                            if ($post) {
                                $post->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'type'      => strtolower(self::TYPE_POST),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $post
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_TRIP])) {
                            $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP], null, true);
                            if ($trip) {
                                $trip->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'type'      => strtolower(self::TYPE_TRIP),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $trip
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_REPORT])) {
                            $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT], true);
                            if ($report) {
                                $report->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $report->id,
                                    'unique_link'   => url("reports/" . $report->id),
                                    'type'      => strtolower(self::TYPE_REPORT),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $report
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_DISCUSSION])) {
                            $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION], true);
                            if ($discussion) {
                                $discussion->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'type'      => strtolower(self::TYPE_DISCUSSION),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $discussion
                                ];
                            }
                        }
                    }

                    //trending post;
                    $post_to_print = $this->getTrendingPosts();
                    if (!empty($post_to_print)) {
                        if (isset($post_to_print[self::TYPE_POST]->id)) {
                            $post = $this->__preparedPost($post_to_print[self::TYPE_POST]->id);
                            if ($post) {
                                $post->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'type'      => strtolower(self::TYPE_POST),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $post
                                ];
                            }
                        } else if (isset($post_to_print[self::TYPE_TRIP]->id)) {
                            $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP]->id);
                            if ($trip) {
                                $trip->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'type'      => strtolower(self::TYPE_TRIP),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $trip
                                ];
                            }
                        } else if (isset($post_to_print[self::TYPE_REPORT]->id)) {
                            $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT]->id);
                            if ($report) {
                                $report->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $report->id,
                                    'unique_link'   => url("reports/" . $report->id),
                                    'type'      => strtolower(self::TYPE_REPORT),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $report
                                ];
                            }
                        } else if (isset($post_to_print[self::TYPE_DISCUSSION]->id)) {
                            $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION]->id);
                            if ($discussion) {
                                $discussion->is_suggested = false;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'type'      => strtolower(self::TYPE_DISCUSSION),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $discussion
                                ];
                            }
                        }
                    }


                    //All time trending
                    $post_to_print = $this->getTrendingPosts(false, true);
                    if (!empty($post_to_print)) {
                        if (isset($post_to_print[self::TYPE_POST]->id)) {
                            $post = $this->__preparedPost($post_to_print[self::TYPE_POST]->id);
                            if ($post) {
                                $post->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'type'      => strtolower(self::TYPE_POST),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $post
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_TRIP]->id)) {
                            $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP]->id);
                            if ($trip) {
                                $trip->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                                    'type'      => strtolower(self::TYPE_TRIP),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $trip
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_REPORT]->id)) {
                            $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT]->id);
                            if ($report) {
                                $report->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $report->id,
                                    'unique_link'   => url("reports/" . $report->id),
                                    'type'      => strtolower(self::TYPE_REPORT),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $report
                                ];
                            }
                        }
                        if (isset($post_to_print[self::TYPE_DISCUSSION]->id)) {
                            $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION]->id);
                            if ($discussion) {
                                $discussion->is_suggested = true;
                                $second_suggestion[] = [
                                    'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'type'      => strtolower(self::TYPE_DISCUSSION),
                                    'action'    => "trending_on_travooo",
                                    'data'      => $discussion
                                ];
                            }
                        }
                    }
                }

                if ($page % 2 == 0) {
                    $already_posted = $loadedFeedService->getOtherSecondaryLoadedNewsfeed();
                    $secondaryPost = self::SECONDARY_POST;
                    shuffle($secondaryPost);
                    $shuffle_result = array_diff($secondaryPost, $already_posted);
                    if ($shuffle_result) {
                        shuffle($shuffle_result);
                        $_sp = collect($shuffle_result)->random();
                        $loadedFeedService->create([
                            'type' => $_sp,
                            'action' => 'secondary',
                            'page' => $page,
                        ]);

                        switch ($_sp) {
                            case PrimaryPostService::SECONDARY_TOP_PLACES:
                                $getTopPlaces = $this->getTopPlaces();
                                if ($getTopPlaces && is_array($getTopPlaces) && count($getTopPlaces)) {
                                    $responsePost[]     =  ['page_id'   => null, 'unique_link' => null, 'action' => null, 'type' => PrimaryPostService::SECONDARY_TOP_PLACES, 'data' => $getTopPlaces];
                                }
                                break;
                            case PrimaryPostService::SECONDARY_NEW_FOLLOWER:
                                $secondaryNewsfeedService->newFollower($responsePost);
                                break;
                            case PrimaryPostService::SECONDARY_CHECKIN:
                                $secondaryNewsfeedService->weatherUpdateForCheckins($responsePost);
                                break;
                            case 'my_friend_today_trip':
                                $this->todayFriendFollowerTrip($responsePost);
                                break;
                            case PrimaryPostService::SECONDARY_VIDEO_YOU_MIGHT_LIKE:
                                $_result = $secondaryNewsfeedService->videoYouMightLike();
                                if ($_result != null) {
                                    $responsePost[] =  [
                                        'page_id'   => null,
                                        'unique_link' => null,
                                        'action' => null,
                                        'type' => PrimaryPostService::SECONDARY_VIDEO_YOU_MIGHT_LIKE,
                                        'data' => $_result
                                    ];
                                }
                                break;
                            case PrimaryPostService::SECONDARY_PLACES_YOU_MIGHT_LIKE:
                                $places = $secondaryNewsfeedService->placesYouMightLike();
                                if ($places != null) {
                                    $responsePost[] =  [
                                        'page_id'   => null,
                                        'unique_link' => null,
                                        'action' => null,
                                        'type' => PrimaryPostService::SECONDARY_PLACES_YOU_MIGHT_LIKE,
                                        'data' => $places
                                    ];
                                }
                                break;
                            case PrimaryPostService::SECONDARY_RECOMMENDED_PLACES:
                                $secondaryNewsfeedService->recommendedPlaces($responsePost);
                                break;
                            case PrimaryPostService::SECONDARY_COLLECTIVE_FOLLOWERS_PLACES:
                                $secondaryNewsfeedService->collectiveFollowersPlaces($responsePost);
                                break;
                            case PrimaryPostService::SECONDARY_DISCOVER_NEW_TRAVELLERS:
                                $secondaryNewsfeedService->discoverNewTravellers($responsePost);
                                break;
                            case PrimaryPostService::SECONDARY_TRENDING_DESTINATIONS:
                                $_result = $secondaryNewsfeedService->trendingDestinations();
                                if ($_result != null) {
                                    $responsePost[] =  [
                                        'page_id'   => null,
                                        'unique_link' => null,
                                        'action' => null,
                                        'type' => PrimaryPostService::SECONDARY_TRENDING_DESTINATIONS,
                                        'data' => $_result
                                    ];
                                }
                                break;
                            case PrimaryPostService::SECONDARY_NEW_PEOPLE:
                                $_users = $secondaryNewsfeedService->getNewPeople();
                                if ($_users != null) {
                                    $responsePost[] =  [
                                        'page_id'   => null,
                                        'unique_link' => null,
                                        'action' => null,
                                        'type' => PrimaryPostService::SECONDARY_NEW_PEOPLE,
                                        'data' => $_users
                                    ];
                                }
                                break;
                            case PrimaryPostService::SECONDARY_TRENDING_EVENTS:
                                if ($trendingEvents = $this->trendingEvents()) {
                                    $responsePost[] =  ['page_id'   => null, 'unique_link' => null, 'action' => null, 'type' => PrimaryPostService::SECONDARY_TRENDING_EVENTS, 'data' => $trendingEvents];
                                }
                                break;

                            case PrimaryPostService::SECONDARY_HOLIDAY:
                                $holiday = $this->getHoliday();
                                if (isset($holiday) && is_array($holiday)) {
                                    $responsePost[] = [
                                        'page_id'       => null,
                                        'unique_link'   => null,
                                        'type'          => PrimaryPostService::SECONDARY_HOLIDAY,
                                        'action'        => $holiday['type'],
                                        'data'          => $holiday['holiday']
                                    ];
                                }
                                break;
                        }
                    }
                }
            }
        }

        // return  $posts;
        if ($_primary) {
            // $posts = collect($posts)->shuffle();
            // Print primary post
            foreach ($posts as $postItem) {

                if (true) {
                    if (Auth::check() && isset($postItem->permission) and $postItem->permission == Posts::POST_PERMISSION_PRIVATE and $postItem->users_id != Auth::user()->id) {
                        continue;
                    } elseif (Auth::check() && isset($postItem->permission) and $postItem->permission == Posts::POST_PERMISSION_FRIENDS_ONLY and $postItem->users_id != Auth::user()->id and !in_array($postItem->users_id, getUserFriendsIds())) {
                        continue;
                    }
                }


                switch (strtolower($postItem->type)) {
                    case self::TYPE_POST:
                        if (in_array(strtolower($postItem->action), ['publish'])) {
                            $post = $this->__preparedPost($postItem->variable);
                            if ($post) {
                                $responsePost[] = [
                                    'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                    'type'      => strtolower(self::TYPE_POST),
                                    'action'    => strtolower($postItem->action),
                                    'data'      => $post
                                ];
                            }
                        }
                        break;

                        // case PrimaryPostService::TYPE_EVENT:
                        //     if (in_array(strtolower($postItem->action), ['show'])) {
                        //         $event = $this->__preparedEvent($postItem->variable);
                        //         if ($event) {
                        //             $responsePost[] = [
                        //                 'page_id'   => $this->link("newsfeed/event/event/" . $event->id),
                        //                 'unique_link'   => url("newsfeed/event/event/" . $event->id),
                        //                 'type'      => strtolower(PrimaryPostService::TYPE_EVENT),
                        //                 'action'    => strtolower($postItem->action),
                        //                 'data'      => $event
                        //             ];
                        //         }
                        //     }
                        //     break;


                    case "place":
                        if (in_array(strtolower($postItem->action), [PrimaryPostService::TYPE_REVIEW])) {
                            $review = $this->__preparedPlaceReview($postItem->variable);
                            if ($review) {
                                $responsePost[] = [
                                    'page_id'   => $this->link("newsfeed/" . $review->author->username . "/review/" . $review->id),
                                    'unique_link'   => url("newsfeed/" . $review->author->username . "/review/" . $review->id),
                                    'type'      => strtolower(PrimaryPostService::TYPE_REVIEW),
                                    'action'    => strtolower($postItem->type),
                                    'data'      => $review
                                ];
                            }
                        }
                        break;

                    case "hotel":
                        if (in_array(strtolower($postItem->action), [PrimaryPostService::TYPE_REVIEW])) {
                            $review = $this->__preparedHotelReview($postItem->variable, $postItem->users_id);
                            if ($review) {
                                $responsePost[] = [
                                    'page_id'   => $this->link("newsfeed/" . $review->author->username . "/review/" . $review->id),
                                    'unique_link'   => url("newsfeed/" . $review->author->username . "/review/" . $review->id),
                                    'type'      => strtolower(PrimaryPostService::TYPE_REVIEW),
                                    'action'    => strtolower($postItem->type),
                                    'data'      => $review
                                ];
                            }
                        }
                        break;

                    case self::TYPE_DISCUSSION:
                        if (in_array(strtolower($postItem->action), ['create'])) {
                            $discussion = $this->__preparedDiscussion($postItem->variable);
                            if ($discussion) {
                                $responsePost[] =  [
                                    'page_id'       => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                    'type'          => strtolower(self::TYPE_DISCUSSION),
                                    'action'        => strtolower($postItem->action),
                                    'data'          => $discussion
                                ];
                            }
                        }
                        break;

                    case self::TYPE_TRIP:
                        if (in_array(strtolower($postItem->action), ['create', 'plan_updated'])) {
                            $trip = $this->__preparedRegularTrip($postItem->variable, @$postItem->time);
                            if ($trip) {
                                $responsePost[] = [
                                    'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $postItem->id),
                                    'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $postItem->id),
                                    'type'      => strtolower(self::TYPE_TRIP),
                                    'action'    => strtolower($postItem->action),
                                    'data'      => $trip
                                ];
                            }
                        }
                        break;


                    case self::TYPE_REPORT:
                        if (in_array(strtolower($postItem->action), ['create', 'update'])) {
                            $report = $this->__preparedReport($postItem->variable);
                            if ($report) {
                                $responsePost[] = [
                                    'page_id'   => $report->id,
                                    'unique_link'   => url("reports/" . $report->id),
                                    'type'      => strtolower(self::TYPE_REPORT),
                                    'action'    => strtolower($postItem->action),
                                    'data'      => $report
                                ];
                            }
                        }
                        break;

                    case "share":
                        if (in_array(strtolower($postItem->action), [
                            ShareService::TYPE_REVIEW,
                            ShareService::TYPE_EVENT,
                            ShareService::TYPE_DISCUSSION,
                            ShareService::TYPE_POST,
                            ShareService::TYPE_REPORT,
                            ShareService::TYPE_COUNTRY,
                            ShareService::TYPE_CITY,
                            ShareService::TYPE_PLACE,
                        ])) {
                            $this->shareFeed($responsePost, $postItem);
                        } else if (in_array(strtolower($postItem->action), [
                            ShareService::TYPE_TRIP_SHARE,
                            ShareService::TYPE_TRIP_PLACE_SHARE,
                            ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE
                        ])) {
                            $this->shareTripAndPlaceAdnMediaFeed($responsePost, $postItem);
                        }
                        break;
                }
            }
        }

        // if ($anySecondaryPost && count($responsePost)) shuffle($responsePost);

        $me->profile_picture = check_profile_picture($me->profile_picture);
        $me->cover_photo = check_cover_photo($me->cover_photo);

        if (isset($_event) && $_event) {
            $event = $this->__preparedEvent($_event->id);
            if ($event) {
                $responsePost = array_merge([
                    [
                        'page_id'       => $this->link("newsfeed/event/event/" . $event->id),
                        'unique_link'   => url("newsfeed/event/event/" . $event->id),
                        'type'          => strtolower(PrimaryPostService::TYPE_EVENT),
                        'action'        => "show",
                        'data'          => $event
                    ]
                ], $responsePost);
            }
        }

        $final = array_merge($second_suggestion, $responsePost);

        // new user signup suggestion posts time
        if (count($final) == 0 && $page == 1) {
            $second_suggestion = [];

            $final_array = $this->userPlacesContent();
            if (!empty($final_array)) {
                if (isset($post_to_print[self::TYPE_POST])) {
                    $post = $this->__preparedPost($post_to_print[self::TYPE_POST]);
                    if ($post) {
                        $post->is_suggested = false;
                        $second_suggestion[] = [
                            'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                            'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                            'type'      => strtolower(self::TYPE_POST),
                            'action'    => "trending_on_travooo",
                            'data'      => $post
                        ];
                    }
                }
                if (isset($post_to_print[self::TYPE_TRIP])) {
                    $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP]);
                    if ($trip) {
                        $trip->is_suggested = false;
                        $second_suggestion[] = [
                            'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                            'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                            'type'      => strtolower(self::TYPE_TRIP),
                            'action'    => "trending_on_travooo",
                            'data'      => $trip
                        ];
                    }
                }
                if (isset($post_to_print[self::TYPE_REPORT])) {
                    $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT]);
                    if ($report) {
                        $report->is_suggested = false;
                        $second_suggestion[] = [
                            'page_id'   => $report->id,
                            'unique_link'   => url("reports/" . $report->id),
                            'type'      => strtolower(self::TYPE_REPORT),
                            'action'    => "trending_on_travooo",
                            'data'      => $report
                        ];
                    }
                }
                if (isset($post_to_print[self::TYPE_DISCUSSION])) {
                    $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION]);
                    if ($discussion) {
                        $discussion->is_suggested = false;
                        $second_suggestion[] = [
                            'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                            'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                            'type'      => strtolower(self::TYPE_DISCUSSION),
                            'action'    => "trending_on_travooo",
                            'data'      => $discussion
                        ];
                    }
                }
            }

            //All time trending
            $post_to_print = $this->getTrendingPosts(true, true);
            $shuffled_array = array();
            $keys = array_keys($post_to_print);
            shuffle($keys);
            foreach ($keys as $key) {
                $shuffled_array[$key] = $post_to_print[$key];
            }
            $post_to_print = $shuffled_array;
            if (!empty($post_to_print)) {
                if (isset($post_to_print[self::TYPE_POST]->id)) {
                    $post = $this->__preparedPost($post_to_print[self::TYPE_POST]->id);
                    if ($post) {
                        $post->is_suggested = true;
                        $second_suggestion[] = [
                            'page_id'   => $this->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                            'unique_link'   => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                            'type'      => strtolower(self::TYPE_POST),
                            'action'    => "trending_on_travooo",
                            'data'      => $post
                        ];
                    }
                }
                if (isset($post_to_print[self::TYPE_TRIP]->id)) {
                    $trip = $this->__preparedRegularTrip($post_to_print[self::TYPE_TRIP]->id);
                    if ($trip) {
                        $trip->is_suggested = true;
                        $second_suggestion[] = [
                            'page_id'   => $this->link("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                            'unique_link'   => url("newsfeed/" . $trip->author->username . "/trip/" . $trip->id . "/0"),
                            'type'      => strtolower(self::TYPE_TRIP),
                            'action'    => "trending_on_travooo",
                            'data'      => $trip
                        ];
                    }
                }
                if (isset($post_to_print[self::TYPE_REPORT]->id)) {
                    $report = $this->__preparedReport($post_to_print[self::TYPE_REPORT]->id);
                    if ($report) {
                        $report->is_suggested = true;
                        $second_suggestion[] = [
                            'page_id'   => $report->id,
                            'unique_link'   => url("reports/" . $report->id),
                            'type'      => strtolower(self::TYPE_REPORT),
                            'action'    => "trending_on_travooo",
                            'data'      => $report
                        ];
                    }
                }
                if (isset($post_to_print[self::TYPE_DISCUSSION]->id)) {
                    $discussion = $this->__preparedDiscussion($post_to_print[self::TYPE_DISCUSSION]->id);
                    if ($discussion) {
                        $discussion->is_suggested = true;
                        $second_suggestion[] = [
                            'page_id'   => $this->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                            'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                            'type'      => strtolower(self::TYPE_DISCUSSION),
                            'action'    => "trending_on_travooo",
                            'data'      => $discussion
                        ];
                    }
                }
            }
            $final = array_merge($second_suggestion, $final);
        }

        if ($page == 1 && $_primary && !$is_suggestion) {
            shuffle($final);
        }

        $data['current_page'] = $page;
        $data['newsfeeds']    = $final;


        unset($me->friends, $me->followings, $me->followed_places, $me->get_followers, $me->following_user, $me->confirmation_code, $me->password_reset_token, $me->sms, $me->messages, $me->notifications, $me->travelstyles);

        // get updated value from session & save into DB
        sessionPut(self::SESSION_LOADED_KEYS[self::TYPE_POST], sessionNullSafety(self::SESSION_LOADED_KEYS[self::TYPE_POST], []));
        sessionPut(self::SESSION_LOADED_KEYS[self::TYPE_DISCUSSION], sessionNullSafety(self::SESSION_LOADED_KEYS[self::TYPE_DISCUSSION], []));
        sessionPut(self::SESSION_LOADED_KEYS[self::TYPE_TRIP], sessionNullSafety(self::SESSION_LOADED_KEYS[self::TYPE_TRIP], []));
        sessionPut(self::SESSION_LOADED_KEYS[self::TYPE_REPORT], sessionNullSafety(self::SESSION_LOADED_KEYS[self::TYPE_REPORT], []));

        return $data;
    }

    private function getEventForUser($cities_array = [])
    {
        $event_activity = sessionGet('event_activity', []);
        if (isset($cities_array) && !empty($cities_array)) {
            $event = Events::query()
                ->whereIn('cities_id', $cities_array)
                ->when(count($event_activity), function ($q) use ($event_activity) {
                    return $q->whereNotIn('id', $event_activity);
                })
                ->whereDate('created_at', '>', Carbon::now()->subDays(7))
                ->orderBy(DB::raw('ratings,reviews,variable'), 'DESC')->first();

            if ($event) {
                $event_activity = collect($event_activity)->merge([$event->id])->toArray();
                $event_activity = sessionPut('event_activity', $event_activity);
            }
            return $event;
        }
        return null;
    }

    private function getHoliday()
    {
        $authUser = auth()->user();
        $types = ['city', 'country'];
        foreach (range(1, rand(25, 50)) as $random) {
            shuffle($types);
        }
        $selected_type = collect($types)->first();
        $key = 'holiday_' . $selected_type;
        $check_loaded = sessionGet($key, []);

        if ($selected_type == "city") {
            $cities_followers = CitiesFollowers::where('users_id', $authUser->id)->pluck('cities_id')->toArray();
            if (DUMMY_DATA_MODE) {
                $citiesHolidays = CitiesHolidays::query()
                    ->whereHas('city')
                    ->when(count($check_loaded), function ($q) use ($check_loaded) {
                        return $q->whereNotIn('holidays_id', $check_loaded);
                    })->inRandomOrder()->first();
            } else {
                $citiesHolidays = CitiesHolidays::query()
                    ->whereHas('city')
                    ->whereIn('cities_id', $cities_followers)
                    ->whereNotNull('date')
                    ->where([
                        [DB::raw('date(date)'), '>=', Carbon::now()->format('Y-m-d')],
                        [DB::raw('date(date)'), '<=', Carbon::now()->addDays(7)->format('Y-m-d')]
                    ])
                    ->when(count($check_loaded), function ($q) use ($check_loaded) {
                        return $q->whereNotIn('holidays_id', $check_loaded);
                    })->inRandomOrder()->first();
            }


            if ($citiesHolidays) {
                $cityInCountry_id = Cities::where('id', $citiesHolidays->cities_id)->first()->countries_id;
                $city_trans = CitiesTranslations::select('cities_id', 'title AS city_title')->where('cities_id', $citiesHolidays->cities_id)->first()->toArray();
                $country_trans = CountriesTranslations::select('countries_id', 'title AS country_title')->where('countries_id', $cityInCountry_id)->first()->toArray();
                $citiesHolidays['count_city_folower'] = CitiesFollowers::where('cities_id', $citiesHolidays->cities_id)->count();
                $citiesHolidays['iso_code'] = Countries::where('id', $cityInCountry_id)->first()->iso_code;
                $citiesHolidays['city_trans'] = array_merge($city_trans, $country_trans);
                $citiesHolidays['trans'] = HolidaysTranslations::where('holidays_id', $citiesHolidays->holidays_id)->first()->toArray();
                $citiesHolidays['type'] = $selected_type;

                $medias_id = HolidaysMedias::where('holidays_id', $citiesHolidays->holidays_id)->first();
                if ($medias_id) {
                    $media = Media::where('id', $medias_id->medias_id)->first();
                    if ($media) $citiesHolidays['media'] = $media->url;
                }
                if (!isset($citiesHolidays['media'])) $citiesHolidays['media'] = url(PATTERN_HOLIDAY);

                $holiday = $citiesHolidays;
            }
        } elseif ($selected_type == "country") {
            $countries_followers = CountriesFollowers::where('users_id', $authUser->id)->pluck('countries_id')->toArray();
            if (DUMMY_DATA_MODE) {
                $countriesHolidays = CountriesHolidays::query()
                    ->whereHas('country')
                    ->when(count($check_loaded), function ($q) use ($check_loaded) {
                        return $q->whereNotIn('holidays_id', $check_loaded);
                    })->inRandomOrder()->first();
            } else {
                $countriesHolidays = CountriesHolidays::query()
                    ->whereHas('country')
                    ->whereIn('countries_id', $countries_followers)
                    ->whereNotNull('date')
                    ->where([
                        [DB::raw('date(date)'), '>=', Carbon::now()->format('Y-m-d')],
                        [DB::raw('date(date)'), '<=', Carbon::now()->addDays(7)->format('Y-m-d')]
                    ])
                    ->when(count($check_loaded), function ($q) use ($check_loaded) {
                        return $q->whereNotIn('holidays_id', $check_loaded);
                    })->inRandomOrder()->first();
            }

            if ($countriesHolidays) {
                $countriesHolidays['count_country_folower'] = CountriesFollowers::where('countries_id', $countriesHolidays->countries_id)->count();
                $countriesHolidays['iso_code'] = Countries::where('id', $countriesHolidays->countries_id)->first()->iso_code;
                $countriesHolidays['country_trans'] = CountriesTranslations::select('countries_id', 'title AS country_title')->where('countries_id', $countriesHolidays->countries_id)->first()->toArray();
                $countriesHolidays['trans'] = HolidaysTranslations::where('holidays_id', $countriesHolidays->holidays_id)->first()->toArray();
                $countriesHolidays['type'] = $selected_type;

                $medias_id = HolidaysMedias::where('holidays_id', $countriesHolidays->holidays_id)->first();
                if ($medias_id) {
                    $media = Media::where('id', $medias_id->medias_id)->first();
                    if ($media) $countriesHolidays['media'] = $media->url;
                }
                if (!isset($countriesHolidays['media'])) $countriesHolidays['media'] = url(PATTERN_HOLIDAY);

                $holiday = $countriesHolidays;
            }
        }
        if (isset($holiday)) {
            $check_loaded = collect($check_loaded)->merge([$holiday->holidays_id])->toArray();
            $check_loaded = sessionPut($key, $check_loaded);
            return [
                'type' => $selected_type,
                'holiday' => $holiday
            ];
        }
        return null;
    }

    function shareFeed(&$responsePost, $postItem)
    {
        $feed = $this->__preparedShareFeed($postItem);
        if ($feed && is_array($feed)) {
            $responsePost[] = $feed;
        }
    }

    // helpers
    function __preparedShareFeed($postItem)
    {
        $authUser = auth()->user();
        $postshare = PostsShares::whereHas('post')->with('author', 'post', 'post.author', 'reference')->where('id', $postItem->variable)->first();
        if ($postshare) {
            $is_multiple_share_available = false;
            $check_availeble_shares = PostsShares::whereNull('text')->where(['type' => $postshare->type, 'posts_type_id' => $postshare->posts_type_id])->where('id', '<>', $postshare->id)->pluck('users_id')->toArray();
            if (count($check_availeble_shares)) {
                $is_multiple_share_available = true;
            }

            if ($is_multiple_share_available == false) {
                if ($postshare->post && $postshare->reference) {
                    if ($postshare->post->author) {
                        $postshare->post->author->profile_picture = check_profile_picture($postshare->post->author->profile_picture);
                    }
                    $referenceId = $postshare->reference->id;
                    $postshare->post->reference = null;
                    switch (strtolower($postItem->action)) {
                        case ShareService::TYPE_POST:
                            $postshare->post->reference = $this->__preparedPost($referenceId);
                            break;

                        case ShareService::TYPE_DISCUSSION:
                            $postshare->post->reference = $this->__preparedDiscussion($referenceId);
                            break;

                        case ShareService::TYPE_REPORT:
                            $postshare->post->reference = $this->__preparedReport($referenceId);
                            break;

                        case ShareService::TYPE_REVIEW:
                            $postshare->post->reference = $this->__preparedPlaceReview($referenceId);
                            break;

                        case ShareService::TYPE_EVENT:
                            $postshare->post->reference = $this->__preparedEvent($referenceId);
                            break;

                        case ShareService::TYPE_COUNTRY:
                            $country = Countries::find($referenceId);
                            if ($country) {
                                $country->title = @$country->trans[0]->title ?? null;
                                $country->population = @$country->trans[0]->population ?? null;
                                $country->nationality = @$country->trans[0]->nationality ?? null;
                                $country->working_days = @$country->trans[0]->working_days ?? null;
                                $country->image = check_country_photo(@$country->getMedias[0]->url, 180);
                                $country->total_shares = PostsShares::where('type', ShareService::TYPE_COUNTRY)->where('posts_type_id', $country->id)->count();
                                $country->total_followers = $country->followers()->whereHas('user')->count();
                                $country->follow_flag = $authUser ? ($country->followers()->where('users_id', $authUser->id)->exists()) : false;
                                $country->latest_followers = $country->followers()->whereHas('user')->take(5)->get()->map(function ($follower) {
                                    return [
                                        'id'                => $follower->user->id,
                                        'name'              => $follower->user->name,
                                        'profile_picture'   => check_profile_picture($follower->user->profile_picture),
                                    ];
                                })->toArray();
                                unset($country->followers, $country->trans, $country->getMedias);
                                $postshare->post->reference = $country;
                            }
                            break;

                        case ShareService::TYPE_CITY:
                            $city = Cities::find($referenceId);
                            if ($city) {
                                $city->title = @$city->trans[0]->title ?? null;
                                $city->population = @$city->trans[0]->population ?? null;
                                $city->nationality = @$city->trans[0]->nationality ?? null;
                                $city->working_days = @$city->trans[0]->working_days ?? null;
                                $city->image = @check_city_photo(@$city->getMedias[0]->url);
                                $city->total_shares = PostsShares::where('type', ShareService::TYPE_CITY)->where('posts_type_id', $city->id)->count();
                                $city->total_followers = $city->followers()->whereHas('user')->count();
                                $city->follow_flag = $authUser ? ($city->followers()->where('users_id', $authUser->id)->exists()) : false;
                                $city->latest_followers = $city->followers()->whereHas('user')->take(5)->get()->map(function ($follower) {
                                    return [
                                        'id'                => $follower->user->id,
                                        'name'              => $follower->user->name,
                                        'profile_picture'   => check_profile_picture($follower->user->profile_picture),
                                    ];
                                })->toArray();
                                unset($city->followers, $city->trans, $city->getMedias);
                                $postshare->post->reference = $city;
                            }
                            break;

                        case ShareService::TYPE_PLACE:
                            $place = Place::find($referenceId);
                            if ($place) {
                                $place->place_type = do_placetype($place->place_type);
                                $place->title = @$place->transsingle->title ?? null;
                                $place->population = @$place->transsingle->population ?? null;
                                $place->working_days = @$place->transsingle->working_days ?? null;
                                $place->image = @check_place_photo(@$place);
                                $place->total_shares = PostsShares::where('type', ShareService::TYPE_PLACE)->where('posts_type_id', $place->id)->count();
                                $place->total_followers = $place->followers()->whereHas('user')->count();
                                $place->follow_flag = $authUser ? ($place->followers()->where('users_id', $authUser->id)->exists()) : false;
                                $place->latest_followers = $place->followers()->whereHas('user')->take(5)->get()->map(function ($follower) {
                                    return [
                                        'id'                => $follower->user->id,
                                        'name'              => $follower->user->name,
                                        'profile_picture'   => check_profile_picture($follower->user->profile_picture),
                                    ];
                                })->toArray();
                                unset($place->followers, $place->transsingle, $place->medias);
                                $postshare->post->reference = $place;
                            }
                            break;
                    }
                    if ($postshare->post->reference) {
                        $postshare->post->like_flag   = ($authUser) ? ($postshare->post->likes()->where('users_id', $authUser->id)->exists()) : false;
                        $postshare->post->total_likes =  $postshare->post->likes()->count();
                        $postshare->post->total_shares =  PostsShares::where('type', $postItem->action)->where('posts_type_id', $referenceId)->count();

                        // Comment Section
                        $postshare->post = $this->__addCommentSection($postshare->post);

                        $postshare->post->text = isset($postshare->post->tags) ? convert_post_text_to_tags(strip_tags($postshare->post->text), $postshare->post->tags, false) : strip_tags($postshare->post->text);
                        unset($postshare->post->tags, $postshare->post->likes);

                        // $page_id = null;
                        // if (in_array($postItem->action, ["review", "discussion", "trip", "report", "post"])) {
                        // }

                        return [
                            'page_id'   => $this->link("newsfeed/" . $postshare->post->author->username . "/share/" . $postItem->id),
                            'unique_link'   => url("newsfeed/" . $postshare->post->author->username . "/share/" . $postItem->id),
                            'type'      => trim(strtolower($postItem->action)),
                            'action'    => 'share',
                            'data'      => $postshare->post
                        ];
                    }
                }
            } else {
                if ($postshare->post && $postshare->reference) {
                    $referenceId = $postshare->reference->id;
                    $postshare->post->reference = null;
                    switch (strtolower($postItem->action)) {
                        case ShareService::TYPE_POST:
                            $postshare->post->reference = $this->__preparedPost($referenceId);
                            break;

                        case ShareService::TYPE_DISCUSSION:
                            $postshare->post->reference = $this->__preparedDiscussion($referenceId);
                            break;

                        case ShareService::TYPE_REPORT:
                            $postshare->post->reference = $this->__preparedReport($referenceId);
                            break;

                        case ShareService::TYPE_REVIEW:
                            $postshare->post->reference = $this->__preparedPlaceReview($referenceId);
                            break;

                        case ShareService::TYPE_EVENT:
                            $postshare->post->reference = $this->__preparedEvent($referenceId);
                            break;
                    }

                    if ($postshare->post->reference) {
                        $postshare->post->reference->header = User::whereIn('id', $check_availeble_shares)->take(5)->select(['username', 'profile_picture'])->get()->map(function ($user) {
                            $user->profile_picture = check_profile_picture($user->profile_picture);
                            return $user;
                        })->toArray();

                        return [
                            'page_id'   => null,
                            'unique_link'   => null,
                            'type'      => trim(strtolower($postItem->action)),
                            'action'    => 'multiple_share',
                            'data'      => $postshare->post->reference
                        ];
                    }
                }
            }
        }
        return null;
    }

    function __preparedPost($post_id, $isFollowContent = false)
    {
        $authUser = auth()->user();
        $post = Posts::whereHas('author')->with('author', 'checkin')->where('id', $post_id)->first();
        if ($post) {
            // if (!$post->author) {
            //     return null;
            // }

            $post->author->profile_picture = check_profile_picture($post->author->profile_picture);
            $post->like_flag   = ($authUser) ? (($post->likes()->where('users_id', $authUser->id)->first()) ? true : false) : false;
            $post->total_likes = $post->likes()->count();
            $post->total_shares = PostsShares::where('type', ShareService::TYPE_POST)->where('posts_type_id', $post->id)->count();

            // Comment Section
            $post = $this->__addCommentSection($post);

            $medias = $post->medias()->whereHas('media')->with('media')->get();
            foreach ($medias as $postMedia) {
                $file_url = $postMedia->media->url;
                $file_url_array = explode(".", $file_url);
                $ext = end($file_url_array);
                $allowed_video = ["mp4", "mov", "ogg", "wmv"];
                if (!in_array(strtolower($ext), $allowed_video)) {
                    $postMedia->media->url = replace_s3_path_with_cloudfront($postMedia->media->url, '800x0');
                }
            }
            $post->medias = $medias;

            if ($isFollowContent) {
                $post->follow_header = getPostDestinationDetails($post);
            } else {
                $post->follow_header = null;
            }


            // Convert Text With Deeplink and remove text
            // $post->text = convert_post_text($post, false);
            $post->text = convert_post_text_to_tags(strip_tags($post->text), $post->tags, false);
            unset($post->tags, $post->likes);

            $post->check_in = null;
            if (isset($post->checkin[0])) {
                $postCheckin = $post->checkin[0];
                $postCheckin->type = null;
                if ($postCheckin->place_id) {
                    $postCheckin->type = CommonConst::POST_CHECKIN_PLACE;
                    if (!$postCheckin->place) {
                        return null;
                    }
                    $postCheckin->place->title = _fieldValueFromTrans($postCheckin->place, 'title');
                    $postCheckin->place->image = check_place_photo($postCheckin->place);
                    unset($postCheckin->place->trans, $postCheckin->place->getMedias);
                } else if ($postCheckin->country_id) {
                    $postCheckin->type = CommonConst::POST_CHECKIN_COUNTRY;
                    if (!$postCheckin->country) {
                        return null;
                    }
                    $postCheckin->country->title = _fieldValueFromTrans($postCheckin->country, 'title');
                    $postCheckin->country->image = (isset($postCheckin->country->medias[0]->media->path)) ? check_country_photo($postCheckin->country->medias[0]->media->path, 180) : url(PLACE_PLACEHOLDERS);
                    unset($postCheckin->country->transsingle, $postCheckin->country->trans, $postCheckin->country->medias);
                } else if ($postCheckin->city_id) {
                    $postCheckin->type = CommonConst::POST_CHECKIN_CITY;
                    if (!$postCheckin->city) {
                        return null;
                    }

                    $postCheckin->city->title = _fieldValueFromTrans($postCheckin->city, 'title');
                    $postCheckin->city->image = (isset($postCheckin->city->medias[0]->media->path)) ? check_city_photo($postCheckin->city->medias[0]->media->path) : url(PLACE_PLACEHOLDERS);
                    unset($postCheckin->city->trans, $postCheckin->city->medias);
                }
                if ($postCheckin->type)
                    $post->check_in = $postCheckin;
            }
            unset($post->checkin);
            if ($authUser) {
                $pv = new \App\Models\Posts\PostsViews;
                $pv->posts_id = $post->id;
                $pv->users_id = $authUser->id;
                $pv->gender = $authUser->gender;
                $pv->nationality = $authUser->nationality;
                $pv->save();
            }

            if ($post->follow_header) {
                unset($post->posts_place, $post->posts_city, $post->posts_country);
            }
            return $post;
        }
        return null;
    }

    function __preparedReport($report_id, $isFollowContent = false)
    {
        $authUser = auth()->user();
        $report = Reports::with('author', 'story', 'cover')->where('id', $report_id)->first();
        if ($report && isset($report->cover[0])) {
            $report->cover_url =  replace_s3_path_with_cloudfront($report->cover[0]->val, '800x0');
            $story = $report->story;
            $report->author->profile_picture = check_profile_picture($report->author->profile_picture);

            if ($isFollowContent) {
                $report->follow_header = getReportsDestinationDetails($report);
            } else {
                $report->follow_header = null;
            }


            $report->like_flag   = $authUser ? ($report->likes()->where('users_id', $authUser->id)->exists()) : false;
            $report->total_likes = $report->likes()->count();
            $report->total_shares =  PostsShares::where('type', ShareService::TYPE_REPORT)->where('posts_type_id', $report->id)->count();

            // For Comment
            $report = $this->__addPostCommentSection($report);
            unset($report->cover, $report->likes, $report->story);
            $report->story = (isset($story->val)) ? $story->val : '';
            if ($report->follow_header) {
                unset($report->location);
            }
            return $report;
        }
        return null;
    }

    function __preparedDiscussion($discussion_id, $isFollowContent = false)
    {
        $authUser = auth()->user();
        $discussion = Discussion::with('author', 'medias')->where('id', $discussion_id)->first();
        if ($discussion) {
            $discussion->author = User::select('id', 'profile_picture', 'email', 'name', 'username', 'type', 'expert', 'cover_photo')->where('id', $discussion->users_id)->first();
            if ($discussion->author) {
                $discussion->author->profile_picture = check_profile_picture($discussion->author->profile_picture);
            }

            if ($isFollowContent) {
                $discussion->follow_header = getDiscussionDestinationDetails($discussion);
            } else {
                $discussion->follow_header = null;
            }

            // up/down vote + share + media + views
            $discussion->upvote_flag = $authUser ? ($discussion->discussion_upvotes()->where('users_id', $authUser->id)->exists()) : false;
            $discussion->downvotes_flag = $authUser ? ($discussion->discussion_downvotes()->where('users_id', $authUser->id)->exists()) : false;
            $discussion->total_upvotes = $discussion->discussion_upvotes()->count();
            $discussion->total_downvotes = $discussion->discussion_downvotes()->count();
            $discussion->total_share = PostsShares::where('type', ShareService::TYPE_DISCUSSION)->where('posts_type_id', $discussion->id)->count();
            $discussion->total_views = $discussion->views()->count();
            $_medias = [];
            $discussion->medias->map(function ($media) use (&$_medias) {
                $media->media_url = check_media_url($media->media_url);
                if (url(PATTERN_PLACEHOLDERS) != $media->media_url) {
                    $media->media_url = replace_s3_path_with_cloudfront($media->media_url, '200x0');
                }
                $media->url = $media->media_url;
                if (url(PATTERN_PLACEHOLDERS) != $media->media_url || url(PATTERN_PLACEHOLDERS) != $media->url) {
                    $_medias[] = $media;
                }
                return $media;
            });
            unset($discussion->medias);
            $discussion->medias = $_medias;

            //experts
            $discussion->total_experts = $discussion->experts()->count();
            $latest_experts = [];
            foreach ($discussion->experts()->take(5)->get() as $expert) {
                $expert->user = User::select('id', 'profile_picture', 'email', 'name', 'username', 'type', 'expert', 'cover_photo')->where('id', $expert->expert_id)->first();
                if (!$expert->user) continue;
                $expert->user->profile_picture = check_profile_picture($expert->user->profile_picture);
                $latest_experts[] = $expert;
            }
            $discussion->latest_experts = $latest_experts;
            $discussion->is_tagged = $authUser ? $discussion->experts()->where('expert_id', $authUser->id)->exists() : false;

            //replies
            $discussion->total_replies = $discussion->replies()->where('parents_id', 0)->count();
            $discussion->comment_flag = $authUser ? ($discussion->replies()->where('users_id', $authUser->id)->count() ? true : false) : false;
            $latest_replies =  $discussion->replies()->take(3)->get();
            foreach ($latest_replies as $parent_comment) {
                // For Parent Comment
                $parent_comment->text = isset($parent_comment->tags) ? convert_post_text_to_tags(strip_tags($parent_comment->reply), $parent_comment->tags, false) : strip_tags($parent_comment->text);

                $parent_comment->upvote_flag = $authUser ? ($parent_comment->upvotes()->where('users_id', $authUser->id)->exists()) : false;
                $parent_comment->downvotes_flag = $authUser ? ($parent_comment->downvotes()->where('users_id', $authUser->id)->exists()) : false;
                $parent_comment->total_upvotes = $parent_comment->upvotes()->count();
                $parent_comment->total_downvotes = $parent_comment->downvotes()->count();

                $parent_comment->comment_flag = $authUser ? ($parent_comment->sub()->where('users_id', $authUser->id)->count() ? true : false) : false;

                if ($parent_comment->author) {
                    $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                }
                unset($parent_comment->tags, $parent_comment->reply, $parent_comment->upvotes, $parent_comment->downvotes);

                // For Sub Comment
                if ($parent_comment->sub) {
                    foreach ($parent_comment->sub as $sub_comment) {
                        $sub_comment->text = isset($sub_comment->tags) ? convert_post_text_to_tags(strip_tags($sub_comment->reply), $sub_comment->tags, false) : strip_tags($sub_comment->text);

                        $sub_comment->upvote_flag = $authUser ? ($sub_comment->upvotes()->where('users_id', $authUser->id)->exists()) : false;
                        $sub_comment->downvotes_flag = $authUser ? ($sub_comment->downvotes()->where('users_id', $authUser->id)->exists()) : false;
                        $sub_comment->total_upvotes = $sub_comment->upvotes()->count();
                        $sub_comment->total_downvotes = $sub_comment->downvotes()->count();

                        if ($sub_comment->author) {
                            $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                        }
                        unset($sub_comment->tags, $sub_comment->reply, $sub_comment->upvotes, $sub_comment->downvotes);
                    }
                }
            }
            unset($discussion->replies);
            $discussion->replies = $latest_replies;


            //destination
            $discussion->destination_type = strtolower($discussion->destination_type);
            if ($discussion->destination_type == "place") {
                $place = Place::find($discussion->destination_id);
                if ($place) {
                    $place->title = isset($place->trans[0]->title) ? $place->trans[0]->title : null;
                    $place->image = check_place_photo($place);
                    unset($place->trans, $place->getMedias);
                }
                $discussion->place = $place;
            } else if ($discussion->destination_type == "country") {
                $country = Countries::find($discussion->destination_id);
                if ($country) {
                    $country->title = isset($country->transsingle->title) ? $country->transsingle->title : null;
                    $country->image = (isset($country->medias[0]->media->path)) ? check_country_photo($country->medias[0]->media->path, 180) : url(PLACE_PLACEHOLDERS);
                    unset($country->transsingle, $country->medias);
                }
                $discussion->country = $country;
            } else if ($discussion->destination_type == "city") {
                $city = Cities::find($discussion->destination_id);
                if ($city) {
                    $city->title = isset($city->trans[0]->title) ? $city->trans[0]->title : null;
                    $city->image = (isset($city->medias[0]->media->path)) ? check_city_photo($city->medias[0]->media->path) : url(PLACE_PLACEHOLDERS);
                    unset($city->trans, $city->medias);
                }
                $discussion->city = $city;
            }

            unset(
                $discussion->discussion_downvotes,
                $discussion->discussion_upvotes,
                $discussion->experts,
                $discussion->views
            );

            return $discussion;
        }
        return null;
    }

    function  __preparedRegularTrip($trip_id, $updated_at = null, $isFollowContent = false)
    {
        $cacheData = [];
        $cacheKey = "NEWSFEED_TRIP_" . $trip_id;
        if (Cache::has($cacheKey)) {
            $cacheData = Cache::get($cacheKey); // GET trip & trip author & trip published_places & trip thumb only (no need comment & like)
        }

        $authUser = auth()->user();
        if (!empty($cacheData) && isset($cacheData['trip_plan'])) {
            $trip_plan = $cacheData['trip_plan'];
        } else {
            $trip_plan = TripPlans::with('author')
                ->whereRaw("EXISTS (SELECT * FROM `users` WHERE `trips`.`users_id` = `users`.`id` AND `users`.`deleted_at` IS NULL) AND EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `trips_places`.`deleted_at` IS NULL)")
                ->where('id', $trip_id)->first();
        }

        if ($trip_plan) {
            // $contributer = $trip_plan->contribution_requests->where('status', 1)->pluck('users_id')->toArray();
            //(in_array($authUser->id, $contributer) && in_array($trip_plan->id, get_triplist4me())) || $authUser->id == $trip_plan->users_id
            if (true) {
                if ($updated_at) {
                    $trip_plan->updated_at = $updated_at;
                } else {
                    $lastUpdatedLog = ActivityLog::query()->where([
                        'type' => 'Trip',
                        'variable' => $trip_plan->id
                    ])->whereIn('action', ['create', 'update'])->orderBy('time', 'DESC')->first();
                    $trip_plan->updated_at = ($lastUpdatedLog) ? $lastUpdatedLog->time : null;
                }

                if (!$trip_plan->author) {
                    return null;
                }

                // $author = $authUser;
                // if ($author->id != $trip_plan->author->id) {
                //     if (in_array($author->id, $contributer)) {
                //         $author = $trip_plan->author;
                //     } else {
                //         $following = \App\Models\UsersFollowers\UsersFollowers::where('followers_id', $author->id)->pluck('users_id')->toArray();
                //         $ans =  array_values(array_intersect($following, $contributer));
                //         if (count($ans) > 0) {
                //             $user_find = \App\Models\User\User::find($ans[0]);
                //             if (isset($user_find)) {
                //                 $author = $user_find;
                //             }
                //         } else {
                //             $author = $trip_plan->author;
                //         }
                //     }
                // }
                // unset($trip_plan->author);
                // $trip_plan->author = $author;

                if ($trip_plan->author) {
                    $trip_plan->author->profile_picture = check_profile_picture($trip_plan->author->profile_picture);
                }

                $published_places = NULL;
                if (!empty($cacheData) && isset($cacheData['published_places'])) {
                    $published_places = $cacheData['published_places'];
                } else {
                    $published_places = $trip_plan->published_places;
                }

                if ($published_places) {
                    $places = [];
                    $countries = [];
                    $cities = [];
                    $total_destination = 0;
                    $_totalDestination = ['city_wise' => [], 'country_wise' => []];

                    foreach ($published_places as $tripPlace) {
                        if (!$tripPlace->place or !$tripPlace->country or !$tripPlace->city) {
                            continue;
                        }
                        if (!isset($places[$tripPlace->places_id])) {
                            $total_destination++;
                            $_totalDestination['city_wise'][$tripPlace->cities_id][$tripPlace->places_id] = $tripPlace->place;
                            $_totalDestination['country_wise'][$tripPlace->countries_id][$tripPlace->places_id] = $tripPlace->place;
                            $places[$tripPlace->places_id] = [
                                'id' => $tripPlace->places_id,
                                'country_id' => $tripPlace->countries_id,
                                'city_id' => $tripPlace->cities_id,
                                'title' => isset($tripPlace->place->transsingle->title) ? $tripPlace->place->transsingle->title : null,
                                'lat' => $tripPlace->place->lat,
                                'lng' => $tripPlace->place->lng,
                                'image' => check_place_photo($tripPlace->place),
                            ];
                        }
                        if (!isset($countries[$tripPlace->countries_id])) {
                            $countries[$tripPlace->countries_id] = [
                                'id' => $tripPlace->countries_id,
                                'title' => isset($tripPlace->country->trans[0]->title) ? $tripPlace->country->trans[0]->title : null,
                                'lat' => $tripPlace->country->lat,
                                'lng' => $tripPlace->country->lng,
                                'image' => get_country_flag($tripPlace->country),
                                'total_destination' => 0,
                            ];
                        }
                        if (!isset($cities[$tripPlace->cities_id])) {
                            $cities[$tripPlace->cities_id] = [
                                'id' => $tripPlace->cities_id,
                                'country_id' => $tripPlace->countries_id,
                                'title' => isset($tripPlace->city->trans[0]->title) ? $tripPlace->city->trans[0]->title : null,
                                'lat' => $tripPlace->city->lat,
                                'lng' => $tripPlace->city->lng,
                                'image' => isset($tripPlace->city->medias[0]->media->path) ?
                                    check_city_photo($tripPlace->city->medias[0]->media->path) :
                                    url(PLACE_PLACEHOLDERS),
                                'total_destination' => 0,
                            ];
                        }
                    }

                    $cacheData['published_places'] = $published_places;

                    if (!empty($cacheData) && isset($cacheData['static_map_image'])) {
                        $trip_plan->static_map_image = $cacheData['static_map_image'];
                    } else {
                        try {
                            $trip_plan->static_map_image = tripPlanThumb($trip_plan, 615, 400);
                        } catch (\Throwable $e) {
                            $trip_plan->static_map_image = tripPlanStaticImage($trip_plan);
                        }
                    }

                    if ($isFollowContent) {
                        $trip_plan->follow_header = getTripDestinationDetails($trip_plan);
                    } else {
                        $trip_plan->follow_header = null;
                    }

                    foreach ($countries as $country_id => &$country) {
                        $country['total_destination'] = isset($_totalDestination['country_wise'][$country_id]) ? count($_totalDestination['country_wise'][$country_id]) : 0;
                    }
                    foreach ($cities as $city_id =>  &$city) {
                        $city['total_destination'] = isset($_totalDestination['city_wise'][$city_id]) ? count($_totalDestination['city_wise'][$city_id]) : 0;
                    }
                    $country_title =  isset(array_values($countries)[0]['title']) ? array_values($countries)[0]['title'] : null;
                    $trip_plan->total_destination = $total_destination;
                    if (count($countries) > 1) {
                        $trip_plan->type = "multiple-country";
                        $trip_plan->data = array_values($countries);
                    } else {
                        if (count($cities) > 1) {
                            $trip_plan->type = "multiple-city";
                            $trip_plan->country_title = $country_title;
                            $trip_plan->data = array_values($cities);
                        } else {
                            $trip_plan->type = "single-city";
                            $trip_plan->country_title = $country_title;
                            $trip_plan->data = array_values($places);
                        }
                    }

                    $trip_plan->like_flag   = $authUser ? (TripsLikes::where('trips_id', $trip_plan->id)->where('users_id', $authUser->id)->exists()) : false;
                    $trip_plan->total_likes = $trip_plan->likes()->count();
                    $trip_plan->total_shares = TripsShares::where('trip_id', $trip_plan->id)->count();

                    // For Comment
                    $trip_plan = $this->__addPostCommentSection($trip_plan);
                    unset($trip_plan->published_places, $trip_plan->contribution_requests, $trip_plan->likes);
                    // $trip_plan->_totalDestination = $_totalDestination;
                    // $trip_plan->_places = $places;
                    // $trip_plan->_countries = $countries;
                    // $trip_plan->_cities = $cities;
                    if (count($trip_plan->data)) {
                        Cache::put($cacheKey, $cacheData, 60);
                        return $trip_plan;
                    }
                } else {
                    $cacheData['published_places'] = $published_places;
                    Cache::put($cacheKey, $cacheData, 60);
                }
            }
        }
        return null;
    }

    // no comment section for review
    function __preparedPlaceReview($review_id)
    {
        $authUser = auth()->user();
        $review = Reviews::whereHas('place')->with('author', 'place', 'place.transsingle', 'medias')->where('id', $review_id)->first();
        if ($review) {

            if ($review->place) {
                if ($review->author) {
                    $review->author->profile_picture = check_profile_picture($review->author->profile_picture);
                }
                $review->avg_review     = Reviews::where('places_id', $review->places_id)->avg('score');
                $review->total_review   = $review->place->reviews()->count();
                $review->place->follow_status = $authUser ?  ($review->place->followers()->where('users_id', $authUser->id)->exists()) : false;


                $_medias = [];
                $review->medias->map(function ($media) use (&$_medias) {
                    // check_media_url($media->url);
                    $media->url = replace_s3_path_with_cloudfront($media->url, '800x0');
                    if (url(PLACE_PLACEHOLDERS) != $media->url) {
                        $_medias[] = $media;
                    }
                    return $media;
                });
                unset($review->medias);
                $review->medias = $_medias;

                $review->place->name = @$review->place->transsingle->title ?? null;
                $review->place->image = @check_place_photo(@$review->place);
                $review->total_shares = PostsShares::where('type', ShareService::TYPE_REVIEW)->where('posts_type_id', $review->id)->count();

                $review->like_flag =  $authUser ? ($review->updownvotes()->where('users_id', $authUser->id)->where('vote_type', 1)->exists()) : false;
                $review->total_likes = $review->updownvotes()->where('vote_type', 1)->count();

                if (isset($review->place->transsingle)) {
                    unset($review->place->transsingle);
                }
                unset($review->updownvotes, $review->place->medias, $review->place->getMedias);
                return $review;
            }
        }
        return null;
    }
    // no comment section for review
    function __preparedHotelReview($place_id, $users_id)
    {
        $authUser = auth()->user();
        $place = Place::find($place_id);
        if ($place) {
            $review = Reviews::whereHas('place')->with('author', 'place', 'place.transsingle', 'medias')->where([
                'by_users_id' => $users_id,
                'places_id' => $place->id
            ])->first();
            if ($review) {
                if ($review->place) {
                    if ($review->author) {
                        $review->author->profile_picture = check_profile_picture($review->author->profile_picture);
                    }
                    $review->avg_review     = Reviews::where('places_id', $review->places_id)->avg('score');
                    $review->total_review   = $review->place->reviews()->count();
                    $review->place->follow_status = $authUser ?  ($review->place->followers()->where('users_id', $authUser->id)->exists()) : false;


                    $_medias = [];
                    $review->medias->map(function ($media) use (&$_medias) {
                        // check_media_url($media->url);
                        $media->url = replace_s3_path_with_cloudfront($media->url, '800x0');
                        if (url(PLACE_PLACEHOLDERS) != $media->url) {
                            $_medias[] = $media;
                        }
                        return $media;
                    });
                    unset($review->medias);
                    $review->medias = $_medias;

                    if (count($review->medias)) {
                        $review->place->name = @$review->place->transsingle->title ?? null;

                        $review->place->subtitle = null;
                        $city = @$review->place->city->transsingle->title ?? null;
                        $country = @$review->place->country->transsingle->title ?? null;
                        if ($city && $country) {
                            $review->place->subtitle = $city . ' in ' . $country;
                        } else if ($city) {
                            $review->place->subtitle = $city;
                        } else if ($country) {
                            $review->place->subtitle = $country;
                        }

                        $review->total_shares = PostsShares::where('type', ShareService::TYPE_REVIEW)->where('posts_type_id', $review->id)->count();

                        $review->like_flag =  $authUser ? ($review->updownvotes()->where('users_id', $authUser->id)->where('vote_type', 1)->exists()) : false;
                        $review->total_likes = $review->updownvotes()->where('vote_type', 1)->count();

                        if (isset($review->place->transsingle)) {
                            unset($review->place->transsingle);
                        }
                        unset($review->updownvotes, $review->place->city, $review->place->country);
                        return $review;
                    }
                }
            }
        }
        return null;
    }


    function __preparedEvent($event_id)
    {
        $authUser = auth()->user();
        $event = Events::with('getMedias')->where('id', $event_id)->first();
        if ($event) {
            $variable = json_decode($event->variable);
            if (isset($variable->url)) {
                $event->booking_url = $variable->url;
            } else {
                $event->booking_url = '';
            }
            $event->img = eventImg($event);

            $event->address = str_replace("\n", ", ", $event->address ?? '');
            $event->created_at = ($event->created_at) ? Carbon::parse($event->created_at)->format(Carbon::DEFAULT_TO_STRING_FORMAT) : null;

            $event->like_flag       = $authUser ? ($event->likes()->where('users_id', $authUser->id)->exists()) : false;
            $event->total_likes     = $event->likes()->count();
            $event->total_share     = PostsShares::where('type', ShareService::TYPE_EVENT)->where('posts_type_id', $event->id)->count();

            /*location*/ {
                $_location = null;
                $_location_type = null;
                if ($event->places_id) {
                    $_location = Place::with('transsingle')->where('id', $event->places_id)->first();
                    if ($_location) {
                        $_location_type = 'place';
                        $_location->place_type = do_placetype(@$_location->place_type ?: 'Event');
                        $_location->title = isset($_location->transsingle->title) ? $_location->transsingle->title : null;
                        unset($_location->transsingle);
                    }
                } else if ($event->countries_id) {
                    $_location = Countries::with('trans')->where('id', $event->countries_id)->first();
                    if ($_location) {
                        $_location_type = 'country';
                        $_location->title = isset($_location->trans[0]->title) ? $_location->trans[0]->title : null;
                        unset($_location->trans);
                    }
                } else if ($event->cities_id) {
                    $_location = Cities::with('trans')->where('id', $event->cities_id)->first();
                    if ($_location) {
                        $_location_type = 'city';
                        $_location->title = isset($_location->trans[0]->title) ? $_location->trans[0]->title : null;
                        unset($_location->trans);
                    }
                }

                $location = [];
                $location['type'] = $_location_type;
                $location['data'] = $_location;
                $event->location = $location;
            }

            // $mediaFile = [];
            // if ($event->getMedias) {
            //     foreach ($event->getMedias as $media) {
            //         $mediaFile[] = S3_BASE_URL . $media->url;
            //     }
            // }

            $event = $this->__addPostCommentSection($event);
            unset($event->getMedias, $event->likes);
            unset($event->variable);
            // $event->medias = $mediaFile;
            return $event;
        }
        return null;
    }

    function __addPostCommentSection(&$post)
    {
        // PostsComments::query()->with('author', 'tags', 'sub', 'sub.author', 'sub.tags','medias','medias.media')->where('posts_id', 358107)->where('type', 'post')->whereNull('parents_id')->orderBy('created_at', 'desc')->limit(2)->get();
        $authUser = auth()->user();
        $post_comments = [];
        $post->total_comments = $post->postComments()->count();
        $post->comment_flag = $authUser ? ($post->postComments()->where('users_id', $authUser->id)->count() ? true : false) : false;
        foreach ($post->postComments()->take(2)->get() as $parent_comment) {
            // For Parent Comment
            $parent_comment->like_status = $authUser ? ($parent_comment->likes()->where('users_id', $authUser->id)->exists()) : false;
            $parent_comment->total_likes = $parent_comment->likes()->count();
            if ($parent_comment->medias) {
                foreach ($parent_comment->medias as $media) {
                    $media->media;
                }
            }
            if ($parent_comment->author) {
                $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
            }
            $parent_comment->comment_flag = $authUser ? ($parent_comment->sub()->where('users_id', $authUser->id)->count() ? true : false) : false;
            $parent_comment->text = convert_post_text_to_tags(strip_tags($parent_comment->text), $parent_comment->tags, false);
            unset($parent_comment->likes, $parent_comment->tags);

            // For Sub Comment
            if ($parent_comment->sub) {
                foreach ($parent_comment->sub as $sub_comment) {
                    $sub_comment->like_status = $authUser ? ($sub_comment->likes()->where('users_id', $authUser->id)->exists()) : false;
                    $sub_comment->total_likes = $sub_comment->likes()->count();
                    if ($sub_comment->medias) {
                        foreach ($sub_comment->medias as $media) {
                            $media->media;
                        }
                    }
                    if ($sub_comment->author) {
                        $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                    }
                    $sub_comment->text = convert_post_text_to_tags(strip_tags($sub_comment->text), $sub_comment->tags, false);
                    unset($sub_comment->likes, $sub_comment->tags);
                }
            }
            $post_comments[] = $parent_comment;
        }
        unset($post->postComments);
        $post->post_comments = $post_comments;
        return $post;
    }

    function __addCommentSection(&$post)
    {
        $authUser = auth()->user();
        $post_comments = [];
        $post->total_comments = $post->comments()->count();
        $post->comment_flag = $authUser ? ($post->comments()->where('users_id', $authUser->id)->count() ? true : false) : false;
        foreach ($post->comments()->take(2)->get() as $parent_comment) {
            // For Parent Comment
            $parent_comment->like_status = ($authUser) ? ($parent_comment->likes()->where('users_id', $authUser->id)->exists()) : false;
            $parent_comment->total_likes = $parent_comment->likes()->count();
            if ($parent_comment->medias) {
                foreach ($parent_comment->medias as $media) {
                    $media->media;
                }
            }
            if ($parent_comment->author) {
                $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
            }
            $parent_comment->comment_flag = $authUser ? ($parent_comment->sub()->where('users_id', $authUser->id)->count() ? true : false) : false;
            $parent_comment->text = convert_post_text_to_tags(strip_tags($parent_comment->text), $parent_comment->tags, false);
            unset($parent_comment->likes, $parent_comment->tags);

            // For Sub Comment
            if ($parent_comment->sub) {
                foreach ($parent_comment->sub as $sub_comment) {
                    $sub_comment->like_status = ($authUser) ? ($sub_comment->likes()->where('users_id', $authUser->id)->exists()) : false;
                    $sub_comment->total_likes = $sub_comment->likes()->count();
                    if ($sub_comment->medias) {
                        foreach ($sub_comment->medias as $media) {
                            $media->media;
                        }
                    }
                    if ($sub_comment->author) {
                        $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                    }
                    $sub_comment->text = convert_post_text_to_tags(strip_tags($sub_comment->text), $sub_comment->tags, false);
                    unset($sub_comment->likes, $sub_comment->tags);
                }
            }
            $post_comments[] = $parent_comment;
        }
        unset($post->comments);
        $post->post_comments = $post_comments;
        return $post;
    }

    function shareTripAndPlaceAdnMediaFeed(&$responsePost, $postItem)
    {

        $feed = $this->__preparedShareTripAndPlaceAdnMediaFeed($postItem);
        if ($feed) {
            $responsePost[] = $feed;
        }
    }

    function __preparedShareTripAndPlaceAdnMediaFeed($postItem)
    {
        $authUser = auth()->user();
        switch ($postItem->action) {
            case ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE:
                $sharePost = TripsMediasShares::with('author', 'tags')->find('id', $postItem->variable)->first();
                if (!(!$sharePost or !$sharePost->author)) {
                    $sharePost->author->profile_picture = check_profile_picture($sharePost->author->profile_picture);
                    $sharePost->comment = convert_post_text_to_tags(strip_tags($sharePost->comment), $sharePost->tags, false);
                    unset($sharePost->tags);

                    $tripMedias = TripMedias::find($sharePost->trip_media_id);
                    if (!(!$tripMedias or !$tripMedias->media)) {
                        $trip = TripPlans::with('author')->where('id', $tripMedias->trips_id)->first();
                        if ($trip) {
                            $contributer = $trip->contribution_requests()->where('status', 1)->pluck('users_id')->toArray();
                            $mediaAuthor = $trip->author;
                            if (in_array($sharePost->author->id, $contributer) || $sharePost->author->id == $trip->users_id) { // Share Person Is Part Of Plan
                                $mediaAuthor = $sharePost->author;
                            }
                            if ($mediaAuthor) {
                                $mediaAuthor->profile_picture = check_profile_picture($mediaAuthor->profile_picture);
                            }


                            $sharePost->total_shares =  TripsMediasShares::where('trip_media_id', $sharePost->trip_media_id)->count();
                            $sharePost->total_likes = TripContentPostLike::where('posts_type', ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE)->where('posts_id', $sharePost->id)->count();

                            $tripMedias->media->users = User::find($tripMedias->media->users_id);
                            if ($tripMedias->media->users && $tripMedias->tripPlace) {
                                // PLACE
                                $tripMedias->place_title = isset($tripMedias->tripPlace->place->trans[0]->title) ? $tripMedias->tripPlace->place->trans[0]->title : null;
                                $tripMedias->lat = $tripMedias->tripPlace->place->lat;
                                $tripMedias->lng = $tripMedias->tripPlace->place->lng;

                                unset($tripMedias->media->users);
                                $tripMedias->media->users = $mediaAuthor;

                                $tripMedias->media->url = check_cover_photo($tripMedias->media->url);

                                unset($tripMedias->tripPlace);
                                $sharePost->reference = $tripMedias;
                                $sharePost = $this->__addPostCommentSection($sharePost);
                                return [
                                    'page_id'   => $this->link("newsfeed/" . $sharePost->author->username . "/share/" . $postItem->id),
                                    'unique_link'   => url("newsfeed/" . $sharePost->author->username . "/share/" . $postItem->id),
                                    'type'      => ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE,
                                    'action'    => 'share',
                                    'data'      => $sharePost
                                ];
                            }
                        }
                    }
                }
                break;
            case ShareService::TYPE_TRIP_PLACE_SHARE:
                $sharePost = TripsPlacesShares::with('author', 'tags')->where('id', $postItem->variable)->first();
                if ($sharePost) {
                    if ($sharePost->author) {
                        $sharePost->author->profile_picture = check_profile_picture($sharePost->author->profile_picture);
                    }
                    $sharePost->comment = convert_post_text_to_tags(strip_tags($sharePost->comment), $sharePost->tags, false);
                    unset($sharePost->tags);
                    $tripPlaces = TripPlaces::select('id', 'trips_id')->where('id', $sharePost->trip_place_id)->first();
                    if ($tripPlaces) {
                        $sharePost->total_shares =  TripsPlacesShares::where('trip_place_id', $tripPlaces->id)->count();
                        $sharePost->total_likes = TripContentPostLike::where('posts_type', ShareService::TYPE_TRIP_PLACE_SHARE)->where('posts_id', $sharePost->id)->count();
                        $sharePost = $this->__addPostCommentSection($sharePost);
                        $sharePost->reference = TripPlans::with('author')->where('id', $tripPlaces->trips_id)->first();
                        if ($sharePost->reference) {
                            $lastUpdatedLog = ActivityLog::query()->where([
                                'type' => 'Trip',
                                'variable' => $tripPlaces->trips_id
                            ])->whereIn('action', ['create', 'update'])->orderBy('time', 'DESC')->first();
                            $sharePost->reference->updated_at = ($lastUpdatedLog) ? $lastUpdatedLog->time : null;

                            if ($sharePost->reference->author) {
                                $sharePost->reference->author->profile_picture = check_profile_picture($sharePost->reference->author->profile_picture);
                            }
                            if ($sharePost->reference->published_places) {
                                $places = [];
                                $countries = [];
                                $cities = [];
                                $total_destination = 0;
                                $_totalDestination = ['city_wise' => [], 'country_wise' => []];

                                foreach ($sharePost->reference->published_places as $tripPlace) {
                                    if (!$tripPlace->place or !$tripPlace->country or !$tripPlace->city) {
                                        continue;
                                    }
                                    if (!isset($places[$tripPlace->places_id])) {
                                        $total_destination++;
                                        $_totalDestination['city_wise'][$tripPlace->cities_id][$tripPlace->places_id] = $tripPlace->place;
                                        $_totalDestination['country_wise'][$tripPlace->countries_id][$tripPlace->places_id] = $tripPlace->place;
                                        $places[$tripPlace->places_id] = [
                                            'id' => $tripPlace->places_id,
                                            'country_id' => $tripPlace->countries_id,
                                            'city_id' => $tripPlace->cities_id,
                                            'title' => isset($tripPlace->place->transsingle->title) ? $tripPlace->place->transsingle->title : null,
                                            'lat' => $tripPlace->place->lat,
                                            'lng' => $tripPlace->place->lng,
                                            'image' => check_place_photo($tripPlace->place),
                                            'is_share' => $sharePost->trip_place_id == $tripPlace->id
                                        ];
                                    }
                                    if (!isset($countries[$tripPlace->countries_id])) {
                                        $countries[$tripPlace->countries_id] = [
                                            'id' => $tripPlace->countries_id,
                                            'title' => isset($tripPlace->country->trans[0]->title) ? $tripPlace->country->trans[0]->title : null,
                                            'lat' => $tripPlace->country->lat,
                                            'lng' => $tripPlace->country->lng,
                                            'image' => get_country_flag($tripPlace->country),
                                            'total_destination' => 0,
                                            'is_share' => $sharePost->trip_place_id == $tripPlace->id
                                        ];
                                    }
                                    if (!isset($cities[$tripPlace->cities_id])) {
                                        $cities[$tripPlace->cities_id] = [
                                            'id' => $tripPlace->cities_id,
                                            'country_id' => $tripPlace->countries_id,
                                            'title' => isset($tripPlace->city->trans[0]->title) ? $tripPlace->city->trans[0]->title : null,
                                            'lat' => $tripPlace->city->lat,
                                            'lng' => $tripPlace->city->lng,
                                            'image' => isset($tripPlace->city->medias[0]->media->path) ?
                                                check_city_photo($tripPlace->city->medias[0]->media->path) :
                                                url(PLACE_PLACEHOLDERS),
                                            'total_destination' => 0,
                                            'is_share' => $sharePost->trip_place_id == $tripPlace->id
                                        ];
                                    }
                                }

                                foreach ($countries as $country_id => &$country) {
                                    $country['total_destination'] = isset($_totalDestination['country_wise'][$country_id]) ? count($_totalDestination['country_wise'][$country_id]) : 0;
                                }
                                foreach ($cities as $city_id =>  &$city) {
                                    $city['total_destination'] = isset($_totalDestination['city_wise'][$city_id]) ? count($_totalDestination['city_wise'][$city_id]) : 0;
                                }
                                $country_title =  isset(array_values($countries)[0]['title']) ? array_values($countries)[0]['title'] : null;
                                $sharePost->reference->total_destination = $total_destination;
                                if (count($countries) > 1) {
                                    $sharePost->reference->type = "multiple-country";
                                    $sharePost->reference->data = array_values($countries);
                                } else {
                                    if (count($cities) > 1) {
                                        $sharePost->reference->type = "multiple-city";
                                        $sharePost->reference->country_title = $country_title;
                                        $sharePost->reference->data = array_values($cities);
                                    } else {
                                        $sharePost->reference->type = "single-city";
                                        $sharePost->reference->country_title = $country_title;
                                        $sharePost->reference->data = array_values($places);
                                    }
                                }

                                $sharePost->reference->like_flag   = $authUser ?  (TripsLikes::where('trips_id', $sharePost->reference->id)->where('users_id', $authUser->id)->exists()) : false;
                                $sharePost->reference->total_likes = $sharePost->reference->likes()->count();
                                $sharePost->reference->total_shares = TripsShares::where('trip_id', $sharePost->reference->id)->count();
                                unset($sharePost->reference->published_places);
                                return  [
                                    'page_id'   => $this->link("newsfeed/" . $sharePost->author->username . "/share/" . $postItem->id),
                                    'unique_link'   => url("newsfeed/" . $sharePost->author->username . "/share/" . $postItem->id),
                                    'type'      => ShareService::TYPE_TRIP_PLACE_SHARE,
                                    'action'    => 'share',
                                    'data'      => $sharePost
                                ];
                            }
                        }
                    }
                }
                break;
            case ShareService::TYPE_TRIP_SHARE:
                $sharePost = TripsShares::with('author', 'tags')->where('id', $postItem->variable)->first();
                if ($sharePost) {

                    $is_multiple_share_available = false;
                    $check_availeble_shares = TripsShares::whereNull('comment')->where(['trip_id' => $sharePost->trip_id])->where('id', '<>', $sharePost->id)->pluck('user_id')->toArray();
                    if (count($check_availeble_shares)) {
                        $is_multiple_share_available = true;
                    }

                    if ($is_multiple_share_available) {
                        $sharePost->reference = $this->__preparedRegularTrip($sharePost->trip_id);
                        if ($sharePost->reference) {

                            $sharePost->reference->header = User::whereIn('id', $check_availeble_shares)->take(5)->select(['username', 'profile_picture'])->get()->map(function ($user) {
                                $user->profile_picture = check_profile_picture($user->profile_picture);
                                return $user;
                            })->toArray();

                            return [
                                'page_id'       => null,
                                'unique_link'   => null,
                                'type'      => self::TYPE_TRIP,
                                'action'    => 'multiple_share',
                                'data'      => $sharePost->reference
                            ];
                        }
                    } else {
                        if ($sharePost->author) {
                            $sharePost->author->profile_picture = check_profile_picture($sharePost->author->profile_picture);
                        }
                        $sharePost->fgd = convert_post_text_to_tags(strip_tags($sharePost->comment), $sharePost->tags, false);
                        unset($sharePost->tags);

                        $sharePost->total_shares =  TripsShares::where('trip_id', $sharePost->trip_id)->count();
                        $sharePost->total_likes = TripContentPostLike::where('posts_type', ShareService::TYPE_TRIP_SHARE)->where('posts_id', $sharePost->id)->count();

                        $sharePost = $this->__addPostCommentSection($sharePost);

                        $sharePost->reference = $this->__preparedRegularTrip($sharePost->trip_id);
                        if ($sharePost->reference) {
                            return [
                                'page_id'   => $this->link("newsfeed/" . $sharePost->author->username . "/share/" . $postItem->id),
                                'unique_link'   => url("newsfeed/" . $sharePost->author->username . "/share/" . $postItem->id),
                                'type'      => ShareService::TYPE_TRIP_SHARE,
                                'action'    => 'share',
                                'data'      => $sharePost
                            ];
                        }
                    }
                }
                break;
        }
        return null;
    }

    function onGoingTrip(&$responsePost)
    {
        $temp = sessionGet('loaded_on_going_trip', []);
        $trip = getMyFriendsOnGoingTripsRecursively($temp);
        if (!is_null($trip) && is_int($trip)) {
            $temp[] = $trip;
            $this->__onGoingTrip($responsePost, $trip);
            sessionPut('loaded_on_going_trip', $temp);
        }
    }

    function __onGoingTrip(&$responsePost, $trip_id)
    {
        $authUser = auth()->user();
        $trip = TripPlans::with('author', 'countries')->whereHas('author')->where('id', $trip_id)->first();
        if ($trip) {
            if ($trip->author) {
                $trip->author->profile_picture = check_profile_picture($trip->author->profile_picture);
            }
            $log = ActivityLog::query()->where([
                'type' => 'Trip',
                'variable' => $trip->id
            ])->whereIn('action', ['create', 'update'])->orderBy('time', 'DESC')->first();

            $trip->updated_at =  (!$log) ? null : $log->time;

            $countries = [];
            if ($trip->countries) {
                $trip->countries->filter(function ($country)  use (&$countries) {
                    $countries[$country->id] =  [
                        'id' => $country->id,
                        'title' => isset($country->trans[0]->title) ? $country->trans[0]->title : null,
                        'image' => "https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/" . strtolower($country->iso_code) . ".png"
                    ];
                });
                $countries = array_values($countries);
            }
            unset($trip->countries);
            $trip->countries = $countries;
            $destinations = [];
            $days = [];
            $times = [];
            foreach ($trip->published_places as $tripPlace) {
                if (!$tripPlace->place) {
                    continue;
                }
                $days[] = $tripPlace->date;
                $times[] = $tripPlace->time;
                $img = @check_place_photo(@$tripPlace->place);
                $destinations[$tripPlace->place->id] = [
                    'trip_place_id'  => $tripPlace->id,
                    'place_id'  => $tripPlace->place->id,
                    'title'  => isset($tripPlace->place->transsingle->title) ? $tripPlace->place->transsingle->title : null,
                    'image'  => $img,
                    'priority' => $img != url(PLACE_PLACEHOLDERS)
                ];
            }

            // display images first if place have
            $destinations = collect($destinations)->sortByDesc('priority')->map(function ($des) {
                unset($des['priority']);
                return $des;
            })->values();

            $endTime = collect($times)->filter(function ($time) {
                return $time != null;
            })->unique()->sort()->last();
            if (!$endTime) {
                $endTime = collect($days)->unique()->sort()->last();
            }

            $trip->total_destinations = count($destinations);
            $trip->destinations = $destinations;
            $trip->budget = TripPlaces::query()->whereIn('id', collect($destinations)->pluck('trip_place_id')->toArray())->sum('budget');
            $trip->days = collect($days)->unique()->count();
            $trip->start_time = collect($days)->unique()->min();
            $trip->end_time = Carbon::parse($endTime)->format('Y-m-d');
            $trip->trip_is_ended = Carbon::parse($endTime)->lte(Carbon::now());
            $trip->like_flag   = (TripsLikes::where('trips_id', $trip->id)->where('users_id', $authUser->id)->first()) ? true : false;
            $trip->total_likes = $trip->likes()->count();
            $trip->total_shares = TripsShares::where('trip_id', $trip->id)->count();


            // For Comment
            $trip = $this->__addPostCommentSection($trip);
            unset($trip->likes, $trip->shares, $trip->published_places);
            if (isset($trip->cover) && $trip->cover != '') {
                $trip->cover = "https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg";
            }
            $responsePost[] = [
                'page_id'   => null,
                'unique_link'   => null,
                'type'      => strtolower(self::TYPE_TRIP),
                'action'    => "my_friend_on_going_trip",
                'data'      => $trip
            ];
        }
    }

    function todayFriendFollowerTrip(&$responsePost)
    {
        $authUser                   = auth()->user();
        // $userProfileService         = app(UserProfileService::class);
        // $tripsSuggestionsService    = app(TripsSuggestionsService::class);
        $followrs = UsersFollowers::where('followers_id', $authUser->id)->where('follow_type', 1)->pluck('users_id')->toArray();
        $trips =  TripPlans::whereIn('users_id', $followrs)->pluck('id')->toArray();
        if (count($trips) > 0) {
            $trips = TripPlaces::query()
                ->whereRaw('trips_id in (' . implode(',', $trips) . ')')
                ->whereRaw('versions_id = (SELECT trips.active_version FROM trips WHERE trips.id = trips_places.trips_id and trips.deleted_at is null)')
                ->where('date',  date('Y-m-d'))
                ->get()
                ->toArray();
            if (count($trips) > 0) {
                $random_keys = array_rand($trips, 1);
                if (isset($trips[$random_keys]['time'])) {
                    // $tripStartTime = $trips[$random_keys]['time'];
                    $trips_id = $trips[$random_keys]['trips_id'];
                    $trip = TripPlans::with('author')->where('id', $trips_id)->first();
                    if ($trip) {
                        if ($trip->author) {
                            $trip->author->profile_picture = check_profile_picture($trip->author->profile_picture);
                        }

                        $log = ActivityLog::query()->where([
                            'type' => 'Trip',
                            'variable' => $trip->id
                        ])->whereIn('action', ['create', 'update', 'plan_updated'])->orderBy('time', 'DESC')->first();

                        $trip->updated_at = $log ? $log->time : null;

                        $cities = [];
                        $places = [];
                        //->where('date', date('Y-m-d'))
                        if ($trip->published_places) {
                            $todayPlaces = collect($trip->published_places)->sortBy('order');
                            foreach ($todayPlaces as $tripPlace) {
                                $tripPlace->city->title  = isset($tripPlace->city->trans[0]->title) ? $tripPlace->city->trans[0]->title : null;
                                $tripPlace->city->image  = check_city_photo(@$tripPlace->city->getMedias[0]->url);

                                $tripPlace->place->title  = isset($tripPlace->place->trans[0]->title) ? $tripPlace->place->trans[0]->title : null;
                                $tripPlace->place->image  = check_place_photo(@$tripPlace->place);
                                unset($tripPlace->city->getMedias, $tripPlace->city->trans, $tripPlace->place->trans, $tripPlace->place->medias);
                                $tripPlace->place->is_active = false;

                                $places[$tripPlace->cities_id][] =  $tripPlace->place;
                                $cities[$tripPlace->cities_id] = $tripPlace->city;
                            }

                            $cities = collect($cities)->toArray();
                            foreach ($todayPlaces as $tripPlace) {
                                $cities[$tripPlace->cities_id]['places'] = $places[$tripPlace->cities_id];
                            }

                            $cities = array_values(collect($cities)->toArray());
                            $cities[0]['places'][0]->is_active = true;

                            $like_flag   = $authUser ? (TripsLikes::where('trips_id', $trip->id)->where('users_id', $authUser->id)->exists()) : false;
                            $total_likes = $trip->likes()->count();
                            $total_shares =  TripsShares::where('trip_id', $trip->id)->count();

                            $total_comments = $trip->postComments()->count();
                            $comment_flag = $authUser ? ($trip->postComments()->where('users_id', $authUser->id)->exists()) : false;
                            $post_comments = [];
                            foreach ($trip->postComments()->take(2)->get() as $parent_comment) {
                                // For Parent Comment
                                $parent_comment->like_status = $authUser ? ($parent_comment->likes()->where('users_id', $authUser->id)->exists()) : false;
                                $parent_comment->total_likes = $parent_comment->likes()->count();
                                if ($parent_comment->medias) {
                                    foreach ($parent_comment->medias as $media) {
                                        $media->media;
                                    }
                                }
                                if ($parent_comment->author) {
                                    $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                                }
                                $parent_comment->comment_flag = $authUser ? ($parent_comment->sub->where('users_id', $authUser->id)->exists()) : false;
                                $parent_comment->text = convert_post_text_to_tags(strip_tags($parent_comment->text), $parent_comment->tags, false);

                                // For Sub Comment
                                if ($parent_comment->sub) {
                                    foreach ($parent_comment->sub as $sub_comment) {
                                        $sub_comment->like_status = $authUser ? ($sub_comment->likes()->where('users_id', $authUser->id)->exists()) : false;
                                        $sub_comment->total_likes = $sub_comment->likes()->count();
                                        if ($sub_comment->medias) {
                                            foreach ($sub_comment->medias as $media) {
                                                $media->media;
                                            }
                                        }
                                        if ($sub_comment->author) {
                                            $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                                        }
                                        $sub_comment->text = convert_post_text_to_tags(strip_tags($sub_comment->text), $sub_comment->tags, false);
                                    }
                                }

                                $post_comments[] = $parent_comment;
                            }
                            // $timeline['post_comments'] = $post_comments;
                            // unset($timeline['trip']->versions, $timeline['trip']->likes, $timeline['trip']->postComments, $timeline['trip']->shares);

                            unset($trip->published_places, $trip->postComments, $trip->likes);
                            $responsePost[] = [
                                'page_id'   => null,
                                'unique_link'   => null,
                                'type'      => strtolower(self::TYPE_TRIP),
                                'action'    => "my_friend_today_trip",
                                'data'      => [
                                    'trip' => $trip,
                                    'total_cities' => count($cities),
                                    'cities' => $cities,
                                    'comment_flag' => $comment_flag,
                                    'post_comments' => $post_comments,
                                    'total_comments' => $total_comments,
                                    'like_flag' => $like_flag,
                                    'total_likes' => $total_likes,
                                    'total_shares' => $total_shares,
                                ],
                            ];
                        }
                    }
                }
            }
        }
    }

    // Algoritm Not Implemented
    public function trendingEvents()
    {
        $events         = collect([]);
        $followCities   = [];
        $ip             = getClientIP();
        $geoLoc         = ip_details($ip);
        $days           = DUMMY_DATA_MODE ? 1000 : 28;
        $authUser       = auth()->user();
        $limit          = 45;

        $getLocCity = Cities::with('transsingle')->whereHas('transsingle', function ($q) use ($geoLoc) {
            $q->where('title', 'LIKE', "%" . $geoLoc['city'] . "%");
        })->first();

        $followCities[] = isset($getLocCity) ? $getLocCity->id : 0;
        if (Events::query()->whereIn('cities_id', $followCities)->whereDate('created_at', '>', Carbon::now()->subDays($days))->exists()) {
            $events = Events::whereIn('cities_id', $followCities)->whereDate('created_at', '>', Carbon::now()->subDays($days))->orderBy(DB::raw('ratings,reviews,variable'), 'DESC')->limit($limit)->get();
        } else {
            $followCities = getUserFollwoingForEvents(Auth::user()->id);
            if (Events::query()->whereIn('cities_id', $followCities)->whereDate('created_at', '>', Carbon::now()->subDays($days))->exists()) {
                $events = Events::whereIn('cities_id', $followCities)->whereDate('created_at', '>', Carbon::now()->subDays($days))->orderBy(DB::raw('ratings,reviews,variable'), 'DESC')->limit($limit)->get();
            }
        }

        if ($authUser && $events->count()) {
            $events = $events->sortByDesc(function ($event) {
                return $event->shares->count();
            });

            $finalEvents = [];
            $events->each(function ($event) use (&$finalEvents) {

                $img = eventImg($event);

                $returnArr = [
                    'id'            => $event->id,
                    'title'         => $event->title,
                    'description'   => !empty($event->address) ? str_replace("\n", ", ", $event->address ?? '') : '',
                    'img'           => $img,
                    'place_type'    => do_placetype(@$event->place->place_type ?: 'Event'),
                    'category'      => $event->category,
                    'lat'           => $event->lat,
                    'lng'           => $event->lng,
                    'start'         => $event->start,
                    'end'           => $event->end,
                    'created_at'    => Carbon::parse($event->created_at)->format(Carbon::DEFAULT_TO_STRING_FORMAT),
                ];
                if ($event->places_id) {
                    $returnArr = array_merge($returnArr, [
                        'type'          => 'place',
                        'location'      => [
                            'id'    => ($event->place->id) ? $event->place->id : null,
                            'title' => @isset($event->place->transsingle->title) ? $event->place->transsingle->title : null
                        ]
                    ]);
                    $finalEvents[] = $returnArr;
                } elseif ($event->cities_id) {
                    $returnArr =  array_merge($returnArr, [
                        'type'          => 'city',
                        'location'      => [
                            'id'    => $event->city->id,
                            'title' => isset($event->city->trans[0]->title) ? $event->city->trans[0]->title : null
                        ]
                    ]);
                    $finalEvents[] = $returnArr;
                } elseif ($event->countries_id) {
                    $returnArr =  array_merge($returnArr, [
                        'type'          => 'country',
                        'location'      => [
                            'id'    => isset($event->country->id) ? $event->country->id : null,
                            'title' => isset($event->country->trans[0]->title) ? $event->country->trans[0]->title : null
                        ]
                    ]);
                    $finalEvents[] = $returnArr;
                }
            })->toArray();

            $finalEvents = array_values(collect($finalEvents)->toArray());
            if (count($finalEvents) > 3) return $finalEvents;
        }
        return null;
    }


    /***************************************************************************************************************************
    Secondary Post Helpers :: Secondary Post Helpers :: Secondary Post Helpers :: Secondary Post Helpers :: Secondary Post Helpers
     ***************************************************************************************************************************/

    public function getTopPlaces()
    {
        if (DUMMY_DATA_MODE) {
            $places_id = Place::select('id')->inRandomOrder()->take(rand(10, 15))->pluck('id');
        } else {
            $places_id = getTopPlacesByAuthUser();
        }
        if (count($places_id) > 4) {
            $top_places = Place::whereIn('id', $places_id)->get();
            foreach ($top_places as $tplace) {
                if (!@$tplace) {
                    continue;
                }
                $tplace->medias_url = S3_BASE_URL . "th180/" . @$tplace->medias[0]->url;
                $tplace->place_title = @$tplace->trans[0]->title ?? null;
                $tplace->place_type = @do_placetype($tplace->place_type) ?? null;
                $tplace->city_title = @$tplace->city->trans[0]->title ?? null;
                $tplace->country_title = @$tplace->country->trans[0]->title ?? null;
                $tplace->is_follow = @$tplace->followers()->where('users_id', Auth::user()->id)->exists();
                $img_list = [];
                if (isset($tplace->place)) {
                    foreach ($tplace->medias as $pm)
                        $img_list[] = S3_BASE_URL . "th180/" . @$pm->url;
                }
                $tplace->img_list = $img_list;
                unset($tplace->trans, $tplace->place_type, $tplace->city, $tplace->country, $tplace->place, $tplace->followers, $tplace->medias);
            }
            if (count($places_id) > 4)
                return $top_places;
        }
        return [];
    }

    public function link($link)
    {
        $link = url($link);
        if (true && is_string($link)) {
            return base64_encode($link);
        }
        return $link;
    }
}
