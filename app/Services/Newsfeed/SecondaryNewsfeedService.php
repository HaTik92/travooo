<?php

namespace App\Services\Newsfeed;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\ActivityMedia\Media;
use App\Models\City\Cities;
use App\Models\City\CitiesFollowers;
use App\Models\Country\Countries;
use App\Models\Country\CountriesFollowers;
use App\Models\Place\Medias;
use App\Models\Place\Place;
use App\Models\Place\PlaceFollowers;
use App\Models\Posts\Checkins;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsCities;
use App\Models\Posts\PostsCountries;
use App\Models\Posts\PostsMedia;
use App\Models\Posts\PostsPlaces;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use App\Models\User\User;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Api\PrimaryPostService;
use Carbon\Carbon;
use stdClass;
use App\Services\Api\UserProfileService;

/**
 * Hear All Secandry Post Defind With Algorithm
 */
defined("DUMMY_DATA_MODE") or define("DUMMY_DATA_MODE", false);
class SecondaryNewsfeedService
{
    public function __construct()
    {
        $this->per_page = 6;
    }

    public function videoYouMightLike()
    {
        $medias = null;
        if (DUMMY_DATA_MODE) {
            $results = DB::select('SELECT id FROM medias where RIGHT(url, 4) = ".mp4" AND deleted_at IS NULL');
            if (count($results) > 0) {
                $medias = Media::whereIn('id', array_column($results, 'id'))
                    ->whereRaw('EXISTS(select * from  users where id=medias.users_id)')
                    ->whereRaw('users_id is not null')
                    ->paginate($this->per_page);
            }
        } else {
            $counterChecker = DB::select("select count(*) as count from (select * from `posts` where date(`created_at`) > ? and `posts`.`deleted_at` is null GROUP BY users_id) as tbl", [Carbon::now()->subDays(7)]);
            if ($counterChecker[0]->count >= 5) {
                $tempPostsId = Posts::whereDate('created_at', '>', Carbon::now()->subDays(7))->pluck('id')->toArray();
                $PostsCities = PostsCities::whereIn('posts_id', $tempPostsId)->groupBy('posts_id')->pluck('posts_id');
                $PostsCountries = PostsCountries::whereIn('posts_id', $tempPostsId)->groupBy('posts_id')->pluck('posts_id');
                $PostsPlaces = PostsPlaces::whereIn('posts_id', $tempPostsId)->groupBy('posts_id')->pluck('posts_id');
                $finalPosts = $PostsCities->merge($PostsCountries)->merge($PostsPlaces)->unique()->toArray();
                $mediaIds = PostsMedia::whereIn('posts_id', $finalPosts)->pluck('medias_id')->unique()->toArray();
                $medias =  Medias::selectRaw('*, ((select count(*) from `medias_comments` where `medias_id`=`medias`.`id`) 
                + (select count(*) from `medias_likes` where `medias_id`=`medias`.`id`) 
                + (select count(*) from `medias_shares` where `medias_id`=`medias`.`id`)
                + (SELECT COUNT(*) FROM `post_tags` WHERE `posts_type`="trips_places_media_comment" AND `posts_id` =`medias`.`id`)) as rate')
                    ->whereRaw('EXISTS(select * from  users where id=medias.users_id)')
                    ->whereRaw('users_id is not null')
                    ->where('url', 'like', '%.mp4')
                    ->whereIn('id', $mediaIds)
                    ->orderBy('rate', 'desc')
                    ->paginate($this->per_page);
            }
        }

        if ($medias) {
            // $faker = \Faker\Factory::create();
            $medias = modifyPagination($medias);
            if (!($medias->total > 3)) {
                return NULL;
            }
            $medias->data =  $medias->data->map(function ($media) {
                $user = User::find($media->users_id);
                return [
                    'id'                => $media->id,
                    'title'             => $media->title,
                    'url'               => check_media_video($media->url),
                    'uploaded_at'       => $media->uploaded_at,
                    'source_url'        => check_cover_photo($media->source_url),
                    'post_by'           => [
                        'id'                => $media->users_id,
                        'name'              => $user->name,
                        'profile_picture'   => check_profile_picture($user->profile_picture),
                    ]
                ];
            })->toArray();
            return $medias;
        }
        return null;
    }

    public function trendingDestinations()
    {
        $authUser   = auth()->user();
        $places = null;
        if (DUMMY_DATA_MODE) {
            $places = Place::with('transsingle')->paginate($this->per_page);
        } else {
            $authUser   = auth()->user();
            $places = PlaceFollowers::select('places_id', DB::raw('COUNT(users_id) AS total_follower'))
                ->whereHas('place')->with('place', 'place.transsingle')
                ->where('created_at', ">=", Carbon::now()->subDays(7))
                ->where('users_id', '!=', $authUser->id)
                ->groupBy('places_id')
                ->orderBy('total_follower', 'DESC')
                ->paginate($this->per_page);
            if (!$places->total() > 3) {
                return null;
            }
        }

        if ($places) {
            $places = modifyPagination($places);
            if ($places->total > 3) {
                $places->data =  $places->data->map(function ($place) {
                    if (!DUMMY_DATA_MODE) {
                        $place = $place->place;
                    }
                    return [
                        'id'                => $place->id,
                        'title'             => @$place->transsingle->title ?? null,
                        'description'       => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                        'img'               => check_place_photo(@$place),
                        'total_followers'   => count($place->followers),
                        'place_type'        => do_placetype(@$place->place_type ?: 'Event'),
                        'city'              => @$place->city->transsingle->title ?? null,
                        'country'           => @$place->city->country->transsingle->title ?? null,
                    ];
                })->toArray();
                return $places;
            }
        }
        return null;
    }

    public function discoverNewTravellers(&$responsePost)
    {
        if (DUMMY_DATA_MODE) {
            $users = User::whereDate('created_at', '>', Carbon::now()->subDays(500))->orderBy('created_at', 'DESC')->take(50)->get()->each(function ($user) {
                $user->follow_flag = UsersFollowers::where('followers_id', auth()->user()->id)
                    ->where('users_id', $user->id)->where('follow_type', 1)->first()  ? true : false;
                $user->total_follower = $user->get_followers()->whereHas('follower')->count();
                $user->latest_followers = $user->get_followers()->whereHas('follower')->take(3)->get()->map(function ($follower) {
                    return [
                        'id' => $follower->followers_id,
                        'name' => $follower->follower->name,
                    ];
                })->toArray();
                unset($user->get_followers);
            })->toArray();

            if (count($users)) {
                $responsePost[] =  ['page_id'   => null, 'unique_link' => null, 'action' => null, 'type' => PrimaryPostService::SECONDARY_DISCOVER_NEW_TRAVELLERS, 'data' => $users];
            }
        } else {
            $authUser   = auth()->user();
            $userLoc = userLoc($authUser, TRUE);

            // $ip = get_client_ip();
            $myTravelStyles = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();

            // #1 & #2
            $users = User::whereNotNull('profile_picture')
                ->whereNotIn('id', [1109])
                ->whereIn('nationality', $userLoc)
                ->orderBy('created_at', 'DESC')
                ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                    $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                        $travelstylesSubQuery->whereIn('conf_lifestyles_id', $myTravelStyles);
                    });
                })
                ->take(50)->get();

            if (collect($users)->count() == 0) {
                // #3 & #4
                $otherUser = userLoc($authUser, TRUE);
                $users = User::whereNotNull('profile_picture')
                    ->whereNotIn('id', [1109])
                    ->whereNotIn('nationality', [$authUser->nationality])
                    ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                        $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                            $travelstylesSubQuery->whereIn('conf_lifestyles_id', $myTravelStyles);
                        });
                    })
                    ->when(count($otherUser), function ($q) use ($otherUser) {
                        $q->orWhereIn('nationality', $otherUser);
                    })
                    ->orderBy('created_at', 'DESC')->take(50)->get();
                if (collect($users)->count() == 0) {
                    // #5 & #6
                    $users = User::whereNotNull('profile_picture')
                        ->whereNotIn('id', [1109])
                        ->whereIn('nationality', [$authUser->nationality])
                        ->when(count($otherUser), function ($q) use ($otherUser) {
                            $q->orWhereIn('nationality', $otherUser);
                        })
                        ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                            $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                                $travelstylesSubQuery->orWhereIn('conf_lifestyles_id', $myTravelStyles);
                            });
                        })
                        ->orderBy('created_at', 'DESC')->take(50)->get();
                }
            }

            $users = collect($users)->map(function ($user) use ($authUser) {
                $user->profile_picture = check_profile_picture($user->profile_picture);
                $user->follow_flag      = (UsersFollowers::where('followers_id', $authUser->id)->where('users_id', $user->id)->where('follow_type', 1)->first())  ? true : false;
                $user->total_follower   = $user->get_followers->count();
                $user->latest_followers = $user->get_followers()->whereHas('follower')->take(3)->get()->map(function ($follower) {
                    return [
                        'id' => $follower->followers_id,
                        'name' => $follower->follower->name,
                    ];
                })->toArray();
                unset($user->get_followers);
                return $user;
            });

            $users = collect($users);
            if ($users->count()) {
                $responsePost[] =  ['page_id'   => null, 'unique_link' => null, 'action' => null, 'type' => PrimaryPostService::SECONDARY_DISCOVER_NEW_TRAVELLERS, 'data' => $users];
            }
        }
    }

    public function placesYouMightLike()
    {
        $places = null;
        $authUser = auth()->user();
        if (DUMMY_DATA_MODE) {
            $placesIds      = Place::take(50)->orderBy('id', 'DESC')->pluck('id');
            $places         = Place::whereIn('id', $placesIds)->paginate($this->per_page);
        } else {
            $myTravelStyles  = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();
            $otherUsersOfSameNationality  = User::where('nationality', $authUser->nationality)->whereNotIn('id', [$authUser->id])->pluck('id')->toArray();

            if ($authUser->nationality) {
                if (count($otherUsersOfSameNationality) > 0) {
                    $placeFollowers = PlaceFollowers::select(
                        'places_id',
                        DB::raw('COUNT(users_id) AS total_follower')
                    )
                        ->whereIn('users_id', $otherUsersOfSameNationality)
                        ->whereNotIn('places_id', $authUser->followedPlaces->pluck('places_id')->toArray())
                        ->groupBy('places_id')
                        ->havingRaw('(select COUNT(users.id) from users where nationality="' . $authUser->nationality . '" and users.id!= "' . $authUser->id . '") >= 10')
                        ->orderBy('total_follower', 'DESC');

                    if (count($myTravelStyles) > 0) {
                        $placeFollowers->whereHas('user.travelstyles', function ($travelstyles) use ($myTravelStyles) {
                            $travelstyles->whereIn('conf_lifestyles_id', $myTravelStyles);
                        });
                    } else {
                        $placeFollowers->whereHas('place', function ($place) {
                            $place->withCount('followers')->orderBy('followers_count', 'DESC');
                        });
                    }
                    $places = $placeFollowers->paginate($this->per_page);
                }
            }
        }

        if ($places != null) {
            $places = modifyPagination($places);
            if (!($places->total > 3)) {
                return NULL;
            }

            $places->data =  $places->data->map(function ($place) {
                return [
                    'id'                => $place->id,
                    'title'             => @$place->transsingle->title,
                    'description'       => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                    'img'               => check_place_photo(@$place),
                    'total_followers'   => count($place->followers),
                    'place_type'        => do_placetype(@$place->place_type ?: 'Event'),
                    'city'              => @$place->city->transsingle->title,
                    'latest_followers'  => $place->followers->take(3)->map(function ($follower) {
                        return [
                            'id'                => $follower->user->id,
                            'name'              => $follower->user->name,
                            'profile_picture'   => check_profile_picture($follower->user->profile_picture),
                        ];
                    })->toArray()
                ];
            })->toArray();
            return $places;
        }
        return null;
    }

    public function collectiveFollowersPlaces(&$responsePost)
    {
        if (DUMMY_DATA_MODE) {
            $authUser   = auth()->user();
            $followersArr = array_merge(
                $authUser->friends->pluck('friends_id')->toArray(),
                $authUser->followings->pluck('users_id')->toArray()
            );

            $followers      = User::whereIn('id', $followersArr)->get();
            $countriesds    = Countries::whereHas('followers')->withCount('followers')->having('followers_count', '>', 2)->take(11)->pluck('id');
            $citiesIds      = Cities::whereHas('followers')->withCount('followers')->having('followers_count', '>', 2)->take(11)->pluck('id');
            $placesIds      = Place::whereHas('followers')->withCount('followers')->having('followers_count', '>', 2)->take(11)->pluck('id');

            $places = Place::whereIn('id', $placesIds)->get()
                ->map(function ($place) use ($authUser) {

                    $recent_followers_arr = [];
                    if ($recent_followers = $place->followers->take(3)) {
                        foreach ($recent_followers as $followeObj) {
                            $recent_followers_arr[] = [
                                'id' => $followeObj->users_id,
                                'profile_picture' => check_profile_picture($followeObj->user->profile_picture),
                            ];
                        }
                    }

                    return [
                        'type'          => 'place',
                        'id'            => $place->id,
                        'title'         => @$place->transsingle->title,
                        'description'   => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                        'img'           => check_place_photo(@$place),
                        'total_followers' => count($place->followers),
                        'country_title' => @$place->country->transsingle->title,
                        'recent_followers' => $recent_followers_arr
                    ];
                })->toArray();
            $countries = Countries::whereIn('id', $countriesds)->get()
                ->map(function ($country) use ($authUser) {

                    $recent_followers_arr = [];
                    if ($recent_followers = $country->followers->take(3)) {
                        foreach ($recent_followers as $followeObj) {
                            $recent_followers_arr[] = [
                                'id' => $followeObj->users_id,
                                'profile_picture' => check_profile_picture($followeObj->user->profile_picture),
                            ];
                        }
                    }

                    return [
                        'type'          => 'country',
                        'id'            => $country->id,
                        'title'         => @$country->transsingle->title,
                        'description'   => !empty(@$country->transsingle->address) ? @$country->transsingle->address : '',
                        'img'           => check_country_photo(@$country->getMedias[0]->url, 180),
                        'total_followers' => count($country->followers),
                        'recent_followers' => $recent_followers_arr
                    ];
                })->toArray();
            $cities = Cities::whereIn('id', $citiesIds)->get()
                ->map(function ($city) use ($authUser) {

                    $recent_followers_arr = [];
                    if ($recent_followers = $city->followers->take(3)) {
                        foreach ($recent_followers as $followeObj) {
                            $recent_followers_arr[] = [
                                'id' => $followeObj->users_id,
                                'profile_picture' => check_profile_picture($followeObj->user->profile_picture),
                            ];
                        }
                    }

                    return [
                        'type'          => 'city',
                        'id'            => $city->id,
                        'title'         => @$city->transsingle->title,
                        'description'   => !empty(@$city->transsingle->address) ? @$city->transsingle->address : '',
                        'img'           => check_city_photo(@$city->getMedias[0]->url),
                        'total_followers' => count($city->followers),
                        'country_title' => @$city->country->transsingle->title,
                        'recent_followers' => $recent_followers_arr
                    ];
                })->toArray();
            $latest_followers = [];
            if ($followers->take(5)) {
                foreach ($followers->take(5) as $follower) {
                    $latest_followers[] = [
                        'name'              => $follower->name,
                        'profile_picture'   => check_profile_picture($follower->profile_picture)
                    ];
                }
            }
            unset($authUser->followedCities, $authUser->followedCountries, $authUser->followedPlaces);

            $responsePost[] =  [
                'page_id'   => null, 'unique_link' => null, 'action' => null,
                'type' => PrimaryPostService::SECONDARY_COLLECTIVE_FOLLOWERS_PLACES, 'data' => [
                    'total_followers'       => count($followers),
                    'latest_followers'      => $latest_followers,
                    'data'                  => collect(array_merge($places, $countries, $cities))
                ]
            ];
        } else {
            $authUser   = auth()->user();
            $result     = collect([]);
            $checkUsers = collect([]);
            $lastDays   = 120;

            $userProfileService = app(UserProfileService::class);
            $friends    = $userProfileService->getFriendIds($authUser->id)->toArray();
            $followers  = $userProfileService->getFollowerIds($authUser->id)->toArray();
            $findFollowers  = User::whereIn('id', $followers)->take(5)->get();
            $otherUsers = collect($followers)->merge($friends)->unique()->toArray();
            PlaceFollowers::query()
                ->whereIn('users_id', $otherUsers)
                ->whereNotIn('users_id', [$authUser->id])
                ->whereNotIn('places_id', $authUser->followedPlaces->pluck('places_id')->toArray())
                ->groupBy('places_id')
                ->where('created_at', ">=", Carbon::now()->subDays($lastDays))
                ->take(30)
                ->orderBy('created_at', 'DESC')
                ->get()->each(function ($placeFollowers) use ($result, $checkUsers, $authUser) {
                    if ($place = $placeFollowers->place) {
                        $checkUsers->push(['users_id' => $placeFollowers->users_id]);

                        $recent_followers_arr = [];
                        if ($recent_followers = $place->followers->take(3)) {
                            foreach ($recent_followers as $followeObj) {
                                $recent_followers_arr[] = [
                                    'id' => $followeObj->users_id,
                                    'profile_picture' => check_profile_picture($followeObj->user->profile_picture),
                                ];
                            }
                        }

                        $result->push([
                            'type'          => 'place',
                            'id'            => $place->id,
                            'title'         => @$place->transsingle->title,
                            'description'   => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                            'img'           => check_place_photo(@$place),
                            'country_title' => @$place->country->transsingle->title,
                            'total_followers'   => count($place->followers),
                            'recent_followers' => $recent_followers_arr
                        ]);
                    }
                });
            CountriesFollowers::query()
                ->whereIn('users_id', $otherUsers)
                ->whereNotIn('users_id', [$authUser->id])
                ->whereNotIn('countries_id', $authUser->followedCountries->pluck('countries_id')->toArray())
                ->groupBy('countries_id')
                ->where('created_at', ">=", Carbon::now()->subDays($lastDays))
                ->take(30)
                ->orderBy('created_at', 'DESC')
                ->get()->each(function ($countriesFollower) use ($result, $checkUsers, $authUser) {
                    if ($country = $countriesFollower->country) {
                        $checkUsers->push(['users_id' => $countriesFollower->users_id]);

                        $recent_followers_arr = [];
                        if ($recent_followers = $country->followers->take(3)) {
                            foreach ($recent_followers as $followeObj) {
                                $recent_followers_arr[] = [
                                    'id' => $followeObj->users_id,
                                    'profile_picture' => check_profile_picture($followeObj->user->profile_picture),
                                ];
                            }
                        }

                        $result->push([
                            'type'          => 'country',
                            'id'            => $country->id,
                            'title'         => @$country->transsingle->title,
                            'description'   => !empty(@$country->transsingle->address) ? @$country->transsingle->address : '',
                            'img'           => check_country_photo(@$country->getMedias[0]->url, 180),
                            'total_followers'   => count($country->followers),
                            'recent_followers' => $recent_followers_arr
                        ]);
                    }
                });
            CitiesFollowers::query()
                ->whereIn('users_id', $otherUsers)
                ->whereNotIn('users_id', [$authUser->id])
                ->whereNotIn('cities_id', $authUser->followedCities->pluck('cities_id')->toArray())
                ->groupBy('cities_id')
                ->where('created_at', ">=", Carbon::now()->subDays($lastDays))
                ->take(30)
                ->orderBy('created_at', 'DESC')
                ->get()->each(function ($citiesFollower) use ($result, $checkUsers, $authUser) {
                    if ($city = $citiesFollower->city) {
                        $checkUsers->push(['users_id' => $citiesFollower->users_id]);

                        $recent_followers_arr = [];
                        if ($recent_followers = $city->followers->take(3)) {
                            foreach ($recent_followers as $followeObj) {
                                $recent_followers_arr[] = [
                                    'id' => $followeObj->users_id,
                                    'profile_picture' => check_profile_picture($followeObj->user->profile_picture),
                                ];
                            }
                        }

                        $result->push([
                            'type'          => 'city',
                            'id'            => $city->id,
                            'title'         => @$city->transsingle->title,
                            'description'   => !empty(@$city->transsingle->address) ? @$city->transsingle->address : '',
                            'img'           => check_city_photo(@$city->getMedias[0]->url),
                            'country_title' => @$city->country->transsingle->title,
                            'total_followers'   => count($city->followers),
                            'recent_followers' => $recent_followers_arr
                        ]);
                    }
                });

            if (collect($checkUsers)->unique()->pluck('users_id')->count() >= 3) {

                $latest_followers = [];
                if ($findFollowers) {
                    foreach ($findFollowers as $follower) {
                        $latest_followers[] = [
                            'name'              => $follower->name,
                            'profile_picture'   => check_profile_picture($follower->profile_picture)
                        ];
                    }
                }

                $responsePost[] =  [
                    'page_id'   => null, 'unique_link' => null, 'action' => null,
                    'type' => PrimaryPostService::SECONDARY_COLLECTIVE_FOLLOWERS_PLACES, 'data' => [
                        'total_followers'       => count($followers),
                        'latest_followers'      => $latest_followers,
                        'data'                  => collect($result)->toArray()
                    ]
                ];
            }
        }
    }

    public function recommendedPlaces(&$responsePost)
    {
        if (DUMMY_DATA_MODE) {
            $authUser   = auth()->user();
            $countriesds    = Countries::take(10)->pluck('id');
            $citiesIds      = Cities::take(10)->pluck('id');
            $result = collect(array_merge(
                Countries::whereIn('id', $countriesds)->get()->map(function ($country) {
                    return collect([
                        'type'          => 'country',
                        'id'            => $country->id,
                        'title'         => @$country->transsingle->title,
                        'description'   => !empty(@$country->transsingle->address) ? @$country->transsingle->address : '',
                        'img'           => check_country_photo(@$country->getMedias[0]->url, 180),
                        'followers'     => count($country->followers),
                    ]);
                })->toArray(),
                Cities::whereIn('id', $citiesIds)->get()->map(function ($city) {
                    return collect([
                        'type'          => 'city',
                        'id'            => $city->id,
                        'title'         => @$city->transsingle->title,
                        'description'   => !empty(@$city->transsingle->address) ? @$city->transsingle->address : '',
                        'img'           => check_city_photo(@$city->getMedias[0]->url, 180),
                        'followers'     => count($city->followers),
                        'country_title' => @$city->country->transsingle->title,
                    ]);
                })->toArray()
            ));
            if (count($result)) {
                $responsePost[] = ['page_id'   => null, 'unique_link' => null, 'action' => null, 'type' => PrimaryPostService::SECONDARY_RECOMMENDED_PLACES,  'data' => $result];
            }
        } else {
            $authUser   = auth()->user();
            $myTravelStyles  = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();
            if ($authUser->nationality) {
                $list = collect([]);
                $countriesFollowers = CountriesFollowers::query()
                    ->select(
                        'countries_id',
                        DB::raw('COUNT(users_id) AS total_follower'),
                        DB::raw('(select count(*) from countries_followers as cf where cf.countries_id = countries_followers.countries_id and cf.users_id not in(' . $authUser->id . ') and cf.users_id in( select id from users where users.nationality in (' . $authUser->nationality . ') ) ) AS county_follower')
                    )
                    ->groupBy('countries_id')
                    ->orderBy('total_follower', 'DESC')
                    ->whereHas('user', function ($countryQ) use ($authUser) {
                        $countryQ->where('nationality', $authUser->nationality)
                            ->whereNotIn('id', [$authUser->id]);
                    })
                    ->having('county_follower', '>=', 10)
                    ->take(50);


                $citiesFollowers    = CitiesFollowers::query()
                    ->select(
                        'cities_id',
                        DB::raw('COUNT(users_id) AS total_follower'),
                        DB::raw('(select count(*) from cities_followers as cf where cf.cities_id = cities_followers.cities_id and cf.users_id not in(' . $authUser->id . ') and cf.users_id in( select id from users where users.nationality in (' . $authUser->nationality . ') ) ) AS city_follower')
                    )
                    ->groupBy('cities_id')
                    ->orderBy('total_follower', 'DESC')
                    ->whereHas('user', function ($cityQ) use ($authUser) {
                        $cityQ->where('nationality', $authUser->nationality)
                            ->whereNotIn('id', [$authUser->id]);
                    })
                    ->having('city_follower', '>=', 10)
                    ->take(50);

                if (count($myTravelStyles) > 0) {
                    $countriesFollowers->whereHas('user.travelstyles', function ($travelstyles) use ($myTravelStyles) {
                        $travelstyles->whereIn('conf_lifestyles_id', $myTravelStyles);
                    });
                    $citiesFollowers->whereHas('user.travelstyles', function ($travelstyles) use ($myTravelStyles) {
                        $travelstyles->whereIn('conf_lifestyles_id', $myTravelStyles);
                    });
                } else {
                    $countriesFollowers->whereHas('country', function ($country) {
                        $country->withCount('followers')->orderBy('followers_count', 'DESC');
                    });
                    $citiesFollowers->whereHas('city', function ($city) {
                        $city->withCount('followers')->orderBy('followers_count', 'DESC');
                    });
                }

                $countriesFollowers = $countriesFollowers->whereNotIn('countries_id', $authUser->followingCountries->pluck('countries_id')->toArray())->get();
                $citiesFollowers    = $citiesFollowers->whereNotIn('cities_id', $authUser->followingCities->pluck('cities_id')->toArray())->get();
                foreach ([$countriesFollowers, $citiesFollowers] as $objectFollower) {
                    if ($objectFollower) {
                        $objectFollower->each(function ($itemFollower) use (&$list, $authUser) {
                            if (get_class($itemFollower) == CountriesFollowers::class) {
                                $list->push([
                                    'type'          => 'country',
                                    'id'            => $itemFollower->countries_id,
                                    'title'         => @$itemFollower->country->transsingle->title,
                                    'description'   => !empty(@$itemFollower->country->transsingle->address) ? @$itemFollower->country->transsingle->address : '',
                                    'img'           => check_country_photo(@$itemFollower->country->getMedias[0]->url, 180),
                                    'followers'     => count($itemFollower->country->followers),
                                ]);
                            } else {
                                $list->push([
                                    'type'          => 'city',
                                    'id'            => $itemFollower->cities_id,
                                    'title'         => @$itemFollower->city->transsingle->title,
                                    'description'   => !empty(@$itemFollower->city->transsingle->address) ? @$itemFollower->city->transsingle->address : '',
                                    'img'           => check_country_photo(@$itemFollower->city->getMedias[0]->url, 180),
                                    'followers'     => count($itemFollower->city->followers),
                                    'country_title' => @$itemFollower->city->country->transsingle->title,
                                ]);
                            }
                        });
                    }
                }

                $list->sortByDesc('followers')->values()->take(50)->toArray();
                if (count($list)) {
                    $responsePost[] = ['page_id'   => null, 'unique_link' => null, 'action' => null, 'type' => PrimaryPostService::SECONDARY_RECOMMENDED_PLACES, 'data' => $list];
                }
            }
        }
    }

    public function newFollower(&$responsePost)
    {
        if (DUMMY_DATA_MODE) {
            $authUser = Auth::user();
            $users = User::select(
                'id',
                'name',
                'username',
                'email',
                'nationality',
                'gender',
                'about',
                'cover_photo',
                'profile_picture',
                'birth_date',
                'age',
                'created_at',
                'is_approved',
                DB::raw('(SELECT count(id) FROM medias where id in ( SELECT medias_id FROM `users_medias` WHERE users_id=users.id)) as avg_count')
            )
                ->whereIn('id', $authUser->get_followers->pluck('followers_id')->unique()->toArray())
                ->whereNotIn('id', $authUser->following_user->pluck('users_id')->unique()->toArray())
                ->whereHas('my_medias')
                ->orderBy('created_at', 'DESC')
                // ->having('avg_count', '>', 1)
                ->take(50)->get();
            foreach ($users as $user) {
                $user->country = @$user->country_of_nationality->transsingle->title ?? null;
                $user->profile_picture = check_profile_picture($user->profile_picture);
                $user->cover_photo = check_cover_photo($user->cover_photo);
                $medias = [];
                $i = 0;
                if ($user->my_medias) {
                    foreach ($user->my_medias->take(4) as $mymedia) {
                        if ($mymedia->media) {
                            $i++;
                            if ($i == 4) break;
                            $medias[] = check_media_url($mymedia->media->url);
                        }
                    }
                }
                $user->medias = $medias;
                unset($user->country_of_nationality, $user->my_medias);
            }
            $header = collect($users)->map(function ($user) {
                return [
                    'id' => $user->id,
                    'username' => $user->username,
                    'profile_picture' => $user->profile_picture,
                ];
            });
            if (collect($users)->count() >= 1) {
                $responsePost[] = [
                    'page_id'   => null, 'unique_link' => null, 'action' => null,
                    'type' => PrimaryPostService::SECONDARY_NEW_FOLLOWER, 'data' => [
                        'friend' => $header[0],
                        'suggestions'  => $users,
                    ]
                ];
            }
        } else {
            $authUser = Auth::user();
            $users = User::select(
                'id',
                'name',
                'username',
                'email',
                'nationality',
                'gender',
                'about',
                'cover_photo',
                'profile_picture',
                'birth_date',
                'age',
                'created_at',
                'is_approved',
                DB::raw('(SELECT count(id) FROM medias where id in ( SELECT medias_id FROM `users_medias` WHERE users_id=users.id)) as avg_count')
            )
                ->whereIn('id', $authUser->get_followers->pluck('followers_id')->unique()->toArray())
                ->whereNotIn('id', $authUser->following_user->pluck('users_id')->unique()->toArray())
                ->whereHas('my_medias')
                ->where('created_at', ">=", Carbon::now()->subDays(30))
                ->orderBy('created_at', 'DESC')
                ->having('avg_count', '>', 4)
                ->take(50)->get();
            foreach ($users as $user) {
                $user->country = @$user->country_of_nationality->transsingle->title ?? null;
                $user->profile_picture = check_profile_picture($user->profile_picture);
                $user->cover_photo = check_cover_photo($user->cover_photo);
                $medias = [];
                $i = 0;
                if ($user->my_medias) {
                    foreach ($user->my_medias->take(4) as $mymedia) {
                        if ($mymedia->media) {
                            $i++;
                            if ($i == 4) break;
                            $medias[] = check_media_url($mymedia->media->url);
                        }
                    }
                }
                $user->medias = $medias;
                unset($user->country_of_nationality, $user->my_medias);
            }
            $header = collect($users)->map(function ($user) {
                return [
                    'id' => $user->id,
                    'username' => $user->username,
                    'profile_picture' => $user->profile_picture,
                ];
            });
            if (collect($users)->count() > 3) {
                $responsePost[] = [
                    'page_id'   => null, 'unique_link' => null, 'action' => null,
                    'type' => PrimaryPostService::SECONDARY_NEW_FOLLOWER, 'data' => [
                        'friend' => $header[rand(0, 2)],
                        'suggestions'  => $users,
                    ]
                ];
            }
        }
    }

    // weather secondary post
    public function weatherUpdateForCheckins(&$responsePost)
    {
        if (DUMMY_DATA_MODE) {
            $authUser = auth()->user();
            $my_checkins = Checkins::where('users_id', $authUser->id)
                ->where('checkin_time', '<=', Carbon::now()->addDay(2))
                ->where('checkin_time', '>=', Carbon::now()->addDay(1))
                ->first();

            if (isset($my_checkins->place) && isset($my_checkins)) {
                $resp = weatherApi(1, [
                    'lat' =>  $my_checkins->place->lat,
                    'lng' =>  $my_checkins->place->lng
                ]);
                if ($resp && $resp != "null") {
                    $resp = json_decode($resp);
                    $daily_weather = json_decode(weatherApi(3, [
                        'place_key' => $resp->Key,
                        'day' => '10day'
                    ]));

                    if (isset($daily_weather) && (get_class($daily_weather) == stdClass::class) && isset($daily_weather->DailyForecasts)) {
                        $days = [];
                        $day = 0;
                        foreach ($daily_weather->DailyForecasts as $dayData) {
                            if ($day == 7) continue;
                            $days[] = $dayData;
                            $day++;
                        }
                        $daily_weather->DailyForecasts  = $days;
                        $my_checkins->place->title = $my_checkins->place->transsingle->title ?? null;
                        $my_checkins->place->description = $my_checkins->place->transsingle->description ?? null;
                        $my_checkins->place->place_type = do_placetype($my_checkins->place->place_type);
                        $my_checkins->place->image = @check_place_photo($my_checkins->place);
                        $my_checkins->total_likes = $my_checkins->likes->count();
                        $my_checkins->like_flag = ($my_checkins->likes->where('users_id', $authUser->id)->first()) ? true : false;
                        $my_checkins->total_comments = $my_checkins->postComments->where('parents_id', 0)->count();
                        $latest_commenter = [];
                        foreach ($my_checkins->postComments->take(3) as $comment) {
                            if (isset($comment->author))
                                $latest_commenter[] = check_profile_picture($comment->author->profile_picture);
                        }
                        $my_checkins->latest_commenter = $latest_commenter;
                        unset($my_checkins->postComments, $my_checkins->likes, $my_checkins->place->transsingle, $my_checkins->place->medias);

                        $responsePost[]     =  [
                            'page_id'   => null, 'unique_link' => null, 'action' => null,
                            'type'      => PrimaryPostService::SECONDARY_CHECKIN, 'data'      => [
                                'checkin' => $my_checkins,
                                'weather' => $daily_weather,
                            ]
                        ];
                    }
                }
            }
        } else {
            $authUser = auth()->user();
            $my_checkins = Checkins::where('users_id', $authUser->id)
                ->where('checkin_time', '<=', Carbon::now()->addDay(2))
                ->where('checkin_time', '>=', Carbon::now()->addDay(1))
                ->first();

            if (isset($my_checkins->place) && isset($my_checkins)) {
                $resp = weatherApi(1, [
                    'lat' =>  $my_checkins->place->lat,
                    'lng' =>  $my_checkins->place->lng
                ]);
                if ($resp && $resp != "null") {
                    $resp = json_decode($resp);
                    $daily_weather = json_decode(weatherApi(3, [
                        'place_key' => $resp->Key,
                        'day' => '10day'
                    ]));

                    if (get_class($daily_weather) == stdClass::class) {
                        $days = [];
                        $day = 0;
                        foreach ($daily_weather->DailyForecasts as $dayData) {
                            if ($day == 7) continue;
                            $days[] = $dayData;
                            $day++;
                        }
                        $daily_weather->DailyForecasts  = $days;
                        $my_checkins->place->title = $my_checkins->place->transsingle->title ?? null;
                        $my_checkins->place->description = $my_checkins->place->transsingle->description ?? null;
                        $my_checkins->place->place_type = do_placetype($my_checkins->place->place_type);
                        $my_checkins->place->image = @check_place_photo($my_checkins->place);
                        $my_checkins->total_likes = $my_checkins->likes->count();
                        $my_checkins->like_flag = ($my_checkins->likes->where('users_id', $authUser->id)->first()) ? true : false;
                        $my_checkins->total_comments = $my_checkins->postComments->where('parents_id', 0)->count();
                        $latest_commenter = [];
                        foreach ($my_checkins->postComments->take(3) as $comment) {
                            if (isset($comment->author))
                                $latest_commenter[] = check_profile_picture($comment->author->profile_picture);
                        }
                        $my_checkins->latest_commenter = $latest_commenter;
                        unset($my_checkins->postComments, $my_checkins->likes, $my_checkins->place->transsingle, $my_checkins->place->medias);
                        $responsePost[]     =  [
                            'page_id'   => null, 'unique_link' => null, 'action' => null,
                            'type'      => PrimaryPostService::SECONDARY_CHECKIN, 'data'      => [
                                'checkin' => $my_checkins,
                                'weather' => $daily_weather,
                            ]
                        ];
                    }
                }
            }
        }
    }

    public function weatherUpdateForMyTrips(&$responsePost)
    {
        if (DUMMY_DATA_MODE) {
            $authUser = auth()->user();
            $findTripPlace = TripPlaces::whereHas('trip', function ($q) use ($authUser) {
                $q->where('users_id', $authUser->id);
                $q->where('memory', 0);
            })
                ->where('versions_id', '!=', 0)
                ->where('date', '<=', Carbon::now()->addDay(2))
                ->where('date', '>=', Carbon::now())
                ->orderBy('id', 'DESC')->first();

            if (isset($findTripPlace) && isset($findTripPlace->city) && isset($findTripPlace->trip->places)) {

                $trip = TripPlans::find($findTripPlace->trip->id);
                if ($trip) {
                    $totalDates = TripPlaces::where('trips_id', $findTripPlace->trip->id)
                        ->select('date')
                        ->where('versions_id', '!=', 0)
                        ->groupBy('date')->get('date')->sortBy('date')->toArray();
                    $currentDay = 0;
                    foreach ($totalDates as $day => $date) {
                        if (Carbon::parse($date['date'])->eq(Carbon::now())) {
                            $currentDay = $day + 1;
                        }
                    }
                    $total_days = count($totalDates);
                    $cityies = [];
                    $citiesIds =  TripPlaces::where('trips_id', $findTripPlace->trip->id)
                        ->where('date', $findTripPlace->date)
                        ->where('versions_id', '!=', 0)
                        ->groupBy('cities_id')->pluck('cities_id')->toArray();
                    if ($total_days && count($citiesIds)) {
                        foreach ($citiesIds as $citiesId) {
                            $city = Cities::find($citiesId);
                            if ($city) {
                                $resp = weatherApi(1, [
                                    'lat' => $city->lat,
                                    'lng' => $city->lng
                                ]);
                                if ($resp && $resp != "null") {
                                    $resp = json_decode($resp);
                                    $weatherResp = weatherApi(3, [
                                        'place_key' => $resp->Key,
                                        'day'       => '1day'
                                    ]);
                                    $get_current_weather = ($weatherResp && $weatherResp != "null") ? json_decode($weatherResp) : null;
                                    $citiesTotalDates = TripPlaces::where('trips_id', $findTripPlace->trip->id)
                                        ->where('cities_id', $city->id)
                                        ->select('date')
                                        ->whereHas('active_trip')
                                        ->where('versions_id', '!=', 0)
                                        ->groupBy('date')
                                        ->get()->sortBy('date')->toArray();

                                    $city->title = $city->transsingle->title;
                                    $city->description = $city->transsingle->description;
                                    $city->image = check_city_photo(@$city->getMedias[0]->url);
                                    $city->start_date = $findTripPlace->date;
                                    $city->end_date = isset($citiesTotalDates[count($citiesTotalDates) - 1]['date']) ? $citiesTotalDates[count($citiesTotalDates) - 1]['date'] : $findTripPlace->date;
                                    $city->weather = $get_current_weather;
                                    unset($city->transsingle, $city->getMedias);
                                    $cityies[] = $city;
                                }
                            }
                        }
                        unset($findTripPlace->trip->places);
                        $responsePost[] =  [
                            'page_id'   => null, 'unique_link' => null,
                            'type'      => 'trip', 'action'  => PrimaryPostService::SECONDARY_WEATHER_FORECAST, 'data'      => [
                                'trip'          => $findTripPlace->trip,
                                'total_days'    => $total_days,
                                'current_day'   => $currentDay,
                                'total_cities'  => count($cityies),
                                'cities'        => $cityies
                            ]
                        ];
                    }
                }
            }
        } else {
            $authUser = auth()->user();
            $findTripPlace = TripPlaces::whereHas('trip', function ($q) use ($authUser) {
                $q->where('users_id', $authUser->id);
                $q->where('memory', 0);
            })->where('versions_id', '!=', 0)
                ->where('date', '<=', Carbon::now()->addDay(2))
                ->where('date', '>=', Carbon::now())
                ->orderBy('id', 'DESC')->first();

            if (isset($findTripPlace) && isset($findTripPlace->city) && isset($findTripPlace->trip->places)) {
                $trip = TripPlans::find($findTripPlace->trip->id);
                if ($trip) {
                    $totalDates = TripPlaces::where('trips_id', $findTripPlace->trip->id)
                        ->select('date')
                        ->where('versions_id', '!=', 0)
                        ->groupBy('date')->get('date')->sortBy('date')->toArray();
                    $currentDay = 0;
                    foreach ($totalDates as $day => $date) {
                        if (Carbon::parse($date['date'])->eq(Carbon::now())) {
                            $currentDay = $day + 1;
                        }
                    }
                    $total_days = count($totalDates);
                    $cityies = [];
                    $citiesIds =  TripPlaces::where('trips_id', $findTripPlace->trip->id)
                        ->where('date', $findTripPlace->date)
                        ->where('versions_id', '!=', 0)
                        ->groupBy('cities_id')->pluck('cities_id')->toArray();
                    if ($total_days && count($citiesIds)) {
                        foreach ($citiesIds as $citiesId) {
                            $city = Cities::find($citiesId);
                            if ($city) {
                                $resp = weatherApi(1, [
                                    'lat' => $city->lat,
                                    'lng' => $city->lng
                                ]);
                                if ($resp && $resp != "null") {
                                    $resp = json_decode($resp);
                                    $weatherResp = weatherApi(3, [
                                        'place_key' => $resp->Key,
                                        'day'       => '1day'
                                    ]);
                                    $get_current_weather = ($weatherResp && $weatherResp != "null") ? json_decode($weatherResp) : null;
                                    $citiesTotalDates = TripPlaces::where('trips_id', $findTripPlace->trip->id)
                                        ->where('cities_id', $city->id)
                                        ->select('date')
                                        ->whereHas('active_trip')
                                        ->where('versions_id', '!=', 0)
                                        ->groupBy('date')
                                        ->get()->sortBy('date')->toArray();

                                    $city->title = $city->transsingle->title;
                                    $city->description = $city->transsingle->description;
                                    $city->image = check_city_photo(@$city->getMedias[0]->url);
                                    $city->start_date = $findTripPlace->date;
                                    $city->end_date = isset($citiesTotalDates[count($citiesTotalDates) - 1]['date']) ? $citiesTotalDates[count($citiesTotalDates) - 1]['date'] : $findTripPlace->date;
                                    $city->weather = $get_current_weather;
                                    unset($city->transsingle, $city->getMedias);
                                    $cityies[] = $city;
                                }
                            }
                        }
                        unset($findTripPlace->trip->places);
                        $responsePost[] =  [
                            'page_id'   => null, 'unique_link' => null,
                            'type'      => 'trip',
                            'action'    => PrimaryPostService::SECONDARY_WEATHER_FORECAST,
                            'data'      => [
                                'trip'          => $findTripPlace->trip,
                                'total_days'    => $total_days,
                                'current_day'   => $currentDay,
                                'total_cities'  => count($cityies),
                                'cities'        => $cityies
                            ]
                        ];
                    }
                }
            }
        }
    }

    public function getNewPeople()
    {
        $userId = Auth::user()->id;
        $users =  User::select('id', 'name', 'username', 'email', 'contact_email', 'gender', 'birth_date', 'profile_picture', 'expert', 'public_profile', 'created_at', 'updated_at', 'deleted_at', 'travel_type', 'display_name', 'cover_photo', 'interests', 'about')
            ->where('register_steps', 'complated')
            ->where('created_at', ">=", Carbon::now()->subDays(90))
            ->orderBy('id', 'desc')
            ->paginate(6);

        foreach ($users as $user) {
            $user->country          = ($user->country_of_nationality) ? _fieldValueFromTrans($user->country_of_nationality, 'title') : null;
            $user->profile_picture  = check_profile_picture($user->profile_picture);
            $user->cover_photo      = check_cover_photo($user->cover_photo);
            $user->follow_status    = (UsersFollowers::where('users_id', $user->id)->where('followers_id', $userId)->first()) ? true : false;
            unset($user->country_of_nationality);
        }
        if (count($users)) {
            $finalUsers = modifyPagination($users);
            if ($finalUsers->total > 3) {
                return $finalUsers;
            }
        }
        return null;
    }
}
