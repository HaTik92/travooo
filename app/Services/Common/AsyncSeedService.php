<?php

namespace App\Services\Common;

use App\Http\Constants\CommonConst;
use App\Http\Controllers\Api\AutoGenerateController;
use App\Http\Controllers\Api\CommonApiController;
use App\Http\Controllers\Api\PlaceController2;
use App\Http\Controllers\Api\PostController;
use App\Http\Requests\Api\Post\CreatePostRequest;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Place\Place;
use App\Models\Place\PlaceMedias;
use App\Models\Reviews\Reviews;
use App\Models\User\User;
use App\Http\Requests\Api\Place2\ReviewRequest;
use App\Services\Users\ImageModerateService;
use Faker\Factory as Faker;
use Illuminate\Http\UploadedFile;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use stdClass;
use  App\Http\Controllers\Api\Traits\PlaceLocationTrait;
use App\Models\ActivityMedia\Media;
use App\Models\City\Cities;
use App\Models\City\CitiesMedias;
use App\Models\Country\Countries;
use App\Models\Country\CountriesMedias;
use App\Models\Posts\Checkins;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsCheckins;
use App\Models\Posts\PostsMedia;
use App\Models\Posts\PostsTags;
use App\Models\User\UsersMedias;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Api\PrimaryPostService;
use App\Services\PlaceChat\PlaceChatsService;
use App\Services\Translations\TranslationService;
use App\Services\Trips\PlanActivityLogService;
use App\Services\Trips\TripsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

// please don't change anything in this 
class AsyncSeedService
{
    const TYPE_POST         = "post";   // General post (text , media , check-in) +  External or internal links(others) + share
    const TYPE_TRIP         = "trip";
    // const TYPE_EVENT        = "event";
    // const TYPE_REPORT       = "report"; // Travlog
    // const TYPE_DISCUSSION   = "discussion";
    // const TYPE_REVIEW       = "review"; // Place


    const TYPE_MAPPING = [
        self::TYPE_POST,
        self::TYPE_TRIP,
        // self::TYPE_EVENT,
        // self::TYPE_REPORT,
        // self::TYPE_DISCUSSION,
        // self::TYPE_REVIEW,
    ];


    private $faker;
    private $pair;
    private $primaryPostService;
    private $placeChatsService, $tripsSuggestionsService, $tripsService, $planActivityLogService;

    public function __construct(
        PrimaryPostService $primaryPostService,
        PlaceChatsService $placeChatsService,
        TripsSuggestionsService $tripsSuggestionsService,
        TripsService $tripsService,
        PlanActivityLogService $planActivityLogService
    ) {
        $this->faker = Faker::create();
        $this->primaryPostService = $primaryPostService;

        $this->placeChatsService = $placeChatsService;
        $this->tripsSuggestionsService = $tripsSuggestionsService;
        $this->tripsService = $tripsService;
        $this->planActivityLogService = $planActivityLogService;
    }

    // please don't change anything in this 
    public function  post($times)
    {
        $baseCacheKey = "CACHE_" . last(explode("\\", get_class($this)));
        try { // optimize
            $locationKey = $baseCacheKey . "_locations";
            if (Cache::has($locationKey)) {
                $locationData = Cache::get($locationKey);
            } else {
                $locationData = $this->getDestinationTypes();
                Cache::put($locationKey, $locationData, 100);
            }
        } catch (\Throwable $th) { // Cache folder permission issue
            $locationData = $this->getDestinationTypes();
        }

        $me = auth()->user();
        $this->dumpMyFollowigsIfNotLessThen(20);
        $my_followigs = UsersFollowers::where('followers_id', $me->id)->where('follow_type', 1)->pluck('users_id')->toArray();
        $posts = [];
        if (count($my_followigs) > 0) {
            foreach (range(1, $times) as $time) {
                $destination = $locationData->random();
                $checkinParams = $this->getCheckinParams($destination);

                $isCheckin = rand(0, 1);
                $params = [];
                $request = new Request;
                Auth::loginUsingId(collect($my_followigs)->random());
                $this->pair = getPair();

                if (rand(0, 1)) { // from country city place page
                    $params['page'] = $checkinParams['type'];
                    $params['page_id'] = $checkinParams['id'];
                }

                if ($isCheckin && $times >= 4) { // checkin post
                    $params = array_merge($params, ['location' => $checkinParams]);
                    $params['checkin_date'] = Carbon::today()->subDays(rand(0, 20))->format(Carbon::DEFAULT_TO_STRING_FORMAT);
                }


                if (rand(0, 1)) {
                    if (rand(0, 1)) {
                        $this->randImgs(); // with images post or not
                    }
                }

                $params['text'] = $this->getUniqueText();
                $params['pair'] = $this->pair;
                $params['permission'] = 0;
                $request->merge($params);
                $post_id = $this->createPost($request);
                app(CommonApiController::class)->deleteTempFiles($this->pair);
                if (is_int($post_id)) {
                    $posts[] = $post_id;
                }
            }
        }
        return $posts;
    }


    // public function review($times)
    // {
    //     $me = auth()->user();
    //     $this->dumpMyFollowigsIfNotLessThen(20);
    //     $my_followigs = UsersFollowers::where('followers_id', $me->id)->where('follow_type', 1)->pluck('users_id')->toArray();
    //     $reviews = [];
    //     if (count($my_followigs)) {
    //         foreach (range(1, $times) as $time) {
    //             $_ = Reviews::query()
    //                 ->whereNotNull('places_id')
    //                 ->whereIn(
    //                     'id',
    //                     ActivityLog::query()->where([
    //                         'type' => 'place',
    //                         'action' => 'review'
    //                     ])->whereDate('time', '>', Carbon::now()->subDays(10))->pluck('variable')->toArray()
    //                 )->pluck('places_id')->toArray();

    //             $review = PlaceMedias::query()
    //                 ->whereNotIn('places_id', $_)
    //                 ->selectRaw('places_id, count(medias_id) as total_media')
    //                 ->orderBy('total_media', 'DESC')
    //                 ->first();

    //             if ($review) {
    //                 $places_id = $review->places_id;
    //                 // $userId = User::inRandomOrder()->first()->id;
    //                 // Auth::loginUsingId($userId);
    //                 Auth::loginUsingId(collect($my_followigs)->random());

    //                 $request = new ReviewRequest;
    //                 $request->merge([
    //                     'text'      => $this->faker->paragraph,
    //                     'place_id'  => $places_id,
    //                     'score'     => rand(1, 5),
    //                 ]);
    //                 $review_id = app(PlaceController2::class)->postReview($request, $places_id);
    //                 if (is_int($review_id)) {
    //                     $reviews[] = $review_id;
    //                 }
    //             }
    //         }
    //     }
    //     return $reviews;
    // }

    // please don't change anything in this 
    public function dumpLikes($type, $posts)
    {
        if (in_array($type, [self::TYPE_POST])) {
            foreach ($posts as $post_id) {
                $users = User::inRandomOrder()->limit(rand(3, 46))->pluck('id')->toArray();
                foreach ($users as $userId) {
                    Auth::loginUsingId($userId);
                    $request = new Request;
                    $this->primaryPostService->likeOrUnlike($type, $post_id, $request);
                }
            }
        }
    }

    // please don't change anything in this 
    public function trip()
    {
        $me = auth()->user();
        $this->dumpMyFollowigsIfNotLessThen(20);
        $my_followigs = UsersFollowers::where('followers_id', $me->id)->where('follow_type', 1)->pluck('users_id')->toArray();
        if (count($my_followigs)) {
            Auth::loginUsingId(collect($my_followigs)->random());
            $autoGenerateController = app(AutoGenerateController::class);
            return $autoGenerateController->tripGenerate(
                $this->placeChatsService,
                $this->tripsSuggestionsService,
                $this->tripsService,
                $this->planActivityLogService,
                "seed"
            );
        }
        return 'zero my followigs';
    }

    //  helpers
    public function getUniqueText()
    {
        $text  = implode(" ", $this->faker->words(collect([rand(0, 50), rand(50, 100), rand(25, 75), rand(0, 250)])->random()));
        $text =  collect(explode(" ", $text))->shuffle();
        $text = implode(" ", $text->toArray());
        return $text;
    }
    private function randImgs()
    {
        try {
            foreach (range(1, rand(1, 4)) as $i) {
                $image              = $this->faker->image();
                $imageName          = last(explode("\\", $image));
                $mineType           = 'image/' . last(explode(".", $imageName));
                $file               = new UploadedFile($image, $imageName, $mineType, null, null, true);
                $_base_path         = '/assets2/upload_tmp';
                $fileOriginalName   = $file->getClientOriginalName();
                $filename           = $this->pair . '_' . auth()->id() . '_' . $fileOriginalName;
                $path               = public_path($_base_path) . '/';
                @chmod($path, 0777);
                $file->move($path, $filename);
            }
        } catch (\Throwable $th) {
        }
    }

    private function getDestinationTypes()
    {
        $followed_places = auth()->user()->followedPlaces()->pluck('places_id');
        $followed_countries = auth()->user()->followedCountries()->pluck('countries_id');
        $followed_cities = auth()->user()->followedCities()->pluck('cities_id');
        return collect([
            ['type' => 'place', 'ids' => $followed_places->merge(Place::withCount('followers')->orderBy('followers_count', 'DESC')->take(rand(10, 15))->get()->pluck('id'))],
            ['type' => 'country', 'ids'  => $followed_countries->merge(Countries::withCount('followers')->orderBy('followers_count', 'DESC')->take(rand(10, 15))->get()->pluck('id'))],
            ['type' => 'city', 'ids'  => $followed_cities->merge(Cities::withCount('followers')->orderBy('followers_count', 'DESC')->take(rand(10, 15))->get()->pluck('id'))]
        ]);
    }

    private function getCheckinParams($destination)
    {
        $destinationType = $destination['type'];
        $destinationId = $destination['ids']->random();
        $destinationId = $destination['ids']->random();

        return [
            "type" => $destinationType,
            "id" => $destinationId,
            "title" => getDiscussionDestination($destinationType, $destinationId)
        ];
    }

    private function dumpMyFollowigsIfNotLessThen($no)
    {
        $me = auth()->user();
        $my_followigs = UsersFollowers::where('followers_id', $me->id)->where('follow_type', 1)->pluck('users_id')->toArray();
        if (count($my_followigs) <= $no) {
            $already = auth()->user()->get_followers;
            // $lst  = User::inRandomOrder()->whereDate('created_at', '>', Carbon::now()->subDays(500))->whereNotIn('id', $already->pluck('followers_id')->toArray())->take(20)->pluck('id');
            $weeklyTrendingUser = User::query()
                ->selectRaw('id, (
                                    (select count(*) from `posts` where users_id=`users`.`id` and date >  "' . Carbon::now()->subDays(7) . '") 
                                    + (select count(*) from `trips` where users_id=`users`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '") 
                                    + (select count(*) from `reports` where users_id=`users`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '") 
                                    + (select count(*) from `discussions` where users_id=`users`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '") 
                                ) as ranks')
                ->orderBy('ranks', 'desc')
                ->take(10)->get();
            $allTimeUsers = User::query()
                ->selectRaw('id, (
                                    (select count(*) from `posts` where users_id=`users`.`id`) 
                                    + (select count(*) from `trips` where users_id=`users`.`id`) 
                                    + (select count(*) from `reports` where users_id=`users`.`id`) 
                                    + (select count(*) from `discussions` where users_id=`users`.`id`) 
                                ) as ranks')
                ->orderBy('ranks', 'desc')
                ->take(10 + (10 - count($weeklyTrendingUser)))->get();
            $lst = $weeklyTrendingUser->merge($allTimeUsers)->pluck('id')->unique();

            collect($lst)->each(function ($user_id) use ($me) {
                if (!UsersFollowers::where([
                    'followers_id'  => $me->id,
                    'users_id'  =>  $user_id,
                    'from_where'  => 'profile',
                    'follow_type' => 1
                ])->first()) {
                    UsersFollowers::create([
                        'followers_id'  => $me->id,
                        'users_id'  =>  $user_id,
                        'from_where'  => 'profile',
                        'follow_type' => 1
                    ]);
                }
            });
        }
    }

    // Create post [text|media|checkin] in [newsfeed|country|city|place] pages
    public function createPost(Request $request)
    {
        try {
            $authUser       = Auth::user();
            $pair = $request->input("pair", "");
            $fileLists      = app(CommonApiController::class)->getTempFiles($pair);
            $isFiles        = count($fileLists) > 0 ? true : false;
            $postText       = processString($request->input("text", ""));
            $permission     = $request->permission ?? 0;
            $lat            = $request->lat ?? null;
            $lng            = $request->lng ?? null;
            $tags           = $request->input("tags", []);

            if (!in_array($permission, [0, 1, 2])) {
                return;
            }

            // Create a new Post
            // DB::beginTransaction();

            $post = new Posts();
            if (!is_null($postText)) {
                $translationService = new TranslationService();
                $post->language_id = $translationService->getLanguageId($postText);
            }
            $post->users_id         = $authUser->id;
            $post->text             = $postText;
            $post->date             = $request->input("post_date", date("Y/m/d H:i:s"), time());
            $post->permission       = $permission;
            $post->save();
            $isAleadySave           =  false;
            $locationFlag         = [];
            if ($request->has('location') && $request->has('checkin_date') && is_array($request->location)) {
                if (count($request->location) == 3 && isset($request->location['id'], $request->location['title'], $request->location['type'])) {
                    $checkins                   = new Checkins;
                    $checkins->users_id         = $authUser->id;
                    $checkins->location         = $request->location['title'];
                    $checkins->lat_lng          = $lat . ',' . $lng;
                    $checkins->checkin_time     = date("Y/m/d H:i:s", strtotime($request->checkin_date));
                    switch (strtolower($request->location['type'])) {
                        case CommonConst::POST_CHECKIN_PLACE:
                            $checkins->place_id = $request->location['id'];
                            $place = Place::find($checkins->place_id);
                            $isAleadySave = savePostInLocation($place, $post->id);
                            $locationFlag = [
                                CommonConst::POST_CHECKIN_PLACE,
                                $checkins->place_id
                            ];
                            break;
                        case CommonConst::POST_CHECKIN_CITY:
                            $checkins->city_id = $request->location['id'];
                            $city = Cities::find($checkins->city_id);
                            $isAleadySave = savePostInLocation($city, $post->id);
                            $locationFlag = [
                                CommonConst::POST_CHECKIN_CITY,
                                $checkins->city_id
                            ];
                            break;
                        case CommonConst::POST_CHECKIN_COUNTRY:
                            $checkins->country_id = $request->location['id'];
                            $country = Countries::find($checkins->country_id);
                            $isAleadySave = savePostInLocation($country, $post->id);
                            $locationFlag = [
                                CommonConst::POST_CHECKIN_COUNTRY,
                                $checkins->country_id
                            ];
                            break;
                    }
                    $checkins->save();

                    $post_checkins = new PostsCheckins;
                    $post_checkins->checkins_id = $checkins->id;
                    $post_checkins->posts_id = $post->id;
                    $post_checkins->save();
                }
            }

            if ($isAleadySave === false) {
                if ($request->has("page") && $request->has("page_id") && $request->get("page_id", 0) > 0) {
                    $page = strtolower(trim($request->get("page", '')));
                    switch ($page) {
                        case CommonConst::POST_CHECKIN_PLACE:
                            log_user_activity('Place_Post', 'post', $post->id);
                            savePostInLocation(Place::find($request->page_id), $post->id);
                            break;
                        case CommonConst::POST_CHECKIN_CITY:
                            log_user_activity('City_Post', 'post', $post->id);
                            savePostInLocation(Cities::find($request->page_id), $post->id);
                            break;
                        case CommonConst::POST_CHECKIN_COUNTRY:
                            log_user_activity('Country_Post', 'post', $post->id);
                            savePostInLocation(Countries::find($request->page_id), $post->id);
                            break;
                    }
                    $locationFlag = [
                        $page,
                        $request->page_id
                    ];
                }
            }

            log_user_activity(PrimaryPostService::TYPE_POST, 'publish', $post->id);

            if ($isFiles) {
                foreach ($fileLists as $file) {
                    $filename = $authUser->id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('post-photo/' . $filename, fopen($file[0], 'r+'), 'public');
                    $path = S3_BASE_URL . 'post-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $authUser->name;
                    $media->title = strip_tags($post->text);
                    $media->author_url = check_profile_picture($authUser->profile_picture);
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->users_id  = $authUser->id;
                    $media->type  = getMediaTypeByMediaUrl($path);
                    $media->save();

                    $users_medias = new UsersMedias();
                    $users_medias->users_id = $authUser->id;
                    $users_medias->medias_id = $media->id;
                    $users_medias->save();

                    $posts_medias = new PostsMedia();
                    $posts_medias->posts_id = $post->id;
                    $posts_medias->medias_id = $media->id;
                    $posts_medias->save();

                    if (count($locationFlag) == 2) {
                        if ($locationFlag[0] == CommonConst::POST_CHECKIN_PLACE) {
                            $placeMediaSave = new PlaceMedias();
                            $placeMediaSave->places_id = $locationFlag[1];
                            $placeMediaSave->medias_id = $media->id;
                            $placeMediaSave->save();
                        } else if ($locationFlag[0] == CommonConst::POST_CHECKIN_CITY) {
                            $cityMediaSave = new CitiesMedias();
                            $cityMediaSave->cities_id = $locationFlag[1];
                            $cityMediaSave->medias_id = $media->id;
                            $cityMediaSave->save();
                        } else if ($locationFlag[0] == CommonConst::POST_CHECKIN_COUNTRY) {
                            $countryMediaSave = new CountriesMedias();
                            $countryMediaSave->countries_id = $locationFlag[1];
                            $countryMediaSave->medias_id = $media->id;
                            $countryMediaSave->save();
                        }
                    }

                    $isValideImage = moderateImage($media, $post);
                    if ($isValideImage === false) {
                        $post->delete();
                        return;
                    }
                    if ($media && $posts_medias && $users_medias && $post) {
                        if (count($fileLists) == 1) {
                            log_user_activity('Media', 'upload', $media->id);
                        }
                    }
                }
                app(CommonApiController::class)->deleteTempFiles($pair);
            }
            save_tags($tags, PostsTags::TYPE_POST, $post->id);
            // DB::commit();

            return $post->id;
        } catch (\Throwable $e) {
            return;
        }
    }
}
