<?php

namespace App\Services\Ranking;

use App\Models\Ranking\RankingBadges;
use App\Models\Ranking\RankingPointsEarners;
use App\Models\Ranking\RankingUsersBadges;
use App\Models\TripPlaces\TripPlaces;
use App\Models\User\User;
use App\Resolvers\RankingPointsEarners\BasePointsEarners;
use App\Resolvers\RankingPointsEarners\PointsEarnersResolverInterface;
use App\Resolvers\RankingPointsEarners\TripPlacePointsEarners;
use App\Services\RankingPoints\RankingPointsService;
use App\Services\RankingUserTransactions\RankingUserTransactionsService;
use App\Services\Trips\TripInvitationsService;

class RankingService
{
    /**
     * @var RankingPointsService
     */
    private $rankingPointsService;
    /**
     * @var TripInvitationsService
     */
    private $tripInvitationsService;
    /**
     * @var RankingUserTransactionsService
     */
    private $rankingUserTransactionsService;


    public function __construct(RankingPointsService $rankingPointsService, TripInvitationsService $tripInvitationsService, RankingUserTransactionsService $rankingUserTransactionsService)
    {
        $this->rankingPointsService = $rankingPointsService;
        $this->tripInvitationsService = $tripInvitationsService;
        $this->rankingUserTransactionsService = $rankingUserTransactionsService;
    }

    const POINTS_EARNERS_RESOLVERS_MAPPER = [
        TripPlaces::class => TripPlacePointsEarners::class
    ];

    const POINTS_EARNERS_BASE_RESOLVER = BasePointsEarners::class;

    public static function getAvailableBadgesList()
    {
        return RankingBadges::query()->pluck('name', 'id');
    }

    public function addPointsEarners($entityId, $entityType, $userId)
    {
        RankingPointsEarners::query()->firstOrCreate([
            'entity_id' => $entityId,
            'entity_type' => $entityType,
            'user_id' => $userId
        ]);
    }

    public function getPointsEarners($entity)
    {
        $entityClass = get_class($entity);
        $resolverClass = self::POINTS_EARNERS_BASE_RESOLVER;

        if (isset(self::POINTS_EARNERS_RESOLVERS_MAPPER[$entityClass])) {
            $resolverClass = self::POINTS_EARNERS_RESOLVERS_MAPPER[$entityClass];
        }

        /** @var PointsEarnersResolverInterface $resolver */
        $resolver = app($resolverClass);

        return $resolver->resolve($entity);
    }

    public function addPointsToEarners($entity)
    {
        $earners = $this->getPointsEarners($entity);

        if (empty($earners)) {
            return;
        }

        foreach ($earners as $earner) {
            if ($earner === auth()->id()) {
                continue;
            }

            $this->rankingPointsService->addPoints($earner, 1);
            $this->rankingUserTransactionsService->addPoints($entity, $earner, 1);
        }
    }

    public function subPointsFromEarners($entity)
    {
        $earners = $this->getPointsEarners($entity);

        if (empty($earners)) {
            return;
        }

        foreach ($earners as $earner) {
            if ($earner === auth()->id()) {
                continue;
            }

            $this->rankingPointsService->subPoints($earner, 1);
            $this->rankingUserTransactionsService->subPoints($entity, $earner, 1);
        }
    }

    /**
     * @param $userId
     * @return RankingUsersBadges|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getCurrentBadge($userId)
    {
        return RankingUsersBadges::query()
            ->whereHas('user', function ($q) {
                $q->where('expert', 1)->where('is_approved', 1);
            })
            ->where('users_id', $userId)
            ->first();
    }

    /**
     * @param $badgeId
     * @return RankingUsersBadges|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getUsersByBadgeId($badgeId)
    {
        if ($badgeId) {
            return RankingUsersBadges::query()
                ->whereHas('user', function ($q) {
                    $q->where('expert', 1)->where('is_approved', 1);
                })
                ->where('badges_id', $badgeId)->orderBy('created_at', 'DESC')
                ->get();
        }
    }

    public function getNextBudge($userId)
    {
        $currentBadge = $this->getCurrentBadge($userId);

        $query = RankingBadges::query();

        if ($currentBadge) {
            $query->where('order', '>', $currentBadge->badge->order);
        }

        return $query
            ->orderBy('order')
            ->first();
    }

    public function getProgressData($userId)
    {
        $user = User::find($userId);
        $currentBadge = $this->getCurrentBadge($userId);
        $nextBadge = $this->getNextBudge($userId);

        if ($nextBadge) {
            $normallyNeedFollowers = $nextBadge->followers;
            $normallyNeedInteractions = $nextBadge->points;
        } else {
            $normallyNeedFollowers = 1;
            $normallyNeedInteractions = 1;
        }

        $needFollowers = $normallyNeedFollowers;
        $needInteractions = $normallyNeedInteractions;

        if ($currentBadge && $currentBadge->is_preset) {
            $needFollowers -= $currentBadge->badge->followers;
            $needInteractions -= $currentBadge->badge->points;
        }

        $interactions = $this->rankingPointsService->findByUserId($userId)->points ?? 0;
        $followers = $this->tripInvitationsService->findFollowers($userId)->count();

        if (!$needInteractions) {
            $needInteractions = 1;
        }

        if (!$needFollowers) {
            $needFollowers = 1;
        }

        $interactionsProgress = $interactions * 100 / $needInteractions;
        $followersProgress = $followers * 100 / $needFollowers;

        if ($interactionsProgress > 100) {
            $interactionsProgress = 100;
        }

        if ($followersProgress > 100) {
            $followersProgress = 100;
        }

        $overallProgress = round(($interactionsProgress + $followersProgress) / 2, 2);

        $discount = (($needInteractions * 100 / $normallyNeedInteractions) + ($needFollowers * 100 / $normallyNeedFollowers)) / 2;

        return [
            'current_badge' => $currentBadge,
            'next_badge' => $nextBadge,
            'interactions' => $interactions,
            'followers' => $followers,
            'normally_need_followers' => $normallyNeedFollowers,
            'normally_need_interactions' => $normallyNeedInteractions,
            'need_followers' => $needFollowers,
            'need_interactions' => $needInteractions,
            'interactions_progress' => $interactionsProgress,
            'followers_progress' => $followersProgress,
            'overall_progress' => $overallProgress,
            'discount' => $discount,
            'has_picture' => ($user) ? (bool) $user->profile_picture : false,
            'has_cover' => ($user) ? (bool) $user->cover_photo : false,
        ];
    }

    public function presetBadge($userId, $badgeId)
    {
        RankingUsersBadges::query()->firstOrCreate([
            'users_id' => $userId,
            'badges_id' => $badgeId
        ], [
            'users_id' => $userId,
            'badges_id' => $badgeId,
            'is_preset' => true
        ]);
    }

    public function changeBadge($userId, $badgeId, $isPreset = true)
    {
        if ($badgeId) {
            RankingUsersBadges::query()->firstOrCreate([
                'users_id' => $userId,
            ], [
                'users_id' => $userId,
                'badges_id' => $badgeId,
                'is_preset' => $isPreset
            ]);

            return true;
        }

        RankingUsersBadges::query()->where('users_id', $userId)->delete();

        return true;
    }
}
