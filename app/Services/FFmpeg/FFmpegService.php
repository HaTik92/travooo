<?php

namespace App\Services\FFmpeg;

use FFMpeg\FFMpeg;
use FFMpeg\Format\Video\X264;
use Illuminate\Support\Facades\Storage;

class FFmpegService
{
    /**
     * @param $path
     * @param $filename
     * @param $new_filename
     */
    public function convertVideoFile($path, $filename, $new_filename)
    {
        try {
            $ffmpeg =  FFMpeg::create([
                'ffmpeg.binaries' => config('ffmpeg.binaries.ffmpeg'),
                'ffprobe.binaries' => config('ffmpeg.binaries.ffprobe'),
                'timeout' => 3600, // The timeout for the underlying process
                'ffmpeg.threads' => 12, // The number of threads that FFMpeg should use
            ]);

            $video = $ffmpeg->open($path . '/' . $filename);

            // Configure the new mp4 formax (x264)
            $mp4Format = new X264();

            $mp4Format->setAudioCodec("libmp3lame");

            // Save the video in the same directory with the new format
            $video->save($mp4Format, $path . '/' . $new_filename);
            @unlink($path . '/' . $filename);
        } catch (\Throwable $e) {
        }
    }

    /**
     * @param string $videoPath
     * @param int $max_width
     * @param int $max_height
     * @param int $quality
     * @return string
     */
    public function convertVideoThumbnail($videoPath, $max_width = 1920, $max_height = 1080, $quality = 70)
    {
        $folderPath = storage_path('app\public\video-thumbs');
        $videoThumbnail = app(VideoThumbnail::class);
        return $videoThumbnail->createThumbnail($videoPath, $folderPath, 0, $max_width, $max_height, $quality);
    }

    /**
     * @param string $thumbnailPath
     * @param bool $withDeleteLocal
     * @return string
     */
    public function moveThumbnailToS3($thumbnailPath, $withDeleteLocal = false)
    {
        $folderPath = storage_path('app\public\video-thumbs');
        $fileName = str_replace($folderPath, '', $thumbnailPath);
        Storage::disk('s3')->put('video-thumbs/' . $fileName, fopen($thumbnailPath, 'r+'), 'public');
        $s3Path = S3_BASE_URL . 'video-thumbs' . $fileName;
        if ($withDeleteLocal) {
            Storage::disk('public')->delete('video-thumbs/' . $fileName);
        }
        return $s3Path;
    }
}
