<?php

/*
 * Video thumbnail class for implementing FFMpeg Features
 */

namespace App\Services\FFmpeg;

use FFMpeg\FFMpeg;
use FFMpeg\Coordinate;
use Exception;
use Illuminate\Support\Facades\Log;

class VideoThumbnail
{

    private $FFMpeg = NULL;
    private $videoObject = NULL;
    public $videoURL = NULL;
    public $storageURL = NULL;
    public $thumbName = NULL;
    public $fullFile = NULL;
    public $height = 240;
    public $width = 320;
    private $screenShotTime = 1;
    private $quality = 80;

    /**
     * @param string $videoPath
     * @param string $storageUrl
     * @param int $second
     * @param int $width
     * @param int $height
     * @param int $quality
     */
    public function createThumbnail($videoUrl, $storageUrl, $second, $width = 640, $height = 480, $quality = 80)
    {
        if (!file_exists($storageUrl)) {
            mkdir($storageUrl, 0777, true);
        }

        $this->videoURL = $videoUrl;
        $fileName = $this->getFileName();

        $this->storageURL = $storageUrl;
        $this->thumbName = $fileName;
        $this->fullFile = "{$this->storageURL}/{$this->thumbName}";
        $this->quality = $quality;

        $this->screenShotTime = $second;

        $this->width = $width;
        $this->height = $height;

        try {
            $this->create();
            $this->thumbnail();
            $this->resizeCropImage($this->width, $this->height, $this->fullFile, $this->fullFile, $this->quality);
            return $this->fullFile;
        } catch (Exception $e) {
            Log::error(json_encode([
                'videoURL' => $this->videoURL,
                'fullFile' => $this->fullFile,
                'message' => $e->getMessage()
            ], JSON_PRETTY_PRINT));
        }
        return null;
    }

    public function resizeCropImage($max_width, $max_height, $source_file, $dst_dir, $quality)
    {
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];

        switch ($mime) {
            case 'image/gif':
                $image_create = "imagecreatefromgif";
                $image = "imagegif";
                break;

            case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";
                $quality = 7;
                break;

            case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";
                $quality = 80;
                break;

            default:
                return false;
                break;
        }

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if ($width_new > $width) {
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        } else {
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $image($dst_img, $dst_dir, $quality);

        if ($dst_img)
            imagedestroy($dst_img);
        if ($src_img)
            imagedestroy($src_img);
    }

    private function create()
    {
        $this->FFMpeg = FFMpeg::create([
            'ffmpeg.binaries'  => config('ffmpeg.binaries.ffmpeg'),
            'ffprobe.binaries' => config('ffmpeg.binaries.ffprobe')
        ]);
        $this->videoObject = $this->FFMpeg->open($this->videoURL);
        return $this->videoObject;
    }

    public function getFileName()
    {
        $fileName = last(explode("/", $this->videoURL));
        $fileName = explode(".", $fileName)[0] . "_thumbnail.jpg";
        return $fileName;
    }

    private function thumbnail()
    {
        $this->videoObject->frame(Coordinate\TimeCode::fromSeconds($this->screenShotTime))->save($this->fullFile);
        return $this->videoObject;
    }
}
