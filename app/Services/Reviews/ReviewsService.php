<?php

namespace App\Services\Reviews;

use App\Models\ActivityMedia\Media;
use App\Models\City\Cities;
use App\Models\Place\Place;
use App\Models\User\User;
use Illuminate\Support\Collection;
use App\Models\Reviews\Reviews;
use Illuminate\Support\Facades\DB;

class ReviewsService
{

    /**
     * @param Collection $reviews
     * @param Collection $allReviews
     *
     * @return array
     */
    protected function prepareValues($reviews, $allReviews)
    {
        $onePercentValue = $allReviews->count() / 100;

        return [
            'count' => $reviews->count(),
            'percent' => $onePercentValue != 0 ?(int) ($reviews->count() / $onePercentValue):0,
        ];
    }

    //TODO move to repository
    private function getReviewsCollections($user_id, $limit, $offset, $sort, $score, $place_id)
    {
        $query = $this->getReviewsQuery($user_id, $score, $place_id)->with('medias', 'author', 'place', 'place.trans');
        switch ($sort) {
            case 'date_down':
                 $query->orderBy('created_at', 'desc');
                break;
            case 'date_up':
                $query->orderBy('created_at', 'asc');
                break;
            default:
                $query->orderBy('created_at', 'desc');
        }

        if ($offset != '') {
            $query->skip($offset);
        }
        if ($limit != '') {
            $query->take($limit);
        }

        return $query->get();
    }

    //TODO move to repository
    private function getReviewsQuery($user_id, $score, $place_id) {
        $query = Reviews::where('by_users_id', $user_id)->whereNotNull('places_id');
        if($score && $score !='all'){
            $query->where('score', $score);
        }

        if($place_id !=''){
            $query->where('places_id', $place_id);
        }

        return $query;
    }

    public function getReviews($userId, $page, $perPage, $sort, $score, $place_id = '') {
        $authUserId = auth()->id();
        $offset = ($page - 1) * $perPage;

        $reviews = $this->getReviewsCollections($userId, $perPage, $offset, $sort, $score, $place_id);
        $placesIds = $reviews->pluck('place.id')->toArray();
        $scoreAndTotal = Reviews::select(DB::raw('COUNT(*) as total, AVG(score) as score'), 'places_id')
            ->whereIn('places_id', $placesIds)
            ->groupBy('places_id')
            ->get()->keyBy('places_id')->toArray();

        $data['reviews'] = $reviews->map(function ($review) use ($authUserId, $scoreAndTotal) {
                $trans = $review->place->trans->first();

                return [
                    'id' => $review->id,
                    'media' => [
                        'list' => $review->medias->map(function ($media) {
                            return [
                                'id' => $media->id,
                                'thumbnail_url' => $media->type == Media::TYPE_VIDEO
                                    ? $media->video_thumbnail_url
                                    : S3_BASE_URL . 'th230/' . $media->url,
                                'url' => S3_BASE_URL . $media->url,
                                'type' => $media->type == Media::TYPE_VIDEO ? 'video' : 'image'
                            ];
                        }),
                        'count' => $review->medias->count()
                    ],
                    'date' => date_format($review->created_at, 'l d M Y'),
                    'author' => [
                        'id' => $review->by_users_id,
                        'profile_picture' => check_profile_picture($review->author->profile_picture)
                    ],
                    'reviewed_object' => [
                        'id' => $review->place->id,
                        'avatar' => check_place_photo($review->place, true),
                        'link' => url_with_locale('place/' . $review->place->id),
                        'title' => $trans ? $trans->title : '',
                        'is_followed' => $review->place->followers()->select('id')->where('users_id', $authUserId)->exists(),
                        'reviews_count' => $scoreAndTotal[$review->place->id]['total'],
                        'score' => round($scoreAndTotal[$review->place->id]['score'], 1)
                    ],
                    'text' => $review->text,
                    'score' => intval($review->score),
                ];
            });
        $data['count'] = $this->getReviewsQuery($userId, $score, $place_id)->count();

        return $data;
    }
}
