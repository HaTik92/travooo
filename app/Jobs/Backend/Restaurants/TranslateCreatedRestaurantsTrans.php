<?php

namespace App\Jobs\Backend\Restaurants;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Dedicated\GoogleTranslate\Translator;
use App\Models\Restaurants\RestaurantsTranslations;

class TranslateCreatedRestaurantsTrans implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $defaultTrans;
    protected $translation;
    protected $languageCode;

    /**
     * Create a new job instance.
     *
     * @param $defaultTrans
     * @param $translation
     * @param $languageCode
     */
    public function __construct($defaultTrans, $translation, $languageCode)
    {
        $this->defaultTrans    = $defaultTrans;
        $this->translation     = $translation;
        $this->languageCode    = $languageCode;
    }

    /**
     * Execute the job.
     *
     * @param Translator $translator
     * @param RestaurantsTranslations $restaurantsTranslations
     * @return void
     */
    public function handle(Translator $translator, RestaurantsTranslations $restaurantsTranslations)
    {
        $translation = [];
        foreach ($this->translation as $key => $value) {
            if (($value == null) && ($this->defaultTrans[$key] != null)) {

                $translation[$key] = $translator->setSourceLang('en')
                    ->setTargetLang($this->languageCode)
                    ->translate($this->defaultTrans[$key]);
            } else {
                $translation[$key] = $this->translation[$key];
            }
        }
        $restaurantsTranslations->insert($translation);
    }
}
