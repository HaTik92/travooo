<?php

namespace App\Jobs\Backend\Weather;

use App\Helpers\Backend\Weather\Weather;
use App\Models\Access\language\Languages;
use App\Models\City\Cities;
use App\Models\City\CitiesWeather;
use App\Models\City\CitiesWeatherTranslations;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Get implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $offset;
    protected $limit;

    /**
     * Create a new job instance.
     *
     * @param $offset
     * @param $limit
     */
    public function __construct($offset, $limit)
    {
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        sleep(60);
        $cities = Cities::offset($this->offset)->limit($this->limit)->get();
        $languages = Languages::all();

        foreach ($cities as $city) {
            foreach ($languages as $language) {
                $weather = new Weather($city->lat, $city->lng, $language->code);

                if ($weather->getDailyForecast() !== false && $weather->getUpcomingForecast() !== false) {
                    $daily = $weather->getDailyForecast();
                    $upcoming = $weather->getUpcomingForecast();

                    $weather = CitiesWeather::UpdateOrCreate(
                        ['cities_id' => $city->id], ['schedule' => Carbon::now()]
                    );

                    CitiesWeatherTranslations::UpdateOrCreate(
                        [
                            'weather_id' => $weather->id,
                            'language_id' => $language->id
                        ],
                        [
                            'daily_forecast' => $daily,
                            'upcoming_forecast' => $upcoming
                        ]
                    );
                }
            }
        }
    }
}