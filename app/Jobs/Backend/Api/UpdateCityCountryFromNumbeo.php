<?php

namespace App\Jobs\Backend\Api;

use App\Models\City\Cities;
use App\Models\Country\Countries;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateCityCountryFromNumbeo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Update all countries
        $this->updateCountries();

        //Update all cities
        $this->updateCities();
    }

    public function updateCities() {
        $cities = Cities::all();
        foreach ($cities as $city) {
            try {
                $data = $this->getData($city->trans[0]->title, 'city');
                $city->trans[0]->geo_stats = $data['geo_stats'];
                $city->trans[0]->demographics = $data['demographics'];
                $city->trans[0]->pollution_index = $data['pollution_index'];
                $city->trans[0]->cost_of_living = $data['cost_of_living'];

                $city->trans[0]->save();
            } catch (\Exception $e) {
                continue;
            }
        }
    }

    public function updateCountries() {
        $countries = Countries::all();
        foreach ($countries as $country) {
            try {
                $data = $this->getData($country->trans[0]->title, 'country');
                //crime_rate
                if ($data['geo_stats'])
                    $country->trans[0]->geo_stats = $data['geo_stats'];
                if ($data['cost_of_living'])
                    $country->trans[0]->cost_of_living = $data['cost_of_living'];
                if ($data['demographics'])
                    $country->trans[0]->demographics = $data['demographics'];
                //quality_of_life
                if ($data['pollution_index'])
                    $country->trans[0]->pollution_index = $data['pollution_index'];

                $country->trans[0]->save();
            } catch (\Exception $e) {
                continue;
            }
        }
    }

    /**
     * @param $title
     * @param $type
     * @return mixed
     */
    private function getData($title, $type) {
        $crimeRate= '';
        $costOfLiving= '';
        $qualityOfLife= '';
        $pollutionIndex= '';
        $client = new Client();
        $indicesType = $type === 'city' ? 'indices' : 'country_indices';
        $queryType = $type === 'city' ? 'query' : 'country';
        $indices = @$client->post( 'https://www.numbeo.com/api/' . $indicesType . '?api_key=il3zvogvvbthch&' . $queryType . '=' . $title, [
            'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'TZUhMoqSkz0JGuiBjL22c8x5Bsm9zgymv7AhZgg3'],
            'verify' => false
        ]);
        if ($indices->getStatusCode() == 200) {
            $json_response = $indices->getBody()->read(1024);
            $response = json_decode($json_response);
            if (!isset($response->error)) {
                $pollutionIndex = isset($response->pollution_index) ? $response->pollution_index : '';
                $qualityOfLife = isset($response->quality_of_life_index) ? $response->quality_of_life_index : '';
                $crimeRate = isset($response->crime_index) ? $response->crime_index : '';
                $costOfLiving = isset($response->cpi_index) ? $response->cpi_index : '';
            }
        }
        $data['geo_stats']          = $crimeRate;
        $data['cost_of_living']     = $costOfLiving;
        $data['demographics']       = $qualityOfLife;
        $data['pollution_index']    = $pollutionIndex;

        return $data;
    }
}
