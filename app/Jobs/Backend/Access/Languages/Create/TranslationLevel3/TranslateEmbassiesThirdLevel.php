<?php

namespace App\Jobs\Backend\Access\Languages\Create\TranslationLevel3;

use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Backend\Access\Languages\Create\TranslationLevel4\TranslateEmbassiesFourthLevel;

class TranslateEmbassiesThirdLevel implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, DispatchesJobs;

    protected $language;
    protected $models;
    protected $progressId;

    /**
     * Create a new job instance.
     *
     * @param $language
     * @param $models
     * @param $progressId
     */
    public function __construct($language, $models, $progressId)
    {
        $this->language = $language;
        $this->models = $models;
        $this->progressId = $progressId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->models as $model) {
            $this->dispatch(
                (new TranslateEmbassiesFourthLevel($this->language, $model, $this->progressId))
                    ->onQueue('translateLevel4')
            );
        }
    }
}
