<?php

namespace App\Jobs\Backend\Access\Languages\Create\TranslationLevel1;

use App\Models\City\CitiesTranslations;
use App\Models\Country\CountriesTranslations;
use App\Models\Hotels\HotelsTranslations;
use App\Models\Place\PlaceTranslations;
use App\Models\Restaurants\RestaurantsTranslations;
use App\Models\Embassies\EmbassiesTranslations;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Backend\Access\Languages\Create\TranslationLevel2\TranslateCountries;
use App\Jobs\Backend\Access\Languages\Create\TranslationLevel2\TranslateCities;
use App\Jobs\Backend\Access\Languages\Create\TranslationLevel2\TranslateHotels;
use App\Jobs\Backend\Access\Languages\Create\TranslationLevel2\TranslatePlaces;
use App\Jobs\Backend\Access\Languages\Create\TranslationLevel2\TranslateRestaurants;
use App\Jobs\Backend\Access\Languages\Create\TranslationLevel2\TranslateEmbassies;
use App\Models\Access\Language\ConfTranslationsProgress;

class LanguagesCreateTransManager implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, DispatchesJobs;

    protected $language;

    /**
     * Create a new job instance.
     *
     * @param $language
     */
    public function __construct($language)
    {
        $this->language = $language;
    }

    /**
     * Execute the job.
     *
     * @param ConfTranslationsProgress $confTranslationsProgress
     * @return void
     */
    public function handle(ConfTranslationsProgress $confTranslationsProgress)
    {
        $totalCount = 0;
        $totalCount += CitiesTranslations::where('languages_id', 1)->count();
        $totalCount += CountriesTranslations::where('languages_id', 1)->count();
        $totalCount += HotelsTranslations::where('languages_id', 1)->count();
        $totalCount += PlaceTranslations::where('languages_id', 1)->count();
        $totalCount += RestaurantsTranslations::where('languages_id', 1)->count();
        $totalCount += EmbassiesTranslations::where('languages_id', 1)->count();
        $progress = $confTranslationsProgress->create(['language_title' => $this->language['title'], 'language_code' => $this->language['code'], 'event' => 'create', 'total_count' => $totalCount]);
        $this->dispatch(
            (new TranslateCountries($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );

        $this->dispatch(
            (new TranslateCities($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );

        $this->dispatch(
            (new TranslateHotels($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );

        $this->dispatch(
            (new TranslatePlaces($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );

        $this->dispatch(
            (new TranslateRestaurants($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );

        $this->dispatch(
            (new TranslateEmbassies($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );
    }
}
