<?php

namespace App\Jobs\Backend\Access\Languages\Create\TranslationLevel2;

use App\Models\Access\Language\ConfTranslationsProgress;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Embassies\EmbassiesTranslations;
use App\Jobs\Backend\Access\Languages\Create\TranslationLevel3\TranslateEmbassiesThirdLevel;

class TranslateEmbassies implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, DispatchesJobs;

    protected $language;
    protected $progressId;

    /**
     * Create a new job instance.
     *
     * @param $language
     * @param $progressId
     */
    public function __construct($language, $progressId)
    {
        $this->language = $language;
        $this->progressId = $progressId;
    }

    /**
     * Execute the job.
     *
     * @param EmbassiesTranslations $embassiesTranslations
     * @return void
     */
    public function handle(EmbassiesTranslations $embassiesTranslations)
    {
        $translationsCount  = $embassiesTranslations->where('languages_id', 1)->count();
        $limit              = 10;
        for ($i = 0; $i < ceil($translationsCount / $limit); $i++) {
            $models = $embassiesTranslations->where('languages_id', 1)->skip($i * $limit)->take($limit)->get();
            $this->dispatch(
                (new TranslateEmbassiesThirdLevel($this->language, $models, $this->progressId))
                    ->onQueue('translateLevel3')
            );
        }
    }
}
