<?php

namespace App\Jobs\Backend\Access\Languages\Create\TranslationLevel4;

use App\Models\Access\Language\ConfTranslationsProgress;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Embassies\EmbassiesTranslations;

class TranslateEmbassiesFourthLevel implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $language;
    protected $model;
    protected $progressId;

    /**
     * Create a new job instance.
     *
     * @param $language
     * @param $model
     * @param $progressId
     */
    public function __construct($language, $model, $progressId)
    {
        $this->language = $language;
        $this->model = $model;
        $this->progressId = $progressId;
    }

    /**
     * Execute the job.
     *
     * @param EmbassiesTranslations $embassiesTranslations
     * @param ConfTranslationsProgress $confTranslationsProgress
     * @return void
     */
    public function handle(EmbassiesTranslations $embassiesTranslations, ConfTranslationsProgress $confTranslationsProgress)
    {
        try {
            if ( $embassiesTranslations->where(['languages_id' => $this->language['id'], 'embassies_id' => $this->model->embassies_id])->first() ) {
                $progress = $confTranslationsProgress->find($this->progressId);
                $inputs['done_count'] = $progress->done_count + 1;
                if ($progress->total_count == ($progress->done_count + 1)) {
                    $inputs['completed_at'] = Carbon::now();
                }
                $progress->update($inputs);
                return true;
            }
            $model = $this->model->toArray();
            unset($model['id']);
            $data = [];
            foreach ($model as $key => $value) {

                if ($value != '') {
                    $data[$key] = $value;
                }
                $data['embassies_id'] = $model['embassies_id'];
                $data['languages_id'] = $this->language['id'];
            }

            $urlParams = implode("&q=", $data);
            $url = 'https://translation.googleapis.com/language/translate/v2?q='.$urlParams;
            $client = new \GuzzleHttp\Client([
                'headers' => [ 'Content-Type' => 'application/x-www-form-urlencoded' ]
            ]);

            $response = $client->post($url,
                ['form_params' =>
                    [
                        'target' => $this->language['code'],
                        'source' => 'en',
                        'key' => env('GOOGLE-API-KEY')
                    ]
                ]
            );
            $res = json_decode((string)$response->getBody())->data->translations;
            $i = 0;
            foreach ($data as $key => $value) {
                $data[$key] = $res[$i]->translatedText;
                $i++;
            }
            if ($embassiesTranslations->insert($data)) {
                $progress = $confTranslationsProgress->find($this->progressId);
                $inputs['done_count'] = $progress->done_count + 1;
                if ($progress->total_count == ($progress->done_count + 1)) {
                    $inputs['completed_at'] = Carbon::now();
                }
                $progress->update($inputs);
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }
}
