<?php

namespace App\Jobs\Backend\Access\Languages\Update\TranslationLevel2;

use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Restaurants\RestaurantsTranslations;
use App\Jobs\Backend\Access\Languages\Update\TranslationLevel3\TranslateRestaurantsThirdLevel;


class TranslateRestaurants implements ShouldQueue
{
    use DispatchesJobs, InteractsWithQueue, Queueable, SerializesModels;

    protected $language;
    protected $progressId;

    /**
     * Create a new job instance.
     *
     * @param $language
     * @param $progressId
     */
    public function __construct($language, $progressId)
    {
        $this->language = $language;
        $this->progressId = $progressId;
    }

    public function handle(RestaurantsTranslations $restaurantsTranslations)
    {
        $translationsCount  = $restaurantsTranslations->where('languages_id', $this->language['id'])->count();
        $limit              = 10;
        for ($i = 0; $i < ceil($translationsCount / $limit); $i++) {
            $models = $restaurantsTranslations->where('languages_id', $this->language['id'])->skip($i * $limit)->take($limit)->get();
            $this->dispatch(
                (new TranslateRestaurantsThirdLevel($this->language, $models, $this->progressId))
                    ->onQueue('translateLevel3')
            );
        }
    }
}
