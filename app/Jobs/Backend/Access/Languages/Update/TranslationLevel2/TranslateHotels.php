<?php

namespace App\Jobs\Backend\Access\Languages\Update\TranslationLevel2;

use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Hotels\HotelsTranslations;
use App\Jobs\Backend\Access\Languages\Update\TranslationLevel3\TranslateHotelsThirdLevel;


class TranslateHotels implements ShouldQueue
{
    use DispatchesJobs, InteractsWithQueue, Queueable, SerializesModels;

    protected $language;
    protected $progressId;

    /**
     * Create a new job instance.
     *
     * @param $language
     * @param $progressId
     */
    public function __construct($language, $progressId)
    {
        $this->language = $language;
        $this->progressId = $progressId;
    }

    /**
     * @param HotelsTranslations $hotelsTranslations
     */
    public function handle(HotelsTranslations $hotelsTranslations)
    {
        $translationsCount  = $hotelsTranslations->where('languages_id', $this->language['id'])->count();
        $limit               = 10;
        for ($i = 0; $i < ceil($translationsCount / $limit); $i++) {
            $models = $hotelsTranslations->where('languages_id', $this->language['id'])->skip($i * $limit)->take($limit)->get();
            $this->dispatch(
                (new TranslateHotelsThirdLevel($this->language, $models, $this->progressId))
                    ->onQueue('translateLevel3')
            );
        }
    }
}
