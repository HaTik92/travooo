<?php

namespace App\Jobs\Backend\Access\Languages\Update\TranslationLevel4;

use App\Models\Access\Language\ConfTranslationsProgress;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Country\CountriesTranslations;

class TranslateCountriesFourthLevel implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $language;
    protected $model;
    protected $progressId;

    /**
     * Create a new job instance.
     *
     * @param $language
     * @param $model
     * @param $progressId
     */
    public function __construct($language, $model, $progressId)
    {
        $this->language = $language;
        $this->model = $model;
        $this->progressId = $progressId;
    }

    /**
     * Execute the job.
     *
     * @param CountriesTranslations $countriesTranslations
     * @param ConfTranslationsProgress $confTranslationsProgress
     * @return void
     */
    public function handle(CountriesTranslations $countriesTranslations, ConfTranslationsProgress $confTranslationsProgress)
    {
        \Log::info('***********************TranslateCountriesFourthLevel 4****************************');

        try {
            $model          = $this->model->toArray();
            $defaultModel   = $countriesTranslations->where([ 'countries_id' => $model['countries_id'], 'languages_id' => 1 ])->first()->toArray();
            $urlParams      = implode("&q=", $defaultModel);
            $url = 'https://translation.googleapis.com/language/translate/v2?q=' . $urlParams;
            $client = new \GuzzleHttp\Client([
                'headers' => ['Content-Type' => 'application/x-www-form-urlencoded']
            ]);

            $response = $client->post($url,
                ['form_params' =>
                    [
                        'target' => $this->language['code'],
                        'source' => 'en',
                        'key' => env('GOOGLE-API-KEY')
                    ]
                ]
            );
            $res = json_decode((string)$response->getBody())->data->translations;
            $i = 0;
            foreach ($defaultModel as $key => $value) {
                $defaultModel[$key] = $res[$i]->translatedText;
                $i++;
            }
            $id = $model['id'];
            unset($defaultModel['id'], $defaultModel['languages_id']);

            if ($countriesTranslations->find($id)->update($defaultModel)) {
                $progress = $confTranslationsProgress->find($this->progressId);
                $inputs['done_count'] = $progress->done_count + 1;
                if ($progress->total_count == ($progress->done_count + 1)) {
                    $inputs['completed_at'] = Carbon::now();
                }
                $progress->update($inputs);
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
