<?php

namespace App\Jobs\Backend\Access\Languages\Update\TranslationLevel1;

use App\Models\Access\Language\ConfTranslationsProgress;
use App\Models\City\CitiesTranslations;
use App\Models\Country\CountriesTranslations;
use App\Models\Hotels\HotelsTranslations;
use App\Models\Place\PlaceTranslations;
use App\Models\Restaurants\RestaurantsTranslations;
use App\Models\Embassies\EmbassiesTranslations;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\Backend\Access\Languages\Update\TranslationLevel2\TranslateCountries;
use App\Jobs\Backend\Access\Languages\Update\TranslationLevel2\TranslateCities;
use App\Jobs\Backend\Access\Languages\Update\TranslationLevel2\TranslateHotels;
use App\Jobs\Backend\Access\Languages\Update\TranslationLevel2\TranslatePlaces;
use App\Jobs\Backend\Access\Languages\Update\TranslationLevel2\TranslateRestaurants;
use App\Jobs\Backend\Access\Languages\Update\TranslationLevel2\TranslateEmbassies;

class LanguagesUpdateTransManager implements ShouldQueue
{
    use DispatchesJobs, InteractsWithQueue, Queueable, SerializesModels;

    protected $language;

    /**
     * Create a new job instance.
     *
     * @param $language
     */
    public function __construct( $language )
    {
        $this->language = $language;
    }

    /**
     * Execute the job.
     *
     * @param ConfTranslationsProgress $confTranslationsProgress
     * @return void
     */
    public function handle(ConfTranslationsProgress $confTranslationsProgress)
    {
        $totalCount = 0;
        $totalCount += CitiesTranslations::where('languages_id', $this->language['id'])->count();
        $totalCount += CountriesTranslations::where('languages_id', $this->language['id'])->count();
        $totalCount += HotelsTranslations::where('languages_id', $this->language['id'])->count();
        $totalCount += PlaceTranslations::where('languages_id', $this->language['id'])->count();
        $totalCount += RestaurantsTranslations::where('languages_id', $this->language['id'])->count();
        $totalCount += EmbassiesTranslations::where('languages_id', $this->language['id'])->count();
        $progress = $confTranslationsProgress->create([
                                                    'language_title'    => $this->language['title'],
                                                    'language_code'     => $this->language['code'],
                                                    'event'             => 'update',
                                                    'total_count'       => $totalCount
                                                ]);


        $this->dispatch(
            (new TranslateCountries($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );

        $this->dispatch(
            (new TranslateCities($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );

        $this->dispatch(
            (new TranslateHotels($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );

        $this->dispatch(
            (new TranslatePlaces($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );

        $this->dispatch(
            (new TranslateRestaurants($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );

        $this->dispatch(
            (new TranslateEmbassies($this->language, $progress->id))
                ->onQueue('translateLevel2')
        );
    }
}
