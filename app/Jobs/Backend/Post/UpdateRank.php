<?php

namespace App\Jobs\Backend\Post;

use App\Http\Constants\CommonConst;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class UpdateRank implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $postId;
    protected $postType;
    protected $action;

    public function __construct($postId, $postType, $action)
    {
        $this->postId = $postId;
        $this->postType = $postType;
        $this->action = $action;
    }

    public function handle()
    {
        if (in_array($this->action, self::upvoteArray())) {
            $updateArray = [
                'rank' => DB::raw('rank + 1'),
                'rank_7' => DB::raw('rank_7 + 1')
            ];
        } else if (in_array($this->action, self::downvoteArray())) {
            $updateArray = [
                'rank' => DB::raw('rank - 1'),
                'rank_7' => DB::raw('rank_7 - 1')
            ];
        }

        if (!empty($updateArray) && !empty(CommonConst::postModels()[$this->postType])) {
            (CommonConst::postModels()[$this->postType])::whereId($this->postId)
                ->update($updateArray);
        }
    }

    private static function upvoteArray() {
        return [
            CommonConst::ACTION_LIKE,
            CommonConst::ACTION_COMMENT,
            CommonConst::ACTION_SHARE,
            CommonConst::ACTION_REPLY,
            CommonConst::ACTION_VIEW
        ];
    }

    private static function downvoteArray() {
        return [
            CommonConst::ACTION_UNLIKE,
            CommonConst::ACTION_UNCOMMENT,
            CommonConst::ACTION_UNSHARE,
            CommonConst::ACTION_DELETE_REPLY
        ];
    }
}
