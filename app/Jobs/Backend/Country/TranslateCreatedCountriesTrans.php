<?php

namespace App\Jobs\Backend\Country;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Country\CountriesTranslations;
use Dedicated\GoogleTranslate\Translator;

class TranslateCreatedCountriesTrans implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $default_trans;
    protected $trans;
    protected $language_code;

    /**
     * Create a new job instance.
     *
     * @param $defaultTrans
     * @param $trans
     * @param $languageCode
     */
    public function __construct($defaultTrans, $trans, $languageCode)
    {
        $this->default_trans    = $defaultTrans;
        $this->trans            = $trans;
        $this->language_code    = $languageCode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Translator $translator, CountriesTranslations $countriesTranslations)
    {
        $trans = [];
        foreach ($this->trans as $key => $value) {
            if (($value == null) && ($this->default_trans[$key] != null)) {

                $trans[$key] = $translator->setSourceLang('en')
                    ->setTargetLang($this->language_code)
                    ->translate($this->default_trans[$key]);
            } else {
                $trans[$key] = $this->trans[$key];
            }
        }
        $countriesTranslations->insert($trans);
    }
}
