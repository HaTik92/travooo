<?php

namespace App\Jobs\Backend\Country;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Country\CountriesTranslations;
use Dedicated\GoogleTranslate\Translator;

class TranslateUpdatedCountriesTrans implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $defaultTrans;
    protected $trans;
    protected $languageCode;
    /**
     * Create a new job instance.
     *
     * @param $defaultTrans
     * @param $trans
     * @param $languageCode
     */
    public function __construct($defaultTrans, $trans, $languageCode)
    {
        $this->defaultTrans    = $defaultTrans;
        $this->trans           = $trans;
        $this->languageCode    = $languageCode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Translator $translator)
    {
        $trans = [];
        unset($this->defaultTrans['id']);
        foreach ($this->trans as $key => $value) {
            if (($value == null) && ($this->defaultTrans[$key] != null)) {
                $trans[$key] = $translator->setSourceLang('en')
                    ->setTargetLang($this->languageCode)
                    ->translate($this->defaultTrans[$key]);
            } else {
                $trans[$key] = $this->trans[$key];
            }
        }
        $id = $this->trans['id'];
        unset($trans['id']);

        CountriesTranslations::find($id)->update($trans);
    }
}
