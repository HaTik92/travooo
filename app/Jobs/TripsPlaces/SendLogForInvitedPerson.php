<?php

namespace App\Jobs\TripsPlaces;

use App\Events\PlanLogs\PlanLogAddedEvent;
use App\Models\TripPlans\PlanActivityLog;
use App\Services\Trips\PlanActivityLogService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendLogForInvitedPerson implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $personId;

    /**
     * @var PlanActivityLog
     */
    private $log;

    /**
     * @var bool
     */
    private $hardRefresh;

    public function __construct($personId, $log, $hardRefresh = false)
    {
        $this->personId = $personId;
        $this->log = $log;
        $this->hardRefresh = $hardRefresh;
    }

    /**
     * Execute the job.
     *
     * @param PlanActivityLogService $planActivityLogService
     * @return void
     */
    public function handle(PlanActivityLogService $planActivityLogService)
    {
        try {
            broadcast(new PlanLogAddedEvent($this->personId, $planActivityLogService->prepareLogForResponse($this->log), $this->hardRefresh));
        } catch (\Exception $e) {
            $this->fail($e);
        }
    }
}