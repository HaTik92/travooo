<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Events\Events;
use App\Models\Events\EventCategories;
use App\Models\Events\EventCities;

class EventsFetch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:musment-events';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Events/activities from Musment weekly basis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ignore_user_abort(true);
        set_time_limit(0);
        $EventCities = EventCities::select(['tp_id', 'countries_id', 'cities_id'])->get();
        foreach ($EventCities as $ec) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.musement.com/api/v3/cities/" . $ec->tp_id . "/activities",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                continue;
            } else {
                $response = json_decode($response, true);
                if (isset($response['data'])) {
                    foreach ($response['data'] as $event) {
                        if (!Events::where(['provider_id' => $event['uuid']])->exists()) {
                            Events::create([
                                'countries_id' => $ec->countries_id,
                                'cities_id' => $ec->cities_id,
                                'city_id' => $ec->cities_id,
                                'provider_id' => $event['uuid'],
                                'category' => $event['categories'][0]['name'],
                                'title' => $event['title'],
                                'description' => $event['description'],
                                'lat' => isset($event['latitude']) ? $event['latitude'] : 0,
                                'lng' => isset($event['longitude']) ? $event['longitude'] : 0,
                                'variable' => json_encode($event),
                                'ratings' => $event['reviews_avg'],
                                'reviews' => $event['reviews_number']
                            ]);
                        }
                    }
                }
            }
        }
        $this->info("finish");
    }
}
