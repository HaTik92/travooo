<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User\User;
use App\Services\Common\AsyncSeedService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AsyncSeed extends Command
{
    protected $signature = 'seed {userId} {--type= } {--times= : how many you want to seed data }';
    protected $description = 'Seeding testing data';


    public function __construct()
    {
        parent::__construct();
        session(['seed' => true]);
    }

    public function handle(AsyncSeedService $seed)
    {
        $userId = $this->argument('userId');
        if (!User::find($userId)) {
            echo "invalid userid";
            session(['seed' => false]);
            return;
        }
        Auth::loginUsingId($userId);
        $options = $this->options();

        if (!isset($options['type'])) {
            echo "type must be required";
            session(['seed' => false]);
            return;
        }

        $type = $options['type'];

        if (!in_array($type, AsyncSeedService::TYPE_MAPPING)) {
            echo "type invalid";
            session(['seed' => false]);
            return;
        }

        try {
            $times = (int)($options['times'] ?? 1);
            if ($type == AsyncSeedService::TYPE_POST) {
                $posts = $seed->post($times);
                $seed->dumpLikes($type, $posts);
                echo print_r($posts);
            }
            //  else  if ($type == AsyncSeedService::TYPE_REVIEW) {
            //     $reviews = $seed->review($times);
            //     $seed->dumpLikes($type, $reviews);
            //     echo print_r($reviews);
            // }
            else  if ($type == AsyncSeedService::TYPE_TRIP) {
                echo $seed->trip();
            } else {
                echo 'seed not available';
            }
        } catch (\Throwable $th) {
            $errorInfo = [
                'error' => true,
                'message' => $th->getMessage(),
                'code' => $th->getCode(),
                'class' => get_class($th),
            ];
            Log::useDailyFiles(storage_path() . '/logs/cron.log');
            Log::emergency('API_SERVER_ERROR : ', $errorInfo, PHP_EOL . PHP_EOL);
            echo print_r($errorInfo);
        } finally {
            session(['seed' => false]);
        }
    }
}
