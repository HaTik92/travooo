<?php

namespace App\Console\Commands;

use App\Models\Country\Countries;
use App\Services\Trends\TrendsService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

set_time_limit(0);
ini_set('memory_limit', -1);

class UpdateTrendingsCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trendings:update-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates cache of the trending data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param TrendsService $trendsService
     * @return mixed
     */
    public function handle(TrendsService $trendsService)
    {
        DB::connection()->enableQueryLog();
        Log::useDailyFiles(storage_path() . '/logs/trendings-update-cache.log');
        $nationalities = Countries::query()->pluck('id')->toArray();

        Log::info('SUCCESS : ', ['message'  => "Start Process", 'total_countries' => count($nationalities), 'countries' => $nationalities], PHP_EOL . PHP_EOL);
        $trendsService->forceUpdateEsCache = true;
        try {
            foreach ($nationalities as $nationality) {
                $startTime = microtime(true);
                $trendsService->nationality = $nationality;
                $trendsService->getRecursiveTrendingLocations();
                Log::info('SUCCESS : ', ['responseTime' => number_format(microtime(true) - $startTime, 3), 'nationality' => $nationality, 'debugData' => $trendsService->debugData,], PHP_EOL . PHP_EOL);
            }
            Log::info('SUCCESS : ', ['responseTime' => number_format(microtime(true) - LARAVEL_START, 3), 'debugData' => $trendsService->debugData, 'message' => 'Finish All', 'queries_list' => DB::getQueryLog()], PHP_EOL . PHP_EOL);
        } catch (Exception $e) {
            Log::info('ERROR : ', ['responseTime' => number_format(microtime(true) - LARAVEL_START, 3), 'debugData' => $trendsService->debugData, 'message' => $e->getMessage()], PHP_EOL . PHP_EOL);
        }
    }
}
