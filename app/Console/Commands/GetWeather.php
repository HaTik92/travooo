<?php

namespace App\Console\Commands;

use App\Models\City\Cities;
use App\Jobs\Backend;
use Illuminate\Console\Command;

class GetWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:weather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get current and upcoming weather information.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities_count = Cities::count();

        $offset = 0;
        while ( $offset < $cities_count ) {
            dispatch(
                (new Backend\Weather\Get($offset, 100))->delay(180)
                    ->onQueue('getWeather')
            );
            $offset += 100;
        }
    }
}