<?php

namespace App\Console\Commands;

use App\Models\City\Cities;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class UpdateCitiesLocation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cities:update-location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = Cities::with('cityHolidays', 'country', 'trans')->get()->all();
        foreach ($cities as $city) {
            $iso_code = $city->country && $city->country->trans[0] ? $city->country->trans[0]->iso_code : null;
            if ($translation = $city->trans->first() ) {
                $params = [
                    'key' => env('GOOGLE-API-KEY'),
                    'address' => $translation['title'],
                    'region' => $iso_code
                ];
                $client = new Client();
                $response = json_decode($client->get('https://maps.googleapis.com/maps/api/geocode/json', [
                    'query' => $params
                ])->getBody());
                if ($response->results && $response->results[0]) {
                    $city->update(['lat' => $response->results[0]->geometry->location->lat,
                        'lng' => $response->results[0]->geometry->location->lng]);
                }
            }
        }
    }
}