<?php

namespace App\Console\Commands;

use App\Models\TripPlans\TripsSuggestion;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Console\Command;

class MigrateSuggestionsTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:suggestions:table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set is_saved = 1 and is_published = 1 to all approved suggestions';


    public function handle()
    {
        $suggestions = TripsSuggestion::where([
            'status' => TripsSuggestionsService::SUGGESTION_APPROVED_STATUS
        ])->update([
            'is_saved' => 1,
            'is_published' => 1,
        ]);
    }
}