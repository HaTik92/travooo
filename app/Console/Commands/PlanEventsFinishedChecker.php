<?php

namespace App\Console\Commands;

use App\Models\TripPlans\PlanActivityLog;
use App\Models\TripPlans\TripPlans;
use App\Services\Trips\PlanActivityLogService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class PlanEventsFinishedChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plans:events-finished:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if events of the plan finished';

    public function handle(PlanActivityLogService $planActivityLogService)
    {
        $now = Carbon::now()->startOfDay();
        $dayBefore = Carbon::now()->subDay()->startOfDay();

        $tripPlansWithFinishedDaysQuery = TripPlans::query()
            ->select('trips.*')
            ->where('trips.active', 1)
            ->join('trips_places', function($q) use ($now, $dayBefore) {
                $q->on('trips.active_version', 'trips_places.versions_id')
                    ->where('trips_places.time', '<', $now)
                    ->where('trips_places.time', '>=', $dayBefore);
            })
            ->with('trips_places')
            ->groupBy('trips.id');

        $eventDay = $dayBefore->format('Y-m-d');

        $tripPlansWithFinishedDaysQuery->chunk(100, function($tripPlansWithFinishedDays) use ($eventDay, $planActivityLogService) {
            foreach ($tripPlansWithFinishedDays as $tripPlansWithFinishedDay) {
                $count = 1;

                $activeTripsPlaces = $tripPlansWithFinishedDay->trips_places->where('versions_id', $tripPlansWithFinishedDay->active_version)->groupBy('date')->toArray();
                ksort($activeTripsPlaces);

                foreach ($activeTripsPlaces as $key => $activeTripsPlace) {
                    if ($key === $eventDay) {
                        $meta = [
                            'day' => $count
                        ];

                        $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_PLAN_TYPE_CHANGED, $tripPlansWithFinishedDay->id, null, auth()->id(), $meta);
                    }

                    $count++;
                }
            }
        });


    }
}