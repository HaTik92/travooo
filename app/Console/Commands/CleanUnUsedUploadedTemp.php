<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class CleanUnUsedUploadedTemp extends Command
{
    protected $signature    = 'clean:temp-uploaded-media';
    protected $description  = 'Cleaning Un Used Server Temp Uploaded Media';

    private $logDir, $fromCleanDate;

    public function __construct()
    {
        parent::__construct();
        $this->logDir = public_path('assets2/upload_tmp/');
        $this->fromCleanDate = date('Y-m-d', strtotime('-5 days'));
    }

    public function handle()
    {
        gc_collect_cycles();
        if (is_dir($this->logDir)) {
            if ($handle = opendir($this->logDir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..' || $file == '.gitignore') continue;
                    $filePath = $this->logDir . $file;
                    if (file_exists($filePath)) {
                        if (Carbon::parse(Carbon::parse(date("Y-m-d", filemtime($filePath))))->lt(Carbon::parse($this->fromCleanDate))) {
                            @unlink($filePath);
                        }
                    }
                }
                closedir($handle);
            }
        }

        // delete unsed trip thumb
        // $ignore = [
        //     '.',
        //     '..',
        //     '.gitignore',
        //     'app',
        //     'framework',
        //     'logs',
        //     'debugbar',
        //     'api-docs',
        //     'oauth-public.key',
        //     'oauth-private.key',
        // ];
        // if ($handle = opendir(storage_path())) {
        //     while (false !== ($file = readdir($handle))) {
        //         if (in_array($file, $ignore)) continue;
        //         if (strpos($file, "x") !== FALSE && (strpos($file, "_") !== FALSE) && (strpos($file, ".jpg") !== FALSE)) {
        //             unlink(storage_path($file));
        //         }
        //     }
        //     closedir($handle);
        // }
    }
}
