<?php

namespace App\Console\Commands;

use Carbon\Carbon as Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AddSuperAdminDataToRoleTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:super-admin';


    protected $description = 'Add Super Administrator to roles table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $role = [
            'name'       => 'Super Administrator',
            'all'        => true,
            'sort'       => 4,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        DB::table(config('access.roles_table'))->insert($role);
    }
}
