<?php

namespace App\Console\Commands;

use App\Models\DeleteAccount\DeleteReasons;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class FillDeleteReasonsList extends Command
{

    protected $signature = 'fill:delete-reasons-list';
    protected $description = 'Command description';


    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Schema::disableForeignKeyConstraints();
        DeleteReasons::truncate();
        Schema::enableForeignKeyConstraints();

        DeleteReasons::insert([
            ['title' => 'No interesting', 'status' => 1],
            ['title' => 'No useful', 'status' => 1],
            ['title' => 'Created second account', 'status' => 1],
            ['title' => 'Trouble getting started', 'status' => 1],
            ['title' => 'Want to remove something', 'status' => 1],
            ['title' => 'Too busy/too distracting', 'status' => 1],
            ['title' => 'Something else', 'status' => 1],
        ]);
    }
}
