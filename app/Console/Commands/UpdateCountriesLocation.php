<?php

namespace App\Console\Commands;

use App\Models\Country\Countries;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class UpdateCountriesLocation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'countries:update-location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $countries = Countries::with('transsingle')->get()->all();
        foreach ($countries as $country) {
            $iso_code = $country->iso_code;
            if ($translation = $country->transsingle ) {
                $params = [
                    'key' => env('GOOGLE-API-KEY'),
                    'address' => $translation['title'],
                    'region' => $iso_code
                ];
                $client = new Client();
                $response = json_decode($client->get('https://maps.googleapis.com/maps/api/geocode/json', [
                    'query' => $params
                ])->getBody());
                if ($response->results && $response->results[0]) {
                    $country->update(['lat' => $response->results[0]->geometry->location->lat,
                        'lng' => $response->results[0]->geometry->location->lng]);
                }
            }
        }
    }
}
