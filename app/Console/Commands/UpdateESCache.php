<?php

namespace App\Console\Commands;

use App\Models\City\Cities;
use App\Services\Trends\TrendsService;
use Illuminate\Console\Command;

class UpdateESCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'es-places:update-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates cache of the cities rests and hotels';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param TrendsService $trendsService
     * @return mixed
     */
    public function handle(TrendsService $trendsService)
    {
        $notPlacesTypes = array_merge(TrendsService::REST_TYPES, TrendsService::HOTELS_TYPES);

        $trendsService->forceUpdateEsCache = true;

        $citiesQuery = Cities::query()->select('id');
        $citiesQuery->chunk(100, function($cities) use ($trendsService, $notPlacesTypes) {
            foreach ($cities as $city) {
                $trendsService->getElasticPlaces($city->id, $notPlacesTypes);
                $this->info("Generated ES cache for city_id: " . $city->id);
            }
        });

    }
}
