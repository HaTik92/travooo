<?php

namespace App\Console\Commands\Seeds;

use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Discussion\DiscussionLikes;
use App\Models\Discussion\DiscussionMedias;
use App\Models\Discussion\DiscussionTopics;
use App\Models\Place\Place;
use App\Models\UsersFollowers\UsersFollowers;
use App\Models\User\User;
use App\Models\Discussion\Discussion;
use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;

class DiscussionSeed extends Command
{
    protected $signature = 'seed-discussion {userId}';
    protected $description = 'Seeding testing discussion data';

    public function handle(Generator $faker)
    {
        $baseCacheKey = "CACHE_" . last(explode("\\", get_class($this)));
        $userId = $this->argument('userId');
        $count = rand(3, 6);

        User::findOrFail($userId);
        $myFollowigs = $this->getFollowers($userId);
        $topics = DiscussionTopics::select('topics')->inRandomOrder()->limit(100)->pluck('topics');

        $now = Carbon::now();

        try { // optimize
            $destinationTypesKey = $baseCacheKey . "_destinationTypes";
            if (Cache::has($destinationTypesKey)) {
                $destinationTypes = Cache::get($destinationTypesKey);
            } else {
                $destinationTypes = $this->getDestinationTypes();
                Cache::put($destinationTypesKey, $destinationTypes, 100);
            }
        } catch (\Throwable $th) { // Cache folder permission issue
            $destinationTypes = $this->getDestinationTypes();
        }

        $newDiscussionId = [];
        $discussionTopicsData = [];
        $discussionLikesData = [];
        $discussionMedias = [];

        while ($count > 0) {
            $destination = $destinationTypes->random();
            $randomUserId = $myFollowigs->random();

            try {
                switch ($destination['type']) {
                    case 'city':
                        $country_id = Cities::find($destination['ids']->random())->country->id;
                        break;
                    case 'place':
                        $country_id = Place::find($destination['ids']->random())->country->id;
                        break;
                    default:
                        $country_id = $destination['ids']->random();
                }
            } catch (\Throwable $th) {
                $country_id = NULL;
            }

            $id = Discussion::insertGetId([
                'users_id' => $randomUserId,
                'type' => 1,
                'destination_type' => $destination['type'],
                'destination_id' => $destination['ids']->random(),
                'question' => $faker->sentence,
                'description' => $faker->paragraph,
                'language_id' => 1,
                'countries_id' => $country_id,
                'created_at' => $now,
                'updated_at' => $now,
            ]);

            // display newsfeed 
            log_user_activity('Discussion', 'create', $id, 0, User::find($randomUserId));

            if (count($topics)) {
                $discussionTopicsData[] = [
                    'discussions_id' => $id,
                    'topics' => $topics->random(),
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            }
            $imageCount = rand(1, 5);
            while ($imageCount > 0) {
                try {
                    $filename = $randomUserId . '_' . time() . '_' . $faker->md5 . 'jpg';
                    Storage::disk('s3')->put('discussion-photo/' . $filename, fopen($faker->image(), 'r+'), 'public');
                    $discussionMedias[] = [
                        'discussion_id' => $id,
                        'media_url' => S3_BASE_URL . 'discussion-photo/' . $filename,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                } catch (\Exception $e) {
                }
                $imageCount--;
            }

            $newDiscussionId[] = $id;
            $count--;
        }
        $popularCounts = [];
        $randomUser = User::inRandomOrder()->limit(100)->pluck('id')->toArray();
        foreach ($newDiscussionId as $id) {
            for ($i = rand(1, 40); $i > 0; $i--) {
                $flag = rand(0, 1);
                if (!isset($popularCounts[$id])) {
                    $popularCounts[$id] = 0;
                }
                $discussionLikesData[] = [
                    'discussions_id' => $id,
                    'users_id' => $randomUser[$i - 1],
                    'vote' => $flag
                ];
                if ($flag) {
                    $popularCounts[$id] += 1;
                } else {
                    $popularCounts[$id] -= 1;
                }
            }
        }

        DiscussionLikes::insert($discussionLikesData);
        DiscussionTopics::insert($discussionTopicsData);
        DiscussionMedias::insert($discussionMedias);

        // update popular_count based on upvotes likes
        foreach ($popularCounts as $discussionId => $popular_count) {
            $dis = Discussion::find($discussionId);
            if ($dis) {
                $dis->popular_count = $popular_count;
                $dis->save();
            }
        }
    }

    private function getFollowers(int $userId)
    {
        $myFollowigs = UsersFollowers::where('followers_id', $userId)->where('follow_type', 1)
            ->take(10)->pluck('users_id');

        if (count($myFollowigs) < 10) {
            $notIn = count($myFollowigs) ? 'and users_id NOT IN (' . implode(',', $myFollowigs->toArray()) . ') ' : '';
            $weeklyTrendingUser = User::query()
                ->selectRaw('id, (
                        (select count(*) from `posts` where users_id=`users`.`id` ' . $notIn . ' and date >  "' . Carbon::now()->subDays(7) . '") 
                        + (select count(*) from `trips` where users_id=`users`.`id` ' . $notIn . ' and created_at >  "' . Carbon::now()->subDays(7) . '") 
                        + (select count(*) from `reports` where users_id=`users`.`id` ' . $notIn . ' and created_at >  "' . Carbon::now()->subDays(7) . '") 
                        + (select count(*) from `discussions` where users_id=`users`.`id` ' . $notIn . ' and created_at >  "' . Carbon::now()->subDays(7) . '") 
                    ) as ranks')
                ->orderBy('ranks', 'desc')
                ->take(10)->get();
            $allTimeUsers = User::query()
                ->selectRaw('id, (
                        (select count(*) from `posts` where users_id=`users`.`id` ' . $notIn . ') 
                        + (select count(*) from `trips` where users_id=`users`.`id` ' . $notIn . ') 
                        + (select count(*) from `reports` where users_id=`users`.`id` ' . $notIn . ') 
                        + (select count(*) from `discussions` where users_id=`users`.`id` ' . $notIn . ') 
                    ) as ranks')
                ->orderBy('ranks', 'desc')
                ->take(10 + (10 - count($weeklyTrendingUser)))->get();

            $newFollower = $weeklyTrendingUser->merge($allTimeUsers)->pluck('id')->unique()->toArray();

            $queryData = [];
            foreach ($newFollower as $id) {
                if ($userId == $id) continue;
                $queryData[] = ['followers_id' => $userId, 'follow_type' => 1, 'users_id' => $id];
            }
            UsersFollowers::insert($queryData);
            $myFollowigs = UsersFollowers::where('followers_id', $userId)->where('follow_type', 1)
                ->take(10)->pluck('users_id');
        }
        return $myFollowigs;
    }

    private function getDestinationTypes()
    {
        return collect([
            ['type' => 'place', 'ids' => Place::withCount('followers')->orderBy('followers_count', 'DESC')->take(50)->get()->pluck('id')],
            ['type' => 'country', 'ids'  => Countries::withCount('followers')->orderBy('followers_count', 'DESC')->take(50)->get()->pluck('id')],
            ['type' => 'city', 'ids'  => Cities::withCount('followers')->orderBy('followers_count', 'DESC')->take(50)->get()->pluck('id')]
        ]);
    }
}
