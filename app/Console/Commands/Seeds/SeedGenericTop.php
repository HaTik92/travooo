<?php
namespace App\Console\Commands\Seeds;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SeedGenericTop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed-generic-tops';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    public function handle()
    {
        $topCountriesName = [
            'France',
            'Spain',
            'United States',
            'China',
            'Italy',
            'Turkey',
            'Mexico',
            'Germany',
            'Thailand',
            'United Kingdom',
        ];

        $topCitiesName = [
            'Venice',
            'Paris',
            'Barcelona',
            'London',
            'Bangkok',
            'Dubai',
            'Singapore',
            'New York',
            'Istanbul',
            'Tokyo',
        ];

        $topPlaces = [
            '256033' => "Niagara Whirlpool",
            '2614525' => "Statue of Liberty National Monument",
            '1583762' => "Lindos Acropolis",
            '5798302' => "Gurudwara Pathar Sahib",
            '3341416' => "The Pyramids Of Giza",
            '3494423' => "The Hashemite Plaza",
            '3352767' => "Pintu Masuk DUFAN (Dunia Fantasi)",
            '2999' => "Eiffel Tower",
            '415895' => "Taj Mahal",
            '4988023' => "Arashiyama Bamboo Forest",
            '714505' => "machu picchu",
            '1168530' => "Cappadocia Balloons",
            '3752954' => "Bora Bora",
            '223271' => "Neuschwanstein Castle",
            '2232' => "Colosseum",
            '5759798' => "Tikal",
            '437416' => "Mount Fuji",
            '5760235' => "Batu Caves",
            '3798397' => "Stonehenge",
            '180080' => "Amalfi Coast",
        ];

        if (!DB::table('generic_top_countries')->count()) {
            $topCountries = DB::table('countries_trans')->select('countries_id')->whereIn('title', $topCountriesName)->get();
            DB::table('generic_top_countries')->insert(
                $topCountries->map(function ($countryTrans) {
                    return ['countries_id' => $countryTrans->countries_id];
                })->toArray());
            echo "Countries seeded\n";
        }

        if (!DB::table('generic_top_cities')->count()) {
            $topCitiesIds = DB::table('cities_trans')->select('cities_id');
            foreach ($topCitiesName as $city) {
                $topCitiesIds->orWhere('title', 'like', "$city%");
            }
            $topCities = DB::table('cities')->select('id', 'countries_id')
                ->whereIn('id', $topCitiesIds->pluck('cities_id')->toArray())
                ->get();
            DB::table('generic_top_cities')->insert(
                $topCities->map(function ($city) {
                    return ['cities_id' => $city->id, 'countries_id' => $city->countries_id];
                })->toArray());
            echo "Cities seeded\n";
        }

        if (!DB::table('generic_top_places')->count()) {
            $topPlaces = DB::table('places')->whereIn('id', array_flip($topPlaces))->get();
            DB::table('generic_top_places')->insert(
                $topPlaces->map(function ($place) {
                    return ['cities_id' => $place->cities_id, 'countries_id' => $place->countries_id, 'places_id' => $place->id];
                })->toArray());
            echo "Places seeded\n";
        }

    }
}