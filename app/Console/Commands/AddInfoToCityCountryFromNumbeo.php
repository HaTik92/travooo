<?php

namespace App\Console\Commands;

use App\Jobs\Backend\Api\UpdateCityCountryFromNumbeo;
use Illuminate\Console\Command;

class AddInfoToCityCountryFromNumbeo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:from-numbeo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add information to cities and countries from numbeo api';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Command start');
        dispatch(new UpdateCityCountryFromNumbeo());
        $this->info('Command end');
    }
}
