<?php

namespace App\Console\Commands;

use App\Models\Posts\Posts;
use App\Models\User\User;
use App\Models\User\UsersFriends;
use App\Models\UsersFollowers\UsersFollowers;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CreateRecommendedPlaces extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:create-recommended-places';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create collective posts to show suggested places based on user travel styles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $friends = UsersFriends::all()->pluck('users_id')->toArray();
        $followers = UsersFollowers::all()->pluck('users_id')->toArray();

        $ids = array_unique(array_merge($friends, $followers));

        $users = User::whereIn('id', $ids)->with(['friends', 'followings'])->get();

        $users->each(function($user) {

            $places = [];

            $user->friends->each(function($friend) use (&$places) {
                $places = array_merge($places,
                    $friend->friend
                        ->followedPlaces
                        ->filter(function($place) {
                            return $place->created_at->greaterThan(Carbon::now()->subDays(3));
                        })
                        ->pluck('places_id')->toArray());
            });

            $user->followings->each(function($friend) use (&$places) {
                $places = array_merge($places,
                    $friend->following
                        ->followedPlaces
                        ->filter(function($place) {
                            return $place->created_at->greaterThan(Carbon::now()->subDays(3));
                        })
                        ->pluck('places_id')->toArray());
            });

            $friends = $user->friends->pluck('friends_id')->toArray();
            $followings = $user->followings->pluck('users_id')->toArray();

            $followers = array_merge($friends, $followings);

            $post_data = [
                'places' => $places,
                'followers' => $followers
            ];

            $post_data = json_encode($post_data);

            $post = new Posts();

            $post->users_id = $user->id;
            $post->text = $post_data;
            $post->save();

            log_user_activity('RecommendedPlaces', 'show', $post->id, 0, $user);

        });
    }
}
