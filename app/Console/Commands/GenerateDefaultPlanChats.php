<?php

namespace App\Console\Commands;

use App\Models\Chat\ChatConversation;
use App\Models\Chat\ChatConversationParticipant;
use App\Models\TripPlans\TripPlans;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Console\Command;

class GenerateDefaultPlanChats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:default:plan-chats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate default group chat for every plan';


    public function handle(TripsSuggestionsService $tripsSuggestionsService, TripInvitationsService $tripInvitationsService)
    {
        $plansQuery = TripPlans::query()->orderBy('created_at');

        $plansQuery->chunk(100, function($plans) use ($tripsSuggestionsService, $tripInvitationsService) {
            foreach ($plans as $tripPlan) {
                $invitedPeople = $tripInvitationsService->getInvitedPeopleIds($tripPlan->id);
                $invitedPeopleIds = array_keys($invitedPeople);

                $invitedPeopleIds[] = $tripPlan->users_id;

                $conversation = ChatConversation::create([
                    'created_by_id' => $tripPlan->users_id,
                    'subject' => $tripPlan->title,
                    'plans_id' => $tripPlan->id
                ]);

                foreach ($invitedPeopleIds as $invitedPeopleId) {
                    ChatConversationParticipant::create([
                        'chat_conversation_id' => $conversation->id,
                        'user_id' => $invitedPeopleId
                    ]);
                }
            }
        });
    }
}
