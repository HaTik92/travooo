<?php

namespace App\Console\Commands;

use App\Models\TripPlans\TripsSuggestion;
use App\Models\User\User;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class SetThumbForAvatars extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:avatar:thumbs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate cropped version for all avatars';


    public function handle()
    {
        $users = User::all();

        $bar = $this->output->createProgressBar(count($users));

        $bar->start();

        foreach ($users as $user) {
            $amazonfilename = explode('/', $user->profile_picture);

            if (!$amazonfilename) {
                $this->line('');
                $this->warn('Original avatar not found for user: ' . $user->id);
                $bar->advance();
                continue;
            }

            $amazonfilename = end($amazonfilename);

            if (!$amazonfilename) {
                $this->line('');
                $this->warn('Original avatar not found for user: ' . $user->id);
                $bar->advance();
                continue;
            }

            try {
                $originalImg = Storage::disk('s3')->get('users/profile/original/' . $user->id . '/' . $amazonfilename);
            } catch (\Exception $exception) {
                $this->line('');
                $this->warn('Original avatar not found for user: ' . $user->id);
                $bar->advance();
                continue;
            }

            if (!$originalImg) {
                $this->line('');
                $this->warn('Original avatar not found for user: ' . $user->id);
                $bar->advance();
                continue;
            }

            if (Storage::disk('s3')->exists('users/profile/th180/' . $user->id . '/' . $amazonfilename)) {
                $this->line('');
                $this->info('Cropped avatar already exists for user ' . $user->id);
                $bar->advance();
                continue;
            }

            $cropped = Image::make($originalImg)->resize(180, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            Storage::disk('s3')->put(
                'users/profile/th180/' . $user->id . '/' . $amazonfilename, $cropped->encode(), 'public'
            );

            $this->line('');
            $this->info('Cropped avatar generated for user: ' . $user->id);

            $bar->advance();
        }

        $bar->finish();
    }
}