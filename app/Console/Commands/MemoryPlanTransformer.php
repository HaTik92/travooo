<?php

namespace App\Console\Commands;

use App\Models\TripPlans\PlanActivityLog;
use App\Models\TripPlans\TripPlans;
use App\Services\Trips\PlanActivityLogService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MemoryPlanTransformer extends Command
{
    const CONFIRMED_MEMORY_AFTER_DAYS = 10;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plans:transform';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transform upcoming plans to memory plans';

    public function handle(PlanActivityLogService $planActivityLogService)
    {
        $memoryTripPlansToMakeUpcomingQuery = TripPlans::query()
            ->where('memory', 1)
            ->where('active', 1)
            ->whereHas('trips_places', function ($query) {
                $now = Carbon::now();
                return $query->where('date', '>', $now);
            })->orderBy('created_at');

        $memoryTripPlansToMakeUpcomingQuery->chunk(100, function($memoryTripPlansToMakeUpcoming) use ($planActivityLogService) {
            foreach ($memoryTripPlansToMakeUpcoming as $tripPlan) {
                $tripPlan->memory = 0;
                $tripPlan->is_memory_confirmed = 0;
                $tripPlan->save();
            }
        });

        $tripPlansToMakeMemoryQuery = TripPlans::query()
            ->where('memory', 0)
            ->where('active', 1)
            ->whereDoesntHave('trips_places', function ($query) {
                $now = Carbon::now();
                return $query->where('date', '>', $now);
            })->orderBy('created_at');

        $tripPlansToMakeMemoryQuery->chunk(100, function($tripPlansToMakeMemory) use ($planActivityLogService) {
            foreach ($tripPlansToMakeMemory as $tripPlan) {
                $tripPlan->is_memory_confirmed = 1;
                $tripPlan->save();
            }
        });

        $tripPlansToMakeMemoryConfirmedQuery = TripPlans::query()
            ->where('memory', 0)
            ->where('active', 1)
            ->where('is_memory_confirmed', 1)
            ->whereDoesntHave('trips_places', function ($query) {
                $now = Carbon::now();
                $daysAgo = Carbon::now()->subDays(self::CONFIRMED_MEMORY_AFTER_DAYS);
                return $query->where('date', '>', $now)->where('created_at', '>', $daysAgo);
            });

        $tripPlansToMakeMemoryConfirmedQuery->chunk(100, function($tripPlansToMakeMemoryConfirmed) use ($planActivityLogService) {
            foreach ($tripPlansToMakeMemoryConfirmed as $tripPlan) {
                $tripPlan->memory = 1;
                $tripPlan->is_memory_confirmed = 1;
                $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_PLAN_TYPE_CHANGED_TO_MEMORY, $tripPlan->id, null, auth()->id());
                $tripPlan->save();
            }
        });
    }
}