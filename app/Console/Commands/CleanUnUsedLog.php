<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class CleanUnUsedLog extends Command
{
    protected $signature    = 'clean:logs';
    protected $description  = 'Cleaning Un Used Server Log';

    private $logDir;

    public function __construct()
    {
        parent::__construct();
        $this->logDir = storage_path('logs') . "/";
    }

    public function handle()
    {
        $this->cleanDir([
            'trendings-update-cache', 'chat_list',
        ], date('Y-m-d', strtotime('-5 days')), $this->logDir);

        $this->cleanDir([
            'api-newsfeed'
        ], date('Y-m-d', strtotime('-7 days')), $this->logDir);

        $this->cleanDir([
            'debug_api', 'restapi', 'laravel'
        ], date('Y-m-d', strtotime('-20 days')), $this->logDir);

        echo "done";
    }

    private function cleanDir($cleanFilePreFix, $cleanLogDate, $logDir)
    {
        if (is_dir($logDir)) {
            if ($handle = opendir($logDir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..' || $file == '.gitignore') continue;
                    foreach ($cleanFilePreFix as $prefix) {
                        if (strpos($file, $prefix) !== false) {
                            $fileCreatedDate = str_replace([$prefix . "-", ".log"], "", $file);;
                            if ($fileCreatedDate != "" &&  $cleanLogDate != "" && Carbon::parse($fileCreatedDate)->lt(Carbon::parse($cleanLogDate))) {
                                $filePath = $logDir . $file;
                                if (file_exists($filePath)) {
                                    @unlink($filePath);
                                }
                            }
                        }
                    }
                }
                closedir($handle);
            }
        }
    }
}
