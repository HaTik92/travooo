<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Place\Place;

class AddMediasCountToPlaces extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:media_count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fill places.media_count column';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $i = 0;
        while ( count($places = Place::offset($i)->limit(100)->get()) ) {
            foreach ($places as $place) {
                if ($place->medias->isNotEmpty()) {
                    $place->update(['media_count' => count($place->medias)]);
                }
            }
            $i += 100;
        }
    }
}
