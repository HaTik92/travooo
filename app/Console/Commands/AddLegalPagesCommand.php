<?php

namespace App\Console\Commands;

use App\Models\Access\Language\Languages;
use App\Models\CommonPage\CommonPage;
use App\Models\CommonPage\CommonPageTranslations;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddLegalPagesCommand extends Command
{
    protected $signature = 'add:legal-pages';
    protected $description = 'Add Legal Pages Like Privacy Page, Terms Page etc...';

    private $languages;

    public function __construct()
    {
        parent::__construct();
        $this->languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    public function handle()
    {
        Schema::disableForeignKeyConstraints();
        CommonPage::truncate();
        CommonPageTranslations::truncate();
        Schema::enableForeignKeyConstraints();

        foreach ($this->pages() as $page) {
            $commonPage = CommonPage::create([
                'slug' => $page['slug'],
                'active' => CommonPage::ACTIVE
            ]);

            foreach ($this->languages as $language) {
                CommonPageTranslations::create([
                    'common_page_id' => $commonPage->id,
                    'languages_id' => $language->id,
                    'title' => $page['title'],
                    'description' => $page['description'],
                ]);
            }
        }
    }

    public function pages()
    {
        return [
            [
                'slug' => CommonPage::SLUG_PRIVACY_POLICY,
                'title' => CommonPage::SLUG_MAPPING[CommonPage::SLUG_PRIVACY_POLICY],
                'description' => '<div class="basic-contain-area py-4">
    <p>Your privacy is critically important to us. At Dribbble we have a few fundamental principles:</p>
    <ol class="ml-3 pl-1">
        <li> We don’t ask you for personal information unless we truly need it. (We can’t stand services that ask you for things like your gender or income level for no apparent reason.)</li>
        <li>We don’t share your personal information with anyone except to comply with the law, develop our products, or protect our rights.</li>
        <li>We don’t store personal information on our servers unless required for the on-going operation of one of our services.</li>
    </ol>
    <p>Below is our privacy policy which incorporates these goals:</p>
    <p>Dribbble Holdings Ltd (Dribbble) operates dribbble.com. It is Dribbble’s policy to respect your privacy regarding any information we may collect while operating our websites.</p>

    <h4 class="py-2"><b>Website Visitors</b></h4>
    <p>Like most website operators, Dribbble collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Dribbble’s purpose in collecting non-personally identifying information is to better understand how Dribbble’s visitors use its website. From time to time, Dribbble may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.</p>
    <p>Dribbble also collects potentially personally-identifying information like Internet Protocol (IP) addresses. Dribbble does not use such information to identify its visitors, however, and does not disclose such information, other than under the same circumstances that it uses and discloses personally-identifying information, as described below.</p>

    <h4 class="py-2"><b>Gathering of Personally-Identifying Information</b></h4>
    <p>Certain visitors to Dribbbles websites choose to interact with Dribbble in ways that require Dribbble to gather personally-identifying information. The amount and type of information that Dribbble gathers depends on the nature of the interaction. For example, we ask visitors who sign up account at dribbble.com to provide a username and email address. Dribbble collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitors interaction with Dribbble. Dribbble does not disclose personally-identifying information other than as described below. And visitors can always refuse to supply personally-identifying information, with the caveat that it may prevent them from engaging in certain website-related activities.</p>

    <h4 class="py-2"><b>Aggregated Statistics</b></h4>
    <p>Dribbble may collect statistics about the behavior of visitors to its websites. Dribbble may display this information publicly or provide it to others. However, Dribbble does not disclose personally-identifying information other than as described below.</p>

    <h4 class="py-2"><b>Protection of Certain Personally-Identifying Information</b></h4>
    <p>Dribbble discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors and affiliated organizations that (i) need to know that information in order to process it on Dribbbles behalf or to provide services available at Dribbbles websites, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of your home country; by using Dribbbles websites, you consent to the transfer of such information to them. Dribbble will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors and affiliated organizations, as described above, Dribbble discloses potentially personally-identifying and personally-identifying information only when required to do so by law, or when Dribbble believes in good faith that disclosure is reasonably necessary to protect the property or rights of Dribbble, third parties or the public at large. If you are a registered user of an Dribbble website and have supplied your email address, Dribbble may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with whats going on with Dribbble and our products. We primarily use our various product blogs to communicate this type of information, so we expect to keep this type of email to a minimum. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. Dribbble takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.</p>

    <h4 class="py-2"><b>Cookies</b></h4>
    <p>A cookie is a string of information that a website stores on a visitors computer, and that the visitors browser provides to the website each time the visitor returns. Dribbble uses cookies to help Dribbble identify and track visitors, their usage of Dribbble website, and their website access preferences. Dribbble visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using Dribbbles websites, with the drawback that certain features of Dribbbles websites may not function properly without the aid of cookies.</p>

    <h4 class="py-2"><b>Privacy Policy Changes</b></h4>
    <p>Although most changes are likely to be minor, Dribbble may change its Privacy Policy from time to time, and in Dribbbles sole discretion. Dribbble encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.</p>
    
    <p class="light-info">This Privacy Policy was crafted from Wordpress.com\'s version, which is available under a Creative Commons Sharealike license.</p>
</div>'
            ],
            [
                'slug' => CommonPage::SLUG_TERMS_OF_SERVICE,
                'title' => CommonPage::SLUG_MAPPING[CommonPage::SLUG_TERMS_OF_SERVICE],
                'description' => '<div class="basic-contain-area py-4">
    <p style="text-indent: 30px">This agreement was written in English (US). To the extent any translated version of this agreement conflicts with the English version, the English version controls.  Please note that Section 16 contains certain changes to the general terms for users outside the United States.</p>
    
    <h4 class="py-2"><b>Statement of Rights and Responsibilities</b></h4>
    <p>his Statement of Rights and Responsibilities ("Statement," "Terms," or "SRR") derives from the Facebook Principles, and is our terms of service that governs our relationship with users and others who interact with Facebook, as well as Facebook brands, products and services, which we call the “Facebook Services” or “Services”. By using or accessing the Facebook Services, you agree to this Statement, as updated from time to time in accordance with Section 13 below. Additionally, you will find resources at the end of this document that help you understand how Facebook works.
    Because Facebook provides a wide range of Services, we may ask you to review and accept supplemental terms that apply to your interaction with a specific app, product, or service. To the extent those supplemental terms conflict with this SRR, the supplemental terms associated with the app, product, or service govern with respect to your use of such app, product or service to the extent of the conflict.</p>

     <ol class="ml-3 pl-1">
        <li>
            <h4 class="py-2"><b>Privacy</b></h4>
            <p>Your privacy is very important to us. We designed our Data Policy to make important disclosures about how you can use Facebook to share with others and how we collect and can use your content and information. We encourage you to read the Data Policy, and to use it to help you make informed decisions.</p>
        </li>
        <li>
            <h4 class="py-2"><b>Sharing Your Content and Information</b></h4>
            <p>You own all of the content and information you post on Facebook, and you can control how it is shared through your privacy and application settings. In addition:</p>
            <p>For content that is covered by intellectual property rights, like photos and videos (IP content), you specifically give us the following permission, subject to your privacy and application settings: you grant us a non-exclusive, transferable, sub-licensable, royalty-free, worldwide license to use any IP content that you post on or in connection with Facebook (IP License). This IP License ends when you delete your IP content or your account unless your content has been shared with others, and they have not deleted it.</p>
            <p>When you delete IP content, it is deleted in a manner similar to emptying the recycle bin on a computer. However, you understand that removed content may persist in backup copies for a reasonable period of time (but will not be available to others).</p>
            <p>When you use an application, the application may ask for your permission to access your content and information as well as content and information that others have shared with you.  We require applications to respect your privacy, and your agreement with that application will control how the application can use, store, and transfer that content and information.  (To learn more about Platform, including how you can control what information other people may share with applications, read our Data Policy and Platform Page.)</p>
            <p>When you publish content or information using the Public setting, it means that you are allowing everyone, including people off of Facebook, to access and use that information, and to associate it with you (i.e., your name and profile picture).</p>
            <p>We always appreciate your feedback or other suggestions about Facebook, but you understand that we may use your feedback or suggestions without any obligation to compensate you for them (just as you have no obligation to offer them).</p>
        </li>
    </ol>
</div>'
            ]
        ];
    }
}
