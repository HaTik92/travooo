<?php

namespace App\Console\Commands;

use App\Models\PlacesTop\PlacesTop;
use App\Models\Posts\Posts;
use App\Models\User\User;
use Illuminate\Console\Command;

class CreateTrendingDestinations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:trending-destinations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create trending destinations the user is not following yet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        $topPlaces = PlacesTop::all()->pluck('places_id')->toArray();

        $users->each(function($user) use($topPlaces) {
            $followedPlaces = $user->following_places->pluck('places_id')->toArray();

            $diff = array_diff($topPlaces, $followedPlaces);

            if(count($diff) && $user->id == 120) {
                $post_data = json_encode($diff);

                $post = new Posts();

                $post->users_id = $user->id;
                $post->text = $post_data;
                $post->save();

                log_user_activity('TrendingDestinations', 'show', $post->id, 0, $user);
            }
        });
    }
}
