<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncDbFunctionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add-db-functions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add or Modify mysql custom function';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $baseUrl = __DIR__ . "/../../../";
        $this->info("Start...");
        DB::unprepared(file_get_contents($baseUrl . "database/sql/fun_basic_algo.sql"));
        DB::unprepared(file_get_contents($baseUrl . "database/sql/fun_user_access_denied.sql"));
        $this->info("End...");
    }
}
