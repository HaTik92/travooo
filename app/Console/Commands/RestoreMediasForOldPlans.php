<?php

namespace App\Console\Commands;

use App\Models\TripMedias\TripMedias;
use App\Models\TripPlaces\TripPlaces;
use Illuminate\Console\Command;

class RestoreMediasForOldPlans extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plans:restore:medias';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore medias for old plan places';

    public function handle()
    {
        TripMedias::query()->whereNull('trip_place_id')->orderBy('created_at')->chunk(100, function ($tripMedias) {
            foreach ($tripMedias as $tripMedia) {
                $tripPlace = TripPlaces::query()->where([
                    'trips_id' => $tripMedia->trips_id,
                    'places_id' => $tripMedia->places_id
                ])->latest()->first();

                if (!$tripPlace) {
                    continue;
                }

                $tripMedia->trip_place_id = $tripPlace->id;
                $tripMedia->save();
            }
        });
    }
}