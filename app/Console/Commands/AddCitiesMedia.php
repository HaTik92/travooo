<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\City\Cities;

use App\Services\Medias\LocationsAddMediaService;


class AddCitiesMedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:cities-medias';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add citites medias if count less then 5';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(LocationsAddMediaService $locationsAddMediaService)
    {
        $offset = 0;
        while ( count($cities = Cities::offset($offset)->limit(100)->get()) ) {
            foreach ($cities as $city) {
                $locationsAddMediaService->addCityMedia($city);
            }
            $offset += 100;
        }
    }
}
