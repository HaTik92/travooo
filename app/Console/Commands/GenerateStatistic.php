<?php

namespace App\Console\Commands;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateStatistic extends Command
{
    use CreateUpdateStatisticTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:statistic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate statistic for cities and countries';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('cities_statistics')->truncate();
        DB::table('countries_statistics')->truncate();
        $this->generateCitiesStatistic();
        $this->generateCountriesStatistic();
    }

    public function generateCitiesStatistic() {
        $cities = Cities::all();
        foreach ($cities as $city) {
            $tripCities = DB::table('trips_places')->where('cities_id', $city->id)->get();
            DB::table('cities_statistics')->insert([
                'cities_id' => $city->id,
                'medias' => count($city->medias),
                'followers' => count($city->followers),
                'trips' => count($tripCities),
                'places' => count($city->places)
            ]);
        }
    }

    public function generateCountriesStatistic() {
        $countries = Countries::all();
        foreach ($countries as $country) {
            $tripCountries = DB::table('trips_places')->where('countries_id', $country->id)->get();
            DB::table('countries_statistics')->insert([
                'countries_id' => $country->id,
                'medias' => count($country->medias),
                'followers' => count($country->followers),
                'trips' => count($tripCountries),
                'places' => count($country->places),
                'cities' => count($country->cities)
            ]);
        }
    }
}
