<?php

namespace App\Console\Commands;

use App\Models\Posts\Posts;
use App\Models\User\User;
use Illuminate\Console\Command;

class CreateRecommendedCountriesCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:create-recommended-countries-cities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create collective posts to show suggested cities and countries based on user travel styles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::with(
            [
                'travelstyles.countriesLifestyles',
                'travelstyles.citiesLifestyles',
                'followingCountries',
                'followingCities'
            ])
            ->whereHas('travelstyles')->get();

        $users->each(function($user) {
            $followingCountries = $user->followingCountries;
            $followingCities = $user->followingCities;

            $placesCount = $followingCountries->count() + $followingCities->count();

            if($placesCount < 20) {
                $tsCountries = collect([]);
                $tsCities = collect([]);

                $user->travelstyles->each(function($ts) use(&$tsCountries, &$tsCities) {
                    $tsCountries = $tsCountries->merge($ts->countriesLifestyles->pluck('countries_id'));
                    $tsCities = $tsCities->merge($ts->citiesLifestyles->pluck('cities_id'));
                });

                $post_data = [
                    'countries' => $tsCountries->unique(),
                    'cities' => $tsCities->unique()
                ];

                $post_data = json_encode($post_data);

                $post = new Posts();

                $post->users_id = $user->id;
                $post->text = $post_data;
                $post->save();

                log_user_activity('RecommendedCountriesCities', 'show', $post->id, 0, $user);
            }
        });
    }
}
