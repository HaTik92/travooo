<?php

namespace App\Console\Commands;

use App\Http\Constants\CommonConst;
use App\Models\Discussion\Discussion;
use App\Models\Logs\RankCronLog;
use App\Models\Posts\Posts;
use App\Models\Reports\Reports;
use App\Models\TripPlans\TripPlans;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClalculateRanks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate-ranks {type?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate post ranks';

    const TYPE_ALL_TIME = 'all_time';
    const COUNT = 1000;

    protected $cronId;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->cronId = uniqid();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allTime = $this->argument('type') === self::TYPE_ALL_TIME;
        $creation = Carbon::now()->subDays(7);

        $this->calculatePostRanks($allTime, $creation);
        $this->calculateTripRanks($allTime, $creation);
        $this->calculateReportRanks($allTime, $creation);
        $this->calculateDiscussionRanks($allTime, $creation);
    }

    protected function calculatePostRanks ($allTime, $creation)
    {
        $count = self::COUNT;
        $loopCount = 0;
        $skip = 0;
        $this->logCron(CommonConst::TYPE_POST, RankCronLog::ACTION_START, $allTime);
        do {
            $posts = Posts::withCount(['likes' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'allComments' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'postShares' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }])
            ->skip($skip)
            ->limit($count)
            ->get();

            if ($posts->count()) {
                $loopCount++;
            }

            foreach ($posts as $post) {
                try {
                    $rank = $post->likes_count +  $post->all_comments_count +  $post->post_shares_count;
                    if ($allTime) {
                        $post->rank = $rank;
                    } else {
                        $post->rank_7 = $rank;
                    }
                    $post->save();
                } catch (\Exception $exception) {
                    $this->logCron(CommonConst::TYPE_POST, RankCronLog::ACTION_FAIL, $allTime, $loopCount, $exception->getMessage(), $post->id);
                }
            }
            $skip += $count;
        } while ($posts->count());

        $this->logCron(CommonConst::TYPE_POST, RankCronLog::ACTION_END, $allTime, $loopCount);
    }

    protected function calculateTripRanks ($allTime, $creation)
    {
        $count = self::COUNT;
        $loopCount = 0;
        $skip = 0;
        $this->logCron(CommonConst::TYPE_TRIP, RankCronLog::ACTION_START, $allTime);
        do {
            $trips = TripPlans::withCount(['likes' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'allComments' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'postShares' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }])
            ->skip($skip)
            ->limit($count)
            ->get();

            if ($trips->count()) {
                $loopCount++;
            }

            foreach ($trips as $trip) {
                try {
                    $rank = $trip->likes_count +  $trip->all_comments_count +  $trip->post_shares_count;
                    if ($allTime) {
                        $trip->rank = $rank;
                    } else {
                        $trip->rank_7 = $rank;
                    }
                    $trip->save();
                } catch (\Exception $exception) {
                    $this->logCron(CommonConst::TYPE_TRIP, RankCronLog::ACTION_FAIL, $allTime, $loopCount, $exception->getMessage(), $trip->id);
                }
            }

            $skip += $count;
        } while ($trips->count());


        $this->logCron(CommonConst::TYPE_TRIP, RankCronLog::ACTION_END, $allTime, $loopCount);
    }

    protected function calculateReportRanks ($allTime, $creation)
    {
        $count = self::COUNT;
        $loopCount = 0;
        $skip = 0;
        $this->logCron(CommonConst::TYPE_REPORT, RankCronLog::ACTION_START, $allTime);
        do {
            $reports = Reports::withCount(['likes' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'allComments' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'postShares' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }, 'views' => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }])
            ->skip($skip)
            ->limit($count)
            ->get();

            if ($reports->count()) {
                $loopCount++;
            }

            foreach ($reports as $report) {
                try {
                    $rank = $report->likes_count +  $report->all_comments_count +  $report->post_shares_count + $report->views_count;
                    if ($allTime) {
                        $report->rank = $rank;
                    } else {
                        $report->rank_7 = $rank;
                    }
                    $report->save();
                } catch (\Exception $exception) {
                    $this->logCron(CommonConst::TYPE_REPORT, RankCronLog::ACTION_FAIL, $allTime, $loopCount, $exception->getMessage(), $report->id);
                }
            }
            $skip += $count;
        } while ($reports->count());

        $this->logCron(CommonConst::TYPE_REPORT, RankCronLog::ACTION_END, $allTime, $loopCount);
    }

    protected function calculateDiscussionRanks ($allTime, $creation)
    {
        $count = self::COUNT;
        $loopCount = 0;
        $skip = 0;
        $this->logCron(CommonConst::TYPE_DISCUSSION, RankCronLog::ACTION_START, $allTime);
        do {
            $discussions = Discussion::withCount(['replies'  => function ($query) use ($allTime, $creation) {
                if (!$allTime) {
                    $query->where('created_at', '>', $creation);
                }
            }])
            ->skip($skip)
            ->limit($count)
            ->get();

            if ($discussions->count()) {
                $loopCount++;
            }

            foreach ($discussions as $discussion) {
                try {
                    if ($allTime) {
                        $discussion->rank = $discussion->replies_count;
                    } else {
                        $discussion->rank_7 = $discussion->replies_count;
                    }
                    $discussion->save();
                } catch (\Exception $exception) {
                    $this->logCron(CommonConst::TYPE_DISCUSSION, RankCronLog::ACTION_FAIL, $allTime, $loopCount, $exception->getMessage(), $discussion->id);
                }

            }
            $skip += $count;
        } while ($discussions->count());

        $this->logCron(CommonConst::TYPE_DISCUSSION, RankCronLog::ACTION_END, $allTime, $loopCount);
    }

    protected function logCron ($type, $action, $allTime, $loopCount = null, $message = null, $postId = null)
    {
        RankCronLog::create([
            'cron_id' => $this->cronId,
            'post_type' => $type,
            'action' => $action,
            'message' => $message,
            'post_id' => $postId,
            'loop_count' => $loopCount,
            'rank_type' => $allTime ? RankCronLog::RANK_TYPE_ALL : RankCronLog::RANK_TYPE_7,
        ]);
    }
}
