<?php

namespace App\Console\Commands;

use App\Models\Ranking\RankingBadges;
use Illuminate\Console\Command;

class SeedNewRankingBadges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed-new-ranking-badges';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    public function handle()
    {
        $badges = [
            [
                'name' => 'Blue',
                'url' => '/assets2/image/badges/blue.png',
                'order' => 0,
                'followers' => 5000,
                'points' => 25000,
                'active' => 1
            ],
            [
                'name' => 'Bronze',
                'url' => '/assets2/image/badges/bronze.png',
                'order' => 1,
                'followers' => 20000,
                'points' => 100000,
                'active' => 1
            ],
            [
                'name' => 'Silver',
                'url' => '/assets2/image/badges/silver.png',
                'order' => 2,
                'followers' => 50000,
                'points' => 200000,
                'active' => 1
            ],
            [
                'name' => 'Gold',
                'url' => '/assets2/image/badges/gold.png',
                'order' => 3,
                'followers' => 100000,
                'points' => 500000,
                'active' => 1
            ]
        ];

        foreach ($badges as $badge) {
            RankingBadges::query()->create($badge);
        }
    }
}