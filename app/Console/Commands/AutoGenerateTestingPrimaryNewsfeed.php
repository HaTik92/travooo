<?php

namespace App\Console\Commands;

use App\Models\User\User;
use App\Models\UsersFollowers\UsersFollowers;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Throwable;

set_time_limit(0);
ini_set('memory_limit', -1);
class AutoGenerateTestingPrimaryNewsfeed extends Command
{
    protected $signature = 'auto:add-testing-primary-newsfeed';
    protected $description = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            // $testingUsers = collect([1594, 74, 76]); // local
            $testingUsers = collect([74, 76, 1969, 1984, 2125, 26]);
            $fromFollowers = $this->fromFollowers($testingUsers);
            if ($fromFollowers->count()) {
                $times =  rand(10, 15);
                $progress = $this->output->createProgressBar(($times * 7) + 1);
                $progress->advance();
                for ($i = 0; $i < $times; $i++) {
                    // add 1 trip
                    Artisan::call("seed", [
                        'userId' => $fromFollowers->random(),
                        '--type' => 'trip',
                        '--times' => 1, // times doesn't matter for trip only
                    ]);
                    $progress->advance();

                    // add discussion
                    Artisan::call("seed-discussion", [
                        'userId' => $fromFollowers->random(),
                    ]);
                    $progress->advance();

                    // add 1 trip
                    Artisan::call("seed", [
                        'userId' => $fromFollowers->random(),
                        '--type' => 'trip',
                        '--times' => 1,  // times doesn't matter for trip only
                    ]);
                    $progress->advance();

                    if (rand(0, 1)) { // random sleep
                        sleep(rand(60, 180));
                    }

                    // add posts
                    Artisan::call("seed", [
                        'userId' => $testingUsers->random(),
                        '--type' => 'post',
                        '--times' => rand(2, 3),
                    ]);
                    $progress->advance();

                    // add discussion
                    Artisan::call("seed-discussion", [
                        'userId' => $fromFollowers->random(),
                    ]);
                    $progress->advance();

                    // add 1 trip
                    Artisan::call("seed", [
                        'userId' => $testingUsers->random(),
                        '--type' => 'trip',
                        '--times' => 1,  // times doesn't matter for trip only
                    ]);
                    $progress->advance();

                    if (rand(0, 1)) { // random sleep
                        sleep(rand(60, 180));
                    }

                    // add posts
                    Artisan::call("seed", [
                        'userId' => $testingUsers->random(),
                        '--type' => 'post',
                        '--times' => rand(4, 6),
                    ]);
                    $progress->advance();
                }
                $progress->finish();
            }
        } catch (Throwable $th) {
            $errorInfo = [
                'error' => true,
                'message' => $th->getMessage(),
                'code' => $th->getCode(),
                'class' => get_class($th),
            ];
            Log::useDailyFiles(storage_path() . '/logs/cron.log');
            Log::emergency('API_SERVER_ERROR : ', $errorInfo, PHP_EOL . PHP_EOL);
            echo print_r($errorInfo);
        }
    }

    private function fromFollowers($testingUsers)
    {
        $ids = collect([]);
        $testingUsers->each(function ($id) use (&$ids) {
            $ids = $ids->merge($this->dumpMyFollowigsIfNotLessThen($id, rand(5, 7)));
        });
        return $ids->unique();
    }

    private function dumpMyFollowigsIfNotLessThen($id, $no)
    {
        $me = User::find($id);
        $my_followigs = UsersFollowers::where('followers_id', $me->id)->where('follow_type', 1)->pluck('users_id')->toArray();
        if (count($my_followigs) <= $no) {
            $weeklyTrendingUser = User::query()
                ->selectRaw('id, (
                                    (select count(*) from `posts` where users_id=`users`.`id` and date >  "' . Carbon::now()->subDays(7) . '") 
                                    + (select count(*) from `trips` where users_id=`users`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '") 
                                    + (select count(*) from `reports` where users_id=`users`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '") 
                                    + (select count(*) from `discussions` where users_id=`users`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '") 
                                ) as ranks')
                ->orderBy('ranks', 'desc')
                ->take(10)->get();
            $allTimeUsers = User::query()
                ->selectRaw('id, (
                                    (select count(*) from `posts` where users_id=`users`.`id`) 
                                    + (select count(*) from `trips` where users_id=`users`.`id`) 
                                    + (select count(*) from `reports` where users_id=`users`.`id`) 
                                    + (select count(*) from `discussions` where users_id=`users`.`id`) 
                                ) as ranks')
                ->orderBy('ranks', 'desc')
                ->take(10 + (10 - count($weeklyTrendingUser)))->get();
            $lst = $weeklyTrendingUser->merge($allTimeUsers)->pluck('id')->unique();

            collect($lst)->each(function ($user_id) use ($me) {
                if (!UsersFollowers::where([
                    'followers_id'  => $me->id,
                    'users_id'  =>  $user_id,
                    'from_where'  => 'profile',
                    'follow_type' => 1
                ])->first()) {
                    UsersFollowers::create([
                        'followers_id'  => $me->id,
                        'users_id'  =>  $user_id,
                        'from_where'  => 'profile',
                        'follow_type' => 1
                    ]);
                }
            });
        }

        return UsersFollowers::where('followers_id', $me->id)->where('follow_type', 1)->pluck('users_id');
    }
}
