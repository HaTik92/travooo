<?php

namespace App\Console\Commands;

use App\Models\User\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TruncateOldRankingSystem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'truncate-old-ranking-tables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    public function handle()
    {
        \Schema::disableForeignKeyConstraints();
        User::query()->whereNotNull('invite_expert_link_id')->update(['invite_expert_link_id' => null]);
        DB::table('invite_expert_links')->truncate();
        DB::table('ranking_user_transactions')->truncate();
        DB::table('ranking_user_points')->truncate();
        DB::table('ranking_user_badges')->truncate();
        DB::table('ranking_badges')->truncate();
        \Schema::enableForeignKeyConstraints();
    }
}