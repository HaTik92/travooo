<?php

namespace App\Console\Commands;

use App\Models\User\User;
use Illuminate\Console\Command;

class SetConfirmedToAllUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:confirm:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set confirm = 1 to all users';


    public function handle()
    {
        User::query()->update(['confirmed' => 1]);
    }
}
