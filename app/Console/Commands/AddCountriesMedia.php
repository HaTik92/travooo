<?php

namespace App\Console\Commands;

use App\Services\Medias\LocationsAddMediaService;
use App\Models\Country\Countries;
use Illuminate\Console\Command;


class AddCountriesMedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:countries-medias';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add countries medias if count less then 5';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(LocationsAddMediaService $locationsAddMediaService)
    {
        $offset = 0;
        while ( count($countries = Countries::offset($offset)->limit(100)->get()) ) {
            foreach ($countries as $country) {
                $locationsAddMediaService->addCountryMedia($country);
            }
            $offset += 100;
        }
    }
}
