<?php

namespace App\Console\Commands\OptimizedAndChanges;

use App\Models\Discussion\Discussion;
use App\Models\Posts\Posts;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsInfos;
use App\Models\TripPlans\TripPlans;
use App\Services\Reports\ReportsService;
use App\Services\Translations\TranslationService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

set_time_limit(0);
ini_set('memory_limit', -1);

class FillLanguageIdInAllModule extends Command
{
    protected $signature = 'fill:language_id';
    protected $description = 'fill:language_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(TranslationService $translationService)
    {
        try {
            Log::useDailyFiles(storage_path() . '/logs/cronstatus.log');
            Log::info('ERROR : ', [
                'file' => 'FillLanguageIdInAllModule',
                'time' => date('Y-m-d H:i:s A'),
            ], PHP_EOL . PHP_EOL);
            $query = Discussion::query()->whereHas('author')->whereNull('language_id');
            if ((clone $query)->exists()) {
                (clone $query)->orderBy('id', 'DESC')->limit(250)->get()->each(function ($discussion) use ($translationService) {
                    $discussion->language_id = $translationService->getLanguageId($discussion->question . ' ' . $discussion->description);;
                    $discussion->save();
                });
            }

            $query = Reports::query()->whereHas('author')->whereNull('language_id');
            if ((clone $query)->exists()) {
                (clone $query)->orderBy('id', 'DESC')->limit(250)->get()->each(function ($report) use ($translationService) {
                    $report->language_id = $translationService->getLanguageId($report->title . ' ' . $report->description);;
                    $report->save();
                });
            }

            $query = TripPlans::query()->whereHas('author')->whereNull('language_id');
            if ((clone $query)->exists()) {
                (clone $query)->orderBy('id', 'DESC')->limit(250)->get()->each(function ($trip) use ($translationService) {
                    $trip->language_id = $translationService->getLanguageId($trip->title . ' ' . $trip->description);;
                    $trip->save();
                });
            }

            $query = Posts::query()->whereHas('author')->whereNull('language_id')->whereNull('text');
            if ((clone $query)->exists()) {
                (clone $query)->orderBy('id', 'DESC')->limit(500)->get()->each(function ($post) use ($translationService) {
                    $post->language_id = $translationService->getLanguageId($post->text);
                    $post->save();
                });
            }

            $query = ReportsInfos::query()->whereHas('report')->whereIn('var', [ReportsService::VAR_TEXT, ReportsService::VAR_INFO])->whereNull('language_id')->whereNotNull('val');
            if ((clone $query)->exists()) {
                (clone $query)->orderBy('id', 'DESC')->limit(700)->get()->each(function ($reportsInfo) use ($translationService) {
                    $reportsInfo->language_id = $translationService->getLanguageId($reportsInfo->val);
                    $reportsInfo->save();
                });
            }
        } catch (\Throwable $e) {
            Log::useDailyFiles(storage_path() . '/logs/cron.log');
            Log::info('ERROR : ', [
                'file' => 'FillLanguageIdInAllModule',
                'message' => $e->getMessage(),
                'class' => get_class($e)
            ], PHP_EOL . PHP_EOL);
        }
    }
}
