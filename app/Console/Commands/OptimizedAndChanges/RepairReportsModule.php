<?php

namespace App\Console\Commands\OptimizedAndChanges;

use App\Models\Reports\Reports;
use App\Models\Reports\ReportsDraft;
use App\Models\Reports\ReportsDraftsLocation;
use App\Models\Reports\ReportsInfos;
use App\Models\Reports\ReportsLocation;
use App\Models\Reports\ReportsPlaces;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class RepairReportsModule extends Command
{

    protected $signature = 'repair:reports-module';
    protected $description = 'repair reports-module';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Schema::disableForeignKeyConstraints();
        ReportsPlaces::truncate();
        ReportsLocation::truncate();
        ReportsDraftsLocation::truncate();
        Schema::enableForeignKeyConstraints();

        /**
         * transfer all published report added places into ReportsPlaces
         */
        Reports::query()->whereHas('draft_report')
            ->where('flag', 1)
            ->whereNotNull('published_date')
            ->get()->each(function ($reports) {
                $reports->infos()
                    ->where('var', 'place')
                    ->whereNotNull('val')
                    ->pluck('val')
                    ->each(function ($places_id) use ($reports) {
                        $data = ['reports_id' => $reports->id, 'places_id' => $places_id];
                        if (!ReportsPlaces::query()->where($data)->exists()) {
                            ReportsPlaces::create($data);
                        }
                    });
            });


        /**
         * transfer all reports comma seprated countries_id into ReportsLocation
         */
        Reports::query()->whereNotNull('countries_id')->orderBy('id', 'desc')
            ->pluck('countries_id', 'id')
            ->each(function ($countries_id, $id) {
                $var = explode(",", "" . $countries_id);
                collect($var)
                    ->each(function ($country_id) use ($id) {
                        $data = [
                            'reports_id' => $id,
                            'location_type' => Reports::LOCATION_COUNTRY,
                            'location_id' => $country_id
                        ];
                        if (!ReportsLocation::query()->where($data)->exists()) {
                            ReportsLocation::create($data);
                        }
                    });
            });

        /**
         * transfer all reports comma seprated cities_id into ReportsLocation
         */
        Reports::query()->whereNotNull('cities_id')->orderBy('id', 'desc')
            ->pluck('cities_id', 'id')
            ->each(function ($cities_id, $id) {
                $var = explode(",", "" . $cities_id);
                collect($var)
                    ->each(function ($city_id) use ($id) {
                        $data = [
                            'reports_id' => $id,
                            'location_type' => Reports::LOCATION_CITY,
                            'location_id' => $city_id
                        ];
                        if (!ReportsLocation::query()->where($data)->exists()) {
                            ReportsLocation::create($data);
                        }
                    });
            });


        /**
         * transfer all draft reports comma seprated countries_id into ReportsDraftsLocation
         */
        ReportsDraft::query()->orderBy('id', 'desc')
            ->select(['id', 'reports_id', 'countries_id'])
            ->whereNotNull('countries_id')
            ->get()
            ->each(function ($item) {
                $var = explode(",", "" . $item->countries_id);
                collect($var)
                    ->each(function ($country_id) use ($item) {
                        $data = [
                            'reports_draft_id' => $item->id,
                            'reports_id' => $item->reports_id,
                            'location_type' => Reports::LOCATION_COUNTRY,
                            'location_id' => $country_id
                        ];
                        if (!ReportsDraftsLocation::query()->where($data)->exists()) {
                            ReportsDraftsLocation::create($data);
                        }
                    });
            });

        /**
         * transfer all draft reports comma seprated cities_id into ReportsDraftsLocation
         */
        ReportsDraft::query()->orderBy('id', 'desc')
            ->select(['id', 'reports_id', 'cities_id'])
            ->whereNotNull('cities_id')
            ->get()
            ->each(function ($item) {
                $var = explode(",", "" . $item->cities_id);
                collect($var)
                    ->each(function ($city_id) use ($item) {
                        $data = [
                            'reports_id' => $item->reports_id,
                            'reports_draft_id' => $item->id,
                            'location_type' => Reports::LOCATION_CITY,
                            'location_id' => $city_id
                        ];
                        if (!ReportsDraftsLocation::query()->where($data)->exists()) {
                            ReportsDraftsLocation::create($data);
                        }
                    });
            });
    }
}
