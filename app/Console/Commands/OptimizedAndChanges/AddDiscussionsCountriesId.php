<?php

namespace App\Console\Commands\OptimizedAndChanges;

use App\Models\City\Cities;
use App\Models\Discussion\Discussion;
use App\Models\Place\Place;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class AddDiscussionsCountriesId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discussion-add:countries-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add countries id of selected cities/places';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offset = 0;
        while (count($discussions = Discussion::offset($offset)->limit(100)->get())) {
            foreach ($discussions as $discussion) {
                $country_id = NULL;
                try {
                    switch (Str::lower($discussion->destination_type)) {
                        case 'city':
                            $country_id = Cities::find($discussion->destination_id)->country->id;
                            break;
                        case 'place':
                            $country_id = Place::find($discussion->destination_id)->country->id;
                            break;
                        case 'country':
                            $country_id = $discussion->destination_id;
                            break;
                    }
                } catch (\Throwable $th) {
                }
                $discussion->countries_id = $country_id;
                $discussion->save();
            }
            $offset += 100;
        }
    }
}
