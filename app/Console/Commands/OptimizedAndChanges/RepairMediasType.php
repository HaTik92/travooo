<?php

namespace App\Console\Commands\OptimizedAndChanges;

use App\Models\ActivityMedia\Media;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

set_time_limit(0);
ini_set('memory_limit', -1);

class RepairMediasType extends Command
{
    protected $signature = 'repair:medias-type';
    protected $description = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            Media::query()
                ->whereIn(DB::raw('lower(RIGHT(url, 3))'), ["jpeg", "jpg", "png", "gif", "tiff", "heic"])
                ->whereNull('type')
                ->whereNotNull('url')
                ->update([
                    'type' => Media::TYPE_IMAGE
                ]);

            Media::query()
                ->whereIn(DB::raw('lower(RIGHT(url, 3))'), ["mp4", "mov", "ogg", "wmv"])
                ->whereNull('type')
                ->whereNotNull('url')
                ->update([
                    'type' => Media::TYPE_VIDEO
                ]);
        } catch (\Throwable $e) {
            Log::useDailyFiles(storage_path() . '/logs/cron.log');
            Log::info(json_encode([
                'file' => 'RepairMediasType',
                'message' => $e->getMessage(),
                'class' => get_class($e)
            ], JSON_PRETTY_PRINT));
        }
    }
}
