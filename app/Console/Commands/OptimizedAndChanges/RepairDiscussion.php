<?php

namespace App\Console\Commands\OptimizedAndChanges;

use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionTopics;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class RepairDiscussion extends Command
{
    protected $signature = 'repair:discussion';
    protected $description = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Schema::disableForeignKeyConstraints();
        DiscussionTopics::truncate();
        Schema::enableForeignKeyConstraints();

        if (Discussion::query()->whereNotNull('topic')->exists()) {
            Discussion::query()->whereNotNull('topic')->get()->each(function ($discussion) {
                $topics = explode(',', $discussion->topic);
                foreach ($topics as $topic) {
                    if (strlen(trim($topic))) {
                        $disc_topic = new DiscussionTopics;
                        $disc_topic->discussions_id = $discussion->id;
                        $disc_topic->topics = trim($topic);
                        $disc_topic->save();
                    }
                }
            });
        }
        echo "finish";
    }
}
