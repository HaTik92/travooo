<?php

namespace App\Console\Commands\OptimizedAndChanges;

use Illuminate\Console\Command;
use App\Services\FFmpeg\FFmpegService;
use App\Models\ActivityMedia\Media;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

set_time_limit(0);
ini_set('memory_limit', -1);

class AddVideoThumbnailCron extends Command
{
    protected $signature = 'add:video-thumbnail';
    protected $description = 'Create video thumbnail';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(FFmpegService $ffmpegService)
    {

        Log::useDailyFiles(storage_path() . '/logs/videothumbnail.log');
        Media::query()
            ->where('type', Media::TYPE_VIDEO)
            ->whereNotNull('url')
            ->whereNull('video_thumbnail_url')
            ->orderBy('id', 'DESC')
            ->limit(10)
            ->get()
            ->each(function ($media) use ($ffmpegService) {
                $s3VideoThumbnail = NULL;
                $done = false;
                $s3Path = str_replace(S3_BASE_URL, '', $media->url); // remove S3_BASE_URL if found
                $some_file = Storage::disk('s3')->get($s3Path); // GET s3 video
                Storage::disk('public')->put($s3Path, $some_file); // store s3 video in local server
                $localVideoThumbnail = $ffmpegService->convertVideoThumbnail(public_path('storage/' . $s3Path), 600, 600, 70); // create Thumbnail in local based on local video
                if ($localVideoThumbnail) {
                    Storage::disk('public')->delete($s3Path); // delete local video
                    $s3VideoThumbnail = $ffmpegService->moveThumbnailToS3($localVideoThumbnail, true); // move local Thumbnail in s3 & delete local Thumbnail
                    $media->video_thumbnail_url = $s3VideoThumbnail; // save s3 Thumbnail path in media table
                    if ($media->save()) {
                        $done = true;
                    }
                }
                if ($done) {
                    Log::info(json_encode([
                        'id' => $media->id,
                        'media_url' => $media->url,
                        'video_thumbnail_url' =>  $s3VideoThumbnail
                    ], JSON_PRETTY_PRINT));
                }
            });
    }
}
