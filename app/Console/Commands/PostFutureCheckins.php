<?php

namespace App\Console\Commands;

use App\Models\ActivityLog\ActivityLog;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PostFutureCheckins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:future-checkins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post future checkin posts on checkin day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $activity_logs = ActivityLog::select('activity_log.*')
            ->where('type', 'Checkin')
            ->where('action', 'do')
            ->join('posts','activity_log.variable','=','posts.id')
            ->join('posts_checkins','posts.id','=','posts_checkins.posts_id')
            ->join('checkins','posts_checkins.checkins_id','=','checkins.id')
            ->whereRaw('checkins.checkin_time > checkins.created_at')
            ->whereRaw('DateDiff(checkins.checkin_time, NOW()) = 0')
            ->get('activity_log.*');

        $activity_logs->each(function($activity_log) {
            log_user_activity('Checkin', 'update', $activity_log->id, 0, $activity_log->user);
        });

    }
}
