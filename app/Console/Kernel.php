<?php

namespace App\Console;

use App\Console\Commands\Seeds\{
    SeedGenericTop,
    DiscussionSeed
};
use App\Console\Commands\AddInfoToCityCountryFromNumbeo;
use App\Console\Commands\AddLegalPagesCommand;
use App\Console\Commands\AddSuperAdminDataToRoleTable;
use App\Console\Commands\GenerateDefaultPlanChats;
use App\Console\Commands\GenerateStatistic;
use App\Console\Commands\AddMediasCountToPlaces;
use App\Console\Commands\AsyncSeed;
use App\Console\Commands\AutoGenerateTestingPrimaryNewsfeed;
use App\Console\Commands\ClalculateRanks;
use App\Console\Commands\CleanUnUsedLog;
use App\Console\Commands\CleanUnUsedUploadedTemp;
use App\Console\Commands\GetWeather;
use App\Console\Commands\MemoryPlanTransformer;
use App\Console\Commands\MigrateSuggestionsTable;
use App\Console\Commands\PlanEventsFinishedChecker;
use App\Console\Commands\RestoreMediasForOldPlans;
use App\Console\Commands\SeedNewRankingBadges;
use App\Console\Commands\SetThumbForAvatars;
use App\Console\Commands\TruncateOldRankingSystem;
use App\Console\Commands\UpdateCitiesLocation;
use App\Console\Commands\UpdateCountriesLocation;
use App\Console\Commands\UpdateESCache;
use App\Console\Commands\UpdateTrendingsCache;
use App\Console\Commands\EventsFetch;
use App\Console\Commands\OptimizedAndChanges\{
    AddDiscussionsCountriesId,
    FillLanguageIdInAllModule,
    RepairReportsModule,
    RepairDiscussion,
    RepairMediasType,
};
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        GenerateStatistic::class,
        AddMediasCountToPlaces::class,
        AddMediasCountToPlaces::class,
        GetWeather::class,
        UpdateCitiesLocation::class,
        UpdateCountriesLocation::class,
        MigrateSuggestionsTable::class,
        SetThumbForAvatars::class,
        AddInfoToCityCountryFromNumbeo::class,
        MemoryPlanTransformer::class,
        PlanEventsFinishedChecker::class,
        RestoreMediasForOldPlans::class,
        AddSuperAdminDataToRoleTable::class,
        GenerateDefaultPlanChats::class,
        TruncateOldRankingSystem::class,
        SeedNewRankingBadges::class,
        UpdateTrendingsCache::class,
        CleanUnUsedLog::class,
        CleanUnUsedUploadedTemp::class,
        AddLegalPagesCommand::class,
        EventsFetch::class,
        UpdateESCache::class,
        AutoGenerateTestingPrimaryNewsfeed::class,
        AsyncSeed::class,
        SeedGenericTop::class,
        DiscussionSeed::class,
        AddDiscussionsCountriesId::class,
        RepairReportsModule::class,
        FillLanguageIdInAllModule::class,
        RepairDiscussion::class,
        RepairMediasType::class,
        ClalculateRanks::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('command:weather')
            ->daily('23:00');
        $schedule->command('update:from-numbeo')->monthly();
        $schedule->command('plans:transform')
            ->hourly()->withoutOverlapping();
        $schedule->command('post:future-checkin')->daily();
        $schedule->command('plans:events-finished:check')->daily();
        //$schedule->command('trendings:update-cache')->daily()->withoutOverlapping();
        $schedule->command('es-places:update-cache')->daily()->withoutOverlapping();
        // Clean Unused Data
        $schedule->command('clean:temp-uploaded-media')->daily();
        $schedule->command('clean:logs')->daily();
        $schedule->command('fetch:musment-events')->weekly();

        //only for testing in dev site
        if (isDevSite()) {
            // $schedule->command('auto:add-testing-primary-newsfeed')->daily();
        }

        // fixing DB
        $schedule->command('fill:language_id')->everyThirtyMinutes();
        $schedule->command('repair:medias-type')->hourly();
        $schedule->command('add:video-thumbnail')->hourly();

        // Calculate Post Last 7 Days Rank
        $schedule->command('calculate-ranks')->dailyAt('00:00');;
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }
}
