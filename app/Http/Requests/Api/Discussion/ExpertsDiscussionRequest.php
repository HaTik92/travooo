<?php

namespace App\Http\Requests\Api\Discussion;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class ExpertsDiscussionRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'destination_type'          => 'required|in:place,city,country',
            'destination_id'            => 'required',
        ];
    }

    public function messages()
    {
        return [
            'destination_type.in'     => "destination_type must be in list of place, city, and country.",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
