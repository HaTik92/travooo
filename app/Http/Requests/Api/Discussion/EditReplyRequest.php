<?php

namespace App\Http\Requests\Api\Discussion;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class EditReplyRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'text.required'  => "Text not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
