<?php

namespace App\Http\Requests\Api\Discussion;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class ReportSpamRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'         => 'required|string',
            'text'         => 'required|string',
            'reply_id'         => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'type.required' => "Type not provided",
            'type.string' => "Type must be a string",
            'text.required'  => "Text not provided",
            'text.string'  => "Text must be a string",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
