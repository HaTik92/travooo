<?php

namespace App\Http\Requests\Api\Discussion;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class DiscussionRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'topId'         => 'required|integer',
            'order'         => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'topId.required' => "Topic Id not provided",
            'topId.integer' => "Topic Id format must be a integer",
            'order.required'  => "Order format not provided",
            'order.string'  => "Order format must be a string",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
