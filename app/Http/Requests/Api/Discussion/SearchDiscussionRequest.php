<?php

namespace App\Http\Requests\Api\Discussion;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class SearchDiscussionRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'              => 'sometimes|integer',
            'trending_type'     => 'string|sometimes',
            'answers_user_id'   => 'integer|sometimes',
            'dest_type'         => 'string|sometimes',
            'order'             => 'sometimes|in:popular,new,old',
            'my_activity_type'  => 'sometimes|in:discussions,replies',
        ];
    }

    public function messages()
    {
        return [
            'type.integer'              => "User Type must be a interger",
            'trending_type.string'      => "Tranding type must be a string",
            'answers_user_id.integer'   => "User id must be a interger",
            'dest_type.string'          => "Destination type must be a string",
            'order.in'                  => "Order type must be in list of popular, new, and old",
            'my_activity_type.in'       => "My Activity type must be in list of discussions, replies",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
