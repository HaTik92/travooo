<?php

namespace App\Http\Requests\Api\Discussion;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CheckUpDownVoteRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'replies_id'        => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'replies_id.required' => "replies Id not provided",
            'replies_id.integer'  => "replies Id must be a integer"
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
