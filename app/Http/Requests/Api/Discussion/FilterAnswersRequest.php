<?php

namespace App\Http\Requests\Api\Discussion;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class FilterAnswersRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'         => 'required',
            'discussion_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'type.required' => "Type not provided",
            'discussion_id.required'  => "Discussion Id not provided",
            'discussion_id.integer' => "Discussion Id must be a Integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
