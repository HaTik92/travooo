<?php

namespace App\Http\Requests\Api\Discussion;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CreateDiscussionRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'destination_type'          => 'required|in:place,city,country',
            'destination_id'            => 'required',
            'question'                  => 'required|max:150',
            'description'               => 'sometimes|max:1000',
            'experts'                   => 'sometimes|array',
        ];
    }

    public function messages()
    {
        return [
            'destination_type.in'       => "destination_type must be in list of place, city, and country.",
            'destination_id.required'   => "Destination Id not provided",
            'question.max'              => "Question Maximum characters are 150",
            'question.required'         => "Question not provided",
            'description.max'           => "Description Maximum characters are 1000",
            'description.required'      => "Description not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
