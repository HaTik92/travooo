<?php

namespace App\Http\Requests\Api\Post;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CreatePostRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pair'                 => 'sometimes',
            'text'                 => 'sometimes|max:21844',
            'location'             => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'pair.required'                 => 'Pair token not provided.',
            'text.max'                      => 'Post content exceed the limit (max : 21844 character).',
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
