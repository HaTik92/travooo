<?php

namespace App\Http\Requests\Api\Post;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;
use Illuminate\Validation\Rule;
use App\Models\Posts\Posts;

class DeletePostRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $postTypes = Posts::getPostTypes();
        return [
            'post_id'   => 'required|integer|exists:posts,id',
            'post_type' => 'required|string|in:'.implode(',', $postTypes),
        ];
    }

    public function messages()
    {
        return [
            'post_id.integer'  => 'Post id must be an integer.',
            'post_id.exists'   => 'Post does not exists.',
            'post_type.string'     => 'Post type is invalid',
            'post_type.in' => 'Post type must be from (' . implode(',', $postTypes) . ')'
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
