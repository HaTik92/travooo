<?php

namespace App\Http\Requests\Api\Post;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;
use Illuminate\Foundation\Http\FormRequest;

class ShareAnyPostRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'  => 'required|string',
            'id'    => 'sometimes|integer',
            'text'  => 'sometimes|max:21844',
        ];
    }

    public function messages()
    {
        return [
            'text.max' => 'Post content exceed the limit (max : 21844 character).',
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
