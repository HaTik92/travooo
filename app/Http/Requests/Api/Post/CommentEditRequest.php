<?php

namespace App\Http\Requests\Api\Post;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CommentEditRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'post_id'        => 'required|integer|exists:posts,id',
            'comment_id'     => 'required|integer|exists:posts_comments,id',
            'pair'           => 'required',
            'text'           => 'required|max:21844',
            'existing_media' => 'sometimes|array'
        ];
        return $rules;
    }

    public function messages()
    {
        $messages = [
            'post_id.integer'       => 'Post id must be an integer.',
            'post_id.exists'        => 'Post does not exists.',
            'comment_id.integer'    => 'Comment id must be an integer.',
            'comment_id.exists'     => 'Comment does not exists.',
            'pair.required'         => 'Pair token not provided.',
            'text.required'         => 'Post body not provided.',
            'text.max'              => 'Post content exceed the limit (max : 21844 character).',
            'existing_media.array'  => 'Media must be an array format',
        ];
        return $messages;
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
