<?php

namespace App\Http\Requests\Api\Post;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;
use Illuminate\Validation\Rule;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsComments;

class SpamPostRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $postTypes      = Posts::postTypes();
        $commentTypes   = PostsComments::commentTypes();
        return [
            'id'           => 'required|integer',
            'post_type'         => 'required|string|in:'.implode(',', (array_merge($postTypes, $commentTypes))),
            'report_type'       => 'required|integer',
        ];
    }

    public function messages()
    {
        $postTypes      = Posts::postTypes();
        $commentTypes   = PostsComments::commentTypes();
        return [
            'id.integer'           => 'Post id must be an integer.',
            'id.exists'            => 'Post does not exists.',
            // 'post_type.string'          => 'Post type is invalid',
            // 'post_type.in'              => 'Post type must be from (' . implode(',', (array_merge($postTypes, $commentTypes))) . ')',
            'report_type.integer'       => 'Report Type must be an integer.',
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
