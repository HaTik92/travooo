<?php

namespace App\Http\Requests\Api\TripPlaceComments;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CommentsRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment'  => 'required',
            'trip_place_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'comment.required' => "Comment not provided",
            'trip_place_id.required' => "Trip Place Id not provided",
            'trip_place_id.integer' => "Trip Place Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
