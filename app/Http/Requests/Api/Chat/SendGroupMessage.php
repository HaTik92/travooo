<?php

namespace App\Http\Requests\Api\Chat;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;

class SendGroupMessage extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_id'       => 'required|integer',
            'message'      => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
