<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;

class GlobalTripPlannerFetchRequest extends StepRequest
{
    public function rules()
    {
        return [
            'countries'     => 'sometimes|string',
            'cities'        => 'sometimes|string',
            'type'          => 'sometimes|string',
            'search_text'   => 'sometimes|string',
            'trip_type'     => 'sometimes|string',
            'order'         => 'sometimes|string',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
