<?php

namespace App\Http\Requests\Api\Trips;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RemovePlaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'place_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'place_id.required' => 'Place Id Not Provided.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
