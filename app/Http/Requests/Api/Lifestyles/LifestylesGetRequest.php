<?php

namespace App\Http\Requests\Api\Lifestyles;

use Illuminate\Foundation\Http\FormRequest;

class LifestylesGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'language_id' => 'required|integer',
            'limit'       => 'integer',
            'offset'      => 'integer'
        ];
    }

    public function messages()
    {
        return [
            'language_id.required' => 'Language id not provided.',
            'language_id.integer'  => 'Language id should be numeric.',
            'limit.integer'        => 'limit should be numeric.',
            'offset.integer'       => 'Offset should be numeric.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error'   => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
