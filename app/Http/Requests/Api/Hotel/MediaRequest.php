<?php

namespace App\Http\Requests\Api\Hotel;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class MediaRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hotel_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'hotel_id.required' => "Hotel Id not provided",
            'hotel_id.integer' => "Hotel Id must be a integer"
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
