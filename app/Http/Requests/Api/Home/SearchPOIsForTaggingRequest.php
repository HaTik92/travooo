<?php

namespace App\Http\Requests\Api\Home;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class SearchPOIsForTaggingRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'query.required' => "Search term not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
