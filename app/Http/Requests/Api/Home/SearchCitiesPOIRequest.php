<?php

namespace App\Http\Requests\Api\Home;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;

class SearchCitiesPOIRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'q'         => 'required',
            'city_id'   => 'required',
            'type'      => 'string|sometimes',
        ];
    }

    public function messages()
    {
        return [
            'q.required' => "Search term not provided",
            'type.string' => "Type must be a string",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
