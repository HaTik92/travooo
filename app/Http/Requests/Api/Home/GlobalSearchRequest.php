<?php

namespace App\Http\Requests\Api\Home;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class GlobalSearchRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'q'  => 'required|min:2',
        ];
    }

    public function messages()
    {
        return [
            'q.required' => "Search term not provided",
            'q.min' => "Search term must be at least 2 characters"
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
