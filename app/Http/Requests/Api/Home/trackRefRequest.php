<?php

namespace App\Http\Requests\Api\Home;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class trackRefRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ref'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
