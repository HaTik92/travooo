<?php

namespace App\Http\Requests\Api\Home;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class URLPreviewRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
