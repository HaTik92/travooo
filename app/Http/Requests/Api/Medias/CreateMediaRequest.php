<?php

namespace App\Http\Requests\Api\Medias;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'language_id' => 'required|numeric',
            'media_file' => 'file'
        ];
    }

    public function messages()
    {
        return [
            'language_id.required' => 'Language Id Not Provided.',
            'language_id.numeric' => 'Language Id must be numeric.',
            'media_file.file' => 'File Not Provided For Upload.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
