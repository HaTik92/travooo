<?php

namespace App\Http\Requests\Api\Medias;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateDescriptionMediasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'medias_id'    => 'required|integer',
            'languages_id' => 'integer',
            'description'  => 'required'
        ];
    }

    public function messages()
    {
        return [
            'medias_id.required'    => 'Media Id not provided.',
            'description.required'  => 'Description not provided.',
            'medias_id.integer'     => 'Media Id should be numeric.',
            'languages_id.integer'  => 'Language Id should be numeric.',
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
