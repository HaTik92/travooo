<?php

namespace App\Http\Requests\Api\Restaurant;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class MediaRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'restaurant_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'restaurant_id.required' => "Restaurant Id not provided",
            'restaurant_id.integer' => "Restaurant Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
