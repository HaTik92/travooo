<?php

namespace App\Http\Requests\Api\Restaurant;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class ReviewRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text'  => 'required',
            'score'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'text.required' => "Review not provided",
            'score.required' => "Rating not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
