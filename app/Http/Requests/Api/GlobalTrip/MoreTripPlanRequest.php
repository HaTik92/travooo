<?php

namespace App\Http\Requests\Api\GlobalTrip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class MoreTripPlanRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pagenum'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'pagenum.required' => "Page Number not provided",
            'pagenum.integer' => "Page Number must be a interger",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
