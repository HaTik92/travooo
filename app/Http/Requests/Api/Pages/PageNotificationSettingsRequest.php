<?php

namespace App\Http\Requests\Api\Pages;

use Illuminate\Foundation\Http\FormRequest;

class PageNotificationSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'                           => 'required|integer',
            'session_token'                     => 'required',
            'page_id'                           => 'required|integer',
            'follow_notification'               => 'required|regex:/^[0-1]$/',
            'message_notification'              => 'required|regex:/^[0-1]$/',
            'comment_notification'              => 'required|regex:/^[0-1]$/',
            'like_notification'                 => 'required|regex:/^[0-1]$/',
            'travooo_announcement_notification' => 'required|regex:/^[0-1]$/',
            'email_notification'                => 'required|regex:/^[0-1]$/'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'                           => 'User Id not provided.',
            'user_id.integer'                            => 'User Id should be numeric.',
            'session_token.required'                     => 'Session Token not provided.',
            'page_id.required'                           => 'Page Id Not Provided.',
            'page_id.integer'                            => 'Page Id Should Be An Integer.',
            'follow_notification.required'               => 'Follow notification setting not provided.',
            'follow_notification.regex'                  => 'Invalid "Follow Notification" value provided.',
            'message_notification.required'              => 'Message notification setting not provided.',
            'message_notification.regex'                 => 'Invalid "Message Notification" value provided.',
            'comment_notification.required'              => 'Comment notification not provided.',
            'comment_notification.regex'                 => 'Invalid "Comment Notification" value provided.',
            'like_notification.required'                 => 'Like notification setting not provided.',
            'like_notification.regex'                    => 'Invalid "Like Notification" value provided.',
            'travooo_announcement_notification.required' => 'Travooo announcement notification setting not provided.',
            'travooo_announcement_notification.regex'    => 'Invalid "Travooo Announcement Notification" value provided.',
            'email_notification.required'                => 'Email notification setting not provided.',
            'email_notification.regex'                   => 'Invalid "Email Notification" value provided.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
