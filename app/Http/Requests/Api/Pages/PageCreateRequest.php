<?php

namespace App\Http\Requests\Api\Pages;

use Illuminate\Foundation\Http\FormRequest;

class PageCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'       => 'required|integer',
            'session_token' => 'required',
            'language_id'   => 'required|integer',
            'name'          => 'required|max:30',
            'description'   => 'required',
            'category_id'   => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'       => 'User Id not provided.',
            'user_id.integer'        => 'User Id should be numeric.',
            'session_token.required' => 'Session Token not provided.',
            'language_id.required'   => 'Language Id Not Provided.',
            'language_id.integer'    => 'Language Id Should Be An Integer.',
            'name.required'          => 'Name not provided.',
            'name.max'               => 'Name can only be of < 30 characters.',
            'description.required'   => 'Description not provided.',
            'category_id.required'   => 'Category Id not provided.',
            'category_id.integer'    => 'Category Id should be an integer.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
