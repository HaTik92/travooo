<?php

namespace App\Http\Requests\Api\Pages;

use Illuminate\Foundation\Http\FormRequest;

class PageRemoveAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'       => 'required|integer',
            'session_token' => 'required',
            'page_id'       => 'required|integer',
            'admin_id'      => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'       => 'User Id not provided.',
            'user_id.integer'        => 'User Id should be numeric.',
            'session_token.required' => 'Session Token not provided.',
            'page_id.required'       => 'Page Id Not Provided.',
            'page_id.integer'        => 'Page Id Should Be An Integer.',
            'admin_id.required'      => 'Admin Id Not Provided.',
            'admin_id.integer'       => 'Admin Id Should Be An Integer.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
