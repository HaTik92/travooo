<?php

namespace App\Http\Requests\Api\Media;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CommentReplyRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text'  => 'required',
            'comment_id'  => 'required|integer',
            'media_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'text.required' => "Comment not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
