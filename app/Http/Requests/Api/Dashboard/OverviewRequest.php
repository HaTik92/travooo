<?php

namespace App\Http\Requests\Api\Dashboard;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class OverviewRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filter'  => 'sometimes|in:7,15,30',
        ];
    }

    public function messages()
    {
        return [
            'filter.in' => "Filter Term must be one of this: 7,15,30",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
