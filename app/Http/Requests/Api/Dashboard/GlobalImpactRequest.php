<?php

namespace App\Http\Requests\Api\Dashboard;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class GlobalImpactRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
