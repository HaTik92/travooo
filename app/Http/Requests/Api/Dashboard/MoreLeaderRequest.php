<?php

namespace App\Http\Requests\Api\Dashboard;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class MoreLeaderRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pagenum'   => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'pagenum.required'  => "Pagenum must be filled in",
            'pagenum.integer'   => "Pagenum must be a integer"
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
