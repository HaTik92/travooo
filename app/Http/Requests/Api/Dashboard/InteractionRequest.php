<?php

namespace App\Http\Requests\Api\Dashboard;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class InteractionRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filter'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'filter.required' => "Filter Term not provided",
            'filter.integer' => "Filter Term must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
