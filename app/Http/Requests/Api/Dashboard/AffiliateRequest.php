<?php

namespace App\Http\Requests\Api\Dashboard;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class AffiliateRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pagenum'  => 'required|integer',
            'order'  => 'string|sometimes',
        ];
    }

    public function messages()
    {
        return [
            'pagenum.required' => "Page Number not provided",
            'pagenum.integer' => "Page Number must be a integer",
            'order.string' => "Order Type must be a string",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
