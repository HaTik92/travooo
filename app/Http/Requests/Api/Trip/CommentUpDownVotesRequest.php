<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CommentUpDownVotesRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment_id'  => 'required|integer',
            'vote_type'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'comment_id.required' => "Comment Id not provided",
            'comment_id.integer' => "Comment Id must be a integer",
            'vote_type.required' => "Vote Type not provided",
            'vote_type.integer' => "Vote Type must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
