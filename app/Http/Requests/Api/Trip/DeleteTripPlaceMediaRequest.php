<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class DeleteTripPlaceMediaRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'media_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'media_id.required' => "Media Id not provided",
            'media_id.integer' => "Media Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
