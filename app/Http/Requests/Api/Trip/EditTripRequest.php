<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class EditTripRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_id'  => 'required|integer',
            'title'  => 'string|required|max:100',
            'description'  => 'string|nullable|max:260',
            'actual_start_date'  => 'required',
            'privacy'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'trip_id.integer' => "Trip Id must be a integer",
            'trip_id.required' => "Trip Id not provided",
            'title.required' => "Title not provided",
            'description.required' => "Description not provided",
            'actual_start_date.required' => "Actual Start Date not provided",
            'privacy.required' => "Privacy not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
