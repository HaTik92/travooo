<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;
use App\Services\Api\PrimaryPostService;

class ChangePrivacyRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'privacy' => 'required|in:0,1,2'
        ];
    }

    public function messages()
    {
        return [
            'privacy.required'      => "Privacy not provided",
            'privacy.in'            => "Privacy must be in list 0,1,2",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
