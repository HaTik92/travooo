<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class removePlaceFromTripRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_place_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'trip_place_id.integer' => "Trip Place Id must be a integer",
            'trip_place_id.required' => "Trip Place Id not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
