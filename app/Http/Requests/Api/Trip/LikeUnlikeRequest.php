<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class LikeUnlikeRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'  => 'required|integer',
            // 'place_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'id.required' => "Post Id not provided",
            'id.integer' => "Post Id must be a integer",
            'place_id.required' => "Place Id not provided",
            'place_id.integer' => "Place Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
