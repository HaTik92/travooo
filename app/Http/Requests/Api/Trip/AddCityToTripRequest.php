<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class AddCityToTripRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id'  => 'required|integer',
            'trip_id'  => 'required|integer',
            'version_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'city_id.required' => "City Id not provided",
            'trip_id.required' => "Trip Id not provided",
            'version_id.required' => "Version Id not provided",

            'city_id.integer' => "City Id must be a integer",
            'trip_id.integer' => "Trip Id must be a integer",
            'version_id.integer' => "Version Id must be a integer"
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
