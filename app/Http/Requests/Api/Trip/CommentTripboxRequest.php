<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CommentTripboxRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_id'  => 'required|integer',
            'text'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'post_id.required' => "Post Id not provided",
            'post_id.integer' => "Post Id must be a integer",
            'text.required' => "comments not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
