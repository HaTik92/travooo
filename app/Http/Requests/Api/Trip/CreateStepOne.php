<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;

class CreateStepOne extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'         => 'string|required|max:100',
            'description'   => 'string|nullable|max:260',
            'privacy'       => 'required|in:public,friends,private',
            'plan_type'     => 'required|in:memory,upcoming',
        ];

        if (!($this->cover == null || $this->cover == "null" || $this->cover == "")) {
            $rules = array_merge($rules, [
                'cover'         => 'mimes:jpeg,jpg,png,bmp,tiff,gif,heic',
            ]);
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'title.required'        => "Title not provided",
            'description.required'  => "Description not provided",
            'privacy.required'      => "Privacy not provided",
            'privacy.in'            => "Privacy must be in list public, friends, private",
            'plan_type.in'          => "Plan Type must be in list memory, upcoming",
            'cover.mimes'           => "The type of the uploaded cover should be an image.",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
