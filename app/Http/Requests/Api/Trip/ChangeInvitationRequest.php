<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class ChangeInvitationRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'   => 'required|integer',
            'trip_id'   => 'required|integer',
            'role'      => 'required|in:editor,admin,viewer',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'  => "User Id not provided",
            'user_id.integer'   => "User Id must be a integer",
            'trip_id.required'  => "Trip Id not provided",
            'trip_id.integer'   => "Trip Id must be a integer",
            'role.required'     => "Role not provided",
            'role.in'           => "Role must be in list editor, admin, viewer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
