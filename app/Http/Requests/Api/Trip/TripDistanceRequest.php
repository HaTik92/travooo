<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class TripDistanceRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trips_id'  => 'required|integer',
            'traval_mode'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'trips_id.required' => "Trip Id not provided",
            'trips_id.integer' => "Trip Id must be a integer",
            'traval_mode.required' => "Travel Mode not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
