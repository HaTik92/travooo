<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class UploadMediaRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'  => 'mimes:jpeg,jpg,png,gif|required|max:10000',
            // 'place_id'  => '|integer',
            // 'trip_place_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'file.mimes' => "The type of the uploaded file should be an image.",
            'file.required' => "File not provided",
            'trip_id.required' => "Trip Id not provided",
            // 'place_id.required' => "Place Id not provided",
            // 'trip_place_id.required' => "Trip Place Id not provided",
            'trip_id.integer' => "Trip Id must be a integer",
            // 'place_id.integer' => "Place Id must be a integer",
            // 'trip_place_id.integer' => "Trip Place Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
