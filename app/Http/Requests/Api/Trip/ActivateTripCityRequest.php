<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class ActivateTripCityRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_city_id'  => 'required|integer',
            'transportation'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'trip_city_id.required' => "Trip City Id not provided",
            'transportation.required' => "Transportation not provided",
            'trip_city_id.integer' => "Trip City Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
