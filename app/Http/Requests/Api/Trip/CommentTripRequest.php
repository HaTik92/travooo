<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CommentTripRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plan_id'  => 'required|integer',
            'text'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'plan_id.required' => "Plan Id not provided",
            'plan_id.integer' => "Plan Id must be a integer",
            'text.required' => "Comment not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
