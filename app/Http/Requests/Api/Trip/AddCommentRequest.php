<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class AddCommentRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_id'  => 'required|integer',
            'description_id'  => 'required|integer',
            'place_id'  => 'required|integer',
            'comment'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'trip_id.required' => "Trip Id not provided",
            'trip_id.integer' => "Trip Id must be a integer",
            'description_id.required' => "Description Id not provided",
            'description_id.integer' => "Description Id must be a integer",
            'place_id.required' => "Place Id not provided",
            'place_id.integer' => "Place Id must be a integer",
            'comment.required' => "Comment not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
