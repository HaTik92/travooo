<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class SearchPlaceRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'term'  => 'required',
            'cities_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'term.required' => "Search Term not provided",
            'cities_id.required' => "Cities Id not provided",
            'cities_id.integer' => "Cities Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
