<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class ActivateTripPlaceRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_place_id'  => 'required|integer',
            'trip_id'  => 'required|integer',
            'version_id'  => 'required|integer',
            'place_id'  => 'required|integer',
            'city_id'  => 'required|integer',
            'budget'  => 'required|integer',
            'duration_hours'  => 'required|integer',
            'duration_minutes'  => 'required|integer',
            'time'  => 'required',
            'actual_place_date'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'trip_place_id.required' => "Trip Place Id not provided",
            'version_id.required' => "Version Id not provided",
            'trip_id.required' => "Trip Id not provided",
            'city_id.required' => "City Id not provided",
            'place_id.required' => "Place Id not provided",
            'budget.required'   => "Budget not provided",
            'duration_hours.required' => "Duration Hours not provided",
            'duration_minutes.required' => "Duration Minutes not provided",
            'time.required' => "Time not provided",

            'trip_place_id.required' => "Trip Place Id must be a integer",
            'version_id.required' => "Version Id must be a integer",
            'trip_id.required' => "Trip Id must be a integer",
            'city_id.required' => "City Id must be a integer",
            'place_id.required' => "Place Id must be a integer",
            'budget.required'   => "Budget must be a integer",
            'duration_hours.required' => "Duration Hours must be a integer",
            'duration_minutes.required' => "Duration Minutes must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
