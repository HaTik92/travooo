<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class PostShareUnshareRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'trip_id.required' => "Trip Id not provided",
            'trip_id.integer' => "Trip Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
