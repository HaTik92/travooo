<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;

class ModifyStepOneRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'string|required|max:100',
            'description'   => 'string|nullable|max:260',
            'privacy'       => 'required|in:public,friends,private',
        ];
    }

    public function messages()
    {
        return [
            'title.required'        => "Title not provided",
            'description.required'  => "Description not provided",
            'privacy.required'      => "Privacy not provided",
            'privacy.in'            => "Privacy must be in list public, friends, private",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
