<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class TripRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'memory'  => 'integer',
            'do'  => 'string',
            'city_id'  => 'integer',
        ];
    }

    public function messages()
    {
        return [
            'memory.integer' => "Memory must be a integer",
            'do.integer' => "Do must be a string",
            'city_id.integer' => "City Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
