<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class AddPlaceToTripRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id'   => 'required|integer',
            'place_id'  => 'required',
            'date'      => 'required',
            'time'      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'city_id.integer'   => "The city id field must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
