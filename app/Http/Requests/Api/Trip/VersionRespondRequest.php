<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class VersionRespondRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_id'  => 'required|integer',
            'version_id'  => 'required|integer',
            'action'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'trip_id.required' => "Trip Id not provided",
            'version_id.required' => "Version Id not providex",
            'action.required' => "Action not provided",

            'trip_id.integer' => "Trip Id must be a integer",
            'version_id.integer' => "Version Id must be a string",
            'action.integer' => "Action must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
