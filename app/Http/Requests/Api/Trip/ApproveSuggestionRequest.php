<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class ApproveSuggestionRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'suggestion_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'suggestion_id.required' => "Suggestion Id not provided",
            'suggestion_id.integer' => "Suggestion Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
