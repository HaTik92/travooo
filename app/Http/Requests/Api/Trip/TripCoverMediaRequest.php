<?php

namespace App\Http\Requests\Api\Trip;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;

class TripCoverMediaRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cover'  => 'required|mimes:jpeg,jpg,png,gif',
        ];
    }

    public function messages()
    {
        return [
            'cover.required' => "Cover not provided",
            'cover.mimes' => "The type of the uploaded cover should be an image.",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
