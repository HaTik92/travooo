<?php

namespace App\Http\Requests\Api\Reports;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class UploadImageRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'report_id'  => 'required',
            'local_image'  => 'mimes:jpeg,jpg,png,gif|required|max:10000',
        ];
    }

    public function messages()
    {
        return [
            'report_id.required' => "Report Id not provided",
            'local_image.mimes' => "The type of the uploaded file should be an image.",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
