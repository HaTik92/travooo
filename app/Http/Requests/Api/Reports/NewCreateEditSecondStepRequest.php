<?php

namespace App\Http\Requests\Api\Reports;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;
use Illuminate\Foundation\Http\FormRequest;

class NewCreateEditSecondStepRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  => 'required',
            'report_id'  => 'required',
            'saving_mode'  => 'required',
            'info'  => 'sometimes|array',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
