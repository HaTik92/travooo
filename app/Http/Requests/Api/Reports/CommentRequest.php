<?php

namespace App\Http\Requests\Api\Reports;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CommentRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'report_id'  => 'required',
            'text'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'report_id.required' => "Report Id not provided",
            'text.required' => "Comments not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
