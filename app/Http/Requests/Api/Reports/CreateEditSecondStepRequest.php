<?php

namespace App\Http\Requests\Api\Reports;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CreateEditSecondStepRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  => 'required',
            'sort'  => 'required',
            'report_id'  => 'required',
            'saving_mode'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => "Title not provided",
            'sort.required' => "Sort not provided",
            'report_id.required' => "Report Id not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
