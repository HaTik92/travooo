<?php

namespace App\Http\Requests\Api\Reports;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class ReportListRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filter_country'  => 'sometimes|string',
            'filter_city'  => 'sometimes|string',
            'order'  => 'sometimes|string',
            'type'  => 'sometimes|string',
        ];
    }

    public function messages()
    {
        return [
            'filter_country.string' => "Country must be a string",
            'filter_city.string' => "City must be a string",
            'order.string' => "Order must be a string",
            'type.string' => "Type must be a string",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
