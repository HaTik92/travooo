<?php

namespace App\Http\Requests\Api\Reports;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CreateEditFirstStepRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  => 'required',
            'locations_ids'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => "Title not provided",
            'description.required' => "Description not provided",
            'locations_ids.required' => "Location Ids not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
