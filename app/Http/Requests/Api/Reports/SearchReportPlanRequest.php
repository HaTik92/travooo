<?php

namespace App\Http\Requests\Api\Reports;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class SearchReportPlanRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'filter'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'filter.required' => "Filter not provided"
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
