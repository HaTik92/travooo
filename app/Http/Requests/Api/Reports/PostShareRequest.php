<?php

namespace App\Http\Requests\Api\Reports;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class PostShareRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_id'  => 'required',
            'permission'  => 'required',
            'text'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'post_id.required' => "Post Id not provided",
            'permission.required' => "Permission not provided",
            'text.required' => "Comments not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
