<?php

namespace App\Http\Requests\Api\Countries;

use Illuminate\Foundation\Http\FormRequest;

class CountryShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'country_id.required' => 'Country Id Not Provided.',
            'country_id.numeric'  => 'Country Id must be numeric.',
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
