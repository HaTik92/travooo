<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;
use Illuminate\Validation\Rule;

class ReviewRequest extends PaginationRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return parent::rules() + [
            'sort' => ['required', Rule::in(['date_down', 'date_up'])],
            'score' => 'sometimes|integer|digits_between:1,5'
        ];
    }

    public function messages()
    {
        return parent::messages() + [
            'sort.required' => "Sort not provided",
            'score.integer' => "Score must be a integer",
            'score.digits_between' => "Score must be between 1 and 5",
            'score.required' => "Score not provided",
        ];
    }

    public function defaultValue()
    {
        return parent::defaultValue() + [
            'sort' => 'date_down',
            'score' => ''
        ];
    }

}
