<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class TimlineMediasRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'  => 'required|integer',
            'type'  => 'string|sometimes',
            'media_type'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'user_id.integer' => "User Id must be a integer",
            'user_id.required' => "User Id not provided",
            'type.string' => "Type must be a string",
            'media_type.required' => "Media Type not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
