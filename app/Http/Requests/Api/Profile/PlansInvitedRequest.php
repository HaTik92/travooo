<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class PlansInvitedRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pagenum'  => 'integer|sometimes',
            'sort'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'pagenum.integer' => "Page Number must be a integer",
            'sort.required' => "Sort not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
