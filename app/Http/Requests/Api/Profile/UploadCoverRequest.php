<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class UploadCoverRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cover'     => 'mimes:jpeg,jpg,png,gif|sometimes|max:10000',
            'image'     => 'required',
            'photo'     => 'required',
            'type'      => 'required'
        ];
    }

    public function messages()
    {
        return [
            'cover.mimes'       => "The type of the uploaded file should be an image",
            'cover.max'         => "File  Maximum characters are 10000",
            'image.required'    => "Image must be filled in",
            'photo.required'    => "Photo must be filled in",
            'type.required'     => "Type must be filled in"
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
