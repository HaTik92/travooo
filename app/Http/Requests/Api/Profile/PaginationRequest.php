<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class PaginationRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page'  => 'integer|min:1|required',
            'per_page'  => 'integer|min:1|required',
        ];
    }

    public function messages()
    {
        return [
            'page.integer' => "Page must be a integer",
            'page.min' => "Page must be greater then 0",
            'page.required' => "Page not provided",
            'per_page.integer' => "Per page must be a integer",
            'per_page.min' => "Per page must be greater then 0",
            'per_page.required' => "Per page not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }

    public function defaultValue()
    {
        return [
            'page' => 1,
            'per_page' => 12,
        ];
    }

    protected function getValidatorInstance()
    {
        $data = $this->all();
        $isChanged = false;

        foreach ($this->defaultValue() as $item => $value) {
            if (empty($data[$item])) {
                $data[$item] = $value;
                $isChanged = true;
            }
        }

        if ($isChanged) {
            $this->getInputSource()->replace($data);
        }

        return parent::getValidatorInstance();
    }
}
