<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class TripPlanRequest extends PaginationRequest
{
    CONST SORT_LIST = ['asc', 'desc', 'creation_date'];
    CONST FILTER_LIST = ['all', 'upcoming', 'memory'];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return parent::rules() + [
            'sort'  => 'sometimes|in:' . implode(",", self::SORT_LIST),
            'filter' => 'sometimes|in:' . implode(",", self::FILTER_LIST)
        ];
    }

    public function messages()
    {
        return parent::messages() + [
            'sort.in' => 'Sort value must be in list: ' . implode(",", self::SORT_LIST),
            'filter.in' => 'Filter value must be in list: ' . implode(",", self::FILTER_LIST)
        ];
    }

    public function defaultValue()
    {
        return parent::defaultValue() + [
                'sort' => 'desc',
                'filter' => 'all'
            ];
    }

}
