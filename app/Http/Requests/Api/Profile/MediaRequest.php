<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;
use Illuminate\Validation\Rule;

class MediaRequest extends PaginationRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return parent::rules() + [
                'sort' => ['required', Rule::in(['date_down', 'date_up', 'most_liked', 'most_commented'])]
            ];
    }

    public function defaultValue()
    {
        return parent::defaultValue() + [
                'sort' => 'date_down'
            ];
    }
}
