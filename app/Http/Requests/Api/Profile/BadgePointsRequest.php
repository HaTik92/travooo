<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class BadgePointsRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'  => 'required|integer',
            'pagenum'  => 'integer|sometimes',
        ];
    }

    public function messages()
    {
        return [
            'user_id.integer' => "User Id must be a integer",
            'user_id.required' => "User Id not provided",
            'pagenum.integer' => "Page Number must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
