<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class FollowUserRequest extends PaginationRequest
{

    CONST TYPE_LIST = ['all', 'people', 'countries', 'cities', 'places'];

    public function rules()
    {
        return parent::rules() + [
            'type' => 'sometimes|in:' . implode(",", self::TYPE_LIST)
        ];
    }

    public function messages()
    {
        return parent::messages() + [
            'type.in' => 'Type value must be in list: ' . implode(",", self::TYPE_LIST),
        ];
    }

    public function defaultValue()
    {
        return parent::defaultValue() + [
                'type' => 'all'
            ];
    }
}
