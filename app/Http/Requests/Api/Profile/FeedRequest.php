<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;
use Illuminate\Validation\Rule;

class FeedRequest extends PaginationRequest
{
    CONST TYPE_LIST = [
        'trip_plans',
        'travelogs',
        'discussions',
        'reviews',
        'all_feed'
    ];
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return parent::rules() + [
                'type' => 'sometimes|in:' . implode(",", self::TYPE_LIST)
            ];
    }

    public function messages()
    {
        return parent::messages() + [
                'type' => 'Type value must be in list: ' . implode(",", self::TYPE_LIST),
            ];
    }

    public function defaultValue()
    {
        return parent::defaultValue() + [
                'type' => 'all_feed'
            ];
    }
}
