<?php

namespace App\Http\Requests\Api\Profile;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class ExpertRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'social_links' => 'required|array'
        ];
    }

    public function messages()
    {
        return [
            'social_links.required' => "Any one field is required",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
