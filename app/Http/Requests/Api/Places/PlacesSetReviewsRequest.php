<?php

namespace App\Http\Requests\Api\Places;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PlacesSetReviewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'  => 'required|integer',
            'score'    => 'required|regex:/^[1-5]{1}$/'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'User Id not provided.',
            'user_id.integer'  => 'User Id should be numeric.',
            'score.required'   => 'Score not provided.',
            'score.regex'      => 'Score must be 1-5.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
