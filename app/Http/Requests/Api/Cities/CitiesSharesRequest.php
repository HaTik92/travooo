<?php

namespace App\Http\Requests\Api\Cities;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CitiesSharesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'scope' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'scope.required' => 'Scope not provided.',
            'scope.integer'  => 'Scope should be numeric.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
