<?php

namespace App\Http\Requests\Api\Cities;

use Illuminate\Foundation\Http\FormRequest;

class CityShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'city_id.required' => 'City Id Not Provided.',
            'city_id.numeric'  => 'City Id must be numeric.',
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
