<?php

namespace App\Http\Requests\Api\Cities;

use Illuminate\Foundation\Http\FormRequest;

class CityMediasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'language_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'language_id.required' => 'Language Id Not Provided.',
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
