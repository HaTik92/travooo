<?php

namespace App\Http\Requests\Api\User;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class StepSevenRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'travel_styles' => 'required|array|nullable',
        ];
    }

    public function messages()
    {
        return [
            'travel_styles.required'    => 'Travel styles is required',
            'travel_styles.array'    => 'Travel styles should be in array.',
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
