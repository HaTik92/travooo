<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateContentPrivacyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'                       => 'required|integer',
            'session_token'                 => 'required',
            'permission_friendship_request' => 'integer',
            'permission_follow'             => 'integer',
            'permission_message'            => 'integer',
            'permission_view_email_phone'   => 'integer',
            'permission_view_last_seen'     => 'integer',
            'permission_view_address'       => 'integer',
            'permission_view_post'          => 'integer',
            'permission_view_friendlist'    => 'integer',
            'permission_view_age'           => 'integer',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'                      => 'User Id not provided.',
            'user_id.integer'                       => 'User Id should be numeric.',
            'session_token.required'                => 'Session Token not provided.',
            'permission_friendship_request.integer' => 'Invalid Value For \'permission_friendship_request\', Should Be An Integer.',
            'permission_follow.integer'             => 'Invalid Value For \'permission_follow\', Should Be An Integer.',
            'permission_message.integer'            => 'Invalid Value For \'permission_message\', Should Be An Integer.',
            'permission_view_email_phone.integer'   => 'Invalid Value For \'permission_view_email_phone\', Should Be An Integer.',
            'permission_view_last_seen.integer'     => 'Invalid Value For \'permission_view_last_seen\', Should Be An Integer.',
            'permission_view_address.integer'       => 'Invalid Value For \'permission_view_address\', Should Be An Integer.',
            'permission_view_post.integer'          => 'Invalid Value For \'permission_view_post\', Should Be An Integer.',
            'permission_view_friendlist.integer'    => 'Invalid Value For \'permission_view_friendlist\', Should Be An Integer.',
            'permission_view_age.integer'           => 'Invalid Value For \'permission_view_age\', Should Be An Integer.',

        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
