<?php

namespace App\Http\Requests\Api\User;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class StepFourExpertRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        return [
            'facebook'   => 'regex:' . $regex . '|nullable',
            'twitter'   => 'regex:' . $regex . '|nullable',
            'instagram' => 'regex:' . $regex . '|nullable',
            'pinterest' => 'regex:' . $regex . '|nullable',
            'tripadvisor'   => 'regex:' . $regex . '|nullable',
            'youtube'   => 'regex:' . $regex . '|nullable',
            'medium'  => 'regex:' . $regex . '|nullable',
            'website'    => 'regex:' . $regex . '|nullable',
            'vimeo'    => 'regex:' . $regex . '|nullable',
        ];
    }

    public function messages()
    {
        return [
            'facebook.regex'    => 'Facebook link has invalid format',
            'twitter.regex'    => 'Twitter link has invalid format',
            'instagram.regex'  => 'Instagram link has invalid format',
            'pinterest.regex'    => 'Pinterest link has invalid format',
            'tripadvisor.regex'   => 'Trip Advisor link has invalid format',
            'youtube.regex'  => 'Youtube link has invalid format',
            'medium.regex'  => 'Medium link has invalid format',
            'website.regex'  => 'website link has invalid format',
            'vimeo.regex'  => 'vimeo link has invalid format',
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
