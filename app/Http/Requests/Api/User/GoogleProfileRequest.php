<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;

class GoogleProfileRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'userID' => 'required',
            'userName' => 'required',
            'profile_picture' => ''
        ];
    }

    /**
     * Get the validation messages
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'email must be required.',
            'userID.required' => 'userID must be required.',
            'userName.required' => 'userName must be required.'
        ];
    }

    /**
     * Get the validation messages
     * 
     * @param array $errors
     * @return ApiResponse 
     */
    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
