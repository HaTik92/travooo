<?php

namespace App\Http\Requests\Api\User;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class StepFiveRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'places' => 'array|nullable',
        ];
    }

    public function messages()
    {
        return [
            'places.array'    => 'Cities should be in array.',
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
