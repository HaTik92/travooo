<?php

namespace App\Http\Requests\Api\User;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class StepConfirmationEmailRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'confirmation_code' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'confirmation_code.required'           => 'Confirmation Code not provided.',
            'confirmation_code.string'             => 'Confirmation Code is not properly formated.'
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
