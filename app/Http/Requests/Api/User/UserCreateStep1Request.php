<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateStep1Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'              => 'required|min:3|max:255|regex:/^[a-zA-Z0-9 ]+$/|unique:users,username',
            'email'                 => 'required|email|unique:users,email',
            'password'              => 'required|min:6|max:20|confirmed|regex:/^[a-zA-Z0-9_]+$/',
            'password_confirmation' => 'required|min:6|max:20|required_with:password|same:password',
        ];
    }

    public function messages()
    {
        return [
            'username.required'                    => 'Username not provided.',
            'username.digits_between'              => 'Length of username should be between (8-32) characters.',
            'username.regex'                       => 'Username can only contain alphanumeric characters.',
            'email.required'                       => 'Password not provided.',
            'email.email'                          => 'Not valid email address.',
            'password.required'                    => 'Password not provided.',
            'password.regex'                       => 'Password can only contain alphanumeric or underscore characters.',
            'password.min'                         => 'Length of password should be between (3-255) characters.',
            'password.max'                         => 'Length of password should be between (3-255) characters.',
            'password_confirmation.required'       => 'Confirm password not provided.',
            'password_confirmation.digits_between' => 'Length of confirm password should be between (6-20) characters.',
            'password_confirmation.min'            => 'Length of confirm password should be between (6-20) characters.',
            'password_confirmation.max'            => 'Length of confirm password should be between (6-20) characters.',
        ];
    }

    public function response(array $errors)
    {
        $result = [
            'status' => false,
            'message' => $errors
        ];
        return response()->json($result, 200);
    }
}
