<?php

namespace App\Http\Requests\Api\User;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class StepThreeCompanyRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        return [
            'name' => 'required|string|min:1|max:15|regex:/^([A-Za-z0-9_])+$/',
            'website' => 'required|regex:' . $regex,
            'birth_date' => 'required|date',
            'nationality' => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'name.required'           => 'Name not provided.',
            'name.min'                => 'Length of name should be between (1-255) characters.',
            'name.max'                => 'Length of name should be between (3-255) characters.',
            'name.regex'              => 'Name can only contain alphanumeric characters.',
            'website.required'        => 'Website not provided.',
            'website.regex'           => 'Website link has invalid format',
            'birth_date.required'     => 'Birthdate not provided',
            'birth_date.date'         => 'Birthdate format is not valid.',
            'nationality.required'    => 'Nationality not provided.',
            'nationality.integer'     => 'Nationality must be integer'
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
