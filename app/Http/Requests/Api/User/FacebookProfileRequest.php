<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;

class FacebookProfileRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required'
        ];
    }

    /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'token.required' => 'Token must be required.',
        ];
    }

    /**
     * Get the validation messages
     * 
     * @param array $errors
     * @return ApiResponse 
     */
    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
