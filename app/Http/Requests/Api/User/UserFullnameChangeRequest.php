<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class UserFullnameChangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'       => 'required|integer',
            'session_token' => 'required',
            'fullname'      => 'required|min:8|max:32|regex:/^[a-zA-Z0-9._ ]+$/'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'       => 'User Id not provided.',
            'user_id.integer'        => 'User Id should be numeric.',
            'session_token.required' => 'Session Token not provided.',
            'fullname.required'      => 'Age must be integer.',
            'fullname.min'           => 'Fullname length should be between (8-32) characters.',
            'fullname.max'           => 'Fullname length should be between (8-32) characters.',
            'fullname.regex'         => 'Fullname can only contain alphanumeric characters (A-Z,a-z,0-9,\' \',\'.\',\'_\').',
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
