<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class UserForgotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'email.required'          => 'Password not provided.',
            'email.email'             => 'Not valid email address.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
