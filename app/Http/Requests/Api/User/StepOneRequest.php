<?php

namespace App\Http\Requests\Api\User;

use App\Models\User\User;
use Illuminate\Validation\Rule;
use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class StepOneRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $email = $this->request->get('email');

        return [
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->where(function ($query) use ($email) {
                    $query->where('email', $email);
                    return $query->where('type', '!=', User::TYPE_INVITED_EXPERT);
                })
            ],
            'birth_date' => 'required',
            'gender' => 'required|integer|in:0,1,2',
            'nationality' => 'required|integer',
            'type' => 'required|integer',
            'name' => 'required|string|max:50',
            'username' => 'required|string|min:4|max:15|regex:/^([A-Za-z0-9_])+$/|unique:users,username',
            'password' => 'required|string|min:6|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])(?=\S+$).{6,255}$/',
        ];
    }

    public function messages()
    {
        return [
            'email.required'                       => 'Email not provided.',
            'email.email'                          => 'Email format is not valid.',
            'password.required'                    => 'Password not provided.',
            'password.regex'                       => 'Password should contain 6 minimum characters consisting of at least 1 letter and number.',
            'password.min'                         => 'Password should contain 6 minimum characters consisting of at least 1 letter and number.',
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
