<?php

namespace App\Http\Requests\Api\User;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;
use App\Models\User\User;

class StepThreeRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:1|max:15|regex:/^([A-Za-z0-9_])+$/|unique:users,username,' . $this->get('user_id'),
            'name' => 'required|string|max:255',
            'birth_date' => 'required|date',
            'gender' => 'required|integer|in:0,1,2',
            'nationality' => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'username.required'       => 'Username not provided.',
            'username.digits_between' => 'Length of username should be between (8-32) characters.',
            'username.regex'          => 'Username can only contain alphanumeric characters.',
            'name.required'           => 'Name not provided.',
            'name.max'                => 'Length of name should be between (1-255) characters.',
            'birth_date.required'     => 'Birthdate not provided',
            'birth_date.date'         => 'Birthdate format is not valid.',
            'gender.required'         => 'Gender not provided.',
            'gender.integer'          => 'Gender can only contain following values (' . User::GENDER_MALE . '-Male, ' . User::GENDER_FEMALE . '-Female, ' . User::GENDER_UNSPECIFIED . '-Unspecified).',
            'nationality.required'    => 'Nationality not provided.',
            'nationality.integer'     => 'Nationality must be integer'
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
