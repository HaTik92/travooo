<?php

namespace App\Http\Requests\Api\User;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class StepTwoRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|integer',
            'token' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'type.required'           => 'Type not provided.',
            'type.integer'            => 'Type must be integer'
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
