<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'newpassword_confirmation' => 'required',
            'newpassword'              => 'required|min:6|max:32|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'newpassword.required'              => 'New Password Not Provided.',
            'newpassword_confirmation.required' => 'Password Confirmation Not Provided.',
            'newpassword.min'                   => 'New Password Length Should Be Between (6-32) characters.',
            'newpassword.max'                   => 'New Password Length Should Be Between (6-32) characters.',
            'newpassword.confirmed'             => 'New Password And Confirm Password Do not Match.',
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
