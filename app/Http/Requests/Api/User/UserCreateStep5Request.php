<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateStep5Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lifeStyles' => 'required|array'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'        => 'User Id not provided.',
            'user_id.integer'         => 'User Id should be numeric.',
            'lifeStyles.required'     => 'Travel styles not provided.'
        ];
    }

    public function response(array $errors)
    {
        $result = [
            'status' => false,
            'message' => $errors
        ];
        return response()->json($result, 200);
    }
}
