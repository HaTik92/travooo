<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateStep3Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'countries' => 'required|array'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'   => 'User Id not provided.',
            'user_id.integer'    => 'User Id should be numeric.',
            'countries.required' => 'Countries not provided.',
            'countries.array'    => 'Countries should be in array.'
        ];
    }

    public function response(array $errors)
    {
        $result = [
            'status' => false,
            'message' => $errors
        ];
        return response()->json($result, 200);
    }
}
