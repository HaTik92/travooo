<?php

namespace App\Http\Requests\Api\User;

use App\Models\User\User;
use Illuminate\Foundation\Http\FormRequest;

class UserCreateStep2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|min:3|max:255|regex:/^[a-zA-Z]+$/',
            'gender'  => 'required|integer',
            'age'     => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'User Id not provided.',
            'user_id.integer'  => 'User Id should be numeric.',
            'name.required'    => 'Name not provided.',
            'name.min'         => 'Length of name should be between (3-255) characters.',
            'name.max'         => 'Length of name should be between (3-255) characters.',
            'name.regex'       => 'Name can only contain alphabetic characters.',
            'gender.required'  => 'Gender not provided.',
            'gender.integer'   => 'Gender can only contain following values (' . User::GENDER_MALE . '-Male, ' . User::GENDER_FEMALE . '-Female, ' . User::GENDER_UNSPECIFIED . '-Unspecified).',
            'age.required'     => 'Age not provided.',
            'age.integer'      => 'Age must be integer.'
        ];
    }

    public function response(array $errors)
    {
        $result = [
            'status' => false,
            'message' => $errors
        ];
        return response()->json($result, 200);
    }
}
