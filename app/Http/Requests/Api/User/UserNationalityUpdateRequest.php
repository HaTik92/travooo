<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class UserNationalityUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'       => 'required|integer',
            'session_token' => 'required',
            'nationality'   => 'required'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'       => 'User Id not provided.',
            'user_id.integer'        => 'User Id should be numeric.',
            'session_token.required' => 'Session Token not provided.',
            'nationality.required'   => 'Nationality Not Provided.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
