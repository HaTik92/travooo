<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password'              => 'required',
            'new_password_confirmation' => 'required',
            'new_password'              => 'required|min:6|max:32|confirmed'
        ];
    }

    public function messages()
    {
        return [
            'old_password.required'              => 'Old Password Not Provided.',
            'new_password.required'              => 'New Password Not Provided.',
            'new_password_confirmation.required' => 'Password Confirmation Not Provided.',
            'new_password.min'                   => 'New Password Length Should Be Between (6-32) characters.',
            'new_password.max'                   => 'New Password Length Should Be Between (6-32) characters.',
            'new_password.confirmed'             => 'New Password And Confirm Password Do not Match.',
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                    'error' => 400,
                    'message' => current($errors)[0],
                ],
            'status' => false
        ]);
    }
}
