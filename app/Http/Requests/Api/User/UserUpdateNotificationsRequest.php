<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateNotificationsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'                           => 'required|integer',
            'session_token'                     => 'required',
            'message_notification'              => 'integer',
            'friendship_notification'           => 'integer',
            'comment_notification'              => 'integer',
            'nearby_activity_notification'      => 'integer',
            'travooo_announcement_notification' => 'integer',
            'plan_notification'                 => 'integer',
            'friend_group_request_notification' => 'integer',
            'follow_notification'               => 'integer',
            'email_notification'                => 'integer',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'                          => 'User Id not provided.',
            'user_id.integer'                           => 'User Id should be numeric.',
            'session_token.required'                    => 'Session Token not provided.',
            'message_notification.integer'              => 'Invalid Value For \'message_notification\', Should Be An Integer.',
            'friendship_notification.integer'           => 'Invalid Value For \'friendship_notification\', Should Be An Integer.',
            'comment_notification.integer'              => 'Invalid Value For \'comment_notification\', Should Be An Integer.',
            'nearby_activity_notification.integer'      => 'Invalid Value For \'nearby_activity_notification\', Should Be An Integer.',
            'travooo_announcement_notification.integer' => 'Invalid Value For \'travooo_announcement_notification\', Should Be An Integer.',
            'plan_notification.integer'                 => 'Invalid Value For \'plan_notification\', Should Be An Integer.',
            'friend_group_request_notification.integer' => 'Invalid Value For \'friend_group_request_notification\', Should Be An Integer.',
            'follow_notification.integer'               => 'Invalid Value For \'follow_notification\', Should Be An Integer.',
            'email_notification.integer'                => 'Invalid Value For \'email_notification\', Should Be An Integer.'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'data' => [
                'error' => 400,
                'message' => current($errors)[0],
            ],
            'status' => false
        ]);
    }
}
