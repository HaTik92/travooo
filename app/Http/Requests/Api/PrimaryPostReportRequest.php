<?php

namespace App\Http\Requests\Api;

use App\Http\Responses\ApiResponse;
use App\Models\Posts\Posts;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PrimaryPostReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'report_type'       => 'required|integer|in:' . implode(',', array_keys(Posts::reportType())),
        ];
    }

    public function messages()
    {
        return [
            'report_type.integer'  => 'Report Type must be an integer.',
            'report_type.in'       => 'Report type must be from (' . implode(',', array_keys(Posts::reportType())) . ')',
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
