<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;

class PostPrivacyRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(){
        $permissionList                 = [0, 1, 2, 3];
        return [
            'post_id'                   => 'required|integer',
            'permission'                => 'required|integer|in:'.implode(',', $permissionList),
        ];
    }

    public function messages(){
        $permissionList                 = [0, 1, 2, 3];
        return [
            'post_id.integer'           => "post_id must be an integer.",
            'permission.integer'        => 'Permission must be an integer.',
            'permission.in'             => 'Permission must be from ('.implode(',', $permissionList).')',
        ];
    }

    public function response(array $errors){
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
