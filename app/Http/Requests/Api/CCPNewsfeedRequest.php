<?php

namespace App\Http\Requests\Api;

use App\Http\Responses\ApiResponse;
use App\Services\Newsfeed\CCPNewsfeed;

class CCPNewsfeedRequest extends StepRequest
{
    public function rules()
    {
        $subTypes     = array_keys(CCPNewsfeed::SUB_TYPES);
        return [
            'page'          => 'required|integer',
            'tab'           => 'required|string',
            'sub_type'      => 'sometimes|string|in:' . implode(',', $subTypes),
        ];
    }

    public function messages()
    {
        return [];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
