<?php

namespace App\Http\Requests\Api\Place2;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class TripCommentsModelRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_id'  => 'required|integer',
            'skip'  => 'integer|sometimes',
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
