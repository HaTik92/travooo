<?php

namespace App\Http\Requests\Api\Place2;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class SearchAllDatasRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'q'  => 'required',
            'search_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'q.required'=>'Search Term not provided'
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
