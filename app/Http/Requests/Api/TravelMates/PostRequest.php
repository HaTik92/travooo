<?php

namespace App\Http\Requests\Api\TravelMates;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class PostRequest  extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'request_id'  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'request_id.required' => "Request Id not provided",
            'request_id.integer' => "Request Id must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
