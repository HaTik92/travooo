<?php

namespace App\Http\Requests\Api\TravelMates;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class FilterTravelMatesRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pagenum'  => 'required|integer',
            'type'  => 'integer|sometimes',
        ];
    }

    public function messages()
    {
        return [
            'pagenum.required' => "Page Number not provided",
            'pagenum.integer' => "Page Number must be a integer",
            'type.integer' => "Type must be a integer",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
