<?php

namespace App\Http\Requests\Api\TravelMates;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class CreateWithoutPlanRequest  extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'destinations_list' => 'required',
            'title' => 'required',
            'current_location' => 'required|integer',
            'mutual_start_date' => 'required',
            'mutual_end_date' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'destinations_list.required' => "Destinations List not provided",
            'title.required' => "Title not provided",
            'current_location.required' => "Current Location not provided",
            'current_location.integer' => "Current Location must be a integer",
            'mutual_start_date.required' => "Start Date not provided",
            'mutual_end_date.required' => "End Date not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
