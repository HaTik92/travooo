<?php

namespace App\Http\Requests\Api;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class PrimaryPostLikeUnlikeRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'flag'  => 'required|integer|in:0,1',
        ];
    }

    public function messages()
    {
        return [
            'flag.required' => "flag not provided.",
            'flag.integer'  => "flag must be in integer.",
            'flag.in'       => "flag must be in list 0, 1.",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
