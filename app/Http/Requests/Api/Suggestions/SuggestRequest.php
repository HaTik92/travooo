<?php

namespace App\Http\Requests\Api\Suggestions;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;
use App\Services\Trips\TripsSuggestionsService;

class SuggestRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $tripsSuggestions   = implode(",", TripsSuggestionsService::SUGGESTION_EDIT_TYPES);

        return [
            // 'suggestion_edit_type'      => 'required|in:' . $tripsSuggestions,
            'values'                    => 'required', //array
            // 'reason'                    => 'required',
        ];
    }

    public function messages()
    {
        // $tripsSuggestions   = implode(", ", TripsSuggestionsService::SUGGESTION_EDIT_TYPES);

        return [
            // 'suggestion_edit_type.required' => "Suggestion edit type field is required.",
            // 'suggestion_edit_type.in'       => "Suggestion edit type in list " . $tripsSuggestions,
            'values.array'                  => "values must be in array format",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
