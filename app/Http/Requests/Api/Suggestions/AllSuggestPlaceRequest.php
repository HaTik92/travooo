<?php

namespace App\Http\Requests\Api\Suggestions;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;

class AllSuggestPlaceRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_id'                   => 'required|integer',
            'place_id'                  => 'required|integer',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function response(array $errors)
    {
        return ApiResponse::create(
            $errors,
            false,
            ApiResponse::VALIDATION
        );
    }
}
