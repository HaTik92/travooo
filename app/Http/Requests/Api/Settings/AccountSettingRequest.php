<?php

namespace App\Http\Requests\Api\Settings;

use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\StepRequest;

class AccountSettingRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|string|min:1|max:15|regex:/^([A-Za-z0-9_])+$/|unique:users,username'
        ];
    }

    public function messages()
    {
        return [
            'username.required' => "User Name not provided",
        ];
    }

    public function response(array $errors)
    {
        return ApiResponse::create( $errors, false, ApiResponse::VALIDATION );
    }
}
