<?php

namespace App\Http\Requests\Api\CommonRequest;

use App\Http\Requests\Api\StepRequest;
use App\Http\Responses\ApiResponse;
use Illuminate\Foundation\Http\FormRequest;

class UploadMediaRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'          => 'required',
            'pair'          => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function response(array $errors)
    {
        return ApiResponse::create($errors, false, ApiResponse::VALIDATION);
    }
}
