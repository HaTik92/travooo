<?php

namespace App\Http\Requests\Frontend\User\Registration;

use App\Models\User\User;
use Illuminate\Validation\Rule;

class StepOneRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $email = $this->request->get('email');

        $rules = [
            'password' => 'required_with:confirmation_password|same:confirmation_password|string|min:6|regex:/^(?=.*[A-Za-z])(?=.*\d)[a-zA-Z0-9!@#$%^&*]{6,}$/',
            'confirmation_password' => 'min:6',
            'username' => 'required|string|min:4|max:15|regex:/^([A-Za-z0-9_])+$/|unique:users,username,' . $this->get('user_id'),
            'name' => 'required|string|max:50',
            'birth_date' => 'required|date',
            'gender' => 'required|integer|in:1,2',
            'nationality' => 'required|integer',
            'type' => 'required|integer',
            'recaptcha' => 'required|recaptcha'
        ];
                
         if($this->request->get('is_back') == 1){
            $rules['email'] = [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->where(function ($query) use ($email) {
                    return $query->where('email', $email)->where('type', '!=', User::TYPE_INVITED_EXPERT);
                })
            ];
           $rules['username'] = 'required|string|min:4|max:15|regex:/^([A-Za-z0-9_])+$/|unique:users,username,' . $this->get('user_id');
         } else{
             $rules['email'] = 'required|string|email|max:255';
             $rules['username'] = 'required|string|min:4|max:15|regex:/^([A-Za-z0-9_])+$/';
         }      
                
        return $rules;
    }

    public function messages()
    {
        return [
            'password.regex' => 'Password should contain 6 minimum characters consisting of at least 1 letter and number.'
        ];
    }
}
