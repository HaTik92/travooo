<?php

namespace App\Http\Requests\Frontend\User\Registration;

class StepFourRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'places' => 'required|array|nullable',
        ];
    }
}
