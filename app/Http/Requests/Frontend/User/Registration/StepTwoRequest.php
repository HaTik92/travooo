<?php

namespace App\Http\Requests\Frontend\User\Registration;

class StepTwoRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|integer'
        ];
    }
}
