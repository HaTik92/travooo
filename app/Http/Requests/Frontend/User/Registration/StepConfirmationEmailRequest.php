<?php

namespace App\Http\Requests\Frontend\User\Registration;

class StepConfirmationEmailRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'confirmation_code' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'confirmation_code.required' => 'Enter confirmation code.'
        ];
    }
}
