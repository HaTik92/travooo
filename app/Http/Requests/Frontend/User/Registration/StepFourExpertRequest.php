<?php

namespace App\Http\Requests\Frontend\User\Registration;

class StepFourExpertRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'website'   => 'string|nullable',
            'youtube'   => 'string|nullable',
            'twitter'   => 'string|nullable',
            'facebook'  => 'string|nullable',
            'instagram' => 'string|nullable',
            //'pinterest' => 'string|nullable',
            //'tumblr'    => 'string|nullable',
        ];
    }
}
