<?php

namespace App\Http\Requests\Frontend\User\Registration;

class StepThreeRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
            'cities'    => 'array|nullable',
            'countries' => 'array|nullable',
        ];
    }
}
