<?php

namespace App\Http\Requests\Frontend\User\Registration;

class StepSevenRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'travel_styles' => 'required|array|nullable',
        ];
    }
}
