<?php

namespace App\Http\Requests\Frontend\User\Registration;

class StepFiveRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'interests' => 'required|array|nullable',
        ];
    }
}
