<?php

namespace App\Http\Requests\Frontend\User\Registration;

class StepThreeCompanyRequest extends StepRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:50',
            'username' => 'required|string|min:1|max:15|regex:/^([A-Za-z0-9_])+$/|unique:users,username,' . $this->get('user_id'),
            'website' => 'required|string|max:255',
            'birth_date' => 'required|date',
            'nationality' => 'required|integer',
        ];
    }
}
