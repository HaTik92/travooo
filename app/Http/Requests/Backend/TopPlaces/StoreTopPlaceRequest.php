<?php

namespace App\Http\Requests\Backend\TopPlaces;

use App\Http\Requests\Request;
use App\Models\Access\language\Languages;
/**
 * Class StoreTopPlaceRequest.
 */
class StoreTopPlaceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(4);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'place_id' => 'required',
        ];
    }

}
