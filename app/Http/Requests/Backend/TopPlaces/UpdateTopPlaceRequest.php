<?php

namespace App\Http\Requests\Backend\TopPlaces;

use App\Http\Requests\Request;
/**
 * Class UpdateTopPlaceRequest.
 */
class UpdateTopPlaceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(4);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_number' => 'required|integer',
        ];
    }

}
