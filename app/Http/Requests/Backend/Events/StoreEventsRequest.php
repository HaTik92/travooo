<?php

namespace App\Http\Requests\Backend\Events;

use App\Http\Requests\Request;
use App\Models\Access\language\Languages;
/**
 * Class StoreEventsRequest
 */
class StoreEventsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(5);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = [];
//        $languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
//        foreach ($languages as $key => $language) {
//            $inputs['translations.'.$language->code.'.title']   = 'required|max:255';
//            $inputs['translations.'.$language->code.'.working_days']    = 'max:5000';
//            $inputs['translations.'.$language->code.'.when_to_go']      = 'max:5000';
//            $inputs['translations.'.$language->code.'.history']         = 'max:5000';
//        }
//        $inputs['translations.en.description']                      = 'required';
//        $inputs['translations.en.address']                          = 'required';
//        $inputs['translations.en.phone']                            = 'required';
//        $inputs['translations.en.working_days']                     = 'required';
//        $inputs['translations.en.when_to_go']                       = 'required';
//        $inputs['translations.en.price_level']                      = 'required';
//        $inputs['translations.en.history']                          = 'required';

        $inputs['name']           = 'required';
        $inputs['category']         = 'required';
        $inputs['description']      = 'required';
        $inputs['start']            = 'nullable|date';
        $inputs['end']              = 'nullable|date';
        $inputs['location_country_id']      = 'required';
        $inputs['cities_id']        = 'required';
        $inputs['lat_lng']          = 'required';
        return $inputs;
    }

    public function messages()
    {
        $inputs = [];
//        $languages = \DB::table('conf_languages')->where('active', 1)->get();
//        foreach ($languages as $key => $language) {
//            $inputs['translations.'.$language->code.'.title.required']   = 'Title is required for all languages';
//        }
//        $inputs['translations.en.description.required']     = 'Quick Facts is required for English language';
//        $inputs['translations.en.address.required']         = 'Address is required for English language';
//        $inputs['translations.en.phone.required']           = 'Phone is required for English language';
//        $inputs['translations.en.working_days.required']    = 'Working Hours is required for English language';
//        $inputs['translations.en.when_to_go.required']      = 'Best Time to Visit is required for English language';
//        $inputs['translations.en.price_level.required']     = 'Price Level is required for English language';
//        $inputs['translations.en.history.required']         = 'Website is required for English language';
        return $inputs;
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'category' => 'Category',
            'description' => 'Description',
            'start' => 'Date Start',
            'end' => 'Date End',
            'location_country_id' => 'Country',
            'cities_id' => 'City',
            'lat_lng' => 'Lat, Lng',
        ];
    }
}
