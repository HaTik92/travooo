<?php

namespace App\Http\Requests\Backend\Access\User;

use App\Http\Requests\Request;

/**
 * Class UpdateUserRequest.
 */
class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:191',
            'name'  => 'required|max:191',
            'pinterest' => ['nullable', 'regex:/(^$)|(null)|((?:(?:http|https):\/\/)?(?:www\.)?(?:pinterest\.com|instagr\.am)\/([A-Za-z0-9-_\.]+))/i'],
            'website' => ['nullable', 'regex:/(^$)|(null)|(^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$)/i'],
            'twitter' => ['nullable', 'regex:/(^$)|(null)|(^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/i'],
            'facebook' => ['nullable', 'regex:/(^$)|(null)|((?:https?:\/\/)?(?:www\.)?facebook\.com\/.(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-\.]*))/i'],
            'tumblr' => ['nullable', 'regex:/(^$)|(null)|([^"\/www\."](?<!w{3})[A-Za-z0-9]*(?=\.tumblr\.com)|(?<=\.tumblr\.com\/blog\/).*)/i'],
            'youtube' => ['nullable', 'regex:/(^$)|(null)|(^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$)/i'],
            'instagram' => ['nullable', 'regex:/(^$)|(null)|((https?:\/\/(?:www\.)?instagram\.com\/([^\/?#&]+)).*)/i'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'pinterest.regex' => 'Please enter a valid pinterest URL.',
            'website.regex' => 'Please enter a valid URL.',
            'twitter.regex' => 'Please enter a valid twitter URL.',
            'facebook.regex' => 'Please enter a valid facebook URL.',
            'tumblr.regex' => 'Please enter a valid tumblr URL.',
            'youtube.regex' => 'Please enter a valid youtube URL.',
            'instagram.regex' => 'Please enter a valid instagram URL.',
        ];
    }
}
