<?php

namespace App\Http\Requests\Backend\Access\Language;

use App\Http\Requests\Request;

/**
 * Class UpdateUserRequest.
 */
class UpdateLanguageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $codes = config('google-translate.codes');
        return [
            'title'     => 'required|max:255|unique:conf_languages,title,'.$this->route('language')->id,
            'code'      => 'required|max:255|unique:conf_languages,code,'.$this->route('language')->id.'|in:'.implode(',', $codes),
        ];
    }

    public function messages()
    {
        return [
            'title.unique'  => 'This language title already exist in the database',
            'code.unique'   => 'This language code already exist in the database',
            'code.in' => 'The language code is invalid, go to this link "https://cloud.google.com/translate/docs/languages"'
        ];
    }

}
