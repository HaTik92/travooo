<?php

namespace App\Http\Requests\Backend\Place;

use App\Http\Requests\Request;

/**
 * Class PlaceMassApplyFiltersRequest
 */
class PlaceMassApplyFiltersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(4);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id'=>'required_without_all:city_id|nullable|integer|min:1',
            'city_id'=>'required_without_all:country_id|nullable|integer|min:1',
            'address'=>'nullable|string',
            'address_not_match'=>'nullable|string',
            'pluscode'=>'nullable|string',
            'pluscode_not_match'=>'nullable|string',
        ];
    }

    public function attributes()
    {
        return [
            'country_id'    => 'Country',
            'city_id'       => 'City',
            'address'       => 'Address',
            'address_not_match'       => 'Address not match',
            'pluscode'       => 'Pluscode',
            'pluscode_not_match'       => 'Pluscode not match',
        ];
    }
}
