<?php

namespace App\Http\Requests\Backend\Place;

use App\Http\Requests\Request;

/**
 * Class PlaceMassApplyMoveToRequest
 */
class PlaceMassApplyMoveToRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(4);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id'=>'required|integer|min:1|exists:countries,id',
            'city_id'=>'required|integer|min:1|exists:cities,id',
            'ids'=>'required|array|min:1'
        ];
    }

    public function attributes()
    {
        return [
            'country_id'    => 'Country',
            'city_id'       => 'City',
            'ids'       => 'Places IDs',
        ];
    }
}
