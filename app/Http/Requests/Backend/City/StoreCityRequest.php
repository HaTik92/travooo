<?php

namespace App\Http\Requests\Backend\City;

use App\Http\Requests\Request;
use App\Models\Access\language\Languages;
/**
 * Class StoreCountryRequest.
 */
class StoreCityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = [];
        $languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
        foreach ($languages as $key => $language) {
            $inputs['translations.'.$language->code.'.title'] = 'required|max:255';
            $inputs['translations.'.$language->code.'.best_time']       = 'max:5000';
            $inputs['translations.'.$language->code.'.geo_stats']       = 'max:5000';
            $inputs['translations.'.$language->code.'.demographics']    = 'max:5000';
        }
        return $inputs;
    }

    public function messages()
    {
        $inputs = [];
        $languages = \DB::table('conf_languages')->where('active', 1)->get();
        foreach ($languages as $key => $language) {
            $inputs['translations.'.$language->code.'.title.required'] = 'A title is required for all languages';
        }
        return $inputs;
    }
}
