<?php

namespace App\Http\Requests\Backend\Destinations;

use App\Http\Requests\Request;

/**
 * Class GenericPlacesRequestv.
 */
class GenericPlacesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = [];

        $inputs['place_id'] = 'required';

        return $inputs;
    }
}
