<?php

namespace App\Http\Requests\Backend\PagesCategories;

use App\Http\Requests\Request;
use App\Models\Access\language\Languages;
/**
 * Class StorePagesCategoriesRequest.
 */
class StorePagesCategoriesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = [];
        $inputs['title'] = 'required';
        return $inputs;
    }
}
