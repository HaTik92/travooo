<?php

namespace App\Http\Requests\Backend\Hotels;

use App\Http\Requests\Request;
use App\Models\Access\language\Languages;

/**
 * Class UpdateHotelsRequest.
 */
class UpdateHotelsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(6);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = [];
        $languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
        foreach ($languages as $key => $language) {
            $inputs['translations.'.$language->code.'.title']           = 'required|max:255';
            $inputs['translations.'.$language->code.'.working_days']    = 'max:5000';
            $inputs['translations.'.$language->code.'.working_times']   = 'max:5000';
            $inputs['translations.'.$language->code.'.how_to_go']       = 'max:5000';
            $inputs['translations.'.$language->code.'.when_to_go']      = 'max:5000';
            $inputs['translations.'.$language->code.'.num_activities']  = 'max:5000';
            $inputs['translations.'.$language->code.'.popularity']      = 'max:5000';
            $inputs['translations.'.$language->code.'.conditions']      = 'max:5000';
            $inputs['place_id']                                         = 'required';
        }
        $inputs['active'] = 'required';
        return $inputs;
    }

    public function messages()
    {
        $inputs = [];
        $languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
        foreach ($languages as $key => $language) {
            $inputs['translations.'.$language->code.'.title']   = 'A title is required for all languages';
            $inputs['place_id.required']                        = 'A place is required';
        }
        return $inputs;
    }
}
