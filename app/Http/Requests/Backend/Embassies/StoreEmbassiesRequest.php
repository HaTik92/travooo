<?php

namespace App\Http\Requests\Backend\Embassies;

use App\Http\Requests\Request;
use App\Models\Access\language\Languages;
/**
 * Class StoreEmbassiesRequest.
 */
class StoreEmbassiesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(5);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = [];
        $languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
        foreach ($languages as $key => $language) {
            $inputs['translations.'.$language->code.'.title']   = 'required|max:255';
            $inputs['translations.'.$language->code.'.working_days']    = 'max:5000';
            $inputs['translations.'.$language->code.'.when_to_go']      = 'max:5000';
            $inputs['translations.'.$language->code.'.history']         = 'max:5000';
        }
        $inputs['translations.en.description']                      = 'required';
        $inputs['translations.en.address']                          = 'required';
        $inputs['translations.en.phone']                            = 'required';
        $inputs['translations.en.working_days']                     = 'required';
        $inputs['translations.en.when_to_go']                       = 'required';
        $inputs['translations.en.price_level']                      = 'required';
        $inputs['translations.en.history']                          = 'required';
        return $inputs;
    }

    public function messages()
    {
        $inputs = [];
        $languages = \DB::table('conf_languages')->where('active', 1)->get();
        foreach ($languages as $key => $language) {
            $inputs['translations.'.$language->code.'.title.required']   = 'Title is required for all languages';
        }
        $inputs['translations.en.description.required']     = 'Quick Facts is required for English language';
        $inputs['translations.en.address.required']         = 'Address is required for English language';
        $inputs['translations.en.phone.required']           = 'Phone is required for English language';
        $inputs['translations.en.working_days.required']    = 'Working Hours is required for English language';
        $inputs['translations.en.when_to_go.required']      = 'Best Time to Visit is required for English language';
        $inputs['translations.en.price_level.required']     = 'Price Level is required for English language';
        $inputs['translations.en.history.required']         = 'Website is required for English language';
        return $inputs;
    }
}
