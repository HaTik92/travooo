<?php

namespace App\Http\Requests\Backend\Restaurants;

use App\Http\Requests\Request;
use App\Models\Access\language\Languages;


/**
 * Class ManageRestaurantsRequest.
 */
class ManageRestaurantsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(7);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
