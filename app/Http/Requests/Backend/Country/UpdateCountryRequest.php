<?php

namespace App\Http\Requests\Backend\Country;

use App\Http\Requests\Request;

/**
 * Class UpdateCountryRequest.
 */
class UpdateCountryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = [];
        $languages = \DB::table('conf_languages')->where('active', 1)->get();
        foreach ($languages as $key => $language) {
            $inputs['translations.'.$language->code.'.title']               = 'required|max:255';
            $inputs['translations.'.$language->code.'.economy']             = 'max:5000';
            $inputs['translations.'.$language->code.'.suitable_for']        = 'max:5000';
            $inputs['translations.'.$language->code.'.metrics']             = 'max:5000';
            $inputs['translations.'.$language->code.'.sockets']             = 'max:5000';
            $inputs['translations.'.$language->code.'.working_days']        = 'max:5000';
            $inputs['translations.'.$language->code.'.best_time']           = 'max:5000';
            $inputs['translations.'.$language->code.'.internet']            = 'max:5000';
            $inputs['translations.'.$language->code.'.other']               = 'max:5000';
//            $inputs['translations.'.$language->code.'.highlights']          = 'max:5000';
            $inputs['translations.'.$language->code.'.health_notes']        = 'max:5000';
//            $inputs['translations.'.$language->code.'.accommodation']       = 'max:5000';
            $inputs['translations.'.$language->code.'.potential_dangers']   = 'max:5000';
//            $inputs['translations.'.$language->code.'.local_poisons']       = 'max:5000';
            $inputs['translations.'.$language->code.'.speed_limit']         = 'max:5000';
            $inputs['translations.'.$language->code.'.etiquette']           = 'max:5000';
            $inputs['translations.'.$language->code.'.pollution_index']     = 'max:5000';
            $inputs['translations.'.$language->code.'.transportation']      = 'max:5000';
            $inputs['translations.'.$language->code.'.planning_tips']       = 'max:5000';

        }
        return $inputs;
    }

    public function messages()
    {
        $inputs = [];
        $languages = \DB::table('conf_languages')->where('active', 1)->get();
        foreach ($languages as $key => $language) {
            $inputs['translations.'.$language->code.'.title'] = 'A title is required for all languages';
        }
        return $inputs;
    }
}
