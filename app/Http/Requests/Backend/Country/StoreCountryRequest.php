<?php

namespace App\Http\Requests\Backend\Country;

use App\Http\Requests\Request;
use App\Models\Access\language\Languages;
/**
 * Class StoreCountryRequest.
 */
class StoreCountryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = [];
        $languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
        foreach ($languages as $key => $language) {
            $inputs['translations.'.$language->code.'.title']           = 'required|max:255';
            $inputs['translations.'.$language->code.'.economy']         = 'max:5000';
//            $inputs['translations.'.$language->code.'.suitable_for']    = 'max:5000';
        }
        return $inputs;
    }
//English description should not be greater than 5000 characters.'

    public function messages()
    {
        $inputs = [];
        $languages = \DB::table('conf_languages')->where('active', 1)->get();
        foreach ($languages as $key => $language) {
            $inputs['translations.'.$language->code.'.title.required'] = 'A title is required for all languages';
        }
        return $inputs;
    }
}
