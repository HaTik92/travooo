<?php

namespace App\Http\Requests\Backend\CopyrightInfringement;

use Illuminate\Foundation\Http\FormRequest;

class CopyrightRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sender_type' => 'required',
            'owner_name' => 'required',
            'sender_name' => 'required',
            'job_title' => 'required',
            'sender_email' => 'required|email',
            'street' => 'required',
            'state' => 'required',
            'postal_code' => 'required',
            'city' => 'required',
            'country' => 'required',
            'description' => 'required',
            'reported_url' => 'required',
            'infringement_description' => 'required',
            "signature" =>  'required'
        ];
    }

    public function messages()
    {
        return [
            'sender_email.required' => 'A your email is required',
            'sender_type.required'  => 'A tell us about yourself is required',
            'owner_name.required'  => 'A owner full name is required',
            'sender_name.required'  => 'A your sender name is required',
            'job_title.required'  => 'A job title is required',
            'street.required'  => 'A street is required',
            'state.required'  => 'A state is required',
            'postal_code.required'  => 'Postal code is required',
            'city.required'  => 'City is required',
            'country.required'  => 'Country is required',
            'description.required'  => 'A description is required',
            'original_link.required'  => 'A Link to the original work is required',
            'reported_url.required'  => 'A reported Trip Plan url is required',
            'infringement_description.required'  => 'A describe infringement is required',
            "signature.required" =>  'A signature is required'
        ];
    }
}
