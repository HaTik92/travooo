<?php

namespace App\Http\Controllers;

use App\Models\Posts\Checkins;
use App\Models\Posts\PostsCheckins;
use App\Services\Ranking\RankingService;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Models\User\UsersMedias;
use App\Models\Posts\PostsMedia;
use App\Models\Posts\PostsLikes;
use App\Models\Posts\PostsShares;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCommentsLikes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\ActivityMedia\MediasComments;
use App\Models\Posts\PostsTags;
use App\Models\Posts\PostsCommentsMedias;
use App\Models\ActivityMedia\MediasLikes;
use App\Models\Posts\SpamsComments;
use App\Models\Posts\SpamsPosts;
use App\Models\Posts\CheckinsLikes;
use App\Models\Posts\CheckinsComments;
use App\Models\Posts\CheckinsCommentsLikes;
use App\Models\Reviews\ReviewsVotes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
use App\Models\TripPlans\TripsComments;
use App\Models\TripPlaces\TripPlacesComments;
use App\Models\Events\EventsComments;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsComments;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Discussion\DiscussionReplies;

use App\Models\Events\Events;
use App\Models\Place\Place;
use App\Models\TripPlans\TripsMediasShares;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlaces\TripPlaces;
use App\Models\User\User;
use App\Models\TripPlans\TripPlans;
use App\Models\Reviews\Reviews;
use App\Models\ActivityMedia\Media;
use App\Models\City\Cities;
use App\Models\Posts\Posts;
use App\Models\Country\Countries;
use App\Models\Discussion\Discussion;
use App\Models\UsersFollowers\UsersFollowers;
use App\Models\PlacesTop\PlacesTop;

class NewsFeedController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Get newsfeed post type view.
     *
     * @param  string    $username
     * @param  string    $username
     * @param  integer   $id
     * @return \Illuminate\View\View
     */
    public function getNewsFeed(Request $request, $username, $id, $islog = 1)
    {

        $prefix = $request->route()->getPrefix();
        $type = str_replace('/','', $prefix);

        $newsfeed_view = '';
        $followers = [];
        $data['ogMeta'] = [
            'url' => request()->url(),
            'type' => 'website',
            'title' => "",
            'description' => "",
            'image' => "",
        ];

        // If link open from mobile device then open Travooo Mobile Application 
//        if (isLiveSite() && isMobileDevice()) {
//            $page_id = base64_encode(request()->url());
//            $url = generateDeepLink(url("api/v1/newsfeed/{$page_id}"));
//            return view('app.index', ['link' => $url]);
//        }

        $user = User::where('username', trim($username))->first();
        if (!in_array(trim(strtolower($type)), ["event"])) {
            if (!$user) {
                return abort(404);
            }
            $followers = UsersFollowers::where('followers_id', $user->id)->where('follow_type', 1)->pluck('users_id')->toArray();
        }

        if (isset($id)) {
            switch ($type) {
                case 'post':
                    $post = Posts::find($id);
                    if (!$post) {
                        return abort(404);
                    }

                    if((isset($post->permission) && $post->permission == Posts::POST_PERMISSION_PRIVATE && $post->users_id != @Auth::user()->id)
                    || (isset($post->permission) && $post->permission == Posts::POST_PERMISSION_FRIENDS_ONLY && $post->users_id != @Auth::user()->id && !in_array( $post->users_id, getUserFriendsIds()))){
                        return redirect('permission-denied');
                    }


                    $media_array = [];
                    if (isset($post->medias)) {
                        foreach ($post->medias as $media) {
                            $media_array[] = $media->medias_id;
                        }
                        $media_array = Media::whereIn('id', $media_array)->get();
                    }

                    $info_title = '';
                    if (isset($post->checkin[0])) {
                        if (strtotime($post->checkin[0]->checkin_time) == strtotime(date('Y-m-d'))) {
                            $info_title = ' is checking-in ' . @$post->checkin[0]->place->transsingle->title . ' ' . diffForHumans($post->checkin[0]->checkin_time);
                        } else if (strtotime($post->checkin[0]->checkin_time) > strtotime(date('Y-m-d'))) {
                            $info_title = ' will check-in to ' . @$post->checkin[0]->place->transsingle->title . ' ' . diffForHumans($post->checkin[0]->checkin_time);
                        } else {
                            $info_title = ' Checked-in to ' . @$post->checkin[0]->place->transsingle->title . ' ' . diffForHumans($post->checkin[0]->checkin_time);
                        }
                    }

                    $data['ogMeta']['title'] = convert_post_text($post);
                    $data['ogMeta']['description'] = convert_post_text($post);
                    $data['ogMeta']['image'] = isset($media_array[0]) ? check_media_url($media_array[0]->url) : null;
                    $newsfeed_view = view('site.newsfeed.partials._feedPostPage', ['post' => $post, 'media_array' => $media_array, 'info_title' => $info_title, 'followers' => $followers]);
                    break;

                case 'review':
                    $review = Reviews::find($id);
                    if (!$review) {
                        return abort(404);
                    }
                    $avg = 0;
                    $total = 0;

                    if (isset($review->place)) {
                        $avg = Reviews::where('places_id', $review->places_id)->avg('score');
                        $total = $review->place->reviews()->count();
                    }

                    $data['ogMeta']['title'] = $review->text;
                    $data['ogMeta']['description'] = $review->text;
                    $data['ogMeta']['image'] = $review->google_profile_photo_url;
                    $newsfeed_view = view('site.newsfeed.partials._reviewPostPage', ['review' => $review, 'avg' => $avg, 'total' => $total]);
                    break;

                case 'discussion':
                    $discussion = Discussion::find($id);
                    if (!$discussion) {
                        return abort(404);
                    }

                    $data['ogMeta']['title'] = $discussion->question;
                    $data['ogMeta']['description'] = $discussion->description;
                    $data['ogMeta']['image'] = isset($discussion->media[0]->media_url) ? $discussion->media[0]->media_url : null;
                    $newsfeed_view = view('site.newsfeed.partials._discussionPostPage', ['discussion' => $discussion, 'followers' => $followers]);
                    break;

                case 'trip-plan':
                    if ($islog) {
                        $log       = ActivityLog::find($id);
                        $trip_plan = TripPlans::find($log->variable);
                    } else {
                        $log       = null;
                        $trip_plan = TripPlans::find($id);
                    }

                    $places                     = [];
                    $countries                  = [];
                    $cities                     = [];
                    $total_destination          = 0;
                    $trip_data                  = [];
                    $trip_data['mapindex']      = rand(1, 5000);
                    $trip_data['intial_latlng'] = [];
                    $trip_data['trip_card_type'] = 'single-city';
                    $contributer                = ($trip_plan) ? $trip_plan->contribution_requests->pluck('users_id')->toArray() : [];
                    $_totalDestination          = ['city_wise' => [], 'country_wise' => []];

                    if (!($trip_plan && $trip_plan->published_places && count($trip_plan->published_places))) {
                        return abort(404);
                    }

                    $trip_data['intial_latlng'] = [$trip_plan->published_places[0]->place->lng, $trip_plan->published_places[0]->place->lat];
                    foreach ($trip_plan->published_places as $tripPlace) {
                        if (!$tripPlace->place or !$tripPlace->country or !$tripPlace->city) {
                            continue;
                        }
                        if (!isset($places[$tripPlace->places_id])) {
                            $total_destination++;
                            $_totalDestination['city_wise'][$tripPlace->cities_id][$tripPlace->places_id] = $tripPlace->place;
                            $_totalDestination['country_wise'][$tripPlace->countries_id][$tripPlace->places_id] = $tripPlace->place;
                            $places[$tripPlace->places_id] = [
                                'type' => 'place',
                                'id' => $tripPlace->places_id,
                                'country_id' => $tripPlace->countries_id,
                                'city_id' => $tripPlace->cities_id,
                                'title' => isset($tripPlace->place->transsingle->title) ? str_replace("'", '', $tripPlace->place->transsingle->title) : null,
                                'lat' => $tripPlace->place->lat,
                                'lng' => $tripPlace->place->lng,
                                'image' => check_place_photo($tripPlace->place),
                            ];
                        }
                        if (!isset($countries[$tripPlace->countries_id])) {
                            $countries[$tripPlace->countries_id] = [
                                'type' => 'country',
                                'id' => $tripPlace->countries_id,
                                'title' => isset($tripPlace->country->trans[0]->title) ? str_replace("'", '', $tripPlace->country->trans[0]->title) : null,
                                'lat' => $tripPlace->country->lat,
                                'lng' => $tripPlace->country->lng,
                                'image' => get_country_flag($tripPlace->country),
                                'total_destination' => 0,
                            ];
                        }
                        if (!isset($cities[$tripPlace->cities_id])) {
                            $cities[$tripPlace->cities_id] = [
                                'type' => 'city',
                                'id' => $tripPlace->cities_id,
                                'country_id' => $tripPlace->countries_id,
                                'title' => isset($tripPlace->city->trans[0]->title) ? str_replace("'", '', $tripPlace->city->trans[0]->title) : null,
                                'lat' => $tripPlace->city->lat,
                                'lng' => $tripPlace->city->lng,
                                'image' => isset($tripPlace->city->medias[0]->media->path) ?
                                    check_city_photo($tripPlace->city->medias[0]->media->path) :
                                    url('assets2/image/placeholders/pattern.png'),
                                'total_destination' => 0,
                            ];
                        }
                    }
                    foreach ($countries as $country_id => &$country) {
                        $country['total_destination'] = isset($_totalDestination['country_wise'][$country_id]) ? count($_totalDestination['country_wise'][$country_id]) : 0;
                    }
                    foreach ($cities as $city_id =>  &$city) {
                        $city['total_destination'] = isset($_totalDestination['city_wise'][$city_id]) ? count($_totalDestination['city_wise'][$city_id]) : 0;
                    }

                    $country_title =  isset(array_values($countries)[0]['title']) ? array_values($countries)[0]['title'] : null;
                    $trip_plan->total_destination = $total_destination;
                    if (count($countries) > 1) {
                        $trip_data['trip_card_type'] = "multiple-country";
                        $trip_data['locationData'] = array_values($countries);
                        $trip_data['country_title'] = null;
                    } else {
                        if (count($cities) > 1) {
                            $trip_data['trip_card_type'] = "multiple-city";
                            $trip_data['country_title'] = $country_title;
                            $trip_data['locationData'] = array_values($cities);
                        } else {
                            $trip_data['trip_card_type'] = "single-city";
                            $trip_data['country_title'] = $country_title;
                            $trip_data['locationData'] = array_values($places);
                        }
                    }
                    // $trip_data['locationData'] = array_slice($trip_data['locationData'], 2, 2);
                    $author = (Auth::check()) ? Auth::user() : $user;


                    if (isset($trip_plan->author->id)) {
                        if ($author->id != $trip_plan->author->id) {
                            if (in_array($author->id, $contributer)) {

                                $author = $trip_plan->author;
                            } else {
                                $following = UsersFollowers::where('followers_id', $author->id)->pluck('users_id')->toArray();

                                $ans = array_values(array_intersect($following, $contributer));

                                if (count($ans) > 0) {
                                    $user_find = User::find($ans[0]);
                                    if (isset($user_find)) {
                                        $author = $user_find;
                                    }
                                } else {
                                    $author = $trip_plan->author;
                                }
                            }
                        }
                    }

                    $trip_data['author'] = $author;
                    $trip_data['trip_plan'] = $trip_plan;
                    $trip_data['followers'] = $followers;
                    if ($islog) {
                        $trip_data['post_action'] = $log->action;
                        $trip_data['log_time'] = $log->time;
                    } else {
                        $trip_data['post_action'] = 'create';
                        $trip_data['log_time'] = $trip_plan->created_at;
                    }
                    $trip_data['places'] = $places;
                    $trip_data['cities'] = $cities;
                    $trip_data['countries'] = $countries;

                    $data['ogMeta']['title'] = $trip_plan->title;
                    $data['ogMeta']['description'] = $trip_plan->description;
                    $data['ogMeta']['image'] = $trip_plan->cover;

                    $newsfeed_view = view('site.newsfeed.partials._tripPostPage', $trip_data);
                    break;

                case 'trip-media':
                    $media_data = [];

                    $media_data['post_type'] = 'TripsMediasShares';
                    $trip_share_media = TripsMediasShares::find($id);

                    if (!$trip_share_media) {
                        return abort(404);
                    }

                    if (is_object($trip_share_media) && isset($trip_share_media->trip_media_id)) {
                        $trip_media_details = TripMedias::find($trip_share_media->trip_media_id);
                        $media_data['media'] = $trip_media_details->media;
                        $media_data['trip'] = $trip_media_details->trip;
                        $media_data['place'] = Place::find($trip_media_details->places_id);
                    }
                    $media_data['trip_plan'] = TripPlaces::find($trip_media_details->trip_place_id);

                    $variable = $media_data['media']->id;
                    $file_url = $media_data['media']->url;
                    $file_url_array = explode(".", $file_url);
                    $media_data['ext'] = end($file_url_array);
                    $media_data['allowed_video'] = array('mp4');

                    $media_data['followers'] = $followers;
                    $media_data['newsfeed_id'] = $id;
                    $media_data['trip_media_details'] = $trip_media_details;

                    $newsfeed_view = view('site.newsfeed.partials._tripMediaPostPage', $media_data);
                    break;

                case 'event':
                    $evt = Events::find($id);
                    if (!$evt) {
                        return abort(404);
                    }
                    $event_data = [];

                    $event_data['post_type'] = 'event';
                    $event_data['rand'] = rand(1, 10);
                    $loc = $evt->place ? $evt->place : ($evt->city ? $evt->city : ($evt->country ? $evt->country : []));
                    $post = $evt;
                    $event_data['dest_type'] = ($loc && get_class($loc) == Place::class) ? 'place' : (($loc && get_class($loc) == Cities::class) ? 'city' : (($loc && get_class($loc) == Countries::class) ? 'country' : ''));

                    $variable = $post->id;
                    $url = json_decode($post->variable);
                    $event_data['url'] = isset($url->booking_info->vendor_object_url) ? $url->booking_info->vendor_object_url : "";

                    $event_data['loc'] = $loc;
                    $event_data['evt'] = $evt;
                    $event_data['followers'] = $followers;
                    $event_data['user'] = $user;

                    $data['ogMeta']['title'] = $evt->title;
                    $data['ogMeta']['description'] = $evt->description;

                    $newsfeed_view = view('site.newsfeed.partials._eventPostPage', $event_data);
                    break;

                case 'external':

                    $internal = false;

                    $post = Posts::find($id);
                    if (!$post) {
                        return abort(404);
                    }

                    $url = strip_tags($post->text);

                    $given_domain = parse_url($url)['host'];
                    $internal_domain = request()->getHttpHost();

                    if ($given_domain == $internal_domain) {
                        $internal = true;
                    }

                    $link_data['post'] = $post;
                    $link_data['url'] = $url;
                    $link_data['internal'] = $internal;
                    $link_data['followers'] = $followers;

                    $data['ogMeta']['title'] = convert_post_text($post);
                    $data['ogMeta']['description'] = convert_post_text($post);

                    $newsfeed_view = view('site.newsfeed.partials._linkPostPage', $link_data);
                    break;

                case 'share':

                    $_post = ActivityLog::find($id);
                    if (!$_post) {
                        return abort(404);
                    }
                    $post_type = $_post->type;
                    $post_action = $_post->action;
                    $variable = $_post->variable;
                    $sharing_user = $_post->users_id;

                    $original_post = PostsShares::find($variable);
                    if (is_object($original_post))
                        $sharing_user = User::find($original_post->users_id);

                    $share_data['post_type'] = $post_type;
                    $share_data['post_action'] = $post_action;
                    $share_data['variable'] = $variable;
                    $share_data['sharing_user'] = $sharing_user;
                    $share_data['original_post'] = $original_post;
                    $share_data['followers'] = $followers;
                    $share_data['auth_user'] = $user;

                    $data['ogMeta']['title'] = $original_post->text;
                    $data['ogMeta']['description'] = $original_post->text;

                    $newsfeed_view = view('site.newsfeed.partials._shareablePost', $share_data);
                    break;
            }
        }

        $data['view'] = $newsfeed_view;
        $data['top_places'] = $this->getTopPlace($user);

        return view('site.newsfeed.index', $data);
    }

    /* helpers */
    private function getTopPlace($user)
    {
        /* Get top places */
        $data = [];
        $tp_array = [];
        $ip = getClientIP();
        $geoLoc = ip_details($ip);

        $user_country = (!$user) ?: Countries::find($user->nationality);
        $details['country']  = $geoLoc['iso_code'];
        $countries_suggested = '';
        $same_country_flag = false;
        if ($user && isset($details['country']) && isset($user_country->iso_code) && $details['country'] == $user_country->iso_code) {
            $tp_array = getTopPlacesByUserPref(false, $user->nationality);
            $same_country_flag = true;
        } else {
            if ($user)
                if (isset($details['country'])) {
                    $countries_suggested = Countries::where('iso_code', $details['country'])->first();
                    $tp_array = getTopPlacesByUserPref(['country' => $countries_suggested->id], $user->nationality);
                } else {
                    $tp_array = getTopPlacesByUserPref(false, $user->nationality);
                }
        }


        $follows_places = [];
        if ($user) {
            $p = $user->id;
            $follows_places = DB::select('SELECT * FROM `places_followers` WHERE users_id   =' . $p);
        }
        $follow_list = [];
        foreach ($follows_places as $placex) {
            $follow_list[$placex->places_id] = $placex->places_id;
        }
        if (!empty($follow_list))
            $tp_array = array_diff($tp_array, array_values($follow_list));

        if ($same_country_flag) {
            $data['top_places'] = PlacesTop::whereIn('places_id', $tp_array)
                ->whereHas('place', function ($query) {
                    $query->whereHas('medias');
                })
                ->inRandomOrder()
                ->limit(6)
                ->get();
        } else {
            $data['top_places'] = PlacesTop::whereIn('places_id', $tp_array)
                ->whereHas('place', function ($query) {
                    $query->whereHas('medias');
                })
                ->where('country_id', @$countries_suggested->id)
                ->inRandomOrder()
                ->limit(6)
                ->get();
        }
        if (!empty($follow_list))
            $tp_array = array_merge($tp_array, $follow_list);
        if (count($data['top_places']) < 6) {
            if ($same_country_flag) {
                $remaining = 6 - count($data['top_places']);
                $count_remaing = PlacesTop::whereNotIn('places_id', $tp_array)
                    ->whereHas('place', function ($query) {
                        $query->whereHas('medias');
                    })
                    ->whereNotIn('country_id', [373, 326])
                    ->whereNotIn('city_id', [2031])
                    ->inRandomOrder()
                    ->limit($remaining)
                    ->get();
            } else {
                $remaining = 6 - count($data['top_places']);
                $count_remaing = PlacesTop::whereNotIn('places_id', $tp_array)
                    ->whereHas('place', function ($query) {
                        $query->whereHas('medias');
                    })
                    ->whereNotIn('country_id', [373, 326])
                    ->whereNotIn('city_id', [2031])
                    ->where('country_id', @$countries_suggested->id)
                    ->inRandomOrder()
                    ->limit($remaining)
                    ->get();
            }
            $data['top_places'] = $data['top_places']->merge($count_remaing);
        }
        return $data['top_places'];
    }
}
