<?php

namespace App\Http\Controllers;

use App\Models\Place\PlaceReviews;
use App\Services\Ranking\RankingService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Access\language\Languages;
use League\Fractal\Resource\Item;
use App\Transformers\Country\CountryTransformer;
use Illuminate\Support\Facades\DB;
use App\Models\Country\ApiCountry as Country;
use App\Models\Country\CountriesFollowers;
use Illuminate\Support\Facades\Auth;
use App\Models\ActivityLog\ActivityLog;
use App\CountriesContributions;
use Illuminate\Support\Facades\Input;
use App\Models\Place\Place;
use App\Models\Place\PlaceFollowers;
use App\Models\Place\PlacesShares;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Reviews\Reviews;
use App\Models\Reviews\ReviewsVotes;
use App\Models\Reviews\ReviewsShares;
use App\Models\Posts\Checkins;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Models\User\User;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsInfos;
use App\Models\TripPlans\TripPlans;
use App\Models\Posts\PostsPlaces;
use App\Models\Posts\PostsCities;
use App\Models\Posts\PostsCountries;
use App\Models\Posts\Posts;
use App\Models\TripPlaces\TripPlaces;
use App\Models\Discussion\Discussion;
use App\Models\ActivityMedia\Media;
use App\Models\User\UsersMedias;
use App\Models\Posts\PostsMedia;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCommentsMedias;
use App\Models\Place\PlaceMedias;
use App\Models\City\Cities;
use App\Models\City\CitiesTranslations;
use App\Models\Country\Countries;
use App\Models\Country\CountriesTranslations;
use App\Models\States\States;
use App\Models\States\StateTranslation;
use Illuminate\Support\Facades\Storage;
use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Discussion\DiscussionReplies;
use Illuminate\Support\Facades\View;
use App\Models\Events\Events;
use App\Models\Events\EventsComments;
use App\Models\Events\EventsCommentsMedias;
use App\Models\Events\EventsCommentsLikes;
use App\Models\Events\EventsLikes;
use App\Models\Events\EventsShares;
use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\LanguagesSpoken\LanguagesSpokenTranslation;
use App\Models\Posts\PostsCheckins;
use App\Models\Reviews\ReviewsMedia;
use App\Models\UsersFollowers\UsersFollowers;
use Google\Cloud\Translate\V2\TranslateClient;


class PlaceController2 extends Controller
{
    protected  $placeTypesArray = [
        'point_of_interest',
        'airport',
        'amusement_park',
        'aquarium',
        'art_gallery',
        'bakery',
        'bar',
        'cafe',
        'campground',
        'casino',
        'cemetery',
        'church',
        'city_hall',
        'embassy',
        'hindu_temple',
        'library',
        'light_rail_station',
        'lodging',
        'meal_delivery',
        'meal_takeaway',
        'mosque',
        'movie_theater',
        'museum',
        'night_club',
        'park',
        'restaurant',
        'shopping_mall',
        'stadium',
        'subway_station',
        'synagogue',
        'tourist_attraction',
        'train_station',
        'transit_station',
        'zoo',
        'food',
        'place_of_worship',
        'natural_feature'

    ];

    public function __construct()
    {
        //        $this->middleware('auth:user');
    }

    public function _create_thumbs($media)
    {

        $complete_url = $media->url;
        $url = explode("/", $complete_url);

        $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

        $options = [
            'region' => 'us-east-1',
            'version' => 'latest',
            'http' => ['verify' => false],
            'credentials' => $credentials,
            'endpoint' => 'https://s3.amazonaws.com'
        ];

        $s3Client = new S3Client($options);
        //$s3 = AWS::createClient('s3');
        $result = $s3Client->getObject([
            'Bucket' => 'travooo-images2', // REQUIRED
            'Key' => $complete_url, // REQUIRED
            'ResponseContentType' => 'text/plain',
        ]);

        $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        //return $img_700->response('jpg');
        //echo $complete_url . '<br />';
        //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

        $put_1100 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_1100->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_700 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_700->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_230 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_230->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_180 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_180->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);


        $media->thumbs_done = 1;
        $media->save();
    }

    private function _getRandom($key)
    {

        $init_array = [];
        for ($i = 1; $i < 32; $i++)
            $init_array[] = $i;

        $return = [];

        foreach ($init_array as $val) {
            $return[] = $val * $key % 32;
        }

        return implode(',', $return);
    }

    public function getIndex($place_id)
    {

        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->getIndex($place_id);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->getIndex($place_id);

        $is_new =  Input::get('is_new');
        $place_addr = Input::get('address');
        if (isset($is_new) && $is_new == 1) {
            $tempPlaceID =  $place_id;
            if (!Place::where('provider_id', '=', $place_id)->exists()) {
                $place_id =   $this->addNewPlaceByProviderId($place_id, $place_addr);
            } else {
                $place_id = Place::where('provider_id', '=', $place_id)->get()->first()->id;
            }
        }
        $ip = getClientIP();
        $geoLoc = ip_details($ip);
        $getLocCity = $geoLoc['city'];
        $data['myCity'] = $getLocCity;
        $language_id = 1;
        $place = Place::find($place_id);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }
        $place->visits = $place->visits + 1;
        $place->save();
        $random_val = $this->_getRandom(date('j'));


        $place = Place::with([
            'trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'getMedias',
            'getMedias.users',
            'followers'
        ])
            ->where('id', $place_id)
            ->where('active', 1)
            ->first();

        // $top_places = PlacesTop::where('country_id', $place->country->id)
        //         ->orderBy('reviews_num', 'desc')
        //         ->take(20)
        //         ->get();


        if (!$place) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }
        //secondary
        $scondary_post_collection = ['new_travellers','trending_places','video_you_might_like','recommended_places'];
        shuffle($scondary_post_collection);
        session()->forget(['secondary_post_style']);
        $value1 = session(['secondary_post_style' =>  $scondary_post_collection]);

        $db_place_reviews = Reviews::where('places_id', $place->id)->orderBy('created_at', 'desc')->get();
        if (!count($db_place_reviews)) {
            $place_reviews = get_google_reviews($place->provider_id);
            if (is_array($place_reviews)) {
                foreach ($place_reviews as $review) {
                    $r = new Reviews;
                    $r->places_id = $place->id;
                    $r->google_author_name = @$review->author_name;
                    $r->google_author_url = @$review->author_url;
                    $r->google_language = @$review->language;
                    $r->google_profile_photo_url = @$review->profile_photo_url;
                    $r->score = @$review->rating;
                    $r->google_relative_time_description = @$review->relative_time_description;
                    $r->text = @$review->text;
                    $r->google_time = @$review->time;
                    $r->save();
                }
            }
        }

        $db_place_reviews = Reviews::where('places_id', $place->id)->orderBy('created_at', 'desc')->get();
        $data['reviews'] = $db_place_reviews;
        $data['reviews_places'] = Reviews::where('places_id', $place->id)->get();

        $data['reviews_avg'] = Reviews::where('places_id', $place->id)->avg('score');
        // get places nearby
        // $data['places_nearby'] = Place::with('medias')
        //         ->whereHas("medias", function ($query) {
        //             $query->where("medias_id", ">", 0);
        //         })
        //         ->where('cities_id', $place->cities_id)
        //         ->where('id', '!=', $place->id)
        //         ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
        //         ->having('distance', '<', 15)
        //         ->orderBy('distance')
        //         ->take(10)
        //         ->get();
        // $checkins = Checkins::whereHas("place", function ($query) use ($place) {
        //             $query->where('cities_id', $place->cities_id);
        //             $query->where('id', '!=', $place->id);
        //         })
        //         ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) * cos( radians( SUBSTRING(lat_lng, INSTR(lat_lng, \',\') + 1) ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) ) ) AS distance'))
        //         // ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
        //         ->having('distance', '<', 15)
        //         ->orderBy('distance')
        //         ->take(10)
        //         ->get();
        // $result = array();
        // foreach ($checkins AS $checkin) {
        //     $result[] = array(
        //         // 'post_id' => $checkin->post_checkin->post->id,
        //         'lat' => $checkin->place->lat,
        //         'lng' => $checkin->place->lng,
        //         // 'photo' => @$checkin->post_checkin->post->medias[0]->media->url,
        //         'name' => @$checkin->user->name,
        //         'id' => @$checkin->user->id,
        //         'title' => $checkin->location,
        //         'profile_picture' => check_profile_picture(@$checkin->user->profile_picture),
        //         'date' => diffForHumans($checkin->created_at)
        //     );
        // }
        // $data['places_nearby'] = $result;
        // $checkins = Checkins::where('place_id', $place->id)->orderBy('id', 'DESC')->get();
        // $result = array();
        // $done = array();
        // foreach ($checkins AS $checkin) {
        //     if (!isset($done[$checkin->users_id])) {
        //         $result[] = $checkin;
        //         $done[$checkin->users_id] = true;
        //     }
        // }
        // $data['checkins'] = $result;


        $data['place'] = $place;
        // $data['top_places'] = $top_places;

        $city_name = @$place->city->transsingle->title;
        $data['city_code'] = 0;
        $res = @file_get_contents('https://srv.wego.com/places/search?query=' . $city_name);
        $results = json_decode($res);
        $cities = array();
        $i = 0;
        if (isset($results[0]) and is_object($results[0]) and $results[0]->type == "city") {
            $data['city_code'] = $results[0]->code;
        }

        $list_trip_plan = TripPlaces::where('places_id', $place->id)->pluck('trips_id');

        if (Auth::check()) {
            $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)
                ->limit(10)
                ->get();

            $data['me'] = Auth::guard('user')->user();
        }

        $data['travelmates'] = TravelMatesRequests::whereHas('plan_place', function ($q) use ($place) {
            $q->where('places_id', '=', $place->id);
        })
            ->groupBy('users_id')
            ->get();
        if (count($data['travelmates']) == 0) {
            $data['travelmates'] = TravelMatesRequests::whereHas('plan_city', function ($q) use ($place) {
                $q->where('cities_id', '=', @$place->city->id);
            })
                ->groupBy('users_id')
                ->get();
        }

        $data['reportsinfo'] = ReportsInfos::where('var', '=', 'place')
            ->where('val', '=', $place->id)
            ->whereHas('report', function ($q) {
                $q->where('flag', '=', 1);
                $q->whereNotNull('published_date');
            })
            ->get();
        if (count($data['reportsinfo']) == 0) {
            $data['reports'] = Reports::where('cities_id', '=', @$place->city->id)->where('flag', '=', 1)
                ->get();
        }



        // start posts collection
        //$data['pposts'] = collect_posts('place', $place->id);
        $data['pposts'] = '';

        $data['place_discussions'] = Discussion::where('destination_type', 'Place')
            ->where('destination_id', $place->id)
            ->get();


        // get experts into categories
        $experts_all = $experts_top = $experts_local = $experts_friends = $live_checkins = array();
        if (isset($place->city->experts)) {
            foreach ($place->city->experts as $key => $pce) {
                $experts_all[] = $pce;
                if (Auth::check() && is_object($pce->user) && is_object($data['me'])) {
                    if ($pce->user->nationality != $data['me']->nationality) {
                        $experts_top[] = $pce;
                    } elseif ($pce->user->nationality == $data['me']->nationality) {
                        $experts_local[] = $pce;
                    }
                }
                if (is_object($pce->user)) {
                    if (is_friend($pce->user->id)) {
                        $experts_friends[] = $pce;
                    }
                }
            }
        }
        $data['experts_top'] = $experts_top;
        $data['experts_local'] = $experts_local;
        $data['experts_friends'] = $experts_friends;
        $data['all_expert'] = $experts_all;
        $data['live_checkins'] = $place->checkins()->groupBy('users_id')->whereDate('checkin_time', '=', Carbon::today()->toDateString())->get();

        $experts_ids = array();
        foreach ($experts_all as $ea) {
            $experts_ids[] = $ea->users_id;
        }

        // Select *,count(*) as r, sum(A.replies) from
        // (SELECT created_at, users_id, count(*) as replies FROM `discussion_replies` GROUP BY YEAR(created_at), Month(created_at), Day(created_at))
        // as A GROUP BY A.users_id ORDER BY `users_id`
        $data['place_top_contributors'] = [];
        if (count($experts_ids) > 0) {
            $last_month = date("Y-m-d", strtotime('-180 days'));

            $sub = DiscussionReplies::selectRaw('*, count(*) as reps, sum(num_views) as views')
                ->whereRaw('users_id in (' . implode(',', $experts_ids) . ')')
                ->whereRaw('created_at >' . $last_month)
                ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'));

            $data['place_top_contributors'] = DiscussionReplies::from(DB::raw('(' . $sub->toSql() . ') as a'))
                ->selectRaw('*, count(*) as count, sum(a.reps) as reply_cnt, sum(a.views) as views')
                ->groupBy('a.users_id')
                ->orderBy('count', 'desc')
                ->take(20)
                ->get();
        }

        $follow_list = [];

        if (Auth::check()) {
            $tp_array = getTopPlacesByUserPref(['city' => $place->cities_id]);
            $p = Auth::user()->id;
            $follows_places = DB::select('SELECT * FROM `places_followers` WHERE users_id   =' . $p);

            foreach ($follows_places as $placex) {
                $follow_list[$placex->places_id] = $placex->places_id;
            }
        } else {
            $current_user_loc_info = ip_details(getClientIP());
            $current_country = Countries::where('iso_code', $current_user_loc_info['iso_code'])->first();

            $tp_array = getTopPlacesByUserPref(['city' => $place->cities_id], $current_country->id);

            $data['current_country_id'] = $current_country->id;
        }


        $tp_array = array_diff($tp_array, array_values($follow_list));
        $tp_array1 = array_diff($tp_array, [$place->id]);
        $tp_array = array_merge($tp_array1, $follow_list);
        $data['top_places'] = PlacesTop::whereIn('places_id', array_values($tp_array1))
            ->whereHas('place', function ($query) {
                $query->whereHas('medias');
            })
            ->whereNotIn('country_id', [373, 326])
            ->inRandomOrder()
            ->limit(6)
            ->get();
        $tp_array[] = [$place->id];
        if (count($data['top_places']) < 6) {
            $remaining = 6 - count($data['top_places']);
            $count_remaing = PlacesTop::whereNotIn('places_id', $tp_array)
                ->whereHas('place', function ($query) {
                    $query->whereHas('medias');
                })->where('city_id', $place->cities_id)
                ->whereNotIn('country_id', [373, 326])
                ->inRandomOrder()
                ->limit($remaining)
                ->get();

            $data['top_places'] = $data['top_places']->merge($count_remaing);
        }


        $data['trip_plans_collection'] = TripPlans::query()
            ->whereHas('trip_places_by_active_version', function ($q) use ($place) {
                $q->where('places_id', $place->id);
            })
            ->with('trip_places_by_active_version')
            ->orderByDesc('id')
            ->get();

        if (count($data['trip_plans_collection']) == 0) {
            $data['trip_plans_collection'] = TripPlans::query()
                ->whereHas('trip_places_by_active_version', function ($q) use ($place) {
                    $q->where('cities_id', $place->cities_id);
                })
                ->with('trip_places_by_active_version')
                ->orderByDesc('id')
                ->get();
        }

        if (Auth::check()) {
            $data['i_was_here_flag'] =  $place->checkins()->where('users_id', Auth::user()->id)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->count();
            if (request()->has('ref') and intval(request()->get('ref'))) {
                $data['referrer_is_there'] = intval(request()->get('ref'));
            }
        }

        $live_users = [];
        foreach ($data['live_checkins'] as $live_here) {
            $live_users[] = $live_here->users_id;
        }
        $temp_live_users =  User::whereIn('id', $live_users)->get();
        $live_users = [];
        foreach ($temp_live_users as $user) {
            $is_followed =  app('App\Http\Controllers\ProfileController')->postCheckFollow($user->id);
            $live_users[$user->id] = ['users' => $user, 'is_followed' => $is_followed['success']];
        }
        $data['live_users'] = $live_users;
        $data['place_type'] = 'place';
        return view('site.place2.index', $data);
    }

    public function postFollow($placeId)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->postFollow($placeId);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->postFollow($placeId);

        $userId = Auth::guard('user')->user()->id;
        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }


        $follower = PlaceFollowers::where('places_id', $placeId)
            ->where('users_id', $userId)
            ->first();

        if ($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that Place',
                ],
                'success' => false
            ];
        }

        $place->followers()->create([
            'users_id' => $userId
        ]);

        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('Place', 'follow', $placeId);

        $count = PlaceFollowers::where('places_id', $placeId)->count();

        return [
            'success' => true,
            'count' => $count
        ];
    }

    public function postUnFollow($placeId)
    {

        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->postUnFollow($placeId);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->postUnFollow($placeId);

        $userId = Auth::guard('user')->user()->id;

        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $follower = PlaceFollowers::where('places_id', $placeId)
            ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this Place.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('Place', 'unfollow', $placeId);
        }

        $count = PlaceFollowers::where('places_id', $placeId)->count();

        return [
            'success' => true,
            'count' => $count
        ];
    }

    public function postCheckFollow($placeId)
    {

        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->postCheckFollow($placeId);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->postCheckFollow($placeId);

        $userId = Auth::guard('user')->user()->id;
        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $follower = PlaceFollowers::where('places_id', $placeId)
            ->where('users_id', $userId)
            ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    public function postCheckin($placeId)
    {

        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->postCheckin($placeId);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->postCheckin($placeId);

        $userId = Auth::guard('user')->user()->id;
        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }


        $check = new Checkins();
        $postCheckedIn =     Input::get('checkin_time');
        if (isset($postCheckedIn)) {
            $check->checkin_time = date("Y-m-d", strtotime(Input::get('checkin_time')));
        }
        $check->users_id = $userId;
        $check->place_id = $placeId;
        $check->location = $place->transsingle->title ?? 'unnamed';
        $check->lat_lng = $place->lat . ',' . $place->lng;
        $check->save();




        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('Place', 'checkin', $placeId);

        if (strtotime($check->checkin_time) == strtotime(date('Y-m-d'))) {
            return [
                'success' => true,
                'live' => 1,
                'checkin_id' => $check->id,
                'message' => 'Thank you for checking in! You are live on this page now.'
            ];
        } else if (strtotime($check->checkin_time) < strtotime(date('Y-m-d'))) {
            return [
                'success' => true,
                'live' => 0,
                'checkin_id' => $check->id,
                'message' => 'Thank you for recording your past check-in!'
            ];
        } else if (strtotime($check->checkin_time) > strtotime(date('Y-m-d'))) {
            return [
                'success' => true,
                'live' => 0,
                'checkin_id' => $check->id,
                'message' => 'Thank you for checking in! You will appear live on this page on your check-in date.'
            ];
        }
    }

    public function postCheckout($placeId)
    {

        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->postCheckout($placeId);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->postCheckout($placeId);

        $userId = Auth::guard('user')->user()->id;

        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $follower = PlaceFollowers::where('places_id', $placeId)
            ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this Place.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('Place', 'checkout', $placeId);
        }

        return [
            'success' => true
        ];
    }

    public function postCheckCheckin($placeId)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->postCheckCheckin($placeId);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->postCheckCheckin($placeId);

        $userId = Auth::guard('user')->user()->id;
        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $checkin = Checkins::where('place_id', $placeId)
            ->where('users_id', $userId)
            ->first();

        return empty($checkin) ? ['success' => false] : ['success' => true];
    }

    public function postTalkingAbout($placeId)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->postTalkingAbout($placeId);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->postTalkingAbout($placeId);

        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $shares = $place->shares;

        foreach ($shares as $share) {
            $data['shares'][] = array(
                'user_id' => $share->user->id,
                'user_profile_picture' => check_profile_picture($share->user->profile_picture)
            );
        }

        $data['num_shares'] = count($shares);
        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function postNowInPlace($placeId)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->postNowInPlace($placeId);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->postNowInPlace($placeId);

        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $checkins = Checkins::where('place_id', $place->id)
            ->orderBy('id', 'DESC')
            ->take(5)
            ->get();

        $result = array();
        $done = array();
        foreach ($checkins as $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                if (time() - strtotime($checkin->post_checkin->post->date) < (24 * 60 * 60)) {
                    $live = 1;
                } else {
                    $live = 0;
                }
                $result[] = array(
                    'name' => $checkin->post_checkin->post->author->name,
                    'id' => $checkin->post_checkin->post->author->id,
                    'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                    'live' => $live
                );
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['live_checkins'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function ajaxPostReview($placeId, RankingService $rankingService)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->ajaxPostReview($placeId);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->ajaxPostReview($placeId);

        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        if (Input::has('text')) {
            $review_text = Input::get('text');
            $review_score = Input::get('score');
            $review_author = Auth::guard('user')->user();
            $review_place_id = $place->id;

            $review = new Reviews;
            $review->places_id = $review_place_id;
            $review->by_users_id = $review_author->id;
            $review->score = $review_score * 1;
            $review->text = $review_text;

            if ($review->save()) {
                $rankingService->addPointsEarners($review->id, get_class($review), $review->by_users_id);

                if (Input::has('mediafiles')) {
                    foreach (Input::get('mediafiles') as $media) {
                        if ($media) {
                            $userId = Auth::guard('user')->user()->id;
                            $userName = Auth::guard('user')->user()->name;
                            $media_url = $media;

                            $media_model = new Media();
                            $media_model->url = $media_url;
                            $media_model->author_name = $userName;
                            $media_model->title = Input::get('text');
                            $media_model->author_url = '';
                            $media_model->source_url = '';
                            $media_model->license_name = '';
                            $media_model->license_url = '';
                            $media_model->uploaded_at = date('Y-m-d H:i:s');
                            $media_model->save();

                            $users_medias = new UsersMedias();
                            $users_medias->users_id = $userId;
                            $users_medias->medias_id = $media_model->id;
                            $users_medias->save();

                            $posts_medias = new ReviewsMedia();
                            $posts_medias->reviews_id = $review->id;
                            $posts_medias->medias_id = $media_model->id;
                            $posts_medias->save();

                            $places_medias = new PlaceMedias();
                            $places_medias->places_id = $place->id;
                            $places_medias->medias_id = $media_model->id;
                            $places_medias->save();

                            $this->_create_thumbs($media_model);
                        }
                    }
                }
                log_user_activity('Place', 'review', $review->id);
                $data['success'] = true;
                $data['id'] = $review->id;
                $data['posted_review'] = View::make('site.place2.new.review', array('post' => ['type' => 'Review', 'variable' => $review->id]));

                // $data['posted_review'] =View::make('site.place2.partials.post_review', ['place' => $place,'review' => $review])->render();
                $data['prepend'] = '<div class="comment review_box_div displayed" id="' . $review->id . '" topsort="' . $review->score . '" newsort="' . strtotime($review->created_at) . '">
                            <img class="comment__avatar" src="' . check_profile_picture(Auth::guard('user')->user()->profile_picture) . '" alt="" style="width:45px;height:45px;">
                            <div class="comment__content" style="width: calc(100% - 44px)">
                                <div class="comment__header"><a class="comment__username" href="#">' . Auth::guard('user')->user()->name . '</a>
                                    <div class="comment__user-id"></div>
                                </div>
                                <div class="comment__text" style="overflow-wrap: break-word;margin-top:10px">' . $review_text . '</div>
                                <div class="comment__footer">
                                    <div class="rating">
                                    <svg class="icon icon--star ' . ($review->score >= 1 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 2 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 3 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 4 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 5 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <div class="rating__value"><strong>' . $review->score . '</strong> / 5</div>
                                    </div>
                                    <div class="comment__posted">' . diffForHumans($review->created_at) . '</div>
                                </div>
                                <div class="mt-2 updownvote" id="updownvote">
                                    <a href="#" class="upvote-link review-vote up disabled" id="' . $review->id . '">
                                      <span class="arrow-icon-wrap"><i class="trav-angle-up"></i></span>
                                    </a>
                                    <span class="upvote-' . $review->id . '"><b>' . $review->updownvotes()->where('vote_type', 1)->count() . '</b> Upvotes</span>
                                    &nbsp;&nbsp;
                                    <a href="#" class="upvote-link review-vote down disabled" id="' . $review->id . '">
                                      <span class="arrow-icon-wrap"><i class="trav-angle-down"></i></span>
                                    </a>
                                    <span class="downvote-' . $review->id . '"><b>' . $review->updownvotes()->where('vote_type', 2)->count() . '</b> Downvotes</span>
                                    
                                    <a href="#" class="review-action review-delete" onclick="post_delete(' . $review->id . ',\'Review\', this, event)" review-id="' . $review->id . '">delete</a>
                                </div>                                
                            </div>
                        </div>';
            } else {
                $data['success'] = false;
            }
        } else {
            $data['success'] = false;
        }
        return json_encode($data);
    }

    public function ajaxHappeningToday($placeId)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->ajaxHappeningToday($placeId);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->ajaxHappeningToday($placeId);

        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $checkins = Checkins::whereHas("place", function ($query) use ($place) {
            $query->where('cities_id', $place->cities_id);
            $query->where('id', '!=', $place->id);
        })
            ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) * cos( radians( SUBSTRING(lat_lng, INSTR(lat_lng, \',\') + 1) ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) ) ) AS distance'))
            // ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
            ->having('distance', '<', 15)
            ->orderBy('distance')
            ->take(10)
            ->get();

        //$checkins = Checkins::where('place_id', $place->id)->orderBy('id', 'DESC')->take(5)->get();

        $result = array();
        foreach ($checkins as $checkin) {
            $result[] = array(
                // 'post_id' => $checkin->post_checkin->post->id,
                'lat' => $checkin->place->lat,
                'lng' => $checkin->place->lng,
                // 'photo' => @$checkin->post_checkin->post->medias[0]->media->url,
                'name' => @$checkin->user->name,
                'id' => @$checkin->user->id,
                'title' => $checkin->location,
                'profile_picture' => check_profile_picture(@$checkin->user->profile_picture),
                'date' => diffForHumans($checkin->created_at)
            );
        }
        $data['happenings'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function ajaxPostMorePhoto($placeId, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->ajaxPostMorePhoto($placeId, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->ajaxPostMorePhoto($placeId, $request);

        $language_id = 1;
        $num = $request->pagenum;
        $skip = ($num) * 6;
        $place = Place::find($placeId);

        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $medias = $place->getMedias;

        if (!$medias) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }
        $data = array();
        $c = (count($medias) <= ($skip + 6)) ? count($medias) : $skip + 6;
        for ($i = $skip; $i < $c; $i++) {
            $data[] = '<img class="gallery__img" src="https://s3.amazonaws.com/travooo-images2/th230/' . @$medias[$i]->url . '" alt="#" title="" /> ';
        }
        return [
            'data' => $data,
            'success' => true
        ];
    }

    public function postAddPost($place_id, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->postAddPost($place_id, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->postAddPost($place_id, $request);

        $post_data = array();
        foreach ($request->data as $data) {
            if (strpos($data["name"], "[]")) {
                if (!isset($post_data[$data["name"]]))
                    $post_data[$data["name"]] = [];

                $post_data[$data["name"]][] = $data["value"];
            } else {
                $post_data[$data["name"]] = $data["value"];
            }
        }

        $userId = Auth::guard('user')->user()->id;
        $userName = Auth::guard('user')->user()->name;

        $place = Place::find($place_id);

        if (!$place) {
            echo "error";
        }

        $post = new Posts;
        $post->users_id = $userId;
        $post->text = $post_data["text"];
        $post->permission = $post_data["permission"];
        $post->save();

        log_user_activity('Place_Post', 'post', $post->id);

        //create post checkin
        $checkins = '';
        if (isset($post_data['checkinPost'])) {
            $postCheckin =  PostsCheckins::create(['checkins_id' => $post_data['checkinPost'], 'posts_id' => $post->id]);
            $checkins  = Checkins::find($postCheckin->checkins_id);
        }


        if ($post_data["type"] == "text") {

            $postplace = new PostsPlaces;
            $postplace->posts_id = $post->id;
            $postplace->places_id = $place_id;
            $postplace->save();
            //make country post
            $postplacecountry = new PostsCountries;
            $postplacecountry->posts_id = $post->id;
            $postplacecountry->places_id = $place_id;
            $postplacecountry->countries_id = $place->countries_id;
            $postplacecountry->save();

            //make city post aswell
            $postplacecity = new PostsCities;
            $postplacecity->posts_id = $post->id;
            $postplacecity->places_id = $place_id;
            $postplacecity->cities_id = $place->cities_id;
            $postplacecity->save();
            //end post creation
            echo view('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $post->id]));

            // echo view('site.place2.partials.post_text', array("place" => $place, "post" => $post,'checkins'=>$checkins));
        } else if ($post_data["type"] == "image" || $post_data["type"] == "video") {
            $postplace = new PostsPlaces;
            $postplace->posts_id = $post->id;
            $postplace->places_id = $place_id;
            $postplace->save();
            //make country post
            $postplacecountry = new PostsCountries;
            $postplacecountry->posts_id = $post->id;
            $postplacecountry->places_id = $place_id;
            $postplacecountry->countries_id = $place->countries_id;
            $postplacecountry->save();

            //make city post aswell
            $postplacecity = new PostsCities;
            $postplacecity->posts_id = $post->id;
            $postplacecity->places_id = $place_id;
            $postplacecity->cities_id = $place->cities_id;
            $postplacecity->save();
            //end post creation

            foreach ($post_data["mediafiles[]"] as $media) {
                if ($media) {
                    // list($baseType, $media) = explode(';', $media);
                    // list(, $media) = explode(',', $media);
                    // $media = base64_decode($media);

                    // if (strpos($baseType, 'image') !== false)
                    //     $filename = sha1(microtime()) . "_place_added_file.png";
                    // else if (strpos($baseType, 'video') !== false)
                    //     $filename = sha1(microtime()) . "_place_added_file.mp4";

                    // Storage::disk('s3')->put(
                    //         $userId . '/medias/' . $filename, $media, 'public'
                    // );
                    $media_url = $media;

                    $media_model = new Media();
                    $media_model->url = $media_url;
                    $media_model->author_name = $userName;
                    $media_model->title = $post_data["text"];
                    $media_model->author_url = '';
                    $media_model->source_url = '';
                    $media_model->license_name = '';
                    $media_model->license_url = '';
                    $media_model->uploaded_at = date('Y-m-d H:i:s');
                    $media_model->save();

                    $users_medias = new UsersMedias();
                    $users_medias->users_id = $userId;
                    $users_medias->medias_id = $media_model->id;
                    $users_medias->save();

                    $posts_medias = new PostsMedia();
                    $posts_medias->posts_id = $post->id;
                    $posts_medias->medias_id = $media_model->id;
                    $posts_medias->save();

                    $places_medias = new PlaceMedias();
                    $places_medias->places_id = $place_id;
                    $places_medias->medias_id = $media_model->id;
                    $places_medias->save();

                    $this->_create_thumbs($media_model);
                }
            }


            echo view('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $post->id]));
            // if ($post_data["text"] && count($post_data["mediafiles[]"]) > 0) {
            //     echo view('site.place2.partials.post_media_with_text', array("post" => $post,'checkins'=>$checkins));
            // } else if (!$post_data["text"] && count($post_data["mediafiles[]"]) > 0) {
            //     echo view('site.place2.partials.post_media_without_text', array("post" => $post,'checkins'=>$checkins));
            // } else if ($post_data['text'] && count($post_data["mediafiles[]"]) == 0) {
            //     echo view('site.place2.partials.post_text', array("place" => $place, "post" => $post,'checkins'=>$checkins));
            // }
        }
    }

    public function shareReview(Request $request)
    {

        $user_id = Auth::guard('user')->user()->id;
        $id = $request->id;
        $review = ReviewsShares::where('users_id', $user_id)->where('review_id', $id)->get()->first();

        if (is_object($review)) {
            return ['msg' => 'You had already shared this place.'];
        } else {
            $share = new ReviewsShares;
            $share->users_id = $user_id;
            $share->review_id = $id;

            log_user_activity('Review', 'share', $id);
            if ($share->save())
                return ['msg' => 'You have just shared this post.'];
            else
                return ['msg' => 'You can not share this post now. Please try to it later.'];
        }
    }

    public function postComment(Request $request)
    {
        $post_id = $request->post_id;
        $pair = $request->pair;
        $post_comment = processString($request->text);

        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;

        $author_id = Posts::find($post_id)->users_id;

        $post_comment = is_null($post_comment) ? '' : $post_comment;

        $is_files = false;
        $file_lists = getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;

        if (!$is_files && $post_comment == "")
            return 0;

        $post = Posts::find($post_id);
        if (is_object($post)) {
            $new_comment = new PostsComments;
            $new_comment->users_id = $user_id;
            $new_comment->posts_id = $post_id;
            $new_comment->text = $post_comment;
            if ($new_comment->save()) {
                switch (session('place_country')) {
                    case 'country':
                        log_user_activity('Country_Post', 'comment', $post_id);
                        break;
                    case 'city':
                        log_user_activity('City_Post', 'comment', $post_id);
                        break;

                    default:
                        log_user_activity('Place_Post', 'comment', $post_id);
                        break;
                }

                // if($user_id != $author_id)
                //     notify($author_id, 'status_comment', $post_id);

                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('place-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/place-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $post_comment;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->save();

                    $posts_comments_medias = new PostsCommentsMedias();
                    $posts_comments_medias->posts_comments_id = $new_comment->id;
                    $posts_comments_medias->medias_id = $media->id;
                    $posts_comments_medias->save();
                }

                return view('site.place2.partials._post_comment_block', array('ppc' => $new_comment, 'post' => $post));
            } else {
                return 0;
            }
        }
    }

    public function postCommentEdit(Request $request)
    {
        $pair = $request['pair'];
        $post_id = $request->post_id;
        $comment_id = $request->comment_id;

        $post_comment_edit = processString($request->text);
        $user_id = Auth::guard('user')->user()->id;
        $author_id = Posts::find($post_id)->users_id;
        $post_comment = PostsComments::find($comment_id);

        $user_name = Auth::guard('user')->user()->name;

        $is_files = false;
        $file_lists = getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;
        $post_comment_edit = is_null($post_comment_edit) ? '' : $post_comment_edit;
        if (!$is_files && $post_comment_edit == "")
            return 0;

        $post = Posts::find($post_id);
        if (is_object($post)) {
            $post_comment->text = $post_comment_edit;
            if ($post_comment->save()) {
                switch (session('place_country')) {
                    case 'country':
                        log_user_activity('Country_Post', 'comment', $post_id);
                        break;
                    case 'city':
                        log_user_activity('City_Post', 'comment', $post_id);
                        break;

                    default:
                        log_user_activity('Place_Post', 'comment', $post_id);
                        break;
                }
                if ($user_id != $post_comment->users_id) {
                    notify($author_id, 'status_comment', $post_id);
                }

                if (isset($request->existing_media) && count($post_comment->medias) > 0) {
                    foreach ($post_comment->medias as $media_list) {
                        if (in_array($media_list->media->id, $request->existing_media)) {
                            $medias_exist = Media::find($media_list->medias_id);

                            $media_list->delete();

                            if ($medias_exist) {
                                $amazonefilename = explode('/', $medias_exist->url);
                                Storage::disk('s3')->delete('post-comment-photo/' . end($amazonefilename));

                                $medias_exist->delete();
                            }
                        }
                    }
                }



                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('place-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/place-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $post_comment_edit;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->save();


                    $posts_comments_medias = new PostsCommentsMedias();
                    $posts_comments_medias->posts_comments_id = $post_comment->id;
                    $posts_comments_medias->medias_id = $media->id;
                    $posts_comments_medias->save();
                }

                $post_comment->load('medias');

                if ($request->comment_type == 1) {
                    return view('site.place2.partials._post_comment_block', array('ppc' => $post_comment, 'post' => $post));
                } else {
                    return view('site.place2.partials._post_comment_reply_block', array('child' => $post_comment, 'post' => $post));
                }
            } else {
                return 0;
            }
        }
    }

    public function postCommentReply(Request $request)
    {
        $post_id = $request->post_id;
        $comment_id = $request->comment_id;
        $pair = $request->pair;

        $post_comment_reply = processString($request->text);
        $user_id = Auth::guard('user')->user()->id;

        $author_id = Posts::find($post_id)->users_id;
        $comment_author_id = PostsComments::find($comment_id)->users_id;

        $user_name = Auth::guard('user')->user()->name;

        $is_files = false;
        $file_lists = getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;

        $post_comment_reply = is_null($post_comment_reply) ? '' : $post_comment_reply;
        if (!$is_files && $post_comment_reply == "")
            return 0;

        $post = Posts::find($post_id);
        if (is_object($post)) {
            $new_comment = new PostsComments;
            $new_comment->users_id = $user_id;
            $new_comment->posts_id = $post_id;
            $new_comment->parents_id = $comment_id;
            $new_comment->text = $post_comment_reply;
            if ($new_comment->save()) {
                switch (session('place_country')) {
                    case 'country':
                        log_user_activity('Country_Post', 'commentreply', $post_id);
                        break;
                    case 'city':
                        log_user_activity('City_Post', 'commentreply', $post_id);
                        break;

                    default:
                        log_user_activity('Place_Post', 'commentreply', $post_id);
                        break;
                }
                // if($user_id != $comment_author_id)
                //     notify($author_id, 'status_comment', $post_id);

                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('place-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/place-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $post_comment_reply;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->save();

                    $posts_comments_medias = new PostsCommentsMedias();
                    $posts_comments_medias->posts_comments_id = $new_comment->id;
                    $posts_comments_medias->medias_id = $media->id;
                    $posts_comments_medias->save();
                }

                return view('site.place2.partials._post_comment_reply_block', array('child' => $new_comment, 'post' => $post));
            } else {
                return 0;
            }
        }
    }

    public function postShare($place_id, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->postShare($place_id, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->postShare($place_id, $request);

        $user_id = Auth::guard('user')->user()->id;
        $shared = PlacesShares::where('user_id', $user_id)->where('place_id', $place_id)->get()->first();

        if (is_object($shared)) {
            return ['msg' => 'You had already shared this place.'];
        } else {
            $share = new PlacesShares;
            $share->user_id = $user_id;
            $share->place_id = $place_id;
            $share->created_at = date("Y-m-d H:i:s");

            log_user_activity('Place', 'share', $place_id);

            if ($share->save()) {


                return ['msg' => 'You have just shared this place.'];
            } else
                return ['msg' => 'You can not share this place now. Please try to it later.'];
        }
    }

    public function ajaxCheckEvent($place_id, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->ajaxCheckEvent($place_id, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->ajaxCheckEvent($place_id, $request);

        $place = Place::find($place_id);
        $client = new Client();
        //$client->getHttpClient()->setDefaultOption('verify', false);

        $data = array();
        $data['data'] = '';
        $data['cnt'] = 0;
        try {

            $result = @$client->post('https://auth.predicthq.com/token', [
                'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'scope' => 'account events signals'
                ],
                'verify' => false
            ]);
            $events_array = false;
            if ($result->getStatusCode() == 200) {
                $json_response = $result->getBody()->read(1024);
                $response = json_decode($json_response);
                $access_token = $response->access_token;

                $get_events1 = $client->get('https://api.predicthq.com/v1/events/?end.gte=' . date('Y-m-d') . '&within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                    'headers' => ['Authorization' => 'Bearer ' . $access_token],
                    'verify' => false
                ]);
                if ($get_events1->getStatusCode() == 200) {
                    $events1 = json_decode($get_events1->getBody()->read(100024));
                    //dd($events);
                    $events_array1 = $events1->results;
                    $events_final = $events_array1;
                    if ($events1->next) {
                        $get_events2 = $client->get($events1->next, [
                            'headers' => ['Authorization' => 'Bearer ' . $access_token],
                            'verify' => false
                        ]);
                        if ($get_events2->getStatusCode() == 200) {
                            $events2 = json_decode($get_events2->getBody()->read(100024));
                            //dd($events);
                            $events_array2 = $events2->results;
                            $events_final = array_merge($events_array1, $events_array2);
                        }
                    }
                    $events = $events_final;

                    foreach ($events as $event) {
                        $check_envent = Events::where('provider_id', $event->id)->where('places_id', $place->id)->get()->first();
                        if (!is_object($check_envent)) {
                            $new_event = new Events();
                            $new_event->places_id = $place->id;
                            $new_event->provider_id = @$event->id;
                            $new_event->category = @$event->category;
                            $new_event->title = @$event->title;
                            $new_event->description = @$event->description;
                            $new_event->address = @$event->entities[0]->formatted_address;
                            $new_event->labels = @join(",", $event->labels);
                            $new_event->lat = @$event->location[1];
                            $new_event->lng = @$event->location[0];
                            $new_event->start = @$event->start;
                            $new_event->end = @$event->end;
                            $new_event->save();
                        }
                    }
                    $event_datas = $place->events()->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')->get();
                    $data['cnt'] = count($event_datas);
                    foreach ($event_datas as $evt) {
                        $data['data'] .= View::make('site.place2.partials._event_block4modal', ['evt' => $evt])->render();
                    }
                }
            }
        } catch (\Throwable $th) {
        }

        $postIds = get_postlist4placemedia($place->id, 'place');
        foreach ($place->getMedias as $media) {
            $posts = PostsMedia::where('medias_id', $media->id)->pluck('posts_id')->toArray();
            if (!count(array_intersect($posts, $postIds))) {
                $post = new Posts();
                $post->updated_at = $media->uploaded_at;
                $post->permission = 0;
                $post->save();
                $postplace = new PostsPlaces();
                $postplace->posts_id = $post->id;
                $postplace->places_id = $place->id;
                $postplace->save();
                $postmedia = new PostsMedia();
                $postmedia->posts_id = $post->id;
                $postmedia->medias_id = $media->id;
                $postmedia->save();
            }
        }

        return json_encode($data);
    }

    public function ajaxDiscussionTipLoadmore(Request $request)
    {
        $discussion_id = $request->dis_id;
        $page_num = $request->page_num;

        $skip = ($page_num - 1) * 3 + 1;

        $discussion = Discussion::find($discussion_id);
        if (is_object($discussion)) {
            $replies = $discussion->replies()
                ->orderBy('created_at', 'desc')
                ->skip($skip)
                ->take(3)
                ->get();

            $totalrows = count($discussion->replies);
            $has_next = ($totalrows > ($page_num * 3 + 1)) ? true : false;
            $rowcontent = "";

            foreach ($replies as $re) {
                $rowcontent .= '<div class="post__tip">
                    <div class="post__tip-main">
                        <img class="post__tip-avatar" src="' . check_profile_picture($re->author->profile_picture) . '" alt="" role="presentation">
                        <div class="post__tip-content">
                            <div class="post__tip-header">
                                <a href="#">' . $re->author->name . '</a> said<span class="post__tip-posted">' . diffForHumans($re->created_at) . '</span>
                            </div>
                            <div class="post__tip-text">' . $re->reply . '</div>
                        </div>
                    </div>
                    <div class="post__tip-footer">
                        <button class="post__tip-upvotes" type="button" reply-id="' . $re->id . '">
                            <svg class="icon icon--angle-up-solid">
                                <use xlink:href="' . asset('assets3/img/sprite.svg#angle-up-solid') . '"></use>
                            </svg>
                            <span><strong>' . @count($re->upvotes) . '</strong>  Upvotes</span>
                        </button>
                    </div>
                </div>';
            }

            return json_encode([
                "data" => $rowcontent,
                "has_next" => $has_next,
            ]);
        } else {
            return json_encode([
                "data" => "",
                "has_next" => false
            ]);
        }
    }

    public function ajaxEventComments(Request $request)
    {
        $evt_id = $request->event_id;
        $evt_comment = processString($request->text);
        $pair = $request->pair;

        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;

        $evt_comment = is_null($evt_comment) ? '' : $evt_comment;

        $is_files = false;
        $file_lists = getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;

        if (!$is_files && $evt_comment == "")
            return 0;

        $event = Events::find($evt_id);
        if (is_object($event)) {
            $new_comment = new EventsComments;
            $new_comment->users_id = $user_id;
            $new_comment->events_id = $evt_id;
            $new_comment->text = $evt_comment;
            if ($new_comment->save()) {
                log_user_activity('Event', 'comment', $evt_id);
                // if($user_id != $author_id)
                //     notify($author_id, 'status_comment', $post_id);
                // ranking_add_points($author_id, 1, 0.2, 'event_comment', $evt_id, $user_id);
                // ranking_add_points($author_id, 5, 0.05, 'event_comment', $evt_id, $user_id);

                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('event-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/event-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $evt_comment;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->save();

                    $event_comments_medias = new EventsCommentsMedias();
                    $event_comments_medias->events_comments_id = $new_comment->id;
                    $event_comments_medias->medias_id = $media->id;
                    $event_comments_medias->save();
                }

                return view('site.place2.partials.event_comment_block', array('comment' => $new_comment, 'evt' => $event));
            } else {
                return 0;
            }
        }
    }

    public function ajaxEventCommentsReply(Request $request)
    {
        $event_id = $request->event_id;
        $comment_id = $request->comment_id;
        $pair = $request->pair;

        $event_comment_reply = processString($request->text);
        $user_id = Auth::guard('user')->user()->id;

        $user_name = Auth::guard('user')->user()->name;

        $is_files = false;
        $file_lists = getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;

        $event_comment_reply = is_null($event_comment_reply) ? '' : $event_comment_reply;
        if (!$is_files && $event_comment_reply == "")
            return 0;

        $event = Events::find($event_id);
        if (is_object($event)) {
            $new_comment = new EventsComments;
            $new_comment->users_id = $user_id;
            $new_comment->events_id = $event_id;
            $new_comment->parents_id = $comment_id;
            $new_comment->text = $event_comment_reply;
            if ($new_comment->save()) {
                log_user_activity('Event', 'commentreply', $event_id);
                // if($user_id != $comment_author_id)
                //     notify($author_id, 'status_comment', $post_id);


                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('event-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/event-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $event_comment_reply;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->save();

                    $event_comments_medias = new EventsCommentsMedias();
                    $event_comments_medias->events_comments_id = $new_comment->id;
                    $event_comments_medias->medias_id = $media->id;
                    $event_comments_medias->save();
                }

                return view('site.place2.partials.event_comment_reply_block', array('child' => $new_comment, 'evt' => $event));
            } else {
                return 0;
            }
        }
    }

    public function ajaxEventCommentsEdit(Request $request)
    {
        $pair = $request['pair'];
        $event_id = $request->event_id;
        $comment_id = $request->comment_id;

        $event_comment_edit = processString($request->text);
        $user_id = Auth::guard('user')->user()->id;
        $event_comment = EventsComments::find($comment_id);

        $user_name = Auth::guard('user')->user()->name;

        $is_files = false;
        $file_lists = getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;
        $event_comment_edit = is_null($event_comment_edit) ? '' : $event_comment_edit;
        if (!$is_files && $event_comment_edit == "")
            return 0;

        $event = Events::find($event_id);
        if (is_object($event)) {
            $event_comment->text = $event_comment_edit;
            if ($event_comment->save()) {

                if (isset($request->existing_media) && count($event_comment->medias) > 0) {
                    foreach ($event_comment->medias as $media_list) {
                        if (in_array($media_list->media->id, $request->existing_media)) {
                            $medias_exist = Media::find($media_list->medias_id);

                            $media_list->delete();

                            if ($medias_exist) {
                                $amazonefilename = explode('/', $medias_exist->url);
                                Storage::disk('s3')->delete('event-comment-photo/' . end($amazonefilename));

                                $medias_exist->delete();
                            }
                        }
                    }
                }



                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('event-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/event-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $event_comment_edit;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->save();


                    $event_comments_medias = new EventsCommentsMedias();
                    $event_comments_medias->events_comments_id = $event_comment->id;
                    $event_comments_medias->medias_id = $media->id;
                    $event_comments_medias->save();
                }

                $event_comment->load('medias');

                if ($request->comment_type == 1) {
                    return view('site.place2.partials.event_comment_block', array('comment' => $event_comment, 'evt' => $event));
                } else {
                    return view('site.place2.partials.event_comment_reply_block', array('child' => $event_comment, 'evt' => $event));
                }
            } else {
                return 0;
            }
        }
    }

    public function ajaxEventCommentsDelete(Request $request)
    {
        $comment_id = $request->comment_id;

        $user_id = Auth::guard('user')->user()->id;

        // $author_id = Events::find($post_id)->users_id;

        $comment = EventsComments::where('users_id', $user_id)->find($comment_id);

        foreach ($comment->medias as $com_media) {
            $medias_exist = Media::find($com_media->medias_id);

            $com_media->delete();

            if ($medias_exist) {
                $amazonefilename = explode('/', $medias_exist->url);
                Storage::disk('s3')->delete('event-comment-photo/' . end($amazonefilename));

                $medias_exist->delete();
            }
        }

        if ($comment->delete()) {
            return json_encode([
                'status' => 'yes'
            ]);
        } else {
            return json_encode([
                'status' => 'no',
                'msg' => 'You can not delete this comment now. Please try to it later.'
            ]);
        }
    }

    public function ajaxEventCommentsLike(Request $request)
    {
        $comment_id = $request->comment_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = EventsComments::find($comment_id)->users_id;
        $event_id = EventsComments::find($comment_id)->events_id;
        $check_like_exists = EventsCommentsLikes::where('events_comments_id', $comment_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            //dd($check_like_exists);
            $check_like_exists->delete();
            log_user_activity('Event', 'commentunlike', $comment_id);
            //notify($author_id, 'status_commentunlike', $comment_id);

            $data['status'] = 'no';
        } else {
            $like = new EventsCommentsLikes;
            $like->events_comments_id = $comment_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Event', 'commentlike', $comment_id);

            $data['status'] = 'yes';
        }
        $data['count'] = count(EventsCommentsLikes::where('events_comments_id', $comment_id)->get());
        $data['name'] = Auth::guard('user')->user()->name;
        return json_encode($data);
    }

    public function ajaxEventLikes(Request $request)
    {
        $event_id = $request->event_id;
        $user_id = Auth::guard('user')->user()->id;

        $check_like_exists = EventsLikes::where('events_id', $event_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            $check_like_exists->delete();

            $data['status'] = 'no';
        } else {
            $like = new EventsLikes;
            $like->events_id = $event_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Event', 'like', $event_id);

            $data['status'] = 'yes';
        }
        $data['count'] = count(EventsLikes::where('events_id', $event_id)->get());
        return json_encode($data);
    }

    public function ajaxMediaLikes(Request $request)
    {
        $media_id = $request->media_id;
        $user_id = Auth::guard('user')->user()->id;



        $check_like_exists = \App\Models\ActivityMedia\MediasLikes::where('medias_id', $media_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            $check_like_exists->delete();

            $data['status'] = 'no';
        } else {
            $like = new \App\Models\ActivityMedia\MediasLikes;
            $like->medias_id = $media_id;
            $like->users_id = $user_id;
            $like->save();

            switch (session('place_country')) {
                case 'country':
                    log_user_activity('Country_Media', 'like', $media_id);
                    break;
                case 'city':
                    log_user_activity('City_Media', 'like', $media_id);
                    break;

                default:
                    log_user_activity('Place_Media', 'like', $media_id);
                    break;
            }

            $data['status'] = 'yes';
        }
        $data['count'] = count(\App\Models\ActivityMedia\MediasLikes::where('medias_id', $media_id)->get());
        return json_encode($data);
    }

    public function ajaxEventShares(Request $request)
    {
        $event_id = $request->event_id;
        $user_id = Auth::guard('user')->user()->id;
        $shared = EventsShares::where('users_id', $user_id)->where('events_id', $event_id)->get()->first();

        $data = array();
        if (is_object($shared)) {
            $data['status'] = 'no';
            $data['msg'] = 'You had already shared this event.';
        } else {
            $share = new EventsShares;
            $share->users_id = $user_id;
            $share->events_id = $event_id;
            $share->created_at = date("Y-m-d H:i:s");

            if ($share->save()) {
                log_user_activity('Event', 'share', $event_id);
                $data['status'] = 'yes';
                $data['count'] = EventsShares::where('users_id', $user_id)->where('events_id', $event_id)->count();
                $data['msg'] = 'You have just shared this event.';
            }
        }
        return json_encode($data);
    }

    public function ajaxEventComments4Modal(Request $request)
    {
        $event_id = $request->event_id;
        $count = $request->count;

        $skip = $count;

        $event = Events::find($event_id);
        $rowcontent = '';
        $totalrows = 0;
        if (is_object($event)) {
            $comments = $event->comments()->skip($skip)
                ->take(5)
                ->get();

            foreach ($comments as $comment) {
                $rowcontent .= view('site.place2.partials.event_comment_block', array('comment' => $comment, 'evt' => $event));
            }

            $totalrows = count($event->comments);
        }
        return json_encode([
            "data" => $rowcontent,
            "rows" => $totalrows
        ]);
    }

    public function ajaxMediaComments(Request $request)
    {
        $media_id = $request->media_id;

        $media = Media::find($media_id);
        if (is_object($media)) {
            $comments = $media->comments;

            $totalrows = count($comments);
            $rowcontent = "";

            foreach ($comments as $comment) {
                $rowcontent .= view('site.home.partials.media_comment_block', array('comment' => $comment, 'post_object' => $comment));
            }

            return json_encode([
                "data" => $rowcontent,
                "totalrows" => $totalrows,
            ]);
        } else {
            return json_encode([
                "data" => "",
                "totalrows" => $totalrows,
            ]);
        }
    }

    public function ajaxMediaComments4Modal(Request $request)
    {
        $media_id = $request->media_id;
        $count = $request->count;

        $skip = $count;

        $media = Media::find($media_id);
        $rowcontent = '';
        $totalrows = 0;
        $author = '';
        if (is_object($media)) {
            $comments = $media->comments()->skip($skip)
                ->take(5)
                ->get();

            foreach ($comments as $comment) {
                $rowcontent .= view('site.home.partials.media_comment_block', array('comment' => $comment, 'post_object' => $media));
            }

            $totalrows = count($media->comments);
            $author = is_object($media->mediaUser) ? $media->mediaUser->user : '';
            if ($author != '') {
                $author->profile_picture = $author->profile_picture == '' ? '' : check_profile_picture($author->profile_picture);
                $author->name = $author->name == null ? '' : $author->name;
            }
        }

        return json_encode([
            "data" => $rowcontent,
            "author" => $author,
            "uploaded_at" => diffForHumans($media->uploaded_at),
            "caption" => $media->title,
            "rows" => $totalrows,
            "media" => $media,
            "media_likes" => count($media->likes),
            "like_class" => i_like('media', $media->id) ? 'red' : ''
        ]);
    }

    // public function ajaxUpdatePosts($place_id, Request $request) {
    //     $page = $request->get('pageno');
    //     $skip = ($page - 1) * 10;
    //     $return = false;
    //     $place = Place::find($place_id);
    //     $posts = collect_posts('place', $place_id, $page);
    //     foreach ($posts AS $post) {
    //         echo " ";
    //         if ($post['type'] == 'regular') {
    //             $post = Posts::find($post['id']);
    //             if ($post->text != '' && count($post->medias) > 0)
    //                 $return .= view('site.place2.partials.post_media_with_text', ['post' => $post, 'place' => $place]);
    //             elseif ($post->text == '' && count($post->medias) > 0)
    //                 $return .= view('site.place2.partials.post_media_without_text', ['post' => $post, 'place' => $place]);
    //             else
    //                 $return .= view('site.place2.partials.post_text', ['post' => $post, 'place' => $place]);
    //         }
    //         else if ($post['type'] == 'discussion') {
    //             $discussion = Discussion::find($post['id']);
    //             $return .= view('site.place2.partials.post_discussion', ['discussion' => $discussion, 'place' => $place]);
    //         } elseif ($post['type'] == 'plan') {
    //             $plan = TripPlans::find($post['id']);
    //             $return .= view('site.place2.partials.post_plan', ['plan' => $plan, 'place' => $place]);
    //         } elseif ($post['type'] == 'report') {
    //             $report = Reports::find($post['id']);
    //             $return .= view('site.place2.partials.post_report', ['report' => $report, 'place' => $place]);
    //         }
    //         // else if ($post['type'] == 'travelmate') {
    //         //     $travelmate_request = TravelMatesRequests::find($post['id']);
    //         //     $return .= view('site.place2.partials.post_travelmate', ['travelmate_request' => $travelmate_request, 'place' => $place]);
    //         // }
    //     }
    //     echo $return;
    // }

    public function postReviewUpDownVote(Request $request, RankingService $rankingService)
    {
        $review_id = $request->review_id;
        $user_id = Auth::guard('user')->user()->id;

        $review = Reviews::find($review_id);

        $author_id = $review->by_users_id;

        if (count($review->updownvotes()->where('users_id', $user_id)->where('vote_type', $request->vote_type)->get()) == 0) {
            $reviewUpvote = new ReviewsVotes;
            $reviewUpvote->review_id = $review_id;
            $reviewUpvote->users_id = $user_id;
            $reviewUpvote->vote_type = $request->vote_type;
            $reviewUpvote->save();

            if ($request->vote_type == 1) {
                $rankingService->addPointsToEarners($reviewUpvote->review);

                if (count($review->updownvotes()->where('users_id', $user_id)->where('vote_type', 2)->get()) != 0) {
                    $review->updownvotes()->where('users_id', $user_id)->where('vote_type', 2)->delete();
                }
                log_user_activity('Review', 'upvote', $review_id);
            } else {
                if (count($review->updownvotes()->where('users_id', $user_id)->where('vote_type', 1)->get()) != 0) {
                    $review->updownvotes()->where('users_id', $user_id)->where('vote_type', 1)->delete();
                    $rankingService->subPointsFromEarners($reviewUpvote->review);
                }
                log_user_activity('Review', 'downvote', $review_id);
            }
            return json_encode(array(
                'status' => 1,
                'count_upvotes' => count($review->updownvotes()->where('vote_type', 1)->get()),
                'count_downvotes' => count($review->updownvotes()->where('vote_type', 2)->get())
            ));
        } else {
            $reviewUpvote = $review->updownvotes()->where('users_id', $user_id)->where('vote_type', $request->vote_type)->first();
            $reviewUpvote->delete();

            if ($request->vote_type == 1) {
                log_user_activity('Review', 'upvote', $review_id);
                $rankingService->subPointsFromEarners($reviewUpvote->review);
            } else {
                log_user_activity('Review', 'downvote', $review_id);
            }
            return json_encode(array(
                'status' => 1,
                'count_upvotes' => count($review->updownvotes()->where('vote_type', 1)->get()),
                'count_downvotes' => count($review->updownvotes()->where('vote_type', 2)->get())
            ));
        }
    }

    public function ajaxGetMedia(Request $request)
    {
        $place_id = $request->get('place_id');
        $final_results = array();
        $h = Place::find($place_id);
        if (is_object($h)) {
            $details_link = file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?'
                . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
                . '&placeid=' . $h->provider_id);
            $details = json_decode($details_link);
            //dd($details);
            if (isset($details->result)) {
                $details = $details->result;
                if (isset($details->photos)) {
                    $place_photos = $details->photos;
                    $raw_photos = array();
                    $i = 1;

                    foreach ($place_photos as $pp) {
                        $file_contents = file_get_contents('https://maps.googleapis.com/maps/api/place/photo?'
                            . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
                            . '&maxwidth=1600'
                            . '&photoreference=' . $pp->photo_reference);

                        $sha1 = sha1($file_contents);
                        $md5 = md5($file_contents);
                        $size = strlen($file_contents);
                        $raw_photos[$i]['sha1'] = $sha1;
                        $raw_photos[$i]['md5'] = $md5;
                        $raw_photos[$i]['size'] = $size;
                        $raw_photos[$i]['contents'] = $file_contents;
                        $i++;


                        $check_media_exists = \App\Models\ActivityMedia\Media::where('sha1', $sha1)
                            ->where('md5', $md5)
                            ->where('filesize', $size)
                            ->get()
                            ->count();
                        if ($check_media_exists == 0) {
                            $media_file = 'places/' . $h->provider_id . '/' . sha1(microtime()) . '.jpg';
                            \Illuminate\Support\Facades\Storage::disk('s3')->put($media_file, $file_contents, 'public');


                            $media = new \App\Models\ActivityMedia\Media;
                            $media->url = $media_file;
                            $media->sha1 = $sha1;
                            $media->filesize = $size;
                            $media->md5 = $md5;
                            $media->html_attributions = join(",", $pp->html_attributions);
                            $media->save();
                            if ($media->save()) {
                                $place_media = new \App\Models\Place\PlaceMedias;
                                $place_media->places_id = $h->id;
                                $place_media->medias_id = $media->id;
                                $place_media->save();

                                $complete_url = $media->url;
                                $url = explode("/", $complete_url);
                                //$data = file_get_contents('https://s3.amazonaws.com/travooo-images2/'.$complete_url);
                                //$s3 = App::make('aws')->createClient('s3');

                                $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

                                $options = [
                                    'region' => 'us-east-1',
                                    'version' => 'latest',
                                    'http' => ['verify' => false],
                                    'credentials' => $credentials,
                                    'endpoint' => 'https://s3.amazonaws.com'
                                ];

                                $s3Client = new S3Client($options);
                                //$s3 = AWS::createClient('s3');
                                $result = $s3Client->getObject([
                                    'Bucket' => 'travooo-images2', // REQUIRED
                                    'Key' => $complete_url, // REQUIRED
                                    'ResponseContentType' => 'text/plain',
                                ]);

                                $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });
                                $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });
                                $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });
                                $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });

                                //return $img_700->response('jpg');
                                //echo $complete_url . '<br />';
                                //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

                                $put_1100 = $s3Client->putObject([
                                    'ACL' => 'public-read',
                                    'Body' => $img_1100->encode(),
                                    'Bucket' => 'travooo-images2',
                                    'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                ]);

                                $put_700 = $s3Client->putObject([
                                    'ACL' => 'public-read',
                                    'Body' => $img_700->encode(),
                                    'Bucket' => 'travooo-images2',
                                    'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                ]);

                                $put_230 = $s3Client->putObject([
                                    'ACL' => 'public-read',
                                    'Body' => $img_230->encode(),
                                    'Bucket' => 'travooo-images2',
                                    'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                ]);

                                $put_180 = $s3Client->putObject([
                                    'ACL' => 'public-read',
                                    'Body' => $img_180->encode(),
                                    'Bucket' => 'travooo-images2',
                                    'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                ]);


                                $media->thumbs_done = 1;
                                $media->save();
                            }
                        }
                    }

                    if (isset($put_180))
                        return 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2];

                    //return serialize($raw_photos);
                    //return $raw_photos;
                }
            }
        }
    }

    public function ajaxGetPOIMedia(Request $request)
    {
        $place_id = $request->get('place_id');
        $is_new = $request->get('is_new');
        $final_results = array();
        $h = Place::find($place_id);
        if (isset($is_new) && $is_new == 1 && $h->is_media_added == 1) {
            return 0;
        }
        if (is_object($h)) {
            $details_link = file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?'
                . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
                . '&placeid=' . $h->provider_id);
            $details = json_decode($details_link);
            if (isset($details->result)) {
                $details = $details->result;
                if (isset($details->photos)) {
                    $place_photos = $details->photos;
                    $raw_photos = array();
                    $i = 1;

                    foreach ($place_photos as $pp) {
                        //dd($pp);
                        $file_contents = file_get_contents('https://maps.googleapis.com/maps/api/place/photo?'
                            . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
                            . '&maxwidth=1600'
                            . '&photoreference=' . $pp->photo_reference);

                        $sha1 = sha1($file_contents);
                        $md5 = md5($file_contents);
                        $size = strlen($file_contents);
                        $raw_photos[$i]['sha1'] = $sha1;
                        $raw_photos[$i]['md5'] = $md5;
                        $raw_photos[$i]['size'] = $size;
                        $raw_photos[$i]['contents'] = $file_contents;
                        $i++;


                        $check_media_exists = \App\Models\ActivityMedia\Media::where('sha1', $sha1)
                            ->where('md5', $md5)
                            ->where('filesize', $size)
                            ->get();


                        if ($check_media_exists->count() <= 0) {
                            $media_file = 'places/' . $h->provider_id . '/' . sha1(microtime()) . '.jpg';
                            \Illuminate\Support\Facades\Storage::disk('s3')->put($media_file, $file_contents, 'public');


                            $media = new \App\Models\ActivityMedia\Media;
                            $media->url = $media_file;
                            $media->sha1 = $sha1;
                            $media->filesize = $size;
                            $media->md5 = $md5;
                            $media->html_attributions = join(",", $pp->html_attributions);
                            $media->save();

                            if ($media->save()) {
                                $place_media = new \App\Models\Place\PlaceMedias;
                                $place_media->places_id = $h->id;
                                $place_media->medias_id = $media->id;
                                $place_media->save();

                                // create new post
                                $po = new Posts();
                                $po->save();

                                // link post to place
                                $pp = new PostsPlaces();
                                $pp->posts_id = $po->id;
                                $pp->places_id = $h->id;
                                $pp->save();

                                // link media to post
                                $posts_medias = new PostsMedia();
                                $posts_medias->posts_id = $po->id;
                                $posts_medias->medias_id = $media->id;
                                $posts_medias->save();




                                $complete_url = $media->url;
                                $url = explode("/", $complete_url);
                                //$data = file_get_contents('https://s3.amazonaws.com/travooo-images2/'.$complete_url);
                                //$s3 = App::make('aws')->createClient('s3');

                                $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

                                $options = [
                                    'region' => 'us-east-1',
                                    'version' => 'latest',
                                    'http' => ['verify' => false],
                                    'credentials' => $credentials,
                                    'endpoint' => 'https://s3.amazonaws.com'
                                ];

                                $s3Client = new S3Client($options);
                                //$s3 = AWS::createClient('s3');
                                $result = $s3Client->getObject([
                                    'Bucket' => 'travooo-images2', // REQUIRED
                                    'Key' => $complete_url, // REQUIRED
                                    'ResponseContentType' => 'text/plain',
                                ]);

                                $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });
                                $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });
                                $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });
                                $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });

                                //return $img_700->response('jpg');
                                //echo $complete_url . '<br />';
                                //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

                                $put_1100 = $s3Client->putObject([
                                    'ACL' => 'public-read',
                                    'Body' => $img_1100->encode(),
                                    'Bucket' => 'travooo-images2',
                                    'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                ]);

                                $put_700 = $s3Client->putObject([
                                    'ACL' => 'public-read',
                                    'Body' => $img_700->encode(),
                                    'Bucket' => 'travooo-images2',
                                    'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                ]);

                                $put_230 = $s3Client->putObject([
                                    'ACL' => 'public-read',
                                    'Body' => $img_230->encode(),
                                    'Bucket' => 'travooo-images2',
                                    'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                ]);

                                $put_180 = $s3Client->putObject([
                                    'ACL' => 'public-read',
                                    'Body' => $img_180->encode(),
                                    'Bucket' => 'travooo-images2',
                                    'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                ]);


                                $media->thumbs_done = 1;
                                $media->save();
                            }
                        }
                    }

                    if (isset($is_new) && $is_new == 1 && $h->is_media_added == 0) {
                        $h->is_media_added = 1;
                        $h->save();
                        $place = Place::with([
                            'getMedias',
                            'getMedias.users',
                        ])
                            ->where('id', $place_id)
                            ->where('active', 1)
                            ->first();
                        return json_encode($place);
                    } else {
                        if (isset($put_180))
                            return 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2];
                    }
                    //return serialize($raw_photos);
                    //return $raw_photos;
                }
            }
        }
    } // end ajaxGetPOIMedia

    public function ajaxGetTripData(Request $request)
    {

        $id = $request->id;

        $plan = TripPlans::find($id);

        $data = [];

        if (is_object($plan)) {
            $points = [];
            $polylines = "";
            $center = [];
            $zoom = 8;
            $markers = [];
            $waypoint = [];
            $lats = [];
            $lngs = [];

            foreach ($plan->places as $pl) :
                $markers[] = "markers=anchor:center|icon:" . asset("assets2/image/marker_s.png") . "|" . $pl->lat . "," . $pl->lng;

                $points[] = $pl->lat . "," . $pl->lng;
                $waypoint[] = "via:" . $pl->lat . "," . $pl->lng;
                $lats[] = $pl->lat;
                $lngs[] = $pl->lng;
            endforeach;

            $newwaypoint = $waypoint;
            @$origin = $points[0];
            @$destin = $points[count($points) - 1];

            $url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' . $origin . '&destination=' . $destin . '&waypoints=optimize:true|' . implode("|", $newwaypoint) . '&mode=driving&key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, false);
            $result = curl_exec($ch);
            curl_close($ch);
            $googleDirection = json_decode($result, true);
            try {
                $polyline = urlencode($googleDirection['routes'][0]['overview_polyline']['points']);
                $polylines = "enc:" . $polyline;

                $lat = ($googleDirection['routes'][0]['bounds']['southwest']['lat'] + $googleDirection['routes'][0]['bounds']['northeast']['lat']) / 2;
                $lng = ($googleDirection['routes'][0]['bounds']['southwest']['lng'] + $googleDirection['routes'][0]['bounds']['northeast']['lng']) / 2;
                $center[0] = isset($center[0]) ? ($center[0] + $lat) / 2 : $lat;
                $center[1] = isset($center[1]) ? ($center[1] + $lng) / 2 : $lng;

                $west = $googleDirection['routes'][0]['bounds']['southwest']['lng'];
                $east = $googleDirection['routes'][0]['bounds']['northeast']['lng'];

                $angle = $east - $west;
                if ($angle < 0) {
                    $angle += 360;
                }
                $zoom = @floor(log(712 * 360 / $angle / 256) / log(2)) - 1;
            } catch (\Throwable $th) {
                if (count($lats) > 0) {
                    $center[0] = array_sum($lats) / count($lats);
                    $center[1] = array_sum($lngs) / count($lngs);

                    $angle = max($lngs) - min($lngs);
                    if ($angle < 0) {
                        $angle += 360;
                    }
                    $zoom = @floor(log(712 * 360 / $angle / 256) / log(2)) - 2;
                }
                $polylines = "geodesic:true|" . implode('|', $points);
            }

            @$center = count($center) > 0 ? $center : [$plan->places[0]->lat, $plan->places[0]->lng];
            $zoom = $zoom === INF ? 14 : $zoom;
            $markers = implode("&", $markers);
            $data['msg'] = 'ok';
            $data['content'] = View::make('site.place2.partials._trip_inner4modal', ['plan' => $plan, 'center' => $center, 'zoom' => $zoom, 'markers' => $markers, 'polylines' => $polylines])->render();
        } else {
            $data['msg'] = "Try later.";
        }
        echo json_encode($data);
    }

    public function ajaxTripComments4Modal(Request $request)
    {
        $trip_id = $request->trip_id;
        $count = $request->count;

        $skip = $count;

        $plan = TripPlans::find($trip_id);
        $rowcontent = '';
        $totalrows = 0;
        $author = '';
        if (is_object($plan)) {
            $comments = $plan->comments()->skip($skip)
                ->take(5)
                ->get();

            foreach ($comments as $comment) {
                $rowcontent .= view('site.home.partials.trip_comment_block', array('comment' => $comment, 'plan' => $plan));
            }

            $totalrows = count($plan->comments);
        }

        return json_encode([
            "data" => $rowcontent,
            "rows" => $totalrows
        ]);
    }

    public function searchAllDatas(Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->searchAllDatas($request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->searchAllDatas($request);

        $query = $request->get('q');
        $search_in = $request->get('search_in');
        $search_id = $request->get('search_id');

        $place = Place::find($search_id);

        if (!$place) {
            // return json_encode([
            //     'search_discussion' => ['count'=>0, 'data'=>""],
            //     'search_post' => ['count'=>0, 'data'=>""],
            //     'search_media' => ['count'=>0, 'data'=>""],
            //     'search_plan' => ['count'=>0, 'data'=>""],
            //     'search_mate' => ['count'=>0, 'data'=>""],
            //     'search_event' => ['count'=>0, 'data'=>""],
            //     'search_report' => ['count'=>0, 'data'=>""],
            //     'search_review' => ['count'=>0, 'data'=>""]
            // ]);
            echo "";
        } else {
            $data = '';

            // getting discussion for place
            $discussions = Discussion::where('destination_type', 'Place')
                ->where('destination_id', $place->id)
                ->whereRaw('(question like "%' . $query . '%" or description like "%' . $query . '%")')
                ->paginate(10);

            foreach ($discussions as $discussion) {

                $data .= view('site.place2.partials.post_discussion', array('discussion' => $discussion, 'place' => $place, 'search_result' => true));
            }


            // getting text post for place
            $post_ary = PostsPlaces::where('places_id', $place->id)->pluck('posts_id')->toArray();
            if (count($post_ary) > 0) {
                $posts = Posts::whereIn('id', $post_ary)
                    ->whereRaw('text like "%' . $query . '%"')
                    ->paginate(10);
                foreach ($posts as $post) {

                    $data .= view('site.place2.partials.post_text', array('post' => $post, 'place' => $place, 'search_result' => true));
                }
            }

            // getting media post for place
            $media_ary = $place->medias->pluck('id')->toArray();
            $post_ary = PostsMedia::whereIn('medias_id', $media_ary)->pluck('posts_id')->toArray();
            if (count($post_ary) > 0) {
                $media = Posts::whereIn('id', $post_ary)
                    ->whereRaw('text like "%' . $query . '%"')
                    ->paginate(10);
                foreach ($media as $m) {
                    $data .= view('site.place2.partials.media_block', array('media' => $m, 'place' => $place, 'search_result' => true));
                }
            }

            // getting travelmates for place
            $mates_ary = User::whereRaw('(name like "%' . $query . '%" or interests like "%' . $query . '%")')
                ->pluck('id');

            $travelmates = TravelMatesRequests::whereHas('plan_place', function ($q) use ($place) {
                $q->where('places_id', '=', $place->id);
            })
                ->whereIn('users_id', $mates_ary)
                ->paginate(10);
            if (count($travelmates) == 0) {
                $travelmates = TravelMatesRequests::whereHas('plan_city', function ($q) use ($place) {
                    $q->where('cities_id', '=', $place->city->id);
                })
                    ->whereIn('users_id', $mates_ary)
                    ->paginate(10);
            }

            foreach ($travelmates as $travelmate_request) {
                $data .= view('site.place2.partials.post_travelmate', array('travelmate_request' => $travelmate_request, 'place' => $place, 'search_result' => true));
            }

            // getting reports for place
            $reports_ary = ReportsInfos::where('var', 'place')
                ->where('val', $place->id)
                ->whereHas('report', function ($q) {
                    $q->where('flag', '=', 1);
                })
                ->pluck('reports_id')->toArray();
            if (count($reports_ary) == 0) {
                $reports = Reports::where('cities_id', '=', $place->city->id)
                    ->whereRaw('(title like "%' . $query . '%" or description like "%' . $query . '%")')
                    ->paginate(10);
            } else {
                $reports = Reports::whereIn('id', $reports_ary)
                    ->whereRaw('(title like "%' . $query . '%" or description like "%' . $query . '%")')
                    ->paginate(10);
            }

            foreach ($reports as $rp) {
                $data .= view('site.place2.partials.post_report', array('report' => $rp, 'place' => $place, 'search_result' => true));
            }

            // getting reviews for place
            $reviews = Reviews::where('places_id', $place->id)
                ->whereRaw('text like "%' . $query . '%"')
                ->paginate(10);

            foreach ($reviews as $r) {
                $data .= view('site.place2.partials.post_review', array('review' => $r, 'place' => $place, 'search_result' => true));
            }


            //getting trip plans for place
            $trip_plans = TripPlaces::where('places_id', $place->id)->pluck('trips_id')->toArray();

            if (count($trip_plans) > 0) {
                $trips = TripPlans::whereIn('id', $trip_plans)
                    ->whereRaw('title like "%' . $query . '%"')
                    ->paginate(10);

                foreach ($trips as $tr) {
                    $data .= view('site.place2.partials.post_plan', array('plan' => $tr, 'place' => $place, 'search_result' => true));
                }
            }

            //getting events for place
            $events = array();
            try {
                $client = new Client();
                $result = @$client->post('https://auth.predicthq.com/token', [
                    'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
                    'form_params' => [
                        'grant_type' => 'client_credentials',
                        'scope' => 'account events signals'
                    ],
                    'verify' => false
                ]);
                $events_array = false;
                if ($result->getStatusCode() == 200) {
                    $json_response = $result->getBody()->read(1024);
                    $response = json_decode($json_response);
                    $access_token = $response->access_token;

                    $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                        'headers' => ['Authorization' => 'Bearer ' . $access_token],
                        'verify' => false
                    ]);
                    if ($get_events1->getStatusCode() == 200) {
                        $events1 = json_decode($get_events1->getBody()->read(100024));
                        //dd($events);
                        $events_array1 = $events1->results;
                        $events_final = $events_array1;
                        if ($events1->next) {
                            $get_events2 = $client->get($events1->next, [
                                'headers' => ['Authorization' => 'Bearer ' . $access_token],
                                'verify' => false
                            ]);
                            if ($get_events2->getStatusCode() == 200) {
                                $events2 = json_decode($get_events2->getBody()->read(100024));
                                //dd($events);
                                $events_array2 = $events2->results;
                                $events_final = array_merge($events_array1, $events_array2);
                            }
                        }
                        $events = $events_final;
                    }
                }

                if (count($events) > 0) {
                    foreach ($events as $evt) {
                        if (strpos($evt->title, $query) || strpos($evt->description, $query) || strpos(@$evt->entities[0]->formatted_address, $query)) {
                            $data .= view('site.place2.partials.post_event', array('evt' => $evt, 'place' => $place, 'search_result' => true));
                        }
                    }
                }
            } catch (\Throwable $th) {
            }
            echo $data;
        }
        // if ( !$place ) {
        //     return json_encode([
        //         'search_discussion' => ['count'=>0, 'data'=>""],
        //         'search_post' => ['count'=>0, 'data'=>""],
        //         'search_media' => ['count'=>0, 'data'=>""],
        //         'search_plan' => ['count'=>0, 'data'=>""],
        //         'search_mate' => ['count'=>0, 'data'=>""],
        //         'search_event' => ['count'=>0, 'data'=>""],
        //         'search_report' => ['count'=>0, 'data'=>""],
        //         'search_review' => ['count'=>0, 'data'=>""]
        //     ]);
        // } else {
        //     $data = [];
        //     // getting discussion for place
        //     $discussion = Discussion::where('destination_type', 'Place')
        //         ->where('destination_id', $place->id)
        //         ->whereRaw('(question like "%'.$query.'%" or description like "%'.$query.'%")')
        //         ->paginate(10);
        //     $data['search_discussion']['count'] = $discussion->total();
        //     $data['search_discussion']['data'] = "";
        //     foreach($discussion as $dis) {
        //         $data['search_discussion']['data'] .= '<div class="drop-row" style="cursor:pointer"><div class="img-wrap rounded place-img"><img src="'.check_profile_picture($dis->author->profile_picture).'" alt="logo" style="width:60px;height:60px;min-width:60px!important;"></div><div class="drop-content place-content"><h4 class="content-ttl">' . $dis->question . '</h4><p class="place-name"><span class="follow-tag">' . $dis->author->name . '</span> <span>' . str_limit($dis->description, 30) . '</span></p></div></div>';
        //     }
        //     // getting text post for place
        //     $post_ary = PostsPlaces::where('places_id', $place->id)->pluck('posts_id')->toArray();
        //     if( count($post_ary) > 0 ) {
        //         $post = Posts::whereIn('id', $post_ary)
        //             ->whereRaw('text like "%' . $query . '%"')
        //             ->paginate(10);
        //         $data['search_post']['count'] = $post->total();
        //         $data['search_post']['data'] = '';
        //         foreach( $post as $p ) {
        //             $data['search_post']['data'] .= '<div class="drop-row" style="cursor:pointer"><div class="img-wrap rounded place-img"><img src="'.check_profile_picture($p->author->profile_picture).'" alt="logo" style="width:60px;height:60px;min-width:60px!important;"></div><div class="drop-content place-content"><h4 class="content-ttl">' . $p->author->name . '</h4><p class="place-name"><span class="follow-tag"></span> <span>' . str_limit($p->text, 30) . '</span></p></div></div>';
        //         }
        //     }
        //     // getting media post for place
        //     $media_ary = $place->medias->pluck('id')->toArray();
        //     $post_ary = PostsMedia::whereIn('medias_id', $media_ary)->pluck('posts_id')->toArray();
        //     if( count($post_ary) > 0 ) {
        //         $media = Posts::whereIn('id', $post_ary)
        //             ->whereRaw('text like "%' . $query . '%"')
        //             ->paginate(10);
        //         $data['search_media']['count'] = $media->total();
        //         $data['search_media']['data'] = '';
        //         foreach($media as $m) {
        //             $data['search_media']['data'] .= '<div class="drop-row" style="cursor:pointer"><div class="img-wrap place-img"><img src="https://s3.amazonaws.com/travooo-images2/'.$m->medias[0]->media->url.'" alt="logo" style="width:60px;height:60px;min-width:60px!important;"></div><div class="drop-content place-content"><h4 class="content-ttl">' . $m->author->name . '</h4><p class="place-name"><span class="follow-tag"></span> <span>' . str_limit($m->text, 30) . '</span></p></div></div>';
        //         }
        //     }
        //     // getting travelmates for place
        //     $travelmates = TravelMatesRequests::whereHas('plan_place', function($q) use ($place) {
        //                 $q->where('places_id', '=', $place->id);
        //             })
        //             ->pluck('users_id');
        //     if(count($travelmates)==0) {
        //         $travelmates = TravelMatesRequests::whereHas('plan_city', function($q) use ($place) {
        //                 $q->where('cities_id', '=', $place->city->id);
        //             })
        //             ->pluck('users_id');
        //     }
        //     $mates = User::whereIn('id', $travelmates)
        //         ->whereRaw('(name like "%'.$query.'%" or interests like "%'.$query.'%")')
        //         ->paginate(10);
        //     $data['search_mate']['count'] = $mates->total();
        //     $data['search_mate']['data'] = '';
        //     foreach($mates as $m) {
        //         $data['search_mate']['data'] .= '<div class="drop-row" style="cursor:pointer"><div class="img-wrap rounded place-img"><img src="'.check_profile_picture($m->profile_picture).'" alt="logo" style="width:60px;height:60px;min-width:60px!important;"></div><div class="drop-content place-content"><h4 class="content-ttl">' . $m->name . '</h4><p class="place-name"><span class="follow-tag"></span> <span>' . str_limit($m->interests, 30) . '</span></p></div></div>';
        //     }
        //     // getting reports for place
        //     $reports_ary = ReportsInfos::where('var', 'place')
        //             ->where('val', $place->id)
        //             ->pluck('reports_id')->toArray();
        //     if(count($reports_ary)==0) {
        //         $reports = Reports::where('cities_id', '=', $place->city->id)
        //             ->whereRaw('(title like "%'.$query.'%" or description like "%'.$query.'%")')
        //             ->paginate(10);
        //     } else {
        //         $reports = Reports::whereIn('id', $reports_ary)
        //             ->whereRaw('(title like "%'.$query.'%" or description like "%'.$query.'%")')
        //             ->paginate(10);
        //     }
        //     $data['search_report']['count'] = $reports->total();
        //     $data['search_report']['data'] = '';
        //     foreach ($reports AS $rp) {
        //         $data['search_report']['data'] .= '<div class="drop-row" style="cursor:pointer"><div class="img-wrap rounded place-img"><img src="'.check_profile_picture($rp->author->profile_picture).'" alt="logo" style="width:60px;height:60px;min-width:60px!important;"></div><div class="drop-content place-content"><h4 class="content-ttl">' . str_limit($rp->title, 30) . '</h4><p class="place-name"><span class="follow-tag">' . $rp->author->name . '</span> <span>' . str_limit($rp->description, 60) . '</span></p></div></div>';
        //     }
        //     // getting reviews for place
        //     $reviews = Reviews::where('places_id', $place->id)
        //         ->whereRaw('text like "%'.$query.'%"')
        //         ->paginate(10);
        //     $data['search_review']['count'] = $reviews->total();
        //     $data['search_review']['data'] = '';
        //     foreach( $reviews as $r ) {
        //         $img = $r->google_profile_photo_url ? $r->google_profile_photo_url : check_profile_picture($r->author->profile_picture);
        //         $name = $r->google_author_name ? $r->google_author_name : $r->author->name;
        //         $data['search_review']['data'] .= '<div class="drop-row" style="cursor:pointer"><div class="img-wrap rounded place-img"><img src="'.$img.'" alt="logo" style="width:60px;height:60px;min-width:60px!important;"></div><div class="drop-content place-content"><h4 class="content-ttl">' . $name . '</h4><p class="place-name"><span class="follow-tag"></span> <span>' . str_limit($r->text, 30) . '</span></p></div></div>';
        //     }
        //     //getting trip plans for place
        //     $trip_plans = TripPlaces::where('places_id', $place->id)->pluck('trips_id')->toArray();
        //     if ( count($trip_plans) > 0) {
        //         $trips = TripPlans::whereIn('id', $trip_plans)
        //             ->whereRaw('title like "%'.$query.'%"')
        //             ->paginate(10);
        //         $data['search_plan']['count'] = $trips->total();
        //         $data['search_plan']['data'] = '';
        //         foreach ($trips AS $tr) {
        //             $img = 'https://maps.googleapis.com/maps/api/staticmap?center='.$tr->places[0]->lat.','.$tr->places[0]->lng.'&zoom=8&size=60x60&scale=2&maptype=satellite&markers=%7Clabel:T%7C'.$tr->places[0]->lat.','.$tr->places[0]->lng.'&key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
        //             $data['search_plan']['data'] .= '<div class="drop-row" style="cursor:pointer"><div class="img-wrap place-img"><img src="'.$img.'" alt="logo" style="width:60px;height:60px;min-width:60px!important;"></div><div class="drop-content place-content"><h4 class="content-ttl">' . str_limit($tr->title, 30) . '</h4><p class="place-name"><span class="follow-tag">' . $tr->author->name . '</span> <span></span></p></div></div>';
        //         }
        //     }
        //     return json_encode($data);
        // }
    }

    public function get64Image(Request $request)
    {
        $url = $request->url;
        return "data:image/png;base64," . base64_encode(file_get_contents($url));
    }

    public function discussion_replies_24hrs($place_id, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->discussion_replies_24hrs($place_id, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->discussion_replies_24hrs($place_id, $request);

        $place = Place::find($place_id);
        $user_id = $request->get('user_id');

        $filters = View::make('site.place2.partials._posts_filters_block');
        $output = $filters->render();

        $output = '';

        $posts_discussion_collection = array();
        $get_posts_discussion_collection = DiscussionReplies::whereHas('discussion', function ($q) use ($place_id) {
            $q->where('destination_type', 'place')
                ->where('destination_id', $place_id);
        })
            ->where('created_at', '>', date("Y-m-d", strtotime('-24 hours')))
            ->where('users_id', $user_id)
            ->orderBy('id', 'DESC')
            ->get();
        if (count($get_posts_discussion_collection) > 0) {
            foreach ($get_posts_discussion_collection as $gpdc) {
                $posts_discussion_collection[] = array(
                    'id' => $gpdc->discussions_id,
                    'type' => 'discussion',
                    'timestamp' => $gpdc->created_at
                );

                $view = View::make('site.place2.partials.post_discussion', [
                    'place' => $place,
                    'discussion' => $gpdc->discussion
                ]);
                $output .= $view->render();
            }
        } else {
            $output = 'no';
        }
        return json_encode($output);
    }

    public function newsfeed_show_all($place_id, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->newsfeed_show_all($place_id, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->newsfeed_show_all($place_id, $request);

        $place = Place::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $is_parent = $request->get('is_parent'); 
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();
        $output = [];
        //$all_collection = collect_posts('place', $place_id, $page);
        $filter = $request->all();
        $all_collection  = getPostsIdForAllFeed($place_id, $page, 'place', $filter);

        if (count($all_collection) == 0) {
           
            if(!isset($is_parent)){
                $page =1;
                $view = View::make('site.place2.partials.parent_banner',['is_parent'=>true,'text'=>'No more content to show for '.$place->transsingle->title.'. Displaying content for '.$place->city->transsingle->title.' instead.']);
                $output .=$view->render();
            }

            $all_collection  = getPostsIdForAllFeed($place->city->id, $page, 'city', $filter);
        }
        if(count($all_collection)>0){
            $postsId = array_map(function($post) {
                return $post = $post['id'];
            }, $all_collection);

            $show_post = Posts::whereIn('id', $postsId)->select('id', 'users_id', 'permission')->get()->toArray();
            foreach ($all_collection as $ppost) {
                $post_permission = [];
                foreach($show_post as $post) {
                    if($post['id'] == $ppost['id']) {
                        $post_permission = $post;
                    }
                }
                
                if (Auth::check() && isset($post_permission['permission']) && $post_permission['permission'] == Posts::POST_PERMISSION_PRIVATE && $post_permission['users_id'] != Auth::user()->id) {
                    continue;
                } elseif (Auth::check() && isset($post_permission['permission']) && $post_permission['permission'] == Posts::POST_PERMISSION_FRIENDS_ONLY && $post_permission['users_id'] != Auth::user()->id and !in_array($post_permission['users_id'], getUserFriendsIds())) {
                    continue;
                }
                $more = '';

                if ($ppost['type'] == 'regular') {

                    // $post = Posts::find($ppost['id']);
                    $more = View::make('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $ppost['id']]));

                    // if(is_object($post)){
                    //     if ($post->text != '' && count($post->medias) > 0) {
                    //         $more = View::make('site.place2.partials.post_media_with_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));
                    //     } elseif ($post->text == '' && count($post->medias) > 0) {
                    //         if(!is_object($post->author)){
                    //             if(isset($post->medias[0]->media))
                    //                 $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));
                    //         }else
                    //             $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));

                    //     } else {
                    //         $more = View::make('site.place2.partials.post_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));

                    //     }
                    // }
                } elseif ($ppost['type'] == 'discussion') {
                    // $discussion = Discussion::find($ppost['id']);
                    $view = View::make('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $ppost['id']]));

                    // if(is_object($discussion))
                    //     $more = View::make('site.place2.partials.post_discussion', array('discussion' => $discussion, 'place' => $place));
                } elseif ($ppost['type'] == 'plan') {
                    $more = View::make('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $ppost['id']]));
                } elseif ($ppost['type'] == 'report') {

                    $more = View::make('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $ppost['id']]));

                    // if((isset($filter['location']) || isset($filter['people']))){
                    //     $followers = getUsersByFilters($filter);
                    //     $report = Reports::whereId($ppost['id'])->whereIn('users_id',$followers)->first();
                    // }else{
                    //     $report = Reports::find($ppost['id']);
                    // }
                    // if (is_object($report)) {
                    //     $more = View::make('site.place2.partials.post_report', array('report' => $report, 'place' => $place));
                    // }
                } elseif ($ppost['type'] == 'review') {
                    $more = View::make('site.place2.new.review', array('post' => ['type' => 'Review', 'variable' => $ppost['id']]));

                    // $reviews = Reviews::find($ppost['id']);
                    // if(is_object($reviews))
                    //     $more = View::make('site.place2.partials.post_review', ['place' => $place,'review' => $reviews]);
                }
                $timestamp = (new Carbon($ppost['timestamp']))->timestamp;
                if (!array_key_exists($timestamp, $output)) {
                    $output[$timestamp] = '';
                }
                if (isset($more) && $more != '')
                    $output[$timestamp] .= $more->render();
                //dd($output);
            }
        }

        //** dont append in the series  */
        // foreach ($this->newsfeed_show_travelmates($place_id, $request, true) as $key => $travelmates) {
        //     $output[$key] = $travelmates;
        // }
        // if (!isset($filter['q']) && !isset($filter['poeple']) && !isset($filter['location'])) {
        //     foreach ($this->newsfeed_show_events($place_id, $request, true) as $key => $events) {
        //         $output[$key] = $events;
        //     }
        // }

        // foreach ($this->newsfeed_show_reports($place_id, $request, true) as $key => $reports) {
        //     $output[$key] = $reports;
        // }

        // foreach ($this->newsfeed_show_reviews($place_id, $request, true) as $key => $reviews) {
        //     $output[$key] = $reviews;
        // }

        krsort($output);

        $output = implode($output);
        if (!$output) {
            $output = 'no';
        }else {
           
            $secondary  = $this->getSecondary( $place ,'place');
            $output .=$secondary;

        }
        // dd($output);
        // $this->utf8_encode_deep($output);
        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_about($place_id, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->newsfeed_show_about($place_id, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->newsfeed_show_about($place_id, $request);
    }

    public function newsfeed_show_discussions($place_id, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->newsfeed_show_discussions($place_id, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->newsfeed_show_discussions($place_id, $request);

        $place = Place::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $is_parent_flag = $request->get('is_parent');
        $skip = ($page - 1) * 5;
        $filter = $request->all();
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();
        $parent_indicator = 0 ;
        $output = '';
        $get_posts_discussions_collection = '';
        if (isset($filter['location']) || isset($filter['people']) || isset($filter['q']) || isset($filter['sort'])) {
            if (isset($filter['location']) || isset($filter['people']))
                $followers = getUsersByFilters($filter);
            else
                $followers = [];
            $query_search = isset($filter['q']) ? $filter['q'] : "";
            $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
                ->where('destination_type', 'place')
                ->where(function ($get_posts_discussions_collection) use ($query_search) {
                    if (isset($query_search)) {
                        $get_posts_discussions_collection->where('question', 'like', '%' . $query_search . '%');
                    }
                })
                ->orderBy('id', 'desc')
                ->where(function ($get_posts_discussions_collection) use ($followers) {
                    if (!empty($followers)) {
                        $get_posts_discussions_collection->whereIn('users_id', $followers);
                    }
                })
                ->skip($skip)
                ->take(5)
                ->get();
        } else {
            $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
            ->where('destination_type','place')
                ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '" ) as replies')
                ->orderBy('replies', 'DESC')
                ->offset($skip)
                ->limit(5)
                ->get();
                if(count($get_posts_discussions_collection) == 0){
                    $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
                    ->where('destination_type','place')
                    ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id` and created_at <  "' . Carbon::now()->subDays(7) . '" ) as replies')
                    ->offset($skip)
                    ->limit(5)
                    ->get();
                }
                // for parent level posts

                if(count($get_posts_discussions_collection) == 0){
                    if(!isset($is_parent_flag)){
                        $parent_indicator =1;
                        $skip =0;
                    }else {
                        $parent_indicator =0;
                    }
                    $get_posts_discussions_collection = Discussion::where('destination_id',$place->city->id)
                    ->where('destination_type','city')
                    ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id` ) as replies')
                    ->orderBy('replies', 'DESC')
                    ->offset($skip)
                    ->limit(5)
                    ->get();
                }

            // $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
            //         ->where('destination_type', 'place')
            //         ->orderBy('id', 'desc')
            //         ->skip($skip)
            //         ->take(5)
            //         ->get();
            
        }
        if (count($get_posts_discussions_collection) > 0) {
            if($parent_indicator){
                $view = View::make('site.place2.partials.parent_banner',['is_parent'=>true,'text'=>'No more content to show for '.$place->transsingle->title.'. Displaying content for '.$place->city->transsingle->title.' instead.']);
                $output .= $view->render();
            }
            foreach ($get_posts_discussions_collection as $gpdc) {

                $view = View::make('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $gpdc->id]));



                // $view = View::make('site.place2.partials.post_discussion', ['place' => $place,
                //             'discussion' => $gpdc]);

                $output .= $view->render();
            }
            $secondary  = $this->getSecondary( $place ,'place');
            $output .=$secondary;
        } else {
            $output = 'no';
        }

        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_top($place_id, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->newsfeed_show_top($place_id, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->newsfeed_show_top($place_id, $request);

        $place = Place::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $is_parent = $request->get('is_parent');
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        $filter = $request->all();
        $all_collection = collect_posts('place', $place_id, $page, true, false, $is_parent);

        if (count($all_collection) == 0) {
           
            if(!isset($is_parent)){
                $page =1;
                $view = View::make('site.place2.partials.parent_banner',['is_parent'=>true,'text'=>'No more content to show for '.$place->transsingle->title.'. Displaying content for '.$place->city->transsingle->title.' instead.']);
                $output .=$view->render();
            }

            $all_collection  =  collect_posts('city', $place->city->id, $page, true, false, $filter);
        }
        if (count($all_collection) > 0) {
            foreach ($all_collection as $ppost) {
                $more = '';

                if ($ppost['type'] == 'regular') {

                    $more = View::make('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $ppost['id']]));

                    // $post = Posts::find($ppost['id']);
                    // if ($post->text != '' && count($post->medias) > 0) {
                    //     $more = View::make('site.place2.partials.post_media_with_text', array('post' => $post, 'place' => $place));
                    // } elseif ($post->text == '' && count($post->medias) > 0) {
                    //     if(!is_object($post->author)){
                    //         if(isset($post->medias[0]->media))
                    //             $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));
                    //     }else
                    //         $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));
                    // } else {
                    //     $more = View::make('site.place2.partials.post_text', array('post' => $post, 'place' => $place));
                    // }
                } elseif ($ppost['type'] == 'discussion') {
                    $view = View::make('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $ppost['id']]));

                    // $discussion = Discussion::find($ppost['id']);
                    // $more = View::make('site.place2.partials.post_discussion', array('discussion' => $discussion, 'place' => $place));
                } elseif ($ppost['type'] == 'plan') {
                    $plan = Tripplans::find($ppost['id']);
                    if ($plan) {
                        // try {
                        if (isset($filter['q'])) {
                            if (strpos(strtolower($plan->title), strtolower($filter['q'])) !== false) {
                                $more = View::make('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $plan->id]));

                                // $more = View::make('site.place2.partials.post_plan', array('plan' => $plan->trip, 'place' => $place));
                            }
                        } else {
                            $more = View::make('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $plan->id]));

                            // $more = View::make('site.place2.partials.post_plan', array('plan' => $plan->trip, 'place' => $place));
                        }
                        // } catch (\Throwable $e) {
                        //     dd($e->getMessage());
                        // }
                    }
                } elseif ($ppost['type'] == 'report') {
                    $report = Reports::find($ppost['id']);
                    if ($report) {
                        if (isset($filter['q'])) {
                            if (strpos(strtolower($report->title), strtolower($filter['q'])) !== false) {
                                $more = View::make('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $report->id]));
                            }
                        } else
                            $more = View::make('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $report->id]));
                    }
                }
                if ($more != '')
                    $output .= $more->render();
            }
        }

        // $nf_travelmates = $this->newsfeed_show_travelmates($place_id, $request, true, true);
        // foreach ($nf_travelmates as $nf_item) {
        //     $output .= $nf_item;
        // }

        $nf_events = $this->newsfeed_show_events($place_id, $request, true);
        foreach ($nf_events as $nf_item) {
            $output .= $nf_item;
        }

        // $nf_reports = $this->newsfeed_show_reports($place_id, $request, true, true);
        // foreach($nf_reports as $nf_item){
        //     $output .= $nf_item;
        // }

        $nf_reviews = $this->newsfeed_show_reviews($place_id, $request, true, true);
        foreach($nf_reviews as $nf_item){
            $output .= $nf_item;
        }

        if ($output == '') {
            $output = 'no';
        }else{
            $secondary  = $this->getSecondary( $place ,'place');
            $output .=$secondary;
        }

        // $this->utf8_encode_deep($output);
        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_media($place_id, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->newsfeed_show_media($place_id, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->newsfeed_show_media($place_id, $request);

        $place = Place::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        $filter = $request->all();
        $all_collection = collect_posts('place', $place_id, $page, false, true, $filter);
        if (count($all_collection) > 0) {
            foreach ($all_collection as $ppost) {

                if ($ppost['type'] == 'regular') {
                    $more = View::make('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $ppost['id']]));
                    $output .= $more->render();
                    // $post = Posts::find($ppost['id']);
                    // if ($post->text != '' && count($post->medias) > 0) {
                    //     $more = View::make('site.place2.partials.post_media_with_text', array('post' => $post));
                    //     $output .= $more->render();
                    // } elseif ($post->text == '' && count($post->medias) > 0) {
                    //     if(!is_object($post->author)){
                    //         if(isset($post->medias[0]->media))
                    //             $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));
                    //     }else
                    //         $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));

                    //     $output .= $more->render();
                    // } else {

                    // }
                }
            }
        } else {
            $output = 'no';
        }

        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_tripplan($place_id, Request $request)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->newsfeed_show_tripplan($place_id, $request);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->newsfeed_show_tripplan($place_id, $request);

        $place = Place::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();
        $is_parent = 0;
        $output = '';
        $filter = $request->all();
        $triplist = [];
        if (isset($filter['location']) || isset($filter['people']) || isset($filter['q']) || isset($filter['sort'])) {
            if (isset($filter['location']) || isset($filter['people']))
                $triplist = get_triplist4me($filter, $place_id, 'place');
            else
                $triplist = get_triplist4me(false, $place_id, 'place');
        } else {
            $triplist = get_triplist4me(false, $place_id, 'place');
        }

        if (count($triplist) == 0) {
            
            if (isset($filter['location']) || isset($filter['people']) || isset($filter['q']) || isset($filter['sort'])) {
                if (isset($filter['location']) || isset($filter['people'])){
                    $triplist = get_triplist4me($filter, $place->city->id, 'city');
                    $is_parent =1;
                }
                else{
                    $triplist = get_triplist4me(false, $place->city->id, 'city');
                    $is_parent =1;
                }
            } else {
                $is_parent =1;
                $triplist = get_triplist4me(false, $place->city->id, 'city');
            }
        }

        // $get_posts_plans_collection = TripPlaces::where('places_id', $place_id)
        //             ->whereIn('trips_id', $triplist)
        //             ->orderBy('id', 'DESC')
        //             ->paginate(5);
        // ->skip($skip)
        // ->take(5)
        // ->get();
        $get_posts_plans_collection = Tripplans::leftjoin('trips_places', 'trips_places.trips_id', '=', 'trips.id')
            ->selectRaw('distinct(trips.id) as id, ((select count(*) from `posts_comments` where `posts_id`=`trips`.`id` and type="trip"  and created_at >  "' . Carbon::now()->subDays(7) . '") 
                    + (select count(*) from `trips_likes` where `trips_id`=`trips`.`id`  and created_at >  "' . Carbon::now()->subDays(7) . '") 
                    + (select count(*) from `posts_shares` where `posts_type_id`=`trips`.`id` and type="trip"  and created_at >  "' . Carbon::now()->subDays(7) . '")) as ranks')
            ->whereIn('trips_id', $triplist)
            ->orderBy('ranks', 'DESC')
            ->groupBy('trips.id')
            ->paginate(5)->pluck('id')->toArray();


        if (count($get_posts_plans_collection ) == 0) {
                $get_posts_plans_collection = Tripplans::leftjoin('trips_places', 'trips_places.trips_id', '=', 'trips.id')
                ->selectRaw('distinct(trips.id) as id, ((select count(*) from `posts_comments` where `posts_id`=`trips`.`id` and type="trip"  and created_at <  "' . Carbon::now()->subDays(7) . '") 
                        + (select count(*) from `trips_likes` where `trips_id`=`trips`.`id`  and created_at <  "' . Carbon::now()->subDays(7) . '") 
                        + (select count(*) from `posts_shares` where `posts_type_id`=`trips`.`id` and type="trip"  and created_at <  "' . Carbon::now()->subDays(7) . '")) as ranks')
                ->whereIn('trips_id', $triplist)
                ->orderBy('ranks', 'DESC')
                ->groupBy('trips.id')
                ->paginate(5)->pluck('id')->toArray();  
        }

        $get_posts_plans_collection = Tripplans::whereIn('id', $get_posts_plans_collection)->get();
        if ($get_posts_plans_collection->count() > 0) {
            if($is_parent == 1){
                $view = View::make('site.place2.partials.parent_banner',['is_parent'=>true,'text'=>'Trip plans for '.$place->city->transsingle->title]);
                $output .= $view->render();
            }
            foreach ($get_posts_plans_collection as $gpdc) {
                if (isset($filter['q'])) {
                    if (strpos(strtolower($gpdc->title), strtolower($filter['q'])) !== false) {
                        $view = View::make('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $gpdc->id]));
                        // $view = View::make('site.place2.partials.post_plan', ['place' => $place,
                        // 'plan' => $gpdc->trip]);
                        $output .= $view->render();
                    }
                } else {
                        $view = View::make('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $gpdc->id]));
                    // $view = View::make('site.place2.partials.post_plan', ['place' => $place,
                    // 'plan' => $gpdc->trip]);
                    $output .= $view->render();
                }
            }
        } else {
            $output = 'no';
        }
        if ($output == ''){
            $output = 'no';
        }else{
            $secondary  = $this->getSecondary( $place ,'place');
            $output .=    $secondary;
        }
            return json_encode(['html'=>$output]);
        }

    public function newsfeed_show_travelmates($place_id, Request $request, $internal = false, $top = false)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->newsfeed_show_travelmates($place_id, $request, $internal, $top);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->newsfeed_show_travelmates($place_id, $request, $internal, $top);

        $place = Place::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        $output = '';
        $followers = [];
        $filter = $request->all();
        $get_posts_mates_collection =  [];
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();
        if (isset($filter['location']) || isset($filter['people'])) {
            $followers = getUsersByFilters($filter);
            $get_posts_mates_collection = TravelMatesRequests::whereHas('plan_place', function ($q) use ($place) {
                $q->where('places_id', '=', $place->id);
            })
                ->whereIn('users_id', $followers)
                ->groupBy('users_id')
                ->orderBy('id', 'desc')
                ->paginate(5);
        } else {
            $get_posts_mates_collection = TravelMatesRequests::whereHas('plan_place', function ($q) use ($place) {
                $q->where('places_id', '=', $place->id);
            })
                ->groupBy('users_id')
                ->orderBy('id', 'desc')
                ->paginate(5);
        }



        // ->skip($skip)
        // ->take(5)
        // ->get();
        // if ($get_posts_mates_collection->total() == 0) {
        //     $get_posts_mates_collection = TravelMatesRequests::whereHas('plan_city', function($q) use ($place) {
        //                 $q->where('cities_id', '=', $place->city->id);
        //             })
        //             ->groupBy('users_id')
        //             ->orderBy('id', 'desc')
        //             ->paginate(5);
        //     // ->skip($skip)
        //     // ->take(5)
        //     // ->get();
        // }
        $internalOutput = [];

        if ($get_posts_mates_collection->count() > 0) {
            foreach ($get_posts_mates_collection as $gpdc) {
                $view = View::make(
                    'site.place2.partials.post_travelmate',
                    [
                        'place' => $place,
                        'travelmate_request' => $gpdc
                    ]
                );

                if (!array_key_exists($gpdc->created_at->timestamp, $internalOutput)) {
                    $internalOutput[$gpdc->created_at->timestamp] = '';
                }

                $internalOutput[$gpdc->created_at->timestamp] .= $view->render();
                $output .= $view->render();
            }
            krsort($internalOutput);
        } else {
            $output = $internal ? '' : 'no';
        }

        return $internal ? $internalOutput : json_encode($output);
    }

    public function newsfeed_show_events($place_id, Request $request, $internal = false)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->newsfeed_show_events($place_id, $request, $internal);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->newsfeed_show_events($place_id, $request, $internal);

        $place = Place::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;

        $output = '';

        $get_posts_events_collection = Events::where('places_id', $place_id)
            ->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')
            ->orderBy('id', 'desc')
            ->skip($skip)
            ->take(5)
            ->get();

        $internalOutput = [];
        if (count($get_posts_events_collection) > 0) {
            foreach ($get_posts_events_collection as $gpec) {
                $view = View::make('site.home.partials.post_event', [
                    'place' => $place,
                    'post' => $gpec, 'evt' => $gpec, 'me' => Auth::user()
                ]);
                $output .= $view->render();
                if (!array_key_exists($gpec->created_at->timestamp, $internalOutput)) {
                    $internalOutput[$gpec->created_at->timestamp] = '';
                }
                $internalOutput[$gpec->created_at->timestamp] .= $view->render();
            }

            krsort($internalOutput);
        } else {
            $output = $internal ? '' : 'no';
        }

        return $internal ? $internalOutput :json_encode(['html'=>$output]);
        ;
    }

    public function newsfeed_show_reports($place_id, Request $request, $internal = false, $top = false)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->newsfeed_show_reports($place_id, $request, $internal, $top);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->newsfeed_show_reports($place_id, $request, $internal, $top);

        $place = Place::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();
        $output = '';
        $flag = false;
        $filter = $request->all();

        if (!$top) {
            

            if (isset($filter['location']) || isset($filter['people']) || isset($filter['q']) || isset($filter['sort'])) {
                if (isset($filter['location']) || isset($filter['people']))
                    $followers = getUsersByFilters($filter);
                else
                    $followers = [];

                $query_search = isset($filter['q']) ? $filter['q'] : "";
                $followers = getUsersByFilters($filter);
                $get_posts_reports_collection = ReportsInfos::query()
                    ->whereHas('report', function ($q) use ($place_id, $followers) {
                        $q->whereNotNull('published_date')
                            ->where('flag', 1)
                            ->whereHas('places', function ($_q) use ($place_id) {
                                $_q->where('places_id', $place_id);
                            });

                        if (!empty($followers))
                            $q->whereIn('users_id', $followers);
                    })->groupBy('reports_id')->paginate(5);
            } else {
                $get_posts_reports_collection = ReportsInfos::query()
                    ->whereHas('report', function ($q) use ($place_id) {
                        $q->whereNotNull('published_date')
                            ->where('flag', 1)
                            ->whereHas('places', function ($_q) use ($place_id) {
                                $_q->where('places_id', $place_id);
                            });
                    })->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`reports_infos`.`reports_id` and type= "report"and created_at >  "' . Carbon::now()->subDays(7) . '") 
                                    + (select count(*) from `reports_likes` where `reports_id`=`reports_infos`.`reports_id` and created_at >  "' . Carbon::now()->subDays(7) . '") 
                                    + (select count(*) from `posts_shares` where `posts_type_id`=`reports_infos`.`reports_id`  and type="report" and created_at >  "' . Carbon::now()->subDays(7) . '")) as rank')
                    ->orderBy('rank', 'DESC')
                    ->groupBy('reports_id')
                    ->paginate(5);
                    if($get_posts_reports_collection->count() > 0 ){
                        $get_posts_reports_collection = ReportsInfos::query()
                        ->whereHas('report', function ($q) use ($place_id) {
                            $q->whereNotNull('published_date')
                                ->where('flag', 1)
                                ->whereHas('places', function ($_q) use ($place_id) {
                                    $_q->where('places_id', $place_id);
                                });
                        })->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`reports_infos`.`reports_id` and type= "report"and created_at <  "' . Carbon::now()->subDays(7) . '") 
                                        + (select count(*) from `reports_likes` where `reports_id`=`reports_infos`.`reports_id` and created_at < "' . Carbon::now()->subDays(7) . '") 
                                        + (select count(*) from `posts_shares` where `posts_type_id`=`reports_infos`.`reports_id`  and type="report" and created_at <  "' . Carbon::now()->subDays(7) . '")) as rank')
                        ->orderBy('rank', 'DESC')
                        ->groupBy('reports_id')
                        ->paginate(5);
                    }
            }

            // ->skip($skip)
            // ->take(5)
            // ->get();
            if ($get_posts_reports_collection->count()  ==  0  && (!isset($filter['location']) && !isset($filter['people']))) {
                $flag = true;
                $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($place) {
                    $q->where('location_type', Reports::LOCATION_CITY)
                        ->where('location_id', $place->city->id);
                })->paginate(5);
                // ->skip($skip)
                // ->take(5)
                // ->get();
            }
        } else {
            $get_posts_reports_collection = ReportsInfos::query()
                ->whereHas('report', function ($q) use ($place_id) {
                    $q->whereNotNull('published_date')
                        ->where('flag', 1)
                        ->whereHas('places', function ($_q) use ($place_id) {
                            $_q->where('places_id', $place_id);
                        });
                })->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`reports_infos`.`reports_id` and type= "report") 
                                    + (select count(*) from `reports_likes` where `reports_id`=`reports_infos`.`reports_id`) 
                                    + (select count(*) from `reports_views` where `reports_id`=`reports_infos`.`reports_id`) 
                                    + (select count(*) from `posts_shares` where `posts_type_id`=`reports_infos`.`reports_id`  and type="report")) as rank')
                ->orderBy('rank', 'DESC')
                ->groupBy('reports_id')
                ->paginate(5);
            // ->skip($skip)
            // ->take(5)
            // ->get();
            if ($get_posts_reports_collection->total() == 0) {
                $flag = true;
                $get_posts_reports_collection = Reports::query()
                    ->whereHas('location', function ($q) use ($place) {
                        $q->where('location_type', Reports::LOCATION_CITY)
                            ->where('location_id', $place->city->id);
                    })
                    ->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`reports`.`id` and type ="report") 
                                        + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id`) 
                                        + (select count(*) from `reports_views` where `reports_id`=`reports`.`id`) 
                                        + (select count(*) from `posts_shares` where `posts_type_id`=`reports`.`id` and type="report")) as rank')
                    ->orderBy('rank', 'DESC')
                    ->paginate(5);
                // ->skip($skip)
                // ->take(5)
                // ->get();
            }
        }

        $internalOutput = [];

        if ($get_posts_reports_collection->count() > 0) {
            if($flag == 1){
                $view = View::make('site.place2.partials.parent_banner',['is_parent'=>true,'text'=>'No more content to show for '.$place->transsingle->title.'. Displaying content for '.$place->city->transsingle->title.' instead.']);
                $output .= $view->render();
            }
            foreach ($get_posts_reports_collection as $gpdc) {
                $report = $flag ? $gpdc : $gpdc->report;

               

                if (isset($filter['q'])) {
                    if (isset($report->title) && strpos(strtolower($report->title), strtolower($query_search)) !== false) {
                        $view = View::make('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $report->id]));

                        // $view = View::make('site.place2.partials.post_report', ['place' => $place,'report' => $report]);
                        $output .= $view->render();
                        if (!array_key_exists($report->created_at->timestamp, $internalOutput)) {
                            $internalOutput[$report->created_at->timestamp] = '';
                        }
                        $internalOutput[$report->created_at->timestamp] .= $view->render();
                    }
                } else {
                    $view = View::make('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $report->id]));


                    // $view = View::make('site.place2.partials.post_report', ['place' => $place,
                    //             'report' => $report]);
                    $output .= $view->render();
                    if (!array_key_exists($report->created_at->timestamp, $internalOutput)) {
                        $internalOutput[$report->created_at->timestamp] = '';
                    }
                    $internalOutput[$report->created_at->timestamp] .= $view->render();
                }
            }

            krsort($internalOutput);
            $secondary  = $this->getSecondary( $place ,'place');
            $output .=    $secondary;
        } else {
            $output = $internal ? '' : 'no';
        }

        // $this->utf8_encode_deep($output);
        /// dd($output);
        return $internal ? $internalOutput :  json_encode(['html'=>$output]);
        ;
    }

    public function newsfeed_show_reviews($place_id, Request $request, $internal = false, $top = false)
    {
        session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

        if (strpos(session('place_country'), 'country') !== false)
            return app('App\Http\Controllers\CountryController2')->newsfeed_show_reviews($place_id, $request, $internal, $top);

        if (strpos(session('place_country'), 'city') !== false)
            return app('App\Http\Controllers\CityController')->newsfeed_show_reviews($place_id, $request, $internal, $top);

        $place = Place::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();
        $filter = $request->all();
        $output = '';
        if (!$top) {
            if (isset($filter['location']) || isset($filter['people']) || isset($filter['q']) || isset($filter['sort'])) {
                if (isset($filter['location']) || isset($filter['people']))
                    $followers = getUsersByFilters($filter);
                else
                    $followers = [];
                $query_search = isset($filter['q']) ? $filter['q'] : "";
                $get_posts_review_collection = Reviews::where('places_id', $place_id)
                    ->orderBy('id', 'desc')
                    ->where(function ($get_posts_review_collection) use ($query_search) {
                        if (isset($query_search)) {
                            $get_posts_review_collection->where('text', 'like', '%' . $query_search . '%');
                        }
                    })
                    ->where(function ($get_posts_review_collection) use ($followers) {
                        if (!empty($followers))
                            $get_posts_review_collection->whereIn('users_id', $followers);
                    })
                    ->skip($skip)
                    ->take(5)
                    ->get();
            } else {
                $get_posts_review_collection = Reviews::where('places_id', $place_id)
                    ->selectRaw('* , ((select count(*) from `reviews_votes` where `review_id`=`reviews`.`id` and `vote_type`=1) 
                    + (select count(*) from `posts_shares` where `posts_type_id`=`reviews`.`id` and type ="review")) as rank')
                    ->orderBy('rank', 'desc')
                    ->offset($skip)
                    ->limit(5)
                    ->get();
                // $get_posts_review_collection = Reviews::where('places_id', $place_id)
                //     ->whereNotIn('by_users_id', blocked_users_list())
                //     ->orderBy('id', 'desc')
                //     ->skip($skip)
                //     ->take(5)
                //     ->get();
            }
        } else {
            $get_posts_review_collection = Reviews::where('places_id', $place_id)
                ->selectRaw('*, ((select count(*) from `reviews_votes` where `review_id`=`reviews`.`id` and `vote_type`=1) 
                                    + (select count(*) from `reviews_shares` where `review_id`=`reviews`.`id`)) as rank')
                ->orderBy('rank', 'desc')
                ->skip($skip)
                ->take(1)
                ->get();
        }
        $internalOutput = [];

        if (count($get_posts_review_collection) > 0) {
            foreach ($get_posts_review_collection as $gpdc) {
                // $view = View::make('site.place2.partials.post_review', ['place' => $place,
                //             'review' => $gpdc]);
                $view = View::make('site.place2.new.review', array('post' => ['type' => 'Review', 'variable' => $gpdc->id]));


                $output .= $view->render();

                if (!array_key_exists($gpdc->created_at->timestamp, $internalOutput)) {
                    $internalOutput[$gpdc->created_at->timestamp] = '';
                }

                $internalOutput[$gpdc->created_at->timestamp] .= $view->render();
            }
            krsort($internalOutput);
        } else {
            $output = $internalOutput ? '' : 'no';
        }

        return $internal ? $internalOutput :  json_encode(['html'=>$output]);
        ;
    }

    public function getCommentLikeUsers(Request $request)
    {
        $comment_id = $request->comment_id;
        $comment = EventsComments::find($comment_id);


        if ($comment->likes) {
            return view('site.home.partials.modal_comments_like_block', array('likes' => $comment->likes()->orderBy('created_at', 'DESC')->get()));
        }
    }

    /**
     * @param Request $request
     * @param Reviews $review
     * @return JsonResponse
     */
    public function ajaxEditReview(Request $request, Reviews $review)
    {
        $data['success'] = false;

        if ($review->author->getKey() !== \Auth::id()) {
            return new JsonResponse($data, 403);
        }

        $review->text = $request->input('text');
        $review->save();

        $data['success'] = true;

        return new JsonResponse($data);
    }

    /**
     * @param Request $request
     * @param Reviews $review
     * @return JsonResponse
     */
    public function ajaxDeleteReview(Request $request, Reviews $review)
    {
        $data['success'] = false;

        if ($review->author->getKey() !== \Auth::id()) {
            return new JsonResponse($data, 403);
        }

        try {
            $review->delete();
        } catch (\Exception $e) {
            return new JsonResponse($data);
        }

        $data['success'] = true;

        return new JsonResponse($data);
    }

    /**
     * @param string $providerId given by google API
     * Add place from google provider ID to our system
     * @return int $placeid
     */


    private function addNewPlaceByProviderId($providerId, $addr)
    {
        $geo_link = 'https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
            . '&place_id=' . urlencode($providerId);
        $fetch_geo_link = file_get_contents($geo_link);
        $geo_result = json_decode($fetch_geo_link);
        $geo_city = @$geo_result->result->address_components;
        $geo_city_guess = '';
        $geo_country = '';
        $geo_state = '';
        $country_code = '';
        $city_found_flag = false;
        if (isset($geo_city)) {
            foreach ($geo_city as $gc) {
                if (isset($gc->types) && !empty($gc->types)) {
                    if (($gc->types[0] == 'locality' ||  $gc->types[0] == 'administrative_area_level_3') && !$city_found_flag) {
                        $city_found_flag = true;
                        $geo_city_guess = $gc->long_name;
                    }
                    if ($gc->types[0] == 'country') {
                        $country_code = $gc->short_name;
                        $geo_country = $gc->long_name;
                    }
                    if ($gc->types[0] == 'administrative_area_level_1') {
                        $geo_state = $gc->long_name;
                    }
                }
            }
        }
        if (isset($geo_city_guess) && !empty($geo_city_guess)) {
            $get_city = Cities::whereHas('transsingle', function ($query) use ($geo_city_guess) {
                $query->where('title', 'like', '%' . $geo_city_guess . '%');
            })->get()->first();
        }

        $p = new Place();
        $p->place_type = @join(",", $geo_result->result->types);
        $p->safety_degrees_id = 1;
        $p->provider_id = $providerId;

        if (isset($get_city) && is_object($get_city)) {

            $p->countries_id = $get_city->countries_id;
            $p->cities_id = $get_city->id;
            $p->states_id = $get_city->states_id;
        } else {
            $p->cities_id = '2031';
            $p->countries_id = '373';
            $p->states_id  = '0';

            // $geo_country_info=  Countries::whereHas('transsingle', function ($query) use ( $geo_country) {
            //     $query->where('title','like', '%'.$geo_country.'%');
            // })->get()->first();

            // $country_id =326;
            // // check if country exists or else add new 
            // if(isset($geo_country_info)){
            // $country_id  = $geo_country_info->id;
            // }else {
            //         $country  = new Countries();
            //         $country->lat = $geo_result->result->geometry->location->lat;
            //         $country->lng = $geo_result->result->geometry->location->lng;
            //         $country->iso_code = $country_code;
            //         $country->regions_id = 22;
            //         $country->safety_degree_id = 1;
            //         $country->active =1;
            //         if($country->save()){
            //             $country_id = $country->id;
            //             $country_trans=  new CountriesTranslations();
            //             $country_trans->countries_id =$country->id;
            //             $country_trans->title = $geo_country;
            //             $country_trans->languages_id = 1;
            //             $country_trans->save();
            //         }
            // }


            // //check if state exists or add new
            // if(isset($geo_state) && !empty($geo_state)){
            //     $state = States::whereHas('transsingle',function($query) use ($geo_state){
            //         $query->where('title','like', '%'.$geo_state.'%');
            //     })->get()->first();

            // }
            // $states_id =0;
            // if(isset($state) && is_object($state)){
            //     $states_id =  $state->id;
            // }else {
            //     $state  = new States();
            //     $state->lat = $geo_result->result->geometry->location->lat;
            //     $state->lng = $geo_result->result->geometry->location->lng;
            //     $state->countries_id  = $country_id;
            //     $state->active = 1;
            //     if($country_id != 227){
            //         $state->is_province =1;
            //     }
            //     if($state->save()){
            //         $states_id = $state->id;
            //         $state_trans=  new StateTranslation();
            //         $state_trans->states_id =$state->id;
            //         $state_trans->title = $geo_state;
            //         $state_trans->languages_id = 1;
            //         $state_trans->save();
            //     }
            // }
            // $city_id = 2031;
            // // add City as it wasnt added before
            // if(!empty($geo_city_guess)){
            //     $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'.'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($geo_city_guess);
            //     $fetch_link_for_city = file_get_contents($google_link_for_city);
            //     $query_result_for_city = json_decode($fetch_link_for_city);
            //     $qr_city = $query_result_for_city->results;

            //     $city  = new Cities();
            //     $city->lat = $qr_city[0]->geometry->location->lat;
            //     $city->lng = $qr_city[0]->geometry->location->lng;
            //     $city->countries_id  = $country_id;
            //     $city->states_id  = $states_id;
            //     $city->safety_degree_id  = 1;
            //     $city->level_of_living_id  = 0;
            //     $city->active  = 1;
            //     if($city->save()){
            //         $city_id = $city->id;
            //             $city_trans=  new CitiesTranslations();
            //             $city_trans->cities_id =$city->id;
            //             $city_trans->title = $geo_city_guess;
            //             $city_trans->languages_id = 1;
            //             $city_trans->save();
            //     }
            // }   
            // $p->countries_id =$country_id;
            // $p->states_id = $states_id;
            // $p->cities_id =$city_id;
        }
        $p->lat = $geo_result->result->geometry->location->lat;
        $p->lng = $geo_result->result->geometry->location->lng;
        $p->pluscode = @$geo_result->result->plus_code->compound_code;
        $p->rating = @$geo_result->result->rating;
        $p->active = 1;
        $p->auto_import = 1;
        $p->save();

        $pt = new \App\Models\Place\PlaceTranslations();
        $pt->languages_id = 1;
        $pt->places_id = $p->id;
        $pt->title = $geo_result->result->name;
        $pt->address = $geo_result->result->formatted_address;
        $pt->save();
        return $p->id;
    }
    public function getCitiesInfoFromGoogle(Request $request)
    {
        $page = $request->page;
        if ($page != 0)
            $page = $page * 50 + 1;

        $language_id = 1;
        $available_cities = Cities::with([
            'trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'countryTitle'
        ])
            ->limit(50)
            ->offset($page)
            ->get();
        $no_city_found = [];
        $not_a_city = [];
        $cities_without_title = [];
        $correctly_added = [];
        $final_results = array();

        foreach ($available_cities as $cities) {
            if (isset($cities->trans[0]->title)) {
                $query = $cities->trans[0]->title . ' in ' . $cities->countryTitle->title;
                $google_link = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
                    . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
                $google_link .= '&query=' . urlencode($query);
                $fetch_link = file_get_contents($google_link);

                $query_result = json_decode($fetch_link);
                $qr = $query_result->results;

                if (isset($qr) && count($qr) > 0) {
                    if ($qr[0]->types[0] == 'locality' || $qr[0]->types[0] == 'administrative_area_level_3') {
                        if (strcasecmp($qr[0]->name, $cities->trans[0]->title) != 0) {
                            $cities_differnt_name[] = ['id' => $cities->id, 'title' => $cities->trans[0]->title, 'google title' => $qr[0]->name, 'searched Query' => $google_link];
                        } else {
                            $correctly_added[] = ['id' => $cities->id, 'title' => $cities->trans[0]->title, 'searched Query' => $google_link];
                        }
                    } else {
                        $not_a_city[] = ['id' => $cities->id, 'title' => $cities->trans[0]->title, 'google level' => $qr[0]->types[0], 'searched Query' => $google_link];
                    }
                } else {
                    $no_city_found[] =  ['id' => $cities->id, 'title' => $cities->trans[0]->title, 'searched Query' => $google_link];
                }
            } else {
                $cities_without_title[]  = $cities->id;
            }
        }
        $final_result = [
            'correctly Added Cities' => $correctly_added,
            'Cities with different Title' => $cities_differnt_name,
            'DB Cities without Title' => $cities_without_title,
            'No City Found' => $no_city_found,
            'Google has a different level' => $not_a_city,

        ];
        dd($final_result);
    }
    public function getPlaceAboutInfo($place_id,$page=0)
    {
        $pagenum = Input::get('pagnum');
        $data = [];
        $place = Place::find($place_id);
        $countries_id = isset($place->countries_id) ? $place->countries_id : 0;
        $cities_id = isset($place->cities_id) ? $place->cities_id : 0;
        $language_id  = 1;
        $city = '';
        $country = '';
        if ($cities_id != 0) {
            $city = Cities::with([
                'trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'languages_spoken.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'religions.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'currencies.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'getMedias'

            ])
                ->where('id', $cities_id)
                ->where('active', 1)
                ->first();
        }
        if ($countries_id != 0) {
            $country = Countries::with([
                'trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'religions.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'languages.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'religions.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                }, 'region.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'currencies.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'getMedias',

            ])
                ->where('id', $countries_id)
                ->where('active', 1)
                ->first();
        }
        // get Etiquettes of city 
        $etiquette = [];
        if (isset($city->trans[0]->etiquette)) {
            $etiquette_array = explode('|', $city->trans[0]->etiquette);
            if (count($etiquette_array) > 0) {
                foreach ($etiquette_array as $et) {
                    $temp  = explode(':-', $et);
                    if (isset($temp[1])) {
                        $temp[0] = str_replace("\r", "", $temp[0]);
                        $temp[0] = str_replace("\n", "", $temp[0]);
                        $etiquette[$temp[0]] = $temp[1];
                    }
                }
            }
        }
        // if not exist get from counrty

        if (!empty($etiquette)) {
            $data[] = ['Ettiquette' => $etiquette];
        } else {
            if (isset($country->transsingle->etiquette)) {
                $etiquette_array = explode('|', $country->transsingle->etiquette);
                if (count($etiquette_array) > 0) {
                    foreach ($etiquette_array as $et) {
                        $temp  = explode(':', $et);
                        if (isset($temp[1])) {
                            $temp[0] = str_replace("\r", "", $temp[0]);
                            $temp[0] = str_replace("\n", "", $temp[0]);
                            $etiquette[$temp[0]] = $temp[1];
                        }
                    }
                }
            }
            if (!empty($etiquette))
                $data[] = ['Ettiquette' => $etiquette];
        }
        // get Potential Dangers of City
        $potential_dangers = [];
        if (isset($city->trans[0]->potential_dangers)) {
            $potential_dangers_array = explode('|', $city->trans[0]->potential_dangers);
            if (count($potential_dangers_array) > 0) {
                foreach ($potential_dangers_array as $et) {
                    $temp  = explode(':-', $et);
                    if (isset($temp[1])) {
                        $temp[0] = str_replace("\r", "", $temp[0]);
                        $temp[0] = str_replace("\n", "", $temp[0]);
                        $potential_dangers[$temp[0]] = $temp[1];
                    }
                }
            }
        }
        // if not exist get from counrty
        if (!empty($potential_dangers)) {
            $data[] = ['Potential Dangers' => $potential_dangers];
        } else {
            if (isset($country->trans[0]->potential_dangers)) {
                $potential_dangers_array = explode('|', $country->trans[0]->potential_dangers);
                if (count($potential_dangers_array) > 0) {
                    foreach ($potential_dangers_array as $et) {
                        $temp  = explode(':-', $et);
                        if (isset($temp[1])) {
                            $temp[0] = str_replace("\r", "", $temp[0]);
                            $temp[0] = str_replace("\n", "", $temp[0]);
                            $potential_dangers[$temp[0]] = $temp[1];
                        }
                    }
                }
            }
            if (!empty($potential_dangers))
                $data[] = ['Potential Dangers' => $potential_dangers];
        }

        // get restrictions for City

        $restrictions = [];
        if (isset($city->trans[0]->economy)) {
            $restrictions_array = explode('|', $city->trans[0]->economy);
            if (count($restrictions_array) > 0) {
                foreach ($restrictions_array as $et) {
                    $temp  = explode(':-', $et);
                    if (isset($temp[1])) {
                        $temp[0] = str_replace("\r", "", $temp[0]);
                        $temp[0] = str_replace("\n", "", $temp[0]);
                        $restrictions[$temp[0]] = $temp[1];
                    }
                }
            }
        }
        // if not exist get from counrty
        if (!empty($restrictions)) {
            $data[] = ['Restrictions' => $restrictions];
        } else {
            if (isset($country->trans[0]->economy)) {
                $restrictions_array = explode('|', $country->trans[0]->economy);
                if (count($restrictions_array) > 0) {
                    foreach ($restrictions_array as $et) {
                        $temp  = explode(':-', $et);
                        if (isset($temp[1])) {
                            $temp[0] = str_replace("\r", "", $temp[0]);
                            $temp[0] = str_replace("\n", "", $temp[0]);
                            $restrictions[$temp[0]] = $temp[1];
                        }
                    }
                }
            }
            if (!empty($restrictions))
                $data[] = ['Restrictions' => $restrictions];
        }


        // get packing trips for city

        $packing_tips = [];
        if (isset($city->trans[0]->planning_tips)) {
            $packing_tips_array = explode('|', $city->trans[0]->planning_tips);
            if (count($packing_tips_array) > 0) {
                foreach ($packing_tips_array as $et) {
                    $temp  = explode(':-', $et);
                    if (isset($temp[1])) {
                        $temp[0] = str_replace("\r", "", $temp[0]);
                        $temp[0] = str_replace("\n", "", $temp[0]);
                        $packing_tips[$temp[0]] = $temp[1];
                    }
                }
            }
        }
        // if not exist get from counrty
        if (!empty($packing_tips)) {
            $data[] = ['Packing Tips' => $packing_tips];
        } else {
            if (isset($country->trans[0]->planning_tips)) {
                $packing_tips_array = explode('|', $country->trans[0]->planning_tips);
                if (count($packing_tips_array) > 0) {
                    foreach ($packing_tips_array as $et) {
                        $temp  = explode(':-', $et);
                        if (isset($temp[1])) {
                            $temp[0] = str_replace("\r", "", $temp[0]);
                            $temp[0] = str_replace("\n", "", $temp[0]);
                            $packing_tips[$temp[0]] = $temp[1];
                        }
                    }
                }
            }
            if (!empty($packing_tips))
                $data[] = ['Packing Tips' => $packing_tips];
        }


        // get description +image of City
        if (isset($city->getMedias[0]->url) && isset($city->trans[0]->description)) {
            $data[] = ['Description' => ['image' => $city->getMedias[0]->url, 'description' => strip_tags($city->trans[0]->description)]];
        } else {
            $data[] = ['Description' => ['image' => @$country->getMedias[0]->url, 'description' => strip_tags(@$country->trans[0]->description)]];
        }


        // get population map
        if (isset($city->trans[0]->population) && isset($city->lng) && isset($city->lat)) {
            $data[] = ['population' => ['population' => $city->trans[0]->population, 'lat' => $city->lat, 'lng' => $city->lng]];
        } else {
            $data[] = ['population' => ['population' => $country->trans[0]->population, 'lat' => $country->lat, 'lng' => $country->lng]];
        }


        // get languages with language code
        $religions = [];
        if (count($city->religions) > 0) {
            foreach ($city->religions as $lang) {
                if (isset($lang->trans[0]->title)) {
                    $religions[$lang->trans[0]->title] = $lang->trans[0]->title;
                }
            }
        }
        // if not exist get from country
        if (isset($religions)) {
            $data[] = ['Religions' => $religions];
        } else {

            if (count($country->religions) > 0) {
                foreach ($country->religions as $lang) {
                    if (isset($lang->trans[0]->title)) {
                        $religions[$lang->trans[0]->title] = $lang->trans[0]->title;
                    }
                }
            }
            if (isset($religions))
                $data[] = ['Religions' => $religions];
        }

        // get relegions of city

        $languages = [];
        if (count($city->languages_spoken) > 0) {
            foreach ($city->languages_spoken as $lang) {
                if (isset($lang->trans)) {
                    $languages[$lang->trans->iso_code] = $lang->trans->title;
                }
            }
        }
        // if not exist get from country
        if (isset($languages)) {
            $data[] = ['Languages' => $languages];
        } else {

            if (count($country->languages) > 0) {
                foreach ($country->languages as $lang) {
                    if (isset($lang->trans)) {
                        $languages[$lang->trans->iso_code] = $lang->trans->title;
                    }
                }
            }
            if (isset($languages))
                $data[] = ['Languages' => $languages];
        }

        // get currencies  of city

        // get Indexes of the city
        $indexes = [];
        if (isset($city->trans[0]->cost_of_living)) {
            $index['Cost of Living Index'] = $city->trans[0]->cost_of_living;
        } else {
            $index['Cost of Living Index'] = $country->trans[0]->cost_of_living;
        }
        if (isset($city->trans[0]->geo_stats)) {
            $index['Crime Rate Index'] = $city->trans[0]->geo_stats;
        } else {
            $index['Crime Rate Index'] = $country->trans[0]->geo_stats;
        }

        if (isset($city->trans[0]->demographics)) {
            $index['Quality of Life Index'] = $city->trans[0]->demographics;
        } else {
            $index['Quality of Life Index'] = $country->trans[0]->demographics;
        }

        if (isset($city->trans[0]->pollution_index)) {
            $index['Pollution Index'] = $city->trans[0]->pollution_index;
        } else {
            $index['Pollution Index'] = $country->trans[0]->pollution_index;
        }
        if (isset($city->trans[0]->title)) {
            $title = $city->trans[0]->title;
        } else {
            $title = $country->trans[0]->title;
        }
        //$data[]  =['index'=>$index];
        if (isset($data[$pagenum]) && count($data[$pagenum]) > 0) {
            $html = view('site.place2.partials.get_about_portions', ['data' => $data[$pagenum], 'pagenum' => $pagenum, 'title' => $title, 'flag' => strtolower($country->iso_code), 'country' => @$country->trans[0]->title, 'region' => @$country->region->trans[0]->title])->render();
            return json_encode(['html' => $html]);
        } else {
            return json_encode([]);
        }
    }

    public function updateLanguageCodes()
    {
        $row = 1;
        $languages = [];
        if (($handle = fopen(public_path("citiescsv/langwithCodes.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                if ($row > 2) {
                    $languages[$data[1]] = $data[0];
                }
            }
        }
        $language_id = 1;
        foreach ($languages as $key => $lang) {
            $translatedLang = LanguagesSpokenTranslation::where('title', 'like', '%' . $lang . '%')->first();

            if (isset($translatedLang->id)) {
                LanguagesSpokenTranslation::whereId($translatedLang->id)->update(['iso_code' => strtoupper($key)]);
            }
        }
    }
    public function uploadMedia(Request $request)
    {

        $media  = $request['mediafiles'];
        if ($media) {
            list($baseType, $media) = explode(';', $media);
            list(, $media) = explode(',', $media);
            $media = base64_decode($media);
            if (strpos($baseType, 'image') !== false)
                $filename = sha1(microtime()) . "_place_added_file.png";
            else if (strpos($baseType, 'video') !== false)
                $filename = sha1(microtime()) . "_place_added_file.mp4";

            $userId = Auth::guard('user')->user()->id;
            $userName = Auth::guard('user')->user()->name;
            Storage::disk('s3')->put(
                $userId . '/medias/' . $filename,
                $media,
                'public'
            );
            $media_url = $userId . '/medias/' . $filename;
            return $media_url;
        } else {
            return '';
        }
    }
    public function updateCiteisLatLong(Request $request)
    {

        if (!isset($request->page)) {
            return 'Please enter Page number from 0';
        }
        $language_id = 1;
        $cities = Cities::with([
            'trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'countryTitle'
        ])
            ->where('lat', '0.00000000')

            ->where('active', 1)
            ->limit(100)
            ->orderBy('id', 'ASC')
            ->offset($request->page * 100)
            ->get();
        // dd($cities);
        $counter = 0;
        $res = false;
        if (isset($cities) && count($cities) > 0) {
            foreach ($cities as $city) {
                // dd( $city);
                $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?' . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($city->trans[0]->title . ' City  in ' . $city->countryTitle->title);
                $fetch_link_for_city = file_get_contents($google_link_for_city);
                // dd($fetch_link_for_city);
                $query_result_for_city = json_decode($fetch_link_for_city);
                $qr_city = $query_result_for_city->results;
                if (isset($qr_city[0]->geometry->location->lat) && $qr_city[0]->geometry->location->lng) {
                    if ($qr_city[0]->types[0] == 'locality' || $qr_city[0]->types[0] == 'administrative_area_level_3' || $qr_city[0]->types[0] == 'administrative_area_level_1') {
                        $res = Cities::whereId($city->id)->update(['lat' => $qr_city[0]->geometry->location->lat, 'lng' => $qr_city[0]->geometry->location->lng]);
                        // dd($res);
                    }
                    if ($res) {
                        // dd($city->id);
                        $counter++;
                        $res = false;
                    }
                }
            }
        }
        echo $counter . ' Cities updated';
    }
    public function ajaxSetPOIMedia(Request $request)
    {
        ignore_user_abort();
        $request = new Request();
        $request->request->add([
            'place_id' => $request->place_id,
        ]);
        $images  = app(PlaceController2::class)->ajaxGetPOIMedia($request);
    }
    public function ajaxGetPostById(Request $request)
    {
        $post = Posts::find($request->postId);
        $place = [];
        if (isset($post->posts_place[0]['places_id'])) {
            $place = Place::find($post->posts_place[0]['places_id']);
        }
        $more = View::make('site.home.partials.post-text-popup', array('post' => $post, 'place' => $place, 'checkins' => @$post->checkin[0]));
        echo $more->render();
    }
    function utf8_encode_deep(&$input)
    {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                self::utf8_encode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                self::utf8_encode_deep($input->$var);
            }
        }
    }
    public function updateTopPlaces(Request $request)
    {
        $page = $request->page;
        if (!isset($page)) {
            return 'Page no is required';
        }
        $places = [];
        $row = 1;
        if (($handle = fopen(public_path("citiescsv/top_places/Top Restaurants (Unsolved).csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $row++;
                if ($row > 2)
                    $places[] = $data;
            }
        }
        $fixed = [];
        $non_fixed = [];
        $places = array_chunk($places, 10);
        // dd($places[$page]);
        foreach ($places[$page] as $place) {
            $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?' . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($place[2] . ' in ' . $place[1]);
            $fetch_link_for_city = file_get_contents($google_link_for_city);
            $query_result_for_city = json_decode($fetch_link_for_city);
            $qr_city = $query_result_for_city->results;
            $counter = 0;
            if (isset($qr_city[0]->place_id)) {
                $existing = Place::where('provider_id', '=', $qr_city[0]->place_id)->first();
                if (is_object($existing) && isset($existing->id)) {
                    if (PlacesTop::where('places_id', $existing->id)->exists()) {
                    } else {
                        $tp_place = [
                            'places_id' => @$existing->id,
                            'rating' => isset($qr_city[0]->rating) ? $qr_city[0]->rating : 0,
                            'country' => @$existing->country->trans[0]->title,
                            'country_id' => @$existing->countries_id,
                            'city' => @$existing->city->trans[0]->title,
                            'city_id' => @$existing->cities_id,
                            'title' => isset($qr_city[0]->name) ? $qr_city[0]->name : 0,
                            'travooo_title' => @$existing->transsingle->title,
                            'reviews_num' => '0',
                            'destination_type' => 'restaurant'
                        ];
                        $count  = PlacesTop::create($tp_place);
                        if ($count)
                            $counter++;
                        $fixed[] = [@$existing->id];
                    }
                } else {
                    $id = $this->getPlaceDetailsByProvider($qr_city[0]->place_id);
                    $existing = Place::find($id);
                    $tp_place = [
                        'places_id' => @$existing->id,
                        'rating' => isset($qr_city[0]->rating) ? $qr_city[0]->rating : 0,
                        'country' => isset($existing->country->trans[0]->title) ? $existing->country->trans[0]->title : "Dummy Country",
                        'country_id' => @$existing->countries_id,
                        'city' => isset($existing->city->trans[0]->title) ? $existing->city->trans[0]->title : "Dummy City",
                        'city_id' => $existing->cities_id,
                        'title' => isset($qr_city[0]->name) ? $qr_city[0]->name : "",
                        'travooo_title' => isset($existing->transsingle->title) ? $existing->transsingle->title : " _",
                        'reviews_num' => '0',
                        'destination_type' => 'restaurant'
                    ];
                    $count  = PlacesTop::create($tp_place);
                    if ($count)
                        $counter++;
                    $fixed[] = [@$existing->id];
                }
            } else {
                $non_fixed[] = [@$place[0]];
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="fixed_restaurnt_' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($fixed as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    private function getPlaceDetailsByProvider($providerId)
    {
        $geo_link = 'https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
            . '&place_id=' . urlencode($providerId);
        $fetch_geo_link = file_get_contents($geo_link);
        $geo_result = json_decode($fetch_geo_link);
        $geo_city = @$geo_result->result->address_components;
        $geo_city_guess = '';
        $geo_country = '';
        $geo_state = '';
        $country_code = '';
        $city_found_flag = false;
        if (isset($geo_city)) {
            // dd($geo_city);
            foreach ($geo_city as $gc) {
                if (isset($gc->types) && !empty($gc->types)) {
                    if (($gc->types[0] == 'locality' ||  $gc->types[0] == 'administrative_area_level_3') && !$city_found_flag) {
                        $city_found_flag = true;
                        $geo_city_guess = $gc->long_name;
                    }
                    if ($gc->types[0] == 'country') {
                        $country_code = $gc->short_name;
                        $geo_country = $gc->long_name;
                    }
                    if ($gc->types[0] == 'administrative_area_level_1') {
                        $geo_state = $gc->long_name;
                    }
                }
            }
        }
        $geo_country_info =  Countries::whereHas('transsingle', function ($query) use ($geo_country) {
            $query->where('title', 'like', '%' . $geo_country . '%');
        })->get()->first();
        if (isset($geo_city_guess) && !empty($geo_city_guess)) {
            $get_city = Cities::whereHas('transsingle', function ($query) use ($geo_city_guess) {
                $query->where('title', 'like', '%' . $geo_city_guess . '%');
            })->where('countries_id', @$geo_country_info->id)->get()->first();
        }
        $p = new Place();
        $p->place_type = @join(",", $geo_result->result->types);
        $p->safety_degrees_id = 1;
        $p->provider_id = $providerId;

        if (isset($get_city) && is_object($get_city)) {

            $p->countries_id = $get_city->countries_id;
            $p->cities_id = $get_city->id;
            $p->states_id = $get_city->states_id;
        } else {
            // $p->cities_id ='2031';
            // $p->countries_id='373';
            // $p->states_id  ='0';

            $geo_country_info =  Countries::whereHas('transsingle', function ($query) use ($geo_country) {
                $query->where('title', 'like', '%' . $geo_country . '%');
            })->get()->first();

            $country_id = 326;
            // check if country exists or else add new 
            if (isset($geo_country_info)) {
                $country_id  = $geo_country_info->id;
            } else {
                $country  = new Countries();
                $country->lat = $geo_result->result->geometry->location->lat;
                $country->lng = $geo_result->result->geometry->location->lng;
                $country->iso_code = $country_code;
                $country->regions_id = 22;
                $country->safety_degree_id = 1;
                $country->active = 1;
                if ($country->save()) {
                    $country_id = $country->id;
                    $country_trans =  new CountriesTranslations();
                    $country_trans->countries_id = $country->id;
                    $country_trans->title = $geo_country;
                    $country_trans->languages_id = 1;
                    $country_trans->save();
                }
            }


            //check if state exists or add new
            if (isset($geo_state) && !empty($geo_state)) {
                $state = States::whereHas('transsingle', function ($query) use ($geo_state) {
                    $query->where('title', 'like', '%' . $geo_state . '%');
                })->get()->first();
            }
            $states_id = 0;
            if (isset($state) && is_object($state)) {
                $states_id =  $state->id;
                // dd($states_id);
            } else {
                $state  = new States();
                $state->lat = $geo_result->result->geometry->location->lat;
                $state->lng = $geo_result->result->geometry->location->lng;
                $state->countries_id  = $country_id;
                $state->active = 1;
                if ($country_id != 227) {
                    $state->is_province = 1;
                }
                if ($state->save()) {
                    $states_id = $state->id;
                    $state_trans =  new StateTranslation();
                    $state_trans->states_id = $states_id;
                    $state_trans->title = $geo_state;
                    $state_trans->languages_id = 1;
                    $state_trans->save();
                }
            }
            $city_id = 2031;
            // add City as it wasnt added before
            if (!empty($geo_city_guess)) {
                $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?' . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($geo_city_guess);
                $fetch_link_for_city = file_get_contents($google_link_for_city);
                $query_result_for_city = json_decode($fetch_link_for_city);
                $qr_city = $query_result_for_city->results;

                $city  = new Cities();
                $city->lat = $qr_city[0]->geometry->location->lat;
                $city->lng = $qr_city[0]->geometry->location->lng;
                $city->countries_id  = $country_id;
                $city->states_id  = $states_id;
                $city->safety_degree_id  = 1;
                $city->level_of_living_id  = 0;
                $city->active  = 1;
                if ($city->save()) {
                    $city_id = $city->id;
                    $city_trans =  new CitiesTranslations();
                    $city_trans->cities_id = $city->id;
                    $city_trans->title = $geo_city_guess;
                    $city_trans->languages_id = 1;
                    $city_trans->save();
                }
            }
            $p->countries_id = $country_id;
            $p->states_id = $states_id;
            $p->cities_id = $city_id;
        }
        $p->lat = $geo_result->result->geometry->location->lat;
        $p->lng = $geo_result->result->geometry->location->lng;
        $p->pluscode = @$geo_result->result->plus_code->compound_code;
        $p->rating = @$geo_result->result->rating;
        $p->active = 1;
        $p->auto_import = 1;
        $p->save();

        $pt = new \App\Models\Place\PlaceTranslations();
        $pt->languages_id = 1;
        $pt->places_id = $p->id;
        $pt->title = $geo_result->result->name;
        $pt->address = $geo_result->result->formatted_address;
        $pt->save();
        return $p->id;
    }
    private function getSecondary($place,$type='place'){
        $secondary_post_list = session('secondary_post_style');
        $return = '';
        if(isset($secondary_post_list[0]) && $secondary_post_list[0] == 'recommended_places'){
            $data['places'] = $this->placesYouMightLike($place);
            $return = view('site.place2.new.places_you_might_like',$data)->render();

        }else if(isset($secondary_post_list[0]) &&  $secondary_post_list[0] == 'video_you_might_like'){
            $return = view('site.home.partials.videos_you_might_like',['suggestion'=>['places'=>$place->id]])->render();
        }else if(isset($secondary_post_list[0]) &&  $secondary_post_list[0] == 'trending_places'){
            $return = view('site.place2.new.trending_places',array('city'=>$place->city->id))->render();
        }else if(isset($secondary_post_list[0]) &&  $secondary_post_list[0] == 'new_travellers'){
            $data['new_travellers'] = $this->discoverNewTravellers(Auth::user());
            $return = view('site.home.partials.discover_new_travellers', $data)->render();
        }
        if(isset($secondary_post_list[0])){
            unset($secondary_post_list[0]);
            session()->forget(['secondary_post_style']);
            $value1 = session(['secondary_post_style' =>  $secondary_post_list]);
        }
        return $return;
    }
    private function discoverNewTravellers($authUser)
    {
        $userLoc = userLoc($authUser, TRUE);
        $ip = get_client_ip();
        $myTravelStyles = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();

        // #1 & #2
        $users = User::whereNotNull('profile_picture')
            ->whereNotIn('id', [1109])
            ->whereIn('nationality', $userLoc)
            ->orderBy('created_at', 'DESC')
            ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                    $travelstylesSubQuery->whereIn('conf_lifestyles_id', $myTravelStyles);
                });
            })->take(50)->get();
        if (count($users) == 0) {
            // #3 & #4
            $otherUser = userLoc($authUser, TRUE);
            $users = User::whereNotNull('profile_picture')
                ->whereNotIn('id', [1109])
                ->whereNotIn('nationality', [$authUser->nationality])
                ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                    $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                        $travelstylesSubQuery->whereIn('conf_lifestyles_id', $myTravelStyles);
                    });
                })
                ->when(count($otherUser), function ($q) use ($otherUser) {
                    $q->orWhereIn('nationality', $otherUser);
                })
                ->orderBy('created_at', 'DESC')->take(50)->get();
            if (count($users) == 0) {
                // #3 & #4
                $users = User::whereNotNull('profile_picture')
                    ->whereNotIn('id', [1109])
                    ->whereIn('nationality', [$authUser->nationality])
                    ->when(count($otherUser), function ($q) use ($otherUser) {
                        $q->orWhereIn('nationality', $otherUser);
                    })
                    ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                        $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                            $travelstylesSubQuery->orWhereIn('conf_lifestyles_id', $myTravelStyles);
                        });
                    })
                    ->orderBy('created_at', 'DESC')->take(50)->get();
            }
        }
        $users = $users->each(function ($user) use ($authUser) {
            $user->follow_flag      = (UsersFollowers::where('followers_id', $authUser->id)->where('users_id', $user->id)->where('follow_type', 1)->first())  ? true : false;
            $user->total_follower   = $user->get_followers()->whereHas('follower')->count();
            $user->latest_followers = $user->get_followers()->whereHas('follower')->take(3)->get()->map(function ($follower) {
                return [
                    'id' => $follower->followers_id,
                    'name' => $follower->follower->name,
                ];
            })->toArray();
            unset($user->get_followers);
        });
        return $users;
    }
    private function placesYouMightLike($place)
    {
        $places = null;
        $authUser = auth()->user();
        
        $myTravelStyles  = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();
        $otherUsersOfSameNationality  = User::where('nationality', $authUser->nationality)->whereNotIn('id', [$authUser->id])->pluck('id')->toArray();
        $myPlaces = Place::where('cities_id',$place->cities_id)->pluck('id')->toArray();
        if ($authUser->nationality) {
            if (count($otherUsersOfSameNationality) > 0) {
                $placeFollowers = PlaceFollowers::select(
                    'places_id',
                    DB::raw('COUNT(users_id) AS total_follower')
                )
                    ->whereIn('users_id', $otherUsersOfSameNationality)
                    ->whereNotIn('places_id', $authUser->followedPlaces->pluck('places_id')->toArray())
                    ->whereIn('places_id',$myPlaces)
                    ->groupBy('places_id')
                    ->havingRaw('(select COUNT(users.id) from users where nationality="' . $authUser->nationality . '" and users.id!= "' . $authUser->id . '") >= 10')
                    ->orderBy('total_follower', 'DESC');

                if (count($myTravelStyles) > 0) {
                    $placeFollowers->whereHas('user.travelstyles', function ($travelstyles) use ($myTravelStyles) {
                        $travelstyles->whereIn('conf_lifestyles_id', $myTravelStyles);
                    });
                } else {
                    $placeFollowers->whereHas('place', function ($place) {
                        $place->withCount('followers')->orderBy('followers_count', 'DESC');
                    });
                }
                $places = $placeFollowers->paginate(20);
            }
        }
        if ($places != null) {
            $places = modifyPagination($places);
            if (!($places->total > 3)) {
                return NULL;
            }

            $places =  $places->data->map(function ($place) {
                return [
                    'id'                => $place->place->id,
                    'title'             => @$place->place->transsingle->title,
                    'description'       => !empty(@$place->place->transsingle->address) ? @$place->place->transsingle->address : '',
                    'img'               => check_place_photo(@$place->place),
                    'total_followers'   => count($place->place->followers),
                    'place_type'        => do_placetype(@$place->place->place_type ?: 'Event'),
                    'city'              => @$place->place->city->transsingle->title,
                    'followersList '    => @$place->place->followers,
                    'followers'         => count(@$place->place->followers) ,
                    'url'               => url('place/'.$place->place->id),

                    'latest_followers'  => $place->place->followers->take(3)->map(function ($follower) {
                        return [
                            'id'                => $follower->user->id,
                            'name'              => $follower->user->name,
                            'profile_picture'   => check_profile_picture($follower->user->profile_picture),
                        ];
                    })->toArray()
                ];
            })->toArray();
            return $places;
        }
        return null;
    }

    public function setPlacesLanguages (Request $request){
        $page = $request->get('page');
        if(!isset($page)){
            return 'Enter page no';
        }
       
        $found  = [];
        $languages_array = [
            '4'  =>'ar',
            '3'  =>'fr',
            '32' =>'pt',
            '37' =>'it',
            '38' =>'de',
            '39' =>'ru',
            '58' =>'hi',
            '67' =>'ja',
            '79' =>'ko',
            '2'  =>'es',
            '48' =>'zh',
            '96' =>'tr'

        ];
        $places = PlacesTop::limit(100)
        ->offset($page * 100)->get();
        $translate = new TranslateClient([
            'key' => 'AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
        ]);
        $counter =0;
        foreach($places as $place){
            
            if(isset($place->travooo_title)){
                foreach($languages_array as $key =>$lang){
                        $result = $translate->translate($place->travooo_title, [
                            'target' => $lang
                        ]);
                        $place_trans = new \App\Models\Place\PlaceTranslations();
                        $place_trans->languages_id =$key;
                        $place_trans->title  = $result['text'];
                        $place_trans->places_id = $place->places_id;
                        $place_trans->save();
                        $found [] = [$place->id,$lang,$key,$place->travooo_title,$result['text']];
                }
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="languages_problema_' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($found as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
        
    }



}
