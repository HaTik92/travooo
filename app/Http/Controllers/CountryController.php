<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country\Countries;
use App\Models\Access\language\Languages;
use App\Models\Place\Place;
use League\Fractal\Resource\Item;
use App\Transformers\Country\CountryTransformer;
use Illuminate\Support\Facades\DB;
use App\Models\Country\ApiCountry as Country;
use App\Models\Country\CountriesFollowers;
use Illuminate\Support\Facades\Auth;
use App\Models\ActivityLog\ActivityLog;
use App\CountriesContributions;
use Illuminate\Support\Facades\Input;
use App\Models\Posts\Checkins;

use App\Models\TripPlans\TripPlans;

use GuzzleHttp\Client;

class CountryController extends Controller {
    
    public function __construct()
    {
        //$this->middleware('auth:user');
    }

    public function getIndex($country_id) {
        $language_id = 1;
        $country = Countries::find($country_id);

        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }
        
        $data['search_in'] = $country;

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'region.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'capitals.city.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'cities',
                    'cities.getMedias.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $country_id)
                ->where('active', 1)
                ->first();

        if (!$country) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('countries_id', '=', $country->id)
                ->count();
        $languages = $country->getLanguages();
        $country->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Country::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'countries.id', '=', 'places.countries_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.countries_id', '=', 'countries.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('countries.id', $country_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('countries_id', '=', $country->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['country'] = $country;
        $data['airports'] = $airports;
        $data['user'] = Auth::guard('user')->user();
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        
        
        $client = new Client();
        //$client->getHttpClient()->setDefaultOption('verify', false);
        /*
        $result = $client->post('https://api.predicthq.com/oauth2/token', [
            'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'TZUhMoqSkz0JGuiBjL22c8x5Bsm9zgymv7AhZgg3'],
            'form_params' => [
                'grant_type' => 'client_credentials',
                'scope' => 'account events signals'
            ],
            'verify' => false
        ]);
        $events_array = false;
        if ($result->getStatusCode() == 200) {
            $json_response = $result->getBody()->read(1024);
            $response = json_decode($json_response);
            $access_token = $response->access_token;

            $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $country->lat . ',' . $country->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                'headers' => ['Authorization' => 'Bearer ' . $access_token],
                'verify' => false
            ]);
            if ($get_events1->getStatusCode() == 200) {
                $events1 = json_decode($get_events1->getBody()->read(100024));
                //dd($events);
                $events_array1 = $events1->results;
                $events_final = $events_array1;
                if ($events1->next) {
                    $get_events2 = $client->get($events1->next, [
                        'headers' => ['Authorization' => 'Bearer ' . $access_token],
                        'verify' => false
                    ]);
                    if ($get_events2->getStatusCode() == 200) {
                        $events2 = json_decode($get_events2->getBody()->read(100024));
                        //dd($events);
                        $events_array2 = $events2->results;
                        $events_final = array_merge($events_array1, $events_array2);
                    }
                }
                $data['events'] = $events_final;
            }
        }
         * 
         */
        $data['events'] = array();
        
        
        $checkins = Checkins::where('country_id', $country->id)->orderBy('id', 'DESC')->get();
        
        $result = array();
        $done = array();
        foreach ($checkins AS $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                $result[] = $checkin;
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['checkins'] = $result;
        
        return view('site.country.index', $data);
    }
    
    public function getWeather($country_id) {
        $language_id = 1;
        $country = Countries::find($country_id);

        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'region.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'capitals.city.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'cities',
                    'cities.getMedias.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $country_id)
                ->where('active', 1)
                ->first();

        if (!$country) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('countries_id', '=', $country->id)
                ->count();
        $languages = $country->getLanguages();
        $country->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Country::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'countries.id', '=', 'places.countries_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.countries_id', '=', 'countries.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('countries.id', $country_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('countries_id', '=', $country->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['country'] = $country;
        $data['airports'] = $airports;
        
        $get_place_key = curl_init();
            curl_setopt_array($get_place_key, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q='.$country->lat.','.$country->lng,
            ));
            $resp = curl_exec($get_place_key);
            curl_close($get_place_key);
            if ($resp) {
                $resp = json_decode($resp);
                $place_key = $resp->Key;

                $get_country_weather = curl_init();
                curl_setopt_array($get_country_weather, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
                ));
                $country_weather = curl_exec($get_country_weather);
                curl_close($get_country_weather);
                $data['country_weather'] = json_decode($country_weather);
                
                $get_current_weather = curl_init();
                curl_setopt_array($get_current_weather, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://dataservice.accuweather.com/currentconditions/v1/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
                ));
                $current_weather = curl_exec($get_current_weather);
                curl_close($get_current_weather);
                $data['current_weather'] = json_decode($current_weather);
                
                
                $get_daily_weather = curl_init();
                curl_setopt_array($get_daily_weather, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/daily/10day/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
                ));
                $daily_weather = curl_exec($get_daily_weather);
                curl_close($get_daily_weather);
                $data['daily_weather'] = json_decode($daily_weather)->DailyForecasts;
               
            }

            $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
            
        return view('site.country.weather', $data);
    }
    
    public function getEtiquette($country_id) {
        $language_id = 1;
        $country = Countries::find($country_id);

        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'region.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'capitals.city.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'cities',
                    'cities.getMedias.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $country_id)
                ->where('active', 1)
                ->first();

        if (!$country) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('countries_id', '=', $country->id)
                ->count();
        $languages = $country->getLanguages();
        $country->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Country::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'countries.id', '=', 'places.countries_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.countries_id', '=', 'countries.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('countries.id', $country_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('countries_id', '=', $country->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['country'] = $country;
        $data['airports'] = $airports;
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        return view('site.country.etiquette', $data);
    }
    
    public function getPackingTips($country_id) {
        $language_id = 1;
        $country = Countries::find($country_id);

        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'region.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'capitals.city.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'cities',
                    'cities.getMedias.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $country_id)
                ->where('active', 1)
                ->first();

        if (!$country) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('countries_id', '=', $country->id)
                ->count();
        $languages = $country->getLanguages();
        $country->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Country::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'countries.id', '=', 'places.countries_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.countries_id', '=', 'countries.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('countries.id', $country_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('countries_id', '=', $country->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['country'] = $country;
        $data['airports'] = $airports;
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        

        return view('site.country.packing-tips', $data);
    }
    
    
    public function getHealth($country_id) {
        $language_id = 1;
        $country = Countries::find($country_id);

        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'region.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'capitals.city.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'cities',
                    'cities.getMedias.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $country_id)
                ->where('active', 1)
                ->first();

        if (!$country) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('countries_id', '=', $country->id)
                ->count();
        $languages = $country->getLanguages();
        $country->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Country::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'countries.id', '=', 'places.countries_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.countries_id', '=', 'countries.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('countries.id', $country_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('countries_id', '=', $country->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['country'] = $country;
        $data['airports'] = $airports;
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        

        return view('site.country.health', $data);
    }
    
    public function getVisaRequirements($country_id) {
        $language_id = 1;
        $country = Countries::find($country_id);

        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'region.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'capitals.city.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'cities',
                    'cities.getMedias.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $country_id)
                ->where('active', 1)
                ->first();

        if (!$country) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('countries_id', '=', $country->id)
                ->count();
        $languages = $country->getLanguages();
        $country->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Country::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'countries.id', '=', 'places.countries_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.countries_id', '=', 'countries.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('countries.id', $country_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('countries_id', '=', $country->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['country'] = $country;
        $data['airports'] = $airports;
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        return view('site.country.visa-requirements', $data);
    }
    
    public function getWhenToGo($country_id) {
        $language_id = 1;
        $country = Countries::find($country_id);

        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'region.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'capitals.city.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'cities',
                    'cities.getMedias.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $country_id)
                ->where('active', 1)
                ->first();

        if (!$country) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('countries_id', '=', $country->id)
                ->count();
        $languages = $country->getLanguages();
        $country->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Country::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'countries.id', '=', 'places.countries_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.countries_id', '=', 'countries.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('countries.id', $country_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('countries_id', '=', $country->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['country'] = $country;
        $data['airports'] = $airports;
        
        

        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        
        return view('site.country.when-to-go', $data);
    }
    
    public function getCaution($country_id) {
        $language_id = 1;
        $country = Countries::find($country_id);

        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'region.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'capitals.city.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'cities',
                    'cities.getMedias.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $country_id)
                ->where('active', 1)
                ->first();

        if (!$country) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('countries_id', '=', $country->id)
                ->count();
        $languages = $country->getLanguages();
        $country->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Country::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'countries.id', '=', 'places.countries_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.countries_id', '=', 'countries.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('countries.id', $country_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('countries_id', '=', $country->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['country'] = $country;
        $data['airports'] = $airports;
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        return view('site.country.caution', $data);
    }

    public function postFollow($countryId) {
        $userId = Auth::Id();
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $follower = CountriesFollowers::where('countries_id', $countryId)
                ->where('users_id', $userId)
                ->first();

        if ($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that country',
                ],
                'success' => false
            ];
        }

        $country->followers()->create([
            'users_id' => $userId
        ]);

        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('Country', 'follow', $countryId);

        return [
            'success' => true
        ];
    }

    public function postUnFollow($countryId) {
        $userId = Auth::Id();

        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $follower = CountriesFollowers::where('countries_id', $countryId)
                ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this country.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('Country', 'unfollow', $countryId);
        }

        return [
            'success' => true
        ];
    }

    public function postCheckFollow($countryId) {
        $userId = Auth::Id();
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $follower = CountriesFollowers::where('countries_id', $countryId)
                ->where('users_id', $userId)
                ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    public function postVisaRequirements($countryId) {
        $userId = Auth::Id();
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $my_country = 'SA';
        $target_country = $country->iso_code;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.joinsherpa.com/v2/visa-requirements/' . $my_country . '-' . $target_country,
            CURLOPT_HTTPHEADER => array(
                'Content-Type:application/json',
                'Authorization: Basic ' . base64_encode("6801f898-5190-4177-ba1c-661e39596370:d41d8cd98f00b204e9800998ecf8427e")
            )
        ));
        $resp = curl_exec($curl);
        curl_close($curl);


        dd($resp);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function postWeather($countryId) {
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $latlng = $country->lat . ',' . $country->lng;

        $get_place_key = curl_init();
        curl_setopt_array($get_place_key, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q=41.87194000%2C12.56738000',
        ));
        $resp = curl_exec($get_place_key);
        curl_close($get_place_key);
        if ($resp) {
            $resp = json_decode($resp);
            $place_key = $resp->Key;

            $get_country_weather = curl_init();
            curl_setopt_array($get_country_weather, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/'.$place_key.'?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9',
            ));
            $country_weather = curl_exec($get_country_weather);
            curl_close($get_country_weather);
            $country_weather = json_decode($country_weather);
            dd($country_weather);
        }



        dd($place_key);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }
    
    public function postTalkingAbout($countryId) {
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $shares = $country->shares;
        
        foreach($shares AS $share) {
            $data['shares'][] = array(
                'user_id' => $share->user->id,
                'user_profile_picture'=> check_profile_picture($share->user->profile_picture)
                );
        }
        
        $data['num_shares'] = count($shares);
        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }
    
    public function postNowInCountry($countryId) {
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        

        $checkins = Checkins::where('country_id', $country->id)
                ->orderBy('id', 'DESC')
                ->take(8)
                ->get();

        $result = array();
        $done = array();
        foreach ($checkins AS $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                if(time()-strtotime($checkin->post_checkin->post->date) < (24*60*60)) {
                    $live = 1;
                }
                else {
                    $live = 0;
                }
                $result[] = array(
                    'name' => $checkin->post_checkin->post->author->name,
                    'id' => $checkin->post_checkin->post->author->id,
                    'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                    'live' => $live
                );
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['live_checkins'] = $result;

        $data['success'] = true;
        return json_encode($data);
    }
    
    public function postContribute($countryId) {
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }
        
        if(Input::has('contents')) {
            $contents = Input::get('contents');
            $country_id = Input::get('country_id');
            $type = Input::get('type');
            $user_id = 0;
            if(Auth::user()) {
                $user_id = Auth::user()->id;
            }
            $create =  new CountriesContributions;
            $create->countries_id = $country_id;
            $create->users_id = $user_id;
            $create->type = $type;
            $create->contents = $contents;
            $create->status = 0;
            if($create->save()) {
                log_user_activity('Country', 'contribute', $country_id);
                $data['success'] = true;
            }
            else {
                $data['success'] = false;
            }
                
        }
        else {
            $data['success'] = false;
            
        }
        return json_encode($data);
       
    }

}
