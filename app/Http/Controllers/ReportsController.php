<?php

namespace App\Http\Controllers;

use App\Http\Constants\CommonConst;
use App\Services\Ranking\RankingService;
use Illuminate\Http\Request;
use App\Models\Access\Language\Languages;
use League\Fractal\Resource\Item;
use App\Transformers\Country\CountryTransformer;
use Illuminate\Support\Facades\DB;
use App\Models\Country\ApiCountry as Country;
use App\Models\Country\CountriesFollowers;
use Illuminate\Support\Facades\Auth;
use App\Models\ActivityLog\ActivityLog;
use App\CountriesContributions;
use Illuminate\Support\Facades\Input;
use App\Models\Place\Place;
use App\Models\Place\PlaceFollowers;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Reviews\Reviews;
use App\Models\Posts\Checkins;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\TripPlans\TripPlans;
use App\Models\User\User;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsInfos;
use App\Models\Reports\ReportsDraft;
use App\Models\Reports\ReportsDraftInfos;
use App\Models\Reports\ReportsMedias;
use Illuminate\Support\Facades\Storage;
use App\Models\Reports\ReportsComments;
use App\Models\Reports\ReportsCommentsMedias;
use App\Models\Reports\ReportsLikes;
use App\Models\Reports\ReportsShares;
use App\Models\Reports\ReportsViews;
use App\Models\Reports\ReportsCommentsLikes;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\UsersFollowers\UsersFollowers;
use App\Models\ActivityMedia\Media;
use App\Models\User\UsersMedias;
use Illuminate\Http\JsonResponse;
use Artisan;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCommentsLikes;
use App\Models\Posts\PostsCommentsMedias;
use App\Models\Reports\ReportsLocation;
use App\Models\Reports\ReportsPlaces;
use Carbon\Carbon;
use App\Services\Translations\TranslationService;
use App\Services\Reports\ReportsService;
use App\Models\User\UsersContentLanguages;

class ReportsController extends Controller
{

    public function __construct()
    {
        //        $this->middleware('auth:user', ['except' => ['getEmbedShow']]);
    }

    public function getList(Request $request, ReportsService $reportsService)
    {
        $location_ids = $reportsService->getTrendingLocations(true);
        $is_active = true;

        if (count($location_ids) == 0) {
            $location_ids = $reportsService->getTrendingLocations(false);
            $is_active = false;
        }

        $locations = [];
        foreach ($location_ids as $ids) {
            if ($ids->type == "country") {
                $locations[] = [
                    'ob' => Countries::with('transsingle')->where('id', $ids->location_id)->first(),
                    'type' => 'country',
                    'report_count' => $is_active ? $reportsService->getLocationCount($ids->location_id) : $ids->rank
                ];
            } elseif ($ids->type == "city") {
                $locations[] = [
                    'ob' => Cities::with('transsingle')->where('id', $ids->location_id)->first(),
                    'type' => 'city',
                    'report_count' =>  $is_active ? $reportsService->getLocationCount($ids->location_id) : $ids->rank
                ];
            }
        }

        $data['top_locations'] = $locations;

        return view('site.reports.list', $data);
    }


    public function getReportFilter(Request $request)
    {
        $filters = $request->has('filters') ? $request->filters : [];
        $order = $request->order;
        $type = $request->type;
        $is_draft = false;
        $view = false;
        $from_last = false;
        $searched_text = '';
        $locations = [];
        $top_places = [];

        if ($order == 'draft') {
            $is_draft = true;
        }

        $report_list = $this->_getFilterReports('', $type, $filters, $order);

        if (isset($filters['search_text'])) {
            $searched_text = $filters['search_text'];
        }

        if (isset($filters['locations'])) {
            $locations = $filters['locations'];
        }

        if ($report_list['count'] > 0) {
            $view .= view('site.reports.partials.travloginfo_block', array('reports' => $report_list['reports'], 'from_last' => $from_last, 'is_draft' => $is_draft, 'top_places' => $top_places, 'searched_text' => $searched_text, 'f_locations' => $locations));
        }

        return new JsonResponse(['count' => $report_list['count'], 'view' => $view]);
    }

    public function geUpdatetList(Request $request, ReportsService $reportsService)
    {
        $filters = $request->filters;
        $order = $request->order;
        $type = $request->type;


        $page = $request->pagenum;
        $skip = ($page - 1) * 10;
        $view = false;
        $trending_view = false;
        $is_draft = false;
        $from_last = false;
        $searched_text = '';
        $locations = [];
        $top_places = [];

        $report_list = $this->_getFilterReports($skip, $type, $filters, $order);

        if ($order == 'draft') {
            $is_draft = true;
        }

        if (isset($filters['search_text'])) {
            $searched_text = $filters['search_text'];
        }

        if (isset($filters['locations'])) {
            $locations = $filters['locations'];
        }

        if (count($report_list['reports']) > 0) {
            if ($page >= 2 && (($page * 10 - 5) % 15 == 0 || $page % 3 == 0)) {
                $top_places = $reportsService->getTrendingDestination($page - 1);

                if (count($top_places) > 0) {
                    $trending_view = true;
                }

                if ($page % 3 == 0) {
                    $from_last = true;
                }
            }

            $view .= view('site.reports.partials.travloginfo_block', array('reports' =>  $report_list['reports'], 'from_last' => $from_last, 'top_places' => $top_places, 'is_draft' => $is_draft, 'searched_text' => $searched_text, 'f_locations' => $locations));
        }

        return new JsonResponse(['view' => $view, 'trending_view' => $trending_view]);
    }

    public function getShow($report_id, Request $request)
    {
        // If link open from mobile device then Open Travooo Mobile Application
        // now I uncomment this code for testing app
        // if (isLiveSite() && isMobileDevice()) {
        //     $url = generateDeepLink(url("api/v1/reports/{$report_id}"));
        //     return view('app.index', ['link' => $url]);
        // }

        Artisan::call('view:clear');

        $data = array();
        $expertise = array();
        $report_info = Reports::withTrashed()->findOrFail($report_id);

        if (!empty($report_info->deleted_at)) {
            return redirect('content-deleted?type=1');
        }


        if (user_access_denied($report_info->users_id, true)) {
            return redirect('report-access-denied');
        }

        if (Auth::check() && $report_info->users_id == Auth::guard('user')->user()->id) {
            $data['current_report_info'] = ReportsDraft::where('reports_id', $report_id)->first();
            $data['is_draft'] = true;
        } else {
            $data['is_draft'] = false;
        }

        //for non logged in user draft reports redirect access denied
        if (!Auth::check() && $report_info->published_date == NULL) {
            return redirect('report-access-denied');
        }

        if (Auth::check() && $report_info->users_id != Auth::user()->id) {

            if ($report_info->published_date == NULL) {
                return redirect('report-access-denied');
            }

            $report_view = new ReportsViews;
            $report_view->reports_id = $report_id;
            $report_view->users_id = Auth::user()->id;
            $report_view->save();

            updatePostRanks($report_id, CommonConst::TYPE_REPORT, CommonConst::ACTION_VIEW);
        }

        $data['report'] = $report_info;
        $data['title'] = $data['report']->title;
        $data['more_reports'] = Reports::where('id', '!=', $report_id)->whereNotIn('users_id', blocked_users_list())->whereNotNull('published_date')->orderBy('id', 'desc')->take(6)->get();

        if (isset($report_info->place_type) && $report_info->place_type == 2) {
            $data['report_dest'] = ['lat' => @$report_info->city->lat, 'lng' => @$report_info->city->lng];
        } else {
            $data['report_dest'] = ['lat' => @$report_info->country->lat, 'lng' => @$report_info->country->lng];
        }

        $author_id = $report_info->users_id;

        $cities = ExpertsCities::with('cities')->where('users_id', $author_id)->get();
        $countries = ExpertsCountries::with('countries')->where('users_id', $author_id)->get();

        if (count($cities) > 0) {
            foreach ($cities as $city) {
                $expertise[] = [
                    'id' => 'city,' . $city->cities_id,
                    'text' => $city->cities->transsingle->title
                ];
            }
        }

        if (count($countries) > 0) {
            foreach ($countries as $country) {
                $expertise[] = [
                    'id' => 'country,' . $country->countries_id,
                    'text' => $country->countries->transsingle->title
                ];
            }
        }

        if (Auth::check()) {
            $follower = UsersFollowers::where('users_id', $author_id)
                ->where('followers_id', Auth::guard('user')->user()->id)
                ->first();

            $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
            $data['follower'] = $follower;
        }

        if (isset($request->checking) && $request->checking == 1) {
            $r = ReportsInfos::where('reports_id', $report_info->id)->first();
            dd($r);
        }

        $data['date_type'] = '';
        $data['create_or_publish_date'] = '';
        if ($report_info->published_date) {
            $report_p_infos = ReportsInfos::where('reports_id', $report_info->id)->first();
            $publish_date = Carbon::parse($report_info->published_date);
            $edit_date = Carbon::parse(@$report_p_infos->created_at);

            if (isset($report_p_infos) && $edit_date->gt($publish_date)) {
                $data['date_type'] = 'Edited';
                $data['create_or_publish_date'] = diffForHumans($edit_date);
            } else {
                $data['date_type'] = 'Published';
                $data['create_or_publish_date'] = diffForHumans($publish_date);
            }
        }

        $data['expertise'] = $expertise;

        return view('site.reports.show', $data);
    }

    public function getEmbedShow($report_id, $user_id, Request $request)
    {
        $user_id = url_decode($user_id);
        $expertise = [];

        if ($user_id) {
            $data = array();
            $report_info = Reports::findOrFail($report_id);
            $user_info = User::find($user_id);

            if ($user_info->id == $report_info->users_id) {

                $data['me'] = $user_info;
                $data['report'] = $report_info;
                $data['title'] = $data['report']->title;
                $data['more_reports'] = Reports::where('id', '!=', $report_id)->whereNotNull('published_date')->orderBy('id', 'desc')->take(6)->get();
                $data['my_plans'] = TripPlans::where('users_id', $user_info->id)->get();


                $author_id = $report_info->users_id;

                $cities = ExpertsCities::with('cities')->where('users_id', $author_id)->get();
                $countries = ExpertsCountries::with('countries')->where('users_id', $author_id)->get();
                if (count($cities) > 0) {
                    foreach ($cities as $city) {
                        $expertise[] = [
                            'id' => 'city,' . $city->cities_id,
                            'text' => $city->cities->transsingle->title
                        ];
                    }
                }

                if (count($countries) > 0) {
                    foreach ($countries as $country) {
                        $expertise[] = [
                            'id' => 'country,' . $country->countries_id,
                            'text' => $country->countries->transsingle->title
                        ];
                    }
                }
                $report_info->loadMissing('infos');
                $data['expertise'] = $expertise;

                return view('site.reports.partials.view-embed', $data);
            }
        }
    }

    public function getCreate(Request $request)
    {


        $data = array();
        $data['countries'] = Countries::all();
        $data['cities'] = Cities::all();

        return view('site.reports.create', $data);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function postCreateEditFirstStep(Request $request, RankingService $rankingService, TranslationService $translationService, ReportsService $reportsService)
    {
        $title = $request->get('title');
        $description = processString($request->get('description'));
        $countries_ids = [];
        $cities_ids = [];
        $locations = $request->input('locations_ids', []);
        $report_id = $request->get('report_id');

        foreach ($locations as $location) {
            $location_type = explode('-', $location);

            if ($location_type[1] == 'country')
                $countries_ids[] = $location_type[0];

            if ($location_type[1] == 'city')
                $cities_ids[] = $location_type[0];
        }
        $new_flag = false;
        if ($report_id == "0" || $report_id == "") {
            $rep = new ReportsDraft;
            $new_flag = true;
        } else {
            if (ReportsDraft::where('reports_id', $report_id)->first()) {
                $rep = ReportsDraft::where('reports_id', $report_id)->first();
            } else {
                $rep = new ReportsDraft;
                $rep->reports_id = $report_id;
            }
        }

        $user_id = $rep && isset($rep->users_id) ? $rep->users_id : Auth::guard('user')->user()->id;
        $countries_id = implode(',', $countries_ids) ? implode(',', $countries_ids) : NULL;
        $cities_id = implode(',', $cities_ids) ? implode(',', $cities_ids) : NULL;

        if ($new_flag) {
            $p_report = new Reports;

            $p_report->users_id = $user_id;
            $p_report->countries_id = $countries_id;
            $p_report->cities_id = $cities_id;
            $p_report->title = $title;
            $p_report->description = $description;
            $p_report->flag = 0;
            $p_report->language_id = $translationService->getLanguageId($title . ' ' . $description);

            if ($p_report->save()) {
                $rep->reports_id = $p_report->id;
                $rankingService->addPointsEarners($p_report->id, get_class($p_report), auth()->id());
            }
        }

        $rep->users_id = $user_id;
        $rep->countries_id = $countries_id;
        $rep->cities_id = $cities_id;
        $rep->title = $title;
        $rep->description = $description;

        if ($rep->save()) {
            $reportsService->saveLocations($rep->reports_id, $locations);
            if ($new_flag) $reportsService->saveLocations($rep->reports_id, $locations, false);

            $info_count_step = 0;
            $report_info_count = ReportsDraftInfos::where('reports_draft_id', $rep->id)->count();
            if ($report_info_count > 0) {
                $info_count_step = ceil($report_info_count / 50);
            }
            return new JsonResponse(['id' => $rep->reports_id, 'info_count_step' => $info_count_step]);
        }
    }

    public function postCreateEditSecondStep(Request $request)
    {
        $title = $request->get('title');
        $report_infos = $request->get('rep_infos', []);
        // validate report_infos
        // $report_infos = collect($report_infos)->filter(function ($item) {
        //     $key = key(json_decode($item, true));
        //     return in_array($key, ReportsService::MAPPING_INFO_VARS);
        // })->toArray();

        $report_id = $request->get('report_id');
        $order = 1;

        $current_report = ReportsDraft::where('reports_id', $report_id)->first();
        $current_report_id = $current_report->id;

        if ($title != $current_report->title) {
            $current_report->title = $title;
            $current_report->save();
        }

        ReportsDraftInfos::where('reports_draft_id', $current_report_id)->where('var', '!=', 'cover')->delete();


        if (isset($report_infos)) {

            foreach ($report_infos as $report_info) {
                if ($report_info) {
                    $info = json_decode($report_info, true);

                    ReportsDraftInfos::updateOrCreate([
                        'reports_draft_id' => $current_report_id,
                        'var' =>  key($info),
                        'val' => $info[key($info)],
                        'order' => $order
                    ]);

                    $order++;
                }
            }
        }

        $p_report = Reports::find($report_id);
        $p_report->flag = 0;
        $p_report->save();
        $p_report->touch();


        if (isset($request->save_draft)) {
            echo $report_id;
        } else {
            if ($report_id == "0" || $report_id == "") {
                return redirect('reports/' . $report_id)->with('status', 'Report created!');
            } else {

                return redirect('reports/' . $report_id)->with('status', 'Report Updated!');
            }
        }
    }

    public function postUploadCover(Request $request)
    {
        $cropped_image = $request->cover_image;
        $report_id = $request->report_id;
        $draft_report = ReportsDraft::where('reports_id', $report_id)->first();
        if ($cropped_image) {
            $filename = microtime(true) . '-' . Auth::user()->id . '-cover-image.' . $cropped_image->getClientOriginalExtension();

            Storage::disk('s3')->putFileAs(
                'reports_cover/',
                $cropped_image,
                $filename,
                'public'
            );
            $report_cover_url = 'https://s3.amazonaws.com/travooo-images2/reports_cover/' . $filename;

            $rep_info = ReportsDraftInfos::where('reports_draft_id', $draft_report->id)->where('var', 'cover')->first();

            if ($rep_info) {
                $rep_info->val = $report_cover_url;

                if ($rep_info->save()) {
                    return new JsonResponse($report_cover_url);
                }
            } else {
                $cover = new ReportsDraftInfos;
                $cover->reports_draft_id = $draft_report->id;
                $cover->var = ReportsService::VAR_COVER;
                $cover->val = $report_cover_url;
                $cover->order = 0;

                if ($cover->save()) {
                    return new JsonResponse($report_cover_url);
                }
            }
        }
    }

    public function postUploadImage(Request $request)
    {
        $local_image = $request->local_image;
        $report_id = $request->report_id;
        $draft_report = ReportsDraft::where('reports_id', $report_id)->first();

        $filename = microtime(true) . '-' . Auth::user()->id . '-report-image.' . $local_image->getClientOriginalExtension();

        if (@$local_image->getSize() > 204800) {
            ini_set('memory_limit', '256M');

            $cropped = Image::make($local_image)->resize(670, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            Storage::disk('s3')->put(
                'reports_photo/th670/' . $filename,
                $cropped->encode(),
                'public'
            );

            $report_photo_url = 'https://s3.amazonaws.com/travooo-images2/reports_photo/th670/' . $filename;
        } else {

            Storage::disk('s3')->putFileAs(
                'reports_photo/',
                $local_image,
                $filename,
                'public'
            );

            $report_photo_url = 'https://s3.amazonaws.com/travooo-images2/reports_photo/' . $filename;
        }

        $rep_info = ReportsDraftInfos::where('reports_draft_id', $draft_report->id)->orderBy('order', 'DESC')->first();

        $photo = new ReportsDraftInfos;
        $photo->reports_draft_id = $draft_report->id;
        $photo->var = ReportsService::VAR_PHOTO;
        $photo->val = $report_photo_url;
        $photo->order = @$rep_info->order + 1;
        if ($photo->save()) {
            return new JsonResponse($report_photo_url);
        }
    }

    public function postUploadVideo(Request $request)
    {
        $local_video = $request->local_video;
        $report_id = $request->report_id;
        $draft_report = ReportsDraft::where('reports_id', $report_id)->first();

        $filename = microtime(true) . '-' . Auth::user()->id . '-report-video.' . $local_video->getClientOriginalExtension();

        Storage::disk('s3')->putFileAs(
            'reports_video/',
            $local_video,
            $filename,
            'public'
        );
        $report_video_url = 'https://s3.amazonaws.com/travooo-images2/reports_video/' . $filename;

        $rep_info = ReportsDraftInfos::where('reports_draft_id', $draft_report->id)->orderBy('order', 'DESC')->first();

        $video = new ReportsDraftInfos;
        $video->reports_draft_id = $draft_report->id;
        $video->var = ReportsService::VAR_LOCAL_VIDEO;
        $video->val = $report_video_url;
        $video->order = @$rep_info->order + 1;
        if ($video->save()) {
            return new JsonResponse($report_video_url);
        }
    }

    public function ajaxGetInfoByID(Request $request)
    {
        $id = $request->get("id");
        $data = [];
        if ($id == "0") {
            return $data;
        } else {
            $report = ReportsDraft::where('users_id', Auth::user()->id)->where('reports_id', $id)->first();

            if ($report) {
                $locations = [];
                $fetchLocations = $report->prettyLocation();

                if ($fetchLocations['total']) {
                    collect($fetchLocations[Reports::LOCATION_CITY])
                        ->each(function ($city_info) use (&$locations) {
                            $cities_id = $city_info->id;
                            $locations[] = [
                                'id' => $cities_id . '-city',
                                'text' => @$city_info->transsingle->title,
                                'info' => [
                                    'id' => $cities_id,
                                    'title' => @$city_info->transsingle->title,
                                    'country_id' => @$city_info->country->id,
                                    'country_name' => @$city_info->country->transsingle->title
                                ]
                            ];
                        });
                    collect($fetchLocations[Reports::LOCATION_COUNTRY])
                        ->each(function ($country_info) use (&$locations) {
                            $countries_id = $country_info->id;
                            $locations[] = [
                                'id' => $countries_id . '-country',
                                'text' => @$country_info->transsingle->title,
                                'info' => [
                                    'id' => $countries_id,
                                    'title' => @$country_info->transsingle->title,
                                    'country_id' => @$country_info->country->id,
                                    'country_name' => @$country_info->country->transsingle->title
                                ]
                            ];
                        });
                }

                $data["title"] = $report->title;
                $data["description"] = $report->description;
                $data["locations"] = $locations;
                $data["step1_report_id"] = $report->id;

                return $data;
            } else {
                return $data;
            }
        }
    }

    public function ajaxGetDraftByID(Request $request)
    {
        $id = $request->get("id");
        $draft_info = ReportsDraft::where('reports_id', $id)->first();
        $step = $request->step;
        $skip = $step * 50;

        $data = [];
        if ($id == "0") {

            return $data;
        } else {

            $report_info = ReportsDraftInfos::where('reports_draft_id', $draft_info->id)->skip($skip)->take(50)->get();
            if (is_object($report_info) > 0) {
                foreach ($report_info as $info) {
                    if ($info->var == "user") {
                        $users = explode(",", $info->val);
                        $usersdata = [];
                        foreach ($users as $user) {
                            $usermodel = User::find($user);

                            $user_info = array();

                            $user_info['id'] = $usermodel->id;
                            $user_info['text'] = $usermodel->name;
                            $user_info['country'] = ($usermodel->nationality != '') ? @$usermodel->country_of_nationality->transsingle->title : '';
                            $user_info['profile_picture'] = check_profile_picture($usermodel->profile_picture);
                            $user_info['cover_photo'] = check_cover_photo($usermodel->cover_photo);
                            $user_info['media'] = [];

                            $usermedias = $usermodel->my_medias()->pluck('medias_id');

                            $photos_list = Media::select('id')->where(\DB::raw('RIGHT(url, 4)'), '!=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
                            $medias = UsersMedias::where('users_id', $usermodel->id)
                                ->whereIn('medias_id', $photos_list)
                                ->orderBy('id', 'DESC')
                                ->limit(3)->get();
                            if ($medias) {
                                foreach ($medias as $media) {
                                    $user_info['media'][] = get_profile_media($media->media->url);
                                }
                            }
                            $user_follow = UsersFollowers::where('users_id', $usermodel->id)
                                ->where('followers_id', Auth::guard('user')->user()->id)
                                ->first();
                            if ($user_follow && $user_follow->follow_type == 1) {
                                $follow = "unfollow";
                                $follow_class = "btn-light-grey";
                            } else {
                                $follow = "follow";
                                $follow_class = "btn-blue-follow";
                            }
                            $user_info['follow'] = '<button type="button" class="btn ' . $follow_class . ' btn-bordered follow-button follow-button_' . $usermodel->id . '" data-user-id="' . $usermodel->id . '"  data-type="' . $follow . '" data-route-follow="' . route('profile.follow', $usermodel->id) . '" data-route-unfollow="' . route('profile.unfolllow', $usermodel->id) . '">' . $follow . '</button>';


                            $usersdata[] = [$usermodel->id, $usermodel->name, $user_info];
                        }
                        $info['val'] = $usersdata;
                    } else if ($info->var == "place") {
                        $place = Place::find($info->val);
                        $place_info = array();

                        if (is_object($place)) {
                            $place_info[] = [
                                'id' => $place->id,
                                'title' => @$place->transsingle->title,
                                'image' => (isset($place->getMedias[0]->url)) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : 'http://s3.amazonaws.com/travooo-images2/placeholders/place.png',
                                'type' => do_placetype(@$place->place_type),
                                'address' => @$place->transsingle->address,
                                'rating' => @$place->reviews()->avg('score') ? round(@$place->reviews()->avg('score'), 1) : '0.0',
                                'visited_list' => $this->_getPlaceReviews($place),
                                'place_info' => ['reaview_count' => $place->reviews()->count(), 'place_city' => @$place->city->transsingle->title . ', ' . @$place->country->transsingle->title],
                            ];
                        }

                        $info['val'] = [$info->val, @$place->transsingle->title, $place_info];
                    }
                }
                return $report_info;
            } else {
                return $data;
            }
        }
    }

    public function postDelete($report_id, Request $request, ReportsService $reportsService)
    {
        $data = [];
        $report = $reportsService->find($report_id);
        if ($report->users_id == Auth::guard('user')->user()->id) {
            $reportsService->delete($report);
            $data['status'] = 'yes';
        }
        return json_encode($data);
    }

    public function postPublish(Request $request, TranslationService $translationService, ReportsService $reportsService)
    {
        if ($request->report_id) {
            $report_id = $request->report_id;
            $p_report = Reports::findOrFail($report_id);
            $d_report = ReportsDraft::where('reports_id', $report_id)->first();

            $p_report->users_id = $d_report->users_id;
            $p_report->countries_id = $d_report->countries_id;
            $p_report->cities_id = $d_report->cities_id;
            $p_report->title = $d_report->title;
            $p_report->description = $d_report->description;
            $p_report->language_id  = $translationService->getLanguageId($d_report->title . ' ' . $d_report->description);
            $p_report->flag = 1;

            $report_infos = ReportsInfos::where('reports_id', $report_id)->delete();
            $d_report_info = ReportsDraftInfos::where('reports_draft_id', $d_report->id)->get();

            foreach ($d_report_info as $report_info) {
                $r_info = new ReportsInfos;
                $r_info->reports_id = $report_id;
                $r_info->var = $report_info->var;
                $r_info->val = $report_info->val;
                $r_info->order = $report_info->order;
                $r_info->language_id = $translationService->getLanguageId($report_info->val);
                $r_info->save();
            }

            if ($p_report->published_date == NULL) {
                $p_report->published_date = date('Y-m-d H:i:s');
                log_user_activity('Report', 'create', $p_report->id);
            } else {
                del_user_activity('Report', 'update', $p_report->id, Auth::guard('user')->user()->id);
                log_user_activity('Report', 'update', $p_report->id);
            }

            if ($p_report->save()) {
                $reportsService->publishedPlace($p_report);
                $reportsService->publishedLocation($p_report->id);
                return json_encode(['status' => 'yes']);
            }
        }
    }

    public function postComment(Request $request, RankingService $rankingService)
    {
        $pair = $request['pair'];
        $report_id = $request->report_id;
        $report = Reports::find($report_id);
        $com_text = $request->text;

        $text = convert_string($com_text);

        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;
        $file_lists = getTempFiles($pair);
        $is_files = count($file_lists) > 0;
        $text = is_null($text) ? '' : $text;

        if (!$is_files && $text == "")
            return 0;

        if (is_object($report)) {
            $new_comment = new PostsComments;
            $new_comment->posts_id = $report->id;
            $new_comment->users_id = $user_id;
            $new_comment->parents_id = (isset($request->comment_id)) ? $request->comment_id : null;
            $new_comment->text = $text;
            $new_comment->type = 'report';
            if ($new_comment->save()) {
                log_user_activity('Report', 'comment', $new_comment->id);
                updatePostRanks($report->id, CommonConst::TYPE_REPORT, CommonConst::ACTION_COMMENT);

                $rankingService->addPointsToEarners($report);

                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . preg_replace("/([#]|[%]|[+]|[$])/", "_", $file[1]);

                    Storage::disk('s3')->put('report-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/report-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->users_id = $user_id;
                    $media->type  = getMediaTypeByMediaUrl($media->url);
                    $media->author_name = $user_name;
                    $media->title = $text;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->save();

                    $report_comments_medias = new PostsCommentsMedias();
                    $report_comments_medias->posts_comments_id = $new_comment->id;
                    $report_comments_medias->medias_id = $media->id;
                    $report_comments_medias->save();
                }

                if (isset($request->comment_id)) {
                    return view('site.reports.partials.report_comment_reply_block2', array('child' => $new_comment, 'report' => $report));
                } else {
                    return view('site.reports.partials.report_comment_block2', array('comment' => $new_comment, 'report' => $report));
                }
            } else {
                return 0;
            }
        }
    }

    public function postCommentEdit(Request $request)
    {
        $pair = $request['pair'];
        $report_id = $request->report_id;
        $comment_id = $request->comment_id;

        $com_text = $request->text;
        $text = convert_string($com_text);

        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;

        $report = Reports::find($report_id);
        $report_comment = PostsComments::find($comment_id);
        $author_id = $report->users_id;

        $is_files = false;
        $file_lists = getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;
        $text = is_null($text) ? '' : $text;
        if (!$is_files && $text == "")
            return 0;

        if (is_object($report)) {
            $report_comment->text = $text;
            if ($report_comment->save()) {
                log_user_activity('Status', 'comment', $report_id);


                if (isset($request->existing_medias) && count($report_comment->medias) > 0) {

                    foreach ($report_comment->medias as $media_list) {
                        if (in_array($media_list->media->id, $request->existing_medias)) {
                            $medias_exist = Media::find($media_list->medias_id);

                            $media_list->delete();

                            if ($medias_exist) {
                                $amazonefilename = explode('/', $medias_exist->url);
                                Storage::disk('s3')->delete('report-comment-photo/' . end($amazonefilename));

                                $medias_exist->delete();
                            }
                        }
                    }
                }



                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . preg_replace("/([#]|[%]|[+]|[$])/", "_", $file[1]);
                    Storage::disk('s3')->put('report-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/report-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->users_id = $user_id;
                    $media->type  = getMediaTypeByMediaUrl($media->url);
                    $media->author_name = $user_name;
                    $media->title = $text;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->save();

                    $report_comments_medias = new PostsCommentsMedias();
                    $report_comments_medias->posts_comments_id = $report_comment->id;
                    $report_comments_medias->medias_id = $media->id;
                    $report_comments_medias->save();
                }

                $report_comment->load('medias');

                if ($request->comment_type == 1) {
                    return view('site.reports.partials.report_comment_block2', array('comment' => $report_comment, 'report' => $report));
                } else {
                    return view('site.reports.partials.report_comment_reply_block2', array('child' => $report_comment, 'report' => $report));
                }
            } else {
                return 0;
            }
        }
    }

    public function postCommentForPlace(Request $request)
    {
        $report_id = $request->get('report_id');
        $report = Reports::find($report_id);
        $text = $request->get('text');
        $user_id = Auth::guard('user')->user()->id;
        $author_id = $report->users_id;

        $comment = new ReportsComments;
        $comment->reports_id = $report->id;
        $comment->users_id = $user_id;
        $comment->comment = $text;

        if ($comment->save()) {
            if ($comment->users_id == Auth::user()->id) {
                $delete_str = '- <a href="#" reportid="' . $report_id . '" class="report-comment-delete" id="' . $comment->id . '" style="color:red">Delete</a>';
            }

            $data = '
            <div class="comment" topsort="' . count($comment->likes) . '" newsort="' . strtotime($comment->created_at) . '">
                <img class="comment__avatar" src="' . check_profile_picture($comment->author->profile_picture) . '" alt="" role="presentation" />
                <div class="comment__content" style="width: 89%;">
                    <div class="comment__header" style="position: relative;">
                    <a class="comment__username" href="' . url_with_locale('profile/' . $comment->author->id) . '">' . $comment->author->name . '</a>
                    <div class="comment__posted com-time" style="position: absolute;right: 0;">' . diffForHumans($comment->created_at) . '</div>
                    </div>
                    <div class="comment__text" style="overflow-wrap: break-word;padding: 12px 0;">' . $comment->comment . '</div>
                    <div class="comment__footer" posttype="Reportcomment">
                       <a href="#" class="report_comment_like" id="' . $comment->id . '">' . count($comment->likes) . ' ' . __("home.likes") . '</a>
                       ' . $delete_str . '
                       - <a href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(' . $comment->id . ',this)"> Report</a>
                    </div>
                </div>
            </div>';

            return $data;
        } else {
            return 0;
        }
    }

    public function ajaxReportComments4Modal(Request $request)
    {
        $report_id = $request->report_id;
        $count = $request->count;

        $skip = $count;

        $report = Reports::find($report_id);
        $rowcontent = '';
        $totalrows = 0;
        if (is_object($report)) {
            $comments = $report->comments()->skip($skip)
                ->take(5)
                ->get();

            foreach ($comments as $comment) {
                $rowcontent .= view('site.reports.partials.report_comment_block', array('comment' => $comment, 'report' => $report));
            }

            $totalrows = count($report->comments);
        }
        return json_encode([
            "data" => $rowcontent,
            "rows" => $totalrows
        ]);
    }

    public function postCommentDelete(Request $request)
    {
        $comment_id = $request->comment_id;
        $comment = PostsComments::find($comment_id);
        if ($comment) {
            $post_id = $comment->posts_id;
            $comment_likes = $comment->likes;
            foreach ($comment->medias as $com_media) {
                $medias_exist = Media::find($com_media->medias_id);
                $com_media->delete();
                if ($medias_exist) {
                    $amazonefilename = explode('/', $medias_exist->url);
                    Storage::disk('s3')->delete('report-comment-photo/' . end($amazonefilename));

                    $medias_exist->delete();
                }
            }
            if (count($comment_likes) > 0) {
                foreach ($comment_likes as $comment_like) {
                    $comment_like->delete();
                }
            }
            if ($comment->delete()) {
                updatePostRanks($post_id, CommonConst::TYPE_REPORT, CommonConst::ACTION_UNCOMMENT);

                return 1;
            }
            return 0;
        }
        return 0;
    }

    public function postShare(Request $request)
    {

        $post_id = $request->post_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = Reports::find($post_id)->users_id;
        $check_share_exists = ReportsShares::where('reports_id', $post_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_share_exists)) {
            $data['status'] = 'no';
        } else {
            $share = new ReportsShares;
            $share->reports_id = $post_id;
            $share->permissions = $request->permission;
            $share->users_id = $user_id;
            $share->text = $request->text;
            $share->save();

            log_user_activity('Report', 'share', $post_id);

            $data['status'] = 'yes';
        }
        $data['count'] = count(ReportsShares::where('reports_id', $post_id)->get());
        return json_encode($data);
    }

    public function postLike(Request $request, RankingService $rankingService)
    {

        $post_id = $request->post_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = Reports::find($post_id)->users_id;
        $check_like_exists = ReportsLikes::where('reports_id', $post_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {

            $check_like_exists->delete();
            // log_user_activity('Report', 'unlike', $post_id);
            updatePostRanks($post_id, CommonConst::TYPE_REPORT, CommonConst::ACTION_UNLIKE);

            $rankingService->subPointsFromEarners($check_like_exists->report);

            $data['status'] = 'no';
        } else {
            $like = new ReportsLikes;
            $like->reports_id = $post_id;
            $like->users_id = $user_id;
            $like->save();

            $rankingService->addPointsToEarners($like->report);

            log_user_activity('Report', 'like', $post_id);
            updatePostRanks($post_id, CommonConst::TYPE_REPORT, CommonConst::ACTION_LIKE);

            $data['status'] = 'yes';
        }
        $data['count'] = count(ReportsLikes::where('reports_id', $post_id)->get());
        return json_encode($data);
    }

    public function postCommentLike(Request $request)
    {

        $comment_id = $request->comment_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = PostsComments::find($comment_id)->users_id;
        $report_id = PostsComments::find($comment_id)->posts_id;
        $report_author_id = Reports::find($report_id)->users_id;
        $check_like_exists = PostsCommentsLikes::where('posts_comments_id', $comment_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {

            $check_like_exists->delete();
            log_user_activity('Report', 'commentunlike', $comment_id);
            //notify($author_id, 'status_commentunlike', $comment_id);

            $data['status'] = 'no';
        } else {
            $like = new PostsCommentsLikes;
            $like->posts_comments_id = $comment_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Report', 'commentlike', $comment_id);
            if ($author_id != $user_id) {
                notify($author_id, 'status_commentlike', $comment_id);
            }

            $data['status'] = 'yes';
        }
        $data['count'] = count(PostsCommentsLikes::where('posts_comments_id', $comment_id)->get());
        $data['name'] = Auth::guard('user')->user()->name;
        return json_encode($data);
    }

    public function _construct_comment($comment_id, $report_id)
    {
        $comment = PostsComments::find($comment_id);
        $delete_str = '';

        if ($comment->users_id == Auth::user()->id) {
            $delete_str = '- <a href="#" reportid="' . $report_id . '" class="report-comment-delete" id="' . $comment_id . '" style="color:red">Delete</a>';
        }

        $outta = '<div class="post-comment-row" topsort="0" newsort="' . strtotime($comment->created_at) . '">
                            <div class="post-com-avatar-wrap">
                                <img src="' . check_profile_picture($comment->author->profile_picture) . '" alt="" width="45px" height="45px">
                            </div>
                            <div class="post-comment-text" style="width:89%">
                                <div class="post-com-name-layer">
                                    <a href="' . url("profile/" . $comment->author->id) . '" class="comment-name">' . $comment->author->name . '</a>
                                    <span class="com-time">' . diffForHumans($comment->created_at) . '</span>
                                </div>
                                <div class="comment-txt" style="overflow-wrap: break-word;">
                                    <p>' . $comment->comment . '</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-time report-comment-actions" posttype="Reportcomment">
                                        <a href="#" class="report_comment_like" id="' . $comment->id . '">' . count($comment->likes) . ' ' . __("home.likes") . '</a>
                                        ' . $delete_str . '
                                        - <a href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(' . $comment->id . ',this)"> Report</a>
                                    </div>
                                </div>
                            </div>
                        </div>';

        return $outta;
    }


    public function ajaxSearchLocation(Request $request)
    {
        $query = $request->get('q');
        if ($query) {
            $locations = array();
            $queryParam = $query;
            $get_countries = Country::whereHas('transsingle', function ($query) use ($queryParam) {
                $query->where('title', 'like', "%" . $queryParam . "%");
            })->take(20)->get();
            foreach ($get_countries as $c) {
                $locations[] = [
                    'id' => $c->id . '-country',
                    'text' => $c->transsingle->title,
                    'image' => (isset($c->getMedias[0]) && is_object($c->getMedias[0])) ? S3_BASE_URL . @$c->getMedias[0]->url : url(PLACE_PLACEHOLDERS),
                    'type' => '',
                    'query' => $query
                ];
            }

            $get_cities = Cities::whereHas('transsingle', function ($query) use ($queryParam) {
                $query->where('title', 'like', "%" . $queryParam . "%");
            })->take(20)->get();
            foreach ($get_cities as $city) {
                $locations[] = [
                    'id' => $city->id . '-city',
                    'text' => $city->transsingle->title,
                    'image' => (isset($city->getMedias[0]) && is_object($city->getMedias[0])) ? S3_BASE_URL . @$city->getMedias[0]->url : url(PLACE_PLACEHOLDERS),
                    'type' => $city->country->transsingle->title,
                    'query' => $query
                ];
            }

            return json_encode($locations);
        }
    }

    public function ajaxGetLocationInfo(Request $request)
    {
        if ($request->id) {
            $location_id = $request->id;
            $loc_info = array();
            $location_ids = explode('-', $location_id);

            if ($location_ids[1] == 'country') {
                $location = Countries::find($location_ids[0]);
            } else {
                $location = Cities::find($location_ids[0]);
            }

            $loc_info = [
                'selected_id' => $location_id,
                'id' => $location->id,
                'title' => $location->transsingle->title,
                'country_id' => @$location->country->id,
                'country_name' => @$location->country->transsingle->title
            ];

            return json_encode($loc_info);
        }
    }

    public function ajaxGetLocationList(Request $request)
    {
        $locations = [];
        if ($request->id) {
            $report = ReportsDraft::where('reports_id', $request->id)->first();
            $fetchLocations = $report->prettyLocation();

            if ($fetchLocations['total']) {
                collect($fetchLocations[Reports::LOCATION_CITY])
                    ->each(function ($city) use (&$locations) {
                        $locations[] = [
                            'id' => $city->id,
                            'link' => url('city/' . $city->id),
                            'title' => $city->transsingle->title,
                            'image' => check_country_photo(@$city->getMedias[0]->url, 180),
                            'country_id' => $city->country->id,
                            'country_name' => $city->country->transsingle->title,
                            'country_image' => check_country_photo(@$city->country->getMedias[0]->url, 180),
                        ];
                    });
                collect($fetchLocations[Reports::LOCATION_COUNTRY])
                    ->each(function ($country) use (&$locations) {
                        $locations[] = [
                            'id' => $country->id,
                            'link' => url('country/' . $country->id),
                            'title' => $country->transsingle->title,
                            'image' => check_country_photo(@$country->getMedias[0]->url, 180),
                            'country_id' => '',
                            'country_image' => ''
                        ];
                    });
            }
        }
        return json_encode($locations);
    }

    public function ajaxSearchUser(Request $request)
    {
        $logged_user_id = Auth::user()->id;
        $query = $request->get('q');
        if ($query) {
            $users = array();
            $i = 0;
            $get_users = User::where(function ($q) use ($query) {
                $q->where('name', 'like', '%' . $query . '%');
                $q->orWhere('username', 'like', '%' . $query . '%');
            })->where('id', '!=', $logged_user_id)->get();

            foreach ($get_users as $u) {

                if (!user_access_denied($u->id)) {
                    $users[] = [
                        'id' => $u->id,
                        'text' => $u->name,
                        'image' => check_profile_picture($u->profile_picture),
                        'nationality' => $u->nationality ? $u->country_of_nationality->transsingle->title : '',
                        'query' => $query,
                    ];
                }
            }
            return json_encode($users);
        }
    }

    public function ajaxSearchMentionUser(Request $request)
    {
        $logged_user_id = Auth::user()->id;
        $query = $request->get('q');
        if ($query) {
            $users = array();
            $i = 0;
            $get_users = User::where('name', 'like', '%' . $query . '%')->where('id', '!=', $logged_user_id)->take(30)->get();
            foreach ($get_users as $u) {
                $users[$i]['id'] = $u->id;
                $users[$i]['value'] = $u->name;
                $users[$i]['link'] = "/profile/" . $u->id;
                $users[$i]['image'] = check_profile_picture($u->profile_picture);
                $users[$i]['nationality'] = ($u->nationality != '') ? @$u->country_of_nationality->transsingle->title : '';
                $users[$i]['contenteditable'] = false;
                $i++;
            }
            return json_encode($users);
        }
    }

    public function ajaxGetUserInfo(Request $request)
    {
        if ($request->id) {
            $user_id = $request->id;
            $user_info = array();

            $user = User::find($user_id);
            $user_info['id'] = $user_id;
            $user_info['text'] = $user->name;
            $user_info['country'] = ($user->nationality != '') ? @$user->country_of_nationality->transsingle->title : '';
            $user_info['profile_picture'] = check_profile_picture($user->profile_picture);
            $user_info['cover_photo'] = check_cover_photo($user->cover_photo);
            $user_info['media'] = [];

            $usermedias = $user->my_medias()->pluck('medias_id');

            $photos_list = Media::select('id')->where(\DB::raw('RIGHT(url, 4)'), '!=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
            $medias = UsersMedias::where('users_id', $user_id)
                ->whereIn('medias_id', $photos_list)
                ->orderBy('id', 'DESC')
                ->limit(3)->get();
            if ($medias) {
                foreach ($medias as $media) {
                    $user_info['media'][] = get_profile_media($media->media->url);
                }
            }
            $user_follow = UsersFollowers::where('users_id', $user_id)
                ->where('followers_id', Auth::guard('user')->user()->id)
                ->first();
            if ($user_follow) {
                $follow = "unfollow";
                $follow_class = "btn-light-grey";
            } else {
                $follow = "follow";
                $follow_class = "btn-blue-follow";
            }
            $user_info['follow'] = '<button type="button" class="btn ' . $follow_class . ' btn-bordered follow-button follow-button_' . $user_id . '" data-user-id="' . $user_id . '" data-type="' . $follow . '" data-route-follow="' . route('profile.follow', $user_id) . '" data-route-unfollow="' . route('profile.unfolllow', $user_id) . '">' . $follow . '</button>';
            return json_encode($user_info);
        }
    }

    public function ajaxSearchPlace(Request $request)
    {
        $query = $request->get('q');
        $final_results = [];

        if ($query != '') {
            $qr = $this->_getESPlaceResult($request);

            if ($request->has('check_query') && $request->check_query == 1) {
                dd($qr);
            }

            foreach ($qr as $r) {
                $p = Place::find(@$r->id);

                if ($p) {
                    $visited_list = $this->_getPlaceReviews($p);

                    $final_results[] = [
                        'id' => @$r->id,
                        'title' => $r->title,
                        'visited_list' => $visited_list,
                        'image' => @$r->image,
                        'query' => $query,
                        'address' => @$r->address,
                        'type' => @$r->place_type,
                        'place_location' => @$p->city->transsingle->title . ', ' . @$p->country->transsingle->title,
                        'reaview_count' => isset($p->reviews) ? @$p->reviews()->count() : 0,
                        'rating' => (isset($p->reviews) && $p->reviews()->avg('score')) ? round(@$p->reviews()->avg('score'), 1) : '0.0',
                    ];
                }
            }


            return json_encode($final_results);
        }
    }

    public function ajaxGetPlaceInfo(Request $request)
    {
        if ($request->id) {
            $place_id = $request->id;
            $place_info = array();

            $place = Place::find($place_id);

            if ($request->has('s_type') && $request->s_type == 'country_info') {
                $place_country_info = @$place->lat . '-:' . @$place->lng . '-:' . @$place->transsingle->title . '-:12-:' . $this->_getPlaceCountryInfo(@$place->country->id);
                return json_encode($place_country_info);
            } else {

                $place_info['id'] = $place_id;
                $place_info['title'] = @$place->transsingle->title;
                $place_info['image'] = (isset($place->getMedias[0]->url)) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : 'http://s3.amazonaws.com/travooo-images2/placeholders/place.png';
                $place_info['type'] = do_placetype(@$place->place_type);
                $place_info['address'] = @$place->transsingle->address;
                $place_info['rating'] = @$place->reviews()->avg('score') ? round(@$place->reviews()->avg('score'), 1) : '0.0';
                $place_info['visited_list'] = $this->_getPlaceReviews($place);
                $place_info['place_info'] = ['reaview_count' => $place->reviews()->count(), 'place_city' => @$place->city->transsingle->title . ', ' . @$place->country->transsingle->title];

                return json_encode($place_info);
            }
        }
    }



    public function ajaxGetPlanByID(Request $request)
    {
        $plan_id = $request->get('plan_id');
        $pNum = $request->current_number;
        $plan_div_id = 'add_plan_cont' . $pNum;
        $plan = TripPlans::findOrFail($plan_id);

        if ($plan) {
            $plan_country = '';
            $place_info = '';
            foreach ($plan->places as $places) {
                if ($plan_country == $places->countries_id) {
                    $place_info = 'in';
                    $plan_country = $places->countries_id;
                } else {
                    $place_info = 'across';
                }
            }

            $return = '<div class="side left pl-0 w-67">
            <div class="label">Trip Plan</div>
        </div>
        <div class="content">
            <div class="post-image-container post-follow-container travlog-map-wrap">
                <div class="post-map-wrap">
                    <div class="post-map-inner">               
                        <img src="' . get_plan_map($plan, '595', '360') . '" alt="map">                    
                    </div>
                </div>
                <div class="post-bottom-block">
                    <div class="bottom-txt-wrap">
                        <p class="bottom-ttl">' . $plan->title . '</p>
                        <div class="bottom-sub-txt">
                            <span>Trip Plan by <a href="' . url('profile/' . $plan->author->id) . '" class="name-link" target="_blank">' . $plan->author->name . '</a> for ' . count($plan->places) . ' Destinations </span>
                        </div>
                    </div>
                    <div class="bottom-btn-wrap">
                        <a type="button" target="_blank" href="' . url('trip/plan/' . $plan->id) . '" class="btn btn-light-grey btn-bordered">
                            View plan
                        </a>
                    </div>
                </div>
            </div>
        </div><div class="side right pl-15 m-side-block">
                    <ul class="video-upload-control">
                        <li>
                            <a href="#" class="handle d-inline-flex">
                                <i class="trav-move-icon rep-move-icon"></i>
                                <span>Move</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class=" d-inline-flex" onclick="reomovePlan(' . $plan_div_id . ', ' . $pNum . ', ' . $plan->id . ')" >
                                <div class="close-handle red rep-close-handle">
                                    <i class="trav-close-icon"></i>
                                </div>
                                <span>Delete</span>
                            </a>
                        </li>
                    </ul>
                </div>';
        }
        return json_encode($return);
    }

    public function ajaxGetCountryByID(Request $request, ReportsService $reportsService)
    {
        $country_id = $request->get('countries_id');
        $country = Countries::find($country_id);

        $return = $reportsService->getLocationAllInfo($country, 'country');

        return json_encode($return);
    }

    public function ajaxGetCityByID(Request $request, ReportsService $reportsService)
    {
        $city_id = $request->get('cities_id');
        $city = Cities::findOrFail($city_id);

        $return = $reportsService->getLocationAllInfo($city, 'city');

        return json_encode($return);
    }

    public function getCommentLikeUsers(Request $request)
    {
        $comment_id = $request->comment_id;
        $comment = ReportsComments::find($comment_id);

        if ($comment->likes) {
            $likes = $comment->likes()->whereNotIn('users_id', blocked_users_list())->orderBy('created_at', 'DESC')->take(8)->get();
            return view('site.home.partials.modal_comments_like_block', array('likes' => $likes));
        }
    }

    public function getMoreComment(Request $request)
    {


        $page = $request->pagenum;
        $next = ($page - 1) * 5;

        $report_id = $request->report_id;
        $report = Reports::find($report_id);
        $order = $request->order;

        if ($order == 'new') {
            $report_comments = $report->comments()->orderBy('created_at', 'desc')->skip($next)->take(5)->get();
        } else {
            $report_comments = PostsComments::withCount(['likes', 'sub'])->where('posts_id', $report_id)->where('parents_id', '=', NULL)->where('type', 'report')->orderByRaw('(likes_count + sub_count) desc')->skip($next)->take(5)->get();
        }

        $return_text = '';
        if (count($report_comments) > 0) {
            foreach ($report_comments as $report_comment) {
                $return_text .= view('site.reports.partials.report_comment_block2', array('comment' => $report_comment, 'report' => $report));
            }
        }

        return $return_text;
    }

    public function getMoreCommentLikeUsers(Request $request)
    {


        $page = $request->pagenum;
        $next = ($page - 1) * 8;
        $comment_id = $request->comment_id;
        $comment = PostsComments::find($comment_id);

        if ($comment->likes) {
            $likes = $comment->likes()->whereNotIn('users_id', blocked_users_list())->orderBy('created_at', 'DESC')->skip($next)->take(8)->get();
            return view('site.home.partials.modal_comments_like_block', array('likes' => $likes));
        }
    }

    public function postLikes4Modal(Request $request)
    {

        $report_id = $request->report_id;
        $post_likes = ReportsLikes::where('reports_id', $report_id)->orderBy('created_at', 'DESC')->take(10)->get();
        $data = '<div class="post-comment-wrapper" dir="auto">';

        foreach ($post_likes as $like) {
            $data .= '
                <div class="post-comment-row" dir="auto">
                    <div class="post-com-avatar-wrap" dir="auto">
                        <a href="' . url_with_locale('profile/' . $like->user->id) . '"><img src="' . check_profile_picture($like->user->profile_picture) . '" alt="" dir="auto"></a>
                        <a href="' . url_with_locale('profile/' . $like->user->id) . '"><span class="comment-name" dir="auto">' . $like->user->name . '</span></a>
                    </div>
                </div>
                ';
        }

        $data .= '</div>';


        return json_encode($data);
    }

    public function getMoreReportLikeUsers(Request $request)
    {

        $page = $request->pagenum;
        $next = ($page - 1) * 10;
        $report_id = $request->report_id;
        $report_likes = ReportsLikes::where('reports_id', $report_id)->orderBy('created_at', 'DESC')->skip($next)->take(10)->get();
        if (count($report_likes) > 0) {
            $data = '<div class="post-comment-wrapper" dir="auto">';

            foreach ($report_likes as $like) {
                $data .= '
                    <div class="post-comment-row" dir="auto">
                        <div class="post-com-avatar-wrap" dir="auto">
                            <a href="' . url_with_locale('profile/' . $like->user->id) . '"><img src="' . check_profile_picture($like->user->profile_picture) . '" alt="" dir="auto"></a>
                            <a href="' . url_with_locale('profile/' . $like->user->id) . '"><span class="comment-name" dir="auto">' . $like->user->name . '</span></a>
                        </div>
                    </div>
                    ';
            }

            $data .= '</div>';


            return json_encode($data);
        }
    }

    public function getMorePlan(Request $request, ReportsService $reportsService)
    {
        $page = $request->pagenum;
        $filter = $request->plan_filter;
        $type = $request->type;
        $skip = ($page - 1) * 10;

        $plan_result = $reportsService->getTripPlans($skip, $filter, $type);

        return $plan_result['view'];
    }

    public function postSearchReportPlan(Request $request, ReportsService $reportsService)
    {
        $filter = $request->filter;
        $type = $request->type;

        $plan_result = $reportsService->getTripPlans(0, $filter, $type, true);

        return new JsonResponse(['count' => $plan_result['plan_count'], 'view' => $plan_result['view']]);
    }

    public function postCommentLikes4Modal(Request $request)
    {

        $comment_id = $request->comment_id;
        $post_likes = ReportsCommentsLikes::where('comments_id', $comment_id)->get();
        $data = '<div class="post-comment-wrapper" dir="auto">';

        foreach ($post_likes as $like) {
            $data .= '
                <div class="post-comment-row" dir="auto">
                    <div class="post-com-avatar-wrap" dir="auto">
                        <a href="' . url_with_locale('profile/' . $like->author->id) . '"><img src="' . check_profile_picture($like->author->profile_picture) . '" alt="" dir="auto"></a>
                        <a href="' . url_with_locale('profile/' . $like->author->id) . '"><span class="comment-name" dir="auto">' . $like->author->name . '</span></a>
                    </div>
                </div>
                ';
        }

        $data .= '</div>';


        return json_encode($data);
    }

    public function getSearchUsersPlaces(Request $request)
    {
        $queryParam = $request->get('q');
        $return = [];
        $places_count = 0;
        $users_count = 0;
        $countries_count = 0;
        $cities_count = 0;
        $all = [];
        if ($queryParam != '') {
            $all_users = User::where('name', 'like', "%" . $queryParam . "%")->take(10)->get();
            foreach ($all_users as $user) {
                if ($user->name != null)
                    $return['users'][] = [
                        'id' => $user->id,
                        'name' => $user->name,
                        'image' => check_profile_picture($user->profile_picture),
                        'type' => 'User'
                    ];
                $users_count++;
            }
            if (isset($return['users'])) {
                $all = array_merge($all, $return['users']);
            }
            $all_countries = Countries::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($queryParam) {
                    $q->where('title', 'LIKE', "%$queryParam%");
                })
                ->limit(20)
                ->get();

            foreach ($all_countries as $place) {
                $return['countries'][] = array(
                    'id' => $place->id,
                    'title' => @$place->transsingle->title,
                    'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                    'image' => isset($place->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                    'type' => 'Country'
                );
                $countries_count++;
            }
            if (isset($return['countries'])) {
                $all = array_merge($all, $return['countries']);
            }
            $all_cities = Cities::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($queryParam) {
                    $q->where('title', 'LIKE', "%$queryParam%");
                })
                ->limit(20)
                ->get();

            foreach ($all_cities as $place) {
                $return['cities'][] = array(
                    'id' => $place->id,
                    'title' => @$place->transsingle->title,
                    'description' => !empty(@$place->transsingle->description) ? @$place->transsingle->description : '',
                    'image' => isset($place->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                    'type' => 'City'
                );
                $cities_count++;
            }
            if (isset($return['cities'])) {
                $all = array_merge($all, $return['cities']);
            }
            //elastic basaed seach for places
            $all_places = $this->_getESPlaceResult($request);

            //search from top places only
            // $this->getTopPlacesByText($request);

            foreach ($all_places as $place) {
                $places_count++;
                if ($places_count <= 10) {
                    $return['places'][] = array(
                        'id' => $place->id,
                        'title' => @$place->title,
                        'address' => @$place->address,
                        'image' => isset($place->image) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->image : asset('assets2/image/placeholders/pattern.png'),
                        'type' => 'Place'
                    );
                }
            }
            if (isset($return['places'])) {
                $all = array_merge($all, $return['places']);
            }
        }
        if (count($all) > 0) {
            $return['all'] = $all;
        }
        if ($users_count > 10) {
            $return['users_count'] = '+10';
        } else {
            $return['users_count'] = $users_count;
        }
        if ($cities_count > 10) {
            $return['cities_count'] = '+10';
        } else {
            $return['cities_count'] = $cities_count;
        }
        if ($countries_count > 10) {
            $return['countries_count'] = '+10';
        } else {
            $return['countries_count'] = $countries_count;
        }

        if ($places_count > 10) {
            $return['places_count'] = '+10';
        } else {
            $return['places_count'] = $places_count;
        }
        $return['query'] = $queryParam;

        return json_encode($return);
    }


    protected function _getPlaceReviews($place)
    {
        $visited_user_count = 0;
        $visited_list = '';
        if (isset($place->reviews)) {
            $visited_list .= '<ul class="foot-avatar-list report-place-search-users report-place-avatar-list">';
            foreach ($place->reviews()->orderBy('created_at', 'desc')->groupBy('by_users_id')->get() as $reviews) {
                if ($reviews->author) {
                    $visited_user_count++;
                    if ($visited_user_count < 4) {
                        $visited_list .= '<li><img class="small-ava" src="' . check_profile_picture($reviews->author->profile_picture) . '" alt="ava"></li>';
                    }
                }
            }
            $visited_list .= '</ul>';
            if ($visited_user_count <= 3) {
                $visited_list .= "<span>" . __('report.users_have_visited_this_place', ['count' => $visited_user_count]) . "</span>";
            } else {
                $visited_list .= "<span>" . __('report.count_others_have_visited_this_place', ['count' => ($visited_user_count - 3) . '+']) . "</span>";
            }
        } else {
            $visited_list .= "<span>" . __('report.users_have_visited_this_place', ['count' => $visited_user_count]) . "</span>";
        }

        return $visited_list;
    }

    protected function _getPlaceCountryInfo($id)
    {

        $country = Countries::findOrFail($id);

        if ($country) {
            return $country->transsingle->title . '-:' . $country->iso_code . '-:' . str_replace(',', '.', $country->transsingle->population) . '-:' . $country->id;
        }
    }

    protected function _getESPlaceResult($request)
    {
        $request->request->add(['without_users' => true]);

        $places = app('App\Http\Controllers\HomeController')->getSearchPOIs($request);
        $result = json_decode($places);

        if (count($result) > 0 && is_object($result[0])) {
            return $result;
        } else {
            return [];
        }
    }


    protected function _getFilterReports($skip, $type = 'all', $filters, $sort = 'popular')
    {
        if (!in_array($sort, ['popular', 'newest', 'oldest', 'draft'])) {
            $sort = 'popular';
        }

        $langugae_id = session('report_langugae_id');
        if ($langugae_id == null) {
            $langugae_id = getDesiredLanguage();
            if (isset($langugae_id[0])) {
                $langugae_id = $langugae_id[0];
            } else {
                $langugae_id = UsersContentLanguages::DEFAULT_LANGUAGES;
            }
            session(['report_langugae_id' => $langugae_id]);
        }

        $reports = Reports::query()
            ->where('language_id', $langugae_id)
            ->whereNotIn('users_id', blocked_users_list())
            ->when(isset($filters['search_text']), function ($q) use ($filters) {
                $search_text = $filters['search_text'];
                $q->where(function ($query) use ($search_text) {
                    $query->where('title', 'like', "%" . $search_text . "%");
                    $query->orWhere('description', 'like', "%" . $search_text . "%");
                });
            });

        if ($sort == 'newest') {
            $reports->whereNotNull('published_date')->orderBy('published_date', 'DESC');
        } elseif ($sort == 'oldest') {
            $reports->whereNotNull('published_date')->orderBy('published_date', 'ASC');
        } elseif ($sort == 'popular') {
            $reports->withCount(['likes', 'allComments', 'postShares'])
                ->whereNotNull('published_date')
                ->groupBy('reports.id')
                ->orderByDesc(DB::raw('likes_count + all_comments_count + post_shares_count'));
        } elseif ($sort == 'draft') {
            $reports->where('flag', 0)->where('users_id', Auth::guard('user')->user()->id)->orderBy('updated_at', 'DESC');
        }

        $reports->when(isset($filters['countries']) && isset($filters['cities']), function ($q) use ($filters) {
            $q->whereHas('location', function ($_q) use ($filters) {
                $_q->where(function ($query) use ($filters) {
                    $query->where('location_type', Reports::LOCATION_COUNTRY)
                        ->whereIn('location_id', $filters['countries']);
                });
                $_q->orWhere(function ($query) use ($filters) {
                    $query->where('location_type', Reports::LOCATION_CITY)
                        ->whereIn('location_id', $filters['cities']);
                });
            });
        }, function ($q) use ($filters) {
            if (isset($filters['countries'])) {
                $q->whereHas('location', function ($_q) use ($filters) {
                    $_q->where('location_type', Reports::LOCATION_COUNTRY)
                        ->whereIn('location_id', $filters['countries']);
                });
            } elseif (isset($filters['cities'])) {
                $q->whereHas('location', function ($_q) use ($filters) {
                    $_q->where('location_type', Reports::LOCATION_CITY)
                        ->whereIn('location_id', $filters['cities']);
                });
            }
        });

        $with = ['author', 'cover'];
        if ($type == 'mine' && $sort != 'draft') {
            $with = ['author', 'draft_report', 'draft_report.cover'];
            $reports->where('flag', 1)->where('users_id', Auth::user()->id);
        }

        $reports->whereHas('author');

        $count = 0;
        if ($skip != '') {
            $reports = $reports->skip($skip);
        } else {
            // $count = $reports->get()->count();
            $count = DB::table(DB::raw('(' . _getDBQuery($reports) . ') as a'))->count();
        }

        $reports = $reports->take(10);
        $reports = $reports->with($with)->get();

        if (count($reports) == 0 && $langugae_id != UsersContentLanguages::DEFAULT_LANGUAGES) {
            session(['report_langugae_id' => UsersContentLanguages::DEFAULT_LANGUAGES]);
            return $this->_getFilterReports($skip, $type, $filters, $sort);
        }

        return ['count' => $count, 'reports' => $reports];
    }
    private function getTopPlacesByText(Request $request){
        // PlacesTop::
    }
}
