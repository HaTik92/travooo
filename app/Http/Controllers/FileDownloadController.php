<?php

namespace App\Http\Controllers;

use App\Models\Chat\ChatConversationMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class FileDownloadController extends Controller
{
    //
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    public function download($folder_name, $file_name)
    {
        if ($folder_name == 'chat') {
            //Check does message exists
            $file_path = $folder_name . "/" . $file_name;

            $messages = ChatConversationMessage::where('file_path', "LIKE","%".$file_path."%")->get();

            if (count($messages) == 0) {
                return response("Message doesn't exist.", 422);
            }

            //Check is user allowed to access to this file
            $messages_from = count($messages->where('from_id', Auth::id()));
            $messages_to = count($messages->where('to_id', Auth::id()));

            if ($messages_to == 0 && $messages_from == 0) {
                return response("You are not allowed to download this file.", 403);
            }

            // Check if file exists in app/storage/file folder
            $file_name = str_replace("|", "", $file_name);
            $messages[0]->file_name = str_replace("|", "", $messages[0]->file_name);
            $file_path = "https://travooo-images2.s3.amazonaws.com/chat/" . urlencode($file_name);
            //if (file_exists($file_path)) {
                // Send Download
            
                //$head = array_change_key_case(get_headers($file_path, TRUE));
                //$filesize = $head['content-length'];
            
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary"); 
            header("Content-disposition: attachment; filename=\"" . basename($messages[0]->file_name) . "\""); 
            readfile($file_path);
/*

            $filesize = 0;
                return Response::download($file_path, $messages[0]->file_name, [
                    'Content-Length: ' . $filesize
                ]);
 * 
 */
            //} else {
                // Error
            //    echo "https://travooo-images2.s3.amazonaws.com/chat/" . $file_name;
            //    exit('File not found.');
            //}
        }
    }
}
