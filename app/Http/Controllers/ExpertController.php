<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TripPlans\TripPlans;

class ExpertController extends Controller
{
    public function getIndex() {
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        return view('site.expert.index', $data);
    }
}
