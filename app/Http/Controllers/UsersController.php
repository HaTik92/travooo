<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User\User;

use App\Models\TripPlans\TripPlans;

class UsersController extends Controller {

    public function getIndex() {
        
    }

    public function getAutoComplete(Request $request) {
        $query = $request->input('q');
        $users = array();
        $get_users = User::where('username', 'like', '%' . $query . '%')->select(array('username'))
                ->get();
        foreach ($get_users AS $gusers) {
            $users[] = $gusers->username;
        }


        return $users;
    }
    
    
    

}
