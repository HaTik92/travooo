<?php

namespace App\Http\Controllers;

use App\Services\EnlargedViews\EnlargedViewsService;
use Illuminate\Http\Request;

class EnlargedViewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }

    public function index(Request $request, $urlKey, EnlargedViewsService $enlargedViewsService)
    {
        $post = $enlargedViewsService->getPostByUrlKey($urlKey);

        return $post->buildPostView();
    }
}
