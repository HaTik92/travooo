<?php

/**
 * Created by PhpStorm.
 * User: Syuniq
 * Date: 23-Oct-18
 * Time: 4:54 PM
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Constants\CommonConst;
use App\Models\City\Cities;
use App\Models\Place\Place;
use App\Models\Posts\Checkins;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsCheckins;
use App\Services\Ranking\RankingService;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Models\ActivityMedia\Media;
use App\Models\User\UsersMedias;
use App\Models\Posts\PostsMedia;
use App\Models\Country\Countries;
use App\Models\Posts\PostsLikes;
use App\Models\Posts\PostsShares;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCommentsLikes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\ActivityMedia\MediasComments;
use App\Models\Posts\PostsTags;

use App\Models\Posts\PostsCommentsMedias;
use App\Models\ActivityMedia\MediasLikes;

use App\Models\Posts\SpamsComments;
use App\Models\Posts\SpamsPosts;

use App\Models\Posts\CheckinsLikes;
use App\Models\Posts\CheckinsComments;
use App\Models\Posts\CheckinsCommentsLikes;

use App\Models\Posts\PostsPlaces;
use App\Models\Posts\PostsCities;
use App\Models\Posts\PostsCountries;
use App\Models\Reviews\Reviews;
use App\Models\Reviews\ReviewsVotes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
use App\Models\TripPlans\TripsComments;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlaces\TripPlacesComments;
use App\Models\Events\Events;
use App\Models\Events\EventsComments;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsComments;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionReplies;
use App\Models\ActivityMedia\MediascommentsMedias;
use App\Models\TripPlans\TripsCommentsMedias;
use App\Models\User\User;
use Illuminate\Http\JsonResponse;
use App\Services\Translations\TranslationService;
use App\Services\Discussions\DiscussionsService;
use App\Services\FFmpeg\FFmpegService;


class PostController extends Controller
{
    /**
     * @var RankingService
     */
    private $rankingService;

    /**
     * @var DiscussionsService
     */
    private $discussionsService;

    public function __construct(RankingService $rankingService, DiscussionsService $discussionsService)
    {
        $this->rankingService = $rankingService;
        $this->discussionsService = $discussionsService;
    }

    /**
     * @param pair for tempfile prefix
     * @return array fileNameArray(filepath, filename)
     */
    public function getTempFiles($pair)
    {
        $temp_dir = public_path() . '/assets2/upload_tmp/';
        $auth_user_id = Auth::guard('user')->user()->id;

        $file_lists = [];

        if (is_dir($temp_dir)) {
            if ($handle = opendir($temp_dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    if (strpos($file, $pair . '_' . $auth_user_id) !== FALSE) {
                        $filename_split = explode($pair . '_' . $auth_user_id . '_', $file, 2);
                        $file_lists[] = [$temp_dir . $file, $filename_split[1]];
                    }
                }
                closedir($handle);
            }
        }

        return $file_lists;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createPost(Request $request, FFmpegService $ffmpegService)
    {
        $pair = $request['pair'];

        $auth_user_id = Auth::guard('user')->user()->id;
        $auth_user_name = Auth::guard('user')->user()->name;
        $taggedDestinarions = false;
        $taggedPlaces = @$request->taggedPlaces;
        $taggedCities = @$request->taggedCities;
        $taggedCountries = @$request->taggedCountries;
        $file_lists = $this->getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;

        if ($request->has('post_actual_date')) {
            $post_datetime = $request->get('post_actual_date');
        } else {
            $post_datetime = date("Y/m/d H:i:s", time());
        }

        if (is_null($request['text']) && !$is_files && is_null($request['location'])) {
            // return redirect()->back()->with('alert-danger', "Status can't be empty.");
            return "error";
        }


        if (is_null($auth_user_name)) {
            $auth_user_name = Auth::guard('user')->user()->username;
        }


        $validator = Validator::make($request->all(), [
            'text' => 'max:21844',
        ]);

        if ($validator->fails()) {
            // return redirect()->back()->withErrors($validator);
            return "error";
        }

        $requestText = trim(str_replace('&nbsp;', ' ', strip_tags($request['text'])));
        $isUrl = filter_var($requestText, FILTER_VALIDATE_URL);

        $post = new Posts();
        $post->users_id = $auth_user_id;

        if (!is_null($request['text'])) {
            $translationService = new TranslationService();
            $text = true;
            $post->text = $isUrl && !$is_files ? $requestText : set_links_in_content(convert_string($request['text']));
            $post->language_id = $translationService->getLanguageId($post->text);
        } else {
            $text = false;
            $post->text = '';
        }

        if (!is_null($request['permission'])) {
            $post->permission = $request['permission'];
        } else {
            $post->permission = 0;
        }

        if ($post_datetime) $post->date = $post_datetime;
        $post->save();
        // if tagged than create post in respective destination 

        if ($taggedPlaces && is_array($taggedPlaces)) {
            foreach ($taggedPlaces as $tagPlace) {
                $postplace = new PostsPlaces;
                $postplace->posts_id = $post->id;
                $postplace->places_id = $tagPlace;
                $postplace->save();
            }
        }
        if ($taggedCities && is_array($taggedCities)) {
            foreach ($taggedCities as $tagCity) {

                $postplacecity = new PostsCities;
                $postplacecity->posts_id = $post->id;
                $postplacecity->cities_id = $tagCity;
                $postplacecity->save();
            }
        }
        if ($taggedCountries && is_array($taggedCountries)) {
            foreach ($taggedCountries as $tagCountry) {

                $postplacecountry = new PostsCountries;
                $postplacecountry->posts_id = $post->id;
                $postplacecountry->countries_id = $tagCountry;
                $postplacecountry->save();
            }
        }



        //end script



        if ($isUrl && !$is_files) {
            $act = log_user_activity('Other', 'show', $post->id);

            if (is_object($act)) {
                return view('site.home.partials.other_post', array('post' => $act, 'me' => Auth::guard('user')->user()));
            }

            //            try {
            //                $given_domain = parse_url($url)['host'];
            //                $internal_domain = $request->getHttpHost();
            //
            //                if($routes->match(Request::create($url)) && $given_domain == $internal_domain) {
            //
            //                    $act = log_user_activity('Other', 'show', $post->id);
            //
            //                    if (is_object($act)) {
            //                        return view('site.home.partials.other_post', array('post' => $act, 'me' => Auth::guard('user')->user()));
            //                    }
            //
            //                }
            //            }
            //            catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e){
            //                dd('almost');
            //            }
        }

        foreach ($file_lists as $file) {
            $folderName = 'post-photo/';
            $filename = $auth_user_id . '_' . time() . '_' . Str::random(10) . '_' . removeSpecialCharactersFromFileName($file[1]);
            $filePath = $folderName . $filename;
            Storage::disk('s3')->put($filePath, fopen($file[0], 'r+'),  'public');
            $mediaPath = S3_BASE_URL . $filePath;

            $media = new Media();
            $media->url = $mediaPath;
            $media->author_name = $auth_user_name;
            $media->title = $text ? $post->text : '';
            $media->author_url = '';
            $media->source_url = '';
            $media->license_name = '';
            $media->license_url = '';
            $media->uploaded_at = date('Y-m-d H:i:s');
            $media->users_id  = $auth_user_id;
            $media->type  = getMediaTypeByMediaUrl($mediaPath);
            $media->save();

            // get video thumbnail & save s3 & then DB
            if ($media->type == Media::TYPE_VIDEO) {
                $s3Path = str_replace(S3_BASE_URL, '', $media->url); // remove S3_BASE_URL if found
                $some_file = Storage::disk('s3')->get($s3Path); // GET s3 video
                Storage::disk('public')->put($s3Path, $some_file); // store s3 video in local server
                $localVideoThumbnail = $ffmpegService->convertVideoThumbnail(public_path('storage/' . $s3Path), 600, 600, 70); // create Thumbnail in local based on local video
                if ($localVideoThumbnail) {
                    Storage::disk('public')->delete($s3Path); // delete local video
                    $s3VideoThumbnail = $ffmpegService->moveThumbnailToS3($localVideoThumbnail, true); // move local Thumbnail in s3 & delete local Thumbnail
                    $media->video_thumbnail_url = $s3VideoThumbnail; // save s3 Thumbnail path in media table
                    $media->save();
                }
            }

            // delete temp media
            gc_collect_cycles();
            @unlink($file[0]);

            $users_medias = new UsersMedias();
            $users_medias->users_id = $auth_user_id;
            $users_medias->medias_id = $media->id;
            $users_medias->save();

            $posts_medias = new PostsMedia();
            $posts_medias->posts_id = $post->id;
            $posts_medias->medias_id = $media->id;
            $posts_medias->save();

            $isValideImage = moderateImage($media, $post);
            if ($isValideImage === false) {
                $post->delete();
                return "api-error";
            }


            if ($media && $posts_medias && $users_medias && $post) {
                $check_saving = true;
                if (!$text && count($file_lists) == 1) {
                    $act = log_user_activity('Media', 'upload', $media->id);
                    if (is_object($act)) {
                        deleteUploadedTempFiles($file_lists);

                        return view('site.home.new.partials.primary-media-post', array('post' => $act));
                    }
                }
            }
        }

        if ($request->location) {
            $locationData = explode(",", $request->location);
            $location = $locationData[1];
            $lat_lng = $locationData[2] . ',' . $locationData[3];



            $checkins = new Checkins();
            $checkins->users_id = Auth::guard('user')->user()->id;
            $checkins->location = $location;
            $checkins->lat_lng = $lat_lng;
            $checkins->checkin_time = date("Y/m/d H:i:s", strtotime($request->checkin_date));


            $actual_location = $request->post_actual_location;
            $act_location = @explode(",", $actual_location);
            // $actual_distance = haversineGreatCircleDistance(@$act_location[0], @$act_location[1], @$locationData[2], @$locationData[3]);

            if ($locationData[4] == 'Place') {
                $checkins->place_id = $locationData[0];
            } else if ($locationData[4] == 'City') {
                $checkins->city_id = $locationData[0];
            } else if ($locationData[4] == 'Country') {
                $checkins->country_id = $locationData[0];
            }

            $checkins->save();

            $post_checkins = new PostsCheckins();
            $post_checkins->checkins_id = $checkins->id;
            $post_checkins->posts_id = $post->id;
            $post_checkins->save();

            $act = log_user_activity('Post', 'publish', $post->id, $post->permission);
            if (is_object($act)) {
                deleteUploadedTempFiles($file_lists);

                return view('site.home.new.partials.primary-text-only-post', array('post' => $act, 'me' => Auth::guard('user')->user()));
            }
        } elseif (!$is_files) {
            $act = log_user_activity('Post', 'publish', $post->id, $post->permission);
            if (is_object($act)) {
                deleteUploadedTempFiles($file_lists);

                return view('site.home.new.partials.primary-text-only-post', array('post' => $act, 'me' => Auth::guard('user')->user()));
            }
        }

        $check_saving = false;

        if (($is_files && $text) || count($file_lists) > 1) {
            $act = log_user_activity('Post', 'publish', $post->id, $post->permission);
            if (is_object($act)) {
                deleteUploadedTempFiles($file_lists);

                return view('site.home.new.partials.primary-text-only-post', array('post' => $act, 'me' => Auth::guard('user')->user()));
            }
        }
        // return $return;
        // if ($check_saving == true && $text == true) {
        //     return redirect()->back()->with('alert-success', 'Your status posted successfully.');
        // } elseif ($check_saving == true && $text == false) {
        //     return redirect()->back()->with('alert-success', 'Your photos (videos) posted successfully.');
        // }
        // return redirect()->back()->with('alert-success', 'Your status posted successfully.');

    }

    /**
     * @param Request $request
     */
    public function postTmpUpload(Request $request, FFmpegService $ffmpegService)
    {
        $extension_list = ['avi', 'qt'];

        $file = $request->file('file');
        $pair = $request['pair'];
        $auth_user_id = Auth::guard('user')->user()->id;
        $extension = $file->extension();

        $filename = $pair . '_' . $auth_user_id . '_' . $file->getClientOriginalName();
        $path = public_path() . '/assets2/upload_tmp/';
        @chmod($path, 0777);
        $file->move($path, $filename);

        // covert avi, qt to mp4
        if (in_array($extension, $extension_list)) {
            $new_filename =  $pair . '_' . $auth_user_id . '_' . microtime(true) . '_video.mp4';
            $ffmpegService->convertVideoFile($path, $filename, $new_filename);
        }
    }

    /**
     * @param Request $request
     */
    public function uploadTmpDelete(Request $request)
    {
        $pair = $request['pair'];
        $filename = $request['fileName'];
        $auth_user_id = Auth::guard('user')->user()->id;

        $filename = $pair . '_' . $auth_user_id . '_' . $filename;
        $filepath = './assets2/upload_tmp/' . $filename;
        @unlink($filepath);
    }

    /**
     * @return array
     */
    public function location(Request $request)
    {
        $latitude = $request->lat;
        $longitude = $request->lon;

        /*
          $countries = Countries::with('trans')->select(DB::raw('*, ( 6367 * acos( cos( radians('.$latitude.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( lat ) ) ) ) AS distance'))
          ->having('distance', '<', 10000)
          ->orderBy('distance')
          ->limit(5)->get();

          $cities = Cities::with('trans')->select(DB::raw('*, ( 6367 * acos( cos( radians('.$latitude.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( lat ) ) ) ) AS distance'))
          ->having('distance', '<', 10000)
          ->orderBy('distance')
          ->limit(10)->get();

          $places = Place::with('trans')->select(DB::raw('*, ( 6367 * acos( cos( radians('.$latitude.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( lat ) ) ) ) AS distance'))
          ->having('distance', '<', 150)
          ->orderBy('distance')
          ->limit(20)->get();

          $location_data = ['countries' => $countries, 'cities' => $cities, 'places' => $places];
         * 
         */


        //return $location_data;
    }

    /**
     * @return array
     */
    public function postLikeUnlike(Request $request)
    {

        $post_id = $request->post_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = Posts::find($post_id)->users_id;
        $check_like_exists = PostsLikes::where('posts_id', $post_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            //dd($check_like_exists);
            $check_like_exists->delete();
            log_user_activity('Post', 'unlike', $post_id);
            updatePostRanks($post_id, CommonConst::TYPE_POST, CommonConst::ACTION_UNLIKE);
            unnotify($author_id, 'status_like', $post_id);

            $data['status'] = 'no';
        } else {
            $like = new PostsLikes;
            $like->posts_id = $post_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Post', 'like', $post_id);
            updatePostRanks($post_id, CommonConst::TYPE_POST, CommonConst::ACTION_LIKE);

            //notify($author_id, 'status_like', $post_id);

            if ($user_id != $author_id) {
                notify($author_id, 'status_like', $post_id);
            }


            $data['status'] = 'yes';
        }
        $data['count'] = count(PostsLikes::where('posts_id', $post_id)->get());
        return json_encode($data);
    }
    public function postLikes4Modal(Request $request)
    {

        $post_id = $request->post_id;
        $post_likes = PostsLikes::where('posts_id', $post_id)->get();
        $data = '<div class="post-comment-wrapper" dir="auto">';

        foreach ($post_likes as $like) {
            $data .= '
                <div class="post-comment-row" dir="auto">
                    <div class="post-com-avatar-wrap" dir="auto">
                        <a href="' . url_with_locale('profile/' . $like->user->id) . '"><img src="' . check_profile_picture($like->user->profile_picture) . '" alt="" dir="auto"></a>
                        <a href="' . url_with_locale('profile/' . $like->user->id) . '"><span class="comment-name" dir="auto">' . $like->user->name . '</span></a>
                    </div>
                </div>
                ';
        }

        $data .= '</div>';


        return json_encode($data);
    }
    /**
     * @return array
     */
    public function postCommentLikeUnlike(Request $request)
    {
        $comment_id = $request->comment_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = PostsComments::find($comment_id)->users_id;
        $post_id = PostsComments::find($comment_id)->posts_id;

        //        if ($request->get('type') == 'report') {
        //            $post_author_id = Reports::find($post_id)->users_id;
        //        } else {
        //            $post_author_id = Posts::find($post_id)->users_id;
        //        }

        $check_like_exists = PostsCommentsLikes::where('posts_comments_id', $comment_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            //dd($check_like_exists);
            $check_like_exists->delete();
            log_user_activity('Post', 'commentunlike', $comment_id);
            //notify($author_id, 'status_commentunlike', $comment_id);

            $data['status'] = 'no';
        } else {
            $like = new PostsCommentsLikes;
            $like->posts_comments_id = $comment_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Post', 'commentlike', $comment_id);
            if ($user_id != $author_id) {
                notify($author_id, 'status_commentlike', $post_id);
            }

            $data['status'] = 'yes';
        }
        $data['count'] = count(PostsCommentsLikes::where('posts_comments_id', $comment_id)->get());
        $data['name'] = Auth::guard('user')->user()->name;
        return json_encode($data);
    }

    /**
     * @return array
     */
    public function postShareUnshare(Request $request)
    {

        $post_id = $request->post_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = Posts::find($post_id)->users_id;
        $check_share_exists = PostsShares::where('posts_id', $post_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_share_exists)) {
            $shareid = $check_share_exists->id;
            $check_share_exists->delete();
            log_user_activity('Post', 'unshare', $post_id);
            updatePostRanks($post_id, CommonConst::TYPE_POST, CommonConst::ACTION_UNSHARE);
            // notify($author_id, 'status_unlike', $post_id);

            $data['status'] = 'no';
            $data['rowId'] = $shareid;
        } else {
            $share = new PostsShares;
            $share->posts_id = $post_id;
            $share->users_id = $user_id;
            $share->save();

            log_user_activity('Post', 'share', $post_id);
            updatePostRanks($post_id, CommonConst::TYPE_POST, CommonConst::ACTION_SHARE);
            // notify($author_id, 'status_share', $post_id);

            $data['status'] = 'yes';
            $data['rowId'] = $post_id;
            $data['row'] = '<div class="post-comment-row" id="shareRow' . $share->id . '">
                                <div class="post-com-avatar-wrap"><img src="' . check_profile_picture($share->author->profile_picture) . '" alt=""></div>
                                <div class="post-comment-text">
                                    <div class="post-com-name-layer"><a href="#" class="comment-name">' . $share->author->name . '</a></div>
                                    <div class="comment-bottom-info"><div class="com-time">' . diffForHumans($share->created_at) . '</div></div>
                                </div>
                            </div>';
        }





        $data['count'] = count(PostsShares::where('posts_id', $post_id)->get());
        return json_encode($data);
    }

    /**
     * @return array
     */
    public function postComment(Request $request)
    {
        $pair = $request['pair'];
        $post_id = $request->post_id;
        $post_comment = $request->text;

        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;

        $author_id = Posts::find($post_id)->users_id;

        $is_files = false;
        $file_lists = $this->getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;
        $post_comment = is_null($post_comment) ? '' : $post_comment;

        if (!$is_files && $post_comment == "")
            return 0;

        $post = Posts::find($post_id);

        $text = convert_string($post_comment);
        if (is_object($post)) {
            $new_comment = new PostsComments;
            $new_comment->users_id = $user_id;
            $new_comment->posts_id = $post_id;
            $new_comment->text = $text;
            if ($new_comment->save()) {
                log_user_activity('Status', 'comment', $post_id);
                if ($user_id != $author_id) {
                    // notify($author_id, 'status_comment', $post_id);
                }

                $this->uploadCommentMedia($file_lists, $new_comment, $text);

                return view('site.home.partials.post_comment_block', array('comment' => $new_comment, 'post' => $post));
            } else {
                return 0;
            }
        }
    }

    public function postCommentReply(Request $request)
    {
        $pair = $request['pair'];
        $post_id = $request->post_id;
        $comment_id = $request->comment_id;

        $post_comment_reply = $request->text;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = Posts::find($post_id)->users_id;
        $comment_author_id = PostsComments::find($comment_id)->users_id;

        $user_name = Auth::guard('user')->user()->name;

        $is_files = false;
        $file_lists = $this->getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;
        $post_comment_reply = is_null($post_comment_reply) ? '' : $post_comment_reply;
        if (!$is_files && $post_comment_reply == "")
            return 0;

        $text = convert_string($post_comment_reply);

        $post = Posts::find($post_id);
        if (is_object($post)) {
            $new_comment = new PostsComments;
            $new_comment->users_id = $user_id;
            $new_comment->posts_id = $post_id;
            $new_comment->parents_id = $comment_id;
            $new_comment->text = $text;
            if ($new_comment->save()) {
                log_user_activity('Status', 'comment', $post_id);
                if ($user_id != $comment_author_id) {
                    // notify($author_id, 'status_comment', $post_id);
                }

                $this->uploadCommentMedia($file_lists, $new_comment, $text);

                return view('site.home.partials.post_comment_reply_block', array('child' => $new_comment, 'post' => $post));
            } else {
                return 0;
            }
        }
    }

    public function postCommentDelete(Request $request)
    {
        $comment = PostsComments::find($request->comment_id);
        if ($comment) {
            $post_id = $comment->posts_id;
            $post_type = $comment->type;

            foreach ($comment->medias as $com_media) {
                $medias_exist = Media::find($com_media->medias_id);

                $com_media->delete();

                if ($medias_exist) {
                    $amazonefilename = explode('/', $medias_exist->url);
                    Storage::disk('s3')->delete('post-comment-photo/' . end($amazonefilename));

                    $medias_exist->delete();
                }

                $user_media_exist = UsersMedias::where('medias_id', $com_media->medias_id)->where('users_id', Auth::user()->id)->first();

                if ($user_media_exist) {
                    $user_media_exist->delete();
                }
            }

            if ($comment->delete()) {
                if (in_array($post_type, CommonConst::postTypes(true))) {
                    updatePostRanks($post_id, $post_type, CommonConst::ACTION_UNCOMMENT);
                }
                return 1;
            }
            return 0;
        }
        return 0;
    }

    /**
     * @return array
     */
    public function postDelete(Request $request)
    {


        $post_id = $request->post_id;
        $post_type = $request->post_type;
        $user_id = Auth::guard('user')->user()->id;


        $data = [];
        if (strtolower($post_type) == "post" || strtolower($post_type) == "checkin") {

            $check_post_exists = Posts::where('id', $post_id)->where('users_id', $user_id)->get()->first();
            if (is_object($check_post_exists)) {


                if (strtolower($post_type) == "checkin") {
                    $check_checkin_exists = $check_post_exists->checkin[0];
                    $check_postcheckin_exists = PostsCheckins::where('posts_id', $post_id)->get();
                    if (is_object($check_postcheckin_exists)) {
                        foreach ($check_postcheckin_exists as $checkin) {
                            $checkin->delete();
                        }
                    }
                    if (is_object($check_checkin_exists)) {
                        $check_checkin_exists->delete();
                        // log_user_activity('Post', 'delete', $post_id);
                    }
                }

                if (strtolower($post_type) == "post") {
                    $check_share_exists = PostsShares::where('posts_id', $post_id)->get();
                    if (is_object($check_share_exists)) {
                        foreach ($check_share_exists as $share) {
                            $share->delete();
                        }
                    }

                    $check_comment_exists = PostsComments::where('posts_id', $post_id)->get();
                    if (is_object($check_comment_exists)) {
                        foreach ($check_comment_exists as $comment) {
                            foreach ($comment->medias as $com_media) {
                                $com_media->delete();
                            }
                            foreach ($comment->likes as $com_like) {
                                $com_like->delete();
                            }
                            $comment->delete();
                        }
                    }

                    $check_likes_exists = PostsLikes::where('posts_id', $post_id)->get();
                    if (is_object($check_likes_exists)) {
                        foreach ($check_likes_exists as $like) {
                            $like->delete();
                        }
                    }

                    $check_postmedia_exists = PostsMedia::where('posts_id', $post_id)->get();
                    if (is_object($check_postmedia_exists)) {
                        foreach ($check_postmedia_exists as $media) {
                            $medias_exist = Media::find($media->medias_id);
                            $user_media_exist = UsersMedias::where('medias_id', $media->medias_id)->where('users_id', $user_id)->first();

                            $media->delete();
                            if ($medias_exist) {
                                $amazonefilename = explode('/', $medias_exist->url);
                                Storage::disk('s3')->delete('post-photo/' . end($amazonefilename));

                                $medias_exist->delete();
                            }

                            if ($user_media_exist) {
                                $user_media_exist->delete();
                            }
                        }
                    }

                    $check_posttag_exists = PostsTags::where('posts_id', $post_id)->get();
                    if (is_object($check_posttag_exists)) {
                        foreach ($check_posttag_exists as $tag) {
                            $tag->delete();
                        }
                    }
                }
                $check_post_exists->delete();
                log_user_activity('Post', 'delete', $post_id);
                //notify($author_id, 'status_unlike', $post_id);

                $data['status'] = 'yes';
            } else {

                $data['status'] = 'no';
            }
        } else if (strtolower($post_type) == 'media') {

            /*----------- After discussion ---------*/
            $check_user_exists = UsersMedias::where('medias_id', $post_id)->where('users_id', $user_id)->get();
            $check_post_exists = PostsMedia::where('medias_id', $post_id)->get();
            $check_media_exists = Media::where('id', $post_id)->get()->first();
            if (
                count($check_post_exists) > 0 &&
                count($check_user_exists) > 0 &&
                is_object($check_media_exists)
            ) {


                // deleting media comments
                foreach ($check_media_exists->comments as $comment) {
                    // deleting media comment likes
                    foreach ($comment->likes as $like) {
                        $like->delete();
                    }
                    $comment->delete();
                }

                // deleting media likes
                foreach ($check_media_exists->likes as $like) {
                    $like->delete();
                }

                // deleting media likes
                foreach ($check_media_exists->shares as $share) {
                    $share->delete();
                }

                // deleting media post
                foreach ($check_post_exists as $post) {
                    $post->delete();
                }

                // deleting user media
                foreach ($check_user_exists as $usermedia) {
                    $usermedia->delete();
                }

                $amazonefilename = explode('/', $check_media_exists->url);
                Storage::disk('s3')->delete('post-photo/' . end($amazonefilename));

                $check_media_exists->delete();
                $data['status'] = 'yes';
                log_user_activity('Media', 'delete', $post_id);
            } else {
                $data['status'] = 'no';
            }
        } else if (strtolower($post_type) == 'review') {

            $review_exists = Reviews::where('id', $post_id)->where('by_users_id', $user_id)->get()->first();
            if (is_object($review_exists)) {

                $review_vote_exists = ReviewsVotes::where('review_id', $post_id)->get();
                if (is_object($review_vote_exists)) {
                    foreach ($review_vote_exists as $vote) {
                        $vote->delete();
                    }
                }

                $review_exists->delete();
                log_user_activity('Review', 'delete', $post_id);
                //notify($author_id, 'status_unlike', $post_id);

                $data['status'] = 'yes';
            } else {

                $data['status'] = 'no';
            }
        }
        return json_encode($data);
    }

    /**
     *
     */
    public function reportSpam(Request $request)
    {
        $type = $request->type;     //report type   0: spam, 1: other
        $posttype = (isset($request->post_type) && !empty($request->post_type)) ? $request->post_type : 'Post'; //post type
        $id = $request->data_id;    //dataid
        $text = $request->text;     //report text
        $users_id = Auth::guard('user')->user()->id;
        $alreadyReported = SpamsPosts::where(
            [
                'posts_id' => $id,
                'users_id' => $users_id,
                'post_type' => $posttype,
                'report_type' => $type
            ]
        )->first();

        if ($alreadyReported) {
            return json_encode("You have already reported before");
        }


        $model = new SpamsPosts();
        $model->posts_id = $id;
        $model->users_id = $users_id;
        $model->post_type = $posttype;
        $model->report_type = $type;
        $model->report_text = $text;
        $model->save();

        return json_encode("Thanks for your report.");
    }

    public function getAjaxView(Request $request)
    {
        $post_id = $request->post_id;
        // dd($post_id, $request->shared_id);
        $shared = filter_var($request->post_shared, FILTER_VALIDATE_BOOLEAN);
        $shared_post = $shared ? Posts::find($request->shared_id) : null;
        switch ($request->post_type) {
            case 'post':
            case 'Post':
            case 'text':
                $post = Posts::find($post_id);
                $view = 'site.home.new.partials.modals._standAloneTextPopup';
                break;
            case 'trip':
            case 'Trip':
                $post = TripPlans::find($post_id);
                $view = 'site.home.new.partials.modals._tripTextPopup';
                break;
            case 'event':
                $post = Events::find($post_id);
                $view = 'site.home.new.partials.modals._eventTextPopup';
        }
        if (isset($post)) {
            if ($shared) {
                $check_availeble_shares = PostsShares::where(['type' => $request->post_type, 'posts_type_id' => $post->id])->distinct('users_id')->pluck('users_id');
            }
            return view($view, ["post" => $post, "sharing_users_list" => @$check_availeble_shares, "shareable_flag" => @$shared, "shared_post" => $shared_post]);
        } else {
            return response(null, 500);
        }
    }

    /**
     * @return array
     */
    public function checkinLikeUnlike(Request $request)
    {

        $post_id = $request->id;
        $user_id = Auth::guard('user')->user()->id;
        // $author_id = Checkins::find($post_id)->users_id;
        $check_like_exists = CheckinsLikes::where('checkins_id', $post_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            //dd($check_like_exists);
            $check_like_exists->delete();
            log_user_activity('Checkin', 'unlike', $post_id);
            // notify($author_id, 'status_unlike', $post_id);

            $data['status'] = 'no';
        } else {
            $like = new CheckinsLikes;
            $like->checkins_id = $post_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Checkin', 'like', $post_id);
            // notify($author_id, 'status_like', $post_id);

            $data['status'] = 'yes';
        }
        $data['count'] = count(CheckinsLikes::where('checkins_id', $post_id)->get());
        return json_encode($data);
    }


    public function checkinCommentDelete(Request $request)
    {
        $comment_id = $request->comment_id;

        $comment = CheckinsComments::find($comment_id);

        if ($comment->delete()) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * @return array
     */
    public function checkinComment(Request $request)
    {
        $post_id = $request->post_id;
        $post_comment = processString($request->text);

        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;

        $author_id = Posts::find($post_id)->users_id;

        $post_comment = is_null($post_comment) ? '' : $post_comment;

        if ($post_comment == "")
            return 0;

        $post = Checkins::find($post_id);
        if (is_object($post)) {
            $new_comment = new CheckinsComments;
            $new_comment->users_id = $user_id;
            $new_comment->checkins_id = $post_id;
            $new_comment->text = $post_comment;
            if ($new_comment->save()) {
                log_user_activity('Checkin', 'comment', $post_id);
                // notify($author_id, 'status_comment', $post_id);

                return view('site.home.partials.checkin_do_comment_block', array('comment' => $new_comment, 'post' => $post));
            } else {
                return 0;
            }
        }
    }

    /**
     * @return array
     */
    public function checkinCommentLikeUnlike(Request $request)
    {

        $comment_id = $request->comment_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = CheckinsComments::find($comment_id)->users_id;
        $check_like_exists = CheckinsCommentsLikes::where('checkins_comments_id', $comment_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            //dd($check_like_exists);
            $check_like_exists->delete();
            log_user_activity('Checkin', 'commentunlike', $comment_id);
            // notify($author_id, 'status_commentunlike', $comment_id);

            $data['status'] = 'no';
        } else {
            $like = new CheckinsCommentsLikes;
            $like->checkins_comments_id = $comment_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Checkin', 'commentlike', $comment_id);
            // notify($author_id, 'status_commentlike', $comment_id);

            $data['status'] = 'yes';
        }
        $data['count'] = count(CheckinsCommentsLikes::where('checkins_comments_id', $comment_id)->get());
        return json_encode($data);
    }


    public function getCommentLikeUsers(Request $request)
    {
        $comment_id = $request->comment_id;
        $user_id = Auth::guard('user')->user()->id;
        $str = '';
        $comment = PostsComments::find($comment_id);
        if ($comment->likes) {
            foreach ($comment->likes()->orderBy('created_at', 'DESC')->get() as $likes) {
                $disabled = '';
                if (count($likes->author->get_followers()->where('users_id', $likes->author->id)->where('followers_id', Auth::user()->id)->get()) > 0) {
                    $follow_btn = '<button class="btn btn-light-grey btn-bordered pcl-unfollow" data-id="' . $likes->author->id . '">Unfollow</button>';
                } else {
                    if ($likes->author->id == Auth::user()->id) {
                        $disabled = 'disabled';
                    }
                    $follow_btn = '<button class="btn btn-light-primary btn-bordered pcl-follow" data-id="' . $likes->author->id . '" ' . $disabled . '>Follow</button>';
                }
                $str .= '<div class="people-row">
                                <div class="main-info-layer">
                                    <div class="img-wrap">
                                        <img class="ava" src="' . check_profile_picture($likes->author->profile_picture) . '" alt="ava" style="width:50px;height:50px;">
                                    </div>
                                    <div class="txt-block">
                                        <div class="name">' . $likes->author->name . '</div>
                                    </div>
                                </div>
                                <div class="button-wrap">
                                    ' . $follow_btn . '
                                </div>
                            </div>';
            }
        }
        return json_encode(['count' => count($comment->likes), 'text' => $str]);
    }

    public function postCommentEdit(Request $request)
    {
        $pair = $request['pair'];
        $post_id = $request->post_id;
        $comment_id = $request->comment_id;

        $post_comment_edit = $request->text;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = Posts::find($post_id)->users_id;
        $post_comment = PostsComments::find($comment_id);

        $user_name = Auth::guard('user')->user()->name;

        $is_files = false;
        $file_lists = $this->getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;
        $post_comment_edit = is_null($post_comment_edit) ? '' : $post_comment_edit;
        if (!$is_files && $post_comment_edit == "")
            return 0;

        $text = convert_string($post_comment_edit);

        $post = Posts::find($post_id);
        if (is_object($post)) {
            $post_comment->text = $text;
            if ($post_comment->save()) {
                log_user_activity('Status', 'comment', $post_id);
                if ($user_id != $post_comment->users_id) {
                    notify($author_id, 'status_comment', $post_id);
                }

                if (isset($request->existing_media) && count($post_comment->medias) > 0) {
                    foreach ($post_comment->medias as $media_list) {
                        if (in_array($media_list->media->id, $request->existing_media)) {
                            $medias_exist = Media::find($media_list->medias_id);

                            $media_list->delete();

                            if ($medias_exist) {
                                $amazonefilename = explode('/', $medias_exist->url);
                                Storage::disk('s3')->delete('post-comment-photo/' . end($amazonefilename));

                                $medias_exist->delete();
                            }
                        }
                    }
                }

                $this->uploadCommentMedia($file_lists, $post_comment, $text);

                $post_comment->load('medias');

                if ($request->comment_type == 1) {
                    return view('site.home.partials.post_comment_block', array('comment' => $post_comment, 'post' => $post));
                } else {
                    return view('site.home.partials.post_comment_reply_block', array('child' => $post_comment, 'post' => $post));
                }
            } else {
                return 0;
            }
        }
    }

    public function updatePostPermissionByPostId(Request  $request)
    {
        if (isset($request->post_id) && isset($request->type)) {
            $update = Posts::whereId($request->post_id)->update(['permission' => $request->type]);
            if ($update == 1)
                return 1; //successfull updation
            else
                return 2; //no change detect
        } else
            return 0; //field missing

    }
    public function createNewPost(Request $request)
    {

        $data = $request->all();
        if (isset($data['post_id']) && isset($data['post_type'])) {
            if ($data['post_type'] != 'discussion') {
                return $this->saveSimplePostComment($data);
            } else {
                return $this->saveDiscussionComment($data);
            }
        }
    }

    /**
     * Get more comments by type.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getMoreComment(Request $request)
    {
        $post_id = $request->post_id;
        $post_type = $request->post_type;
        $post_skip = $request->skip;
        $post_count = 0;

        $page = $request->pagenum;
        $skip = ($page - 1) * 10 + $post_skip;
        $return_view = false;

        if ($post_type != 'discussion') {
            $post_comments = PostsComments::where('posts_id', $post_id)->where('type', $post_type)->whereNull('parents_id')->orderby('created_at', 'DESC')->skip($skip)->take(10)->get();

            if (count($post_comments) > 0) {
                foreach ($post_comments as $post_comment) {
                    if (!user_access_denied($post_comment->author->id)) {
                        $return_view .= view('site.home.partials.new-post_comment_block', ['post_id' => $post_id, 'type' => $post_type, 'comment' => $post_comment]);
                    }
                }

                $post_count = count($post_comments);
            }
        } else {
            $replies = DiscussionReplies::where('discussions_id', $post_id)->orderby('created_at', 'DESC')->skip($skip)->take(10)->get();

            if (count($replies) > 0) {
                foreach ($replies as $reply) {
                    $return_view .= view('site.discussions.partials._reply-block', ['reply' => $reply, 'is_single_page' => true]);
                }

                $post_count = count($replies);
            }
        }

        return new JsonResponse(['view' => $return_view, 'count' => $post_count]);
    }



    private function saveSimplePostComment($data)
    {
        $post_id = $data['post_id'];
        $comment_id = @$data['child'];

        $text = $data['text'];
        $user_id = Auth::guard('user')->user()->id;

        if ($data['post_type'] === 'report') {
            $report = Reports::find($post_id);
            //            $author_id = $report->users_id;
            $post_id = $report->id;
        } else {
            //            $author_id = Posts::find($post_id)->users_id;
        }

        if (isset($data['edit_comment'])) {
            $comment = PostsComments::find($data['comment_id']);
            if (isset($data['pair'])) {
                $fileLists = getTempFiles($data['pair']);

                $this->uploadCommentMedia($fileLists, $comment, $text);
            }

            if (isset($data['existing_media'])) {
                foreach ($data['existing_media'] as $mediaId) {
                    $comment->medias()->where('medias_id', $mediaId)->delete();
                }
            }
            $comment->text = $text;
            $comment->save();

            return view::make('site.home.partials.new-post_comment_block', ['post_id' => $post_id, 'type' => $data['post_type'], 'comment' => $comment])->render();
        }

        if (isset($comment_id))
            $post_comment = PostsComments::find($comment_id);
        $text = is_null($text) ? '' : $text;

        $file_lists = [];
        if (isset($data['pair'])) {
            $file_lists = getTempFiles($data['pair']);
        }

        $is_files = count($file_lists) > 0 ? true : false;

        if (!$is_files && $text == "")
            return 0;

        $text = convert_string($text);

        $new_comment = new PostsComments;
        $new_comment->users_id = $user_id;
        if (isset($comment_id) && isset($post_comment))
            $new_comment->parents_id = $post_comment->id;
        $new_comment->posts_id = $post_id;
        $new_comment->text = $text;
        $new_comment->type = $data['post_type'];
        if ($new_comment->save()) {
            log_user_activity('Status', 'comment', $post_id);
            if (in_array($data['post_type'], CommonConst::postTypes())) {
                updatePostRanks($post_id, $data['post_type'], CommonConst::ACTION_COMMENT);
            }
            //            if($user_id != $author_id) {
            //                // notify($author_id, 'status_comment', $post_id);
            //            }

            $this->uploadCommentMedia($file_lists, $new_comment, $text);
            if (isset($comment_id))
                return view('site.home.partials.post_comment_reply_block', array('child' => $new_comment));
            else
                return view::make('site.home.partials.new-post_comment_block', ['post_id' => $post_id, 'type' => $data['post_type'], 'comment' => $new_comment])->render();
        } else {
            return 0;
        }
    }


    private function saveTripPlaceComments($data)
    {
        $post_id = $data['post_id'];
        $comment_id = @$data['child'];

        $text = $data['text'];
        $user_id = Auth::guard('user')->user()->id;
        $author_id = TripPlaces::find($post_id)->users_id;
        if (isset($comment_id))
            $post_comment = TripPlacesComments::find($comment_id);
        $user_name = Auth::guard('user')->user()->name;
        $text = is_null($text) ? '' : $text;

        $is_files = false;
        $file_lists = getTempFiles(isset($data['pair']) ? $data['pair'] : '');

        $is_files = count($file_lists) > 0 ? true : false;

        if (!$is_files && $text == "")
            return 0;

        $text = convert_string($text);

        $post = TripPlaces::find($post_id);
        if (is_object($post)) {
            $new_comment = new TripPlacesComments();
            $new_comment->user_id = $user_id;
            if (isset($comment_id) && isset($post_comment))
                $new_comment->reply_to = $post_comment->id;
            $new_comment->trip_place_id = $post_id;
            $new_comment->comment = $text;
            if ($new_comment->save()) {
                log_user_activity('Status', 'comment', $post_id);
                $this->rankingService->addPointsToEarners($new_comment->tripPlace);

                if (isset($file_lists)) {
                    foreach ($file_lists as $file) {
                        $filename = $user_id . '_' . time() . '_' .  preg_replace("/([#]|[%]|[+]|[$])/", "_", $file[1]);
                        Storage::disk('s3')->put('trip-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                        $path = 'https://s3.amazonaws.com/travooo-images2/trip-comment-photo/' . $filename;

                        $media = new Media();
                        $media->url = $path;
                        $media->author_name = $user_name;
                        $media->title = $text;
                        $media->author_url = '';
                        $media->source_url = '';
                        $media->license_name = '';
                        $media->license_url = '';
                        $media->uploaded_at = date('Y-m-d H:i:s');
                        $media->save();

                        $posts_comments_medias = new TripsCommentsMedias();
                        $posts_comments_medias->trips_comments_id = $new_comment->id;
                        $posts_comments_medias->medias_id = $media->id;
                        $posts_comments_medias->save();
                    }
                }
                if (isset($comment_id))
                    return view('site.home.new.partials.trip_place_reply_block', array('child' => $new_comment, 'trip_plan' => $post));
                else
                    return view::make('site.home.new.partials.trip_place-comment', array('comment' => $new_comment, 'trip_plan' => $post))->render();
                // dd($return );
            } else {
                return 0;
            }
        }
    }

    private function saveDiscussionComment($data)
    {
        $discussion_id = $data['post_id'];
        $discussion = Discussion::find($discussion_id);
        $text = @$data['text'];
        $user_id = Auth::guard('user')->user()->id;
        $view = '';
        //$user_id = 1;

        $text = is_null($text) ? '' : $text;

        $is_files = false;
        $file_lists = getTempFiles(isset($data['pair']) ? $data['pair'] : '');

        $is_files = count($file_lists) > 0 ? true : false;

        if (!$is_files && $text == "")
            return 0;

        $text = convert_string($text);

        $reply = new DiscussionReplies;
        $reply->discussions_id = $discussion->id;
        $reply->users_id = $user_id;
        $reply->type = $discussion->type;
        $reply->reply = $text;
        $reply->num_upvotes = 0;
        $reply->num_downvotes = 0;
        $reply->num_views = 0;
        $reply->num_follows = 0;
        if (isset($data['child'])) {
            $reply->parents_id = $data['child'];
        }

        if (isset($file_lists)) {
            foreach ($file_lists as $file) {
                $filename = $user_id . '_' . time() . '_' .  preg_replace("/([#]|[%]|[+]|[$])/", "_", $file[1]);
                Storage::disk('s3')->put('replies_media/' . $filename, fopen($file[0], 'r+'),  'public');

                $path = 'https://s3.amazonaws.com/travooo-images2/replies_media/' . $filename;

                $reply->medias_type = 'image';
                $reply->medias_url = $path;
            }
        }

        if ($reply->save()) {

            $this->discussionsService->updatePopularCount($discussion->id);

            $_ = ['reply' => $reply];
            if (isset($data['child'])) {
                $_['is_sub'] = true;
            }

            $_['is_single_page'] = true;


            $view .= view('site.discussions.partials._reply-block', $_);
            log_user_activity('Discussion', 'reply', $discussion->id);


            updatePostRanks($discussion->id, CommonConst::TYPE_DISCUSSION, CommonConst::ACTION_REPLY);
        }
        echo $view;
    }
    public function deletePost(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        if (isset($id) && isset($type)) {

            if ($type == 'review') {
                Reviews::where('id', $id)->delete();
                $activites = ActivityLog::where(['variable' => $id, 'action' => $type])->delete();
            } else {
                $activites = ActivityLog::where(['variable' => $id, 'type' => $type])->delete();
            }

            return $activites;
        }
        return 0;
    }
    public function getSharePost(Request $request)
    {
        $id = $request->id;
        $type = $request->type;

        if (!isset($id) || !isset($type)) {
            return 0;
        }
        $return = '';
        if ($type == 'post') {
            $data['type'] = 'Post';
            $data['variable'] = $id;
            return  view('site.home.new.partials.shareable-text', ['post' => $data]);
        } else if ($type == 'discussion') {
            $data['type'] = 'Discussion';
            $data['variable'] = $id;
            return  view('site.home.new.partials.shareable-discussion', ['post' => $data]);
        } else if ($type == 'trip') {
            $data['type'] = 'Trip';
            $data['variable'] = $id;
            return  view('site.home.new.partials.shareable-tripplan', ['post' => $data]);
        } else if ($type == 'event') {
            $data['type'] = 'Event';
            $data['variable'] = $id;
            return  view('site.home.new.partials.shareable-event', ['post' => $data]);
        } else if ($type == 'report') {
            $data['type'] = 'Report';
            $data['variable'] = $id;
            return  view('site.home.new.partials.shareable-report', ['post' => $data]);
        } else if ($type == 'review') {
            $data['type'] = 'review';
            $data['variable'] = $id;
            return  view('site.home.new.partials.shareable-rating', ['post' => $data]);
        }
    }

    public function sharePost(Request $request)
    {
//        dd($request->all());
        $post_id = $request->id;
        $post_text =  $request->text;
        $post_type =  $request->type;
        if (!$post_id && !$post_type)
            return json_encode(['status' => 0, 'messages' => 'Invalid Request']);
        $user_id = Auth::guard('user')->user()->id;
        $data = array();

        if (PostsShares::where('users_id', $user_id)->where('type', $post_type)->where('posts_type_id', $post_id)->exists()) {
            $data['status'] = 0;
            $data['messages'] = "You have already shared!";
            return json_encode($data);
        }

        //create Sharing Post
        $newPost = Posts::create(['users_id' =>  $user_id, 'text' => $post_text, 'permission' => isset($request->permission) ? $request->permission : 0]);

        //end sharing post
        $share = new PostsShares;
        $share->posts_id = $newPost->id;
        $share->users_id = $user_id;
        $share->type =  $post_type;
        $share->posts_type_id =  $post_id;
        $share->text = @$request->text;
        if ($share->save()) {
            log_user_activity('Share', $request->type, $share->id);
            if (in_array($request->type, CommonConst::postTypes())) {
                updatePostRanks($post_id, $request->type, CommonConst::ACTION_SHARE);
            }
            $data['count'] = count(PostsShares::where('posts_type_id', $post_id)->get());
            $data['status'] = 1;

            if ($post_type == 'discussion') {
                $this->discussionsService->updatePopularCount($post_id);
            }

            return json_encode($data);
        }
        return  0;
    }
    public function createDummyPost(Request $request)
    {

        $type = $request->type;
        if (!$type) {
            return 'City Country or place param is missin';
        }
        $id = $request->id;
        if (!$id) {
            return 'ID is missing';
        }
        $users = User::get()->shuffle()->take(10);
        $commentsUsers = User::whereNotIn('id', $users->pluck('id')->toArray())->get()->shuffle()->take(10);
        $likesUsers = User::whereNotIn('id', $users->pluck('id')->toArray())->get()->shuffle()->take(10);

        if ($type == 'city') {
            $city = Cities::find($id);
            if (is_object($city)) {
                foreach ($users as $user) {

                    $post = new Posts();
                    $post->users_id = $user->id;
                    $post->permission = 0;
                    $post->text  = 'This is a dummy post by ' . @$user->name;

                    if ($post->save()) {
                        $postplacecity = new PostsCities;
                        $postplacecity->posts_id = $post->id;
                        $postplacecity->cities_id = $city->id;
                        $postplacecity->save();

                        //consective CountryPost
                        $postplacecountry = new PostsCountries;
                        $postplacecountry->posts_id = $post->id;
                        $postplacecountry->countries_id = $city->countries_id;
                        $postplacecountry->save();
                    }
                    //posts Comments
                    foreach ($commentsUsers as $c_user) {
                        $new_comment = new PostsComments;
                        $new_comment->users_id = $c_user->id;
                        $new_comment->posts_id = $post->id;
                        $new_comment->text = 'This is comment by' . @$c_user->name;
                        $new_comment->type = 'post';
                        $new_comment->save();
                    }
                    //posts likes
                    foreach ($likesUsers as $l_users) {
                        $like = new PostsLikes;
                        $like->posts_id = $post->id;
                        $like->users_id = $l_users->id;
                        $like->save();
                    }
                }
            } else {
                return 'City not exist try another ID';
            }
        } else if ($type == 'country') {
            $country = Countries::find($id);
            if (is_object($country)) {
                foreach ($users as $user) {

                    $post = new Posts();
                    $post->users_id = $user->id;
                    $post->permission = 0;
                    $post->text  = 'This is a dummy post by ' . @$user->name;

                    if ($post->save()) {

                        //consective CountryPost
                        $postplacecountry = new PostsCountries;
                        $postplacecountry->posts_id = $post->id;
                        $postplacecountry->countries_id = $country->id;
                        $postplacecountry->save();
                    }
                    //posts Comments
                    foreach ($commentsUsers as $c_user) {
                        $new_comment = new PostsComments;
                        $new_comment->users_id = $c_user->id;
                        $new_comment->posts_id = $post->id;
                        $new_comment->text = 'This is comment by' . @$c_user->name;
                        $new_comment->type = 'post';
                        $new_comment->save();
                    }
                    //posts likes
                    foreach ($likesUsers as $l_users) {
                        $like = new PostsLikes;
                        $like->posts_id = $post->id;
                        $like->users_id = $l_users->id;
                        $like->save();
                    }
                }
            } else {
                return 'Country not exist try another ID';
            }
        } else if ($type == 'place') {
            $place = Place::find($id);
            if (is_object($place)) {
                foreach ($users as $user) {

                    $post = new Posts();
                    $post->users_id = $user->id;
                    $post->permission = 0;
                    $post->text  = 'This is a dummy post by ' . @$user->name;

                    if ($post->save()) {

                        $postplace = new PostsPlaces;
                        $postplace->posts_id = $post->id;
                        $postplace->places_id = $place->id;
                        $postplace->save();
                        //consective city posts
                        $postplacecity = new PostsCities;
                        $postplacecity->posts_id = $post->id;
                        $postplacecity->cities_id = $place->cities_id;
                        $postplacecity->save();

                        //consective CountryPost
                        $postplacecountry = new PostsCountries;
                        $postplacecountry->posts_id = $post->id;
                        $postplacecountry->countries_id = $place->countries_id;
                        $postplacecountry->save();
                    }
                    //posts Comments
                    foreach ($commentsUsers as $c_user) {
                        $new_comment = new PostsComments;
                        $new_comment->users_id = $c_user->id;
                        $new_comment->posts_id = $post->id;
                        $new_comment->text = 'This is comment by' . @$c_user->name;
                        $new_comment->type = 'post';
                        $new_comment->save();
                    }
                    //posts likes
                    foreach ($likesUsers as $l_users) {
                        $like = new PostsLikes;
                        $like->posts_id = $post->id;
                        $like->users_id = $l_users->id;
                        $like->save();
                    }
                }
            } else {
                return 'Place not exist try another ID';
            }
        } else {
            return 'wrong type passed';
        }
    }

    private function uploadCommentMedia($fileLists, $comment, $text)
    {
        foreach ($fileLists as $file) {
            $filename = Auth::user()->id . '_' . time() . '_' .  preg_replace("/([#]|[%]|[+]|[$])/", "_", $file[1]);
            Storage::disk('s3')->put('post-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

            $path = 'https://s3.amazonaws.com/travooo-images2/post-comment-photo/' . $filename;

            $media = new Media();
            $media->url = $path;
            $media->author_name = Auth::user()->name;;
            $media->title = $text;
            $media->author_url = '';
            $media->source_url = '';
            $media->license_name = '';
            $media->license_url = '';
            $media->uploaded_at = date('Y-m-d H:i:s');
            $media->save();

            $posts_comments_medias = new PostsCommentsMedias();
            $posts_comments_medias->posts_comments_id = $comment->id;
            $posts_comments_medias->medias_id = $media->id;
            $posts_comments_medias->save();
        }
        deleteUploadedTempFiles($fileLists);
    }

    public function deleteUploadedTempFiles(Request $request)
    {
        deleteUploadedTempFiles($this->getTempFiles($request->get('pair')), $request->get('name'));

        return new JsonResponse(['success' => true]);
    }
}
