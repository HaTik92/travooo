<?php

namespace App\Http\Controllers\PlaceChat;

use App\Http\Responses\AjaxResponse;
use App\Services\PlaceChat\PlaceChatsService;
use Illuminate\Http\Request;

class PlaceChatsController
{
    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return \Illuminate\Http\Response
     */
    public function ajaxGetPlaceChatMessages(Request $request, PlaceChatsService $placeChatsService)
    {
        $chatId = $request->get('chat_id');

        if (!$chatId) {
            return AjaxResponse::create([], false);
        }

        return AjaxResponse::create($placeChatsService->getPlaceChatMessages($chatId));
    }

    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return \Illuminate\Http\Response
     */
    public function ajaxGetPlaceChatUsers(Request $request, PlaceChatsService $placeChatsService)
    {
        $chatId = $request->get('chat_id');

        if (!$chatId) {
            return AjaxResponse::create([], false);
        }

        return AjaxResponse::create($placeChatsService->getPlaceChatParticipants($chatId));
    }

    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return \Illuminate\Http\Response
     */
    public function ajaxGetPlaceUsersToChat(Request $request, PlaceChatsService $placeChatsService)
    {
        $planId = $request->get('plan_id');
        $placeId = $request->get('place_id');

        if (!$placeId || !$planId) {
            return AjaxResponse::create([], false);
        }

        return AjaxResponse::create($placeChatsService->getPlaceUsersToChat($planId, $placeId));
    }

    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return \Illuminate\Http\Response
     */
    public function ajaxCreatePlaceChatMessage(Request $request, PlaceChatsService $placeChatsService)
    {
        $chatId = $request->get('chat_id');
        $message = $request->get('message');
        $placeId = $request->get('place_id');
        $planId = $request->get('plan_id');

        //todo request  validation
        if (!$chatId
            || !$planId
            || !$message
            || !$message = $placeChatsService->createPlaceChatMessage($chatId, $message, $planId, $placeId)) {
            return AjaxResponse::create([], false);
        }

        return AjaxResponse::create($message);
    }

    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return \Illuminate\Http\Response
     */
    public function ajaxGetPlanUsersToChat(Request $request, PlaceChatsService $placeChatsService)
    {
        $planId = $request->get('plan_id');

        if (!$planId) {
            return AjaxResponse::create([], false);
        }

        return AjaxResponse::create($placeChatsService->getPlanUsersToChat($planId));
    }

    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return \Illuminate\Http\Response
     */
    public function ajaxGetPlanPlaceUsersToChat(Request $request, PlaceChatsService $placeChatsService)
    {
        $planId = $request->get('plan_id');

        if (!$planId) {
            return AjaxResponse::create([], false);
        }

        return AjaxResponse::create($placeChatsService->getPlanPlaceUsersToChat($planId));
    }

    public function ajaxGetActualChats(Request $request, PlaceChatsService $placeChatsService)
    {
        $planId = $request->get('plan_id');
        $edit = $request->get('edit');

        if (!$planId) {
            return AjaxResponse::create([], false);
        }

        return $placeChatsService->renderActualChats($planId, $edit);
    }

    public function ajaxReadMessage(Request $request, PlaceChatsService $placeChatsService)
    {
        $messageId = $request->get('message_id');

        if (!$messageId) {
            return AjaxResponse::create([], false);
        }

        return AjaxResponse::create($placeChatsService->read($messageId));
    }
}