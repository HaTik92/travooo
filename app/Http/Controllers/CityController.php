<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City\Cities;
use App\Models\City\CitiesFollowers;
use App\Models\City\CitiesMedias;
use App\Models\City\CitiesShares;
use App\Models\City\CitiesAbouts;
use App\Models\ActivityLog\ActivityLog;
use App\Models\User\UsersMedias;
use App\Models\ActivityMedia\Media;

use Illuminate\Support\Facades\Auth;
use App\Models\Access\Language\Languages;
use App\Models\Country\Countries;
use App\Models\Place\Place;
use App\Models\Place\PlaceTranslations;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Posts\Checkins;
use App\Models\Posts\PostsCheckins;
use Illuminate\Support\Facades\Input;

use App\Models\TravelMates\TravelMatesRequests;
use App\Models\Reports\ReportsInfos;
use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Reports\Reports;
use App\Models\Reviews\Reviews;
use App\Models\Reviews\ReviewsVotes;
use App\Models\Reviews\ReviewsShares;

use App\Models\TripPlans\TripPlans;
use App\Models\TripPlaces\TripPlaces;
use App\Models\Posts\PostsCities;
use App\Models\Posts\PostsCountries;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsMedia;
use App\Models\Events\Events;
use App\Models\Events\EventsComments;
use App\Models\Events\EventsCommentsMedias;
use App\Models\Events\EventsCommentsLikes;
use App\Models\Events\EventsLikes;
use App\Models\Events\EventsShares;
use App\Models\User\User;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use Carbon\Carbon;

use Intervention\Image\ImageManagerStatic as Image;

use GuzzleHttp\Client;

class CityController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth:user');
    }

    public function _create_thumbs($media)
    {

        $complete_url = $media->url;
        $url = explode("/", $complete_url);

        $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

        $options = [
            'region' => 'us-east-1',
            'version' => 'latest',
            'http' => ['verify' => false],
            'credentials' => $credentials,
            'endpoint' => 'https://s3.amazonaws.com'
        ];

        $s3Client = new S3Client($options);
        //$s3 = AWS::createClient('s3');
        $result = $s3Client->getObject([
            'Bucket' => 'travooo-images2', // REQUIRED
            'Key' => $complete_url, // REQUIRED
            'ResponseContentType' => 'text/plain',
        ]);

        $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        //return $img_700->response('jpg');
        //echo $complete_url . '<br />';
        //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

        $put_1100 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_1100->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_700 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_700->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_230 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_230->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_180 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_180->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);


        $media->thumbs_done = 1;
        $media->save();
    }

    private function _getRandom($key)
    {

        $init_array = [];
        for ($i = 1; $i < 32; $i++) $init_array[] = $i;

        $return = [];

        foreach ($init_array as $val) {
            $return[] = $val * $key % 32;
        }

        return implode(',', $return);
    }

    public function getIndex($city_id)
    {
        $language_id = 1;
        $city = Cities::find($city_id);

        $random_val = $this->_getRandom(date('j'));

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $data['search_in'] = $city;

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
            'trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'languages_spoken.trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'holidays.trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'religions.trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'currencies.trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'emergency.trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },

            'atms' => function ($query) {
                $query->where('place_type', 'like', '%atm%');
            },
            'hotels',
            'getMedias',
            'getMedias.users',
            'timezone',
            'followers'
        ])
            ->where('id', $city_id)
            ->where('active', 1)
            ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }
        $ip = getClientIP();
        $geoLoc = ip_details($ip);
        $getLocCity = $geoLoc['city'];
        $data['myCity'] = $getLocCity;
        $db_place_reviews = Reviews::where('cities_id', $city->id)->orderBy('created_at', 'desc')->get();
        $data['reviews'] = $db_place_reviews;
        $data['reviews_places'] = Reviews::where('cities_id', $city->id)->get();

        $data['reviews_avg'] = Reviews::where('cities_id', $city->id)->whereNull('google_author_url')->avg('score');

        $data['place'] = $city;

        // $list_trip_plan = TripPlaces::where('cities_id', $city->id)->pluck('trips_id');
        if (Auth::check()) {
            $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)
                //    ->whereNotIn('id', $list_trip_plan)
                ->get();

            $data['me'] = Auth::guard('user')->user();
        }

        $data['travelmates'] = TravelMatesRequests::whereHas('plan_city', function ($q) use ($city) {
            $q->where('cities_id', '=', $city->id);
        })
            ->groupBy('users_id')
            ->get();

        $data['reportsinfo'] = [];
        $data['reports'] = Reports::where('cities_id', '=', $city->id)->where('flag', '=', 1)
            ->get();

        $data['pposts'] = '';

        $data['place_discussions'] = Discussion::where('destination_type', 'City')
            ->where('destination_id', $city->id)
            ->get();

        // get experts into categories
        $experts_all = $experts_top = $experts_local = $experts_friends = $live_checkins = array();
        foreach ($city->experts as $key => $pce) {
            $experts_all[] = $pce;
            if (Auth::check() && is_object($pce->user) && is_object($data['me'])) {
                if ($pce->user->nationality != $data['me']->nationality) {
                    $experts_top[] = $pce;
                } elseif ($pce->user->nationality == $data['me']->nationality) {
                    $experts_local[] = $pce;
                }
            }
            if (is_object($pce->user)) {
                if (Auth::check() && is_friend($pce->user->id)) {
                    $experts_friends[] = $pce;
                }
            }
        }
        $data['experts_top'] = $experts_top;
        $data['experts_local'] = $experts_local;
        $data['experts_friends'] = $experts_friends;
        $data['all_expert'] = $experts_all;
        $data['live_checkins'] = $city->checkins()->groupBy('users_id')->get();

        $experts_ids = array();
        foreach ($experts_all as $ea) {
            $experts_ids[] = $ea->users_id;
        }

        // Select *,count(*) as r, sum(A.replies) from 
        // (SELECT created_at, users_id, count(*) as replies FROM `discussion_replies` GROUP BY YEAR(created_at), Month(created_at), Day(created_at)) 
        // as A GROUP BY A.users_id ORDER BY `users_id`
        $data['place_top_contributors'] = [];
        if (count($experts_ids) > 0) {
            $last_month = date("Y-m-d", strtotime('-180 days'));

            $sub = DiscussionReplies::selectRaw('*, count(*) as reps, sum(num_views) as views')
                ->whereRaw('users_id in (' . implode(',', $experts_ids) . ')')
                ->whereRaw('created_at >' . $last_month)
                ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'));

            $data['place_top_contributors'] = DiscussionReplies::from(DB::raw('(' . $sub->toSql() . ') as a'))
                ->selectRaw('*, count(*) as count, sum(a.reps) as reply_cnt, sum(a.views) as views')
                ->groupBy('a.users_id')
                ->orderBy('count', 'desc')
                ->take(10)
                ->get();
        }


        $follow_list = [];

        if (Auth::check()) {
            $tp_array = getTopPlacesByUserPref(['city' => $city->id]);
            $p = Auth::user()->id;
            $follows_places = DB::select('SELECT * FROM `places_followers` WHERE users_id   =' . $p);

            foreach ($follows_places as $placex) {
                $follow_list[$placex->places_id] = $placex->places_id;
            }
        } else {
            $current_user_loc_info = ip_details(getClientIP());
            $current_country = Countries::where('iso_code', $current_user_loc_info['iso_code'])->first();

            $tp_array = getTopPlacesByUserPref(['city' => $city->id], $current_country->id);

            $data['current_country_id'] = $current_country->id;
        }

        $tp_array = array_diff($tp_array, array_values($follow_list));
        $data['top_places'] = PlacesTop::whereIn('places_id', $tp_array)
            ->whereHas('place', function ($query) {
                $query->whereHas('medias');
            })
            ->whereNotIn('country_id', [373, 326])
            ->inRandomOrder()
            ->limit(6)
            ->get();
        $tp_array = array_merge($tp_array, $follow_list);

        if (count($data['top_places']) < 6) {
            $remaining = 6 - count($data['top_places']);
            $count_remaing = PlacesTop::whereNotIn('places_id', $tp_array)
                ->whereHas('place', function ($query) {
                    $query->whereHas('medias');
                })->where('city_id', $city->id)
                ->whereNotIn('country_id', [373, 326])
                ->inRandomOrder()
                ->limit($remaining)
                ->get();

            $data['top_places'] = $data['top_places']->merge($count_remaing);
        }
        // $data['top_places'] = PlacesTop::leftJoin('places', 'places_top.places_id', '=', 'places.id')
        //             ->where('places.cities_id', $city->id)
        //             ->orderByRaw('FIELD(places_top.id % 32, '. $random_val . ')')
        //             ->take(4)
        //             ->get();


        //dd($data['top_places']);
        // $checkins = Checkins::where('city_id', $city->id)->orderBy('id', 'DESC')->get();

        // $result = array();
        // $done = array();
        // foreach ($checkins AS $checkin) {
        //     if (!isset($done[$checkin->post_checkin->post->author->id])) {
        //         $result[] = $checkin;
        //         $done[$checkin->post_checkin->post->author->id] = true;
        //     }
        // }
        $data['live_checkins'] = $city->checkins()->groupBy('users_id')->whereDate('checkin_time', '=', Carbon::today()->toDateString())->get();

        //$data['checkins'] = $result;
        $live_users = [];
        foreach ($data['live_checkins'] as $live_here) {
            $live_users[] = $live_here->users_id;
        }
        $temp_live_users =  User::whereIn('id', $live_users)->get();
        $live_users = [];
        foreach ($temp_live_users as $user) {
            $is_followed =  app('App\Http\Controllers\ProfileController')->postCheckFollow($user->id);
            $live_users[$user->id] = ['users' => $user, 'is_followed' => $is_followed['success']];
        }
        $data['live_users'] = $live_users;
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));

        $data['trip_plans_collection'] = TripPlans::query()
            ->whereHas('trip_places_by_active_version', function ($q) use ($city) {
                $q->where('cities_id', $city->id);
            })
            ->orderByDesc('id')
            ->with('trip_places_by_active_version')
            ->get();

        $data['place_type'] = 'city';

        return view('site.place2.index', $data);
    }

    public function postFollow($cityId)
    {
        $userId = Auth::guard('user')->user()->id;
        $city = Cities::find($cityId);
        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }


        $follower = CitiesFollowers::where('cities_id', $cityId)
            ->where('users_id', $userId)
            ->first();

        if ($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that City',
                ],
                'success' => false
            ];
        }

        $city->followers()->create([
            'users_id' => $userId
        ]);

        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('City', 'follow', $cityId);

        $count = CitiesFollowers::where('cities_id', $cityId)->count();

        return [
            'success' => true,
            'count' => $count
        ];
    }

    public function postUnFollow($placeId)
    {

        $userId = Auth::guard('user')->user()->id;

        $place = Cities::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $placeId)
            ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this City.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();

            log_user_activity('City', 'unfollow', $placeId);
        }

        $count = CitiesFollowers::where('cities_id', $placeId)->count();

        return [
            'success' => true,
            'count' => $count
        ];
    }

    public function postCheckFollow($placeId)
    {

        $userId = Auth::guard('user')->user()->id;
        $place = Cities::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $placeId)
            ->where('users_id', $userId)
            ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    public function postCheckin($placeId)
    {

        $userId = Auth::guard('user')->user()->id;
        $place = Cities::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }


        $check = new Checkins();
        $postCheckedIn =     Input::get('checkin_time');
        if (isset($postCheckedIn)) {
            $check->checkin_time = date("Y-m-d", strtotime(Input::get('checkin_time')));
        }
        $check->users_id = $userId;
        $check->city_id = $placeId;
        $check->location = $place->transsingle->title;
        $check->lat_lng = $place->lat . ',' . $place->lng;
        $check->save();

        log_user_activity('City', 'checkin', $placeId);

        if (strtotime($check->checkin_time) == strtotime(date('Y-m-d'))) {
            return [
                'success' => true,
                'live' => 1,
                'checkin_id' => $check->id,
                'message' => 'Thank you for checking in! You are live on this page now.'
            ];
        } else if (strtotime($check->checkin_time) < strtotime(date('Y-m-d'))) {
            return [
                'success' => true,
                'live' => 0,
                'checkin_id' => $check->id,
                'message' => 'Thank you for recording your past check-in!'
            ];
        } else if (strtotime($check->checkin_time) > strtotime(date('Y-m-d'))) {
            return [
                'success' => true,
                'live' => 0,
                'checkin_id' => $check->id,
                'message' => 'Thank you for checking in! You will appear live on this page on your check-in date.'
            ];
        }
    }

    public function postCheckout($placeId)
    {

        $userId = Auth::guard('user')->user()->id;

        $place = Cities::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $placeId)
            ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this City.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();

            log_user_activity('City', 'checkout', $placeId);
        }

        return [
            'success' => true
        ];
    }

    public function postCheckCheckin($placeId)
    {

        $userId = Auth::guard('user')->user()->id;
        $place = Cities::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $checkin = Checkins::where('city_id', $placeId)
            ->where('users_id', $userId)
            ->first();

        return empty($checkin) ? ['success' => false] : ['success' => true];
    }

    public function postTalkingAbout($placeId)
    {

        $place = Cities::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $shares = $place->shares;

        foreach ($shares as $share) {
            $data['shares'][] = array(
                'user_id' => $share->user->id,
                'user_profile_picture' => check_profile_picture(@$share->user->profile_picture)
            );
        }

        $data['num_shares'] = count($shares);
        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function postNowInPlace($placeId)
    {

        $place = Cities::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $checkins = Checkins::where('city_id', $place->id)
            ->orderBy('id', 'DESC')
            ->take(5)
            ->get();

        $result = array();
        $done = array();
        foreach ($checkins as $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                if (time() - strtotime($checkin->post_checkin->post->date) < (24 * 60 * 60)) {
                    $live = 1;
                } else {
                    $live = 0;
                }
                $result[] = array(
                    'name' => $checkin->post_checkin->post->author->name,
                    'id' => $checkin->post_checkin->post->author->id,
                    'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                    'live' => $live
                );
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['live_checkins'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function ajaxPostReview($placeId)
    {

        $place = Cities::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        if (Input::has('text')) {
            $review_text = Input::get('text');
            $review_score = Input::get('score');
            $review_author = Auth::guard('user')->user();
            $review_place_id = $place->id;

            $review = new Reviews;
            $review->cities_id = $review_place_id;
            $review->by_users_id = $review_author->id;
            $review->score = $review_score * 1;
            $review->text = $review_text;

            if ($review->save()) {
                if (Input::has('mediafiles')) {
                    foreach (Input::get('mediafiles') as $media) {
                        if ($media) {
                            $userId = Auth::guard('user')->user()->id;
                            $userName = Auth::guard('user')->user()->name;
                            $media_url = $media;

                            $media_model = new Media();
                            $media_model->url = $media_url;
                            $media_model->users_id = $userId;
                            $media_model->type  = getMediaTypeByMediaUrl($media_model->url);
                            $media_model->author_name = $userName;
                            $media_model->title = Input::get('text');
                            $media_model->author_url = '';
                            $media_model->source_url = '';
                            $media_model->license_name = '';
                            $media_model->license_url = '';
                            $media_model->uploaded_at = date('Y-m-d H:i:s');
                            $media_model->save();

                            $users_medias = new UsersMedias();
                            $users_medias->users_id = $userId;
                            $users_medias->medias_id = $media_model->id;
                            $users_medias->save();

                            $posts_medias = new ReviewsMedia();
                            $posts_medias->reviews_id = $review->id;
                            $posts_medias->medias_id = $media_model->id;
                            $posts_medias->save();

                            $places_medias = new CitiesMedias();
                            $places_medias->cities_id = $place->id;
                            $places_medias->medias_id = $media_model->id;
                            $places_medias->save();

                            $this->_create_thumbs($media_model);
                        }
                    }
                }

                log_user_activity('City', 'review', $place->id);
                $data['success'] = true;
                $data['id'] = $review->id;
                $data['posted_review'] = View::make('site.place2.partials.post_review', ['place' => $place, 'review' => $review])->render();
                $data['prepend'] = '<div class="comment" topsort="' . $review->score . '" newsort="' . strtotime($review->created_at) . '">
                            <img class="comment__avatar" src="' . check_profile_picture(Auth::guard('user')->user()->profile_picture) . '" alt="" style="width:45px;height:45px;">
                            <div class="comment__content" style="width: calc(100% - 44px)">
                                <div class="comment__header"><a class="comment__username" href="#">' . Auth::guard('user')->user()->name . '</a>
                                    <div class="comment__user-id"></div>
                                </div>
                                <div class="comment__text" style="overflow-wrap: break-word;margin-top:10px">' . $review_text . '</div>
                                <div class="comment__footer">
                                    <div class="rating">
                                    <svg class="icon icon--star ' . ($review->score >= 1 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 2 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 3 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 4 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 5 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <div class="rating__value"><strong>' . $review->score . '</strong> / 5</div>
                                    </div>
                                    <div class="comment__posted">' . diffForHumans($review->created_at) . '</div>
                                </div>
                            </div>
                        </div>';
            } else {
                $data['success'] = false;
            }
        } else {
            $data['success'] = false;
        }
        return json_encode($data);
    }

    public function ajaxPostMorePhoto($placeId, Request $request)
    {

        $language_id = 1;
        $num = $request->pagenum;
        $skip = ($num) * 6;
        $place = Cities::find($placeId);

        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $medias = $place->getMedias;

        if (!$medias) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }
        $data = array();
        $c = (count($medias) <= ($skip + 6)) ? count($medias) : $skip + 6;
        for ($i = $skip; $i < $c; $i++) {
            $data[] = '<img class="gallery__img" src="https://s3.amazonaws.com/travooo-images2/th230/' . @$medias[$i]->url . '" alt="#" title="" /> ';
        }
        return [
            'data' => $data,
            'success' => true
        ];
    }

    public function postAddPost($place_id, Request $request)
    {

        $post_data = array();
        foreach ($request->data as $data) {
            if (strpos($data["name"], "[]")) {
                if (!isset($post_data[$data["name"]]))
                    $post_data[$data["name"]] = [];

                $post_data[$data["name"]][] = $data["value"];
            } else {
                $post_data[$data["name"]] = $data["value"];
            }
        }

        $userId = Auth::guard('user')->user()->id;
        $userName = Auth::guard('user')->user()->name;

        $place = Cities::find($place_id);

        if (!$place) {
            echo "error";
        }

        $post = new Posts;
        $post->users_id = $userId;
        $post->text = $post_data["text"];
        $post->permission = $post_data["permission"];
        $post->save();
        $checkins = '';

        log_user_activity('City_Post', 'post', $post->id);
        //create post checkin
        $checkins = '';
        if (isset($post_data['checkinPost'])) {
            $postCheckin =  PostsCheckins::create(['checkins_id' => $post_data['checkinPost'], 'posts_id' => $post->id]);
            $checkins  = Checkins::find($postCheckin->checkins_id);
        }
        if ($post_data["type"] == "text") {

            $postplace = new PostsCities;
            $postplace->posts_id = $post->id;
            $postplace->cities_id = $place_id;
            $postplace->save();
            //create Country Post
            $postplacecountry = new PostsCountries;
            $postplacecountry->posts_id = $post->id;
            $postplacecountry->cities_id = $place_id;
            $postplacecountry->countries_id = $place->countries_id;
            $postplacecountry->save();
            //end creation post

            echo view('site.place2.partials.post_text', array("place" => $place, "post" => $post));
        } else if ($post_data["type"] == "image" || $post_data["type"] == "video") {
            $postplace = new PostsCities;
            $postplace->posts_id = $post->id;
            $postplace->cities_id = $place_id;
            $postplace->save();

            foreach ($post_data["mediafiles[]"] as $media) {
                if ($media) {
                    // list($baseType, $media) = explode(';', $media);
                    // list(, $media) = explode(',', $media);
                    // $media = base64_decode($media);

                    // if (strpos($baseType, 'image') !== false)
                    //     $filename = sha1(microtime()) . "_place_added_file.png";
                    // else if (strpos($baseType, 'video') !== false)
                    //     $filename = sha1(microtime()) . "_place_added_file.mp4";

                    // Storage::disk('s3')->put(
                    //     $userId . '/medias/' . $filename,
                    //     $media,
                    //     'public'
                    // );
                    $media_url = $media;

                    $media_model = new Media();
                    $media_model->url = $media_url;
                    $media_model->users_id = $userId;
                    $media_model->type  = getMediaTypeByMediaUrl($media_model->url);
                    $media_model->author_name = $userName;
                    $media_model->title = $post_data["text"];
                    $media_model->author_url = '';
                    $media_model->source_url = '';
                    $media_model->license_name = '';
                    $media_model->license_url = '';
                    $media_model->uploaded_at = date('Y-m-d H:i:s');
                    $media_model->save();

                    $users_medias = new UsersMedias();
                    $users_medias->users_id = $userId;
                    $users_medias->medias_id = $media_model->id;
                    $users_medias->save();

                    $posts_medias = new PostsMedia();
                    $posts_medias->posts_id = $post->id;
                    $posts_medias->medias_id = $media_model->id;
                    $posts_medias->save();

                    $places_medias = new CitiesMedias();
                    $places_medias->cities_id = $place_id;
                    $places_medias->medias_id = $media_model->id;
                    $places_medias->save();

                    $this->_create_thumbs($media_model);
                }
            }

            if ($post_data["text"] && count($post_data["mediafiles[]"]) > 0) {
                echo view('site.place2.partials.post_media_with_text', array("post" => $post));
            } else if (!$post_data["text"] && count($post_data["mediafiles[]"]) > 0) {
                echo view('site.place2.partials.post_media_without_text', array("post" => $post));
            } else if ($post_data['text'] && count($post_data["mediafiles[]"]) == 0) {
                echo view('site.place2.partials.post_text', array("place" => $place, "post" => $post));
            }
        }
    }

    public function postShare($place_id, Request $request)
    {

        $user_id = Auth::guard('user')->user()->id;
        $shared = CitiesShares::where('user_id', $user_id)->where('city_id', $place_id)->get()->first();

        if (is_object($shared)) {
            return ['msg' => 'You had already shared this country.'];
        } else {
            $share = new CitiesShares;
            $share->user_id = $user_id;
            $share->city_id = $place_id;
            $share->created_at = date("Y-m-d H:i:s");

            log_user_activity('City', 'share', $place_id);

            if ($share->save()) {


                return ['msg' => 'You have just shared this country.'];
            } else
                return ['msg' => 'You can not share this country now. Please try to it later.'];
        }
    }

    public function ajaxCheckEvent($place_id, Request $request)
    {

        $place = Cities::find($place_id);
        $client = new Client();
        //$client->getHttpClient()->setDefaultOption('verify', false);

        $data = array();
        $data['data'] = '';
        $data['cnt'] = 0;
        try {

            $result = @$client->post('https://auth.predicthq.com/token', [
                'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'scope' => 'account events signals'
                ],
                'verify' => false
            ]);
            $events_array = false;
            if ($result->getStatusCode() == 200) {
                $json_response = $result->getBody()->read(1024);
                $response = json_decode($json_response);
                $access_token = $response->access_token;

                $get_events1 = $client->get('https://api.predicthq.com/v1/events/?end.gte=' . date('Y-m-d') . '&within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                    'headers' => ['Authorization' => 'Bearer ' . $access_token],
                    'verify' => false
                ]);
                if ($get_events1->getStatusCode() == 200) {
                    $events1 = json_decode($get_events1->getBody()->read(100024));
                    //dd($events);
                    $events_array1 = $events1->results;
                    $events_final = $events_array1;
                    if ($events1->next) {
                        $get_events2 = $client->get($events1->next, [
                            'headers' => ['Authorization' => 'Bearer ' . $access_token],
                            'verify' => false
                        ]);
                        if ($get_events2->getStatusCode() == 200) {
                            $events2 = json_decode($get_events2->getBody()->read(100024));
                            //dd($events);
                            $events_array2 = $events2->results;
                            $events_final = array_merge($events_array1, $events_array2);
                        }
                    }
                    $events = $events_final;

                    foreach ($events as $event) {
                        $check_envent = Events::where('provider_id', $event->id)->where('cities_id', $place->id)->get()->first();
                        if (!is_object($check_envent)) {
                            $new_event = new Events();
                            $new_event->cities_id = $place->id;
                            $new_event->provider_id = @$event->id;
                            $new_event->category = @$event->category;
                            $new_event->title = @$event->title;
                            $new_event->description = @$event->description;
                            $new_event->address = @$event->entities[0]->formatted_address;
                            $new_event->labels = @join(",", $event->labels);
                            $new_event->lat = @$event->location[1];
                            $new_event->lng = @$event->location[0];
                            $new_event->start = @$event->start;
                            $new_event->end = @$event->end;
                            $new_event->save();
                        }
                    }
                    $event_datas = $place->events()->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')->get();
                    $data['cnt'] = count($event_datas);
                    foreach ($event_datas as $evt) {
                        $data['data'] .= View::make('site.place2.partials._event_block4modal', ['evt' => $evt])->render();
                    }
                }
            }
        } catch (\Throwable $th) {
        }

        $postIds = get_postlist4placemedia($place->id, 'city');
        foreach ($place->getMedias as $media) {
            $posts = PostsMedia::where('medias_id', $media->id)->pluck('posts_id')->toArray();
            if (!count(array_intersect($posts, $postIds))) {
                $post = new Posts();
                $post->updated_at = $media->uploaded_at;
                $post->permission = 0;
                $post->save();
                $postplace = new PostsCities();
                $postplace->posts_id = $post->id;
                $postplace->cities_id = $place->id;
                $postplace->save();
                $postmedia = new PostsMedia();
                $postmedia->posts_id = $post->id;
                $postmedia->medias_id = $media->id;
                $postmedia->save();
            }
        }

        return json_encode($data);
    }

    public function searchAllDatas(Request $request)
    {

        $query = $request->get('q');
        $search_in = $request->get('search_in');
        $search_id = $request->get('search_id');

        $place = Cities::find($search_id);

        if (!$place) {

            echo "";
        } else {
            $data = '';

            // getting discussion for place
            $discussions = Discussion::where('destination_type', 'City')
                ->where('destination_id', $place->id)
                ->whereRaw('(question like "%' . $query . '%" or description like "%' . $query . '%")')
                ->paginate(10);

            foreach ($discussions as $discussion) {

                $data .= view('site.place2.partials.post_discussion', array('discussion' => $discussion, 'place' => $place, 'search_result' => true));
            }


            // getting text post for place
            $post_ary = PostsCities::where('cities_id', $place->id)->pluck('posts_id')->toArray();
            if (count($post_ary) > 0) {
                $posts = Posts::whereIn('id', $post_ary)
                    ->whereRaw('text like "%' . $query . '%"')
                    ->paginate(10);

                foreach ($posts as $post) {

                    $data .= view('site.place2.partials.post_text', array('post' => $post, 'place' => $place, 'search_result' => true));
                }
            }

            // getting media post for place
            $media_ary = $place->medias->pluck('id')->toArray();
            $post_ary = PostsMedia::whereIn('medias_id', $media_ary)->pluck('posts_id')->toArray();
            if (count($post_ary) > 0) {
                $media = Posts::whereIn('id', $post_ary)
                    ->whereRaw('text like "%' . $query . '%"')
                    ->paginate(10);

                foreach ($media as $m) {
                    $data .= view('site.place2.partials.media_block', array('media' => $m, 'place' => $place, 'search_result' => true));
                }
            }

            // getting travelmates for place
            $mates_ary = User::whereRaw('(name like "%' . $query . '%" or interests like "%' . $query . '%")')
                ->pluck('id');

            $travelmates = TravelMatesRequests::whereHas('plan_city', function ($q) use ($place) {
                $q->where('cities_id', '=', $place->id);
            })
                ->whereIn('users_id', $mates_ary)
                ->paginate(10);
            // if (count($travelmates) == 0) {
            //     $travelmates = TravelMatesRequests::whereHas('plan_city', function($q) use ($place) {
            //                 $q->where('cities_id', '=', $place->city->id);
            //             })
            //             ->whereIn('users_id', $mates_ary)
            //             ->paginate(10);
            // }

            foreach ($travelmates as $travelmate_request) {
                $data .= view('site.place2.partials.post_travelmate', array('travelmate_request' => $travelmate_request, 'place' => $place, 'search_result' => true));
            }

            $reports = Reports::where('cities_id', '=', $place->id)
                ->get();

            foreach ($reports as $rp) {
                $data .= view('site.place2.partials.post_report', array('report' => $rp, 'place' => $place, 'search_result' => true));
            }

            // getting reviews for place
            $reviews = Reviews::where('cities_id', $place->id)
                ->whereRaw('text like "%' . $query . '%"')
                ->paginate(10);

            foreach ($reviews as $r) {
                $data .= view('site.place2.partials.post_review', array('review' => $r, 'place' => $place, 'search_result' => true));
            }


            //getting trip plans for place
            $trip_plans = TripPlaces::where('cities_id', $place->id)->pluck('trips_id')->toArray();

            if (count($trip_plans) > 0) {
                $trips = TripPlans::whereIn('id', $trip_plans)
                    ->whereRaw('title like "%' . $query . '%"')
                    ->paginate(10);

                foreach ($trips as $tr) {
                    $data .= view('site.place2.partials.post_plan', array('plan' => $tr, 'place' => $place, 'search_result' => true));
                }
            }

            //getting events for place
            $events = array();
            try {
                $client = new Client();
                $result = @$client->post('https://auth.predicthq.com/token', [
                    'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
                    'form_params' => [
                        'grant_type' => 'client_credentials',
                        'scope' => 'account events signals'
                    ],
                    'verify' => false
                ]);
                $events_array = false;
                if ($result->getStatusCode() == 200) {
                    $json_response = $result->getBody()->read(1024);
                    $response = json_decode($json_response);
                    $access_token = $response->access_token;

                    $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                        'headers' => ['Authorization' => 'Bearer ' . $access_token],
                        'verify' => false
                    ]);
                    if ($get_events1->getStatusCode() == 200) {
                        $events1 = json_decode($get_events1->getBody()->read(100024));
                        //dd($events);
                        $events_array1 = $events1->results;
                        $events_final = $events_array1;
                        if ($events1->next) {
                            $get_events2 = $client->get($events1->next, [
                                'headers' => ['Authorization' => 'Bearer ' . $access_token],
                                'verify' => false
                            ]);
                            if ($get_events2->getStatusCode() == 200) {
                                $events2 = json_decode($get_events2->getBody()->read(100024));
                                //dd($events);
                                $events_array2 = $events2->results;
                                $events_final = array_merge($events_array1, $events_array2);
                            }
                        }
                        $events = $events_final;
                    }
                }

                if (count($events) > 0) {
                    foreach ($events as $evt) {
                        if (strpos($evt->title, $query) || strpos($evt->description, $query) || strpos(@$evt->entities[0]->formatted_address, $query)) {
                            $data .= view('site.place2.partials.post_event', array('evt' => $evt, 'place' => $place, 'search_result' => true));
                        }
                    }
                }
            } catch (\Throwable $th) {
            }
            echo $data;
        }
    }

    public function discussion_replies_24hrs($place_id, Request $request)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');

        $filters = View::make('site.place2.partials._posts_filters_block');
        $output = $filters->render();

        $output = '';

        $posts_discussion_collection = array();
        $get_posts_discussion_collection = DiscussionReplies::whereHas('discussion', function ($q) use ($place_id) {
            $q->where('destination_type', 'city')
                ->where('destination_id', $place_id);
        })
            ->where('created_at', '>', date("Y-m-d", strtotime('-24 hours')))
            ->where('users_id', $user_id)
            ->orderBy('id', 'DESC')
            ->get();
        if (count($get_posts_discussion_collection) > 0) {
            foreach ($get_posts_discussion_collection as $gpdc) {
                $posts_discussion_collection[] = array(
                    'id' => $gpdc->discussions_id,
                    'type' => 'discussion',
                    'timestamp' => $gpdc->created_at
                );

                $view = View::make('site.place2.partials.post_discussion', [
                    'place' => $place,
                    'discussion' => $gpdc->discussion
                ]);
                $output .= $view->render();
            }
        } else {
            $output = 'no';
        }
        return json_encode($output);
    }

    public function newsfeed_show_all($place_id, Request $request)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');

        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = [];
        $filter = $request->all();
        $all_collection  = getPostsIdForAllFeed($place_id, $page, 'city', $filter);
        if (count($all_collection) > 0) {
            $postsId = array_map(function($post) {
                return $post = $post['id'];
            }, $all_collection);

            $show_post = Posts::whereIn('id', $postsId)->select('id', 'users_id', 'permission')->get()->toArray();
            foreach ($all_collection as $ppost) {
                $post_permission = [];
                foreach($show_post as $post) {
                    if($post['id'] == $ppost['id']) {
                        $post_permission = $post;
                    }
                }
                
                if (Auth::check() && isset($post_permission['permission']) && $post_permission['permission'] == Posts::POST_PERMISSION_PRIVATE && $post_permission['users_id'] != Auth::user()->id) {
                    continue;
                } elseif (Auth::check() && isset($post_permission['permission']) && $post_permission['permission'] == Posts::POST_PERMISSION_FRIENDS_ONLY && $post_permission['users_id'] != Auth::user()->id and !in_array($post_permission['users_id'], getUserFriendsIds())) {
                    continue;
                }
                $place_x = [];
                if ($ppost['place'] != 0) {
                    $ret =  PlaceTranslations::where('places_id', $ppost['place'])->first();
                    if (isset($ret) && is_object($ret)) {
                        $place_x['title'] = $ret->title;
                        $place_x['id'] = $ret->places_id;
                    }
                }
                $more = '';
                if ($ppost['type'] == 'regular') {
                    $more = View::make('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $ppost['id']]));

                    // $post = Posts::find($ppost['id']);
                    // if ($post->text != '' && count($post->medias) > 0) {
                    //     $more = View::make('site.place2.partials.post_media_with_text', array('post'=>$post, 'place'=>$place, 'selected_place'=>$place_x));
                    // } elseif ($post->text == '' && count($post->medias) > 0) {
                    //     if(is_object($post->author))
                    //         if(isset($post->medias[0]->media))
                    //             $more = View::make('site.place2.partials.post_media_without_text', array('post'=>$post, 'place'=>$place, 'selected_place'=>$place_x));
                    // } else {
                    //     $more = View::make('site.place2.partials.post_text', array('post'=>$post, 'place'=>$place, 'selected_place'=>$place_x));
                    // }
                } elseif ($ppost['type'] == 'discussion') {
                    $view = View::make('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $ppost['id']]));
                    // $discussion = Discussion::find($ppost['id']);
                    // $more = View::make('site.place2.partials.post_discussion', array('discussion'=>$discussion, 'place'=>$place));
                } elseif ($ppost['type'] == 'plan') {
                    // $plan = TripPlaces::find($ppost['id']);
                    // if(isset($plan->trip))
                    $more = View::make('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $ppost['id']]));
                } elseif ($ppost['type'] == 'report') {
                    $more = View::make('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $ppost['id']]));

                    // $report = Reports::find($ppost['id']);
                    // if ($report) {
                    //     $more = View::make('site.place2.partials.post_report', array('report'=>$report, 'place'=>$place));

                    // }
                } elseif ($ppost['type'] == 'review') {
                    $view = View::make('site.place2.new.review', array('post' => ['type' => 'Review', 'variable' => $ppost['id']]));

                    // $reviews = Reviews::find($ppost['id']);
                    // if(is_object($reviews))
                    //     $more = View::make('site.place2.partials.post_review', ['place' => $place,'review' => $reviews]);
                }

                $timestamp = (new Carbon($ppost['timestamp']))->timestamp;
                if (!array_key_exists($timestamp, $output)) {
                    $output[$timestamp] = '';
                }
                if (isset($more) && $more != '')
                    $output[$timestamp] .= $more->render();
            }
        }
        //$output .= $this->newsfeed_show_travelmates($place_id, $request, true);
        //$output .= $this->newsfeed_show_events($place_id, $request, true);
        //$output .= $this->newsfeed_show_reports($place_id, $request, true);
        //$output .= $this->newsfeed_show_reviews($place_id, $request, true);
        krsort($output);

        $output = implode($output);
        if (!$output) {
            $output = 'no';
        }
        // $this->utf8_encode_deep($output);

        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_about($place_id, Request $request)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');

        $data['city'] = $place;

        $data['country_weather'] = false;
        $data['current_weather'] = false;
        $data['daily_weather'] = false;
        $get_place_key = curl_init();
        curl_setopt_array($get_place_key, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q=' . $place->lat . ',' . $place->lng,
        ));
        $resp = curl_exec($get_place_key);
        curl_close($get_place_key);
        if ($resp && $resp != "null") {
            $resp = json_decode($resp);
            $place_key = $resp->Key;

            $get_country_weather = curl_init();
            curl_setopt_array($get_country_weather, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
            ));
            $country_weather = curl_exec($get_country_weather);
            curl_close($get_country_weather);
            $data['country_weather'] = json_decode($country_weather);

            $get_current_weather = curl_init();
            curl_setopt_array($get_current_weather, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/currentconditions/v1/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
            ));
            $current_weather = curl_exec($get_current_weather);
            curl_close($get_current_weather);
            $data['current_weather'] = json_decode($current_weather);


            $get_daily_weather = curl_init();
            curl_setopt_array($get_daily_weather, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/daily/10day/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
            ));
            $daily_weather = curl_exec($get_daily_weather);
            curl_close($get_daily_weather);
            $data['daily_weather'] = json_decode($daily_weather)->DailyForecasts;
        }
        // $country = Countries::find($place->countries_id);
        // $data['country'] = $country ;
        $about = View::make('site.place2.partials.post_about', $data);
        $output = $about->render();



        return json_encode($output);
    }

    public function newsfeed_show_discussions($place_id, Request $request)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        $is_parent_flag = $request->get('is_parent');
        $parent_indicator = 0 ;

        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();
        $filter = $request->all();
        $output = '';
        if (isset($filter['location']) || isset($filter['people'])) {
            $users_id_array = getUsersByFilters($filter);
            $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
                ->where('destination_type', 'city')
                ->whereIn('users_id', $users_id_array)
                ->orderBy('id', 'desc')
                ->skip($skip)
                ->take(5)
                ->get();
        } else {
            $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
            ->where('destination_type','city')
                ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '" ) as replies')
                ->orderBy('replies', 'DESC')
                ->offset($skip)
                ->limit(5)
                ->get();
                if(count($get_posts_discussions_collection) == 0){
                    $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
                    ->where('destination_type','city')
                    ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id` and created_at <  "' . Carbon::now()->subDays(7) . '" ) as replies')
                    ->offset($skip)
                    ->limit(5)
                    ->get();
            
            }
        }
        if(count($get_posts_discussions_collection) == 0){
            if(!isset($is_parent_flag)){
                $parent_indicator =1;
                $skip =0;
            }else {
                $parent_indicator =0;
            }
            $get_posts_discussions_collection = Discussion::where('destination_id',$place->country->id)
            ->where('destination_type','country')
            ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id` ) as replies')
            ->orderBy('replies', 'DESC')
            ->offset($skip)
            ->limit(5)
            ->get();
        }

        if (count($get_posts_discussions_collection) > 0) {
            foreach ($get_posts_discussions_collection as $gpdc) {
                // $view = View::make('site.place2.partials.post_discussion', ['place' => $place,
                //             'discussion' => $gpdc]);
                if($parent_indicator ){
                    $view = View::make('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $gpdc->id],'is_parent'=>true,'text'=>'Discussions about '.$place->country->transsingle->title));
                }else{
                    $view = View::make('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $gpdc->id]));

                }

                $output .= $view->render();
            }
        } else {
            $output = 'no';
        }

        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_top($place_id, Request $request)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $is_parent = $request->get('is_parent');
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        $filter = $request->all();
        $all_collection = collect_posts('city', $place_id, $page, true, false, $filter);
        if(count($all_collection) == 0){
            if(!isset($is_parent)){
                $page =1;
                $view = View::make('site.place2.partials.parent_banner',['is_parent'=>true,'text'=>'No more content to show for '.$place->transsingle->title.'. Displaying content for '.$place->country->transsingle->title.' instead.']);
                $output .=$view->render();
            }
            $all_collection = collect_posts('country', $place->country->id, $page, true, false, $filter);

        }
        if (count($all_collection) > 0) {
            foreach ($all_collection as $ppost) {
                $more = '';
                if ($ppost['type'] == 'regular') {
                    $more = View::make('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $ppost['id']]));

                    // $post = Posts::find($ppost['id']);
                    // if ($post->text != '' && count($post->medias) > 0) {
                    //     $more = View::make('site.place2.partials.post_media_with_text', array('post'=>$post, 'place'=>$place));
                    // } elseif ($post->text == '' && count($post->medias) > 0) {
                    //     if(is_object($post->author))
                    //         if(isset($post->medias[0]->media))
                    //             $more = View::make('site.place2.partials.post_media_without_text', array('post'=>$post, 'place'=>$place));
                    // } else {
                    //     $more = View::make('site.place2.partials.post_text', array('post'=>$post, 'place'=>$place));
                    // }
                } elseif ($ppost['type'] == 'discussion') {
                    $more = View::make('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $ppost['id']]));

                    // $discussion = Discussion::find($ppost['id']);
                    // if(is_object($discussion ))
                    // $more = View::make('site.place2.partials.post_discussion', array('discussion'=>$discussion, 'place'=>$place));
                } elseif ($ppost['type'] == 'plan') {
                    // $plan = TripPlaces::find($ppost['id']);
                    // if(isset($plan->trip))
                    $more = View::make('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $ppost['id']]));
                } elseif ($ppost['type'] == 'report')
                    $more = view('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $ppost['id']]));
                    
                $output .= $more->render();

            }
        }


        // $output .= $this->newsfeed_show_travelmates($place_id, $request, true, true);
        // $output .= $this->newsfeed_show_events($place_id, $request, true);
        // $output .= $this->newsfeed_show_reports($place_id, $request, true, true);
        // $output .= $this->newsfeed_show_reviews($place_id, $request, true, true);
        // krsort($output);
        // $output = implode($output);
        if ($output == [] || $output == '') {
            $output = 'no';
        }
        // $output=  utf8_encode($output);
        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_media($place_id, Request $request)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        $filter = $request->all();
        $all_collection = collect_posts('city', $place_id, $page, false, true, $filter);
        if (count($all_collection) > 0) {
            foreach ($all_collection as $ppost) {
                $output .= view('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $ppost['id']]));

                // if ($ppost['type'] == 'regular') {
                //     $post = Posts::find($ppost['id']);
                //     if ($post->text != '' && count($post->medias) > 0) {
                //         $more = View::make('site.place2.partials.post_media_with_text', array('post'=>$post));
                //         $output .= $more->render();
                //     } elseif ($post->text == '' && count($post->medias) > 0) {
                //         $more = View::make('site.place2.partials.post_media_without_text', array('post'=>$post));
                //         $output .= $more->render();
                //     } else {
                //     }
                // }
            }
        } else {
            $output = 'no';
        }

        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_tripplan($place_id, Request $request)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        $is_parent = 0;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        $filter = $request->all();
        $triplist = get_triplist4me($filter, $place_id, 'city');

        if(count($triplist ) == 0){
            $triplist = get_triplist4me($filter, $place->country->id, 'country');
            $is_parent = 1;
        }

        // $get_posts_plans_collection = TripPlaces::where('cities_id', $place_id)
        //             ->whereIn('trips_id', $triplist)
        //             ->orderBy('id', 'DESC')
        //             ->paginate(5);
        // ->skip($skip)
        // ->take(5)
        // ->get();



        $get_posts_plans_collection = Tripplans::leftjoin('trips_places', 'trips_places.trips_id', '=', 'trips.id')
            ->selectRaw('distinct(trips.id) as id, ((select count(*) from `posts_comments` where `posts_id`=`trips`.`id` and type="trip" and created_at >  "' . Carbon::now()->subDays(7) . '" ) 
                    + (select count(*) from `trips_likes` where `trips_id`=`trips`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '" ) 
                    + (select count(*) from `posts_shares` where `posts_type_id`=`trips`.`id` and type="trip" and created_at >  "' . Carbon::now()->subDays(7) . '" )) as ranks')
            ->whereIn('trips_id', $triplist)
            ->orderBy('ranks', 'DESC')
            ->groupBy('trips.id')
            ->paginate(5)->pluck('id')->toArray(); 
            if(empty($get_posts_plans_collection)){
                $get_posts_plans_collection = Tripplans::leftjoin('trips_places', 'trips_places.trips_id', '=', 'trips.id')
                ->selectRaw('distinct(trips.id) as id, ((select count(*) from `posts_comments` where `posts_id`=`trips`.`id` and type="trip" and created_at <  "' . Carbon::now()->subDays(7) . '" ) 
                        + (select count(*) from `trips_likes` where `trips_id`=`trips`.`id` and created_at <  "' . Carbon::now()->subDays(7) . '" ) 
                        + (select count(*) from `posts_shares` where `posts_type_id`=`trips`.`id` and type="trip" and created_at <  "' . Carbon::now()->subDays(7) . '" )) as ranks')
                ->whereIn('trips_id', $triplist)
                ->orderBy('ranks', 'DESC')
                ->groupBy('trips.id')
                ->paginate(5)->pluck('id')->toArray(); 
            }
        // $get_posts_plans_collection = TripPlaces::where('countries_id', $place_id)
        //             ->whereIn('trips_id', $triplist)
        //             ->orderBy('id', 'DESC')
        //             ->paginate(5);
        // ->skip($skip)
        // ->take(5)
        // ->get();
        $get_posts_plans_collection = Tripplans::whereIn('id', $get_posts_plans_collection)->get();

        if ($get_posts_plans_collection->count() > 0) {
            if($is_parent){
                $view = View::make('site.place2.partials.parent_banner',['is_parent'=>true,'text'=>'No more content to show for '.$place->transsingle->title.'. Displaying content for '.$place->country->transsingle->title.' instead.']);
                $output .=$view->render();
            }
            foreach ($get_posts_plans_collection as $gpdc) {
                    $view = View::make('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $gpdc->id]));


                // $view = View::make('site.place2.partials.post_plan', ['place' => $place,
                //             'plan' => $gpdc->trip]);
                $output .= $view->render();
            }
        } else {
            $output = 'no';
        }

        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_travelmates($place_id, Request $request, $internal = false, $top = false)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        $get_posts_mates_collection = TravelMatesRequests::whereHas('plan_city', function ($q) use ($place) {
            $q->where('cities_id', '=', $place->id);
        })
            ->groupBy('users_id')
            ->orderBy('id', 'desc')
            ->paginate(5);
        // ->skip($skip)
        // ->take(5)
        // ->get();

        if ($get_posts_mates_collection->count() > 0) {
            foreach ($get_posts_mates_collection as $gpdc) {
                $view = View::make('site.place2.partials.post_travelmate', [
                    'place' => $place,
                    'travelmate_request' => $gpdc
                ]);
                $output .= $view->render();
            }
        } else {
            $output = $internal ? '' : 'no';
        }

        return $internal ? $output :  json_encode(['html'=>$output]);
        ;
    }

    public function newsfeed_show_events($place_id, Request $request, $internal = false)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;

        $output = '';

        $get_posts_events_collection = Events::where('cities_id', $place_id)
            ->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')
            ->orderBy('id', 'desc')
            ->skip($skip)
            ->take(5)
            ->get();
        if (count($get_posts_events_collection) > 0) {
            foreach ($get_posts_events_collection as $gpec) {
                $view = View::make('site.place2.partials.post_event', [
                    'place' => $place,
                    'evt' => $gpec
                ]);
                $output .= $view->render();
            }
        } else {
            $output = $internal ? '' : 'no';
        }

        return $internal ? $output : json_encode($output);
    }

    public function newsfeed_show_reports($place_id, Request $request, $internal = false, $top = false)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();
        $is_parent =0;

        $output = '';
        $filter = $request->all();
        if (!$top) {
            if (isset($filter['location']) || isset($filter['people'])) {
                $users_id_array = getUsersByFilters($filter);
                $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($place_id) {
                    $q->where('location_type', Reports::LOCATION_CITY)
                        ->where('location_id', $place_id);
                })
                    ->whereNotNull('published_date')
                    ->where('flag', 1)
                    ->whereIn('users_id', $users_id_array)
                    ->paginate(5);
                // ->skip($skip)
                // ->take(5)
                // ->get();
            } else {
                $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($place_id) {
                    $q->where('location_type', Reports::LOCATION_CITY)
                        ->where('location_id', $place_id);
                })->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`reports`.`id` and type= "report" and created_at >  "' . Carbon::now()->subDays(7) . '") 
                    + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '") 
                    + (select count(*) from `posts_shares` where `posts_type_id`=`reports`.`id`  and type="report" and created_at >  "' . Carbon::now()->subDays(7) . '")) as rank')
                    ->orderBy('rank', 'DESC')       
                    ->whereNotNull('published_date')
                    ->where('flag', 1)
                    ->paginate(5);
                    if($get_posts_reports_collection->count()  == 0){
                        $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($place_id) {
                            $q->where('location_type', Reports::LOCATION_CITY)
                                ->where('location_id', $place_id);
                        })->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`reports`.`id` and type= "report"and created_at <  "' . Carbon::now()->subDays(7) . '") 
                            + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id` and created_at <  "' . Carbon::now()->subDays(7) . '") 
                            + (select count(*) from `posts_shares` where `posts_type_id`=`reports`.`id`  and type="report" and created_at < "' . Carbon::now()->subDays(7) . '")) as rank')
                            ->orderBy('rank', 'DESC')       
                            ->whereNotNull('published_date')
                            ->where('flag', 1)
                            ->paginate(5);
                    }
                    if($get_posts_reports_collection->count()  == 0){
                        $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($place) {
                            $q->where('location_type', Reports::LOCATION_COUNTRY)
                                ->where('location_id', $place->country->id);
                        })->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`reports`.`id` and type= "report"and created_at <  "' . Carbon::now()->subDays(7) . '") 
                            + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id` and created_at <  "' . Carbon::now()->subDays(7) . '") 
                            + (select count(*) from `posts_shares` where `posts_type_id`=`reports`.`id`  and type="report" and created_at < "' . Carbon::now()->subDays(7) . '")) as rank')
                            ->orderBy('rank', 'DESC')       
                            ->whereNotNull('published_date')
                            ->where('flag', 1)
                            ->paginate(5);
                            $is_parent =1;
                    }
                }
        } else {

            $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($place_id) {
                $q->where('location_type', Reports::LOCATION_CITY)
                    ->where('location_id', $place_id);
            })
                ->whereNotNull('published_date')
                ->where('flag', 1)
                ->selectRaw('*, ((select count(*) from `reports_comments` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `reports_views` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `reports_shares` where `reports_id`=`reports`.`id`)) as rank')
                ->orderBy('rank', 'DESC')
                ->paginate(5);
            // ->skip($skip)
            // ->take(5)
            // ->get();
        }
        if ($get_posts_reports_collection->count() > 0) {
            foreach ($get_posts_reports_collection as $gpdc) {
                if($is_parent){
                    $view = View::make('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $gpdc->id],'is_parent'=>true,'text'=>'Reports about for '.$place->country->transsingle->title));
                }else
                    $view = View::make('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $gpdc->id]));

                // $view = View::make('site.place2.partials.post_report', ['place' => $place,
                //             'report' => $gpdc]);
                $output .= $view->render();
            }
        } else {
            $output = $internal ? '' : 'no';
        }

        return $internal ? $output : json_encode(['html'=>$output]);
        ;
    }

    public function newsfeed_show_reviews($place_id, Request $request, $internal = false, $top = false)
    {

        $place = Cities::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        if (!$top) {

            $get_posts_review_collection = Reviews::where('cities_id', $place_id)
                ->orderBy('id', 'desc')
                ->skip($skip)
                ->take(5)
                ->get();
        } else {
            $get_posts_review_collection = Reviews::where('cities_id', $place_id)
                ->selectRaw('*, ((select count(*) from `reviews_votes` where `review_id`=`reviews`.`id` and `vote_type`=1) 
                                    + (select count(*) from `reviews_shares` where `review_id`=`reviews`.`id`)) as rank')
                ->orderBy('rank', 'desc')
                ->skip($skip)
                ->take(5)
                ->get();
        }
        if (count($get_posts_review_collection) > 0) {
            foreach ($get_posts_review_collection as $gpdc) {
                $view = View::make('site.place2.new.review', array('post' => ['type' => 'Review', 'variable' => $gpdc->id]));

                // $view = View::make('site.place2.partials.post_review', ['place' => $place,
                //             'review' => $gpdc]);
                $output .= $view->render();
            }
        } else {
            $output = $internal ? '' : 'no';
        }

        return $internal ? $output : json_encode($output);
    }
    /*
    public function getWeather($city_id) {
        $language_id = 1;
        $city = Cities::find($city_id);
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $city_id)
                ->where('active', 1)
                ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('cities_id', '=', $city->id)
                ->count();
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Cities::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'cities.id', '=', 'places.cities_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('cities.id', $city_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('cities_id', '=', $city->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['city'] = $city;
        $data['airports'] = $airports;
        
        
        
        $get_place_key = curl_init();
            curl_setopt_array($get_place_key, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q='.$city->lat.','.$city->lng,
            ));
            $resp = curl_exec($get_place_key);
            curl_close($get_place_key);
            if ($resp) {
                $resp = json_decode($resp);
                $place_key = $resp->Key;

                $get_country_weather = curl_init();
                curl_setopt_array($get_country_weather, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
                ));
                $country_weather = curl_exec($get_country_weather);
                curl_close($get_country_weather);
                $data['country_weather'] = json_decode($country_weather);
                
                $get_current_weather = curl_init();
                curl_setopt_array($get_current_weather, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://dataservice.accuweather.com/currentconditions/v1/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
                ));
                $current_weather = curl_exec($get_current_weather);
                curl_close($get_current_weather);
                $data['current_weather'] = json_decode($current_weather);
                
                
                $get_daily_weather = curl_init();
                curl_setopt_array($get_daily_weather, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/daily/10day/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
                ));
                $daily_weather = curl_exec($get_daily_weather);
                curl_close($get_daily_weather);
                $data['daily_weather'] = json_decode($daily_weather)->DailyForecasts;
               
            }

        return view('site.city.weather', $data);
    }
    
    public function getEtiquette($city_id) {
        $language_id = 1;
        $city = Cities::find($city_id);

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $city_id)
                ->where('active', 1)
                ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('cities_id', '=', $city->id)
                ->count();
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Cities::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'cities.id', '=', 'places.cities_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('cities.id', $city_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('cities_id', '=', $city->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['city'] = $city;
        $data['airports'] = $airports;
        
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        

        return view('site.city.etiquette', $data);
    }
    
    public function getPackingTips($city_id) {
        $language_id = 1;
        $city = Cities::find($city_id);

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $city_id)
                ->where('active', 1)
                ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('cities_id', '=', $city->id)
                ->count();
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Cities::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'cities.id', '=', 'places.cities_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('cities.id', $city_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('cities_id', '=', $city->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['city'] = $city;
        $data['airports'] = $airports;
        
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        return view('site.city.packing-tips', $data);
    }
    
    
    public function getHealth($city_id) {
        $language_id = 1;
        $city = Cities::find($city_id);

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $city_id)
                ->where('active', 1)
                ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('cities_id', '=', $city->id)
                ->count();
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Cities::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'cities.id', '=', 'places.cities_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('cities.id', $city_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('cities_id', '=', $city->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['city'] = $city;
        $data['airports'] = $airports;
        
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        return view('site.city.health', $data);
    }
    
    public function getVisaRequirements($city_id) {
        $language_id = 1;
        $city = Cities::find($city_id);

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $city_id)
                ->where('active', 1)
                ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('cities_id', '=', $city->id)
                ->count();
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Cities::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'cities.id', '=', 'places.cities_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('cities.id', $city_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('cities_id', '=', $city->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['city'] = $city;
        $data['airports'] = $airports;
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        return view('site.city.visa-requirements', $data);
    }
    
    public function getWhenToGo($city_id) {
        $language_id = 1;
        $city = Cities::find($city_id);

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $city_id)
                ->where('active', 1)
                ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('cities_id', '=', $city->id)
                ->count();
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Cities::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'cities.id', '=', 'places.cities_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('cities.id', $city_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('cities_id', '=', $city->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['city'] = $city;
        $data['airports'] = $airports;
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        return view('site.city.when-to-go', $data);
    }
    
    public function getCaution($city_id) {
        $language_id = 1;
        $city = Cities::find($city_id);

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $city_id)
                ->where('active', 1)
                ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('cities_id', '=', $city->id)
                ->count();
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Cities::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'cities.id', '=', 'places.cities_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('cities.id', $city_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('cities_id', '=', $city->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['city'] = $city;
        $data['airports'] = $airports;
        
        

        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        return view('site.city.caution', $data);
    }
    
    public function postFollow($cityId) {
        $userId = Auth::Id();
        $city = Cities::find($cityId);
        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $cityId)
                ->where('users_id', $userId)
                ->first();

        if ($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that city',
                ],
                'success' => false
            ];
        }

        $city->followers()->create([
            'users_id' => $userId
        ]);

        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('City', 'follow', $cityId);

        return [
            'success' => true
        ];
    }

    public function postUnFollow($cityId) {
        $userId = Auth::Id();

        $city = Cities::find($cityId);
        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $cityId)
                ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this city.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('City', 'unfollow', $cityId);
        }

        return [
            'success' => true
        ];
    }
    
    public function postCheckFollow($cityId) {
        $userId = Auth::Id();
        $city = Cities::find($cityId);
        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $cityId)
                ->where('users_id', $userId)
                ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }
    
    public function postVisaRequirements($countryId) {
        $userId = Auth::Id();
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $my_country = 'SA';
        $target_country = $country->iso_code;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.joinsherpa.com/v2/visa-requirements/' . $my_country . '-' . $target_country,
            CURLOPT_HTTPHEADER => array(
                'Content-Type:application/json',
                'Authorization: Basic ' . base64_encode("6801f898-5190-4177-ba1c-661e39596370:d41d8cd98f00b204e9800998ecf8427e")
            )
        ));
        $resp = curl_exec($curl);
        curl_close($curl);


        dd($resp);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function postWeather($countryId) {
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $latlng = $country->lat . ',' . $country->lng;

        $get_place_key = curl_init();
        curl_setopt_array($get_place_key, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q=41.87194000%2C12.56738000',
        ));
        $resp = curl_exec($get_place_key);
        curl_close($get_place_key);
        if ($resp) {
            $resp = json_decode($resp);
            $place_key = $resp->Key;

            $get_country_weather = curl_init();
            curl_setopt_array($get_country_weather, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/'.$place_key.'?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9',
            ));
            $country_weather = curl_exec($get_country_weather);
            curl_close($get_country_weather);
            $country_weather = json_decode($country_weather);
            dd($country_weather);
        }



        dd($place_key);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }
    
    public function postTalkingAbout($countryId) {
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $shares = $country->shares;
        
        foreach($shares AS $share) {
            $data['shares'][] = array(
                'user_id' => $share->user->id,
                'user_profile_picture'=> check_profile_picture($share->user->profile_picture)
                );
        }
        
        $data['num_shares'] = count($shares);
        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }
    
    public function postNowInCity($cityId) {
        
        $city = Cities::find($cityId);
        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $checkins = Checkins::where('city_id', $city->id)
                ->orderBy('id', 'DESC')
                ->take(8)
                ->get();

        $result = array();
        $done = array();
        foreach ($checkins AS $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                if(time()-strtotime($checkin->post_checkin->post->date) < (24*60*60)) {
                    $live = 1;
                }
                else {
                    $live = 0;
                }
                $result[] = array(
                    'name' => $checkin->post_checkin->post->author->name,
                    'id' => $checkin->post_checkin->post->author->id,
                    'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                    'live' => $live
                );
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['live_checkins'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }
    
    public function postContribute($countryId) {
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }
        
        if(Input::has('contents')) {
            $contents = Input::get('contents');
            $country_id = Input::get('country_id');
            $type = Input::get('type');
            $user_id = 0;
            if(Auth::user()) {
                $user_id = Auth::user()->id;
            }
            $create =  new CountriesContributions;
            $create->countries_id = $country_id;
            $create->users_id = $user_id;
            $create->type = $type;
            $create->contents = $contents;
            $create->status = 0;
            if($create->save()) {
                log_user_activity('Country', 'contribute', $country_id);
                $data['success'] = true;
            }
            else {
                $data['success'] = false;
            }
                
        }
        else {
            $data['success'] = false;
            
        }
        return json_encode($data);
       
    } 
    */
    function utf8_encode_deep(&$input)
    {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                self::utf8_encode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                self::utf8_encode_deep($input->$var);
            }
        }
    }
}
