<?php

namespace App\Http\Controllers;

use App\Events\Api\TripChat\ChatNewConversationApiEvent;
use App\Events\ChatNewConversationEvent;
use App\Events\ChatSendPrivateMessageEvent;
use App\Events\UpdatePlanChatsEvent;
use App\Http\Responses\AjaxResponse;
use App\Models\Chat\ChatConversation;
use App\Models\Chat\ChatConversationMessage;
use App\Models\Chat\ChatConversationParticipant;
use App\Models\User\User;
use App\Services\PlaceChat\PlaceChatsService;
use App\Services\Trips\TripInvitationsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Models\TripPlans\TripPlans;

class ChatPrivateController extends Controller
{

    //TODO: provide a (url / js function) to place on users profile, to be able to start chat immediately with that user when clicked. the url should be redirecting to new conversation or existing one with the user.
    //Route is /new/{user_id}
    //TODO: Change class for delete conversation button in index.blade and chat-functions.js

    public function __construct(Request $request)
    {
        $this->middleware('auth:user');
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index($conversation_id = 0)
    {
        $my_plans = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        $conversations = ChatConversation::whereHas("participants", function ($query) {
            $query->where("user_id", "=", Auth::id());
        })
            ->where(function ($query) {
                $query->where('type', '!=', PlaceChatsService::CHAT_TYPE_PLACE)
                    ->orWhereHas('messages', function ($q) {
                        $q->whereNull('type');
                    });
            })
            ->orderByDesc('updated_at')->get();

        //return $conversations;
        return view('site.chat.index', compact('conversations', 'conversation_id', 'my_plans'));
    }

    public function getLastFive($number)
    {
        if ($number > 20) {
            return response('Max. 20 is allowed.', 422);
        }

        $user_id = Auth::id();

        $conversations = ChatConversation::whereHas("participants", function ($query) use ($user_id) {
            $query->where("user_id", $user_id);
        })->orderByDesc('updated_at')->paginate($number);

        return $conversations;
    }

    //This is normal GET
    public function createNewConversation($user_id)
    {

        if ($user_id == Auth::id()) {
            return response('You can\'t create chat with yourself.', 422);
        }

        $result = $this->createNewChatConversation([$user_id], Auth::id(), true);

        if ($result['code'] == 200) {
            $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
            return redirect()->route('chat.index', ['conversation_id' => $result['conversation']->id]);
        } else {
            return response($result['message'], $result['code']);
        }
    }

    public function deleteConversation($conversation_id)
    {
        //This is not deleting conversation in total but only removing participant
        //Other participants will still be able to check messages and continue conversation
        ChatConversationParticipant::where('user_id', '=', Auth::id())->where('chat_conversation_id', $conversation_id)->delete();
        ChatConversationMessage::where('to_id', '=', Auth::id())->where('chat_conversation_id', $conversation_id)->delete();
        return redirect('/chat');
        //        return response('User removed from participants list.', 200);
    }

    //This is AJAX so redirect won't work
    public function send(Request $request)
    {
        //Check does request contains files
        $multi_file_name = null;
        $multi_file_type = null;
        $multi_file_size = null;
        $multi_file_path = null;

        $select_conversation_id = null;

        if ($request->hasFile('documents') === false) {
            $this->validate($request, [
                "chat_conversation_id" => ['required', 'integer', 'min:0'],
                "message" => ['string', 'required', 'min:1'],
            ]);
        } else {
            $multi_file_name = '';
            $multi_file_type = '';
            $multi_file_size = '';
            $multi_file_path = '';

            $this->validate($request, [
                "chat_conversation_id" => ['required', 'integer', 'min:0'],
                "message" => ['string', 'sometimes', 'nullable'],
            ]);

            //Save file to Storage
            $files = $request->file('documents');



            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                $file_type = $file->getClientOriginalExtension();
                $file_size = $file->getClientSize();
                // $file_path = $file->store('chat');
                $name = uniqid() . $file_name;

                Storage::disk('s3')->put(
                    'chat/' . $name,
                    file_get_contents($file),
                    'public'
                );

                $file_path = 'chat/' . $name;

                $multi_file_name .= $file_name . "|";
                $multi_file_type .= $file_type . "|";
                $multi_file_size .= $file_size . "|";
                $multi_file_path .= $file_path . "|";
            }
        }

        //It means new conversation must be created
        if ($request['chat_conversation_id'] == 0) {
            $this->validate($request, [
                "participants" => ['required', 'string', 'min:1']
            ]);

            $participants_received = explode(',', $request['participants']);

            $result = $this->createNewChatConversation($participants_received, Auth::id(), true);


            if ($result['code'] == 200) {
                $conversation = $result['conversation'];
                if ($result['select_conversation'] == true) {

                    $select_conversation_id = $conversation->id;
                }
            } else {
                return response($result['message'], $result['code']);
            }

            //Set new conversation as current
            $request['chat_conversation_id'] = $conversation->id;
        }

        $data = $request->all();

        //Check is conversation exists
        $conversation = ChatConversation::find($data['chat_conversation_id']);

        if ($conversation === null) {
            return response(['message' => 'Chat conversation not found.'], 422);
        }

        //Check is user allowed to use this conversation
        $participant = $conversation->participants->where('user_id', Auth::id())->first();

        if (!$participant || $participant->removed_at) {
            return response(['message' => 'You are not allow to use this conversation.'], 422);
        }

        //Save message to database
        $participants = $conversation->participants()->whereNull('removed_at')->get();

        //Check is there at least 2 participants
        if (count($participants) < 2 && $conversation->type === null) {
            return response(['message' => 'At least 2 participants are needed for conversation.'], 422);
        }

        $data['from_id'] = Auth::id();
        $data['file_path'] = $multi_file_path;
        $data['file_name'] = $multi_file_name;
        $data['file_size'] = $multi_file_size;
        $data['file_type'] = $multi_file_type;

        //Sending batch
        $data['send_id'] = ChatConversationMessage::max('send_id') + 1;

        //Update conversation updated_at
        date_default_timezone_set("UTC");
        $conversation->touch();
        date_default_timezone_set("Asia/Riyadh");

        foreach ($participants as $participant) {

            //Save even when from_id == to_id in order to avoid getting duplicates in getMessages()
            $data['to_id'] = $participant->user_id;

            if ($data['to_id'] == $data['from_id']) {
                $data['is_read'] = true;
            } else {
                $data['is_read'] = false;
            }

            $data['message'] = processString($data['message']);

            $message = ChatConversationMessage::create($data);

            broadcast(new ChatSendPrivateMessageEvent($participant->user_id, $message, $select_conversation_id));

            if ($conversation->plans_id) {
                try {
                    broadcast(new UpdatePlanChatsEvent($participant->user_id, $conversation->plans_id));
                } catch (\Exception $e) {
                }
            }
        }

        $message_new = $this->getMessage_last($conversation->id);

        return response(['conversation' => $conversation, 'message_new' => $message_new, 'message' => 'Chat message sent.'], 200);
    }

    public function start(Request $request)
    {
        $select_conversation_id = null;

        $this->validate($request, [
            "participants" => ['required']
        ]);

        $participants_received = explode(',', $request['participants']);

        $result = $this->createNewChatConversation($participants_received, 19, true);



        if ($result['code'] == 200) {
            $conversation = $result['conversation'];
            return response(['message' => 'ok', 'conversation_id' => $cnversation->id], 200);
        } else {
            return response($result['message'], $result['code']);
        }


        return response(['message' => 'Chat message sent.'], 200);
    }

    private function getMessage_last($chat_conversation_id)
    {
        //Check is conversation exists
        $conversation = ChatConversation::find($chat_conversation_id);

        if ($conversation === null) {
            return response(['message' => 'Chat conversation not found.'], 422);
        }

        //Check is user allowed to use this conversation
        $participant = $conversation->participants->where('user_id', Auth::id())->count();

        if ($participant == 0) {
            return response(['message' => 'You are not allow to read this conversation.'], 422);
        }

        //Mark all as read
        $conversation->markAllAsRead(Auth::id());

        return $conversation->myMessages(Auth::id())->last();
    }

    public function getMessages(Request $request, $chat_conversation_id, TripInvitationsService $tripInvitationsService)
    {

        //Check is conversation exists
        $conversation = ChatConversation::find($chat_conversation_id);

        if ($conversation === null) {
            return response(['message' => 'Chat conversation not found.'], 422);
        }

        //Check is user allowed to use this conversation
        $participant = $conversation->participants->where('user_id', Auth::id())->count();

        if ($participant == 0) {
            return response(['message' => 'You are not allow to read this conversation.'], 422);
        }

        //Mark all as read
        $conversation->markAllAsRead(Auth::id());

        $perPage = null;

        if ($request->get('page', null)) {
            $perPage = 10;
        }

        $result = $conversation->myMessages(Auth::id(), $perPage);

        return $result;
    }

    public function getParticipantsCombo()
    {
        $participants = User::where('id', '<>', Auth::id())->where('status', 1)->get();
        return $participants->pluck('name', 'id');
    }

    public function hasConversationId(Request $request){
        $participant_ids = $request->participants;
        $created_by = Auth::id();
        $chat_conversation_id = 0;

        $check_conversation = $this->checkConversation($participant_ids, $created_by);

        if($check_conversation){
            $chat_conversation_id = $check_conversation->id;
        }
        return response(['conversation_id' => $chat_conversation_id], 200);

    }

    public function getParticipantsComboImage(Request $request)
    {
        $participants = User::where('id', '<>', Auth::id())->where('status', 1)->where('name', 'like', '%' . $request->term['term'] . '%')->get()->toArray();
        foreach ($participants as &$val) {
            $val['profile_picture'] = check_profile_picture($val['profile_picture']);
        }
        return $participants;
    }

    private function createNewChatConversation(array $user_ids, $created_by, $broadcast_to_owner = false)
    {
        //Check does user exists
        foreach ($user_ids as $user_id) {
            if (User::find($user_id) === null) {
                return [
                    "message" => "Non existing user.",
                    "url" => "",
                    "code" => 422,
                    "conversation" => null,
                    "select_conversation" => false
                ];
            }
        }

        //Check does conversation exists
        $check_conversation = $this->checkConversation($user_ids, $created_by);

        if ($check_conversation === -1) {
            return [
                "message" => "At least 2 participants are needed for conversation.",
                "url" => "",
                "code" => 422,
                "conversation" => null,
                "select_conversation" => false
            ];
        }

        if ($check_conversation) { //count($check_conversation) > 0
            return [
                "message" => "",
                "url" => "chat.index",
                "code" => 200,
                "conversation" => $check_conversation, //[0]
                "select_conversation" => true
            ];
        }

        //Create new conversation
        $conversation = ChatConversation::create([
            'created_by_id' => $created_by,
            'subject' => ''
        ]);

        //Add participants
        ChatConversationParticipant::create([
            'chat_conversation_id' => $conversation->id,
            'user_id' => Auth::id()
        ]);

        foreach ($user_ids as $user_id) {
            ChatConversationParticipant::create([
                'chat_conversation_id' => $conversation->id,
                'user_id' => $user_id
            ]);

            //Broadcast event only to user
            try {
                broadcast(new ChatNewConversationEvent($user_id, $conversation));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new ChatNewConversationApiEvent($user_id, $conversation));
            } catch (\Throwable $e) {
            }
        }

        if ($broadcast_to_owner === true) {
            try {
                broadcast(new ChatNewConversationEvent(Auth::id(), $conversation));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new ChatNewConversationApiEvent(Auth::id(), $conversation));
            } catch (\Throwable $e) {
            }
        }

        return [
            "message" => "",
            "url" => "chat.index",
            "code" => 200,
            "conversation" => $conversation,
            "select_conversation" => false
        ];
    }

    private function checkConversation(array $participants_id, $created_by)
    {

        //We need at least 2 participants
        if (count($participants_id) < 1) {
            return -1;
        }

        // Get conversations by created_by field
        $conv_ids = ChatConversation::select('id')->whereHas("participants", function ($query) use ($created_by) {
            $query->where("user_id", $created_by);
        })->pluck('id', 'id')->toArray();

        if ($conv_ids == null) {
            return null;
        }
        // Get conversations participant by conversation ids
        $only_conversations = ChatConversationParticipant::whereIn('chat_conversation_id', $conv_ids)->get();

        // Collect convaersation ids and user ids to a array
        $conv_id = [];
        foreach ($only_conversations as $val) {
            if ($created_by == $val->user_id) continue;

            $conv_id[$val->chat_conversation_id][] = $val->user_id;
        }

        // Check if $participants in array of users, and also is same length
        // it means if chat is group and contains same people as in array($participants_id), or individual chat
        foreach ($conv_id as $key => $val) {
            if (in_array($participants_id, [$val]) && count($val) == count($participants_id)) {
                return ChatConversation::where('id', $key)->first();
            }
        }
        return [];
    }

    public function getPostSentUsers($post_type, $post_id)
    {
        $messages = ChatConversationMessage::where('from_id', Auth::id())
            ->where('to_id', '!=', Auth::id())
            ->where('post_type', $post_type)
            ->where('post_id', $post_id)
            ->pluck('to_id')
            ->toArray();

        return AjaxResponse::create($messages);
    }
}
