<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Access\language\Languages;
use League\Fractal\Resource\Item;
use App\Transformers\Country\CountryTransformer;
use Illuminate\Support\Facades\DB;
use App\Models\Country\ApiCountry as Country;
use App\Models\Country\CountriesFollowers;
use Illuminate\Support\Facades\Auth;
use App\Models\ActivityLog\ActivityLog;
use App\CountriesContributions;
use Illuminate\Support\Facades\Input;
use App\Models\Restaurants\Restaurants;
use App\Models\Restaurants\RestaurantFollowers;
use App\Models\Reviews\Reviews;
use App\Models\Posts\Checkins;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Models\TripPlans\TripPlans;
use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use Intervention\Image\ImageManagerStatic as Image;

class RestaurantController extends Controller {

    public function __construct() {
        //$this->middleware('auth:user');
    }

    public function getIndex($restaurant_id) {
        $language_id = 1;
        $restaurant = \App\Models\Restaurants\Restaurants::find($restaurant_id);

        if (!$restaurant) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Restaurant ID',
                ],
                'success' => false
            ];
        }

        $place = Restaurants::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'getMedias.comments',
                    'followers'
                ])
                ->where('id', $restaurant_id)
                ->where('active', 1)
                ->first();

        $db_restaurant_reviews = Reviews::where('restaurants_id', $restaurant->id)->get();
        if (!count($db_restaurant_reviews)) {
            $restaurant_reviews = get_google_reviews($restaurant->provider_id);
            if (is_array($restaurant_reviews)) {
                foreach ($restaurant_reviews AS $review) {
                    //dd($review);
                    $r = new Reviews;
                    $r->restaurants_id = $restaurant->id;
                    $r->google_author_name = $review->author_name;
                    $r->google_author_url = $review->author_url;
                    $r->google_language = @$review->language;
                    $r->google_profile_photo_url = $review->profile_photo_url;
                    $r->score = $review->rating;
                    $r->google_relative_time_description = $review->relative_time_description;
                    $r->text = $review->text;
                    $r->google_time = $review->time;
                    $r->save();
                }
            }
        }

        $db_restaurant_reviews = Reviews::where('restaurants_id', $restaurant->id)->get();
        $data['reviews'] = $db_restaurant_reviews;

        $data['reviews_avg'] = Reviews::where('restaurants_id', $restaurant->id)->avg('score');

        // get hotels nearby
        $data['restaurants_nearby'] = Restaurants::with('medias')
                ->whereHas("medias", function ($query) {
                    $query->where("medias_id", ">", 0);
                })
                ->where('cities_id', $restaurant->cities_id)
                ->where('id', '!=', $restaurant->id)
                ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $restaurant->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $restaurant->lng . ') ) + sin( radians(' . $restaurant->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
                ->having('distance', '<', 15)
                ->orderBy('distance')
                ->take(10)
                ->get();

                
        $checkins = Checkins::where('restaurants_id', $restaurant->id)->orderBy('id', 'DESC')->get();
        
        $result = array();
        $done = array();
        foreach ($checkins AS $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                $result[] = $checkin;
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['checkins'] = $result;


        $data['restaurant'] = $place;
        
        //dd($data['restaurant']);
        

        $client = new Client();
        //$client->getHttpClient()->setDefaultOption('verify', false);

        $result = $client->post('https://api.predicthq.com/oauth2/token', [
            'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'TZUhMoqSkz0JGuiBjL22c8x5Bsm9zgymv7AhZgg3'],
            'form_params' => [
                'grant_type' => 'client_credentials',
                'scope' => 'account events signals'
            ],
            'verify' => false
        ]);
        $events_array = false;
        if ($result->getStatusCode() == 200) {
            $json_response = $result->getBody()->read(1024);
            $response = json_decode($json_response);
            $access_token = $response->access_token;

            $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $restaurant->lat . ',' . $restaurant->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                'headers' => ['Authorization' => 'Bearer ' . $access_token],
                'verify' => false
            ]);
            if ($get_events1->getStatusCode() == 200) {
                $events1 = json_decode($get_events1->getBody()->read(100024));
                //dd($events);
                $events_array1 = $events1->results;
                $events_final = $events_array1;
                if ($events1->next) {
                    $get_events2 = $client->get($events1->next, [
                        'headers' => ['Authorization' => 'Bearer ' . $access_token],
                        'verify' => false
                    ]);
                    if ($get_events2->getStatusCode() == 200) {
                        $events2 = json_decode($get_events2->getBody()->read(100024));
                        //dd($events);
                        $events_array2 = $events2->results;
                        $events_final = array_merge($events_array1, $events_array2);
                    }
                }
                $data['events'] = $events_final;
            }
        }


        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        
        //dd($data);
        return view('site.restaurant.index', $data);
    }

    public function postFollow($restaurantId) {
        $userId = Auth::guard('user')->user()->id;
        $restaurant = Hotels::find($restaurantId);
        if (!$restaurant) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }


        $follower = HotelFollowers::where('hotels_id', $restaurantId)
                ->where('users_id', $userId)
                ->first();

        if ($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that Hotel',
                ],
                'success' => false
            ];
        }

        $new_follower = new HotelFollowers;
        $new_follower->hotels_id = $restaurantId;
        $new_follower->users_id = $userId;
        $new_follower->save();

        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('Hotel', 'follow', $restaurantId);


        return [
            'success' => true
        ];
    }

    public function postUnFollow($restaurantId) {
        $userId = Auth::guard('user')->user()->id;

        $restaurant = Hotels::find($restaurantId);
        if (!$restaurant) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        $follower = HotelFollowers::where('hotels_id', $restaurantId)
                ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this Hotel.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('Hotel', 'unfollow', $restaurantId);
        }

        return [
            'success' => true
        ];
    }

    public function postCheckFollow($restaurantId) {
        $userId = Auth::guard('user')->user()->id;
        $restaurant = Hotels::find($restaurantId);
        if (!$restaurant) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        $follower = HotelFollowers::where('hotels_id', $restaurantId)
                ->where('users_id', $userId)
                ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    public function postTalkingAbout($restaurantId) {
        $restaurant = Hotels::find($restaurantId);
        if (!$restaurant) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        $shares = $restaurant->shares;

        foreach ($shares AS $share) {
            $data['shares'][] = array(
                'user_id' => $share->user->id,
                'user_profile_picture' => check_profile_picture($share->user->profile_picture)
            );
        }

        $data['num_shares'] = count($shares);
        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function postNowInPlace($restaurantId) {
        $restaurant = Hotels::find($restaurantId);
        if (!$restaurant) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        $checkins = Checkins::where('hotels_id', $restaurant->id)
                ->orderBy('id', 'DESC')
                ->take(5)
                ->get();

        $result = array();
        $done = array();
        foreach ($checkins AS $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                $result[] = array(
                    'name' => $checkin->post_checkin->post->author->name,
                    'id' => $checkin->post_checkin->post->author->id,
                    'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture)
                );
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['live_checkins'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function ajaxPostReview($restaurantId) {
        $restaurant = Hotels::find($restaurantId);
        if (!$restaurant) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        if (Input::has('text')) {
            $review_text = Input::get('text');
            $review_score = Input::get('score');
            $review_author = Auth::guard('user')->user();
            $review_place_id = $restaurant->id;

            $review = new Reviews;
            $review->hotels_id = $review_place_id;
            $review->by_users_id = $review_author->id;
            $review->score = $review_score;
            $review->text = $review_text;

            if ($review->save()) {
                log_user_activity('Hotel', 'review', $restaurant->id);
                $data['success'] = true;
                $data['prepend'] = '<div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="' . check_profile_picture(Auth::guard('user')->user()->profile_picture) . '" alt="" style="width:45px;height:45px;">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">' . Auth::guard('user')->user()->name . '</a>
                                    <a href="#" class="comment-nickname"></a>
                                </div>
                                <div class="comment-txt">
                                    <p>' . $review_text . '</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-star-block">
                                        <select class="rating_stars">
                                            <option value="1" ' . ($review->score == 1 ? "selected" : "") . '>1</option>
                                            <option value="2" ' . ($review->score == 2 ? "selected" : "") . '>2</option>
                                            <option value="3" ' . ($review->score == 3 ? "selected" : "") . '>3</option>
                                            <option value="4" ' . ($review->score == 4 ? "selected" : "") . '>4</option>
                                            <option value="5" ' . ($review->score == 5 ? "selected" : "") . '>5</option>
                                        </select>
                                        <span class="count">
                                            <b>' . $review->score . '</b> / 5
                                        </span>
                                    </div>
                                    <div class="dot">·</div>
                                    <div class="com-time">' . diffForHumans(date("Y-m-d", time())) . '</div>
                                </div>
                            </div>
                        </div>';
            } else {
                $data['success'] = false;
            }
        } else {
            $data['success'] = false;
        }
        return json_encode($data);
    }

    public function ajaxHappeningToday($restaurantId) {
        $restaurant = Hotels::find($restaurantId);
        if (!$restaurant) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }


        $checkins = Checkins::whereHas("place", function ($query) use ($restaurant) {
                    $query->where('cities_id', $restaurant->cities_id);
                    $query->where('id', '!=', $restaurant->id);
                })
                ->orderBy('id', 'DESC')
                ->get();

        /*
          ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
          ->having('distance', '<', 15)
          ->orderBy('distance')
          ->take(10)
          ->get();
         * 
         */

        //$checkins = Checkins::where('place_id', $place->id)->orderBy('id', 'DESC')->take(5)->get();

        $result = array();
        foreach ($checkins AS $checkin) {
            $result[] = array(
                'post_id' => $checkin->post_checkin->post->id,
                'lat' => $checkin->hotel->lat,
                'lng' => $checkin->hotel->lng,
                'photo' => @$checkin->post_checkin->post->medias[0]->media->url,
                'name' => $checkin->post_checkin->post->author->name,
                'id' => $checkin->post_checkin->post->author->id,
                'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                'date' => diffForHumans($checkin->post_checkin->post->created_at)
            );
        }
        $data['happenings'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }
    
    
    public function ajaxGetMedia(Request $request) {
        $restaurant_id = $request->get('restaurant_id');
        $final_results = array();
        $h = Restaurants::find($restaurant_id);
        if (is_object($h)) {
            $details_link = file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?'
                    . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
                    . '&placeid=' . $h->provider_id);
            $details = json_decode($details_link);
            //dd($details);
            if (isset($details->result)) {
                $details = $details->result;
                if (isset($details->photos)) {
                    $place_photos = $details->photos;
                    $raw_photos = array();
                    $i = 1;

                    foreach ($place_photos AS $pp) {
                        $file_contents = file_get_contents('https://maps.googleapis.com/maps/api/place/photo?'
                                . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
                                . '&maxwidth=1600'
                                . '&photoreference=' . $pp->photo_reference);

                        $sha1 = sha1($file_contents);
                        $md5 = md5($file_contents);
                        $size = strlen($file_contents);
                        $raw_photos[$i]['sha1'] = $sha1;
                        $raw_photos[$i]['md5'] = $md5;
                        $raw_photos[$i]['size'] = $size;
                        $raw_photos[$i]['contents'] = $file_contents;
                        $i++;

                        
                            $check_media_exists = \App\Models\ActivityMedia\Media::where('sha1', $sha1)
                                    ->where('md5', $md5)
                                    ->where('filesize', $size)
                                    ->get()
                                    ->count();
                            if ($check_media_exists == 0) {
                                $media_file = 'restaurants/' . $h->provider_id . '/' . sha1(microtime()) . '.jpg';
                                \Illuminate\Support\Facades\Storage::disk('s3')->put($media_file, $file_contents, 'public');

                                
                                $media = new \App\Models\ActivityMedia\Media;
                                $media->url = $media_file;
                                $media->sha1 = $sha1;
                                $media->filesize = $size;
                                $media->md5 = $md5;
                                $media->html_attributions = join(",", $pp->html_attributions);
                                $media->save();
                                if ($media->save()) {
                                    $place_media = new \App\Models\Restaurants\RestaurantsMedias;
                                    $place_media->restaurants_id = $h->id;
                                    $place_media->medias_id = $media->id;
                                    $place_media->save();
                                    
                                    
                                    $complete_url = $media->url;
                                    $url = explode("/", $complete_url);
                                    //$data = file_get_contents('https://s3.amazonaws.com/travooo-images2/'.$complete_url);
                                    //$s3 = App::make('aws')->createClient('s3');
                                
                                    $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

                                    $options = [
                                        'region' => 'us-east-1',
                                        'version' => 'latest',
                                        'http' => ['verify' => false],
                                        'credentials' => $credentials,
                                        'endpoint' => 'https://s3.amazonaws.com'
                                    ];

                                    $s3Client = new S3Client($options);
                                    //$s3 = AWS::createClient('s3');
                                    $result = $s3Client->getObject([
                                        'Bucket' => 'travooo-images2', // REQUIRED
                                        'Key' => $complete_url, // REQUIRED
                                        'ResponseContentType' => 'text/plain',
                                    ]);

                                    $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
                                        $constraint->aspectRatio();
                                    });
                                    $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
                                        $constraint->aspectRatio();
                                    });
                                    $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
                                        $constraint->aspectRatio();
                                    });
                                    $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
                                        $constraint->aspectRatio();
                                    });

                                    //return $img_700->response('jpg');
                                    //echo $complete_url . '<br />';
                                    //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

                                    $put_1100 = $s3Client->putObject([
                                        'ACL' => 'public-read',
                                        'Body' => $img_1100->encode(),
                                        'Bucket' => 'travooo-images2',
                                        'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                    ]);

                                    $put_700 = $s3Client->putObject([
                                        'ACL' => 'public-read',
                                        'Body' => $img_700->encode(),
                                        'Bucket' => 'travooo-images2',
                                        'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                    ]);

                                    $put_230 = $s3Client->putObject([
                                        'ACL' => 'public-read',
                                        'Body' => $img_230->encode(),
                                        'Bucket' => 'travooo-images2',
                                        'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                    ]);

                                    $put_180 = $s3Client->putObject([
                                        'ACL' => 'public-read',
                                        'Body' => $img_180->encode(),
                                        'Bucket' => 'travooo-images2',
                                        'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                    ]);


                                    $media->thumbs_done = 1;
                                    $media->save();
                                }
                            }
                        
                    }
                    
                    if(isset($put_180)) return 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2];

                    //return serialize($raw_photos);
                    //return $raw_photos;
                }
            }
        }
    }

}
