<?php

namespace App\Http\Controllers;

use App\Events\Api\LikeUnLike\TripPlaceMediaCommentLikeUnLike;
use App\Events\Api\PlanMedia\TripMediaCommentAddedApiEvent;
use App\Events\Api\PlanMedia\TripMediaCommentChangedApiEvent;
use App\Events\Api\PlanMedia\TripMediaCommentRemovedApiEvent;
use App\Events\Api\PlanMedia\TripMediaLikeApiEvent;
use App\Events\PlanMedia\TripMediaCommentRemovedEvent;
use App\Events\PlanMedia\TripMediaCommentAddedEvent;
use App\Events\PlanMedia\TripMediaCommentChangedEvent;
use App\Events\PlanMedia\TripMediaLikeEvent;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlans\TripsLikes;
use App\Services\Medias\MediaCommentsService;
use App\Services\Ranking\RankingService;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

use App\Models\ActivityMedia\MediasComments;
use App\Models\User\UsersMedias;
use App\Models\ActivityMedia\MediasLikes;
use App\Models\ActivityMedia\MediasCommentsLikes;

use App\Models\ActivityMedia\MediascommentsMedias;
use App\Models\ActivityMedia\Media;
use App\Models\Posts\PostsMedia;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsLikes;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class LanguageController.
 */
class MediasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user')->except('tripMediaContentLoad');
    }

    /**
     * @param pair for tempfile prefix
     * @return fileNameArray(filepath, filename)
     */
    public function getTempFiles($pair)
    {
        $temp_dir = public_path() . '/assets2/upload_tmp/';
        $auth_user_id = Auth::guard('user')->user()->id;

        $file_lists = [];

        if (is_dir($temp_dir)) {
            if ($handle = opendir($temp_dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }

                    if (strpos($file, $pair . '_' . $auth_user_id) !== FALSE) {
                        $filename_split = explode($pair . '_' . $auth_user_id . '_', $file, 2);
                        $file_lists[] = [$temp_dir . $file, $filename_split[1]];
                    }
                }
                closedir($handle);
            }
        }

        return $file_lists;
    }

    public function tripMediaContentLoad(Request $request, $tripMediaId, TripsSuggestionsService $tripsSuggestionsService)
    {
        $authUserId = null;
        $auth_picture = null;
        $authUser = auth()->user();
        $is_invited = false;

        $photo = TripMedias::query()->with('tripPlace')->find($tripMediaId);

        if (!$photo) {
            throw new \Exception();
        }

        $tp = $photo->tripPlace;
        $isPublished = $tp->versions_id === $tp->trip->active_version;
        $tp->published = $isPublished;

        if ($authUser) {
            $auth_picture = check_profile_picture($authUser->profile_picture);
            $authUserId = $authUser->id;

            $invitedUserRequest = $tripsSuggestionsService->getInvitedUserRequest($tp->trips_id, $authUserId, false);
            $is_invited = $invitedUserRequest && $invitedUserRequest->status === TripInvitationsService::STATUS_PENDING;
        }

        $data = [
            'photo' => $photo,
            'auth_picture' => $auth_picture,
            'authUserId' => $authUserId,
            'tp' => $tp,
            'is_invited' => $is_invited,
            'trip' => $tp->trip
        ];

        return view('site.trip2.partials._trip_photo_modal_block', $data);
    }

    public function postComment(Request $request, $medias_id, RankingService $rankingService)
    {
        $comment = $request->text;
        $comment = is_null($comment) ? '' : $comment;
        $pair = $request->pair;
        @$author_id = Media::find($medias_id)->mediaUser->users_id;

        $is_files = false;
        $file_lists = [];

        if ($pair) {
            $file_lists = $this->getTempFiles($pair);
            $is_files = count($file_lists) > 0 ? true : false;
        }

        if (!$is_files && $comment == "")
            return 0;

        $text = convert_string($comment);

        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;

        $new_comment = new MediasComments;
        $new_comment->medias_id = $medias_id;
        $new_comment->users_id = $user_id;
        $new_comment->reply_to = $request->comment_id ? $request->comment_id : 0;
        $new_comment->comment = $text;
        $data = array();
        if ($new_comment->save()) {
            $rankingService->addPointsToEarners($new_comment->media);

            log_user_activity('Media', 'comment', $medias_id);

            foreach ($file_lists as $file) {
                $filename = $user_id . '_' . time() . '_' . $file[1];
                Storage::disk('s3')->put('media-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                $path = 'https://s3.amazonaws.com/travooo-images2/media-comment-photo/' . $filename;

                $media = new Media();
                $media->url = $path;
                $media->author_name = $user_name;
                $media->users_id = $user_id;
                $media->type  = getMediaTypeByMediaUrl($media->url);
                $media->title = $text;
                $media->author_url = '';
                $media->source_url = '';
                $media->license_name = '';
                $media->license_url = '';
                $media->uploaded_at = date('Y-m-d H:i:s');
                $media->save();

                $posts_comments_medias = new MediascommentsMedias();
                $posts_comments_medias->medias_comments_id = $new_comment->id;
                $posts_comments_medias->medias_id = $media->id;
                $posts_comments_medias->save();
            }

            $params = [
                'comment' => $new_comment,
                'authUserId' => Auth::id(),
                'photo' => TripMedias::query()->where('medias_id', $medias_id)->first()
            ];

            if (!$new_comment->reply_to) {
                try {
                    broadcast(new TripMediaCommentAddedEvent($new_comment->id));
                } catch (\Throwable $e) {
                }
                try {
                    broadcast(new TripMediaCommentAddedApiEvent($new_comment->id));
                } catch (\Throwable $e) {
                }
            } else {
                try {
                    broadcast(new TripMediaCommentChangedEvent($new_comment->reply_to));
                } catch (\Throwable $e) {
                }
                try {
                    broadcast(new TripMediaCommentChangedApiEvent($new_comment->reply_to));
                } catch (\Throwable $e) {
                }
            }

            if (isset($request->comment_id)) {
                $params['child'] = $new_comment;
                return view('site.trip2.partials.trip_place_media_comment_reply_block', $params);
            } else {
                return view('site.trip2.partials.trip_place_media_comment_block', $params);
            }
        }
    }

    public function postComment4Place(Request $request, $medias_id)
    {
        $comment = processString($request->text);
        $comment = is_null($comment) ? '' : $comment;
        @$author_id = Media::find($medias_id)->mediaUser->users_id;

        // $post_id = PostsMedia::where('medias_id', $medias_id)->get()->first()->posts_id;
        // $author_id = Posts::find($post_id)->users_id;

        if ($comment == "")
            return 0;

        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;

        $new_comment = new MediasComments;
        $new_comment->medias_id = $medias_id;
        $new_comment->users_id = $user_id;
        $new_comment->reply_to = 0;
        $new_comment->comment = $comment;
        $data = array();
        if ($new_comment->save()) {
            log_user_activity('Media', 'comment', $medias_id);

            // $post_object = Media::find($medias_id);
            return view('site.place2.partials._media_comment_block4modal', array('comment' => $new_comment));
        }
    }

    public function postCommentReply(Request $request)
    {
        $comment = $request->text;
        $comment = is_null($comment) ? '' : $comment;
        $comment_id = $request->comment_id;
        $medias_id = $request->media_id;
        $pair = $request->pair;
        @$author_id = Media::find($medias_id)->mediaUser->users_id;

        $is_files = false;
        $file_lists = $this->getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;

        if (!$is_files && $comment == "")
            return 0;


        $text = convert_string($comment);
        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;

        $new_comment = new MediasComments;
        $new_comment->medias_id = $medias_id;
        $new_comment->users_id = $user_id;
        $new_comment->reply_to = $comment_id;
        $new_comment->comment = $text;
        $data = array();
        if ($new_comment->save()) {
            log_user_activity('Media', 'comment', $medias_id);

            foreach ($file_lists as $file) {
                $filename = $user_id . '_' . time() . '_' . $file[1];
                Storage::disk('s3')->put('media-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                $path = 'https://s3.amazonaws.com/travooo-images2/media-comment-photo/' . $filename;

                $media = new Media();
                $media->url = $path;
                $media->author_name = $user_name;
                $media->users_id = $user_id;
                $media->type  = getMediaTypeByMediaUrl($media->url);
                $media->title = $text;
                $media->author_url = '';
                $media->source_url = '';
                $media->license_name = '';
                $media->license_url = '';
                $media->uploaded_at = date('Y-m-d H:i:s');
                $media->save();

                $posts_comments_medias = new MediascommentsMedias();
                $posts_comments_medias->medias_comments_id = $new_comment->id;
                $posts_comments_medias->medias_id = $media->id;
                $posts_comments_medias->save();
            }

            $params = [
                'child' => $new_comment,
                'authUserId' => Auth::id(),
                'photo' => TripMedias::query()->where('medias_id', $medias_id)->first()
            ];

            return view('site.trip2.partials.trip_place_media_comment_reply_block', $params);
        }
    }


    public function postCommentEdit(Request $request)
    {
        $pair = $request['pair'];
        $medias_id = $request->media_id;
        $comment_id = $request->comment_id;

        $media_comment_edit = $request->text;
        $user_id = Auth::guard('user')->user()->id;
        @$author_id = Media::find($medias_id)->mediaUser->users_id;
        $media_comment = MediasComments::find($comment_id);

        $user_name = Auth::guard('user')->user()->name;

        $is_files = false;
        $file_lists = $this->getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;
        $media_comment_edit = is_null($media_comment_edit) ? '' : $media_comment_edit;
        if (!$is_files && $media_comment_edit == "")
            return 0;

        $post_object = Media::find($medias_id);

        $text = convert_string($media_comment_edit);

        if (is_object($post_object)) {
            $media_comment->comment = $text;
            if ($media_comment->save()) {
                log_user_activity('Status', 'comment', $medias_id);


                if (isset($request->existing_medias) && count($media_comment->medias) > 0) {
                    foreach ($media_comment->medias as $media_list) {
                        if (in_array($media_list->media->id, $request->existing_medias)) {
                            $medias_exist = Media::find($media_list->medias_id);

                            $media_list->delete();

                            if ($medias_exist) {
                                $amazonefilename = explode('/', $medias_exist->url);
                                Storage::disk('s3')->delete('post-comment-photo/' . end($amazonefilename));

                                $medias_exist->delete();
                            }
                        }
                    }
                }



                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('media-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/media-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $text;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->users_id = $user_id;
                    $media->type  = getMediaTypeByMediaUrl($media->url);
                    $media->save();

                    $posts_comments_medias = new MediascommentsMedias();
                    $posts_comments_medias->medias_comments_id = $media_comment->id;
                    $posts_comments_medias->medias_id = $media->id;
                    $posts_comments_medias->save();
                }

                $media_comment->load('medias');

                $params = [
                    'comment' => $media_comment,
                    'authUserId' => Auth::id(),
                    'photo' => TripMedias::query()->where('medias_id', $medias_id)->first()
                ];

                if ($request->comment_type == 1) {
                    try {
                        broadcast(new TripMediaCommentChangedEvent($media_comment->id));
                    } catch (\Throwable $e) {
                    }
                    try {
                        broadcast(new TripMediaCommentChangedApiEvent($media_comment->id));
                    } catch (\Throwable $e) {
                    }
                    return view('site.trip2.partials.trip_place_media_comment_block', $params);
                } else {
                    try {
                        broadcast(new TripMediaCommentChangedEvent($media_comment->reply_to));
                    } catch (\Throwable $e) {
                    }
                    try {
                        broadcast(new TripMediaCommentChangedApiEvent($media_comment->reply_to));
                    } catch (\Throwable $e) {
                    }

                    $params['child'] = $media_comment;
                    return view('site.trip2.partials.trip_place_media_comment_reply_block', $params);
                }
            } else {
                return 0;
            }
        }
    }

    /**
     * @param Request $request
     * @return false|string
     * @throws \Exception
     */
    public function tripPlaceLikeUnlike(Request $request, RankingService $rankingService)
    {
        $mediaId = $request->media_id;

        $tripPlaceMedia = TripMedias::where('medias_id', $mediaId)->get()->first();

        if (!$tripPlaceMedia) {
            throw new NotFoundHttpException();
        }

        $userId = auth()->id();

        $check_like_exists = MediasLikes::where('medias_id', $mediaId)->where('users_id', $userId)->first();

        $data = [];

        if ($check_like_exists) {
            $check_like_exists->delete();
            log_user_activity('Media', 'unlike', $mediaId);
            $data['status'] = 'no';
            $rankingService->subPointsFromEarners($check_like_exists->media);
        } else {

            $like = new MediasLikes();
            $like->medias_id = $mediaId;
            $like->users_id = $userId;
            $like->save();

            $rankingService->addPointsToEarners($like->media);

            log_user_activity('Media', 'like', $mediaId);

            $authorId = $tripPlaceMedia->trip->users_id;

            if ($authorId && $userId != $authorId) {
                notify($authorId, 'trip_place_like', $tripPlaceMedia->trips_id);
            }

            $data['status'] = 'yes';
        }

        $data['count'] = MediasLikes::where('medias_id', $mediaId)->count();

        try {
            broadcast(new TripMediaLikeEvent($mediaId));
        } catch (\Throwable $e) {
        }
        try {
            broadcast(new TripMediaLikeApiEvent($mediaId));
        } catch (\Throwable $e) {
        }

        return json_encode($data);
    }

    public function getMediaLikesData($mediaId)
    {
        $userId = Auth::id();
        $allLikes = MediasLikes::query()->where('medias_id', $mediaId)->get();

        return [
            'count' => $allLikes->count(),
            'is_my_like' => $allLikes->contains('users_id', $userId)
        ];
    }

    public function postLikeUnlike(Request $request)
    {
        $media_id = $request->media_id;
        $user_id = Auth::guard('user')->user()->id;
        $post_id = PostsMedia::where('medias_id', $media_id)->get()->first()->posts_id;
        $author_id = Posts::find($post_id)->users_id;

        // $author_id = UsersMedias::where('medias_id', $media_id)->get()->first()->user->id;

        $check_like_exists = MediasLikes::where('medias_id', $media_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            //dd($check_like_exists);
            $check_like_exists->delete();
            log_user_activity('Media', 'unlike', $media_id);
            //notify($author_id, 'status_unlike', $post_id);

            $data['status'] = 'no';
        } else {
            $like = new MediasLikes;
            $like->medias_id = $media_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Media', 'like', $media_id);
            //notify($author_id, 'media_like', $media_id);

            if ($author_id && $user_id != $author_id) {
                notify($author_id, 'status_like', $post_id);
            }
            $data['status'] = 'yes';
        }
        $data['count'] = count(MediasLikes::where('medias_id', $media_id)->get());
        return json_encode($data);
    }

    public function postCommentLikeUnlike(Request $request)
    {

        $comment_id = $request->comment_id;

        $comment = MediasComments::query()->find($comment_id);

        if (!$comment) {
            throw new NotFoundHttpException();
        }

        $user_id = Auth::guard('user')->user()->id;

        $author_id = MediasComments::find($comment_id)->users_id;
        $media_id = MediasComments::find($comment_id)->medias_id;

        @$post_author_id = Media::find($media_id)->mediaUser->users_id;
        // $post_id = PostsMedia::where('medias_id', $media_id)->get()->first()->posts_id;
        // $post_author_id = Posts::find($post_id)->users_id;

        $check_like_exists = MediasCommentsLikes::where('medias_comments_id', $comment_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            //dd($check_like_exists);
            $check_like_exists->delete();
            log_user_activity('Media', 'commentunlike', $comment_id);
            //notify($author_id, 'status_unlike', $post_id);

            $data['status'] = 'no';
        } else {
            $like = new MediasCommentsLikes;
            $like->medias_comments_id = $comment_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Media', 'commentlike', $comment_id);
            //notify($author_id, 'media_like', $media_id);

            $data['status'] = 'yes';
        }

        if ($comment->reply_to) {
            $updateCommentId = $comment->reply_to;
        } else {
            $updateCommentId = $comment_id;
        }


        try {
            broadcast(new TripMediaCommentChangedEvent($updateCommentId));
        } catch (\Throwable $e) {
        }
        try {
            broadcast(new TripPlaceMediaCommentLikeUnLike($updateCommentId));
        } catch (\Throwable $e) {
        }

        $data['count'] = count(MediasCommentsLikes::where('medias_comments_id', $comment_id)->get());
        $data['name'] = Auth::guard('user')->user()->name;

        return json_encode($data);
    }

    public function postCommentDelete(Request $request)
    {

        $comment_id = $request->comment_id;

        $comment = MediasComments::find($comment_id);

        foreach ($comment->medias as $com_media) {
            $medias_exist = Media::find($com_media->medias_id);

            $com_media->delete();

            if ($medias_exist) {
                $amazonefilename = explode('/', $medias_exist->url);
                Storage::disk('s3')->delete('media-comment-photo/' . end($amazonefilename));

                $medias_exist->delete();
            }
        }

        if ($comment->delete()) {
            if ($comment->reply_to) {
                try {
                    broadcast(new TripMediaCommentChangedEvent($comment->reply_to));
                } catch (\Throwable $e) {
                }
                try {
                    broadcast(new TripMediaCommentRemovedApiEvent($comment->reply_to, $comment->medias_id));
                } catch (\Throwable $e) {
                }
            } else {
                try {
                    broadcast(new TripMediaCommentRemovedEvent($comment->id, $comment->medias_id));
                } catch (\Throwable $e) {
                }
                try {
                    broadcast(new TripMediaCommentRemovedApiEvent($comment->id, $comment->medias_id));
                } catch (\Throwable $e) {
                }
            }

            return 1;
        } else {
            return 0;
        }
    }

    public function postShowLikes(Request $request)
    {

        $media_id = $request->media_id;
        $data['likes'] = MediasLikes::where('medias_id', $media_id)->get();


        return json_encode($data);
    }

    public function getCommentLikeUsers(Request $request)
    {
        $comment_id = $request->comment_id;
        $comment = MediasComments::find($comment_id);


        if ($comment->likes) {
            return view('site.home.partials.modal_comments_like_block', array('likes' => $comment->likes()->orderBy('created_at', 'DESC')->get()));
        }
    }


    public function postUpdateViews(Request $request)
    {

        $media_id = $request->vid_id;
        Media::find($media_id)->increment('views');
    }

    public function getMoreComments(Request $request, MediaCommentsService $mediaCommentsService)
    {
        $page = $request->pagenum;
        $next = ($page - 1) * 5;

        $mediaId = $request->media_id;
        $media = Media::find($mediaId);

        $comments = $media->comments()->orderBy('created_at', 'desc')->skip($next)->take(5)->get();

        $result = '';

        if (count($comments) > 0) {
            foreach ($comments as $comment) {
                $result .= $mediaCommentsService->renderComment($comment);
            }
        }

        return $result;
    }

    public function getComment($commentId, MediaCommentsService $mediaCommentsService)
    {
        $comment = MediasComments::query()->find($commentId);

        //todo add access check
        $hasAccess = true;

        if (!$comment || !$hasAccess) {
            throw new ModelNotFoundException();
        }

        return $mediaCommentsService->renderComment($comment);
    }
}
