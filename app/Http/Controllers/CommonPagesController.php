<?php

namespace App\Http\Controllers;

use App\Models\CommonPage\CommonPage;
use Illuminate\Http\Request;

class CommonPagesController extends Controller
{
    function privacyPolicyPage()
    {
        $page = CommonPage::where('slug', CommonPage::SLUG_PRIVACY_POLICY)->first();
        if ($page && $page->active == CommonPage::ACTIVE && $page->trans) {
            return view('site.common-pages.index', [
                'page' => $page,
                'withContainer' => false
            ]);
        }
    }

    function termsOfServicePage()
    {
        $page = CommonPage::where('slug', CommonPage::SLUG_TERMS_OF_SERVICE)->first();
        if ($page && $page->active == CommonPage::ACTIVE && $page->trans) {
            return view('site.common-pages.index', [
                'page' => $page,
                'withContainer' => false
            ]);
        }
    }

    function privacyPolicyPageApp()
    {
        $page = CommonPage::where('slug', CommonPage::SLUG_PRIVACY_POLICY)->first();
        if ($page && $page->active == CommonPage::ACTIVE && $page->trans) {
            return view('react-native.static-pages.privacy-and-tnc', [
                'page' => $page,
                'app' => true
            ]);
        }
        return '';
    }

    function termsOfServicePageApp()
    {
        $page = CommonPage::where('slug', CommonPage::SLUG_TERMS_OF_SERVICE)->first();
        if ($page && $page->active == CommonPage::ACTIVE && $page->trans) {
            return view('react-native.static-pages.privacy-and-tnc', [
                'title' => '',
                'page' => $page,
                'app' => true
            ]);
        }
        return '';
    }
}
