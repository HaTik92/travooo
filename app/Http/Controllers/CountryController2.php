<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country\Countries;
use App\Models\Country\CountriesAbouts;
use App\Models\Access\Language\Languages;
use App\Models\Place\Place;
use App\Models\Place\PlaceTranslations;
use App\Models\PlacesTop\PlacesTop;
use League\Fractal\Resource\Item;
use App\Transformers\Country\CountryTransformer;
use Illuminate\Support\Facades\DB;
use App\Models\Country\ApiCountry as Country;
use App\Models\Country\CountriesFollowers;
use App\Models\Country\CountriesMedias;
use App\Models\Country\CountriesShares;
use Illuminate\Support\Facades\Auth;
use App\Models\ActivityLog\ActivityLog;
use App\CountriesContributions;
use App\Models\Access\User\User;
use App\Models\ActivityMedia\Media;
use Illuminate\Support\Facades\Input;
use App\Models\Posts\Checkins;
use App\Models\Posts\PostsCheckins;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\Reports\ReportsInfos;
use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Reports\Reports;
use App\Models\Reviews\Reviews;
use App\Models\Reviews\ReviewsVotes;
use App\Models\Reviews\ReviewsShares;
use App\Models\TopCities\TopCities;
use App\Models\City\CitiesTranslations;
use App\Models\City\Cities;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlaces\TripPlaces;
use App\Models\Posts\PostsCountries;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsMedia;
use App\Models\Events\Events;
use App\Models\Events\EventsComments;
use App\Models\Events\EventsCommentsMedias;
use App\Models\Events\EventsCommentsLikes;
use App\Models\Events\EventsLikes;
use App\Models\Events\EventsShares;
use App\Models\User\UsersMedias;
use Illuminate\Support\Facades\View;

use Illuminate\Support\Facades\Storage;
use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

use GuzzleHttp\Client;
use Illuminate\View\ViewFinderInterface;

class CountryController2 extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth:user');
    }

    public function _create_thumbs($media)
    {

        $complete_url = $media->url;
        $url = explode("/", $complete_url);

        $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

        $options = [
            'region' => 'us-east-1',
            'version' => 'latest',
            'http' => ['verify' => false],
            'credentials' => $credentials,
            'endpoint' => 'https://s3.amazonaws.com'
        ];

        $s3Client = new S3Client($options);
        //$s3 = AWS::createClient('s3');
        $result = $s3Client->getObject([
            'Bucket' => 'travooo-images2', // REQUIRED
            'Key' => $complete_url, // REQUIRED
            'ResponseContentType' => 'text/plain',
        ]);

        $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        //return $img_700->response('jpg');
        //echo $complete_url . '<br />';
        //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

        $put_1100 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_1100->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_700 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_700->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_230 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_230->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_180 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_180->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);


        $media->thumbs_done = 1;
        $media->save();
    }

    private function _getRandom($key)
    {

        $init_array = [];
        for ($i = 1; $i < 32; $i++) $init_array[] = $i;

        $return = [];

        foreach ($init_array as $val) {
            $return[] = $val * $key % 32;
        }

        return implode(',', $return);
    }

    public function getIndex($country_id)
    {
        $language_id = 1;
        $country = Countries::find($country_id);

        $random_val = $this->_getRandom(date('j'));

        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $data['search_in'] = $country;

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
            'getMedias',
            'getMedias.users',
            'timezone',
        ])
            ->where('id', $country_id)
            ->where('active', 1)
            ->first();

        if (!$country) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }
        $ip = getClientIP();
        $geoLoc = ip_details($ip);
        $getLocCity = $geoLoc['city'];
        $data['myCity'] = $getLocCity;
        // exit('ok');
        $db_place_reviews = Reviews::where('countries_id', $country->id)->orderBy('created_at', 'desc')->get();
        $data['reviews'] = $db_place_reviews;
        $data['reviews_places'] = Reviews::where('countries_id', $country->id)->get();

        $data['reviews_avg'] = Reviews::where('countries_id', $country->id)->whereNull('google_author_url')->avg('score');

        $data['place'] = $country;
        $cites = [];
        foreach ($country->cities as $city) {
            $cityTrans = CitiesTranslations::where('cities_id', $city->id)->first();
            if (is_object($cityTrans))
                $cites[$city->id] = $cityTrans->title;
        }
        $data['countries_cities'] = $cites;

        if (Auth::check()) {
            // $list_trip_plan = TripPlaces::where('countries_id', $country->id)->pluck('trips_id');
            $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)
                //    ->whereNotIn('id', $list_trip_plan)
                ->get();

            $data['me'] = Auth::guard('user')->user();
        }

        $data['travelmates'] = TravelMatesRequests::whereHas('plan_country', function ($q) use ($country) {
            $q->where('countries_id', '=', $country->id);
        })
            ->groupBy('users_id')
            ->get();

        $data['reportsinfo'] = [];
        $data['reports'] = Reports::where('countries_id', '=', $country->id)->where('flag', '=', 1)
            ->get();

        $data['pposts'] = '';

        $data['place_discussions'] = Discussion::where('destination_type', 'Country')
            ->where('destination_id', $country->id)
            ->get();

        // get experts into categories
        $experts_all = $experts_top = $experts_local = $experts_friends = $live_checkins = array();
        foreach ($country->experts as $key => $pce) {
            $experts_all[] = $pce;
            if (Auth::check() && is_object($pce->user) && is_object($data['me'])) {
                if ($pce->user->nationality != $data['me']->nationality) {
                    $experts_top[] = $pce;
                } elseif ($pce->user->nationality == $data['me']->nationality) {
                    $experts_local[] = $pce;
                }
            }
            if (Auth::check() && is_object($pce->user)) {
                if (is_friend($pce->user->id)) {
                    $experts_friends[] = $pce;
                }
            }
        }
        $data['experts_top'] = $experts_top;
        $data['experts_local'] = $experts_local;
        $data['experts_friends'] = $experts_friends;
        $data['all_expert'] = $experts_all;
        $data['live_checkins'] = $country->checkins()->groupBy('users_id')->whereDate('checkin_time', '=', Carbon::today()->toDateString())->get();

        $experts_ids = array();
        foreach ($experts_all as $ea) {
            $experts_ids[] = $ea->users_id;
        }

        // Select *,count(*) as r, sum(A.replies) from 
        // (SELECT created_at, users_id, count(*) as replies FROM `discussion_replies` GROUP BY YEAR(created_at), Month(created_at), Day(created_at)) 
        // as A GROUP BY A.users_id ORDER BY `users_id`
        $data['place_top_contributors'] = [];
        if (count($experts_ids) > 0) {
            $last_month = date("Y-m-d", strtotime('-180 days'));

            $sub = DiscussionReplies::selectRaw('*, count(*) as reps, sum(num_views) as views')
                ->whereRaw('users_id in (' . implode(',', $experts_ids) . ')')
                ->whereRaw('created_at >' . $last_month)
                ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'));

            $data['place_top_contributors'] = DiscussionReplies::from(DB::raw('(' . $sub->toSql() . ') as a'))
                ->selectRaw('*, count(*) as count, sum(a.reps) as reply_cnt, sum(a.views) as views')
                ->groupBy('a.users_id')
                ->orderBy('count', 'desc')
                ->take(10)
                ->get();
        }

        $follow_list = [];

        if (Auth::check()) {
            $tp_array = getTopPlacesByUserPref(['country' => $country->id]);
            $p = Auth::user()->id;
            $follows_places = DB::select('SELECT * FROM `places_followers` WHERE users_id   =' . $p);

            foreach ($follows_places as $placex) {
                $follow_list[$placex->places_id] = $placex->places_id;
            }
        } else {
            $current_user_loc_info = ip_details(getClientIP());
            $current_country = Countries::where('iso_code', $current_user_loc_info['iso_code'])->first();

            $tp_array = getTopPlacesByUserPref(['country' => $country->id], $current_country->id);

            $data['current_country_id'] = $current_country->id;
        }


        $tp_array = array_diff($tp_array, array_values($follow_list));
        $data['top_places'] = PlacesTop::whereIn('places_id', $tp_array)
            ->whereHas('place', function ($query) {
                $query->whereHas('medias');
            })
            ->whereNotIn('city_id', [2031])
            ->orderByRaw('FIELD(places_top.id % 32, ' . $random_val . ')')
            ->limit(6)
            ->get();
        $tp_array = array_merge($tp_array, $follow_list);

        if (count($data['top_places']) < 6) {
            $remaining = 6 - count($data['top_places']);
            $count_remaing = PlacesTop::whereNotIn('places_id', $tp_array)
                ->whereHas('place', function ($query) {
                    $query->whereHas('medias');
                })->where('country_id', $country->id)
                ->whereNotIn('city_id', [2031])
                ->orderByRaw('FIELD(places_top.id % 32, ' . $random_val . ')')
                ->limit($remaining)
                ->get();

            $data['top_places'] = $data['top_places']->merge($count_remaing);
        }
        //top citites 
        $top_cities = TopCities::where('countries_id', $country->id)
            ->take(10)
            ->orderBy('no_of_reviews', 'DESC')
            ->pluck('cities_id');
        $top_cities = Cities::with([
            'trans',
            'getMedias',
            'followers'
        ])->whereIn('id', $top_cities)->get();
        $data['top_cities'] = $top_cities;
        $languages = $country->getLanguages();
        $country->setRelation('languages', collect($languages));

        $data['trip_plans_collection'] = TripPlans::query()
            ->whereHas('trip_places_by_active_version', function ($q) use ($country) {
                $q->where('countries_id', $country->id);
            })
            ->orderByDesc('id')
            ->limit(20)
            ->with('trip_places_by_active_version')
            ->get();

        $live_users = [];
        foreach ($data['live_checkins'] as $live_here) {
            $live_users[] = $live_here->users_id;
        }
        $temp_live_users =  User::whereIn('id', $live_users)->get();
        $live_users = [];
        foreach ($temp_live_users as $user) {
            $is_followed =  app('App\Http\Controllers\ProfileController')->postCheckFollow($user->id);
            $live_users[$user->id] = ['users' => $user, 'is_followed' => $is_followed['success']];
        }
        $data['live_users'] = $live_users;
        // dd($data['top_cities']);

        $data['place_type'] = 'country';
        return view('site.place2.index', $data);
        // $placeCount = Place::where('countries_id', '=', $country->id)
        //         ->count();






        $citiesMediasSql = DB::table('cities_medias')
            ->select([
                'cities_id',
                DB::raw('max(medias_id) AS medias_id')
            ])
            ->groupBy('cities_id')
            ->toSql();

        $tripPlans = Country::select(
            'medias.id AS media_id',
            'medias.url',
            'trips_cities.cities_id',
            'trips.id',
            'trips.title',
            'trips.users_id',
            'trips.created_at',
            'trips_places.budget',
            'trips_places.id'
        )
            ->join('places', 'countries.id', '=', 'places.countries_id')
            ->join('trips_places', function ($join) {
                $join->on('trips_places.countries_id', '=', 'countries.id')
                    ->on('trips_places.places_id', '=', 'places.id');
            })
            ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
            ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
            ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
            ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
            ->where('countries.id', $country_id)
            ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('countries_id', '=', $country->id)
            ->where('place_type', 'like', 'airport,%')
            ->limit(10)
            ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['country'] = $country;
        $data['airports'] = $airports;
        $data['user'] = Auth::guard('user')->user();

        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();


        $client = new Client();
        //$client->getHttpClient()->setDefaultOption('verify', false);

        $result = $client->post('https://auth.predicthq.com/token', [
            'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
            'form_params' => [
                'grant_type' => 'client_credentials',
                'scope' => 'account events signals'
            ],
            'verify' => false
        ]);
        $events_array = false;
        if ($result->getStatusCode() == 200) {
            $json_response = $result->getBody()->read(1024);
            $response = json_decode($json_response);
            $access_token = $response->access_token;

            $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $country->lat . ',' . $country->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                'headers' => ['Authorization' => 'Bearer ' . $access_token],
                'verify' => false
            ]);
            if ($get_events1->getStatusCode() == 200) {
                $events1 = json_decode($get_events1->getBody()->read(100024));
                //dd($events);
                $events_array1 = $events1->results;
                $events_final = $events_array1;
                if ($events1->next) {
                    $get_events2 = $client->get($events1->next, [
                        'headers' => ['Authorization' => 'Bearer ' . $access_token],
                        'verify' => false
                    ]);
                    if ($get_events2->getStatusCode() == 200) {
                        $events2 = json_decode($get_events2->getBody()->read(100024));
                        //dd($events);
                        $events_array2 = $events2->results;
                        $events_final = array_merge($events_array1, $events_array2);
                    }
                }
                $data['events'] = $events_final;
            }
        }



        $checkins = Checkins::where('country_id', $country->id)->orderBy('id', 'DESC')->get();

        $result = array();
        $done = array();
        foreach ($checkins as $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                $result[] = $checkin;
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['checkins'] = $result;


        $data['travelmates'] = TravelMatesRequests::whereHas('plan_country', function ($q) use ($country) {
            $q->where('countries_id', '=', $country->id);
        })
            ->get();

        $data['reports'] = Reports::where('countries_id', '=', $country->id)
            ->get();

        $data['me'] = Auth::guard('user')->user();


        $data['pposts'] = collect_posts('country', $country->id);

        $data['country_discussions'] = Discussion::where('destination_type', 'Country')
            ->where('destination_id', $country->id)
            ->get();



        // get experts into categories
        $experts_top = $experts_local = $experts_friends = $live_checkins = array();
        foreach ($country->experts as $key => $pce) {
            if ($pce->user->nationality != $data['me']->nationality) {
                $experts_top[] = $pce;
            } elseif ($pce->user->nationality == $data['me']->nationality) {
                $experts_local[] = $pce;
            }
            if (is_friend($pce->user->id)) {
                $experts_friends[] = $pce;
            }
        }
        $data['experts_top']        =       $experts_top;
        $data['experts_local']      =       $experts_local;
        $data['experts_friends']    =       $experts_friends;
        $data['live_checkins']      =       $country->checkins()->groupBy('users_id')->get();


        $checkins = Checkins::whereHas("country", function ($query) use ($country) {
            $query->where('country_id', $country->id);
        })
            ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $country->lat . ') ) * cos( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) * cos( radians( SUBSTRING(lat_lng, INSTR(lat_lng, \',\') + 1) ) - radians(' . $country->lng . ') ) + sin( radians(' . $country->lat . ') ) * sin( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) ) ) AS distance'))
            // ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
            ->having('distance', '<', 15)
            ->orderBy('distance')
            ->take(10)
            ->get();

        $result = array();
        foreach ($checkins as $checkin) {
            $result[] = array(
                // 'post_id' => $checkin->post_checkin->post->id,
                'lat' => $checkin->country->lat,
                'lng' => $checkin->country->lng,
                // 'photo' => @$checkin->post_checkin->post->medias[0]->media->url,
                'name' => @$checkin->user->name,
                'id' => @$checkin->user->id,
                'title' => $checkin->location,
                'profile_picture' => check_profile_picture(@$checkin->user->profile_picture),
                'date' => diffForHumans($checkin->created_at)
            );
        }
        $data['places_nearby'] = $result;

        $data['place_type'] = 'country';

        return view('site.country2.index', $data);
    }

    public function postFollow($countryId)
    {
        $userId = Auth::guard('user')->user()->id;
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }


        $follower = CountriesFollowers::where('countries_id', $countryId)
            ->where('users_id', $userId)
            ->first();

        if ($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that Country',
                ],
                'success' => false
            ];
        }

        $country->followers()->create([
            'users_id' => $userId
        ]);

        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('Country', 'follow', $countryId);

        $count = CountriesFollowers::where('countries_id', $countryId)->count();

        return [
            'success' => true,
            'count' => $count
        ];
    }

    public function postUnFollow($placeId)
    {

        $userId = Auth::guard('user')->user()->id;

        $place = Countries::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $follower = CountriesFollowers::where('countries_id', $placeId)
            ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this Country.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('Country', 'unfollow', $placeId);
        }

        $count = CountriesFollowers::where('countries_id', $placeId)->count();

        return [
            'success' => true,
            'count' => $count
        ];
    }

    public function postCheckFollow($placeId)
    {

        $userId = Auth::guard('user')->user()->id;
        $place = Countries::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $follower = CountriesFollowers::where('countries_id', $placeId)
            ->where('users_id', $userId)
            ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    public function postCheckin($placeId)
    {

        $userId = Auth::guard('user')->user()->id;
        $place = Countries::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }


        $check = new Checkins();
        $postCheckedIn =     Input::get('checkin_time');
        if (isset($postCheckedIn)) {
            $check->checkin_time = date("Y-m-d", strtotime(Input::get('checkin_time')));
        }
        $check->users_id = $userId;
        $check->country_id = $placeId;
        $check->location = $place->transsingle->title;
        $check->lat_lng = $place->lat . ',' . $place->lng;
        $check->save();




        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('Country', 'checkin', $placeId);


        if (strtotime($check->checkin_time) == strtotime(date('Y-m-d'))) {
            return [
                'success' => true,
                'live' => 1,
                'checkin_id' => $check->id,
                'message' => 'Thank you for checking in! You are live on this page now.'
            ];
        } else if (strtotime($check->checkin_time) < strtotime(date('Y-m-d'))) {
            return [
                'success' => true,
                'live' => 0,
                'checkin_id' => $check->id,
                'message' => 'Thank you for recording your past check-in!'
            ];
        } else if (strtotime($check->checkin_time) > strtotime(date('Y-m-d'))) {
            return [
                'success' => true,
                'live' => 0,
                'checkin_id' => $check->id,
                'message' => 'Thank you for checking in! You will appear live on this page on your check-in date.'
            ];
        }
    }

    public function postCheckout($placeId)
    {

        $userId = Auth::guard('user')->user()->id;

        $place = Countries::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $follower = CountriesFollowers::where('countries_id', $placeId)
            ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this Country.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('Country', 'checkout', $placeId);
        }

        return [
            'success' => true
        ];
    }

    public function postCheckCheckin($placeId)
    {

        $userId = Auth::guard('user')->user()->id;
        $place = Countries::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $checkin = Checkins::where('country_id', $placeId)
            ->where('users_id', $userId)
            ->first();

        return empty($checkin) ? ['success' => false] : ['success' => true];
    }

    public function postTalkingAbout($placeId)
    {

        $place = Countries::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $shares = $place->shares;

        foreach ($shares as $share) {
            $data['shares'][] = array(
                'user_id' => $share->user->id,
                'user_profile_picture' => check_profile_picture($share->user->profile_picture)
            );
        }

        $data['num_shares'] = count($shares);
        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function postNowInPlace($placeId)
    {

        $place = Countries::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $checkins = Checkins::where('country_id', $place->id)
            ->orderBy('id', 'DESC')
            ->take(5)
            ->get();

        $result = array();
        $done = array();
        foreach ($checkins as $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                if (time() - strtotime($checkin->post_checkin->post->date) < (24 * 60 * 60)) {
                    $live = 1;
                } else {
                    $live = 0;
                }
                $result[] = array(
                    'name' => $checkin->post_checkin->post->author->name,
                    'id' => $checkin->post_checkin->post->author->id,
                    'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                    'live' => $live
                );
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['live_checkins'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function ajaxPostReview($placeId)
    {

        $place = Countries::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        if (Input::has('text')) {
            $review_text = Input::get('text');
            $review_score = Input::get('score');
            $review_author = Auth::guard('user')->user();
            $review_place_id = $place->id;

            $review = new Reviews;
            $review->countries_id = $review_place_id;
            $review->by_users_id = $review_author->id;
            $review->score = $review_score * 1;
            $review->text = $review_text;

            if ($review->save()) {
                log_user_activity('Country', 'review', $place->id);
                $data['success'] = true;
                $data['posted_review'] = View::make('site.place2.partials.post_review', ['place' => $place, 'review' => $review])->render();
                $data['prepend'] = '<div class="comment" topsort="' . $review->score . '" newsort="' . strtotime($review->created_at) . '">
                            <img class="comment__avatar" src="' . check_profile_picture(Auth::guard('user')->user()->profile_picture) . '" alt="" style="width:45px;height:45px;">
                            <div class="comment__content" style="width: calc(100% - 44px)">
                                <div class="comment__header"><a class="comment__username" href="#">' . Auth::guard('user')->user()->name . '</a>
                                    <div class="comment__user-id"></div>
                                </div>
                                <div class="comment__text" style="overflow-wrap: break-word;margin-top:10px">' . $review_text . '</div>
                                <div class="comment__footer">
                                    <div class="rating">
                                    <svg class="icon icon--star ' . ($review->score >= 1 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 2 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 3 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 4 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <svg class="icon icon--star ' . ($review->score >= 5 ? "icon--star-active" : "") . '">
                                        <use xlink:href="' . asset("assets3/img/sprite.svg#star") . '"></use>
                                    </svg>
                                    <div class="rating__value"><strong>' . $review->score . '</strong> / 5</div>
                                    </div>
                                    <div class="comment__posted">' . diffForHumans($review->created_at) . '</div>
                                </div>
                            </div>
                        </div>';
            } else {
                $data['success'] = false;
            }
        } else {
            $data['success'] = false;
        }
        return json_encode($data);
    }

    public function ajaxPostMorePhoto($placeId, Request $request)
    {

        $language_id = 1;
        $num = $request->pagenum;
        $skip = ($num) * 6;
        $place = Countries::find($placeId);

        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $medias = $place->getMedias;

        if (!$medias) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }
        $data = array();
        $c = (count($medias) <= ($skip + 6)) ? count($medias) : $skip + 6;
        for ($i = $skip; $i < $c; $i++) {
            $data[] = '<img class="gallery__img" src="https://s3.amazonaws.com/travooo-images2/th230/' . @$medias[$i]->url . '" alt="#" title="" /> ';
        }
        return [
            'data' => $data,
            'success' => true
        ];
    }

    public function postAddPost($place_id, Request $request)
    {

        $post_data = array();
        foreach ($request->data as $data) {
            if (strpos($data["name"], "[]")) {
                if (!isset($post_data[$data["name"]]))
                    $post_data[$data["name"]] = [];

                $post_data[$data["name"]][] = $data["value"];
            } else {
                $post_data[$data["name"]] = $data["value"];
            }
        }

        $userId = Auth::guard('user')->user()->id;
        $userName = Auth::guard('user')->user()->name;

        $place = Countries::find($place_id);

        if (!$place) {
            echo "error";
        }

        $post = new Posts;
        $post->users_id = $userId;
        $post->text = $post_data["text"];
        $post->permission = $post_data["permission"];
        $post->save();

        log_user_activity('Country_Post', 'post', $post->id);
        //create post checkin
        $checkins = '';
        if (isset($post_data['checkinPost'])) {
            $postCheckin =  PostsCheckins::create(['checkins_id' => $post_data['checkinPost'], 'posts_id' => $post->id]);
            $checkins  = Checkins::find($postCheckin->checkins_id);
        }
        if ($post_data["type"] == "text") {

            $postplace = new PostsCountries;
            $postplace->posts_id = $post->id;
            $postplace->countries_id = $place_id;
            $postplace->save();

            echo view('site.place2.partials.post_text', array("place" => $place, "post" => $post));
        } else if ($post_data["type"] == "image" || $post_data["type"] == "video") {
            $postplace = new PostsCountries;
            $postplace->posts_id = $post->id;
            $postplace->countries_id = $place_id;
            $postplace->save();

            foreach ($post_data["mediafiles[]"] as $media) {
                if ($media) {
                    // list($baseType, $media) = explode(';', $media);
                    // list(, $media) = explode(',', $media);
                    // $media = base64_decode($media);

                    // if (strpos($baseType, 'image') !== false)
                    //     $filename = sha1(microtime()) . "_place_added_file.png";
                    // else if (strpos($baseType, 'video') !== false)
                    //     $filename = sha1(microtime()) . "_place_added_file.mp4";

                    // Storage::disk('s3')->put(
                    //         $userId . '/medias/' . $filename, $media, 'public'
                    // );
                    // $media_url = $userId . '/medias/' . $filename;

                    $media_url = $media;

                    $media_model = new Media();
                    $media_model->url = $media_url;
                    $media_model->author_name = $userName;
                    $media_model->users_id = $userId;
                    $media_model->type  = getMediaTypeByMediaUrl($media_model->url);
                    $media_model->title = $post_data["text"];
                    $media_model->author_url = '';
                    $media_model->source_url = '';
                    $media_model->license_name = '';
                    $media_model->license_url = '';
                    $media_model->uploaded_at = date('Y-m-d H:i:s');
                    $media_model->save();

                    $users_medias = new UsersMedias();
                    $users_medias->users_id = $userId;
                    $users_medias->medias_id = $media_model->id;
                    $users_medias->save();

                    $posts_medias = new PostsMedia();
                    $posts_medias->posts_id = $post->id;
                    $posts_medias->medias_id = $media_model->id;
                    $posts_medias->save();

                    $places_medias = new CountriesMedias();
                    $places_medias->countries_id = $place_id;
                    $places_medias->medias_id = $media_model->id;
                    $places_medias->save();

                    $this->_create_thumbs($media_model);
                }
            }

            if ($post_data["text"] && count($post_data["mediafiles[]"]) > 0) {
                echo view('site.place2.partials.post_media_with_text', array("post" => $post));
            } else if (!$post_data["text"] && count($post_data["mediafiles[]"]) > 0) {
                echo view('site.place2.partials.post_media_without_text', array("post" => $post));
            } else if ($post_data['text'] && count($post_data["mediafiles[]"]) == 0) {
                echo view('site.place2.partials.post_text', array("place" => $place, "post" => $post));
            }
        }
    }

    public function postShare($place_id, Request $request)
    {

        $user_id = Auth::guard('user')->user()->id;
        $shared = CountriesShares::where('user_id', $user_id)->where('country_id', $place_id)->get()->first();

        if (is_object($shared)) {
            return ['msg' => 'You had already shared this country.'];
        } else {
            $share = new CountriesShares;
            $share->user_id = $user_id;
            $share->country_id = $place_id;
            $share->created_at = date("Y-m-d H:i:s");

            log_user_activity('Country', 'share', $place_id);

            if ($share->save()) {


                return ['msg' => 'You have just shared this country.'];
            } else
                return ['msg' => 'You can not share this country now. Please try to it later.'];
        }
    }

    public function ajaxCheckEvent($place_id, Request $request)
    {

        $place = Countries::find($place_id);
        $client = new Client();
        //$client->getHttpClient()->setDefaultOption('verify', false);

        $data = array();
        $data['data'] = '';
        $data['cnt'] = 0;
        try {

            $result = @$client->post('https://auth.predicthq.com/token', [
                'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'scope' => 'account events signals'
                ],
                'verify' => false
            ]);
            $events_array = false;
            if ($result->getStatusCode() == 200) {
                $json_response = $result->getBody()->read(1024);
                $response = json_decode($json_response);
                $access_token = $response->access_token;

                $get_events1 = $client->get('https://api.predicthq.com/v1/events/?end.gte=' . date('Y-m-d') . '&within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                    'headers' => ['Authorization' => 'Bearer ' . $access_token],
                    'verify' => false
                ]);
                if ($get_events1->getStatusCode() == 200) {
                    $events1 = json_decode($get_events1->getBody()->read(100024));
                    //dd($events);
                    $events_array1 = $events1->results;
                    $events_final = $events_array1;
                    if ($events1->next) {
                        $get_events2 = $client->get($events1->next, [
                            'headers' => ['Authorization' => 'Bearer ' . $access_token],
                            'verify' => false
                        ]);
                        if ($get_events2->getStatusCode() == 200) {
                            $events2 = json_decode($get_events2->getBody()->read(100024));
                            //dd($events);
                            $events_array2 = $events2->results;
                            $events_final = array_merge($events_array1, $events_array2);
                        }
                    }
                    $events = $events_final;

                    foreach ($events as $event) {
                        $check_envent = Events::where('provider_id', $event->id)->where('countries_id', $place->id)->get()->first();
                        if (!is_object($check_envent)) {
                            $new_event = new Events();
                            $new_event->countries_id = $place->id;
                            $new_event->provider_id = @$event->id;
                            $new_event->category = @$event->category;
                            $new_event->title = @$event->title;
                            $new_event->description = @$event->description;
                            $new_event->address = @$event->entities[0]->formatted_address;
                            $new_event->labels = @join(",", $event->labels);
                            $new_event->lat = @$event->location[1];
                            $new_event->lng = @$event->location[0];
                            $new_event->start = @$event->start;
                            $new_event->end = @$event->end;
                            $new_event->save();
                        }
                    }
                    $event_datas = $place->events()->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')->get();
                    $data['cnt'] = count($event_datas);
                    foreach ($event_datas as $evt) {
                        $data['data'] .= View::make('site.place2.partials._event_block4modal', ['evt' => $evt])->render();
                    }
                }
            }
        } catch (\Throwable $th) {
        }

        $postIds = get_postlist4placemedia($place->id, 'country');
        foreach ($place->getMedias as $media) {
            $posts = PostsMedia::where('medias_id', $media->id)->pluck('posts_id')->toArray();
            if (!count(array_intersect($posts, $postIds))) {
                $post = new Posts();
                $post->updated_at = $media->uploaded_at;
                $post->permission = 0;
                $post->save();
                $postplace = new PostsCountries();
                $postplace->posts_id = $post->id;
                $postplace->countries_id = $place->id;
                $postplace->save();
                $postmedia = new PostsMedia();
                $postmedia->posts_id = $post->id;
                $postmedia->medias_id = $media->id;
                $postmedia->save();
            }
        }

        return json_encode($data);
    }

    public function searchAllDatas(Request $request)
    {

        $query = $request->get('q');
        $search_in = $request->get('search_in');
        $search_id = $request->get('search_id');

        $place = Countries::find($search_id);

        if (!$place) {

            echo "";
        } else {
            $data = '';

            // getting discussion for place
            $discussions = Discussion::where('destination_type', 'Country')
                ->where('destination_id', $place->id)
                ->whereRaw('(question like "%' . $query . '%" or description like "%' . $query . '%")')
                ->paginate(10);

            foreach ($discussions as $discussion) {

                $data .= view('site.place2.partials.post_discussion', array('discussion' => $discussion, 'place' => $place, 'search_result' => true));
            }


            // getting text post for place
            $post_ary = PostsCountries::where('countries_id', $place->id)->pluck('posts_id')->toArray();
            if (count($post_ary) > 0) {
                $posts = Posts::whereIn('id', $post_ary)
                    ->whereRaw('text like "%' . $query . '%"')
                    ->paginate(10);

                foreach ($posts as $post) {

                    $data .= view('site.place2.partials.post_text', array('post' => $post, 'place' => $place, 'search_result' => true));
                }
            }

            // getting media post for place
            $media_ary = $place->medias->pluck('id')->toArray();
            $post_ary = PostsMedia::whereIn('medias_id', $media_ary)->pluck('posts_id')->toArray();
            if (count($post_ary) > 0) {
                $media = Posts::whereIn('id', $post_ary)
                    ->whereRaw('text like "%' . $query . '%"')
                    ->paginate(10);

                foreach ($media as $m) {
                    $data .= view('site.place2.partials.media_block', array('media' => $m, 'place' => $place, 'search_result' => true));
                }
            }

            // getting travelmates for place
            $mates_ary = User::whereRaw('(name like "%' . $query . '%" or interests like "%' . $query . '%")')
                ->pluck('id');

            $travelmates = TravelMatesRequests::whereHas('plan_country', function ($q) use ($place) {
                $q->where('countries_id', '=', $place->id);
            })
                ->whereIn('users_id', $mates_ary)
                ->paginate(10);
            // if (count($travelmates) == 0) {
            //     $travelmates = TravelMatesRequests::whereHas('plan_city', function($q) use ($place) {
            //                 $q->where('cities_id', '=', $place->city->id);
            //             })
            //             ->whereIn('users_id', $mates_ary)
            //             ->paginate(10);
            // }

            foreach ($travelmates as $travelmate_request) {
                $data .= view('site.place2.partials.post_travelmate', array('travelmate_request' => $travelmate_request, 'place' => $place, 'search_result' => true));
            }

            $reports = Reports::where('countries_id', '=', $place->id)
                ->get();

            foreach ($reports as $rp) {
                $data .= view('site.place2.partials.post_report', array('report' => $rp, 'place' => $place, 'search_result' => true));
            }

            // getting reviews for place
            $reviews = Reviews::where('countries_id', $place->id)
                ->whereRaw('text like "%' . $query . '%"')
                ->paginate(10);

            foreach ($reviews as $r) {
                $data .= view('site.place2.partials.post_review', array('review' => $r, 'place' => $place, 'search_result' => true));
            }


            //getting trip plans for place
            $trip_plans = TripPlaces::where('countries_id', $place->id)->pluck('trips_id')->toArray();

            if (count($trip_plans) > 0) {
                $trips = TripPlans::whereIn('id', $trip_plans)
                    ->whereRaw('title like "%' . $query . '%"')
                    ->paginate(10);

                foreach ($trips as $tr) {
                    $data .= view('site.place2.partials.post_plan', array('plan' => $tr, 'place' => $place, 'search_result' => true));
                }
            }

            //getting events for place
            $events = array();
            try {
                $client = new Client();
                $result = @$client->post('https://auth.predicthq.com/token', [
                    'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
                    'form_params' => [
                        'grant_type' => 'client_credentials',
                        'scope' => 'account events signals'
                    ],
                    'verify' => false
                ]);
                $events_array = false;
                if ($result->getStatusCode() == 200) {
                    $json_response = $result->getBody()->read(1024);
                    $response = json_decode($json_response);
                    $access_token = $response->access_token;

                    $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                        'headers' => ['Authorization' => 'Bearer ' . $access_token],
                        'verify' => false
                    ]);
                    if ($get_events1->getStatusCode() == 200) {
                        $events1 = json_decode($get_events1->getBody()->read(100024));
                        //dd($events);
                        $events_array1 = $events1->results;
                        $events_final = $events_array1;
                        if ($events1->next) {
                            $get_events2 = $client->get($events1->next, [
                                'headers' => ['Authorization' => 'Bearer ' . $access_token],
                                'verify' => false
                            ]);
                            if ($get_events2->getStatusCode() == 200) {
                                $events2 = json_decode($get_events2->getBody()->read(100024));
                                //dd($events);
                                $events_array2 = $events2->results;
                                $events_final = array_merge($events_array1, $events_array2);
                            }
                        }
                        $events = $events_final;
                    }
                }

                if (count($events) > 0) {
                    foreach ($events as $evt) {
                        if (strpos($evt->title, $query) || strpos($evt->description, $query) || strpos(@$evt->entities[0]->formatted_address, $query)) {
                            $data .= view('site.place2.partials.post_event', array('evt' => $evt, 'place' => $place, 'search_result' => true));
                        }
                    }
                }
            } catch (\Throwable $th) {
            }
            echo $data;
        }
    }

    public function discussion_replies_24hrs($place_id, Request $request)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');

        $filters = View::make('site.place2.partials._posts_filters_block');
        $output = $filters->render();

        $output = '';

        $posts_discussion_collection = array();
        $get_posts_discussion_collection = DiscussionReplies::whereHas('discussion', function ($q) use ($place_id) {
            $q->where('destination_type', 'country')
                ->where('destination_id', $place_id);
        })
            ->where('created_at', '>', date("Y-m-d", strtotime('-24 hours')))
            ->where('users_id', $user_id)
            ->orderBy('id', 'DESC')
            ->get();
        if (count($get_posts_discussion_collection) > 0) {
            foreach ($get_posts_discussion_collection as $gpdc) {
                $posts_discussion_collection[] = array(
                    'id' => $gpdc->discussions_id,
                    'type' => 'discussion',
                    'timestamp' => $gpdc->created_at
                );

                $view = View::make('site.place2.partials.post_discussion', [
                    'place' => $place,
                    'discussion' => $gpdc->discussion
                ]);
                $output .= $view->render();
            }
        } else {
            $output = 'no';
        }
        return json_encode($output);
    }

    public function newsfeed_show_all($place_id, Request $request)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');

        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = [];
        $filter = $request->all();
        $all_collection  = getPostsIdForAllFeed($place_id, $page, 'country', $filter);
        $counter = 0;

        $more = '';
        if (count($all_collection) > 0) {
            $postsId = array_map(function($post) {
                return $post = $post['id'];
            }, $all_collection);

            $show_post = Posts::whereIn('id', $postsId)->select('id', 'users_id', 'permission')->get()->toArray();
            foreach ($all_collection as $ppost) {
                $post_permission = [];
                foreach($show_post as $post) {
                    if($post['id'] == $ppost['id']) {
                        $post_permission = $post;
                    }
                }
                
                if (Auth::check() && isset($post_permission['permission']) && $post_permission['permission'] == Posts::POST_PERMISSION_PRIVATE && $post_permission['users_id'] != Auth::user()->id) {
                    continue;
                } elseif (Auth::check() && isset($post_permission['permission']) && $post_permission['permission'] == Posts::POST_PERMISSION_FRIENDS_ONLY && $post_permission['users_id'] != Auth::user()->id and !in_array($post_permission['users_id'], getUserFriendsIds())) {
                    continue;
                }
                $more = '';
                if ($ppost['type'] == 'regular') {
                    $city_x = [];
                    $place_X = [];
                    if ($ppost['city'] != 0) {
                        $ret =  CitiesTranslations::where('cities_id', $ppost['city'])->first();
                        if (isset($ret) && is_object($ret)) {
                            $city_x['title'] = $ret->title;
                            $city_x['id'] = $ret->places_id;
                        }
                    }
                    if (empty($city_x) && $ppost['place'] != 0) {
                        $ret = PlaceTranslations::where('places_id', $ppost['place'])->first();
                        if (isset($ret) && is_object($ret)) {
                            $place_X['title'] = $ret->title;
                            $place_X['id'] = $ret->places_id;
                        }
                    }
                    $more .= view('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $ppost['id']]));

                    // $post = Posts::find($ppost['id']);
                    // if(is_object($post)){
                    //     if ($post->text != '' && count($post->medias) > 0) {
                    //         $more = View::make('site.place2.partials.post_media_with_text', array('post'=>$post, 'place'=>$place, 'selected_place'=>$place_X, 'selected_city'=>$city_x));
                    //         $output .= $more->render();
                    //     } elseif ($post->text == '' && count($post->medias) > 0) {
                    //         if(!is_object($post->author)){
                    //             if(isset($post->medias[0]->media))
                    //                 $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0], 'selected_place'=>$place_X, 'selected_city'=>$city_x));
                    //         }else
                    //             $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0], 'selected_place'=>$place_X, 'selected_city'=>$city_x));
                    //     } else {
                    //         $more = View::make('site.place2.partials.post_text', array('post'=>$post, 'place'=>$place, 'selected_place'=>$place_X, 'selected_city'=>$city_x));
                    //     }
                    // }
                } elseif ($ppost['type'] == 'discussion') {
                    $more = '';
                    $more .= view('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $ppost['id']]));

                    // $discussion = Discussion::find($ppost['id']);
                    //     $more = View::make('site.place2.partials.post_discussion', array('discussion'=>$discussion, 'place'=>$place));
                } elseif ($ppost['type'] == 'plan') {
                    $counter++;
                    $more = '';
                    // $plan = TripPlaces::find($ppost['id']);
                    // if(isset($plan->trip))
                    $more .= view('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $ppost['id']]));
                } elseif ($ppost['type'] == 'report') {
                    $more = '';
                    $more .= view('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $ppost['id']]));
                    // $report = Reports::find($ppost['id']);
                    // if ($report) {
                    //     $more = View::make('site.place2.partials.post_report', array('report'=>$report, 'place'=>$place));
                    // }
                } elseif ($ppost['type'] == 'review') {
                    $more = '';
                    $more .= view('site.place2.new.review', array('post' => ['type' => 'Review', 'variable' => $ppost['id']]));
                    // $reviews = Reviews::find($ppost['id']);
                    // if(is_object($reviews))
                    //     $more = View::make('site.place2.partials.post_review', ['place' => $place,'review' => $reviews]);
                }


                $timestamp = (new Carbon($ppost['timestamp']))->timestamp;
                if (!array_key_exists($timestamp, $output)) {
                    $output[$timestamp] = '';
                }
                if (isset($more) && $more != '')
                    $output[$timestamp] .= $more;
            }
        }
        //$output .= $this->newsfeed_show_travelmates($place_id, $request, true, true);
        //$output .= $this->newsfeed_show_events($place_id, $request, true);
        //$output .= $this->newsfeed_show_reports($place_id, $request, true, true);
        //$output .= $this->newsfeed_show_reviews($place_id, $request, true, true);

        krsort($output);

        $output = implode($output);
        if (!$output) {
            $output = 'no';
        }
        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_about($place_id, Request $request)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $data['city'] = $place;

        $data['country_weather'] = false;
        $data['current_weather'] = false;
        $data['daily_weather'] = false;
        $get_place_key = curl_init();
        curl_setopt_array($get_place_key, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q=' . $place->lat . ',' . $place->lng,
        ));
        $resp = curl_exec($get_place_key);
        curl_close($get_place_key);
        if ($resp) {
            $resp = json_decode($resp);
            $place_key = $resp->Key;

            $get_country_weather = curl_init();
            curl_setopt_array($get_country_weather, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
            ));
            $country_weather = curl_exec($get_country_weather);
            curl_close($get_country_weather);
            $data['country_weather'] = json_decode($country_weather);

            $get_current_weather = curl_init();
            curl_setopt_array($get_current_weather, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/currentconditions/v1/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
            ));
            $current_weather = curl_exec($get_current_weather);
            curl_close($get_current_weather);
            $data['current_weather'] = json_decode($current_weather);


            $get_daily_weather = curl_init();
            curl_setopt_array($get_daily_weather, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/daily/10day/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
            ));
            $daily_weather = curl_exec($get_daily_weather);
            curl_close($get_daily_weather);
            $data['daily_weather'] = json_decode($daily_weather)->DailyForecasts;
        }
        // $data  = [];
        $holiday = [];
        foreach ($data['city']->countryHolidays as $cHoliday) {
            $holiday[] = $cHoliday['date'];
        }
        $data['cHoliday'] =  $holiday;
        $about = View::make('site.country2.partials.post_about', $data);
        $output = $about->render();



        return json_encode($output);
    }

    public function newsfeed_show_discussions($place_id, Request $request)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        $is_parent_flag = $request->get('is_parent');
        $parent_indicator = 0 ;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();
        $filter = $request->all();
        $output = '';
        if (isset($filter['location']) || isset($filter['people'])) {
            $users_id_array = getUsersByFilters($filter);
            $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
                ->where('destination_type', 'country')
                ->orderBy('id', 'desc')
                ->whereIn('users_id', $users_id_array)
                ->skip($skip)
                ->take(5)
                ->get();
        } else {
            $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
                ->where('destination_type', 'country')
                ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`) as replies')
                ->orderBy('replies', 'DESC')
                ->offset($skip)
                ->limit(5)
                ->get();
        }
        if(count($get_posts_discussions_collection) ==0) {
            if(!isset($is_parent_flag)){
                $parent_indicator =1;
                $skip =0;
            }else {
                $parent_indicator =0;
            }
                $cities = Cities::where('countries_id',$place_id)->get()->pluck('id')->toArray();
                $get_posts_discussions_collection = Discussion::whereIn('destination_id', $cities)
                ->where('destination_type', 'city')
                ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`) as replies')
                ->orderBy('replies', 'DESC')
                ->offset($skip)
                ->limit(5)
                ->get();
           
        }
        if (count($get_posts_discussions_collection) > 0) {
            foreach ($get_posts_discussions_collection as $gpdc) {
                if($parent_indicator){
                    $view = View::make('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $gpdc->id],'is_parent'=>true,'text'=>'Discussions about '.$place->transsingle->title.' citites'));

                }else{
                    $view = View::make('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $gpdc->id]));

                }

                // $view = View::make('site.place2.partials.post_discussion', ['place' => $place,
                //             'discussion' => $gpdc]);
                $output .= $view->render();
            }
        } else {
            $output = 'no';
        }

        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_top($place_id, Request $request)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = [];
        $filter = $request->all();
        $all_collection = collect_posts('country', $place_id, $page, true, false, $filter);
        if (count($all_collection) > 0) {
            foreach ($all_collection as $ppost) {
                $more = '';

                if ($ppost['type'] == 'regular') {
                    $more = view('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $ppost['id']]));

                    // $post = Posts::find($ppost['id']);
                    // if(is_object($post)){
                    //     if ($post->text != '' && count($post->medias) > 0) {
                    //         $more = View::make('site.place2.partials.post_media_with_text', array('post'=>$post, 'place'=>$place));
                    //     } elseif ($post->text == '' && count($post->medias) > 0) {
                    //         if(!is_object($post->author)){
                    //             if(isset($post->medias[0]->media))
                    //                 $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));
                    //         }else
                    //             $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));
                    //     } else {
                    //         $more = View::make('site.place2.partials.post_text', array('post'=>$post, 'place'=>$place));
                    //     }
                    // }
                } elseif ($ppost['type'] == 'discussion') {
                    $more = view('site.place2.new.discussion', array('post' => ['type' => 'Discussion', 'variable' => $ppost['id']]));

                    // $discussion = Discussion::find($ppost['id']);
                    // if(is_object($discussion))
                    //     $more = View::make('site.place2.partials.post_discussion', array('discussion'=>$discussion, 'place'=>$place));
                } elseif ($ppost['type'] == 'plan') {
                    $more = view('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $ppost['id']]));

                    // $plan = TripPlaces::find($ppost['id']);
                    // if(isset($plan->trip)){
                    //     $more = View::make('site.place2.partials.post_plan', array('plan'=>$plan->trip, 'place'=>$place));
                    // }
                } elseif ($ppost['type'] == 'report') {
                    $more = view('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $ppost['id']]));

                    // $report = Reports::find($ppost['id']);
                    // if (is_object($report)) {
                    //     $more = View::make('site.place2.partials.post_report', array('report'=>$report, 'place'=>$place));
                    // }
                } elseif ($ppost['type'] == 'review') {
                    $more = view('site.place2.new.review', array('post' => ['type' => 'Review', 'variable' => $ppost['id']]));

                    // $reviews = Reviews::find($ppost['id']);
                    // if(is_object($reviews))
                    //     $more = View::make('site.place2.partials.post_review', ['place' => $place,'review' => $reviews]);
                }

                $timestamp = (new Carbon($ppost['timestamp']))->timestamp;
                if (!array_key_exists($timestamp, $output)) {
                    $output[$timestamp] = '';
                }
                if (isset($more) && $more != '')
                    $output[$timestamp] .= $more;
            }
        }


        //$output .= $this->newsfeed_show_travelmates($place_id, $request, true, true);
        //$output .= $this->newsfeed_show_events($place_id, $request, true);
        //$output .= $this->newsfeed_show_reports($place_id, $request, true, true);
        //$output .= $this->newsfeed_show_reviews($place_id, $request, true, true);
        krsort($output);

        $output = implode($output);
        if ($output == [] || $output == '') {
            $output = 'no';
        }
        // $output=  utf8_encode($output);

        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_media($place_id, Request $request)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        $filter = $request->all();
        $all_collection = collect_posts('country', $place_id, $page, false, true, $filter);

        if (count($all_collection) > 0) {
            foreach ($all_collection as $ppost) {

                if ($ppost['type'] == 'regular') {
                    // $post = Posts::find($ppost['id']);
                    $output .= view('site.place2.new.posts', array('post' => ['type' => 'Post', 'variable' => $ppost['id']]));

                    // if ($post->text != '' && count($post->medias) > 0) {
                    //     $more = View::make('site.place2.partials.post_media_with_text', array('post'=>$post));
                    //     $output .= $more->render();
                    // } elseif ($post->text == '' && count($post->medias) > 0) {
                    //     if(is_object($post->author))
                    //         if(isset($post->medias[0]->media)){
                    //             $more = View::make('site.place2.partials.post_media_without_text', array('post'=>$post));
                    //             $output .= $more->render();
                    //         }
                    // } else {
                    // }
                }
            }
        } else {
            $output = 'no';
        }

        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_tripplan($place_id, Request $request)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        $filter = $request->all();
        $triplist = get_triplist4me($filter, $place_id, 'country');

        $get_posts_plans_collection = Tripplans::leftjoin('trips_places', 'trips_places.trips_id', '=', 'trips.id')
            ->selectRaw('distinct(trips.id) as id, ((select count(*) from `posts_comments` where `posts_id`=`trips`.`id` and type="trip") 
                    + (select count(*) from `trips_likes` where `trips_id`=`trips`.`id`) 
                    + (select count(*) from `posts_shares` where `posts_type_id`=`trips`.`id` and type="trip")) as ranks')
            ->whereIn('trips_id', $triplist)
            ->groupBy('trips.id')
            ->orderBy('ranks', 'DESC')
            ->paginate(5)->pluck('id')->toArray();
        // $get_posts_plans_collection = TripPlaces::where('countries_id', $place_id)
        //             ->whereIn('trips_id', $triplist)
        //             ->orderBy('id', 'DESC')
        //             ->paginate(5);
        // ->skip($skip)
        // ->take(5)
        // ->get();
        $get_posts_plans_collection = Tripplans::whereIn('id', $get_posts_plans_collection)->get();
        if ($get_posts_plans_collection->count() > 0) {
            foreach ($get_posts_plans_collection as $gpdc) {
                $view = View::make('site.place2.new.trip', array('post' => ['type' => 'Trip', 'variable' => $gpdc->id]));

                // $view = View::make('site.place2.partials.post_plan', ['place' => $place,
                //             'plan' => $gpdc->trip]);
                $output .= $view->render();
            }
        } else {
            $output = 'no';
        }

        return json_encode(['html'=>$output]);
    }

    public function newsfeed_show_travelmates($place_id, Request $request, $internal = false, $top = false)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        $get_posts_mates_collection = TravelMatesRequests::whereHas('plan_country', function ($q) use ($place) {
            $q->where('countries_id', '=', $place->id);
        })
            ->groupBy('users_id')
            ->orderBy('id', 'desc')
            ->paginate(5);
        // ->skip($skip)
        // ->take(5)
        // ->get();

        if ($get_posts_mates_collection->count() > 0) {
            foreach ($get_posts_mates_collection as $gpdc) {
                $view = View::make('site.place2.partials.post_travelmate', [
                    'place' => $place,
                    'travelmate_request' => $gpdc
                ]);
                $output .= $view->render();
            }
        } else {
            $output = $internal ? '' : 'no';
        }

        return $internal ? $output : json_encode($output);
    }

    public function newsfeed_show_events($place_id, Request $request, $internal = false)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;

        $output = '';

        $get_posts_events_collection = Events::where('countries_id', $place_id)
            ->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')
            ->orderBy('id', 'desc')
            ->skip($skip)
            ->take(5)
            ->get();
        if (count($get_posts_events_collection) > 0) {
            foreach ($get_posts_events_collection as $gpec) {
                $view = View::make('site.place2.partials.post_event', [
                    'place' => $place,
                    'evt' => $gpec
                ]);
                $output .= $view->render();
            }
        } else {
            $output = $internal ? '' : 'no';
        }

        return $internal ? $output : json_encode(['html'=>$output]);
    }

    public function newsfeed_show_reports($place_id, Request $request, $internal = false, $top = false)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        $is_parent =0;

        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();
        $filter = $request->all();
        $output = '';

        if (!$top) {
            if (isset($filter['location']) || isset($filter['people'])) {
                $users_id_array = getUsersByFilters($filter);
                $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($place_id) {
                    $q->where('location_type', Reports::LOCATION_COUNTRY)
                        ->where('location_id', $place_id);
                })
                    ->whereIn('users_id', $users_id_array)
                    ->orderBy('id', 'DESC')
                    ->whereNotNull('published_date')
                    ->where('flag', 1)
                    ->paginate(5);
            } else {
                $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($place_id) {
                    $q->where('location_type', Reports::LOCATION_COUNTRY)
                        ->where('location_id', $place_id);
                })
                    ->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`reports`.`id` and type = "report" and created_at >  "' . Carbon::now()->subDays(7) . '") 
                                + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id` and created_at >  "' . Carbon::now()->subDays(7) . '") 
                                + (select count(*) from `posts_shares` where `posts_type_id`=`reports`.`id` and type ="report" and created_at >  "' . Carbon::now()->subDays(7) . '")) as rank')
                    ->orderBy('rank', 'DESC')
                    ->whereNotNull('published_date')
                    ->where('flag', 1)
                    ->paginate(5);
                    if($get_posts_reports_collection->count()  == 0){
                        $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($place_id) {
                            $q->where('location_type', Reports::LOCATION_COUNTRY)
                                ->where('location_id', $place_id);
                        })
                            ->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`reports`.`id` and type = "report" and created_at <  "' . Carbon::now()->subDays(7) . '") 
                                        + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id` and created_at < "' . Carbon::now()->subDays(7) . '") 
                                        + (select count(*) from `posts_shares` where `posts_type_id`=`reports`.`id` and type ="report" and created_at <  "' . Carbon::now()->subDays(7) . '")) as rank')
                            ->orderBy('rank', 'DESC')
                            ->whereNotNull('published_date')
                            ->where('flag', 1)
                            ->paginate(5);
                    }                       
                    if($get_posts_reports_collection->count()  == 0){
                        $cities = Cities::where('countries_id',$place_id)->get()->pluck('id')->toArray();
                        $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($cities) {
                            $q->where('location_type', Reports::LOCATION_CITY)
                                ->whereIn('location_id', $cities);
                        })
                            ->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`reports`.`id` and type = "report") 
                                        + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id` ) 
                                        + (select count(*) from `posts_shares` where `posts_type_id`=`reports`.`id` and type ="report")) as rank')
                            ->orderBy('rank', 'DESC')
                            ->whereNotNull('published_date')
                            ->where('flag', 1)
                            ->paginate(5);
                            $is_parent =1;

                    }
                // $get_posts_reports_collection = Reports::where('countries_id', '=', $place->id)->orderBy('id','DESC')
                //     ->paginate(5);
            }
            // ->skip($skip)
            // ->take(5)
            // ->get();
        } else {

            $get_posts_reports_collection = Reports::whereHas('location', function ($q) use ($place_id) {
                $q->where('location_type', Reports::LOCATION_COUNTRY)
                    ->where('location_id', $place_id);
            })
                ->whereNotNull('published_date')
                ->where('flag', 1)
                ->selectRaw('*, ((select count(*) from `reports_comments` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `reports_views` where `reports_id`=`reports`.`id`) 
                                    + (select count(*) from `reports_shares` where `reports_id`=`reports`.`id`)) as rank')
                ->orderBy('rank', 'DESC')
                ->paginate(5);
            // ->skip($skip)
            // ->take(5)
            // ->get();
        }
        if ($get_posts_reports_collection->count() > 0) {
            foreach ($get_posts_reports_collection as $gpdc) {
                if($is_parent == 1){
                    $view = View::make('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $gpdc->id],'is_parent'=>true,'text'=>'Reports for '.$place->transsingle->title .' Cities'));

                }else{
                    $view = View::make('site.place2.new.report', array('post' => ['type' => 'Report', 'variable' => $gpdc->id]));

                }
                // $view = View::make('site.place2.partials.post_report', ['place' => $place,
                //             'report' => $gpdc]);
                $output .= $view->render();
            }
        } else {
            $output = $internal ? '' : 'no';
        }

        return $internal ? $output : json_encode(['html'=>$output]);
    }

    public function newsfeed_show_reviews($place_id, Request $request, $internal = false, $top = false)
    {

        $place = Countries::find($place_id);
        $user_id = $request->get('user_id');
        $page = $request->get('page');
        $skip = ($page - 1) * 5;
        //$filters = View::make('site.place2.partials._posts_filters_block');
        //$output = $filters->render();

        $output = '';
        if (!$top) {

            $get_posts_review_collection = Reviews::where('countries_id', $place_id)
                ->orderBy('id', 'desc')
                ->skip($skip)
                ->take(5)
                ->get();
        } else {
            $get_posts_review_collection = Reviews::where('countries_id', $place_id)
                ->selectRaw('*, ((select count(*) from `reviews_votes` where `review_id`=`reviews`.`id` and `vote_type`=1) 
                                    + (select count(*) from `reviews_shares` where `review_id`=`reviews`.`id`)) as rank')
                ->orderBy('rank', 'desc')
                ->skip($skip)
                ->take(5)
                ->get();
        }
        if (count($get_posts_review_collection) > 0) {
            foreach ($get_posts_review_collection as $gpdc) {
                $view = View::make('site.place2.new.review', array('post' => ['type' => 'Review', 'variable' => $gpdc->id]));
                // $view = View::make('site.place2.partials.post_review', ['place' => $place,
                //             'review' => $gpdc]);
                $output .= $view->render();
            }
        } else {
            $output = $internal ? '' : 'no';
        }

        return $internal ? $output : json_encode(['html'=>$output]);
    }
    // public function getWeather($country_id) {
    //     $language_id = 1;
    //     $country = Countries::find($country_id);

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $language = Languages::where('id', $language_id)->first();

    //     if (!$language) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Language ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $country = Countries::with([
    //                 'trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'region.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'languages.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'holidays.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'religions.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'currencies.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'capitals.city.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'emergency.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'places' => function ($query) {
    //                     $query->where('media_count', '>', 0)
    //                     ->limit(50);
    //                 },
    //                 'atms' => function ($query) {
    //                     $query->where('place_type', 'like', '%atm%');
    //                 },
    //                 'hotels',
    //                 'cities',
    //                 'cities.getMedias.trans' => function ($query) use ($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'getMedias',
    //                 'getMedias.users',
    //                 'timezone',
    //                 'followers'
    //             ])
    //             ->where('id', $country_id)
    //             ->where('active', 1)
    //             ->first();

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 404,
    //                 'message' => 'Not found',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $placeCount = Place::where('countries_id', '=', $country->id)
    //             ->count();
    //     $languages = $country->getLanguages();
    //     $country->setRelation('languages', collect($languages));


    //     $citiesMediasSql = DB::table('cities_medias')
    //             ->select([
    //                 'cities_id',
    //                 DB::raw('max(medias_id) AS medias_id')
    //             ])
    //             ->groupBy('cities_id')
    //             ->toSql();

    //     $tripPlans = Country::select(
    //                     'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
    //             )
    //             ->join('places', 'countries.id', '=', 'places.countries_id')
    //             ->join('trips_places', function($join) {
    //                 $join->on('trips_places.countries_id', '=', 'countries.id')
    //                 ->on('trips_places.places_id', '=', 'places.id');
    //             })
    //             ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //             ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //             ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //             ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //             ->where('countries.id', $country_id)
    //             ->get();

    //     $sumBudget = 0;
    //     foreach ($tripPlans as $tripPlan) {
    //         $sumBudget += $tripPlan['budget'];
    //         $media = [
    //             'id' => $tripPlan['media_id'],
    //             'url' => $tripPlan['url']
    //         ];
    //         unset($tripPlan['media_id'], $tripPlan['url']);
    //         $tripPlan['medias'] = (object) $media;
    //     }

    //     $airports = Place::where('countries_id', '=', $country->id)
    //             ->where('place_type', 'like', 'airport,%')
    //             ->limit(10)
    //             ->get();

    //     $data['plans'] = $tripPlans;
    //     $data['plans_count'] = count($tripPlans);
    //     $data['sum_budget'] = $sumBudget;

    //     $data['country'] = $country;
    //     $data['airports'] = $airports;

    //     $get_place_key = curl_init();
    //         curl_setopt_array($get_place_key, array(
    //             CURLOPT_RETURNTRANSFER => 1,
    //             CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q='.$country->lat.','.$country->lng,
    //         ));
    //         $resp = curl_exec($get_place_key);
    //         curl_close($get_place_key);
    //         if ($resp) {
    //             $resp = json_decode($resp);
    //             $place_key = $resp->Key;

    //             $get_country_weather = curl_init();
    //             curl_setopt_array($get_country_weather, array(
    //                 CURLOPT_RETURNTRANSFER => 1,
    //                 CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
    //             ));
    //             $country_weather = curl_exec($get_country_weather);
    //             curl_close($get_country_weather);
    //             $data['country_weather'] = json_decode($country_weather);

    //             $get_current_weather = curl_init();
    //             curl_setopt_array($get_current_weather, array(
    //                 CURLOPT_RETURNTRANSFER => 1,
    //                 CURLOPT_URL => 'http://dataservice.accuweather.com/currentconditions/v1/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
    //             ));
    //             $current_weather = curl_exec($get_current_weather);
    //             curl_close($get_current_weather);
    //             $data['current_weather'] = json_decode($current_weather);


    //             $get_daily_weather = curl_init();
    //             curl_setopt_array($get_daily_weather, array(
    //                 CURLOPT_RETURNTRANSFER => 1,
    //                 CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/daily/10day/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
    //             ));
    //             $daily_weather = curl_exec($get_daily_weather);
    //             curl_close($get_daily_weather);
    //             $data['daily_weather'] = json_decode($daily_weather)->DailyForecasts;

    //         }

    //         $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

    //     return view('site.country2.weather', $data);
    // }

    // public function getEtiquette($country_id) {
    //     $language_id = 1;
    //     $country = Countries::find($country_id);

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $language = Languages::where('id', $language_id)->first();

    //     if (!$language) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Language ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $country = Countries::with([
    //                 'trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'region.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'languages.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'holidays.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'religions.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'currencies.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'capitals.city.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'emergency.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'places' => function ($query) {
    //                     $query->where('media_count', '>', 0)
    //                     ->limit(50);
    //                 },
    //                 'atms' => function ($query) {
    //                     $query->where('place_type', 'like', '%atm%');
    //                 },
    //                 'hotels',
    //                 'cities',
    //                 'cities.getMedias.trans' => function ($query) use ($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'getMedias',
    //                 'getMedias.users',
    //                 'timezone',
    //                 'followers'
    //             ])
    //             ->where('id', $country_id)
    //             ->where('active', 1)
    //             ->first();

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 404,
    //                 'message' => 'Not found',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $placeCount = Place::where('countries_id', '=', $country->id)
    //             ->count();
    //     $languages = $country->getLanguages();
    //     $country->setRelation('languages', collect($languages));


    //     $citiesMediasSql = DB::table('cities_medias')
    //             ->select([
    //                 'cities_id',
    //                 DB::raw('max(medias_id) AS medias_id')
    //             ])
    //             ->groupBy('cities_id')
    //             ->toSql();

    //     $tripPlans = Country::select(
    //                     'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
    //             )
    //             ->join('places', 'countries.id', '=', 'places.countries_id')
    //             ->join('trips_places', function($join) {
    //                 $join->on('trips_places.countries_id', '=', 'countries.id')
    //                 ->on('trips_places.places_id', '=', 'places.id');
    //             })
    //             ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //             ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //             ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //             ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //             ->where('countries.id', $country_id)
    //             ->get();

    //     $sumBudget = 0;
    //     foreach ($tripPlans as $tripPlan) {
    //         $sumBudget += $tripPlan['budget'];
    //         $media = [
    //             'id' => $tripPlan['media_id'],
    //             'url' => $tripPlan['url']
    //         ];
    //         unset($tripPlan['media_id'], $tripPlan['url']);
    //         $tripPlan['medias'] = (object) $media;
    //     }

    //     $airports = Place::where('countries_id', '=', $country->id)
    //             ->where('place_type', 'like', 'airport,%')
    //             ->limit(10)
    //             ->get();

    //     $data['plans'] = $tripPlans;
    //     $data['plans_count'] = count($tripPlans);
    //     $data['sum_budget'] = $sumBudget;

    //     $data['country'] = $country;
    //     $data['airports'] = $airports;

    //     $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

    //     return view('site.country2.etiquette', $data);
    // }

    // public function getPackingTips($country_id) {
    //     $language_id = 1;
    //     $country = Countries::find($country_id);

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $language = Languages::where('id', $language_id)->first();

    //     if (!$language) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Language ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $country = Countries::with([
    //                 'trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'region.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'languages.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'holidays.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'religions.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'currencies.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'capitals.city.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'emergency.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'places' => function ($query) {
    //                     $query->where('media_count', '>', 0)
    //                     ->limit(50);
    //                 },
    //                 'atms' => function ($query) {
    //                     $query->where('place_type', 'like', '%atm%');
    //                 },
    //                 'hotels',
    //                 'cities',
    //                 'cities.getMedias.trans' => function ($query) use ($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'getMedias',
    //                 'getMedias.users',
    //                 'timezone',
    //                 'followers'
    //             ])
    //             ->where('id', $country_id)
    //             ->where('active', 1)
    //             ->first();

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 404,
    //                 'message' => 'Not found',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $placeCount = Place::where('countries_id', '=', $country->id)
    //             ->count();
    //     $languages = $country->getLanguages();
    //     $country->setRelation('languages', collect($languages));


    //     $citiesMediasSql = DB::table('cities_medias')
    //             ->select([
    //                 'cities_id',
    //                 DB::raw('max(medias_id) AS medias_id')
    //             ])
    //             ->groupBy('cities_id')
    //             ->toSql();

    //     $tripPlans = Country::select(
    //                     'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
    //             )
    //             ->join('places', 'countries.id', '=', 'places.countries_id')
    //             ->join('trips_places', function($join) {
    //                 $join->on('trips_places.countries_id', '=', 'countries.id')
    //                 ->on('trips_places.places_id', '=', 'places.id');
    //             })
    //             ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //             ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //             ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //             ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //             ->where('countries.id', $country_id)
    //             ->get();

    //     $sumBudget = 0;
    //     foreach ($tripPlans as $tripPlan) {
    //         $sumBudget += $tripPlan['budget'];
    //         $media = [
    //             'id' => $tripPlan['media_id'],
    //             'url' => $tripPlan['url']
    //         ];
    //         unset($tripPlan['media_id'], $tripPlan['url']);
    //         $tripPlan['medias'] = (object) $media;
    //     }

    //     $airports = Place::where('countries_id', '=', $country->id)
    //             ->where('place_type', 'like', 'airport,%')
    //             ->limit(10)
    //             ->get();

    //     $data['plans'] = $tripPlans;
    //     $data['plans_count'] = count($tripPlans);
    //     $data['sum_budget'] = $sumBudget;

    //     $data['country'] = $country;
    //     $data['airports'] = $airports;

    //     $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();


    //     return view('site.country2.packing-tips', $data);
    // }


    // public function getHealth($country_id) {
    //     $language_id = 1;
    //     $country = Countries::find($country_id);

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $language = Languages::where('id', $language_id)->first();

    //     if (!$language) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Language ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $country = Countries::with([
    //                 'trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'region.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'languages.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'holidays.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'religions.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'currencies.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'capitals.city.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'emergency.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'places' => function ($query) {
    //                     $query->where('media_count', '>', 0)
    //                     ->limit(50);
    //                 },
    //                 'atms' => function ($query) {
    //                     $query->where('place_type', 'like', '%atm%');
    //                 },
    //                 'hotels',
    //                 'cities',
    //                 'cities.getMedias.trans' => function ($query) use ($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'getMedias',
    //                 'getMedias.users',
    //                 'timezone',
    //                 'followers'
    //             ])
    //             ->where('id', $country_id)
    //             ->where('active', 1)
    //             ->first();

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 404,
    //                 'message' => 'Not found',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $placeCount = Place::where('countries_id', '=', $country->id)
    //             ->count();
    //     $languages = $country->getLanguages();
    //     $country->setRelation('languages', collect($languages));


    //     $citiesMediasSql = DB::table('cities_medias')
    //             ->select([
    //                 'cities_id',
    //                 DB::raw('max(medias_id) AS medias_id')
    //             ])
    //             ->groupBy('cities_id')
    //             ->toSql();

    //     $tripPlans = Country::select(
    //                     'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
    //             )
    //             ->join('places', 'countries.id', '=', 'places.countries_id')
    //             ->join('trips_places', function($join) {
    //                 $join->on('trips_places.countries_id', '=', 'countries.id')
    //                 ->on('trips_places.places_id', '=', 'places.id');
    //             })
    //             ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //             ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //             ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //             ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //             ->where('countries.id', $country_id)
    //             ->get();

    //     $sumBudget = 0;
    //     foreach ($tripPlans as $tripPlan) {
    //         $sumBudget += $tripPlan['budget'];
    //         $media = [
    //             'id' => $tripPlan['media_id'],
    //             'url' => $tripPlan['url']
    //         ];
    //         unset($tripPlan['media_id'], $tripPlan['url']);
    //         $tripPlan['medias'] = (object) $media;
    //     }

    //     $airports = Place::where('countries_id', '=', $country->id)
    //             ->where('place_type', 'like', 'airport,%')
    //             ->limit(10)
    //             ->get();

    //     $data['plans'] = $tripPlans;
    //     $data['plans_count'] = count($tripPlans);
    //     $data['sum_budget'] = $sumBudget;

    //     $data['country'] = $country;
    //     $data['airports'] = $airports;

    //     $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();


    //     return view('site.country2.health', $data);
    // }

    // public function getVisaRequirements($country_id) {
    //     $language_id = 1;
    //     $country = Countries::find($country_id);

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $language = Languages::where('id', $language_id)->first();

    //     if (!$language) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Language ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $country = Countries::with([
    //                 'trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'region.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'languages.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'holidays.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'religions.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'currencies.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'capitals.city.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'emergency.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'places' => function ($query) {
    //                     $query->where('media_count', '>', 0)
    //                     ->limit(50);
    //                 },
    //                 'atms' => function ($query) {
    //                     $query->where('place_type', 'like', '%atm%');
    //                 },
    //                 'hotels',
    //                 'cities',
    //                 'cities.getMedias.trans' => function ($query) use ($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'getMedias',
    //                 'getMedias.users',
    //                 'timezone',
    //                 'followers'
    //             ])
    //             ->where('id', $country_id)
    //             ->where('active', 1)
    //             ->first();

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 404,
    //                 'message' => 'Not found',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $placeCount = Place::where('countries_id', '=', $country->id)
    //             ->count();
    //     $languages = $country->getLanguages();
    //     $country->setRelation('languages', collect($languages));


    //     $citiesMediasSql = DB::table('cities_medias')
    //             ->select([
    //                 'cities_id',
    //                 DB::raw('max(medias_id) AS medias_id')
    //             ])
    //             ->groupBy('cities_id')
    //             ->toSql();

    //     $tripPlans = Country::select(
    //                     'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
    //             )
    //             ->join('places', 'countries.id', '=', 'places.countries_id')
    //             ->join('trips_places', function($join) {
    //                 $join->on('trips_places.countries_id', '=', 'countries.id')
    //                 ->on('trips_places.places_id', '=', 'places.id');
    //             })
    //             ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //             ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //             ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //             ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //             ->where('countries.id', $country_id)
    //             ->get();

    //     $sumBudget = 0;
    //     foreach ($tripPlans as $tripPlan) {
    //         $sumBudget += $tripPlan['budget'];
    //         $media = [
    //             'id' => $tripPlan['media_id'],
    //             'url' => $tripPlan['url']
    //         ];
    //         unset($tripPlan['media_id'], $tripPlan['url']);
    //         $tripPlan['medias'] = (object) $media;
    //     }

    //     $airports = Place::where('countries_id', '=', $country->id)
    //             ->where('place_type', 'like', 'airport,%')
    //             ->limit(10)
    //             ->get();

    //     $data['plans'] = $tripPlans;
    //     $data['plans_count'] = count($tripPlans);
    //     $data['sum_budget'] = $sumBudget;

    //     $data['country'] = $country;
    //     $data['airports'] = $airports;

    //     $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

    //     return view('site.country2.visa-requirements', $data);
    // }

    // public function getWhenToGo($country_id) {
    //     $language_id = 1;
    //     $country = Countries::find($country_id);

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $language = Languages::where('id', $language_id)->first();

    //     if (!$language) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Language ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $country = Countries::with([
    //                 'trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'region.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'languages.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'holidays.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'religions.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'currencies.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'capitals.city.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'emergency.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'places' => function ($query) {
    //                     $query->where('media_count', '>', 0)
    //                     ->limit(50);
    //                 },
    //                 'atms' => function ($query) {
    //                     $query->where('place_type', 'like', '%atm%');
    //                 },
    //                 'hotels',
    //                 'cities',
    //                 'cities.getMedias.trans' => function ($query) use ($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'getMedias',
    //                 'getMedias.users',
    //                 'timezone',
    //                 'followers'
    //             ])
    //             ->where('id', $country_id)
    //             ->where('active', 1)
    //             ->first();

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 404,
    //                 'message' => 'Not found',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $placeCount = Place::where('countries_id', '=', $country->id)
    //             ->count();
    //     $languages = $country->getLanguages();
    //     $country->setRelation('languages', collect($languages));


    //     $citiesMediasSql = DB::table('cities_medias')
    //             ->select([
    //                 'cities_id',
    //                 DB::raw('max(medias_id) AS medias_id')
    //             ])
    //             ->groupBy('cities_id')
    //             ->toSql();

    //     $tripPlans = Country::select(
    //                     'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
    //             )
    //             ->join('places', 'countries.id', '=', 'places.countries_id')
    //             ->join('trips_places', function($join) {
    //                 $join->on('trips_places.countries_id', '=', 'countries.id')
    //                 ->on('trips_places.places_id', '=', 'places.id');
    //             })
    //             ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //             ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //             ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //             ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //             ->where('countries.id', $country_id)
    //             ->get();

    //     $sumBudget = 0;
    //     foreach ($tripPlans as $tripPlan) {
    //         $sumBudget += $tripPlan['budget'];
    //         $media = [
    //             'id' => $tripPlan['media_id'],
    //             'url' => $tripPlan['url']
    //         ];
    //         unset($tripPlan['media_id'], $tripPlan['url']);
    //         $tripPlan['medias'] = (object) $media;
    //     }

    //     $airports = Place::where('countries_id', '=', $country->id)
    //             ->where('place_type', 'like', 'airport,%')
    //             ->limit(10)
    //             ->get();

    //     $data['plans'] = $tripPlans;
    //     $data['plans_count'] = count($tripPlans);
    //     $data['sum_budget'] = $sumBudget;

    //     $data['country'] = $country;
    //     $data['airports'] = $airports;



    //     $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

    //     return view('site.country2.when-to-go', $data);
    // }

    // public function getCaution($country_id) {
    //     $language_id = 1;
    //     $country = Countries::find($country_id);

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $language = Languages::where('id', $language_id)->first();

    //     if (!$language) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Language ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $country = Countries::with([
    //                 'trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'region.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'languages.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'holidays.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'religions.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'currencies.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'capitals.city.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'emergency.trans' => function ($query) use($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'places' => function ($query) {
    //                     $query->where('media_count', '>', 0)
    //                     ->limit(50);
    //                 },
    //                 'atms' => function ($query) {
    //                     $query->where('place_type', 'like', '%atm%');
    //                 },
    //                 'hotels',
    //                 'cities',
    //                 'cities.getMedias.trans' => function ($query) use ($language_id) {
    //                     $query->where('languages_id', $language_id);
    //                 },
    //                 'getMedias',
    //                 'getMedias.users',
    //                 'timezone',
    //                 'followers'
    //             ])
    //             ->where('id', $country_id)
    //             ->where('active', 1)
    //             ->first();

    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 404,
    //                 'message' => 'Not found',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $placeCount = Place::where('countries_id', '=', $country->id)
    //             ->count();
    //     $languages = $country->getLanguages();
    //     $country->setRelation('languages', collect($languages));


    //     $citiesMediasSql = DB::table('cities_medias')
    //             ->select([
    //                 'cities_id',
    //                 DB::raw('max(medias_id) AS medias_id')
    //             ])
    //             ->groupBy('cities_id')
    //             ->toSql();

    //     $tripPlans = Country::select(
    //                     'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
    //             )
    //             ->join('places', 'countries.id', '=', 'places.countries_id')
    //             ->join('trips_places', function($join) {
    //                 $join->on('trips_places.countries_id', '=', 'countries.id')
    //                 ->on('trips_places.places_id', '=', 'places.id');
    //             })
    //             ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //             ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //             ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //             ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //             ->where('countries.id', $country_id)
    //             ->get();

    //     $sumBudget = 0;
    //     foreach ($tripPlans as $tripPlan) {
    //         $sumBudget += $tripPlan['budget'];
    //         $media = [
    //             'id' => $tripPlan['media_id'],
    //             'url' => $tripPlan['url']
    //         ];
    //         unset($tripPlan['media_id'], $tripPlan['url']);
    //         $tripPlan['medias'] = (object) $media;
    //     }

    //     $airports = Place::where('countries_id', '=', $country->id)
    //             ->where('place_type', 'like', 'airport,%')
    //             ->limit(10)
    //             ->get();

    //     $data['plans'] = $tripPlans;
    //     $data['plans_count'] = count($tripPlans);
    //     $data['sum_budget'] = $sumBudget;

    //     $data['country'] = $country;
    //     $data['airports'] = $airports;

    //     $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

    //     return view('site.country2.caution', $data);
    // }

    // public function postFollow($countryId) {
    //     $userId = Auth::Id();
    //     $country = Countries::find($countryId);
    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $follower = CountriesFollowers::where('countries_id', $countryId)
    //             ->where('users_id', $userId)
    //             ->first();

    //     if ($follower) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'This user is already following that country',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $country->followers()->create([
    //         'users_id' => $userId
    //     ]);

    //     //$this->updateStatistic($country, 'followers', count($country->followers));

    //     log_user_activity('Country', 'follow', $countryId);

    //     return [
    //         'success' => true
    //     ];
    // }

    // public function postUnFollow($countryId) {
    //     $userId = Auth::Id();

    //     $country = Countries::find($countryId);
    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $follower = CountriesFollowers::where('countries_id', $countryId)
    //             ->where('users_id', $userId);

    //     if (!$follower->first()) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'You are not following this country.',
    //             ],
    //             'success' => false
    //         ];
    //     } else {
    //         $follower->delete();
    //         //$this->updateStatistic($country, 'followers', count($country->followers));

    //         log_user_activity('Country', 'unfollow', $countryId);
    //     }

    //     return [
    //         'success' => true
    //     ];
    // }

    // public function postCheckFollow($countryId) {
    //     $userId = Auth::Id();
    //     $country = Countries::find($countryId);
    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $follower = CountriesFollowers::where('countries_id', $countryId)
    //             ->where('users_id', $userId)
    //             ->first();

    //     return empty($follower) ? ['success' => false] : ['success' => true];
    // }

    // public function postVisaRequirements($countryId) {
    //     $userId = Auth::Id();
    //     $country = Countries::find($countryId);
    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $my_country = 'SA';
    //     $target_country = $country->iso_code;

    //     $curl = curl_init();
    //     curl_setopt_array($curl, array(
    //         CURLOPT_RETURNTRANSFER => 1,
    //         CURLOPT_URL => 'https://api.joinsherpa.com/v2/visa-requirements/' . $my_country . '-' . $target_country,
    //         CURLOPT_HTTPHEADER => array(
    //             'Content-Type:application/json',
    //             'Authorization: Basic ' . base64_encode("6801f898-5190-4177-ba1c-661e39596370:d41d8cd98f00b204e9800998ecf8427e")
    //         )
    //     ));
    //     $resp = curl_exec($curl);
    //     curl_close($curl);


    //     dd($resp);

    //     //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    // }

    // public function postWeather($countryId) {
    //     $country = Countries::find($countryId);
    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $latlng = $country->lat . ',' . $country->lng;

    //     $get_place_key = curl_init();
    //     curl_setopt_array($get_place_key, array(
    //         CURLOPT_RETURNTRANSFER => 1,
    //         CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q=41.87194000%2C12.56738000',
    //     ));
    //     $resp = curl_exec($get_place_key);
    //     curl_close($get_place_key);
    //     if ($resp) {
    //         $resp = json_decode($resp);
    //         $place_key = $resp->Key;

    //         $get_country_weather = curl_init();
    //         curl_setopt_array($get_country_weather, array(
    //             CURLOPT_RETURNTRANSFER => 1,
    //             CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/'.$place_key.'?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9',
    //         ));
    //         $country_weather = curl_exec($get_country_weather);
    //         curl_close($get_country_weather);
    //         $country_weather = json_decode($country_weather);
    //         dd($country_weather);
    //     }



    //     dd($place_key);

    //     //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    // }

    // public function postTalkingAbout($countryId) {
    //     $country = Countries::find($countryId);
    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $shares = $country->shares;

    //     foreach($shares AS $share) {
    //         $data['shares'][] = array(
    //             'user_id' => $share->user->id,
    //             'user_profile_picture'=> check_profile_picture($share->user->profile_picture)
    //             );
    //     }

    //     $data['num_shares'] = count($shares);
    //     $data['success'] = true;
    //     return json_encode($data);

    //     //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    // }

    // public function postNowInCountry($countryId) {
    //     $country = Countries::find($countryId);
    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }



    //     $checkins = Checkins::where('country_id', $country->id)
    //             ->orderBy('id', 'DESC')
    //             ->take(8)
    //             ->get();

    //     $result = array();
    //     $done = array();
    //     foreach ($checkins AS $checkin) {
    //         if (!isset($done[$checkin->post_checkin->post->author->id])) {
    //             if(time()-strtotime($checkin->post_checkin->post->date) < (24*60*60)) {
    //                 $live = 1;
    //             }
    //             else {
    //                 $live = 0;
    //             }
    //             $result[] = array(
    //                 'name' => $checkin->post_checkin->post->author->name,
    //                 'id' => $checkin->post_checkin->post->author->id,
    //                 'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
    //                 'live' => $live
    //             );
    //             $done[$checkin->post_checkin->post->author->id] = true;
    //         }
    //     }
    //     $data['live_checkins'] = $result;

    //     $data['success'] = true;
    //     return json_encode($data);
    // }

    // public function postContribute($countryId) {
    //     $country = Countries::find($countryId);
    //     if (!$country) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid Country ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     if(Input::has('contents')) {
    //         $contents = Input::get('contents');
    //         $country_id = Input::get('country_id');
    //         $type = Input::get('type');
    //         $user_id = 0;
    //         if(Auth::user()) {
    //             $user_id = Auth::user()->id;
    //         }
    //         $create =  new CountriesContributions;
    //         $create->countries_id = $country_id;
    //         $create->users_id = $user_id;
    //         $create->type = $type;
    //         $create->contents = $contents;
    //         $create->status = 0;
    //         if($create->save()) {
    //             log_user_activity('Country', 'contribute', $country_id);
    //             $data['success'] = true;
    //         }
    //         else {
    //             $data['success'] = false;
    //         }

    //     }
    //     else {
    //         $data['success'] = false;

    //     }
    //     return json_encode($data);

    // }

}
