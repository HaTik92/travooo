<?php

namespace App\Http\Controllers\Api;

use App\Services\Newsfeed\HomeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Models\ActivityLog\ActivityLog;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\HomeController as FrontHomeController;
use App\LoadedFeed;
use App\Models\ActivityMedia\Media;
use App\Models\City\Cities;
use App\Models\City\CitiesFollowers;
use App\Models\Country\Countries;
use App\Models\Country\CountriesFollowers;
use App\Models\Discussion\Discussion;
use App\Models\Events\Events;
use App\Models\Events\EventsCommentsLikes;
use App\Models\Events\EventsCommentsMedias;
use App\Models\Hotels\Hotels;
use App\Models\Place\Medias;
use App\Models\Place\Place;
use App\Models\Place\PlaceFollowers;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Posts\Checkins;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsCities;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCountries;
use App\Models\Posts\PostsMedia;
use App\Models\Posts\PostsPlaces;
use App\Models\Posts\PostsShares;
use App\Models\Posts\PostsViews;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsCommentsLikes;
use App\Models\Reports\ReportsCommentsMedias;
use App\Models\Reviews\Reviews;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripContentPostLike;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsCommentsLikes;
use App\Models\TripPlans\TripsCommentsMedias;
use App\Models\TripPlans\TripsLikes;
use App\Models\TripPlans\TripsMediasShares;
use App\Models\TripPlans\TripsPlacesShares;
use App\Models\TripPlans\TripsShares;
use App\Models\User\User;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Api\LoadedFeedService;
use App\Services\Api\PrimaryPostService;
use App\Services\Trips\TripsSuggestionsService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use stdClass;
use App\Services\Api\UserProfileService;
use App\Services\Api\ShareService;

class NewsfeedPageController extends Controller
{
    function __construct()
    {
        if (in_array(request()->route()->uri, optionalAuthRoutes(self::class))) {
            if (request()->bearerToken()) {
                $this->middleware('auth:api');
            }
        }
    }

    /**
     * @OA\GET(
     ** path="/newsfeed/{page_id}",
     *   tags={"Newsfeed"},
     *   summary="This Api used to fetch newsfeed page without authentication.",
     *   operationId="Api\NewsfeedPageController@index",
     *   @OA\Parameter(
     *        name="page_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function index(Request $request, $page_id)
    {
        // testing
        try {
            $helper = app(HomeService::class);
            $params = $this->decodePage($page_id);
            if ($params) {
                $islog = isset($params[3]) ? $params[3] : 1;
                list($username, $type, $newsfeed_id) = $params;
                $response = [];
                switch (strtolower($type)) {
                    case 'trip':
                        $log = null;
                        if ($islog) {
                            $log       = ActivityLog::find($newsfeed_id);
                            $newsfeed_id = $log->variable;
                        }
                        $trip = $this->__preparedRegularTrip($newsfeed_id, (($log) ? $log->time : null));
                        if ($trip) {
                            $response = [
                                'page_id'   => $helper->link("newsfeed/" . $trip->author->username . "/trip/" . $newsfeed_id . "/0"),
                                'unique_link' => url("newsfeed/" . $trip->author->username . "/trip/" . $newsfeed_id . "/0"),
                                'type'      => strtolower(PrimaryPostService::TYPE_TRIP),
                                'action'    => ($log) ? strtolower($log->action) : 'create',
                                'data'      => $trip
                            ];
                        }
                        break;

                    case 'post':
                    case 'external':
                        $post = $helper->__preparedPost($newsfeed_id);
                        if ($post) {
                            $response = [
                                'page_id'   => $helper->link("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                'unique_link' => url("newsfeed/" . $post->author->username . "/post/" . $post->id),
                                'type'      => strtolower(PrimaryPostService::TYPE_POST),
                                'action'    => 'publish',
                                'data'      => $post
                            ];
                        }
                        break;

                    case 'review':
                        $review = $helper->__preparedPlaceReview($newsfeed_id);
                        if ($review) {
                            $response = [
                                'page_id'   => $helper->link("newsfeed/" . $review->author->username . "/review/" . $review->id),
                                'unique_link' => url("newsfeed/" . $review->author->username . "/review/" . $review->id),
                                'type'      => strtolower(PrimaryPostService::TYPE_REVIEW),
                                'action'    => 'place',
                                'data'      => $review
                            ];
                        }
                        break;
                    case 'share':
                        $log = ActivityLog::find($newsfeed_id);
                        if ($log && strtolower($log->type) == "share") {
                            if (in_array(strtolower($log->action), [
                                ShareService::TYPE_REVIEW,
                                ShareService::TYPE_EVENT,
                                ShareService::TYPE_DISCUSSION,
                                ShareService::TYPE_POST,
                                ShareService::TYPE_REPORT,
                                ShareService::TYPE_COUNTRY,
                                ShareService::TYPE_CITY,
                                ShareService::TYPE_PLACE,
                            ])) {
                                $feed = $helper->__preparedShareFeed($log);
                                if ($feed && is_array($feed)) {
                                    $response = $feed;
                                }
                            } else if (in_array(strtolower($log->action), [
                                ShareService::TYPE_TRIP_SHARE,
                                ShareService::TYPE_TRIP_PLACE_SHARE,
                                ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE
                            ])) {
                                $feed = $helper->__preparedShareTripAndPlaceAdnMediaFeed($log);
                                if ($feed && is_array($feed)) {
                                    $response = $feed;
                                }
                            }
                        }
                        break;

                    case 'discussion':
                        $discussion = $helper->__preparedDiscussion($newsfeed_id);
                        if ($discussion) {
                            $response =  [
                                'page_id'   => $helper->link("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                'unique_link' => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                                'type'      => strtolower(PrimaryPostService::TYPE_DISCUSSION),
                                'action'    => 'create',
                                'data'      => $discussion
                            ];
                        }
                        break;

                    case "event":
                        $event = $helper->__preparedEvent($newsfeed_id);
                        if ($event) {
                            $response =  [
                                'page_id'   => $helper->link("newsfeed/event/event/" . $event->id),
                                'unique_link' => url("newsfeed/event/event/" . $event->id),
                                'type'      => strtolower(PrimaryPostService::TYPE_EVENT),
                                'action'    => 'show',
                                'data'      => $event
                            ];
                        }
                        break;

                    default:
                        return ApiResponse::__createNotFound("Invalid page");
                        break;
                }
                if ($response) {
                    return ApiResponse::create($response);
                } else {
                    return ApiResponse::__createNotFound("Invalid page");
                }
                // return ApiResponse::create([$username, $type, $newsfeed_id]);
            } else {
                return ApiResponse::__createNotFound("Invalid page");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    public function decodePage($page)
    {
        $decode = @base64_decode($page);
        if (is_string($decode) && strlen($decode)) {
            $paramsString = str_replace(url('/newsfeed') . "/", "", $decode);
            $paramsArr = explode("/", $paramsString);
            if (count($paramsArr) >= 3) {
                return $paramsArr;
            }
        }
        return null;
    }

    function  __preparedRegularTrip($trip_id, $updated_at = null)
    {
        $authUser = auth()->user();
        $trip_plan = TripPlans::find($trip_id);
        if ($trip_plan) {
            $contributer = $trip_plan->contribution_requests->where('status', 1)->pluck('users_id')->toArray();
            if (true) {
                if ($updated_at) {
                    $trip_plan->updated_at = $updated_at;
                } else {
                    $lastUpdatedLog = ActivityLog::query()->where([
                        'type' => 'Trip',
                        'variable' => $trip_plan->id
                    ])->whereIn('action', ['create', 'update'])->orderBy('time', 'DESC')->first();
                    $trip_plan->updated_at = (!$lastUpdatedLog) ?: $lastUpdatedLog->time;
                }
                if (!$trip_plan->author) {
                    return null;
                }

                // $author = $authUser;
                // if ($author->id != $trip_plan->author->id) {
                //     if (in_array($author->id, $contributer)) {
                //         $author = $trip_plan->author;
                //     } else {
                //         $following = \App\Models\UsersFollowers\UsersFollowers::where('followers_id', $author->id)->pluck('users_id')->toArray();
                //         $ans =  array_values(array_intersect($following, $contributer));
                //         if (count($ans) > 0) {
                //             $user_find = \App\Models\User\User::find($ans[0]);
                //             if (isset($user_find)) {
                //                 $author = $user_find;
                //             }
                //         } else {
                //             $author = $trip_plan->author;
                //         }
                //     }
                // }
                // unset($trip_plan->author);
                // $trip_plan->author = $author;

                if ($trip_plan->author) {
                    $trip_plan->author->profile_picture = check_profile_picture($trip_plan->author->profile_picture);
                }
                if ($trip_plan->published_places) {
                    $places = [];
                    $countries = [];
                    $cities = [];
                    $total_destination = 0;
                    $_totalDestination = ['city_wise' => [], 'country_wise' => []];

                    foreach ($trip_plan->published_places as $tripPlace) {
                        if (!$tripPlace->place or !$tripPlace->country or !$tripPlace->city) {
                            continue;
                        }
                        if (!isset($places[$tripPlace->places_id])) {
                            $total_destination++;
                            $_totalDestination['city_wise'][$tripPlace->cities_id][$tripPlace->places_id] = $tripPlace->place;
                            $_totalDestination['country_wise'][$tripPlace->countries_id][$tripPlace->places_id] = $tripPlace->place;
                            $places[$tripPlace->places_id] = [
                                'id' => $tripPlace->places_id,
                                'country_id' => $tripPlace->countries_id,
                                'city_id' => $tripPlace->cities_id,
                                'title' => isset($tripPlace->place->transsingle->title) ? $tripPlace->place->transsingle->title : null,
                                'lat' => $tripPlace->place->lat,
                                'lng' => $tripPlace->place->lng,
                                'image' => check_place_photo($tripPlace->place),
                            ];
                        }
                        if (!isset($countries[$tripPlace->countries_id])) {
                            $countries[$tripPlace->countries_id] = [
                                'id' => $tripPlace->countries_id,
                                'title' => isset($tripPlace->country->trans[0]->title) ? $tripPlace->country->trans[0]->title : null,
                                'lat' => $tripPlace->country->lat,
                                'lng' => $tripPlace->country->lng,
                                'image' => get_country_flag($tripPlace->country),
                                'total_destination' => 0,
                            ];
                        }
                        if (!isset($cities[$tripPlace->cities_id])) {
                            $cities[$tripPlace->cities_id] = [
                                'id' => $tripPlace->cities_id,
                                'country_id' => $tripPlace->countries_id,
                                'title' => isset($tripPlace->city->trans[0]->title) ? $tripPlace->city->trans[0]->title : null,
                                'lat' => $tripPlace->city->lat,
                                'lng' => $tripPlace->city->lng,
                                'image' => isset($tripPlace->city->medias[0]->media->path) ?
                                    check_city_photo($tripPlace->city->medias[0]->media->path) :
                                    url('assets2/image/placeholders/pattern.png'),
                                'total_destination' => 0,
                            ];
                        }
                    }

                    foreach ($countries as $country_id => &$country) {
                        $country['total_destination'] = isset($_totalDestination['country_wise'][$country_id]) ? count($_totalDestination['country_wise'][$country_id]) : 0;
                    }
                    foreach ($cities as $city_id =>  &$city) {
                        $city['total_destination'] = isset($_totalDestination['city_wise'][$city_id]) ? count($_totalDestination['city_wise'][$city_id]) : 0;
                    }
                    $country_title =  isset(array_values($countries)[0]['title']) ? array_values($countries)[0]['title'] : null;
                    $trip_plan->total_destination = $total_destination;
                    if (count($countries) > 1) {
                        $trip_plan->type = "multiple-country";
                        $trip_plan->data = array_values($countries);
                    } else {
                        if (count($cities) > 1) {
                            $trip_plan->type = "multiple-city";
                            $trip_plan->country_title = $country_title;
                            $trip_plan->data = array_values($cities);
                        } else {
                            $trip_plan->type = "single-city";
                            $trip_plan->country_title = $country_title;
                            $trip_plan->data = array_values($places);
                        }
                    }

                    $trip_plan->like_flag   = false;
                    $trip_plan->total_likes = $trip_plan->likes->count();
                    $trip_plan->total_shares = TripsShares::where('trip_id', $trip_plan->id)->count();

                    // For Comment
                    $post_comments = [];
                    $trip_plan->total_comments  = $trip_plan->postComments->count();
                    $trip_plan->comment_flag = $authUser ? ($trip_plan->postComments->where('users_id', $authUser->id)->count() ? true : false) : false;
                    foreach ($trip_plan->postComments->take(2) as $parent_comment) {
                        // For Parent Comment
                        $parent_comment->like_status = false;
                        $parent_comment->total_likes = $authUser ? (TripsCommentsLikes::where('trips_comments_id', $parent_comment->id)->where('users_id', $authUser->id)->first() ? true : false) : false;
                        $medias = TripsCommentsMedias::where('trips_comments_id', $parent_comment->id)->get();
                        if ($medias) {
                            foreach ($medias as $media) {
                                $media->media;
                            }
                        }
                        $parent_comment->medias = $medias;
                        if ($parent_comment->author) {
                            $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                        }
                        $parent_comment->comment_flag = $authUser ? ($parent_comment->sub->where('users_id', $authUser->id)->count() ? true : false) : false;
                        $parent_comment->text = convert_post_text_to_tags($parent_comment->text, $parent_comment->tags, false);
                        unset($parent_comment->tags);

                        // For Sub Comment
                        if ($parent_comment->sub) {
                            foreach ($parent_comment->sub as $sub_comment) {
                                $sub_comment->like_status = $authUser ? (TripsCommentsLikes::where('trips_comments_id', $sub_comment->id)->where('users_id', $authUser->id)->first() ? true : false) : false;
                                $sub_comment->total_likes = TripsCommentsLikes::where('trips_comments_id', $sub_comment->id)->count();
                                $medias = TripsCommentsMedias::where('trips_comments_id', $sub_comment->id)->get();
                                if ($medias) {
                                    foreach ($medias as $media) {
                                        $media->media;
                                    }
                                }
                                $sub_comment->medias = $medias;
                                if ($sub_comment->author) {
                                    $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                                }
                                $sub_comment->text = convert_post_text_to_tags($sub_comment->text, $sub_comment->tags, false);
                                unset($sub_comment->tags);
                            }
                        }

                        $post_comments[] = $parent_comment;
                    }

                    unset($trip_plan->postComments, $trip_plan->published_places, $trip_plan->contribution_requests, $trip_plan->likes);
                    $trip_plan->post_comments = $post_comments;
                    // $trip_plan->_totalDestination = $_totalDestination;
                    // $trip_plan->_places = $places;
                    // $trip_plan->_countries = $countries;
                    // $trip_plan->_cities = $cities;
                    return $trip_plan;
                }
            }
        }
        return null;
    }
}
