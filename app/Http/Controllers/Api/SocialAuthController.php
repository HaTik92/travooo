<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User\User;
use App\SocialLogin;

class SocialAuthController extends Controller
{
    /**
     * List of providers configured in config/services acts as whitelist
     *
     * @var array
     */
    protected $providers = [
        'facebook',
        'twitter',
        'google',
        'apple'
    ];

    protected $loginTypes = [
        'facebook' => User::FACEBOOK,
        'twitter'  => User::TWITTER,
        'google'  => User::GOOGLE,
        'apple'  => User::APPLE
    ];

    protected $providerTypes = [
        'facebook' => 'FB',
        'twitter'  => 'TW',
        'google'  => 'GO',
        'apple'  => 'AP'
    ];

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Redirect to provider for authentication
     *
     * @param $driver
     * @return mixed
     */
    public function redirectToProvider($driver)
    {
        if (! $this->isProviderAllowed($driver)) {
            return $this->sendFailedResponse("{$driver} is not currently supported");
        }

        try {
            return Socialite::driver($driver)->stateless()->redirect();
        } catch (\Throwable $e) {
            // You should show something simple fail message
            return $this->sendFailedResponse($e->getMessage());
        }
    }

    /**
     * Handle response of authentication redirect callback
     *
     * @param $driver
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback($driver)
    {
        try {
            $user = Socialite::driver($driver)->stateless()->user();
        } catch (Exception $e) {
            return $this->sendFailedResponse($e->getMessage());
        }

        // check for email in returned user
        return empty($user->email)
            ? $this->sendFailedResponse("No email id returned from {$driver} provider.")
            : $this->loginOrCreateAccount($user, $driver);
    }

    /**
     * Send a successful response
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendSuccessResponse(User $user)
    {
        $tokenResult = $user->createToken('TRAVOOO');
        $result = [
                'user' => $user->getArrayResponse(),
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ];
        return ApiResponse::create($result);
    }

    /**
     * Send a failed response with a msg
     *
     * @param null $msg
     * @return Response
     */
    protected function sendFailedResponse($msg = null)
    {
        return ApiResponse::create(
            ['msg' => $msg ?: 'Unable to login, try with another provider to login.'],
            false
        );
    }

    protected function loginOrCreateAccount($providerUser, $driver)
    {
        // check for already has account
        $user = User::updateOrCreate(
            ['email' => $providerUser->getEmail()],
            [
                'name'       => $providerUser->getName(),
                'password'   => Hash::make(rand(1, 10000)),
                'login_type' => $this->loginTypes[$driver],
                'social_key' => $providerUser->id
            ]
        );

        SocialLogin::updateOrCreate(
            ['provider_id' => $providerUser->id],
            [
                'user_id'     => $user->id,
                'provider_id' => $providerUser->id,
                'provider'    => $this->providerTypes[$driver],
                'token'       => $providerUser->token,
                'avatar'      => $providerUser->avatar,
            ]
        );

        // login the user
        Auth::login($user, true);
        $tokenResult = $user->createToken('TRAVOOO');
        return $this->sendSuccessResponse($user);
    }

    /**
     * Check for provider allowed and services configured
     *
     * @param $driver
     * @return bool
     */
    private function isProviderAllowed($driver)
    {
        return in_array($driver, $this->providers) && config()->has("services.{$driver}");
    }
}
