<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Suggestions\AllSuggestPlaceRequest;
use App\Http\Requests\Api\Suggestions\SuggestRequest;
use App\Http\Requests\Api\Trip\UploadMediaRequest;
use App\Http\Responses\AjaxResponse;
use App\Http\Responses\ApiResponse;
use App\Models\ActivityMedia\Media;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsSuggestion;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use App\Http\Controllers\Api\TripController;
use Illuminate\Http\UploadedFile;

class SuggestionsController extends Controller
{

    /**
     * @OA\Post(
     ** path="/trip/{trip_id}/places/{trip_place_id}/suggestions",
     *  tags={"Trip Suggestion"},
     *  summary="create or edit trip place suggestion (CREATE_TRIP_PLACE_SUGGESTION_URL)",
     *  operationId="Api\SuggestionsController@suggest",
     *  security={
     *       {"bearer_token": {}
     *    }},
     *  @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="trip_place_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\RequestBody(
     *        @OA\MediaType(
     *        mediaType="application/json",
     *        @OA\Schema(
     *            @OA\Property(
     *                property="reason",
     *                type="string"
     *            ),
     *            @OA\Property(
     *                property="values",
     *                type="string"
     *            ),
     *            example={
     * { "reason": "this is time suggestion", "values": { "date": { "date": "2021-01-15", "time": "12:30am" } } },
     * { "reason": "this is duration suggestion", "values": { "duration": { "days": "5", "hours": "1", "minutes": "30" } } },
     * { "reason": "this is budget suggestion", "values": { "budget":5 } },
     * { "reason": "this is story suggestion", "values": { "story": "hello" } },
     * { "reason": "this is duration suggestion", "values": { "tags": "Place,Known,For,Suggestion" } },
     * { "reason": "this is media suggestion", "values": { "media": { "0": "base_64", "1": "base_64" } } },
     * },
     *        )
     *     )
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param SuggestRequest $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param TripInvitationsService $invitationsService
     * @param int $trip_id
     * @param int $place_id
     */
    public function suggest(
        SuggestRequest $request,
        TripsSuggestionsService $tripsSuggestionsService,
        TripInvitationsService $invitationsService,
        $trip_id,
        $trip_place_id
    ) {
        try {
            $planId             = $trip_id;
            $placeId            = $trip_place_id;
            $userId             = auth()->id();
            $suggestionEditType = null;
            $reason             = $request->reason;
            $keyAndValues       = $request->values;
            $values             = [];

            $trip = TripPlans::find($trip_id);
            if (!$trip) return ApiResponse::__createBadResponse("Trip is invalid");
            if (!TripPlaces::find($trip_place_id)) return ApiResponse::__createBadResponse("Place is invalid.");

            // Start Re-format api request 
            $keyAndValues =  (is_array($keyAndValues)) ? $request->values : ((array) json_decode($keyAndValues));
            if (is_array(array_keys($keyAndValues)) && count(array_keys($keyAndValues)) == 1) {
                $suggestionEditType = array_keys($keyAndValues)[0];
                if (in_array($suggestionEditType, TripsSuggestionsService::SUGGESTION_EDIT_TYPES)) {
                    switch (strtolower($suggestionEditType)) {
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_BUDGET:
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_STORY:
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_TAGS:
                            $values[$suggestionEditType] = $keyAndValues[$suggestionEditType];
                            break;
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_DATE:
                            $values['date'] = $keyAndValues[$suggestionEditType]['date'];
                            $values['time'] = $keyAndValues[$suggestionEditType]['time'];
                            break;
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_DURATION:
                            $values['duration_days'] = $keyAndValues[$suggestionEditType]['days'] ?? '';
                            $values['duration_hours'] = $keyAndValues[$suggestionEditType]['hours'] ?? '';
                            $values['duration_minutes'] = $keyAndValues[$suggestionEditType]['minutes'] ?? '';
                            break;
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_MEDIA:
                            $medias = $keyAndValues[$suggestionEditType];
                            if (count($medias) == 0) return ApiResponse::__createBadResponse('Media not found');
                            $values = ['medias' => $medias];
                            break;
                    }
                } else {
                    return ApiResponse::__createBadResponse(
                        'Suggestion edit type in list ' . (implode(", ", TripsSuggestionsService::SUGGESTION_EDIT_TYPES))
                    );
                }
            } else {
                return ApiResponse::__createBadResponse("Something went wrong in values.");
            }
            // End Re-format api request 

            $role = $invitationsService->getUserRole($planId, $userId);
            if (!$role || $role !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]) {
                return ApiResponse::__createUnAuthorizedResponse("Access denied");
            }

            $placeId = $tripsSuggestionsService->getRootTripPlace($placeId, true);
            $place   = TripPlaces::query()->where('id', $placeId)->withTrashed()->first();
            if (!$place) return ApiResponse::__createBadResponse("place not found");

            $oldTripsSuggestion =  TripsSuggestion::where('users_id', auth()->id())
                ->where('is_published', 0)
                ->where('trips_id', $planId)
                ->where('type', TripsSuggestionsService::SUGGESTION_EDIT_BY_EDITOR_TYPE)
                ->where('active_trips_places_id', $trip_place_id)
                ->where('status', TripsSuggestionsService::SUGGESTION_PENDING_STATUS)
                ->where('meta', 'like', '%"edit_type":"' . $suggestionEditType . '"%')->first();

            $resolver = $tripsSuggestionsService->getSuggestionEditResolver($suggestionEditType, $place, $values, $oldTripsSuggestion);
            $place = $resolver->resolvePlace();
            if ($place->save()) {
                if (!$oldTripsSuggestion) {
                    $tripsSuggestionsService->createSuggestion($place->id, $planId, TripsSuggestionsService::SUGGESTION_EDIT_BY_EDITOR_TYPE, $placeId, $reason, $suggestionEditType);
                }

                // timeline // need to hard reload whole timeline
                $timeline = app(TripController::class)->getPlanContentsApi(true, $trip_id, auth()->id(), $tripsSuggestionsService);
                $timeline['invited_people'] = app(TripController::class)->getFlatInvitedPeopleList($invitationsService, $trip_id);
                unset($timeline['is_edited'], $timeline['updated_places'], $timeline['unread_logs_count']);

                return ApiResponse::create([
                    "message" => ["Suggestion save successfully."],
                    "timeline" => $timeline
                ]);
            } else {
                return ApiResponse::__createServerError("Sonmething went wrong. please try again.");
            }
        } catch (\Throwable $e) {
            if (get_class($e) == ModelNotFoundException::class) {
                return ApiResponse::__createBadResponse("Trip not found");
            } else if (get_class($e) == BadRequestHttpException::class) {
                return ApiResponse::__createServerError("Sonmething went wrong. please try again.");
            } else if (get_class($e) == MaxRejectedTimesException::class) {
                return ApiResponse::__createBadResponse("Your suggestions were rejected 3 times");
            } else {
                return ApiResponse::createServerError($e);
            }
        }
    }

    /**
     * @OA\GET(
     **  path="/trip/{trip_id}/places/{trip_place_id}/suggestions",
     *   tags={"Trip Suggestion"},
     *   summary="fetch all suggestion of specific trip place",
     *   operationId="Api\SuggestionsController@getSuggestions",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="trip_place_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @param TripInvitationsService $invitationsService
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param int $trip_id
     * @param int $place_id
     */
    public function getSuggestions(
        Request $request,
        TripInvitationsService $invitationsService,
        TripsSuggestionsService $tripsSuggestionsService,
        $trip_id,
        $trip_place_id
    ) {
        try {
            $planId     = $trip_id;
            $placeId    = $trip_place_id;
            $userId     = auth()->id();

            $trip = TripPlans::find($trip_id);
            if (!$trip) return ApiResponse::__createBadResponse("Trip is invalid");
            if (!TripPlaces::find($placeId)) return ApiResponse::__createBadResponse("Place is invalid.");


            $role = $invitationsService->getUserRole($planId, $userId);
            if (!$role || ($role !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN] && $role !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR])) {
                return ApiResponse::__createUnAuthorizedResponse("Access denied");
            }

            $tripsSuggestions   = TripsSuggestionsService::SUGGESTION_EDIT_TYPES;
            $suggestions = $tripsSuggestionsService->getPlaceSuggestions($tripsSuggestionsService->getRootTripPlace($placeId, true), $role);

            $result = [];
            foreach ($tripsSuggestions as $type) {
                $result[$type] = [];
            }

            foreach ($suggestions as $suggestion) {
                $type = $suggestion->meta['edit_type'];

                $result[$type][] = [
                    'suggested_place' => $suggestion->suggestedPlace()->with(['medias', 'medias.media'])->first(),
                    'id' => $suggestion->id,
                    'reason' => $suggestion->reason,
                    'date' => diffForHumans($suggestion->created_at, 'UTC'),
                    'user' => [
                        'name' => $suggestion->user->name,
                        'img' => check_profile_picture($suggestion->user->profile_picture),
                        'role' => $role
                    ]
                ];
            }

            return ApiResponse::create([
                'suggestions' => $result
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    //TB-414
    /**
     * @OA\GET(
     ** path="/trip/{trip_id}/suggestions",
     *   tags={"Trip Suggestion"},
     *   summary="This Api used to fetch all suggestion for review",
     *   operationId="Api\SuggestionsController@getReviewList",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getReviewList(
        Request $request,
        TripInvitationsService $invitationsService,
        TripsSuggestionsService $tripsSuggestionsService,
        $trip_id
    ) {
        try {
            $placeWiseSuggestions = [];

            // get all exsting trips places suggestions 
            $activeTripsPlacesId = TripsSuggestion::where([
                'status' => TripsSuggestionsService::SUGGESTION_PENDING_STATUS,
                'trips_id' => $trip_id,
                'is_saved' => 0,
                'users_id' => auth()->id()
            ])->groupBy('active_trips_places_id')->pluck('active_trips_places_id')->toArray();

            // get only new suggested trips places
            $suggestedTripsPlacesId = TripsSuggestion::where([
                'status' => TripsSuggestionsService::SUGGESTION_PENDING_STATUS,
                'trips_id' => $trip_id,
                'is_saved' => 0,
                'users_id' => auth()->id()
            ])->whereNull('meta')->groupBy('suggested_trips_places_id')->pluck('suggested_trips_places_id')->toArray();

            if (count($activeTripsPlacesId) > 0 || count($suggestedTripsPlacesId) > 0) {
                $placeWiseSuggestions = TripPlaces::whereIn('id', array_merge($activeTripsPlacesId, $suggestedTripsPlacesId))->get();
                $total_places = 0;
                foreach ($placeWiseSuggestions as $tripPlace) {
                    $tripPlace->place;
                    $to_place                   = $tripPlace->place;
                    $tripPlace->title           = isset($to_place->transsingle->title) ? $to_place->transsingle->title : null;
                    $tripPlace->description     = isset($to_place->transsingle->description) ? $to_place->transsingle->description : null;
                    $tripPlace->image           = isset($to_place->medias[0]->media->path) ? $to_place->medias[0]->media->path : url('assets2/image/placeholders/pattern.png');

                    $tripPlace->is_new_suggested = in_array($tripPlace->id, $suggestedTripsPlacesId);

                    $tripPlace->suggestions = TripsSuggestion::where([
                        'status' => TripsSuggestionsService::SUGGESTION_PENDING_STATUS,
                        'trips_id' => $trip_id,
                        'is_saved' => 0,
                        'users_id' => auth()->id(),
                        'active_trips_places_id' => $tripPlace->id
                    ])->get();
                    $tripPlace->total_suggestions = count($tripPlace->suggestions);

                    foreach ($tripPlace->suggestions as $suggestions) {
                        $suggestions->suggestedPlace;
                        $total_places++;
                    }
                    unset($tripPlace->place);
                }
                $res = [
                    'information' => [
                        'total_places' => count($placeWiseSuggestions),
                        'total_suggestions' =>  $total_places,
                    ],
                    'data' => $placeWiseSuggestions,
                ];
                return ApiResponse::create($res);
            } else {
                return ApiResponse::create([]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    //TB-414
    /**
     * @OA\POST(
     ** path="/trip/{trip_id}/places/{place_id}/review",
     *   tags={"Trip Suggestion"},
     *   summary="This Api used to post all review(create/edit)",
     *   operationId="Api\SuggestionsController@postReview",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="place_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     * @OA\RequestBody(
     *        @OA\MediaType(
     *        mediaType="application/json",
     *        @OA\Schema(
     *            @OA\Property(
     *                property="values",
     *                type="string"
     *            ),
     *            example={
     * "date": { "date": "2021-01-15", "time": "12:30am" } ,
     * "duration": { "days": "5", "hours": "1", "minutes": "30" } ,
     * "budget":5,
     * "story": "hello",
     * "tags": "Place,Known,For,Suggestion",
     * "media": { "0": "base_64", "1": "base_64" }
     * },
     *        )
     *     )
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function postReview(
        Request $request,
        TripInvitationsService $invitationsService,
        TripsSuggestionsService $tripsSuggestionsService,
        $trip_id,
        $place_id
    ) {
        try {
            $userId             = auth()->id();
            $values             = $request->values;
            $finalValue         = [];
            if (is_array($values)) {
                $trip = TripPlans::find($trip_id);
                if (!$trip) return ApiResponse::__createBadResponse("Trip is invalid");
                if (!TripPlaces::find($place_id)) return ApiResponse::__createBadResponse("Place is invalid.");

                foreach (array_keys($values) as $key) {
                    if (!in_array($key, TripsSuggestionsService::SUGGESTION_EDIT_TYPES)) {
                        return ApiResponse::__createBadResponse(
                            $key . ' invalid suggestion, Suggestion edit type in list ' . (implode(", ", TripsSuggestionsService::SUGGESTION_EDIT_TYPES))
                        );
                    }
                }
                $error = null;
                foreach ($values as $suggestionKey => $suggestionValues) {
                    switch (strtolower($suggestionKey)) {
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_BUDGET:
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_STORY:
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_TAGS:
                            if (is_array($suggestionValues)) {
                                $error = ApiResponse::__createBadResponse("invalid value in " . strtolower($suggestionKey) . " suggestion");
                            }
                            $finalValue[strtolower($suggestionKey)] = [strtolower($suggestionKey) => $suggestionValues];
                            break;
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_DATE:
                            if (!isset($suggestionValues['date'])) {
                                $error = ApiResponse::__createBadResponse("date must be required in date suggestion");
                            }
                            if (!isset($suggestionValues['time'])) {
                                $error = ApiResponse::__createBadResponse("time must be required in date suggestion");
                            }
                            $finalValue[strtolower($suggestionKey)]['date'] = $suggestionValues['date'];
                            $finalValue[strtolower($suggestionKey)]['time'] = $suggestionValues['time'];
                            break;
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_DURATION:
                            if (!isset($suggestionValues['days'])) {
                                $error = ApiResponse::__createBadResponse("days must be required in duration suggestion");
                            }
                            if (!isset($suggestionValues['hours'])) {
                                $error = ApiResponse::__createBadResponse("hours must be required in duration suggestion");
                            }
                            if (!isset($suggestionValues['minutes'])) {
                                $error = ApiResponse::__createBadResponse("minutes must be required in duration suggestion");
                            }
                            $finalValue[strtolower($suggestionKey)]['duration_days'] = $suggestionValues['days'] ?? '';
                            $finalValue[strtolower($suggestionKey)]['duration_hours'] = $suggestionValues['hours'] ?? '';
                            $finalValue[strtolower($suggestionKey)]['duration_minutes'] = $suggestionValues['minutes'] ?? '';
                            break;
                        case TripsSuggestionsService::SUGGESTION_EDIT_TYPE_MEDIA:
                            $medias = $suggestionValues;
                            if (!is_array($medias)) {
                                $error = ApiResponse::__createBadResponse('Array must be required in media suggestion');
                            }
                            if (count($medias) == 0) return ApiResponse::__createBadResponse('Media not found');
                            $finalValue['media'] = ['medias' => $medias];
                            break;
                    }
                }
                if ($error != null) return $error;
                // Start 
                $role = $invitationsService->getUserRole($trip_id, $userId);
                if (!$role || $role !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]) {
                    return ApiResponse::__createUnAuthorizedResponse("Access denied");
                }

                $placeId = $tripsSuggestionsService->getRootTripPlace($place_id, true);
                $place   = TripPlaces::query()->where('id', $placeId)->withTrashed()->first();
                if (!$place) return ApiResponse::__createBadResponse("place not found");

                $returnResult = [];
                foreach ($finalValue as $suggestionEditType => $suggestionEditValue) {
                    $oldTripsSuggestion =  TripsSuggestion::where('users_id', auth()->id())
                        ->where('is_published', 0)
                        ->where('trips_id', $trip_id)
                        ->where('type', TripsSuggestionsService::SUGGESTION_EDIT_BY_EDITOR_TYPE)
                        ->where('active_trips_places_id', $place_id)
                        ->where('status', TripsSuggestionsService::SUGGESTION_PENDING_STATUS)
                        ->where('meta', 'like', '%"edit_type":"' . $suggestionEditType . '"%')->first();

                    $resolver = $tripsSuggestionsService->getSuggestionEditResolver($suggestionEditType, $place, $suggestionEditValue, $oldTripsSuggestion);
                    $place = $resolver->resolvePlace();
                    if ($place->save()) {
                        if (!$oldTripsSuggestion) {
                            $tripsSuggestionsService->createSuggestion($place->id, $trip_id, TripsSuggestionsService::SUGGESTION_EDIT_BY_EDITOR_TYPE, $placeId, " ", $suggestionEditType);
                        }
                        $returnResult[] = $suggestionEditType . " Suggestion save successfully.";
                    } else {
                        $returnResult[] = "Sonmething went wrong in " . $suggestionEditType . " suggestion";
                    }
                }
                return ApiResponse::create($returnResult);
                // End
            } else {
                return ApiResponse::__createBadResponse("values must be array.");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    // Helper
    function uploap($trip_id, $media_file)
    {
        try {
            if ($media_file) {
                $filename = time() . md5($media_file->getClientOriginalName()) . '.' . $media_file->getClientOriginalExtension();

                $type = Media::TYPE_IMAGE;

                Storage::disk('s3')->putFileAs('trips/' . $trip_id . '/', $media_file, $filename, 'public');
                $media_file_url = 'https://s3.amazonaws.com/travooo-images2/trips/' . $trip_id . '/' . $filename;

                if (strpos($media_file->getMimeType(), 'video/') !== false) {
                    $type = Media::TYPE_VIDEO;
                    $ffmpeg = \FFMpeg\FFMpeg::create();

                    $video = $ffmpeg->open($media_file->getRealPath());

                    $duration = @$video->getStreams()->videos()->first()->get('duration');

                    if ($duration) {
                        $frame = $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds($duration * 0.01));
                        $name = bin2hex(openssl_random_pseudo_bytes(10)) . '.png';
                        $path = storage_path() . '/' . $name;
                        $frame->save($path);

                        Storage::disk('s3')->putFileAs(
                            'trips/' . $trip_id . '/thumb/',
                            new UploadedFile($path, $name),
                            $name,
                            'public'
                        );

                        $thumb_file_url = 'https://s3.amazonaws.com/travooo-images2/trips/' . $trip_id . '/thumb/' . $name;

                        unlink($path);
                    }
                } else {
                    //$originalImg = Storage::disk('s3')->get('trips/' . $trip_id . '/' . $filename);

                    $cropped = \Intervention\Image\ImageManagerStatic::make($media_file)->orientate()->resize(180, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    Storage::disk('s3')->put(
                        'trips/' . $trip_id . '/th180/' . $filename,
                        $cropped->encode(),
                        'public'
                    );

                    $thumb_file_url = 'https://s3.amazonaws.com/travooo-images2/trips/' . $trip_id . '/th180/' . $filename;
                }

                $trip_media = new Media();
                $trip_media->url = $media_file_url;
                $trip_media->type = $type;
                $trip_media->source_url = $thumb_file_url;
                $trip_media->users_id = auth()->id();

                if ($type === Media::TYPE_VIDEO) {
                    $trip_media->video_thumbnail_url = $thumb_file_url;
                }


                $trip_media->save();

                return [
                    'status' => true,
                    'id' => $trip_media->getKey(),
                    'media_url' => $trip_media->url
                ];
            } else {
                return [
                    'status' => false,
                    'msg' => "Please Provide base64"
                ];
            }
        } catch (\Throwable $e) {
            return [
                'status' => false,
                'msg' => $e->getMessage()
            ];
        }
    }
}
