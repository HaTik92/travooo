<?php

namespace App\Http\Controllers\Api;

use App\Services\Newsfeed\HomeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Api\Post\CreatePostRequest;
use App\Http\Requests\Api\Post\DeletePostRequest;
use App\Http\Requests\Api\Post\SpamPostRequest;
use App\Http\Requests\Api\Post\CommentEditRequest;
use App\Http\Requests\Api\PostPrivacyRequest;
use App\Models\Posts\Checkins;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsCheckins;
use App\Models\Posts\PostsShares;
use App\Models\Posts\PostsMedia;
use App\Models\Posts\PostsLikes;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsTags;
use App\Models\Posts\SpamsPosts;
use App\Models\Posts\CheckinsLikes;
use App\Models\Posts\PostsCommentsMedias;
use App\Models\Posts\CheckinsComments;
use App\Models\Posts\CheckinsCommentsLikes;
use App\Models\ActivityMedia\Media;
use App\Models\Reviews\Reviews;
use App\Models\Reviews\ReviewsVotes;
use App\Models\User\UsersMedias;
use App\Services\Api\CommentService;
use App\Services\Api\PrimaryPostService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\CommonApiController;
use App\Models\City\Cities;
use App\Models\City\CitiesMedias;
use App\Models\Country\Countries;
use App\Models\Country\CountriesMedias;
use App\Models\Place\Place;
use App\Models\Place\PlaceMedias;
use App\Http\Constants\CommonConst;
use App\Services\Translations\TranslationService;
use App\Services\FFmpeg\FFmpegService;

// use app\Models\Posts\SpamsComments;

class PostController extends Controller
{
    /**
     * @OA\Post(
     ** path="/posts",
     *  tags={"POST Creation"},
     *  summary="location and checkin_date only for checkin post + pair for media upload. permission[default = 0 | 0 = public, 1= Friends Only, 2 = private]  & page and page_id only for country, city, place module",
     *  operationId="Api\PostController@createPost",
     *  security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\RequestBody(
     *        @OA\MediaType(
     *        mediaType="application/json",
     *        @OA\Schema(
     *            example={
     *      "page": "city|country|place",
     *      "page_id": "city_id|country_id|place_id",
     *      "pair": "878787", 
     *      "text": "I am going to {{city_1256}}. I will create trip for {{user_1942}}. We will go to {{place_4974339}}", 
     *      "permission": 0,
     *      "location": {
     *          "type":"place",
     *          "id":4715795,
     *          "title":"India"
     *      },
     *      "checkin_date" : "FEB 15 21 09:50 AM",
     *      "lat": "",
     *      "lng": "",
     *      "tags": {
     *              { "type": "city", "id": "1256"  },
     *              { "type": "user", "id": "1942"  },
     *              { "type": "place", "id": "4974339"  }
     *      }
     * },
     *        )
     *     )
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param CreatePostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function createPost(CreatePostRequest $request, FFmpegService $ffmpegService)
    {
        try {
            $authUser       = Auth::user();
            $pair = $request->input("pair", "");
            $fileLists      = app(CommonApiController::class)->getTempFiles($pair);

            $isFiles        = count($fileLists) > 0 ? true : false;
            // dd($request->all(), $fileLists,  $isFiles);
            $postText       = processString($request->input("text", ""));
            $permission     = $request->permission ?? 0;
            $lat            = $request->lat ?? null;
            $lng            = $request->lng ?? null;
            $tags           = $request->input("tags", []);

            if (!in_array($permission, [0, 1, 2])) {
                return ApiResponse::createValidationResponse([
                    'permission' => 'permission must be in list of 0, 1, 2'
                ]);
            }
            // Create a new Post
            DB::beginTransaction();

            $post                   = new Posts();
            if (!is_null($postText)) {
                $translationService = new TranslationService();
                $post->language_id = $translationService->getLanguageId($postText);
            }
            $post->users_id         = $authUser->id;
            $post->text             = $postText;
            $post->date             = $request->input("post_date", date("Y/m/d H:i:s"), time());
            $post->permission       = $permission;
            $post->save();
            $isAleadySave           =  false;
            $locationFlag         = [];
            if ($request->has('location') && $request->has('checkin_date') && is_array($request->location)) {
                if (count($request->location) == 3 && isset($request->location['id'], $request->location['title'], $request->location['type'])) {
                    $checkins                   = new Checkins();
                    $checkins->users_id         = $authUser->id;
                    $checkins->location         = $request->location['title'];
                    $checkins->lat_lng          = $lat . ',' . $lng;
                    $checkins->checkin_time     = date("Y/m/d H:i:s", strtotime($request->checkin_date));
                    switch (strtolower($request->location['type'])) {
                        case CommonConst::POST_CHECKIN_PLACE:
                            $checkins->place_id = $request->location['id'];
                            $place = Place::find($checkins->place_id);
                            $isAleadySave = savePostInLocation($place, $post->id);
                            $locationFlag = [
                                CommonConst::POST_CHECKIN_PLACE,
                                $checkins->place_id
                            ];
                            break;
                        case CommonConst::POST_CHECKIN_CITY:
                            $checkins->city_id = $request->location['id'];
                            $city = Cities::find($checkins->city_id);
                            $isAleadySave = savePostInLocation($city, $post->id);
                            $locationFlag = [
                                CommonConst::POST_CHECKIN_CITY,
                                $checkins->city_id
                            ];
                            break;
                        case CommonConst::POST_CHECKIN_COUNTRY:
                            $checkins->country_id = $request->location['id'];
                            $country = Countries::find($checkins->country_id);
                            $isAleadySave = savePostInLocation($country, $post->id);
                            $locationFlag = [
                                CommonConst::POST_CHECKIN_COUNTRY,
                                $checkins->country_id
                            ];
                            break;
                    }
                    $checkins->save();

                    $post_checkins = new PostsCheckins();
                    $post_checkins->checkins_id = $checkins->id;
                    $post_checkins->posts_id = $post->id;
                    $post_checkins->save();
                }
            }

            if ($isAleadySave === false) {
                if ($request->has("page") && $request->has("page_id") && $request->get("page_id", 0) > 0) {
                    $page = strtolower(trim($request->get("page", '')));
                    switch ($page) {
                        case CommonConst::POST_CHECKIN_PLACE:
                            log_user_activity('Place_Post', 'post', $post->id);
                            savePostInLocation(Place::find($request->page_id), $post->id);
                            break;
                        case CommonConst::POST_CHECKIN_CITY:
                            log_user_activity('City_Post', 'post', $post->id);
                            savePostInLocation(Cities::find($request->page_id), $post->id);
                            break;
                        case CommonConst::POST_CHECKIN_COUNTRY:
                            log_user_activity('Country_Post', 'post', $post->id);
                            savePostInLocation(Countries::find($request->page_id), $post->id);
                            break;
                    }
                    $locationFlag = [
                        $page,
                        $request->page_id
                    ];
                }
            }

            log_user_activity(PrimaryPostService::TYPE_POST, 'publish', $post->id);

            if ($isFiles) {
                foreach ($fileLists as $file) {
                    $filename = $authUser->id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('post-photo/' . $filename, fopen($file[0], 'r+'), 'public');
                    $path = S3_BASE_URL . 'post-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $authUser->name;
                    $media->title = strip_tags($post->text);
                    $media->author_url = check_profile_picture($authUser->profile_picture);
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->users_id  = $authUser->id;
                    $media->type  = getMediaTypeByMediaUrl($path);
                    $media->save();

                    // get video thumbnail & save s3 & then DB
                    if ($media->type == Media::TYPE_VIDEO) {
                        $s3Path = str_replace(S3_BASE_URL, '', $media->url); // remove S3_BASE_URL if found
                        $some_file = Storage::disk('s3')->get($s3Path); // GET s3 video
                        Storage::disk('public')->put($s3Path, $some_file); // store s3 video in local server
                        $localVideoThumbnail = $ffmpegService->convertVideoThumbnail(public_path('storage/' . $s3Path), 600, 600, 70); // create Thumbnail in local based on local video
                        if ($localVideoThumbnail) {
                            Storage::disk('public')->delete($s3Path); // delete local video
                            $s3VideoThumbnail = $ffmpegService->moveThumbnailToS3($localVideoThumbnail, true); // move local Thumbnail in s3 & delete local Thumbnail
                            $media->video_thumbnail_url = $s3VideoThumbnail; // save s3 Thumbnail path in media table
                            $media->save();
                        }
                    }

                    $users_medias = new UsersMedias();
                    $users_medias->users_id = $authUser->id;
                    $users_medias->medias_id = $media->id;
                    $users_medias->save();

                    $posts_medias = new PostsMedia();
                    $posts_medias->posts_id = $post->id;
                    $posts_medias->medias_id = $media->id;
                    $posts_medias->save();

                    if (count($locationFlag) == 2) {
                        if ($locationFlag[0] == CommonConst::POST_CHECKIN_PLACE) {
                            $placeMediaSave = new PlaceMedias();
                            $placeMediaSave->places_id = $locationFlag[1];
                            $placeMediaSave->medias_id = $media->id;
                            $placeMediaSave->save();
                        } else if ($locationFlag[0] == CommonConst::POST_CHECKIN_CITY) {
                            $cityMediaSave = new CitiesMedias();
                            $cityMediaSave->cities_id = $locationFlag[1];
                            $cityMediaSave->medias_id = $media->id;
                            $cityMediaSave->save();
                        } else if ($locationFlag[0] == CommonConst::POST_CHECKIN_COUNTRY) {
                            $countryMediaSave = new CountriesMedias();
                            $countryMediaSave->countries_id = $locationFlag[1];
                            $countryMediaSave->medias_id = $media->id;
                            $countryMediaSave->save();
                        }
                    }

                    $isValideImage = moderateImage($media, $post);
                    if ($isValideImage === false) {
                        $post->delete();
                        return ApiResponse::__createBadResponse("Image is broken");
                    }
                    if ($media && $posts_medias && $users_medias && $post) {
                        if (count($fileLists) == 1) {
                            log_user_activity('Media', 'upload', $media->id);
                        }
                    }
                }
                // delete all temp media related to pair = $pair
                app(CommonApiController::class)->deleteTempFiles($pair);
            }

            save_tags($tags, PostsTags::TYPE_POST, $post->id);
            DB::commit();

            return ApiResponse::create([
                'post' => app(HomeService::class)->__preparedPost($post->id)
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param string pair for tempfile prefix
     * @return array [filepath, filename]
     */
    public function getTempFiles($pair)
    {
        $user = Auth::user();
        $temp_dir = public_path() . '/assets2/upload_tmp/';
        $fileLists = [];

        if (is_dir($temp_dir)) {
            if ($handle = opendir($temp_dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    if (strpos($file, $pair . '_' . $user->id) !== false) {
                        $filename_split = explode($pair . '_' . $user->id . '_', $file, 2);
                        $fileLists[] = [$temp_dir . $file, $filename_split[1]];
                    }
                }
                closedir($handle);
            }
        }

        return $fileLists;
    }

    /**
     * @param DeletePostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postDelete(DeletePostRequest $request)
    {
        try {
            DB::beginTransaction();

            $post_id = $request->post_id;
            $post_type = $request->post_type;
            $user = Auth::user();

            $check_post_exists = Posts::where('id', $post_id)->where('users_id', $user->id)->get()->first();
            if (!$check_post_exists) {
                return ApiResponse::create(
                    [
                        'message' => ["Post does not exists"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            switch (strtolower($post_type)) {
                case 'checkin':
                    $check_post_exists = Posts::where('id', $post_id)->where('users_id', $user->id)->get()->first();
                    $check_checkin_exists = $check_post_exists->checkin[0];
                    $check_postcheckin_exists = PostsCheckins::where('posts_id', $post_id)->get();
                    if (is_object($check_postcheckin_exists)) {
                        foreach ($check_postcheckin_exists as $checkin) {
                            $checkin->delete();
                        }
                    }
                    if (is_object($check_checkin_exists)) {
                        $check_checkin_exists->delete();
                    }

                    // Delete main Post
                    $check_post_exists->delete();
                    log_user_activity('Post', 'delete', $post_id);
                    break;

                case 'post':
                    $check_post_exists = Posts::where('id', $post_id)->where('users_id', $user->id)->get()->first();

                    // Delete Shared Post
                    $check_share_exists = PostsShares::where('posts_id', $post_id)->get();
                    if (is_object($check_share_exists)) {
                        foreach ($check_share_exists as $share) {
                            $share->delete();
                        }
                    }

                    // Delete Post comments
                    $check_comment_exists = PostsComments::where('posts_id', $post_id)->get();
                    if (is_object($check_comment_exists)) {
                        foreach ($check_comment_exists as $comment) {
                            foreach ($comment->medias as $com_media) {
                                $com_media->delete();
                            }
                            foreach ($comment->likes as $com_like) {
                                $com_like->delete();
                            }
                            $comment->delete();
                        }
                    }

                    // Delete Post likes
                    $check_likes_exists = PostsLikes::where('posts_id', $post_id)->get();
                    if (is_object($check_likes_exists)) {
                        foreach ($check_likes_exists as $like) {
                            $like->delete();
                        }
                    }

                    // Delete Post medias
                    $check_postmedia_exists = PostsMedia::where('posts_id', $post_id)->get();
                    if (is_object($check_postmedia_exists)) {
                        foreach ($check_postmedia_exists as $media) {
                            $medias_exist = Media::find($media->medias_id);
                            $user_media_exist = UsersMedias::where('medias_id', $media->medias_id)->where('users_id', $user->id)->first();

                            $media->delete();
                            if ($medias_exist) {
                                $amazonefilename = explode('/', $medias_exist->url);
                                Storage::disk('s3')->delete('post-photo/' . end($amazonefilename));
                                $medias_exist->delete();
                            }

                            if ($user_media_exist) {
                                $user_media_exist->delete();
                            }
                        }
                    }

                    // Delete Post tags
                    $check_posttag_exists = PostsTags::where('posts_id', $post_id)->get();
                    if (is_object($check_posttag_exists)) {
                        foreach ($check_posttag_exists as $tag) {
                            $tag->delete();
                        }
                    }

                    // Delete main Post
                    $check_post_exists->delete();
                    log_user_activity('Post', 'delete', $post_id);
                    break;

                case "media":
                    /*----------- After discussion ---------*/
                    $check_user_exists = UsersMedias::where('medias_id', $post_id)->where('users_id', $user->id)->get();
                    $check_post_exists = PostsMedia::where('medias_id', $post_id)->get();
                    $check_media_exists = Media::where('id', $post_id)->get()->first();

                    if (!$check_user_exists || !$check_post_exists || !$check_media_exists) {
                        return ApiResponse::create(
                            [
                                'message' => ["Media Post not deleted"]
                            ],
                            false,
                            ApiResponse::BAD_REQUEST
                        );
                    }

                    // deleting media comments
                    foreach ($check_media_exists->comments as $comment) {
                        // deleting media comment likes
                        foreach ($comment->likes as $like) {
                            $like->delete();
                        }
                        $comment->delete();
                    }

                    // deleting media likes
                    foreach ($check_media_exists->likes as $like) {
                        $like->delete();
                    }

                    // deleting media likes
                    foreach ($check_media_exists->shares as $share) {
                        $share->delete();
                    }

                    // deleting media post
                    foreach ($check_post_exists as $post) {
                        $post->delete();
                    }

                    // deleting user media
                    foreach ($check_user_exists as $usermedia) {
                        $usermedia->delete();
                    }

                    $amazonefilename = explode('/', $check_media_exists->url);
                    Storage::disk('s3')->delete('post-photo/' . end($amazonefilename));

                    // Delete main Media Post
                    $check_media_exists->delete();
                    log_user_activity('Media', 'delete', $post_id);
                    break;

                case "review":
                    $review_exists = Reviews::where('id', $post_id)->where('by_users_id', $user->id)->get()->first();

                    if (!is_object($review_exists)) {
                        return ApiResponse::create(
                            [
                                'message' => ["Review Post does not exist."]
                            ],
                            false,
                            ApiResponse::BAD_REQUEST
                        );
                    }

                    $review_vote_exists = ReviewsVotes::where('review_id', $post_id)->get();

                    // Delete Post Votes
                    if (is_object($review_vote_exists)) {
                        foreach ($review_vote_exists as $vote) {
                            $vote->delete();
                        }
                    }

                    // Delete Origin Review
                    $review_exists->delete();
                    log_user_activity('Review', 'delete', $post_id);
                    break;
            }
            DB::commit();
            return ApiResponse::create(
                [
                    'message' => ["Post Deleted"]
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postLikeUnlike(Request $request)
    {
        try {
            if ($request->post_id) {
                $user = Auth::user();
                $post = Posts::where('id', $request->post_id)->first();
                if (!is_object($post)) {
                    return ApiResponse::create(
                        [
                            'message' => ["Post not found"]
                        ],
                        false,
                        ApiResponse::BAD_REQUEST
                    );
                }
                $check_like_exists = PostsLikes::where('posts_id', $post->id)->where('users_id', $user->id)->get()->first();
                $likeOrNotFlag = false;
                DB::beginTransaction();
                if (is_object($check_like_exists)) {
                    $check_like_exists->delete();
                    log_user_activity('Post', 'unlike', $post->id);
                    unnotify($post->users_id, 'status_like', $post->id);
                } else {
                    $likeOrNotFlag = true;
                    $like = new PostsLikes;
                    $like->posts_id = $post->id;
                    $like->users_id = $user->id;
                    $like->save();
                    log_user_activity('Post', 'like', $post->id);

                    if ($user->id != $post->users_id) {
                        notify($post->users_id, 'status_like', $post->id);
                    }
                }
                DB::commit();
                return ApiResponse::create(
                    [
                        'status' => $likeOrNotFlag,
                        'count' => PostsLikes::where('posts_id', $post->id)->count()
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ["post_id must be required"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    public function setPrivacy(PostPrivacyRequest $request)
    {
        try {
            $post = Posts::where('id',  $request->post_id)->first();
            if (!is_object($post)) {
                return ApiResponse::create(
                    [
                        'message' => ["Post not found"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $post->permission   = $request->permission;
            $post->save();

            return ApiResponse::create([
                'message' => [
                    "Privacy change successfully"
                ]
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postLikes4Modal(Request $request)
    {
        try {
            $user = Auth::user();
            $post = Posts::where('id', $request->post_id)->where('users_id', $user->id)->first();
            if (!is_object($post)) {
                return ApiResponse::create(
                    [
                        'message' => ["Post not found"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $post_likes = PostsLikes::where('posts_id', $post->id)->get();
            $likeUsers = [];
            foreach ($post_likes as $like) {
                $likeUsers[] = [
                    'id' => $like->user->id,
                    'name' => $like->user->name,
                    'profile_picture' => check_profile_picture($like->user->profile_picture)
                ];
            }
            return ApiResponse::create($likeUsers);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postShareUnshare(Request $request)
    {
        try {
            $user = Auth::user();
            $post = Posts::where('id', $request->post_id)->where('users_id', $user->id)->first();
            if (!is_object($post)) {
                return ApiResponse::create(
                    [
                        'message' => ["Post not found"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $check_share_exists = PostsShares::where('posts_id', $post->id)->where('users_id', $user->id)->first();

            $response = [];
            DB::beginTransaction();
            if (is_object($check_share_exists)) {
                $response['post_id'] = $check_share_exists->posts_id;
                $check_share_exists->delete();
                log_user_activity('Post', 'unshare', $post->id);
            } else {
                $share = new PostsShares;
                $share->posts_id = $post->id;
                $share->users_id = $user->id;
                $share->save();
                log_user_activity('Post', 'share', $post->id);

                $response = [
                    'share_id' => $share->id,
                    'post_id' => $post->id,
                    'author_profile_picture' => check_profile_picture($share->author->profile_picture),
                    'author_name' => $share->author->name,
                    'shared_at' => diffForHumans($share->created_at)
                ];
            }
            $response['count'] = PostsShares::where('posts_id', $post->id)->count();
            DB::commit();
            return ApiResponse::create($response);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/posts/spam",
     *   tags={"Posts"},
     *   summary="Report post",
     *   operationId="report_post",
     *   security={
     *       {"bearer_token": {}
     *           }},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="report_type",
     *                  description="Type",
     *                  type="integer"
     *              ),
     *              @OA\Property(
     *                  property="text",
     *                  type="string",
     *                  description="Text"
     *              ),
     *              @OA\Property(
     *                  property="id",
     *                  type="integer",
     *                  description="Post id"
     *              ),
     *              @OA\Property(
     *                  property="post_type",
     *                  type="string",
     *                  description="Discussion | Post | Mediaupload | Trip | Checkin | PlaceImage| Report | Review | Event"
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param SpamPostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function reportSpam(SpamPostRequest $request)
    {
        try {
            $type = $request->report_type;     //report type   0: spam, 1: other
            $posttype = (isset($request->post_type) && !empty($request->post_type)) ? $request->post_type : 'Post'; //post type
            $id = $request->id;    //dataid  
            $text = $request->text;     //report text
            $users_id = Auth::user()->id;
            $alreadyReported = SpamsPosts::where(
                [
                    'posts_id' => $id,
                    'users_id' => $users_id,
                    'post_type' => $posttype,
                    'report_type' => $type
                ]
            )->first();

            if ($alreadyReported) {
                return ApiResponse::create(
                    [
                        'message' => ["You have already reported before"]
                    ]
                );
            }


            $model = new SpamsPosts();
            $model->posts_id = $id;
            $model->users_id = $users_id;
            $model->post_type = $posttype;
            $model->report_type = $type;
            $model->report_text = $text;
            $model->save();
            // $user = Auth::user();
            // $id = $request->id;
            // $postTypes = Posts::postTypes();
            // $commentTypes = PostsComments::commentTypes();
            // $activity = ActivityLog::where('variable', $id)->first();
            // if (!is_object($activity)) {
            //     return ApiResponse::create(
            //         [
            //             'message' => ["Post not found"]
            //         ],
            //         false,
            //         ApiResponse::BAD_REQUEST
            //     );
            // }
            // $post_type = $activity->type;
            // $post_type = $activity->type;

            // DB::beginTransaction();

            //remove post if it reported by API and flagged by user
            // if ($post_type === $postTypes[Posts::POST_TYPE_POST]) {
            //     $isSpamPostDeleted = SpamsPosts::where('posts_id', $id)->where('report_type', 8)->delete();
            // } elseif ($post_type === $postTypes[Posts::POST_TYPE_MEDIAUPLOAD]) {
            //     $postMedia = PostsMedia::where('medias_id', $id)->first();
            //     if ($postMedia) {
            //         $isSpamPostDeleted = SpamsPosts::where('posts_id', $postMedia->posts_id)->where('report_type', 8)->delete();
            //     }
            // }

            // if (isset($isSpamPostDeleted) && $isSpamPostDeleted) {
            //     DB::commit();
            //     return ApiResponse::create(
            //         [
            //             'message' => ["Thanks for your report."]
            //         ]
            //     );
            // }

            // if (in_array($post_type, $postTypes)) {
            //     $spamsPost = new SpamsPosts();
            //     $spamsPost->posts_id = $id;
            //     $spamsPost->users_id = $user->id;
            //     $spamsPost->post_type = $post_type;
            //     $spamsPost->report_type = $request->report_type;
            //     $spamsPost->report_text = $request->text;
            //     $spamsPost->save();
            // } else {
            //     $spamsComment = new SpamsComments();
            //     $spamsComment->comments_id = $id;
            //     $spamsComment->users_id = $user->id;
            //     $spamsComment->comment_type = $post_type;
            //     $spamsComment->report_type = $request->report_type;
            //     $spamsComment->report_text = $request->text;
            //     $spamsComment->save();
            // }

            // DB::commit();
            return ApiResponse::create(
                [
                    'message' => ["Thanks for your report."]
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/posts/report-types",
     *   tags={"Newsfeed"},
     *   summary="This Api used to fetch all report type",
     *   operationId="Api\PostController@getReportType",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @return ApiResponse
     */
    public function getReportType()
    {
        try {
            return ApiResponse::create([
                'report_types' => Posts::reportType()
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**getReportType
     * @return ApiResponse
     */
    public function getPostType()
    {
        try {
            $postTypes      = array_values(Posts::postTypes());

            return ApiResponse::create([
                'post_types' => $postTypes
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function checkinLikeUnlike(Request $request)
    {
        try {
            $user = Auth::user();
            $post = Posts::where('id', $request->post_id)->where('users_id', $user->id)->first();
            if (!is_object($post)) {
                return ApiResponse::create(
                    [
                        'message' => ["Post not found"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $check_like_exists = CheckinsLikes::where('checkins_id', $post->id)->where('users_id', $user->id)->first();

            DB::beginTransaction();
            if (is_object($check_like_exists)) {
                $check_like_exists->delete();
                log_user_activity('Checkin', 'unlike', $post->id);
            } else {
                $like = new CheckinsLikes;
                $like->checkins_id = $post->id;
                $like->users_id = $user->id;
                $like->save();
                log_user_activity('Checkin', 'like', $post->id);
            }
            DB::commit();
            return ApiResponse::create(
                [
                    'count' => CheckinsLikes::where('checkins_id', $post->id)->count()
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function checkinComment(Request $request)
    {
        try {
            $user = Auth::user();
            $post = Posts::where('id', $request->post_id)->where('users_id', $user->id)->first();

            if (!is_object($post) || count($post->checkin) == 0) {
                return ApiResponse::create(
                    [
                        'message' => ["CheckIn Post does not exists."]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            DB::beginTransaction();
            $post_comment = processString($request->input("text", ""));
            if (empty($post_comment)) {
                return ApiResponse::create(
                    [
                        'message' => ["Comment can not be empty"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $new_comment = new CheckinsComments;
            $new_comment->users_id = $user->id;
            $new_comment->checkins_id = $post->id;
            $new_comment->text = $post_comment;
            $new_comment->save();
            log_user_activity('Checkin', 'comment', $post->id);

            DB::commit();
            return ApiResponse::create(
                [
                    'post'    => $post,
                    'comment' => $new_comment
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CommentEditRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCommentEdit(CommentEditRequest $request)
    {
        try {
            $media_file = [];
            $user = Auth::user();

            // Checking Existing Post
            $post = Posts::where('id', $request->post_id)->where('users_id', $user->id)->first();
            if (!is_object($post)) {
                return ApiResponse::create(
                    [
                        'message' => ["Post not found"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            // Checking Existing Comment
            $post_comment = PostsComments::where('id', $request->comment_id)->where('posts_id', $post->id)->first();
            if (!is_object($post_comment)) {
                return ApiResponse::create(
                    [
                        'message' => ["Post Comment not found"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            DB::beginTransaction();

            $fileLists = $this->getTempFiles($request->input("pair", ""));
            $isFiles = count($fileLists) > 0 ? true : false;
            $post_text = convert_string($request->input("text", ""));
            $existing_medias = $request->input('existing_media', []);

            $post_comment->text = $post_text;
            $post_comment->save();
            log_user_activity('Status', 'comment', $post->id);

            if ($user->id != $post_comment->users_id) {
                notify($post->users_id, 'status_comment', $post->id);
            }

            // Check existing media for the comment
            if (count($post_comment->medias) > 0) {
                foreach ($post_comment->medias as $media_list) {
                    if (in_array($media_list->media->id, $existing_medias)) {
                        $medias_exist = Media::find($media_list->medias_id);

                        // Delete media list
                        $media_list->delete();

                        // Delete medias
                        if ($medias_exist) {
                            $amazonefilename = explode('/', $medias_exist->url);
                            Storage::disk('s3')->delete('post-comment-photo/' . end($amazonefilename));
                            $medias_exist->delete();
                        }
                    }
                }
            }

            if ($isFiles) {
                foreach ($fileLists as $file) {
                    $filename = $user->id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('post-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

                    $media_file[] = $path = 'https://s3.amazonaws.com/travooo-images2/post-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user->name;
                    $media->title = $post_text;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->type  = getMediaTypeByMediaUrl($media->url);
                    $media->users_id = $user->id;
                    $media->save();

                    $posts_comments_medias = new PostsCommentsMedias();
                    $posts_comments_medias->posts_comments_id = $post_comment->id;
                    $posts_comments_medias->medias_id = $media->id;
                    $posts_comments_medias->save();
                }
            }

            $post_comment->load('medias');
            DB::commit();
            return ApiResponse::create(
                [
                    'post'          => $post,
                    'comment'       => $post_comment,
                    'media_file'    => $media_file,
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postCommentDelete(Request $request, PrimaryPostService $primaryPostService, CommentService $commentService, $post_id, $comment_id)
    {
        try {
            $user = Auth::user();
            // Checking Existing Post
            $post = $primaryPostService->find(PrimaryPostService::TYPE_POST, $post_id);
            if (!$post) return ApiResponse::__createBadResponse("Post not found");

            // Checking Existing Comment
            $comment = $commentService->findComment(PrimaryPostService::TYPE_POST, $comment_id, true);
            if (!$comment)  return ApiResponse::__createBadResponse("Post Comment not found");

            DB::beginTransaction();
            if ($comment->medias) {
                foreach ($comment->medias as $com_media) {
                    $medias_exist = Media::find($com_media->medias_id);
                    $com_media->delete();

                    if ($medias_exist) {
                        $amazonefilename = explode('/', $medias_exist->url);
                        Storage::disk('s3')->delete('post-comment-photo/' . end($amazonefilename));
                        $medias_exist->delete();
                    }

                    $user_media_exist = UsersMedias::where('medias_id', $com_media->medias_id)->where('users_id', $user->id)->first();
                    if ($user_media_exist) {
                        $user_media_exist->delete();
                    }
                }
            }
            $comment->delete();
            DB::commit();

            return ApiResponse::__create("Comment deleted");
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function checkinCommentDelete(Request $request)
    {
        try {
            $user = Auth::user();
            // Checking Existing Comment
            $comment = CheckinsComments::where('id', $request->comment_id)->where('users_id', $user->id)->first();
            if (!is_object($comment)) {
                return ApiResponse::create(
                    [
                        'message' => ["Post Comment not found"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $comment->delete();
            return ApiResponse::create(
                [
                    'message' => ["Comment deleted"]
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function checkinCommentLikeUnlike(Request $request)
    {
        try {
            $user = Auth::user();
            // Checking Existing Comment
            $comment = CheckinsComments::where('id', $request->comment_id)->where('users_id', $user->id)->first();
            if (!is_object($comment)) {
                return ApiResponse::create(
                    [
                        'message' => ["Post Comment not found"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $check_like_exists = CheckinsCommentsLikes::where('checkins_comments_id', $comment->id)->where('users_id', $user->id)->first();

            DB::beginTransaction();
            if (is_object($check_like_exists)) {
                $check_like_exists->delete();
                log_user_activity('Checkin', 'commentunlike', $comment->id);
            } else {
                $like = new CheckinsCommentsLikes;
                $like->checkins_comments_id = $comment->id;
                $like->users_id = $user->id;
                $like->save();

                log_user_activity('Checkin', 'commentlike', $comment->id);
            }
            DB::commit();
            return ApiResponse::create(
                [
                    'count' => CheckinsCommentsLikes::where('checkins_comments_id', $comment->id)->count(),
                    'name' => $user->name,
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    public function getCommentLikeUsers(Request $request)
    {
        try {
            $user = Auth::user();
            // Checking Existing Comment
            $comment = PostsComments::where('id', $request->comment_id)->where('users_id', $user->id)->first();
            if (!is_object($comment)) {
                return ApiResponse::create(
                    [
                        'message' => ["Post Comment not found"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            // Fetch Latest comment likes user
            $commentLikes = $comment->likes()->orderBy('created_at', 'DESC')->get();
            $likeUsers = [];
            foreach ($commentLikes as $like) {
                $likeUsers[] = [
                    'author_id' => $like->author->id,
                    'name' => $like->author->name,
                    'profile_picture' => check_profile_picture($like->author->profile_picture)
                ];
            }
            return ApiResponse::create(
                [
                    'users' => $likeUsers,
                    'count' => count($comment->likes),
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
