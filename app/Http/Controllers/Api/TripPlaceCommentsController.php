<?php

namespace App\Http\Controllers\Api;


use Carbon\Carbon;
use Auth;
use DB;
use App\Http\Responses\ApiResponse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Api\TripPlaceComments\CommentsRequest;

use App\Models\User\User;
use App\Services\Trips\TripPlaceCommentsService;

class TripPlaceCommentsController extends Controller
{
   
    /**
     * @param Request $request
     * @param TripPlaceCommentsService $tripPlaceCommentsService
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request, TripPlaceCommentsService $tripPlaceCommentsService)
    {
        try {
            $tripPlaceId = $request->get('trip_place_id');
            $list = $tripPlaceCommentsService->getList($tripPlaceId, $request->get('per-page', null));

            $data = [
                'meta' => [
                    'total' => $list->total(),
                    'current_page' => $list->currentPage(),
                    'from' => $list->firstItem(),
                    'last_page' => $list->lastPage(),
                    'next_page_url' => $list->nextPageUrl(),
                    'per_page' => $list->perPage(),
                    'prev_page_url' => $list->previousPageUrl(),
                    'to' => $list->lastItem(),
                ],
                'comments' => []
            ];

            foreach ($list->items() as $comment) {
                $user = User::query()->find($comment->user_id);

                if (!$user) {
                    continue;
                }

                $data['comments'][] = [
                    'comment' => $comment->comment,
                    'user' => [
                        'img' => check_profile_picture($user->profile_picture),
                        'name' => $user->name,
                        'username' => $user->username,
                    ],
                    'created' => $comment->created_at->diffForHumans()
                ];
            }
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CommentsRequest $request
     * @param TripPlaceCommentsService $tripPlaceCommentsService
     * @return \Illuminate\Http\Response
     */
    public function create(CommentsRequest $request, TripPlaceCommentsService $tripPlaceCommentsService)
    {
        try {
            $comment = $request->get('comment');
            //todo same trip place id in suggestions
            $tripPlaceId = $request->get('trip_place_id');

            $tripPlaceCommentsService->create($tripPlaceId, $comment);
            return ApiResponse::create(
                [
                    'message'=>[true]
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
