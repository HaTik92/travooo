<?php

namespace App\Http\Controllers\Api\Country;

/* Dependencies */

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Http\Requests\Api\Countries\CountryCitiesRequest;
use App\Http\Requests\Api\Countries\CountryDiscussionsRequest;
use App\Http\Requests\Api\Countries\CountryInformationRequest;
use App\Http\Requests\Api\Countries\CountryMediasRequest;
use App\Http\Requests\Api\Countries\CountryPlacesRequest;
use App\Http\Requests\Api\Countries\CountryShareRequest;
use App\Http\Requests\Api\Countries\CountryShowRequest;
use App\Http\Requests\Api\Countries\CountryTripPlansRequest;
use App\Http\Requests\Api\Countries\CountryUnfollowRequest;
use App\Models\Access\language\Languages;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Country\ApiCountry as Country;
use App\Models\Country\Countries;
use App\Models\Country\CountriesFollowers;
use App\Models\Place\Place;
use App\Transformers\Country\CountryTransformer;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Item;

/**
 * Class Api/CountryController.
 */
class CountryController extends Controller {

    use CreateUpdateStatisticTrait;
    public function __construct() {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
    }

    /**
     * @param CountryShowRequest $request
     * @return array
     */
    public function show(CountryShowRequest $request) {
        $countryId = $request->input('country_id');

        /* Find Country For The Provided Country Id */
        $country = Country::find($countryId);

        /* If Country Not Found, Return Error */
        if(empty($country)){
            return [
                'status' => false,
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong country id provided.'
                ]
            ];
        }

        /* Get Country Information In Array Format */
        $country_arr = $country->getArrayResponse();

        /* Return Success Status, And Country Information */
        return [
            'status' => true,
            'data'   => [
                'country_info' => $country_arr
            ]
        ];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function get_countries(Request $request){

        $post = $request->input();

        $query     = null;
        $countries = null;
        $language  = 1; 

        $offset = 0;
        $limit  = 20;
        
        if(!isset($post['language_id']) || empty($post['language_id'])){
            return [
                'success' => false,
                'code'    => 400,
                'data'    => 'Language id not provided.'
            ];
        }

        if(isset($post['language_id']) && !empty($post['language_id'])){
            $language = $post['language_id'];
        }

        if(isset($post['query']) && !empty($post['query'])){
            $query = $post['query'];
        }

        if(isset($post['offset']) && !empty($post['offset'])){
            $offset = $post['offset'];
        }

        if(isset($post['limit']) && !empty($post['limit'])){
            $limit = $post['limit'];
        }

        $countries = Country::select('countries.id as cId','countries.*','countries_trans.*');

        $countries = $countries->join('countries_trans', 'countries.id', '=', 'countries_trans.countries_id')->where(['active' => 1,'languages_id' => $language]);

        if(!empty($query)){
            $countries = $countries->where('title', 'REGEXP', $query);
        }

        $countries = $countries->orderBy('title', 'asc')->offset($offset)->limit($limit);
        $countries = $countries->get();
        
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            $countries_arr[] = $value->getResponse();   
        }

        // $countries = json_encode($countries_arr);
        $countries = $countries_arr;
        
        return [
            'success' => true,
            'code'    => 200,
            'data'    => $countries
        ];
    }

    /**
     * api/countries/{country_id}
     *
     * Show Country information
     *
     * parameter Integer "language_id" (required) <br />
     *
     * @param $countryId
     * @param CountryInformationRequest $request
     * @return array
     */

    public function getCountryInformation($countryId, CountryInformationRequest $request)
    {
        $languageId =  $request->input('language_id');
        $country    = Countries::find($countryId);

        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
            'trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'region.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'languages.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'holidays.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'religions.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'currencies.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'capitals.city.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'emergency.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'places' => function ($query) {
                $query->where('place_type', 'like', '%atm%');
            },
            'timezone'
        ])
            ->where('id', $countryId)
            ->where('active', 1)
            ->first();

        if(!$country) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('countries_id', '=', $country->id)
            ->count();

        $languages = $country->getLanguages();
        $country->setRelation('languages', collect($languages));

        $resource = new Item($country, new CountryTransformer());

        return apiResponse(
            $resource,
            [
                'trans',
                'region:',
                'region.trans:title',
                'languages:title',
                'holidays:',
                'holidays.trans:title|slug',
                'religions:',
                'religions.trans:title',
                'currencies:',
                'currencies.trans:title',
                'capitals:',
                'capitals.city:',
                'capitals.city.trans:title',
                'emergency:',
                'emergency.trans:title|emergency_numbers_id|description',
                'timezone:time_zone_id|time_zone_name|dst_offset|raw_offset'
            ],
            [
                'atms_count' => count($country->places),
                'places_count' => $placeCount
            ]
        );
    }

    /**
     * @param $countryId
     * @return array
     */
    public function getCountryNumberFollowers($countryId)
    {
        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }


        $country = Countries::with([
            'followers'
        ])->where('id', $countryId)->first();

        return [
            'data' => [
                'followers' => count($country->followers)
            ],
            'success' => true
        ];
    }

    /**
     * @param $countryId
     * @return array
     */
    public function postFollow($countryId)
    {
        $userId = Auth::Id();
        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $follower = CountriesFollowers::where('countries_id', $countryId)
            ->where('users_id', $userId)
            ->first();

        if($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that country',
                ],
                'success' => false
            ];
        }

       $country->followers()->create([
            'users_id' => $userId
        ]);

        $this->updateStatistic($country, 'followers', count($country->followers));
        log_user_activity('Country', 'follow', $countryId);

        return [
            'success' => true
        ];
    }

    /**
     * @param $countryId
     * @param CountryUnfollowRequest $request
     * @return array
     */
    public function postUnFollow($countryId, CountryUnfollowRequest $request)
    {
        $userId     = Auth::Id();
        $languageId =  $request->input('language_id');

        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $follower = CountriesFollowers::where('countries_id', $countryId)
                                      ->where('users_id', $userId);

        if(!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this country.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            $this->updateStatistic($country, 'followers', count($country->followers));
            log_user_activity('Country', 'unfollow', $countryId);
        }

        return [
            'success' => true
        ];
    }

    /**
     * @param $countryId
     * @return array
     */
    public function getCountryStats($countryId) {
        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        return [
            'data' => [
                'stats' => $country->statistics,
            ],
            'success' => true
        ];
    }

    /**
     * @param $countryId
     * @param CountryCitiesRequest $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCountryCities($countryId, CountryCitiesRequest $request) {
        $languageId = $request->input('language_id');
        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
            'cities',
            'cities.getMedias.trans' => function ($query) use ($languageId) {
                $query->where('languages_id', $languageId);
            },
        ])->where('id', $countryId)->first();

        $resource = new Item($country, new CountryTransformer());

        return apiResponse(
            $resource,
            [
                'cities:id',
                'cities.trans:title|population',
                'cities.medias'
            ]
        );
    }

    /**
     * @param $countryId
     * @param CountryMediasRequest $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCountryMedia($countryId, CountryMediasRequest $request) {
        $languageId = $request->input('language_id');
        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
            'getMedias',
            'getMedias.users'
        ])->where('id', $countryId)->first();

        $resource = new Item($country, new CountryTransformer());

        return apiResponse(
            $resource,
            [
                'medias:id|url|uploaded_at',
                'medias.users:id|name|username|profile_picture|address'
            ]
        );
    }

    /**
     * @param $countryId
     * @param CountryTripPlansRequest $request
     * @return array
     */
    public function getCountryTripPlans($countryId, CountryTripPlansRequest $request) {
        $languageId = $request->input('language_id');
        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $citiesMediasSql = DB::table('cities_medias')
            ->select([
                'cities_id',
                DB::raw('max(medias_id) AS medias_id')
            ])
            ->groupBy('cities_id')
            ->toSql();

        $tripPlans = Country::select(
            'medias.id AS media_id',
            'medias.url',
            'trips_cities.cities_id',
            'trips.id',
            'trips.title',
            'trips.users_id',
            'trips.created_at',
            'trips_places.budget',
            'trips_places.id'
        )
            ->join('places', 'countries.id', '=', 'places.countries_id')
            ->join('trips_places', function($join) {
                $join->on('trips_places.countries_id', '=', 'countries.id')
                     ->on('trips_places.places_id', '=', 'places.id');
            })
            ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
            ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
            ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
            ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
            ->where('countries.id', $countryId)
            ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id'  => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object)$media;
        }

        $country['plans'] = $tripPlans;
        $country['count'] = count($tripPlans);
        $country['sum_budget'] = $sumBudget;

        return [
            'data' => $country,
            'success' => true
        ];
    }

    /**
     * @param $countryId
     * @param CountryPlacesRequest $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCountryPlaces($countryId, CountryPlacesRequest $request) {
        $languageId = $request->input('language_id');
        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
            'places' => function ($query) use ($languageId) {
                $query->orderBy('rating');
                $query->limit(50);
            },
            'places.getMedias.trans' => function ($query) use ($languageId) {
                $query->where('languages_id', $languageId);
            },
        ])->where('id', $countryId)->first();

        $resource = new Item($country, new CountryTransformer());

        return apiResponse(
            $resource,
            [
                'places',
                'places:id',
                'places.trans:title',
                'places.medias'
            ]
        );
    }

    /**
     * @param $countryId
     * @param CountryDiscussionsRequest $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCountryDiscussions($countryId, CountryDiscussionsRequest $request) {
        $languageId = $request->input('language_id');
        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $country = Countries::with([
            'discussions',
            'discussions.UpVotes',
            'discussions.DownVotes',
            'discussions.users',
        ])->where('id', $countryId)->first();

        $resource = new Item($country, new CountryTransformer());

        return apiResponse(
            $resource,
            [
                'discussions',
                'discussions.users:username|profile_picture',
                'discussions.UpVotes',
                'discussions.DownVotes',
            ]
        );
    }

    /**
     * @param $countryId
     * @return array
     */
    public function checkFollow($countryId)
    {
        $userId = Auth::Id();
        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $follower = CountriesFollowers::where('countries_id', $countryId)
            ->where('users_id', $userId)
            ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    /**
     * @param $countryId
     * @param CountryShareRequest $request
     * @return array
     */
    public function postShare($countryId, CountryShareRequest $request) {
        $userId  = Auth::Id();
        $scope   = $request->input('scope', 2);
        $comment = $request->input('comment', null);
        $country = Countries::find($countryId);
        if(!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $country->shares()->create([
            'user_id'    => $userId,
            'scope'      => $scope,
            'comment'    => $comment,
            'created_at' => date('Y-m-d H:i:s', time())
        ]);

        $this->updateStatistic($country, 'shares', count($country->shares));

        return [
            'success' => true
        ];
    }
    
    
    
    
    
}
