<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Auth;
use DB;
use App\Http\Responses\ApiResponse;

use App\Models\User\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getUserList(Request $request) {
        try {
            $query = $request->input('q');
            $users = array();
            $get_users = User::where('username', 'like', '%' . $query . '%')->select(array('username'))
                    ->get();
            foreach ($get_users AS $gusers) {
                $users[] = $gusers->username;
            }

            return ApiResponse::create(
                [
                    'users' => $users
                ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
