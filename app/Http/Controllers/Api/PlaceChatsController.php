<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Chat\SendGroupMessage;
use App\Http\Responses\AjaxResponse;
use App\Services\PlaceChat\PlaceChatsService;
use Illuminate\Http\Request;
use App\Http\Responses\ApiResponse;
use App\Models\Chat\ChatConversation;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsContributionRequests;
use App\Services\Trips\TripInvitationsService;
use Carbon\Carbon;
use Auth;
use DB;

class PlaceChatsController extends Controller
{

    /**
     * @OA\POST(
     ** path="/chat/{chat_conversation_id}/message/",
     *   tags={"Trip Chat"},
     *   summary="Send Message",
     *   operationId="Api/PlaceChatsController@ajaxCreatePlaceChatMessage",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="chat_conversation_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="place_id",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="message",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @return ApiResponse
     * @param PlaceChatsService $placeChatsService
     */
    public function ajaxCreatePlaceChatMessage(
        SendGroupMessage $request,
        PlaceChatsService $placeChatsService,
        $chat_conversation_id
    ) {
        try {
            $chatId     = $chat_conversation_id;
            $message    = $request->message;
            $placeId    = $request->place_id;
            $planId     = $request->trip_id;

            $conversation = ChatConversation::find($chatId);
            if (!$conversation) {
                return ApiResponse::__createBadResponse('chat not found');
            }

            $trip = TripPlans::find($planId);
            if (!$trip) {
                return ApiResponse::__createBadResponse('trip plan not found.');
            }
            $placeChatsService->setApi(true);
            if ($message = $placeChatsService->createPlaceChatMessage($chatId, $message, $planId, $placeId)) {
                return ApiResponse::create($message);
            } else {
                return ApiResponse::__createBadResponse("sorry, you can not send message");
            }
        } catch (\Throwable $e) {
            if (get_class($e) == ModelNotFoundException::class) {
                return ApiResponse::__createBadResponse('trip not found');
            } else {
                return ApiResponse::createServerError($e);
            }
        }
    }

    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return ApiResponse
     */
    public function ajaxPlanChats(
        Request $request,
        PlaceChatsService $placeChatsService,
        $trip_id
    ) {
        try {
            return ApiResponse::create(
                $placeChatsService->getPlanChats($trip_id)['data'][0]
            );
        } catch (\Throwable $e) {
            if (get_class($e) == ModelNotFoundException::class) {
                return ApiResponse::__createBadResponse('trip not found');
            } else {
                return ApiResponse::createServerError($e);
            }
        }
    }


    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return ApiResponse
     */
    public function ajaxGetPlaceChatMessages(
        Request $request,
        PlaceChatsService $placeChatsService,
        $chatId
    ) {
        $chatId = $request->chat_id;
        return ApiResponse::create(
            $placeChatsService->getPlaceChatMessages($chatId)
        );
    }


    /**
     * @OA\Get(
     ** path="/chat/place/users",
     *   tags={"Trip Chat"},
     *   summary="Fetch users (FETCH_TRIP_PLACE_USERS_LIST_URL)",
     *   operationId="Api/PlaceChatsController@ajaxGetPlaceUsersToChat",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="place_id",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return ApiResponse
     */
    public function ajaxGetPlaceUsersToChat(Request $request, TripInvitationsService $tripInvitationsService, PlaceChatsService $placeChatsService)
    {
        try {
            $tripId = $request->trip_id;
            $placeId = $request->place_id;

            $trip = TripPlans::find($tripId);
            if (!$trip) return ApiResponse::__createBadResponse('trip plan not found.');

            $result = $placeChatsService->getPlaceUsersToChat($tripId, $placeId);

            foreach ($result as &$group) {
                foreach ($group as &$user) {
                    $user['is_plan_member'] = $tripInvitationsService->isPlanMember($tripId, $user['id']);
                    $user['is_expert'] = ($user['is_expert']) ? true : false;
                }
            }

            $result['available_today'] = isset($result['available today']) ? $result['available today'] : [];
            $result['followers'] = isset($result['followers']) ? $result['followers'] : [];
            $result['friends'] = isset($result['friends']) ? $result['friends'] : [];
            unset($result['available today']);

            return ApiResponse::create($result);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return \Illuminate\Http\Response
     */
    public function ajaxGetPlaceChatUsers(Request $request, PlaceChatsService $placeChatsService)
    {
        $chatId = $request->chat_id;
        $conversation = ChatConversation::find($chatId);

        if (!$conversation) {
            return ApiResponse::__createBadResponse('chat not found');
        }

        return ApiResponse::create(
            $placeChatsService->getPlaceChatParticipants($chatId)
        );
    }


    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return \Illuminate\Http\Response
     */
    public function ajaxGetPlanUsersToChat(Request $request, PlaceChatsService $placeChatsService)
    {
        $planId = $request->get('plan_id');

        if (!$planId) {
            return ApiResponse::create([], false);
        }

        return ApiResponse::create($placeChatsService->getPlanUsersToChat($planId));
    }

    /**
     * @param Request $request
     * @param PlaceChatsService $placeChatsService
     * @return \Illuminate\Http\Response
     */
    public function ajaxGetPlanPlaceUsersToChat(Request $request, PlaceChatsService $placeChatsService)
    {
        $planId = $request->get('plan_id');

        if (!$planId) {
            return ApiResponse::create([], false);
        }

        return ApiResponse::create(
            $placeChatsService->getPlanPlaceUsersToChat($planId)
        );
    }

    public function ajaxGetActualChats(Request $request, PlaceChatsService $placeChatsService)
    {
        $planId = $request->get('plan_id');

        if (!$planId) {
            return ApiResponse::create([], false);
        }

        return ApiResponse::create(
            $placeChatsService->renderActualChats($planId, true)
        );
    }
}
