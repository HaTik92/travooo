<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Post\CommentCreateRequest;
use App\Http\Requests\Api\Post\ShareAnyPostRequest;
use App\Http\Requests\Api\Post\SpamPostRequest;
use App\Http\Requests\Api\PrimaryPost\ReportRequest;
use App\Http\Requests\Api\PrimaryPostLikeUnlikeRequest;
use App\Http\Requests\Api\Trip\ChangePrivacyRequest;
use App\Http\Responses\ApiResponse;
use App\Models\ActivityLog\ActivityLog;
use App\Models\ActivityMedia\Media;
use App\Models\ActivityMedia\MediasComments;
use App\Models\ActivityMedia\MediasCommentsLikes;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Events\EventsCommentsLikes;
use App\Models\Events\EventsCommentsMedias;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCommentsLikes;
use App\Models\Posts\PostsShares;
use App\Models\Posts\PostsTags;
use App\Models\Posts\SpamsPosts;
use App\Models\Reports\ReportsCommentsLikes;
use App\Models\Reports\ReportsCommentsMedias;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlaces\TripPlacesComments;
use App\Models\TripPlaces\TripPlacesCommentsLikes;
use App\Models\TripPlans\TripContentPostLike;
use App\Models\TripPlans\TripsCommentsLikes;
use App\Models\TripPlans\TripsCommentsMedias;
use App\Models\TripPlans\TripsMediasShares;
use App\Models\TripPlans\TripsPlacesShares;
use App\Models\TripPlans\TripsShares;
use App\Services\Api\CommentService;
use App\Services\Api\PrimaryPostService;
use App\Services\Api\ShareService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use SpamsComments;
use App\Http\Constants\CommonConst;

class PrimaryPostController extends Controller
{

    function __construct()
    {
        if (in_array(request()->route()->uri, optionalAuthRoutes(self::class))) {
            if (request()->bearerToken()) {
                $this->middleware('auth:api');
            }
        }
    }
    /**
     * @OA\POST(
     ** path="/{type}/{post_id}/report",
     *   tags={"Primary Post [ post, trip, event, report, review, discussion ]"},
     *   summary="report on post, trip, profile type",
     *   operationId="Api\PrimaryPostController@report",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="type",
     *        description="Post Type.: post, trip",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="post_id",
     *        description="Post id Based On Type trip_id, post_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="report_type",
     *        description="report_type",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="text",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="trip_place_id",
     *        description="pas trip_place_id if you want to report on trip place",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @return \Illuminate\Http\Response
     */
    public function report(Request $request, PrimaryPostService $primaryPostService, $type, $post_id)
    {
        try {
            $type = strtolower(trim($type));
            $_types = array_merge(PrimaryPostService::REPORT_MAPPING, ['profile']);
            if (!in_array($type, $_types)) {
                return ApiResponse::__createBadResponse(
                    "type is invalid. type must be in list of " . implode(", ", $_types) . "."
                );
            }
            if ($request->has('report_type')) {
                if (!in_array($request->report_type, array_column(Posts::reportType(), 'type'))) {
                    return ApiResponse::__createBadResponse(
                        "report_type is invalid. type must be in list of " . implode(", ", array_column(Posts::reportType(), 'type')) . "."
                    );
                }
                // Checking Existing Post
                $post = $primaryPostService->find($type, $post_id);
                if (!$post) return ApiResponse::__createBadResponse("Post not found");

                if ($request->trip_place_id) {
                    $tripPlaces = TripPlaces::where('id', $request->trip_place_id)->where('trips_id', $post_id)->first();
                    if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');
                }

                $user = Auth::user();
                $spamsPost = new SpamsPosts();
                if ($request->trip_place_id) {
                    $spamsPost->posts_id = $tripPlaces->id;
                    $spamsPost->post_type = "tripplace";
                } else {
                    $spamsPost->posts_id = $post_id;
                    $spamsPost->post_type = $type;
                }
                $spamsPost->users_id = $user->id;
                $spamsPost->report_type = $request->report_type;
                $spamsPost->report_text = $request->text ?? '';
                $spamsPost->save();

                return ApiResponse::create([
                    'message' => ["Thanks for your report."],
                    'data'    => $spamsPost
                ]);
            } else {
                return ApiResponse::__createBadResponse("report type must be required");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/{type}/{post_id}/like-unlike",
     *   tags={"Primary Post [ post, trip, event, report, review, discussion ]"},
     *   summary="This Api used to like or unlike of any primary post",
     *   operationId="Api\PrimaryPostController@postLike",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="type",
     *        description="Post Type. Like: post, trip, event etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="post_id",
     *        description="Post id Based On Type. Like trip_id, event_id, post_id etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="trip_place_id",
     *        description="pass trip_place_id if you want to like or unlike of trip places",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="trip_place_media_id",
     *        description="pass trip_place_media_id if you want to like or unlike of trip places media",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Post Like Section || Post Like Section || Post Like Section || Post Like Section || Post Like Section
     */
    /**
     * @param Request $request
     * @param PrimaryPostService $primaryPostService
     * @param string $type
     * @param int $id
     * @return App\Http\Responses\ApiResponse
     */
    public function postLike(Request $request, PrimaryPostService $primaryPostService, $type, $post_id)
    {
        try {
            if (is_string($type)) $type = trim(strtolower($type));
            if ($type == "media") {
                return ApiResponse::create(
                    $primaryPostService->likeOrUnlikeOfMedia($post_id)
                );
            } else {
                $allService = array_merge(PrimaryPostService::LIKE_TYPE_MAPPING, ShareService::TYPE_MAPPING_TRIP_SHARE);
                if (!in_array($type, $allService)) {
                    return ApiResponse::__createBadResponse(
                        "type is invalid. type must be in list of " . implode(", ", $allService) . "."
                    );
                }

                // trip shared post like, trip place shared post like, trip place media shared post like
                if (in_array($type, ShareService::TYPE_MAPPING_TRIP_SHARE)) {
                    return ApiResponse::create(
                        $primaryPostService->likeOrUnlikeOfTripsSharing($type, $post_id, $request)
                    );
                } else {
                    // trip place media like
                    if ($request->trip_place_media_id && $request->trip_place_id) {
                        return ApiResponse::create(
                            $primaryPostService->likeOrUnlikeOfTripPlaceMedia($type, $post_id, $request)
                        );
                    } else {
                        // trip place like
                        if ($request->trip_place_id) {
                            return ApiResponse::create(
                                $primaryPostService->likeOrUnlikeOfTripPlace($post_id, $request->trip_place_id)
                            );
                        } else {
                            // trip, event, post, review, report like
                            return ApiResponse::create(
                                $primaryPostService->likeOrUnlike($type, $post_id, $request)
                            );
                        }
                    }
                }
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\GET(
     ** path="/{type}/{post_id}/likes",
     *   tags={"Primary Post [ post, trip, event, report, review, discussion ]"},
     *   summary="This Api used to fetch all user like of any primary post",
     *   operationId="Api\PrimaryPostController@getUserByLikes",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="type",
     *        description="Post Type. Like: post, trip, event etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="post_id",
     *        description="Post id Based On Type. Like trip_id, event_id, post_id etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * [Get All Like Of Any Primary Post Based On Post Type & ID]
     * @param Request $request
     * @param PrimaryPostService $primaryPostService
     * @param string $type
     * @param int $id
     * @return App\Http\Responses\ApiResponse
     */
    public function getUserByLikes(Request $request, PrimaryPostService $primaryPostService, $type, $post_id)
    {
        try {
            $allService = array_merge(PrimaryPostService::LIKE_TYPE_MAPPING, ShareService::TYPE_MAPPING_TRIP_SHARE);
            if (!in_array($type, $allService)) {
                return ApiResponse::__createBadResponse(
                    "type is invalid. type must be in list of " . implode(", ", $allService) . "."
                );
            }
            $perPage = $request->per_page ?? PrimaryPostService::DEFAULT_PER_PAGE;
            if (in_array($type, ShareService::TYPE_MAPPING_TRIP_SHARE)) {
                $result =  TripContentPostLike::with(['user'])
                    ->where('posts_id', $post_id)
                    ->where('posts_type', $type)
                    ->orderBy('created_at', 'DESC')
                    ->paginate($perPage);
            } else {
                $result = $primaryPostService->loadMoreLikes($type, $post_id, $perPage);
            }
            if ($result) {
                foreach ($result as $like) {
                    if ($like->user) {
                        $friendStatus = app(\App\Http\Controllers\Api\HomeController::class)->findFriendFlag($like->user->id);
                        $like->follower = $friendStatus['follower'];
                        $like->following = $friendStatus['following'];
                        $like->user->profile_picture = check_profile_picture($like->user->profile_picture);
                    }
                }
            }
            return ApiResponse::create($result);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * Comment Section || Comment Section || Comment Section || Comment Section || Comment Section
     */

    /**
     * @OA\POST(
     ** path="/{type}/{post_id}/comment",
     *   tags={"Primary Post [ post, trip, event, report, review, discussion ]"},
     *   summary="This Api used to create comment and reply of any primary post",
     *   operationId="Api\PrimaryPostController@postComment",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\RequestBody(
     *        @OA\MediaType(
     *        mediaType="application/json",
     *        @OA\Schema(
     *            example={
     *      "tags": {
     *              { "type": "city", "id": "1256"  },
     *              { "type": "user", "id": "1942"  },
     *              { "type": "place", "id": "4974339"  }
     *      }
     * },
     *        )
     *     )
     * ),
     *   @OA\Parameter(
     *        name="type",
     *        description="Post Type. Like: post, trip, event | [plan_shared, plan_step_shared, plan_media_shared] etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="post_id",
     *        description="Post id Based On Type. Like trip_id, event_id, post_id etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="parents_id",
     *        description="If you want to create reply then pass parent comment id in this field",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="text",
     *        description="Description of comment",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="pair",
     *        description="Pass unique ganrated id",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="trip_place_id",
     *        description="pass trip_place_id if you want to create comment or reply of trip places",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="trip_place_media_id",
     *        description="pass trip_place_media_id if you want to create comment or reply of trip places media",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * [Create Comment Of Any Primary Post Based On Post Type & ID]
     * @param CommentCreateRequest $request
     * @param PrimaryPostService $primaryPostService
     * @param CommentService $commentService
     * @param string $slug
     * @param int $id
     * @return App\Http\Responses\ApiResponse
     */
    public function postComment(
        CommentCreateRequest $request,
        PrimaryPostService $primaryPostService,
        CommentService $commentService,
        $type,
        $post_id
    ) {
        try {
            if (is_string($type)) $type = strtolower(trim($type));
            if (in_array($type, ShareService::TYPE_MAPPING_TRIP_SHARE)) {
                $result = $commentService->createCommentOrReply($request, $type, $post_id);
                return ApiResponse::create($result);
            } else {
                if ($type == "media") {
                    return ApiResponse::create($commentService->createCommentOrReplyOMedia($request, $post_id));
                } else {
                    // Check Type
                    if (!in_array($type, PrimaryPostService::TYPE_MAPPING)) {
                        return ApiResponse::__createBadResponse(
                            "type is invalid. type must be in list of " . implode(", ", PrimaryPostService::TYPE_MAPPING) . "."
                        );
                    }

                    if (!in_array($type, [
                        PrimaryPostService::TYPE_POST,
                        PrimaryPostService::TYPE_DISCUSSION,
                        PrimaryPostService::TYPE_TRIP,
                        PrimaryPostService::TYPE_REPORT,
                        PrimaryPostService::TYPE_EVENT,
                    ])) {
                        throw new ModelNotFoundException('Comment service not available for ' . $type . ' type');
                    }

                    if ($request->trip_place_media_id && $request->trip_place_id) {
                        $result = $commentService->createCommentOrReplyOfTripPlaceMedia($request, $type, $post_id);
                    } else {
                        if ($request->trip_place_id) {
                            $result = $commentService->createCommentOrReplyOfTripPlace($request, $type, $post_id);
                        } else {
                            $result = $commentService->createCommentOrReply($request, $type, $post_id);
                        }
                    }
                    return ApiResponse::create($result);
                }
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\DELETE(
     ** path="/{type}/{post_id}/comment/{comment_id}",
     *   tags={"Primary Post [ post, trip, event, report, review, discussion ]"},
     *   summary="This Api used to create comment and reply of any primary post",
     *   operationId="Api\PrimaryPostController@deleteComment",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="type",
     *        description="Post Type. Like: post, trip, event | [plan_shared, plan_step_shared, plan_media_shared] etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="post_id",
     *        description="Post id Based On Type. Like trip_id, event_id, post_id etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="comment_id",
     *        description="comment_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="trip_place_id",
     *        description="pass trip_place_id if you want to delete comment or reply of trip places",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="trip_place_media_id",
     *        description="pass trip_place_media_id if you want to delete comment or reply of trip places media",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * [Create Comment Of Any Primary Post Based On Post Type & ID]
     * @param Request $request
     * @param CommentService $commentService
     * @param string $slug
     * @param int $id
     * @return App\Http\Responses\ApiResponse
     */
    public function deleteComment(
        Request $request,
        CommentService $commentService,
        $type,
        $post_id,
        $comment_id
    ) {
        try {
            if (is_string($type)) $type = strtolower(trim($type));

            if (in_array($type, ShareService::TYPE_MAPPING_TRIP_SHARE)) {
                $result = $commentService->deleteCommentOrReply($request, $type, $post_id, $comment_id);
                return ApiResponse::create($result);
            } else {
                if ($type == "media") {
                    return ApiResponse::create($commentService->deleteCommentOrReplyOMedia($request, $post_id, $comment_id));
                } else {
                    // Check Type
                    if (!in_array($type, PrimaryPostService::TYPE_MAPPING)) {
                        return ApiResponse::__createBadResponse(
                            "type is invalid. type must be in list of " . implode(", ", PrimaryPostService::TYPE_MAPPING) . "."
                        );
                    }

                    if (!in_array($type, [
                        PrimaryPostService::TYPE_POST,
                        PrimaryPostService::TYPE_DISCUSSION,
                        PrimaryPostService::TYPE_TRIP,
                        PrimaryPostService::TYPE_REPORT,
                        PrimaryPostService::TYPE_EVENT,
                    ])) {
                        throw new ModelNotFoundException('Comment service not available for ' . $type . ' type');
                    }

                    if ($request->trip_place_media_id && $request->trip_place_id) {
                        $result = $commentService->deleteCommentOrReplyOfTripPlaceMedia($request, $type, $post_id, $comment_id);
                    } else {
                        if ($request->trip_place_id) {
                            $result = $commentService->deleteCommentOrReplyOfTripPlace($request, $type, $post_id, $comment_id);
                        } else {
                            $result = $commentService->deleteCommentOrReply($type, $post_id, $comment_id);
                        }
                    }
                    return ApiResponse::create($result);
                }
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }




    /**
     * @OA\GET(
     ** path="/{type}/{post_id}/comment",
     *   tags={"Primary Post [ post, trip, event, report, review, discussion ]"},
     *   summary="This Api used to fetch comment in pagination format.",
     *   operationId="Api\PrimaryPostController@loadMoreComments",
     *   @OA\Parameter(
     *        name="type",
     *        description="Post Type. Like: post, trip, event etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="post_id",
     *        description="Post id Based On Type. Like trip_id, event_id, post_id etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="trip_place_id",
     *        description="pass trip_place_id if you want to get comment of trip places",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="trip_place_media_id",
     *        description="pass trip_place_media_id if you want to get comment of trip places media",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="page",
     *        description="page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * * @OA\Parameter(
     *        name="filter",
     *        description="1=new,2=top for other | for discussion 1=new,2=top,3=old",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function loadMoreComments(
        Request $request,
        PrimaryPostService $primaryPostService,
        CommentService $commentService,
        $type,
        $post_id
    ) {
        try {
            $user       = Auth::user();
            $filter     = $request->filter      ?? CommentService::FILTER_NEW;
            $perPage    = $request->per_page    ?? PrimaryPostService::DEFAULT_PER_PAGE;

            if (is_string($type)) $type = strtolower(trim($type));

            if ($type == "media") {
                $post = Media::where('id', $post_id)->first();
                if (!$post) throw new ModelNotFoundException('media not found');

                $post_comment = MediasComments::select('*')
                    // ->addSelect(DB::raw('fun_basic_algo(medias_id, "medias") as total'))
                    ->addSelect(DB::raw('(0) as total'))
                    ->where('medias_id', $post->id)
                    ->where('reply_to', 0);

                $post_comment = ($filter == CommentService::FILTER_TOP) ? $post_comment->orderBy('total', 'desc') : $post_comment->orderBy('created_at', 'desc');
                $post_comment = $post_comment->paginate($perPage);
                $post_comment = modifyPagination($post_comment);

                if ($post_comment->data) {
                    foreach ($post_comment->data as $parent_comment) {

                        // For Parent Comment
                        $parent_comment->text = isset($parent_comment->tags) ? convert_post_text_to_tags(strip_tags($parent_comment->comment), $parent_comment->tags, false) : strip_tags($parent_comment->comment);
                        unset($parent_comment->tags, $parent_comment->comment);

                        $parent_comment->like_status = $user ? ($parent_comment->likes->where('users_id', $user->id)->first() ? true : false) : false;
                        $parent_comment->total_likes = $parent_comment->likes->count();
                        if ($parent_comment->medias) {
                            foreach ($parent_comment->medias as $media) {
                                $media->media;
                            }
                        }
                        if ($parent_comment->author) {
                            $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                        }
                        unset($parent_comment->likes);

                        // For Sub Comment
                        if ($parent_comment->sub) {
                            foreach ($parent_comment->sub as $sub_comment) {

                                $sub_comment->text = isset($sub_comment->tags) ? convert_post_text_to_tags(strip_tags($sub_comment->comment), $sub_comment->tags, false) : strip_tags($sub_comment->comment);
                                unset($sub_comment->tags, $sub_comment->comment);

                                $sub_comment->like_status = $user ? ($parent_comment->likes->where('users_id', $user->id)->first() ? true : false) : false;
                                $sub_comment->total_likes = $sub_comment->likes->count();
                                if ($sub_comment->medias) {
                                    foreach ($sub_comment->medias as $media) {
                                        $media->media;
                                    }
                                }
                                if ($sub_comment->author) {
                                    $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                                }
                                unset($sub_comment->likes);
                            }
                        }
                    }
                }
                return ApiResponse::create($post_comment);
            } else {
                // Check Type
                $allService = array_merge(PrimaryPostService::TYPE_MAPPING, ShareService::TYPE_MAPPING_TRIP_SHARE);
                if (!in_array($type, $allService)) {
                    return ApiResponse::__createBadResponse(
                        "type is invalid. type must be in list of " . implode(", ", $allService) . "."
                    );
                }

                if (!in_array($type, [
                    PrimaryPostService::TYPE_POST,
                    PrimaryPostService::TYPE_TRIP,
                    PrimaryPostService::TYPE_REPORT,
                    PrimaryPostService::TYPE_EVENT,
                    PrimaryPostService::TYPE_DISCUSSION,
                    ShareService::TYPE_TRIP_SHARE,
                    ShareService::TYPE_TRIP_PLACE_SHARE,
                    ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE,
                ])) {
                    throw new ModelNotFoundException('Comment service not available for ' . $type . ' type');
                }

                // Checking Existing Post
                $post = $primaryPostService->find($type, $post_id);
                if (!$post) return ApiResponse::__createBadResponse("Post not found");

                if ($type == PrimaryPostService::TYPE_DISCUSSION) {
                    //1=new,2=top,3=old
                    $post_comment = DiscussionReplies::select('*')
                        // ->addSelect(DB::raw('fun_basic_algo(id, "discussion") as total'))
                        ->addSelect(DB::raw('(0) as total'))
                        ->where('discussions_id', $post_id)
                        ->where('parents_id', 0);
                    $post_comment = ($filter == CommentService::FILTER_TOP) ? $post_comment->orderBy('total', 'desc') : ($post_comment->orderBy('created_at', (($filter == CommentService::FILTER_NEW) ? 'desc' : 'ASC')));
                    $post_comment = $post_comment->paginate($perPage);
                    $post_comment = modifyPagination($post_comment);

                    if ($post_comment->data) {
                        foreach ($post_comment->data as $parent_comment) {
                            // For Parent Comment
                            $parent_comment->text = isset($parent_comment->tags) ? convert_post_text_to_tags(strip_tags($parent_comment->reply), $parent_comment->tags, false) : strip_tags($parent_comment->text);

                            $parent_comment->upvote_flag = $user ? ($parent_comment->upvotes->where('users_id', $user->id)->first() ? true : false) : false;
                            $parent_comment->downvotes_flag = $user ? ($parent_comment->downvotes->where('users_id', $user->id)->first() ? true : false) : false;
                            $parent_comment->total_upvotes = $parent_comment->upvotes->count();
                            $parent_comment->total_downvotes = $parent_comment->downvotes->count();

                            if ($parent_comment->author) {
                                $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                            }
                            unset($parent_comment->tags, $parent_comment->reply, $parent_comment->upvotes, $parent_comment->downvotes);

                            // For Sub Comment
                            if ($parent_comment->sub) {
                                foreach ($parent_comment->sub as $sub_comment) {
                                    $sub_comment->text = isset($sub_comment->tags) ? convert_post_text_to_tags(strip_tags($sub_comment->reply), $sub_comment->tags, false) : strip_tags($sub_comment->text);

                                    $sub_comment->upvote_flag = $user ? ($sub_comment->upvotes->where('users_id', $user->id)->first() ? true : false) : false;
                                    $sub_comment->downvotes_flag = $user ? ($sub_comment->downvotes->where('users_id', $user->id)->first() ? true : false) : false;
                                    $sub_comment->total_upvotes = $sub_comment->upvotes->count();
                                    $sub_comment->total_downvotes = $sub_comment->downvotes->count();

                                    if ($sub_comment->author) {
                                        $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                                    }
                                    unset($sub_comment->tags, $sub_comment->reply, $sub_comment->upvotes, $sub_comment->downvotes);
                                }
                            }
                        }
                    }

                    return ApiResponse::create($post_comment);
                } else {
                    if ($request->trip_place_media_id && $request->trip_place_id) {
                        $tripPlaces = TripPlaces::where('id', $request->trip_place_id)->where('trips_id', $post_id)->first();
                        if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');


                        $tripMedia = TripMedias::where('id', $request->trip_place_media_id)
                            ->where('trips_id', $post->id)
                            ->where('trip_place_id', $request->trip_place_id)->first();
                        if (!$tripMedia) throw new ModelNotFoundException('Trip Place media not found');

                        $post_comment = MediasComments::select('*')
                            // ->addSelect(DB::raw('fun_basic_algo(medias_id, "trip_place_media") as total'))
                            ->addSelect(DB::raw('(0) as total'))
                            ->where('medias_id', $tripMedia->media->id)
                            ->where('reply_to', 0);

                        $post_comment = ($filter == CommentService::FILTER_TOP) ? $post_comment->orderBy('total', 'desc') : $post_comment->orderBy('created_at', 'desc');
                        $post_comment = $post_comment->paginate($perPage);
                        $post_comment = modifyPagination($post_comment);
                        if ($post_comment->data) {
                            foreach ($post_comment->data as $parent_comment) {

                                // For Parent Comment
                                $parent_comment->text = isset($parent_comment->tags) ? convert_post_text_to_tags(strip_tags($parent_comment->comment), $parent_comment->tags, false) : strip_tags($parent_comment->comment);
                                unset($parent_comment->tags, $parent_comment->comment);

                                $parent_comment->like_status = $user ? (MediasCommentsLikes::where('medias_comments_id', $parent_comment->id)->where('users_id', $user->id)->first() ? true : false) : false;
                                $parent_comment->total_likes = MediasCommentsLikes::where('medias_comments_id', $parent_comment->id)->count();
                                if ($parent_comment->medias) {
                                    foreach ($parent_comment->medias as $media) {
                                        $media->media;
                                    }
                                }
                                if ($parent_comment->author) {
                                    $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                                }
                                unset($parent_comment->likes);

                                // For Sub Comment
                                if ($parent_comment->sub) {
                                    foreach ($parent_comment->sub as $sub_comment) {
                                        $sub_comment->text = isset($sub_comment->tags) ? convert_post_text_to_tags(strip_tags($sub_comment->comment), $sub_comment->tags, false) : strip_tags($sub_comment->comment);
                                        unset($sub_comment->tags, $sub_comment->comment);

                                        $sub_comment->like_status =  $user ? (MediasCommentsLikes::where('medias_comments_id', $sub_comment->id)->where('users_id', $user->id)->first() ? true : false) : false;
                                        $sub_comment->total_likes = MediasCommentsLikes::where('medias_comments_id', $sub_comment->id)->count();
                                        if ($sub_comment->medias) {
                                            foreach ($sub_comment->medias as $media) {
                                                $media->media;
                                            }
                                        }
                                        if ($sub_comment->author) {
                                            $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                                        }
                                        unset($sub_comment->likes);
                                    }
                                }
                            }
                        }

                        return ApiResponse::create($post_comment);
                    } else {
                        if ($request->trip_place_id) {
                            $tripPlaces = TripPlaces::where('id', $request->trip_place_id)->where('trips_id', $post_id)->first();
                            if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');

                            $post_comment = TripPlacesComments::select('*')
                                // ->addSelect(DB::raw('fun_basic_algo(trip_place_id, "trip_place") as total'))
                                ->addSelect(DB::raw('(0) as total'))
                                ->where('trip_place_id', $tripPlaces->id)
                                ->where('reply_to', 0);

                            $post_comment = ($filter == CommentService::FILTER_TOP) ? $post_comment->orderBy('total', 'desc') : $post_comment->orderBy('created_at', 'desc');
                            $post_comment = $post_comment->paginate($perPage);
                            $post_comment = modifyPagination($post_comment);

                            if ($post_comment->data) {
                                foreach ($post_comment->data as $parent_comment) {

                                    // For Parent Comment
                                    $parent_comment->text = isset($parent_comment->tags) ? convert_post_text_to_tags(strip_tags($parent_comment->comment), $parent_comment->tags, false) : strip_tags($parent_comment->comment);
                                    unset($parent_comment->tags, $parent_comment->comment);

                                    $parent_comment->like_status = $user ? (TripPlacesCommentsLikes::where('trip_place_comment_id', $parent_comment->id)->where('user_id', $user->id)->first() ? true : false) : false;
                                    $parent_comment->total_likes = $parent_comment->likes->count();
                                    if ($parent_comment->medias) {
                                        foreach ($parent_comment->medias as $media) {
                                            $media->media;
                                        }
                                    }
                                    $parent_comment->author = $parent_comment->user;
                                    unset($parent_comment->user);
                                    if ($parent_comment->author) {
                                        $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                                    }
                                    unset($parent_comment->likes);

                                    // For Sub Comment
                                    if ($parent_comment->sub) {
                                        foreach ($parent_comment->sub as $sub_comment) {
                                            $sub_comment->text = isset($sub_comment->tags) ? convert_post_text_to_tags(strip_tags($sub_comment->comment), $sub_comment->tags, false) : strip_tags($sub_comment->comment);
                                            unset($sub_comment->tags, $sub_comment->comment);

                                            $sub_comment->like_status = $user ? (TripPlacesCommentsLikes::where('trip_place_comment_id', $sub_comment->id)->where('user_id', $user->id)->first() ? true : false) : false;
                                            $sub_comment->total_likes = $sub_comment->likes->count();
                                            if ($sub_comment->medias) {
                                                foreach ($sub_comment->medias as $media) {
                                                    $media->media;
                                                }
                                            }
                                            $sub_comment->author = $sub_comment->user;
                                            unset($sub_comment->user);
                                            if ($sub_comment->author) {
                                                $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                                            }
                                            unset($sub_comment->likes);
                                        }
                                    }
                                }
                            }

                            return ApiResponse::create($post_comment);
                        } else {
                            $post_comment = $commentService->loadMore($type, $post_id, $perPage, $filter);
                            $post_comment = modifyPagination($post_comment);

                            switch ($type) {
                                case PrimaryPostService::TYPE_POST:
                                case ShareService::TYPE_TRIP_SHARE:
                                case ShareService::TYPE_TRIP_PLACE_SHARE:
                                case ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE:
                                case PrimaryPostService::TYPE_TRIP:
                                case PrimaryPostService::TYPE_REPORT:
                                case PrimaryPostService::TYPE_EVENT:
                                    if ($post_comment->data) {
                                        foreach ($post_comment->data as $parent_comment) {

                                            // For Parent Comment
                                            $parent_comment->text = isset($parent_comment->tags) ? convert_post_text_to_tags(strip_tags($parent_comment->text), $parent_comment->tags, false) : strip_tags($parent_comment->text);
                                            unset($parent_comment->tags);

                                            $parent_comment->like_status = $user ? ($parent_comment->likes->where('users_id', $user->id)->first() ? true : false) : false;
                                            $parent_comment->total_likes = $parent_comment->likes->count();
                                            if ($parent_comment->medias) {
                                                foreach ($parent_comment->medias as $media) {
                                                    $media->media;
                                                }
                                            }
                                            if ($parent_comment->author) {
                                                $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                                            }
                                            unset($parent_comment->likes);

                                            // For Sub Comment
                                            if ($parent_comment->sub) {
                                                foreach ($parent_comment->sub as $sub_comment) {

                                                    $sub_comment->text = isset($sub_comment->tags) ? convert_post_text_to_tags(strip_tags($sub_comment->text), $sub_comment->tags, false) : strip_tags($sub_comment->text);
                                                    unset($sub_comment->tags);

                                                    $sub_comment->like_status = $user ? ($parent_comment->likes->where('users_id', $user->id)->first() ? true : false) : false;
                                                    $sub_comment->total_likes = $sub_comment->likes->count();
                                                    if ($sub_comment->medias) {
                                                        foreach ($sub_comment->medias as $media) {
                                                            $media->media;
                                                        }
                                                    }
                                                    if ($sub_comment->author) {
                                                        $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                                                    }
                                                    unset($sub_comment->likes);
                                                }
                                            }
                                        }
                                    }
                                    break;
                            }

                            return ApiResponse::create($post_comment);
                        }
                    }
                }
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * Comment Like Section || Comment Like Section || Comment Like Section || Comment Like Section || Comment Like Section
     */



    /**
     * @OA\POST(
     ** path="/{type}/{post_id}/comment/{comment_id}/like-unlike",
     *   tags={"Primary Post [ post, trip, event, report, review, discussion ]"},
     *   summary="This Api used to like or unlike any pimary post comment.",
     *   operationId="Api\PrimaryPostController@postCommetLike",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="type",
     *        description="Post Type. Like: post, trip, event etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="post_id",
     *        description="Post id Based On Type. Like trip_id, event_id, post_id etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="comment_id",
     *        description="comment id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="trip_place_id",
     *        description="pass trip_place_id if you want to like or unlike of trip places comment",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="trip_place_media_id",
     *        description="pass trip_place_media_id if you want to like or unlike of trip places media comment",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @param PrimaryPostService $primaryPostService
     * @param string $type
     * @param int $id
     * @return App\Http\Responses\ApiResponse
     */
    public function postCommetLike(Request $request, CommentService $commentService, $type, $post_id, $comment_id)
    {
        try {
            if (is_string($type)) $type = strtolower(trim($type));

            if ($type == "media") {
                return ApiResponse::create(
                    $commentService->likeOrUnlikeOfMedia($post_id, $comment_id)
                );
            } else {
                if (!in_array($type, PrimaryPostService::LIKE_TYPE_MAPPING)) {
                    return ApiResponse::__createBadResponse(
                        "type is invalid. type must be in list of " . implode(", ", PrimaryPostService::LIKE_TYPE_MAPPING) . "."
                    );
                }

                if (!in_array($type, [
                    PrimaryPostService::TYPE_POST,
                    PrimaryPostService::TYPE_TRIP,
                    PrimaryPostService::TYPE_REPORT,
                    PrimaryPostService::TYPE_EVENT
                ])) {
                    throw new ModelNotFoundException('like service not available for ' . $type . ' type');
                }

                if ($request->trip_place_media_id && $request->trip_place_id) {
                    return ApiResponse::create(
                        $commentService->likeOrUnlikeOfTripPlaceMedia($type, $post_id, $comment_id, $request)
                    );
                } else {
                    if ($request->trip_place_id) {
                        return ApiResponse::create(
                            $commentService->likeOrUnlikeOfTripPlace($type, $post_id, $comment_id, $request)
                        );
                    } else {
                        return ApiResponse::create(
                            $commentService->likeOrUnlike($type, $post_id, $comment_id)
                        );
                    }
                }
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * Privacy Section || Privacy Section || Privacy Section || Privacy Section || Privacy Section
     */
    /**
     * @OA\POST(
     ** path="/{type}/{post_id}/privacy",
     *   tags={"Primary Post [ post, trip, event, report, review, discussion ]"},
     *   summary="This Api used to change privacy of any post.",
     *   operationId="Api\PrimaryPostController@changePrivacy",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="type",
     *        description="Post Type. Like: post, trip, event etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="post_id",
     *        description="Post id Based On Type. Like trip_id, event_id, post_id etc..",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="privacy",
     *        description="privacy must be in 0= public, 1 = friends only, 2 = private",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @param PrimaryPostService $primaryPostService
     * @param string $type
     * @param int $id
     * @return App\Http\Responses\ApiResponse
     */
    public function changePrivacy(ChangePrivacyRequest $request, PrimaryPostService $primaryPostService, $type, $post_id)
    {
        try {
            if (!in_array($type, PrimaryPostService::PRIVACY_MAPPING)) {
                return ApiResponse::__createBadResponse(
                    "type is invalid. type must be in list of " . implode(", ", PrimaryPostService::PRIVACY_MAPPING) . "."
                );
            }
            if ($primaryPostService->chnagePrivacy($type, $post_id, $request->privacy)) {
                return ApiResponse::__create('Privacy change successfully.');
            } else {
                return ApiResponse::__createBadResponse('Privacy can not change.');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * Share Section || Share Section || Share Section || Share Section || Share Section
     */
    /**
     * @OA\POST(
     ** path="/share",
     *   tags={"Primary Post [ post, trip, event, report, review, discussion ]"},
     *   summary="permission[default = 0 | 0 = public, 1= Friends Only, 2 = private]",
     *   operationId="Api\PrimaryPostController@sharePost",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *  @OA\RequestBody(
     *        @OA\MediaType(
     *        mediaType="application/json",
     *        @OA\Schema(
     *            example={
     *      "type":"post",
     *      "id":45748,
     *      "text": "I am going to {{city_1256}}. I will create trip for {{user_1942}}. We will go to {{place_4974339}}", 
     *      "permission": 0,
     *      "tags": {
     *              { "type": "city", "id": "1256"  },
     *              { "type": "user", "id": "1942"  },
     *              { "type": "place", "id": "4974339"  }
     *      },
     *      "trip_place_id":1,
     *      "trip_place_media_id":1
     * },
     *        )
     *     )
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function sharePost(ShareAnyPostRequest $request, ShareService $shareService)
    {
        try {
            $authUser   = auth()->user();
            $type       = trim(strtolower($request->type ?? ''));
            $id         = $request->id ?? null;
            $tags       = $request->input("tags", []);
            $permission = $request->permission ?? 0;
            $postText   = processString($request->input("text", ""));

            if (!in_array($permission, [0, 1, 2])) {
                return ApiResponse::createValidationResponse([
                    'permission' => 'permission must be in list of 0, 1, 2'
                ]);
            }

            if (!in_array($type, ShareService::TYPE_MAPPING)) {
                return ApiResponse::__createBadResponse("type is invalid. type must be in list of " . implode(", ", ShareService::TYPE_MAPPING) . ".");
            }
            $relatedToTrip = [];
            if ($type == ShareService::TYPE_TRIP) {
                if ($request->trip_place_media_id) {
                    $existsOrNot = TripMedias::find($request->trip_place_media_id);
                    if (!$existsOrNot) return ApiResponse::__createBadResponse("trip place media not found");

                    $alreadyExists = TripsMediasShares::where('user_id', $authUser->id)->where('trip_media_id', $request->trip_place_media_id)->exists();
                    if ($alreadyExists) {
                        return ApiResponse::create([
                            'message' => ['You have already shared!'],
                            'count' => TripsMediasShares::where('trip_media_id', $request->trip_place_media_id)->count()
                        ]);
                    }

                    DB::beginTransaction();
                    $share                  = new TripsMediasShares;
                    $share->trip_media_id   = $request->trip_place_media_id;
                    $share->user_id         = $authUser->id;
                    $share->comment         = $postText;
                    $share->save();

                    save_tags($tags, PostsTags::TYPE_TRIP_PLACE_MEDIA_SHARE, $share->id);
                    log_user_activity('Share', ShareService::TYPE_TRIP_PLACE_MEDIA_SHARE, $share->id);
                    DB::commit();

                    return ApiResponse::create([
                        'message' => ['Share successfully.'],
                        'count' => TripsMediasShares::where('trip_media_id', $request->trip_place_media_id)->count()
                    ]);
                } else {
                    if ($request->trip_place_id) {
                        $existsOrNot = TripPlaces::find($request->trip_place_id);
                        if (!$existsOrNot) return ApiResponse::__createBadResponse("trip place not found");

                        $alreadyExists = TripsPlacesShares::where('user_id', $authUser->id)->where('trip_place_id', $request->trip_place_id)->exists();
                        if ($alreadyExists) {
                            return ApiResponse::create([
                                'message' => ['You have already shared!'],
                                'count' => TripsPlacesShares::where('trip_place_id', $request->trip_place_id)->count()
                            ]);
                        }

                        DB::beginTransaction();
                        $share                  = new TripsPlacesShares;
                        $share->trip_place_id   = $request->trip_place_id;
                        $share->user_id         = $authUser->id;
                        $share->comment         = $postText;
                        $share->save();

                        save_tags($tags, PostsTags::TYPE_TRIP_PLACE_SHARE, $share->id);
                        log_user_activity('Share', ShareService::TYPE_TRIP_PLACE_SHARE, $share->id);
                        DB::commit();

                        return ApiResponse::create([
                            'message' => ['Share successfully.'],
                            'count' => TripsPlacesShares::where('trip_place_id', $request->trip_place_id)->count()
                        ]);
                    } else {
                        $existsOrNot = $shareService->__check($type, $id);
                        if (!$existsOrNot) return ApiResponse::__createBadResponse("trip place not found");

                        $alreadyExists = TripsShares::where('user_id', $authUser->id)->where('trip_id', $id)->exists();
                        if ($alreadyExists) {
                            return ApiResponse::create([
                                'message' => ['You have already shared!'],
                                'count' => TripsShares::where('trip_id', $id)->count()
                            ]);
                        }

                        DB::beginTransaction();
                        $share                  = new TripsShares;
                        $share->trip_id         = $id;
                        $share->user_id         = $authUser->id;
                        $share->comment         = $postText;
                        $share->save();

                        save_tags($tags, PostsTags::TYPE_TRIP_SHARE, $share->id);
                        log_user_activity('Share', ShareService::TYPE_TRIP_SHARE, $share->id);
                        DB::commit();

                        return ApiResponse::create([
                            'message' => ['Share successfully.'],
                            'count' => TripsShares::where('trip_id', $id)->count()
                        ]);
                    }
                }
            } else {
                if (!$id) return ApiResponse::__createBadResponse("id must be required");

                $existsOrNot = $shareService->__check($type, $id);
                if (!$existsOrNot) return ApiResponse::__createBadResponse($type . " not found");

                $alreadyExists = PostsShares::where('users_id', $authUser->id)->where('type', $type)->where('posts_type_id', $id)->exists();
                if ($alreadyExists) {
                    return ApiResponse::create([
                        'message' => ['You have already shared!'],
                        'count' => PostsShares::where('type', $type)->where('posts_type_id', $id)->count()
                    ]);
                }

                DB::beginTransaction();

                $newPost = Posts::create([
                    'users_id'      => $authUser->id,
                    'text'          => $postText,
                    'permision'     => $permission,
                ]);

                $share                  = new PostsShares;
                $share->posts_id        = $newPost->id;
                $share->users_id        = $authUser->id;
                $share->type            = $type;
                $share->posts_type_id   = $id;
                $share->text            = $postText;
                $share->save();

                save_tags($tags, PostsTags::TYPE_POST, $newPost->id);
                log_user_activity('Share', $type, $share->id);
                DB::commit();

                return ApiResponse::create([
                    'message' => ['Share successfully.'],
                    'count' => PostsShares::where('type', $type)->where('posts_type_id', $id)->count()
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
