<?php

namespace App\Http\Controllers\api;

use App\Http\Requests\Api\Profile\FeedRequest;
use App\Http\Requests\Api\Profile\PaginationRequest;
use App\Http\Requests\Api\Profile\ReviewRequest;
use App\Models\City\CitiesFollowers;
use App\Models\Country\CountriesFollowers;
use App\Models\Place\PlaceFollowers;
use App\Services\Medias\MediaService;
use Carbon\Carbon;
use App\Http\Responses\ApiResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests\Api\Profile\PlansInvitedRequest;
use App\Http\Requests\Api\Profile\PlansUpcomingRequest;
use App\Http\Requests\Api\Profile\PlansMemoryRequest;
use App\Http\Requests\Api\Profile\MyDraftPlansRequest;
use App\Http\Requests\Api\Profile\UploadCoverRequest;
use App\Http\Requests\Api\Profile\UploadProfileImageRequest;
use App\Http\Requests\Api\Profile\MediaCommentsRequest;
use App\Http\Requests\Api\Profile\VisitedCitiesRequest;
use App\Http\Requests\Api\Profile\BadgePointsRequest;
use App\Http\Requests\Api\Profile\TimlineMediasRequest;
use App\Http\Requests\Api\Profile\UdateBioRequest;
use App\Http\Requests\Api\Profile\TripPlanRequest;
use App\Http\Requests\Api\Profile\MediaRequest;
use App\Http\Requests\Api\Profile\FollowUserRequest;
use App\Http\Requests\Api\Profile\ExpertRequest;

use App\Services\Reviews\ReviewsService;
use App\Services\Profile\ProfileService;
use App\Services\Users\ExpertiseService;


use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\Posts\Posts;
use App\Models\Lifestyle\LifestyleTrans;
use App\Models\User\User;
use App\Models\TripPlans\TripPlans;
use App\Models\Country\Countries;
use App\Models\PlacesTop\PlacesTop;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Posts\Checkins;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripsVersions;
use App\Models\User\UsersMedias;
use App\Models\ActivityMedia\Media;
use App\Models\Reviews\Reviews;
use App\Models\UsersFollowers\UsersFollowers;
use App\Models\Events\Events;
use App\Models\Posts\PostsPlaces;
use App\Models\Country\CountriesTranslations;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;

class ProfileController extends Controller
{

    /**
     * @OA\GET(
     ** path="/profile/{user_id}",
     *   tags={"Profile"},
     *   summary="Get info about profile",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="User id",
     *        required=false,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param null|integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function getAbout($userId, ProfileService $profileService)
    {
        $user = User::findOrFail($userId);

        $social = [
            'website',
            'facebook',
            'twitter',
            'instagram',
            'medium',
            'youtube',
            'vimeo',
            'tripadvisor',
            'tumblr',
            'pinterest'
        ];

        $followingUsersCount = UsersFollowers::where('followers_id', $user->id)->count();
        $followingCountriesCount = CountriesFollowers::where('users_id', $user->id)->count();
        $followingCitiesCount = CitiesFollowers::where('users_id', $user->id)->count();
        $followingPlaceCount = PlaceFollowers::where('users_id', $user->id)->count();

        $data = [
            'id' => $user->id,
            'name' => $user->name,
            'username' => $user->username,
            'profile_picture' => check_profile_picture($user->profile_picture),
            'cover' => get_profile_original_picture($user->cover_photo, $user->id, 'cover'),
            'about' => $user->about,
            'links' => $user->only($social),
            'post_count' => $profileService->getPostCount($userId),
            'followers_count' => UsersFollowers::where('users_id', $user->id)->count(),
            'following_count' => [
                'all' => $followingUsersCount + $followingCountriesCount + $followingCitiesCount + $followingPlaceCount,
                'people' => $followingUsersCount,
                'country' => $followingCountriesCount,
                'city' => $followingCitiesCount,
                'place' => $followingPlaceCount
            ],
            'is_expert' => (bool)$user->expert,
            'user_expertise' => $user->expert ? $profileService->getExpertiseList($userId) : NULL,
            'travel_styles' => !$user->travelstyles->count() ? [] : $user->travelstyles()->with('travelstyle.trans')->get()
                ->map(function ($styles) {
                    return $styles->travelstyle->trans[0]->title;
                }),
            'gender' => $user->gender == 1 ? __('setting.male') : ($user->gender == 2 ? __('setting.female') : __('setting.unspecified')),
            'email' => $user->contact_email ?? NULL,
            'mobile' => $user->mobile ?? NULL,
            'nationality' => $user->nationality ? $user->country_of_nationality->transsingle->title : NULL,
            'flag' => $user->nationality ? get_country_flag_by_iso_code($user->country_of_nationality->iso_code) : NULL,
            'birth_date' => $user->birth_date ?? NULL,
            'member_since' => $user->created_at->format('M, Y'),
            'interests' => $user->interests ? str_replace(',', ', ', $user->interests) : NULL,
        ];

        if ($user->id != Auth::user()->id) {
            $data['is_followed'] = UsersFollowers::where('followers_id', Auth::user()->id)->where('users_id', $user->id)->exists();
        }

        return ApiResponse::create($data);
    }

    /**
     * @param null|integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function getTravelHistory($user_id = null)
    {
        try {
            if ($user_id && $user_id != Auth::user()->id) {
                $userId = $user_id;
            } else {
                $userId = Auth::user()->id;
            }

            $user = User::find($userId);

            $data['visited_countries'] = Posts::with('checkin')
                ->where('users_id', $userId)
                ->whereHas('checkin', function ($q) {
                    $q->where('country_id', '!=', 0);
                })
                ->get();

            $data['visited_cities'] = Posts::with('checkin')
                ->where('users_id', $userId)
                ->whereHas('checkin', function ($q) {
                    $q->where('city_id', '!=', 0);
                })
                ->get();

            $data['visited_places'] = Posts::with('checkin')
                ->where('users_id', $userId)
                ->whereHas('checkin', function ($q) {
                    $q->where('place_id', '!=', 0);
                })
                ->get();


            $data['user'] = $user;
            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->get();
            $data['user_id'] = $user_id;


            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/profile/{user_id}/profile-map",
     *   tags={"Profile"},
     *   summary="Get user posts",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="User id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param null|integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function getMap($user_id, ProfileService $profileService)
    {
        return ApiResponse::create($profileService->getVisitedPlaceForMap($user_id));
    }

    /**
     * @OA\GET(
     ** path="/profile/plans-invited",
     *   tags={"Profile"},
     *   summary="Get profile trip plans",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="pagenum",
     *        description="Page num",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * *   @OA\Parameter(
     *        name="sort",
     *        description="Sort type, types = asc | desc | current_date",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param PlansInvitedRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getPlansInvited(PlansInvitedRequest $request)
    {
        try {
            $page = $request->pagenum;
            $skip = ($page - 1) * 10;
            $sort = $request->sort;

            $userId = Auth::user()->id;

            $user = User::find($userId);

            $plans_invited = TripsVersions::leftJoin('trips', 'trips_versions.plans_id', '=', 'trips.id')
                ->leftJoin('trips_contribution_requests', 'trips_contribution_requests.plans_id', '=', 'trips.id')
                ->where('trips_contribution_requests.users_id', $user->id)
                ->where('trips.users_id', '!=', $user->id)
                ->groupBy('trips.id');


            if ($sort == 'creation_date') {
                $plans_invited = $plans_invited->orderBy('trips_versions.start_date', 'DESC');
            } else {
                $plans_invited = $plans_invited->orderBy('trips.created_at', $sort);
            }

            $plans_invited = $plans_invited->skip($skip)->take(10)->get();

            $return_invited_plan = [];
            if (count($plans_invited) > 0) {
                foreach ($plans_invited as $plan) {
                    $return_invited_plan[] = array('inv' => $plan, 'user_id' => $userId, 'user' => $user);
                }
            }

            return ApiResponse::create($return_invited_plan);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\GET(
     * path="/profile/{user_id}/plans-mydraft",
     *   tags={"Profile"},
     *   summary="Get profile trip plans",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="pagenum",
     *        description="Page num",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * *   @OA\Parameter(
     *        name="sort",
     *        description="Sort type, types = asc | desc | current_date",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */

    /**
     * @param MyDraftPlansRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getMyDraftPlans(MyDraftPlansRequest $request, $id)
    {
        try {
            $user_id = $id;
            $page = $request->pagenum;
            $skip = ($page - 1) * 10;
            $sort = $request->sort;

            if ($user_id && $user_id != Auth::user()->id) {
                $userId = $user_id;
                $login_user_id = '';
            } else {
                $userId = Auth::user()->id;
                $login_user_id = $userId;
            }


            $user = User::find($userId);

            $plans_my_draft = TripPlans::where('trips.users_id', $userId)->where('trips.active', 0);

            if ($sort == 'creation_date') {
                $plans_my_draft = $plans_my_draft->select('trips.*')->leftJoin('trips_versions', 'trips_versions.plans_id', '=', 'trips.id')
                    ->groupBy('trips.id')
                    ->orderBy('trips_versions.start_date', 'DESC');
            } else {
                $plans_my_draft = $plans_my_draft->orderBy('trips.created_at', $sort);
            }

            $plans_my_draft = $plans_my_draft->skip($skip)->take(10)->get();

            $return_mydraft_plan = [];
            if (count($plans_my_draft) > 0) {
                foreach ($plans_my_draft as $my_draft) {
                    $return_mydraft_plan[] = array('login_user_id' => $login_user_id, 'inv' => $my_draft, 'user_id' => $userId, 'user' => $user);
                }
            }

            return ApiResponse::create($return_mydraft_plan);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param null|integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function getFavourites($user_id = null)
    {
        try {
            if ($user_id && $user_id != Auth::user()->id) {
                $userId = $user_id;
            } else {
                $userId = Auth::user()->id;
            }

            $user = User::find($userId);

            $data['user'] = $user;

            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->get();
            $data['user_id'] = $user_id;

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/profile/{user_id}/photos",
     *   tags={"Profile"},
     *   summary="Get user photos",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="User id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="sort",
     *        description="Sort types : date_down | date_up | most_liked | most_commented",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            default="date_down",
     *            type="string"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="page",
     *        description="Page number",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            default="1",
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="per_page",
     *        description="The number of items to be shown per page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            default="12",
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param MediaRequest $request
     * @param MediaService $service
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function getPhotos(MediaRequest $request, MediaService $service, int $userId)
    {
        $page = $request->page ?? 1;
        $offset = ($page - 1) * $request->per_page;

        return ApiResponse::create($service->getPhotos($userId, $request->sort, $offset, $request->per_page));
    }


    /**
     * @OA\GET(
     ** path="/profile/{user_id}/videos",
     *   tags={"Profile"},
     *   summary="Get user videos",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="User id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="sort",
     *        description="Sort types : date_down | date_up | most_liked | most_commented",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            default="date_down",
     *            type="string"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="page",
     *        description="Page number",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            default="1",
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="per_page",
     *        description="The number of items to be shown per page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            default="12",
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param Request $request
     * @param MediaService $service
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function getVideos(MediaRequest $request, MediaService $service, int $userId)
    {
        $page = $request->page ?? 1;
        $offset = ($page - 1) * $request->per_page;

        return ApiResponse::create($service->getVideos($userId, $request->sort, $offset, $request->per_page));
    }

    /**
     * @OA\GET(
     ** path="/profile/{user_id}/reviews",
     *   tags={"Profile"},
     *   summary="Get user reviews",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="User id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="page",
     *        description="Page number",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            default="1",
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="per_page",
     *        description="The number of items to be shown per page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            default="12",
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="sort",
     *        description="Sort types : date_down | date_up",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            default="date_down",
     *            type="string"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="score",
     *        description="Review score from 1 to 5",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param ReviewRequest $request
     * @param ReviewsService $service
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function getReviews(ReviewRequest $request, ReviewsService $service, int $userId)
    {
        return ApiResponse::create($service->getReviews($userId, $request->page, $request->per_page, $request->sort, $request->score));
    }

    /**
     * @param null|integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function postCheckFollow($user_id)
    {
        try {
            $user = User::find($user_id);
            if (!$user) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid User ID']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $follower = UsersFollowers::where('users_id', $user->id)
                ->where('followers_id', Auth::user()->id)
                ->first();

            if (empty($follower)) {
                return ApiResponse::create([], false, ApiResponse::NO_CONTENT);
            } else {
                return ApiResponse::create(
                    [
                        'success' => true
                    ]
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\POST(
     ** path="/profile/{user_id}/follow",
     *   tags={"Common API"},
     *   summary="This Api used to follow any user",
     *   operationId="Api/ProfileController@postFollow",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param null|integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function postFollow($user_id)
    {
        try {
            $user = User::find($user_id);
            if (!$user) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid User ID']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $follower = UsersFollowers::where('users_id', $user_id)
                ->where('followers_id', Auth::user()->id)
                ->first();

            if ($follower) {
                return ApiResponse::create(
                    [
                        'message' => ['This user is already in follow']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }



            $follow = new UsersFollowers;
            $follow->users_id = $user->id;
            $follow->followers_id = Auth::user()->id;
            $follow->save();

            //$this->updateStatistic($country, 'followers', count($country->followers));
            log_user_activity('User', 'follow', $user_id);

            return ApiResponse::create([
                'message' => ["follow successfully"]
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/profile/{user_id}/unfollow",
     *   tags={"Common API"},
     *   summary="This Api used to unfollow any user",
     *   operationId="Api/ProfileController@postUnFollow",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param null|integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function postUnFollow($user_id)
    {
        try {
            $user = User::find($user_id);
            if (!$user) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid User ID']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $follower = UsersFollowers::where('users_id', $user_id)
                ->where('followers_id', Auth::user()->id);

            if (!$follower->first()) {
                return ApiResponse::create(
                    [
                        'message' => ['You are not following this User.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            } else {
                $follower->delete();
                //$this->updateStatistic($country, 'followers', count($country->followers));
                log_user_activity('User', 'unfollow', $user_id);
            }
            return ApiResponse::create(['message' => ["unfollow successfully"]]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param null|integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function postCheckFollowContent($user_id)
    {
        try {
            $user = User::find($user_id);
            return ApiResponse::create([
                'user' => $user
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Post(
     ** path="/profile/upload_cover",
     *   tags={"Profile"},
     *   summary="Upload or Update Cover",
     *   operationId="Api/ProfileController@postUploadCover",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="photo",
     *                  type="string",
     *                  format="binary"
     *              ),
     *              @OA\Property(
     *                  property="image",
     *                  description="croped image data",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="type",
     *                  description="save|update",
     *                  type="string",
     *              ),
     *          )
     *      )
     *  ),
     * 
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *   )
     *)
     **/
    /**
     * @param UploadCoverRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postUploadCover(UploadCoverRequest $request, ProfileService $profileservise)
    {
        try {
            $croped_image = $request->image;
            $original_file = $request->file('photo');
            $user_id = Auth::user()->id;

            $result = $profileservise->uploadProfileImage($user_id, $original_file, $croped_image, 'cover', $request->type, 'cover_photo');

            return ApiResponse::create($result);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/profile/upload_profile_image",
     *   tags={"Profile"},
     *   summary="Upload or Updat Profile Image",
     *   operationId="Api/ProfileController@postUploadProfileImage",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="photo",
     *                  type="string",
     *                  format="binary"
     *              ),
     *              @OA\Property(
     *                  property="image",
     *                  description="croped image data",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="type",
     *                  description="save|update",
     *                  type="string",
     *              ),
     *          )
     *      )
     *  ),
     * 
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *   )
     *)
     **/
    /**
     * @param UploadProfileImageRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postUploadProfileImage(UploadProfileImageRequest $request, ProfileService $profileservise)
    {
        try {
            $croped_image = $request->image;
            $original_file = $request->file('photo');
            $user_id = Auth::user()->id;

            $result = $profileservise->uploadProfileImage($user_id, $original_file, $croped_image, 'profile', $request->type, 'profile_picture');

            return ApiResponse::create($result);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @return \Illuminate\Http\Response
     */
    public function postDeleteCover()
    {
        try {
            $user_id = Auth::user()->id;
            $user = User::find($user_id);
            if ($user->cover_photo != "" || $user->cover_photo != null) {
                $amazonefilename = explode("https://s3.amazonaws.com/travooo-images2/", $user->cover_photo);
                Storage::disk('s3')->delete($amazonefilename[1]);

                $user->cover_photo = null;
                $user->save();
                return ApiResponse::create(
                    [
                        'message' => ['Profile Image deleted']
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['somethings went wrong']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param null|integer $user_id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateReport($user_id, Request $request)
    {
        try {
            $user_id = $user_id ? $user_id : Auth::user()->id;
            $page = $request->get("pageno");
            $skip = ($page - 1) * 10;

            $user = User::find($user_id);
            $data = [];
            foreach ($user->reports()->skip($skip)->take(10)->get() as $r) {
                $data[] = $r;
            }
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function deleteProfileImage()
    {
        try {
            $user_id = Auth::user()->id;
            $user = User::find($user_id);

            if ($user->profile_picture != "" || $user->profile_picture != null) {
                $amazonefilename = explode('/', $user->profile_picture);
                Storage::disk('s3')->delete('users/profile/' . $user_id . '/' . end($amazonefilename));
                Storage::disk('s3')->delete('users/profile/original/' . $user_id . '/' . end($amazonefilename));

                $user->profile_picture = null;
                $user->save();

                return ApiResponse::create([
                    'message' => ['Image Deleted']
                ]);
            } else {
                return ApiResponse::create(['message' => ["profile picture not found"]], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param MediaCommentsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getMediaComments(MediaCommentsRequest $request)
    {
        try {
            $id = $request->id;
            $media = Media::find($id);
            $mediaComments = [];
            if (is_object($media)) {
                foreach (@$media->comments as $comment) {
                    $mediaComments[] = $comment;
                }
                return ApiResponse::create([
                    'media_comments' => $mediaComments
                ]);
            } else {
                return ApiResponse::create([
                    'media_comments' => $mediaComments
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param VisitedCitiesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getVisitedCities(VisitedCitiesRequest $request)
    {
        try {
            $user_id = $request->user_id;
            $visited_cities = Checkins::where('users_id', $user_id)
                ->whereNotNull('city_id')
                ->whereNull('place_id')
                ->orderBy('created_at', 'DESC')
                ->groupBy('id')
                ->take(10)
                ->get();
            if ($visited_cities) {
                return ApiResponse::create(
                    [
                        'visited_cities' => $visited_cities
                    ]
                );
            } else {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/profile/{user_id}/feed",
     *   tags={"Profile"},
     *   summary="Get user feed",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="User id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="page",
     *        description="Page number",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer",
     *            default="1"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="per_page",
     *        description="The number of items to be shown per page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer",
     *            default="12"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="type",
     *        description="Types = all_feed | trip_plans | travelogs | discussions | reviews",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string",
     *            default="all_feed",
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param null|integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function getFeed(FeedRequest $request, ProfileService $service, $userId)
    {
        return ApiResponse::create(
            $service->getFeed($userId, $request->type, $request->page, $request->per_page)
        );
    }

    /**
     * @param TimlineMediasRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getTimlineMedias(TimlineMediasRequest $request)
    {
        try {
            $user = User::find($request->user_id);
            $media_list = [];
            if ($request->type == 'day') {
                $date_format = 'Y-m-d';
                $format = 'F d';
            } elseif ($request->type == 'month') {
                $date_format = 'Y-m';
                $format = 'Y F';
            } else {
                $date_format = 'Y';
                $format = 'Y';
            }

            $usermedias = $user->my_medias()->pluck('medias_id');

            if ($request->media_type == 'photo') {
                $photos_list  = Media::select('id')->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
            } else if ($request->media_type == 'video') {
                $photos_list  = Media::select('id')->where(DB::raw('RIGHT(url, 4)'), '=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
            }

            $my_medias = UsersMedias::where('users_id', $request->user_id)
                ->whereIn('medias_id', $photos_list)
                ->orderBy('id', 'DESC')
                ->get();

            foreach ($my_medias as $media) {
                if ($media->media->uploaded_at) {
                    $date_type = date($date_format, strtotime($media->media->uploaded_at));

                    $date_type = date_create($date_type);
                    $media_list[date_format($date_type, $format)][$media->id] = $media->media->url;
                }
            }

            return ApiResponse::create(
                [
                    'media_list' => $media_list,
                    'media_type' => $request->media_type
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\POST(
     ** path="/profile/postBecomeExpert",
     *   tags={"Profile"},
     *   summary="Switch To Expert Account",
     *   operationId="Api/ProfileController@postBecomeExpert",
     *   security={
     *      {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *      name="social_links{}",
     *      description="'website|facebook|instagram|twitter|tumblr|pinterest|youtube':'www.website.com/user'",
     *      in="query",     
     *      @OA\Schema( 
     *          @OA\Property(
     *             property="social_links",
     *             type="object",
     *             description="The response data",
     *             @OA\Items
     *          ),
     *          example={"social_links":{"facebook": "", "twitter": "", "instagram": "", "pinterest": "", "tripadvisor": "", "youtube": "", "medium": "", "website": "", "vimeo": ""}}
     *      )
     * 
     *   ),
     *   @OA\Parameter(
     *        name="cities[]",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *  @OA\Parameter(
     *        name="countries[]",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     * @OA\Parameter(
     *        name="places[]",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *  @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/

    /**
     * @param ExpertRequest $request
     * @param ExpertiseService $expertiseService
     * @return \Illuminate\Http\Response
     */
    public function postBecomeExpert(ExpertRequest $request, ExpertiseService $expertiseService)
    {
        try {
            if ((Auth::user()->type == 1 && Auth::user()->expert == 0) || (Auth::user()->type == 2 && Auth::user()->expert == 0 && Auth::user()->is_approved === 0)) {
                $user = Auth::user();

                $expertiseService->setExpertiseCities($user->id, $request->input('cities', []));
                $expertiseService->setExpertiseCountries($user->id, $request->input('countries', []));
                $expertiseService->setExpertisePlaces($user->id, $request->input('places', []));

                $social_links = collect($request->social_links)->only('facebook', 'twitter', 'instagram', 'pinterest', 'tripadvisor', 'youtube', 'medium', 'website', 'vimeo')->toArray();
                foreach ($social_links as $field => $social_link) {
                    if ($social_link) {
                        $user->$field = $social_link;
                    }
                }

                $user->is_approved = NULL;
                $user->type = 2;
                $user->save();
                return ApiResponse::__create("Successfully submitted, Now your application is under review");
            } else {
                return ApiResponse::__create("Your application is under review");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param UdateBioRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postUdateBio(UdateBioRequest $request)
    {
        try {
            $user = Auth::user();
            $user->about = $request->bio;

            if ($user->save()) {
                return ApiResponse::create(
                    [
                        'user' => $user
                    ]
                );
            } else {
                return ApiResponse::create([], false, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/profile/{user_id}/plans",
     *   tags={"Profile"},
     *   summary="Get profile trip plans",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="User id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="page",
     *        description="Page number",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer",
     *            default="1"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="per_page",
     *        description="The number of items to be shown per page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer",
     *            default="12"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="sort",
     *        description="Sort type, types = asc | desc | creation_date",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string",
     *            default="desc",
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="filter",
     *        description="Filter type, types = all | upcoming | memory",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string",
     *            default="all",
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param TripPlanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getPlans(TripPlanRequest $request, ProfileService $service, $userId)
    {
        return ApiResponse::create(
            $service->getTripPlans($userId, $request->page, $request->per_page, $request->sort, $request->filter)
        );
    }

    /**
     * @OA\GET(
     ** path="/profile/{user_id}/getFollowers",
     *   tags={"Profile"},
     *   summary="Get profile followers",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="User id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="page",
     *        description="Page number",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer",
     *            default="1"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="per_page",
     *        description="The number of items to be shown per page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer",
     *            default="12"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param FollowUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getFollowers(PaginationRequest $request, ProfileService $service, $userId)
    {
        $page = $request->page;
        $perPage = $request->per_page;
        return ApiResponse::create($service->getFollowers($userId, $page, $perPage));
    }

    /**
     * @OA\GET(
     ** path="/profile/{user_id}/getFollowing",
     *   tags={"Profile"},
     *   summary="Get profile followers",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="User id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="page",
     *        description="Page number",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer",
     *            default="1"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="per_page",
     *        description="The number of items to be shown per page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer",
     *            default="12"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="type",
     *        description="Type value must be in list: all | people | countries | cities | places",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string",
     *            default="all"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param FollowUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getFollowing(FollowUserRequest $request, ProfileService $service, $userId)
    {
        $page = $request->page;
        $perPage = $request->per_page;
        $type = $request->type;
        return ApiResponse::create($service->getFollowing($type, $userId, $page, $perPage));
    }

    /**
     * @OA\GET(
     ** path="/profile/{user_id}/liked_feed",
     *   tags={"Profile"},
     *   summary="Get user feed",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="user_id",
     *        description="User id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="page",
     *        description="Page number",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer",
     *            default="1"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="per_page",
     *        description="The number of items to be shown per page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer",
     *            default="12"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param null|integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function getLikedFeed(PaginationRequest $request, ProfileService $service, $userId)
    {
        return ApiResponse::create(
            $service->getFeed($userId, 'liked', $request->page, $request->per_page)
        );
    }
}
