<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Models\User\ApiUser as User;
use App\PasswordReset;

class PasswordResetController extends Controller
{
    /**
     * @OA\Post(
     ** path="/password/create",
     *   tags={"Password Reset"},
     *   summary="Step 1",
     *   operationId="Api\PasswordResetController@create",
     *   @OA\Parameter(
     *        name="email",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Create token password reset
     */
    public function create(Request $request)
    {
        try {
            $validator = \Validator::make(
                $request->all(),
                [
                    'email' => 'required|exists:users'
                ],
                [
                    'email.exists' => "Email you entered doesn't exist.."
                ]
            );
            if ($validator->fails()) {
                return ApiResponse::create($validator->errors(), false, ApiResponse::VALIDATION);
            }
            $user = User::where('email', $request->email)->first();
            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'email' => $user->email,
                    'token' => str_random(60)
                ]
            );

            if ($user && $passwordReset) {
                $user->notify(
                    new PasswordResetRequest($passwordReset->token)
                );
            }

            return ApiResponse::create([
                'message' => ['We have e-mailed your password reset link!']
            ], true, ApiResponse::OK);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Post(
     ** path="/password/find/{token}",
     *   tags={"Password Reset"},
     *   summary="Step 2 or click in mail",
     *   operationId="Api\PasswordResetController@find",
     *   @OA\Parameter(
     *        name="token",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [ApiResponse] passwordReset object
     */
    public function find($token)
    {
        try {
            $passwordReset = PasswordReset::where('token', $token)->first();
            if (!$passwordReset) {
                return ApiResponse::create([
                    'message' => ['This password reset token is invalid.']
                ], false, ApiResponse::BAD_REQUEST);
            }

            if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
                $passwordReset->delete();
                return ApiResponse::create(
                    [
                        'message' => ['This password reset token is invalid.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            return ApiResponse::create($passwordReset);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/password/reset",
     *   tags={"Password Reset"},
     *   summary="Step 3",
     *   operationId="Api\PasswordResetController@reset",
     *   @OA\Parameter(
     *        name="token",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="email",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="password",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="password_confirmation",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Reset password
     */
    public function reset(Request $request)
    {
        try {
            $validator = \Validator::make(
                $request->all(),
                [
                    'email' => 'required|string|email',
                    'password' => 'required|confirmed|string|min:6|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])(?=\S+$).{6,255}$/',
                    'token' => 'required|string'
                ],
                [
                    'password.regex' => 'Password Format is invalid.'
                ]
            );
            if ($validator->fails()) {
                return ApiResponse::create($validator->errors(), false, ApiResponse::VALIDATION);
            }
            $passwordReset = PasswordReset::where(
                [
                    ['token', $request->token],
                    ['email', $request->email]
                ]
            )->first();

            if (!$passwordReset) {
                return ApiResponse::create(
                    [
                        'message' => ['This password reset token is invalid.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $user = User::where('email', $passwordReset->email)->first();

            if (!$user) {
                return ApiResponse::create(
                    [
                        'message' => ["Email doesn't exist"]
                    ],
                    false,
                    ApiResponse::UNAUTHORIZED
                );
            }

            $user->password = bcrypt($request->password);
            $user->save();
            $passwordReset->delete();
            $user->notify(new PasswordResetSuccess($passwordReset));
            return ApiResponse::create($user);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    function verifyDevice(Request $request, $email = "", $code = "")
    {
        $email = trim(base64_decode($email));
        $user = User::where('email', $email)->first();
        if ($user != null) {
            if ($request->isMobileDevice) {
                $url = generateDeepLink(url('/api/v1/invite/expert/' . $user->email));
            } else {
                $url = url('/signup?expert_email=' . $user->email);
            }
            return redirect($url);
        } else {
            return "Invalid link";
        }
    }
}
