<?php

namespace App\Http\Controllers\Api;

use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Models\TripPlans\TripPlans;
use App\Models\City\Cities;

class BookController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function getHotels()
    {
        $user = Auth::user();
        $tripPlans = TripPlans::where('users_id', $user->id)->get();
        return ApiResponse::create(
            $tripPlans
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function searchHotels(Request $request)
    {
        $response = [];
        $user = Auth::user();
        $response['plans'] = TripPlans::where('users_id', $user->id)->get();

        try {
            $validator = \Validator::make(
                $request->all(),
                [
                    'city' => 'required|string',
                    'from_date'   => 'required|date_format:Y-m-d',
                    'to_date'     => 'required|date_format:Y-m-d|after_or_equal:from_date',
                ]
            );
            if ($validator->fails()) {
                return ApiResponse::create( $validator->errors(), false, ApiResponse::VALIDATION );
            }
            $results = $this->doSearchHotel($request);

            if (!is_array($results) || count($results)) {
                return ApiResponse::create(
                    [
                        'message' => ["Search not found"]
                    ]
                );
            }

            $response['search_id'] = $results['search']['id'];
            $response['city_name'] = $results['search']['city']['name'];
            
            $response['city'] = Cities::with('transsingle')
                ->with('medias')
                ->whereHas(
                    'transsingle',
                    function ($query) use ($response) {
                        $query->where('title', 'LIKE', '%'.$response['city_name'].'%');
                    }
                )->get();
            
            $results2 = $this->doCompleteSearchHotel($response['search_id']);
            $rates = $results2->rates;
            
            $frates = [];
            foreach ($rates as $rate) {
                $frates[$rate->hotelId][] = $rate;
            }
            
            $response['hotels'] = $results['hotels'];
            $response['rates'] = $frates;
            
            return ApiResponse::create($response);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);   
        }
    }

    /**
     * @param Request $request
     * @return array|null
     */
    private function doSearchHotel($request)
    {
        $from_date = date("Y-n-d", strtotime($request->from_date));
        $to_date = date("Y-n-d", strtotime($request->to_date));

        $token = $this->doAPIAuthorization();
        if (!$token = $this->doAPIAuthorization()) {
            return null;
        }

        $client = new Client(
            [
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => "Bearer ${token}"]
            ]
        );

        $response = $client->post(
            'https://srv.wego.com/metasearch/hotels/searches',
            [
                'body' => json_encode(
                    [
                        'search' => [
                            'locale' => 'en',
                            'cityCode' => $request->city,
                            'siteCode' => 'US',
                            'currencyCode' => 'USD',
                            'guestsCount' => 1,
                            'roomsCount' => 1,
                            'checkIn' => $from_date,
                            'checkOut' => $to_date,
                            'deviceType' => 'mobile'
                        ]
                    ]
                )
            ]
        );
        $result = [];
        if ($response->getStatusCode() == 200) {
            $result = json_decode($response->getBody()->getContents(), true);
        }
        return $result;
    }

    /**
     * @param string $search_id
     * @return array|null
    */
    private function doCompleteSearchHotel($search_id)
    {
        $token = $this->doAPIAuthorization();
        if (!$token = $this->doAPIAuthorization()) {
            return null;
        }

        $client = new Client(
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => "Bearer ${token}"
                ]
            ]
        );

        $response = $client->get(
            'https://srv.wego.com/metasearch/hotels/searches',
            [
                'body' => json_encode(
                    [
                        'search' => [
                            'id' => $search_id
                        ]
                    ]
                )
            ]
        );
        
        $result = [];
        if ($response->getStatusCode() == 200) {
            $result = json_decode($response->getBody()->getContents(), true);
        }
        return $result;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
    */
    public function completeSearchHotels(Request $request)
    {
        try {
            $search_id = $request->get('search_id', "");
            return ApiResponse::create(
                $this->doCompleteSearchHotel($search_id)
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
        
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getFlights()
    {
        $user = Auth::user();
        $tripPlans = TripPlans::where('users_id', $user->id)->get();
        return ApiResponse::create($tripPlans);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function searchFlights(Request $request)
    {
        $response = [];
        $user = Auth::user();
        $response['plans'] = TripPlans::where('users_id', $user->id)->get();

        try {
            $validator = \Validator::make(
                $request->all(),
                [
                    'city_from' => 'required|string',
                    'city_to'   => 'required|string',
                    'from_date' => 'required|date_format:Y-m-d',
                    'to_date'   => 'required|date_format:Y-m-d|after_or_equal:from_date',
                ]
            );
            if ($validator->fails()) {
                return ApiResponse::create( $validator->errors() , false, ApiResponse::VALIDATION );
            }

            $results = $this->doSearchFlights($request);

            if (!is_array($results) || count($results)) {
                return ApiResponse::create(
                    [
                        'message' => ["Search not found"]
                    ],
                    true,
                    ApiResponse::NO_CONTENT
                );
            }

            $response['search_id'] = $results['search']['id'];
            return ApiResponse::create($response);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return array|null
    */
    private function doSearchFlights($request)
    {
        $from_date = date("Y-m-d", strtotime($request->from_date));
        $to_date = date("Y-m-d", strtotime($request->to_date));

        $token = $this->doAPIAuthorization();
        if (!$token = $this->doAPIAuthorization()) {
            return null;
        }

        $client = new Client(
            [
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => "Bearer ${token}"]
            ]
        );

        $legs = [
            [
                "departureAirportCode" => $request->city_from,
                "arrivalCityCode" => $request->city_to,
                "outboundDate" => $from_date
            ],
            [
                "departureAirportCode" => $request->city_to,
                "arrivalCityCode" => $request->city_from,
                "outboundDate" => $to_date
            ]
        ];

        $response = $client->post(
            'https://srv.wego.com/metasearch/flights/searches',
            [
                'body' => json_encode(
                    [
                        'search' => [
                            "cabin" => "economy",
                            "adultsCount" => 1,
                            "childrenCount" => 0,
                            "infantsCount" => 0,
                            "locale" => "ar",
                            "siteCode" => "SG",
                            "currencyCode" => "SGD",
                            'legs'=> $legs,
                            "deviceType" => "mobile",
                            "appType" => "WEB_APP"
                        ]
                    ]
                )
            ]
        );

        $result = [];
        if ($response->getStatusCode() == 200) {
            $result = json_decode($response->getBody()->getContents(), true);
        }
        return $result;
    }

    /**
     * @param string $search_id
     * @return array|null
    */
    private function doCompleteSearchFlights($search_id)
    {
        $token = $this->doAPIAuthorization();
        if (!$token = $this->doAPIAuthorization()) {
            return null;
        }

        $client = new Client(
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => "Bearer ${token}"
                ]
            ]
        );
        $response = $client->get(
            'https://srv.wego.com/metasearch/flights/searches/'.$search_id."?offset=0"
        );

        $result = [];
        if ($response->getStatusCode() == 200) {
            $result = json_decode($response->getBody()->getContents(), true);
        }
        return $result;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
    */
    public function completeSearchFlights(Request $request)
    {
        try {
            $search_id = $request->get('search_id', "");
            return ApiResponse::create(
                $this->doCompleteSearchFlights($search_id)
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return string|null
     */
    private function doAPIAuthorization()
    {
        $client = new Client(
            [
                'headers' => ['Content-Type' => 'application/json', 'X-Wego-Version' => 1]
            ]
        );

        $response = $client->post(
            'https://srv.wego.com/users/oauth/token',
            [
                'body' => json_encode(
                    [
                        'grant_type' => 'client_credentials',
                        'client_id' => 'ac9ac5c6c1c603fb33e61f01',
                        'client_secret' => '4ebe013c772c81b0ff3f764e',
                        'scope' => 'affiliate'
                    ]
                )
            ]
        );
        if ($response->getStatusCode() == 200) {
            $result = json_decode($response->getBody()->getContents());
            return $result->access_token;
        } else {
            return null;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
    */
    public function searchCity(Request $request)
    {
        if (!$query = $request->get('q', '')) {
            return ApiResponse::create( [
                'message' => ["Search keyword should not be empty"]
            ], false, ApiResponse::VALIDATION );
        }
        try {
            $res = file_get_contents('https://srv.wego.com/places/search?query=' . $query);

            if (empty($res)) {
                return ApiResponse::create(
                    [
                        'message' => ["Result not found"]
                    ],
                    true,
                    ApiResponse::NO_CONTENT
                );
            }

            $results = json_decode($res);
            
            $cities = [];
            foreach ($results as $result) {
                if ($result->type == "city") {
                    $cities[] = [
                        'id' => $result->code,
                        'text' => $result->code . " - " . $result->name,
                    ];
                }
            }
            return ApiResponse::create($cities);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
