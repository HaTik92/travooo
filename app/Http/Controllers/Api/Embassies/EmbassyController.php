<?php

namespace App\Http\Controllers\Api\Embassies;

/* Dependencies */
use App\Models\Country\Countries;
use App\Models\Embassies\Embassies;
use App\Models\System\Session;
use App\Models\User\User;
use Illuminate\Routing\Controller;

/**
 * Class EmbassyController.
 */
class EmbassyController extends Controller
{
	/* Show Embassies Api */
    /**
     * @param $userId
     * @param $sessionToken
     * @param $countryId
     * @param int $embassyId
     * @return array
     */
	public function show_embassies($userId, $sessionToken, $countryId, $embassyId = 0) {
        /* If Provided User Id Is Not An Integer, Return Error */
        if(! is_numeric($userId) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User id should be an integer.',
                ],
                'success' => false
            ];
        }

        /* Find User For The Provided User Id */
        $user = User::find($userId);

        /* If User Id Not Found Return Error */
        if(empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'success' => false
            ];
        }

        /* Find Session For The Provided Session Token */
        $session = Session::find(['id' => $sessionToken])->first();

        /* If Session Not Found, Return Error */
        if(empty($session)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong session token provided.',
                ],
                'success' => false
            ];
        }

        /* If Session's User Id Doesn't Matches Provided User Id, Return Error */
        if($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'success' => false
            ];
        }

        /* If Country Id Is Not An Integer, Return Error */
        if(!is_numeric($countryId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Country id should be an integer.',
                ],
                'success' => false
            ];
        }

        /* Find Country For The Provided Country Id */
        $country = Countries::find($countryId);

        /* If Country Not Found, Return Error */
        if(empty($country)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong country id provided.',
                ],
                'success' => false
            ];
        }

        /* If Embassy Id Is Provided, Find Embassy And Return Its Information */
        if(!empty($embassyId)) {

            /* If Embassy Id Is Not An Integer, Return Error */
            if(!is_numeric($embassyId)){
                return [
                    'data' => [
                        'error'   => 400,
                        'message' => 'Embassy id should be an integer.',
                    ],
                    'success' => false
                ];
            }

            /* Find Embassy For The Provided Embassy Id */
            $embassies = Embassies::find($embassyId);

            /* if Embassy Not Found, Return Error */
            if(empty($embassies)){
                return [
                    'data' => [
                        'error'   => 400,
                        'message' => 'Wrong embassy id provided.',
                    ],
                    'success' => false
                ];
            }

            /* If Embassy Not Active, Return Deactivated Error */
            if($embassies->active != Embassies::ACTIVE) {
                return [
                    'data' => [
                        'error'   => 400,
                        'message' => 'This embassy is deactivated.',
                    ],
                    'success' => false
                ];
            }

            /* Get Embassy Information And Translations In Array Format */
            $embassies_trans = [];
            if(!empty($embassies->trans)) {
                foreach ($embassies->trans as $key => $value) {
                    $embassies_trans[$value->languages_id] = [
                        'title'         => $value->title,
                        'description'   => $value->description
                    ];
                }
            }

            /* Return Success Message, Along With Embassy Data */
            return [
                'status' => true,
                'data'   => [
                    'embassies' => [
                        'id'          => $embassies->id,
                        'lat'         => $embassies->lat,
                        'lng'         => $embassies->lng,
                        'active'      => $embassies->active,
                        'translation' => $embassies_trans
                    ]
                ]
            ];
        }

        $embassies_arr = [];

        /* Find Embassies Having Provided Country Id */
        $embassies = Embassies::where(['countries_id' => $countryId, 'active' => Embassies::ACTIVE])->get();

        /* If Embassies Found, Get Embassies Information In Array Format */
        if(!empty($embassies[0])) {

            $embassies_translation = [];

            foreach ($embassies as $key => $value) {

                if(!empty($value->trans)) {
                    foreach ($value->trans as $key_trans => $value_trans) {
                        $embassies_translation[$value_trans->languages_id] = [
                            'title'       => $value_trans->title,
                            'description' => $value_trans->description
                        ];
                    }
                }

                array_push($embassies_arr,[
                    'id'     => $value->id,
                    'lat'    => $value->lat,
                    'lng'    => $value->lng,
                    'active' => $value->active,
                    'translations' => $embassies_translation,
                ]);
            }
        }

        /* If Embassies Exist, Return Success Status Along With Embassies Information */
        /* Else Return Success Status, Along With Embassies Not Found Message */
        if(!empty($embassies_arr)) {
            return [
                'status' => true,
                'data'   => [
                    'embassies' => $embassies_arr
                ]
            ];
        } else {
            return [
                'status' => true,
                'data'   => [
                    'embassies' => $embassies_arr,
                    'message'   => 'No embassy found for provided country id.'
                ]
            ];
        }
	}
}