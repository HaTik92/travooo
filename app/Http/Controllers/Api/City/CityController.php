<?php

namespace App\Http\Controllers\Api\City;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Http\Requests\Api\Cities\CitiesSearchRequest;
use App\Http\Requests\Api\Cities\CitiesSharesRequest;
use App\Http\Requests\Api\Cities\CityDiscussionsRequest;
use App\Http\Requests\Api\Cities\CityInformationRequest;
use App\Http\Requests\Api\Cities\CityMediasRequest;
use App\Http\Requests\Api\Cities\CityPlacesRequest;
use App\Http\Requests\Api\Cities\CityShowRequest;
use App\Http\Requests\Api\Cities\CityTripPlansRequest;
use App\Http\Requests\Api\Cities\CityUnfollowRequest;
use App\Models\Access\language\Languages;
use App\Http\Controllers\Controller;
use App\Models\ActivityLog\ActivityLog;
use App\Models\City\Cities;
use App\Models\City\CitiesFollowers;
use App\Models\City\CitiesShares;
use App\Models\City\CitiesWeather;
use App\Transformers\City\CityTransformer;
use App\Transformers\City\CityWeatherTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Resource\Item;

class CityController extends Controller
{
    use CreateUpdateStatisticTrait;

    /**
     * @param CityShowRequest $request
     *
     * @return array
     */
    public function show(CityShowRequest $request) {
        $cityId = $request->input('city_id');

        /* Find Country For The Provided Country Id */
        $cities = Cities::find($cityId);

        /* If Country Not Found, Return Error */
        if(empty($cities)){
            return [
                'status' => false,
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong city id provided.'
                ]
            ];
        }

        /* Get Country Information In Array Format */
        $city_arr = $cities->getArrayResponse();

        /* Return Success Status, And Country Information */
        return [
            'status' => true,
            'data'   => [
                'city_info' => $city_arr
            ]
        ];
    }

    /**
     * @param CitiesSearchRequest $request
     *
     * @return array
     */
    public function postSearch(CitiesSearchRequest $request){
        $post = $request->input();
        $languageId = $request->input('language_id');

        $queryParam = null;
        if(isset($post['query']) && !empty($post['query'])){
            $queryParam = $post['query'];
        }
        
        $offset = 0;
        $limit  = 20;

        $cities = Cities::with([
            'trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'country.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            }
        ])
        ->whereHas('trans', function ($query) use ($queryParam) {
            if($queryParam) {
                $query->where('title', 'like', "%".$queryParam."%");
            }
        })
        ->offset($offset)
        ->limit($limit)
        ->get()
        ->sortBy(function ($cities) {
            return $cities->trans[0]['title'];
        })
        ->values()
        ->all();

        $cities = json_encode($cities);

        return [
            'success' => true,
            'code'    => 200,
            'data'    => $cities
        ];
    }

    /**
     * api/cities/{city_id}
     *
     * Show City information
     *
     * parameter Integer "language_id" (required) <br />
     *
     * @param $cityId
     * @param CityInformationRequest $request
     *
     * @return array
     */
    public function getCityInformation($cityId, CityInformationRequest $request)
    {
        $languageId =  $request->input('language_id');
        $city       = Cities::where('id', $cityId)->first();

        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
            'trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'currencies.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'religions.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'holidays.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'emergency.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            },
            'country.trans',
            'timezone'
        ])
            ->where('id', $cityId)
            ->where('active', 1)
            ->first();

        if(!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placesAtm = Cities::with([
            'places' => function ($query) use ($languageId) {
                $query->orderBy('rating');
                $query->where('place_type', 'like', 'atm,%');
            }
        ])
            ->where('id', $cityId)
            ->where('active', 1)
            ->first();

        $placesHospitals = Cities::with([
            'places' => function ($query) use ($languageId) {
                $query->orderBy('rating');
                $query->where('place_type', 'like', 'hospital,%');
            }
        ])
            ->where('id', $cityId)
            ->where('active', 1)
            ->first();

        $languages = $city->getCitiesOrCountryLanguages();
        $city->setRelation('languages', collect($languages));

        $resource = new Item($city, new CityTransformer());

        return apiResponse(
            $resource,
            [
                'trans',
                'country:',
                'country.trans:title|nationality|metrics',
                'languages:title|type',
                'currencies:',
                'currencies.trans:title',
                'religions:',
                'religions.trans:title',
                'holidays:',
                'holidays.trans:title|slug',
                'emergency:',
                'emergency.trans:title|emergency_numbers_id|description',
                'timezone:time_zone_id|time_zone_name|dst_offset|raw_offset'
            ],
            [
                'atms_count' => count($placesAtm->places),
                'hospitals_count' => count($placesHospitals->places)
            ]
        );
    }

    /**
     * @param $cityId
     *
     * @return array
     */
    public function getCityNumberFollowers($cityId)
    {
        $city = Cities::find($cityId);
        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }


        $city = Cities::with([
            'followers'
        ])->where('id', $cityId)->first();

        return [
            'data' => [
                'followers' => count($city->followers)
            ],
            'success' => true
        ];
    }

    /**
     * @param $cityId
     *
     * @return array
     */
    public function postFollow($cityId)
    {
        $userId = Auth::Id();
        $city = Cities::find($cityId);
        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $cityId)
            ->where('users_id', $userId)
            ->first();

        if($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that country',
                ],
                'success' => false
            ];
        }

        $city->followers()->create([
            'users_id' => $userId
        ]);

        $this->updateStatistic($city, 'followers', count($city->followers));
        log_user_activity('City', 'follow', $cityId);

        return [
            'success' => true
        ];
    }

    /**
     * @param CityUnfollowRequest $request
     * @param $cityId
     *
     * @return array
     */
    public function postUnFollow(CityUnfollowRequest $request, $cityId)
    {
        $userId     = Auth::Id();
        $languageId = $request->input('language_id');
        $city = Cities::find($cityId);
        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $cityId)
            ->where('users_id', $userId);

        if(!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user didn\'t follow this country',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            $this->updateStatistic($city, 'followers', count($city->followers));
            log_user_activity('City', 'unfollow', $cityId);
        }

        return [
            'success' => true
        ];
    }

    /**
     * @param $cityId
     *
     * @return array
     */
    public function getCityStats($cityId) {
        $city = Cities::find($cityId);
        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        return [
            'data' => [
                'stats' => $city->statistics,
            ],
            'success' => true
        ];
    }

    /**
     * @param $cityId
     * @param CityMediasRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCityMedia($cityId, CityMediasRequest $request) {
        $languageId = $request->input('language_id');
        $city = Cities::find($cityId);
        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
            'getMedias',
            'getMedias.users'
        ])->where('id', $cityId)->first();

        $resource = new Item($city, new CityTransformer());

        return apiResponse(
            $resource,
            [
                'medias:id|url',
                'medias.users:id|name|username|profile_picture'
            ]
        );
    }

    /**
     * @param $cityId
     * @param CityTripPlansRequest $request
     *
     * @return array
     */
    public function getCityTripPlans($cityId, CityTripPlansRequest $request) {
        $languageId = $request->input('language_id');
        $city = Cities::find($cityId);
        if(!$city ) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $tripPlans = Cities::select(
            'trips.id',
            'trips.title',
            'trips.users_id',
            'trips.created_at',
            'trips_places.budget',
            'trips_places.id'
        )
            ->join('places', 'cities.id', '=', 'places.cities_id')
            ->join('trips_places', function($join)
            {
                $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
            })
            ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
            ->where('cities.id', $cityId)->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
        }

        $city['plans'] = $tripPlans;
        $city['count'] = count($tripPlans);
        $city['sum_budget'] = $sumBudget;

        return [
            'data' => $city,
            'success' => true
        ];
    }

    /**
     * @param $cityId
     * @param CityPlacesRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCityPlaces($cityId, CityPlacesRequest $request) {
        $languageId = $request->input('language_id');
        $city = Cities::find($cityId);
        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
            'places' => function ($query) use ($languageId) {
                $query->orderBy('rating');
                $query->limit(50);
            },
            'places.getMedias.trans' => function ($query) use ($languageId) {
                $query->where('languages_id', $languageId);
            },
        ])->where('id', $cityId)->first();

        $resource = new Item($city, new CityTransformer());

        return apiResponse(
            $resource,
            [
                'places',
                'places:id',
                'places.trans:title',
                'places.medias'
            ]
        );
    }

    /**
     * @param $cityId
     * @param CityDiscussionsRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCityDiscussions($cityId, CityDiscussionsRequest $request) {
        $languageId = $request->input('language_id');
        $city = Cities::find($cityId);
        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
            'discussions',
            'discussions.UpVotes',
            'discussions.DownVotes',
            'discussions.users',
        ])->where('id', $cityId)->first();

        $resource = new Item($city, new CityTransformer());

        return apiResponse(
            $resource,
            [
                'discussions',
                'discussions.users:username|profile_picture',
                'discussions.UpVotes',
                'discussions.DownVotes',
            ]
        );
    }

    /**
     * @param $cityId
     *
     * @return array
     */
    public function checkFollow($cityId) {

        $userId = Auth::Id();
        $city = Cities::find($cityId);
        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $cityId)
                                   ->where('users_id', $userId)
                                   ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    /**
     * @param $cityId
     * @param CitiesSharesRequest $request
     *
     * @return array
     */
    public function postShare($cityId, CitiesSharesRequest $request) {

        $userId = Auth::Id();
        $scope  = $request->input('scope');
        $coment = $request->input('comment');

        $city = Cities::find($cityId);
        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $citiesShares = new CitiesShares();
        $citiesShares->city_id    = $cityId;
        $citiesShares->user_id    = $userId;
        $citiesShares->scope      = $scope;
        $citiesShares->comment    = $coment;
        $citiesShares->created_at = date('Y-m-d H:i:s', time());
        $citiesShares->save();

        return [
            'success' => true
        ];
    }

    /**
     * @param $cityId
     * @param Request $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCityWeather($cityId, Request $request)
    {
        $city = Cities::find($cityId);

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $languageId = $request->input('language_id');
        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $weather = CitiesWeather::with([
            'trans' => function ($query) use ($languageId) {
                $query->where('language_id', $languageId);
            },
        ])->where('cities_id', $cityId)->first();

        $resource = new Item($weather, new CityWeatherTransformer());

        return apiResponse($resource, ['trans']);
    }
}
