<?php

namespace App\Http\Controllers\Api;

use App\Services\Experts\DashboardService as ExpertDashboardService;
use Carbon\Carbon;

use App\Http\Responses\ApiResponse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Api\Dashboard\AffiliateRequest;
use App\Http\Requests\Api\Dashboard\OverviewRequest;
use App\Http\Requests\Api\Dashboard\EngagementRequest;
use App\Http\Requests\Api\Dashboard\EngagementFilterRequest;
use App\Http\Requests\Api\Dashboard\InteractionRequest;
use App\Http\Requests\Api\Dashboard\PostsRequest;
use App\Http\Requests\Api\Dashboard\ReportsRequest;
use App\Http\Requests\Api\Dashboard\RankingRequest;
use App\Http\Requests\Api\Dashboard\FollowersRequest;
use App\Http\Requests\Api\Dashboard\FollowersFilterRequest;
use App\Http\Requests\Api\Dashboard\MoreFollowersRequest;
use App\Http\Requests\Api\Dashboard\GlobalImpactRequest;
use App\Http\Requests\Api\Dashboard\MoreLeaderRequest;
use App\Http\Requests\Api\Dashboard\EngagementDigInfoRequest;

use App\Models\TripPlans\TripPlans;

use App\Models\Posts\Posts;
use App\Models\Posts\PostsViews;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsLikes;
use App\Models\Posts\PostsShares;

use App\Models\User\User;
use App\Models\UsersFollowers\UsersFollowers;

use App\Models\Country\Countries;

use App\Models\Referral\ReferralLinks;

use App\Models\Reports\Reports;
use App\Models\Reports\ReportsComments;
use App\Models\Reports\ReportsShares;
use App\Models\Reports\ReportsViews;
use App\Models\Reports\ReportsLikes;

use App\Models\Chat\ChatConversationMessage;

use App\Models\Notifications\Notifications;

use App\Services\Ranking\RankingService;

use Illuminate\Http\JsonResponse;
use App\Services\Users\ExpertiseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    /**
     * @var ExpertDashboardService
     */
    private $service;

    public function __construct(ExpertDashboardService $service)
    {
        $this->service = $service;
    }

    /**
     * @OA\Get(
     ** path="/dashboard",
     *   tags={"Dashboard Expert"},
     *   summary="dashboards",
     *   operationId="Api/DashboardController@getIndex",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        try {

            $user_id = Auth::user()->id;
            $data['chat_count'] = ChatConversationMessage::where('to_id', $user_id)->where('from_id', '<>', $user_id)->where('is_read', false)->count();
            $data['notification_count'] = Notifications::where('users_id', $user_id)->where('read', 0)->count();
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }



    /**
     * @OA\Get(
     ** path="/dashboard/get-more-affiliate",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard get more affiliate",
     *   operationId="Api/DashboardController@getMoreAffiliate",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *       name="pagenum",
     *       description="Page Number",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="order",
     *       description="Order",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param AffiliateRequest $request
     * @return \Illuminate\Http\Response
     */

    public function getMoreAffiliate(AffiliateRequest $request)
    {
        try {

            $page = $request->pagenum;
            $skip = ($page - 1) * 20;
            $order = $request->order;

            $my_links = ReferralLinks::getReferralLinks(20, $skip, $order);
            if (count($my_links) > 0) {
                return ApiResponse::create(
                    [
                        'my_links' => $my_links
                    ]
                );
            } else {
                return ApiResponse::create("ReferralLinks is not found");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }



    /**
     * @OA\Get(
     ** path="/dashboard/overview",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard get overview",
     *   operationId="Api/DashboardController@getOverview",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *       name="filter",
     *       description="Filter",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param OverviewRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getOverview(OverviewRequest $request)
    {
        $filter = $request->filter ?? 7;
        return ApiResponse::create($this->service->getOverview($filter));
    }



    /**
     * @OA\Get(
     ** path="/dashboard/engagement",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard get engagement",
     *   operationId="Api/DashboardController@getEngagement",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *  @OA\Parameter(
     *       name="type",
     *       description="view, comment, like, share",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="days",
     *       description="days [31 : for last 31 days, 7: for last week ........] : only required if get specific type data | default : 31 days",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param EngagementRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getEngagement(EngagementRequest $request)
    {
        try {


            $filter = $request->input('days', 31);
            $filter = $filter - 1;

            if ($request->has('type') && $request->type) {
                $type = strtolower($request->type);
                if (in_array($type, ['view', 'comment', 'like', 'share'])) {
                    switch ($type) {
                        case 'view':
                            $data['view'] = $this->_getEngagementByType('view', $filter, true);
                            break;
                        case 'comment':
                            $data['comment'] = $this->_getEngagementByType('comment', $filter, true);
                            break;
                        case 'like':
                            $data['like'] = $this->_getEngagementByType('like', $filter, true);
                            break;
                        case 'share':
                            $data['share'] = $this->_getEngagementByType('share', $filter, true);
                            break;
                    }
                    return ApiResponse::create($data);
                } else {
                    return ApiResponse::createValidationResponse([
                        'type' => ['type is invalid']
                    ]);
                }
            }

            $data['view'] = $this->_getEngagementByType('view', $filter, true);
            $data['comment'] = $this->_getEngagementByType('comment', $filter, true);
            $data['like'] = $this->_getEngagementByType('like', $filter, true);
            $data['share'] = $this->_getEngagementByType('share', $filter, true);
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }



    /**
     * @OA\Post(
     ** path="/dashboard/get-engagement-diginfo",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard get engagement diginfo",
     *   operationId="Api/DashboardController@getEngagementDigInfo",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="filter",
     *                  type="integer",
     *              ),
     *              @OA\Property(
     *                  property="dig_type",
     *                  description="Views|Comments|Likes|Shares",
     *                  type="string",
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param EngagementDigInfoRequest $request
     * @return View
     */
    public function getEngagementDigInfo(EngagementDigInfoRequest $request)
    {
        try {

            $filter = $request->filter;
            $dig_type = $request->dig_type;
            $progress_color = '';

            $posts = Posts::where('users_id', Auth::user()->id)
                ->pluck('id');

            switch ($dig_type) {
                case 'Views':
                    $dig_infos = PostsViews::select('*', DB::raw('count(*) as total'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('posts_id')->get();
                    $progress_color = 'orange';
                    break;
                case 'Comments':
                    $dig_infos = PostsComments::select('*', DB::raw('count(*) as total'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('posts_id')->get();
                    $progress_color = 'primary';
                    break;
                case 'Likes':
                    $dig_infos = PostsLikes::select('*', DB::raw('count(*) as total'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('posts_id')->get();
                    $progress_color = 'red';
                    break;
                case 'Shares':
                    $dig_infos = PostsShares::select('*', DB::raw('count(*) as total'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('posts_id')->get();
                    $progress_color = 'green';
                    break;
            }

            return ApiResponse::create([
                'dig_infos' => $dig_infos,
                'dig_type' => $dig_type,
                'progress_color' => $progress_color
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Get(
     ** path="/dashboard/get-ranking",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard get ranking",
     *   operationId="Api/DashboardController@getRanking",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param RankingRequest $request
     * @param RankingService $rankingService
     * @return view
     */
    public function getRanking(RankingRequest $request, RankingService $rankingService)
    {
        try {

            $user = auth()->user();
            $data = $rankingService->getProgressData($user->id);
            $data['next_badge']['url'] = check_profile_picture($data['next_badge']['url']);
            $data['user'] = $user;
            $data['badge'] = [
                'left_url' => ($data['current_badge']) ? url($data['current_badge']->badge->url) : null,
                'left_name' => ($data['current_badge']) ? $data['current_badge']->badge->name : null,
                'right_url' => ($data['next_badge']) ? url($data['next_badge']->url) : null,
                'right_name' => ($data['next_badge']) ? $data['next_badge']->name : null,
            ];

            if (!$data['user']->apply_to_badge) {
                $button_text = "Apply for the next badge";
                $is_disabled = false;
                if (!$data['has_cover'] || !$data['has_picture'] || $data['interactions_progress'] < 100 || $data['followers_progress'] < 100) {
                    $is_disabled = true;
                }
            } else {
                $button_text = "Pending...";
                $is_disabled = true;
            }
            $data['apply_button'] = [
                'text' => $button_text,
                'is_disabled' => $is_disabled,
            ];
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Post(
     ** path="/dashboard/apply-to-badge",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard apply to badge",
     *   operationId="Api/DashboardController@ApplyToBadge",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param RankingService $rankingService
     * @return view
     */
    public function ApplyToBadge(RankingService $rankingService)
    {
        try {

            $user = auth()->user();

            $progress = $rankingService->getProgressData($user->id);

            if (
                !$user->expert ||
                !$user->is_approved ||
                !$progress ||
                !$progress['has_cover'] ||
                !$progress['has_picture'] ||
                $progress['interactions_progress'] < 100 ||
                $progress['followers_progress'] < 100
            ) {
                return ApiResponse::__createBadResponse("You can not able to apply dadge now.");
            }

            $user->apply_to_badge = 1;
            $user->save();

            if (!$user->apply_to_badge) {
                $button_text = "Apply for the next badge";
                $is_disabled = false;
                if (!$progress['has_cover'] || !$progress['has_picture'] || $progress['interactions_progress'] < 100 || $progress['followers_progress'] < 100) {
                    $is_disabled = true;
                }
            } else {
                $button_text = "Pending...";
                $is_disabled = true;
            }

            $data = [];
            $data['badge'] = [
                'left_url' => ($progress['current_badge']) ? url($progress['current_badge']->badge->url) : null,
                'left_name' => ($progress['current_badge']) ? $progress['current_badge']->badge->name : null,
                'right_url' => ($progress['next_badge']) ? url($progress['next_badge']->url) : null,
                'right_name' => ($progress['next_badge']) ? $progress['next_badge']->name : null,
            ];
            $data['apply_button'] = [
                'text' => $button_text,
                'is_disabled' => $is_disabled,
            ];
            $data['badge_name'] = $data['badge']['right_name'];
            if ($data['badge']['left_url'] != null && $data['badge']['right_url'] != null) {
                $data['popup_type'] = "in_review";
            } else {
                $data['popup_type'] = "congratulation";
            }
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }



    /**
     * @OA\Get(
     ** path="/dashboard/interaction",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard get interaction",
     *   operationId="Api/DashboardController@getInteraction",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *       name="filter",
     *       description="default = 30 || [7|15|30]",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param InteractionRequest $request
     * @return view
     */
    public function getInteraction(Request $request)
    {
        try {
            $filter = $request->get('filter');
            if ($filter === null || $filter === "null" || $filter === "all" || $filter === 0) {
                $is_filter = false;
                $filter = 30;
            } else {
                $is_filter = true;
                if (!in_array($filter, [7, 15, 30])) {
                    return ApiResponse::createValidationResponse([
                        'filter' => ["invalid filter value"]
                    ]);
                }
            }

            $interaction_men_ages = $interaction_women_ages = ['18-22' => 0, '23-30' => 0, '31-36' => 0, '37-48' => 0, '49-58' => 0, '59+' => 0];
            $user_ids  = $world_countries = $countries_list = [];
            $man_count = $woman_count = 0;
            $posts = Posts::where('users_id', Auth::user()->id)
                ->pluck('id');

            $post_comment_query = PostsComments::whereIn('posts_id', $posts)->with('author.country_of_nationality');
            $post_like_query = PostsLikes::whereIn('posts_id', $posts)->with('user.country_of_nationality');
            $post_shere_query = PostsShares::whereIn('posts_id', $posts)->with('author.country_of_nationality');

            $post_commant_users = $post_comment_query->groupBy('users_id')->get();
            foreach ($post_commant_users as $post_commant_user) {
                if (!in_array($post_commant_user->author->id, $user_ids)) {
                    $user_age = get_users_age($post_commant_user->author->id);
                    $user_ids[] = $post_commant_user->author->id;
                    if ($post_commant_user->author->country_of_nationality) {
                        if (array_key_exists($post_commant_user->author->country_of_nationality->iso_code, $world_countries)) {
                            $world_countries[$post_commant_user->author->country_of_nationality->iso_code]++;
                        } else {
                            $world_countries[$post_commant_user->author->country_of_nationality->iso_code] = 1;
                        }
                    }

                    if ($user_age >= 18) {
                        if ($post_commant_user->author->gender == 1) {
                            $interaction_men_ages = $this->_getUserAges($interaction_men_ages, $user_age);
                            $man_count++;
                        } else {
                            $interaction_women_ages = $this->_getUserAges($interaction_women_ages, $user_age);
                            $woman_count++;
                        }
                    }
                }
            }

            $post_like_users = $post_like_query->groupBy('users_id')->get();
            foreach ($post_like_users as $post_like_user) {
                if (!in_array($post_like_user->user->id, $user_ids)) {
                    $user_age = get_users_age($post_like_user->user->id);
                    $user_ids[] = $post_like_user->user->id;
                    if ($post_like_user->user->country_of_nationality) {
                        if (array_key_exists($post_like_user->user->country_of_nationality->iso_code, $world_countries)) {
                            $world_countries[$post_like_user->user->country_of_nationality->iso_code]++;
                        } else {
                            $world_countries[$post_like_user->user->country_of_nationality->iso_code] = 1;
                        }
                    }

                    if ($user_age >= 18) {
                        if ($post_like_user->user->gender == 1) {
                            $interaction_men_ages = $this->_getUserAges($interaction_men_ages, $user_age);
                            $man_count++;
                        } else {
                            $interaction_women_ages = $this->_getUserAges($interaction_women_ages, $user_age);
                            $woman_count++;
                        }
                    }
                }
            }

            $post_share_users = $post_shere_query->groupBy('users_id')->get();
            foreach ($post_share_users as $post_share_user) {
                if (!in_array($post_share_user->author->id, $user_ids)) {
                    $user_age = get_users_age($post_share_user->author->id);
                    $user_ids[] = $post_share_user->author->id;

                    if ($post_share_user->author->country_of_nationality) {
                        if (array_key_exists($post_share_user->author->country_of_nationality->iso_code, $world_countries)) {
                            $world_countries[$post_share_user->author->country_of_nationality->iso_code]++;
                        } else {
                            $world_countries[$post_share_user->author->country_of_nationality->iso_code] = 1;
                        }
                    }
                    if ($user_age >= 18) {
                        if ($post_share_user->author->gender == 1) {
                            $interaction_men_ages = $this->_getUserAges($interaction_men_ages, $user_age);
                            $man_count++;
                        } else {
                            $interaction_women_ages = $this->_getUserAges($interaction_women_ages, $user_age);
                            $woman_count++;
                        }
                    }
                }
            }

            $world_countries =  array_map(function ($key, $value) {
                return array($this->service->_convertCountryIsoCode($key), $value);
            }, array_keys($world_countries), $world_countries);

            $total_count =  $man_count + $woman_count;
            $range      = collect(['18-22', '23-30', '31-36', '37-48', '49-58', '59+']);
            $interaction = [
                'range' => $range,
                'men' => [
                    'total' => $man_count,
                    'percentage' => $total_count > 0 ? round($man_count / $total_count * 100) : 0,
                    'args' => $range->map(function ($key) use ($interaction_men_ages) {
                        return ['y' => $interaction_men_ages[$key]];
                    })
                ],
                'women' => [
                    'total' => $woman_count,
                    'percentage' => $total_count > 0 ? round($woman_count / $total_count * 100) : 0,
                    'args' =>  $range->map(function ($key) use ($interaction_women_ages) {
                        return ['y' => $interaction_women_ages[$key]];
                    })
                ],
            ];
            $newResponse = [];
            if (!$is_filter) {
                $newResponse['interaction'] = $interaction;
                $newResponse['world_countries'] = collect($world_countries)->map(function ($item) {
                    return [
                        'iso_code' => $item[0],
                        'people' => $item[1],
                    ];
                });
            }
            $location_info = $this->_getInteractionCountries($filter);
            $newResponse['countries_table'] = collect($location_info['interaction_countries'])->map(function ($item) {
                $item = json_decode(json_encode($item));
                $item->flag = get_country_flag_by_iso_code($item->iso_code);
                $item->engagements = isset($item->engagements) ? $item->engagements : 0;
                $item->comments = isset($item->comments) ? $item->comments : 0;
                $item->likes = isset($item->likes) ? $item->likes : 0;
                $item->shares = isset($item->shares) ? $item->shares : 0;
                return $item;
            })->values()->toArray();

            return ApiResponse::create($newResponse);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Get(
     ** path="/dashboard/get-posts",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard get posts",
     *   operationId="Api/DashboardController@getPosts",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *       name="filter",
     *       description="Filter",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="type",
     *       description="all|summary|reports",
     *       in="query",
     *       required=true,
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param PostsRequest $request
     * @return view
     */
    public function getPosts(PostsRequest $request)
    {
        try {

            $filter = $request->filter;
            $filter = $filter - 1;
            $type = $request->type;

            switch ($type) {
                case 'all':
                    $data['summary'] = $this->_getPostsByType('summary', $filter);
                    $posts_reports = $this->_getPostsByType('reports', $filter);
                    ksort($posts_reports);
                    $data['post_reports'] = $posts_reports;

                    return ApiResponse::create($data);
                    break;
                case 'summary':
                    $data['summary'] = $this->_getPostsByType('summary', $filter);

                    return ApiResponse::create($data);
                    break;
                case 'reports':
                    $posts_reports = $this->_getPostsByType('reports', $filter);
                    ksort($posts_reports);
                    $data['post_reports'] = $posts_reports;

                    return ApiResponse::create($data);
                    break;
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }



    /**
     * @OA\Get(
     ** path="/dashboard/get-reports",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard get reports",
     *   operationId="Api/DashboardController@getReports",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *       name="filter",
     *       description="Default = 30 (means last 30 days)",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param ReportsRequest $request
     * @return view
     */
    public function getReports(ReportsRequest $request)
    {
        try {

            $filter = $request->get('filter', 30);

            $reports_views = $reports_comments = $reports_shares = $reports_likes = $reports_ids = [];
            $reports = Reports::where('users_id', Auth::user()->id)
                ->pluck('id');

            $views = ReportsViews::whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
            $comments = ReportsComments::whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
            $shares = ReportsShares::whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
            $likes = ReportsLikes::whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter))->get();

            foreach ($views as $view) {
                if (!in_array($view->reports_id, $reports_ids)) {
                    $reports_ids[] = $view->reports_id;
                }
                if (array_key_exists($view->created_at->format('Y-m-d'), $reports_views)) {
                    $reports_views[$view->created_at->format('Y-m-d')]++;
                } else {
                    $reports_views[$view->created_at->format('Y-m-d')] = 1;
                }
            }

            foreach ($comments as $comment) {
                if (!in_array($comment->reports_id, $reports_ids)) {
                    $reports_ids[] = $comment->reports_id;
                }
                if (array_key_exists($comment->created_at->format('Y-m-d'), $reports_comments)) {
                    $reports_comments[$comment->created_at->format('Y-m-d')]++;
                } else {
                    $reports_comments[$comment->created_at->format('Y-m-d')] = 1;
                }
            }

            foreach ($shares as $share) {
                if (!in_array($share->reports_id, $reports_ids)) {
                    $reports_ids[] = $share->reports_id;
                }
                if (array_key_exists($share->created_at->format('Y-m-d'), $reports_shares)) {
                    $reports_shares[$share->created_at->format('Y-m-d')]++;
                } else {
                    $reports_shares[$share->created_at->format('Y-m-d')] = 1;
                }
            }

            foreach ($likes as $like) {
                if (!in_array($like->reports_id, $reports_ids)) {
                    $reports_ids[] = $like->reports_id;
                }
                if (array_key_exists($like->created_at->format('Y-m-d'), $reports_likes)) {
                    $reports_likes[$like->created_at->format('Y-m-d')]++;
                } else {
                    $reports_likes[$like->created_at->format('Y-m-d')] = 1;
                }
            }


            for ($day = $filter - 1; $day >= 0; $day--) {
                $reports_label[] = Carbon::now()->subDays($day)->format('d');
                if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $reports_views)) {
                    $view_values[] = $reports_views[Carbon::now()->subDays($day)->format('Y-m-d')];
                } else {
                    $view_values[] = 0;
                }

                if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $reports_comments)) {
                    $comment_values[] = $reports_comments[Carbon::now()->subDays($day)->format('Y-m-d')];
                } else {
                    $comment_values[] = 0;
                }

                if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $reports_shares)) {
                    $share_values[] = $reports_shares[Carbon::now()->subDays($day)->format('Y-m-d')];
                } else {
                    $share_values[] = 0;
                }

                if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $reports_likes)) {
                    $like_values[] = $reports_likes[Carbon::now()->subDays($day)->format('Y-m-d')];
                } else {
                    $like_values[] = 0;
                }
            }

            $last_view = ReportsViews::whereHas('repots', function ($q) use ($filter) {
                $q->where('users_id', Auth::user()->id);
            })->where('created_at', '>=', Carbon::now()->subDays($filter))->orderBy('created_at', 'DESC')->first();

            $all_reports = Reports::with('cover')->whereIn('id', $reports_ids)
                ->withCount(['views' => function ($q) use ($filter) {
                    $q->where('created_at', '>=', Carbon::now()->subDays($filter));
                }])
                ->withCount(['comments' => function ($q) use ($filter) {
                    $q->where('created_at', '>=', Carbon::now()->subDays($filter));
                }])
                ->withCount(['shares' => function ($q) use ($filter) {
                    $q->where('created_at', '>=', Carbon::now()->subDays($filter));
                }])
                ->withCount(['likes' => function ($q) use ($filter) {
                    $q->where('created_at', '>=', Carbon::now()->subDays($filter));
                }])->get()->map(function ($report) {
                    $cover = asset(HOTEL_NO_PHOTO);
                    if (isset($report->cover[0]->val)) {
                        $cover = $report->cover[0]->val;
                    }
                    unset($report->cover);
                    $report->cover = $cover;


                    $report->engagement = [
                        'total' => $report->comments_count + $report->likes_count + $report->shares_count,
                        'percentage' => calculate_percent($report->comments_count + $report->likes_count + $report->shares_count),
                    ];

                    return $report;
                });

            // $tempSummary['days'] = $reports_label;
            // $tempSummary['graph'] = [
            //     'reports_views' => $view_values,
            //     'reports_comments' => $comment_values,
            //     'reports_shares' => $share_values,
            //     'reports_likes' => $like_values,
            // ];
            $summary = ['days' => collect($reports_label)->map(function ($val) {
                return (int)$val;
            }), 'reports_views' => [], 'reports_comments' => [], 'reports_shares' => [], 'reports_likes' => []];
            foreach ($view_values as $key => $value) {
                $summary['reports_views'][] = [
                    'x' => (int)$reports_label[$key],
                    'y' => $value,
                    'marker' => "$value views",
                ];
            }
            foreach ($comment_values as $key => $value) {
                $summary['reports_comments'][] = [
                    'x' => (int) $reports_label[$key],
                    'y' => $value,
                    'marker' => "$value comments",
                ];
            }
            foreach ($share_values as $key => $value) {
                $summary['reports_shares'][] = [
                    'x' => (int) $reports_label[$key],
                    'y' => $value,
                    'marker' => "$value shares",
                ];
            }
            foreach ($like_values as $key => $value) {
                $summary['reports_likes'][] = [
                    'x' => (int)$reports_label[$key],
                    'y' => $value,
                    'marker' => "$value likes",
                ];
            }


            $data['summary'] = $summary;
            $data['all_reports'] = $all_reports;
            $data['last_view'] = $last_view;

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Get(
     ** path="/dashboard/followers",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard get followers",
     *   operationId="Api/DashboardController@getFollowers",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *       name="type",
     *       description="followers, unfollowers",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="days",
     *       description="days [31 : for last 31 days, 7: for last week ........] : only required if get specific type data | default : 31 days",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param FollowersRequest $request
     * @return view
     */
    public function getFollowers(FollowersRequest $request)
    {
        try {


            $filter = $request->input('days', 31);
            $filter = $filter - 1;

            if ($request->has('type') && $request->type) {
                $type = strtolower($request->type);
                if (in_array($type, ['followers', 'unfollowers'])) {
                    switch ($type) {
                        case 'followers':
                            $data['followers'] = $this->service->_getFollowByType('follow', $filter);
                            break;
                        case 'unfollowers':
                            $data['unfollowers'] = $this->service->_getFollowByType('unfollow', $filter);
                            break;
                    }
                    return ApiResponse::create($data);
                } else {
                    return ApiResponse::createValidationResponse([
                        'type' => ['type is invalid']
                    ]);
                }
            }

            return ApiResponse::create([
                'followers' => $this->service->_getFollowByType('follow', $filter),
                'unfollowers' => $this->service->_getFollowByType('unfollow', $filter)
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Get(
     ** path="/dashboard/load-more-followers",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard load more followers",
     *   operationId="Api/DashboardController@getMoreFollowers",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *       name="filter",
     *       description="Filter",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="pagenum",
     *       description="Pagenum",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="type",
     *       description="followers|unfollowers",
     *       in="query",
     *       required=true,
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param MoreFollowersRequest $request
     * @return view
     */
    public function getMoreFollowers(MoreFollowersRequest $request)
    {
        try {

            $filter = $request->filter;
            $filter = $filter - 1;
            $type = $request->type;
            $page = $request->pagenum;
            $skip = ($page - 1) * 10;

            switch ($type) {
                case 'followers':
                    $followers_list = UsersFollowers::where('users_id', Auth::user()->id)->where('follow_type', 1)->where('created_at', '>=', Carbon::now()->subDays($filter))->skip($skip)->take(10)->get();

                    return ApiREsponse::create([
                        'follows' => $followers_list
                    ]);
                    break;
                case 'unfollowers':
                    $followers_list = UsersFollowers::where('users_id', Auth::user()->id)->where('follow_type', 2)->where('created_at', '>=', Carbon::now()->subDays($filter))->skip($skip)->take(10)->get();

                    return ApiREsponse::create([
                        'follows' => $followers_list
                    ]);
                    break;
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Get(
     ** path="/dashboard/global-impact",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard get global Impact",
     *   operationId="Api/DashboardController@getGlobalImpact",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param GlobalImpactRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getGlobalImpact(GlobalImpactRequest $request)
    {
        $result = $this->service->getGlobalImpact();
        $leaders = [];
        $expList = [];

        foreach ($result['leaderboard_lists'] as $countryId => $users) {
            $leaders[] = [
                'country_id' => $countryId,
                'users' => $users
            ];
        }

        foreach ($result['exp_lists'] as $countryId => $title) {
            $expList[] = [
                'country_id' => $countryId,
                'title' => $title
            ];
        }

        $result['leaderboard_lists'] = $leaders;
        $result['exp_lists'] = $expList;

        return ApiResponse::create($result);
    }


    /**
     * @OA\Get(
     ** path="/dashboard/load-more-leader",
     *   tags={"Dashboard Expert"},
     *   summary="Dashboard load more leader",
     *   operationId="Api/DashboardController@getMoreLeader",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *       name="pagenum",
     *       description="Pagenum",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param MoreLeaderRequest $request
     * @return JsonResponse
     */
    public function getMoreLeader(MoreLeaderRequest $request)
    {
        try {

            $page = $request->pagenum;
            $skip = ($page - 1) * 10;
            $view_list = '';
            $expertise_country_lists = [];
            $filter_country_id = $request->country_id;

            $leader_badge_lists = collect();

            if (count($leader_badge_lists) > 0) {
                foreach ($leader_badge_lists as $leader_badge_list) {
                    if ($leader_badge_list->point_author->expert == 1 && $leader_badge_list->point_author->nationality != 0) {
                        $expertise_country_lists[$leader_badge_list->point_author->nationality] = @$leader_badge_list->point_author->country_of_nationality->transsingle->title;
                    }
                }

                $view_list .= view('site.dashboard.partials._global-impact-leader-block', ['leader_badge_lists' => $leader_badge_lists]);
            }

            return ApiResponse::create([
                'expertise_country_lists'   => $expertise_country_lists,
                'view_list'                 => $view_list
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @param string $post_type
     * @param int $filter
     * @return array
     */
    protected function _getPostsByType($post_type, $filter)
    {
        $post_reports = [];
        $post_images_list = [];
        $post_videos_list = [];
        $post_texts = $this->service->_getOverviewByType('post_text', $filter, 'post');
        $post_images = $this->service > _getOverviewByType('post_image', $filter, 'post');
        $post_videos = $this->service->_getOverviewByType('post_video', $filter, 'post');
        $trip_plans = $this->service->_getOverviewByType('trip_plan', $filter, 'post');
        $reports = $this->service->_getOverviewByType('reports', $filter, 'post');
        $links = $this->service->_getOverviewByType('links', $filter, 'post');

        if ($post_type == 'summary') {
            $data['post_view'] = $this->service->_getOverviewByType('post_view', $filter);
            $data['texts_count'] = $post_texts['count'];
            $data['images_count'] = $post_images['count'];
            $data['videos_count'] = $post_videos['count'];
            $data['trip_plans_count'] = $trip_plans['count'];
            $data['reports_count'] = $reports['count'];
            $data['links_count'] = $links['count'];

            return $data;
        } else {
            foreach ($post_texts['post_info'] as $post_text) {
                $post_reports[strtotime($post_text->created_at)] = [
                    'type' => 'text',
                    'title' => $post_text->text,
                    'icon' => 'trav-post-icon',
                    'views_count' => $post_text->views_count,
                    'engag_count' => $post_text->comments_count + $post_text->shares_count + $post_text->likes_count,
                    'click_count' => 0
                ];
            }

            foreach ($post_images['post_info'] as $post_image) {
                if ($post_image->medias[0] && !in_array($post_image->medias[0]->media->url, $post_images_list)) {
                    $post_reports[strtotime($post_image->created_at)] = [
                        'type' => 'image',
                        'title' => ($post_image->text ? $post_image->text : ''),
                        'icon' => 'trav-camera',
                        'views_count' => $post_image->views_count,
                        'engag_count' => $post_image->comments_count + $post_image->shares_count + $post_image->likes_count,
                        'click_count' => 0
                    ];

                    $post_images_list[] = $post_image->medias[0]->media->url;
                }
            }

            foreach ($post_videos['post_info'] as $post_video) {
                if ($post_video->medias[0] && !in_array($post_video->medias[0]->media->url, $post_videos_list)) {
                    $post_reports[strtotime($post_video->created_at)] = [
                        'type' => 'video',
                        'title' => ($post_video->text ? $post_video->text : ''),
                        'icon' => 'trav-video-icon',
                        'views_count' => $post_video->views_count,
                        'engag_count' => $post_video->comments_count + $post_video->shares_count + $post_video->likes_count,
                        'click_count' => 0
                    ];

                    $post_videos_list[] = $post_video->medias[0]->media->url;
                }
            }

            foreach ($trip_plans['post_info'] as $trip_plan) {
                $post_reports[strtotime($trip_plan->created_at)] = [
                    'type' => 'plan',
                    'title' => $trip_plan->title,
                    'icon' => 'trav-trip-plan-loc-icon',
                    'views_count' => 0,
                    'engag_count' => $trip_plan->comments_count + $trip_plan->shares_count + $trip_plan->likes_count,
                    'click_count' => 0
                ];
            }

            foreach ($reports['post_info'] as $report) {
                $post_reports[strtotime($report->created_at)] = [
                    'type' => 'report',
                    'title' => $report->title,
                    'icon' => 'trav-report-icon',
                    'views_count' => $report->views_count,
                    'engag_count' => $report->comments_count + $report->shares_count + $report->likes_count,
                    'click_count' => 0
                ];
            }

            foreach ($links['post_info'] as $link) {
                $post_reports[strtotime($link->created_at)] = [
                    'type' => 'link',
                    'title' => $link->target,
                    'icon' => 'trav-link',
                    'views_count' => $link->clicks_count,
                    'engag_count' => $link->exits_count,
                    'click_count' => $link->exits_count
                ];
            }

            return $post_reports;
        }
    }


    /**
     * @param string $engagment_type
     * @param int $filter
     * @return array
     */
    protected function _getEngagementByType($engagment_type, $filter, $withoutJsonEncode = false)
    {
        $daily_type = [];

        $posts = Posts::where('users_id', Auth::user()->id)
            ->pluck('id');

        switch ($engagment_type) {
            case 'view':
                //Engagement views
                $daily_count = PostsViews::whereIn('posts_id', $posts)->where('created_at', '>', Carbon::now()->subDays(1))->count();
                $total_count = PostsViews::whereIn('posts_id', $posts)->where('created_at', '<', Carbon::now()->subDays($filter))->count();
                $engagments = PostsViews::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('day')->get();
                break;
            case 'comment':
                //Engagement comment
                $daily_count = PostsComments::whereIn('posts_id', $posts)->where('created_at', '>', Carbon::now()->subDays(1))->count();
                $total_count = PostsComments::whereIn('posts_id', $posts)->where('created_at', '<', Carbon::now()->subDays($filter))->count();
                $engagments = PostsComments::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('day')->get();
                break;
            case 'like':
                //Engagement like
                $daily_count = PostsLikes::whereIn('posts_id', $posts)->where('created_at', '>', Carbon::now()->subDays(1))->count();
                $total_count = PostsLikes::whereIn('posts_id', $posts)->where('created_at', '<', Carbon::now()->subDays($filter))->count();
                $engagments = PostsLikes::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('day')->get();
                break;
            case 'share':
                //Engagement share
                $daily_count = PostsShares::whereIn('posts_id', $posts)->where('created_at', '>', Carbon::now()->subDays(1))->count();
                $total_count = PostsShares::whereIn('posts_id', $posts)->where('created_at', '<', Carbon::now()->subDays($filter))->count();
                $engagments = PostsShares::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('day')->get();
                break;
        }



        foreach ($engagments as $engagment) {
            $daily_type[$engagment->day] = $engagment->total;
        }

        for ($day = $filter; $day >= 0; $day--) {
            $engagment_label[] = Carbon::now()->subDays($day)->format('d');
            if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $daily_type)) {
                $daily_values[] = $daily_type[Carbon::now()->subDays($day)->format('Y-m-d')];
                $total_values[] = $total_count + $daily_type[Carbon::now()->subDays($day)->format('Y-m-d')];
            } else {
                $daily_values[] = 0;
                $total_values[] = $total_count;
            }
        }

        if ($withoutJsonEncode == false) {
            $data['engagement_label'] = json_encode($engagment_label);
            $data['daily_values'] = json_encode($daily_values);
            $data['total_values'] = json_encode($total_values);
        } else {
            $data['engagement_label'] = $engagment_label;
            $data['daily_values'] = $daily_values;
            $data['total_values'] = $total_values;
        }

        $data['daily_count'] = $daily_count;

        return $data;
    }


    protected  function _getInteractionCountries($filter)
    {
        $interaction_countries = [];
        $posts = Posts::where('users_id', Auth::user()->id)
            ->pluck('id');

        $post_comment_query = PostsComments::whereIn('posts_id', $posts);
        $post_like_query = PostsLikes::whereIn('posts_id', $posts);
        $post_shere_query = PostsShares::whereIn('posts_id', $posts);

        $post_commant_countries = $post_comment_query->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
        foreach ($post_commant_countries as $post_commant_country) {
            if ($post_commant_country->author && $post_commant_country->author->country_of_nationality) {
                if (array_key_exists($post_commant_country->author->country_of_nationality->id, $interaction_countries)) {
                    $interaction_countries[$post_commant_country->author->country_of_nationality->id]['engagements']++;
                    $interaction_countries[$post_commant_country->author->country_of_nationality->id]['comments']++;
                } else {
                    $interaction_countries[$post_commant_country->author->country_of_nationality->id] = [
                        'engagements' => 1,
                        'comments' => 1,
                        'country_name' => $post_commant_country->author->country_of_nationality->transsingle->title,
                        'iso_code' => strtolower($post_commant_country->author->country_of_nationality->iso_code),
                    ];
                }
            }
        }



        $post_like_countries = $post_like_query->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
        foreach ($post_like_countries as $post_like_country) {
            if ($post_like_country->user && $post_like_country->user->country_of_nationality) {
                if (array_key_exists($post_like_country->user->country_of_nationality->id, $interaction_countries)) {
                    $interaction_countries[$post_like_country->user->country_of_nationality->id]['engagements']++;
                    if (array_key_exists('like', $interaction_countries[$post_like_country->user->country_of_nationality->id])) {
                        $interaction_countries[$post_like_country->user->country_of_nationality->id]['likes']++;
                    } else {
                        $interaction_countries[$post_like_country->user->country_of_nationality->id]['likes'] = 1;
                    }
                } else {
                    $interaction_countries[$post_like_country->user->country_of_nationality->id] = [
                        'engagements' => 1,
                        'likes' => 1,
                        'country_name' => $post_like_country->user->country_of_nationality->transsingle->title,
                        'iso_code' => strtolower($post_like_country->user->country_of_nationality->iso_code),
                    ];
                }
            }
        }

        $post_share_countries = $post_shere_query->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
        foreach ($post_share_countries as $post_share_country) {
            if ($post_share_country->author && $post_share_country->author->country_of_nationality) {
                if (array_key_exists($post_share_country->author->country_of_nationality->id, $interaction_countries)) {
                    $interaction_countries[$post_share_country->author->country_of_nationality->id]['engagements']++;

                    if (array_key_exists('shares', $interaction_countries[$post_share_country->author->country_of_nationality->id])) {
                        $interaction_countries[$post_share_country->author->country_of_nationality->id]['shares']++;
                    } else {
                        $interaction_countries[$post_share_country->author->country_of_nationality->id]['shares'] = 1;
                    }
                } else {
                    $interaction_countries[$post_share_country->author->country_of_nationality->id] = [
                        'engagement' => 1,
                        'shares' => 1,
                        'country_name' => $post_share_country->author->country_of_nationality->transsingle->title,
                        'iso_code' => strtolower($post_share_country->author->country_of_nationality->iso_code),
                    ];
                }
            }
        }

        return ['interaction_countries' => $interaction_countries];
    }

    protected function _getUserAges($age_arr, $user_age)
    {
        if (18 <= $user_age && $user_age < 22) {
            $age_arr['18-22']++;
        } else if (23 <= $user_age && $user_age < 30) {
            $age_arr['23-30']++;
        } else if (31 <= $user_age && $user_age < 36) {
            $age_arr['31-36']++;
        } else if (37 <= $user_age && $user_age < 48) {
            $age_arr['37-48']++;
        } else if (49 <= $user_age && $user_age < 58) {
            $age_arr['49-58']++;
        } else if (59 <= $user_age) {
            $age_arr['59+']++;
        }

        return $age_arr;
    }
}
