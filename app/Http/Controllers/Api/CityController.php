<?php

namespace App\Http\Controllers\Api;

use App\Http\Constants\CommonConst;
use App\Http\Controllers\Api\Traits\CityLocationTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CCPNewsfeedRequest;
use App\Http\Responses\ApiResponse;
use Illuminate\Database\Eloquent\Model;
use App\Services\Api\ShareService;
use  App\Models\Posts\PostsShares;
use Illuminate\Http\Request;
use App\Models\City\Cities;
use App\Models\City\CitiesFollowers;
use App\Models\City\CitiesMedias;
use App\Models\City\CitiesShares;
use App\Models\ActivityLog\ActivityLog;

use Illuminate\Support\Facades\Auth;
use App\Models\Access\Language\Languages;
use App\Models\ActivityMedia\Media;
use App\Models\City\CitiesTranslations;
use App\Models\Country\Countries;
use App\Models\Place\Place;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Posts\Checkins;
use App\Models\Posts\PostsCheckins;

use App\Models\TravelMates\TravelMatesRequests;
use App\Models\Reports\ReportsInfos;
use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Reports\Reports;
use App\Models\Reviews\Reviews;
use App\Models\Reviews\ReviewsVotes;
use App\Models\Reviews\ReviewsShares;

use App\Models\TripPlans\TripPlans;
use App\Models\TripPlaces\TripPlaces;
use App\Models\Posts\PostsCities;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsMedia;
use App\Models\Events\Events;
use App\Models\Events\EventsComments;
use App\Models\Events\EventsCommentsMedias;
use App\Models\Events\EventsCommentsLikes;
use App\Models\Events\EventsLikes;
use App\Models\Events\EventsShares;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\Place\PlaceFollowers;
use App\Models\Reviews\ReviewsMedia;
use App\Models\User\User;
use App\Models\User\UsersMedias;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Newsfeed\CCPNewsfeed;
use App\Services\Api\PrimaryPostService;
use App\Services\Newsfeed\HomeService;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use Carbon\Carbon;

use Intervention\Image\ImageManagerStatic as Image;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    public function __construct()
    {
        if (in_array(request()->route()->uri, optionalAuthRoutes(self::class))) {
            if (request()->bearerToken()) {
                $this->middleware('auth:api');
            }
        }
    }

    use CityLocationTrait;


    /**
     * @OA\Get(
     ** path="/city/{id}/newsfeed",
     *   tags={"Cities"},
     *   summary="Follow the city",
     *   operationId="app\Http\Controllers\Api\CityController@showNewsfeed",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="id",
     *        description="City Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="page",
     *        description="pagination page number",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="tab",
     *        description="new-posts | top-posts | discussions | trip-plans | reports | events | medias",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="sub_type",
     *        description="country | city | place -> according to algorithm",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function showNewsfeed(CCPNewsfeedRequest $request, CCPNewsfeed $newsfeed, $id)
    {
        $obj = Cities::find($id);
        if (!$obj) return ApiResponse::__createBadResponse("City not found");

        $page       = (int) $request->get('page', 1);
        $per_page   = 10;
        $tab        = strtolower(trim($request->get('tab', CommonConst::CCP_TAB_NEW_POSTS)));
        $sub_type   = request()->get('sub_type', CCPNewsfeed::SUB_TYPES[HomeService::TYPE_CITY][0]);
        $filter     = request()->except(['page', 'per_page', 'tab']);

        if (!in_array($tab, CommonConst::CCP_TAB_MAPPING)) return ApiResponse::createValidationResponse(["tab" => ["invalid tab"]]);
        if ($tab == CommonConst::CCP_TAB_REVIEWS) return ApiResponse::createValidationResponse(["tab" => ["review section not available for city"]]);

        $newsfeed->set($obj, $tab, $sub_type, $filter, $page, $per_page);
        return ApiResponse::create($newsfeed->get());
    }

    /**
     * @OA\Get(
     ** path="/city/{id}/about",
     *   tags={"Cities"},
     *   summary="get city about data",
     *   operationId="app\Http\Controllers\Api\CityController@aboutTab",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="id",
     *        description="country Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function aboutTab($id)
    {
        $obj = Cities::find($id);
        if (!$obj) return ApiResponse::__createBadResponse("City not found");
        $tabs = [];

        $tabs['weather'] = __prepared_ccp_weather(_ccp_weather($obj));

        // return ApiResponse::create(preparedReportCountryDetails($obj));
        return ApiResponse::create($tabs);
    }


    /**
     * @OA\Get(
     ** path="/city/{city_id}/follow",
     *   tags={"Cities"},
     *   summary="Follow the city",
     *   operationId="follow_the_city",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="city_id",
     *        description="City Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function postFollow($cityId)
    {
        try {
            $userId = Auth::user()->id;
            $city = Cities::find($cityId);
            if (!$city) {
                return ApiResponse::create([
                    'data' => [
                        'error' => 400,
                        'message' => 'Invalid City ID',
                    ],
                    'success' => false
                ]);
            }

            $follower = CitiesFollowers::where('cities_id', $cityId)
                ->where('users_id', $userId)
                ->first();

            if ($follower) {
                return ApiResponse::create([
                    'data' => [
                        'error' => 400,
                        'message' => 'This user is already following that City',
                    ],
                    'success' => false
                ]);
            }

            $city->followers()->create([
                'users_id' => $userId
            ]);

            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('City', 'follow', $cityId);

            $count = CitiesFollowers::where('cities_id', $cityId)->count();

            return ApiResponse::create([
                'success' => true,
                'count' => $count
            ]);
        } catch (\Throwable $th) {
            return ApiResponse::createServerError($th);
        }
    }

    /**
     * @OA\Get(
     ** path="/city/{city_id}/unfollow",
     *   tags={"Cities"},
     *   summary="Unfollow the city",
     *   operationId="unfollow_the_city",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="city_id",
     *        description="City Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function postUnFollow($placeId)
    {
        try {
            $userId = Auth::user()->id;

            $place = Cities::find($placeId);
            if (!$place) {
                return ApiResponse::create([
                    'data' => [
                        'error' => 400,
                        'message' => 'Invalid City ID',
                    ],
                    'success' => false
                ]);
            }

            $follower = CitiesFollowers::where('cities_id', $placeId)
                ->where('users_id', $userId);

            if (!$follower->first()) {
                return ApiResponse::create([
                    'data' => [
                        'error' => 400,
                        'message' => 'You are not following this City.',
                    ],
                    'success' => false
                ]);
            } else {
                $follower->delete();

                log_user_activity('City', 'unfollow', $placeId);
            }

            $count = CitiesFollowers::where('cities_id', $placeId)->count();

            return ApiResponse::create([
                'success' => true,
                'count' => $count
            ]);
        } catch (\Throwable $th) {
            return ApiResponse::createServerError($th);
        }
    }

    // public function postCheckFollow($placeId)
    // {
    //     try {
    //         $userId = Auth::user()->id;
    //         $place = Cities::find($placeId);
    //         if (!$place) {
    //             return Apiresponse::create([
    //                 'data' => [
    //                     'error' => 400,
    //                     'message' => 'Invalid City ID',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $follower = CitiesFollowers::where('cities_id', $placeId)
    //             ->where('users_id', $userId)
    //             ->first();

    //         return Apiresponse::create(empty($follower) ? ['success' => false] : ['success' => true]);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }


    // public function postCheckin($placeId, Request $request)
    // {
    //     try {
    //         $user = Auth::user();
    //         if ($user) {
    //             $userId = $user->id;
    //             $place  = Cities::find($placeId);
    //             if (!$place) {
    //                 return ApiResponse::create(
    //                     [
    //                         'message' => ["Invalid City ID"]
    //                     ],
    //                     false,
    //                     ApiResponse::BAD_REQUEST
    //                 );
    //             }

    //             if (!$request->checkin_date) {
    //                 return ApiResponse::create(
    //                     [
    //                         'checkin_date' => ['checkin_date must be required']
    //                     ],
    //                     false,
    //                     ApiResponse::VALIDATION
    //                 );
    //             }

    //             $check = new Checkins();
    //             $postCheckedIn = $request->checkin_date;
    //             if (isset($postCheckedIn)) {
    //                 $check->checkin_time = date("Y-m-d", strtotime($request->checkin_date));
    //             }
    //             $check->users_id = $userId;
    //             $check->city_id = $placeId;
    //             $check->location = $place->transsingle->title;
    //             $check->lat_lng = $place->lat . ',' . $place->lng;
    //             $check->save();

    //             log_user_activity('City', 'checkin', $placeId);

    //             if (strtotime($check->checkin_time) == strtotime(date('Y-m-d'))) {
    //                 return ApiResponse::create([
    //                     'live'          => 1,
    //                     'checkin_id'    => $check->id,
    //                     'message'       => 'Thank you for checking in! You are live on this page now.'
    //                 ]);
    //             } else if (strtotime($check->checkin_time) < strtotime(date('Y-m-d'))) {
    //                 return ApiResponse::create([
    //                     'live'          => 0,
    //                     'checkin_id'    => $check->id,
    //                     'message'       => 'Thank you for recording your past check-in!'
    //                 ]);
    //             } else if (strtotime($check->checkin_time) > strtotime(date('Y-m-d'))) {
    //                 return ApiResponse::create([
    //                     'live'          => 0,
    //                     'checkin_id'    => $check->id,
    //                     'message'       => 'Thank you for checking in! You will appear live on this page on your check-in date.'
    //                 ]);
    //             }
    //         } else {
    //             return ApiResponse::create(['message' => ["Unauthorized user."]], false, ApiResponse::UNAUTHORIZED);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function postCheckout($placeId)
    // {

    //     $userId = Auth::guard('user')->user()->id;

    //     $place = Cities::find($placeId);
    //     if (!$place) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid City ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $follower = CitiesFollowers::where('cities_id', $placeId)
    //         ->where('users_id', $userId);

    //     if (!$follower->first()) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'You are not following this City.',
    //             ],
    //             'success' => false
    //         ];
    //     } else {
    //         $follower->delete();

    //         log_user_activity('City', 'checkout', $placeId);
    //     }

    //     return [
    //         'success' => true
    //     ];
    // }

    // public function postCheckCheckin($placeId)
    // {

    //     $userId = Auth::guard('user')->user()->id;
    //     $place = Cities::find($placeId);
    //     if (!$place) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid City ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $checkin = Checkins::where('city_id', $placeId)
    //         ->where('users_id', $userId)
    //         ->first();

    //     return empty($checkin) ? ['success' => false] : ['success' => true];
    // }

    // public function postTalkingAbout($placeId)
    // {

    //     $place = Cities::find($placeId);
    //     if (!$place) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid City ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $shares = $place->shares;

    //     foreach ($shares as $share) {
    //         $data['shares'][] = array(
    //             'user_id' => $share->user->id,
    //             'user_profile_picture' => check_profile_picture(@$share->user->profile_picture)
    //         );
    //     }

    //     $data['num_shares'] = count($shares);
    //     $data['success'] = true;
    //     return json_encode($data);

    //     //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    // }

    // public function postNowInPlace($placeId)
    // {

    //     $place = Cities::find($placeId);
    //     if (!$place) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid City ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $checkins = Checkins::where('city_id', $place->id)
    //         ->orderBy('id', 'DESC')
    //         ->take(5)
    //         ->get();

    //     $result = array();
    //     $done = array();
    //     foreach ($checkins as $checkin) {
    //         if (!isset($done[$checkin->post_checkin->post->author->id])) {
    //             if (time() - strtotime($checkin->post_checkin->post->date) < (24 * 60 * 60)) {
    //                 $live = 1;
    //             } else {
    //                 $live = 0;
    //             }
    //             $result[] = array(
    //                 'name' => $checkin->post_checkin->post->author->name,
    //                 'id' => $checkin->post_checkin->post->author->id,
    //                 'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
    //                 'live' => $live
    //             );
    //             $done[$checkin->post_checkin->post->author->id] = true;
    //         }
    //     }
    //     $data['live_checkins'] = $result;

    //     $data['success'] = true;
    //     return json_encode($data);

    //     //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    // }


    // public function PostReview(Request $request)
    // {
    //     try {
    //         $validation = Validator::make(
    //             $request->all(),
    //             [
    //                 'text'          => 'required',
    //                 'score'         => 'required|integer'
    //             ],
    //             [
    //                 'score.integer'  => 'score should be numeric.'
    //             ]
    //         );
    //         $placeId = $request->get('city_id');
    //         if ($validation->fails()) {
    //             return ApiResponse::create($validation->errors(), false, ApiResponse::VALIDATION);
    //         } else {
    //             $place = Cities::find($placeId);
    //             if (!$place) {
    //                 return ApiResponse::create(
    //                     [
    //                         'message' => ['Invalid Country ID']
    //                     ],
    //                     false,
    //                     ApiResponse::BAD_REQUEST
    //                 );
    //             }

    //             if ($request->has('text')) {
    //                 $review_text = $request->text;
    //                 $review_score = $request->score;
    //                 $review_author = Auth::user();
    //                 $review_place_id = $place->id;

    //                 $review = new Reviews;
    //                 $review->cities_id = $review_place_id;
    //                 $review->by_users_id = $review_author->id;
    //                 $review->score = $review_score * 1;
    //                 $review->text = $review_text;

    //                 $userId = Auth::user()->id;
    //                 $userName = Auth::user()->name;

    //                 if ($review->save()) {
    //                     log_user_activity('City', 'review', $place->id);

    //                     $data['review'] = $review;
    //                     // $data['place'] =$place;

    //                     if ($request->has('mediafiles')) {
    //                         foreach ($request->mediafiles as $media) {
    //                             if ($media) {
    //                                 $media_url = $media;

    //                                 $media_model = new Media();
    //                                 $media_model->url = $media_url;
    //                                 $media_model->author_name = $userName;
    //                                 $media_model->title = $request->text;
    //                                 $media_model->author_url = '';
    //                                 $media_model->source_url = '';
    //                                 $media_model->license_name = '';
    //                                 $media_model->license_url = '';
    //                                 $media_model->uploaded_at = date('Y-m-d H:i:s');
    //                                 $media_model->users_id = $userId;
    //                                 $media_model->type  = getMediaTypeByMediaUrl($media_model->url);
    //                                 $media_model->save();

    //                                 $users_medias = new UsersMedias();
    //                                 $users_medias->users_id = $userId;
    //                                 $users_medias->medias_id = $media_model->id;
    //                                 $users_medias->save();

    //                                 $posts_medias = new ReviewsMedia();
    //                                 $posts_medias->reviews_id = $review->id;
    //                                 $posts_medias->medias_id = $media_model->id;
    //                                 $posts_medias->save();

    //                                 $places_medias = new CitiesMedias();
    //                                 $places_medias->cities_id = $place->id;
    //                                 $places_medias->medias_id = $media_model->id;
    //                                 $places_medias->save();

    //                                 $this->_create_thumbs($media_model);
    //                             }
    //                         }

    //                         $data['mediafiles'] = $request->mediafiles;
    //                     }
    //                     return ApiResponse::create($data);
    //                 } else {
    //                     return ApiResponse::create(
    //                         [
    //                             'message' => ['something went wrong']
    //                         ],
    //                         false,
    //                         ApiResponse::BAD_REQUEST
    //                     );
    //                 }
    //             } else {
    //                 return ApiResponse::create(
    //                     [
    //                         'message' => ['write something about country']
    //                     ],
    //                     false,
    //                     ApiResponse::BAD_REQUEST
    //                 );
    //             }
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function ajaxPostMorePhoto($placeId, Request $request)
    // {

    //     $language_id = 1;
    //     $num = $request->pagenum;
    //     $skip = ($num) * 6;
    //     $place = Cities::find($placeId);

    //     if (!$place) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid City ID',
    //             ],
    //             'success' => false
    //         ];
    //     }

    //     $medias = $place->getMedias;

    //     if (!$medias) {
    //         return [
    //             'data' => [
    //                 'error' => 400,
    //                 'message' => 'Invalid City ID',
    //             ],
    //             'success' => false
    //         ];
    //     }
    //     $data = array();
    //     $c = (count($medias) <= ($skip + 6)) ? count($medias) : $skip + 6;
    //     for ($i = $skip; $i < $c; $i++) {
    //         $data[] = '<img class="gallery__img" src="https://s3.amazonaws.com/travooo-images2/th230/' . @$medias[$i]->url . '" alt="#" title="" /> ';
    //     }
    //     return [
    //         'data' => $data,
    //         'success' => true
    //     ];
    // }

    function isImage($url)
    {
        $params = array('http' => array(
            'method' => 'HEAD'
        ));
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);
        if (!$fp)
            return false;  // Problem with url

        $meta = stream_get_meta_data($fp);
        if ($meta === false) {
            fclose($fp);
            return false;  // Problem reading data from url
        }

        $wrapper_data = $meta["wrapper_data"];
        if (is_array($wrapper_data)) {
            foreach (array_keys($wrapper_data) as $hh) {
                if (substr($wrapper_data[$hh], 0, 19) == "Content-Type: image") // strlen("Content-Type: image") == 19 
                {
                    fclose($fp);
                    return true;
                }
            }
        }

        fclose($fp);
        return false;
    }

    /**
     * @param Request $request
     * @return ApiResponse
     */
    public function uploadMedia(Request $request)
    {
        try {
            $media  = $request['mediafiles'];
            if ($media) {
                list($baseType, $media) = explode(';', $media);
                list(, $media) = explode(',', $media);
                $media = base64_decode($media);
                if (strpos($baseType, 'image') !== false)
                    $filename = sha1(microtime()) . "_place_added_file.png";
                else if (strpos($baseType, 'video') !== false)
                    $filename = sha1(microtime()) . "_place_added_file.mp4";

                $userId = Auth::user()->id;
                $userName = Auth::user()->name;
                Storage::disk('s3')->put(
                    $userId . '/medias/' . $filename,
                    $media,
                    'public'
                );
                $media_url = $userId . '/medias/' . $filename;

                return ApiResponse::create([
                    'media_url' => [$media_url]
                ]);
            } else {
                return ApiResponse::create(['message' => ["file not found"]], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return ApiResponse
     */
    public function deleteMedia(Request $request)
    {
        try {
            $media_url = $request['media_url'];
            if ($media_url) {
                if (Storage::disk('s3')->exists($media_url)) {
                    Storage::disk('s3')->delete($media_url);
                    return ApiResponse::create([
                        'message' => ["delete successfully"]
                    ]);
                } else {
                    return ApiResponse::create(['media_url' => ["file not found"]], false, ApiResponse::BAD_REQUEST);
                }
            } else {
                return ApiResponse::create(['media_url' => ["media_url must be required"]], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    // public function checkEvent($place_id, Request $request)
    // {

    //     $place = Cities::find($place_id);
    //     $client = new Client();
    //     //$client->getHttpClient()->setDefaultOption('verify', false);

    //     $data = array();
    //     $data['data'] = '';
    //     $data['cnt'] = 0;
    //     try {

    //         $result = @$client->post('https://auth.predicthq.com/token', [
    //             'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
    //             'form_params' => [
    //                 'grant_type' => 'client_credentials',
    //                 'scope' => 'account events signals'
    //             ],
    //             'verify' => false
    //         ]);
    //         $events_array = false;
    //         if ($result->getStatusCode() == 200) {
    //             $json_response = $result->getBody()->read(1024);
    //             $response = json_decode($json_response);
    //             $access_token = $response->access_token;

    //             $get_events1 = $client->get('https://api.predicthq.com/v1/events/?end.gte=' . date('Y-m-d') . '&within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
    //                 'headers' => ['Authorization' => 'Bearer ' . $access_token],
    //                 'verify' => false
    //             ]);
    //             if ($get_events1->getStatusCode() == 200) {
    //                 $events1 = json_decode($get_events1->getBody()->read(100024));
    //                 //dd($events);
    //                 $events_array1 = $events1->results;
    //                 $events_final = $events_array1;
    //                 if ($events1->next) {
    //                     $get_events2 = $client->get($events1->next, [
    //                         'headers' => ['Authorization' => 'Bearer ' . $access_token],
    //                         'verify' => false
    //                     ]);
    //                     if ($get_events2->getStatusCode() == 200) {
    //                         $events2 = json_decode($get_events2->getBody()->read(100024));
    //                         //dd($events);
    //                         $events_array2 = $events2->results;
    //                         $events_final = array_merge($events_array1, $events_array2);
    //                     }
    //                 }
    //                 $events = $events_final;

    //                 foreach ($events as $event) {
    //                     $check_envent = Events::where('provider_id', $event->id)->where('cities_id', $place->id)->get()->first();
    //                     if (!is_object($check_envent)) {
    //                         $new_event = new Events();
    //                         $new_event->cities_id = $place->id;
    //                         $new_event->provider_id = @$event->id;
    //                         $new_event->category = @$event->category;
    //                         $new_event->title = @$event->title;
    //                         $new_event->description = @$event->description;
    //                         $new_event->address = @$event->entities[0]->formatted_address;
    //                         $new_event->labels = @join(",", $event->labels);
    //                         $new_event->lat = @$event->location[1];
    //                         $new_event->lng = @$event->location[0];
    //                         $new_event->start = @$event->start;
    //                         $new_event->end = @$event->end;
    //                         $new_event->save();
    //                     }
    //                 }
    //                 $event_datas = $place->events()->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')->get();
    //                 $data['cnt'] = count($event_datas);
    //                 foreach ($event_datas as $evt) {
    //                     $data['data'] .= View::make('site.place2.partials._event_block4modal', ['evt' => $evt])->render();
    //                 }
    //             }
    //         }
    //     } catch (\Throwable $th) {
    //     }

    //     $postIds = get_postlist4placemedia($place->id, 'city');
    //     foreach ($place->getMedias as $media) {
    //         $posts = PostsMedia::where('medias_id', $media->id)->pluck('posts_id')->toArray();
    //         if (!count(array_intersect($posts, $postIds))) {
    //             $post = new Posts();
    //             $post->updated_at = $media->uploaded_at;
    //             $post->permission = 0;
    //             $post->save();
    //             $postplace = new PostsCities();
    //             $postplace->posts_id = $post->id;
    //             $postplace->cities_id = $place->id;
    //             $postplace->save();
    //             $postmedia = new PostsMedia();
    //             $postmedia->posts_id = $post->id;
    //             $postmedia->medias_id = $media->id;
    //             $postmedia->save();
    //         }
    //     }

    //     return json_encode($data);
    // }

    // public function searchAllDatas(Request $request)
    // {

    //     $query = $request->get('q');
    //     $search_in = $request->get('search_in');
    //     $search_id = $request->get('search_id');

    //     $place = Cities::find($search_id);

    //     if (!$place) {

    //         echo "";
    //     } else {
    //         $data = '';

    //         // getting discussion for place
    //         $discussions = Discussion::where('destination_type', 'City')
    //             ->where('destination_id', $place->id)
    //             ->whereRaw('(question like "%' . $query . '%" or description like "%' . $query . '%")')
    //             ->paginate(10);

    //         foreach ($discussions as $discussion) {

    //             $data .= view('site.place2.partials.post_discussion', array('discussion' => $discussion, 'place' => $place, 'search_result' => true));
    //         }


    //         // getting text post for place
    //         $post_ary = PostsCities::where('cities_id', $place->id)->pluck('posts_id')->toArray();
    //         if (count($post_ary) > 0) {
    //             $posts = Posts::whereIn('id', $post_ary)
    //                 ->whereRaw('text like "%' . $query . '%"')
    //                 ->paginate(10);

    //             foreach ($posts as $post) {

    //                 $data .= view('site.place2.partials.post_text', array('post' => $post, 'place' => $place, 'search_result' => true));
    //             }
    //         }

    //         // getting media post for place
    //         $media_ary = $place->medias->pluck('id')->toArray();
    //         $post_ary = PostsMedia::whereIn('medias_id', $media_ary)->pluck('posts_id')->toArray();
    //         if (count($post_ary) > 0) {
    //             $media = Posts::whereIn('id', $post_ary)
    //                 ->whereRaw('text like "%' . $query . '%"')
    //                 ->paginate(10);

    //             foreach ($media as $m) {
    //                 $data .= view('site.place2.partials.media_block', array('media' => $m, 'place' => $place, 'search_result' => true));
    //             }
    //         }

    //         // getting travelmates for place
    //         $mates_ary = User::whereRaw('(name like "%' . $query . '%" or interests like "%' . $query . '%")')
    //             ->pluck('id');

    //         $travelmates = TravelMatesRequests::whereHas('plan_city', function ($q) use ($place) {
    //             $q->where('cities_id', '=', $place->id);
    //         })
    //             ->whereIn('users_id', $mates_ary)
    //             ->paginate(10);
    //         // if (count($travelmates) == 0) {
    //         //     $travelmates = TravelMatesRequests::whereHas('plan_city', function($q) use ($place) {
    //         //                 $q->where('cities_id', '=', $place->city->id);
    //         //             })
    //         //             ->whereIn('users_id', $mates_ary)
    //         //             ->paginate(10);
    //         // }

    //         foreach ($travelmates as $travelmate_request) {
    //             $data .= view('site.place2.partials.post_travelmate', array('travelmate_request' => $travelmate_request, 'place' => $place, 'search_result' => true));
    //         }

    //         $reports = Reports::where('cities_id', '=', $place->id)
    //             ->get();

    //         foreach ($reports as $rp) {
    //             $data .= view('site.place2.partials.post_report', array('report' => $rp, 'place' => $place, 'search_result' => true));
    //         }

    //         // getting reviews for place
    //         $reviews = Reviews::where('cities_id', $place->id)
    //             ->whereRaw('text like "%' . $query . '%"')
    //             ->paginate(10);

    //         foreach ($reviews as $r) {
    //             $data .= view('site.place2.partials.post_review', array('review' => $r, 'place' => $place, 'search_result' => true));
    //         }


    //         //getting trip plans for place
    //         $trip_plans = TripPlaces::where('cities_id', $place->id)->pluck('trips_id')->toArray();

    //         if (count($trip_plans) > 0) {
    //             $trips = TripPlans::whereIn('id', $trip_plans)
    //                 ->whereRaw('title like "%' . $query . '%"')
    //                 ->paginate(10);

    //             foreach ($trips as $tr) {
    //                 $data .= view('site.place2.partials.post_plan', array('plan' => $tr, 'place' => $place, 'search_result' => true));
    //             }
    //         }

    //         //getting events for place
    //         $events = array();
    //         try {
    //             $client = new Client();
    //             $result = @$client->post('https://auth.predicthq.com/token', [
    //                 'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
    //                 'form_params' => [
    //                     'grant_type' => 'client_credentials',
    //                     'scope' => 'account events signals'
    //                 ],
    //                 'verify' => false
    //             ]);
    //             $events_array = false;
    //             if ($result->getStatusCode() == 200) {
    //                 $json_response = $result->getBody()->read(1024);
    //                 $response = json_decode($json_response);
    //                 $access_token = $response->access_token;

    //                 $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
    //                     'headers' => ['Authorization' => 'Bearer ' . $access_token],
    //                     'verify' => false
    //                 ]);
    //                 if ($get_events1->getStatusCode() == 200) {
    //                     $events1 = json_decode($get_events1->getBody()->read(100024));
    //                     //dd($events);
    //                     $events_array1 = $events1->results;
    //                     $events_final = $events_array1;
    //                     if ($events1->next) {
    //                         $get_events2 = $client->get($events1->next, [
    //                             'headers' => ['Authorization' => 'Bearer ' . $access_token],
    //                             'verify' => false
    //                         ]);
    //                         if ($get_events2->getStatusCode() == 200) {
    //                             $events2 = json_decode($get_events2->getBody()->read(100024));
    //                             //dd($events);
    //                             $events_array2 = $events2->results;
    //                             $events_final = array_merge($events_array1, $events_array2);
    //                         }
    //                     }
    //                     $events = $events_final;
    //                 }
    //             }

    //             if (count($events) > 0) {
    //                 foreach ($events as $evt) {
    //                     if (strpos($evt->title, $query) || strpos($evt->description, $query) || strpos(@$evt->entities[0]->formatted_address, $query)) {
    //                         $data .= view('site.place2.partials.post_event', array('evt' => $evt, 'place' => $place, 'search_result' => true));
    //                     }
    //                 }
    //             }
    //         } catch (\Throwable $th) {
    //         }
    //         echo $data;
    //     }
    // }

    // public function discussion_replies_24hrs($place_id, Request $request)
    // {

    //     $place = Cities::find($place_id);
    //     $user_id = $request->get('user_id');

    //     $filters = View::make('site.place2.partials._posts_filters_block');
    //     $output = $filters->render();

    //     $output = '';

    //     $posts_discussion_collection = array();
    //     $get_posts_discussion_collection = DiscussionReplies::whereHas('discussion', function ($q) use ($place_id) {
    //         $q->where('destination_type', 'city')
    //             ->where('destination_id', $place_id);
    //     })
    //         ->where('created_at', '>', date("Y-m-d", strtotime('-24 hours')))
    //         ->where('users_id', $user_id)
    //         ->orderBy('id', 'DESC')
    //         ->get();
    //     if (count($get_posts_discussion_collection) > 0) {
    //         foreach ($get_posts_discussion_collection as $gpdc) {
    //             $posts_discussion_collection[] = array(
    //                 'id' => $gpdc->discussions_id,
    //                 'type' => 'discussion',
    //                 'timestamp' => $gpdc->created_at
    //             );

    //             $view = View::make('site.place2.partials.post_discussion', [
    //                 'place' => $place,
    //                 'discussion' => $gpdc->discussion
    //             ]);
    //             $output .= $view->render();
    //         }
    //     } else {
    //         $output = 'no';
    //     }
    //     return json_encode($output);
    // }

    // public function newsfeed_show_all($place_id, Request $request)
    // {
    //     try {
    //         $place = Cities::find($place_id);
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');

    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();

    //         $output = [];
    //         $filter = $request->all();
    //         $all_collection  = getPostsIdForAllFeed($place_id, $page, 'city', $filter);
    //         if (count($all_collection) > 0) {
    //             foreach ($all_collection as $ppost) {
    //                 $post = Posts::find($ppost['id']);
    //                 $post->medias;
    //                 $output[] = $post;
    //             }
    //         }
    //         // krsort($output);

    //         // $output = implode($output);
    //         if (!$output) {
    //             $output = 'no';
    //         }
    //         // return json_encode($output);
    //         return ApiResponse::create($output);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    // public function newsfeed_show_about($place_id, Request $request)
    // {
    //     try {
    //         $place = Cities::find($place_id);

    //         $data['city'] = $place;
    //         $data['city']->trans[0];
    //         $data['city']->timezone;
    //         $data['city']->countryTitle;
    //         $data['city']->religions;
    //         $data['city']->currencies;
    //         $data['city']->holidays;
    //         $data['city']->cityHolidays;
    //         $data['city']->languages_spoken;
    //         $data['city']->emergency;
    //         $data['country_weather'] = false;
    //         $data['current_weather'] = false;
    //         $data['daily_weather'] = false;
    //         $get_place_key = curl_init();
    //         curl_setopt_array($get_place_key, array(
    //             CURLOPT_RETURNTRANSFER => 1,
    //             CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q=' . $place->lat . ',' . $place->lng,
    //         ));
    //         $resp = curl_exec($get_place_key);
    //         curl_close($get_place_key);
    //         if ($resp) {
    //             $resp = json_decode($resp);
    //             $place_key = $resp->Key;

    //             $get_country_weather = curl_init();
    //             curl_setopt_array($get_country_weather, array(
    //                 CURLOPT_RETURNTRANSFER => 1,
    //                 CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
    //             ));
    //             $country_weather = curl_exec($get_country_weather);
    //             curl_close($get_country_weather);
    //             $data['country_weather'] = json_decode($country_weather);

    //             $get_current_weather = curl_init();
    //             curl_setopt_array($get_current_weather, array(
    //                 CURLOPT_RETURNTRANSFER => 1,
    //                 CURLOPT_URL => 'http://dataservice.accuweather.com/currentconditions/v1/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
    //             ));
    //             $current_weather = curl_exec($get_current_weather);
    //             curl_close($get_current_weather);
    //             $data['current_weather'] = json_decode($current_weather);


    //             $get_daily_weather = curl_init();
    //             curl_setopt_array($get_daily_weather, array(
    //                 CURLOPT_RETURNTRANSFER => 1,
    //                 CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/daily/10day/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
    //             ));
    //             $daily_weather = curl_exec($get_daily_weather);
    //             curl_close($get_daily_weather);
    //             $data['daily_weather'] = json_decode($daily_weather)->DailyForecasts;
    //         }

    //         // $about = View::make('site.place2.partials.post_about', $data);
    //         // $output = $about->render();

    //         return ApiResponse::create($data);

    //         // return json_encode($output);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    // public function newsfeed_show_discussions($place_id, Request $request)
    // {
    //     try {
    //         $place = Cities::find($place_id);
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');
    //         $skip = ($page - 1) * 5;
    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();
    //         $filter = $request->all();
    //         $output = '';
    //         if (isset($filter['location']) || isset($filter['people'])) {
    //             $users_id_array = getUsersByFilters($filter);
    //             $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
    //                 ->where('destination_type', 'city')
    //                 ->whereIn('users_id', $users_id_array)
    //                 ->orderBy('id', 'desc')
    //                 ->skip($skip)
    //                 ->take(5)
    //                 ->get();
    //         } else {
    //             $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
    //                 ->where('destination_type', 'city')
    //                 ->orderBy('id', 'desc')
    //                 ->skip($skip)
    //                 ->take(5)
    //                 ->get();
    //         }

    //         if (count($get_posts_discussions_collection) > 0) {
    //             foreach ($get_posts_discussions_collection as $gpdc) {
    //                 $view = View::make('site.place2.partials.post_discussion', [
    //                     'place' => $place,
    //                     'discussion' => $gpdc
    //                 ]);
    //                 $output .= $view->render();
    //             }
    //         } else {
    //             $output = 'no';
    //         }

    //         // return json_encode($get_posts_discussions_collection);
    //         return ApiResponse::create($get_posts_discussions_collection);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    // public function newsfeed_show_top($place_id, Request $request)
    // {

    //     $place = Cities::find($place_id);
    //     $user_id = $request->get('user_id');
    //     $page = $request->get('page');
    //     //$filters = View::make('site.place2.partials._posts_filters_block');
    //     //$output = $filters->render();

    //     $output = [];
    //     $filter = $request->all();
    //     $all_collection = collect_posts('city', $place_id, $page, true, false, $filter);
    //     if (count($all_collection) > 0) {
    //         foreach ($all_collection as $ppost) {
    //             $more = '';
    //             if ($ppost['type'] == 'regular') {
    //                 $post = Posts::find($ppost['id']);
    //                 if ($post->text != '' && count($post->medias) > 0) {
    //                     $more = View::make('site.place2.partials.post_media_with_text', array('post' => $post, 'place' => $place));
    //                 } elseif ($post->text == '' && count($post->medias) > 0) {
    //                     if (is_object($post->author))
    //                         if (isset($post->medias[0]->media))
    //                             $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place));
    //                 } else {
    //                     $more = View::make('site.place2.partials.post_text', array('post' => $post, 'place' => $place));
    //                 }
    //             } elseif ($ppost['type'] == 'discussion') {
    //                 $discussion = Discussion::find($ppost['id']);
    //                 $more = View::make('site.place2.partials.post_discussion', array('discussion' => $discussion, 'place' => $place));
    //             } elseif ($ppost['type'] == 'plan') {
    //                 $plan = TripPlaces::find($ppost['id']);
    //                 if (isset($plan->trip))
    //                     $more = View::make('site.place2.partials.post_plan', array('plan' => $plan->trip, 'place' => $place));
    //             } elseif ($ppost['type'] == 'report') {
    //                 $report = Reports::find($ppost['id']);
    //                 if ($report) {
    //                     $more = View::make('site.place2.partials.post_report', array('report' => $report, 'place' => $place));
    //                 }
    //             }
    //             $timestamp = (new Carbon($ppost['timestamp']))->timestamp;
    //             if (!array_key_exists($timestamp, $output)) {
    //                 $output[$timestamp] = '';
    //             }
    //             if (isset($more) && $more != '')
    //                 $output[$timestamp] .= $more->render();
    //         }
    //     }


    //     // $output .= $this->newsfeed_show_travelmates($place_id, $request, true, true);
    //     // $output .= $this->newsfeed_show_events($place_id, $request, true);
    //     // $output .= $this->newsfeed_show_reports($place_id, $request, true, true);
    //     // $output .= $this->newsfeed_show_reviews($place_id, $request, true, true);
    //     krsort($output);

    //     $output = implode($output);
    //     if ($output == []) {
    //         $output = 'no';
    //     }


    //     return json_encode($output);
    // }

    // public function newsfeed_show_media($place_id, Request $request)
    // {
    //     try {
    //         $place = Cities::find($place_id);
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');
    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();

    //         $data = [];
    //         $filter = $request->all();
    //         $all_collection = collect_posts('city', $place_id, $page, false, true, $filter);
    //         if (count($all_collection) > 0) {
    //             foreach ($all_collection as $index => $ppost) {
    //                 if ($ppost['type'] == 'regular') {
    //                     $post = Posts::find($ppost['id']);
    //                     $post->medias;
    //                     $post->likes;
    //                     $post->author;
    //                     $data[$index] = $post;
    //                 }
    //             }
    //         } else {
    //             $data = 'no';
    //         }

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    // public function newsfeed_show_tripplan($place_id, Request $request)
    // {
    //     try {
    //         $place = Cities::find($place_id);
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');
    //         $skip = ($page - 1) * 5;
    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();

    //         $output = '';
    //         $filter = $request->all();
    //         $triplist = get_triplist4me($filter);
    //         $get_posts_plans_collection = TripPlaces::where('cities_id', $place_id)
    //             ->whereIn('trips_id', $triplist)
    //             ->orderBy('id', 'DESC')
    //             ->paginate(5);
    //         // ->skip($skip)
    //         // ->take(5)
    //         // ->get();

    //         return ApiResponse::create($get_posts_plans_collection);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    // public function newsfeed_show_travelmates($place_id, Request $request, $internal = false, $top = false)
    // {

    //     $place = Cities::find($place_id);
    //     $user_id = $request->get('user_id');
    //     $page = $request->get('page');
    //     $skip = ($page - 1) * 5;
    //     //$filters = View::make('site.place2.partials._posts_filters_block');
    //     //$output = $filters->render();

    //     $output = '';
    //     $get_posts_mates_collection = TravelMatesRequests::whereHas('plan_city', function ($q) use ($place) {
    //         $q->where('cities_id', '=', $place->id);
    //     })
    //         ->groupBy('users_id')
    //         ->orderBy('id', 'desc')
    //         ->paginate(5);
    //     // ->skip($skip)
    //     // ->take(5)
    //     // ->get();

    //     if ($get_posts_mates_collection->count() > 0) {
    //         foreach ($get_posts_mates_collection as $gpdc) {
    //             $view = View::make('site.place2.partials.post_travelmate', [
    //                 'place' => $place,
    //                 'travelmate_request' => $gpdc
    //             ]);
    //             $output .= $view->render();
    //         }
    //     } else {
    //         $output = $internal ? '' : 'no';
    //     }

    //     return $internal ? $output : json_encode($output);
    // }

    // public function newsfeed_show_events($place_id, Request $request)
    // {
    //     try {
    //         $place = Cities::find($place_id);

    //         if ($place) {
    //             $place->title = $place->trans[0]->title;
    //             unset($place->trans);

    //             $page = $request->get('page');
    //             $skip = ($page - 1) * 5;

    //             $events_collection = Events::where('cities_id', $place_id)
    //                 ->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')
    //                 ->orderBy('id', 'desc');

    //             $events_collection_total    = $events_collection->count();
    //             $events_collection          = $events_collection->skip($skip)->take(5)->get();

    //             return ApiResponse::create([
    //                 'city' => $place,
    //                 'total' => $events_collection_total,
    //                 'events' => $events_collection,
    //             ]);
    //         } else {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ["city not found"]
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function newsfeed_show_reports($place_id, Request $request, $internal = false, $top = false)
    // {

    //     $place = Cities::find($place_id);
    //     $user_id = $request->get('user_id');
    //     $page = $request->get('page');
    //     $skip = ($page - 1) * 5;
    //     //$filters = View::make('site.place2.partials._posts_filters_block');
    //     //$output = $filters->render();
    //     $output = '';
    //     $filter = $request->all();
    //     if (!$top) {
    //         if (isset($filter['location']) || isset($filter['people'])) {
    //             $users_id_array = getUsersByFilters($filter);
    //             $get_posts_reports_collection = Reports::where('cities_id', '=', $place->id)
    //                 ->whereIn('users_id', $users_id_array)
    //                 ->paginate(5);
    //             // ->skip($skip)
    //             // ->take(5)
    //             // ->get();
    //         } else {
    //             $get_posts_reports_collection = Reports::where('cities_id', '=', $place->id)
    //                 ->paginate(5);
    //         }
    //     } else {

    //         $get_posts_reports_collection = Reports::where('cities_id', '=', $place->id)
    //             ->selectRaw('*, ((select count(*) from `reports_comments` where `reports_id`=`reports`.`id`) 
    //                                 + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id`) 
    //                                 + (select count(*) from `reports_views` where `reports_id`=`reports`.`id`) 
    //                                 + (select count(*) from `reports_shares` where `reports_id`=`reports`.`id`)) as rank')
    //             ->orderBy('rank', 'DESC')
    //             ->paginate(5);
    //         // ->skip($skip)
    //         // ->take(5)
    //         // ->get();
    //     }
    //     if ($get_posts_reports_collection->count() > 0) {
    //         foreach ($get_posts_reports_collection as $gpdc) {
    //             $view = View::make('site.place2.partials.post_report', [
    //                 'place' => $place,
    //                 'report' => $gpdc
    //             ]);
    //             $output .= $view->render();
    //         }
    //     } else {
    //         $output = $internal ? '' : 'no';
    //     }

    //     return $internal ? $output : json_encode($output);
    // }


    public function newsfeed_show_reviews($place_id, Request $request, $internal = false, $top = false)
    {
        try {
            $page = $request->get('page');
            $skip = ($page - 1) * 5;

            if (!$top) {

                $get_posts_review_collection = Reviews::where('cities_id', $place_id)
                    ->orderBy('id', 'desc')
                    ->skip($skip)
                    ->take(5)
                    ->get();
            } else {
                $get_posts_review_collection = Reviews::where('cities_id', $place_id)
                    ->selectRaw('*, ((select count(*) from `reviews_votes` where `review_id`=`reviews`.`id` and `vote_type`=1) 
                                        + (select count(*) from `reviews_shares` where `review_id`=`reviews`.`id`)) as rank')
                    ->orderBy('rank', 'desc')
                    ->skip($skip)
                    ->take(5)
                    ->get();
            }
            return ApiResponse::create($get_posts_review_collection);
        } catch (\Throwable $th) {
            return ApiResponse::createServerError($th);
        }
    }


    // public function city_holiday($id)
    // {
    //     try {
    //         $city = Cities::find($id);
    //         if (!$city) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid City Id']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //         $city->trans;

    //         /* Get Holidays */
    //         $holidays = $city->holidays;
    //         $holidays_arr = [];

    //         /* If Model Exist, Get Translated Title For Each Model */
    //         if (!empty($holidays)) {
    //             foreach ($holidays as $key => $value) {
    //                 $holiday = $value;
    //                 if (!empty($holiday)) {
    //                     $holiday = $holiday->transsingle;
    //                     $holiday_date = $holiday->transsingle;
    //                     if (!empty($holiday)) {
    //                         array_push($holidays_arr, ["title" => $holiday->title, "date" => $holiday_date]);
    //                     }
    //                 }
    //             }
    //         }

    //         $city_holiday = [];
    //         foreach ($city->holidays as $item) {
    //             $holiday = [
    //                 'id' => $item->id,
    //                 'date' => @$item->pivot->date,
    //                 'title' => @$item->transsingle->title,
    //                 'description' => @$item->transsingle->description
    //             ];
    //             $city_holiday[] = $holiday;
    //             unset($item->pivot, $item->transsingle);
    //         }
    //         $city->title = $city->trans[0]->title;
    //         $city->holiday_list = $city_holiday;

    //         unset($city->trans, $city->holidays);
    //         $data['city'] = $city;

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function getWeather($city_id)
    // {
    //     try {
    //         $language_id = 1;
    //         $city = Cities::find($city_id);

    //         if (!$city) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 400,
    //                     'message' => 'Invalid City ID',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $get_place_key = curl_init();
    //         curl_setopt_array($get_place_key, array(
    //             CURLOPT_RETURNTRANSFER => 1,
    //             CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q=' . $city->lat . ',' . $city->lng,
    //         ));
    //         $resp = curl_exec($get_place_key);
    //         curl_close($get_place_key);
    //         if ($resp) {
    //             $resp = json_decode($resp);
    //             $place_key = $resp->Key;

    //             $get_country_weather = curl_init();
    //             curl_setopt_array($get_country_weather, array(
    //                 CURLOPT_RETURNTRANSFER => 1,
    //                 CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
    //             ));
    //             $country_weather = curl_exec($get_country_weather);
    //             curl_close($get_country_weather);
    //             $data['country_weather'] = json_decode($country_weather);

    //             $get_current_weather = curl_init();
    //             curl_setopt_array($get_current_weather, array(
    //                 CURLOPT_RETURNTRANSFER => 1,
    //                 CURLOPT_URL => 'http://dataservice.accuweather.com/currentconditions/v1/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
    //             ));
    //             $current_weather = curl_exec($get_current_weather);
    //             curl_close($get_current_weather);
    //             $data['current_weather'] = json_decode($current_weather);


    //             $get_daily_weather = curl_init();
    //             curl_setopt_array($get_daily_weather, array(
    //                 CURLOPT_RETURNTRANSFER => 1,
    //                 CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/daily/10day/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
    //             ));
    //             $daily_weather = curl_exec($get_daily_weather);
    //             curl_close($get_daily_weather);
    //             $data['daily_weather'] = json_decode($daily_weather)->DailyForecasts;
    //         }

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    // public function getPackingTips($city_id)
    // {
    //     try {
    //         $language_id = 1;
    //         $city = Cities::find($city_id);
    //         if (!$city) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 400,
    //                     'message' => 'Invalid City ID',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $city = Cities::with([
    //             'trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'languages_spoken.trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'holidays.trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'religions.trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'currencies.trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'emergency.trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'places' => function ($query) {
    //                 $query->where('media_count', '>', 0)
    //                     ->limit(50);
    //             },
    //             'atms' => function ($query) {
    //                 $query->where('place_type', 'like', '%atm%');
    //             },
    //             'hotels',
    //             'getMedias',
    //             'getMedias.users',
    //             'timezone',
    //             'followers'
    //         ])
    //             ->where('id', $city_id)
    //             ->where('active', 1)
    //             ->first();

    //         if (!$city) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 404,
    //                     'message' => 'Not found',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $placeCount = Place::where('cities_id', '=', $city->id)
    //             ->count();

    //         $citiesMediasSql = DB::table('cities_medias')
    //             ->select([
    //                 'cities_id',
    //                 DB::raw('max(medias_id) AS medias_id')
    //             ])
    //             ->groupBy('cities_id')
    //             ->toSql();

    //         $tripPlans = Cities::select(
    //             'medias.id AS media_id',
    //             'medias.url',
    //             'trips_cities.cities_id',
    //             'trips.id',
    //             'trips.title',
    //             'trips.users_id',
    //             'trips.created_at',
    //             'trips_places.budget',
    //             'trips_places.id'
    //         )
    //             ->join('places', 'cities.id', '=', 'places.cities_id')
    //             ->join('trips_places', function ($join) {
    //                 $join->on('trips_places.cities_id', '=', 'cities.id')
    //                     ->on('trips_places.places_id', '=', 'places.id');
    //             })
    //             ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //             ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //             ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //             ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //             ->where('cities.id', $city_id)
    //             ->get();

    //         $sumBudget = 0;
    //         foreach ($tripPlans as $tripPlan) {
    //             $sumBudget += $tripPlan['budget'];
    //             $media = [
    //                 'id' => $tripPlan['media_id'],
    //                 'url' => $tripPlan['url']
    //             ];
    //             unset($tripPlan['media_id'], $tripPlan['url']);
    //             $tripPlan['medias'] = (object) $media;
    //         }

    //         $data['plans'] = $tripPlans;
    //         $data['plans_count'] = count($tripPlans);
    //         $data['sum_budget'] = $sumBudget;

    //         $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->get();

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    // public function getEtiquette($city_id) {
    //     try {
    //         $language_id = 1;
    //         $city = Cities::find($city_id);

    //         if (!$city) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 400,
    //                     'message' => 'Invalid City ID',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $language = Languages::where('id', $language_id)->first();

    //         if (!$language) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 400,
    //                     'message' => 'Invalid Language ID',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $city = Cities::with([
    //                     'trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'languages_spoken.trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'holidays.trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'religions.trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'currencies.trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'emergency.trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'places' => function ($query) {
    //                         $query->where('media_count', '>', 0)
    //                         ->limit(50);
    //                     },
    //                     'atms' => function ($query) {
    //                         $query->where('place_type', 'like', '%atm%');
    //                     },
    //                     'hotels',
    //                     'getMedias',
    //                     'getMedias.users',
    //                     'timezone',
    //                     'followers'
    //                 ])
    //                 ->where('id', $city_id)
    //                 ->where('active', 1)
    //                 ->first();

    //         if (!$city) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 404,
    //                     'message' => 'Not found',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $placeCount = Place::where('cities_id', '=', $city->id)
    //                 ->count();
    //         $languages = $city->getLanguages();
    //         $city->setRelation('languages', collect($languages));


    //         $citiesMediasSql = DB::table('cities_medias')
    //                 ->select([
    //                     'cities_id',
    //                     DB::raw('max(medias_id) AS medias_id')
    //                 ])
    //                 ->groupBy('cities_id')
    //                 ->toSql();

    //         $tripPlans = Cities::select(
    //                         'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
    //                 )
    //                 ->join('places', 'cities.id', '=', 'places.cities_id')
    //                 ->join('trips_places', function($join) {
    //                     $join->on('trips_places.cities_id', '=', 'cities.id')
    //                     ->on('trips_places.places_id', '=', 'places.id');
    //                 })
    //                 ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //                 ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //                 ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //                 ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //                 ->where('cities.id', $city_id)
    //                 ->get();

    //         $sumBudget = 0;
    //         foreach ($tripPlans as $tripPlan) {
    //             $sumBudget += $tripPlan['budget'];
    //             $media = [
    //                 'id' => $tripPlan['media_id'],
    //                 'url' => $tripPlan['url']
    //             ];
    //             unset($tripPlan['media_id'], $tripPlan['url']);
    //             $tripPlan['medias'] = (object) $media;
    //         }

    //         $airports = Place::where('cities_id', '=', $city->id)
    //                 ->where('place_type', 'like', 'airport,%')
    //                 ->limit(10)
    //                 ->get();

    //         $data['plans'] = $tripPlans;
    //         $data['plans_count'] = count($tripPlans);
    //         $data['sum_budget'] = $sumBudget;

    //         $data['city'] = $city;
    //         $data['airports'] = $airports;


    //         $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->get();


    //         return ApiResponse::create($data);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    // public function getHealth($city_id) {
    //     try {
    //         $language_id = 1;
    //         $city = Cities::find($city_id);

    //         if (!$city) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 400,
    //                     'message' => 'Invalid City ID',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $language = Languages::where('id', $language_id)->first();

    //         if (!$language) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 400,
    //                     'message' => 'Invalid Language ID',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $city = Cities::with([
    //                     'trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'languages_spoken.trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'holidays.trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'religions.trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'currencies.trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'emergency.trans' => function ($query) use($language_id) {
    //                         $query->where('languages_id', $language_id);
    //                     },
    //                     'places' => function ($query) {
    //                         $query->where('media_count', '>', 0)
    //                         ->limit(50);
    //                     },
    //                     'atms' => function ($query) {
    //                         $query->where('place_type', 'like', '%atm%');
    //                     },
    //                     'hotels',
    //                     'getMedias',
    //                     'getMedias.users',
    //                     'timezone',
    //                     'followers'
    //                 ])
    //                 ->where('id', $city_id)
    //                 ->where('active', 1)
    //                 ->first();

    //         if (!$city) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 404,
    //                     'message' => 'Not found',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $placeCount = Place::where('cities_id', '=', $city->id)
    //                 ->count();
    //         $languages = $city->getLanguages();
    //         $city->setRelation('languages', collect($languages));


    //         $citiesMediasSql = DB::table('cities_medias')
    //                 ->select([
    //                     'cities_id',
    //                     DB::raw('max(medias_id) AS medias_id')
    //                 ])
    //                 ->groupBy('cities_id')
    //                 ->toSql();

    //         $tripPlans = Cities::select(
    //                         'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
    //                 )
    //                 ->join('places', 'cities.id', '=', 'places.cities_id')
    //                 ->join('trips_places', function($join) {
    //                     $join->on('trips_places.cities_id', '=', 'cities.id')
    //                     ->on('trips_places.places_id', '=', 'places.id');
    //                 })
    //                 ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //                 ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //                 ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //                 ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //                 ->where('cities.id', $city_id)
    //                 ->get();

    //         $sumBudget = 0;
    //         foreach ($tripPlans as $tripPlan) {
    //             $sumBudget += $tripPlan['budget'];
    //             $media = [
    //                 'id' => $tripPlan['media_id'],
    //                 'url' => $tripPlan['url']
    //             ];
    //             unset($tripPlan['media_id'], $tripPlan['url']);
    //             $tripPlan['medias'] = (object) $media;
    //         }

    //         $airports = Place::where('cities_id', '=', $city->id)
    //                 ->where('place_type', 'like', 'airport,%')
    //                 ->limit(10)
    //                 ->get();

    //         $data['plans'] = $tripPlans;
    //         $data['plans_count'] = count($tripPlans);
    //         $data['sum_budget'] = $sumBudget;

    //         $data['city'] = $city;
    //         $data['airports'] = $airports;


    //         $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    // public function getVisaRequirements($city_id)
    // {
    //     try {
    //         $language_id = 1;
    //         $city = Cities::find($city_id);

    //         if (!$city) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 400,
    //                     'message' => 'Invalid City ID',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $language = Languages::where('id', $language_id)->first();

    //         if (!$language) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 400,
    //                     'message' => 'Invalid Language ID',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $city = Cities::with([
    //             'trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'languages_spoken.trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'holidays.trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'religions.trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'currencies.trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'emergency.trans' => function ($query) use ($language_id) {
    //                 $query->where('languages_id', $language_id);
    //             },
    //             'places' => function ($query) {
    //                 $query->where('media_count', '>', 0)
    //                     ->limit(50);
    //             },
    //             'atms' => function ($query) {
    //                 $query->where('place_type', 'like', '%atm%');
    //             },
    //             'hotels',
    //             'getMedias',
    //             'getMedias.users',
    //             'timezone',
    //             'followers'
    //         ])
    //             ->where('id', $city_id)
    //             ->where('active', 1)
    //             ->first();

    //         if (!$city) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 404,
    //                     'message' => 'Not found',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }

    //         $placeCount = Place::where('cities_id', '=', $city->id)
    //             ->count();
    //         $languages = $city->getLanguages();
    //         $city->setRelation('languages', collect($languages));


    //         $citiesMediasSql = DB::table('cities_medias')
    //             ->select([
    //                 'cities_id',
    //                 DB::raw('max(medias_id) AS medias_id')
    //             ])
    //             ->groupBy('cities_id')
    //             ->toSql();

    //         $tripPlans = Cities::select(
    //             'medias.id AS media_id',
    //             'medias.url',
    //             'trips_cities.cities_id',
    //             'trips.id',
    //             'trips.title',
    //             'trips.users_id',
    //             'trips.created_at',
    //             'trips_places.budget',
    //             'trips_places.id'
    //         )
    //             ->join('places', 'cities.id', '=', 'places.cities_id')
    //             ->join('trips_places', function ($join) {
    //                 $join->on('trips_places.cities_id', '=', 'cities.id')
    //                     ->on('trips_places.places_id', '=', 'places.id');
    //             })
    //             ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
    //             ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
    //             ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
    //             ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
    //             ->where('cities.id', $city_id)
    //             ->get();

    //         $sumBudget = 0;
    //         foreach ($tripPlans as $tripPlan) {
    //             $sumBudget += $tripPlan['budget'];
    //             $media = [
    //                 'id' => $tripPlan['media_id'],
    //                 'url' => $tripPlan['url']
    //             ];
    //             unset($tripPlan['media_id'], $tripPlan['url']);
    //             $tripPlan['medias'] = (object) $media;
    //         }

    //         $airports = Place::where('cities_id', '=', $city->id)
    //             ->where('place_type', 'like', 'airport,%')
    //             ->limit(10)
    //             ->get();

    //         $data['plans'] = $tripPlans;
    //         $data['plans_count'] = count($tripPlans);
    //         $data['sum_budget'] = $sumBudget;

    //         $data['city'] = $city;
    //         $data['airports'] = $airports;

    //         $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    /*
    
    public function getVisaRequirements($city_id) {
        $language_id = 1;
        $city = Cities::find($city_id);

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $city_id)
                ->where('active', 1)
                ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('cities_id', '=', $city->id)
                ->count();
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Cities::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'cities.id', '=', 'places.cities_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('cities.id', $city_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('cities_id', '=', $city->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['city'] = $city;
        $data['airports'] = $airports;
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        return view('site.city.visa-requirements', $data);
    }
    
    public function getWhenToGo($city_id) {
        $language_id = 1;
        $city = Cities::find($city_id);

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $city_id)
                ->where('active', 1)
                ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('cities_id', '=', $city->id)
                ->count();
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Cities::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'cities.id', '=', 'places.cities_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('cities.id', $city_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('cities_id', '=', $city->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['city'] = $city;
        $data['airports'] = $airports;
        
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        return view('site.city.when-to-go', $data);
    }
    
    public function getCaution($city_id) {
        $language_id = 1;
        $city = Cities::find($city_id);

        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $language_id)->first();

        if (!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'holidays.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'emergency.trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'places' => function ($query) {
                        $query->where('media_count', '>', 0)
                        ->limit(50);
                    },
                    'atms' => function ($query) {
                        $query->where('place_type', 'like', '%atm%');
                    },
                    'hotels',
                    'getMedias',
                    'getMedias.users',
                    'timezone',
                    'followers'
                ])
                ->where('id', $city_id)
                ->where('active', 1)
                ->first();

        if (!$city) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $placeCount = Place::where('cities_id', '=', $city->id)
                ->count();
        $languages = $city->getLanguages();
        $city->setRelation('languages', collect($languages));


        $citiesMediasSql = DB::table('cities_medias')
                ->select([
                    'cities_id',
                    DB::raw('max(medias_id) AS medias_id')
                ])
                ->groupBy('cities_id')
                ->toSql();

        $tripPlans = Cities::select(
                        'medias.id AS media_id', 'medias.url', 'trips_cities.cities_id', 'trips.id', 'trips.title', 'trips.users_id', 'trips.created_at', 'trips_places.budget', 'trips_places.id'
                )
                ->join('places', 'cities.id', '=', 'places.cities_id')
                ->join('trips_places', function($join) {
                    $join->on('trips_places.cities_id', '=', 'cities.id')
                    ->on('trips_places.places_id', '=', 'places.id');
                })
                ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
                ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
                ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
                ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
                ->where('cities.id', $city_id)
                ->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
            $media = [
                'id' => $tripPlan['media_id'],
                'url' => $tripPlan['url']
            ];
            unset($tripPlan['media_id'], $tripPlan['url']);
            $tripPlan['medias'] = (object) $media;
        }

        $airports = Place::where('cities_id', '=', $city->id)
                ->where('place_type', 'like', 'airport,%')
                ->limit(10)
                ->get();

        $data['plans'] = $tripPlans;
        $data['plans_count'] = count($tripPlans);
        $data['sum_budget'] = $sumBudget;

        $data['city'] = $city;
        $data['airports'] = $airports;
        
        

        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        return view('site.city.caution', $data);
    }
    
    public function postFollow($cityId) {
        $userId = Auth::Id();
        $city = Cities::find($cityId);
        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $cityId)
                ->where('users_id', $userId)
                ->first();

        if ($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that city',
                ],
                'success' => false
            ];
        }

        $city->followers()->create([
            'users_id' => $userId
        ]);

        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('City', 'follow', $cityId);

        return [
            'success' => true
        ];
    }

    public function postUnFollow($cityId) {
        $userId = Auth::Id();

        $city = Cities::find($cityId);
        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $cityId)
                ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this city.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('City', 'unfollow', $cityId);
        }

        return [
            'success' => true
        ];
    }
    
    public function postCheckFollow($cityId) {
        $userId = Auth::Id();
        $city = Cities::find($cityId);
        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $follower = CitiesFollowers::where('cities_id', $cityId)
                ->where('users_id', $userId)
                ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }
    
    public function postVisaRequirements($countryId) {
        $userId = Auth::Id();
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $my_country = 'SA';
        $target_country = $country->iso_code;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.joinsherpa.com/v2/visa-requirements/' . $my_country . '-' . $target_country,
            CURLOPT_HTTPHEADER => array(
                'Content-Type:application/json',
                'Authorization: Basic ' . base64_encode("6801f898-5190-4177-ba1c-661e39596370:d41d8cd98f00b204e9800998ecf8427e")
            )
        ));
        $resp = curl_exec($curl);
        curl_close($curl);


        dd($resp);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function postWeather($countryId) {
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $latlng = $country->lat . ',' . $country->lng;

        $get_place_key = curl_init();
        curl_setopt_array($get_place_key, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q=41.87194000%2C12.56738000',
        ));
        $resp = curl_exec($get_place_key);
        curl_close($get_place_key);
        if ($resp) {
            $resp = json_decode($resp);
            $place_key = $resp->Key;

            $get_country_weather = curl_init();
            curl_setopt_array($get_country_weather, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/'.$place_key.'?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9',
            ));
            $country_weather = curl_exec($get_country_weather);
            curl_close($get_country_weather);
            $country_weather = json_decode($country_weather);
            dd($country_weather);
        }



        dd($place_key);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }
    
    public function postTalkingAbout($countryId) {
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }

        $shares = $country->shares;
        
        foreach($shares AS $share) {
            $data['shares'][] = array(
                'user_id' => $share->user->id,
                'user_profile_picture'=> check_profile_picture($share->user->profile_picture)
                );
        }
        
        $data['num_shares'] = count($shares);
        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }
    
    public function postNowInCity($cityId) {
        
        $city = Cities::find($cityId);
        if (!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $checkins = Checkins::where('city_id', $city->id)
                ->orderBy('id', 'DESC')
                ->take(8)
                ->get();

        $result = array();
        $done = array();
        foreach ($checkins AS $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                if(time()-strtotime($checkin->post_checkin->post->date) < (24*60*60)) {
                    $live = 1;
                }
                else {
                    $live = 0;
                }
                $result[] = array(
                    'name' => $checkin->post_checkin->post->author->name,
                    'id' => $checkin->post_checkin->post->author->id,
                    'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                    'live' => $live
                );
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['live_checkins'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }
    
    public function postContribute($countryId) {
        $country = Countries::find($countryId);
        if (!$country) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Country ID',
                ],
                'success' => false
            ];
        }
        
        if(Input::has('contents')) {
            $contents = Input::get('contents');
            $country_id = Input::get('country_id');
            $type = Input::get('type');
            $user_id = 0;
            if(Auth::user()) {
                $user_id = Auth::user()->id;
            }
            $create =  new CountriesContributions;
            $create->countries_id = $country_id;
            $create->users_id = $user_id;
            $create->type = $type;
            $create->contents = $contents;
            $create->status = 0;
            if($create->save()) {
                log_user_activity('Country', 'contribute', $country_id);
                $data['success'] = true;
            }
            else {
                $data['success'] = false;
            }
                
        }
        else {
            $data['success'] = false;
            
        }
        return json_encode($data);
       
    } 
    */


    public function _create_thumbs($media)
    {

        $complete_url = $media->url;
        $url = explode("/", $complete_url);

        $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

        $options = [
            'region' => 'us-east-1',
            'version' => 'latest',
            'http' => ['verify' => false],
            'credentials' => $credentials,
            'endpoint' => 'https://s3.amazonaws.com'
        ];

        $s3Client = new S3Client($options);
        //$s3 = AWS::createClient('s3');
        $result = $s3Client->getObject([
            'Bucket' => 'travooo-images2', // REQUIRED
            'Key' => $complete_url, // REQUIRED
            'ResponseContentType' => 'text/plain',
        ]);

        $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        //return $img_700->response('jpg');
        //echo $complete_url . '<br />';
        //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

        $put_1100 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_1100->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_700 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_700->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_230 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_230->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_180 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_180->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);


        $media->thumbs_done = 1;
        $media->save();
    }

    private function _getRandom($key)
    {

        $init_array = [];
        for ($i = 1; $i < 32; $i++) $init_array[] = $i;

        $return = [];

        foreach ($init_array as $val) {
            $return[] = $val * $key % 32;
        }

        return implode(',', $return);
    }
}
