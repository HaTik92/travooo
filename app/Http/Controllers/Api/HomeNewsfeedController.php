<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\Newsfeed\SecondaryNewsfeedService;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Services\Api\LoadedFeedService;
use App\Services\Api\PrimaryPostService;
use Illuminate\Support\Facades\DB;
use App\Services\Newsfeed\HomeService;

class HomeNewsfeedController extends Controller
{
    private $newsfeedService;
    public $page, $_session_key, $secondaryPost;

    public function __construct(
        HomeService $newsfeedService
    ) {
        DB::connection()->enableQueryLog();
        // dd(DB::getQueryLog());
        $this->page = (int) request('page_no', 1);
        $this->newsfeedService = $newsfeedService;
        // $this->commentStr1 = '<b>Hello</b><a href="/city/46" data-place_id="46" class="summernote-selected-items" dir="auto">Riyadh</a>&nbsp;<a href="/profile/1564" data-user_id="1564" class="summernote-selected-items post-text-tag-1564" data-toggle="popover" data-html="true" data-original-title="" title="" dir="auto">brijesh12</a>&nbsp;hello how are you&nbsp;';
        // $this->commentStr2 = '<b>Hello</b><a href="travoooapp://3a24d18dec9a.ngrok.io/city/46" data-place_id="46" class="summernote-selected-items" dir="auto">Riyadh</a>&nbsp;<a href="travoooapp://3a24d18dec9a.ngrok.io/profile/1564" data-user_id="1564" class="summernote-selected-items post-text-tag-1564" data-toggle="popover" data-html="true" data-original-title="" title="" dir="auto">brijesh12</a>&nbsp;hello how are you&nbsp;';


    }

    /**
     * @OA\GET(
     ** path="/home/newsfeed",
     *   tags={"Newsfeed"},
     *   summary="This Api used to fetch newsfeed.",
     *   operationId="Api\HomeNewsfeedController@index",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="page_no",
     *        description="page no feed| default = 1",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @return App\Http\Responses\ApiResponse
     */
    public function index(Request $request)
    {
        $isSuggestion = $request->get('is_suggestion', false);
        return ApiResponse::create($this->newsfeedService->getNewsFeed($this->page, $isSuggestion));
    }

    // secondary post slider api
    /**
     * @OA\GET(
     ** path="/home/newsfeed/{type}",
     *   tags={"Newsfeed"},
     *   summary="This Api used to fetch secondary post(slider api).",
     *   operationId="Api\HomeNewsfeedController@getSecondaryPost",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="page",
     *        description="page no",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="type",
     *        description="like new_people, video_you_might_like",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getSecondaryPost(Request $request, SecondaryNewsfeedService $secondaryNewsfeedService, string $type)
    {
        try {
            $type = strtolower(trim($type));
            if (!in_array($type, PrimaryPostService::SECONDARY_SLIDER_MAPPING)) {
                return ApiResponse::createValidationResponse([
                    "type" => "type is invalid"
                ]);
            }

            $response = ['page_id'   => null, 'unique_link' => null, 'action' => null];
            $response['type'] = $type;
            $suggestion = null;

            switch ($type) {
                case PrimaryPostService::SECONDARY_VIDEO_YOU_MIGHT_LIKE:
                    $suggestion = $secondaryNewsfeedService->videoYouMightLike();
                    break;
                case PrimaryPostService::SECONDARY_TRENDING_DESTINATIONS:
                    $suggestion = $secondaryNewsfeedService->trendingDestinations();
                    break;
                case PrimaryPostService::SECONDARY_PLACES_YOU_MIGHT_LIKE:
                    $suggestion = $secondaryNewsfeedService->placesYouMightLike();
                    break;
                case PrimaryPostService::SECONDARY_NEW_PEOPLE:
                    $suggestion = $secondaryNewsfeedService->getNewPeople();
                    break;
            }


            if ($suggestion != null) {
                $response['data'] = $suggestion;
                return ApiResponse::create($response);
            } else {
                return ApiResponse::create([
                    "message" => "sorry, suggestion not found"
                ], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
