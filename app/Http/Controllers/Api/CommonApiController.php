<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CommonRequest\UploadMediaRequest;
use App\Http\Responses\ApiResponse;
use App\Models\ActivityMedia\Media;
use App\Services\Api\PrimaryPostService;
use App\Services\FFmpeg\FFmpegService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CommonApiController extends Controller
{
    /**
     * @OA\Post(
     ** path="/media",
     *   tags={"Common API"},
     *   summary="This Api used to upload temporary media. like image, video",
     *   operationId="This Api used to upload temporary media. like image, video",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="pair",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="file",
     *                  type="string",
     *                  format="binary"
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/

    /**
     * @param Request $request
     * @return App\Http\Responses\ApiResponse
     */
    public function postTmpUpload(Request $request, FFmpegService $ffmpegService)
    {
        try {
            $extension_list = ['avi', 'qt'];
            $user       = Auth::user();
            $validator  = Validator::make($request->all(), [
                'file'        => 'required',
                'pair'        => 'required'
            ]);
            if ($validator->fails()) {
                return ApiResponse::create($validator->errors(), false, ApiResponse::VALIDATION);
            } else {
                $validator = Validator::make($request->all(), [
                    'file'    => 'mimes:jpeg,jpg,png,mp4,mov,ogg,wmv,heic,gif,tiff,bmp,avi,qt'
                ], [
                    'file.mimes' => 'only allow .jpeg, .jpg, .png, .mp4, .mov, .ogg, .wmv, .heic, .gif, .tiff, .bmp, .avi, .qt media'
                ]);

                if ($validator->fails()) {
                    return ApiResponse::create($validator->errors(), false, ApiResponse::VALIDATION);
                }
            }
            $_base_path = '/assets2/upload_tmp';
            if ($request->hasFile('file')) {
                $file               = $request->file('file');
                $extension          = $file->extension();
                $pair               = $request->pair ?? '';
                $fileOriginalName   = $file->getClientOriginalName();
                // $filename           = $pair . '_' . $user->id . '_' . $fileOriginalName;
                $filename           = str_replace(".", "", microtime(true)) . '_' . rand(1, 1000) . '_' . $fileOriginalName;
                $fullFileName       = $pair . '_' . $user->id . '_' . $filename;
                $path               = public_path($_base_path) . '/';
                @chmod($path, 0777);
                if ($file->move($path, $fullFileName)) {
                    // covert avi, qt to mp4
                    if (in_array($extension, $extension_list)) {
                        $new_filename =  $pair . '_' . $user->id . '_' . str_replace(".", "", microtime(true)) . '_video.mp4';
                        $ffmpegService->convertVideoFile($path, $fullFileName, $new_filename);
                    } else {
                        $new_filename = $fullFileName;
                    }
                    return ApiResponse::create([
                        "message"   => "upload successfully",
                        "filename"  => $new_filename,
                        'url' => url($_base_path) . "/" . $new_filename,
                    ]);
                } else {
                    return ApiResponse::__createBadResponse("failed to upload media");
                }
            } else {
                return ApiResponse::__createBadResponse("something went wrong in file");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return App\Http\Responses\ApiResponse
     * 
     * @OA\Delete(
     ** path="/media",
     *   tags={"Common API"},
     *   summary="This Api used to remove temporary uploaded media. like image, video",
     *   operationId="This Api used to remove temporary uploaded media. like image, video",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="pair",
     *        description="pair",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="file_name",
     *        description="uploaded filename",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/

    public function uploadTmpDelete(Request $request)
    {
        try {
            $validator  = Validator::make($request->all(), [
                'file_name'     => 'required',
            ]);
            if ($validator->fails()) {
                return ApiResponse::create($validator->errors(), false, ApiResponse::VALIDATION);
            }
            $filename  = $request->file_name;
            $filepath  = public_path('assets2/upload_tmp/' . $filename);
            if (file_exists($filepath)) {
                if (getMediaTypeByMediaUrl($filename) == Media::TYPE_VIDEO) {
                    @unlink(public_path('video-thumbs/' . explode('.', $filename)[0] . "_thumbnail.jpg"));
                }
                @unlink($filepath);
                return ApiResponse::__create("delete successfully");
            } else {
                return ApiResponse::__createBadResponse("file not found.");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param string pair[tempfile prefix]
     * @return array [filepath, filename]
     */
    public function getTempFiles($pair)
    {
        $pair = ($pair) ? $pair : -1;
        $user = Auth::user();
        $temp_dir = public_path() . '/assets2/upload_tmp/';
        $fileLists = [];

        if (is_dir($temp_dir)) {
            if ($handle = opendir($temp_dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    if (strpos(($temp_dir . $file), $pair . '_' . $user->id) !== false) {
                        $filename_split = explode($pair . '_' . $user->id . '_', $file, 2);
                        $fileLists[] = [$temp_dir . $file, $filename_split[1]];
                    }
                }
                closedir($handle);
            }
        }

        return $fileLists;
    }

    public function deleteTempFiles($pair)
    {
        gc_collect_cycles();
        $files = $this->getTempFiles($pair);
        if (is_array($files)) {
            foreach ($files as $file) {
                @unlink($file[0]);
            }
        }
    }
}
