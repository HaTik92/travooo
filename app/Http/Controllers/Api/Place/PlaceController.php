<?php

namespace App\Http\Controllers\Api\Place;

/* Dependencies */

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Http\Requests\Api\Places\PlacesDiscussionsRequest;
use App\Http\Requests\Api\Places\PlacesFollowersRequest;
use App\Http\Requests\Api\Places\PlacesFollowRequest;
use App\Http\Requests\Api\Places\PlacesMediaRequest;
use App\Http\Requests\Api\Places\PlacesNearbyRequest;
use App\Http\Requests\Api\Places\PlacesReviewsRequest;
use App\Http\Requests\Api\Places\PlacesSearchRequest;
use App\Http\Requests\Api\Places\PlacesSetReviewsRequest;
use App\Http\Requests\Api\Places\PlacesSharesRequest;
use App\Http\Requests\Api\Places\PlacesShowInfoRequest;
use App\Http\Requests\Api\Places\PlacesUnfollowRequest;
use App\Models\Access\language\Languages;
use App\Models\ActivityLog\ActivityLog;
use App\Models\City\Cities;
use App\Models\Place\ApiPlace as ApiPlace;
use App\Models\Place\Place;
use App\Models\Place\PlaceFollowers;
use App\Models\Place\PlacesShares;
use App\Transformers\Place\PlacesFollowersTransformer;
use App\Transformers\Place\PlacesMediaTransformer;
use App\Transformers\Place\PlacesTransformer;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection AS FractalCollection;

/**
 * Class Api/CountryController.
 */
class PlaceController extends Controller {
    use CreateUpdateStatisticTrait;

    /**
     * @param PlacesSearchRequest $request
     * @return array
     */
    public function get_places(PlacesSearchRequest $request)
    {
        $post       = $request->input();
        $query      = null;
        $places     = null;
        $languageId = $request->input('language_id');
        $cityId     = $request->input('city_id');

        $language = Languages::find($languageId);
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $offset = 0;
        $limit  = 20;

        if (isset($cityId) && !empty($cityId)) {
            $city = Cities::find($cityId);
            if(!$city) {
                return [
                    'data' => [
                        'error' => 400,
                        'message' => 'Invalid City ID',
                    ],
                    'success' => false
                ];
            }
        }

        if(isset($post['query']) && !empty($post['query'])){
            $query = $post['query'];
        }

        if(isset($post['offset']) && !empty($post['offset'])){
            $offset = $post['offset'];
        }

        if(isset($post['limit']) && !empty($post['limit'])){
            $limit = $post['limit'];
        }

        $places = ApiPlace::select('places.id as pId', 'places.lat as lat', 'places.lng as lng', 'places_trans.title');

        if(!empty($cityId)){
            $places = $places->where('cities_id', $cityId);
        }

        $places = $places->join('places_trans', 'places.id', '=', 'places_trans.places_id')
            ->where(['active' => 1,'languages_id' => $languageId]);

        if(!empty($query)){
            $places = $places->where('title', 'REGEXP', $query);
        }

        $places = $places->orderBy('rating', 'desc')->offset($offset)->limit($limit);
        $places = $places->get();
        
        $places_arr = [];

        foreach ($places as $key => $value) {
            $places_arr[] = $value->getResponse();
        }

        return [
            'data' => [
                'code' => 200,
                'places' => $places_arr,
            ],
            'success' => true
        ];
    }

    /**
     * @param PlacesShowInfoRequest $request
     * @param $placeId
     * @return array
     */
    public function showPlaceInformation(PlacesShowInfoRequest $request, $placeId) {
        $languageId =  $request->input('language_id');
        $place = Place::where('id', $placeId)->first();

        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $place = Place::with([
            'trans' => function ($query) use ($languageId) {
                $query->where('languages_id', $languageId);
            }
        ])->where('id', $placeId)->where('active', 1);

        if(!$place) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }
        
        $return = $place->first();
        $return->place_type = ucfirst(explode(",", $return->place_type)[0]);

        return [
            'data' => [
                'place' => $return
            ],
            'success' => true
        ];
    }

    /**
     * @param PlacesReviewsRequest $request
     * @param $placeId
     * @return array
     */
    public function getReviews(PlacesReviewsRequest $request, $placeId) {
        $languageId =  $request->input('language_id');
        $place = Place::where('id', $placeId)->first();
        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $place = Place::with([
            'trans' => function ($query) use ($languageId) {
                $query->where('languages_id', $languageId);
            },
            'reviews'
        ])->where('id', $placeId)->where('active', 1)->first();

        return [
            'data' => [
                'place' => $place,
            ],
            'success' => true
        ];

    }

    /**
     * @param PlacesSetReviewsRequest $request
     * @param $placeId
     * @return array
     */
    public function postReview(PlacesSetReviewsRequest $request, $placeId) {
        $place = Place::find($placeId);

        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }
        $userId  = $request->input('user_id');
        $score   = $request->input('score');
        $comment = $request->input('comment');
        $userIp  = $request->ip();

        $insertedId =  $place->reviews()->create([
            'users_id'   => $userId,
            'score'      => $score,
            'comment'    => $comment,
            'ip_address' => $userIp
        ]);
        log_user_activity('Place', 'review', $placeId);

        return [
            'success' => $insertedId ? true : false
        ];
    }

    /**
     * @param PlacesFollowersRequest $request
     * @param $placeId
     * @return array
     */
    public function getNumFollowers(PlacesFollowersRequest $request, $placeId) {
        $languageId = $request->input('language_id');
        $place = Place::find($placeId);

        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $place = Place::with([
            'trans' => function ($query) use ($languageId) {
                $query->where('languages_id', $languageId);
            },
            'followers'
        ])->where('id', $placeId)->where('active', 1)->first();

        return [
            'data' => [
                'followers' => count($place->followers),
            ],
            'success' => true
        ];
    }

    /**
     * @param PlacesFollowRequest $request
     * @param $placeId
     * @return array
     */
    public function followPlace(PlacesFollowRequest $request, $placeId) {
        $languageId = $request->input('language_id');
        $userId     = Auth::Id();
        $place      = Place::find($placeId);

        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $follower = PlaceFollowers::where('places_id', $placeId)
            ->where('users_id', $userId)
            ->first();

        if($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that place',
                ],
                'success' => false
            ];
        }

        $place->followers()->create([
            'users_id' => $userId
        ]);

        $this->updateStatistic($place, 'followers', count($place->followers));
        log_user_activity('Place', 'follow', $placeId);

        return [
            'success' => true
        ];
    }

    /**
     * @param PlacesUnfollowRequest $request
     * @param $placeId
     * @return array
     */
    public function postUnFollow(PlacesUnfollowRequest $request, $placeId)
    {
        $languageId = $request->input('language_id');
        $userId     = Auth::Id();

        $place = Place::find($placeId);
        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $follower = PlaceFollowers::where('places_id', $placeId)
                                  ->where('users_id', $userId);

        if(empty($follower->first())) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user didn\'t follow this place',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            $this->updateStatistic($place, 'followers', count($place->followers));
            log_user_activity('Place', 'unfollow', $placeId);
        }

        return [
            'success' => true
        ];
    }

    /**
     * @param $placeId
     * @return array
     */
    public function getPlacesStatistics($placeId) {
        $place = Place::find($placeId);

        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        return [
            'data' => [
                'places_statistics' => $place->statistics,
            ],
            'success' => true
        ];
    }

    /**
     * @param PlacesMediaRequest $request
     * @param $placeId
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getPlacesMedia(PlacesMediaRequest $request, $placeId) {
        $languageId = $request->input('language_id');
        $place = Place::find($placeId);
        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $place = Place::with([
            'getMedias',
            'getMedias.users'
        ])->where('id', $placeId)->first();

        $resource = new Item($place, new PlacesMediaTransformer());

        return apiResponse(
            $resource,
            [
                'medias:id|url',
                'medias.users:id|name|username|profile_picture'
            ]
        );
    }

    /**
     * @param PlacesMediaRequest $request
     * @param $placeId
     * @return array
     */
    public function getPlacesPlans(PlacesMediaRequest $request, $placeId) {
        $languageId = $request->input('language_id');
        $place = Place::find($placeId);
        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $tripPlans = Place::select(
            'trips.id',
            'trips.title',
            'trips.users_id',
            'trips.created_at',
            'trips_places.budget',
            'trips_places.id'
        )
            ->join('trips_places', function($join)
            {
                $join->on('trips_places.places_id', '=', 'places.id');
            })
            ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
            ->where('places.id', $placeId)->get();

        $sumBudget = 0;
        foreach ($tripPlans as $tripPlan) {
            $sumBudget += $tripPlan['budget'];
        }

        $place['plans'] = $tripPlans;
        $place['count'] = count($tripPlans);
        $place['sum_budget'] = $sumBudget;

        return [
            'data' => $place,
            'success' => true
        ];
    }

    /**
     * @param PlacesDiscussionsRequest $request
     * @param $placeId
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getPlacesDiscussions(PlacesDiscussionsRequest $request, $placeId) {
        $languageId = $request->input('language_id');
        $place = Place::find($placeId);
        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $place = Place::with([
            'discussions',
            'discussions.UpVotes',
            'discussions.DownVotes',
            'discussions.users',
        ])->where('id', $placeId)->first();

        $resource = new Item($place, new PlacesTransformer());

        return apiResponse(
            $resource,
            [
                'discussions',
                'discussions.users:username|profile_picture',
                'discussions.UpVotes',
                'discussions.DownVotes',
            ]
        );
    }

    /**
     * @param $placeId
     * @return array
     */
    public function checkFollow($placeId)
    {
        $userId = Auth::Id();
        $place = Place::find($placeId);
        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $follower = PlaceFollowers::where('places_id', $placeId)
                                  ->where('users_id', $userId)
                                  ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    /**
     * @param $placeId
     * @param PlacesSharesRequest $request
     * @return array
     */
    public function postShare($placeId, PlacesSharesRequest $request) {
        $userId  = Auth::Id();
        $scope   = $request->input('scope');
        $comment = $request->input('comment');
        $place   = Place::find($placeId);
        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $placeShares = new PlacesShares();
        $placeShares->place_id   = $placeId;
        $placeShares->user_id    = $userId;
        $placeShares->scope      = $scope;
        $placeShares->comment    = $comment ? $comment : null;
        $placeShares->created_at = date('Y-m-d H:i:s', time());
        $placeShares->save();

        $this->updateStatistic($place, 'shares', count($place->shares));

        return [
            'success' => true
        ];
    }

    /**
     * @param $placeId
     * @param PlacesNearbyRequest $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getNearby($placeId, PlacesNearbyRequest $request) {
        $languageId = $request->input('language_id');
        $place = Place::find($placeId);
        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $latitude  = $place->lng;
        $longitude = $place->lat;

        $place = Place::with([
            'trans',
            'getMedias'
        ])->closeTo($latitude, $longitude)->orderBy('distance')->take(50)->get();

        $resource = new FractalCollection($place, new PlacesMediaTransformer());

        return apiResponse(
            $resource,
            [
                'firstmedia:id|url',
                'trans:title'
            ]
        );
    }

    /**
     * @param $placeId
     * @param PlacesFollowersRequest $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getFollowers($placeId, PlacesFollowersRequest $request) {
        $languageId = $request->input('language_id');

        $place = Place::with([
            'followers'
        ])->where('id', $placeId)->where('active', 1)->first();

        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $placeFollowers = PlaceFollowers::with([
            'followers'
        ])->where('places_id', $placeId)->take(50)->get();

        $resource = new FractalCollection($placeFollowers, new PlacesFollowersTransformer());
        return apiResponse(
            $resource,
            [
                'followers:id|name|profile_picture'
            ]
        );
    }
}