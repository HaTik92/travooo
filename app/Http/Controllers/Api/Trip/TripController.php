<?php

namespace App\Http\Controllers\Api\Trip;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Http\Requests\Api\Trips\FinishCityRequest;
use App\Http\Requests\Api\Trips\FinishPlaceRequest;
use App\Http\Requests\Api\Trips\PlaceOrderRequest;
use App\Http\Requests\Api\Trips\RemoveCityRequest;
use App\Http\Requests\Api\Trips\RemovePlaceRequest;
use App\Http\Requests\Api\Trips\TripsShareRequest;
use App\Models\Access\language\Languages;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Country\Countries;
use App\Models\TripPlans\TripsShares;
use App\Transformers\Trip\TripTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TripPlans\TripPlans;
use App\Models\City\Cities;
use App\Models\Place\Place;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Item;

/**
 * @resource Trip Planner
 *
 * Trip Planner functions
 */
class TripController extends Controller
{
    use CreateUpdateStatisticTrait;
    /**
	 * api/trips/new
	 *
	 * Create Trip Plan - step 1
        * 
        * parameter String "title" (required) <br />
        * parameter unix_timestamp "date" (required) <br />
        * parameter Integer "privacy" (required) <br />
	 *
     * @param Request $request
     * @return array
	 */
    
    public function postNew(Request $request)
    {
        $trip = new TripPlans;
        $trip->users_id = Auth::Id();
        $trip->title = $request->input('title');
        $trip->start_date = date('Y-m-d H:i:s', strtotime($request->input('date')));
        $trip->privacy = $request->input('privacy');
        $trip->created_at = date('Y-m-d H:i:s', time());
        
        if($trip->save()) {
            return [
                'data' => [
                    'trip_id' => $trip->id
                ],
                'success' => true
            ];
        }
        else {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Error saving Trip in DB.',
                ],
                'success' => false
            ];
        }
        
    }
    
    /**
	 * api/trips/add_city
	 *
	 * Create Trip Plan - step 2
        * 
        * parameter Integer "trip_id" (required) <br />
        * parameter Integer "city_id" (required) <br />
        * parameter Integer "order" (required) <br />
        * parameter Unix timestamp "date" (required) <br />
	 *
     * @param $trip_id
     * @param Request $request
     * @return array
	 */
    
    public function postAddCity($trip_id, Request $request)
    {
        $post = $request->input();
        
        $city_id = $post['city_id'];
        $order = $post['order'];
        $date = @$post['date'];
        
        $trip = TripPlans::find($trip_id);
        if(!$trip) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }
        
        $city = Cities::find($city_id);
        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }
        
        if($trip->cities->where('id', $city_id)->count()>0) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'City is already added before!',
                ],
                'success' => false
            ];
        }
        
        $att = $trip->cities()->attach($city_id,['order' => $order, 'date' => date('Y-m-d', $date)]);
        
            return [
                
                'success' => true
            ];
        
        
    }
    
    /**
	 * api/trips/add_place
	 *
	 * Create Trip Plan - step 3
        * 
        * parameter Integer "trip_id" (required) <br />
        * parameter Integer "place_id" (required) <br />
        * parameter Integer "order" (required) <br />
        * parameter Unix timestamp "date" (required) <br />
        * parameter Unix timestamp "time" (optional) <br />
        * parameter Unix timestamp "duration" (optional) <br />
        * parameter Integer "budget" (optional) <br />
	 *
     * @param $trip_id
     * @param Request $request
     * @return array
	 */
    public function postAddPlace($trip_id, Request $request) {

        $post = $request->input();
        
        //isset($post['trip_id']) ? $trip_id = $post['trip_id'] : $trip_id = null;
        isset($post['place_id']) ? $place_id = $post['place_id'] : $place_id = null;
        isset($post['order']) ? $order = $post['order'] : $order = null;
        isset($post['date']) ? $date = $post['date'] : $date = null;
        isset($post['time']) ? $time = $post['time'] : $time = null;
        isset($post['duration']) ? $duration = $post['duration'] : $duration = null;
        isset($post['budget']) ? $budget = $post['budget'] : $budget = null;
        
        $trip = TripPlans::find($trip_id);
        if(!$trip) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }
        
        $place = Place::find($place_id);
        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }
        
        if($trip->places->where('id', $place_id)->count()>0) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Place is already added before!',
                ],
                'success' => false
            ];
        }
        
        $att = $trip->places()->attach($place_id,['countries_id' => $place->country->id, 'cities_id' => $place->city->id
                , 'order' => $order, 'date' => date('Y-m-d', $date)
            , 'time' => $time, 'duration' => $duration, 'budget' => $budget]);

        $country = Countries::find($place->country->id);
        $tripCountries = DB::table('trips_places')->where('countries_id', $place->country->id)->get();
        $this->updateStatistic($country, 'trips', count($tripCountries));

        $city = Cities::find($place->cities_id);
        $tripCities = DB::table('trips_places')->where('cities_id', $place->city->id)->get();
        $this->updateStatistic($city, 'trips', count($tripCities));

        return [
            'success' => true
        ];
        
    }
    
    /**
	 * api/{trip_id}/cities
	 *
	 * Get cities related to Trip
        *
        * parameter Integer "trip_id" (required) <br />
        * parameter Integer "language_id" (required) <br />
	 *
     * @param $tripId
     * @param Request $request
     * @return array
	 */
    
    public function getCities($tripId, Request $request)
    {
        $userId = Auth::Id();
        $languageId =  $request->input('language_id');

        $tripPlan = TripPlans::where('id', $tripId)->first();

        if(!$tripPlan) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $trip = TripPlans::with([
            'cities' => function($query) {
                $query->orderBy('id');
            },
            'cities.trans' => function ($query) use($languageId) {
                $query->where('languages_id', $languageId);
            }
        ])->where('id', $tripId)->where('users_id', $userId)->first();
        
        if(!$trip) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }
        else {
           return [
                'data' => [
                    'cities' => $trip->cities,
                ],
                'success' => true
            ];
        }
    }
    
    /**
	 * api/{trip_id}/places
	 *
	 * Get places related to a trip
     *
         * parameter Integer "trip_id" (required) <br />
         * parameter Integer "language_id" (required) <br />
	 *
     * @param $tripId
     * @param Request $request
     * @return array
	 */
    
    public function getPlaces($tripId, Request $request)
    {
        $userId = Auth::Id();
        $languageId =  $request->input('language_id');

        $tripPlan = TripPlans::where('id', $tripId)->first();

        if(!$tripPlan) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }

        $language = Languages::where('id', $languageId)->first();

        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $trip = TripPlans::with([
            'places' => function($query) {
                $query->orderBy('id');
            },
            'places.trans' => function ($query) use ($languageId) {
                $query->where('languages_id', $languageId);
            },
            'places.city.trans' => function ($query) use ($languageId) {
                $query->where('languages_id', $languageId);
            },
            'places.country.trans' => function ($query) use ($languageId) {
                $query->where('languages_id', $languageId);
            }
        ])->where('id', $tripId)->where('users_id', $userId)->first();

        if(!$trip) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        } else {
            $resource = new Item($trip, new TripTransformer());

            return apiResponse(
                $resource,
                [
                    'places',
                    'places.trans:title',
                    'places.city:id',
                    'places.city.trans:title',
                    'places.country:',
                    'places.country.trans:title'
                ]
            );
        }
    }

    /**
    * api/trips/{trip_id}/finish_place
    *
    * Activate a Place inside a Trip
        *
        * parameter Integer "trip_id" (required) <br />
        * parameter Integer "place_id" (required) <br />
        * parameter Unix timestamp "date" (required) <br />
        * parameter Unix timestamp "time" (required) <br />
        * parameter Integer "duration" (optional) <br />
        * parameter Integer "budget" (optional) <br />
    *
    * @param $tripId
    * @param FinishPlaceRequest $request
    * @return array
    */

    public function postFinishPlace($tripId, FinishPlaceRequest $request)
    {
        $finishData = $request->all();

        $tripPlan = TripPlans::find($tripId);

        if(!$tripPlan) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }

        $place = Place::where('id', $finishData['place_id'])->first();

        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $att = $tripPlan->places()->updateExistingPivot(
            $finishData['place_id'],
            [
                'date' => $finishData['date'],
                'time' => Carbon::parse($finishData['time']),
                'duration' => $finishData['duration'],
                'budget' => $finishData['budget'],
            ]
        );

        return [
            'success' => true
        ];

    }

    /**
     * api/trips/{trip_id}/cancel
     *
     * Delete a Trip
     *
        * parameter Integer "trip_id" (required) <br />
     *
     * @param $tripId
     * @return array
     */

    public function postCancel($tripId)
    {
        $tripPlan = TripPlans::find($tripId);

        if(!$tripPlan) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }

        $tripPlan->cities()->detach();

        $countryIds = [];
        $cityIds = [];
        foreach ($tripPlan->places as $tripPlace) {
            $countryIds[] = $tripPlace->countries_id;
            $cityIds[] = $tripPlace->cities_id;
        }

        $tripPlan->places()->detach();
        $tripPlan->countries()->detach();
        $tripPlan->delete();

        foreach ($countryIds as $countryId) {
            $country = Countries::find($countryId);
            $tripCountries = DB::table('trips_places')->where('countries_id', $countryId)->get();
            $this->updateStatistic($country, 'trips', count($tripCountries));
        }

        foreach ($cityIds as $cityId) {
            $city = Cities::find($cityId);
            $tripCities = DB::table('trips_places')->where('cities_id', $cityId)->get();
            $this->updateStatistic($city, 'trips', count($tripCities));
        }

        return [
            'success' => true
        ];

    }

    /**
     * api/trips/{trip_id}/finish_city
     *
     * Activate a City inside a Trip
     *
         * parameter Integer "trip_id" (required) <br />
         * parameter Integer "city_id" (required) <br />
         * parameter String "transportation" (optional) <br />
     *
     * @param $tripId
     * @param FinishCityRequest $request
     * @return array
     */

    public function postFinishCity($tripId, FinishCityRequest $request)
    {
        $cityId = $request->input('city_id');
        $transportation = $request->input('transportation');

        $tripPlan = TripPlans::find($tripId);

        if(!$tripPlan) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::where('id', $cityId)->first();

        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        $isUpdate = $tripPlan->cities()->updateExistingPivot(
            $cityId,
            [
                'transportation' => $transportation,
            ]
        );

        return [
            'success' => $isUpdate ? true : false
        ];
    }

    /**
     * api/trips/{trip_id}/publish
     *
     * Publish a Trip
     *
         * parameter Integer "trip_id" (required) <br />
     *
     * @param $tripId
     * @return array
     */

    public function postPublish($tripId)
    {
        $tripPlan = TripPlans::find($tripId);
        $userId   = Auth::Id();

        if(!$tripPlan) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }

        $activate = TripPlans::where('id', $tripId)
            ->update(['active' => 1]);


        if($activate) {
            log_user_activity('TripPlan', 'publish', $tripId);

            return [
                'success' => true
            ];
        } else {
            return [
                'success' => false
            ];
        }
    }

    /**
     * api/trips/{trip_id}/remove_place
     *
     * Remove a Place from a Trip
     *
        * parameter Integer "trip_id" (required) <br />
        * parameter Integer "place_id" (required) <br />
     *
     * @param $tripId
     * @param RemovePlaceRequest $request
     * @return array
     */

    public function postRemovePlace($tripId, RemovePlaceRequest $request)
    {
        $placeId = $request->input('place_id');
        $tripPlan = TripPlans::find($tripId);

        if(!$tripPlan) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }

        $place = Place::where('id', $placeId)->first();

        if(!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        if(DB::table('trips_places')->where('trips_id', $tripId)->where('places_id', $placeId)->delete()){
            $country = Countries::find($place->country->id);
            $tripCountries = DB::table('trips_places')->where('countries_id', $place->country->id)->get();
            $this->updateStatistic($country, 'trips', count($tripCountries));

            $city = Cities::find($place->city->id);
            $tripCities = DB::table('trips_places')->where('cities_id', $place->city->id)->get();
            $this->updateStatistic($city, 'trips', count($tripCities));

            return [
                'success' => true
            ];
        } else {
            return [
                'success' => false
            ];
        }
    }

    /**
     * api/trips/{trip_id}/remove_city
     *
     * Remove a City from a Trip
     *
     * parameter Integer "trip_id" (required) <br />
     * parameter Integer "city_id" (required) <br />
     *
     * @param $tripId
     * @param RemoveCityRequest $request
     * @return array
     */

    public function postRemoveCity($tripId, RemoveCityRequest $request)
    {
        $cityId = $request->input('city_id');
        $tripPlan = TripPlans::find($tripId);

        if(!$tripPlan) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }

        $city = Cities::where('id', $cityId)->first();

        if(!$city) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid City ID',
                ],
                'success' => false
            ];
        }

        if(DB::table('trips_cities')->where('trips_id', $tripId)->where('cities_id', $cityId)->delete()){
            DB::table('trips_places')->where('trips_id', $tripId)->where('cities_id', $cityId)->delete();
            return [
                'success' => true
            ];
        } else {
            return [
                'success' => false
            ];
        }
    }

    /**
     * api/trips/{trip_id}/places_order
     *
     * Order places inside a Trip
     *
     * parameter Integer "trip_id" (required) <br />
     * parameter Array "order" (required) <br />
     *
     * @param $tripId
     * @param PlaceOrderRequest $request
     * @return array
     */
    public function postPlaceOrder($tripId, PlaceOrderRequest $request) {
        $orders = $request->input('order');
        $tripPlan = TripPlans::find($tripId);

        if(!$tripPlan) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }
        foreach ($orders as $key => $order) {
            if(!empty($order)) {
                if(isset($order['place_id']) && isset($order['order'])) {
                    $placeId = $order['place_id'];
                    $orderCol = $order['order'];
                } else {
                    return [
                        'data' => [
                            'error' => 400,
                            'message' => 'Missing order params',
                        ],
                        'success' => false
                    ];
                }

                try{
                    $tripPlan->places()->updateExistingPivot(
                        $placeId,
                        [
                            'order' => $orderCol,
                        ]
                    );
                } catch (\Exception $e) {
                    return [
                        'data' => [
                            'error' => 500,
                            'message' => $e->getMessage()
                        ],
                        'success' => false
                    ];
                }
            }
        }
        return [ "success" => true ];
    }

    /**
     * @param $tripId
     * @param TripsShareRequest $request
     * @return array
     */
    public function postShare($tripId, TripsShareRequest $request) {
        $userId   = Auth::Id();
        $scope    = $request->input('scope');
        $comment  = $request->input('comment');
        $tripPlan = TripPlans::find($tripId);

        if(!$tripPlan) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Trip ID',
                ],
                'success' => false
            ];
        }

        $tripsShare = new TripsShares();
        $tripsShare->trip_id    = $tripId;
        $tripsShare->user_id    = $userId;
        $tripsShare->scope      = $scope;
        $tripsShare->comment    = $comment ? $comment : null;
        $tripsShare->created_at = date('Y-m-d H:i:s', time());
        $tripsShare->save();

        return [
            'success' => true
        ];
    }

}