<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\CommonApiController;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Services\Discussions\SpamsRepliesService;
use App\Http\Controllers\Api\HomeController;
use App\Services\EnlargedViews\EnlargedViewsService;
use App\Services\Discussions\DiscussionVotesService;
use App\Models\Discussion\DiscussionTopics;
use App\Http\Requests\Api\Discussion\SearchDiscussionRequest;
use App\Http\Requests\Api\Discussion\DiscussionRequest;
use App\Http\Requests\Api\Discussion\CreateDiscussionRequest;
use App\Http\Requests\Api\Discussion\CheckUpDownVoteRequest;
use App\Http\Constants\CommonConst;
use App\Http\Requests\Api\Discussion\ReplyDiscussionRequest;
use App\Http\Requests\Api\Discussion\ReportSpamRequest;
use App\Http\Requests\Api\Discussion\EditReplyRequest;
use App\Http\Requests\Api\Discussion\FilterAnswersRequest;
use App\Http\Requests\Api\Discussion\DiscussionViewCountRequest;
use App\Http\Requests\Api\Discussion\PostLikeUnlikeRequest;
use App\Http\Requests\Api\Discussion\AjexDiscussionRequest;
use App\Http\Requests\Api\Discussion\ExpertsDiscussionRequest;
use App\Models\Discussion\DiscussionRepliesDownvotes;
use App\Models\Discussion\DiscussionMedias;
use App\Models\Discussion\SpamsReplies;

use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Discussion\DiscussionExperts;
use App\Models\Discussion\DiscussionRepliesUpvotes;
use App\Models\Discussion\DiscussionViews;
use App\Models\Discussion\DiscussionLikes;
use App\Models\Discussion\DiscussionShares;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\User\User;
use App\Models\TripPlans\TripPlans;
use App\Models\Place\Place;
use App\Models\Country\ApiCountry as Country;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\Posts\PostsShares;
use App\Models\User\UsersContentLanguages;
use App\Services\Api\PrimaryPostService;
use App\Services\Api\ShareService;
use App\Services\Discussions\DiscussionsService;
use App\Services\Ranking\RankingService;
use App\Services\Translations\TranslationService;

defined("DISCUSSION_TESTING_MODE")   or define("DISCUSSION_TESTING_MODE", true);


class DiscussionController extends Controller
{
    private $rankingService;

    function __construct(RankingService $rankingService)
    {
        $this->rankingService = $rankingService;
        if (in_array(request()->route()->uri, optionalAuthRoutes(self::class))) {
            if (request()->bearerToken()) {
                $this->middleware('auth:api');
            }
        }
    }

    private function __trendingTopic()
    {
        return DiscussionTopics::query()
            ->where('created_at', '>=', Carbon::now()->subDays(7))
            ->groupBy('topics')
            ->orderBy('created_at', 'desc')
            ->take(20)->get()->pluck('topics');
    }

    private function __mostHelpful()
    {
        $most_helpful = DB::table('discussion_replies')->selectRaw('users_id, count(*) as total');
        if (Auth::check()) {
            $most_helpful = $most_helpful->where('users_id', '!=', Auth::user()->id);
        }
        $most_helpful = $most_helpful->whereNotIn('users_id', blocked_users_list())
            ->groupBy('users_id')
            ->orderBy('total', 'desc')
            ->where('created_at', '>=', Carbon::now()->subDays(7))->take(10)->get();

        $items = [];
        collect($most_helpful)->each(function ($item) use (&$items) {
            $user = \App\Models\User\User::find($item->users_id);
            if ($user) {
                $items[] = [
                    "total_replies" => $item->total,
                    "author" => [
                        "id" => $user->id,
                        "email" =>  $user->email,
                        "profile_picture" =>  check_profile_picture($user->profile_picture),
                        "username" => $user->username,
                        "badge_url" => get_exp_icon_url($user)
                    ]
                ];
            }
        });
        return ($items);
    }

    /**
     * @OA\GET(
     ** path="/discussions",
     *   tags={"Discussions"},
     *   summary="Fetch All Disscusions + User can filter",
     * @OA\Parameter(
     *      name="per_page",
     *      description="per page items | default : 10",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="integer"
     *      )
     * ),
     * @OA\Parameter(
     *      name="page",
     *      description="pass page no | default : 1",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="integer"
     *      )
     * ),
     * @OA\Parameter(
     *      name="q",
     *      description="Question Tab => Search query",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="topic",
     *      description="Topics Tab => (page = 1 | tab->trending_topics",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * @OA\Parameter(
     *        name="cities",
     *        description="comma separated",
     *        in="query",
     *        @OA\Schema(
     *           type="string",
     *        ),
     *     ),
     *  @OA\Parameter(
     *        name="countries",
     *        description="comma separated",
     *        in="query",
     *        @OA\Schema(
     *           type="string",
     *        ),
     *     ),
     * @OA\Parameter(
     *      name="order",
     *      description="Filter => order values [new, old, popular] | default : new",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="my_activity_type",
     *      description="Activity Tab => values [discussions, replies]",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * * @OA\Parameter(
     *      name="answers_user_id",
     *      description="Most Helpful Tab => (page = 1 | tab->most_helpful->[any item]->author->id)",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="integer"
     *      )
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param SearchDiscussionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function searchDiscussion(SearchDiscussionRequest $request)
    {
        try {
            if ($request->has('langugae_id')) {
                $langugae_id = (int) $request->langugae_id;
            } else {
                $langugae_id = getDesiredLanguage();
                if (isset($langugae_id[0])) {
                    $langugae_id = $langugae_id[0];
                } else {
                    $langugae_id = UsersContentLanguages::DEFAULT_LANGUAGES;
                }
            }

            $countries             = collect(explode(",", trim(trim($request->get('countries', ''), ","))))->filter(function ($val) {
                return $val != null || $val != "";
            })->toArray();
            $cities             = collect(explode(",", trim(trim($request->get('cities', ''), ","))))->filter(function ($val) {
                return $val != null || $val != "";
            })->toArray();

            $blocked_users_list = blocked_users_list();
            $per_page           = $request->get('per_page', 10);
            $authUser           = auth()->user();
            $page               = $request->page ?? 1;
            $skip               = ($page - 1) * $per_page;
            $search             = $request->get('q', null);
            $topic              = $request->get('topic', null);
            $order              = strtolower($request->get('order', 'new'));
            $activity_type      = $request->get('my_activity_type', null);
            $answer_user_id     = $request->get('answers_user_id', null);
            $response           = [];

            $per_page           = (int) $per_page;
            $page               = (int) $page;

            $discussionsQuery = Discussion::query()
                ->where('language_id', $langugae_id)
                ->when($search, function ($q) use ($search) {
                    return $q->where('question', 'like', trim($search) . "%");
                })
                ->when($topic, function ($query) use ($topic) {
                    return $query->whereHas('topics', function ($q) use ($topic) {
                        return $q->where('topics', $topic);
                    });
                })
                ->when(is_array($cities) && is_array($countries) && (count($cities) || count($countries)), function ($q) use ($countries, $cities) {
                    return $q->where('destination_id', array_merge($countries, $cities));
                })
                ->when($activity_type, function ($q) use ($activity_type) {
                    if ($activity_type == "discussions") {
                        return $q->where('discussions.users_id', Auth::user()->id);
                    } else if ($activity_type == "replies") {
                        $discussionsIds = DiscussionReplies::query()->select('discussions_id')->byAuthUser()->pluck('discussions_id');
                        return $q->whereIn('discussions.id', $discussionsIds);
                    }
                    return $q;
                })
                ->when($answer_user_id, function ($q) use ($answer_user_id) {
                    $discussionsIds = DiscussionReplies::query()->select('discussions_id')->where('users_id', $answer_user_id)->pluck('discussions_id');
                    return $q->whereIn('discussions.id', $discussionsIds);
                })
                ->whereNotIn('discussions.users_id', $blocked_users_list);

            $total_discussions = (clone $discussionsQuery)->count();

            $discussions = $discussionsQuery->with('medias', 'author')->when($order, function ($q) use ($order) {
                switch ($order) {
                    case 'old':
                        return $q->orderBy('created_at', 'ASC');
                        break;
                    case 'new':
                        return $q->orderBy('created_at', 'DESC');
                        break;
                    default:
                        return $q->withCount([
                            'replies'  => function ($q) {
                                $q->where('created_at', '>', Carbon::now()->subDays(60));
                            },
                            'likes'  => function ($q) {
                                $q->where('vote', 1);
                                $q->where('created_at', '>', Carbon::now()->subDays(60));
                            },
                        ])->orderByDesc(DB::raw('likes_count + replies_count'));
                        break;
                }
            })->skip($skip)->take($per_page)->get();

            $has_more  = true;
            if (count($discussions) == 0) {
                if ($langugae_id == UsersContentLanguages::DEFAULT_LANGUAGES) {
                    $has_more  = false;
                } else {
                    $newRequest = new SearchDiscussionRequest;
                    $params = $request->all();
                    $params['langugae_id'] = UsersContentLanguages::DEFAULT_LANGUAGES;
                    $newRequest->merge($params);
                    return $this->searchDiscussion($newRequest);
                }
            }
            // return [($discussionsQuery->toSql())];
            // return ApiResponse::create($discussions);

            $discussions = $discussions->map(function ($discussion) use ($authUser) {

                if ($discussion->author) {
                    $discussion->author->profile_picture = check_profile_picture($discussion->author->profile_picture);
                }

                // up/down vote + share + media + views
                $discussion->upvote_flag = $authUser ? ($discussion->discussion_upvotes()->where('users_id', $authUser->id)->exists()) : false;
                $discussion->downvotes_flag = $authUser ? ($discussion->discussion_downvotes()->where('users_id', $authUser->id)->exists()) : false;
                $discussion->total_upvotes = $discussion->discussion_upvotes()->count();
                $discussion->total_downvotes = $discussion->discussion_downvotes()->count();
                $discussion->total_share = PostsShares::where('type', ShareService::TYPE_DISCUSSION)->where('posts_type_id', $discussion->id)->count();
                $discussion->total_views = $discussion->views()->count();
                $_medias = [];
                $discussion->medias->map(function ($media) use (&$_medias) {
                    $media->media_url = check_media_url($media->media_url);
                    if (url(PATTERN_PLACEHOLDERS) != $media->media_url) {
                        $media->media_url = replace_s3_path_with_cloudfront($media->media_url, '200x0');
                    }
                    $media->url = $media->media_url;
                    if (url(PATTERN_PLACEHOLDERS) != $media->media_url || url(PATTERN_PLACEHOLDERS) != $media->url) {
                        $_medias[] = $media;
                    }
                    return $media;
                });
                unset($discussion->medias);
                $discussion->medias = $_medias;

                //experts 
                $discussion->total_experts = $discussion->experts->count();
                $latest_experts = [];
                foreach ($discussion->experts->take(5) as $expert) {
                    $expert->user = User::select('id', 'profile_picture', 'email', 'name', 'username', 'type', 'expert', 'cover_photo')->where('id', $expert->expert_id)->first();
                    if (!$expert->user) continue;
                    $expert->user->profile_picture = check_profile_picture($expert->user->profile_picture);
                    $latest_experts[] = $expert;
                }
                $discussion->latest_experts = $latest_experts;
                $discussion->is_tagged = $authUser ? $discussion->experts()->where('expert_id', $authUser->id)->exists() : false;

                //destination 
                $discussion->destination_type = strtolower($discussion->destination_type);
                if ($discussion->destination_type == "place") {
                    $place = Place::find($discussion->destination_id);
                    if ($place) {
                        $place->title = isset($place->trans[0]->title) ? $place->trans[0]->title : null;
                        $place->image = check_place_photo($place);
                        unset($place->trans, $place->medias);
                    }
                    $discussion->place = $place;
                } else if ($discussion->destination_type == "country") {
                    $country = Countries::find($discussion->destination_id);
                    if ($country) {
                        $country->title = isset($country->transsingle->title) ? $country->transsingle->title : null;
                        $country->image = (isset($country->medias[0]->media->path)) ? check_country_photo($country->medias[0]->media->path, 180) : url('assets2/image/placeholders/pattern.png');
                        unset($country->transsingle, $country->medias);
                    }
                    $discussion->country = $country;
                } else if ($discussion->destination_type == "city") {
                    $city = Cities::find($discussion->destination_id);
                    if ($city) {
                        $city->title = isset($city->trans[0]->title) ? $city->trans[0]->title : null;
                        $city->image = (isset($city->medias[0]->media->path)) ? check_city_photo($city->medias[0]->media->path) : url('assets2/image/placeholders/pattern.png');
                        unset($city->trans, $city->medias);
                    }
                    $discussion->city = $city;
                }

                // reply section == comment section
                $discussion->total_replies = $discussion->replies()->where('parents_id', 0)->count();
                $discussion->comment_flag = $authUser ? ($discussion->replies()->where('users_id', $authUser->id)->count() ? true : false) : false;
                $latest_replies =  $discussion->replies()->take(3)->get();
                foreach ($latest_replies as $parent_comment) {
                    // For Parent Comment
                    $parent_comment->text = isset($parent_comment->tags) ? convert_post_text_to_tags(strip_tags($parent_comment->reply), $parent_comment->tags, false) : strip_tags($parent_comment->text);

                    $parent_comment->upvote_flag = $authUser ? ($parent_comment->upvotes()->where('users_id', $authUser->id)->exists()) : false;
                    $parent_comment->downvotes_flag = $authUser ? ($parent_comment->downvotes()->where('users_id', $authUser->id)->exists()) : false;
                    $parent_comment->total_upvotes = $parent_comment->upvotes()->count();
                    $parent_comment->total_downvotes = $parent_comment->downvotes()->count();

                    $parent_comment->comment_flag = $authUser ? ($parent_comment->sub()->where('users_id', $authUser->id)->count() ? true : false) : false;

                    if ($parent_comment->author) {
                        $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                    }
                    unset($parent_comment->tags, $parent_comment->reply, $parent_comment->upvotes, $parent_comment->downvotes);

                    // For Sub Comment
                    if ($parent_comment->sub) {
                        foreach ($parent_comment->sub as $sub_comment) {
                            $sub_comment->text = isset($sub_comment->tags) ? convert_post_text_to_tags(strip_tags($sub_comment->reply), $sub_comment->tags, false) : strip_tags($sub_comment->text);

                            $sub_comment->upvote_flag = $authUser ? ($sub_comment->upvotes()->where('users_id', $authUser->id)->exists()) : false;
                            $sub_comment->downvotes_flag = $authUser ? ($sub_comment->downvotes()->where('users_id', $authUser->id)->exists()) : false;
                            $sub_comment->total_upvotes = $sub_comment->upvotes()->count();
                            $sub_comment->total_downvotes = $sub_comment->downvotes()->count();

                            if ($sub_comment->author) {
                                $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                            }
                            unset($sub_comment->tags, $sub_comment->reply, $sub_comment->upvotes, $sub_comment->downvotes);
                        }
                    }
                }

                unset(
                    $discussion->discussion_downvotes,
                    $discussion->discussion_upvotes,
                    $discussion->experts,
                    $discussion->replies,
                    $discussion->views
                );

                $discussion->replies = $latest_replies;

                return $discussion;
            });

            $feeds = [];
            $i = 0;
            $guessItemNo = rand(5, 10);
            $guessPageNo = rand(1, 3);
            foreach ($discussions as $discussion) {
                $i++;
                if ($guessPageNo == $page && $guessItemNo == $i) {
                    $feeds[] =  [
                        'unique_link'   => null,
                        'type'          => 'what_people_are_talking_about',
                        'action'        => null,
                        'data'          => DB::table('discussions')
                            ->select('destination_type', 'destination_id', DB::raw('count(*) as total_talking'))
                            ->whereNotIn('users_id', $blocked_users_list)
                            ->groupBy('destination_type', 'destination_id')
                            ->inRandomOrder()->take(30)->get()->map(function ($item) {
                                if (strtolower($item->destination_type) == "place") {
                                    $place = Place::find($item->destination_id);
                                    if ($place) {
                                        $place->title = isset($place->trans[0]->title) ? $place->trans[0]->title : null;
                                        $place->image = check_place_photo($place);
                                        unset($place->trans, $place->medias);
                                    }
                                    $item->data = $place;
                                } else if (strtolower($item->destination_type) == "country") {
                                    $country = Countries::find($item->destination_id);
                                    if ($country) {
                                        $country->title = isset($country->transsingle->title) ? $country->transsingle->title : null;
                                        $country->image = (isset($country->medias[0]->media->path)) ? check_country_photo($country->medias[0]->media->path, 180) : url('assets2/image/placeholders/pattern.png');
                                        unset($country->transsingle, $country->medias);
                                    }
                                    $item->data = $country;
                                } else if (strtolower($item->destination_type) == "city") {
                                    $city = Cities::find($item->destination_id);
                                    if ($city) {
                                        $city->title = isset($city->trans[0]->title) ? $city->trans[0]->title : null;
                                        $city->image = (isset($city->medias[0]->media->path)) ? check_city_photo($city->medias[0]->media->path) : url('assets2/image/placeholders/pattern.png');
                                        unset($city->trans, $city->medias);
                                    }
                                    $item->data = $city;
                                }
                                return $item;
                            })
                    ];
                }


                $feeds[] =  [
                    'unique_link'   => url("newsfeed/" . $discussion->author->username . "/discussion/" . $discussion->id),
                    'type'          => strtolower(PrimaryPostService::TYPE_DISCUSSION),
                    'action'        => 'create',
                    'data'          => $discussion
                ];
            }

            $result = DB::select("SELECT 
                    (SELECT COUNT(DISTINCT destination_id) FROM discussions WHERE destination_type = 'city' AND deleted_at IS NULL) AS total_countries, 
                    (SELECT COUNT(DISTINCT destination_id) FROM discussions WHERE destination_type = 'country' AND deleted_at IS NULL) AS total_cities")[0];

            $response['counters'] = ['countries' => $result->total_countries, 'cities' => $result->total_cities];
            $response['counters']['all'] = $response['counters']['countries'] + $response['counters']['cities'];

            $response['is_authorized_user'] = $authUser ? true : false;
            $response['total']              = $total_discussions;
            $response['has_more']           = $has_more;
            $response['langugae_id']        = $langugae_id;
            $response['per_page']           = $per_page;
            // $response['total_page']         = ceil($total_discussions / $per_page);
            $response['current_page']       = $page;
            $response['data']               = $feeds;

            return ApiResponse::create($response);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/discussions/tabs/{tab_name}",
     *   tags={"Discussions"},
     *   summary="Fetch All Disscusions Tabs",
     * * @OA\Parameter(
     *      name="tab_name",
     *      description="Filter => activity, trending_topics, most_helpful",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    public function tabs(Request $request, $tab_name)
    {
        try {
            $tab = [];
            if (!in_array($tab_name, ['activity', 'trending_topics', 'most_helpful'])) {
                return ApiResponse::__createBadResponse("tab_name must be in list from activity, trending_topics, and most_helpful");
            }
            $authUser           = auth()->user();
            if ($tab_name == "activity") {
                if ($authUser) {
                    $tab['activity']['total_discussion'] =  Discussion::where('users_id', $authUser->id)->count();
                    $tab['activity']['total_replies'] = DiscussionReplies::where('users_id', $authUser->id)->count();
                    $tab['activity']['recent_discussion'] = Discussion::select('id', 'question', 'created_at')
                        ->where('users_id', $authUser->id)
                        ->orderBy('created_at', 'desc')->take(3)->get();
                } else {
                    $tab['activity']['total_discussion'] = 0;
                    $tab['activity']['total_replies'] = 0;
                    $tab['activity']['recent_discussion'] = [];
                }
            }
            if ($tab_name == "trending_topics") {
                $tab['trending_topics'] = $this->__trendingTopic();
            }
            if ($tab_name == "most_helpful") {
                $tab['most_helpful'] = $this->__mostHelpful();
            }
            return ApiResponse::create($tab[$tab_name]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param string $str
     * @return string
     */
    protected function _orderTitle($str)
    {
        $title_arr = array(
            'dateasc' => trans('discussion.strings.date_down_up'),
            'datedesc' => trans('discussion.strings.date_up_down'),
            'liked' => trans('discussion.strings.most_liked'),
            'commented' => trans('discussion.strings.most_commented')
        );

        return $title_arr[$str];
    }


    /**
     * @OA\GET(
     ** path="/discussions/{discussion_id}",
     *   tags={"Discussions"},
     *   summary="Get discussion details",
     *   security={
     *       {"bearer_token": {}
     *    }},
     * *   @OA\Parameter(
     *        name="discussion_id",
     *        description="Discussion id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param integer $discussion_id
     * @return \Illuminate\Http\Response
     */
    public function getView($discussion_id)
    {
        try {
            $discussion = Discussion::where('id', $discussion_id)->first();
            if ($discussion) {
                $discussion->replies->each(function ($reply) {
                    $reply->author;
                });
                $discussion->views_count = count($discussion->views);
                unset($discussion->views);
                $discussion->shares;
                $discussion->author;
                $discussion->discussion_downvotes;
                $discussion->discussion_upvotes;
                $discussion->likes;
                return ApiResponse::create($discussion);
            } else {
                return ApiResponse::__createBadResponse('Discussion not found.');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/discussions/create",
     *   tags={"Discussions"},
     *   summary="Create discussion",
     *   operationId="create_discussion",
     *   security={
     *       {"bearer_token": {}
     *           }},
     *   @OA\Parameter(
     *       name="destination_type",
     *       description="place, city, country",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     * @OA\Parameter(
     *       name="destination_id",
     *       description="place_id, city_id, country_id",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="question",
     *       description="Question [max:150]",
     *       required=true,
     *       in="query",
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="description",
     *       description="Description [max:1000]",
     *       required=false,
     *       in="query",
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="pair",
     *       description="pair : for media uploading : using common media api",
     *       in="query",
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="topics",
     *       description="Topics",
     *       in="query",
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     * @OA\Parameter(
     *        name="experts[]",
     *        description="pass experts_id",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param CreateDiscussionRequest $request
     * @param EnlargedViewsService $enlargedViewsService
     * @return \Illuminate\Http\Response
     */
    public function postCreate(CreateDiscussionRequest $request,  TranslationService $translationService)
    {
        try {
            $authUser               = auth()->user();
            $user_id                = $authUser->id;
            $pair                   = $request->input("pair", "");
            $file_lists             = app(CommonApiController::class)->getTempFiles($pair);
            $isFiles                = count($file_lists) > 0 ? true : false;
            $question               = $request->question;
            $description            = processString($request->description);
            $type                   = 1;
            $destination_id         = $request->destination_id;
            $destination_type       = $request->destination_type;
            $with_experts           = false;

            // Validate experts exists or not
            if ($request->experts && is_array($request->experts) && count($request->experts)) {
                $with_experts = true;
                $findUsers = User::whereIn('id', $request->experts)->pluck('id');
                if (count($request->experts) != count($findUsers)) {
                    $notFoundUsers =  array_values(array_diff(
                        $request->experts,
                        $findUsers->toArray()
                    ));
                    if (count($notFoundUsers)) {
                        return ApiResponse::__createBadResponse(implode(",", $notFoundUsers) . " experts not found");
                    }
                }
            }

            switch ($destination_type) {
                case 'city':
                    $country_id = Cities::find($destination_id)->country->id;
                    break;
                case 'place':
                    $country_id = Place::find($destination_id)->country->id;
                    break;
                default:
                    $country_id = $destination_id;
            }

            $disc                   = new Discussion;
            $disc->users_id         = $user_id;
            $disc->type             = $type;
            $disc->question         = $question;
            $disc->description      = $description;
            $disc->destination_id   = $destination_id;
            $disc->destination_type = $destination_type;
            $disc->countries_id     = $country_id;
            // $disc->topic            = $request->topics;
            $disc->language_id      = $translationService->getLanguageId($question . ' ' . $description);

            if ($disc->save()) {
                $this->rankingService->addPointsEarners($disc->id, get_class($disc), $user_id);

                if ($request->topics) {
                    $topics = explode(',', $request->topics);
                    foreach ($topics as $topic) {
                        $disc_topic = new DiscussionTopics();
                        $disc_topic->discussions_id = $disc->id;
                        $disc_topic->topics = $topic;
                        $disc_topic->save();
                    }
                }

                if ($isFiles) {
                    foreach ($file_lists as $k => $file) {
                        $filename = $user_id . '_' . $k . '_' . time() . '_' .  preg_replace("/([#]|[%]|[+]|[$])/", "_", $file[1]);
                        Storage::disk('s3')->put('discussion-photo/' . $filename, fopen($file[0], 'r+'), 'public');

                        $discussion_media = new DiscussionMedias();
                        $discussion_media->discussion_id = $disc->id;
                        $discussion_media->media_url = S3_BASE_URL . 'discussion-photo/' . $filename;
                        $discussion_media->save();
                    }
                    app(CommonApiController::class)->deleteTempFiles($pair);
                }

                if ($with_experts) {
                    foreach ($request->experts as $expert) {
                        $exp = new DiscussionExperts();
                        $exp->discussion_id =  $disc->id;
                        $exp->expert_id = $expert;

                        if ($exp->save()) {
                            notify($expert, 'ask_experts', $disc->id);
                        }
                    }
                }

                // attech relationship
                $disc->author->profile_picture  = check_profile_picture($disc->author->profile_picture);
                $disc->medias;
                if ($disc->experts) {
                    foreach ($disc->experts as $de) {
                        if ($de->user) {
                            $de->user->profile_picture = check_profile_picture($de->user->profile_picture);
                        }
                    }
                }

                log_user_activity('Discussion', 'create', $disc->id);

                return ApiResponse::create([
                    'discussion' => $disc
                ]);
            } else {
                return ApiResponse::__createBadResponse("Discussion can not create. please try again");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CheckUpDownVoteRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCheckUpDownVote(CheckUpDownVoteRequest $request)
    {
        try {
            $reply_id = $request->get('replies_id');
            $reply = DiscussionReplies::find($reply_id);
            if (!$reply) {
                return ApiResponse::__createBadResponse('Reply not found.');
            }
            $count_upvotes = count($reply->upvotes);
            $count_downvotes = count($reply->downvotes);

            $data['reply_id'] = $reply_id;
            $data['count_upvotes'] = $count_upvotes;
            $data['count_downvotes'] = $count_downvotes;
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/discussions/{discussion_id}/updownvote",
     *   tags={"Discussions"},
     *   summary="Discussion upvote or downvote",
     *   operationId="Api/DiscussionController@postUpDownVotes",
     *   security={
     *       {"bearer_token": {}
     *           }},
     * @OA\Parameter(
     *       name="discussion_id",
     *       description="",
     *       in="path",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     * @OA\Parameter(
     *       name="type",
     *       description="UPVOTE_TYPE: 1, DOWNVOTE_TYPE: 0",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function postUpDownVotes(Request $request, $discussion_id)
    {
        try {
            $user_id    = Auth::user()->id;
            $type       = $request->type ?? -1;

            if (!in_array($type, [0, 1])) {
                return ApiResponse::create([
                    'type' => ['Type must be in list 0 and 1']
                ], false, ApiResponse::VALIDATION);
            }

            $discussion = Discussion::find($discussion_id);
            if (!$discussion) return ApiResponse::__createBadResponse('Discussion not found.');

            $discussionVotes = DiscussionLikes::where('discussions_id', $discussion_id)->where('users_id', $user_id)->first();
            if (!$discussionVotes) {
                DiscussionLikes::create([
                    'discussions_id' => $discussion_id,
                    'users_id' => $user_id,
                    'vote' => $type
                ]);
            } else {
                if ($discussionVotes->vote == $type) {
                    $discussionVotes->delete();
                } else {
                    $discussionVotes->update([
                        'vote' => $type
                    ]);
                }
            }

            return ApiResponse::create([
                'upvote_flag'       => $discussion->discussion_upvotes()->where('users_id', $user_id)->first() ? true : false,
                'downvotes_flag'    => $discussion->discussion_downvotes()->where('users_id', $user_id)->first() ? true : false,
                'total_upvotes'     => $discussion->discussion_upvotes()->count(),
                'total_downvotes'   => $discussion->discussion_downvotes()->count()
            ]);
        } catch (\Throwable $th) {
            return ApiResponse::createServerError($th);
        }
    }

    /**
     * @OA\Post(
     ** path="/discussions/{discussion_id}/reply/{reply_id}/updownvote",
     *   tags={"Discussions"},
     *   summary="Discussion upvote or downvote",
     *   operationId="Api/DiscussionController@postUpDownReplyVotes",
     *   security={
     *       {"bearer_token": {}
     *           }},
     * @OA\Parameter(
     *       name="discussion_id",
     *       description="",
     *       in="path",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     * @OA\Parameter(
     *       name="reply_id",
     *       description="",
     *       in="path",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     * @OA\Parameter(
     *       name="type",
     *       description="UPVOTE_TYPE: 1, DOWNVOTE_TYPE: 0",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function postUpDownReplyVotes(Request $request, DiscussionsService $discussionsService, $discussion_id, $reply_id)
    {
        try {
            $user_id    = Auth::user()->id;
            $type       = $request->type ?? -1;

            if (!in_array($type, [0, 1])) {
                return ApiResponse::create([
                    'type' => ['Type must be in list 0 and 1']
                ], false, ApiResponse::VALIDATION);
            }

            $discussion = Discussion::find($discussion_id);
            if (!$discussion) return ApiResponse::__createBadResponse('Discussion not found.');

            $reply = DiscussionReplies::where('discussions_id', $discussion->id)->where('id', $reply_id)->first();
            if (!$reply) return ApiResponse::__createBadResponse('Reply not found.');

            $checkUpvote = DiscussionRepliesUpvotes::where('discussions_id', $discussion->id)->where('replies_id', $reply->id)->where('users_id', $user_id)->first();
            $checkDownvote = DiscussionRepliesDownvotes::where('discussions_id', $discussion->id)->where('replies_id', $reply->id)->where('users_id', $user_id)->first();

            $isDelete = false;
            if ($checkUpvote && $type == 1) {
                $checkUpvote->delete();
                $isDelete = true;
            }

            if ($checkDownvote && $type == 0) {
                $checkDownvote->delete();
                $isDelete = true;
            }

            $insertObj = null;
            if (!$isDelete) {
                if ($type == 1) {
                    if ($checkDownvote) {
                        $checkDownvote->delete();
                        $this->rankingService->subPointsFromEarners($reply);
                    }
                    $insertObj = new DiscussionRepliesUpvotes;
                } else if ($type == 0) {
                    if ($checkUpvote) {
                        $checkUpvote->delete();
                    }
                    $insertObj = new DiscussionRepliesDownvotes;
                }
                if ($insertObj) {
                    $insertObj->discussions_id = $discussion->id;
                    $insertObj->replies_id = $reply->id;
                    $insertObj->users_id = $user_id;
                    $insertObj->save();
                    $this->rankingService->addPointsToEarners($reply);
                }
            }

            $upvote_flag = $reply->upvotes->where('users_id', $user_id)->exists();
            $downvotes_flag = $reply->downvotes->where('users_id', $user_id)->exists();
            if ($upvote_flag) $discussionsService->updatePopularCount($discussion_id);
            if ($downvotes_flag) $discussionsService->updatePopularCount($discussion_id, false);

            return ApiResponse::create([
                'upvote_flag'       => $upvote_flag,
                'downvotes_flag'    => $downvotes_flag,
                'total_upvotes'     => $reply->upvotes->count(),
                'total_downvotes'   => $reply->downvotes->count()
            ]);
        } catch (\Throwable $th) {
            return ApiResponse::createServerError($th);
        }
    }

    /**
     * @param integer $reply_id
     * @return object
     */
    public function _construct_reply_by_filter($reply_id)
    {
        $reply = DiscussionReplies::find($reply_id);
        $count_upvotes = count($reply->upvotes);
        $count_downvotes = count($reply->downvotes);

        $data = $reply;
        $data['count_upvotes'] = $count_upvotes;
        $data['count_downvotes'] = $count_downvotes;

        return $data;
    }

    /**
     * @param ReportSpamRequest $request
     * @param DiscussionReplies $reply
     * @param SpamsRepliesService $spamsRepliesService
     * @return \Illuminate\Http\Response
     */
    public function reportSpam(ReportSpamRequest $request, DiscussionReplies $reply, SpamsRepliesService $spamsRepliesService)
    {
        try {

            $type = $request->input('type');
            $text = $request->input('text');
            $reply_id = $request->input('reply_id');
            $report = new SpamsReplies();

            switch ($type) {
                case SpamsRepliesService::REPORT_TYPE_SPAM:
                    $report->report_type = SpamsRepliesService::REPORT_TYPE_SPAM_CODE;
                    break;
                case SpamsRepliesService::REPORT_TYPE_OTHER:
                    $report->report_type = SpamsRepliesService::REPORT_TYPE_OTHER_CODE;
                    break;
            }

            if (!$report->report_type) {
                throw new \Exception('wrong type');
            }

            $report->users_id    = Auth::id();
            $report->report_text = $text;
            $report->reply_id    = $reply_id;

            $report->save();
            return ApiResponse::create(
                [
                    'message' => ["Thanks for your report.",]
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Put(
     ** path="/discussions/edit-reply/{reply_id}",
     *   tags={"Discussions"},
     *   summary="Reply edit",
     *   operationId="reply_edit",
     *   security={
     *       {"bearer_token": {}
     *           }},
     *               @OA\Parameter(
     *                   name="reply_id",
     *                   in="path",
     *                   description="Reply id",
     *                   required= true,
     *                   @OA\Schema(
     *                       type="integer"
     *                   )
     *              ),
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="media_type",
     *                  type="string",
     *                  description="File",
     *              ),
     *              @OA\Property(
     *                  property="text",
     *                  type="string",
     *                  description="image"
     *              ),
     *              @OA\Property(
     *                  property="media_url",
     *                  type="string",
     *                  description="Media url"
     *              ),
     *              @OA\Property(
     *                  property="reply_id",
     *                  type="integer",
     *                  description="Reply id"
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param EditReplyRequest $request
     * @return \Illuminate\Http\Response
     */
    public function putEditReply(EditReplyRequest $request)
    {
        try {
            $reply = DiscussionReplies::find($request->get('reply_id', 0));
            if ($reply) {
                if ($reply->users_id !== Auth::id()) {
                    return ApiResponse::__createUnAuthorizedResponse('only reply author can edit reply.');
                } else {
                    $reply->reply = processString($request->get('text'));
                    if ($reply->save()) {
                        $data['reply_id'] = $reply->id;
                        $data['reply'] = $this->_construct_reply_by_filter($reply->id);
                        log_user_activity('Discussion', 'edit_reply', $reply->discussion->id);
                        return ApiResponse::create($data);
                    } else {
                        return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
                    }
                }
            } else {
                return ApiResponse::__createBadResponse('Reply not found.');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Delete(
     ** path="/discussions/delete-reply/{reply_id}",
     *   tags={"Discussions"},
     *   summary="Delete discussion reply",
     *   security={
     *       {"bearer_token": {}
     *    }},
     * *   @OA\Parameter(
     *        name="reply_id",
     *        description="Reply id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param integer $reply_id
     * @return \Illuminate\Http\Response
     */
    public function deleteReply($reply_id)
    {
        try {
            $reply = DiscussionReplies::find($reply_id);
            if ($reply) {
                if ($reply->users_id !== Auth::id()) {
                    return ApiResponse::__createUnAuthorizedResponse('only reply author can delete reply.');
                } else {
                    $data['success'] = false;
                    if ($reply->delete()) {
                        $data['success'] = true;
                        log_user_activity('Discussion', 'delete_reply', $reply->discussion->id);
                    }
                    return ApiResponse::create($data);
                }
            } else {
                return ApiResponse::__createBadResponse('Reply not found.');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @param DiscussionViewCountRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postDiscussionViewCount(DiscussionViewCountRequest $request)
    {
        try {
            $user_id = Auth::user()->id;
            $discussion_id = $request->discussion_id;
            $discussion = Discussion::find($discussion_id);
            if (!$discussion) {
                return ApiResponse::__createBadResponse('Discussion not found.');
            }
            if (count($discussion->views()->where('users_id', $user_id)->get()) == 0) {
                $discussion_view = new DiscussionViews;
                $discussion_view->discussions_id = $discussion_id;
                $discussion_view->users_id = $user_id;

                $discussion_view->save();

                //log_user_activity('Discussion', 'view', $discussion_id);
                return ApiResponse::create(
                    [
                        'status' => 1,
                        'count_views' => $discussion->views()->count()
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'status' => 2,
                    ]
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Delete(
     ** path="/discussions/{discussion_id}/delete",
     *   tags={"Discussions"},
     *   summary="Delete discussion",
     *   security={
     *       {"bearer_token": {}
     *    }},
     * *   @OA\Parameter(
     *        name="discussion_id",
     *        description="Discussion id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     */
    /**
     * @param integer $discussion_id
     * @return \Illuminate\Http\Response
     */
    public function postDelete($discussion_id)
    {

        try {
            $temp_dir = public_path() . '/assets2/upload_tmp/';

            if (is_dir($temp_dir)) {
                if ($handle = opendir($temp_dir)) {
                    while (false !== ($file = readdir($handle))) {
                        if ($file == '.' || $file == '..') {
                            continue;
                        }
                        @unlink($temp_dir . '/' . $file);
                    }
                    closedir($handle);
                }
            }

            $user_id = Auth::user()->id;
            $disc = Discussion::where('users_id', $user_id)->where('id', $discussion_id)->first();
            if ($disc) {

                $disc_medias = $disc->medias;

                if ($disc_medias) {
                    foreach ($disc_medias as $disc_media) {
                        $disc_media->delete();
                    }
                }

                $disc_experts = $disc->experts;

                if ($disc_experts) {
                    foreach ($disc_experts as $disc_expert) {
                        $disc_expert->delete();
                    }
                }

                $disc_replies = $disc->replies;

                if ($disc_replies) {
                    foreach ($disc_replies as $disc_reply) {
                        $disc_reply->delete();
                    }
                }
                $disc->delete();
                log_user_activity('Discussion', 'delete', $discussion_id);
                return ApiResponse::__create("Discussion Delete successfully");
            } else {
                return ApiResponse::__createBadResponse('Discussion not found.');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param PostLikeUnlikeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postLikeUnlike(PostLikeUnlikeRequest $request)
    {
        try {
            if ($request->post_id) {
                $post_id = $request->post_id;
                $user_id = Auth::user()->id;
                $discussionData =  Discussion::find($post_id);

                if ($discussionData) {
                    $author_id = $discussionData->users_id;
                    $check_like_exists = DiscussionLikes::where('discussions_id', $post_id)->where('users_id', $user_id)->first();
                    $data = array();

                    if (is_object($check_like_exists)) {
                        //dd($check_like_exists);
                        $check_like_exists->delete();
                        log_user_activity('Discussion', 'unlike', $post_id);
                        //notify($author_id, 'status_unlike', $post_id);

                        $data['status'] = false;
                    } else {
                        $like = new DiscussionLikes;
                        $like->discussions_id = $post_id;
                        $like->users_id = $user_id;
                        $like->save();

                        log_user_activity('Discussion', 'like', $post_id);
                        notify($author_id, 'status_like', $post_id);

                        $data['status'] = true;
                    }
                    $data['count'] = count(DiscussionLikes::where('discussions_id', $post_id)->get());
                    return ApiResponse::create($data);
                } else {
                    return ApiResponse::__createBadResponse('Discussion not found.');
                }
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postShareUnshare(Request $request)
    {
        try {
            $post_id = $request->post_id;
            $user_id = Auth::user()->id;
            $discussionData =  Discussion::find($post_id);

            if (is_object($discussionData)) {
                $author_id = $discussionData->users_id;
                $check_share_exists = DiscussionShares::where('discussions_id', $post_id)->where('users_id', $user_id)->first();
                $data = array();

                if (is_object($check_share_exists)) {
                    $shareid = $check_share_exists->id;
                    $check_share_exists->delete();
                    log_user_activity('Discussion', 'unshare', $post_id);
                    // notify($author_id, 'status_unlike', $post_id);

                    $data['status'] = 'no';
                } else {
                    $share = new DiscussionShares;
                    $share->discussions_id = $post_id;
                    $share->users_id = $user_id;
                    $share->save();

                    log_user_activity('Discussion', 'share', $post_id);
                    // notify($author_id, 'status_share', $post_id);

                    $data['status'] = 'yes';
                }

                $data['count'] = count(DiscussionShares::where('discussions_id', $post_id)->get());
                return ApiResponse::create($data);
            } else {
                return ApiResponse::__createBadResponse('Discussion not found.');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/discussions/search/about",
     *   tags={"Discussions"},
     *   summary="search country, city, place",
     *   operationId="Api/DiscussionController@getAboutDiscussion",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *       name="search",
     *       description="",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     * @param Request $request
     * @return ApiResponse
     */
    public function getAboutDiscussion(Request $request)
    {
        try {
            $locations = [
                // 'country'   => ['type' => 'country', 'data' => []],
                // 'city'      => ['type' => 'city', 'data' => []],
                // 'place'     => ['type' => 'place', 'data' => []]
            ];

            $queryParam = trim($request->get('search', ''));
            $suggestion = (!strlen($queryParam)) ? true : false;

            $qr = app(HomeController::class)->getElSearchResult($queryParam)['result'];
            foreach ($qr as $r) {
                if ($r['countries_id'] != 326 && $r['cities_id'] != 2031 && $r['countries_id'] != 373) {
                    $place = Place::find(@$r['place_id']);
                    if ($place) {
                        $locations[] = [
                            'id' => $place->id,
                            'text' => @$place->transsingle->title,
                            'description' => '',
                            'image' => check_place_photo($place),
                            'type' => 'place',
                        ];
                    }
                }
            }

            $get_cities = Cities::with('transsingle', 'country.transsingle')->take(15)
                ->when(strlen($queryParam), function ($q) use ($queryParam) {
                    return $q->whereHas('transsingle', function ($query) use ($queryParam) {
                        $query->where('title', 'like', $queryParam . "%");
                    });
                })->get();

            $get_countries = Country::with('transsingle')->take(15)
                ->when(strlen($queryParam), function ($q) use ($queryParam) {
                    return $q->whereHas('transsingle', function ($query) use ($queryParam) {
                        $query->where('title', 'like', $queryParam . "%");
                    });
                })->get();

            foreach ($get_countries as $c) {
                $locations[] = [
                    'id' => $c->id,
                    'text' => @$c->transsingle->title,
                    'description' => '',
                    'image' => get_country_flag($c),
                    'type' => 'country',
                ];
            }

            foreach ($get_cities as $city) {
                $locations[] = [
                    'id' => $city->id,
                    'text' => @$city->transsingle->title,
                    'description' => 'city in ' . $city->country->transsingle->title,
                    'image' => check_city_photo(@$city->getMedias[0]->url),
                    'type' => 'city',
                ];
            }

            // $get_places = Place::take(15)->with('transsingle')
            //     ->when(strlen($queryParam), function ($q) use ($queryParam) {
            //         return $q->whereHas('transsingle', function ($query) use ($queryParam) {
            //             $query->where('title', 'like', $queryParam . "%");
            //         });
            //     })->get();
            // foreach ($get_places as $place) {
            //     $locations[] = [
            //         'id' => $place->id,
            //         'text' => $place->transsingle->title,
            //         'description' => '',
            //         'image' => check_place_photo($place),
            //         'type' => 'place',
            //     ];
            // }

            $result = collect(array_values($locations));

            return ApiResponse::create([
                'suggestion' => $suggestion,
                'locations' => $result,
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     *  * @OA\Get(
     ** path="/discussions/search/experts",
     *   tags={"Discussions"},
     *   summary="get experts based on destination (type,id must be required)",
     *   operationId="Api/DiscussionController@getExperts",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *       name="search",
     *       description="",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     * @OA\Parameter(
     *       name="destination_type",
     *       description="",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     * @OA\Parameter(
     *       name="destination_id",
     *       description="",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     * @param ExpertsDiscussionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getExperts(ExpertsDiscussionRequest $request)
    {
        try {
            $user               = Auth::user();
            $queryParam         = trim($request->get('search', ''));
            $experts            = $exp_list = [];
            $destination_type   = $request->destination_type;
            $destination_id     = $request->destination_id;
            $suggestion = (!$queryParam) ? true : false;

            switch ($destination_type) {
                case 'place':
                    $place = Place::find($destination_id);
                    if (!$place) {
                        return ApiResponse::__createBadResponse('Place Not Found');
                    }
                    $exp_countries  = ExpertsCountries::with('user')->whereHas('user', function ($query) use ($queryParam, $user) {
                        $query->when(strlen($queryParam), function ($q) use ($queryParam) {
                            $q->where('name', 'like', $queryParam . "%");
                        });
                        $query->where('id', '!=', $user->id);
                    })->where('countries_id', $place->countries_id)->take(20)->get();
                    $exp_cities     = ExpertsCities::with('user')->whereHas('user', function ($query) use ($queryParam, $user) {
                        $query->when(strlen($queryParam), function ($q) use ($queryParam) {
                            $q->where('name', 'like', $queryParam . "%");
                        });
                        $query->where('id', '!=', $user->id);
                    })->where('cities_id', $place->cities_id)->take(20)->get();
                    $exp_list =  $exp_countries->toBase()->merge($exp_cities);
                    break;
                case 'country':
                    $exp_list = ExpertsCountries::with('user')->whereHas('user', function ($query) use ($queryParam, $user) {
                        $query->when(strlen($queryParam), function ($q) use ($queryParam) {
                            $q->where('name', 'like', $queryParam . "%");
                        });
                        $query->where('id', '!=', $user->id);
                    })->where('countries_id', $destination_id)->take(20)->get();
                    break;
                case 'city':
                    $exp_list       = ExpertsCities::with('user')->whereHas('user', function ($query) use ($queryParam, $user) {
                        $query->when(strlen($queryParam), function ($q) use ($queryParam) {
                            $q->where('name', 'like', $queryParam . "%");
                        });
                        $query->where('id', '!=', $user->id);
                    })->where('cities_id', $destination_id)->take(20)->get();
                    break;
            }

            foreach ($exp_list as $exp) {
                if (!isset($experts[$exp->user->id])) {
                    $experts[$exp->user->id] = [
                        'id'        => $exp->user->id,
                        'text'      => $exp->user->name,
                        'image'     => check_profile_picture($exp->user->profile_picture),
                        'ask_count' => DiscussionReplies::where('users_id', $exp->users_id)->count(),
                        'title'     => rtrim($this->_getUserExpertiseList($exp->user->id), ', ')
                    ];
                }
            }

            return ApiResponse::create([
                'suggestion' => $suggestion,
                'experts' => array_values($experts),
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $userId
     * @return array
     */
    protected function _getUserExpertiseList($userId)
    {
        $expertise = '';

        $cities = ExpertsCities::with('cities')->where('users_id', $userId)->get();
        $countries = ExpertsCountries::with('countries')->where('users_id', $userId)->get();
        if (count($cities) > 0) {
            foreach ($cities as $city) {
                $expertise .= $city->cities->transsingle->title . ', ';
            }
        }

        if (count($countries) > 0) {
            foreach ($countries as $country) {
                $expertise .= $country->countries->transsingle->title . ', ';
            }
        }

        return $expertise;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getDiscussionView(Request $request)
    {
        try {
            $discussion_id = $request->discussion_id;
            $discussion = Discussion::find($discussion_id);

            if ($discussion) {
                return ApiResponse::create(
                    [
                        'discussion' => $discussion
                    ]
                );
            } else {
                return ApiResponse::__createBadResponse('Discussion not found.');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getMoreHelpful(Request $request)
    {
        try {
            $page = $request->pagenum;
            $skip = ($page - 1) * 10;
            $this_week_answers = $request->this_week_answer ? $request->this_week_answer : '';

            $most_helpful = DB::table('discussion_replies')
                ->select('users_id', DB::raw('count(*) as total'));

            if ($this_week_answers != '') {
                $most_helpful->where('created_at', '>=', Carbon::now()->subDays(7));
            }


            $most_helpful->groupBy('users_id')
                ->orderBy('total', 'desc')
                ->skip($skip)->take(10);

            $result = $most_helpful->get();

            if (count($result) > 0) {
                return ApiResponse::create(
                    [
                        'most_helpful' => $result
                    ]
                );
            } else {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getTrendingTopic(Request $request)
    {
        try {
            $place = $request->destination_place;
            if ($place != '') {
                $place_type = explode(',', $place);

                $trending_topics = Discussion::whereNotNull('topic')->where('destination_type', $place_type[1])->where('destination_id', $place_type[0])->get();
            } else {
                $trending_topics = Discussion::whereNotNull('topic')->where('created_at', '>=', Carbon::now()->subDays(7))->orderBy('created_at', 'desc')->get();
            }

            $topic_list = [];
            $walked_arr = [];
            if (count($trending_topics) > 0) {
                foreach ($trending_topics as $trending_topic) {
                    $topics = explode(',', $trending_topic->topic);
                    foreach ($topics as $topic) {
                        $topic_low = strtolower($topic);
                        array_walk($walked_arr, function (&$value) {
                            $value = strtolower($value);
                        });

                        if (!in_array($topic_low, $walked_arr)) {
                            $topic_list[] = $walked_arr[] = $topic;
                        }
                    }
                }
            }

            if (count($topic_list) > 0) {
                return ApiResponse::create(
                    [
                        'trending_topics' => $topic_list
                    ]
                );
            } else {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getMoreReplies(Request $request)
    {
        try {
            $page = $request->pagenum;
            $skip = ($page - 1) * 10;
            $disc_id = $request->discussion_id;
            $replyData = [];

            $discussion_info = Discussion::find($disc_id);
            if (!$discussion_info) {
                return ApiResponse::__createBadResponse('Discussion not found.');
            }

            $disc_replies = $discussion_info->replies()->orderBy('num_upvotes', 'desc')->skip($skip)->take(10)->get();

            if (count($disc_replies) > 0) {
                foreach ($disc_replies as $disc_reply) {
                    $replyData[] = $disc_reply;
                }
                return ApiResponse::create($replyData);
            } else {
                return ApiResponse::__createBadResponse('replies not found.');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
