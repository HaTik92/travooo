<?php

namespace App\Http\Controllers\Api;

use App\Events\Api\Plan\PlanDeletedApiEvent;
use App\Events\Api\PlanLogs\PlanLogAddedApiEvent;
use App\Events\Api\PlanLogs\PlanPublicApiEvent;
use Carbon\Carbon;
use App\Models\User\User;
use App\Models\City\Cities;
use App\Models\Place\Place;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Posts\Checkins;
use App\Models\Reviews\Reviews;
use App\Models\User\UsersMedias;
use App\Models\Country\Countries;
use Illuminate\Http\UploadedFile;
use App\Services\Trends\TrendsService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Responses\ApiResponse;
use App\Models\ActivityMedia\Media;
use App\Models\PlacesTop\PlacesTop;
use App\Models\TripPlans\TripPlans;
use App\Http\Controllers\Controller;
use App\Models\Reports\ReportsInfos;
use App\Models\TripPlans\TripsLikes;
use App\Services\Trips\TripsService;
use Illuminate\Support\Facades\Auth;
use App\Models\TripCities\TripCities;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripsShares;
use App\Services\Places\PlacesService;
use App\Events\InvitePeopleToTripEvent;
use App\Events\Plan\PlanDeletedEvent;
use App\Models\TripPlans\TripsComments;
use App\Models\TripPlans\TripsVersions;
use Illuminate\Support\Facades\Storage;
use App\Events\PlanLogs\PlanPublicEvent;
use Illuminate\Support\Facades\Redirect;
use App\Models\TripPlans\PlanActivityLog;
use App\Models\TripPlans\TripsSuggestion;
use App\Exceptions\PlanNotExistsException;
use App\Services\Checkins\CheckinsService;
use App\Http\Requests\Api\Trip\PlanRequest;
use App\Http\Requests\Api\Trip\TripRequest;
use App\Models\Notifications\Notifications;
use App\Models\TripCountries\TripCountries;
use App\Models\TripPlans\TripsDescriptions;
use App\Http\Controllers\Api\HomeController;
use App\Models\TripPlans\TripsCommentsLikes;
use App\Models\TripPlans\TripsCommentsVotes;
use App\Http\Requests\Api\Trip\CreateStepOne;
use App\Models\TripPlans\TripsCommentsMedias;
use App\Services\PlaceChat\PlaceChatsService;
use App\Services\Trips\PlanActivityLogService;
use App\Services\Trips\TripInvitationsService;
use App\Http\Requests\Api\Trip\EditTripRequest;
use App\Models\TravelMates\TravelMatesRequests;
use App\Services\Trips\TripsSuggestionsService;
use App\Http\Requests\Api\Trip\FirstStepRequest;
use App\Http\Requests\Api\Trip\LeavePlanRequest;
use App\Http\Requests\Api\Trip\AddCommentRequest;
use App\Http\Requests\Api\Trip\DeleteTripRequest;
use App\Http\Requests\Api\Trip\LikeUnlikeRequest;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Http\Requests\Api\Trip\CommentTripRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\Api\Trip\SearchPlaceRequest;
use App\Http\Requests\Api\Trip\UploadMediaRequest;
use App\Models\TripPlans\TripsContributionCommits;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\Api\Trip\ActivateTripRequest;
use App\Http\Requests\Api\Trip\CommentReplyRequest;
use App\Http\Requests\Api\Trip\NearbyHotelsRequest;
use App\Http\Requests\Api\Trip\NearbyPlacesRequest;
use App\Http\Requests\Api\Trip\PlanContentsRequest;
use App\Http\Requests\Api\Trip\SearchCitiesRequest;
use App\Http\Requests\Api\Trip\TripDistanceRequest;
use App\Models\TravelMates\TravelMatesRequestUsers;
use App\Models\TripPlans\TripsContributionRequests;
use App\Models\TripPlans\TripsContributionVersions;
use App\Http\Requests\Api\Trip\AddCityToTripRequest;
use App\Http\Requests\Api\Trip\ChangePrivacyRequest;
use App\Http\Requests\Api\Trip\CommentDeleteRequest;
use App\Http\Requests\Api\Trip\InvitedPeopleRequest;
use App\Http\Requests\Api\Trip\InviteFriendsRequest;
use App\Http\Requests\Api\Trip\ModifyStepOneRequest;
use App\Http\Requests\Api\Trip\AddPlaceToTripRequest;
use App\Http\Requests\Api\Trip\CommentTripboxRequest;
use App\Http\Requests\Api\Trip\PeopleToInviteRequest;
use App\Http\Requests\Api\Trip\TripCoverMediaRequest;
use App\Http\Requests\Api\Trip\VersionRespondRequest;
use App\Http\Requests\Api\Trip\EditPlaceInTripRequest;
use App\Http\Requests\Api\Trip\TripDescriptionRequest;
use App\Http\Requests\Api\Trip\AcceptInvitationRequest;
use App\Http\Requests\Api\Trip\ActivateTripCityRequest;
use App\Http\Requests\Api\Trip\CancelInvitationRequest;
use App\Http\Requests\Api\Trip\ChangeInvitationRequest;
use App\Http\Requests\Api\Trip\PostShareUnshareRequest;
use App\Http\Requests\Api\Trip\ActivateTripPlaceRequest;
use App\Http\Requests\Api\Trip\ApproveSuggestionRequest;
use App\Http\Requests\Api\Trip\CommentLikeUnlikeRequest;
use App\Http\Requests\Api\Trip\CommentUpDownVotesRequest;
use App\Http\Requests\Api\Trip\DeActivateTripCityRequest;
use App\Http\Requests\Api\Trip\DeActivateTripPlaceRequest;
use App\Http\Requests\Api\Trip\removePlaceFromTripRequest;
use App\Http\Requests\Api\Trip\DeleteTripPlaceMediaRequest;
use App\Models\ActivityMedia\MediasComments;
use App\Models\ActivityMedia\MediasCommentsLikes;
use App\Models\TripPlaces\TripPlacesComments;
use App\Services\Api\PrimaryPostService;
use App\Services\Visits\VisitsService;
use App\Http\Constants\CommonConst;
use App\Models\Reports\ReportsDraftInfos;
use App\Services\Ranking\RankingService;
use App\Services\Translations\TranslationService;

class TripController extends Controller
{

    function __construct()
    {
        if (in_array(request()->route()->uri, ["api/v1/trip/{id}"])) {
            if (request()->bearerToken()) {
                $this->middleware('auth:api');
            }
        }
    }

    // Creation API || Creation API || Creation API || Creation API || Creation API || Creation API || Creation API || Creation API || Creation API

    /**
     * @OA\Post(
     ** path="/trip",
     *   tags={"Trip"},
     *   summary="This Api used to create trip. (CREATE_TRIP_URL)",
     *   operationId="Api\TripController@createStepOne",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              required={"title","privacy","plan_type"},
     *              @OA\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="description",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="privacy",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="plan_type",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="cover",
     *                  type="string",
     *                  format="binary"
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Trip creation : Trip Create
     * @param CreateStepOne $request
     * @return ApiResponse
     */
    public function createStepOne(CreateStepOne  $request, PlaceChatsService $placeChatsService, TranslationService $translationService, RankingService $rankingService)
    {
        try {
            $memoryArr = [
                "upcoming" => 0,
                "memory"   => 1,
            ];
            $privacyArr = [
                "public"    => 1,
                "friends"   => 2,
                "private"   => 3,
            ];

            $title          = $request->title;
            $description    = $request->description;
            $privacy        = $privacyArr[$request->privacy];
            $memory         = $memoryArr[$request->plan_type];
            $user_id        = Auth::user()->id;
            $date           = date('Y-m-d h:i:s', time());
            $cover          = $request->file('cover');

            $new_plan                   = new TripPlans;
            $new_plan->users_id         = $user_id;
            $new_plan->title            = $title;
            $new_plan->memory           = $memory;
            $new_plan->description      = addslashes($description);
            $new_plan->privacy          = $privacy;
            $new_plan->created_at       = date('Y-m-d h:i:s a', time());
            $new_plan->language_id      = $translationService->getLanguageId($title . ' ' . $description);
            $new_plan->active           = 0;

            if ($new_plan->save()) {

                $rankingService->addPointsEarners($new_plan->id, get_class($new_plan), $user_id);

                $coverUrl = null;
                if ($cover) {
                    $filename = time() . '_' . bin2hex(random_bytes(10)) . '.' . $cover->getClientOriginalExtension();
                    Storage::disk('s3')->putFileAs('trips/cover/' . $new_plan->id . '/',  $cover, $filename, 'public');
                    $coverUrl = S3_BASE_URL . 'trips/cover/' . $new_plan->id . '/' . $filename;
                }

                $version_0 = new TripsVersions;
                $version_0->plans_id = $new_plan->id;
                $version_0->authors_id = $user_id;
                $version_0->start_date = $date;
                $version_0->save();
                $new_plan->active_version = $version_0->id;
                $new_plan->cover = $coverUrl;
                $new_plan->save();

                $placeChatsService->createConversation($new_plan->id, PlaceChatsService::CHAT_TYPE_MAIN, auth()->id());
                log_user_activity('Trip', 'create', $new_plan->id);

                return ApiResponse::create([
                    'message'   => ['trip plans create successfully.'],
                    'id'        => $new_plan->id,
                    'trip'      => $new_plan
                ]);
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['trip plans can not create please try again after some time.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * Trip creation : Trip Edit
     * @param ChangePrivacyRequest $request
     * @return ApiResponse
     */
    public function changeStepOne(ModifyStepOneRequest $request)
    {
        try {
            $trip_id        = $request->trip_id;
            $user_id        = Auth::user()->id;

            $old_plan   = TripPlans::where('id', $trip_id)
                ->where('users_id', $user_id)->first();
            if ($old_plan) {
                $old_plan->privacy          = $request->privacy;
                $old_plan->title            = $request->title;
                $old_plan->description      = $request->description;
                $old_plan->save();
                return ApiResponse::create([
                    'message'   => ['trip plans change successfully.'],
                    'id'        => $old_plan->id
                ]);
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['trip plans not found.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Delete(
     ** path="/trip/{trip_id}",
     *   tags={"Trip"},
     *   summary="delete trip",
     *   operationId="Api\TripController@postAjaxDeleteTrip",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="trip_id",
     *        description="trip_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function postAjaxDeleteTrip(Request $request, PlaceChatsService $placeChatsService, $trip_id)
    {
        try {
            $trip   = TripPlans::where('id', $trip_id)->first();
            if (!$trip) {
                return ApiResponse::__createBadResponse('trip plans not found.');
            }
            if ($trip && $trip->users_id !== auth()->id()) {
                return ApiResponse::__createUnAuthorizedResponse('unauthorized user can not delete trip.');
            }
            $ret = array('status' => null);
            $travel_mates = TravelMatesRequests::where('plans_id', $trip_id)->first();
            if ($travel_mates) {
                TravelMatesRequestUsers::where('requests_id', $travel_mates->id)->delete();
                $travel_mates->delete();
            }

            ReportsInfos::where('var', 'plan')->where('val', $trip_id)->delete();
            ReportsDraftInfos::where('var', 'plan')->where('val', $trip_id)->delete();

            TripCities::where('trips_id', $trip_id)->delete();
            TripCountries::where('trips_id', $trip_id)->delete();
            TripPlaces::where('trips_id', $trip_id)->delete();
            TripsVersions::where('plans_id', $trip_id)->delete();
            $placeChatsService->removeAllPlanChats($trip_id);
            TripPlans::destroy($trip_id);

            try {
                broadcast(new PlanDeletedEvent($trip_id));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new PlanDeletedApiEvent($trip_id));
            } catch (\Throwable $e) {
            }

            log_user_activity('Trip', 'delete', $trip_id);

            Notifications::whereIn('data_id', TripsContributionRequests::where('plans_id', $trip_id)->pluck('id'))
                ->where('type', 'plan_invitation_received')
                ->delete();

            return ApiResponse::create([
                'message'   => ['trip plans delete successfully.'],
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    // Search API || Search API || Search API || Search API || Search API || Search API || Search API || Search API || Search API

    /**
     * @OA\Get(
     ** path="/trip/search/cities",
     *   tags={"Search"},
     *   summary="Get City",
     *   operationId="Api\TripController@searchCitiesForSelect",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="q",
     *        description="Search",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Search : Cities
     * @param SearchCitiesRequest $request
     * @return ApiResponse
     */
    public function searchCitiesForSelect(SearchCitiesRequest $request)
    {
        try {
            ini_set('memory_limit', '512M');
            $query = $request->q;
            $suggestions = Cities::with('trans')
                ->whereHas('trans', function ($q) use ($query) {
                    $q->where('title', 'LIKE', '%' . $query . '%');
                })
                ->take(200)
                ->orderBy('id', 'desc')
                ->get();

            $res = array();
            foreach ($suggestions as $suggestion) {
                $img = @check_city_photo($suggestion->getMedias[0]->url, 180);
                if (isset($suggestion->transsingle)) {
                    $res[] = [
                        'image' => $img,
                        'id' => $suggestion->id,
                        'text' => $suggestion->transsingle->title,
                        'country' => (isset($suggestion->country->transsingle->title)) ? 'city in ' . $suggestion->country->transsingle->title : '',
                        'lat' => $suggestion->lat,
                        'lng' => $suggestion->lng,
                        // 'top_places' => [],
                        // 'top_places' => app(HomeController::class)->__getSuggestTopPlaces($suggestion->id)
                    ];
                }
            }

            $country_suggestions = Countries::with('trans')
                ->whereHas('trans', function ($q) use ($query) {
                    $q->where('title', 'LIKE', '%' . $query . '%');
                })->first();

            $language_id = 1;
            if (isset($country_suggestions) && is_object($country_suggestions)) {
                $additional_suggestion =  Cities::with([
                    'trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'top'
                ])
                    ->where('countries_id', $country_suggestions->id)
                    ->take(100)
                    ->get();
                foreach ($additional_suggestion as $suggestion) {
                    $img = @check_city_photo($suggestion->getMedias[0]->url, 180);
                    if (isset($suggestion->transsingle)) {
                        $res[] = [
                            'image' => $img,
                            'id' => $suggestion->id,
                            'text' => $suggestion->transsingle->title,
                            'country' => (isset($suggestion->country->transsingle->title)) ? 'city in ' . $suggestion->country->transsingle->title : '',
                            'lat' => $suggestion->lat,
                            'lng' => $suggestion->lng,
                            // 'top_places' => [],
                            // 'top_places' => app(HomeController::class)->__getSuggestTopPlaces($suggestion->id)
                            // 'reviews'=>isset($suggestion->top->no_of_reviews)?$suggestion->top->no_of_reviews:0
                        ];
                    }
                }
            }
            krsort($res);
            return ApiResponse::create(array_values($res));
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SearchCitiesRequest $request
     * @return ApiResponse
     */
    public function ajaxGetTripPlaceByProviderId(
        Request $request,
        TripsService $tripsService
    ) {
        try {
            $providerId = $request->provider_id ?? null;
            $placeId    = $request->place_id;

            if (!$providerId && !$placeId) {
                return ApiResponse::create([
                    'message' => ['provider id must be required'],
                ], false, ApiResponse::BAD_REQUEST);
            }

            $where = ['id' => $placeId];

            if ($providerId) {
                $where = ['provider_id' => $providerId];
            }

            $place = Place::query()->where($where)->with(['city', 'trans', 'city.trans', 'country', 'country.trans'])->first();

            if (!$place) {
                $placeId = $tripsService->addNewPlaceByProviderId($providerId);
                $place = Place::query()->with(['city', 'trans', 'city.trans', 'country', 'country.trans'])->find($placeId);
            }

            $place->src = check_place_photo($place);

            $place->reviews = Reviews::where('places_id', $place->id)->get();
            $place->reviews_avg = Reviews::where('places_id', $place->id)->avg('score');

            return ApiResponse::create($place);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    // Trip Place API || Trip Place API || Trip Place API || Trip Place API || Trip Place API || Trip Place API || Trip Place API 

    /**
     * @OA\Post(
     ** path="/trip/{trip_id}/place",
     *   tags={"Trip Place"},
     *   summary="Add place to trip in timeline(CREATE_TRIP_PLACE_URL)",
     *   operationId="Api\TripController@postAddPlaceToTrip",
     *   security={
     *  {"bearer_token": {}
     *    }},
     * @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="city_id",
     *        description="city_id : get from city search api : /api/v1/trip/search/cities",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="place_id",
     *        description="place_id : get from place search api : /api/v1/home/search/poi",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="budget",
     *        description="",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="duration_days",
     *        description="",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="durations_hours",
     *        description="",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="duratiion_minutes",
     *        description="",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="date",
     *        description="YEAR-MONTH-DAY | 2020-04-12",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="time",
     *        description="1:00am || 1:00pm",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="known_for",
     *        description="Food, Football, ...",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="story",
     *        description="",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="medias[]",
     *        description="medias_id",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param AddPlaceToTripRequest $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param TripsService $tripsService
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function postAddPlaceToTrip(AddPlaceToTripRequest $request,  TripsSuggestionsService $tripsSuggestionsService, TripsService $tripsService, PlanActivityLogService $planActivityLogService, TripInvitationsService $tripInvitationsService, $trip_id)
    {
        try {
            $trip = TripPlans::find($trip_id);
            if (!$trip) {
                return ApiResponse::create(
                    [
                        'message' => ['trip plan not found.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $authUserId = auth()->id();
            $tripUserId = $trip->users_id;
            $invitedUserRequest = null;

            if (!in_array($tripInvitationsService->getUserRole($trip_id, auth()->id()), [
                TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN],
                TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
            ])) {
                return ApiResponse::__createUnAuthorizedResponse("only trip creator and trip member(admin or editor) can edit trip.");
            }

            if ($tripUserId !== $authUserId) {
                $invitedUserRequest = $tripsSuggestionsService->getInvitedUserRequest($trip_id, $authUserId);
            }

            $tripVersion = TripsVersions::where([
                'authors_id' => $authUserId,
                'plans_id' => $trip_id
            ])->latest()->first();

            if (!$tripVersion) {
                // todo error
            }

            $city_id = $request->get('city_id');
            $place_id = $request->get('place_id');
            $budget = $request->get('budget');
            $known_for = $request->get('known_for');
            $duration_days = $request->get('duration_days');
            $duration_hours = $request->get('duration_hours');
            $duration_minutes = $request->get('duration_minutes');
            $date = $request->get('date');
            $time = $request->get('time');

            // if (!Place::find($place_id)) {
            //     return ApiResponse::create([
            //         'message' => ['place is invalid.']
            //     ], false, ApiResponse::BAD_REQUEST);
            // }

            $city_object = Cities::find($city_id);
            if (!$city_object) {
                return ApiResponse::create([
                    'message' => ['city is invalid.']
                ], false, ApiResponse::BAD_REQUEST);
            }

            $version_id = 0;
            $order = $request->get('order');
            $medias = array_slice(array_unique($request->get('medias', [])), 0, 10);

            if ($order === null) {
                $order = TripPlaces::where('trips_id', $trip_id)->where('date', $date)->max('order');
            }

            $duration = ($duration_hours * 60) + $duration_minutes;
            $duration = ($duration_days * 60 * 24) + ($duration_hours * 60) + $duration_minutes;

            $date_time = strtotime($date . " " . $time);
            $date_time = date("Y-n-d H:i:s", $date_time);

            if ($trip->memory && Carbon::parse($date_time)->greaterThan(now())) {
                return ApiResponse::create([
                    'message' => ['invalid date time for memory trip. please select past date and time.']
                ], false, ApiResponse::BAD_REQUEST);
            }

            $withoutTime = 0;

            if (!$time) {
                $withoutTime = 1;
            }


            $time = $date_time;
            $is_new_place = 0;
            if (
                !Place::where('provider_id', '=', $place_id)->exists()
                && !Place::where('id', '=', $place_id)->exists()
            ) {
                $place_id = $tripsService->addNewPlaceByProviderId($place_id);
                $is_new_place = $place_id;
            } elseif (Place::where('provider_id', '=', $place_id)->exists()) {
                $place_id = Place::where('provider_id', '=', $place_id)->first()->id;
            }

            $add_place = new TripPlaces;
            $add_place->trips_id = $trip_id;
            $add_place->versions_id = 0;
            $add_place->cities_id = $city_id;

            $add_place->countries_id = $city_object->countries_id;
            $add_place->places_id = $place_id;
            $add_place->time = $time;
            $add_place->date = $time;
            $add_place->order = $order === null ? 1 : $order + 1;
            $add_place->budget = $budget;
            $add_place->duration = $duration;
            $add_place->without_time = $withoutTime;
            $add_place->comment = $known_for;
            $add_place->active = 0;
            $add_place->story = $request->get('story', '');

            $isPlaceExists = $tripsSuggestionsService->isPlaceOrTimeExistsInPlan($add_place);

            if ($isPlaceExists) {
                return ApiResponse::create([
                    'message' => ['The place already exists or there\'s already a place with that time']
                ], false, ApiResponse::BAD_REQUEST);
            }

            if ($add_place->save() && $tripsService->recalculateOrders($trip_id, $add_place, $request->get('time'))) {
                $suggestionId = $tripsSuggestionsService->createSuggestion($add_place->id, $trip_id, TripsSuggestionsService::SUGGESTION_CREATE_TYPE);

                if (!$invitedUserRequest || $invitedUserRequest->role === 'admin') {
                    $tripsSuggestionsService->approveSuggestion($suggestionId);
                    $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_ADDED_PLACE, $trip_id, $add_place->getKey(), auth()->id(), [], $suggestionId);
                }

                $country = TripCountries::where('trips_id', $trip_id)->where('countries_id', $city_object->countries_id)->first();
                if (!$country) {
                    $add_country = new TripCountries;
                    $add_country->trips_id = $trip_id;
                    $add_country->countries_id = $city_object->countries_id;
                    $add_country->versions_id = $version_id;
                    $add_country->date = date("Y-m-d H:i:s");
                    $add_country->order = 0;

                    $add_country->save();
                }

                if ($budget) {
                    $trip->budget = $trip->budget + $budget;

                    $trip->save();
                }

                $tripMedies = $trip->media;

                foreach ($medias as $mediaId) {
                    if (!(is_array($mediaId) || is_object($mediaId))) {
                        $tm = new TripMedias();
                        $tm->medias_id = $mediaId;
                        $tm->trips_id = $trip_id;
                        $tm->places_id = $place_id;
                        $tm->trip_place_id = $add_place->id;
                        $tm->save();

                        $um = new UsersMedias();
                        $um->users_id = auth()->id();
                        $um->medias_id = $mediaId;
                        $um->save();
                    }
                }

                // $new_trip_place = $this->getSpecificPlace($trip_id, $add_place->id);

                // timeline
                // $timeline = $this->getPlanContentsApi(true, $trip_id, auth()->id(), $tripsSuggestionsService);
                // $timeline['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $trip_id);
                // unset($timeline['is_edited'], $timeline['updated_places'], $timeline['unread_logs_count']);

                return ApiResponse::create([
                    'message' => ['Trip place added successfully'],
                    'trip_place_id' => $add_place->id,
                    'data' => $tripsSuggestionsService->getApiTripPlace(auth()->id(), $trip_id, $add_place->id)
                    // 'new_trip_place'   => $new_trip_place,
                    // 'is_new'        => $is_new_place,
                    // 'timeline'      => $timeline
                ]);
            } else {
                return ApiResponse::create([
                    'message' => ['internal server error']
                ], false, ApiResponse::SERVER_ERROR);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Delete(
     ** path="/trip/{trip_id}/places/{trip_place_id}/",
     *   tags={"Trip Place"},
     *   summary="delete any trip place from timeline",
     *   operationId="Api\TripController@postRemovePlaceFromTrip",
     *   security={
     *  {"bearer_token": {}
     *    }},
     * * @OA\Parameter(
     *        name="trip_id",
     *        description="trip_id",
     *        required=false,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="trip_place_id",
     *        description="trip_place_id",
     *        required=false,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param removePlaceFromTripRequest $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function postRemovePlaceFromTrip(
        Request $request,
        TripInvitationsService $tripInvitationsService,
        TripsSuggestionsService $tripsSuggestionsService,
        PlanActivityLogService $planActivityLogService,
        $trip_id,
        $trip_place_id
    ) {
        try {
            $tripPlaceId    = $trip_place_id;
            $tripPlace      = TripPlaces::find($tripPlaceId);

            if (!$tripPlace) {
                return ApiResponse::__createBadResponse('Trip place not found');
            }

            $trip               = $tripPlace->trip;
            $authUserId         = auth()->id();
            $invitedUserRequest = null;
            $invitedUserRequest = $tripsSuggestionsService->getInvitedUserRequest($trip->id, $authUserId);
            $suggestionId       = $tripsSuggestionsService->createSuggestion($tripPlaceId, $trip->id, TripsSuggestionsService::SUGGESTION_DELETE_TYPE, $tripPlaceId);

            if (!$invitedUserRequest || $invitedUserRequest->role === 'admin') {
                $tripsSuggestionsService->approveSuggestion($suggestionId);
                $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_REMOVED_PLACE, $trip->id, $tripPlaceId, auth()->id());
                $tripsSuggestionsService->dismissSuggestedPlace($tripPlaceId);
            }

            // timeline
            // $timeline = $this->getPlanContentsApi(true, $trip_id, auth()->id(), $tripsSuggestionsService);
            // $timeline['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $trip_id);
            // unset($timeline['is_edited'], $timeline['updated_places'], $timeline['unread_logs_count']);

            return ApiResponse::create([
                "message" => ["Remove successfully."],
                // "timeline" => $timeline
            ]);
        } catch (\Throwable $e) {
            if (get_class($e) == ModelNotFoundException::class) {
                return ApiResponse::create([
                    'message' => ['trip not found']
                ], false, ApiResponse::BAD_REQUEST);
            } else if (get_class($e) == BadRequestHttpException::class) {
                return ApiResponse::create([
                    'message' => ['Sonmething went wrong. please try again.']
                ], false, ApiResponse::SERVER_ERROR);
            } else if (get_class($e) == MaxRejectedTimesException::class) {
                return ApiResponse::create([
                    'message' => ['Your suggestions were rejected 3 times']
                ], false, ApiResponse::SERVER_ERROR);
            } else if (get_class($e) == AuthorizationException::class) {
                return ApiResponse::create([
                    'message' => ['Unauthorized user.']
                ], false, ApiResponse::UNAUTHORIZED);
            } else {
                return ApiResponse::createServerError($e);
            }
        }
    }

    /**
     * @OA\Put(
     ** path="/trip/{trip_id}/places/{trip_place_id}/",
     *   tags={"Trip Place"},
     *   summary="edit any trip place from timeline",
     *   operationId="Api\TripController@postEditPlaceInTrip",
     *   security={
     *       {
     *          "bearer_token": {}
     *       }
     *   },
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              required={"trip_id","trip_place_id","city_id","place_id"},
     *              @OA\Property(
     *                  property="trip_id",
     *                  type="integer"
     *              ),
     *              @OA\Property(
     *                  property="trip_place_id",
     *                  type="integer"
     *              ),
     *              @OA\Property(
     *                  property="city_id",
     *                  type="integer",
     *                  description="city_id : get from city search api : /api/v1/trip/search/cities",
     *              ),
     *              @OA\Property(
     *                  property="place_id",
     *                  type="string",
     *                  description="place_id : get from place search api : /api/v1/home/search/poi"
     *              ),
     *              @OA\Property(
     *                  property="budget",
     *                  type="integer"
     *              ),
     *              @OA\Property(
     *                  property="duration_days",
     *                  type="integer"
     *              ),
     *              @OA\Property(
     *                  property="durations_hours",
     *                  type="integer"
     *              ),
     *              @OA\Property(
     *                  property="duratiion_minutes",
     *                  type="integer"
     *              ),
     *              @OA\Property(
     *                  property="date",
     *                  type="string",
     *                  description="YEAR-MONTH-DAY | 2020-04-12"
     *              ),
     *              @OA\Property(
     *                  property="time",
     *                  type="string",
     *                  description="1:00am || 1:00pm"
     *              ),
     *              @OA\Property(
     *                  property="known_for",
     *                  type="string",
     *                  description="Food, Football, ..."
     *              ),
     *              @OA\Property(
     *                  property="story",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                   property="medias",
     *                   type="array",
     *                   @OA\Items(
     *                      type="integer",
     *                      description="medias_id"
     *                   ),
     *               ),
     *      ))
     *  ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param EditPlaceInTripRequest $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param TripsService $tripsService
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function postEditPlaceInTrip(
        EditPlaceInTripRequest $request,
        TripsSuggestionsService $tripsSuggestionsService,
        TripsService $tripsService,
        PlanActivityLogService $planActivityLogService,
        TripInvitationsService $tripInvitationsService,
        $trip_id,
        $trip_place_id
    ) {
        try {
            $trip = TripPlans::find($trip_id);
            if (!$trip) {
                return ApiResponse::create([
                    'message' => ["trip not found."]
                ], false, ApiResponse::BAD_REQUEST);
            }

            $medias = array_slice(array_unique($request->get('medias', [])), 0, 10);
            $authUserId = auth()->id();
            $tripUserId = $trip->users_id;

            $invitedUserRequest = null;

            if (!in_array($tripInvitationsService->getUserRole($trip_id, auth()->id()), [
                TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN],
                TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
            ])) {
                return ApiResponse::__createUnAuthorizedResponse('only trip creator and trip member(admin or editor) can edit trip.');
            }

            if ($tripUserId !== $authUserId && !is_super_admin($authUserId)) {
                $invitedUserRequest = $tripsSuggestionsService->getInvitedUserRequest($trip_id, $authUserId);
            }

            $tripVersion = TripsVersions::where([
                'authors_id' => $authUserId,
                'plans_id' => $trip_id
            ])->latest()->first();

            if (!$tripVersion) {
                return ApiResponse::create([
                    'message' => ["trip version not found."]
                ], false, ApiResponse::BAD_REQUEST);
            }

            $trip_place_id              = $request->trip_place_id;
            $city_id                    = $request->city_id;
            $place_id                   = $request->place_id;
            $budget                     = $request->budget;
            $duration_days              = $request->duration_days;
            $duration_hours             = $request->duration_hours;
            $duration_minutes           = $request->duration_minutes;
            $date                       = $request->date;
            $time                       = $request->time;
            $known_for                  = $request->known_for;
            $duration                   = ($duration_days * 60 * 24) + ($duration_hours * 60) + $duration_minutes;
            $date_time                  = strtotime($date . " " . $time);
            $date_time                  = date("Y-m-d H:i:s", $date_time);
            $withoutTime = 0;

            if (!$time) {
                $withoutTime = 1;
            }
            $time                       = $date_time;

            if ($trip->memory && Carbon::parse($time)->greaterThan(now()->endOfDay())) {
                return ApiResponse::create([
                    'message' => ['invalid date time for memory trip. please select past date and time.']
                ], false, ApiResponse::BAD_REQUEST);
            }

            if (!$trip->memory && Carbon::parse($time)->lessThan(now()->startOfDay())) {
                return ApiResponse::create([
                    'message' => ['invalid date time for upcoming trip. please select future date and time.']
                ], false, ApiResponse::BAD_REQUEST);
            }

            $trip_place_id = $tripsSuggestionsService->getLastTripPlace($trip_place_id);

            if (!TripPlaces::find($trip_place_id)) {
                return ApiResponse::create([
                    'message' => ['trip place is invalid.']
                ], false, ApiResponse::BAD_REQUEST);
            }

            $city_object = Cities::find($city_id);
            if (!$city_object) {
                return ApiResponse::create([
                    'message' => ['city is invalid.']
                ], false, ApiResponse::BAD_REQUEST);
            }


            if ($tripsSuggestionsService->isDeletedTripPlace($trip_place_id)) {
                return ApiResponse::create([
                    'message' => ['The place was removed']
                ], false, ApiResponse::BAD_REQUEST);
            }

            $trip_place_id              = $tripsSuggestionsService->getLastTripPlace($trip_place_id);
            $add_place                  = TripPlaces::query()->findOrFail($trip_place_id);

            $current_place              = $add_place;
            $add_place                  = $add_place->replicate();
            $add_place->trips_id        = $trip_id;
            $add_place->versions_id     = 0;
            $add_place->cities_id       = $city_id;
            $add_place->without_time    = $withoutTime;

            $add_place->countries_id    = $city_object->countries_id;
            $add_place->places_id       = $place_id;
            $add_place->time            = $time;
            $add_place->date            = $time;
            $add_place->budget          = $budget;
            $add_place->duration        = $duration;
            $add_place->comment         = $known_for;
            $add_place->active          = 0;
            $add_place->story           = $request->story ?? '';


            $isPlaceExists = $tripsSuggestionsService->isPlaceOrTimeExistsInPlan($add_place, $trip_place_id);

            if ($isPlaceExists) {
                return ApiResponse::create([
                    'message' => ["The place already exists or there's already a place with that time"]
                ], false, ApiResponse::BAD_REQUEST);
            }

            if ($add_place->save() && $tripsService->recalculateOrders($trip_id, $add_place, $request->get('time'))) {
                $suggestionId = $tripsSuggestionsService->createSuggestion($add_place->id, $trip_id, TripsSuggestionsService::SUGGESTION_EDIT_TYPE, $trip_place_id);

                $meta = $planActivityLogService->prepareLogMeta($add_place, $current_place);

                if (!$invitedUserRequest || $invitedUserRequest->role === 'admin') {
                    $tripsSuggestionsService->approveSuggestion($suggestionId);
                    $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_EDITED_PLACE, $trip_id, $add_place->getKey(), $authUserId, $meta, $suggestionId);
                }

                foreach ($medias as $mediaId) {
                    if (!(is_array($mediaId) || is_object($mediaId))) {
                        $attributes = [
                            'medias_id' => $mediaId,
                            'trips_id' => $trip_id,
                            'places_id' => $place_id,
                            'trip_place_id' => $add_place->id
                        ];

                        TripMedias::query()->firstOrCreate($attributes);
                        UsersMedias::query()->firstOrCreate(['users_id' => auth()->id(), 'medias_id' => $mediaId]);
                    }
                }

                // $new_trip_place = $this->getSpecificPlace($trip_id, $add_place->id);

                // timeline
                // $timeline = $this->getPlanContentsApi(true, $trip_id, auth()->id(), $tripsSuggestionsService);
                // $timeline['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $trip_id);
                // unset($timeline['is_edited'], $timeline['updated_places'], $timeline['unread_logs_count']);

                return ApiResponse::create([
                    'message' => ["Place details save successfully"],
                    'trip_place_id' => $add_place->id,
                    'data' => $tripsSuggestionsService->getApiTripPlace(auth()->id(), $trip_id, $add_place->id)
                    // 'trip_place_id' => $add_place->id,
                    // 'trip_places'   => $new_trip_place,
                    // "timeline" => $timeline
                ]);
            } else {
                return ApiResponse::create([
                    'message' => ["internal server error"]
                ], false, ApiResponse::SERVER_ERROR);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/trip/{trip_id}/places/{trip_place_id}/",
     *   tags={"Trip Place"},
     *   summary="fetch any trip place from timeline for edit purpose",
     *   operationId="Api\TripController@ajaxGetTripPlace",
     *   security={
     *  {"bearer_token": {}
     *    }},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              required={"trip_id","trip_place_id"},
     *              @OA\Property(
     *                  property="trip_id",
     *                  type="integer"
     *              ),
     *              @OA\Property(
     *                  property="trip_place_id",
     *                  type="integer",
     *                  description="you can get from timnline api",
     *              )
     *      ))
     *  ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function ajaxGetTripPlace(
        Request $request,
        PlanActivityLogService $planActivityLogService,
        TripsSuggestionsService $tripsSuggestionsService,
        $trip_id,
        $trip_place_id
    ) {
        $place_id = $trip_place_id;
        $tripPlace = TripPlaces::findOrFail($place_id);
        $place = @Place::find($tripPlace->places_id);
        $city = @Cities::find($tripPlace->cities_id);
        $tripPlace->dateForPicker = strtotime($tripPlace->date . ' UTC') * 1000;
        $tripPlace->timeForPicker = (new Carbon($tripPlace->time))->format('g:ia');
        $tripPlace->cities_name = $city ? $city->transsingle->title : null;
        $tripPlace->countries_name = Countries::find($tripPlace->countries_id)->transsingle->title;
        $tripPlace->places_name = Place::find($tripPlace->places_id)->transsingle->title;
        $tripPlace->tags = $tripPlace->comment;

        if ($place) {
            if (!isset($place->getMedias[0])) {
                $tripPlace->places_img = asset('assets2/image/placeholders/pattern.png');
            } elseif ($place->getMedias[0]->thumbs_done == 1) {
                $tripPlace->places_img = S3_BASE_URL . 'th1100/' . $place->getMedias[0]->url;
            } else {
                $tripPlace->places_img = S3_BASE_URL . $place->getMedias[0]->url;
            }
        }
        $tripPlace->trip_media = @TripMedias::where('places_id', $place_id)->first()->media->url;

        $tripPlace->original_cities_name = $tripPlace->cities_name;
        $tripPlace->cities_name = $tripPlace->cities_name . ', city in ' . $tripPlace->countries_name;
        $tripPlace->cities_lat = $city ? $city->lat : null;
        $tripPlace->cities_lng = $city ? $city->lng : null;

        $tripPlace->cities_img =  @check_city_photo(Cities::find($tripPlace->cities_id)->getMedias[0]->url, 180);

        $tripPlace->images = TripMedias::where('trips_id', $tripPlace->trips_id)
            ->whereIn('trip_place_id', $tripsSuggestionsService->getAllSuggestionPackTripPlacesIds($place_id))
            ->with('media')
            ->get()
            ->unique('medias_id')
            ->values()
            ->toArray();
        $tripPlace->updated_attributes = $planActivityLogService->getPlaceLogUpdatedAttributes($place_id);

        return ApiResponse::create($tripPlace);
    }

    //TB-474
    /**
     * @OA\GET(
     ** path="/trip/{trip_id}/places/{trip_place_id}/story",
     *   tags={"Trip Place"},
     *   summary="This Api used to fetch trip place with story.",
     *   operationId="This Api used to fetch trip place with story.",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="trip_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="trip_place_id",
     *        description="trip_place_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function ajaxGetTripPlaceStory(
        Request $request,
        PlanActivityLogService $planActivityLogService,
        TripsSuggestionsService $tripsSuggestionsService,
        PrimaryPostService $primaryPostService,
        $trip_id,
        $trip_place_id
    ) {
        try {
            $authUser = auth()->user();

            // Checking Existing Post
            $post = $primaryPostService->find(PrimaryPostService::TYPE_TRIP, $trip_id);
            if (!$post) return ApiResponse::__createBadResponse("Trip not found");

            $tripPlace = TripPlaces::where('id', $trip_place_id)->where('trips_id', $trip_id)->first();
            if (!$tripPlace) throw new ModelNotFoundException('Trip Place not found');

            $tripPlace->tags = $tripPlace->comment;

            $place = Place::find($tripPlace->places_id);
            $city = Cities::find($tripPlace->cities_id);
            $tripPlace->dateForPicker = strtotime($tripPlace->date . ' UTC') * 1000;
            $tripPlace->timeForPicker = (new Carbon($tripPlace->time))->format('g:ia');
            $tripPlace->cities_name = $city ? $city->transsingle->title : null;
            $tripPlace->countries_name = Countries::find($tripPlace->countries_id)->transsingle->title;
            $tripPlace->places_name = Place::find($tripPlace->places_id)->transsingle->title;

            if ($place) {
                if (!isset($place->getMedias[0])) {
                    $tripPlace->places_img = asset('assets2/image/placeholders/pattern.png');
                } elseif ($place->getMedias[0]->thumbs_done == 1) {
                    $tripPlace->places_img = S3_BASE_URL . 'th1100/' . $place->getMedias[0]->url;
                } else {
                    $tripPlace->places_img = S3_BASE_URL . $place->getMedias[0]->url;
                }
            }
            $tripPlace->trip_media = @TripMedias::where('places_id', $trip_place_id)->first()->media->url;

            $tripPlace->original_cities_name = $tripPlace->cities_name;
            $tripPlace->cities_name = $tripPlace->cities_name . ', city in ' . $tripPlace->countries_name;
            $tripPlace->cities_lat = $city ? $city->lat : null;
            $tripPlace->cities_lng = $city ? $city->lng : null;
            $tripPlace->cities_img =  @check_city_photo(Cities::find($tripPlace->cities_id)->getMedias[0]->url, 180);

            $post_comment = TripPlacesComments::select('*')
                ->where('trip_place_id', $tripPlace->id)
                ->where('reply_to', 0)
                ->orderBy('created_at', 'desc')
                ->take(10)->get();

            if ($post_comment) {
                foreach ($post_comment as $parent_comment) {
                    // For Parent Comment
                    $parent_comment->text = isset($parent_comment->tags) ? convert_post_text_to_tags($parent_comment->comment, $parent_comment->tags, false) : $parent_comment->comment;
                    unset($parent_comment->tags, $parent_comment->comment);
                    $parent_comment->like_status = ($parent_comment->likes->where('users_id', $authUser->id)->first()) ? true : false;
                    $parent_comment->total_likes = $parent_comment->likes->count();
                    if ($parent_comment->medias) {
                        foreach ($parent_comment->medias as $media) {
                            $media->media;
                        }
                    }
                    $parent_comment->author = $parent_comment->user;
                    unset($parent_comment->user);
                    if ($parent_comment->author) {
                        $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                    }
                    unset($parent_comment->likes);

                    // For Sub Comment
                    if ($parent_comment->sub) {
                        foreach ($parent_comment->sub as $sub_comment) {
                            $sub_comment->text = isset($sub_comment->tags) ? convert_post_text_to_tags($sub_comment->comment, $sub_comment->tags, false) : $sub_comment->comment;
                            unset($sub_comment->tags, $sub_comment->comment);

                            $sub_comment->like_status = ($parent_comment->likes->where('users_id', $authUser->id)->first()) ? true : false;
                            $sub_comment->total_likes = $sub_comment->likes->count();
                            if ($sub_comment->medias) {
                                foreach ($sub_comment->medias as $media) {
                                    $media->media;
                                }
                            }
                            $sub_comment->author = $sub_comment->user;
                            unset($sub_comment->user);
                            if ($sub_comment->author) {
                                $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                            }
                            unset($sub_comment->likes);
                        }
                    }
                }
            }

            if ($tripPlaceSuggestion = TripsSuggestion::where('suggested_trips_places_id', $tripPlace->id)->orderBy('id', 'ASC')->first()) {
                $TripPlaceAuthorId = $tripPlaceSuggestion->users_id;
            } else {
                $TripPlaceAuthorId = $tripPlace->active_trip->users_id;
            }
            $tripPlace->user = User::where('id', $TripPlaceAuthorId)
                ->select('id', 'name', 'username', 'email', 'profile_picture', 'cover_photo', 'created_at', 'contact_email')
                ->first();

            if (isset($tripPlace->user->profile_picture)) {
                $tripPlace->user->profile_picture = check_profile_picture($tripPlace->user->profile_picture);
                $tripPlace->user->cover_photo = check_cover_photo($tripPlace->user->cover_photo);
            }

            $tripPlace->latest_comments = $post_comment;
            unset($tripPlace->active_trip);

            return ApiResponse::create($tripPlace);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/trip/{trip_id}/places/{trip_place_id}/media/{trip_place_media_id}",
     *   tags={"Trip Place"},
     *   summary="This Api used to fetch trip place media details",
     *   operationId="Api\TripController@findTripPlaceMedia",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="trip_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="trip_place_id",
     *        description="trip_place_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="trip_place_media_id",
     *        description="trip_place_media_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function findTripPlaceMedia(Request $request, PrimaryPostService $primaryPostService, $trip_id, $trip_place_id, $trip_place_media_id)
    {
        try {
            $user = auth()->user();
            // Checking Existing Post
            $post = $primaryPostService->find(PrimaryPostService::TYPE_TRIP, $trip_id);
            if (!$post) return ApiResponse::__createBadResponse("Trip not found");

            $tripPlaces = TripPlaces::where('id', $trip_place_id)->where('trips_id', $trip_id)->first();
            if (!$tripPlaces) throw new ModelNotFoundException('Trip Place not found');

            $tripMedia = TripMedias::where('id', $trip_place_media_id)
                ->where('trips_id', $trip_id)
                ->where('trip_place_id', $trip_place_id)->first();
            if (!$tripMedia) throw new ModelNotFoundException('Trip Place media not found');
            $tripMedia->media;
            $tripMedia->lat = $tripMedia->tripPlace->place->lat;
            $tripMedia->lng = $tripMedia->tripPlace->place->lng;
            $tripMedia->media->total_likes = $tripMedia->media->likes->count();
            $tripMedia->media->like_flag = ($tripMedia->media->likes->where('users_id', $user->id)->first()) ? true : false;
            unset($tripMedia->media->likes);

            $tripMedia->tripPlace->place;
            if ($tripMedia->media->users_id) {
                $tripMedia->media->users = User::find($tripMedia->media->users_id);
                $tripMedia->media->users->profile_picture = check_profile_picture($tripMedia->media->users->profile_picture);
            } else {
                $tripMedia->media->users = null;
            }
            $tripMedia->media->path = check_cover_photo($tripMedia->media->path);
            $tripMedia->media->url = check_cover_photo($tripMedia->media->url);


            $post_comment = MediasComments::select('*')
                ->addSelect(DB::raw('fun_basic_algo(medias_id, "trip_place_media") as total'))
                ->where('medias_id', $tripMedia->media->id)
                ->where('reply_to', 0)
                ->orderBy('created_at', 'desc')->take(10)->get();

            if ($post_comment) {
                foreach ($post_comment as $parent_comment) {

                    // For Parent Comment
                    $parent_comment->text = isset($parent_comment->tags) ? convert_post_text_to_tags($parent_comment->comment, $parent_comment->tags, false) : $parent_comment->comment;
                    unset($parent_comment->tags, $parent_comment->comment);

                    $parent_comment->like_status = (MediasCommentsLikes::where('medias_comments_id', $parent_comment->id)->where('users_id', $user->id)->first()) ? true : false;
                    $parent_comment->total_likes = MediasCommentsLikes::where('medias_comments_id', $parent_comment->id)->count();
                    if ($parent_comment->medias) {
                        foreach ($parent_comment->medias as $media) {
                            $media->media;
                        }
                    }
                    $parent_comment->author = $parent_comment->user;
                    unset($parent_comment->user);
                    if ($parent_comment->author) {
                        $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                    }
                    unset($parent_comment->likes);

                    // For Sub Comment
                    if ($parent_comment->sub) {
                        foreach ($parent_comment->sub as $sub_comment) {
                            $sub_comment->text = isset($sub_comment->tags) ? convert_post_text_to_tags($sub_comment->comment, $sub_comment->tags, false) : $sub_comment->comment;
                            unset($sub_comment->tags, $sub_comment->comment);

                            $sub_comment->like_status = (MediasCommentsLikes::where('medias_comments_id', $sub_comment->id)->where('users_id', $user->id)->first()) ? true : false;
                            $sub_comment->total_likes = MediasCommentsLikes::where('medias_comments_id', $sub_comment->id)->count();
                            if ($sub_comment->medias) {
                                foreach ($sub_comment->medias as $media) {
                                    $media->media;
                                }
                            }
                            $sub_comment->author = $sub_comment->user;
                            unset($sub_comment->user);
                            if ($sub_comment->author) {
                                $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                            }
                            unset($sub_comment->likes);
                        }
                    }
                }
            }

            $tripMedia->comments = $post_comment;
            unset($tripMedia->tripPlace);

            return ApiResponse::create($tripMedia);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/trip/{trip_id}/places/{place_id}/publish",
     *   tags={"Trip Place"},
     *   summary="publish specific trip place",
     *   operationId="Api\TripController@postAjaxPublishTripPlace",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="place_id",
     *        description="trip place id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function postAjaxPublishTripPlace(
        Request $request,
        TripInvitationsService $tripInvitationsService,
        TripsSuggestionsService $tripsSuggestionsService,
        PlanActivityLogService $planActivityLogService,
        $trip_id,
        $place_id
    ) {
        $plan = TripPlans::query()->findOrFail($trip_id);
        if (!$plan) {
            return ApiResponse::__createBadResponse("trip is invalid.");
        }

        if (!TripPlaces::find($place_id)) {
            return ApiResponse::__createBadResponse('trip place is invalid.');
        }

        if ($tripInvitationsService->getUserRole($trip_id, auth()->id()) !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN]) {
            return ApiResponse::__createBadResponse("Only Admin can publish trip place.");
        };

        if ($tripsSuggestionsService->publishPlan($plan, $place_id)) {
            $log = $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_PUBLISHED_PLAN, $trip_id, null, auth()->id());
            try {
                broadcast(new PlanPublicEvent($planActivityLogService->prepareLogForResponse($log), false));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new PlanPublicApiEvent($planActivityLogService->prepareLogForResponse($log), false));
            } catch (\Throwable $e) {
            }
            // timeline
            $timeline = $this->getPlanContentsApi(true, $trip_id, auth()->id(), $tripsSuggestionsService);
            $timeline['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $trip_id);
            unset($timeline['is_edited'], $timeline['updated_places'], $timeline['unread_logs_count']);

            return ApiResponse::create([
                "message" => ["Successfully publish"],
                "timeline" => $timeline
            ]);
        } else {
            return ApiResponse::__createServerError('unpublish places not found');
        }
    }

    /**
     * @OA\POST(
     ** path="/trip/{trip_id}/place/{place_id}/dismiss",
     *   tags={"Trip Suggestion"},
     *   summary="",
     *   operationId="Api\TripController@postAjaxDismissPlaceFromTrip",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="place_id",
     *        description="trip place id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function postAjaxDismissPlaceFromTrip(Request $request, TripInvitationsService $tripInvitationsService, TripsSuggestionsService $tripsSuggestionsService, $trip_id, $place_id)
    {
        $plan = TripPlans::find($trip_id);
        if (!$plan) {
            return ApiResponse::__createBadResponse("trip is not found.");
        }

        $tripPlace = TripPlaces::find($place_id);
        if (!$tripPlace) {
            return ApiResponse::__createBadResponse('Trip place not found');
        }

        if ($tripsSuggestionsService->dismissSuggestedPlace($place_id)) {
            // timeline
            $timeline = $this->getPlanContentsApi(true, $trip_id, auth()->id(), $tripsSuggestionsService);
            $timeline['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $trip_id);
            unset($timeline['is_edited'], $timeline['updated_places'], $timeline['unread_logs_count']);

            return ApiResponse::create([
                "message" => ["Place dismiss successfully."],
                "timeline" => $timeline
            ]);
        }

        return ApiResponse::__createBadResponse('Place can not dismiss, Please try again.');
    }



    // Fetch Api || Fetch Api || Fetch Api || Fetch Api || Fetch Api || Fetch Api || Fetch Api || Fetch Api || Fetch Api

    /**
     * @OA\GET(
     ** path="/trip/{trip_id}/activity-logs",
     *   tags={"Trip"},
     *   summary="fetch all Activity Log",
     *   operationId="Api\TripController@getPlanActivityLog",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param PlanRequest $request
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function getPlanActivityLog(Request $request, PlanActivityLogService $planActivityLogService, $trip_id)
    {
        try {
            $trip = TripPlans::find($trip_id);
            if (!$trip) {
                return ApiResponse::create([
                    'message' => ['Invalid trip.']
                ], false, ApiResponse::BAD_REQUEST);
            }
            $planActivityLogService->setApiFlag(true);
            return ApiResponse::create(
                $planActivityLogService->getFlatPlanActivityLogList($trip_id)
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/place/{place_id}/visits",
     *   tags={"Trip"},
     *   summary="Get visit user list",
     *   operationId="Api\TripController@getPlaceVisits",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="place_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="type",
     *        description="friend, follower, other",
     *        in="query",
     *        required=true,
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="page",
     *        description="pagination page | default 1",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="per_page",
     *        description="per_page  | default 10",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     **/
    public function getPlaceVisits(Request $request, TripInvitationsService $tripInvitationsService, VisitsService $visitsService, $place_id)
    {
        try {
            $visitUsers = [
                'friend' => [],
                'follower' => [],
                'other' => []
            ];

            $type = $request->get('type', 'friend');

            if (!in_array($type, array_keys($visitUsers))) {
                return ApiResponse::__createBadResponse('type must be in list ' . implode(",", array_keys($visitUsers)));
            }

            $per_page = $request->get('per_page', 10);

            $friends = $tripInvitationsService->findFriends();
            $followers = $tripInvitationsService->findFollowers();
            $authId = auth()->id();
            $placesIds = [$place_id];
            $locationType = 0;

            $query = TripPlans::query()
                ->select(['trips.*', 'trips_places.time', 'trips_places.id as trip_place_id', 'trips_places.places_id'])
                ->selectRaw('count(DISTINCT CONCAT(trips_places.places_id, \' \', trips.id)) as places_count')
                ->join('trips_places', function ($q) use ($placesIds, $locationType) {
                    $now = Carbon::now();
                    $q->on('trips.active_version', 'trips_places.versions_id')
                        ->where('trips_places.time', '<', $now)
                        ->whereNull('trips_places.deleted_at');

                    switch ($locationType) {
                        case TrendsService::LOCATION_TYPE_PLACE:
                            $q->whereIn('trips_places.places_id', $placesIds);
                            break;
                        case TrendsService::LOCATION_TYPE_CITY:
                            $q->whereIn('trips_places.cities_id', $placesIds);
                            break;
                        case TrendsService::LOCATION_TYPE_COUNTRY:
                            $q->whereIn('trips_places.countries_id', $placesIds);
                            break;
                        default:
                            break;
                    }
                })
                ->where('users_id', '!=', $authId)
                ->with(['author'])
                ->orderByDesc('time');

            switch ($type) {
                case 'friend':
                    $query->whereIn('users_id', $friends);
                    break;
                case 'follower':
                    $query->whereIn('users_id', $followers);
                    break;
                case 'other':
                    $query->whereNotIn('users_id', $friends);
                    $query->whereNotIn('users_id', $followers);
                    break;
                case 'all':
                    $query->where(function ($q) use ($friends, $followers) {
                        $q->whereIn('users_id', $friends);
                        $q->orWhereIn('users_id', $followers);
                    });
                    break;
                default:
                    break;
            }

            if ($type === 'all') {
                $query->groupBy('places_id');
                return $query->pluck('places_id');
            } else {
                $query->groupBy('trips.id');
            }

            $visits = modifyPagination($query->paginate($per_page));
            $tempVisits = $visits->data;

            foreach ($tempVisits as &$visit) {
                if (in_array($visit->users_id, $friends->values()->toArray())) {
                    $visit->person_type = 'friend';
                } elseif (in_array($visit->users_id, $followers->values()->toArray())) {
                    $visit->person_type = 'follower';
                } else {
                    $visit->person_type = 'other';
                }

                if (!$visit->author) {
                    continue;
                }

                $visit->src = check_profile_picture($visit->author->profile_picture);
            }

            foreach ($tempVisits as $visit) {
                if (!$visit->author) {
                    continue;
                }

                $visitUsers[$visit->person_type][] = [
                    'id' => $visit->users_id,
                    'name' => $visit->author->name,
                    'src' => check_profile_picture($visit->author->profile_picture),
                    'visit' => diffForHumans($visit->time),
                    'plan_id' => $visit->id,
                    'trip_place_id' => $visit->trip_place_id
                ];
            }
            $visits->data = $visitUsers[$type];
            return ApiResponse::create($visits);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/place/{place_id}/visits-thumbs",
     *   tags={"Trip"},
     *   summary="Get visit user list",
     *   operationId="Api\TripController@getPlaceVisitslabels",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="place_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     **/
    public function getPlaceVisitslabels(Request $request, TripInvitationsService $tripInvitationsService, $place_id, VisitsService $visitsService)
    {
        try {
            $placesIds = [$place_id];
            $collect = collect($visitsService->getRelatedVisits($placesIds, false, null, 0));
            return ApiResponse::create([
                'total' => $collect->count(),
                'latest_users' => $collect->map(function ($item) {
                    return $item->src;
                })->take(3),
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/trip/trending-locations",
     *   tags={"Trip"},
     *   summary="Get trending locations for suggestion",
     *   operationId="Api\TripController@getTrendingLocations",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="if we have trip_id then it's required otherwise optional",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="location_type",
     *        description="location_type [0 = place, 1= city, 2 = country]",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="parent_id",
     *        description="Parent ID",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="lat",
     *        description="latitude",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="lng",
     *        description="longitude",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="except",
     *        description="except",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *  @OA\Parameter(
     *        name="is_near_by",
     *        description="is_near_by is boolean field default true",
     *        in="query",
     *        @OA\Schema(
     *            type="boolean"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     **/
    public function getTrendingLocations(Request $request, TrendsService $trendsService)
    {
        try {
            $validator = Validator::make($request->all(), [
                'location_type' => 'numeric',
                'parent_id'     => 'numeric|sometimes',
                'except'        => 'array|sometimes',
                'is_near_by'    => 'bool|sometimes',
                'lat'           => 'numeric|sometimes',
                'lng'           => 'numeric|sometimes'
            ]);

            if ($validator->fails()) return ApiResponse::createValidationResponse($validator->errors());
            $trip_id   = $request->get('trip_id', null);
            $location   = (int) $request->get('location_type', 1);
            $parentId   = $request->get('parent_id', null);
            $nearby     = $request->get('is_near_by', false);
            $except     = $request->get('except', []);
            $lat        = $request->get('lat', NULL);
            $lng        = $request->get('lng', NULL);
            $latlng     = ($lat == null || $lng == null) ? [] : [$lat, $lng];
            ini_set('memory_limit', '512M');
            //$trendsService->getRecursiveTrendingLocations(2, null, false, $except, []); // default trip load

            if ($trip_id) {
                $trip = TripPlans::find($trip_id);
                if ($trip) {
                    $except = array_merge($except, $trip->published_places->pluck('places_id')->toArray());
                }
            }

            $tempLocations = $trendsService->getRecursiveTrendingLocations($location, $parentId, $nearby, $except, $latlng);
            $tempLocations = $this->__cleanKeys($tempLocations);
            return ApiResponse::create($tempLocations);
            // $debugData = $trendsService->debugData;

            $locations = [];
            foreach ($tempLocations as $tempLocation) {
                $tempLocation['location'] = $this->__reFormatMedia($tempLocation['location']);
                if (isset($tempLocation['related_locations'])) {
                    $tempLocation['related_locations'] = $this->__reFormatRelatedLocation($tempLocation['related_locations']);
                }
                $tempLocation = $this->__reFormatTempLocation($tempLocation);
                $locations[] = $tempLocation;
            }

            return ApiResponse::create($locations);
        } catch (\Throwable $e) {
            // return ApiResponse::create([]);
            return ApiResponse::createServerError($e);
        }
    }

    private function __cleanKeys(&$tempLocations)
    {
        $location = [];
        foreach ($tempLocations as $tempLocation) {
            if (isset($tempLocation['related_locations'])) {
                $tempLocation['related_locations'] = collect($tempLocation['related_locations'])->values();

                $i = [];
                foreach ($tempLocation['related_locations'] as $_tempLocation) {
                    if (isset($_tempLocation['related_locations'])) {
                        $_tempLocation['related_locations'] = collect($_tempLocation['related_locations'])->values();
                    }
                    $i[] = $_tempLocation;
                }
                $tempLocation['related_locations'] = $i;
            }
            $location[] = $tempLocation;
        }
        return collect($location)->values();
    }

    private function __resetRelatedLocations(&$related_locations)
    {
        $locations = [];
        foreach ($related_locations as $tempLocation) {
            $tempLocation['location'] = $this->__reFormatMedia($tempLocation['location']);
            if (isset($tempLocation['related_locations'])) {
                $tempLocation['related_locations'] = $this->__reFormatRelatedLocation($tempLocation['related_locations']);
            }
            $tempLocation = $this->__reFormatTempLocation($tempLocation);
            $locations[] = $tempLocation;
        }
        return $locations;
    }

    private function __reFormatMedia(&$location)
    {
        $location->medias = isset($location->getMedias) ? $location->getMedias : [];
        unset($location->getMedias);

        foreach ($location->medias as $media) {
            $media->url = check_media_url($media->url);
        }
        unset($location->medias);

        return $location;
    }

    private function __reFormatRelatedLocation(&$related_locations)
    {
        $related_locations->medias = isset($related_locations->get_medias) ? $related_locations->get_medias : [];
        unset($related_locations->get_medias);
        $related_locations = $this->__resetRelatedLocations($related_locations);
        return $related_locations;
    }

    private function __reFormatTempLocation(&$tempLocation)
    {
        // return $tempLocation;
        $tempLocation = json_decode(json_encode($tempLocation));
        switch ($tempLocation->location_type) {
            case TrendsService::LOCATION_TYPE_PLACE:
                $tempLocation->location->title =  isset($tempLocation->location->trans[0]->title) ? $tempLocation->location->trans[0]->title : null;
                $tempLocation->location->address = isset($tempLocation->location->trans[0]->address) ? $tempLocation->location->trans[0]->address : null;
                $tempLocation->city->title = isset($tempLocation->city->trans[0]->title) ? $tempLocation->city->trans[0]->title : null;
                unset($tempLocation->location->trans, $tempLocation->city->trans);
                $placesIds = [$tempLocation->location->countries_id];
                $tempLocation->location->visits = TripPlans::query()
                    ->select(['trips.*', 'trips_places.time', 'trips_places.id as trip_place_id', 'trips_places.places_id'])
                    ->selectRaw('count(DISTINCT CONCAT(trips_places.places_id, \' \', trips.id)) as places_count')
                    ->join('trips_places', function ($q) use ($placesIds) {
                        $now = Carbon::now();
                        $q->on('trips.active_version', 'trips_places.versions_id')
                            ->where('trips_places.time', '<', $now)
                            ->whereNull('trips_places.deleted_at')
                            ->whereIn('trips_places.countries_id', $placesIds);
                    })
                    ->where('users_id', '!=', auth()->user()->id)
                    ->with(['author'])
                    ->groupBy('places_id')
                    ->orderByDesc('time')->count();
                $tempLocation->location->latest_visited = TripPlans::query()
                    ->select(['trips.*', 'trips_places.time', 'trips_places.id as trip_place_id', 'trips_places.places_id'])
                    ->selectRaw('count(DISTINCT CONCAT(trips_places.places_id, \' \', trips.id)) as places_count')
                    ->join('trips_places', function ($q) use ($placesIds) {
                        $now = Carbon::now();
                        $q->on('trips.active_version', 'trips_places.versions_id')
                            ->where('trips_places.time', '<', $now)
                            ->whereNull('trips_places.deleted_at')
                            ->whereIn('trips_places.countries_id', $placesIds);
                    })
                    ->where('users_id', '!=', auth()->user()->id)
                    ->with(['author'])
                    ->take(3)
                    ->groupBy('places_id')
                    ->orderByDesc('time')->get()->map(function ($visit) {
                        return check_profile_picture($visit->author->profile_picture);
                    });

                break;
            case TrendsService::LOCATION_TYPE_CITY:
                $tempLocation->location->title =  isset($tempLocation->location->trans[0]->title) ? $tempLocation->location->trans[0]->title : null;
                $tempLocation->location->population = isset($tempLocation->location->trans[0]->population) ? $tempLocation->location->trans[0]->population : null;
                unset($tempLocation->location->trans);
                break;
            case TrendsService::LOCATION_TYPE_COUNTRY:
                $tempLocation->location->title =  isset($tempLocation->location->trans[0]->title) ? $tempLocation->location->trans[0]->title : null;
                $tempLocation->location->population = isset($tempLocation->location->trans[0]->population) ? $tempLocation->location->trans[0]->population : null;
                $tempLocation->location->metrics = isset($tempLocation->location->trans[0]->metrics) ? $tempLocation->location->trans[0]->metrics : null;
                $tempLocation->location->working_days = isset($tempLocation->location->trans[0]->working_days) ? $tempLocation->location->trans[0]->working_days : null;
                unset($tempLocation->location->trans);
                break;
        }
        return collect($tempLocation)->toArray();
    }

    /**
     * @OA\Get(
     ** path="/trip/{trip_id}/map",
     *   tags={"Trip"},
     *   summary="Trip Map Points",
     *   operationId="Api/TripController@tripMapPoints",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="trip_id",
     *        description="trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param TripInvitationsService $tripInvitationsService
     * @param PlaceChatsService $placeChatsService
     * @param PlanActivityLogService $planActivityLogService
     * @param TripsService $tripService
     * @param int $id
     * @raturn ApiResponse
     * */
    public function tripMapPoints(
        Request $request,
        TripsSuggestionsService $tripsSuggestionsService,
        TripInvitationsService $tripInvitationsService,
        PlaceChatsService $placeChatsService,
        PlanActivityLogService $planActivityLogService,
        TripsService $tripService,
        $id
    ) {
        $data = $map_places = $trip_places_arr = array();
        $data['trip_id'] = (int)$id;

        $trip = TripPlans::find($id);
        if (!$trip) {
            return ApiResponse::create([
                'message' => ['trip plan does not exist.']
            ], false, ApiResponse::BAD_REQUEST);
        }

        $data['trip'] = $trip;
        $invitedRequest = $tripsSuggestionsService->getInvitedUserRequest($id, auth()->id(), false);
        $cantEdit = !$invitedRequest || $invitedRequest->status === 0 || ($invitedRequest->role !== 'editor' && $invitedRequest->role !== 'admin');
        $data['is_admin'] = $data['trip']->users_id === auth()->id() || is_super_admin(auth()->id()) || ($invitedRequest && $invitedRequest->status === TripInvitationsService::STATUS_ACCEPTED && $invitedRequest->role === 'admin');
        $data['is_editor'] = $invitedRequest && $invitedRequest->status === TripInvitationsService::STATUS_ACCEPTED && $invitedRequest->role === 'editor';
        $data['is_invited'] = $invitedRequest && $invitedRequest->status === TripInvitationsService::STATUS_ACCEPTED;
        $data['draft_view_mode'] = $invitedRequest && $invitedRequest->status === TripInvitationsService::STATUS_PENDING && ($invitedRequest->role === 'editor' || $invitedRequest->role === 'admin');


        $my_version_id = $data['trip']->versions[0]->id;
        $data['my_version_id'] = $my_version_id;
        $trip_places = TripPlaces::where('trips_id', $id)
            ->where('versions_id', $my_version_id)
            ->orderBy('time', 'ASC')
            ->get();

        foreach ($trip_places as $tp) {
            $tripPlaceId = $tripsSuggestionsService->getRootTripPlace($tp->id);
            $map_places[@$tripPlaceId] = [
                'city_id' => @$tp->city->id,
                'place_id' => @$tripPlaceId,
                'lat' => @$tp->place->lat,
                'lng' => @$tp->place->lng,
                'time' => $tp->time,
            ];
        }

        $tripPlaceswithMulti = [];
        foreach ($trip_places as $tp) {
            $tripPlaceId = $tripsSuggestionsService->getRootTripPlace($tp->id);

            $item = $tp->place;
            $item['image'] = check_place_photo($tp->place);
            $item['time'] = $tp->time;
            $item['trip_place_id'] = $tp->id;
            $item['name'] = $tp->place->transsingle->title;
            $item['address'] = $tp->place->transsingle->address;
            $item['description'] = $tp->place->transsingle->description;
            $item['city_name'] = $tp->place->city->trans->first()->title;
            $item['reviews'] = Reviews::where('places_id', $tp->place->id)->limit(3)->orderBy('id', 'DESC')->get();
            $item['reviews_avg'] = Reviews::where('places_id', $tp->place->id)->avg('score');
            $item['checkins'] = $tp->place->checkins;
            unset($tp->city, $tp->place);

            $tripPlaceswithMulti[$tripPlaceId]['place'] = $item;
            $tripPlaceswithMulti[$tripPlaceId]['multiple'][] = $tp;
            unset($item->city, $item->transsingle);
        }

        $map_places = collect($map_places)->sortBy('time')->toArray();
        $trip_map_points = $map_places;
        foreach ($trip_map_points as &$trip_map_point) {
            $place_id = $trip_map_point['place_id'];
            $trip_map_point['data'] = $tripPlaceswithMulti[$place_id];
        }

        $data['trip_map_points'] = array_values($trip_map_points);
        return ApiResponse::create($data);
    }

    //[TB-231,TB-334,TB-229,TB-233]
    /**
     * @OA\Get(
     ** path="/trip/{trip_id}",
     *   tags={"Trip"},
     *   summary="Timeline API [TB-231,TB-334,TB-229,TB-233]",
     *   operationId="Api\TripController@getPlanContents",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="trip_id",
     *        description="Trip plan id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="mode",
     *        description="Action: view | edit",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *  @OA\Response(
     *      response=403,
     *      description="No Permission"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     * @param PlanContentsRequest $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function getPlanContents(
        Request $request,
        TripsSuggestionsService $tripsSuggestionsService,
        PlanActivityLogService $planActivityLogService,
        TripInvitationsService $tripInvitationsService,
        $id
    ) {
        try {
            $trip = TripPlans::find($id);
            if (!$trip) {
                return ApiResponse::createValidationResponse([
                    'id' => ['The trip id is invalid.']
                ]);
            }
            $mode = $request->get('mode', 'view');

            $trip_id    = $id;
            $editMode   = !($mode == "view");
            $editable   = $editMode;

            $invitedRequest = $tripsSuggestionsService->getInvitedUserRequest($trip_id, auth()->id(), false);
            $cantEdit = !$invitedRequest || $invitedRequest->status === TripInvitationsService::STATUS_PENDING || ($invitedRequest->role !== TripInvitationsService::ROLE_EDITOR && $invitedRequest->role !== TripInvitationsService::ROLE_ADMIN);

            // if ($cantEdit && $editMode && $trip->users_id !== auth()->id()) {
            //     return ApiResponse::__createBadResponse("you can not edit plan");
            // }

            if (Auth::check() && !(is_super_admin(Auth::user()->id))) {
                if ($trip->privacy === TripsService::PRIVACY_PRIVATE && $trip->users_id !== auth()->id()) {
                    if (!$invitedRequest) {
                        return ApiResponse::__createNoPermissionResponse();
                    }
                    if (($invitedRequest->role !== TripInvitationsService::ROLE_EDITOR && $invitedRequest->role !== TripInvitationsService::ROLE_ADMIN) && $editMode) {
                        return ApiResponse::__createBadResponse("you can not edit plan");
                    }
                } elseif ($trip->privacy === TripsService::PRIVACY_PUBLIC && $trip->users_id !== auth()->id() && $editMode) {
                    if ($cantEdit) {
                        return ApiResponse::__createBadResponse("you can not edit plan");
                    }
                } elseif ($trip->active === 0 && $trip->users_id !== auth()->id() && !$editMode) {
                    if (!$invitedRequest) {
                        return ApiResponse::__createNoPermissionResponse("access denied");
                    }
                } elseif (Auth::check() && $trip->privacy === TripsService::PRIVACY_FRIENDS && $trip->users_id !== auth()->id()) {
                    $isFriend = in_array($trip->users_id, $tripInvitationsService->findFriends()->toArray());
                    if (!$invitedRequest && !$isFriend || ($editMode && $cantEdit)) {
                        return ApiResponse::__createNoPermissionResponse();
                    }
                    if ($editMode && $cantEdit) {
                        return ApiResponse::__createNoPermissionResponse();
                    }
                }
                if (!$trip->active && $trip->users_id !== auth()->id() && (!$invitedRequest || ($invitedRequest->role !== TripInvitationsService::ROLE_EDITOR && $invitedRequest->role !== TripInvitationsService::ROLE_ADMIN))) {
                    return ApiResponse::__createNoPermissionResponse();
                }
            } elseif (!Auth::check() && $trip->privacy !== TripsService::PRIVACY_PUBLIC) {
                return ApiResponse::__createNoPermissionResponse();
            }

            // timeline
            $content = $this->getPlanContentsApi($editable, $trip_id, auth()->id(), $tripsSuggestionsService);
            $content['invited_people'] = [];
            if (auth()->check()) {
                // $content['invited_people'] = $this->getInvitedPeopleList($tripInvitationsService, $trip_id);
                $content['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $trip_id);
            }

            if (!$editMode) {
                unset($content['is_edited'], $content['updated_places'], $content['unread_logs_count']);
            }

            // if (isDevSite() && $trip_id == 2611) {
            //     dd(collect($content)->toArray());
            // }

            return ApiResponse::create($content);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/trip/{trip_id}/summary",
     *   tags={"Trip"},
     *   summary="Trip Summary",
     *   operationId="Api/TripController@getBasicPlanContents",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="trip_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function getBasicPlanContents(
        Request $request,
        TripsSuggestionsService $tripsSuggestionsService,
        PlanActivityLogService $planActivityLogService,
        TripInvitationsService $tripInvitationsService,
        $trip_id
    ) {
        try {
            $tripPlan = TripPlans::find($trip_id);
            if (!$tripPlan) {
                return ApiResponse::create([
                    'message' => ['The trip id is invalid.']
                ],  false,  ApiResponse::BAD_REQUEST);
            }

            $content    = $tripsSuggestionsService->getBasicPlanContentsApi(true, $trip_id, auth()->id());

            $trip_place_medias = $content['trip_place_medias'];
            $dummy_trip_place_medias = [];
            foreach ($trip_place_medias as $place_id => $media_place) {
                $dummy_media_place = [];
                foreach ($media_place as $media) {
                    $media->media;
                    $dummy_media_place[] = $media;
                }
                $dummy_trip_place_medias[] = [
                    'place_id'  => $place_id,
                    'medias'    => $dummy_media_place
                ];
            }
            $content['trip_place_medias'] = $dummy_trip_place_medias;
            $content['trip']->author;
            $content['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $trip_id);

            unset(
                $content['editable'],
                $content['do'],
                $content['is_edited'],
                $content['trip_places'],
                $content['trip_places_arr'],
                $content['trip_arr'],
                $content['updated_places'],
                $content['unread_logs_count']
            );

            return ApiResponse::create($content);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param PlanContentsRequest $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function getTripMenu(
        PlanContentsRequest $request,
        TripsSuggestionsService $tripsSuggestionsService,
        PlanActivityLogService $planActivityLogService,
        TripInvitationsService $tripInvitationsService
    ) {
        try {
            $trip = TripPlans::find($request->trip_id);
            if (!$trip) {
                return ApiResponse::createValidationResponse([
                    'trip_id' => ['The trip id is invalid.']
                ]);
            }

            $editable   = false;
            $trip_id    = $request->trip_id;
            $editMode   = true;
            $inviteRequest = $tripsSuggestionsService->getInvitedUserRequest($trip_id, auth()->id(), false);

            if ($editMode || ($inviteRequest && $inviteRequest->status === 0)) {
                $editable = true;
            }

            $content = $this->getPlanContentsApi($editable, $trip_id, auth()->id(), $tripsSuggestionsService);
            if (auth()->check()) {
                $content['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $trip_id);
            }

            unset(
                $content['trip_places'],
                $content['trip_place_medias'],
                $content['is_edited'],
                $content['authUserId'],
                $content['not_approved_suggestions']
            );

            return ApiResponse::create($content);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    // Suggestion Api || Suggestion Api || Suggestion Api || Suggestion Api || Suggestion Api || Suggestion Api || Suggestion Api

    /**
     * @OA\POST(
     ** path="/trip/suggestion/{suggestion_id}/approve",
     *   tags={"Trip Suggestion"},
     *   summary="This Api used to approve suggestion. (APPROVE_TRIP_PLACE_SUGGESTION_URL|APPROVE_TRIP_SUGGESTION_URL)",
     *   operationId="Api\TripController@approveSuggestion",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="suggestion_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param ApproveSuggestionRequest $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function approveSuggestion(Request $request, TripInvitationsService $tripInvitationsService, TripsSuggestionsService $tripsSuggestionsService, PlanActivityLogService $planActivityLogService, $suggestion_id)
    {
        try {
            $suggestion = $tripsSuggestionsService->approveSuggestion($suggestion_id);
            $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_APPROVED_SUGGESTION, $suggestion->trips_id, $suggestion->id, auth()->id());

            // timeline // need to hard reload whole timeline
            $timeline = $this->getPlanContentsApi(true, $suggestion->trips_id, auth()->id(), $tripsSuggestionsService);
            $timeline['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $suggestion->trips_id);
            unset($timeline['is_edited'], $timeline['updated_places'], $timeline['unread_logs_count']);

            return ApiResponse::create([
                "message" => ['Suggestion approved successfully'],
                "timeline" => $timeline
            ]);
        } catch (\Throwable $e) {
            if (get_class($e) == ModelNotFoundException::class) {
                return ApiResponse::create([
                    'message' => ['trip not found']
                ], false, ApiResponse::BAD_REQUEST);
            } else if (get_class($e) == BadRequestHttpException::class) {
                return ApiResponse::create([
                    'message' => ['Sonmething went wrong. please try again.']
                ], false, ApiResponse::SERVER_ERROR);
            } else if (get_class($e) == Exception::class) {
                return ApiResponse::create([
                    'message' => ['Wrong suggestion id']
                ], false, ApiResponse::BAD_REQUEST);
            } else if (get_class($e) == AuthorizationException::class) {
                return ApiResponse::create([
                    'message' => ['unauthorized user']
                ], false, ApiResponse::UNAUTHORIZED);
            } else {
                return ApiResponse::createServerError($e);
            }
        }
    }

    /**
     * @OA\POST(
     ** path="/trip/suggestion/{suggestion_id}/disapprove",
     *   tags={"Trip Suggestion"},
     *   summary="This Api used to disapprove suggestion. (REJECT_TRIP_PLACE_SUGGESTION_URL|REJECT_TRIP_SUGGESTION_URL)",
     *   operationId="Api\TripController@disapproveSuggestion",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="suggestion_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param ApproveSuggestionRequest $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function disapproveSuggestion(Request $request, TripInvitationsService $tripInvitationsService, TripsSuggestionsService $tripsSuggestionsService, PlanActivityLogService $planActivityLogService, $suggestion_id)
    {
        try {
            $suggestion = $tripsSuggestionsService->disapproveSuggestion($suggestion_id);
            $tripId = $suggestion->trips_id;
            $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_DISAPPROVED_SUGGESTION, $tripId, $suggestion->id, auth()->id());

            // timeline // need to hard reload whole timeline
            $timeline = $this->getPlanContentsApi(true, $tripId, auth()->id(), $tripsSuggestionsService);
            $timeline['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $tripId);
            unset($timeline['is_edited'], $timeline['updated_places'], $timeline['unread_logs_count']);

            return ApiResponse::create([
                "message" => ['disapprove suggestion'],
                "timeline" => $timeline
            ]);
        } catch (\Throwable $e) {
            if (get_class($e) == ModelNotFoundException::class) {
                return ApiResponse::create([
                    'message' => ['trip not found']
                ], false, ApiResponse::BAD_REQUEST);
            } else if (get_class($e) == BadRequestHttpException::class) {
                return ApiResponse::create([
                    'message' => ['Sonmething went wrong. please try again.']
                ], false, ApiResponse::SERVER_ERROR);
            } else if (get_class($e) == AuthorizationException::class) {
                return ApiResponse::create([
                    'message' => ['unauthorized user']
                ], false, ApiResponse::UNAUTHORIZED);
            } else {
                return ApiResponse::createServerError($e);
            }
        }
    }

    //TB-414
    /**
     * @OA\POST(
     ** path="/trip/{trip_id}/suggestions",
     *   tags={"Trip Suggestion"},
     *   summary="This Api used to send all suggestion to trip creator.",
     *   operationId="Api\TripController@sendSuggestions",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @param TripsService $tripsService
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param int $trip_id
     * @return ApiResponse
     */
    public function sendSuggestions(
        Request $request,
        TripsService $tripsService,
        TripsSuggestionsService $tripsSuggestionsService,
        $trip_id
    ) {
        try {
            $plan = $tripsService->findPlan($trip_id);
            if (!$plan) {
                return ApiResponse::create([
                    'message' => ['Trip not found']
                ], false, ApiResponse::BAD_REQUEST);
            }
            $tripsSuggestionsService->sendSuggestions($trip_id);

            return ApiResponse::create([
                'message' => ["Suggestions send successfully."]
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    // Invitation API || Invitation API || Invitation API || Invitation API || Invitation API || Invitation API || Invitation API


    /**
     * @OA\POST(
     ** path="/trip/{trip_id}/invite",
     *   tags={"Trip Invitation"},
     *   summary="invite user in trip plan",
     *   operationId="Api\TripController@postInviteFriends",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="role",
     *        description="admin, editor",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="notes",
     *        description="notes",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param InviteFriendsRequest $request
     * @param TripInvitationsService $tripInvitationsService
     * @return ApiResponse
     */
    public function postInviteFriends(InviteFriendsRequest $request, TripInvitationsService $tripInvitationsService, $trip_id)
    {
        try {
            $planId = $trip_id;
            $userId = $request->user_id;
            $notes = $request->notes;
            $role = $request->role;
            $w_plans_id = ($request->w_plans_id !== null) ? $request->w_plans_id : null;

            if ($userId == Auth::user()->id) {
                return ApiResponse::create(['message' => ['invalid user']], false, ApiResponse::BAD_REQUEST);
            }

            $trip = TripPlans::find($planId);
            $author  = $trip->author;

            $tripInvitationsService->invite($planId, $userId, $notes, $role, $w_plans_id);
            $invite = TripsContributionRequests::where([
                'plans_id' => $planId,
                'users_id' => $userId
            ])->first();

            try {
                event(new InvitePeopleToTripEvent([
                    'invitation_id' => $invite->id,
                    'trip_id'       => $planId,
                    'users_id'      => $userId,
                    'role'          => $role,
                    'author'        => [
                        'name'              => $author->name,
                        'email'             => $author->email,
                        'username'          => $author->username,
                        'profile_picture'   =>  @check_profile_picture($author->profile_picture),
                    ]
                ]));
            } catch (\Throwable $e) {
                return ApiResponse::createServerError($e);
            }

            return ApiResponse::create([
                'message'       => ["invite successfully."],
                'invitation_id' => $invite->id
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/trip/{trip_id}/people",
     *   tags={"Trip Invitation"},
     *   summary="Get people to invite  (FETCH_TRIP_PEOPLE_SEARCH_URL)",
     *   operationId="Api\TripController@getPeopleToInvite",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="trip_id",
     *        description="Trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="with_styles",
     *        description="for match with my styles | default = false",
     *        in="query",
     *        @OA\Schema(
     *            type="boolean"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="q",
     *        description="Search",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param PeopleToInviteRequest $request
     * @param TripInvitationsService $tripInvitationsService
     * @return ApiResponse
     */
    public function getPeopleToInvite(Request $request, TripInvitationsService $tripInvitationsService, $id)
    {
        try {
            $search = $request->q ?? '';
            $planId = $id;
            $withStyles = $request->get('with_styles', false);

            if ($search == "" || $search == "null" || $search == null || $search == "undefined") {
                $search = null;
            }

            $getPeopleToInvite  = $tripInvitationsService->getPeopleToInvite($search, $planId, $withStyles);
            foreach ($getPeopleToInvite as $groupName => $group) {
                if (is_array($group)) {
                    $j = 0;
                    foreach ($group as $user) {
                        $expert_countries = ExpertsCountries::where('users_id', $user['id'])->take(5)->get();
                        foreach ($expert_countries as $expert_countrie) {
                            $expert_countrie->countries->title = isset($expert_countrie->countries->trans[0]->title) ?
                                $expert_countrie->countries->trans[0]->title : null;
                            unset($expert_countrie->countries->trans);
                        }

                        $getPeopleToInvite[$groupName][$j]['expert_countries'] = $expert_countries;
                        $getPeopleToInvite[$groupName][$j]['resend_disabled'] = null;
                        unset($getPeopleToInvite[$groupName][$j]['role']);
                        $j++;
                    }
                }
            }

            return ApiResponse::create([
                $getPeopleToInvite
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/trip/{id}/invited-people",
     *   tags={"Trip Invitation"},
     *   summary="Get invited-people (FETCH_TRIP_PEOPLE_URL)",
     *   operationId="Api\TripController@getInvitedPeople",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="id",
     *        description="Trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param InvitedPeopleRequest $request
     * @param TripInvitationsService $tripInvitationsService
     * @return ApiResponse
     */
    public function getInvitedPeople(Request $request, TripInvitationsService $tripInvitationsService, $id)
    {
        try {
            $planId = $id;
            $data = $tripInvitationsService->getInvitedPeople($planId, false, false);
            $data['pending']    = isset($data['pending'])   ? $data['pending']  : [];
            $data['admin']      = isset($data['admin'])     ? $data['admin']    : [];
            $data['viewer']     = isset($data['viewer'])    ? $data['viewer']   : [];
            $data['editor']     = isset($data['editor'])    ? $data['editor']   : [];

            foreach ($data as $groupName => $group) {
                if (is_array($group)) {
                    $j = 0;
                    foreach ($group as $user) {
                        $data[$groupName][$j]['timeout'] = 0;
                        $data[$groupName][$j]['invited'] = ($groupName == "pending") ? 0 : 1;
                        unset($data[$groupName][$j]['role']);
                        $j++;
                    }
                }
            }

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Delete(
     ** path="/trip/{trip_id}/invite",
     *   tags={"Trip Invitation"},
     *   summary="Delete Invitation",
     *   operationId="Api\TripController@postCancelInvitation",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param CancelInvitationRequest $request
     * @param TripInvitationsService $tripInvitationsService
     * @return ApiResponse
     */
    public function postCancelInvitation(CancelInvitationRequest $request, TripInvitationsService $tripInvitationsService, $trip_id)
    {
        try {
            $userId = $request->user_id;
            $trip = TripPlans::find($trip_id);
            if (!$trip) {
                return ApiResponse::create([
                    'message' => ["trip not found."]
                ], false, ApiResponse::BAD_REQUEST);
            }

            if ($tripInvitationsService->cancelInvitation($trip_id, $userId)) {
                return ApiResponse::create([
                    'message' => ["Cancel invitation successfully."]
                ]);
            } else {
                return ApiResponse::create(
                    [
                        'message' => ["Invitation not found"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Put(
     ** path="/trip/{trip_id}/invite/role",
     *   tags={"Trip Invitation"},
     *   summary="chnage role of user",
     *   operationId="Api\TripController@changeInvitationRequestRole",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="role",
     *        description="admin, editor",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param ChangeInvitationRequest $request
     * @param TripInvitationsService $tripInvitationsService
     * @return ApiResponse
     */
    public function changeInvitationRequestRole(ChangeInvitationRequest $request, TripInvitationsService $tripInvitationsService, $trip_id)
    {
        try {
            $trip = TripPlans::find($trip_id);
            if (!$trip) {
                return ApiResponse::create([
                    'message' => ["trip not found."]
                ], false, ApiResponse::BAD_REQUEST);
            }
            $userId = $request->user_id;
            $role   = $request->role;

            $tripInvitationsService->changeRole($userId, $trip_id, $role);

            return ApiResponse::create(
                [
                    'message' => ['Role change successfully']
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/trip/invitation/{invitation_id}/accept",
     *   tags={"Trip Invitation"},
     *   summary="Accept Invitation",
     *   operationId="Api\TripController@postAcceptInvitation",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="invitation_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param AcceptInvitationRequest $request
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function postAcceptInvitation(Request $request, PlanActivityLogService $planActivityLogService, $invitation_id)
    {
        try {
            $user_id = Auth::user()->id;
            $ret = [];
            $invite = TripsContributionRequests::find($invitation_id);
            if (!$invite) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Invitation Id']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $invite->status = 1;
            if ($invite->save()) {
                notify($invite->authors_id, 'plan_invitetion_accepted', $invite->id, 'travel-mate');

                $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_JOINED_THE_PLAN, $invite->plans_id, $invite->id, auth()->id());

                Notifications::where('users_id', $user_id)->where('data_id', $invite->id)->delete();

                $from_version = TripsVersions::where('plans_id', $invite->plans_id)->where('authors_id', $invite->authors_id)->get()->first();
                $to_version = new TripsVersions;
                $to_version->authors_id = $user_id;
                $to_version->start_date = $from_version->start_date;
                $to_version->plans_id = $from_version->plans_id;
                if ($to_version->save()) {
                    log_user_activity('Trip', 'accept_invitation', $invitation_id);

                    $v = new TripsContributionVersions;
                    $v->plans_id = $invite->plans_id;
                    $v->requests_id = $invite->id;
                    $v->version_number = 1;
                    $v->status = 0;
                    $v->save();

                    // copy cities
                    $old_cities = TripCities::where('trips_id', $invite->plans_id)->where('versions_id', $from_version->id)->get();
                    //dd($old_cities);
                    foreach ($old_cities as $old_city) {
                        $new_city = new TripCities;
                        $new_city->trips_id = $invite->plans_id;
                        $new_city->cities_id = $old_city->cities_id;
                        $new_city->date = $old_city->date;
                        $new_city->order = $old_city->order;
                        $new_city->active = $old_city->active;
                        $new_city->transportation = $old_city->transportation;
                        $new_city->versions_id = $to_version->id;
                        $new_city->save();
                    }
                    // copy countries
                    $old_countries = TripCountries::where('trips_id', $invite->plans_id)->where('versions_id', $from_version->id)->get();
                    foreach ($old_countries as $old_country) {
                        $new_country = new TripCountries;
                        $new_country->trips_id = $invite->plans_id;
                        $new_country->countries_id = $old_country->countries_id;
                        $new_country->date = $old_country->date;
                        $new_country->order = $old_country->order;
                        $new_country->versions_id = $to_version->id;
                        $new_country->save();
                    }
                    // copy places
                    $old_places = TripPlaces::where('trips_id', $invite->plans_id)->where('versions_id', $from_version->id)->get();
                    foreach ($old_places as $old_place) {
                        $new_place = new TripPlaces;
                        $new_place->trips_id = $invite->plans_id;
                        $new_place->countries_id = $old_place->countries_id;
                        $new_place->cities_id = $old_place->cities_id;
                        $new_place->places_id = $old_place->places_id;
                        $new_place->date = $old_place->date;
                        $new_place->order = $old_place->order;
                        $new_place->time = $old_place->time;
                        $new_place->duration = $old_place->duration;
                        $new_place->budget = $old_place->budget;
                        $new_place->active = $old_place->active;
                        $new_place->versions_id = $to_version->id;
                        $new_place->save();
                    }

                    return ApiResponse::create([
                        'trip_id' => $invite->plans_id,
                        'message' => 'Accept invitation successfully'
                    ]);
                }
            }
            return ApiResponse::create(
                [
                    'message' => 'Please try again after sometime'
                ],
                false,
                APIRESPONSE::BAD_REQUEST
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/trip/invitation/{invitation_id}/reject",
     *   tags={"Trip Invitation"},
     *   summary="Reject Invitation",
     *   operationId="Api\TripController@postRejectInvitation",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="invitation_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param AcceptInvitationRequest $request
     * @return ApiResponse
     */
    public function postRejectInvitation(Request $request, $invitation_id)
    {
        try {
            $user_id = Auth::user()->id;

            $invite = TripsContributionRequests::find($invitation_id);
            $invite->status = 2;
            if ($invite->save()) {
                log_user_activity('Trip', 'reject_invitation', $invitation_id);
                notify($invite->authors_id, 'plan_invitetion_rejected', $invitation_id, 'travel-mate');
                Notifications::where('users_id', $user_id)->where('data_id', $invite->id)->delete();

                return ApiResponse::create([
                    'message' => ['Reject invitation successfully']
                ]);
            } else {
                return ApiResponse::create(['message' => ['fail']], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    // Leave from Trip || Leave from Trip || Leave from Trip || Leave from Trip || Leave from Trip || Leave from Trip

    /**
     * @OA\GET(
     ** path="/trip/{trip_id}/leave",
     *   tags={"Trip Invitation"},
     *   summary="get all suggestion list before leave",
     *   operationId="Api\TripController@getLeaveList",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getLeaveList(TripsSuggestionsService $tripsSuggestionsService, $trip_id)
    {
        try {
            $trip = TripPlans::find($trip_id);
            if (!$trip) {
                return ApiResponse::create([
                    'message' => ['trip not found']
                ], false, ApiResponse::BAD_REQUEST);
            }

            $data = TripsSuggestion::query()
                ->where([
                    'users_id' => auth()->id(),
                    'trips_id' => $trip_id,
                    'status' => TripsSuggestionsService::SUGGESTION_APPROVED_STATUS
                ])
                ->where('type', '!=', TripsSuggestionsService::SUGGESTION_EDIT_BY_EDITOR_TYPE)
                ->with('suggestedPlace', 'suggestedPlace.place', 'user')
                ->get();

            foreach ($data as &$approvedSuggestion) {
                if (!$approvedSuggestion->suggestedPlace) {
                    continue;
                }
                $approvedSuggestion->place_img = check_place_photo($approvedSuggestion->suggestedPlace->place);
                $approvedSuggestion->place_name = $approvedSuggestion->suggestedPlace->place->transsingle->title;
                $approvedSuggestion->city = $approvedSuggestion->suggestedPlace->city->transsingle->title;
                $approvedSuggestion->country = $approvedSuggestion->suggestedPlace->city->country->transsingle->title;
                unset($approvedSuggestion->suggestedPlace->city, $approvedSuggestion->suggestedPlace->city->country);
            }

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/trip/{trip_id}/leave",
     *   tags={"Trip Invitation"},
     *   summary="leave from trip plan",
     *   operationId="Api\TripController@postLeavePlan",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @param TripInvitationsService $tripInvitationsService
     * @param int $trip_id
     * @return ApiResponse
     */
    public function postLeavePlan(Request $request, TripInvitationsService $tripInvitationsService, $trip_id)
    {
        try {
            $tripId     = $trip_id;;
            $old_plan   = TripPlans::where('id', $tripId)->first();

            if ($old_plan) {
                if ($tripInvitationsService->leavePlan($tripId)) {
                    return ApiResponse::create([
                        'message' => ["Leave successfully."]
                    ]);
                } else {
                    return ApiResponse::create([
                        'message' => ['sorry you are not invite for this trip.']
                    ], false,  ApiResponse::BAD_REQUEST);
                }
            } else {
                return ApiResponse::create([
                    'message' => ['trip plans not found.']
                ], false,  ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            if (get_class($e) == ModelNotFoundException::class) {
                return ApiResponse::create([
                    'message' => ['trip not found']
                ], false, ApiResponse::BAD_REQUEST);
            } else {
                return ApiResponse::createServerError($e);
            }
        }
    }

    /**
     * @param Request $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function getTripPlace(Request $request, PlanActivityLogService $planActivityLogService, TripsSuggestionsService $tripsSuggestionsService)
    {
        try {
            $data = array();
            $place_id = $request->get('place_id');
            $tripPlace = TripPlaces::findOrFail($place_id);
            $place = @Place::find($tripPlace->places_id);
            $tripPlace->dateForPicker = strtotime($tripPlace->date) * 1000;
            $tripPlace->timeForPicker = (new Carbon($tripPlace->time))->format('g:ia');
            $tripPlace->cities_name = @Cities::find($tripPlace->cities_id)->transsingle->title;
            $tripPlace->countries_name = Countries::find($tripPlace->countries_id)->transsingle->title;
            @$tripPlace->places_name = Place::find($tripPlace->places_id)->transsingle->title;

            if ($place) {
                if (!isset($place->getMedias[0])) {
                    $tripPlace->places_img = asset('assets2/image/placeholders/pattern.png');
                } elseif ($place->getMedias[0]->thumbs_done == 1) {
                    $tripPlace->places_img = S3_BASE_URL . 'th1100/' . $place->getMedias[0]->url;
                } else {
                    $tripPlace->places_img = S3_BASE_URL . $place->getMedias[0]->url;
                }
            }
            $tripPlace->trip_media = @TripMedias::where('places_id', $place_id)->first()->media->url;

            $tripPlace->cities_name = $tripPlace->cities_name . ', city in ' . $tripPlace->countries_name;
            $tripPlace->cities_img =  @check_city_photo(Cities::find($tripPlace->cities_id)->getMedias[0]->url, 180);

            $tripPlace->images = TripMedias::where('trips_id', $tripPlace->trips_id)
                ->whereIn('trip_place_id', $tripsSuggestionsService->getAllSuggestionPackTripPlacesIds($place_id))
                ->with('media')
                ->get();

            $tripPlace->updated_attributes = $planActivityLogService->getPlaceLogUpdatedAttributes($place_id);

            return ApiResponse::create($tripPlace);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param PlanRequest $request
     * @param TripsService $tripsService
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @return ApiResponse
     */
    public function saveDraft(PlanRequest $request, TripsService $tripsService, TripsSuggestionsService $tripsSuggestionsService)
    {
        try {
            $planId = $request->get('plan_id');
            $plan = $tripsService->findPlan($planId);
            if (!$plan) {
                return ApiResponse::create(['message' => ['Plan not found']], false,  ApiResponse::BAD_REQUEST);
            }

            $tripsSuggestionsService->saveChanges($plan);

            return ApiResponse::create(
                [
                    'message' => [true]
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/trip/{id}/undo",
     *   tags={"Trip"},
     *   summary="",
     *   operationId="Api\TripController@undoChanges",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="id",
     *        description="trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param PlanRequest $request
     * @param TripsService $tripsService
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @return ApiResponse
     */
    public function undoChanges(
        Request $request,
        TripsService $tripsService,
        TripsSuggestionsService $tripsSuggestionsService,
        TripInvitationsService $tripInvitationsService,
        $id
    ) {
        try {
            $planId = $id;
            $plan = $tripsService->findPlan($planId);
            if (!$plan) return ApiResponse::__createBadResponse("Trip not found.");

            if (!in_array($tripInvitationsService->getUserRole($planId, auth()->id()), [
                TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN],
                TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
            ])) {
                return ApiResponse::__createBadResponse("Only Admin or Editor can undo.");
            }

            $result = $tripsSuggestionsService->undoChanges($plan);
            if ($result === false) return ApiResponse::__createBadResponse("No change.");

            return ApiResponse::create([
                "message" => "Undo Successfully.",
                'count' => $result
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param PlanRequest $request
     * @param TripsService $tripsService
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @return ApiResponse
     */
    public function cancelChanges(PlanRequest $request, TripsService $tripsService, TripsSuggestionsService $tripsSuggestionsService)
    {
        try {
            $planId = $request->get('plan_id');
            $plan = $tripsService->findPlan($planId);

            if (!$plan) {
                return ApiResponse::create(['message' => ['Plan not found']], false,  ApiResponse::BAD_REQUEST);
            }

            $tripsSuggestionsService->cancelChanges($plan);

            return ApiResponse::create(
                [
                    'message' => [true]
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\POST(
     ** path="/trip/{trip_id}/activate",
     *   tags={"Trip"},
     *   summary="activate trip",
     *   operationId="Api\TripController@postActivateTrip",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param ActivateTripRequest $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return ApiResponse
     */
    public function postActivateTrip(ActivateTripRequest $request, TripsSuggestionsService $tripsSuggestionsService, PlanActivityLogService $planActivityLogService, $trip_id)
    {
        try {
            $my_trip = TripPlans::find($trip_id);
            if (!$my_trip) {
                return ApiResponse::create([
                    'message' => ["trip not found."]
                ], false, ApiResponse::BAD_REQUEST);
            }

            @$startPlaceDate    = $my_trip->trips_places()->orderBy('date')->first()->date;
            @$endPlaceDate      = $my_trip->trips_places()->orderBy('date', 'desc')->first()->date;

            if ($startPlaceDate) {
                $my_trip->version->start_date = $startPlaceDate;
            }

            if ($endPlaceDate) {
                $my_trip->version->end_date = $endPlaceDate;
            }
            $my_trip->version->save();
            $version_id =  $my_trip->version->id;

            if ($my_trip->update(['active' => 1])) {
                $tripsContributionRequests = TripsContributionRequests::where('plans_id', $trip_id)->where('users_id', Auth::user()->id)->get()->first();

                if ($tripsContributionRequests) {
                    $commit = new TripsContributionCommits;
                    $commit->plans_id = $trip_id;
                    $commit->requests_id = $tripsContributionRequests->id;
                    $commit->users_id = Auth::user()->id;
                    $commit->versions_id = $version_id;
                    $commit->notes = '';
                    $commit->status = 0;
                    $commit->save();
                }

                $not = new Notifications;
                $not->authors_id = Auth::user()->id;
                $not->users_id = $my_trip->users_id;
                $not->type = 'plan_version_sent';
                $not->data_id = $my_trip->id;
                $not->notes = '';
                $not->read = 0;
                $not->save();

                log_user_activity('Trip', 'activate', $trip_id);


                $published = $tripsSuggestionsService->publishPlan($my_trip);
                $my_trip->save();

                if ($published) {
                    $log = $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_PUBLISHED_PLAN, $trip_id, null, auth()->id());

                    try {
                        broadcast(new PlanPublicEvent($planActivityLogService->prepareLogForResponse($log), false));
                    } catch (\Throwable $e) {
                    }

                    try {
                        broadcast(new PlanPublicApiEvent($planActivityLogService->prepareLogForResponse($log), false));
                    } catch (\Throwable $e) {
                    }
                }

                $trip_link = generateDeepLink((url("api/trip") . "/{$trip_id}"));

                return ApiResponse::create([
                    'message' => ['save successfully'],
                    'trip_link' => $trip_link
                ]);
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['something went wrong']
                    ],
                    false,
                    ApiResponse::SERVER_ERROR
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    // Chat Api || Chat Api || Chat Api || Chat Api || Chat Api || Chat Api || Chat Api || Chat Api || Chat Api || Chat Api
    // TB-202, TB-150
    /**
     * @OA\Get(
     ** path="/trip/{trip_id}/chat",
     *   tags={"Trip Chat"},
     *   summary="(FETCH_TRIP_PLACE_CHAT_URL)",
     *   operationId="Api/TripController@__chat",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="trip_id",
     *        description="trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    function __chat(PlaceChatsService $placeChatsService,  $trip_id)
    {
        try {
            $placeChatsService->setApi(true);
            return ApiResponse::create($placeChatsService->getApiPlanChats($trip_id));
        } catch (\Throwable $e) {
            if (get_class($e) == ModelNotFoundException::class) {
                return ApiResponse::__createBadResponse("Trip not found");
            } else {
                return ApiResponse::createServerError($e);
            }
        }
    }

    /**
     * @OA\Get(
     ** path="/trip/{trip_id}/place-chat",
     *   tags={"Trip Chat"},
     *   summary="(FETCH_TRIP_PLACE_CHAT_LIST_URL)",
     *   operationId="Api/TripController@__placeChat",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="trip_id",
     *        description="trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    function __placeChat(PlaceChatsService $placeChatsService, $trip_id)
    {
        try {
            $placeChatsService->setApi(true);
            return ApiResponse::create($placeChatsService->getApiPlanPlaceChats($trip_id));
        } catch (\Throwable $e) {
            if (get_class($e) == ModelNotFoundException::class) {
                return ApiResponse::__createBadResponse("Trip not found");
            } else {
                return ApiResponse::createServerError($e);
            }
        }
    }

    // Common Trip Helper || Common Trip Helper || Common Trip Helper || Common Trip Helper || Common Trip Helper || Common Trip Helper

    public function getSpecificPlace($trip_id, $place_id)
    {
        $trip = TripPlans::findOrFail($trip_id);
        $my_version_id = $trip->versions[0]->id;

        $new_trip_place = TripPlaces::where('trips_id', $trip_id)
            ->with(['place', 'place.transsingle', 'place.city', 'city', 'city.trans', 'place.city.trans', 'medias'])
            ->where('id', $place_id)
            ->orderBy('order', 'ASC')
            ->first();;

        $trip_place = $new_trip_place;
        $trip_place->tags = $trip_place->comment;

        $to_place                   = $trip_place['place'];
        $to_place['title']          = isset($to_place['transsingle']['title']) ? $to_place['transsingle']['title'] : null;
        $to_place['description']    = isset($to_place['transsingle']['description']) ? $to_place['transsingle']['description'] : null;
        $to_place['address']        = isset($to_place['transsingle']['address']) ? $to_place['transsingle']['address'] : null;

        $to_place_city                      = $trip_place['place']['city'];
        $to_place_city['title']             = isset($to_place_city['trans'][0]->title) ? $to_place_city['trans'][0]->title : null;
        $to_place_city['description']       = isset($to_place_city['trans'][0]->description) ? $to_place_city['trans'][0]->description : null;
        $to_place_city['languages_id']      = isset($to_place_city['trans'][0]->languages_id) ? $to_place_city['trans'][0]->languages_id : null;

        $to_city                    = $trip_place['city'];
        $to_city['title']           = isset($to_city->trans[0]->title) ? $to_city->trans[0]->title : null;
        $to_city['description']     = isset($to_city->trans[0]->description) ? $to_city->trans[0]->description : null;
        $to_city['languages_id']    = isset($to_city->trans[0]->languages_id) ? $to_city->trans[0]->languages_id : null;

        $toCountry                    = $trip_place['city']->country;
        $toCountry['image']           = url('assets2/image/placeholders/pattern.png');
        if (isset($toCountry->medias[0]->media->path)) {
            $toCountry['image']   = $toCountry->medias[0]->media->path;
        }
        $toCountry['title']           = isset($toCountry->trans[0]->title) ? $toCountry->trans[0]->title : null;
        $toCountry['description']     = isset($toCountry->trans[0]->description) ? $toCountry->trans[0]->description : null;
        $toCountry['languages_id']    = isset($toCountry->trans[0]->languages_id) ? $toCountry->trans[0]->languages_id : null;

        if (isset($trip_place['medias']) && $trip_place['medias']) {
            foreach ($trip_place['medias'] as $media) {
                $media->media;
            }
        }

        unset(
            $trip_place['place']['medias'],
            $trip_place['place']['transsingle'],
            $trip_place['place']['city']['trans'],
            $trip_place['city']['trans'],
            $trip_place['city']->country->medias,
            $trip_place['city']->country->trans
        );

        return $new_trip_place;
    }

    public function getPlanContentsApi($editable, $tripId, $authUserId, $tripsSuggestionsService)
    {
        $loginUser = auth()->user();

        $contents = $tripsSuggestionsService->getPlanContentsData($editable, $tripId, $authUserId);
        if ($loginUser) {
            $loginUser->profile_picture = check_profile_picture($loginUser->profile_picture);
        }
        $contents['loginUser'] = $loginUser;
        $trip_places = $original_trip_places = $contents['trip_places'];
        $trip_places_arr = $original_trip_places_arr = $contents['trip_places_arr'];

        unset(
            $contents['do'],
            $contents['approved_suggestions'],
            $contents['editable'],
            $contents['trip_places_arr'],
            $contents['trip_arr']
        );

        $contents['trip']->author->profile_picture = check_profile_picture($contents['trip']->author->profile_picture);
        $contents['trip']->cover = check_cover_photo($contents['trip']->cover);


        $contents['trip']->like_flag   = $loginUser ? (TripsLikes::where('trips_id', $contents['trip']->id)->where('users_id', $loginUser->id)->first() ? true : false) : false;
        $contents['trip']->total_likes = $contents['trip']->likes()->count();
        $contents['trip']->total_shares = TripsShares::where('trip_id', $contents['trip']->id)->count();
        $contents['trip']->total_comments = $contents['trip']->postComments()->count();


        // if (user_access_denied($contents['trip']->users_id, true)) {
        //     //access-denied
        // }

        // if (!Auth::check() && $contents['trip']->privacy !== 0) {
        //     // throw new PlanPermissionsException('No permissions', 403);
        // }

        $not_approved_suggestions = $contents['not_approved_suggestions'];
        $trip_place_medias = $contents['trip_place_medias'];
        $dummy_trip_place_medias = [];
        foreach ($trip_place_medias as $place_id => $media_place) {
            $dummy_media_place = [];
            foreach ($media_place as $media) {
                $media->media;
                $dummy_media_place[] = $media;
            }
            $dummy_trip_place_medias[] = [
                'place_id'  => $place_id,
                'medias'    => $dummy_media_place
            ];
        }
        $contents['trip_place_medias'] = $dummy_trip_place_medias;
        $contents['trip']->author;
        $noOfFlags = [];

        $dummy_trip_places = null;
        foreach ($trip_places as $date => $places) {

            $dummyPlaces = [];
            foreach ($places as $place) {

                if ($place['suggestion']  != null) {
                    $place['suggestion']['user'] = User::find($place['suggestion']['users_id']);
                }

                unset($place['suggestion']['suggestedPlace']);
                $trip_place = $place['trip_place'];
                $trip_place->tags = $trip_place->comment;

                if (isset($not_approved_suggestions[$trip_place->id])) {
                    $trip_place['users_suggestions'] = array_values($not_approved_suggestions[$trip_place->id]);
                } else {
                    $trip_place['users_suggestions'] = [];
                }
                if ($trip_place->medias) {
                    foreach ($trip_place->medias as $media) {
                        $media->media;
                    }
                } else {
                    $trip_place->medias = [];
                }
                $to_place                   = $trip_place['place'];
                $to_place['title']          = isset($to_place['transsingle']['title']) ? $to_place['transsingle']['title'] : null;
                $to_place['description']    = isset($to_place['transsingle']['description']) ? $to_place['transsingle']['description'] : null;
                $to_place['address']        = isset($to_place['transsingle']['address']) ? $to_place['transsingle']['address'] : null;
                $to_place['rating']         = $to_place['rating'] ? $to_place['rating'] : 0;
                if (!isset($to_place['name'])) {
                    $to_place['name']           = $to_place['title'];
                }
                if (!isset($to_place['image'])) {
                    $to_place['image'] = check_place_photo($trip_place['place']);
                }

                $to_place_city                      = $trip_place['place']['city'];
                $to_place_city['image']             = (isset($to_place_city->medias[0]->media->path)) ? $to_place_city->medias[0]->media->path : url(PATTERN_PLACEHOLDERS);
                $to_place_city['title']             = isset($to_place_city['trans'][0]->title) ? $to_place_city['trans'][0]->title : null;
                $to_place_city['description']       = isset($to_place_city['trans'][0]->description) ? $to_place_city['trans'][0]->description : null;
                $to_place_city['languages_id']      = isset($to_place_city['trans'][0]->languages_id) ? $to_place_city['trans'][0]->languages_id : null;

                $to_country                    = $trip_place['place']['city']->country;
                $to_country['image']           = (isset($to_country->medias[0]->media->path)) ? $to_country->medias[0]->media->path : url(PATTERN_PLACEHOLDERS);
                $to_country['title']           = isset($to_country->trans[0]->title) ? $to_country->trans[0]->title : null;
                $to_country['description']     = isset($to_country->trans[0]->description) ? $to_country->trans[0]->description : null;
                $to_country['languages_id']    = isset($to_country->trans[0]->languages_id) ? $to_country->trans[0]->languages_id : null;
                $to_country['flag']            = get_country_flag($to_country);
                $noOfFlags[] = $to_country['flag'];

                $to_city                    = $trip_place['city'];
                $to_city['image']           = (isset($to_city->medias[0]->media->path)) ? $to_city->medias[0]->media->path : url(PATTERN_PLACEHOLDERS);
                $to_city['title']           = isset($to_city->trans[0]->title) ? $to_city->trans[0]->title : null;
                $to_city['description']     = isset($to_city->trans[0]->description) ? $to_city->trans[0]->description : null;
                $to_city['languages_id']    = isset($to_city->trans[0]->languages_id) ? $to_city->trans[0]->languages_id : null;

                $toCountry                  = $trip_place['city']->country;
                $toCountry['image']         = (isset($toCountry->medias[0]->media->path)) ? $toCountry->medias[0]->media->path : url(PATTERN_PLACEHOLDERS);
                $toCountry['title']         = isset($toCountry->trans[0]->title) ? $toCountry->trans[0]->title : null;
                $toCountry['description']   = isset($toCountry->trans[0]->description) ? $toCountry->trans[0]->description : null;
                $toCountry['languages_id']  = isset($toCountry->trans[0]->languages_id) ? $toCountry->trans[0]->languages_id : null;
                $toCountry['flag']          = get_country_flag($toCountry);

                unset(
                    $trip_place['place']['medias'],
                    $trip_place['place']['transsingle'],
                    $trip_place['place']['city']['trans'],
                    $trip_place['city']['trans'],
                    $trip_place['city']->country['trans'],
                    $trip_place['place']['city']->country['trans'],
                    $trip_place['city']->country->medias,
                    $trip_place['place']['city']->country->medias,
                    $trip_place['place']['city']->medias,
                    $trip_place['city']->medias
                );
                $dummyPlaces[] = $place;
            }

            $dummy_trip_places[] = [
                'date'      => $date,
                'places'    => $dummyPlaces,
            ];;
        }

        $contents['trip_places'] = $dummy_trip_places;
        $contents['flags'] = array_values(array_unique($noOfFlags));
        unset($contents['not_approved_suggestions']);
        return $contents;
    }

    /**
     * @param integer $trip_comment_id
     * @return object
     */
    public function _construct_comment($trip_comment_id)
    {
        $trip_comment = TripsComments::find($trip_comment_id);
        $data['profile_picture'] = check_profile_picture($trip_comment->author->profile_picture);
        $data['author_id'] = url("profile/" . $trip_comment->author->id);
        $data['created_at'] = diffForHumans($trip_comment->created_at);
        $data['author'] = $trip_comment;

        return $data;
    }

    function getFlatInvitedPeopleList(TripInvitationsService $tripInvitationsService, $trip_id)
    {
        $invite_people_list = $tripInvitationsService->getPeopleToInvite("", $trip_id);
        $invitedRequests    = TripsContributionRequests::query()
            ->select('notes', 'role', 'users_id', 'status', 'count', 'updated_at')
            ->where('plans_id', $trip_id)
            ->get();


        $invitedRes = [];
        foreach ($invitedRequests as $item) {
            $invitedRes[$item->users_id] = [
                'status'    => $item->status,
                'count'     => $item->count,
            ];
        }

        $finalInvitedPeopleList = [];

        foreach ($invite_people_list as $groupName => $group) {
            if (is_array($group)) {
                $j = 0;
                foreach ($group as $user) {
                    if (array_key_exists($user['id'], $invitedRes)) {
                        unset(
                            $invite_people_list[$groupName][$j]['invited'],
                            $invite_people_list[$groupName][$j]['timeout']
                        );
                        if ($invitedRes[$user['id']]['status'] !== 1) {
                            unset($invite_people_list[$groupName][$j]);
                        } else {
                            $invite_people_list[$groupName][$j]['user_type'] = $groupName;
                            $finalInvitedPeopleList[] = $invite_people_list[$groupName][$j];
                        }
                    } else {
                        unset($invite_people_list[$groupName][$j]);
                    }
                    $j++;
                }
            }
        }
        return $finalInvitedPeopleList;
    }


    // Media API || Media API || Media API || Media API || Media API || Media API || Trip Place API 
    /**
     * @OA\Post(
     ** path="/trip/{trip_id}/media",
     *   tags={"Trip Place"},
     *   summary="This Api used to upload trip related media. like image, video",
     *   operationId="app\Http\Controllers\Api\TripController@postUploadMedia",
     *   security={
     *       {"bearer_token": {}
     *    }},
     * @OA\Parameter(
     *        name="trip_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="file",
     *                  type="string",
     *                  format="binary"
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function postUploadMedia(UploadMediaRequest $request, RankingService $rankingService, TripInvitationsService $tripInvitationsService, $trip_id)
    {
        try {
            $base_trip_path = S3_BASE_URL . 'trips/';
            $media_file     = $request->file('file');
            $place_id       = $request->get('place_id');
            $tripPlaceId    = $request->get('trip_place_id');
            $user_id        = Auth::user()->id;

            if (!in_array($tripInvitationsService->getUserRole($trip_id, auth()->id()), [
                TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN],
                TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
            ])) {
                return ApiResponse::__createUnAuthorizedResponse('only trip creator and trip member(admin or editor) can edit trip.');
            }

            $media_file_url = '';
            $thumb_file_url = null;

            if ($media_file) {
                $filename = time() . md5($media_file->getClientOriginalName()) . '.' . $media_file->getClientOriginalExtension();

                $type = Media::TYPE_IMAGE;

                Storage::disk('s3')->putFileAs('trips/' . $trip_id . '/', $media_file, $filename, 'public');
                $media_file_url = $base_trip_path . $trip_id . '/' . $filename;

                if (strpos($media_file->getMimeType(), 'video/') !== false) {
                    $type = Media::TYPE_VIDEO;
                    $ffmpeg = \FFMpeg\FFMpeg::create();

                    $video = $ffmpeg->open($media_file->getRealPath());

                    $duration = @$video->getStreams()->videos()->first()->get('duration');

                    if ($duration) {
                        $frame = $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds($duration * 0.01));
                        $name = bin2hex(openssl_random_pseudo_bytes(10)) . '.png';
                        $path = storage_path() . '/' . $name;
                        $frame->save($path);

                        Storage::disk('s3')->putFileAs(
                            'trips/' . $trip_id . '/thumb/',
                            new UploadedFile($path, $name),
                            $name,
                            'public'
                        );

                        $thumb_file_url = $base_trip_path . $trip_id . '/thumb/' . $name;

                        unlink($path);
                    }
                } else {
                    // $originalImg = Storage::disk('s3')->get('trips/' . $trip_id . '/' . $filename);

                    $cropped = \Intervention\Image\ImageManagerStatic::make($media_file)->orientate()->resize(180, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    Storage::disk('s3')->put(
                        'trips/' . $trip_id . '/th180/' . $filename,
                        $cropped->encode(),
                        'public'
                    );

                    $thumb_file_url = S3_BASE_URL . 'trips/' . $trip_id . '/th180/' . $filename;
                }

                $trip_media = new Media();
                $trip_media->url = $media_file_url;
                $trip_media->type = $type;
                $trip_media->source_url = $thumb_file_url;
                $trip_media->users_id = $user_id;
                $trip_media->type  = getMediaTypeByMediaUrl($trip_media->url);

                if ($type === Media::TYPE_VIDEO) {
                    $trip_media->video_thumbnail_url = $thumb_file_url;
                }


                if (!$trip_media->save()) {
                    $rankingService->addPointsEarners($trip_media->id, get_class($trip_media), $user_id);
                    return ApiResponse::create([
                        'message' => ['media can not save'],
                    ], false, ApiResponse::BAD_REQUEST);
                }
            } else {
                return ApiResponse::create([
                    'message' => ['media not found'],
                ], false, ApiResponse::BAD_REQUEST);
            }

            return ApiResponse::create([
                'url'   => $thumb_file_url,
                'id'    => $trip_media->getKey()
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param UploadMediaRequest $request
     * @return ApiResponse
     * 
     * @OA\Delete(
     ** path="/trip/{trip_id}/media",
     *   tags={"Trip Place"},
     *   summary="This Api used to remove temporary uploaded media. like image, video",
     *   operationId="app\Http\Controllers\Api\TripController@deleteTripPlaceMedia",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="trip_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="media_id",
     *        description="media_id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param DeleteTripPlaceMediaRequest $request
     * @return ApiResponse
     */
    public function deleteTripPlaceMedia(DeleteTripPlaceMediaRequest $request, $trip_id)
    {
        try {
            $mediaId = $request->get('media_id');
            $media = Media::find($mediaId);
            if (!$media) {
                return ApiResponse::create([
                    'message' => ['Media Not Found']
                ], false, ApiResponse::BAD_REQUEST);
            }
            $media->delete();
            return ApiResponse::create(['message' => ["delete successfully"]]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\POST(
     ** path="/trip/{trip_id}/place/{place_id}/discard",
     *   tags={"Trip Suggestion"},
     *   summary="Discard any suggestions from review popup",
     *   operationId="Api\TripController@postAjaxDiscardPlaceFromTrip",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="trip_id",
     *        description="trip id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="place_id",
     *        description="trip place id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function postAjaxDiscardPlaceFromTrip(TripsService $TripsService, TripInvitationsService $tripInvitationsService, TripsSuggestionsService $tripsSuggestionsService, $trip_id, $place_id)
    {
        // validate request
        $trip = $TripsService->findPlan($trip_id);
        if (!$trip) return ApiResponse::__createBadResponse('Trip not found');

        $tripPlace = TripPlaces::find($place_id);
        if (!$tripPlace) return ApiResponse::__createBadResponse('Trip place not found');

        //todo
        if ($tripsSuggestionsService->discardSuggestedPlace($trip_id, $place_id)) {
            // timeline
            $timeline = $this->getPlanContentsApi(true, $trip_id, auth()->id(), $tripsSuggestionsService);
            $timeline['invited_people'] = $this->getFlatInvitedPeopleList($tripInvitationsService, $trip_id);
            unset($timeline['is_edited'], $timeline['updated_places'], $timeline['unread_logs_count']);

            return ApiResponse::create([
                "message" => ["Discard successfully."],
                "timeline" => $timeline
            ]);
        }

        return ApiResponse::__createBadResponse('Place can not discard, please try again');
    }


    /**
     * @param integer $id
     * @param TripRequest $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param TripInvitationsService $TripInvitationsService
     * @return ApiResponse
     */
    // public function getCreateTrip($id, TripRequest $request, TripsSuggestionsService $tripsSuggestionsService, TripInvitationsService $tripInvitationsService)
    // {
    //     try {
    //         //code...$data = array();
    //         $add_city_action = true;
    //         $data['trip_id'] = $id;
    //         if ($request->has('memory') && $request->get('memory') == 1) {
    //             $data['memory'] = 1;
    //         }
    //         $data['me'] = Auth::user();
    //         $data['me_user'] = User::find($data['me']->id);

    //         $data['is_editor'] = 0;
    //         $data['open_affiliate_hint'] = 0;
    //         $data['is_admin'] = 0;
    //         $data['editable'] = 0;
    //         $data['is_invited'] = false;

    //         if ($id == 0) {
    //             $data['goto_first_step'] = true;
    //             $data['my_version_id'] = 1;
    //             $data['im_invited'] = false;
    //         } else {
    //             $data['trip'] = TripPlans::find($id);

    //             $invitedRequest = $tripsSuggestionsService->getInvitedUserRequest($id, auth()->id());
    //             if ($data['trip'] && $data['trip']->privacy === 2 && $data['trip']->users_id !== auth()->id()) {
    //                 if (!$invitedRequest) {
    //                     return ApiResponse::create(
    //                         [
    //                             "message" => ["No permissions"]
    //                         ],
    //                         false,
    //                         ApiResponse::BAD_REQUEST
    //                     );
    //                 }

    //                 if (($invitedRequest->role !== 'editor' && $invitedRequest->role !== 'admin') && $request->has('do') && $request->get('do') == 'edit') {
    //                     return ApiResponse::create([
    //                         'trip' => $data['trip']
    //                     ]);
    //                 }
    //             } elseif ($data['trip'] && $data['trip']->privacy === 0 && $data['trip']->users_id !== auth()->id() && $request->has('do') && $request->get('do') == 'edit') {
    //                 if (!$invitedRequest || ($invitedRequest->role !== 'editor' && $invitedRequest->role !== 'admin')) {
    //                     return ApiResponse::create([
    //                         'trip' => $data['trip']
    //                     ]);
    //                 }
    //             } elseif ($data['trip'] && $data['trip']->privacy === 1 && $data['trip']->users_id !== auth()->id()) {
    //                 if (!in_array($data['trip']->users_id, $tripInvitationsService->findFriends()->toArray())) {
    //                     return ApiResponse::create(
    //                         [
    //                             "message" =>  ["No permissions"]
    //                         ],
    //                         false,
    //                         ApiResponse::BAD_REQUEST
    //                     );
    //                 }
    //             }

    //             $data['show_draft_buttons'] = $tripsSuggestionsService->isUnsavedChanges($id);

    //             $data['is_admin'] = @$data['trip']->users_id === auth()->id() || ($invitedRequest && $invitedRequest->role === 'admin');
    //             $data['is_editor'] = $invitedRequest && $invitedRequest->role === 'editor';
    //             $data['is_invited'] = $invitedRequest ? true : false;

    //             $my_version_id = @$data['trip']->versions[0]->id;
    //             if (Auth::user()->id != @$data['trip']->author->id) {
    //                 $data['im_invited'] = true;
    //             } else {
    //                 $data['im_invited'] = false;
    //             }

    //             $data['my_version_id'] = $my_version_id;

    //             $trip_places = TripPlaces::where('trips_id', $id)
    //                 ->where('versions_id', $my_version_id)
    //                 ->orderBy('time', 'ASC')
    //                 ->get();

    //             $ftrip_places = array();
    //             $map_places = array();
    //             $trip_places_arr = array();
    //             foreach ($trip_places as $tp) {
    //                 $ftrip_places[$tp->date][] = $tp;
    //                 $map_places[@$tp->city->id . '-place-' . @$tp->place->id] = array('lat' => @$tp->place->lat, 'lng' => @$tp->place->lng);
    //                 $map_places[@$tp->city->id . '-place-' . @$tp->place->id]['time'] = $tp->time;

    //                 $trip_places_arr[$tp->place->id] = $tp->place;
    //                 $trip_places_arr[$tp->place->id]['image'] = check_place_photo($tp->place);
    //                 $trip_places_arr[$tp->place->id]['time'] = $tp->time;
    //                 $trip_places_arr[$tp->place->id]['trip_place_id'] = $tp->id;
    //                 $trip_places_arr[$tp->place->id]['name'] = $tp->place->transsingle->title;
    //                 $trip_places_arr[$tp->place->id]['city_name'] = (@$tp->place->city->trans) ? @$tp->place->city->trans->first()->title : '';

    //                 if (!isset($trip_places_arr[$tp->place->id]['count'])) {
    //                     $trip_places_arr[$tp->place->id]['count'] = 0;
    //                 }

    //                 $trip_places_arr[$tp->place->id]['count'] = $trip_places_arr[$tp->place->id]['count'] + 1;
    //             }

    //             $data['trip_places'] = $ftrip_places;

    //             if ($request->has('do') && $request->get('do') == 'edit') {
    //                 $data['do'] = 'edit';

    //                 if ($invitedRequest && $invitedRequest->user->expert) {
    //                     $data['open_affiliate_hint'] = 1;
    //                 }

    //                 $planSuggestions = $tripsSuggestionsService->getEditPlanSuggestions($id);

    //                 foreach ($planSuggestions as $planSuggestion) {
    //                     $meta = $planSuggestion->meta;
    //                     $data['trip']->privacy = Arr::get($meta, 'privacy');
    //                     $data['trip']->description = Arr::get($meta, 'description');
    //                     $data['trip']->title = Arr::get($meta, 'name');
    //                 }

    //                 $tripSuggestions = $tripsSuggestionsService->getPlanSuggestions($id);

    //                 foreach ($tripSuggestions as $tripSuggestion) {
    //                     switch ($tripSuggestion->type) {
    //                         case TripsSuggestionsService::SUGGESTION_CREATE_TYPE:
    //                             if (!$tripSuggestion->suggestedPlace) {
    //                                 continue 2;
    //                             }

    //                             $trip_places_arr[$tripSuggestion->suggestedPlace->place->id] = $tripSuggestion->suggestedPlace->place;
    //                             $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['image'] = check_place_photo($tripSuggestion->suggestedPlace->place);
    //                             $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['time'] = $tripSuggestion->suggestedPlace->time;
    //                             $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['trip_place_id'] = $tripSuggestion->suggestedPlace->id;
    //                             $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['name'] = $tripSuggestion->suggestedPlace->place->transsingle->title;
    //                             $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['city_name'] = $tripSuggestion->suggestedPlace->place->city->trans->first()->title;

    //                             if (!isset($trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['count'])) {
    //                                 $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['count'] = 0;
    //                             }

    //                             $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['count'] = $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['count'] + 1;

    //                             $map_places[@$tripSuggestion->suggestedPlace->city->id . '-place-' . @$tripSuggestion->suggestedPlace->place->id] = array('lat' => @$tripSuggestion->suggestedPlace->place->lat, 'lng' => @$tripSuggestion->suggestedPlace->place->lng);
    //                             $map_places[@$tripSuggestion->suggestedPlace->city->id . '-place-' . @$tripSuggestion->suggestedPlace->place->id]['time'] = $tripSuggestion->suggestedPlace->time;
    //                             break;
    //                         case TripsSuggestionsService::SUGGESTION_EDIT_TYPE:
    //                             if (!$tripSuggestion->activePlace) {
    //                                 continue 2;
    //                             }

    //                             $trip_places_arr[$tripSuggestion->activePlace->place->id] = $tripSuggestion->suggestedPlace->place;
    //                             $trip_places_arr[$tripSuggestion->activePlace->place->id]['image'] = check_place_photo($tripSuggestion->suggestedPlace->place);
    //                             $trip_places_arr[$tripSuggestion->activePlace->place->id]['time'] = $tripSuggestion->suggestedPlace->time;
    //                             $trip_places_arr[$tripSuggestion->activePlace->place->id]['trip_place_id'] = $tripSuggestion->suggestedPlace->id;
    //                             $trip_places_arr[$tripSuggestion->activePlace->place->id]['name'] = $tripSuggestion->suggestedPlace->place->transsingle->title;
    //                             $trip_places_arr[$tripSuggestion->activePlace->place->id]['city_name'] = $tripSuggestion->suggestedPlace->place->city->trans->first()->title;
    //                             $map_places[@$tripSuggestion->activePlace->city->id . '-place-' . @$tripSuggestion->activePlace->place->id] = array('lat' => @$tripSuggestion->activePlace->place->lat, 'lng' => @$tripSuggestion->activePlace->place->lng);
    //                             $map_places[@$tripSuggestion->activePlace->city->id . '-place-' . @$tripSuggestion->activePlace->place->id]['time'] = $tripSuggestion->suggestedPlace->time;
    //                             break;
    //                         case TripsSuggestionsService::SUGGESTION_DELETE_TYPE:
    //                             if (!$tripSuggestion->suggestedPlace) {
    //                                 continue 2;
    //                             }

    //                             if (!isset($trip_places_arr[$tripSuggestion->suggestedPlace->place->id])) {
    //                                 $trip_places_arr[$tripSuggestion->suggestedPlace->place->id] = $tripSuggestion->suggestedPlace->place;
    //                                 $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['count'] = 1;
    //                             }

    //                             $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['count'] = $trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['count'] - 1;

    //                             if ($trip_places_arr[$tripSuggestion->suggestedPlace->place->id]['count'] === 0) {
    //                                 unset($trip_places_arr[$tripSuggestion->suggestedPlace->place->id]);
    //                                 unset($map_places[$tripSuggestion->suggestedPlace->city->id . '-place-' . @$tripSuggestion->suggestedPlace->place->id]);
    //                             }

    //                             break;
    //                         default:
    //                             break;
    //                     }
    //                 }

    //                 $data['editable'] = 1;
    //             }
    //             $trip_places_arr = collect(array_values($trip_places_arr))->sortBy('time')->values()->all();
    //             $map_places = collect($map_places)->sortBy('time')->toArray();

    //             $data['trip_places_arr'] = $trip_places_arr;
    //             $data['trip_arr'] = $map_places;
    //         }

    //         if ($request->has('do')) {
    //             $data['do'] = $request->get('do');
    //             if ($data['do'] == "add-city") {
    //                 $add_city_action = false;
    //             } elseif ($data['do'] == "add-place" && $request->has('city_id')) {
    //             }
    //         }

    //         if ($request->has('city_id')) {
    //             $data['city'] = Cities::find($request->get('city_id'));

    //             $data['place_suggestions'] = PlacesTop::where('city_id', $request->get('city_id'))
    //                 ->take(10)
    //                 ->get();
    //         } elseif (isset($my_version_id)) {
    //             $last_city = TripCities::where('trips_id', $id)
    //                 ->where('versions_id', $my_version_id)
    //                 ->orderBy('id', 'ASC')
    //                 ->get()
    //                 ->first();
    //             if (isset($last_city->city))
    //                 $data['city'] = $last_city->city;
    //         }

    //         $data['city_suggestions'] = TripCities::groupBy('cities_id')
    //             ->select('*', DB::raw('count(*) as total'))
    //             ->take(10)
    //             ->get();

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }
}
