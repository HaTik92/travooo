<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Auth;
use DB;
use App\Http\Responses\ApiResponse;
use App\Models\TripPlans\TripsContributionRequests;

use App\Services\TravelMates\TravelMatesPlansService;
use App\Services\TravelMates\TravelMatesRequestsService;
use App\Services\TravelMates\TravelMatesService;
use App\Services\Trips\TripInvitationsService;

use App\Http\Requests\Api\TravelMates\FilterTravelMatesRequest;
use App\Http\Requests\Api\TravelMates\DoTravelMatesRequest;
use App\Http\Requests\Api\TravelMates\PostRequest;
use App\Http\Requests\Api\TravelMates\RemoveMateRequest;
use App\Http\Requests\Api\TravelMates\CancelInvitationRequest;
use App\Http\Requests\Api\TravelMates\CloseRequest;
use App\Http\Requests\Api\TravelMates\ApproveRequest;
use App\Http\Requests\Api\TravelMates\CreateWithoutPlanRequest;
use App\Http\Requests\Api\TravelMates\CancelAllInvitationsRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\Country\ApiCountry as Country;
use App\Models\City\Cities;
use App\Models\TravelMates\TravelMatesRequestUsers;
use App\Models\TravelMates\TravelMatesWithoutPlan;

use App\Models\TripPlans\TripPlans;
use App\Models\Country\Countries;

class TravelMatesController extends Controller
{

    /**
     * @return \Illuminate\Http\Response
     */
    public function getIndexNew()
    {
        try {
            $data = [];
            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->where('active', 1)->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @param TravelMatesService $travelMatesService
     * @param TravelMatesPlansService $travelMatesPlansService
     * @param TravelMatesRequestsService $travelMatesRequestsService
     * @param TripInvitationsService $tripInvitationsService
     * @return \Illuminate\Http\Response
     */
    public function getIndexNewFunc(Request $request, TravelMatesService $travelMatesService, TravelMatesPlansService $travelMatesPlansService, TravelMatesRequestsService $travelMatesRequestsService, TripInvitationsService $tripInvitationsService)
    {
        try {
            $adminForPlans = $tripInvitationsService->getInvitedUserPlanIds(Auth::id(), 'admin');

            $data['popularTravelMates'] = $travelMatesService->getPopularTravelMates();
            $data['my_plans'] = TripPlans::where(function ($query) use ($adminForPlans) {
                $query->where('users_id', Auth::user()->id)
                    ->orWhereIn('id', $adminForPlans);
            })->whereHas('version', function ($versionQuery) {
                $versionQuery->where('start_date', '>=', date('Y-m-d'));
            })->where('active', 1)->orderBy('created_at', 'DESC')->get();

            $data['plan'] = [];
            $data['without_plan'] = [];
            $data['mates'] = [];
            $data['request_status'] = [];
            $count_my_plans = 0;

            $requests_plan = $travelMatesRequestsService->getTravelMatesRequests(1, '', 10, []);
            $requests_without_plan = $travelMatesRequestsService->getTravelMatesRequests(2, '', 10, []);

            foreach ($requests_plan['items']  as $trave_mate) {
                $data['plan'][] = $trave_mate->plan;
                $data['request_status'][$trave_mate->plans_id] = $trave_mate->going()->where('users_id', Auth::id())->count() ? $trave_mate->going()->where('users_id', Auth::id())->first()->status : 3;
                $data['mates'][$trave_mate->plans_id] = $trave_mate->id;
            }

            $data['invited_plan_id'] = [];

            foreach ($requests_without_plan['items']  as $trave_mate_wplan) {
                $data['without_plan'][] = $trave_mate_wplan->without_plan;
                $data['mates'][$trave_mate_wplan->without_plans_id] = $trave_mate_wplan->id;

                $status = 3;

                $inviteRequest =  TripsContributionRequests::where([
                    'authors_id' => auth()->id(),
                    'users_id' => $trave_mate_wplan->users_id,
                    'role' => 'travel-mate',
                    'w_plans_id' => $trave_mate_wplan->without_plans_id
                ])->first();

                if ($inviteRequest) {
                    $status = $inviteRequest->status;
                    $data['invited_plan_id'][$trave_mate_wplan->without_plans_id] = $inviteRequest->plans_id;
                }

                $data['status'][$trave_mate_wplan->without_plans_id] = $status;
            }

            $now_date = Carbon::now();
            $my_plans_requests = TravelMatesRequests::whereNotNull('plans_id')
                ->whereHas('plan', function ($planQuery) use ($now_date) {
                    $planQuery->whereHas('version', function ($versionQuery) use ($now_date) {
                        $versionQuery->where('authors_id', Auth::user()->id);
                        $versionQuery->where('end_date', '>', $now_date);
                    });
                })
                ->where('users_id', Auth::user()->id)
                ->where('status', '!=', -1)
                ->get();


            if (count($my_plans_requests) == 0) {
                $count_my_plans++;
            }

            $my_without_plans_requests = TravelMatesRequests::whereNotNull('without_plans_id')
                ->whereHas('without_plan', function ($wpQuery) use ($now_date) {
                    $wpQuery->where('end_date', '>', $now_date);
                })
                ->where('users_id', Auth::user()->id)
                ->where('status', '!=', -1)
                ->get();

            if (count($my_without_plans_requests) == 0) {
                $count_my_plans++;
            }

            $data['count_my_plans'] = $count_my_plans;

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param FilterTravelMatesRequest $request
     * @param TravelMatesPlansService $travelMatesPlansService
     * @param TravelMatesRequestsService $travelMatesRequestsService
     * @return \Illuminate\Http\Response
     */
    public function getTravelMates(FilterTravelMatesRequest $request, TravelMatesPlansService $travelMatesPlansService, TravelMatesRequestsService $travelMatesRequestsService)
    {
        try {
            $page = $request->pagenum;
            $skip = ($page - 1) * 10;
            $filters = $request->input('filters', []);
            $type = $request->type;
            $order_filter = $request->order_filter;

            $count = 0;
            $requests = $travelMatesRequestsService->getTravelMatesRequests($type, $skip, 10, $filters, $order_filter);
            $count = $requests['total'];
            $data = [];
            $return = [];
            foreach ($requests['items'] as $request_item) {
                if ($type == 1) {
                    $data['mates'][$request_item->plans_id] = $request_item->id;
                    $data['request_status'][$request_item->plans_id] = $request_item->going()->where('users_id', Auth::id())->count() ? $request_item->going()->where('users_id', Auth::id())->first()->status : 3;
                    $return[] = array('plan' => $request_item->plan, 'mates' => $data['mates'], 'request_status' => $data['request_status']);
                }

                if ($type == 2) {
                    $data['invited_plan_id'] = [];
                    $status = 3;

                    $inviteRequest =  TripsContributionRequests::where([
                        'authors_id' => auth()->id(),
                        'users_id' => $request_item->users_id,
                        'role' => 'travel-mate',
                        'w_plans_id' => $request_item->without_plans_id
                    ])->first();

                    if ($inviteRequest) {
                        $status = $inviteRequest->status;
                        $data['invited_plan_id'][$request_item->without_plans_id] = $inviteRequest->plans_id;
                    }

                    $data['status'][$request_item->without_plans_id] = $status;
                    $data['mates'][$request_item->without_plans_id] = $request_item->id;
                    $return[] = array('invited_plan_id' => $data['invited_plan_id'], 'plan' => $request_item->without_plan, 'mates' => $data['mates'], 'status' => $data['status']);
                }
            }

            return ApiResponse::create(['count' => $count, 'travel_mates' => $return]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getFilters(Request $request)
    {
        try {
            $query = $request->get('q');
            if ($query) {
                $destinations = array();
                $queryParam = $query;
                $get_countries = Country::whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })->take(20)->get();
                foreach ($get_countries as $c) {
                    $destinations[] = [
                        'selected_id' => $c->id . '-country',
                        'id' => $c->id,
                        'text' => $c->transsingle->title,
                        'image' => check_country_photo(@$c->getMedias[0]->url, 180),
                        'query' => $query,
                        'country_id' => @$c->country->id,
                        'country_name' => @$c->country->transsingle->title
                    ];
                }

                $get_cities = Cities::whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })->take(20)->get();
                foreach ($get_cities as $city) {
                    $destinations[] = [
                        'selected_id' => $city->id . '-city',
                        'id' => $city->id,
                        'text' => $city->transsingle->title,
                        'image' => check_country_photo(@$city->getMedias[0]->url, 180),
                        'country_id' => @$city->country->id,
                        'country_name' => @$city->country->transsingle->title,
                        'query' => $query
                    ];
                }

                return ApiResponse::create($destinations);
            } else {
                return ApiResponse::create(
                    [
                        "message" => ["Search Term not provided"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @param TravelMatesService $travelMatesService
     * @param TravelMatesPlansService $travelMatesPlansService
     * @param TravelMatesRequestsService $travelMatesRequestsService
     * @return \Illuminate\Http\Response
     */
    public function getOrderFilter(Request $request, TravelMatesService $travelMatesService, TravelMatesPlansService $travelMatesPlansService, TravelMatesRequestsService $travelMatesRequestsService)
    {
        try {
            $data['plan'] = [];
            $data['without_plan'] = [];
            $data['mates'] = [];
            $data['request_status'] = [];

            $order_filter = $request->order_filter;

            $requests_plan = $travelMatesRequestsService->getTravelMatesRequests(1, '', 10, [], $order_filter);
            $requests_without_plan = $travelMatesRequestsService->getTravelMatesRequests(2, '', 10, [], $order_filter);

            foreach ($requests_plan['items']  as $trave_mate) {
                $data['plan'][] = $trave_mate->plan;
                $data['request_status'][$trave_mate->plans_id] = $trave_mate->going()->where('users_id', Auth::id())->count() ? $trave_mate->going()->where('users_id', Auth::id())->first()->status : 3;
                $data['mates'][$trave_mate->plans_id] = $trave_mate->id;
            }

            $data['invited_plan_id'] = [];

            foreach ($requests_without_plan['items']  as $trave_mate_wplan) {
                $data['without_plan'][] = $trave_mate_wplan->without_plan;
                $data['mates'][$trave_mate_wplan->without_plans_id] = $trave_mate_wplan->id;

                $status = 3;

                $inviteRequest =  TripsContributionRequests::where([
                    'authors_id' => auth()->id(),
                    'users_id' => $trave_mate_wplan->users_id,
                    'role' => 'travel-mate'
                ])->first();

                if ($inviteRequest) {
                    $status = $inviteRequest->status;
                    $data['invited_plan_id'][$trave_mate_wplan->without_plans_id] = $inviteRequest->plans_id;
                }

                $data['status'][$trave_mate_wplan->without_plans_id] = $status;
            }

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getNewest()
    {
        try {
            $data['plans'] = TripPlans::with('versions')
                ->where('users_id', Auth::user()->id)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })
                ->get();

            $data['countries'] = \App\Models\Country\Countries::all();
            $data['cities'] = \App\Models\City\Cities::all();

            $data['requests'] = TravelMatesRequests::with('versions')
                ->where('status', '>', 0)
                ->orderBy('created_at', 'desc')->take(12)->get();

            $data['trending_plan'] = TravelMatesRequests::with('plan')
                ->where('status', '>', 0)
                ->whereHas('plan', function ($q) {
                    //$q->where('start_date', '>', date("Y-m-d", time()));
                    //$q->where('end_date', '>', date("Y-m-d", time()));
                    //$q->where('active', '=', 1);
                })
                ->get()->first();

            //dd($data['trending_plan']);

            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })->where('active', 1)->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getTrending()
    {
        try {
            $data['plans'] = TripPlans::with('versions')
                ->where('users_id', Auth::user()->id)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })
                ->get();
            $data['countries'] = \App\Models\Country\Countries::all();
            $data['cities'] = \App\Models\City\Cities::all();

            $data['requests'] = TravelMatesRequests::with('versions')
                ->where('status', '>', 0)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                    $q->where('end_date', '>', date("Y-m-d", time()));
                })
                ->get()
                ->sortBy(function ($request) {
                    return $request->plan->start_date;
                });

            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })->where('active', 1)->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getSoon()
    {
        try {
            $data['plans'] = TripPlans::with('versions')
                ->where('users_id', Auth::user()->id)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })
                ->get();
            $data['countries'] = \App\Models\Country\Countries::all();
            $data['cities'] = \App\Models\City\Cities::all();

            $data['requests'] = TravelMatesRequests::with('versions')
                ->where('status', '>', 0)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })
                ->get()
                ->sortBy(function ($request) {
                    return $request->plan->start_date;
                });

            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })->where('active', 1)->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getNotificationsInvitations()
    {
        try {
            $data['invitations'] = TripsContributionRequests::where([
                'status' => TripInvitationsService::STATUS_PENDING,
                'authors_id' => Auth::user()->id
            ])->whereNotNull('w_plans_id')->orderByDesc('created_at')->take(10)->get()->unique('plans_id');


            $data['sent_requests'] = TravelMatesRequests::select('travelmate_requests.*')
                ->leftJoin('travelmate_request_users as tru', 'tru.requests_id', '=', 'travelmate_requests.id')
                ->whereNotNull('travelmate_requests.plans_id')
                ->where('tru.users_id', Auth::user()->id)
                ->where('tru.status', '!=', 2)
                ->orderBy('tru.id', 'DESC')
                ->take(10)->get();

            $data['active_tab'] = "invitations";

            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })->where('active', 1)->orderBy('created_at', 'DESC')->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getNotificationsRequests()
    {
        try {
            $data['join_with_plans_requests'] = TravelMatesRequests::with('going')
                ->whereNotNull('plans_id')
                ->where('users_id', Auth::user()->id)
                ->orderBy('id', 'DESC')
                ->take(10)->get();

            $data['join_without_plans_requests'] = TravelMatesRequests::with('going')
                ->whereNotNull('without_plans_id')
                ->where('users_id', Auth::user()->id)
                ->orderBy('id', 'DESC')
                ->take(10)->get();
            $data['active_tab'] = "requests";
            $data['my_plans'] = [];
            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })->where('active', 1)->orderBy('created_at', 'DESC')->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function joinWithPlansRequests(Request $request)
    {
        try {
            $page = $request->pagenum;
            $skip = ($page - 1) * 10;

            $join_with_plans_requests = TravelMatesRequests::with('going')
                ->whereNotNull('plans_id')
                ->where('users_id', Auth::user()->id)
                ->orderBy('id', 'DESC')
                ->skip($skip)->take(10)->get();

            return ApiResponse::create(['join_with_plans_requests' => $join_with_plans_requests]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function joinWithoutPlansRequests(Request $request)
    {
        try {
            $page = $request->pagenum;
            $skip = ($page - 1) * 10;

            $join_without_plans_requests = TravelMatesRequests::with('going')
                ->whereNotNull('without_plans_id')
                ->where('users_id', Auth::user()->id)
                ->orderBy('id', 'DESC')
                ->skip($skip)->take(10)->get();

            return ApiResponse::create(
                [
                    'join_without_plans_requests' => $join_without_plans_requests
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getApprovedRequests()
    {
        try {
            $data['invitations'] = TravelMatesRequestUsers::where('users_id', Auth::user()->id)
                ->where('status', '=', 1)
                ->orderBy('created_at', 'DESC')
                ->get();

            $data['active_tab'] = "approved_requests";

            //dd($data['invitations']);
            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->where('active', 1)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })->doesntHave('join_requests')->orderBy('created_at', 'DESC')->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getRequest()
    {
        try {
            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })->where('active', 1)->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postSearch(Request $request)
    {
        try {
            if ($request->has('actual_trip_start_date')) {
                $trip_start_date = $request->get('actual_trip_start_date');
            } else {
                $trip_start_date = null;
            }
            if ($request->has('actual_trip_end_date')) {
                $trip_end_date = $request->get('actual_trip_end_date');
            } else {
                $trip_end_date = null;
            }
            if ($request->has('destinations')) {
                $destinations = $request->get('destinations');
            } else {
                $destinations = null;
            }
            if ($request->has('origins')) {
                $origins = $request->get('origins');
            } else {
                $origins = null;
            }

            if ($request->has('gender')) {
                $gender = $request->get('gender');
            } else {
                $gender = null;
            }

            if ($request->has('age_range')) {
                $age_range = $request->get('age_range');
                $age_range_array = array();
                switch ($age_range) {
                    case "less20":
                        $age_range_array = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 1819);
                        break;

                    case "20":
                        $age_range_array = array(20, 21, 22, 23, 24);
                        break;

                    case "25":
                        $age_range_array = array(25, 26, 27, 28, 29);
                        break;

                    case "30":
                        $age_range_array = array(30, 31, 32, 33, 34);
                        break;

                    case "35":
                        $age_range_array = array(35, 36, 37, 38, 39);
                        break;

                    case "40":
                        $age_range_array = array(40, 41, 42, 43, 44, 45, 46, 47, 48, 49);
                        break;

                    case "50":
                        $age_range_array = array(50, 51, 52, 53, 54, 55, 56, 57, 58, 59);
                        break;

                    case "greater60":
                        $age_range_array = range(60, 120);
                        break;
                    default:
                }
            } else {
                $age_range_array = null;
            }


            $data['plans'] = TripPlans::with('versions')->where('users_id', Auth::user()->id)
                ->whereHas('versions', function ($q) {
                    $q->where('start_date', '>', date("Y-m-d", time()));
                })
                ->get();
            $data['countries'] = \App\Models\Country\Countries::all();
            $data['cities'] = \App\Models\City\Cities::all();


            if ($trip_start_date or $trip_end_date or $destinations) {

                $filter_plans_by_start_date = TripPlans::whereHas('versions', function ($q) use ($trip_start_date) {
                    $q->where('start_date', '>=', $trip_start_date);
                })
                    ->select(array('id'))
                    ->get();
                $filter_plans_by_start_date_final = array();
                foreach ($filter_plans_by_start_date as $fpbsd) {
                    $filter_plans_by_start_date_final[] = $fpbsd->id;
                }

                $filter_plans_by_end_date = TripPlans::whereHas('versions', function ($q) use ($trip_end_date) {
                    $q->where('end_date', '<=', $trip_end_date);
                })
                    ->select(array('id'))
                    ->get();
                $filter_plans_by_end_date_final = array();
                foreach ($filter_plans_by_end_date as $fpbed) {
                    $filter_plans_by_end_date_final[] = $fpbed->id;
                }

                $data['requests'] = TravelMatesRequests::with('author')->with('plan')->with('plan_country')
                    ->where('plans_id', '!=', 0);
                if ($filter_plans_by_start_date_final) {
                    $data['requests'] = $data['requests']->whereIn('plans_id', $filter_plans_by_start_date_final);
                }
                if ($filter_plans_by_end_date_final) {
                    $data['requests'] = $data['requests']->whereIn('plans_id', $filter_plans_by_end_date_final);
                }

                $data['requests'] = $data['requests']->where('status', '>', 0)
                    ->whereHas('plan_city', function ($q2) use ($destinations) {
                        if ($destinations) {
                            foreach ($destinations as $destination) {
                                $q2->where('cities_id', '=', $destination);
                            }
                        }
                    })->whereHas('author', function ($q3) use ($gender, $age_range_array, $origins) {
                        if ($gender)
                            $q3->where('gender', '=', $gender);
                        if ($age_range_array)
                            $q3->whereIn('age', $age_range_array);
                        if ($origins) {
                            $q3->where('nationality', '!=', '');
                            foreach ($origins as $origin) {
                                $q3->orWhere('nationality', $origin);
                            }
                        }
                    })->get()
                    ->sortBy(function ($request) {
                        return $request->plan->start_date;
                    });
            } else {
                $data['requests'] = TravelMatesRequests::with('author')->with('plan')
                    ->where('plans_id', '!=', 0)
                    ->where('status', '>', 0)
                    ->whereHas('author', function ($q) use ($gender, $age_range_array, $origins) {
                        if ($gender)
                            $q->where('gender', '=', $gender);
                        if ($age_range_array)
                            $q->whereIn('age', $age_range_array);
                        if ($origins) {
                            $q->whereIn('nationality', $origins);
                        }
                    })->get();
            }

            $data['requests_without_plans'] = TravelMatesRequests::whereNull('plans_id')
                ->where('status', '>', 0)
                ->orderBy('created_at', 'desc')
                ->get();

            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->where('active', 1)->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param DoTravelMatesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postDoRequest(DoTravelMatesRequest $request)
    {
        try {
            $plan_id = $request->get('plan_id');
            $user_id = Auth::user()->id;

            $travelMateRequest = new TravelMatesRequests;
            $travelMateRequest->users_id = $user_id;
            if ($plan_id) {
                $travelMateRequest->plans_id = $plan_id;

                if ($travelMateRequest->save()) {
                    log_user_activity('Travelmate', 'request', $travelMateRequest->id);
                    return ApiResponse::create([
                        'message' => ['Your Travel Mates request was added successfully.']
                    ]);
                } else {
                    return ApiResponse::create(
                        [
                            'message' => ['Travel Mates request failed! Please try again later.']
                        ],
                        false,
                        ApiResponse::BAD_REQUEST
                    );
                }
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Plaese select trip plan.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postJoin(PostRequest $request)
    {
        try {
            $request_id = $request->get('request_id');
            $user_id = Auth::user()->id;

            $joinRequest = new TravelMatesRequestUsers;
            $joinRequest->users_id = $user_id;
            $joinRequest->requests_id = $request_id;
            if ($joinRequest->save()) {
                log_user_activity('Travelmate', 'join', $request_id);
                return ApiResponse::create(
                    [
                        'message' => ['Your request to join Travel Mate was sent successfully.']
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Join request failed! Please try again later.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postTravelMatesJoin(PostRequest $request)
    {
        try {
            $request_id = $request->get('request_id');
            $user_id = Auth::user()->id;

            $user_joinRequest = TravelMatesRequestUsers::where('users_id', $user_id)->where('requests_id', $request_id)->get();
            $travMates = TravelMatesRequests::find($request_id);
            if (!$travMates) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Request Id']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $author_id = $travMates->users_id;
            if (count($user_joinRequest) > 0) {
                return ApiResponse::create(
                    [
                        'message' => [true]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            } else {
                if ($travMates->status != -1) {
                    $joinRequest = new TravelMatesRequestUsers();
                    $joinRequest->users_id = $user_id;
                    $joinRequest->requests_id = $request_id;
                    if ($joinRequest->save()) {
                        log_user_activity('Travelmate', 'join', $request_id);
                        if ($user_id != $author_id)
                            notify($author_id, 'ask_to_join', $request_id);

                        return ApiResponse::create(
                            [
                                'message' => ['Your request to join Travel Mate was sent successfully.']
                            ]
                        );
                    } else {
                        return ApiResponse::create(
                            [
                                'message' => ['Join request failed! Please try again later.']
                            ],
                            false,
                            ApiResponse::BAD_REQUEST
                        );
                    }
                } else {
                    return ApiResponse::create(
                        [
                            'message' => ['Request canceled']
                        ],
                        false,
                        ApiResponse::BAD_REQUEST
                    );
                }
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param RemoveMateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postRemoveMate(RemoveMateRequest $request)
    {
        try {
            $request_id = $request->get('request_id');
            $mate_id = $request->get('mate_id');
            $req = TravelMatesRequestUsers::where('requests_id', $request_id)->where('users_id', $mate_id);
            if ($req->delete()) {
                log_user_activity('Travelmate', 'remove_user', $mate_id);
                return ApiResponse::create(
                    [
                        'message' => ['Your request to Remove Travel Mate was sent successfully.']
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Remove request failed! Please try again later.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CancelInvitationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCancelInvitation(CancelInvitationRequest $request)
    {
        try {
            $request_id = $request->get('request_id');
            $req = TravelMatesRequests::find($request_id);
            if (!$req) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Request Id']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $req->status = '-1';
            if ($req->save()) {
                log_user_activity('Travelmate', 'cancel_invitation', $request_id);
                return ApiResponse::create(
                    [
                        'message' => ['Your request to Cancel Invitation was sent successfully.']
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Your request failed! Please try again later.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CloseRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCloseRequest(CloseRequest $request)
    {
        try {
            $request_id = $request->get('request_id');
            $req = TravelMatesRequests::find($request_id);
            $req->status = '-1';
            if ($req->save()) {
                log_user_activity('Travelmate', 'close_request', $request_id);
                return ApiResponse::create(
                    [
                        'message' => [true]
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Your request failed! Please try again later.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postUnpublishRequest(PostRequest $request)
    {
        try {
            $request_id = $request->get('request_id');
            $req = TravelMatesRequests::find($request_id);
            if (!$req) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Request Id']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            foreach ($req->going()->get() as $going) {
                $going->delete();
            }

            if (isset($request->type)) {
                foreach ($req->without_plan->plan_invitations()->get() as $invitation) {
                    unnotify(Auth::user()->id, 'plan_invitetion_accepted', $invitation->id);
                    unnotify(Auth::user()->id, 'plan_invitetion_rejected', $invitation->id);
                    unnotify(Auth::user()->id, 'plan_invitation_received', $invitation->id);

                    notify($invitation->authors_id, 'plan_invitetion_unpublished', $invitation->id, 'travel-mate');
                    $invitation->delete();
                }

                $req->without_plan->delete();
            }
            if ($req->delete()) {
                return ApiResponse::create(
                    [
                        'message' => [true]
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Your request failed! Please try again later.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCancelRequest(PostRequest $request)
    {
        try {
            $request_id = $request->get('request_id');
            $req = TravelMatesRequestUsers::where('requests_id', $request_id)->where('users_id', Auth::user()->id)->first();
            if ($req && $req->delete()) {
                return ApiResponse::create(
                    [
                        'message' => [true]
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Your request failed! Please try again later.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param ApproveRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postApproveRequest(ApproveRequest $request)
    {
        try {
            $request_id = $request->request_id;
            $mates_id = $request->mates_id;
            $req = TravelMatesRequestUsers::where('requests_id', $request_id)->where('users_id', $mates_id)->first();
            if (!$req) {
                return ApiResponse::create(
                    [
                        'message' => ['User Not Found']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $req->status = '1';
            if ($req->save()) {
                log_user_activity('Travelmate', 'approve_request', $request_id);
                notify($req->users_id, 'approved_request', $req->requests_id);
                return ApiResponse::create([
                    'message' => [true]
                ]);
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Your request failed! Please try again later.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param ApproveRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postDisapproveRequest(ApproveRequest $request)
    {
        try {
            $request_id = $request->request_id;
            $mates_id = $request->mates_id;
            $req = TravelMatesRequestUsers::where('requests_id', $request_id)->where('users_id', $mates_id)->first();
            if (!$req) {
                return ApiResponse::create(
                    [
                        'message' => ['User Not Found']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $req->status = '2';
            if ($req->save()) {
                log_user_activity('Travelmate', 'disapprove_request', $request_id);
                return ApiResponse::create(
                    [
                        'message' => [true]
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Your request failed! Please try again later.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postLeaveTripPlan(PostRequest $request)
    {
        try {
            $request_id = $request->request_id;
            $req = TravelMatesRequestUsers::find($request_id);
            if (!$req) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Request Id']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $req->status = '0';
            if ($req->save()) {
                log_user_activity('Travelmate', 'Leaved_trip_pan', $request_id);
                return ApiResponse::create(
                    [
                        'message' => [true]
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Your request failed! Please try again later.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchCity(Request $request)
    {
        try {
            $query = $request->get('search');
            if ($query) {
                $locations = array();
                $i = 0;
                $queryParam = $query;
                $get_cities = Cities::whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })->take(20)->get();
                foreach ($get_cities as $city) {
                    $locations[] = [
                        'id' => $city->id,
                        'city_name' => $city->transsingle->title,
                        'image' => check_country_photo(@$city->getMedias[0]->url, 180),
                        'country_name' => @$city->country->transsingle->title,
                    ];
                }

                return ApiResponse::create($locations);
            } else {
                return ApiResponse::create(
                    [
                        'search' => ['Search Term not provided']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchCountry(Request $request)
    {
        try {
            $query = $request->get('q');
            if ($query) {
                $countries = array();
                $i = 0;
                $queryParam = $query;
                $get_countries = Country::whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })->take(20)->get();
                foreach ($get_countries as $country) {
                    $countries[] = [
                        'id' => $country->id,
                        'text' => $country->transsingle->title,
                        'image' => check_country_photo(@$country->getMedias[0]->url, 180),
                        'query' => $query
                    ];
                }

                return ApiResponse::create($countries);
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Search Term not provided']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CreateWithoutPlanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function createWithoutPlan(CreateWithoutPlanRequest $request)
    {
        try {
            $user_id = Auth::user()->id;
            $destinations_list = explode(',', $request->destinations_list);
            $countries_id = [];
            $cities_id = [];

            foreach ($destinations_list as $destination) {
                $destination_type = explode('-', $destination);

                if ($destination_type[1] == 'country')
                    $countries_id[] = $destination_type[0];

                if ($destination_type[1] == 'city')
                    $cities_id[] = $destination_type[0];
            }

            $withoutPlan = new TravelMatesWithoutPlan;

            $withoutPlan->title = $request->title;
            $withoutPlan->users_id = $user_id;
            $withoutPlan->countries_id = implode(',', $countries_id) ? implode(',', $countries_id) : NULL;;
            $withoutPlan->cities_id = implode(',', $cities_id) ? implode(',', $cities_id) : NULL;;
            $withoutPlan->current_location = $request->current_location;
            $withoutPlan->interests = $request->interests;
            $withoutPlan->start_date = Carbon::parse($request->mutual_start_date)->format('Y-m-d');
            $withoutPlan->end_date = Carbon::parse($request->mutual_end_date)->format('Y-m-d');


            if ($withoutPlan->save()) {
                $travelMateRequest = new TravelMatesRequests;
                $travelMateRequest->users_id = $user_id;
                $travelMateRequest->without_plans_id = $withoutPlan->id;

                $travelMateRequest->save();
                log_user_activity('Travelmate', 'request', $travelMateRequest->id);
                return ApiResponse::create(
                    [
                        'message' => ['Your Travel Mates request was added successfully.']
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['Travel Mates request failed! Please try again later.']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getMyRequests(Request $request)
    {
        try {
            $page = $request->pagenum;
            $skip = ($page - 1) * 10;
            $sent_requests = TravelMatesRequests::select('travelmate_requests.*')
                ->leftJoin('travelmate_request_users as tru', 'tru.requests_id', '=', 'travelmate_requests.id')
                ->whereNotNull('travelmate_requests.plans_id')
                ->where('tru.users_id', Auth::user()->id)
                ->where('tru.status', '!=', 2)
                ->orderBy('tru.id', 'DESC')
                ->skip($skip)->take(10)->get();


            return ApiResponse::create(
                [
                    'sent_requests' => $sent_requests
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getMyInvitations(Request $request)
    {
        try {
            $page = $request->pagenum;
            $skip = ($page - 1) * 10;

            $invitations = TripsContributionRequests::where([
                'role' => 'travel-mate',
                'status' => TripInvitationsService::STATUS_PENDING,
                'authors_id' => Auth::user()->id
            ])->orderByDesc('created_at')->skip($skip)->take(10)->get()->unique('plans_id');

            return ApiResponse::create(
                [
                    'invitations' => $invitations
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CancelAllInvitationsRequest $request
     * @param TripInvitationsService $tripInvitationsService
     * @return \Illuminate\Http\Response
     */
    public function cancelAllInvitations(CancelAllInvitationsRequest $request, TripInvitationsService $tripInvitationsService)
    {
        try {
            $invitations = TripsContributionRequests::where([
                'role' => 'travel-mate',
                'authors_id' => Auth::user()->id,
                'plans_id' => $request->trip_id
            ])->get();

            foreach ($invitations as $invitation) {
                $result =  $tripInvitationsService->cancelInvitation($invitation->plans_id, $invitation->users_id);
            }

            if ($result) {
                return ApiResponse::create(
                    [
                        'message' => [true]
                    ]
                );
            } else {
                return ApiResponse::create(
                    [
                        'message' => [false]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getCountries()
    {
        try {
            $getCountries =  Countries::with('trans')->get()->pluck('trans.0.title', 'id');
            $countries = [];
            foreach ($getCountries as $id => $name) {
                $countries[] = ['id' => $id, 'name' => $name];
            }
            $countries = collect($countries)->sortBy('name')->values();
            $data['countries'] = $countries;
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getCities()
    {
        try {
            $data['cities'] = \App\Models\City\Cities::all();
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
