<?php

namespace App\Http\Controllers\Api\Page;

/* Dependencies */
use App\Http\Requests\Api\Pages\PageAddAdminRequest;
use App\Http\Requests\Api\Pages\PageCreateRequest;
use App\Http\Requests\Api\Pages\PageDeactivateRequest;
use App\Http\Requests\Api\Pages\PageNotificationSettingsRequest;
use App\Http\Requests\Api\Pages\PageRemoveAdminRequest;
use App\Models\Access\language\Languages;
use App\Models\Pages\Pages;
use App\Models\Pages\PagesAdmins;
use App\Models\Pages\PagesNotificationSettings;
use App\Models\Pages\PagesTranslations;
use App\Models\PagesCategories\PagesCategories;
use App\Models\System\Session;
use App\Models\User\ApiUser as User;
use Illuminate\Routing\Controller;

/**
 * Class PagesController.
 */
class PagesController extends Controller
{
	/* Create Page Api */
    /**
     * @param PageCreateRequest $request
     * @return array
     */
	public function create(PageCreateRequest $request){

	    $userId       = $request->input('user_id');
	    $sessionToken = $request->input('session_token');
	    $languageId   = $request->input('language_id');
	    $name         = $request->input('name');
	    $description  = $request->input('description');
	    $categoryId   = $request->input('category_id');

        /* Find User For The Provided User Id */
        $user = User::find($userId);

        if( empty($user) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For The Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found, Return Error */
        if( empty($session) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong session token provided.',
                ],
                'status' => false
            ];
        }

        /* Find Language For The Provided Language Id */
        $language = Languages::find($languageId);

        /* If Languages Not Found, Return Error */
        if( empty($language) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong language id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Category For The Provided Page Category Id */
        $category = PagesCategories::find($categoryId);

        /* If Category Not Found, Return Error */
        if(empty($category)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong category id provided.',
                ],
                'status' => false
            ];
        }

        /* Create New Page */
        $page         = new Pages;
        $page->url    = 'temp_url';
        $page->active = Pages::STATUS_ACTIVE;
        $page->save();

        /* Create New Page Translation */
        $page_trans               = new PagesTranslations;
        $page_trans->title        = $name;
        $page_trans->description  = $description;
        $page_trans->pages_id     = $page->id;
        $page_trans->languages_id = $languageId;
        $page_trans->save();

        /* Create New Page Admin */
        $page_admin           = new PagesAdmins;
        $page_admin->pages_id = $page->id;
        $page_admin->users_id = $userId;
        $page_admin->save();

        /* Return True Status, And Return Success Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'Page created successfully.'
            ]
        ];
	}

	/* Add Admin Api */
    /**
     * @param PageAddAdminRequest $request
     * @return array
     */
	public function add_admin(PageAddAdminRequest $request){

        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $pageId       = $request->input('page_id');
        $adminId      = $request->input('admin_id');

        /* Find User For The Provided User Id */
        $user = User::find($userId);

        if( empty($user) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For The Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found, Return Error */
        if( empty($session) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong session token provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id Doesn't Matches Provided User Id, Return Error */
        if($session->user_id != $userId){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Page For The Provided Page Id */
        $page = Pages::find($pageId);

        /* If Page Not Found, Return Error */
        if(empty($page)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong page id provided.',
                ],
                'status' => false
            ];
        }

        /* Find User For The Provided Admin Id */
        $admin = User::find($adminId);

        /* If User Not Found, Return Error */
        if(empty($admin)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong admin id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Previous Record For Provided Page Id And User Id In The PagesAdmins Table */
        $pagesAdmin = PagesAdmins::where(['pages_id' => $pageId, 'users_id' => $adminId ])->first();

        /* If Record Found, Return Already An Admin Error */
        if(!empty($pagesAdmin)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User already an admin.',
                ],
                'status' => false
            ];
        }

        /* Create New Record For Provided Admin Id And Page Id, In PagesAdmins Table */
        $pagesAdmin = new PagesAdmins;
        /* Load Information In PagesAdmins Table */
        $pagesAdmin->pages_id = $pageId;
        $pagesAdmin->users_id = $adminId;
        /* Save Information To Database */
        $pagesAdmin->save();

        /* Return Success Status, With "Admin Added Successfully" Message.  */
        return [
            'status' => true,
            'data' => [
                'message' => 'Page admin added successfully.'
            ]
        ];

    }

	/* Remove Admin Api */
    /**
     * @param PageRemoveAdminRequest $request
     * @return array
     */
	public function remove_admin(PageRemoveAdminRequest $request){

        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $pageId       = $request->input('page_id');
        $adminId      = $request->input('admin_id');

        /* Find User For The Provided User Id */
        $user = User::find($userId);

        if( empty($user) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For The Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found, Return Error */
        if( empty($session) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong session token provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id Doesn't Matches Provided User Id, Return Error */
        if($session->user_id != $userId){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Page For The Provided Page Id */
        $page = Pages::find($pageId);

        /* If Page Not Found, Return Error */
        if(empty($page)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong page id provided.',
                ],
                'status' => false
            ];
        }

        /* Find User For The Provided Admin Id */
        $admin = User::find($adminId);

        /* If User Not Found, Return Error */
        if(empty($admin)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong admin id provided.',
                ],
                'status' => false
            ];
        }

        /*Find Relation For The Provided Page Id And User Id In The PagesAdmins Table */
        $pageAdmin = PagesAdmins::where([ 'pages_id' => $pageId, 'users_id' => $adminId ])->first();

        /* If Page Admin Not Found, Return Error */
        if(empty($pageAdmin)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'This user is not an admin of this page.',
                ],
                'status' => false
            ];
        }

        /* Delete Page Admin */
        $pageAdmin->delete();

        /* Return Success Status, Along With Success Message */
        return [
            'status' => true,
            'data'   => [
                'message' => 'Admin removed successfully.'
            ]
        ];
	}

	/* Deactivate Page Api */
	public function deactivate(PageDeactivateRequest $request){

        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $pageId       = $request->input('page_id');

        /* Find User For The Provided User Id */
        $user = User::find($userId);

        if( empty($user) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For The Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found, Return Error */
        if( empty($session) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong session token provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id Doesn't Matches Provided User Id, Return Error */
        if($session->user_id != $userId){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Page For The Provided Page Id */
        $page = Pages::find($pageId);

        /* If Page Not Found, Return Error */
        if(empty($page)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong page id provided.',
                ],
                'status' => false
            ];
        }

        /* Check If Provided User Is An Admin Of The Provided Page  */
        $page_admin = PagesAdmins::where(['pages_id' => $pageId, 'users_id' => $userId])->first();

        /* If User Is Not An Admin Of Provided Page, Return Error */
        if(empty($page_admin)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'You are not an admin of this page.',
                ],
                'status' => false
            ];
        }

        /* Set Status Of Page To Deactive, And Save The Page */
        $page->active = Pages::STATUS_INACTIVE;
        $page->save();

        /* Return Success Status, Along With Success Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'Page deactivated successfully.'
            ]
        ];

	}

	/* Notification Settings Api */
    /**
     * @param PageNotificationSettingsRequest $request
     * @return array
     */
	public function notification_settings(PageNotificationSettingsRequest $request){

        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $pageId       = $request->input('page_id');
        $followNot    = $request->input('follow_notification' );
        $messageNot   = $request->input('message_notification');
        $commentNot   = $request->input('comment_notification');
        $likeNot      = $request->input('like_notification');
        $travoooNot   = $request->input('travooo_announcement_notification');
        $email_not    = $request->input('email_notification');

        /* Find User For The Provided User Id */
        $user = User::find($userId);

        if( empty($user) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For The Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found, Return Error */
        if( empty($session) ){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong session token provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id Doesn't Matches Provided User Id, Return Error */
        if($session->user_id != $userId){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Page For The Provided Page Id */
        $page = Pages::find($pageId);

        /* If Page Not Found, Return Error */
        if(empty($page)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong page id provided.',
                ],
                'status' => false
            ];
        }

        /* Check If Provided User Is An Admin Of The Provided Page  */
        $page_admin = PagesAdmins::where(['pages_id' => $pageId, 'users_id' => $userId])->first();

        /* If User Is Not An Admin Of Provided Page, Return Error */
        if(empty($page_admin)){
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'You are not an admin of this page.',
                ],
                'status' => false
            ];
        }

        /* Follow Notification Setting: Start */

        $page_setting = PagesNotificationSettings::where(['pages_id' => $pageId,'var' => 'follow_notification' ])->first();

        if(empty($page_setting)){
            $page_setting           = new PagesNotificationSettings;
            $page_setting->pages_id = $pageId;
            $page_setting->var      = 'follow_notification';
            $page_setting->val      = $followNot;
            $page_setting->save();
        }else{
            $page_setting->val = $followNot;
            $page_setting->save();
        }
        /* Follow Notification Setting: End */

        /* Message Notification Setting: Start */

        $page_setting = PagesNotificationSettings::where(['pages_id' => $pageId,'var' => 'message_notification' ])->first();

        if(empty($page_setting)){
            $page_setting           = new PagesNotificationSettings;
            $page_setting->pages_id = $pageId;
            $page_setting->var      = 'message_notification';
            $page_setting->val      = $messageNot;
            $page_setting->save();
        }else{
            $page_setting->val = $messageNot;
            $page_setting->save();
        }
        /* Message Notification Setting: End */

        /* Comment Notification Setting: Start */

        $page_setting = PagesNotificationSettings::where(['pages_id' => $pageId,'var' => 'comment_notification' ])->first();

        if(empty($page_setting)){
            $page_setting           = new PagesNotificationSettings;
            $page_setting->pages_id = $pageId;
            $page_setting->var      = 'comment_notification';
            $page_setting->val      = $commentNot;
            $page_setting->save();
        }else{
            $page_setting->val = $commentNot;
            $page_setting->save();
        }
        /* Comment Notification Setting: End */

        /* Like Notification Setting: Start */

        $page_setting = PagesNotificationSettings::where(['pages_id' => $pageId,'var' => 'like_notification' ])->first();

        if(empty($page_setting)){
            $page_setting           = new PagesNotificationSettings;
            $page_setting->pages_id = $pageId;
            $page_setting->var      = 'like_notification';
            $page_setting->val      = $likeNot;
            $page_setting->save();
        }else{
            $page_setting->val = $likeNot;
            $page_setting->save();
        }
        /* Like Notification Setting: End */

        /* Travoo Announcement Notification Setting: Start */

        $page_setting = PagesNotificationSettings::where(['pages_id' => $pageId,'var' => 'travooo_announcement_notification' ])->first();

        if(empty($page_setting)){
            $page_setting           = new PagesNotificationSettings;
            $page_setting->pages_id = $pageId;
            $page_setting->var      = 'travooo_announcement_notification';
            $page_setting->val      = $travoooNot;
            $page_setting->save();
        }else{
            $page_setting->val = $travoooNot;
            $page_setting->save();
        }
        /* Travoo Announcement Notification Setting: End */

        /* Email Notification Setting: Start */

        $page_setting = PagesNotificationSettings::where(['pages_id' => $pageId,'var' => 'email_notification' ])->first();

        if(empty($page_setting)){
            $page_setting           = new PagesNotificationSettings;
            $page_setting->pages_id = $pageId;
            $page_setting->var      = 'email_notification';
            $page_setting->val      = $email_not;
            $page_setting->save();
        }else{
            $page_setting->val = $email_not;
            $page_setting->save();
        }
        /* Email Notification Setting: End */

        /* Return Success Status, Along With Success Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'Page settings saved successfully.'
            ]
        ];


	}
}