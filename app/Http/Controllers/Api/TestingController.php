<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Models\ActivityMedia\Media;
use App\Models\City\Cities;
use App\Models\City\CitiesFollowers;
use App\Models\Country\Countries;
use App\Models\Country\CountriesFollowers;
use App\Models\Events\Events;
use App\Models\Lifestyle\Lifestyle;
use App\Models\Place\Medias;
use App\Models\Place\Place;
use App\Models\Place\PlaceFollowers;
use App\Models\Place\PlaceMedias;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Posts\Checkins;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsCities;
use App\Models\Posts\PostsCountries;
use App\Models\Posts\PostsMedia;
use App\Models\Posts\PostsPlaces;
use App\Models\Reports\Reports;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use App\Models\User\User;
use App\Models\User\UsersFriends;
use App\Models\User\UsersTravelStyles;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Trips\PlanActivityLogService;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsSuggestionsService;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use stdClass;
use App\Http\Constants\CommonConst;
use App\Models\ApiRequest;
use App\Models\Discussion\Discussion;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsShares;
use App\Models\Reports\ReportsDraft;
use App\Models\Reports\ReportsDraftsLocation;
use App\Models\Reports\ReportsInfos;
use App\Models\Reports\ReportsLocation;
use App\Models\Reports\ReportsPlaces;
use App\Models\User\UsersContentLanguages;
use App\Services\Api\PrimaryPostService;
use Illuminate\Support\Facades\Schema;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Artisan;
use App\Models\ActivityLog\ActivityLog;
use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\User\UserWatchHistory;
use App\Services\Api\CCPNewsfeed;
use App\Services\Api\LoadedFeedService;
use FFMpeg\FFMpeg;
use App\Services\FFmpeg\FFmpegService;

set_time_limit(0);
ini_set('memory_limit', -1);

class TestingController extends Controller
{

    // This Controller use Only for testing purpose
    public function __construct()
    {
        $this->logDir = storage_path('logs') . "/";
    }

    public function debug(FFmpegService $ffmpegService)
    {
        if (request()->has('storage-link')) {
            \Artisan::call('storage:link');
            dd('linked');
        }
        if (!request()->has('url')) {
            dd('url required');
        }
        try {
            if (request('check_dir')) {
                $lst = [];
                if ($handle = opendir(request('check_dir'))) {
                    while (false !== ($file = readdir($handle))) {
                        if ($file == '.' || $file == '..' || $file == '.gitignore') continue;
                        $lst[] = $file;
                    }
                    closedir($handle);
                }
                dd($lst);
            }

            if (request('clean_log')) {
                $lst = [];
                if ($handle = opendir(storage_path('logs'))) {
                    while (false !== ($file = readdir($handle))) {
                        if ($file == '.' || $file == '..' || $file == '.gitignore') continue;
                        $lst[] = $file;
                    }
                    closedir($handle);
                }
                foreach ($lst as $u) {
                    @unlink(storage_path('logs') . "/" . $u);
                }
                dd("clean");
            }

            $wantDelete = get_as_bool(request('delete', true));
            $done = false;
            $s3VideoThumbnail = NULL;
            $s3Path = str_replace(S3_BASE_URL, '', request('url')); // remove S3_BASE_URL if found
            $some_file = Storage::disk('s3')->get($s3Path); // GET s3 video
            Storage::disk('public')->put($s3Path, $some_file); // store s3 video in local server
            $localVideoThumbnail = $ffmpegService->convertVideoThumbnail(public_path('storage/' . $s3Path), 600, 600, 70); // create Thumbnail in local based on local video
            if ($localVideoThumbnail) {
                $done = true;
                Storage::disk('public')->delete($s3Path); // delete local video
                $s3VideoThumbnail = $ffmpegService->moveThumbnailToS3($localVideoThumbnail, $wantDelete); // move local Thumbnail in s3 & delete local Thumbnail
            }
            dd([
                'status' => $done,
                "thumbnail" => $s3VideoThumbnail,
                's3' => request('url'),
                'local_video' => public_path('storage/' . $s3Path)
            ]);
        } catch (\Throwable $e) {
            dd([
                'status' => false,
                "message" => $e->getMessage()
            ]);
        }
    }

    public function flushSession()
    {
        Auth::loginUsingId(request('user_id'));
        sleep(1);
        ApiSessionFlush();
        dd("Flush success", auth()->user());
    }

    function ayncSeed()
    {
        $users_id = request('users_id');
        $type = request('type');
        $times = request('times');
        if (
            $users_id &&
            $type &&
            $times
        ) {
            if (User::find($users_id)) {
                \Artisan::call("seed", [
                    'userId' => $users_id,
                    '--type' => $type,
                    '--times' => $times,
                ]);
            } else {
                dd('user not found');
            }
        } else {
            dd('something wrong here');
        }
    }

    public function findDuplication($module)
    {
        $search = request('search');
        if ($module == "discussion") {
            return \App\Models\Discussion\Discussion::query()
                ->where('question', 'like', "%$search%")
                ->orWhere('description', 'like', "%$search%")->get();
        } else if ($module == "post") {
            return \App\Models\Posts\Posts::query()
                ->where('text', 'like', "%$search%")->get();
        } else if ($module == "report") {
            return \App\Models\Reports\Reports::query()
                ->where('title', 'like', "%$search%")
                ->orWhere('description', 'like', "%$search%")->get();
        }
        return ["invalid module"];
    }

    public function debugUserMode($id)
    {
        $user = User::find($id);
        $user->debug = ($user->debug) ? 0 : 1;
        $user->save();
        dd($user->debug);
    }

    public function lastApiRequests($num)
    {
        return ApiResponse::create(
            ApiRequest::query()
                ->limit($num)->orderBy('created_at', 'DESC')
                ->when(request()->has('module'), function ($q) {
                    $q->where('module', request('module'));
                })
                ->when(request()->has('method'), function ($q) {
                    $q->where('method', request('method'));
                })
                ->when(request()->has('end_point'), function ($q) {
                    $q->where('end_point', request('end_point'));
                })
                ->when(request()->has('sub'), function ($q) {
                    $q->where('sub', request('sub'));
                })
                ->when(request()->has('date'), function ($q) {
                    $q->whereRaw('date(created_at) = "' . request('date') . '"');
                })
                ->get()->map(function ($item) {
                    if ($item->request_params != NULL) $item->request_params = json_decode($item->request_params, true);
                    if ($item->header != NULL) $item->header = @collect(json_decode($item->header, true))->only('Authorization', 'Accept');
                    if ($item->meta != NULL) $item->meta = json_decode($item->meta, true);
                    unset($item->updated_at);
                    return $item;
                })
        );
    }


    public function downloadLog($file)
    {
        $path = $this->logDir . $file;
        return \Illuminate\Support\Facades\Response::download($path, $file, [
            'Content-Type: text/x-log'
        ]);
    }


    public function checkLink(Request $request)
    {
        if (isLiveSite() && isMobileDevice()) {
            $url = generateDeepLink(url('/api/v1/app-test-link'));
            return view('test.app', [
                'link' => $url
            ]);
        } else {
            return redirect(url('/web-test-link'));
        }
    }

    // temp testing code
    public function deleteUser($email)
    {
        if (request('pass') !== ("Access@Travooo")) {
            dd("Access denied.");
        }
        try {
            $user = User::where('email', $email)->first();
            if ($user != null) {
                if ($user->delete()) {
                    return ApiResponse::create(['message' => ['delete user successfully']]);
                } else {
                    return ApiResponse::create(['message' => ['user can not delete']], false, ApiResponse::BAD_REQUEST);
                }
            } else {
                return ApiResponse::create(['message' => ['user not found']], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    public function users(Request $request, $email = null)
    {
        if (request('pass') !== ("Access@Travooo")) {
            dd("Access denied.");
        }
        if ($email) {
            return User::where('email', 'like', '%' . $email . '%')->get();
        } else {
            return User::orderBy('id', 'DESC')->paginate(10);
        }
    }

    public function script(Request $request)
    {
        if (request('pass') !== ("Access@Travooo")) {
            dd("Access denied.");
        }
        $result = [];
        if ($request->type) {
            switch ($request->type) {
                case 'select':
                    if ($request->select && (strpos(strtolower($request->select), 'select') !== false) && (strpos(strtolower($request->select), 'from') !== false)) {
                        return DB::select($request->select);
                    }
                    return "INVALID";
                    break;
                case 'recommendedPlaces':
                    if ($request->user_id) {
                        $authUser = User::find($request->user_id);
                        if ($authUser) {
                            if (count($authUser->travelstyles) == 0) {
                                \App\Models\Travelstyles\Travelstyles::limit(5)->get()->each(function ($travelstyle) use ($authUser) {
                                    $usersTravelStyles = new UsersTravelStyles;
                                    $usersTravelStyles->users_id = $authUser->id;
                                    $usersTravelStyles->conf_lifestyles_id = $travelstyle->id;
                                    $usersTravelStyles->save();
                                });
                            }
                            foreach ($authUser->travelstyles as $travelstyle) {
                                for ($i = 1; $i <= 10; $i++) {
                                    $countriesLifestyles = new \App\Models\Country\CountriesLifestyles;
                                    $countriesLifestyles->countries_id = Countries::all()->random()->id;
                                    $countriesLifestyles->lifestyles_id = $travelstyle->conf_lifestyles_id;
                                    $countriesLifestyles->save();

                                    $citiesLifestyles = new \App\Models\City\CitiesLifestyles;
                                    $citiesLifestyles->cities_id = Cities::all()->random()->id;
                                    $citiesLifestyles->lifestyles_id = $travelstyle->conf_lifestyles_id;
                                    $citiesLifestyles->save();

                                    $add[] = $countriesLifestyles;
                                    $add[] = $citiesLifestyles;
                                }
                            }
                            return $add;
                        } else {
                            return "user not found";
                        }
                    } else {
                        return "user_id not found";
                    }
                    break;
                case 'discoverNewTravellers':
                    $users =  User::whereDate('created_at', '<', Carbon::now()->subDays(30))->orderBy('created_at', 'DESC')->take(10)->get();
                    foreach ($users as $user) {
                        $user->created_at = Carbon::now()->subDays(40);
                        $user->save();
                        $result[] = $user;
                    }
                    return $result;
                    break;

                case 'newFollower':
                    $mediaUserRes = User::select(
                        'id',
                        DB::raw('(SELECT count(id) FROM medias where id in ( SELECT medias_id FROM `users_medias` WHERE users_id=users.id)) as avg_count')
                    )
                        ->orderBy('created_at', 'DESC')
                        ->having('avg_count', '>', 3)
                        ->take(20)->get();
                    foreach ($mediaUserRes as $mediaUser) {
                        if (!UsersFollowers::where('users_id', $request->user_id)->where('followers_id', $mediaUser->id)->first()) {
                            $userFolllower = new UsersFollowers;
                            $userFolllower->users_id = $request->user_id;
                            $userFolllower->followers_id = $mediaUser->id;
                            $userFolllower->follow_type = 1;
                            $userFolllower->save();
                        }
                    }
                    return $mediaUserRes;
                    break;
                case 'collectiveFollowersPlaces':
                    if ($request->user_id) {
                        $authUser = User::find($request->user_id);
                        if ($authUser) {
                            for ($i = 1; $i <= 5; $i++) {
                                $user_id        = User::all()->random()->id;
                                $places_id      = Place::all()->random()->id;
                                $countries_id   = Countries::all()->random()->id;
                                $cities_id      = Cities::all()->random()->id;
                                if (!UsersFriends::where('users_id', $authUser->id)->where('friends_id', $user_id)->first()) {
                                    $usersFriends = new UsersFriends;
                                    $usersFriends->users_id = $authUser->id;
                                    $usersFriends->friends_id = $user_id;
                                    $usersFriends->save();
                                    $result[] = $usersFriends;
                                }
                                if (!PlaceFollowers::where('users_id', $user_id)->where('places_id', $places_id)->first()) {
                                    $result[] = PlaceFollowers::create([
                                        'users_id' => $user_id,
                                        'places_id' => $places_id,
                                        'created_at' => Carbon::now()->subDays(4)
                                    ]);
                                }
                                if (!CountriesFollowers::where('users_id', $user_id)->where('countries_id', $countries_id)->first()) {
                                    $result[] = CountriesFollowers::create([
                                        'users_id' => $user_id,
                                        'countries_id' => $countries_id,
                                        'created_at' => Carbon::now()->subDays(4)
                                    ]);
                                }
                                if (!CitiesFollowers::where('users_id', $user_id)->where('cities_id', $cities_id)->first()) {
                                    $result[] = CitiesFollowers::create([
                                        'users_id' => $user_id,
                                        'cities_id' => $cities_id,
                                        'created_at' => Carbon::now()->subDays(4)
                                    ]);
                                }
                            }
                            return $result;
                        } else {
                            return "user not found";
                        }
                    } else {
                        return "user_id not found";
                    }
                    break;
                case 'trendingEvents':
                    if ($request->user_id) {
                        $authUser = User::find($request->user_id);
                        if ($authUser) {
                            if (Events::count()) {
                                $_places_id = Events::distinct('places_id')->where('places_id', '!=', 0)->take(2)->whereNotNull('places_id')->pluck('places_id')->toArray();
                                $_countries_id = Events::distinct('countries_id')->where('countries_id', '!=', 0)->take(2)->whereNotNull('countries_id')->pluck('countries_id')->toArray();
                                $_cities_id = Events::distinct('cities_id')->where('cities_id', '!=', 0)->take(2)->whereNotNull('cities_id')->pluck('cities_id')->toArray();
                                $placesIds = PlaceFollowers::query()
                                    ->where('users_id', $authUser->id)
                                    ->whereIn('places_id', $_places_id)
                                    ->with('place')->pluck('places_id');

                                if (!count($placesIds) && count($_places_id)) {
                                    foreach ($_places_id as $places_id) {
                                        $placeFollowers = new PlaceFollowers;
                                        $placeFollowers->places_id = $places_id;
                                        $placeFollowers->users_id = $authUser->id;
                                        $placeFollowers->save();
                                    }
                                }

                                $countriesIds = CountriesFollowers::query()
                                    ->where('users_id', $authUser->id)
                                    ->whereIn('countries_id', $_countries_id)
                                    ->with('country')->pluck('countries_id');

                                if (!count($countriesIds) && count($_countries_id)) {
                                    foreach ($_countries_id as $countries_id) {
                                        $countriesFollowers = new CountriesFollowers();
                                        $countriesFollowers->countries_id = $countries_id;
                                        $countriesFollowers->users_id = $authUser->id;
                                        $countriesFollowers->save();
                                    }
                                }

                                $citiesIds = CitiesFollowers::query()
                                    ->where('users_id', $authUser->id)
                                    ->whereIn('cities_id', Events::distinct('cities_id')->take(2)->whereNotNull('cities_id')->pluck('cities_id')->toArray())
                                    ->with('city')->pluck('cities_id');

                                if (!count($citiesIds) && count($_cities_id)) {
                                    foreach ($_cities_id as $cities_id) {
                                        $citiesFollowers = new CitiesFollowers();
                                        $citiesFollowers->cities_id = $cities_id;
                                        $citiesFollowers->users_id = $authUser->id;
                                        $citiesFollowers->save();
                                    }
                                }

                                $placesIds = PlaceFollowers::query()
                                    ->where('users_id', $authUser->id)
                                    ->with('place')->pluck('places_id');

                                $countriesIds = CountriesFollowers::query()
                                    ->where('users_id', $authUser->id)
                                    ->with('country')->pluck('countries_id');

                                $citiesIds = CitiesFollowers::query()
                                    ->where('users_id', $authUser->id)
                                    ->with('city')->pluck('cities_id');

                                $events = Events::whereIn('places_id', $placesIds)
                                    ->orWhereIn('countries_id', $countriesIds)
                                    ->orWhereIn('cities_id', $citiesIds)
                                    ->get()->sortByDesc(function ($event) {
                                        return $event->shares->count();
                                    })->take(8);
                                return $events;
                            }
                        } else {
                            return "user not found";
                        }
                    } else {
                        return "user_id not found";
                    }
                    break;
            }
        } else {
            return "type not found";
        }
    }

    // dumpingXData | dumpingXData | dumpingXData | dumpingXData | dumpingXData

    const _CALL_REPAIR_REPORTS_MODULE = 'repair-reports-module';
    const _CALL_ADD_DB_FUNCTIONS = 'add-db-functions';
    const _RETURN = 'return';
    const _EXPERTISE = 'expertise';


    const _DUMP_USER_FOLLOWER = 'dump_user_follower';
    const _DUMP_USER_FOLLOWING = 'dump_user_following';
    const _DUMP_MY_TRAVEL_STYLE = 'dump_my_travel_style';

    const _USER_LOCATION_CONTENT = 'user_location_content';
    const _DUMP_COUNTRY_FOLLOWER = 'dump_country_follower';
    const _DUMP_CITY_FOLLOWER = 'dump_city_follower';
    const _DUMP_PLACE_FOLLOWER = 'dump_place_follower';




    public function dumpingXData()
    {
        //2125
        $return = [];
        $myId = isLiveSite() ? 1969 : 1594;
        $myId = request('user_id', $myId);
        $return['user_id'] = $myId;
        Auth::loginUsingId($myId);
        sleep(1);

        if (!auth()->check()) {
            dd('user not found');
        }

        if (request()->has('clean-user-watch')) {
            \Schema::disableForeignKeyConstraints();
            UserWatchHistory::truncate();
            \Schema::enableForeignKeyConstraints();
            dd('done');
        }

        if (request()->has('fill-delete-reasons-list')) {
            \Artisan::call('fill:delete-reasons-list');
            dd('done');
        }

        if (request()->has('clean-api-requests')) {
            \Schema::disableForeignKeyConstraints();
            ApiRequest::truncate();
            \Schema::enableForeignKeyConstraints();
            dd('done');
        }

        if (request()->has('sync-acnt')) { //only for prod
            // $myId = 1888
            UsersFollowers::where('users_id', $myId)->delete();
            UsersFollowers::where('followers_id', $myId)->delete();
            // 26
            $otherId = 26;
            UsersFollowers::where('users_id', $otherId)->get()->map(function ($item) use ($myId) {
                $userfollower = new UsersFollowers;
                $userfollower->users_id = $myId;
                $userfollower->followers_id = $item->followers_id;
                $userfollower->follow_type = $item->follow_type;
                $userfollower->from_where = $item->from_where;
                $userfollower->save();
            });
            UsersFollowers::where('followers_id', $otherId)->get()->map(function ($item) use ($myId) {
                $userfollower = new UsersFollowers;
                $userfollower->users_id = $item->followers_id;
                $userfollower->followers_id = $myId;
                $userfollower->follow_type = $item->follow_type;
                $userfollower->from_where = $item->from_where;
                $userfollower->save();
            });
        }

        if (request()->has(self::_CALL_REPAIR_REPORTS_MODULE)) {
            \Artisan::call('repair:reports-module');
            return ['done'];
        }

        if (request()->has(self::_CALL_ADD_DB_FUNCTIONS)) {
            // \Artisan::call('add-db-functions');
            $flag = false;
            if (request()->has('update') && request('update') == "true") {
                $baseUrl = __DIR__ . "/../../../../";
                DB::unprepared(file_get_contents($baseUrl . "database/sql/fun_basic_algo.sql"));
                DB::unprepared(file_get_contents($baseUrl . "database/sql/fun_user_access_denied.sql"));
                $flag = true;
            }

            $post_comment = null;
            $res = DB::select('SHOW FUNCTION STATUS');
            if (collect($res)->where('Name', 'fun_basic_algo')->count()) {
                $post_comment = PostsComments::select('*')
                    ->addSelect(DB::raw('fun_basic_algo(id, type) as total'))
                    // ->addSelect(DB::raw('(0) as total'))
                    ->where('posts_id', 675258)
                    ->where('type', 'post')
                    ->whereNull('parents_id')->take(1)->get();
            }

            return [$flag, collect($res)->where('Db', '!=', 'sys'), $post_comment];
        }

        $authUser = auth()->user();
        $final_outcome = getDesiredLanguage();
        if (isset($final_outcome[0])) {
            $final_outcome = $final_outcome[0];
        } else {
            $final_outcome = UsersContentLanguages::DEFAULT_LANGUAGES;
        }

        $user_location_content = request()->has(self::_USER_LOCATION_CONTENT);

        if (request()->has(self::_RETURN)) {
            $myfollower = auth()->user()->get_followers->pluck('followers_id');
            $myTravelStyles  = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();
            $followed_places = auth()->user()->followedPlaces->pluck('places_id')->toArray();
            $followed_cities = auth()->user()->followedCities->pluck('cities_id')->toArray();
            $followed_countries = auth()->user()->followedCountries->pluck('countries_id')->toArray();

            $expertiseServise =  app(\App\Services\Users\ExpertiseService::class);

            return [
                'myfollower' => $myfollower,
                'myTravelStyles' => $myTravelStyles,
                'followed_places' => $followed_places,
                'followed_cities' => $followed_cities,
                'followed_countries' => $followed_countries,
                'expertiseServise' => $expertiseServise->getExpertiseArray($authUser->id),
            ];
        }


        if (request()->has(self::_EXPERTISE)) {
            ExpertsCountries::query()->whereHas('countries')
                ->selectRaw('countries_id, COUNT(users_id) AS RANK')
                ->whereNotIn('countries_id', ExpertsCountries::where('users_id', $authUser->id)->pluck('countries_id')->toArray())
                ->groupBy('countries_id')
                ->orderBy('RANK', 'DESC')
                ->take(10)->get()->each(function ($ExpertsCountries) use ($authUser) {
                    ExpertsCountries::create([
                        'users_id' => $authUser->id,
                        'countries_id' => $ExpertsCountries->countries_id,
                    ]);
                });

            ExpertsCities::query()->whereHas('cities')
                ->selectRaw('cities_id, COUNT(users_id) AS RANK')
                ->whereNotIn('cities_id', ExpertsCities::where('users_id', $authUser->id)->pluck('cities_id')->toArray())
                ->groupBy('cities_id')
                ->orderBy('RANK', 'DESC')
                ->take(10)->get()->each(function ($ExpertsCities) use ($authUser) {
                    ExpertsCities::create([
                        'users_id' => $authUser->id,
                        'cities_id' => $ExpertsCities->cities_id,
                    ]);
                });

            $return[self::_EXPERTISE] = true;
        }

        if (request()->has('delete__')) {
            UsersFollowers::where('users_id', $myId)->delete();
            UsersFollowers::where('followers_id', $myId)->delete();
            UsersFollowers::where('followers_id', 0)->delete();
            UsersFollowers::where('users_id', 0)->delete();
        }


        if (request()->has('friend')) {
            $users = collect(
                User::query()
                    ->whereDate('created_at', '>', Carbon::now()->subDays(300))
                    ->whereIn('register_steps', ['7', 'complated'])
                    ->inRandomOrder()->take(rand(30, 40))
                    ->pluck('id')
                    ->toArray()
            )->merge(
                User::query()
                    ->whereDate('created_at', '>', Carbon::now()->subDays(300))
                    ->where('expert', 1)
                    ->where('is_approved', 1)
                    ->take(rand(10, 15))
                    ->pluck('id')->toArray()
            )->unique()->toArray();
            collect($users)->each(function ($user_id) use ($myId) {
                $wh = [
                    'followers_id'  => $user_id,
                    'users_id'  => $myId,
                    'from_where'  => 'profile',
                ];
                if (!UsersFollowers::where($wh)->first()) {
                    UsersFollowers::create($wh);
                }
            });
            collect($users)->each(function ($user_id) use ($myId) {
                $wh = [
                    'followers_id'  => $myId,
                    'users_id'  => $user_id,
                    'from_where'  => 'profile',
                ];
                if (!UsersFollowers::where($wh)->first()) {
                    UsersFollowers::create($wh);
                }
            });

            dd($myId, User::find($myId)->findFriends()->get()->count());
        }


        /*Dump User Following*/
        if (request()->has(self::_DUMP_USER_FOLLOWER)) {
            $already = auth()->user()->get_followers;
            collect(User::inRandomOrder()->whereDate('created_at', '>', Carbon::now()->subDays(350))->whereNotIn('id', $already->pluck('followers_id')->toArray())->take(20)->pluck('id'))->each(function ($user_id) use (&$already,  $myId) {
                if (!UsersFollowers::where([
                    'followers_id'  => $user_id,
                    'users_id'  => $myId,
                    'from_where'  => 'profile',
                ])->first()) {
                    $flag = UsersFollowers::create([
                        'followers_id'  => $user_id,
                        'users_id'  => $myId,
                        'from_where'  => 'profile',
                    ]);
                    $already[] = $flag;
                }
            });
            $return[self::_DUMP_USER_FOLLOWER] = true;
            dd($myId, auth()->user()->get_followers->count());
        }

        /*Dump User Follower*/
        if (request()->has(self::_DUMP_USER_FOLLOWING)) {
            $already = UsersFollowers::where('followers_id', $myId)->where('follow_type', 1)->pluck('users_id')->toArray();
            collect(User::inRandomOrder()->whereNotIn('id', $already)->take(5)->pluck('id'))->each(function ($user_id) use (&$already,  $myId) {
                $userFollower = new UsersFollowers;
                $userFollower->followers_id = $myId;
                $userFollower->follow_type = 1;
                $userFollower->users_id = $user_id;
                $userFollower->from_where = 'profile';
                $userFollower->save();
                $already[] = $user_id;
            });

            if (count($already)) {
                $res = DB::select("SELECT users_id, count(id) as rank FROM `activity_log` where time > '?' and users_id not in(" . implode(",", $already) . ") GROUP BY users_id ORDER BY rank DESC limit 15", [Carbon::now()->subDays(7)]);
            } else {
                $res = DB::select("SELECT users_id, count(id) as rank FROM `activity_log` where time > '?' GROUP BY users_id ORDER BY rank DESC limit 15", [Carbon::now()->subDays(7)]);
            }
            collect($res)->each(function ($item) use (&$already,  $myId) {
                $userFollower = new UsersFollowers;
                $userFollower->followers_id = $myId;
                $userFollower->follow_type = 1;
                $userFollower->users_id = $item->users_id;
                $userFollower->from_where = 'profile';
                $userFollower->save();
                $already[] = $item->users_id;
            });

            if (count($already)) {
                $res = DB::select("SELECT users_id, count(id) as rank FROM `activity_log` where users_id not in(" . implode(",", $already) . ") GROUP BY users_id ORDER BY rank DESC limit 20,20");
            } else {
                $res = DB::select("SELECT users_id, count(id) as rank FROM `activity_log` GROUP BY users_id ORDER BY rank DESC limit 20,20");
            }
            collect($res)->each(function ($item) use (&$already,  $myId) {
                $userFollower = new UsersFollowers;
                $userFollower->followers_id = $myId;
                $userFollower->follow_type = 1;
                $userFollower->users_id = $item->users_id;
                $userFollower->from_where = 'profile';
                $userFollower->save();
                $already[] = $item->users_id;
            });
            UsersFollowers::where('followers_id', $myId)->where('follow_type', 1)->where('users_id', 1)->delete();


            if (request()->has('with-friend')) {
                $myfollower = UsersFollowers::where('followers_id', $myId)->where('follow_type', 1)->pluck('users_id')->toArray();
                collect($myfollower)->take(20)->each(function ($id) use ($myId) {
                    if (!UsersFollowers::where([
                        'followers_id' => $id,
                        'follow_type' => 1,
                        'users_id' => $myId,
                    ])->first()) {
                        $userFollower = new UsersFollowers;
                        $userFollower->followers_id = $id;
                        $userFollower->follow_type = 1;
                        $userFollower->users_id = $myId;
                        $userFollower->from_where = 'profile';
                        $userFollower->save();
                    }
                });
            }

            $return[self::_DUMP_USER_FOLLOWING] = true;
        }

        // Script For Add Data Algo Wise
        if (request()->has(self::_DUMP_MY_TRAVEL_STYLE)) {
            $myTravelStyles  = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();
            foreach (Lifestyle::take(10)->whereNotIn('id', $myTravelStyles)->inRandomOrder()->pluck('id')->toArray() as $ts) {
                $usersTravelStyles = new UsersTravelStyles;
                $usersTravelStyles->users_id = $authUser->id;
                $usersTravelStyles->conf_lifestyles_id = $ts;
                $usersTravelStyles->save();
            }
            $return[self::_DUMP_MY_TRAVEL_STYLE] = true;
        }

        /*Dump Place Follower*/
        if (request()->has(self::_DUMP_PLACE_FOLLOWER)) {
            $limit = 25;
            $followed_places = auth()->user()->followedPlaces->pluck('places_id')->toArray();
            if ($user_location_content) {
                $topPosts = PostsPlaces::leftJoin('posts', function ($join) {
                    $join->on('posts_places.posts_id', '=', 'posts.id');
                })
                    ->whereNotIn('posts_places.places_id', $followed_places)
                    ->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`posts`.`id`) 
            + (select count(*) from `posts_likes` where `posts_id`=`posts`.`id`) 
            + (select count(*) from `posts_shares` where `posts_id`=`posts`.`id`)) as ranks')
                    ->whereRaw('users_id is not null and posts.permission=0')
                    ->where('posts.users_id', '<>', Auth::user()->id)
                    ->orderBy('ranks', 'desc')
                    ->whereDate('posts.created_at', '>', Carbon::now()->subDays(30))
                    ->where('language_id', $final_outcome)
                    ->limit($limit)
                    ->pluck('posts_places.places_id')->toArray();

                $topDiscussion = Discussion::whereNotIn('destination_id', $followed_places)
                    ->where('destination_type', 'place')
                    ->where('users_id', '<>', Auth::user()->id)
                    ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`) as replies')
                    ->orderBy('replies', 'DESC')
                    ->whereRaw('(select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`> 5)')
                    ->limit($limit)
                    ->whereDate('created_at', '>', Carbon::now()->subDays(30))
                    ->where('language_id', $final_outcome)
                    ->pluck('destination_id')
                    ->toArray();
                $topTrips =  TripPlaces::leftJoin('trips', function ($join) {
                    $join->on('trips_places.trips_id', '=', 'trips.id');
                })->whereNotIn('places_id', $followed_places)
                    ->where('trips.users_id', '<>', Auth::user()->id)
                    ->selectRaw('*, ((select count(*) from `trips_comments` where `trips_id`=`trips_places`.`trips_id`) 
                        + (select count(*) from `trips_likes` where `trips_id`=`trips_places`.`trips_id`) 
                        + (select count(*) from `trips_shares` where `trips_id`=`trips_places`.`trips_id`)) as ranks')
                    ->whereIn('trips_id', get_triplist4me())
                    ->whereDate('trips_places.created_at', '>', Carbon::now()->subDays(30))
                    ->orderBy('ranks', 'DESC')
                    ->where('language_id', $final_outcome)
                    ->limit($limit)
                    ->pluck('places_id')
                    ->toArray();

                $topTravlog = ReportsInfos::leftJoin('reports', function ($join) {
                    $join->on('reports_infos.reports_id', '=', 'reports.id');
                })->where('var', 'place')
                    ->selectRaw('*, ((select count(*) from `reports_comments` where `reports_id`=`reports_infos`.`reports_id`) 
                        + (select count(*) from `reports_likes` where `reports_id`=`reports_infos`.`reports_id`) 
                        + (select count(*) from `reports_views` where `reports_id`=`reports_infos`.`reports_id`) 
                        + (select count(*) from `reports_shares` where `reports_id`=`reports_infos`.`reports_id`)) as ranks')
                    ->whereNotIn('val', $followed_places)
                    ->orderBy('ranks', 'DESC')
                    ->whereDate('reports_infos.created_at', '>', Carbon::now()->subDays(30))
                    ->where('reports.language_id', $final_outcome)
                    ->limit($limit)
                    ->whereNotNull('published_date')
                    ->pluck('var')
                    ->toArray();

                $new = collect($topTravlog)->merge($topTrips)->merge($topDiscussion)->merge($topPosts)->unique()->toArray();
                collect($new)->each(function ($new_id) {
                    if (Place::find($new_id)) {
                        if (!PlaceFollowers::where('places_id', $new_id)->where('users_id', auth()->user()->id)->exists()) {
                            $obj = new PlaceFollowers;
                            $obj->places_id = $new_id;
                            $obj->users_id  = auth()->user()->id;
                            $obj->created_at  = Carbon::now();
                            $obj->save();
                        }
                    }
                });

                $followed_places = auth()->user()->followedPlaces->pluck('places_id')->toArray();
            }

            foreach (Place::take(35)->whereNotIn('id', $followed_places)->inRandomOrder()->pluck('id')->toArray() as $placeId) {
                if (Place::find($placeId)) {
                    $obj = new PlaceFollowers;
                    $obj->places_id = $placeId;
                    $obj->users_id  = auth()->user()->id;
                    $obj->created_at  = Carbon::now();
                    $obj->save();
                }
            }

            $return[self::_DUMP_PLACE_FOLLOWER] = true;
        }

        /*Dump City Follower*/
        if (request()->has(self::_DUMP_CITY_FOLLOWER)) {
            $limit = 25;
            $followed_cities = auth()->user()->followedCities->pluck('cities_id')->toArray();
            if ($user_location_content) {
                $topPosts = PostsCities::leftJoin('posts', function ($join) {
                    $join->on('posts_cities.posts_id', '=', 'posts.id');
                })
                    ->whereNotIn('posts_cities.cities_id', $followed_cities)
                    ->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`posts`.`id`) 
            + (select count(*) from `posts_likes` where `posts_id`=`posts`.`id`) 
            + (select count(*) from `posts_shares` where `posts_id`=`posts`.`id`)) as ranks')
                    ->whereRaw('users_id is not null and posts.permission=0')
                    ->where('posts.users_id', '<>', Auth::user()->id)
                    ->orderBy('ranks', 'desc')
                    ->where('language_id', $final_outcome)
                    ->whereDate('posts.created_at', '>', Carbon::now()->subDays(7))
                    ->limit($limit)
                    ->pluck('posts_cities.cities_id')->toArray();
                $topDiscussion = Discussion::whereNotIn('destination_id', $followed_cities)
                    ->where('destination_type', 'city')
                    ->where('users_id', '<>', Auth::user()->id)
                    ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`) as replies')
                    ->whereRaw('(select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`> 5)')
                    ->orderBy('replies', 'DESC')
                    ->limit($limit)
                    ->where('language_id', $final_outcome)
                    ->whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->pluck('destination_id')
                    ->toArray();
                $topTrips =  TripPlaces::leftJoin('trips', function ($join) {
                    $join->on('trips_places.trips_id', '=', 'trips.id');
                })->whereNotIn('cities_id', $followed_cities)
                    ->where('trips.users_id', '<>', Auth::user()->id)
                    ->selectRaw('*, ((select count(*) from `trips_comments` where `trips_id`=`trips_places`.`trips_id`) 
                        + (select count(*) from `trips_likes` where `trips_id`=`trips_places`.`trips_id`) 
                        + (select count(*) from `trips_shares` where `trips_id`=`trips_places`.`trips_id`)) as ranks')
                    ->whereIn('trips_id', get_triplist4me())
                    ->orderBy('ranks', 'DESC')
                    ->limit($limit)
                    ->where('language_id', $final_outcome)
                    ->whereDate('trips_places.created_at', '>', Carbon::now()->subDays(7))
                    ->pluck('cities_id')
                    ->toArray();

                $topTravlog = Reports::whereNotIn('cities_id',  $followed_cities)
                    ->selectRaw('*, ((select count(*) from `reports_comments` where `reports_id`=`reports`.`id`) 
                    + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id`) 
                    + (select count(*) from `reports_views` where `reports_id`=`reports`.`id`) 
                    + (select count(*) from `reports_shares` where `reports_id`=`reports`.`id`)) as ranks ')
                    ->orderBy('ranks', 'DESC')
                    ->limit($limit)
                    ->whereNotNull('published_date')
                    ->where('language_id', $final_outcome)
                    ->whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->pluck('cities_id')
                    ->toArray();


                $new = collect($topTravlog)->merge($topTrips)->merge($topDiscussion)->merge($topPosts)->unique()->toArray();
                collect($new)->each(function ($new_id) {
                    if (!CitiesFollowers::where('cities_id', $new_id)->where('users_id', auth()->user()->id)->exists()) {
                        $obj = new CitiesFollowers;
                        $obj->cities_id = $new_id;
                        $obj->users_id  = auth()->user()->id;
                        $obj->created_at  = Carbon::now();
                        $obj->save();
                    }
                });

                $followed_cities = auth()->user()->followedCities->pluck('cities_id')->toArray();
            }
            foreach (Cities::take(35)->whereNotIn('id', $followed_cities)->inRandomOrder()->pluck('id')->toArray() as $placeId) {
                $obj = new CitiesFollowers;
                $obj->cities_id = $placeId;
                $obj->users_id  = auth()->user()->id;
                $obj->created_at  = Carbon::now();
                $obj->save();
            }


            $return[self::_DUMP_COUNTRY_FOLLOWER] = true;
        }

        /*Dump Country Follower*/
        if (request()->has(self::_DUMP_COUNTRY_FOLLOWER)) {
            $followed_countries = auth()->user()->followedCountries->pluck('countries_id')->toArray();
            $limit = 25;
            if ($user_location_content) {
                $topPosts = PostsCountries::leftJoin('posts', function ($join) {
                    $join->on('posts_countries.posts_id', '=', 'posts.id');
                })
                    ->where('posts.users_id', '<>', Auth::user()->id)
                    ->whereNotIn('posts_countries.countries_id', $followed_countries)
                    ->selectRaw('*, ((select count(*) from `posts_comments` where `posts_id`=`posts`.`id`) 
          + (select count(*) from `posts_likes` where `posts_id`=`posts`.`id`) 
          + (select count(*) from `posts_shares` where `posts_id`=`posts`.`id`)) as ranks')
                    ->whereRaw('users_id is not null and posts.permission=0')
                    ->orderBy('ranks', 'desc')
                    ->limit($limit)
                    ->where('language_id', $final_outcome)
                    ->whereDate('posts.created_at', '>', Carbon::now()->subDays(7))
                    ->pluck('posts_countries.countries_id')->toArray();
                $topDiscussion = Discussion::whereNotIn('destination_id', $followed_countries)
                    ->where('destination_type', 'country')
                    ->where('users_id', '<>', Auth::user()->id)
                    ->selectRaw('*, (select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`) as replies')
                    ->whereRaw('(select count(*) from `discussion_replies` where `discussions_id`=`discussions`.`id`> 5)')
                    ->orderBy('replies', 'DESC')
                    ->limit($limit)
                    ->where('language_id', $final_outcome)
                    ->whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->pluck('destination_id')
                    ->toArray();
                $topTrips =  TripPlaces::leftJoin('trips', function ($join) {
                    $join->on('trips_places.trips_id', '=', 'trips.id');
                })->whereNotIn('countries_id', $followed_countries)
                    ->where('trips.users_id', '<>', Auth::user()->id)
                    ->selectRaw('*, ((select count(*) from `trips_comments` where `trips_id`=`trips_places`.`trips_id`) 
                      + (select count(*) from `trips_likes` where `trips_id`=`trips_places`.`trips_id`) 
                      + (select count(*) from `trips_shares` where `trips_id`=`trips_places`.`trips_id`)) as ranks')
                    ->whereIn('trips_id', get_triplist4me())
                    ->orderBy('ranks', 'DESC')
                    ->limit($limit)
                    ->where('language_id', $final_outcome)
                    ->whereDate('trips_places.created_at', '>', Carbon::now()->subDays(7))
                    ->pluck('countries_id')
                    ->toArray();

                $topTravlog = Reports::whereNotIn('countries_id',  $followed_countries)
                    ->selectRaw('*, ((select count(*) from `reports_comments` where `reports_id`=`reports`.`id`) 
                  + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id`) 
                  + (select count(*) from `reports_views` where `reports_id`=`reports`.`id`) 
                  + (select count(*) from `reports_shares` where `reports_id`=`reports`.`id`))  as ranks')
                    ->orderBy('ranks', 'DESC')
                    ->limit($limit)
                    ->where('language_id', $final_outcome)
                    ->whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->whereNotNull('published_date')
                    ->pluck('countries_id')
                    ->toArray();

                $new = collect($topTravlog)->merge($topTrips)->merge($topDiscussion)->merge($topPosts)->unique()->toArray();
                collect($new)->each(function ($new_id) {
                    if (!CountriesFollowers::where('countries_id', $new_id)->where('users_id', auth()->user()->id)->exists()) {
                        $obj = new CountriesFollowers;
                        $obj->countries_id = $new_id;
                        $obj->users_id  = auth()->user()->id;
                        $obj->created_at  = Carbon::now();
                        $obj->save();
                    }
                });

                $followed_countries = auth()->user()->followedCountries->pluck('countries_id')->toArray();
            }
            foreach (Countries::take(35)->whereNotIn('id', $followed_countries)->inRandomOrder()->pluck('id')->toArray() as $placeId) {
                $obj = new CountriesFollowers;
                $obj->countries_id = $placeId;
                $obj->users_id  = auth()->user()->id;
                $obj->created_at  = Carbon::now();
                $obj->save();
            }

            $return[self::_DUMP_COUNTRY_FOLLOWER] = true;
        }

        ApiSessionFlush();
        dd($return);
    }
}


// public function algoTest(Request $request)
//     {
//         $authUser = auth()->user();
//         if ($authUser) {
//             if ($request->type) {
//                 switch ($request->type) {
//                     case 'recommendedPlaces':
//                         return $this->recommendedPlaces($authUser);
//                         break;

//                     case 'trendingActivities':
//                         return $this->trendingActivities($authUser);
//                         break;

//                     case 'trendingDestination':
//                         return $this->trendingDestination($authUser);
//                         break;

//                     case 'placesYouMightLike':
//                         return $this->placesYouMightLike($authUser);
//                         break;

//                     case 'discoverNewTravellers':
//                         return $this->discoverNewTravellers($authUser);
//                         break;

//                     case 'videoYouMightLike':
//                         return $this->videoYouMightLike($authUser);
//                         break;

//                     case 'collectivePost':
//                         return $this->collectivePost($authUser);
//                         break;

//                     case 'newFollower':
//                         return $this->newFollower($authUser);
//                         break;

//                     case 'topPlaces':
//                         return $this->topPlaces($authUser);
//                         break;

//                         //single post
//                     case 'weatherUpdateForMyTrips':
//                         return $this->weatherUpdateForMyTrips($authUser);
//                         break;

//                     case 'weatherUpdateForCheckins':
//                         return $this->weatherUpdateForCheckins($authUser);
//                         break;
//                 }
//             } else {
//                 return "type not found";
//             }
//         }
//     }

//     // Done
//     public function trendingDestination($authUser)
//     {

//         // Script For Add Data Algo Wise
//         if (request()->has('add-data')) {
//             for ($i = 0; $i <= 50; $i++) {
//                 $placeId = Place::inRandomOrder()->first()->id;
//                 $userId = User::inRandomOrder()->first()->id;
//                 if (!PlaceFollowers::where('places_id', $placeId)->where('users_id', $userId)->first()) {
//                     $placesFollower = new PlaceFollowers;
//                     $placesFollower->places_id = $placeId;
//                     $placesFollower->users_id  = $userId;
//                     $placesFollower->created_at  = Carbon::now();
//                     $placesFollower->save();
//                 }
//             }
//         }

//         // Algo
//         return PlaceFollowers::select('places_id', DB::raw('COUNT(users_id) AS total_follower'))
//             ->where('created_at', ">=", Carbon::now()->subDays(7))->where('users_id', '!=', $authUser->id)
//             ->groupBy('places_id')->orderBy('total_follower', 'DESC')
//             ->take(50)->get()->map(function ($placeFollowers) {
//                 if ($placeFollowers->place) {
//                     return [
//                         'id'                => $placeFollowers->place->id,
//                         'title'             => $placeFollowers->place->transsingle->title ?? null,
//                         'description'       => !empty(@$placeFollowers->place->transsingle->address) ? $placeFollowers->place->transsingle->address : '',
//                         'img'               => @check_place_photo(@$placeFollowers->place),
//                         'total_followers'   => count($placeFollowers->place->followers),
//                         'place_type'        => do_placetype($placeFollowers->place->place_type ?: 'Event'),
//                         'city'              => $placeFollowers->place->city->transsingle->title ?? null,
//                         'country'           => $placeFollowers->place->city->country->transsingle->title ?? null,
//                     ];
//                 }
//             })->toArray();
//     }

//     // On Hold 
//     public function trendingActivities($authUser)
//     {
//         // SELECT * FROM EVENTS WHERE JSON_EXTRACT(variable, "$.score") < 10 AND JSON_EXTRACT(variable, "$.score") > 9
//         // SELECT *, JSON_EXTRACT(variable, "$.score") AS score FROM events ORDER BY `score` DESC
//         // SELECT *, JSON_EXTRACT(variable, "$.score") AS score FROM events where variable is not null limit 2
//         // SELECT * FROM events where variable is not null limit 2
//         $ip = get_client_ip();
//         $userIds = User::where('ip_address', $ip)->pluck('id');
//         $fromTripPlace = TripPlaces::whereHas('trip', function ($tripQuery) use ($userIds) {
//             $tripQuery->whereIn('users_id', $userIds);
//         })->where('created_at', '>=', Carbon::now()->subDays(120))->orderBy('id', 'DESC')->pluck('cities_id')->toArray();
//         $city_followers = CitiesFollowers::whereIn('users_id', $userIds)
//             ->groupBy('cities_id')->orderBy('id', 'DESC')
//             ->pluck('cities_id')->toArray();
//         $citiesIds = collect($city_followers)->merge($fromTripPlace)->unique()->toArray();
//         $events = Events::WhereIn('cities_id', $citiesIds)
//             ->get()->sortByDesc(function ($event) {
//                 return $event->shares->count();
//             })->take(50)->map(function ($event) {
//                 $returnArr = [
//                     'id'            => $event->id,
//                     'title'         => $event->title,
//                     'description'   => !empty($event->address) ? $event->address : '',
//                     'img'           => asset('assets2/image/events/' . $event->category . '/' . rand(1, 10) . '.jpg'),
//                     'place_type'    => do_placetype(@$event->place->place_type ?: 'Event'),
//                     'category'      => $event->category,
//                     'lat'           => $event->lat,
//                     'lng'           => $event->lng,
//                     'start'         => $event->start,
//                     'end'           => $event->end,
//                     'created_at'    => $event->created_at,
//                 ];
//                 if ($event->places_id) {
//                     return array_merge($returnArr, [
//                         'type'          => 'place',
//                         'location'      => [
//                             'id'    => ($event->place->id) ? $event->place->id : null,
//                             'title' => isset($event->place->transsingle->title) ? $event->place->transsingle->title : null
//                         ]
//                     ]);
//                 } elseif ($event->countries_id) {
//                     return array_merge($returnArr, [
//                         'type'          => 'country',
//                         'location'      => [
//                             'id'    => isset($event->country->id) ? $event->country->id : null,
//                             'title' => isset($event->country->trans[0]->title) ? $event->country->trans[0]->title : null
//                         ]
//                     ]);
//                 } else {
//                     return array_merge($returnArr, [
//                         'type'          => 'city',
//                         'location'      => [
//                             'id'    => $event->city->id,
//                             'title' => isset($event->city->trans[0]->title) ? $event->city->trans[0]->title : null
//                         ]
//                     ]);
//                 }
//             })->toArray();
//         return array_values(collect($events)->toArray());
//     }

//     // Done
//     public function recommendedPlaces($authUser)
//     {
//         // $authUser   = auth()->user();
//         $myTravelStyles  = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();
//         $otherUsersOfSameNationality  = User::where('nationality', $authUser->nationality)->whereNotIn('id', [$authUser->id])->pluck('id')->toArray();

//         // Script For Add Data Algo Wise
//         if (request()->has('add-data')) {
//             if (!(count($myTravelStyles) >= 5)) {
//                 foreach (Lifestyle::take(10)->pluck('id')->toArray() as $ts) {
//                     $usersTravelStyles = new UsersTravelStyles;
//                     $usersTravelStyles->users_id = $authUser->id;
//                     $usersTravelStyles->conf_lifestyles_id = $ts;
//                     $usersTravelStyles->save();
//                 }
//             }
//             $myTravelStyles  = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();
//             foreach ($otherUsersOfSameNationality as $user_id) {
//                 foreach ($myTravelStyles as $ts) {
//                     if (!UsersTravelStyles::where('users_id', $user_id)->where('conf_lifestyles_id', $ts)->first()) {
//                         $usersTravelStyles = new UsersTravelStyles;
//                         $usersTravelStyles->users_id = $user_id;
//                         $usersTravelStyles->conf_lifestyles_id = $ts;
//                         $usersTravelStyles->save();
//                     }
//                 }
//             }
//         }

//         // Algo
//         if ($authUser->nationality) {
//             $list = collect([]);
//             $countriesFollowers = CountriesFollowers::query()
//                 ->select(
//                     'countries_id',
//                     DB::raw('COUNT(users_id) AS total_follower'),
//                     DB::raw('(select count(*) from countries_followers as cf where cf.countries_id = countries_followers.countries_id and cf.users_id not in(' . $authUser->id . ') and cf.users_id in( select id from users where users.nationality in (' . $authUser->nationality . ') ) ) AS county_follower')
//                 )
//                 ->groupBy('countries_id')
//                 ->orderBy('total_follower', 'DESC')
//                 ->whereHas('user', function ($countryQ) use ($authUser) {
//                     $countryQ->where('nationality', $authUser->nationality)->whereNotIn('id', [$authUser->id]);
//                 })
//                 ->having('county_follower', '>=', 10)
//                 ->take(50);


//             $citiesFollowers    = CitiesFollowers::query()
//                 ->select(
//                     'cities_id',
//                     DB::raw('COUNT(users_id) AS total_follower'),
//                     DB::raw('(select count(*) from cities_followers as cf where cf.cities_id = cities_followers.cities_id and cf.users_id not in(' . $authUser->id . ') and cf.users_id in( select id from users where users.nationality in (' . $authUser->nationality . ') ) ) AS city_follower')
//                 )
//                 ->groupBy('cities_id')
//                 ->orderBy('total_follower', 'DESC')
//                 ->whereHas('user', function ($cityQ) use ($authUser) {
//                     $cityQ->where('nationality', $authUser->nationality)->whereNotIn('id', [$authUser->id]);
//                 })
//                 ->having('city_follower', '>=', 10)
//                 ->take(50);

//             if (count($myTravelStyles) > 0) {
//                 $countriesFollowers->whereHas('user.travelstyles', function ($travelstyles) use ($myTravelStyles) {
//                     $travelstyles->whereIn('conf_lifestyles_id', $myTravelStyles);
//                 });
//                 $citiesFollowers->whereHas('user.travelstyles', function ($travelstyles) use ($myTravelStyles) {
//                     $travelstyles->whereIn('conf_lifestyles_id', $myTravelStyles);
//                 });
//             } else {
//                 $countriesFollowers->whereHas('country', function ($country) {
//                     $country->withCount('followers')->orderBy('followers_count', 'DESC');
//                 });
//                 $citiesFollowers->whereHas('city', function ($city) {
//                     $city->withCount('followers')->orderBy('followers_count', 'DESC');
//                 });
//             }

//             $countriesFollowers = $countriesFollowers->whereNotIn('countries_id', $authUser->followingCountries->pluck('countries_id')->toArray())->get();
//             $citiesFollowers    = $citiesFollowers->whereNotIn('cities_id', $authUser->followingCities->pluck('cities_id')->toArray())->get();
//             foreach ([$countriesFollowers, $citiesFollowers] as $objectFollower) {
//                 if ($objectFollower) {
//                     $objectFollower->each(function ($itemFollower) use (&$list, $authUser) {
//                         if (get_class($itemFollower) == CountriesFollowers::class) {
//                             $list->push([
//                                 'type'          => 'country',
//                                 'id'            => $itemFollower->countries_id,
//                                 'title'         => @$itemFollower->country->transsingle->title,
//                                 'description'   => !empty(@$itemFollower->country->transsingle->address) ? @$itemFollower->country->transsingle->address : '',
//                                 'img'           => check_country_photo(@$itemFollower->country->getMedias[0]->url, 180),
//                                 'followers'     => count($itemFollower->country->followers),
//                             ]);
//                         } else {
//                             $list->push([
//                                 'type'          => 'city',
//                                 'id'            => $itemFollower->cities_id,
//                                 'title'         => @$itemFollower->city->transsingle->title,
//                                 'description'   => !empty(@$itemFollower->city->transsingle->address) ? @$itemFollower->city->transsingle->address : '',
//                                 'img'           => check_country_photo(@$itemFollower->city->getMedias[0]->url, 180),
//                                 'followers'     => count($itemFollower->city->followers),
//                                 'country_title' => @$itemFollower->city->country->transsingle->title,
//                             ]);
//                         }
//                     });
//                 }
//             }
//             return $list->sortByDesc('followers')->values()->take(50)->toArray();
//         }
//         return collect([])->toArray();
//     }

//     // Done
//     public function placesYouMightLike($authUser)
//     {
//         // $authUser   = auth()->user();
//         $myTravelStyles  = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();
//         $otherUsersOfSameNationality  = User::where('nationality', $authUser->nationality)->whereNotIn('id', [$authUser->id])->pluck('id')->toArray();

//         // Script For Add Data Algo Wise
//         if (request()->has('add-data')) {
//             if (!(count($myTravelStyles) >= 5)) {
//                 foreach (Lifestyle::take(10)->pluck('id')->toArray() as $ts) {
//                     $usersTravelStyles = new UsersTravelStyles;
//                     $usersTravelStyles->users_id = $authUser->id;
//                     $usersTravelStyles->conf_lifestyles_id = $ts;
//                     $usersTravelStyles->save();
//                 }
//             }
//             $myTravelStyles  = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();
//             foreach ($otherUsersOfSameNationality as $user_id) {
//                 foreach ($myTravelStyles as $ts) {
//                     if (!UsersTravelStyles::where('users_id', $user_id)->where('conf_lifestyles_id', $ts)->first()) {
//                         $usersTravelStyles = new UsersTravelStyles;
//                         $usersTravelStyles->users_id = $user_id;
//                         $usersTravelStyles->conf_lifestyles_id = $ts;
//                         $usersTravelStyles->save();
//                     }
//                 }
//             }
//         }

//         // Algo
//         if ($authUser->nationality) {
//             if (count($otherUsersOfSameNationality) > 0) {
//                 $list = collect([]);
//                 $placeFollowers = PlaceFollowers::query()
//                     ->select('places_id', DB::raw('COUNT(users_id) AS total_follower'))
//                     ->whereIn('users_id', $otherUsersOfSameNationality)
//                     ->groupBy('places_id')
//                     ->orderBy('total_follower', 'DESC')
//                     ->take(50);

//                 if (count($myTravelStyles) > 0) {
//                     $placeFollowers->whereHas('user.travelstyles', function ($travelstyles) use ($myTravelStyles) {
//                         $travelstyles->whereIn('conf_lifestyles_id', $myTravelStyles);
//                     });
//                 } else {
//                     $placeFollowers->whereHas('place', function ($place) {
//                         $place->withCount('followers')->orderBy('followers_count', 'DESC');
//                     });
//                 }
//                 $placeFollowers->whereNotIn('places_id', $authUser->followedPlaces->pluck('places_id')->toArray())
//                     ->get()->each(function ($placeFollower) use (&$list, $authUser) {
//                         $myNationalityTotalFollower =  $placeFollower->whereHas('user', function ($userQuery)  use ($authUser) {
//                             $userQuery->where('nationality', $authUser->nationality);
//                             $userQuery->whereNotIn('id', [$authUser->id]);
//                         })->count();
//                         if ($myNationalityTotalFollower >= 10) {
//                             $place = $placeFollower->place;
//                             $list->push([
//                                 'id'                => $place->id,
//                                 'title'             => @$place->transsingle->title,
//                                 'description'       => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
//                                 'img'               => @check_place_photo(@$place),
//                                 'total_followers'   => count($place->followers),
//                                 'place_type'        => do_placetype(@$place->place_type ?: 'Event'),
//                                 'city'              => @$place->city->transsingle->title,
//                                 'latest_followers'  => $place->followers->take(3)->map(function ($follower) {
//                                     return [
//                                         'id'                => $follower->user->id,
//                                         'name'              => $follower->user->name,
//                                         'profile_picture'   => check_profile_picture($follower->user->profile_picture),
//                                     ];
//                                 })->toArray()
//                             ]);
//                         }
//                     });
//                 return $list->sortByDesc('total_followers')->values()->take(50)->toArray();
//             }
//         }
//         return collect([])->toArray();
//     }

//     // Done
//     public function discoverNewTravellers($authUser)
//     {
//         $userLoc = userLoc($authUser, TRUE);
//         $ip = get_client_ip();
//         $myTravelStyles = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();

//         // #1 & #2
//         $users = User::whereNotNull('profile_picture')
//             ->whereIn('nationality', $userLoc)
//             ->orderBy('created_at', 'DESC')
//             ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
//                 $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
//                     $travelstylesSubQuery->whereIn('conf_lifestyles_id', $myTravelStyles);
//                 });
//             })->take(50)->get();
//         if (count($users) == 0) {
//             // #3 & #4
//             $otherUser = userLoc($authUser, TRUE);
//             $users = User::whereNotNull('profile_picture')
//                 ->whereNotIn('nationality', [$authUser->nationality])
//                 ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
//                     $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
//                         $travelstylesSubQuery->whereIn('conf_lifestyles_id', $myTravelStyles);
//                     });
//                 })
//                 ->when(count($otherUser), function ($q) use ($otherUser) {
//                     $q->orWhereIn('nationality', $otherUser);
//                 })
//                 ->orderBy('created_at', 'DESC')->take(50)->get();
//             if (count($users) == 0) {
//                 // #3 & #4
//                 $users = User::whereNotNull('profile_picture')
//                     ->whereIn('nationality', [$authUser->nationality])
//                     ->when(count($otherUser), function ($q) use ($otherUser) {
//                         $q->orWhereIn('nationality', $otherUser);
//                     })
//                     ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
//                         $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
//                             $travelstylesSubQuery->orWhereIn('conf_lifestyles_id', $myTravelStyles);
//                         });
//                     })
//                     ->orderBy('created_at', 'DESC')->take(50)->get();
//             }
//         }
//         $users = $users->each(function ($user) use ($authUser) {
//             $user->follow_flag      = (UsersFollowers::where('followers_id', $authUser->id)->where('users_id', $user->id)->where('follow_type', 1)->first())  ? true : false;
//             $user->total_follower   = $user->get_followers->count();
//             $user->latest_followers = $user->get_followers->take(3)->map(function ($follower) {
//                 return [
//                     'id' => $follower->followers_id,
//                     'name' => $follower->follower->name,
//                 ];
//             })->toArray();
//             unset($user->get_followers);
//         });
//         return collect($users)->toArray();
//     }

//     // Half Done -> only Tag wise miss
//     public function videoYouMightLike($authUser)
//     {
//         $tempPostsId = Posts::whereDate('created_at', '>', Carbon::now()->subDays(7))->pluck('id')->toArray();
//         $PostsCities = PostsCities::whereIn('posts_id', $tempPostsId)->groupBy('posts_id')->pluck('posts_id');
//         $PostsCountries = PostsCountries::whereIn('posts_id', $tempPostsId)->groupBy('posts_id')->pluck('posts_id');
//         $PostsPlaces = PostsPlaces::whereIn('posts_id', $tempPostsId)->groupBy('posts_id')->pluck('posts_id');
//         $finalPosts = $PostsCities->merge($PostsCountries)->merge($PostsPlaces)->unique()->toArray();
//         $mediaIds = PostsMedia::whereIn('posts_id', $finalPosts)->pluck('medias_id')->unique()->toArray();
//         $medias =  Medias::selectRaw('*, ((select count(*) from `medias_comments` where `medias_id`=`medias`.`id`) 
//             + (select count(*) from `medias_likes` where `medias_id`=`medias`.`id`) 
//             + (select count(*) from `medias_shares` where `medias_id`=`medias`.`id`)
//             + (SELECT COUNT(*) FROM `post_tags` WHERE `posts_type`="trips_places_media_comment" AND `posts_id` =`medias`.`id`)) as rate')
//             ->whereHas('users')
//             ->whereRaw('users_id is not null')
//             ->where('url', 'like', '%.mp4')
//             ->whereIn('id', $mediaIds)
//             ->orderBy('rate', 'desc')
//             ->limit(50)
//             ->get()
//             ->filter(function ($media) {
//                 $user = User::find($media->users_id);
//                 if ($user)
//                     return [
//                         'id'                => $media->id,
//                         'title'             => $media->title,
//                         'url'               => check_media_video($media->url),
//                         'uploaded_at'       => $media->uploaded_at,
//                         'source_url'        => check_cover_photo($media->source_url),
//                         'post_by'           => [
//                             'id'                => $media->users_id,
//                             'name'              => $user->name,
//                             'profile_picture'   => check_profile_picture($user->profile_picture),
//                         ]
//                     ];
//             })->toArray();
//         $mediasCollect = collect($medias);
//         if ($mediasCollect->pluck('post_by.id')->unique()->count() >= 5) {
//             return $mediasCollect;
//         }
//         return collect([])->toArray();
//     }

//     public function collectivePost($authUser)
//     {
//         $lastDays   = 30;
//         $result     = collect([]);
//         $checkUsers = collect([]);
//         $userProfileService = app(UserProfileService::class);
//         $friends    = $userProfileService->getFriendIds($authUser->id)->toArray();
//         $followers  = $userProfileService->getFollowerIds($authUser->id)->toArray();
//         $otherUsers = collect($followers)->merge($friends)->unique()->toArray();
//         $findFollowers  = User::whereIn('id', $followers)->take(5)->get();

//         PlaceFollowers::query()
//             ->whereIn('users_id', $otherUsers)
//             ->whereNotIn('users_id', [$authUser->id])
//             ->whereNotIn('places_id', $authUser->followedPlaces->pluck('places_id')->toArray())
//             ->groupBy('places_id')
//             ->where('created_at', ">=", Carbon::now()->subDays($lastDays))
//             ->take(30)
//             ->orderBy('created_at', 'DESC')
//             ->get()->each(function ($placeFollowers) use ($result, $checkUsers, $authUser) {
//                 if ($place = $placeFollowers->place) {
//                     $checkUsers->push(['users_id' => $placeFollowers->users_id]);

//                     $recent_followers_arr = [];
//                     if ($recent_followers = $place->followers->take(3)) {
//                         foreach ($recent_followers as $followeObj) {
//                             $recent_followers_arr[] = [
//                                 'id' => $followeObj->users_id,
//                                 'profile_picture' => check_profile_picture($followeObj->user->profile_picture),
//                             ];
//                         }
//                     }

//                     $result->push([
//                         'type'          => 'place',
//                         'id'            => $place->id,
//                         'title'         => @$place->transsingle->title,
//                         'description'   => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
//                         'img'           => check_place_photo(@$place),
//                         'country_title' => @$place->country->transsingle->title,
//                         'total_followers'   => count($place->followers),
//                         'recent_followers' => $recent_followers_arr
//                     ]);
//                 }
//             });
//         CountriesFollowers::query()
//             ->whereIn('users_id', $otherUsers)
//             ->whereNotIn('users_id', [$authUser->id])
//             ->whereNotIn('countries_id', $authUser->followedCountries->pluck('countries_id')->toArray())
//             ->groupBy('countries_id')
//             ->where('created_at', ">=", Carbon::now()->subDays($lastDays))
//             ->take(30)
//             ->orderBy('created_at', 'DESC')
//             ->get()->each(function ($countriesFollower) use ($result, $checkUsers, $authUser) {
//                 if ($country = $countriesFollower->country) {
//                     $checkUsers->push(['users_id' => $countriesFollower->users_id]);

//                     $recent_followers_arr = [];
//                     if ($recent_followers = $country->followers->take(3)) {
//                         foreach ($recent_followers as $followeObj) {
//                             $recent_followers_arr[] = [
//                                 'id' => $followeObj->users_id,
//                                 'profile_picture' => check_profile_picture($followeObj->user->profile_picture),
//                             ];
//                         }
//                     }

//                     $result->push([
//                         'type'          => 'country',
//                         'id'            => $country->id,
//                         'title'         => @$country->transsingle->title,
//                         'description'   => !empty(@$country->transsingle->address) ? @$country->transsingle->address : '',
//                         'img'           => check_country_photo(@$country->getMedias[0]->url, 180),
//                         'total_followers'   => count($country->followers),
//                         'recent_followers' => $recent_followers_arr
//                     ]);
//                 }
//             });
//         CitiesFollowers::query()
//             ->whereIn('users_id', $otherUsers)
//             ->whereNotIn('users_id', [$authUser->id])
//             ->whereNotIn('cities_id', $authUser->followedCities->pluck('cities_id')->toArray())
//             ->groupBy('cities_id')
//             ->where('created_at', ">=", Carbon::now()->subDays($lastDays))
//             ->take(30)
//             ->orderBy('created_at', 'DESC')
//             ->get()->each(function ($citiesFollower) use ($result, $checkUsers, $authUser) {
//                 if ($city = $citiesFollower->city) {
//                     $checkUsers->push(['users_id' => $citiesFollower->users_id]);

//                     $recent_followers_arr = [];
//                     if ($recent_followers = $city->followers->take(3)) {
//                         foreach ($recent_followers as $followeObj) {
//                             $recent_followers_arr[] = [
//                                 'id' => $followeObj->users_id,
//                                 'profile_picture' => check_profile_picture($followeObj->user->profile_picture),
//                             ];
//                         }
//                     }

//                     $result->push([
//                         'type'          => 'city',
//                         'id'            => $city->id,
//                         'title'         => @$city->transsingle->title,
//                         'description'   => !empty(@$city->transsingle->address) ? @$city->transsingle->address : '',
//                         'img'           => check_city_photo(@$city->getMedias[0]->url),
//                         'country_title' => @$city->country->transsingle->title,
//                         'total_followers'   => count($city->followers),
//                         'recent_followers' => $recent_followers_arr
//                     ]);
//                 }
//             });



//         if (collect($checkUsers)->unique()->pluck('users_id')->count() >= 3) {

//             $latest_followers = [];
//             if ($findFollowers) {
//                 foreach ($findFollowers as $follower) {
//                     $latest_followers[] = [
//                         'name'              => $follower->name,
//                         'profile_picture'   => check_profile_picture($follower->profile_picture)
//                     ];
//                 }
//             }

//             return [
//                 'total_followers'       => count($followers),
//                 'latest_followers'      => $latest_followers,
//                 'data'                  => collect($result)->toArray()
//             ];
//         } else {
//             return collect([])->toArray();
//         }
//     }

//     public function newFollower($authUser)
//     {
//         $users = User::select(
//             'id',
//             'name',
//             'username',
//             'email',
//             'nationality',
//             'gender',
//             'about',
//             'cover_photo',
//             'profile_picture',
//             'birth_date',
//             'age',
//             'created_at',
//             'is_approved',
//             DB::raw('(SELECT count(id) FROM medias where id in ( SELECT medias_id FROM `users_medias` WHERE users_id=users.id)) as avg_count')
//         )
//             ->whereIn('id', $authUser->get_followers->pluck('followers_id')->unique()->toArray())
//             ->whereNotIn('id', $authUser->following_user->pluck('users_id')->unique()->toArray())
//             ->whereHas('my_medias')
//             ->where('created_at', ">=", Carbon::now()->subDays(30))
//             ->orderBy('created_at', 'DESC')
//             ->having('avg_count', '>', 4)
//             ->take(50)->get();
//         foreach ($users as $user) {
//             $user->country = $user->country_of_nationality->transsingle->title;
//             $user->profile_picture = check_profile_picture($user->profile_picture);
//             $user->cover_photo = check_cover_photo($user->cover_photo);
//             $medias = [];
//             $i = 0;
//             if ($user->my_medias) {
//                 foreach ($user->my_medias->take(4) as $mymedia) {
//                     if ($mymedia->media) {
//                         $i++;
//                         if ($i == 4) break;
//                         $medias[] = check_media_url($mymedia->media->url);
//                     }
//                 }
//             }
//             $user->medias = $medias;
//             unset($user->country_of_nationality, $user->my_medias);
//         }
//         $header = collect($users)->map(function ($user) {
//             return [
//                 'id' => $user->id,
//                 'username' => $user->username,
//                 'profile_picture' => $user->profile_picture,
//             ];
//         });
//         if (collect($users)->count() > 3) {
//             return [
//                 'friend' => $header[rand(0, 2)],
//                 'suggestions'  => $users,
//             ];
//         }
//         return [];
//     }

//     public function topPlaces($authUser)
//     {
//         $places_id = getTopPlacesByAuthUser();
//         if (count($places_id) > 4) {
//             $top_places = Place::whereIn('id', $places_id)->get();
//             foreach ($top_places as $tplace) {
//                 $tplace->medias_url = "https://s3.amazonaws.com/travooo-images2/th180/" . @$tplace->medias[0]->url;
//                 $tplace->place_title = @$tplace->trans[0]->title ?? null;
//                 $tplace->place_type = @do_placetype($tplace->place_type) ?? null;
//                 $tplace->city_title = @$tplace->city->trans[0]->title ?? null;
//                 $tplace->country_title = @$tplace->country->trans[0]->title ?? null;
//                 $tplace->is_follow = PlaceFollowers::where('users_id', Auth::user()->id)->where('places_id', $tplace->places_id)->first() ? true : false;
//                 $img_list = [];
//                 if (isset($tplace->place)) :
//                     foreach ($tplace->medias as $pm) :
//                         $img_list[] = $url = "https://s3.amazonaws.com/travooo-images2/th180/" . @$pm->url;
//                     endforeach;
//                 endif;
//                 $tplace->img_list = $img_list;
//                 unset($tplace->trans, $tplace->place_type, $tplace->city, $tplace->country, $tplace->place);
//             }
//             if (count($places_id) > 4)
//                 return $top_places;
//         }
//         return [];
//     }

//     public function weatherUpdateForMyTrips($authUser)
//     {
//         $authUser = auth()->user();
//         $findTripPlace = TripPlaces::whereHas('trip', function ($q) use ($authUser) {
//             $q->where('users_id', $authUser->id);
//             $q->where('memory', 0);
//         })
//             ->where('versions_id', '!=', 0)
//             ->where('date', '<=', Carbon::now()->addDay(2))
//             ->where('date', '>=', Carbon::now())
//             ->orderBy('id', 'DESC')->first();

//         if (isset($findTripPlace) && isset($findTripPlace->city) && isset($findTripPlace->trip->places)) {

//             $trip = TripPlans::find($findTripPlace->trip->id);
//             if ($trip) {
//                 $totalDates = TripPlaces::where('trips_id', $findTripPlace->trip->id)
//                     ->select('date')
//                     ->where('versions_id', '!=', 0)
//                     ->groupBy('date')->get('date')->sortBy('date')->toArray();
//                 $currentDay = 0;
//                 foreach ($totalDates as $day => $date) {
//                     if (Carbon::parse($date['date'])->eq(Carbon::now())) {
//                         $currentDay = $day + 1;
//                     }
//                 }
//                 $total_days = count($totalDates);
//                 $cityies = [];
//                 $citiesIds =  TripPlaces::where('trips_id', $findTripPlace->trip->id)
//                     ->where('date', $findTripPlace->date)
//                     ->where('versions_id', '!=', 0)
//                     ->groupBy('cities_id')->pluck('cities_id')->toArray();
//                 if ($total_days && count($citiesIds)) {
//                     foreach ($citiesIds as $citiesId) {
//                         $city = Cities::find($citiesId);
//                         if ($city) {
//                             $resp = weatherApi(1, [
//                                 'lat' => $city->lat,
//                                 'lng' => $city->lng
//                             ]);
//                             if ($resp && $resp != "null") {
//                                 $resp = json_decode($resp);
//                                 $weatherResp = weatherApi(3, [
//                                     'place_key' => $resp->Key,
//                                     'day'       => '1day'
//                                 ]);
//                                 $get_current_weather = ($weatherResp && $weatherResp != "null") ? json_decode($weatherResp) : null;
//                                 $citiesTotalDates = TripPlaces::where('trips_id', $findTripPlace->trip->id)
//                                     ->where('cities_id', $city->id)
//                                     ->select('date')
//                                     ->whereHas('active_trip')
//                                     ->where('versions_id', '!=', 0)
//                                     ->groupBy('date')
//                                     ->get()->sortBy('date')->toArray();

//                                 $city->title = $city->transsingle->title;
//                                 $city->description = $city->transsingle->description;
//                                 $city->image = check_city_photo(@$city->getMedias[0]->url);
//                                 $city->start_date = $findTripPlace->date;
//                                 $city->end_date = isset($citiesTotalDates[count($citiesTotalDates) - 1]['date']) ? $citiesTotalDates[count($citiesTotalDates) - 1]['date'] : $findTripPlace->date;
//                                 $city->weather = $get_current_weather;
//                                 unset($city->transsingle, $city->getMedias);
//                                 $cityies[] = $city;
//                             }
//                         }
//                     }
//                     unset($findTripPlace->trip->places);
//                     return [
//                         'trip'          => $findTripPlace->trip,
//                         'total_days'    => $total_days,
//                         'current_day'   => $currentDay,
//                         'total_cities'  => count($cityies),
//                         'cities'        => $cityies
//                     ];
//                 }
//             }
//         }
//         return null;
//     }

//     public function weatherUpdateForCheckins($authUser)
//     {
//         $my_checkins = Checkins::where('users_id', $authUser->id)->where('checkin_time', '<=', Carbon::now()->addDay(2))->where('checkin_time', '>=', Carbon::now()->addDay(1))->first();
//         if (isset($my_checkins->place) && isset($my_checkins)) {
//             $resp = weatherApi(1, [
//                 'lat' =>  $my_checkins->place->lat,
//                 'lng' =>  $my_checkins->place->lng
//             ]);
//             if ($resp && $resp != "null") {
//                 $resp = json_decode($resp);
//                 $daily_weather = json_decode(weatherApi(3, [
//                     'place_key' => $resp->Key,
//                     'day' => '10day'
//                 ]));
//                 if (get_class($daily_weather) == stdClass::class) {
//                     $days = [];
//                     $day = 0;
//                     foreach ($daily_weather->DailyForecasts as $dayData) {
//                         if ($day == 7) continue;
//                         $days[] = $dayData;
//                         $day++;
//                     }
//                     $daily_weather->DailyForecasts  = $days;

//                     $my_checkins->place->title = $my_checkins->place->transsingle->title ?? null;
//                     $my_checkins->place->description = $my_checkins->place->transsingle->description ?? null;
//                     unset($my_checkins->place->transsingle);
//                     $my_checkins->place->place_type = do_placetype($my_checkins->place->place_type);
//                     $my_checkins->place->image = @check_place_photo($my_checkins->place);
//                     $my_checkins->total_likes = $my_checkins->likes->count();
//                     $my_checkins->like_flag = ($my_checkins->likes->where('users_id', $authUser->id)->first()) ? true : false;
//                     $my_checkins->total_comments = $my_checkins->postComments->where('parents_id', 0)->count();
//                     $latest_commenter = [];
//                     foreach ($my_checkins->postComments->take(3) as $comment) {
//                         if (isset($comment->author))
//                             $latest_commenter[] = check_profile_picture($comment->author->profile_picture);
//                     }
//                     $my_checkins->latest_commenter = $latest_commenter;
//                     unset($my_checkins->postComments, $my_checkins->likes);
//                     return [
//                         'weather' => $daily_weather,
//                         'checkin' => $my_checkins
//                     ];
//                 }
//             }
//         }
//         return null;
//     }
