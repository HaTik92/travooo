<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Resources\ApiLoginResource;
use App\Http\Constants\CommonConst;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\FacebookProfileRequest;
use App\Http\Requests\Api\User\TwitterProfileRequest;
use App\Http\Requests\Api\User\GoogleProfileRequest;
use App\Http\Requests\Api\User\AppleProfileRequest;
use App\Http\Responses\ApiResponse;
use App\Models\User\ApiUser as User;
use Carbon\Carbon;
use App\Services\Users\UsersService;
use App\Services\Users\RegistrationService;
use App\Services\Experts\ExpertsService;
use Socialite;
use Exception;
use Illuminate\Support\Facades\Auth;

class SocialController extends Controller
{

    /**
     * Initialize Setting
     */
    public function __construct()
    {
        // facebook
        $this->fbClientID           = env('FACEBOOK_CLIENT_ID', ''); // fbAppID
        $this->fbClientSecret       = env('FACEBOOK_CLIENT_SECRET', '');
        $this->fbApi                = "https://graph.facebook.com/v8.0/me";
        $this->fbFields             = "id,name,email,location,age_range,birthday,link,picture{url}";
    }


    /**
     * @OA\Post(
     ** path="/facebook",
     *   tags={"Social Auth"},
     *   summary="facebook login",
     *   operationId="app\Http\Controllers\Api\User\SocialController@facebookLogin",
     *   @OA\Parameter(
     *       name="token",
     *       description="facebook token",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * validate user using facebook access token and login
     * 
     * @param FacebookProfileRequest $request
     * @return ApiResponse 
     *   if Success
     *      -[string] access_token
     *      -[string] token_type
     *      -[string] expires_at
     *      -[User] user
     */
    function facebookLogin(FacebookProfileRequest $request)
    {
        try {
            // Verify Facebook Token First
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => "{$this->fbApi}?fields={$this->fbFields}&access_token={$request->token}",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "GET",
            ]);
            $fbGetResponse = curl_exec($curl);
            $fbErrorResponse = $fbGetResponse = json_decode($fbGetResponse);
            curl_close($curl);

            /* If Any Error Not Get IN Facebook Api Response Then 
            Call If Statment Otherwise Else */
            if (!property_exists($fbGetResponse, 'error') && property_exists($fbGetResponse, 'email')) {
                //Get the user & Verify exists or not
                $user = User::where('email', '=', trim($fbGetResponse->email))->first();
                //Now log in the user if exists
                if ($user) {
                    // Login User 
                    Auth::loginUsingId($user->id);
                    $tokenResult = $user->createToken(CommonConst::OAUTH_CLIENT);
                    /* Set token exipry date */
                    $token = $tokenResult->token;
                    $token->expires_at = Carbon::now()->addWeeks(1);
                    $token->save();

                    if (isset($fbGetResponse->link)) {
                        $user->facebook = $fbGetResponse->link;
                    }
                    if (isset($fbGetResponse->age_range->min)) {
                        $user->age = $fbGetResponse->age_range->min;
                    }
                    if (isset($fbGetResponse->birthday)) {
                        $user->birth_date = date("Y-m-d", strtotime($fbGetResponse->birthday));
                    }
                    if ($user->profile_picture == null && isset($fbGetResponse->picture->data->url)) {
                        $user->profile_picture = $fbGetResponse->picture->data->url;
                    }
                    if ($user->username == null && isset($fbGetResponse->name)) {
                        $user->username = $fbGetResponse->name;
                    }
                    $user->login_type = User::FACEBOOK;
                    $user->social_key = $fbGetResponse->id;
                    $user->save();
                    ApiSessionFlush();

                    $user->is_firsttime = false;

                    // Create Return Response
                    return ApiResponse::create(new ApiLoginResource(["user" => $user, "token" => $tokenResult]));
                } else {
                    $errors = ['message' => ["user is unauthorized."]];
                    return ApiResponse::create($errors, false, ApiResponse::UNAUTHORIZED);
                }
            } else {
                $errors = ['message' => [$fbErrorResponse->error->message]];
                return ApiResponse::create($errors, false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Exception $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/registration/facebook",
     *   tags={"Social Auth"},
     *   summary="facebook registration",
     *   operationId="app\Http\Controllers\Api\User\SocialController@facebookRegistration",
     *   @OA\Parameter(
     *       name="token",
     *       description="facebook token",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * validate user using facebook access token and register
     * 
     * @param FacebookProfileRequest $request
     * @return ApiResponse 
     *   if Success
     *      -[string] token
     *      -[boolean] is_invited
     *      -[int] points
     *      -[User] user
     */
    function facebookRegistration(FacebookProfileRequest $request, UsersService $usersService, RegistrationService $registrationService, ExpertsService $expertsService)
    {
        try {
            // Verify Facebook Token First
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => "{$this->fbApi}?fields={$this->fbFields}&access_token={$request->token}",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "GET",
            ]);
            $fbGetResponse = curl_exec($curl);
            $fbErrorResponse = $fbGetResponse = json_decode($fbGetResponse);
            curl_close($curl);

            /* If Any Error Not Get IN Facebook Api Response Then 
            Call If Statment Otherwise Else */
            if (!property_exists($fbGetResponse, 'error') && property_exists($fbGetResponse, 'email')) {
                /* First Check IF User Is Exist or Not */
                $chkUser = User::where('email', '=', trim($fbGetResponse->email))->first();
                if (!$chkUser) {
                    $userArr = [
                        'email'         => trim($fbGetResponse->email),
                        'login_type'    => User::FACEBOOK,
                    ];
                    if (isset($fbGetResponse->id)) {
                        $userArr['social_key'] = $fbGetResponse->id;
                    }
                    if (isset($fbGetResponse->name)) {
                        $userArr['username'] = $fbGetResponse->name;
                    }
                    if (isset($fbGetResponse->birthday)) {
                        $userArr['birth_date'] = date("Y-m-d", strtotime($fbGetResponse->birthday));
                    }
                    if (isset($fbGetResponse->age_range->min)) {
                        $userArr['age'] = $fbGetResponse->age_range->min;
                    }
                    if (isset($fbGetResponse->link)) {
                        $userArr['facebook'] = $fbGetResponse->link;
                    }
                    if (isset($fbGetResponse->picture->data->url)) {
                        $userArr['profile_picture'] = $fbGetResponse->picture->data->url;
                    }
                    $user = $usersService->create($userArr);

                    // $registrationService->sendConfirmationEmail($user);
                    $content = [
                        'token' => $registrationService->generateRegistrationToken($user, null),
                        'is_invited' => $user->type === \App\Models\User\User::TYPE_INVITED_EXPERT,
                        'points' => $expertsService->getPresetPoints($user)
                    ];

                    return ApiResponse::create($content);
                } else {
                    $errors = ['message' => ['This Account Already Exist.']];
                    return ApiResponse::create($errors, false, ApiResponse::BAD_REQUEST);
                }
            } else {
                $errors = ['message' => [$fbErrorResponse->error->message]];
                return ApiResponse::create($errors, false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Exception $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/twitter",
     *   tags={"Social Auth"},
     *   summary="twitter login",
     *   operationId="app\Http\Controllers\Api\User\SocialController@twitterLogin",
     *   *   @OA\Parameter(
     *       name="email",
     *       description="email@gmail.com",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *  @OA\Parameter(
     *       name="userID",
     *       description="User ID",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="userName",
     *       description="User Name",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * validate user using twitter and login 
     * 
     * @param TwitterProfileRequest $request
     * @return ApiResponse 
     *   if Success
     *      -[string] access_token
     *      -[string] token_type
     *      -[string] expires_at
     *      -[User] user
     */
    function twitterLogin(TwitterProfileRequest $request)
    {
        try {
            //Get the user & Verify exists or not
            $user = User::where('email', '=', trim($request->email))->first();
            //Now log in the user if exists
            if ($user) {
                // Login User 
                Auth::loginUsingId($user->id);
                $tokenResult = $user->createToken(CommonConst::OAUTH_CLIENT);
                $token = $tokenResult->token; /* Set token exipry date */
                $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();

                if ($user->username != "") {
                    $user->username = $request->userName;
                }
                $user->login_type = User::TWITTER;
                $user->social_key = $request->userID;
                $user->save();
                ApiSessionFlush();

                $user->is_firsttime = false;

                // Create Return Response
                return ApiResponse::create(new ApiLoginResource(["user" => $user, "token" => $tokenResult]));
            } else {
                $errors = ['message' => ["user is unauthorized."]];
                return ApiResponse::create($errors, false, ApiResponse::UNAUTHORIZED);
            }
        } catch (\Exception $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/registration/twitter",
     *   tags={"Social Auth"},
     *   summary="twitter registration",
     *   operationId="app\Http\Controllers\Api\User\SocialController@twitterRegistration",
     *   *   @OA\Parameter(
     *       name="email",
     *       description="email@gmail.com",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *  @OA\Parameter(
     *       name="userID",
     *       description="User ID",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="userName",
     *       description="User Name",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * validate user using twitter and register 
     * 
     * @param TwitterProfileRequest $request
     * @return ApiResponse 
     *   if Success
     *      -[string] token
     *      -[boolean] is_invited
     *      -[int] points
     *      -[User] user
     */
    function twitterRegistration(TwitterProfileRequest $request, UsersService $usersService, RegistrationService $registrationService, ExpertsService $expertsService)
    {
        try {
            /* First Check IF User Is Exist or Not */
            $chkUser = User::where('email', '=', trim($request->email))->first();
            if (!$chkUser) {

                $userArr = [
                    'email'         => trim($request->email),
                    'login_type'    => User::TWITTER
                ];
                if (isset($request->userID)) {
                    $userArr['social_key'] = $request->userID;
                }
                if (isset($request->userName)) {
                    $userArr['username'] = $request->userName;
                }
                $user = $usersService->create($userArr);
                // $registrationService->sendConfirmationEmail($user);
                $content = [
                    'token' => $registrationService->generateRegistrationToken($user, null),
                    'is_invited' => $user->type === \App\Models\User\User::TYPE_INVITED_EXPERT,
                    'points' => $expertsService->getPresetPoints($user)
                ];
                return ApiResponse::create($content);
            } else {
                $errors = ['message' => ['This Account Already Exist.']];
                return ApiResponse::create($errors, false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Exception $e) {
            return ApiResponse::createServerError($e);
        }
    }




    /**
     * @OA\Post(
     ** path="/google",
     *   tags={"Social Auth"},
     *   summary="Google Login",
     *   operationId="google_login",
     *   @OA\Parameter(
     *       name="email",
     *       description="email@gmail.com",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *  @OA\Parameter(
     *       name="userID",
     *       description="User ID",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="userName",
     *       description="User Name",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *    @OA\Parameter(
     *       name="profile_picture",
     *       description="profile picture",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/

    /**
     * validate user using google and login 
     * 
     * @param GoogleProfileRequest $request
     * @return ApiResponse 
     *   if Success
     *      -[string] access_token
     *      -[string] token_type
     *      -[string] expires_at
     *      -[User] user
     */
    function googleLogin(GoogleProfileRequest $request)
    {
        try {
            //Get the user & Verify exists or not
            $user = User::where('email', '=', trim($request->email))->first();
            //Now log in the user if exists
            if ($user) {
                // Login User 
                Auth::loginUsingId($user->id);
                $tokenResult = $user->createToken(CommonConst::OAUTH_CLIENT);
                $token = $tokenResult->token; /* Set token exipry date */
                $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();

                if ($user->username != "") $user->username = $request->userName;
                if ($request->profile_picture) $user->profile_picture = $request->profile_picture;
                $user->login_type = User::GOOGLE;
                $user->social_key = $request->userID;
                $user->save();
                ApiSessionFlush();
                $user->is_firsttime = false;
                // Create Return Response
                return ApiResponse::create(new ApiLoginResource(["user" => $user, "token" => $tokenResult]));
            } else {
                $errors = ['message' => ["user is unauthorized."]];
                return ApiResponse::create($errors, false, ApiResponse::UNAUTHORIZED);
            }
        } catch (\Exception $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Post(
     ** path="/registration/google",
     *   tags={"Social Auth"},
     *   summary="Google Registration",
     *   operationId="google_registration",
     *   @OA\Parameter(
     *       name="email",
     *       description="email@gmail.com",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *  @OA\Parameter(
     *       name="userID",
     *       description="User ID",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="userName",
     *       description="User Name",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *  @OA\Parameter(
     *       name="profile_picture",
     *       description="profile picture",
     *       in="query",
     *       required=false,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/

    /**
     * validate user using google and register 
     * 
     * @param GoogleProfileRequest $request
     * @return ApiResponse 
     *   if Success
     *      -[string] token
     *      -[boolean] is_invited
     *      -[int] points
     *      -[User] user
     */
    function googleRegistration(GoogleProfileRequest $request, UsersService $usersService, RegistrationService $registrationService, ExpertsService $expertsService)
    {
        try {
            /* First Check IF User Is Exist or Not */
            $chkUser = User::where('email', '=', trim($request->email))->first();
            if (!$chkUser) {

                $userArr = [
                    'email'         => trim($request->email),
                    'login_type'    => User::GOOGLE,
                    'social_key'    => $request->userID
                ];
                if (isset($request->userName)) $userArr['username'] = $request->userName;
                $user = $usersService->create($userArr);
                // $registrationService->sendConfirmationEmail($user);
                $content = [
                    'token' => $registrationService->generateRegistrationToken($user, null),
                    'is_invited' => $user->type === \App\Models\User\User::TYPE_INVITED_EXPERT,
                    'points' => $expertsService->getPresetPoints($user)
                ];
                return ApiResponse::create($content);
            } else {
                $errors = ['message' => ['This Account Already Exist.']];
                return ApiResponse::create($errors, false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Exception $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Post(
     ** path="/apple",
     *   tags={"Social Auth"},
     *   summary="Apple Login",
     *   operationId="apple_login",
     *   @OA\Parameter(
     *       name="email",
     *       description="email@gmail.com",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *  @OA\Parameter(
     *       name="userID",
     *       description="User ID",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="userName",
     *       description="User Name",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/

    /**
     * validate user using apple and login 
     * 
     * @param AppleProfileRequest $request
     * @return ApiResponse 
     *   if Success
     *      -[string] access_token
     *      -[string] token_type
     *      -[string] expires_at
     *      -[User] user
     */
    function appleLogin(AppleProfileRequest $request)
    {
        try {
            //Get the user & Verify exists or not
            $user = User::where('email', trim($request->email))->first();
            //Now log in the user if exists
            if ($user) {
                // Login User 
                Auth::loginUsingId($user->id);
                $tokenResult = $user->createToken(CommonConst::OAUTH_CLIENT);
                $token = $tokenResult->token; /* Set token exipry date */
                $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();

                if ($user->username != "") $user->username = $request->userName;
                $user->login_type = User::APPLE;
                $user->social_key = $request->userID;
                $user->save();
                ApiSessionFlush();
                $user->is_firsttime = false;
                // Create Return Response
                return ApiResponse::create(new ApiLoginResource(["user" => $user, "token" => $tokenResult]));
            } else {
                return ApiResponse::__createUnAuthorizedResponse();
            }
        } catch (\Exception $e) {
            return ApiResponse::createServerError($e);
        }
    }



    /**
     * @OA\Post(
     ** path="/registration/apple",
     *   tags={"Social Auth"},
     *   summary="Apple Registration",
     *   operationId="apple_registration",
     *   @OA\Parameter(
     *       name="email",
     *       description="email@gmail.com",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *  @OA\Parameter(
     *       name="userID",
     *       description="User ID",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="integer"
     *       )
     *   ),
     *   @OA\Parameter(
     *       name="userName",
     *       description="User Name",
     *       in="query",
     *       required=true,
     *       @OA\Schema(
     *            type="string"
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/

    /**
     * validate user using apple and register 
     * 
     * @param AppleProfileRequest $request
     * @return ApiResponse 
     *   if Success
     *      -[string] token
     *      -[boolean] is_invited
     *      -[int] points
     *      -[User] user
     */
    function appleRegistration(AppleProfileRequest $request, UsersService $usersService, RegistrationService $registrationService, ExpertsService $expertsService)
    {
        try {
            /* First Check IF User Is Exist or Not */
            $chkUser = User::where('email', trim($request->email))->first();
            if (!$chkUser) {
                $userArr = [
                    'email'         => trim($request->email),
                    'login_type'    => User::APPLE
                ];
                if (isset($request->userID)) $userArr['social_key'] = $request->userID;
                if (isset($request->userName)) $userArr['username'] = $request->userName;
                $user = $usersService->create($userArr);
                // $registrationService->sendConfirmationEmail($user);
                $content = [
                    'token' => $registrationService->generateRegistrationToken($user, null),
                    'is_invited' => $user->type === \App\Models\User\User::TYPE_INVITED_EXPERT,
                    'points' => $expertsService->getPresetPoints($user)
                ];
                return ApiResponse::create($content);
            } else {
                $errors = ['message' => ['This Account Already Exist.']];
                return ApiResponse::create($errors, false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Exception $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
