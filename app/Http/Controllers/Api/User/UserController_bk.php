<?php

namespace App\Http\Controllers\Api\User;

/* Dependencies */
use App\Helpers\UrlGenerator;
use App\Http\Requests\Api\User\ChangePasswordRequest;
use App\Http\Requests\Api\User\FacebookLoginRequest;
use App\Http\Requests\Api\User\ResetPasswordRequest;
use App\Http\Requests\Api\User\TwitterLoginRequest;
use App\Http\Requests\Api\User\UserAcceptFriendRequest;
use App\Http\Requests\Api\User\UserAddressUpdateRequest;
use App\Http\Requests\Api\User\UserAgeUpdateRequest;
use App\Http\Requests\Api\User\UserBlockRequest;
use App\Http\Requests\Api\User\UserChangeOnlineStatusRequest;
use App\Http\Requests\Api\User\UserCreateStep1Request;
use App\Http\Requests\Api\User\UserCreateStep2Request;
use App\Http\Requests\Api\User\UserCreateStep3Request;
use App\Http\Requests\Api\User\UserCreateStep4Request;
use App\Http\Requests\Api\User\UserCreateStep5Request;
use App\Http\Requests\Api\User\UserDeactivateAccountRequest;
use App\Http\Requests\Api\User\UserFavoritesAddRequest;
use App\Http\Requests\Api\User\UserFavoritesRemoveRequest;
use App\Http\Requests\Api\User\UserForgotRequest;
use App\Http\Requests\Api\User\UserFriendRequestRequest;
use App\Http\Requests\Api\User\UserFullnameChangeRequest;
use App\Http\Requests\Api\User\UserLoginRequest;
use App\Http\Requests\Api\User\UserLogoutRequest;
use App\Http\Requests\Api\User\UserMobileUpdateRequest;
use App\Http\Requests\Api\User\UserNationalityUpdateRequest;
use App\Http\Requests\Api\User\UserUnblockRequest;
use App\Http\Requests\Api\User\UserUnhideContentRequest;
use App\Http\Requests\Api\User\UserUpdateContentPrivacyRequest;
use App\Http\Requests\Api\User\UserUpdateNotificationsRequest;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Country\ApiCountry;
use App\Models\System\Session;
use App\Models\User\Activation;
use App\Models\User\ApiUser as User;
use App\Models\User\ApiUser;
use App\Models\User\UsersBlocks;
use App\Models\User\UsersFavourites;
use App\Models\User\UsersFriendRequests;
use App\Models\User\UsersFriends;
use App\Models\User\UsersHiddenContent;
use App\Models\User\UsersNotificationSettings;
use App\Models\User\UsersPrivacySettings;
use App\Models\User\UsersTravelStyles;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;
use Carbon\Carbon;

/**
 * @resource User
 *
 * All users & membership operations
 */
class UserController_bk extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * @param UserRepository $users
     * @param RoleRepository $roles
     */
    public function __construct()//(UserRepository $users, RoleRepository $roles)
    {
    }

    /**
     * This is the short description [and should be unique as anchor tags link to this in navigation menu]
     *
     * This can be an optional longer description of your API call, used within the documentation.
     *
     */
    public function create(UserCreateStep1Request $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();
        // $post = $request->input();
        // $response = User::validateInputParams($post);

        if (!empty($response)) {
            return $response;
        }

        $response = User::createUser($post);

        if (!empty($response)) {
            return $response;
        }

        return false;
    }

    /**
     * @param UserCreateStep1Request $request
     * @return array
     */
    public function createStep1(UserCreateStep1Request $request)
    {
        /* New Api User */
        $user = new User;

        /* Load Values In Model */
        $user->username = $request->input('username');
        $user->email    = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->status   = User::STATUS_INACTIVE;

        if ($user->save()) {
            log_user_activity('Users', 'registrate', $user->id, 0, $user->id);

            /* Create a Token */ 
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $result = [
                'user' => $user->id,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer'
            ];
            return [
                'success' => true,
                'data' => $result
            ];
        } else {
            //IF USER NOT SAVED
            return [
                'data' => [
                    'error' => 200,
                    'message' => 'Error saving user in DB.',
                ],
                'success' => false
            ];
        }
    }

    /**
     * @param UserCreateStep2Request $request
     * @return array
     */
    public function createStep2(UserCreateStep2Request $request)
    {
        $userId = $request->input('user_id');
        $gender = '';

        $user = User::find($userId);
        if (empty($user)) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'User not found.',
                ],
                'status' => false
            ];
        }

        /* if ($gender != User::GENDER_MALE && $gender != User::GENDER_FEMALE && $gender != User::GENDER_UNSPECIFIED) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Gender can only contain following values (' . User::GENDER_MALE . '-Male, ' . User::GENDER_FEMALE . '-Female, ' . User::GENDER_UNSPECIFIED . '-Unspecified).',
                ],
                'status' => false
            ];
        } */

        $user->name   = $request->input('name');
        $user->gender = $gender;
        $user->age    = $request->input('age');
        $user->status = User::STATUS_INACTIVE;

        if ($user->save()) {
            $activation_model = new Activation;
            $activation_model->user_id = $user->id;
            $activation_model->token = User::generateRandomString(10) . time() . User::generateRandomString(12);
            $activation_model->save();

            $user->sendActivationMessage();

            return [
                'success' => true,
                'data'    => $user->getArrayResponse(),
                'code'    => 200
            ];
        } else {
            return [
                'success' => false,
                'code'    => 400,
                'data'    => 'Error saving data in DB.'
            ];
        }
    }

    /**
     * @param UserCreateStep3Request $request
     * @return array
     */
    public function createStep3(UserCreateStep3Request $request)
    {
        $userId    = $request->input('user_id');
        $countries = $request->input('countries');

        $user = User::find($userId);
        if (empty($user)) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'User not found.',
                ],
                'status' => false
            ];
        }

        if (!isset($countries[0]['id'])) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Countries id not provided in JSON object.',
                ],
                'status' => false
            ];
        } else {
            foreach ($countries as $key => $value) {
                $country = ApiCountry::find($value['id']);
                if (empty($country)) {
                    return [
                        'data' => [
                            'error' => 400,
                            'message' => 'Country not found with id "'.$value['id'].'"',
                        ],
                        'status' => false
                    ];
                }
            }
        }

        foreach ($countries as $key => $value) {
            $model = new UsersFavourites;
            $model->users_id = $userId;
            $model->fav_type = 'country';
            $model->fav_id   = $value['id'];
            $model->save();
        }

        return [
            'success' => true,
            'code'    => 200,
            'data'    => []
        ];
    }

    /**
     * @param UserCreateStep4Request $request
     * @return array
     */
    public function createStep4(UserCreateStep4Request $request)
    {
        $userId = $request->input('user_id');
        $places = $request->input('places');

        $user = User::find($userId);

        if (empty($user)) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        if (!isset($places[0]['id']) || empty($places[0]['id']) || !is_numeric($places[0]['id'])) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Id not provided in places object.',
                ],
                'status' => false
            ];
        }

        foreach ($places as $key => $value) {
            $model = new UsersFavourites;
            $model->users_id = $userId;
            $model->fav_type = 'place';
            $model->fav_id   = $value['id'];
            $model->save();
        }

        return [
            'success' => true,
            'code'    => 200,
            'data'    => []
        ];
    }

    /**
     * @param UserCreateStep5Request $request
     * @return array
     */
    public function createStep5(UserCreateStep5Request $request)
    {
        $userId       = $request->input('user_id');
        $travelStyles = $request->input('travel_styles');

        $user = User::find($userId);

        if (empty($user)) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Wrong user id provided.',
                ],
                'status' => false
            ];
        }

        if (!isset($travelStyles[0]['id']) || empty($travelStyles[0]['id']) || !is_numeric($travelStyles[0]['id'])) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Id not provided in "travel_styles" object.',
                ],
                'status' => false
            ];
        }

        foreach ($travelStyles as $key => $value) {
            $model = UsersTravelStyles::where(['users_id' => $userId,'conf_lifestyles_id' => $value['id']])->first();

            if (empty($model)) {
                $model                      = new UsersTravelStyles;
                $model->users_id            = $userId;
                $model->conf_lifestyles_id  = $value['id'];
                $model->save();
            }
        }

        return [
            'success' => true,
            'code'    => 200,
            'data'    => "Life styles added."
        ];
    }

    /*
    *   Validate User And Create New Session If Not Exists.
    */
    /**
     * @param UserLoginRequest $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function login(UserLoginRequest $request)
    {
        $message = 'Email';
        // Request input contains username, email and password
        if ($request->has(['username', 'email', 'password'])) {
            $credentials = $request->only('email', 'password');
        } // Request input contains username and password
        elseif ($request->has(['username', 'password'])) {
            $credentials = $request->only('username', 'password');
            $message     = 'Username';
        } // Request input contains email and password
        elseif ($request->has(['email', 'password'])) {
            $credentials = $request->only('email', 'password');
        } else {
            $credentials = $request->only('email', 'password');
        }

        if (! $token = JWTAuth::attempt($credentials)) {
            return response()->json([
                'data' => [
                    'error' => 401,
                    'message' => $message.' or password wrong.',
                ],
                'status' => false
            ]);
        }

        if (Auth::user()->status == User::STATUS_INACTIVE) {
            return ApiUser::generateErrorMessage(false, 400, 'Account not verified. Please verify your account through the verification email sent to your email id.');
        }

        if (Auth::user()->status == User::STATUS_DEACTIVE) {
            return ApiUser::generateErrorMessage(false, 400, 'The user is deactivated.');
        }
        
        $session_id = \Illuminate\Support\Facades\Session::getId();
        $user_id = Auth::user()->id;
        
        
        
        //$sess = \App\Models\System\Session::all()->update(array('user_id', $user_id));
        
        $response = $this->respondWithToken($token);

        return $response;
    }

    /**
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'data' => [
                'user' => Auth::user(),
                'token' => $token,
            ],
            'success' => true
        ]);
    }

    /*
    *   Log Out User And Destroy Session.
    */
    /**
     * @param UserLogoutRequest $request
     * @return array
     */
    public function logout(UserLogoutRequest $request)
    {
        $email        = $request->input('email');
        $sessionToken = $request->input('session_token');
        /* Find User With The Provided Email */
        $user = User::where(['email' => $email])->first();

        /* if User Not Found, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Wrong Email Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For The Provided Session Token */
        $session = Session::Find($sessionToken);

        /* If Session Not Found, return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User and Provided User Are Not Same, Return Error */
        if ($session->user_id != $user->id) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Wrong Session Token Or Email Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session Is For The Provided User, Delete The User Session */
        if ($session->delete()) {
            return [
                'data' => [
                    'error' => null,
                    'message' => 'Session Removed Successfully.',
                ],
                'status' => true
            ];
        } else {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Error Removing Session.',
                ],
                'status' => true
            ];
        }
    }

    /*
    *   Send A Link To User's Email To Reset Their Password.
    */
    /**
     * @param UserForgotRequest $request
     * @return array
     */
    public function forgot(UserForgotRequest $request)
    {
        $email        = $request->input('email');

        $user = User::where([ 'email' => $email])->first();

        if (empty($user)) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Email Doesn\'t Exists.',
                ],
                'status' => false
            ];
        }

        $password_reset_token = User::generateRandomString(20) . time() . User::generateRandomString(20);

        $user->password_reset_token = $password_reset_token;
        $user->save();
        $user->sendPasswordResetEmail();

        return [
            'success' => true,
            'data' => [
                'message' => 'An email with password reset link is sent to your email account.',
                'token'   => $user->password_reset_token
            ],
            'code'  => 200
        ];
    }

    /*
    *   Reset "password" Of Session Token's User With The "new_assword".
    */
    /**
     * @param ResetPasswordRequest $request
     * @return array
     */
    public function reset(ResetPasswordRequest $request)
    {
        $user         = User::find(Auth::Id());
        $new_password = $request->input('newpassword');

        if (!preg_match('/^[a-zA-Z0-9._]+$/', $new_password)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Password can only contain alphanumeric characters.',
                ],
                'status' => false
            ];
        }

        if (Hash::check($new_password, $user->password)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'New password cannot be the same as old password, please enter new password.',
                ],
                'status' => false
            ];
        }

        /* Hash New Password Using Bcrypt Encryption */
        $password_hash  = bcrypt($new_password);
        $user->password = $password_hash;
        $user->save();

        return [
            'success' => true,
            'data'    => [
                'message' => 'Password reset successfully.'
            ],
            'code' => 200,
        ];
    }

    /*
    *   Activate The User Associated With The Provided Activation "token".
    */
    /**
     * @param $token
     * @return array
     */
    public function activate($token)
    {
        $activation_model = Activation::where(['token' => $token])->first();

        /* If No Activation Record Found For Provided Token, Return Error */
        if (empty($activation_model)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Token Provided.',
                ],
                'status' => false
            ];
        }

        /* if Associated User Exists For Current Token */
        if (isset($activation_model->user) && !empty($activation_model->user)) {

            /* Get Associated User Record */
            $user = $activation_model->user;

            if ($user->status == User::STATUS_INACTIVE) {
                /* Activate User And Save */
                $user->status = User::STATUS_ACTIVE;

                if (! ($user->save())) {
                    return [
                        'success' => false,
                        'data'    => 'Error saving data in DB.',
                        'code'    => 400
                    ];
                }
            }

            return [
                'success' => true,
                'data'    => 'Account Activated Successfully',
                'code'    => 200
            ];
        }
    }

    /*
    *   Get Information Of The Session's User After Validating "session_token" and "user_id".
    */
    /**
     * @param $userId
     * @param $sessionToken
     * @return array
     */
    public function information($userId, $sessionToken)
    {

        /* Check If Id Is A Valid Integer */
        if (!is_numeric($userId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        /* Find User For Provided "user_id" */
        $user = User::find($userId);

        /* If No User Found For The Provided Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided "session_token" */
        $session = Session::find($sessionToken);

        /* If No Session Found For The Provided Session, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id Doesn't Matches The Provided User Id, Return Authentication Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Cannot Authenticate User.',
                ],
                'status' => false
            ];
        }

        /* Return Success Status, And Users Information On Success */
        return [
            'status' => true,
            'data'   => [
                'user_info' => $user->getArrayResponse(),
            ]
        ];
    }

    /*
    *   Update Fullname of User After Validating "session_token" and "user_id".
    */
    /**
     * @param UserFullnameChangeRequest $request
     * @return array
     */
    public function update_fullname(UserFullnameChangeRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $fullname     = $request->input('fullname');


        /* Find User With The Provided User Id. */
        $user = User::find($userId);

        /* If User Not Found With The Id, Return Error. */
        if (empty($user)) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For The Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id Doesn't Matches The Provided User Id, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        $user->name = $fullname;
        $user->save();

        return [
            'status' => true,
            'data' => [
                'user_info' => $user->getArrayResponse()
            ]
        ];
    }
    /*
    *   Update Mobile Number Of User After Validating "session_token" and "user_id".
    */
    /**
     * @param UserMobileUpdateRequest $request
     * @return array
     */
    public function update_mobile(UserMobileUpdateRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $mobile       = $request->input('mobile');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $user->id) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        if ($mobile[0] != '+') {
            $mobile = '+' . $mobile;
        }

        $user->mobile = $mobile;
        $user->save();

        return [
            'status' => true,
            'data'   => [
                'user_info' => $user->getArrayResponse()
            ]
        ];
    }

    /*
    *   Update Address Of The Session's User After Validating "session_token" and "user_id".
    */
    public function update_address(UserAddressUpdateRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $address      = $request->input('address');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Update Address Of User And Save */
        $user->address = $address;
        $user->save();

        /* Return Success Status And Updated User Information */
        return [
            'status' => true,
            'data'   => [
                'user_info' => $user->getArrayResponse()
            ],
        ];
    }

    /*
    *   Update Age Of The Session's User After Validating "session_token" and "user_id".
    */
    /**
     * @param UserAgeUpdateRequest $request
     * @return array
     */
    public function update_age(UserAgeUpdateRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $age          = $request->input('age');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Update Age Of User And Save */
        $user->age = $age;
        $user->save();

        /* Return Success Status And Updated User Information */
        return [
            'status' => true,
            'data'   => [
                'user_info' => $user->getArrayResponse()
            ],
        ];
    }

    /*
    *   Update Nationality Of The Session's User After Validating "session_token" and "user_id".
    */
    /**
     * @param UserNationalityUpdateRequest $request
     * @return array
     */
    public function update_nationality(UserNationalityUpdateRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $nationality  = $request->input('nationality');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Update User's Nationality And Save */
        $user->nationality = $nationality;
        $user->save();

        /* Return Success Status And User Information */
        return [
            'status' => true,
            'data'   => [
                'user_info' => $user->getArrayResponse()
            ]
        ];
    }

    /*
    *   Get Friends Information Of The Session's User After Validating "session_token" and "user_id".
    */
    /**
     * @param $userId
     * @param $sessionToken
     * @return array
     */
    public function friends($userId, $sessionToken)
    {

        /* If User id Is Not An Integer, Return Error */
        if (!is_numeric($userId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        /* Find User For Provided User Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Container For Friends Information */
        $friendsArr = [];

        /* If Friends List Of User Is Not Empty, Push Information In Array Format in "friends_arr" array */
        if (!empty($user->user_friends)) {
            foreach ($user->user_friends as $key => $value) {
                array_push($friendsArr, User::getArrayFormat($value->friend));
            }
        }

        if (empty($friendsArr)) {
            return [
                'status' => true,
                'data' => [
                    'user_info' => $user->getArrayResponse(),
                    'user_friends' => $friendsArr,
                    'message' => 'No friend exists for this user.'
                ]
            ];
        } else {
            /* Return Success Status, And User Information In Array Format */
            return [
                'status' => true,
                'data' => [
                    'user_info' => $user->getArrayResponse(),
                    'user_friends' => $friendsArr,
                ]
            ];
        }
    }

    /*
    *   Delete A Friend Of The Session's User After Validating "session_token" and "user_id".
    */
    /**
     * @param $userId
     * @param $sessionToken
     * @param $friendsId
     * @return array
     */
    public function delete_friends($userId, $sessionToken, $friendsId)
    {

        /* If User id Is Not An Integer, Return Error */
        if (!is_numeric($userId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        /* Find User For Provided User Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If User id Is Not An Integer, Return Error */
        if (!is_numeric($friendsId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Frieds Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        if (!isset($friendsId) || empty($friendsId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Frieds Id not provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find The Relation Of Provided User With Friend Provided */
        $friend = UsersFriends::where(['users_id' => $userId, 'friends_id' => $friendsId])->first();

        /* If Friend Not Found For Provided Friends Id, Return Error */
        if (empty($friend)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Friends Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Delete Friends Relation From UsersFriends Table */
        $friend->delete();

        return [
            'status' => true,
            'data' => [],
        ];
    }

    /*
    *   Update Profile Image Of The Session's User After Validating "session_token" and "user_id".
    */
    /**
     * @param $userId
     * @param $sessionToken
     * @param Request $request
     * @return array
     */
    public function update_profile_image($userId, $sessionToken, Request $request)
    {

        /* If User id Is Not An Integer, Return Error */
        if (!is_numeric($userId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        /* Find User For Provided User Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Check That Uploads Directory Exists, If Not Create It */
        $path = storage_path() . DIRECTORY_SEPARATOR . 'uploads';
        if (!is_dir($path)) {
            mkdir($path, 0755);
        }

        /* Check That Users Directory Exists, If Not Create It */
        $path .= DIRECTORY_SEPARATOR . 'users';
        if (!is_dir($path)) {
            mkdir($path, 0755);
        }

        /* Check That Provided Users Directory Exists, If Not Create It */
        $path .= DIRECTORY_SEPARATOR . $userId;
        if (!is_dir($path)) {
            mkdir($path, 0755);
        }

        /* Check That Provided Users Profile Directory Exist, If Not Create It */
        $path .= DIRECTORY_SEPARATOR . 'profile';
        if (!is_dir($path)) {
            mkdir($path, 0755);
        }

        $path .= DIRECTORY_SEPARATOR;

        /* If File Is Not Uploaded, Return Error */
        if (!$request->hasFile('picture')) {
            /* If Old Picture Exists For This User, Remove It. */
            if (!empty($user->profile_picture)) {
                if (is_file($path . $user->profile_picture)) {
                    unlink($path . $user->profile_picture);
                }
            }

            return [
                'status' => true,
                'data' => [
                    'message' => 'No image provided.'
                ],
            ];
        }

        /* If Old Picture Exists For This User, Remove It. */
        if (!empty($user->profile_picture)) {
            if (is_file($path . $user->profile_picture)) {
                unlink($path . $user->profile_picture);
            }
        }

        /* Upload File */
        $new_file_name = time() . '_profile_image.' . $request->picture->extension();
        $new_path = '/uploads/users/' . $userId . '/profile';
        $request->picture->storeAs($new_path, $new_file_name);

        /* Save Updated Image Name In User's Table */
        $user->profile_picture = $new_file_name;
        $user->save();
        log_user_activity('profile_picture', 'new', null, 0, $userId);

        /* New Url Of Uploaded Image */
        $image_url = UrlGenerator::GetUploadsUrl() . 'users/' . $userId . '/profile/' . $user->profile_picture;

        return [
            'status' => true,
            'data' => [
                'image_url' => $image_url,
                'message' => 'Image updated successfully.'
            ],
        ];
    }

    /*
    *   Change Password Of The Session's User After Validating "session_token" And "user_id", And Validation *   Provided Passwords.
    */
    /**
     * @param ChangePasswordRequest $request
     * @return array
     */
    public function change_password(ChangePasswordRequest $request)
    {
        $old_password = $request->input('old_password');
        $new_password = $request->input('new_password');

        $user = User::find(Auth::Id());

        if (!Hash::check($old_password, $user->password)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Old Password Does not Match.',
                ],
                'status' => false
            ];
        }

        /* Check If New Password Matches The Required Format */
        if (!preg_match('/^[a-zA-Z0-9._]+$/', $new_password)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Password can only contain alphanumeric characters.',
                ],
                'status' => false
            ];
        }

        if ($new_password == $old_password) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'New password cannot be the same as old password, please enter new password.',
                ],
                'status' => false
            ];
        }

        /* Encode New Password Using bcrypt Encoding Before Saving To Database */
        $user->password = bcrypt($new_password);
        $user->save();
        $user->sendPasswordChangeEmail();

        return [
            'status' => true,
            'data'   => [
                'message' => 'Password Changed Successfully.'
            ],
        ];
    }

    /*
    *   Refresh JWT token, when it is expired
    */
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshJwtToken(Request $request)
    {
        $newToken = JWTAuth::setRequest($request)->parseToken()->refresh();
        return response()->json([
            'data' => [
                'token' => $newToken
            ],
            'success' => true
        ]);
    }

    /*
    *   Get List Of Blocked Users By The Provided Session's User After Validation Session Token And User Id.
    */
    /**
     * @param $userId
     * @param $sessionToken
     * @return array
     */
    public function block_list($userId, $sessionToken)
    {

        /* If User id Is Not An Integer, Return Error */
        if (!is_numeric($userId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        /* Find User For Provided User Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Container For User's Block List */
        $user_blocks_arr = [];

        /* If Users In Block List Exist, Convert Their Information In Array Formati And Store In "user_blocks_arr" array */
        if (!empty($user->user_blocks)) {
            foreach ($user->user_blocks as $key => $value) {
                array_push($user_blocks_arr, User::getArrayFormat($value->block));
            }
        }

        if (!empty($user_blocks_arr)) {
            /* Return Success Status, And Blocked User's List In Data */
            return [
                'status' => true,
                'data' => [
                    'blocked_list' => $user_blocks_arr
                ]
            ];
        } else {
            /* Return Success Status, And Blocked User's List In Data */
            return [
                'status' => true,
                'data' => [
                    'blocked_list' => $user_blocks_arr,
                    'message' => 'No user exists in your block list.'
                ]
            ];
        }
    }

    /*
    *   Unblock A Friend
    */
    /**
     * @param UserUnblockRequest $request
     * @return array
     */
    public function unblock_friend(UserUnblockRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $friendId     = $request->input('friend_id');


        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Friend For The Provided Friend Id */
        $friend = User::find($friendId);

        /* If Frind Not Found For The Provided Friend Id, Return Error */
        if (empty($friend)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Friend Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Relation Of User With The Friend In The UsersBlocks Table */
        $friendRelation = UsersBlocks::where(['users_id' => $userId, 'blocks_id' => $friendId])->first();

        /* If Relation Not Found, Return Error */
        if (empty($friendRelation)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Friend Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Delete Relation Record With Friend */
        $friendRelation->delete();

        /* Return Status True With Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'Friend Unblocked Successfully.'
            ],
        ];
    }

    /*
    *   Show All Hidden Contents
    */
    /**
     * @param $userId
     * @param $sessionToken
     * @return array
     */
    public function hidden_content($userId, $sessionToken)
    {

        /* If User id Is Not An Integer, Return Error */
        if (!is_numeric($userId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        /* Find User For Provided User Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Get All User's Hidden Content */
        $hiddenContent = $user->user_hidden_content;
        /* Container For Array Format Of Content */
        $hiddenContentArr = [];

        /* If Hidden Content For Provided User Is Not Empty, Get Array Format Of Content */
        if (!empty($hiddenContent)) {
            foreach ($hiddenContent as $key => $value) {
                array_push($hiddenContentArr, $value->getArrayResponse());
            }
        }

        if (empty($hiddenContentArr)) {
            $hiddenContentArr = [
                'message' => 'No hidden content found.'
            ];
        }
        /* Return Status True, And Content Data In Response */
        return [
            'success' => true,
            'data' => $hiddenContentArr
        ];
    }

    /*
    *   Change Online Status Api
    */
    /**
     * @param UserChangeOnlineStatusRequest $request
     * @return array
     */
    public function change_online_status(UserChangeOnlineStatusRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $status       = $request->input('status');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        if ($status != Session::ONLINE_STATUS_SHOW && $status != Session::ONLINE_STATUS_HIDE) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Status should be either (' . Session::ONLINE_STATUS_SHOW . ' = show,' . Session::ONLINE_STATUS_HIDE . ' = hide)',
                ],
                'status' => false
            ];
        }

        /* Set Session's Show Status Equal To Status */
        $session->show_status = $status;
        $session->save();

        /* Return Status True, And Message of Status Change Success */
        return [
            'status' => true,
            'data' => [
                'message' => 'Status Changed Successfully'
            ]
        ];
    }

    /*
    *   Unhide Content Api
    */
    /**
     * @param UserUnhideContentRequest $request
     * @return array
     */
    public function unhide_content(UserUnhideContentRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $hiddenId     = $request->input('hidden_id');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Hidden Content Relation In UsersHiddenContent Table */
        $hidden = UsersHiddenContent::where(['id' => $hiddenId, 'users_id' => $userId])->first();

        /* If Relation Not Found, Return Error */
        if (empty($hidden)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Hidden Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Delete The Searched Relation */
        $hidden->delete();

        /* Return Status True, And Success Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'Content Unhidden Successfully'
            ]
        ];
    }

    /*
    *   Deactivate Api
    */
    /**
     * @param UserDeactivateAccountRequest $request
     * @return array
     */
    public function deactivate(UserDeactivateAccountRequest $request)
    {
        $userId               = $request->input('user_id');
        $sessionToken         = $request->input('session_token');
        $password             = $request->input('password');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* If Provided Password's Hash Doesn't Matches Password Hash Of User, Return Error */
        if (!password_verify($password, $user->password)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Password Entered.',
                ],
                'status' => false
            ];
        }

        /* Change Status Of User's Account To Deactive */
        $user->status = User::STATUS_DEACTIVE;

        /* Save The User */
        $user->save();

        /* Send Email Of Deactivate Confirmation To User */
        $user->sendDeactiveConfirmationEmail();

        /* Return Success Status, And Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'Account Deactivated Successfully.'
            ],
        ];
    }

    /*
    *   Update Contact Privacy Api
    */
    /**
     * @param UserUpdateContentPrivacyRequest $request
     * @return array
     */
    public function update_contact_privacy(UserUpdateContentPrivacyRequest $request)
    {
        $userId               = $request->input('user_id');
        $sessionToken         = $request->input('session_token');
        $permissionFriendship = $request->input('permission_friendship_request');
        $permissionFollow     = $request->input('permission_follow');
        $permissionMessage    = $request->input('permission_message');
        $permissionEmailPhone = $request->input('permission_view_email_phone');
        $permissionLastSeen   = $request->input('permission_view_last_seen');
        $permissionAddress    = $request->input('permission_view_address');
        $permissionPost       = $request->input('permission_view_post');
        $permissionFriendlist = $request->input('permission_view_friendlist');
        $permissionAge        = $request->input('permission_view_age');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Set Permission For Friendship Requests */
        if (isset($permissionFriendship) && !empty($permissionFriendship)) {
            $setting = UsersPrivacySettings::where([ 'users_id' => $userId, 'var' => 'permission_friendship_request'])->first();

            if (empty($setting)) {
                $setting           = new UsersPrivacySettings;
                $setting->users_id = $userId;
                $setting->var      = 'permission_friendship_request';
                $setting->val      = $permissionFriendship;
                $setting->save();
            } else {
                $setting->val = $permissionFriendship;
                $setting->save();
            }
        }

        /* Set Permission For Follow */
        if (isset($permissionFollow) && !empty($permissionFollow)) {
            $setting = UsersPrivacySettings::where([ 'users_id' => $userId, 'var' => 'permission_follow'])->first();

            if (empty($setting)) {
                $setting           = new UsersPrivacySettings;
                $setting->users_id = $userId;
                $setting->var      = 'permission_follow';
                $setting->val      = $permissionFollow;
                $setting->save();
            } else {
                $setting->val = $permissionFollow;
                $setting->save();
            }
        }

        /* Set Permission For Messages */
        if (isset($permissionMessage) && !empty($permissionMessage)) {
            $setting = UsersPrivacySettings::where([ 'users_id' => $userId, 'var' => 'permission_message'])->first();

            if (empty($setting)) {
                $setting           = new UsersPrivacySettings;
                $setting->users_id = $userId;
                $setting->var      = 'permission_message';
                $setting->val      = $permissionMessage;
                $setting->save();
            } else {
                $setting->val = $permissionMessage;
                $setting->save();
            }
        }

        /* Set Permission For Email And Phone View */
        if (isset($permissionEmailPhone) && !empty($permissionEmailPhone)) {
            $setting = UsersPrivacySettings::where([ 'users_id' => $userId, 'var' => 'permission_view_email_phone'])->first();

            if (empty($setting)) {
                $setting           = new UsersPrivacySettings;
                $setting->users_id = $userId;
                $setting->var      = 'permission_view_email_phone';
                $setting->val      = $permissionEmailPhone;
                $setting->save();
            } else {
                $setting->val = $permissionEmailPhone;
                $setting->save();
            }
        }

        /* Set Permission For Last Seen */
        if (isset($permissionLastSeen) && !empty($permissionLastSeen)) {
            $setting = UsersPrivacySettings::where([ 'users_id' => $userId, 'var' => 'permission_view_last_seen'])->first();

            if (empty($setting)) {
                $setting           = new UsersPrivacySettings;
                $setting->users_id = $userId;
                $setting->var      = 'permission_view_last_seen';
                $setting->val      = $permissionLastSeen;
                $setting->save();
            } else {
                $setting->val = $permissionLastSeen;
                $setting->save();
            }
        }

        /* Set Permission For View Address */
        if (isset($permissionAddress) && !empty($permissionAddress)) {
            $setting = UsersPrivacySettings::where([ 'users_id' => $userId, 'var' => 'permission_view_address'])->first();

            if (empty($setting)) {
                $setting           = new UsersPrivacySettings;
                $setting->users_id = $userId;
                $setting->var      = 'permission_view_address';
                $setting->val      = $permissionAddress;
                $setting->save();
            } else {
                $setting->val = $permissionAddress;
                $setting->save();
            }
        }

        /* Set Permission For View Post */
        if (isset($permissionPost) && !empty($permissionPost)) {
            $setting = UsersPrivacySettings::where([ 'users_id' => $userId, 'var' => 'permission_view_post'])->first();

            if (empty($setting)) {
                $setting           = new UsersPrivacySettings;
                $setting->users_id = $userId;
                $setting->var      = 'permission_view_post';
                $setting->val      = $permissionPost;
                $setting->save();
            } else {
                $setting->val = $permissionPost;
                $setting->save();
            }
        }

        /* Set Permission For Friend List */
        if (isset($permissionFriendlist) && !empty($permissionFriendlist)) {
            $setting = UsersPrivacySettings::where([ 'users_id' => $userId, 'var' => 'permission_view_friendlist'])->first();

            if (empty($setting)) {
                $setting           = new UsersPrivacySettings;
                $setting->users_id = $userId;
                $setting->var      = 'permission_view_friendlist';
                $setting->val      = $permissionFriendlist;
                $setting->save();
            } else {
                $setting->val = $permissionFriendlist;
                $setting->save();
            }
        }

        /* Set Permission For View Age */
        if (isset($permissionAge) && !empty($permissionAge)) {
            $setting = UsersPrivacySettings::where([ 'users_id' => $userId, 'var' => 'permission_view_age'])->first();

            if (empty($setting)) {
                $setting           = new UsersPrivacySettings;
                $setting->users_id = $userId;
                $setting->var      = 'permission_view_age';
                $setting->val      = $permissionAge;
                $setting->save();
            } else {
                $setting->val = $permissionAge;
                $setting->save();
            }
        }

        return [
            'status' => true,
            'data' => [
                'message' => 'Permissions Updated Successfully'
            ]
        ];
    }

    /*
    *   Update Notification Settings Api
    */
    /**
     * @param UserUpdateNotificationsRequest $request
     * @return array
     */
    public function update_notification_settings(UserUpdateNotificationsRequest $request)
    {
        $userId                 = $request->input('user_id');
        $sessionToken           = $request->input('session_token');
        $messageNotification    = $request->input('message_notification');
        $friendshipNotification = $request->input('friendship_notification');
        $commentNotification    = $request->input('comment_notification');
        $nearbyActivityNot      = $request->input('nearby_activity_notification');
        $travoooAnnouncementNot = $request->input('travooo_announcement_notification');
        $planNotification       = $request->input('plan_notification');
        $friendGroupRequestNot  = $request->input('friend_group_request_notification');
        $followNotification     = $request->input('follow_notification');
        $emailNotification      = $request->input('email_notification');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Change Settings For Message Notifications */
        if (isset($messageNotification) && !empty($messageNotification)) {
            $setting = UsersNotificationSettings::where(['users_id' => $userId, 'var' => 'message_notification'])->first();

            if (empty($setting)) {
                $setting           = new UsersNotificationSettings;
                $setting->users_id = $userId;
                $setting->var      = 'message_notification';
                $setting->val      = $messageNotification;
                $setting->save();
            } else {
                $setting->val = $messageNotification;
                $setting->save();
            }
        }

        /* Change Settings For Friendship Notification */
        if (isset($friendshipNotification) && !empty($friendshipNotification)) {
            $setting = UsersNotificationSettings::where(['users_id' =>$userId, 'var' => 'friendship_notification'])->first();

            if (empty($setting)) {
                $setting           = new UsersNotificationSettings;
                $setting->users_id = $userId;
                $setting->var      = 'friendship_notification';
                $setting->val      = $friendshipNotification;
                $setting->save();
            } else {
                $setting->val = $friendshipNotification;
                $setting->save();
            }
        }

        /* Change Settings For Comment Notifications */
        if (isset($commentNotification) && !empty($commentNotification)) {
            $setting = UsersNotificationSettings::where(['users_id' => $userId, 'var' => 'comment_notification'])->first();

            if (empty($setting)) {
                $setting           = new UsersNotificationSettings;
                $setting->users_id = $userId;
                $setting->var      = 'comment_notification';
                $setting->val      = $commentNotification;
                $setting->save();
            } else {
                $setting->val = $commentNotification;
                $setting->save();
            }
        }

        /* Change Settings For Nearby Activity Notifiations */
        if (isset($nearbyActivityNot) && !empty($nearbyActivityNot)) {
            $setting = UsersNotificationSettings::where(['users_id' => $userId, 'var' => 'nearby_activity_notification'])->first();

            if (empty($setting)) {
                $setting           = new UsersNotificationSettings;
                $setting->users_id = $userId;
                $setting->var      = 'nearby_activity_notification';
                $setting->val      = $nearbyActivityNot;
                $setting->save();
            } else {
                $setting->val = $nearbyActivityNot;
                $setting->save();
            }
        }

        /* Change Settings For Travoo Announcement Notifications */
        if (isset($travoooAnnouncementNot) && !empty($travoooAnnouncementNot)) {
            $setting = UsersNotificationSettings::where(['users_id' => $userId, 'var' => 'travooo_announcement_notification'])->first();

            if (empty($setting)) {
                $setting           = new UsersNotificationSettings;
                $setting->users_id = $userId;
                $setting->var      = 'travooo_announcement_notification';
                $setting->val      = $travoooAnnouncementNot;
                $setting->save();
            } else {
                $setting->val = $travoooAnnouncementNot;
                $setting->save();
            }
        }

        /* Change Settings For Plan Notification */
        if (isset($planNotification) && !empty($planNotification)) {
            $setting = UsersNotificationSettings::where(['users_id' => $userId, 'var' => 'plan_notification'])->first();

            if (empty($setting)) {
                $setting           = new UsersNotificationSettings;
                $setting->users_id = $userId;
                $setting->var      = 'plan_notification';
                $setting->val      = $planNotification;
                $setting->save();
            } else {
                $setting->val = $planNotification;
                $setting->save();
            }
        }

        /* Change Settings For Friend Group Request Notification */
        if (isset($friendGroupRequestNot) && !empty($friendGroupRequestNot)) {
            $setting = UsersNotificationSettings::where(['users_id' => $userId, 'var' => 'friend_group_request_notification'])->first();

            if (empty($setting)) {
                $setting           = new UsersNotificationSettings;
                $setting->users_id = $userId;
                $setting->var      = 'friend_group_request_notification';
                $setting->val      = $friendGroupRequestNot;
                $setting->save();
            } else {
                $setting->val = $friendGroupRequestNot;
                $setting->save();
            }
        }

        /* Change Settings For Follow Notification */
        if (isset($followNotification) && !empty($followNotification)) {
            $setting = UsersNotificationSettings::where(['users_id' => $userId, 'var' => 'follow_notification'])->first();

            if (empty($setting)) {
                $setting           = new UsersNotificationSettings;
                $setting->users_id = $userId;
                $setting->var      = 'follow_notification';
                $setting->val      = $followNotification;
                $setting->save();
            } else {
                $setting->val = $followNotification;
                $setting->save();
            }
        }

        /* Change Settings For Email Notification */
        if (isset($emailNotification) && !empty($emailNotification)) {
            $setting = UsersNotificationSettings::where(['users_id' => $userId, 'var' => 'email_notification'])->first();

            if (empty($setting)) {
                $setting           = new UsersNotificationSettings;
                $setting->users_id = $userId;
                $setting->var      = 'email_notification';
                $setting->val      = $emailNotification;
                $setting->save();
            } else {
                $setting->val = $emailNotification;
                $setting->save();
            }
        }

        /* Return Success Status, And Success Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'Notification Settings Updated Successfully.'
            ]
        ];
    }

    /* Tag Friends Api */
    /**
     * @param $userId
     * @param $sessionToken
     * @param $query
     * @return array
     */
    public function tag($userId, $sessionToken, $query)
    {

        /* If User id Is Not An Integer, Return Error */
        if (!is_numeric($userId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* If Query's Length Is Not Between 1-100, Return Error */
        if (strlen($query) < 1 || strlen($query) > 100) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Query length should be between (1-100) characters.',
                ],
                'status' => false
            ];
        }

        /* If Query Is Not An Alphanumeric String, Return Error */
        if (!preg_match('/^[a-zA-Z0-9.,_ ]+$/', $query)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Query can only contain alphanumeric characters.',
                ],
                'status' => false
            ];
        }

        /* Container For Friends Information In Array Format */
        $friends_arr = [];

        /* If Friends Exist For Provided User, Return Array Response Of Friends Information */
        if (!empty($user->user_friends)) {
            foreach ($user->user_friends as $key => $value) {
                if (!empty($value->friend)) {
                    $friend = $value->friend;
                    if (strpos(strtolower($friend->name), strtolower($query)) !== false) {
                        array_push($friends_arr, User::getArrayFormat($friend));
                    }
                }
            }
        }

        if (!empty($friends_arr)) {
            /* Return Success Status, Along With Friends Data in "friends" key */
            return [
                'status' => true,
                'data' => [
                    'friends' => $friends_arr
                ]
            ];
        } else {
            /* Return Success Status, Along With Friends Data in "friends" key */
            return [
                'status' => true,
                'data' => [
                    'friends' => $friends_arr,
                    'message' => 'No friends found for the given query.'
                ]
            ];
        }
    }

    /* Send Friend Request Api */
    /**
     * @param UserFriendRequestRequest $request
     * @return array
     */
    public function friend_request(UserFriendRequestRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $friendId     = $request->input('friend_id');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Friend For The Provided Friend Id */
        $friend = User::find($friendId);

        /* If Frind Not Found For The Provided Friend Id, Return Error */
        if (empty($friend)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Friend Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Old Highest Id To Assign Unique Id To New Entry */
        $old_id = UsersFriendRequests::whereRaw('id = (select max(`id`) from `users_friend_requests`)')->first();

        /* If Last Id Not Found, Start From 0 */
        if (!empty($old_id)) {
            $old_id = $old_id->id;
        } else {
            $old_id = 0;
        }

        /* Increment 1 In Last Id And Assign To New Row */
        $old_id++;

        /* Find Previous Friend Request Record For This User Id And Friend Id */
        $friend_request = UsersFriendRequests::where(['to' => $friendId, 'from' => $userId])->first();
        /* If Previous Record Not Found, Create New Entry */
        if (empty($friend_request)) {
            $friend_request = new UsersFriendRequests;

            /* Load Data In UsersFriendRequests Model */
            $friend_request->id     = $old_id;
            $friend_request->from   = $userId;
            $friend_request->to     = $friendId;
            $friend_request->status = UsersFriendRequests::STATUS_PENDING;

            /* Save Record */
            $friend_request->save();
        }

        /* Return Success Status, Along With Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'Friend request sent.'
            ]
        ];
    }

    /* Display Friend Requests Api */
    /**
     * @param $userId
     * @param $sessionToken
     * @return array
     */
    public function my_friend_requests($userId, $sessionToken)
    {

        /* If User id Is Not An Integer, Return Error */
        if (!is_numeric($userId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Container For Array Response */
        $array_response = [];

        /* If User Has Friend Requests, Return Array Response */
        if (!empty($user->my_friend_requests)) {
            foreach ($user->my_friend_requests as $key => $value) {
                array_push($array_response, [
                    'request_id' => $value->id,
                    'from'       => $value->from,
                    'to'         => $value->to,
                    'status'     => $value->status,
                    'created_at' => $value->created_at
                ]);
            }
        }

        if (!empty($array_response)) {
            /* Return Success Status, Along With Friend Requests */
            return [
                'status' => true,
                'data'   => [
                    'friend_requests' => $array_response
                ]
            ];
        } else {
            /* Return Success Status, Along With Friend Requests */
            return [
                'status' => true,
                'data'   => [
                    'friend_requests' => $array_response,
                    'message'         => 'No friend requests found for this user.'
                ]
            ];
        }
    }

    /* Accept Friend Requests Api */
    /**
     * @param UserAcceptFriendRequest $request
     * @return array
     */
    public function accept_friend_request(UserAcceptFriendRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $friendId     = $request->input('friend_id');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Friend For The Provided Friend Id */
        $friend = User::find($friendId);

        /* If Frind Not Found For The Provided Friend Id, Return Error */
        if (empty($friend)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Friend Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Friend Request Record For Provided User Id And Friend Id, With Status STATUS_PENDING */
        $friend_request = UsersFriendRequests::where(['to' => $userId, 'from' => $friendId, 'status' => UsersFriendRequests::STATUS_PENDING])->first();
        /* If Request Not Found, Return Error */
        if (empty($friend_request)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'No pending friend request exists for this friend.',
                ],
                'status' => false
            ];
        }

        /* Update Status Of Friend Request TO Accepted And Save Request */
        $friend_request->status = UsersFriendRequests::STATUS_ACCEPTED;
        $friend_request->save();

        /* Create A UsersFriends Record With User To Friend */
        $user_friend             = new UsersFriends;
        $user_friend->users_id   = $userId;
        $user_friend->friends_id = $friendId;
        $user_friend->save();

        /* Create A UsersFriends Record With Friend To User */
        $user_friend             = new UsersFriends;
        $user_friend->users_id   = $friendId;
        $user_friend->friends_id = $userId;
        $user_friend->save();
        log_user_activity('friend_request', 'accept', $friendId, 0, $userId);

        /* Return Success Status, Along With Success Message in Data */
        return [
            'status' => true,
            'data' => [
                'message' => 'Friend request accepted.'
            ]
        ];
    }

    /* Block User Api */
    /**
     * @param UserBlockRequest $request
     * @return array
     */
    public function block_user(UserBlockRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $blockId      = $request->input('block_id');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find User For The Provided Block Id */
        $block_user = User::find($blockId);

        /* If Block User Not Found For The Provided Block Id, Return Error */
        if (empty($block_user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong block id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Block Record For Provided User Id And Block id */
        $block_record = UsersBlocks::where(['users_id' => $userId, 'blocks_id' => $blockId])->first();

        /* If Record Found. Return Already Blocked Message */
        if (!empty($block_record)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User already blocked.',
                ],
                'status' => false
            ];
        }

        /* Create New Record For Blocked User */
        $block_record = new UsersBlocks;

        /* Load Information In UsersBlocks Record */
        $block_record->users_id  = $userId;
        $block_record->blocks_id = $blockId;

        /* Save Block Record */
        $block_record->save();

        /* Return Success Status, And Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'User blocked successfully.'
            ]
        ];
    }

    /* Show Profile Picture Api */
    /**
     * @param $userId
     * @param $sessionToken
     * @return array
     */
    public function show_profile_picture($userId, $sessionToken)
    {

        /* If User id Is Not An Integer, Return Error */
        if (!is_numeric($userId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        $image_url = '';
        $path      = 'users/' . $user->id . '/profile/' . $user->profile_picture;

        if (is_file(storage_path('uploads' . DIRECTORY_SEPARATOR . $path))) {
            $uploads_url = UrlGenerator::GetUploadsUrl();

            /* get Url For The User's Profile Image */
            $image_url = $uploads_url . $path;
        }


        /* If Url Found, Return In Data, Else Return Not Found Message */
        if (!empty($image_url)) {
            return [
                'status' => true,
                'data' => [
                    'profile_picture_url' => $image_url
                ]
            ];
        } else {
            return [
                'status' => true,
                'data' => [
                    'profile_picture_url' => $image_url,
                    'message' => 'No profile image found for this user.'
                ]
            ];
        }
    }

    /* Add to favourites Api */
    /**
     * @param UserFavoritesAddRequest $request
     * @return array
     */
    public function add_favourites(UserFavoritesAddRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $favId        = $request->input('fav_id');
        $favType      = $request->input('fav_type');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Favourite User For Provided Favourite id */
        $favUser = User::find($favId);

        /* if User Not Found, Return Error */
        if (empty($favUser)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong favourite id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Previous Record For Provided User Id And Favourite Id, In UsersFavourites Model */
        $usersFavourite = UsersFavourites::where([ 'users_id' => $userId, 'fav_id' => $favId])->first();

        /* If Record Not Found, Create New Record */
        if (empty($usersFavourite)) {
            $usersFavourite = new UsersFavourites;

            $usersFavourite->users_id = $userId;
            $usersFavourite->fav_type = $favType;
            $usersFavourite->fav_id   = $favId;

            $usersFavourite->save();
        } else {
            /* If Previous Record Found, Return Error */
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'This user is already in your favourite list.',
                ],
                'status' => false
            ];
        }

        /* Return Success Status, Along With Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'User added to favourites successfully.'
            ]
        ];
    }

    /* Remove Favourites Api */
    /**
     * @param UserFavoritesRemoveRequest $request
     * @return array
     */
    public function remove_favourites(UserFavoritesRemoveRequest $request)
    {
        $userId       = $request->input('user_id');
        $sessionToken = $request->input('session_token');
        $favId        = $request->input('fav_id');

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Favourite User For Provided Favourite id */
        $favUser = User::find($favId);

        /* if User Not Found, Return Error */
        if (empty($favUser)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong favourite id provided.',
                ],
                'status' => false
            ];
        }

        /* Find Record For Provided User Id And Favourite Id In UsersFavourites Relationship Model  */
        $favouriteRecord = UsersFavourites::where([ 'users_id' => $userId, 'fav_id' => $favId])->first();

        /* If Record Not Found, Return Error */
        if (empty($favouriteRecord)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'This user is not in your favourite list.',
                ],
                'status' => false
            ];
        }

        /* Delete The Favourtie Relation Between User And Favourite User */
        $favouriteRecord->delete();

        /* Return Success Status, With Success Message */
        return [
            'status' => true,
            'data' => [
                'message' => 'Favourites is removed from this user.'
            ]
        ];
    }

    /* Show Favourites Api */
    /**
     * @param $userId
     * @param $sessionToken
     * @return array
     */
    public function show_favourites($userId, $sessionToken)
    {

        /* If User id Is Not An Integer, Return Error */
        if (!is_numeric($userId)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'User Id Should Be An Integer.',
                ],
                'status' => false
            ];
        }

        /* Find User For Provided user Id */
        $user = User::find($userId);

        /* If User Not Found For Provided User Id, Return Error */
        if (empty($user)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Find Session For Provided Session Token */
        $session = Session::find($sessionToken);

        /* If Session Not Found For Provided Session Token, Return Error */
        if (empty($session)) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Session Token Provided.',
                ],
                'status' => false
            ];
        }

        /* If Session's User Id And Provided User Id Don't Match, Return Error */
        if ($session->user_id != $userId) {
            return [
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong User Id Provided.',
                ],
                'status' => false
            ];
        }

        /* Container For User's Favourite Items Array Format */
        $userFavouritesArr = [];

        /* If Favourite Items Exist For This User, Get Array Response */
        if (!empty($user->favourites)) {
            foreach ($user->favourites as $key => $value) {
                $fav = User::find($value->fav_id);

                if (!empty($fav)) {
                    array_push($userFavouritesArr, [
                        'fav_type' => $value->fav_type,
                        'fav_info' => $fav->getArrayResponse()
                    ]);
                }
            }
        }

        /* If Favourite Items Exist For This User, Return Success Status, With Favourite Objects In Data */
        if (!empty($userFavouritesArr)) {
            return [
                'status' => true,
                'data' => [
                    'favourites' => $userFavouritesArr
                ]
            ];
        } else {
            /* If Favourite Items Donot Exist For This User, Return Success Status, With "No Favourite item message". */
            return [
                'status' => true,
                'data' => [
                    'favourites' => $userFavouritesArr,
                    'messages' => 'No favourites item found.'
                ]
            ];
        }
    }

    /* Social Media Login */
    /**
     * @param FacebookLoginRequest $request
     * @return array
     */
    public function FacebookSocialLogin(FacebookLoginRequest $request)
    {
        $fbId = $request->input('fbuid');
        $email = $request->input('email');

        $user = User::where(['email' => $email])->first();

        $password = 'fb_123456';

        if (empty($user)) {
            $user = new User();
            $user->username   = 'FB_' . $fbId;
            $user->email      = $email;
            $user->status     = 1;
            $user->password   = bcrypt($password);
            $user->login_type = User::FACEBOOK;
            $user->social_key = $fbId;
        } else {
            $user->username   = 'FB_' . $fbId;
            $user->login_type = User::FACEBOOK;
            $user->social_key = $fbId;
        }

        $user->save();

        $token = JWTAuth::fromUser($user);

        return [
            'data' => [
                'user'  => $user->getArrayResponse(),
                'token' => $token,
                'type'  => 'register'
            ],
            'success' => true,
            'code'    => 200
        ];
    }

    /* Twitter Login Api */
    /**
     * @param TwitterLoginRequest $request
     * @return array
     */
    public function TwitterSocialLogin(TwitterLoginRequest $request)
    {
        $twuid = $request->input('twuid');
        $email = $request->input('email');
        $name = $request->input('name');

        $user = User::where(['email' => $email])->first();

        if (empty($user)) {
            $user = new User();
            $user->username   = 'Tw_' . $twuid;
            $user->email      = $email;
            $user->name       = $name;
            $user->status     = 1;
            $user->password   = sha1('SC_123456');
            $user->login_type = User::TWITTER;
            $user->social_key = $twuid;

            $user->createActivationEntry();
            $user->sendActivationMessage();
        } else {
            $user->username   = 'TW_' . $twuid;
            $user->login_type = User::TWITTER;
            $user->social_key = $twuid;
        }
        $token = JWTAuth::fromUser($user);
        $user->save();

        return [
            'data' => [
                'user'  => $user->getArrayResponse(),
                'token' => $token,
                'type'  => 'register'
            ],
            'success' => true,
            'code'    => 200
        ];
    }

    /* Twitter Login Page */
    /**
     * @return array
     */
    public function TwitterSocialLoginPage()
    {
        set_time_limit(0);
        require_once base_path().'/vendor/autoload.php';

        session_start();
         
        $config = require_once base_path().'/config/config.php';

        $config = config('config');

        try {
            // create TwitterOAuth object
            $twitteroauth = new TwitterOAuth($config['consumer_key'], $config['consumer_secret']);
             
            // request token of application
            $request_token = $twitteroauth->oauth(
                'oauth/request_token',
                [
                    'oauth_callback' => $config['url_callback']
                ]
            );
             
            // throw exception if something gone wrong
            if ($twitteroauth->getLastHttpCode() != 200) {
                return [
                    'success' => false,
                    'data'    => ['There was a problem performing this request'],
                    'code'    => 400
                ];
                // throw new \Exception('There was a problem performing this request');
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'code'    => 400,
                'data'    => [$e->getMessage()]//['Login failed. Please try again.']
            ];
        }
            
        // save token of application to session
        $_SESSION['oauth_token'] = $request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
        
        $arr = [
            'oauth_token'        => $request_token['oauth_token'],
            'oauth_token_secret' => $request_token['oauth_token_secret']
        ];

        return [
            'success' => true,
            'data'    => $arr,
            'code'    => 200
        ];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function TwitterSocialLoginSend(Request $request)
    {
        $post   = $request->input();
        $errors = [];

        if (!isset($post['oauth_token']) || empty($post['oauth_token'])) {
            $errors[] = 'OAuth Token not provided.';
        }

        if (!isset($post['oauth_token_secret']) || empty($post['oauth_token_secret'])) {
            $errors[] = 'OAuth Token Secret not provided.';
        }

        if (!isset($post['oauth_verifier']) || empty($post['oauth_verifier'])) {
            $errors[] = 'OAuth verifier not provided.';
        }

        if (!empty($errors)) {
            return [
                'success' => false,
                'code' => 400,
                'data' => $errors
            ];
        }

        require_once base_path().'/vendor/autoload.php';

        session_start();
         
        $config = require_once base_path().'/config/config.php';

        $config = config('config');

        try {
            $connection = new TwitterOAuth(
                $config['consumer_key'],
                $config['consumer_secret'],
                $post['oauth_token'],
                $post['oauth_token_secret']
            );
        } catch (\Exception $e) {
            return [
                'success' => false,
                'code'    => 400,
                'data'    => ['Login failed. Please try again.']//[$e->getMessage()]
            ];
        }
        
        $token = [];

        try {
            // request user token
            $token = $connection->oauth(
                'oauth/access_token',
                [
                    'oauth_verifier' => $post['oauth_verifier']
                ]
            );
        } catch (\Exception $e) {
            return [
                'success' => false,
                'code'    => 400,
                'data'    => ['Login failed. Please try again.']//[$e->getMessage()]
            ];
        }

        if (!empty($token)) {
            try {
                $connection = new TwitterOAuth(
                    $config['consumer_key'],
                    $config['consumer_secret'],
                    $token['oauth_token'],
                    $token['oauth_token_secret']
                );
            } catch (\Exception $e) {
                return [
                    'success' => false,
                    'code'    => 400,
                    'data'    => ['Login failed. Please try again.']//[$e->getMessage()]
                ];
            }
            
            $user = $connection->get("account/verify_credentials", ['include_email' => 'true']);
            
            if (isset($user->id_str) && isset($user->email) && isset($user->name)) {
                $arr = [
                    'twuid' => $user->id_str,
                    'email' => $user->email,
                    'name'  => $user->name
                ];
            
                $res = User::twitter_social_login($arr);
                
                return $res;
            } else {
                return [
                    'success' => false,
                    'code'    => 400,
                    'data'    => ['Error creating user.']
                ];
            }
        } else {
            return [
                'success' => false,
                'code'    => 400,
                'data'    => ['Token not returned from API.']
            ];
        }
    }

    /**
     * @return mixed
     */
    public function redirectToProvider()
    {
        return Socialite::driver('twitter')
            ->redirect();
    }

    /**
     * @return array
     */
    public function handleProviderCallback()
    {
        $twitterUser = Socialite::driver('twitter')->user();
        $user = User::where(['email' => $twitterUser->getEmail()])->first();

        if (empty($user)) {
            $user = new User();
            $user->username   = 'Tw_' . $twitterUser->getId();
            $user->email      = $twitterUser->getEmail();
            $user->name       = $twitterUser->getName();
            $user->status     = 1;
            $user->password   = sha1('SC_123456');
            $user->login_type = User::TWITTER;
            $user->social_key = $twitterUser->getId();

            $user->createActivationEntry();
            $user->sendActivationMessage();
        } else {
            $user->username   = 'TW_' . $twitterUser->getId();
            $user->login_type = User::TWITTER;
            $user->social_key = $twitterUser->getId();
        }
        $token = JWTAuth::fromUser($user);
        $user->save();

        return [
            'data' => [
                'user'  => $user->getArrayResponse(),
                'token' => $token,
                'type'  => 'register'
            ],
            'success' => true,
            'code'    => 200
        ];
    }
}
