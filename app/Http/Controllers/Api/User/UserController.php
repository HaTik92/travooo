<?php

namespace App\Http\Controllers\Api\User;

use App\Events\PlanLogs\PlanLogAddedEvent;
use App\Events\PlanLogs\TestPlanLogAddedEvent;
use App\Events\PlanLogs\TestPrivatePlanLogAddedEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Responses\ApiResponse;
use Carbon\Carbon;
use App\Models\User\ApiUser as User;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Place\Place;
use App\Models\Travelstyles\Travelstyles;
use App\Models\User\UsersFavourites;
use App\Services\Users\UsersService;
use App\Services\Users\RegistrationService;
use App\Services\Experts\ExpertsService;
use App\Services\Users\FavouritesService;
use App\Services\Users\ExpertiseService;
use App\Services\Users\TravelStylesService;
use App\Http\Requests\Api\User\StepOneRequest;
use App\Http\Requests\Api\User\StepConfirmationEmailRequest;
use App\Http\Requests\Api\User\StepFourRequest;
use App\Http\Requests\Api\User\StepFourExpertRequest;
use App\Http\Requests\Api\User\UserLoginRequest;
use App\Http\Requests\Frontend\User\Registration\StepFiveExpertRequest;
use App\Http\Requests\Frontend\User\Registration\StepFiveRequest as RegistrationStepFiveRequest;
use App\Http\Requests\Frontend\User\Registration\StepSixRequest as RegistrationStepSixRequest;
use App\Models\Chat\ChatConversation;
use App\Models\Destinations\GenericTopPlaces;
use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\Place\PlaceFollowers;
use App\Models\PlacesTop\PlacesTop;
use App\Services\Api\LoadedFeedService;
use App\Services\Api\UserProfileService;
use App\Services\Users\LanguagesService;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

//TB-362
class UserController extends Controller
{
    public function __construct()
    {
        try {
            _ApiRequest('signup', '');
        } catch (\Throwable $e) {
        }
    }

    // Start Testing
    public function eventFire(Request $request, $user_id)
    {
        try {
            return ApiResponse::create([
                "message" => "Success",
                "result" => broadcast(new TestPrivatePlanLogAddedEvent($user_id))
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    public function publicEventFire(Request $request)
    {
        try {
            return ApiResponse::create([
                "message" => "Success",
                "result" => broadcast(new TestPlanLogAddedEvent())
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
    // End Testing


    /**
     * @OA\GET(
     ** path="/users/registration/languages",
     *   tags={"Signup Steps"},
     *   summary="Fetch all languages list",
     *   operationId="Api\UserController@getLanguages",
     * @OA\Parameter(
     *        name="search",
     *        description="search for searching | optional",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="token",
     *        description="registration token",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getLanguages(FavouritesService $favouritesService)
    {
        if (!(request()->has('token') && request('token', null))) {
            return ApiResponse::createValidationResponse([
                "token" => ["token must be required."]
            ]);
        }
        $search = trim(request('search') ?? '');
        $languages_spokens = [];
        $additional_languages_spokens = [];
        $userId = request('user_id');
        $selectedCountries =  UsersFavourites::select('fav_id')->where('users_id', $userId)->where('fav_type', FavouritesService::TYPE_COUNTRY)->pluck('fav_id')->toArray();
        $selectedCities = UsersFavourites::select('fav_id')->where('users_id', $userId)->where('fav_type', FavouritesService::TYPE_CITY)->pluck('fav_id')->toArray();

        try {
            // $selectedCountries =  [119, 98, 315];
            // $selectedCities =  [1385];

            collect($selectedCountries)->each(function ($id) use (&$languages_spokens, &$additional_languages_spokens) {
                $location = Countries::find($id);
                if ($location) {
                    $languages_spoken = @$location->languages;
                    $additional_languages_spoken = @$location->additional_languages;
                    if ($languages_spoken) {
                        $languages_spokens[] = $languages_spoken;
                    }
                    if ($additional_languages_spoken) {
                        $additional_languages_spokens[] = $additional_languages_spoken;
                    }
                }
            });

            collect($selectedCities)->each(function ($id) use (&$languages_spokens, &$additional_languages_spokens) {
                $location = Cities::find($id);
                if ($location) {
                    $languages_spoken = (count(@$location->languages_spoken) > 0) ? @$location->languages_spoken : @$location->country->languages;
                    $additional_languages_spoken = (count(@$location->additional_languages) > 0) ? @$location->additional_languages : @$location->country->additional_languages;
                    if ($languages_spoken) {
                        $languages_spokens[] = $languages_spoken;
                    }
                    if ($additional_languages_spoken) {
                        $additional_languages_spokens[] = $additional_languages_spoken;
                    }
                }
            });

            $ids = [];

            collect($languages_spokens)->each(function ($_) use (&$ids) {
                collect($_)->each(function ($__) use (&$ids) {
                    $__ = collect($__)->toArray();
                    if (isset($__['pivot']['languages_spoken_id'])) $ids[] = $__['pivot']['languages_spoken_id'];
                });
            });


            collect($additional_languages_spokens)->each(function ($_) use (&$ids) {
                collect($_)->each(function ($__) use (&$ids) {
                    $__ = collect($__)->toArray();
                    if (isset($__['pivot']['languages_spoken_id'])) $ids[] = $__['pivot']['languages_spoken_id'];
                });
            });
            $ids = collect($ids)->unique()->toArray();
        } catch (\Throwable $th) {
            $ids = [];
        }

        $default = LanguagesSpoken::query()->whereIn('id', [1])->with('transsingle')->get()->map(function ($language) {
            return  [
                'id' => $language->id,
                'text' => @$language->transsingle->title,
                'default_selected' => true
            ];
        });

        $suggested_languages = LanguagesSpoken::query()->whereNotIn('id', [1, 28, 19])
            ->whereIn('id', $ids)
            ->with('transsingle')
            ->when(strlen($search), function ($query) use ($search) {
                $query->whereHas('transsingle', function ($q) use ($search) {
                    $q->where('title', 'like', "%$search%");
                });
            })
            ->limit(30)
            ->get()->map(function ($language) {
                return  [
                    'id' => $language->id,
                    'text' => $language->transsingle->title,
                    'default_selected' => false
                ];
            });

        $regional_languages = [];
        if ((count($suggested_languages) + 1) < 30) {
            $regional_languages = LanguagesSpoken::query()
                ->whereNotIn('id', array_merge($ids, [1, 28, 19]))
                ->with('transsingle')
                ->when(strlen($search), function ($query) use ($search) {
                    $query->whereHas('transsingle', function ($q) use ($search) {
                        $q->where('title', 'like', "%$search%");
                    });
                })
                ->limit(30 - (count($suggested_languages) + 1))
                ->get()->map(function ($language) {
                    return  [
                        'id' => $language->id,
                        'text' => $language->transsingle->title,
                        'default_selected' => false
                    ];
                });
        }
        $regional_languages = collect($regional_languages)->merge($suggested_languages)->sortBy('text')->values();
        return ApiResponse::create(collect($default)->merge($regional_languages));
    }


    /**
     * @OA\POST(
     ** path="/users/registration",
     *   tags={"Signup Steps"},
     *   summary="Create a account [Regular : Step 1 + Expert : Step 1]",
     *   operationId="Api\User\UserController@registrationStepOne",
     *   @OA\Parameter(
     *        name="email",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="password",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="username",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="name",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="birth_date",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="gender",
     *        description="1 = MALE | 2 = FEMALE",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="nationality",
     *        description="county id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="type",
     *        description="1 = TYPE_REGULAR | 2 = TYPE_EXPERT",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="is_back",
     *        description="true | false (if user go back and click submit then pass true)",
     *        in="query",
     *        @OA\Schema(
     *            type="boolean"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Create Account For All Type Of User
     * @param StepOneRequest $request
     * @param UsersService $usersService
     * @param RegistrationService $registrationService
     * @param ExpertsService $expertsService
     * @return ApiResponse
     */
    public function registrationStepOne(
        StepOneRequest $request,
        UsersService $usersService,
        RegistrationService $registrationService,
        ExpertsService $expertsService
    ) {
        try {
            if ($request->nationality) {
                if (!Countries::find($request->nationality)) {
                    return ApiResponse::__createServerError('Nationality is incorrect');
                }
            }

            if (User::where('username', $request->username)->first()) {
                return ApiResponse::createValidationResponse([
                    'username' => ['Username already exists']
                ]);
            }

            // $request->gender = $request->gender + 1; // web & app use diffrent constants 

            $user = $usersService->create($request->only('password', 'name', 'username', 'email', 'birth_date', 'gender', 'nationality', 'type'), false);
            if (!$registrationService->sendConfirmationEmail($user)) {
                $usersService->forceDelete($user);
                return ApiResponse::__createServerError('There are server issue try again later.');
            }

            $usersService->setSteps($user->id, '2');

            return ApiResponse::create([
                'token' => $registrationService->generateRegistrationToken($user, $request->onlyValidated()['password']),
                'is_invited' => $user->type === \App\Models\User\User::TYPE_INVITED_EXPERT,
                'points' => $expertsService->getPresetPoints($user)
            ]);
        } catch (\Throwable $e) {
            if (get_class($e) == QueryException::class) {
                return ApiResponse::__createBadResponse("This account already exist.");
            } else {
                return ApiResponse::createServerError($e);
            }
        }
    }

    /**
     * @OA\POST(
     ** path="/users/registration/Resend-confirmation-code",
     *   tags={"Signup Steps"},
     *   summary="resend confirmation code [Regular + Expert]",
     *   operationId="Api\User\UserController@resendConfirmationCode",
     *   @OA\Parameter(
     *        name="token",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Resend Confirmation Code
     * @param Request $request
     * @param RegistrationService $registrationService
     * @param UsersService $usersService
     * @return ApiResponse
     */
    public function resendConfirmationCode(
        Request $request,
        RegistrationService $registrationService,
        UsersService $usersService
    ) {
        try {
            $userId     = $request->user_id;
            $timeout    = Cache::get('resend_confirmation_code_' . $userId);

            if ($timeout) return ApiResponse::__createValidationResponse("Time Out");

            $attributes = [];
            $usersService->generateConfirmationCode($attributes);
            $usersService->update($userId, $attributes);
            $user = $usersService->find($userId);

            if (!$registrationService->sendConfirmationEmail($user)) {
                return ApiResponse::__createServerError('There are server issue try again later.');
            }

            Cache::put('resend_confirmation_code_' . $userId, 1, 1);

            return ApiResponse::__create("Confirmation code resend successfully.");
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/users/registration/confirmation",
     *   tags={"Signup Steps"},
     *   summary="Code Confirmation [Regular : Step 2 + Expert : Step 2]",
     *   operationId="Api\User\UserController@registrationStepConfirmationEmail",
     *   @OA\Parameter(
     *        name="confirmation_code",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="token",
     *        description="you can get in Step 1",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Confirmation Using Email For All Type Of User
     * @param StepConfirmationEmailRequest $request
     * @param RegistrationService $registrationService
     * @param UsersService $usersService
     * @return ApiResponse
     */
    public function registrationStepConfirmationEmail(
        StepConfirmationEmailRequest $request,
        RegistrationService $registrationService,
        UsersService $usersService,
        ExpertsService $expertsService
    ) {
        try {
            $code = $request->confirmation_code;
            $userId = $request->user_id;

            if (!$registrationService->checkCode($userId, $code))
                return ApiResponse::__createBadResponse(trans('validation.exists', ['attribute' => 'confirmation code']));

            $token = $registrationService->getRegistrationToken($request->get('token'));
            $usersService->create([
                'email'    => $token->getEmail(),
                'password' => $token->getPassword()
            ]);
            $usersService->setIpAddress($userId, $request->ip());
            $usersService->setSteps($userId, '3');

            $user = User::find($userId);
            return ApiResponse::create([
                'is_invited' => $user->type === \App\Models\User\User::TYPE_INVITED_EXPERT,
                'points' => $expertsService->getPresetPoints($user),
                "message" => ["Confirm successfully"]
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\POST(
     ** path="/users/registration/{step_type}",
     *   tags={"Signup Steps"},
     *   summary="Step Updates [Skip | Go Back] , [Regular + Expert ]",
     *   operationId="Api\User\UserController@registrationUpdateSteps",
     *   @OA\Parameter(
     *        name="step_type",
     *        description="skip | back",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="token",
     *        description="you can get in Step 1",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Step Updates [Skip | Go Back]
     * @param Request $request
     * @param UsersService $usersService
     * @param ExpertiseService $expertiseService
     * @param TravelStylesService $travelStylesService
     * @param FavouritesService $favouritesService
     * @return ApiResponse
     */
    public function registrationUpdateSteps(
        Request $request,
        UsersService $usersService,
        ExpertiseService $expertiseService,
        TravelStylesService $travelStylesService,
        FavouritesService $favouritesService,
        LanguagesService $languagesService,
        UserProfileService $userProfileService,
        $step_type
    ) {

        // $userId = 1594;
        // return ApiResponse::create([
        //     $languagesService->__fetchSelectedUserLanguages($userId),
        //     $travelStylesService->__fetchSelectedTravelStyles($userId),
        //     $favouritesService->__fetchFavouritesCountryCity($userId),
        //     $favouritesService->__fetchFavouritesPlace($userId),
        //     $favouritesService->__fetchAreaExpertise($userId),
        //     $userProfileService->findSocialLinks($userId),
        // ]);

        $step_type = trim(strtolower($step_type));
        $userId = $request->user_id;
        $user = User::find($userId);
        $step = $request->step;
        $type = $user->type;

        if ($step <= 1) return ApiResponse::__createBadResponse("Invalid step");

        if ($step_type == 'skip') {
            if ($type == \App\Models\User\User::TYPE_REGULAR) {
                switch ($step) {
                    case 3:
                        $favouritesService->deleteFavourites($userId, 'city');
                        $favouritesService->deleteFavourites($userId, 'country');
                        break;
                    case 4:
                        $favouritesService->deleteFavourites($userId, 'place');
                        break;
                    case 5:
                        $usersService->update($userId, ['interests' => NULL]);
                        $travelStylesService->deleteTravelStyles($userId);
                        break;
                    case 6:
                        $languagesService->deleteUserLanguages($userId);
                        break;
                }
            } else if ($type == \App\Models\User\User::TYPE_EXPERT) {
                switch ($step) {
                    case 4:
                        $favouritesService->deleteFavourites($userId, 'city');
                        $favouritesService->deleteFavourites($userId, 'country');
                        break;
                    case 5:
                        $expertiseService->deleteExpertiseCities($userId);
                        $expertiseService->deleteExpertiseCountries($userId);
                        $expertiseService->deleteExpertisePlaces($userId);
                        break;
                    case 6:
                        $usersService->update($userId, ['interests' => NULL]);
                        $travelStylesService->deleteTravelStyles($userId);
                        break;
                    case 7:
                        $languagesService->deleteUserLanguages($userId);
                        break;
                }
            }

            if ($step >= 2 && $step <= 7) {
                if ($step == 2) $usersService->update($userId, ['confirmed' => 0]);
                $usersService->setSteps($userId, $step);
            }

            if (($step == 7 && $type == \App\Models\User\User::TYPE_EXPERT) || ($step == 6 && $type == \App\Models\User\User::TYPE_REGULAR)) {
                $tokenResult = $user->createToken('TRAVOOO');
                return ApiResponse::create([
                    'message'       => ["Signup successfully."],
                    'user'          => $user->getArrayResponse(),
                    'access_token'  => $tokenResult->accessToken,
                    'token_type'    => 'Bearer',
                    'expires_at'    => Carbon::parse(
                        $tokenResult->token->expires_at
                    )->toDateTimeString(),
                    'register' =>  null
                ]);
                return ApiResponse::__create("Step changed successfully.");
            }
            return ApiResponse::__create("Step changed successfully.");
        } else if ($step_type == 'back') {
            if (($step == 7 && $type == \App\Models\User\User::TYPE_EXPERT) || ($step == 6 && $type == \App\Models\User\User::TYPE_REGULAR)) {
                // UserLanguages
                return ApiResponse::create($languagesService->__fetchSelectedUserLanguages($userId));
            } else if (($step == 6 && $type == \App\Models\User\User::TYPE_EXPERT) || ($step == 5 && $type == \App\Models\User\User::TYPE_REGULAR)) {
                // TravelStyles
                return ApiResponse::create($travelStylesService->__fetchSelectedTravelStyles($userId));
            } else if (($step == 4 && $type == \App\Models\User\User::TYPE_EXPERT) || ($step == 3 && $type == \App\Models\User\User::TYPE_REGULAR)) {
                // country, city
                return ApiResponse::create($favouritesService->__fetchFavouritesCountryCity($userId));
            } else if (($step == 4 && $type == \App\Models\User\User::TYPE_REGULAR)) {
                //place
                return ApiResponse::create($favouritesService->__fetchFavouritesPlace($userId));
            } else if (($step == 5 && $type == \App\Models\User\User::TYPE_EXPERT)) {
                // Area Expertise 
                return ApiResponse::create($favouritesService->__fetchAreaExpertise($userId));
            } else if (($step == 3 && $type == \App\Models\User\User::TYPE_EXPERT)) {
                // social links
                return ApiResponse::create($userProfileService->findSocialLinks($userId));
            }
        }

        return ApiResponse::__createBadResponse("something went wrong");
    }

    /**
     * @OA\POST(
     ** path="/users/registration/city-and-country",
     *   tags={"Signup Steps"},
     *   summary="submit countries or cities that you want to follow [Regular : Step 3 + Expert : Step 4]",
     *   operationId="Api\User\UserController@registrationStepThree",
     *   @OA\Parameter(
     *        name="token",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="cities[]",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *  @OA\Parameter(
     *        name="countries[]",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Select countries or cities that you want to follow
     * @param StepFourRequest $request
     * @param RegistrationService $registrationService
     * @param FavouritesService $favouritesService
     * @param UsersService $usersService
     * @return ApiResponse
     */
    public function registrationStepThree(
        StepFourRequest $request,
        RegistrationService $registrationService,
        FavouritesService $favouritesService,
        UsersService $usersService
    ) {
        try {
            // if (!$registrationService->validateStepFourRequestApi($request))
            //     return ApiResponse::__createBadResponse("Please select at least 3 city/country");

            $totalSelection = collect($request->input('cities', []))->merge($request->input('countries', []))->count();
            if ($totalSelection >= RegistrationService::MIN_SELECTION_FOR_EACH_STEP) {
                $userId = $request->user_id;
                $user = User::find($userId);
                $favouritesService->setFavouritesCities($userId, $request->input('cities', []));
                $favouritesService->setFavouritesCountries($userId, $request->input('countries', []));

                if ($user->type == 1) {
                    $usersService->setSteps($userId, '4');
                } else {
                    $usersService->setSteps($userId, '5');
                }

                return ApiResponse::__create("selected countries or cities save successfully.");
            } else {
                return ApiResponse::__createBadResponse("Please select at least " . RegistrationService::MIN_SELECTION_FOR_EACH_STEP . " city or country");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/users/registration/area-of-expertise",
     *   tags={"Signup Steps"},
     *   summary="Search area expertise [Expert : Step 5]",
     *   operationId="Api\User\UserController@searchAreaExpertise",
     *   @OA\Parameter(
     *        name="search",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="token",
     *        description="you can get in Step 1",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Search area expertise
     * @param Request $request
     * @param UsersService $usersService
     * @param RegistrationService $registrationService
     * @return ApiResponse
     */
    public function searchAreaExpertise(
        Request $request,
        UsersService $usersService,
        RegistrationService $registrationService
    ) {
        $search = $request->search;
        $result = [];
        $entities = collect([]);
        if ($search && $search != '') {
            $cities = Cities::with('country', 'transsingle')->whereHas('transsingle', function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            })->take(15);
            $countries = Countries::with('transsingle')->whereHas('transsingle', function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            })->take(15);

            $collection = $cities->get();
            $entities = $collection->concat($countries->get())->shuffle();

            foreach ($entities as $entity) {
                $result[] = [
                    'id' => get_class($entity) === Cities::class ? 'city-' . $entity->getKey() : 'country-' . $entity->getKey(),
                    'name' => $entity->transsingle->title,
                    'flag' => get_class($entity) === Cities::class ? (@get_country_flag(@$entity->country) ?? null) : get_country_flag($entity)
                ];
            }
        } else {
            $token = $registrationService->getRegistrationToken($request->get('token'));
            if (!$token || !$token->getUserId() || $token->getExpiredDate() < Carbon::create()) {
                return ApiResponse::__createUnAuthorizedResponse("User is unauthorized");
            } else {
                $user = User::where('id', $token->getUserId())->first();
                if ($user) {
                    $result = $usersService->getUserSuggestionLocations($user->nationality);
                    $result = collect($result)->map(function ($item) {
                        $item = json_decode(json_encode($item));
                        return [
                            'id' => $item->id,
                            'name' => $item->name,
                            'flag' => $item->url,
                        ];
                    });
                }
            }
        }

        // sortby name A-Z
        $result = collect($result)->sortBy('name')->take(22)->values();

        return ApiResponse::create($result);
    }

    /**
     * @OA\POST(
     ** path="/users/registration/area-of-expertise",
     *   tags={"Signup Steps"},
     *   summary="Submit Area Of Expertise [Expert : Step 5]",
     *   operationId="Api\User\UserController@registrationStepFiveExpert",
     *   @OA\Parameter(
     *        name="token",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="cities[]",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *  @OA\Parameter(
     *        name="countries[]",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     * @OA\Parameter(
     *        name="places[]",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Submit Area Of Expertise
     * @param StepFiveExpertRequest $request
     * @param RegistrationService $registrationService
     * @param ExpertiseService $expertiseService
     * @param UsersService $usersService
     * @return ApiResponse
     */
    public function registrationStepFiveExpert(
        StepFiveExpertRequest $request,
        RegistrationService $registrationService,
        ExpertiseService $expertiseService,
        UsersService $usersService
    ) {
        try {
            // if (!$registrationService->validateStepFiveExpertRequest($request)) {
            //     return ApiResponse::__createBadResponse("Please select at least 3 city or country");
            // }
            $totalSelection = collect($request->input('cities', []))->merge($request->input('countries', []))->merge($request->input('places', []))->count();
            if ($totalSelection >= RegistrationService::MIN_SELECTION_FOR_EACH_STEP) {
                $userId = $request->user_id;
                $expertiseService->setExpertiseCities($userId, $request->input('cities', []));
                $expertiseService->setExpertiseCountries($userId, $request->input('countries', []));
                $expertiseService->setExpertisePlaces($userId, $request->input('places', []));
                $usersService->setSteps($userId, '6');
                return ApiResponse::__create("Save successfully.");
            } else {
                return ApiResponse::__createBadResponse("Please select at least " . RegistrationService::MIN_SELECTION_FOR_EACH_STEP . " city or country");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\POST(
     ** path="/users/registration/places",
     *   tags={"Signup Steps"},
     *   summary="submit places that you want to follow [Regular : Step 4]",
     *   operationId="Api\User\UserController@registrationStepFour",
     *   @OA\Parameter(
     *        name="token",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="places[]",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Select places that you want to follow
     * @param StepFourRequest $request
     * @param RegistrationService $registrationService
     * @param FavouritesService $favouritesService
     * @param UsersService $usersService
     * @return ApiResponse
     */
    public function registrationStepFour(
        StepFourRequest $request,
        RegistrationService $registrationService,
        FavouritesService $favouritesService,
        UsersService $usersService
    ) {
        try {
            $totalSelection = collect($request->input('places', []))->count();
            if ($totalSelection >= RegistrationService::MIN_SELECTION_FOR_EACH_STEP) {
                $userId = $request->user_id;
                $usersService->update($userId, $request->onlyValidated());
                $usersService->setSteps($userId, '4');
                return ApiResponse::__create("selected places save successfully.");
            } else {
                return ApiResponse::__createBadResponse("Please select at least " . RegistrationService::MIN_SELECTION_FOR_EACH_STEP . " place");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/users/registration/social-links",
     *   tags={"Signup Steps"},
     *   summary="Submit Social Links [Expert : Step 3]",
     *   operationId="Api\User\UserController@registrationStepFourExpert",
     *   @OA\RequestBody(
     *        @OA\MediaType(
     *        mediaType="application/json",
     *        @OA\Schema(
     *              example={
     *                  "token" : "",
     *                  "facebook" : "",
     *                  "twitter" : "",
     *                  "instagram" : "",
     *                  "pinterest" : "",
     *                  "tripadvisor" : "",
     *                  "youtube" : "",
     *                  "medium" : "",
     *                  "website" : "",
     *                  "vimeo" : ""
     *              },
     *        )
     *     )
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Become a Travooo Expert (Social Links)
     * @param StepFourExpertRequest $request
     * @param UsersService $usersService
     * @param ExpertsService $expertsService
     * @return ApiResponse
     */
    public function registrationStepFourExpert(
        StepFourExpertRequest $request,
        UsersService $usersService,
        ExpertsService $expertsService
    ) {
        try {
            if (count($request->all()) > 2) {
                $userId = $request->user_id;
                $usersService->update($userId, $request->onlyValidated());
                $usersService->setSteps($userId, '4');
                return ApiResponse::__create("Links Save successfully");
            } else {
                return ApiResponse::__createBadResponse("Social links not found");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * Use : Submit your Interests 
     * @param RegistrationStepFiveRequest $request
     * @param RegistrationService $registrationService
     * @param UsersService $usersService
     * @return ApiResponse
     */
    public function registrationStepFive(
        RegistrationStepFiveRequest $request,
        RegistrationService $registrationService,
        UsersService $usersService
    ) {
        try {
            if (!$registrationService->validateStepFiveRequest($request)) {
                return ApiResponse::__createBadResponse("Please enter at least 1 interest");
            }
            $userId = $request->user_id;
            $user = User::find($userId);
            $usersService->update($userId, [
                'interests' => implode(',', $request->interests)
            ]);
            if ($user->type == 1) {
                $usersService->setSteps($userId, '6');
            } else {
                $usersService->setSteps($userId, '7');
            }
            return ApiResponse::__create("Save successfully.");
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * Use : Choose your Interests 
     * @param Request $request
     * @param RegistrationService $registrationService
     * @param UsersService $usersService
     * @return ApiResponse
     */
    public function registrationStepFiveFind(
        Request $request,
        RegistrationService $registrationService,
        UsersService $usersService
    ) {
        try {
            $interests =  trim($usersService->getInterests($request->user_id)->interests);
            return ApiResponse::create([
                'interests' => ($interests != "") ?  explode(",", $interests) : []
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/users/registration/travel-styles",
     *   tags={"Signup Steps"},
     *   summary="submit your preferred travel styles [Regular : Step 5 + Expert : Step 6]",
     *   operationId="Api\User\UserController@registrationStepSix",
     *   @OA\Parameter(
     *        name="token",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="travel_styles[]",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Select your preferred travel styles
     * @param RegistrationStepSixRequest $request
     * @param RegistrationService $registrationService
     * @param TravelStylesService $travelStylesService
     * @return ApiResponse
     */
    public function registrationStepSix(
        RegistrationStepSixRequest $request,
        RegistrationService $registrationService,
        TravelStylesService $travelStylesService,
        UsersService $usersService
    ) {
        try {
            // if (!$registrationService->validateStepSixRequest($request))
            //     return ApiResponse::__createBadResponse("Please select at least 1 travel styles");

            $totalSelection = collect($request->input('travel_styles', []))->count();
            if ($totalSelection >= RegistrationService::MIN_SELECTION_FOR_EACH_STEP) {
                $userId = $request->user_id;
                $travelStylesService->setTravelStyles($userId, $request->get('travel_styles', []));
                $usersService->setSteps($userId, '7');
                return ApiResponse::__create("Save successfully.");
            } else {
                return ApiResponse::__createBadResponse("Please select at least " . RegistrationService::MIN_SELECTION_FOR_EACH_STEP . " travel styles");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\POST(
     ** path="/users/registration/languages",
     *   tags={"Signup Steps"},
     *   summary="submit your preferred languages [Regular : Step 6 + Expert : Step 7] + for seaching api => GET => /api/v1/languages",
     *   operationId="Api\User\UserController@registrationStepTen",
     *   @OA\Parameter(
     *        name="token",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="languages[]",
     *        description="language id",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *        ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function registrationStepTen(Request $request, LanguagesService $languagesService, UsersService $usersService, RegistrationService $registrationService, ExpertsService $expertsService)
    {
        try {

            $totalSelection = collect($request->get('languages', []))->count();
            if (!($totalSelection >= RegistrationService::MIN_SELECTION_FOR_EACH_STEP)) {
                return ApiResponse::__createBadResponse("Please select at least " . RegistrationService::MIN_SELECTION_FOR_EACH_STEP . " language");
            }

            $languagesService->addContentLanguages($request->get('user_id'), $request->get('languages', []));
            // $usersService->setSteps($request->get('user_id'), '9');

            $userId = $request->user_id;

            $registrationService->activate($userId);
            $expertsService->addPresetPoints($userId);
            $usersService->setSteps($userId, 'complated');

            //Login
            Auth::loginUsingId($userId);
            $user = User::find(Auth::user()->id);
            $tokenResult = $user->createToken('TRAVOOO');

            /* Set token exipry date */
            $token = $tokenResult->token;
            if ($request->has('remember_me') && $request->remember_me) {
                $token->expires_at = Carbon::now()->addWeeks(1);
            }
            $token->save();


            $user->is_firsttime = true;

            return ApiResponse::create([
                'message'       => "Save successfully.",
                'user'          => $user,
                'access_token'  => $tokenResult->accessToken,
                'token_type'    => 'Bearer',
                'expires_at'    => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/users/registration/city-and-country",
     *   tags={"Signup Steps"},
     *   summary="Search countries or cities that you want to follow [Regular : Step 3 + Expert : Step 4]",
     *   operationId="Api\User\UserController@searchCitiesOrCountries",
     *   @OA\Parameter(
     *        name="search",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="token",
     *        description="you can get in Step 1",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     *  Use : Select countries or cities that you want to follow
     *  @param Request $request
     *  @param UsersService $usersService
     *  @param RegistrationService $registrationService
     *  @return ApiResponse
     */
    public function searchCitiesOrCountries(
        Request $request,
        UsersService $usersService,
        RegistrationService $registrationService
    ) {
        try {
            $suggestion = false;
            $search = $request->search;
            $token  = $request->token;
            $result = [];
            $user_ip = $request->ip();

            if ($token && $token != '') {
                if ($search && $search != '') {
                    $cities = Cities::with('transsingle', 'country.transsingle')->whereHas('transsingle', function ($query) use ($search) {
                        $query->where('title', 'like', '%' . $search . '%');
                    })->take(15);
                    $countries = Countries::with('transsingle')->whereHas('transsingle', function ($query) use ($search) {
                        $query->where('title', 'like', '%' . $search . '%');
                    })->take(15);

                    $collection = $cities->get();
                    $entities = $collection->concat($countries->get())->shuffle();

                    foreach ($entities as $entity) {
                        $result[] = [
                            'id' => get_class($entity) === Cities::class ? 'city-' . $entity->getKey() : 'country-' . $entity->getKey(),
                            'type' => 'location',
                            'name' => $entity->transsingle->title,
                            'desc' => get_class($entity) === Cities::class ? $entity->country->transsingle->title : '',
                            'url' => get_class($entity) === Cities::class ? @check_city_photo($entity->getMedias[0]->url, 700) : @get_country_flag($entity)
                        ];
                    }
                } else {
                    $suggestion = true;
                    $result = [];
                    $token = $registrationService->getRegistrationToken($request->get('token'));
                    if (!$token || !$token->getUserId() || $token->getExpiredDate() < Carbon::create()) {
                        return ApiResponse::__createUnAuthorizedResponse("User is unauthorized");
                    } else {
                        $user = User::where('id', $token->getUserId())->first();
                        $result = $usersService->getUserSuggestionLocations($user->nationality);
                        
                    }
                }

                $result = collect($result)->map(function ($item) {
                    $item = json_decode(json_encode($item));
                    unset($item->ob);
                    return $item;
                });
              
                // sortby name A-Z
                $result = collect($result)->sortBy('name')->take(22)->values();

                return ApiResponse::create([
                    'suggestion' => $suggestion,
                    'cities_or_countries' => $result
                ]);
            } else {
                return ApiResponse::__createBadResponse("Token must be required");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    function getElSearchResult($query)
    {
        $curl = curl_init();
        $query = urlencode($query);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/places/_search?q=$query&pretty=true&size=50",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: 7f74e27e-ce46-404b-b0a7-fe5b542d7862",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $response_array = [];
        if ($err) {
            return [];
        } else {
            $response = json_decode($response, true);
            if (isset($response['hits']) && count($response['hits']['hits']) != 0) {
                foreach ($response['hits']['hits'] as $hits) {
                    $response_array[] = $hits['_source'];
                }
                return $response_array;
            } else {
                return [];
            }
        }
    }

    /**
     * @OA\GET(
     ** path="/users/registration/places",
     *   tags={"Signup Steps"},
     *   summary="Search places that you want to follow [Regular : Step 4]",
     *   operationId="Api\User\UserController@searchPlaces",
     *   @OA\Parameter(
     *        name="search",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="token",
     *        description="you can get in Step 1",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     *  Use : Search places that you want to follow
     *  @param RegistrationService $registrationService
     *  @return ApiResponse
     */
    public function searchPlaces(Request $request, RegistrationService $registrationService)
    {
        try {
            $suggestion = false;
            $result = [];
            $search = $request->search;
            $getToken  = $request->token;
            $fromExternal = false;

            if ($getToken && $getToken != '') {
                if ($search) {
                    $qr = $this->getElSearchResult($search);
                    if ($qr == null || empty($qr)) {
                        $mapboxLink = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . urlencode($search) . '.json?limit=30&routing=true&access_token=' . config('mapbox.token');
                        $mapboxResult = json_decode(file_get_contents($mapboxLink));
                        $qr = $mapboxResult->features;
                        $fromExternal = true;
                    }
                    if ($qr) {
                        foreach ($qr as $r) {
                            if ($fromExternal) {
                                if (Place::where('provider_id', '=', $r->id)->exists()) {
                                    $p = Place::where('provider_id', '=', $r->id)->first();
                                    $medium['id'] = @$p->id;
                                    $medium['type'] = 'place';
                                    $medium['name'] = $p->transsingle->title;
                                    $medium['desc'] = @$p->country->transsingle->title ? @$p->country->transsingle->title : '';
                                    $medium['url']  = @check_place_photo($p->getMedias[0]->url);
                                    $result[] = $medium;
                                }
                            } else {
                                $medium['id'] = @$r['place_id'];
                                $p = Place::find($medium['id']);
                                $medium['type'] = 'place';
                                $medium['name'] = $r['place_title'];
                                $medium['desc'] = @$p->country->transsingle->title ? @$p->country->transsingle->title : '';
                                if (isset($r['place_media']) && $r['place_media'] != null) {
                                    $medium['url'] = $r['place_media'];
                                } else {
                                    $medium['url'] = asset('assets2/image/placeholders/place.png');
                                }
                                $result[] = $medium;
                            }
                        }
                    }
                } else {
                    $suggestion = true;
                    $token = $registrationService->getRegistrationToken($getToken);
                    if (!(!$token || !$token->getUserId() || $token->getExpiredDate() < Carbon::create())) {
                        $user = User::where('id', $token->getUserId())->first();
                        if ($user) {
                            $place_followers = collect([]);
                            $user_list = User::where('nationality', $user->nationality)->pluck('id');
                            if (count($user_list) > 100) {
                                $place_followers_list = PlaceFollowers::select('*', DB::raw('count(*) as total'))
                                    ->whereHas('place', function ($query) {
                                        $query->whereHas('medias');
                                    })
                                    ->whereIn('users_id', $user_list)->where('created_at', '>=', Carbon::now()->subDays(30))->orderBy('id', 'DESC')->groupBy('places_id')->pluck('total', 'places_id');

                                foreach ($place_followers_list as $place_id => $total_count) {
                                    if ($total_count >= 100 && count($place_followers) < 10) {
                                        $place_followers[] = $place_id;
                                    }
                                }
                            }

                            $countries_for_places = isset($request->countries) ? $request->countries : [];
                            $cities_for_places = isset($request->cities) ? $request->cities : [];

                            if (count($place_followers) < 20) {
                                $place_count = 20 - count($place_followers);

                                $places_by_location = PlacesTop::whereNotNull('places_id')->whereNotIn('places_id', $place_followers)
                                    ->whereHas('place', function ($query) {
                                        $query->whereHas('medias');
                                    })
                                    ->whereIn('country_id', $countries_for_places)
                                    ->orWhereIn('city_id', $cities_for_places)
                                    ->orderBy('rating', 'DESC')->take($place_count)->pluck('places_id');

                                $final_by_location = $place_followers->concat($places_by_location)->shuffle();

                                if (count($final_by_location) < 20) {
                                    $final_place_count = 20 - count($final_by_location);
                                    $generic_places = GenericTopPlaces::whereNotIn('places_id', $final_by_location)->take($final_place_count)->pluck('places_id');
                                    $places =  $generic_places->concat($final_by_location)->shuffle();
                                } else {
                                    $places = $final_by_location;
                                }
                            } else {
                                $places = $place_followers->shuffle();
                            }

                            foreach ($places as $place_id) {
                                $place_info = Place::find($place_id);
                                $result[] = [
                                    'id' => @$place_info->id,
                                    'type' => 'place',
                                    'name' => @$place_info->transsingle->title,
                                    'desc' => @$place_info->country->transsingle->title ? @$place_info->country->transsingle->title : '',
                                    'url' => @check_place_photo($place_info)
                                ];
                            }
                        }
                    } else {
                        return ApiResponse::__createUnAuthorizedResponse("User is unauthorized");
                    }
                }

                // sortby name A-Z
                $result = collect($result)->sortBy('name')->take(22)->values();

                return ApiResponse::create([
                    'suggestion' => $suggestion,
                    'places' => $result
                ]);
            } else {
                return ApiResponse::__createBadResponse("Token must be required");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\GET(
     ** path="/users/registration/travel-styles",
     *   tags={"Signup Steps"},
     *   summary="Search your preferred travel styles [Regular : Step 5 | Expert : Step 6]",
     *   operationId="Api\User\UserController@searchTravelStyles",
     *   @OA\Parameter(
     *        name="search",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     *  Use : Search your preferred travel styles
     *  @param Request $request
     *  @param RegistrationService $registrationService
     *  @return ApiResponse
     */
    public function searchTravelStyles(
        Request $request,
        RegistrationService $registrationService
    ) {
        try {
            $result = [];
            $suggestion = false;
            $search = $request->search;

            $styles = Travelstyles::with('transsingle')->limit(20);
            if ($search) {
                $styles->whereHas('transsingle', function ($query) use ($search) {
                    $query->where('title', 'like', $search . '%');
                });
            }

            foreach ($styles->get() as $entity) {
                $result[] = [
                    'id' => $entity->getKey(),
                    'type' => 'travel_style',
                    'name' => $entity->transsingle->title,
                    'desc' => '',
                    'url' => @check_lifestyles_photo($entity->getMedias[0]->url)
                ];
            }

            // sortby name A-Z
            $result = collect($result)->sortBy('name')->values();
            // we neeed other style in last after sortby
            $result = $result->whereNotIn('name', ['Other', 'other'])->merge($result->whereIn('name', ['Other', 'other']));

            return ApiResponse::create([
                'suggestion' => $suggestion,
                'travel_styles' => $result
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/users/login",
     *   tags={"Login & Logout"},
     *   summary="Login Api",
     *   operationId="app\Http\Controllers\Api\User\UserController@login",
     *
     *  @OA\RequestBody(
     *        @OA\MediaType(
     *        mediaType="application/json",
     *        @OA\Schema(
     *            @OA\Property(
     *                property="email",
     *                type="string"
     *            ),
     *            @OA\Property(
     *                property="password",
     *                type="string"
     *            ),
     *            example={"email": "brijesh@example.com", "password": "12345678"}
     *        )
     *     )
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return []
     */
    public function login(UserLoginRequest $request, RegistrationService $registrationService, ExpertsService $expertsService)
    {
        try {
            $chkUser = User::where('email', $request->email)->first();
            if ($chkUser != null) {
                $credentials = $request->only('email', 'password');
                if (!Auth::attempt($credentials)) {
                    return ApiResponse::__createBadResponse("Password is incorrect");
                }
                $user = User::find(Auth::user()->id);
                $tokenResult = $user->createToken('TRAVOOO');

                /* Set token exipry date */
                $token = $tokenResult->token;
                if ($request->has('remember_me') && $request->remember_me) {
                    $token->expires_at = Carbon::now()->addWeeks(1);
                }
                $token->save();
                $user->profile_picture = check_profile_picture($user->profile_picture);

                ApiSessionFlush();


                $user->is_firsttime = false;
                $user->points = $expertsService->getPresetPoints($user);
                $user->is_invited = $user->type === \App\Models\User\User::TYPE_INVITED_EXPERT;

                return ApiResponse::create([
                    'message'       => "User Logged In",
                    'user'          => $user,
                    'access_token'  => $tokenResult->accessToken,
                    'token_type'    => 'Bearer',
                    'expires_at'    => Carbon::parse(
                        $tokenResult->token->expires_at
                    )->toDateTimeString(),
                    'register' => ($user->register_steps != "complated") ? [
                        'register_steps' => $user->register_steps,
                        'type' => $user->type,
                        'token' => $registrationService->generateRegistrationToken($user, $request->password)
                    ] : null
                ]);
            } else {
                return ApiResponse::__createBadResponse("Email is incorrect");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/users/logout",
     *   tags={"Login & Logout"},
     *   summary="Logout Api",
     *   operationId="app\Http\Controllers\Api\User\UserController@logout",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Logout user (Revoke the token)
     * @param Request $request
     * @return [string] message
     */
    public function logout(Request $request)
    {
        try {
            $request->user()->token()->revoke();
            return ApiResponse::__create("Successfully logged out");
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\GET(
     ** path="/users/registration/check-username",
     *   tags={"Common API"},
     *   summary="Check Username Availability",
     *   operationId="Api\User\UserController@registrationCheckUsername",
     *   @OA\Parameter(
     *        name="username",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Use : Check Username
     * @param Request $request
     * @param UsersService $usersService
     * @return ApiResponse
     */
    public function registrationCheckUsername(
        Request $request,
        UsersService $usersService
    ) {
        try {
            $validator = Validator::make($request->all(), [
                'username' => 'required|string|min:4|max:15|regex:/^([A-Za-z0-9_])+$/'
            ]);

            if ($validator->fails())
                return ApiResponse::createValidationResponse($validator->errors());

            if ($usersService->findByUsername($request->username)) {
                return ApiResponse::__createBadResponse("Exist");
            } else {
                return ApiResponse::__create("Available");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    // Route::get('/db', 'User\UserController@dbGenerator');
    // public function dbGenerator()
    // {
    //     for($i=1;$i<=500;$i++){
    //         $user_id    = User::all()->random()->id;
    //         $places_id  = \App\Models\Place\Place::all()->random()->id;

    //         $chk = \App\Models\Place\PlaceFollowers::where('users_id',$user_id)->where('places_id',$places_id)->first();
    //         if($chk == null){
    //             $user_id    = User::all()->random()->id;
    //             \App\Models\Place\PlaceFollowers::create([
    //                 'users_id' => $user_id,
    //                 'places_id' => $places_id
    //             ]);
    //             for($j=1;$j<=rand(4,7);$j++){
    //                 $media_id = \App\Models\ActivityMedia\Media::all()->random()->id;
    //                 \App\Models\Place\PlaceMedias::create([
    //                     'places_id'  => $places_id,
    //                     'medias_id' => $media_id 
    //                 ]);
    //             }
    //         }
    //     }
    //     return ApiResponse::create(["success"]);
    // }
}
