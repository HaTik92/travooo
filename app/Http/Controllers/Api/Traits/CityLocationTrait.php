<?php

namespace App\Http\Controllers\Api\Traits;

use App\Http\Responses\ApiResponse;
use App\Models\City\Cities;
use App\Models\City\CitiesFollowers;
use App\Models\City\CitiesMedias;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\Posts\Checkins;
use App\Models\Posts\PostsShares;
use App\Models\Reports\Reports;
use App\Models\TripPlaces\TripPlaces;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Api\ShareService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Events\Events;

trait CityLocationTrait
{
    /**
     * @OA\Get(
     ** path="/city/{id}/",
     *   tags={"Cities"},
     *   summary="Fetch city [bearer_token is optional]",
     *   operationId="app\Http\Controllers\Api\Traits\CityLocationTrait@getIndex",
     *    @OA\Parameter(
     *        name="id",
     *        description="City Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function getIndex(Request $request, $id)
    {
        try {
            $authUser = auth()->user();
            $language_id = 1;
            $obj = Cities::with([
                'trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'languages_spoken.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'holidays.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'religions.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'emergency.trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'cityHolidays', 'additional_languages'
            ])->where('id', $id)->where('active', 1)->first();

            if (!$obj) return ApiResponse::__createBadResponse("city not found");
            $country =  @$obj->country;
            unset($obj->country);

            // basic details & flags & counts
            $obj->image_url = url(PLACE_PLACEHOLDERS);
            $firstCityMedia = CitiesMedias::whereHas('medias')->where('cities_id', $obj->id)->first();
            if ($firstCityMedia) {
                $obj->image_url = check_city_photo($firstCityMedia->medias->url);
            }

            $obj->title = @$obj->trans[0]->title;
            $obj->description = strip_tags(@$obj->trans[0]->description ?? '');
            $obj->country = @$country->trans[0]->title;

            $obj->total_follower = CitiesFollowers::where('cities_id', $obj->id)->count();
            $obj->follow_status = $authUser ? ((bool) CitiesFollowers::where('cities_id', $obj->id)->where('users_id', $authUser->id)->exists()) : false;
            $obj->i_was_here_flag = $authUser ? (Checkins::where('city_id', $obj->id)->where('users_id', $authUser->id)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->count() > 0) : false;
            $obj->total_share = PostsShares::where('type', ShareService::TYPE_CITY)->count();
            $obj->total_checkins = Checkins::where('city_id', $obj->id)->whereHas('user')->where('users_id', '>', 0)->count();
            $obj->past_24h_total_checkins = Checkins::where('city_id', $obj->id)->whereHas('user')->where('users_id', '>', 0)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->count();
            $obj->past_24h_top_checkins = Checkins::where('city_id', $obj->id)->whereHas('user')->with('user')->where('users_id', '>', 0)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->orderBy('id', 'DESC')->limit(3)->get()->map(function ($check_in) {
                return [
                    'id' => $check_in->user->id,
                    'username' => $check_in->user->username,
                    'name' => $check_in->user->name,
                    'profile_picture' => check_profile_picture($check_in->user->profile_picture)
                ];
            });

            // ABOUT SECTION
            {
                $about = [];
                $about['phone_code'] = $obj->code;
                $about['population'] = @$obj->trans[0]->population;
                $about['nationality'] = @$country->trans[0]->nationality;
                $about['working_days'] = (@$obj->trans[0]->working_days != '') ? @$obj->trans[0]->working_days : @$country->trans[0]->working_days;

                $speedLimit = (@$obj->trans[0]->speed_limit != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->speed_limit) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->speed_limit);
                $speedLimits = [];
                foreach ($speedLimit as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $speedLimits[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['speed_limit'] = $speedLimits;
                (@$obj->trans[0]->metrics != '') ? preg_match_all("/\(([^\]]*)\)/", @$obj->trans[0]->metrics, $matches) : preg_match_all("/\(([^\]]*)\)/", @$country->trans[0]->metrics, $matches);
                $about['metrics'] = isset($matches[1][0]) ? explode(", ", $matches[1][0]) : [];
                $about['internet'] = (@$obj->trans[0]->internet != '') ? @$obj->trans[0]->internet : @$country->trans[0]->internet;

                $timing = (@$obj->trans[0]->best_time != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->best_time) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->best_time);
                $timings = [];
                foreach ($timing as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $timings[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['general']['timing'] = $timings;
                $about['general']['transportation'] = (@$obj->trans[0]->transportation != '') ? explode(", ", @$obj->trans[0]->transportation) : explode(", ", @$country->trans[0]->transportation);
                $about['general']['currencies'] = @$country->currencies[0]->transsingle->title;

                // $about['medias'] = count(@$obj->getMedias) > 0 ? @$obj->getMedias : @$country->getMedias;

                $socket = (@$obj->trans[0]->sockets != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->sockets) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->sockets);
                $sockets = [];
                foreach ($socket as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $sockets[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['sockets'] = $sockets;
                $about['cost_of_living'] = (@$obj->trans[0]->cost_of_living != '') ? @$obj->trans[0]->cost_of_living : @$country->trans[0]->cost_of_living;
                $about['crime_rate'] = (@$obj->trans[0]->geo_stats != '') ? @$obj->trans[0]->geo_stats : @$country->trans[0]->geo_stats;
                $about['quality_of_life'] = (@$obj->trans[0]->demographics != '') ? @$obj->trans[0]->demographics : @$country->trans[0]->demographics;

                $economy = (@$obj->trans[0]->economy != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->economy) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->economy);
                $economies = [];
                foreach ($economy as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $economies[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['restrictions'] = $economies;

                $about['emergency_numbers'] = '';
                @$obj_emergency = (count((@$obj->emergency ?? [])) > 0) ? @$obj->emergency : @$country->emergency;
                if (@$obj_emergency) {
                    $emerg = [];
                    foreach (@$obj_emergency as $emergency) {
                        $emerg[] = $emergency->transsingle->title;
                    }

                    $about['emergency_numbers'] = $emerg;
                }

                $about['general']['religions'] = '';
                @$obj_religions = (count((@$obj->religions ?? [])) > 0) ? @$obj->religions : @$country->religions;
                if (@$obj_religions) {
                    $rel = [];
                    foreach (@$obj_religions as $religions) {
                        $rel[] = $religions->transsingle->title;
                    }
                    $about['general']['religions'] = $rel;
                }

                $about['holidays'] = '';
                $obj_holiday = (count((@$obj->cityHolidays ?? [])) > 0) ? @$obj->cityHolidays : @$country->countryHolidays;

                if (@$obj_holiday) {
                    $hol = [];
                    foreach (@$obj_holiday as $holidays) {
                        if (isset($holidays->holiday->transsingle) && !empty($holidays->holiday->transsingle)) {
                            $hol[] = $holidays->holiday->transsingle->title;
                        }
                    }
                    $about['holidays'] = $hol;
                }

                $about['general']['languages_spoken'] = '';
                $lng = [];
                @$languages_spoken = (count((@$obj->languages_spoken ?? [])) > 0) ? @$obj->languages_spoken : @$country->languages;
                if (@$languages_spoken) {
                    foreach (@$languages_spoken as $languages) {
                        $lng[] = $languages->transsingle->title;
                    }
                }

                @$obj_additional_languages = (count((@$obj->additional_languages ?? [])) > 0) ? @$obj->additional_languages : @$country->additional_languages;
                if (@$obj_additional_languages) {
                    foreach (@$obj_additional_languages as $additional_languages) {
                        $lng[] = $additional_languages->transsingle->title;
                    }

                    $about['general']['languages_spoken'] = $lng;
                }
            }
            $obj->about = $about;
            // End About Section

            // Top Experts and Local Experts[Page = 1]
            if ($authUser) {
                //where raw
                $friendRaw = 'users_id in (SELECT `uf`.`users_id` FROM `users_followers` AS `uf` INNER JOIN `users_followers` AS `uf2` ON `uf`.`users_id` = `uf2`.`followers_id` AND `uf2`.`users_id` = `uf`.`followers_id` AND `uf2`.`follow_type` = `uf`.`follow_type` WHERE `uf`.`users_id` != `uf`.`followers_id` AND `uf`.`followers_id` =' . $authUser->id . ' AND `uf`.`follow_type` = 1)';
                $topExpertsRaw = "users_id in (select id from users where nationality != $authUser->nationality)";
                $localExpertsRaw = "users_id in (select id from users where nationality = $authUser->nationality)";

                $experts['total_top_experts'] = ExpertsCities::whereHas('user')->whereRaw($topExpertsRaw)->where('cities_id', $obj->id)->count();
                $experts['total_local_experts'] = ExpertsCities::whereHas('user')->whereRaw($localExpertsRaw)->where('cities_id', $obj->id)->count();
                $experts['total_friends_experts'] = ExpertsCities::whereHas('user')->whereRaw($friendRaw)->where('cities_id', $obj->id)->count();
                $experts['top_experts'] = ExpertsCities::whereHas('user')->whereRaw($topExpertsRaw)->where('cities_id', $obj->id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
                $experts['local_experts'] = ExpertsCities::whereHas('user')->whereRaw($localExpertsRaw)->where('cities_id', $obj->id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
                $experts['friends_experts'] = ExpertsCities::whereHas('user')->whereRaw($friendRaw)->where('cities_id', $obj->id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
            } else {
                $experts['total_top_experts'] = ExpertsCities::whereHas('user')->where('cities_id', $obj->id)->count();
                $experts['total_local_experts'] = 0;
                $experts['total_friends_experts'] = 0;
                $experts['top_experts'] = ExpertsCities::whereHas('user')->where('cities_id', $obj->id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
                $experts['local_experts'] = [];
                $experts['friends_experts'] = [];
            }
            $experts['total_live_checkin'] = Checkins::whereHas('user')->where('city_id', $obj->id)->count();
            $experts['live_checkin'] = Checkins::select('id', 'users_id', DB::raw('city_id as cities_id'))->whereHas('user')->where('city_id', $obj->id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
            foreach ([$experts['top_experts'], $experts['local_experts'], $experts['friends_experts'], $experts['live_checkin']] as &$expertBatch) {
                $expertBatch = collect($expertBatch)->map(function ($expertUser) use ($authUser) {
                    $expertUser->follow_flag = ($authUser) ? UsersFollowers::where('users_id', $expertUser->users_id)->where('followers_id', $authUser->id)->exists() : false;
                    $expertUser->user->profile_picture = check_profile_picture($expertUser->user->profile_picture);
                });
            }

            $obj->experts = $experts;
            // End Experts and Local Experts

            $_trips_query = TripPlaces::query()
                ->whereRaw('versions_id = (SELECT trips.active_version FROM trips WHERE trips.id = trips_places.trips_id)')
                ->whereHas('trip', function ($q) use ($authUser) {
                    return $q->where('users_id', ($authUser) ? $authUser->id : -1);
                })
                ->where('cities_id', $obj->id)
                ->groupBy('trips_id');

            $_trips = (clone $_trips_query)->with('trip')->orderBy('trips_id', 'DESC')->limit(5)->get();

            // Trips [Page = 1]
            $obj->trips = collect($_trips)->map(function ($tp) {
                return [
                    'id' => $tp->trips_id,
                    'title' => $tp->trip->title,
                ];
            })->toArray();
            // End Trips [Page = 1]


            $_trip = (clone $_trips_query)->with('trip')->orderBy('trips_id', 'DESC')->first();
            $boxs[] = [
                "title" => "Epic Trip Plans",
                "type" => "trip-plans",
                "img" => ($_trip) ? check_cover_photo($_trip->trip->cover) : url(PATTERN_PLACEHOLDERS),
                "count" => DB::table(DB::raw('(' . _getDBQuery($_trips_query) . ') as a'))->count()
            ];

            $_trending_media = CitiesMedias::with('medias', 'medias.author')->whereHas('medias', function ($q) {
                return $q->whereHas('author')->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4');
            })->where('cities_id', $obj->id)->orderBy('id', 'DESC')->first();
            $boxs[] = [
                "title" => "Trending Media",
                "type" => "medias",
                "img" => ($_trending_media) ? check_media_url($_trending_media->medias->url) : url(PATTERN_PLACEHOLDERS),
                "count" => CitiesMedias::whereHas('medias', function ($q) {
                    return $q->whereHas('author')->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4');
                })->where('cities_id', $obj->id)->count()
            ];

            $totalExperts =  ExpertsCities::whereHas('user')->where('cities_id', $obj->id)->count();
            $boxs[] = [
                "title" => "Experts",
                "type" => "experts",
                "experts_img" => $totalExperts ?  ExpertsCities::whereHas('user')->where('cities_id', $obj->id)->with('user')->orderBy('id', 'desc')->take(6)->get()->map(function ($expertUser) use ($authUser) {
                    return $expertUser->user->profile_picture = check_profile_picture($expertUser->user->profile_picture);
                })->toArray() : [url(MALE_PLACEHOLDERS)],
                "count" => $totalExperts
            ];

            $_report  = Reports::whereHas('cover')->where('cities_id', '=', @$obj->id)->where('flag', '=', 1)->first();
            $boxs[] = [
                "title" => "Reports",
                "type" => "reports",
                "img" =>  isset($_report->cover[0]->val) ? $_report->cover[0]->val : url(PATTERN_PLACEHOLDERS),
                "count" => Reports::where('cities_id', '=', $obj->id)->where('flag', '=', 1)->count()
            ];

            $rand = rand(1, 10);
            $category = array('community', 'concerts', 'conferences', 'expos', 'festivals', 'performing-arts', 'politics', 'school-holidays', 'sports');

            $boxs[] = [
                "title" => "Events",
                "type" => "events",
                "img" =>  url('assets2/image/events/' . $category[$rand % 9] . '/' . $rand . '.jpg'),
                "count" => Events::query()->where('cities_id', $obj->id)->count()
            ];

            $obj->image_cards = $boxs;
            // End Images Box


            unset(
                $obj->trans,
                $obj->languages_spoken,
                $obj->holidays,
                $obj->religions,
                $obj->emergency,
                $obj->cityHolidays,
                $obj->additional_languages
            );
            return ApiResponse::create($obj);


            // old code // old code // old code // old code // old code




            // $language_id = 1;
            // $city = Cities::find($city_id);
            // $random_val = $this->_getRandom(date('j'));

            // if (!$city) {
            //     return [
            //         'data' => [
            //             'error' => 400,
            //             'message' => 'Invalid City ID',
            //         ],
            //         'success' => false
            //     ];
            // }

            // $data['search_in'] = $city;

            // $language = Languages::where('id', $language_id)->first();

            // if (!$language) {
            //     return [
            //         'data' => [
            //             'error' => 400,
            //             'message' => 'Invalid Language ID',
            //         ],
            //         'success' => false
            //     ];
            // }

            // $city = Cities::with([
            //     'trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'languages_spoken.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'holidays.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'religions.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'currencies.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'emergency.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },

            //     'atms' => function ($query) {
            //         $query->where('place_type', 'like', '%atm%');
            //     },
            //     'hotels',
            //     'getMedias',
            //     'getMedias.users',
            //     'timezone',
            //     'followers'
            // ])
            //     ->where('id', $city_id)
            //     ->where('active', 1)
            //     ->first();

            // if (!$city) {
            //     return [
            //         'data' => [
            //             'error' => 404,
            //             'message' => 'Not found',
            //         ],
            //         'success' => false
            //     ];
            // }

            // $db_place_reviews = Reviews::where('cities_id', $city->id)->orderBy('created_at', 'desc')->get();
            // $data['reviews'] = $db_place_reviews;
            // $data['reviews_places'] = Reviews::where('cities_id', $city->id)->get();

            // $data['reviews_avg'] = Reviews::where('cities_id', $city->id)->whereNull('google_author_url')->avg('score');
            // $city->countryTitle;
            // $data['place'] = $city;
            // if (Auth::check()) {
            //     // $list_trip_plan = TripPlaces::where('cities_id', $city->id)->pluck('trips_id');
            //     $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)
            //         //    ->whereNotIn('id', $list_trip_plan)
            //         ->get();

            //     $data['me'] = Auth::user();
            // }

            // $data['travelmates'] = TravelMatesRequests::whereHas('plan_city', function ($q) use ($city) {
            //     $q->where('cities_id', '=', $city->id);
            // })
            //     ->groupBy('users_id')
            //     ->get();

            // $data['reportsinfo'] = [];
            // $data['reports'] = Reports::where('cities_id', '=', $city->id)->where('flag', '=', 1)
            //     ->get();

            // $data['pposts'] = '';

            // $data['place_discussions'] = Discussion::where('destination_type', 'City')
            //     ->where('destination_id', $city->id)
            //     ->get();

            // // get experts into categories
            // $experts_all = $experts_top = $experts_local = $experts_friends = $live_checkins = array();
            // foreach ($city->experts as $key => $pce) {
            //     $experts_all[] = $pce;
            //     if (is_object($pce->user) && is_object($data['me'])) {
            //         if ($pce->user->nationality != $data['me']->nationality) {
            //             $experts_top[] = $pce;
            //         } elseif ($pce->user->nationality == $data['me']->nationality) {
            //             $experts_local[] = $pce;
            //         }
            //     }
            //     if (is_object($pce->user)) {
            //         if (is_friend($pce->user->id)) {
            //             $experts_friends[] = $pce;
            //         }
            //     }
            // }
            // $data['experts_top'] = $experts_top;
            // $data['experts_local'] = $experts_local;
            // $data['experts_friends'] = $experts_friends;
            // $data['live_checkins'] = $city->checkins()->groupBy('users_id')->get();

            // $experts_ids = array();
            // foreach ($experts_all as $ea) {
            //     $experts_ids[] = $ea->users_id;
            // }

            // // Select *,count(*) as r, sum(A.replies) from 
            // // (SELECT created_at, users_id, count(*) as replies FROM `discussion_replies` GROUP BY YEAR(created_at), Month(created_at), Day(created_at)) 
            // // as A GROUP BY A.users_id ORDER BY `users_id`
            // $data['place_top_contributors'] = [];
            // if (count($experts_ids) > 0) {
            //     $last_month = date("Y-m-d", strtotime('-180 days'));

            //     $sub = DiscussionReplies::selectRaw('*, count(*) as reps, sum(num_views) as views')
            //         ->whereRaw('users_id in (' . implode(',', $experts_ids) . ')')
            //         ->whereRaw('created_at >' . $last_month)
            //         ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'));

            //     $data['place_top_contributors'] = DiscussionReplies::from(DB::raw('(' . $sub->toSql() . ') as a'))
            //         ->selectRaw('*, count(*) as count, sum(a.reps) as reply_cnt, sum(a.views) as views')
            //         ->groupBy('a.users_id')
            //         ->orderBy('count', 'desc')
            //         ->take(10)
            //         ->get();
            // }

            // $data['top_places'] = PlacesTop::leftJoin('places', 'places_top.places_id', '=', 'places.id')
            //     ->where('places.cities_id', $city->id)
            //     ->orderByRaw('FIELD(places_top.id % 32, ' . $random_val . ')')
            //     ->take(4)
            //     ->get();


            // //dd($data['top_places']);
            // // $checkins = Checkins::where('city_id', $city->id)->orderBy('id', 'DESC')->get();

            // // $result = array();
            // // $done = array();
            // // foreach ($checkins AS $checkin) {
            // //     if (!isset($done[$checkin->post_checkin->post->author->id])) {
            // //         $result[] = $checkin;
            // //         $done[$checkin->post_checkin->post->author->id] = true;
            // //     }
            // // }
            // $data['live_checkins'] = $city->checkins()->groupBy('users_id')->whereDate('checkin_time', '=', Carbon::today()->toDateString())->get();

            // //$data['checkins'] = $result;
            // $live_users = [];
            // foreach ($data['live_checkins'] as $live_here) {
            //     $live_users[] = $live_here->users_id;
            // }
            // $temp_live_users =  User::whereIn('id', $live_users)->get();
            // $live_users = [];
            // foreach ($temp_live_users as $user) {
            //     $is_followed =  app('App\Http\Controllers\ProfileController')->postCheckFollow($user->id);
            //     $live_users[$user->id] = ['users' => $user, 'is_followed' => $is_followed['success']];
            // }
            // $data['live_users'] = $live_users;
            // $languages = $city->getLanguages();
            // $city->setRelation('languages', collect($languages));

            // $data['trip_plans_collection'] = TripPlaces::where('cities_id', $city->id)
            //     ->orderBy('id', 'DESC')
            //     ->get();

            // return ApiResponse::create($data);
        } catch (\Throwable $th) {
            return ApiResponse::createServerError($th);
        }
    }

    /**
     * @OA\Get(
     ** path="/city/{id}/experts",
     *   tags={"Cities"},
     *   summary="Get City Expert List In Pagination [bearer_token is optional]",
     *   operationId="App\Http\Controllers\Api\Traits\LocationTrait@getExpertsList",
     *   @OA\Parameter(
     *        name="city_id",
     *        description="City Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="page",
     *        description="pagination page number",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="per_page",
     *        description="per page | default = 5",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function getExpertsList(Request $request, $id)
    {
        $obj = Cities::where('id', $id)->where('active', 1)->first();
        if (!$obj) return ApiResponse::createValidationResponse(["id" => ["city not found"]]);

        $authUser   = auth()->user();
        $page       = (int) $request->get('page', 1);
        $per_page   = (int) $request->get('per_page', 5);

        if ($per_page <= 0) return ApiResponse::createValidationResponse(["per_page" => ["per_page must be greater than 0"]]);

        // Page = 1 ||  Top Experts and Local Experts
        if ($authUser) {
            $experts['total_top_experts'] = ExpertsCities::whereRaw("users_id in (select id from users where nationality != $authUser->nationality)")->where('cities_id', $obj->id)->count();
            $experts['total_local_experts'] = ExpertsCities::whereRaw("users_id in (select id from users where nationality = $authUser->nationality)")->where('cities_id', $obj->id)->count();
            $experts['top_experts'] = modifyPagination(ExpertsCities::whereRaw("users_id in (select id from users where nationality != $authUser->nationality)")->where('cities_id', $obj->id)->with('user')->orderBy('id', 'desc')->paginate($per_page))->data;
            $experts['local_experts'] = modifyPagination(ExpertsCities::whereRaw("users_id in (select id from users where nationality = $authUser->nationality)")->where('cities_id', $obj->id)->with('user')->orderBy('id', 'desc')->paginate($per_page))->data;
        } else {
            $experts['total_top_experts'] = ExpertsCities::where('cities_id', $obj->id)->count();
            $experts['total_local_experts'] = 0;
            $experts['top_experts'] = modifyPagination(ExpertsCities::where('cities_id', $obj->id)->with('user')->orderBy('id', 'desc')->paginate($per_page))->data;
            $experts['local_experts'] = [];
        }

        foreach ([$experts['top_experts'], $experts['local_experts']] as &$expertBatch) {
            $expertBatch = collect($expertBatch)->map(function ($expertUser) use ($authUser) {
                $expertUser->follow_flag = ($authUser) ? UsersFollowers::where('users_id', $expertUser->users_id)->where('followers_id', $authUser->id)->exists() : false;
                $expertUser->user->profile_picture = check_profile_picture($expertUser->user->profile_picture);
            });
        }

        $bigCount = max([$experts['total_top_experts'], $experts['total_local_experts']]);

        $data['per_page'] = $per_page;
        $data['current_page'] = $page;
        $data['total_pages'] = ceil($bigCount / $per_page);
        $data['data'] = $experts;

        return ApiResponse::create($data);
    }

    /**
     * @OA\Get(
     ** path="/city/{id}/trips",
     *   tags={"Cities"},
     *   summary="Get City Trips In Pagination [bearer_token is optional]",
     *   operationId="App\Http\Controllers\Api\Traits\LocationTrait@getTrips",
     *    @OA\Parameter(
     *        name="city_id",
     *        description="City Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="page",
     *        description="pagination page number",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="per_page",
     *        description="per page | default = 5",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function getTrips(Request $request, $id)
    {
        $authUser = auth()->user();
        $obj = Cities::where('id', $id)->where('active', 1)->first();
        if (!$obj) return ApiResponse::createValidationResponse(["id" => ["city not found"]]);

        $per_page   = (int) $request->get('per_page', 5);
        if ($per_page <= 0) return ApiResponse::createValidationResponse(["per_page" => ["per_page must be greater than 10"]]);

        $trips =  TripPlaces::query()
            ->whereRaw('versions_id = (SELECT trips.active_version FROM trips WHERE trips.id = trips_places.trips_id)')
            ->whereHas('trip', function ($q) use ($authUser) {
                return $q->where('users_id', ($authUser) ? $authUser->id : -1);
            })
            ->with('trip')
            ->where('cities_id', $obj->id)
            ->orderBy('trips_id', 'DESC')->groupBy('trips_id')->paginate($per_page);

        $trips = modifyPagination($trips);

        $trips->data = $trips->data->map(function ($tp) {
            return [
                'id' => $tp->trips_id,
                'title' => $tp->trip->title,
            ];
        })->toArray();

        return ApiResponse::create($trips);
    }
}
