<?php

namespace App\Http\Controllers\Api\Traits;

use Illuminate\Http\Request;
use App\Http\Responses\ApiResponse;
use Illuminate\Support\Facades\Storage;
use App\Services\Ranking\RankingService;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsInfos;
use App\Models\Reports\ReportsDraft;
use App\Models\Reports\ReportsDraftInfos;
use App\Models\Reports\ReportsLikes;
use App\Models\Reports\ReportsComments;
use App\Models\Reports\ReportsShares;
use App\Models\User\User;
use App\Models\ActivityMedia\Media;
use App\Models\Reports\ReportsCommentsMedias;
use App\Models\User\UsersMedias;
use App\Models\UsersFollowers\UsersFollowers;
use App\Models\Place\Place;
use App\Http\Requests\Api\Reports\SearchUserRequest;
use App\Http\Requests\Api\Reports\ReportListRequest;
use App\Http\Requests\Api\Reports\CreateEditFirstStepRequest;
use App\Http\Requests\Api\Reports\UploadCoverRequest;
use App\Http\Requests\Api\Reports\CreateEditSecondStepRequest;
use App\Http\Requests\Api\Reports\SearchReportPlanRequest;
use App\Http\Requests\Api\Reports\CommentRequest;
use App\Http\Requests\Api\Reports\PostShareRequest;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\TripPlans\TripPlans;
use App\Models\Country\ApiCountry as Country;
use App\Models\Reports\ReportsViews;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\Reports\ReportsCommentsLikes;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Constants\CommonConst;
use App\Http\Controllers\Controller;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Posts\Checkins;
use App\Models\Reviews\Reviews;
use App\Services\Trips\TripInvitationsService;
use App\Http\Controllers\Api\HomeController;
use App\Http\Requests\Api\Reports\NewCreateEditSecondStepRequest;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Place\PlaceFollowers;
use App\Models\Posts\PostsShares;
use App\Services\Api\PrimaryPostService;
use App\Services\Api\ShareService;
use App\Services\Reports\ReportsService;
use App\Services\Translations\TranslationService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


trait ReportCreation
{

    /**
     * @OA\Post(
     ** path="/reports/create",
     *   tags={"Travelogs"},
     *   summary="Create / update first step [if you want to update step 1 then pass report_id otherwise no need]",
     *   operationId="create_first_step",
     *   security={
     *  {"bearer_token": {}
     *    }},
     * @OA\Parameter(
     *        name="title",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="description",
     *        description="",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="report_id",
     *        description="for create 0 : for update : original id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="locations_ids[]",
     *        description="id-country | id-city",
     *        in="query",
     *        @OA\Schema(
     *           type="array",
     *           @OA\Items(type="string"),
     *        ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param CreateEditFirstStepRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCreateEditFirstStep(CreateEditFirstStepRequest $request, RankingService $rankingService, ReportsService $reportsService, TranslationService $translationService)
    {
        try {
            $authUser       = auth()->user();
            $report_id      = $request->get('report_id', 0);
            $isNewReport    = $report_id == 0;

            $title          = $request->get('title', '');
            $description    = processString($request->get('description', ''));
            $locations      = $request->get('locations_ids', []);

            if (!(is_array($locations) && count($locations))) {
                return ApiResponse::createValidationResponse([
                    'locations_ids' => ['locations must be required']
                ]);
            }

            $countries_ids  = [];
            $cities_ids     = [];

            foreach ($locations as $location) {
                $location_type = explode('-', $location);
                if (count($location_type) == 2) {
                    if (strtolower($location_type[1]) == 'country') $countries_ids[] = $location_type[0];
                    if (strtolower($location_type[1]) == 'city') $cities_ids[] = $location_type[0];
                }
            }

            $countries_id   = (count($countries_ids)) ? implode(',', $countries_ids) : NULL;
            $cities_id      = (count($cities_ids)) ? implode(',', $cities_ids) : NULL;

            if ($isNewReport) {
                $report                 = new Reports;
                $report->users_id       = $authUser->id;
                $report->countries_id   = $countries_id;
                $report->cities_id      = $cities_id;
                $report->title          = $title;
                $report->description    = $description;
                $report->language_id    = $translationService->getLanguageId($title . ' ' . $description);
                $report->flag = 0;

                if (!($report->save())) {
                    return ApiResponse::__createBadResponse('Report not saved');
                }

                $rankingService->addPointsEarners($report->id, get_class($report), auth()->id());

                $reportsDraft               = new ReportsDraft;
                $reportsDraft->reports_id   = $report->id;
                $reportsDraft->users_id     = $authUser->id;
                $reportsDraft->countries_id = $countries_id;
                $reportsDraft->cities_id    = $cities_id;
                $reportsDraft->title        = $title;
                $reportsDraft->description  = $description;

                if (!($reportsDraft->save())) {
                    $report->delete();
                    return ApiResponse::__createBadResponse('Report not saved.');
                }
            } else {
                $report = Reports::with('draft_report')->where('id', $report_id)->first();
                if (!($report && $report->draft_report)) {
                    return ApiResponse::__createBadResponse('Report not found.');
                }

                if ($report->users_id  != $authUser->id) {
                    return ApiResponse::__createBadResponse('only author can edit report.');
                }

                $reportsDraft = $report->draft_report;
                $reportsDraft->countries_id     = $countries_id;
                $reportsDraft->cities_id        = $cities_id;
                $reportsDraft->title            = $title;
                $reportsDraft->description      = $description;

                if (!($reportsDraft->save())) {
                    return ApiResponse::__createNoPermissionResponse('Report not saved.');
                }
            }

            $reportsService->saveLocations($reportsDraft->reports_id, $locations);
            if ($isNewReport) {
                $reportsService->saveLocations($reportsDraft->reports_id, $locations, false);
            }

            $fetchLocations =  $tempLocations = $reportsDraft->prettyLocation();
            // unset($reportsDraft->created_at, $reportsDraft->updated_at, $reportsDraft->deleted_at);

            $right_info = [];
            if ($fetchLocations['total'] > 1) {
                $right_info = ['is_multiple' => true, 'data' => api_get_report_locations_info($fetchLocations)];
            } else {
                $right_info = ['is_multiple' => false];
                if (count($fetchLocations[Reports::LOCATION_COUNTRY])) {
                    $right_info['data'][] = [
                        'key' => trans('region.country_capital'),
                        'value' =>  @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->capitals[0]->city->transsingle->title,
                    ];
                    $right_info['data'][] = [
                        'key' => trans('region.population'),
                        'value' =>   @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->transsingle->population,
                    ];
                    $right_info['data'][] = [
                        'key' => "Currency",
                        'value' => @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->currencies[0]->transsingle->title,
                    ];
                } elseif (count($fetchLocations[Reports::LOCATION_CITY])) {
                    $right_info['data'][] = [
                        'key' => trans('region.country_capital'),
                        'value' =>  @collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->title,
                    ];
                    $right_info['data'][] = [
                        'key' => trans('region.population'),
                        'value' =>   @collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->population,
                    ];
                    $right_info['data'][] = [
                        'key' => "Currency",
                        'value' => @collect($fetchLocations[Reports::LOCATION_CITY])->first()->country->currencies[0]->transsingle->title,
                    ];
                } else {
                    $right_info['data'] = [];
                }
            }

            return ApiResponse::create([
                'id' => $reportsDraft->reports_id,
                'title' => $reportsDraft->title,
                'description' => $reportsDraft->description,
                'countries_id' => $reportsDraft->countries_id,
                'cities_id' => $reportsDraft->cities_id,
                'users_id' => $reportsDraft->users_id,
                'author' => $reportsDraft->author,
                'right_info' => $right_info,
                'draft_flag' => !($report->flag && $report->published_date != NULL),
                'is_my_report' => true,
                'location' => _createLocationObjectWithFullDetailsForReport($tempLocations[Reports::LOCATION_CITY], $tempLocations[Reports::LOCATION_COUNTRY])
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/reports/create-infos",
     *   tags={"Travelogs"},
     *   summary="Create second step",
     *   operationId="app\Http\Controllers\Api\Traits\ReportCreation@newPostCreateEditSecondStep",
     *   security={
     *      {"bearer_token": {}
     *    }},
     *   @OA\RequestBody(
     *        @OA\MediaType(
     *        mediaType="application/json",
     *        @OA\Schema(
     *            @OA\Property(
     *                property="reason",
     *                type="string"
     *            ),
     *            @OA\Property(
     *                property="values",
     *                type="string"
     *            ),
     *            example={
     * "report_id": 1344,
     * "title": "test 1",
     * "saving_mode": "draft|publish",
     *  "info": {
     *  {
     *      "var": "map",
     *      "val": "24.633333-:46.716667-:Al Riyadh-:12-:Saudi Arabia-:SA-:33.554.000-:187"
     *  },
     *  {
     *      "var": "place",
     *      "val": 5554721
     *  },
     *  {
     *      "var": "plan",
     *      "val": "857"
     *  },
     *  {
     *      "var": "text",
     *      "val": "<p><strong>html text</strong></p>"
     *  },
     *  {
     *      "var": "user",
     *      "val": "1888,1,19"
     *  },
     *  {
     *      "var": "local_video",
     *      "val": "https://s3.amazonaws.com/travooo-images2/reports_video/1623058255.7633-1594-report-video.WhatsApp Video 2021-04-07 at 10.20.00 AM.mp4-:this is video test-:"
     *  },
     *  {
     *      "var": "photo",
     *      "val": "https://s3.amazonaws.com/travooo-images2/reports_photo/1623057715.9129-1594-report-image.jpg-:test-:"
     *  }
     * }
     * },
     *        )
     *     )
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function newPostCreateEditSecondStep(NewCreateEditSecondStepRequest $request, ReportsService $reportsService, TranslationService $translationService)
    {
        try {
            $authUser  = auth()->user();
            $title = $request->get('title');
            $report_id = $request->get('report_id');
            $saving_mode = $request->get('saving_mode', 'draft');
            $report_infos = $request->get('info', []);
            $report_infos = collect($report_infos)->whereIn('var', ReportsService::MAPPING_INFO_VARS);
            $report_infos = $report_infos->filter(function ($report_info) {
                return count($report_info) == 2;
            })->toArray();
            $current_report = Reports::find($report_id);
            $report_draft = ReportsDraft::where('reports_id', $report_id)->first();

            // start required validate
            if (!($current_report && $report_draft)) {
                return ApiResponse::__createBadResponse('Report not found');
            }

            if (!in_array($saving_mode, ["draft", "publish"])) {
                return ApiResponse::createValidationResponse([
                    'saving_mode' => ['invalid saving_mode']
                ]);
            }

            if ($current_report->users_id != $authUser->id) {
                return ApiResponse::__createUnAuthorizedResponse('only ' . $authUser->username . ' save report.');
            }

            if ($saving_mode == "publish") {
                if ($report_draft->infos()->where('var', ReportsService::VAR_COVER)->count() == 0) {
                    return ApiResponse::__createBadResponse("Cover field must be required.");
                }
            }
            // end required validate

            // change title if found different report title 
            if ($title != $report_draft->title) {
                $report_draft->title = $title;
                $report_draft->save();
            }

            // save as both (draft & publich)
            ReportsDraftInfos::where('reports_draft_id', $report_draft->id)->where('var', '!=', ReportsService::VAR_COVER)->delete();
            if (isset($report_infos) && is_array($report_infos)) {
                $order = 1;
                foreach ($report_infos as $report_info) {
                    if ($report_info) {
                        $report_info = json_decode(json_encode($report_info));
                        ReportsDraftInfos::updateOrCreate([
                            'reports_draft_id' => $report_draft->id,
                            'var' =>  $report_info->var,
                            'val' => $report_info->val,
                            'order' => $order
                        ]);

                        $order++;
                    }
                }
            }

            if ($saving_mode == "publish") {

                ReportsInfos::where('reports_id', $current_report->id)->delete();
                $d_report_info = ReportsDraftInfos::where('reports_draft_id', $report_draft->id)->get();

                foreach ($d_report_info as $report_info) {
                    $r_info = new ReportsInfos;
                    $r_info->reports_id = $current_report->id;
                    $r_info->var = $report_info->var;
                    $r_info->val = $report_info->val;
                    $r_info->order = $report_info->order;
                    $r_info->language_id = $translationService->getLanguageId($report_info->val);
                    $r_info->save();
                }

                // publish report
                $current_report->flag = 1;
                $current_report->users_id = $report_draft->users_id;
                $current_report->countries_id = $report_draft->countries_id;
                $current_report->cities_id = $report_draft->cities_id;
                $current_report->title = $report_draft->title;
                $current_report->description = $report_draft->description;
                $current_report->published_date = date('Y-m-d H:i:s');
                $current_report->language_id  = $translationService->getLanguageId($report_draft->title . ' ' . $report_draft->description);
                $current_report->save();

                // publish places & locations(countries & cities)
                $reportsService->publishedPlace($current_report);
                $reportsService->publishedLocation($current_report->id);

                // publish report activity log
                $action = ActivityLog::query()->where([
                    'type' => PrimaryPostService::TYPE_REPORT,
                    'variable' => $current_report->id,
                    'users_id' => auth()->user()->id
                ])->whereIn('action', ['create', 'update'])->exists() ? 'update' : 'create';
                log_user_activity(PrimaryPostService::TYPE_REPORT, $action, $current_report->id);
            } else {
                // save as draft
                $current_report->flag = 0;
                $current_report->save();
            }

            $isAuthUser     = Auth::check();
            $isDraftView    = true;
            $isMyReport     = true;
            // $cities         = collect([]);
            // $countries      = collect([]);

            $report = Reports::with('draft_report', 'author')->where('id', $current_report->id)->first();

            // Add draft_flag
            $report->draft_flag = !($report->flag && $report->published_date != NULL);
            $report->is_my_report = $isMyReport;

            $draft_report = $report->draft_report;
            unset($report->draft_report);

            if (!$draft_report) return ApiResponse::__createBadResponse("report not found");
            $report->draft_report_id = $draft_report->id;

            $_rep = $draft_report;

            if ($isDraftView) { // change report data with draft data for author view

                $reportsInfos = ReportsDraftInfos::query()->where('reports_draft_id', $draft_report->id);

                $report->title = $draft_report->title;
                $report->description = $draft_report->description;
            } else {
                $_rep =  $report;
                $reportsInfos = ReportsInfos::query()->where('reports_id', $report->id);
            }
            $cover = asset(HOTEL_NO_PHOTO);
            $reportsInfos = $reportsInfos->orderBy('order', 'ASC')->get()->each(function ($info) use ($isDraftView, $authUser, $isAuthUser, &$cover) {
                if ($isDraftView) {
                    unset($info->reports_draft_id);
                } else {
                    unset($info->type, $info->language_id, $info->reports_id);
                }

                $info->var = trim(strtolower($info->var));
                switch ($info->var) {
                    case ReportsService::VAR_PLACE:
                        $p = Place::find($info->val);
                        $medium['title'] = @$p->trans[0]->title;
                        $medium['address'] = @$p->trans[0]->address;
                        $medium['location'] = $p->lat . "," . $p->lng;
                        $medium['lat'] = $p->lat;
                        $medium['lng'] = $p->lng;
                        $medium['place_type'] = do_placetype($p->place_type);

                        $medium['image'] = asset(PLACE_PLACEHOLDERS);
                        if (isset($r['place_media']) && $r['place_media'] != null) {
                            $medium['image'] = check_media_url($r['place_media']);
                        }

                        $medium['city_id'] = $p->cities_id;
                        $medium['city_title'] = @$p->city->transsingle->title;
                        $medium['country_id'] = $p->countries_id;
                        $medium['country_title'] = @$p->country->transsingle->title;

                        $medium['ratting'] = Reviews::where('places_id', $p->id)->avg('score') ?? 0;
                        $medium['total_reviews'] = Reviews::where('places_id', $p->id)->count();
                        $medium['recent_reviews'] = Reviews::where('places_id', $p->id)->take(2)->get()->each(function ($placeReviewer) {
                            $placeReviewer->author;
                        });
                        $medium['total_followers'] = PlaceFollowers::where('places_id', $p->id)->count();
                        $medium['recent_followers'] = PlaceFollowers::where('places_id', $p->id)->take(4)->get()->map(function ($placeFollower) {
                            return [
                                'id' => $placeFollower->users_id,
                                'profile_picture' => check_profile_picture($placeFollower->user->profile_picture),
                            ];
                        });
                        $info->data = $medium;
                        break;

                    case ReportsService::VAR_COVER:
                        $cover = $info->val;
                        break;

                    case ReportsService::VAR_INFO:
                        $_data['id'] = $info->val;
                        $_data['html'] = view('react-native.report.info-element.show', [
                            'info' => $info
                        ])->render();
                        $_data['added_flag'] = true;
                        $info->data = $_data;
                        break;

                    case ReportsService::VAR_MAP:
                        $temp = @explode("-:", $info->val);
                        $__ =  [
                            'id' => $info->val,
                            'text' => @$temp[4],
                            'lat' =>  @$temp[0],
                            'lng' => @$temp[1],
                            'zoom' => (strpos($info->val, "-:12-:")) ? 12 : ((strpos($info->val, "-:8-:")) ? 8 : 5),
                            'image' => "https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/" . strtolower(@$temp[5]) . ".png",
                            'type' => (strpos($info->val, "-:12-:")) ? 'Place' : ((strpos($info->val, "-:8-:")) ? 'City' : 'Country'),
                            'type_desc' => '',
                            'val' => $info->val,
                            'population' => @$temp[6],
                            'country_id' => @$temp[7],
                            'country_follow_flag' => get_country_follow(@$temp[7]),
                        ];
                        $info->data = $__;
                        break;

                    case ReportsService::VAR_LOCAL_VIDEO:
                    case ReportsService::VAR_PHOTO:
                        $_ = @explode("-:", $info->val);
                        $_data['url']   = @$_[0];
                        $_data['desc']  = @$_[1];
                        $_data['link']  = @$_[2];
                        $info->data = $_data;
                        break;

                    case ReportsService::VAR_USER:
                        $info->data  = [];
                        if (strlen(trim($info->val))) {
                            $info->data = User::query()
                                ->select('id', 'name', 'username', 'email', 'cover_photo', 'profile_picture')
                                ->whereIn('id', explode(",", $info->val))
                                ->get()->each(function (&$user) use ($authUser, $isAuthUser) {
                                    $user->follow_flag = ($isAuthUser) ? UsersFollowers::where('users_id', $user->id)->where('followers_id', $authUser->id)->exists() : false;
                                    $user->cover_photo  = check_cover_photo($user->cover_photo);
                                    $user->profile_picture = check_profile_picture($user->profile_picture);
                                    $user->media = Media::select('url')
                                        ->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4')
                                        ->where('users_id', $user->id)->take(3)->pluck('url')
                                        ->map(function (&$url) {
                                            return check_media_url($url);
                                        });
                                });
                        }
                        break;

                    case ReportsService::VAR_PLAN:
                        $info->data = travelogTripObject((int)$info->val);
                        break;
                }
            });
            $report->cover_url =  $cover;

            $fetchLocations = $_rep->prettyLocation();
            if ($fetchLocations['total'] > 1) {
                $right_info = ['is_multiple' => true, 'data' => api_get_report_locations_info($fetchLocations)];
            } else {
                $right_info = ['is_multiple' => false];
                if (count($fetchLocations[Reports::LOCATION_COUNTRY])) {
                    $right_info['data'][] = [
                        'key' => trans('region.country_capital'),
                        'value' =>  @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->capitals[0]->city->transsingle->title,
                    ];
                    $right_info['data'][] = [
                        'key' => trans('region.population'),
                        'value' =>   @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->transsingle->population,
                    ];
                    $right_info['data'][] = [
                        'key' => "Currency",
                        'value' => @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->currencies[0]->transsingle->title,
                    ];
                } elseif (count($fetchLocations[Reports::LOCATION_CITY])) {
                    $right_info['data'][] = [
                        'key' => trans('region.country_capital'),
                        'value' =>  @collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->title,
                    ];
                    $right_info['data'][] = [
                        'key' => trans('region.population'),
                        'value' =>   @collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->population,
                    ];
                    $right_info['data'][] = [
                        'key' => "Currency",
                        'value' => @collect($fetchLocations[Reports::LOCATION_CITY])->first()->country->currencies[0]->transsingle->title,
                    ];
                } else {
                    $right_info['data'] = [];
                }
            }
            $report->right_info = $right_info;

            // Add All Location (step 1)
            $report->locations = _createLocationObjectWithFullDetailsForReport($fetchLocations[REPORTS::LOCATION_CITY], $fetchLocations[REPORTS::LOCATION_COUNTRY]);

            // Add author
            if ($report->author) {
                $report->author->profile_picture = check_profile_picture($report->author->profile_picture);
                if ($isAuthUser) {
                    $report->author->follow_flag = UsersFollowers::where('users_id', $report->author->id)
                        ->where('followers_id', $authUser->id)
                        ->exists();
                } else {
                    $report->author->follow_flag = false;
                }
            }

            // Add infos without cover
            $report->infos = array_values($reportsInfos->filter(function ($_info) {
                return $_info->var !== "cover";
            })->toArray());

            // Add Likes & Share Flag
            $report->like_flag   = $authUser ? ($report->likes->where('users_id', $authUser->id)->first() ? true : false) : false;
            $report->total_liked = $report->likes->count();
            $report->total_shares =  PostsShares::where('type', ShareService::TYPE_REPORT)->where('posts_type_id', $report->id)->count();
            // For Comment
            $post_comments = [];
            $report->total_comments  = $report->postComments->count();
            $report->comment_flag = $authUser ? ($report->postComments->where('users_id', $authUser->id)->count() ? true : false) : false;
            foreach ($report->postComments->take(2) as $parent_comment) {
                // For Parent Comment
                $parent_comment->like_status = $authUser ? (ReportsCommentsLikes::where('comments_id', $parent_comment->id)->where('users_id', $authUser->id)->first() ? true : false) : false;
                $parent_comment->total_likes = ReportsCommentsLikes::where('comments_id', $parent_comment->id)->count();;
                if ($parent_comment->medias) {
                    foreach ($parent_comment->medias as $media) {
                        $media->media;
                    }
                }
                if ($parent_comment->author) {
                    $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                }
                $parent_comment->text = convert_post_text_to_tags(strip_tags($parent_comment->text), $parent_comment->tags, false);
                unset($parent_comment->tags);
                $parent_comment->comment_flag = $authUser ? ($parent_comment->sub->where('users_id', $authUser->id)->count() ? true : false) : false;

                // For Sub Comment
                if ($parent_comment->sub) {
                    foreach ($parent_comment->sub as $sub_comment) {
                        $sub_comment->like_status = $authUser ? (ReportsCommentsLikes::where('comments_id', $sub_comment->id)->where('users_id', $authUser->id)->first() ? true : false) : false;
                        $sub_comment->total_likes = ReportsCommentsLikes::where('comments_id', $sub_comment->id)->count();
                        if ($sub_comment->medias) {
                            foreach ($sub_comment->medias as $media) {
                                $media->media;
                            }
                        }
                        if ($sub_comment->author) {
                            $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                        }
                        $sub_comment->text = convert_post_text_to_tags(strip_tags($sub_comment->text), $sub_comment->tags, false);
                        unset($sub_comment->tags);
                    }
                }

                $post_comments[] = $parent_comment;
            }
            unset($report->cover, $report->likes, $report->postComments, $report->location);
            $report->post_comments = $post_comments;

            // More Travlogs
            $report->more_travlogs = Reports::where('id', '!=', $report->id)
                ->whereNotNull('published_date')->orderBy('id', 'desc')->take(6)->get()
                ->map(function ($_report) use ($authUser) {
                    $_report->cover_url = isset($_report->cover[0]) ? $_report->cover[0]->val : asset(HOTEL_NO_PHOTO);
                    if ($_report->author) {
                        $_report->author->profile_picture = check_profile_picture($_report->author->profile_picture);
                    }
                    $_report->total_liked = $_report->likes->count();
                    $_report->total_comments  = $_report->postComments->count();

                    $_report->like_flag   = $authUser ? ($_report->likes->where('users_id', $authUser->id)->first() ? true : false) : false;
                    $_report->comment_flag = $authUser ? ($_report->postComments->where('users_id', $authUser->id)->count() ? true : false) : false;

                    unset($_report->likes, $_report->postComments, $_report->cover);
                    return $_report;
                });

            return ApiResponse::create(
                $report
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    //-----------------------

    /**
     * @OA\Post(
     ** path="/reports/{report_id}/upload-cover",
     *   tags={"Travelogs"},
     *   summary="Upload Cover",
     *   operationId="app\Http\Controllers\Api\ReportsController@postUploadCover",
     *   security={
     *       {"bearer_token": {}
     *    }},
     * @OA\Parameter(
     *      name="report_id",
     *      description="",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer"
     *      )
     * ),
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="cover_image",
     *                  type="string",
     *                  format="binary"
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param UploadCoverRequest $request
     * @return \IlluminRequestate\Http\Response
     */
    public function postUploadCover(UploadCoverRequest $request, $report_id)
    {
        try {
            $authUser   = auth()->user();
            $report     = Reports::find($report_id);

            if (!($report && $report->draft_report))  return ApiResponse::__createBadResponse('Report not found');
            if ($report->users_id  != $authUser->id) return ApiResponse::__createBadResponse('only author can edit report.');

            if ($request->hasFile('cover_image') && $request->file('cover_image') && $request->file('cover_image')->isValid()) {
                if (strpos($request->file('cover_image')->getMimeType(), 'image') !== false) {
                    $cover_image    = $request->file('cover_image');
                    $filename = microtime(true) . '-' . $authUser->id . '-cover-image.' . $cover_image->getClientOriginalName();
                    Storage::disk('s3')->putFileAs('reports_cover/', $cover_image, $filename, 'public');
                    $report_cover_url = S3_BASE_URL . 'reports_cover/' . $filename;
                    $rep_info = ReportsDraftInfos::where('reports_draft_id', $report->draft_report->id)->where('var', 'cover')->first();

                    if ($rep_info) {
                        $rep_info->val = $report_cover_url;
                        if ($rep_info->save()) {
                            return ApiResponse::create(["cover" => $report_cover_url]);
                        }
                        return ApiResponse::__createBadResponse("cover not save");
                    } else {
                        $cover = new ReportsDraftInfos;
                        $cover->reports_draft_id = $report->draft_report->id;
                        $cover->var = ReportsService::VAR_COVER;
                        $cover->val = $report_cover_url;
                        $cover->order = 0; // fixed for report cover
                        if ($cover->save()) {
                            return ApiResponse::create(["cover" => $report_cover_url]);
                        }
                        return ApiResponse::__createBadResponse("cover not save");
                    }
                }
                return ApiResponse::createValidationResponse(['cover_image' => 'image type is invalid']);
            }
            return ApiResponse::createValidationResponse(['cover_image' => 'image must be required']);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/reports/{report_id}/upload-image",
     *   tags={"Travelogs"},
     *   summary="Upload Image",
     *   operationId="app\Http\Controllers\Api\ReportsController@postUploadImage",
     *   security={
     *       {"bearer_token": {}
     *    }},
     * @OA\Parameter(
     *      name="report_id",
     *      description="",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer"
     *      )
     * ),
     * * @OA\Parameter(
     *      name="desc",
     *      description="",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="link",
     *      description="",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="local_image",
     *                  type="string",
     *                  format="binary"
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param UploadCoverRequest $request
     * @return \IlluminRequestate\Http\Response
     */
    public function postUploadImage(Request $request, $report_id)
    {
        try {
            $authUser   = auth()->user();
            $report     = Reports::find($report_id);
            $desc     = $request->get('desc', '');
            $link     = $request->get('link', '');
            $separator = "-:";
            $report     = Reports::find($report_id);

            if (!($report && $report->draft_report))  return ApiResponse::__createBadResponse('Report not found');
            if ($report->users_id  != $authUser->id) return ApiResponse::__createBadResponse('only author can edit report.');

            if (!$request->has('local_image'))  return ApiResponse::createValidationResponse(['local_image' => 'media must be required']);

            if ($request->hasFile('local_image') && $request->file('local_image') && $request->file('local_image')->isValid()) {
                if (strpos($request->file('local_image')->getMimeType(), 'image') !== false) {
                    $local_image    = $request->file('local_image');
                    $filename = microtime(true) . '-' . $authUser->id . '-report-image.' . $local_image->getClientOriginalExtension();

                    if (@$local_image->getSize() > 204800) {
                        ini_set('memory_limit', '256M');
                        $cropped = Image::make($local_image)->resize(670, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        Storage::disk('s3')->put('reports_photo/th670/' . $filename, $cropped->encode(), 'public');
                        $report_photo_url = 'https://s3.amazonaws.com/travooo-images2/reports_photo/th670/' . $filename;
                    } else {
                        Storage::disk('s3')->putFileAs('reports_photo/', $local_image, $filename, 'public');
                        $report_photo_url = 'https://s3.amazonaws.com/travooo-images2/reports_photo/' . $filename;
                    }


                    $rep_info = ReportsDraftInfos::where('reports_draft_id', $report->draft_report->id)->orderBy('order', 'DESC')->first();

                    $photo = new ReportsDraftInfos;
                    $photo->reports_draft_id = $report->draft_report->id;
                    $photo->var = ReportsService::VAR_PHOTO;
                    $photo->val = $report_photo_url . $separator . $desc . $separator . $link;
                    $photo->order = (@$rep_info->order ?? 0) + 1;

                    if ($photo->save()) {
                        $_ = @explode($separator, $photo->val);
                        $_data['url']   = @$_[0];
                        $_data['desc']  = @$_[1] ?? null;
                        $_data['link']  = @$_[2] ?? null;
                        $photo->data = $_data;
                        return ApiResponse::create($photo);
                    } else {
                        return ApiResponse::__createServerError();
                    }
                }
                return ApiResponse::createValidationResponse(['local_image' => 'file type is invalid']);
            }
            return ApiResponse::createValidationResponse(['local_image' => 'image must be required']);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/reports/{report_id}/upload-video",
     *   tags={"Travelogs"},
     *   summary="Upload Video",
     *   operationId="app\Http\Controllers\Api\ReportsController@postUploadVideo",
     *   security={
     *       {"bearer_token": {}
     *    }},
     * @OA\Parameter(
     *      name="report_id",
     *      description="",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer"
     *      )
     * ),
     * @OA\Parameter(
     *      name="desc",
     *      description="",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="link",
     *      description="",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="local_video",
     *                  type="string",
     *                  format="binary"
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function postUploadVideo(Request $request, $report_id)
    {
        try {
            $authUser   = auth()->user();
            $report     = Reports::find($report_id);
            $desc     = $request->get('desc', '');
            $link     = $request->get('link', '');
            $separator = "-:";

            if (!($report && $report->draft_report))  return ApiResponse::__createBadResponse('Report not found');
            if ($report->users_id  != $authUser->id) return ApiResponse::__createBadResponse('only author can edit report.');

            if (!$request->has(ReportsService::VAR_LOCAL_VIDEO))  return ApiResponse::createValidationResponse([ReportsService::VAR_LOCAL_VIDEO => 'media must be required']);

            if ($request->hasFile(ReportsService::VAR_LOCAL_VIDEO) && $request->file(ReportsService::VAR_LOCAL_VIDEO) && $request->file(ReportsService::VAR_LOCAL_VIDEO)->isValid()) {

                $fileSize = $request->file(ReportsService::VAR_LOCAL_VIDEO)->getSize() / 1024 / 1024;
                if ($fileSize > 25)  return ApiResponse::createValidationResponse([ReportsService::VAR_LOCAL_VIDEO => 'maximum 25mb size video allowed']);

                $local_video    = $request->file(ReportsService::VAR_LOCAL_VIDEO);
                $filename = microtime(true) . '-' . $authUser->id . '-report-video.' . $local_video->getClientOriginalName();
                Storage::disk('s3')->putFileAs('reports_video/', $local_video, $filename, 'public');
                $report_video_url = S3_BASE_URL . 'reports_video/' . $filename;

                $rep_info = ReportsDraftInfos::where('reports_draft_id', $report->draft_report->id)->orderBy('order', 'DESC')->first();

                $video = new ReportsDraftInfos;
                $video->reports_draft_id = $report->draft_report->id;
                $video->var = ReportsService::VAR_LOCAL_VIDEO;
                $video->val = $report_video_url . $separator . $desc . $separator . $link;
                $video->order = (@$rep_info->order ?? 0) + 1;
                if ($video->save()) {
                    $_ = @explode($separator, $video->val);
                    $_data['url']   = @$_[0];
                    $_data['desc']  = @$_[1];
                    $_data['link']  = @$_[2];
                    $video->data = $_data;
                    return ApiResponse::create($video);
                } else {
                    return ApiResponse::__createServerError();
                }
            }
            return ApiResponse::createValidationResponse([ReportsService::VAR_LOCAL_VIDEO => 'video must be required']);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    // search apis | search apis | search apis | search apis | search apis


    /**
     * @OA\Get(
     ** path="/reports/search-place",
     *   tags={"Travelogs"},
     *   summary="Search Place",
     *   operationId="search_place",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="q",
     *        description="Place name",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/



    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    function searchPlace(Request $request)
    {
        try {
            $query = $request->get('q');
            if ($query != '') {
                $qr = app(HomeController::class)->getElSearchResult($query)['result'];
                $result = [];
                foreach ($qr as $r) {
                    $_place_type = explode(",", $r['place_type']);
                    if (isset($_place_type[0], $r['place_id']) && in_array($_place_type[0], getAllowedPlaceTypes())) {
                        if ($r['countries_id'] != 326 && $r['cities_id'] != 2031 && $r['countries_id'] != 373) {
                            $medium = [];
                            $medium['id'] = $r['place_id'];
                            $p = Place::find($medium['id']);
                            if ($p) {
                                $medium['title'] = $r['place_title'];
                                $medium['address'] = $r['address'];
                                $medium['location'] = $r['lat']  . "," . $r['lng'];
                                $medium['lat'] = $r['lat'];
                                $medium['lng'] = $r['lng'];
                                $medium['place_type'] = $_place_type[0];

                                $medium['image'] = asset(PLACE_PLACEHOLDERS);
                                if (isset($r['place_media']) && $r['place_media'] != null) {
                                    $medium['image'] = check_media_url($r['place_media']);
                                }

                                $medium['city_id'] = $p->cities_id;
                                $medium['city_title'] = @$p->city->transsingle->title;
                                $medium['country_id'] = $p->countries_id;
                                $medium['country_title'] = @$p->country->transsingle->title;

                                $medium['ratting'] = Reviews::where('places_id', $r['place_id'])->avg('score') ?? 0;
                                $medium['total_reviews'] = Reviews::where('places_id', $r['place_id'])->count();
                                $medium['recent_reviews'] = Reviews::where('places_id', $r['place_id'])->take(2)->get()->each(function ($placeReviewer) {
                                    $placeReviewer->author;
                                });
                                $medium['total_followers'] = PlaceFollowers::where('places_id', $r['place_id'])->count();
                                $medium['recent_followers'] = PlaceFollowers::where('places_id', $r['place_id'])->take(4)->get()->map(function ($placeFollower) {
                                    return [
                                        'id' => $placeFollower->users_id,
                                        'profile_picture' => check_profile_picture($placeFollower->user->profile_picture),
                                    ];
                                });
                                $result[] = $medium;
                            }
                        }
                    }
                }

                return ApiResponse::create($result);
            } else {
                return ApiResponse::createValidationResponse([
                    'q' => 'Search Term not provided'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/{report_id}/search-location-details",
     *   tags={"Travelogs"},
     *   summary="https://app.zeplin.io/project/5e8dc98975eb98226f385503/screen/5ff6e82a09df755a34b43a41",
     *   operationId="Api/ReportsController@searchLocationWithFullDetails",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="report_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="q",
     *        description="Location name (Country, City)",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     **/
    function searchLocationWithFullDetails(Request $request, ReportsService $reportsService, $report_id)
    {
        try {
            $report = Reports::find($report_id);
            if (!($report && $report->draft_report)) {
                return ApiResponse::__createBadResponse('report not found');
            }

            return  ApiResponse::create($reportsService->preparedInfoPopup($report->draft_report));

            $search = trim($request->get('q', ''));
            $cities = collect([]);
            $countries = collect([]);

            if (strlen(trim($report->countries_id))) {
                $countries = Countries::query()
                    ->whereIn('id', explode(",", $report->countries_id))
                    ->when(strlen($search), function ($q) use ($search) {
                        $q->whereHas('transsingle', function ($query) use ($search) {
                            $query->where('title', 'like', $search . "%");
                        });
                    })->get();
            }

            if (strlen(trim($report->cities_id))) {
                $cities = Cities::query()
                    ->whereIn('id', explode(",", $report->cities_id))
                    ->when(strlen($search), function ($q) use ($search) {
                        $q->whereHas('transsingle', function ($query) use ($search) {
                            $query->where('title', 'like', $search . "%");
                        });
                    })->get();
            }

            $result = _createLocationObjectWithFullDetailsForReport($cities, $countries);

            return ApiResponse::create($result);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/search-location",
     *   tags={"Travelogs"},
     *   summary="Search locations",
     *   operationId="search_locations",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="q",
     *        description="Location Name",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    function searchLocation(Request $request)
    {
        try {
            $query = $request->get('q');
            if ($query) {
                $locations = [];
                $queryParam = $query;

                $get_countries = Countries::with('transsingle', 'getMedias')
                    ->whereHas('transsingle', function ($query) use ($queryParam) {
                        $query->where('title', 'like', $queryParam . "%");
                    })->take(20)->get();

                $get_cities = Cities::with('transsingle', 'getMedias')
                    ->whereHas('transsingle', function ($query) use ($queryParam) {
                        $query->where('title', 'like', $queryParam . "%");
                    })->take(20)->get();


                foreach ($get_countries as $c) {
                    $img = check_country_photo(@$c->getMedias[0]->url, 180);
                    unset($c->getMedias);
                    $locations[] = [
                        'id' => $c->id . '-country',
                        'text' => $c->transsingle->title,
                        'image' => $img,
                        'ob' => $c,
                        'type' => '',
                        'query' => $query
                    ];
                }

                foreach ($get_cities as $city) {
                    $img = check_country_photo(@$city->getMedias[0]->url, 180);
                    unset($city->getMedias);
                    $locations[] = [
                        'id' => $city->id . '-city',
                        'text' => $city->transsingle->title,
                        'image' => $img,
                        'ob' => $city,
                        'type' => $city->country->transsingle->title,
                        'query' => $query
                    ];
                }

                return ApiResponse::create($locations);
            } else {
                return ApiResponse::createValidationResponse([
                    'q' => 'Search Term not provided'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/search-plan",
     *   tags={"Travelogs"},
     *   summary="search trip plan",
     *   operationId="app\Http\Controllers\Api\ReportsController@getMorePlan",
     *   security={
     *       {"bearer_token": {}
     *    }},
     * @OA\Parameter(
     *        name="search",
     *        description="search",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="type",
     *        description="Search reports type: mine | invited",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function getMorePlan(Request $request)
    {
        try {
            $filter = trim($request->get('search', ''));
            $type   = $request->type;

            $plan_query = TripPlans::query();
            if (isset($type) && $type == 'invited') {
                $plan_query->whereHas('contribution_requests', function ($q) {
                    $q->where('users_id', Auth::id());
                });
            } else if (isset($type) && $type == 'mine') {
                $plan_query->where('users_id', Auth::id())->where('active', 1);
            } else {
                $plan_query->where('users_id', Auth::id())->where('active', 1)
                    ->orWhereHas('contribution_requests', function ($q) {
                        $q->where('users_id', Auth::id());
                    });
            }
            $plan_list = $plan_query->when(strlen($filter), function ($q) use ($filter) {
                $q->where('title', 'like', '%' . $filter . '%');
            });

            $plan_list = (clone $plan_query)->take(10)->get();
            $plan_count = (clone $plan_query)->count();

            // ->map(function ($plan) {
            //     return travelogTripObject($plan->id);
            // });

            $data = [];
            foreach ($plan_list as $plan) {
                $plan->photo;
                $plan->_attribute()->each(function ($value, $key) use (&$plan) {
                    $plan->$key = $value;
                });

                try {
                    $plan->static_map_image = tripPlanThumb($plan, 615, 400);
                } catch (\Throwable $e) {
                    $plan->static_map_image = tripPlanStaticImage($plan);
                }

                unset($plan->published_places, $plan->activeversion, $plan->trips_cities);
                $data[] = $plan;
            }

            return ApiResponse::create([
                'count' => $plan_count,
                'plans' => $data
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/search-map",
     *   tags={"Travelogs"},
     *   summary="Search City,Country or Place",
     *   operationId="search_map",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="q",
     *        description="City, Country or Place name",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function searchMap(Request $request)
    {
        try {
            $query = $request->get('q');
            if ($query) {
                $result = [];
                $qr = collect([]);
                try {
                    $google_link = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . urlencode($query) . '.json?limit=20&routing=true&access_token=' . config('mapbox.token');
                    $fetch_link = file_get_contents($google_link);
                    $query_result = json_decode($fetch_link);
                    $qr = $query_result->features;
                } catch (\Throwable $e) {
                }

                foreach ($qr as $r) {
                    if (@$r->types[0] != 'country') {
                        if (!Place::where('provider_id', '=', $r->id)->exists()) {
                            $place_country = explode(', ', $r->place_name);
                            $id = $r->geometry->coordinates[1] . '-:' . $r->geometry->coordinates[0] . '-:' . $r->text . '-:12-:' . $this->_getPlaceCountryInfo(end($place_country));
                            $temp = @explode("-:", $id);
                            $result[] = [
                                'id' => $id,
                                'text' => $r->text,
                                'lat' => $r->geometry->coordinates[1],
                                'lng' => $r->geometry->coordinates[0],
                                'zoom' => 12,
                                'image' => "https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/" . strtolower(@$temp[5]) . ".png",
                                'type' => 'Place',
                                'type_desc' => end($place_country),
                                'val' => $id,
                                'population' => @$temp[6],
                                'country_id' => @$temp[7],
                                'country_follow_flag' => get_country_follow(@$temp[7]),
                            ];
                        } else {
                            $p = Place::with('transsingle')->where('provider_id', '=', $r->id)->get()->first();
                            if ($p) {
                                $id = $p->lat . '-:' . $p->lng . '-:' . $p->transsingle->title . '-:12-:' . $this->_getPlaceCountryInfo(@$p->country->transsingle->title);
                                $temp = @explode("-:", $id);
                                $result[] = [
                                    'id' => $id,
                                    'text' => $p->transsingle->title,
                                    'lat' => $p->lat,
                                    'lng' => $p->lng,
                                    'zoom' => 12,
                                    'image' => "https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/" . strtolower(@$temp[5]) . ".png",
                                    'type' => 'Place',
                                    'type_desc' => @$p->country->transsingle->title,
                                    'val' => $id,
                                    'population' => @$temp[6],
                                    'country_id' => @$temp[7],
                                    'country_follow_flag' => get_country_follow(@$temp[7]),
                                ];
                            }
                        }
                    }
                }

                $get_countries = Countries::with('transsingle')
                    ->whereHas('transsingle', function ($q) use ($query) {
                        $q->where('title', 'like',  $query . "%");
                    })->take(15)->get();
                foreach ($get_countries as $c) {
                    $id = $c->lat . '-:' . $c->lng . '-:' . $c->transsingle->title . '-:5-:' . $c->transsingle->title . '-:' . $c->iso_code . '-:' . str_replace(',', '.', $c->transsingle->population) . '-:' . $c->id;
                    $temp = @explode("-:", $id);
                    $result[] = [
                        'id' => $id,
                        'text' => $c->transsingle->title,
                        'lat' => $c->lat,
                        'lng' => $c->lng,
                        'zoom' => 5,
                        'image' => "https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/" . strtolower(@$temp[5]) . ".png",
                        'type' => 'Country',
                        'type_desc' => '',
                        'val' => $id,
                        'population' => @$temp[6],
                        'country_id' => @$temp[7],
                        'country_follow_flag' => get_country_follow(@$temp[7]),
                    ];
                }

                $get_cities = Cities::with('transsingle')
                    ->whereHas('transsingle', function ($q) use ($query) {
                        $q->where('title', 'like',  $query . "%");
                    })->take(15)->get();
                foreach ($get_cities as $ci) {
                    $id = $ci->lat . '-:' . $ci->lng . '-:' . $ci->transsingle->title . '-:8-:' . $ci->transsingle->title . '-:' . $ci->country->iso_code . '-:' . str_replace(',', '.', $ci->transsingle->population) . '-:' . $ci->country->id;
                    $temp = @explode("-:", $id);
                    $result[] = [
                        'id' => $id,
                        'text' => $ci->transsingle->title,
                        'lat' => $ci->lat,
                        'lng' => $ci->lng,
                        'zoom' => 8,
                        'image' => "https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/" . strtolower(@$temp[5]) . ".png",
                        'type' => 'City',
                        'type_desc' => @$ci->country->transsingle->title,
                        'val' => $id,
                        'population' => @$temp[6],
                        'country_id' => @$temp[7],
                        'country_follow_flag' => get_country_follow(@$temp[7]),
                    ];
                }

                return ApiResponse::create([
                    'results' => $result
                ]);
            } else {
                return ApiResponse::createValidationResponse([
                    'q' => 'Search Term not provided'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/search-user",
     *   tags={"Travelogs"},
     *   summary="Search Users",
     *   operationId="search_users",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="q",
     *        description="User Name",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function searchUser(Request $request)
    {
        try {
            $authUser = auth()->user();
            $query = $request->get('q', '');
            if ($query) {
                $users = User::query()
                    ->select('id', 'name', 'username', 'email', 'cover_photo', 'profile_picture')
                    ->when($authUser, function ($q) use ($authUser) {
                        $q->where('id', '!=', $authUser->id);
                    })
                    ->where('username', 'like', "{$query}%")
                    ->orWhere('name', 'like', "{$query}%")
                    ->take(25)->get()->map(function (&$user) use ($authUser) {
                        $user->follow_flag = ($authUser) ? UsersFollowers::where('users_id', $user->id)->where('followers_id', $authUser->id)->exists() : false;
                        $user->cover_photo  = check_cover_photo($user->cover_photo);
                        $user->profile_picture = check_profile_picture($user->profile_picture);
                        $user->media = Media::select('url')
                            ->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4')
                            ->where('users_id', $user->id)->take(3)->pluck('url')
                            ->map(function (&$url) {
                                return check_media_url($url);
                            });
                        return $user;
                    });
                return ApiResponse::create($users);
            } else {
                return ApiResponse::createValidationResponse([
                    'q' => 'Search Term not provided'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
