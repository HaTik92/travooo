<?php

namespace App\Http\Controllers\Api\Traits;

use App\Http\Responses\ApiResponse;
use App\Models\City\CitiesMedias;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\Place\Place;
use App\Models\Place\PlaceFollowers;
use App\Models\Place\PlaceMedias;
use App\Models\Posts\Checkins;
use App\Models\Posts\PostsShares;
use App\Models\Reports\Reports;
use App\Models\Reviews\Reviews;
use App\Models\TripPlaces\TripPlaces;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Api\ShareService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Events\Events;

trait PlaceLocationTrait
{
    /**
     * @OA\Get(
     ** path="/place/{id}/",
     *   tags={"Places"},
     *   summary="Fetch Place [bearer_token is optional]",
     *   operationId="app\Http\Controllers\Api\Traits\PlaceLocationTrait@getIndex",
     *    @OA\Parameter(
     *        name="id",
     *        description="Place Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     * @param integer $place_id
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request, $id)
    {
        try {
            $authUser = auth()->user();
            $language_id = 1;
            $obj = Place::with([
                'trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
            ])->where('id', $id)->where('active', 1)->first();

            if (!$obj) return ApiResponse::__createBadResponse("place not found");
            $country =  @$obj->country;
            $city =  @$obj->cities;
            unset($obj->country, $obj->cities);

            // basic details & flags & counts
            $obj->image_url = check_place_photo($obj);
            $obj->place_type = do_placetype($obj->place_type);
            $obj->title = @$obj->trans[0]->title;
            $obj->description = strip_tags(@$obj->trans[0]->description ?? '');
            $obj->address = strip_tags(@$obj->trans[0]->address ?? '');
            $obj->country = @$country->trans[0]->title;
            $obj->city = @$city->trans[0]->title;

            $obj->city_image_url = url(PLACE_PLACEHOLDERS);
            $firstCityMedia = CitiesMedias::whereHas('medias')->where('cities_id', @$city->id)->first();
            if ($firstCityMedia) {
                $obj->city_image_url = check_city_photo($firstCityMedia->medias->url);
            }
            $obj->city_lat =  @$city->lng;
            $obj->city_lng =  @$city->lng;

            $obj->total_review = Reviews::where('places_id', $obj->id)->count();
            $obj->total_avg = (float) number_format(Reviews::where('places_id', $obj->id)->avg('score'), 2);
            $obj->total_follower = PlaceFollowers::where('places_id', $obj->id)->count();
            $obj->follow_status = $authUser ? ((bool) PlaceFollowers::where('places_id', $obj->id)->where('users_id', $authUser->id)->exists()) : false;
            $obj->i_was_here_flag = $authUser ? (Checkins::where('city_id', $obj->id)->where('users_id', $authUser->id)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->count() > 0) : false;
            $obj->total_share = PostsShares::where('type', ShareService::TYPE_PLACE)->count();
            $obj->total_checkins = Checkins::where('place_id', $obj->id)->whereHas('user')->where('users_id', '>', 0)->count();
            $obj->past_24h_total_checkins = Checkins::where('place_id', $obj->id)->whereHas('user')->where('users_id', '>', 0)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->count();
            $obj->past_24h_top_checkins = Checkins::where('place_id', $obj->id)->whereHas('user')->with('user')->where('users_id', '>', 0)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->orderBy('id', 'DESC')->limit(3)->get()->map(function ($check_in) {
                return [
                    'id' => $check_in->user->id,
                    'username' => $check_in->user->username,
                    'name' => $check_in->user->name,
                    'profile_picture' => check_profile_picture($check_in->user->profile_picture)
                ];
            });
            // End basic details & flags & counts

            // ABOUT SECTION
            {
                $about = [];
                $about['phone_code'] = $obj->code;
                $about['population'] = @$obj->trans[0]->population;
                $about['nationality'] = @$country->trans[0]->nationality;
                $about['working_days'] = (@$obj->trans[0]->working_days != '') ? @$obj->trans[0]->working_days : @$country->trans[0]->working_days;

                $speedLimit = (@$obj->trans[0]->speed_limit != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->speed_limit) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->speed_limit);
                $speedLimits = [];
                foreach ($speedLimit as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $speedLimits[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['speed_limit'] = $speedLimits;
                (@$obj->trans[0]->metrics != '') ? preg_match_all("/\(([^\]]*)\)/", @$obj->trans[0]->metrics, $matches) : preg_match_all("/\(([^\]]*)\)/", @$country->trans[0]->metrics, $matches);
                $about['metrics'] = isset($matches[1][0]) ? explode(", ", $matches[1][0]) : [];
                $about['internet'] = (@$obj->trans[0]->internet != '') ? @$obj->trans[0]->internet : @$country->trans[0]->internet;

                $timing = (@$obj->trans[0]->best_time != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->best_time) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->best_time);
                $timings = [];
                foreach ($timing as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $timings[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['general']['timing'] = $timings;
                $about['general']['transportation'] = (@$obj->trans[0]->transportation != '') ? explode(", ", @$obj->trans[0]->transportation) : explode(", ", @$country->trans[0]->transportation);
                $about['general']['currencies'] = @$country->currencies[0]->transsingle->title;

                // $about['medias'] = count(@$obj->getMedias) > 0 ? @$obj->getMedias : @$country->getMedias;

                $socket = (@$obj->trans[0]->sockets != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->sockets) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->sockets);
                $sockets = [];
                foreach ($socket as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $sockets[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['sockets'] = $sockets;
                $about['cost_of_living'] = (@$obj->trans[0]->cost_of_living != '') ? @$obj->trans[0]->cost_of_living : @$country->trans[0]->cost_of_living;
                $about['crime_rate'] = (@$obj->trans[0]->geo_stats != '') ? @$obj->trans[0]->geo_stats : @$country->trans[0]->geo_stats;
                $about['quality_of_life'] = (@$obj->trans[0]->demographics != '') ? @$obj->trans[0]->demographics : @$country->trans[0]->demographics;

                $economy = (@$obj->trans[0]->economy != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->economy) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$country->trans[0]->economy);
                $economies = [];
                foreach ($economy as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $economies[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['restrictions'] = $economies;

                $about['emergency_numbers'] = '';
                @$obj_emergency = (count((@$obj->emergency ?? [])) > 0) ? @$obj->emergency : @$country->emergency;
                if (@$obj_emergency) {
                    $emerg = [];
                    foreach (@$obj_emergency as $emergency) {
                        $emerg[] = $emergency->transsingle->title;
                    }

                    $about['emergency_numbers'] = $emerg;
                }

                $about['general']['religions'] = '';
                @$obj_religions = (count((@$obj->religions ?? [])) > 0) ? @$obj->religions : @$country->religions;
                if (@$obj_religions) {
                    $rel = [];
                    foreach (@$obj_religions as $religions) {
                        $rel[] = $religions->transsingle->title;
                    }
                    $about['general']['religions'] = $rel;
                }

                $about['holidays'] = '';
                $obj_holiday = (count((@$city->cityHolidays ?? [])) > 0) ? @$city->cityHolidays : @$country->countryHolidays;
                if (@$obj_holiday) {
                    $hol = [];
                    foreach (@$obj_holiday as $holidays) {
                        if (isset($holidays->holiday->transsingle) && !empty($holidays->holiday->transsingle)) {
                            $hol[] = $holidays->holiday->transsingle->title;
                        }
                    }
                    $about['holidays'] = $hol;
                }

                $about['general']['languages_spoken'] = '';
                $lng = [];
                @$languages_spoken = (count((@$obj->languages_spoken ?? [])) > 0) ? @$obj->languages_spoken : @$country->languages;
                if (@$languages_spoken) {
                    foreach (@$languages_spoken as $languages) {
                        $lng[] = $languages->transsingle->title;
                    }
                }

                @$obj_additional_languages = (count((@$obj->additional_languages ?? [])) > 0) ? @$obj->additional_languages : @$country->additional_languages;
                if (@$obj_additional_languages) {
                    foreach (@$obj_additional_languages as $additional_languages) {
                        $lng[] = $additional_languages->transsingle->title;
                    }

                    $about['general']['languages_spoken'] = $lng;
                }
            }
            $obj->about = $about;
            // End About Section

            if ($authUser) {
                //where raw
                $friendRaw = 'users_id in (SELECT `uf`.`users_id` FROM `users_followers` AS `uf` INNER JOIN `users_followers` AS `uf2` ON `uf`.`users_id` = `uf2`.`followers_id` AND `uf2`.`users_id` = `uf`.`followers_id` AND `uf2`.`follow_type` = `uf`.`follow_type` WHERE `uf`.`users_id` != `uf`.`followers_id` AND `uf`.`followers_id` =' . $authUser->id . ' AND `uf`.`follow_type` = 1)';
                $topExpertsRaw = "users_id in (select id from users where nationality != $authUser->nationality)";
                $localExpertsRaw = "users_id in (select id from users where nationality = $authUser->nationality)";

                $experts['total_top_experts'] = ExpertsCities::whereHas('user')->whereRaw($topExpertsRaw)->where('cities_id', $obj->cities_id)->count();
                $experts['total_local_experts'] = ExpertsCities::whereHas('user')->whereRaw($localExpertsRaw)->where('cities_id', $obj->cities_id)->count();
                $experts['total_friends_experts'] = ExpertsCities::whereHas('user')->whereRaw($friendRaw)->where('cities_id', $obj->cities_id)->count();
                $experts['top_experts'] = ExpertsCities::whereHas('user')->whereRaw($topExpertsRaw)->where('cities_id', $obj->cities_id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
                $experts['local_experts'] = ExpertsCities::whereHas('user')->whereRaw($localExpertsRaw)->where('cities_id', $obj->cities_id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
                $experts['friends_experts'] = ExpertsCities::whereHas('user')->whereRaw($friendRaw)->where('cities_id', $obj->cities_id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
            } else {
                $experts['total_top_experts'] = ExpertsCities::whereHas('user')->where('cities_id', $obj->cities_id)->count();
                $experts['total_local_experts'] = 0;
                $experts['total_friends_experts'] = 0;
                $experts['top_experts'] = ExpertsCities::whereHas('user')->where('cities_id', $obj->cities_id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
                $experts['local_experts'] = [];
                $experts['friends_experts'] = [];
            }
            $experts['total_live_checkin'] = Checkins::whereHas('user')->where('city_id', $obj->cities_id)->count();
            $experts['live_checkin'] = Checkins::select('id', 'users_id', DB::raw('city_id as cities_id'))->whereHas('user')->where('city_id', $obj->cities_id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();

            foreach ([$experts['top_experts'], $experts['local_experts'], $experts['friends_experts'], $experts['live_checkin']] as &$expertBatch) {
                $expertBatch = collect($expertBatch)->map(function ($expertUser) use ($authUser) {
                    $expertUser->follow_flag = ($authUser) ? UsersFollowers::where('users_id', $expertUser->users_id)->where('followers_id', $authUser->id)->exists() : false;
                    $expertUser->user->profile_picture = check_profile_picture($expertUser->user->profile_picture);
                });
            }

            $obj->experts = $experts;
            // End Experts and Local Experts

            $_trips_query = TripPlaces::query()
                ->whereRaw('versions_id = (SELECT trips.active_version FROM trips WHERE trips.id = trips_places.trips_id)')
                ->whereHas('trip', function ($q) use ($authUser) {
                    return $q->where('users_id', ($authUser) ? $authUser->id : -1);
                })
                ->where('places_id', $obj->id)
                ->groupBy('trips_id');

            $_trips = (clone $_trips_query)->with('trip')->orderBy('trips_id', 'DESC')->limit(5)->get();

            // Trips [Page = 1]
            $obj->trips = collect($_trips)->map(function ($tp) {
                return [
                    'id' => $tp->trips_id,
                    'title' => $tp->trip->title,
                ];
            })->toArray();
            // End Trips [Page = 1]

            $boxs = [];

            // Images Box
            $_trip = (clone $_trips_query)->with('trip')->orderBy('trips_id', 'DESC')->first();
            $boxs[] = [
                "title" => "Epic Trip Plans",
                "type" => "trip-plans",
                "img" => ($_trip) ? check_cover_photo($_trip->trip->cover) : url(PATTERN_PLACEHOLDERS),
                "count" => DB::table(DB::raw('(' . _getDBQuery($_trips_query) . ') as a'))->count()
            ];

            $_trending_media = PlaceMedias::with('media', 'media.author')->whereHas('media', function ($q) {
                return $q->whereHas('author')->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4');
            })->where('places_id', $obj->id)->orderBy('id', 'DESC')->first();
            $boxs[] = [
                "title" => "Trending Media",
                "type" => "medias",
                "img" => ($_trending_media) ? check_media_url($_trending_media->media->url) : url(PATTERN_PLACEHOLDERS),
                "count" => PlaceMedias::whereHas('media', function ($q) {
                    return $q->whereHas('author')->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4');
                })->where('places_id', $obj->id)->count()
            ];

            $totalExperts =  ExpertsCities::whereHas('user')->where('cities_id', $obj->cities_id)->count();
            $boxs[] = [
                "title" => "Experts",
                "type" => "experts",
                "experts_img" => $totalExperts ?  ExpertsCities::whereHas('user')->where('cities_id', $obj->cities_id)->with('user')->orderBy('id', 'desc')->take(6)->get()->map(function ($expertUser) use ($authUser) {
                    return $expertUser->user->profile_picture = check_profile_picture($expertUser->user->profile_picture);
                })->toArray() : [url(MALE_PLACEHOLDERS)],
                "count" => $totalExperts
            ];

            $_report  = Reports::whereHas('cover')->where('cities_id', '=', @$obj->city->id)->where('flag', '=', 1)->first();
            $boxs[] = [
                "title" => "Reports",
                "type" => "reports",
                "img" =>  isset($_report->cover[0]->val) ? $_report->cover[0]->val : url(PATTERN_PLACEHOLDERS),
                "count" =>  Reports::where('cities_id', '=', @$obj->city->id)->where('flag', '=', 1)->count()
            ];

            $rand = rand(1, 10);
            $category = array('community', 'concerts', 'conferences', 'expos', 'festivals', 'performing-arts', 'politics', 'school-holidays', 'sports');

            $boxs[] = [
                "title" => "Events",
                "type" => "events",
                "img" =>  url('assets2/image/events/' . $category[$rand % 9] . '/' . $rand . '.jpg'),
                "count" => Events::query()->where('places_id', $obj->id)->count()
            ];

            // $obj->image_cards = $imageBox;
            $obj->image_cards = $boxs;
            // End Images Box

            unset($obj->trans, $obj->getMedias);
            return ApiResponse::create($obj);

















            // old code // old code // old code // old code // old code


            // $place = Place::find($place_id);
            // if (!$place) {
            //     return ApiResponse::create(
            //         [
            //             'message' => ['Invalid Place ID']
            //         ],
            //         false,
            //         ApiResponse::BAD_REQUEST
            //     );
            // }

            // $is_new =  Input::get('is_new');
            // $place_addr = Input::get('address');
            // if (isset($is_new) && $is_new == 1) {
            //     $tempPlaceID =  $place_id;
            //     if (!Place::where('provider_id', '=', $place_id)->exists()) {
            //         $place_id =   $this->addNewPlaceByProviderId($place_id);
            //     } else {
            //         $place_id = Place::where('provider_id', '=', $place_id)->get()->first()->id;
            //     }
            // }

            // $language_id = 1;
            // $place = Place::find($place_id);
            // if (!$place) {
            //     return ApiResponse::create(
            //         [
            //             'message' => ['Invalid Place ID']
            //         ],
            //         false,
            //         ApiResponse::BAD_REQUEST
            //     );
            // }
            // $place->visits = $place->visits + 1;
            // $place->save();
            // $random_val = $this->_getRandom(date('j'));


            // $place = Place::with([
            //     'trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'getMedias',
            //     'getMedias.users',
            //     'followers'
            // ])
            //     ->where('id', $place_id)
            //     ->where('active', 1)
            //     ->first();

            // // $top_places = PlacesTop::where('country_id', $place->country->id)
            // //         ->orderBy('reviews_num', 'desc')
            // //         ->take(20)
            // //         ->get();


            // if (!$place) {
            //     return ApiResponse::create(
            //         [
            //             'message' => ['Not found']
            //         ],
            //         false,
            //         ApiResponse::BAD_REQUEST
            //     );
            // }

            // $db_place_reviews = Reviews::where('places_id', $place->id)->orderBy('created_at', 'desc')->get();
            // if (!count($db_place_reviews)) {
            //     $place_reviews = get_google_reviews($place->provider_id);
            //     if (is_array($place_reviews)) {
            //         foreach ($place_reviews as $review) {
            //             $r = new Reviews;
            //             $r->places_id = $place->id;
            //             $r->google_author_name = @$review->author_name;
            //             $r->google_author_url = @$review->author_url;
            //             $r->google_language = @$review->language;
            //             $r->google_profile_photo_url = @$review->profile_photo_url;
            //             $r->score = @$review->rating;
            //             $r->google_relative_time_description = @$review->relative_time_description;
            //             $r->text = @$review->text;
            //             $r->google_time = @$review->time;
            //             $r->save();
            //         }
            //     }
            // }

            // $db_place_reviews = Reviews::where('places_id', $place->id)->orderBy('created_at', 'desc')->get();
            // $data['reviews'] = $db_place_reviews;
            // $data['reviews_places'] = Reviews::where('places_id', $place->id)->get();

            // $data['reviews_avg'] = Reviews::where('places_id', $place->id)->avg('score');
            // get places nearby
            // $data['places_nearby'] = Place::with('medias')
            //         ->whereHas("medias", function ($query) {
            //             $query->where("medias_id", ">", 0);
            //         })
            //         ->where('cities_id', $place->cities_id)
            //         ->where('id', '!=', $place->id)
            //         ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
            //         ->having('distance', '<', 15)
            //         ->orderBy('distance')
            //         ->take(10)
            //         ->get();
            // $checkins = Checkins::whereHas("place", function ($query) use ($place) {
            //             $query->where('cities_id', $place->cities_id);
            //             $query->where('id', '!=', $place->id);
            //         })
            //         ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) * cos( radians( SUBSTRING(lat_lng, INSTR(lat_lng, \',\') + 1) ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) ) ) AS distance'))
            //         // ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
            //         ->having('distance', '<', 15)
            //         ->orderBy('distance')
            //         ->take(10)
            //         ->get();
            // $result = array();
            // foreach ($checkins AS $checkin) {
            //     $result[] = array(
            //         // 'post_id' => $checkin->post_checkin->post->id,
            //         'lat' => $checkin->place->lat,
            //         'lng' => $checkin->place->lng,
            //         // 'photo' => @$checkin->post_checkin->post->medias[0]->media->url,
            //         'name' => @$checkin->user->name,
            //         'id' => @$checkin->user->id,
            //         'title' => $checkin->location,
            //         'profile_picture' => check_profile_picture(@$checkin->user->profile_picture),
            //         'date' => diffForHumans($checkin->created_at)
            //     );
            // }
            // $data['places_nearby'] = $result;
            // $checkins = Checkins::where('place_id', $place->id)->orderBy('id', 'DESC')->get();
            // $result = array();
            // $done = array();
            // foreach ($checkins AS $checkin) {
            //     if (!isset($done[$checkin->users_id])) {
            //         $result[] = $checkin;
            //         $done[$checkin->users_id] = true;
            //     }
            // }
            // $data['checkins'] = $result;


            // $data['place'] = $place;
            // $data['top_places'] = $top_places;

            // $city_name = @$place->city->transsingle->title;

            // $city = $place->city;
            // $city->country;
            // $return['id'] = $city->id;
            // $return['lat'] = $city->lat;
            // $return['lng'] = $city->lng;
            // $return['city'] = @$city->trans[0]->title;
            // $return['country'] = @$city->country->trans[0]->title;
            // $return['phone_code'] = $city->code;
            // $return['population'] = @$city->trans[0]->population;
            // $return['nationality'] = @$city->country->trans[0]->nationality;
            // $return['working_days'] = (@$city->trans[0]->working_days != '') ? @$city->trans[0]->working_days : @$city->country->trans[0]->working_days;
            // $return['transportation'] = (@$city->trans[0]->transportation != '') ? explode(", ", @$city->trans[0]->transportation) : explode(", ", @$city->country->trans[0]->transportation);
            // $speedLimit = (@$city->trans[0]->speed_limit != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->speed_limit) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->speed_limit);
            // $speedLimits = [];
            // foreach ($speedLimit as $value) {
            //     $speedLimits[] = [
            //         "type" => explode(": ", $value)[0],
            //         "value" => explode(": ", $value)[1]
            //     ];
            // }
            // $return['speed_limit'] = $speedLimits;
            // (@$city->trans[0]->metrics != '') ? preg_match_all("/\(([^\]]*)\)/", @$city->trans[0]->metrics, $matches) : preg_match_all("/\(([^\]]*)\)/", @$city->country->trans[0]->metrics, $matches);
            // $return['metrics'] = explode(", ", $matches[1][0]);
            // $return['internet'] = (@$city->trans[0]->internet != '') ? @$city->trans[0]->internet : @$city->country->trans[0]->internet;

            // $timing = (@$city->trans[0]->best_time != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->best_time) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->best_time);
            // $timings = [];
            // foreach ($timing as $value) {
            //     $timings[] = [
            //         "type" => explode(": ", $value)[0],
            //         "value" => explode(": ", $value)[1]
            //     ];
            // }
            // $return['timing'] = $timings;

            // $return['medias'] = count(@$city->getMedias) > 0 ? @$city->getMedias : @$city->country->getMedias;

            // $socket = (@$city->trans[0]->sockets != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->sockets) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->sockets);
            // $sockets = [];
            // foreach ($socket as $value) {
            //     $sockets[] = [
            //         "type" => explode(": ", $value)[0],
            //         "value" => explode(": ", $value)[1]
            //     ];
            // }
            // $return['sockets'] = $sockets;
            // $return['cost_of_living'] = (@$city->trans[0]->cost_of_living != '') ? @$city->trans[0]->cost_of_living : @$city->country->trans[0]->cost_of_living;
            // $return['crime_rate'] = (@$city->trans[0]->geo_stats != '') ? @$city->trans[0]->geo_stats : @$city->country->trans[0]->geo_stats;
            // $return['quality_of_life'] = (@$city->trans[0]->demographics != '') ? @$city->trans[0]->demographics : @$city->country->trans[0]->demographics;
            // $return['currencies'] = @$city->country->currencies[0]->transsingle->title;
            // $economy = (@$city->trans[0]->economy != '') ? preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->trans[0]->economy) : preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$city->country->trans[0]->economy);
            // $economies = [];
            // foreach ($economy as $value) {
            //     $economies[] = [
            //         "type" => explode(": ", $value)[0],
            //         "value" => explode(": ", $value)[1]
            //     ];
            // }
            // $return['restrictions'] = $economies;

            // $return['emergency_numbers'] = '';
            // @$city_emergency = (count(@$city->emergency) > 0) ? @$city->emergency : @$city->country->emergency;
            // if (@$city_emergency) {
            //     $emerg = [];
            //     foreach (@$city_emergency as $emergency) {
            //         $emerg[] = $emergency->transsingle->title;
            //     }

            //     $return['emergency_numbers'] = $emerg;
            // }

            // $return['religions'] = '';
            // @$city_religions = (count(@$city->religions) > 0) ? @$city->religions : @$city->country->religions;
            // if (@$city_religions) {
            //     $rel = [];
            //     foreach (@$city_religions as $religions) {
            //         $rel[] = $religions->transsingle->title;
            //     }
            //     $return['religions'] = $rel;
            // }

            // $return['holidays'] = '';
            // $city_holiday = (count(@$city->cityHolidays) > 0) ? @$city->cityHolidays : @$city->country->countryHolidays;

            // if (@$city_holiday) {
            //     $hol = [];
            //     foreach (@$city_holiday as $holidays) {
            //         if (isset($holidays->holiday->transsingle) && !empty($holidays->holiday->transsingle)) {
            //             $hol[] = $holidays->holiday->transsingle->title;
            //         }
            //     }
            //     $return['holidays'] = $hol;
            // }

            // $return['languages_spoken'] = '';
            // $lng = [];
            // @$languages_spoken = (count(@$city->languages_spoken) > 0) ? @$city->languages_spoken : @$city->country->languages;
            // if (@$languages_spoken) {
            //     foreach (@$languages_spoken as $languages) {
            //         $lng[] = $languages->transsingle->title;
            //     }
            // }

            // @$city_additional_languages = (count(@$city->additional_languages) > 0) ? @$city->additional_languages : @$city->country->additional_languages;
            // if (@$city_additional_languages) {
            //     foreach (@$city_additional_languages as $additional_languages) {
            //         $lng[] = $additional_languages->transsingle->title;
            //     }

            //     $return['languages_spoken'] = $lng;
            // }

            // $data["city"] = $return;

            // $data['city_code'] = 0;
            // $res = @file_get_contents('https://srv.wego.com/places/search?query=' . $city_name);
            // $results = json_decode($res);
            // $cities = array();
            // $i = 0;
            // if (isset($results[0]) and is_object($results[0]) and $results[0]->type == "city") {
            //     $data['city_code'] = $results[0]->code;
            // }

            // if (Auth::check()) {

            //     $list_trip_plan = TripPlaces::where('places_id', $place->id)->pluck('trips_id');
            //     $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)
            //         // ->whereNotIn('id', $list_trip_plan)
            //         ->get();

            //     $data['me'] = Auth::user();
            // }

            // $data['travelmates'] = TravelMatesRequests::whereHas('plan_place', function ($q) use ($place) {
            //     $q->where('places_id', '=', $place->id);
            // })
            //     ->groupBy('users_id')
            //     ->get();
            // if (count($data['travelmates']) == 0) {
            //     $data['travelmates'] = TravelMatesRequests::whereHas('plan_city', function ($q) use ($place) {
            //         $q->where('cities_id', '=', @$place->city->id);
            //     })
            //         ->groupBy('users_id')
            //         ->get();
            // }

            // $data['reportsinfo'] = ReportsInfos::where('var', '=', 'place')
            //     ->where('val', '=', $place->id)
            //     ->whereHas('report', function ($q) {
            //         $q->where('flag', '=', 1);
            //     })
            //     ->get();
            // if (count($data['reportsinfo']) == 0) {
            //     $data['reports'] = Reports::where('cities_id', '=', @$place->city->id)->where('flag', '=', 1)
            //         ->get();
            // }



            // // start posts collection
            // //$data['pposts'] = collect_posts('place', $place->id);
            // $data['pposts'] = '';

            // $data['place_discussions'] = Discussion::where('destination_type', 'Place')
            //     ->where('destination_id', $place->id)
            //     ->get();


            // // get experts into categories
            // $experts_all = $experts_top = $experts_local = $experts_friends = $live_checkins = array();
            // foreach ($place->city->experts as $key => $pce) {
            //     $experts_all[] = $pce;
            //     if (is_object($pce->user) && isset($data['me']) && is_object($data['me'])) {
            //         if ($pce->user->nationality != $data['me']->nationality) {
            //             $experts_top[] = $pce;
            //         } elseif ($pce->user->nationality == $data['me']->nationality) {
            //             $experts_local[] = $pce;
            //         }
            //     }
            //     if (is_object($pce->user)) {
            //         if (is_friend($pce->user->id)) {
            //             $experts_friends[] = $pce;
            //         }
            //     }
            // }
            // $data['experts_top'] = $experts_top;
            // $data['experts_local'] = $experts_local;
            // $data['experts_friends'] = $experts_friends;
            // $data['live_checkins'] = $place->checkins()->groupBy('users_id')->whereDate('checkin_time', '=', Carbon::today()->toDateString())->get();

            // $experts_ids = array();
            // foreach ($experts_all as $ea) {
            //     $experts_ids[] = $ea->users_id;
            // }



            // // Select *,count(*) as r, sum(A.replies) from
            // // (SELECT created_at, users_id, count(*) as replies FROM `discussion_replies` GROUP BY YEAR(created_at), Month(created_at), Day(created_at))
            // // as A GROUP BY A.users_id ORDER BY `users_id`
            // $data['place_top_contributors'] = [];
            // if (count($experts_ids) > 0) {
            //     $last_month = date("Y-m-d", strtotime('-180 days'));

            //     $sub = DiscussionReplies::selectRaw('*, count(*) as reps, sum(num_views) as views')
            //         ->whereRaw('users_id in (' . implode(',', $experts_ids) . ')')
            //         ->whereRaw('created_at >' . $last_month)
            //         ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'));

            //     $data['place_top_contributors'] = DiscussionReplies::from(DB::raw('(' . $sub->toSql() . ') as a'))
            //         ->selectRaw('*, count(*) as count, sum(a.reps) as reply_cnt, sum(a.views) as views')
            //         ->groupBy('a.users_id')
            //         ->orderBy('count', 'desc')
            //         ->take(20)
            //         ->get();
            // }

            // $data['top_places'] = PlacesTop::where('places_id', '!=', $place_id)
            //     ->where('city_id', $place->city->id)
            //     ->whereHas('place', function ($query) {
            //         $query->whereHas('medias');
            //     })
            //     ->orderByRaw('FIELD(id % 32, ' . $random_val . ')')
            //     ->take(4)
            //     ->get();

            // $data['trip_plans_collection'] = TripPlaces::where('places_id', $place->id)
            //     ->orderBy('id', 'DESC')
            //     ->get();
            // if (count($data['trip_plans_collection']) == 0) {
            //     $data['trip_plans_collection'] = TripPlaces::where('cities_id', $place->city->id)
            //         ->orderBy('id', 'DESC')
            //         // ->groupBy('trips_id')
            //         ->get();
            // }

            // if (Auth::check()) {
            //     $data['follower_status'] = (bool) PlaceFollowers::where('places_id', $place->id)
            //         ->where('users_id', auth()->user()->id)
            //         ->first();
            //     $data['i_was_here_flag'] =  $place->checkins()->where('users_id', Auth::user()->id)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->count();
            // } else {
            //     $data['i_was_here_flag'] = false;
            //     $data['follower_status'] = false;
            // }

            // if (request()->has('ref') and intval(request()->get('ref'))) {
            //     $data['referrer_is_there'] = intval(request()->get('ref'));
            // }
            // $live_users = [];
            // foreach ($data['live_checkins'] as $live_here) {
            //     $live_users[] = $live_here->users_id;
            // }
            // $temp_live_users =  User::whereIn('id', $live_users)->get();
            // $live_users = [];
            // foreach ($temp_live_users as $user) {
            //     $is_followed =  app('App\Http\Controllers\Api\ProfileController')->postCheckFollow($user->id);
            //     $live_users[$user->id] = ['users' => $user, 'is_followed' => $is_followed['success']];
            // }
            // $data['live_users'] = $live_users;
            // return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/place/{id}/experts",
     *   tags={"Places"},
     *   summary="Get Place Expert List In Pagination [bearer_token is optional]",
     *   operationId="App\Http\Controllers\Api\Traits\PlaceLocationTrait@getExpertsList",
     *   @OA\Parameter(
     *        name="place_id",
     *        description="Place Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="page",
     *        description="pagination page number",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="per_page",
     *        description="per page | default = 5",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function getExpertsList(Request $request, $id)
    {
        $obj = Place::where('id', $id)->where('active', 1)->first();
        if (!$obj) return ApiResponse::createValidationResponse(["id" => ["place not found"]]);

        $authUser   = auth()->user();
        $page       = (int) $request->get('page', 1);
        $per_page   = (int) $request->get('per_page', 5);

        if ($per_page <= 0) return ApiResponse::createValidationResponse(["per_page" => ["per_page must be greater than 0"]]);

        // Page = 1 ||  Top Experts and Local Experts
        if ($authUser) {
            $experts['total_top_experts'] = ExpertsCities::whereRaw("users_id in (select id from users where nationality != $authUser->nationality)")->where('cities_id', $obj->cities_id)->count();
            $experts['total_local_experts'] = ExpertsCities::whereRaw("users_id in (select id from users where nationality = $authUser->nationality)")->where('cities_id', $obj->cities_id)->count();
            $experts['top_experts'] = modifyPagination(ExpertsCities::whereRaw("users_id in (select id from users where nationality != $authUser->nationality)")->where('cities_id', $obj->cities_id)->with('user')->orderBy('id', 'desc')->paginate($per_page))->data;
            $experts['local_experts'] = modifyPagination(ExpertsCities::whereRaw("users_id in (select id from users where nationality = $authUser->nationality)")->where('cities_id', $obj->cities_id)->with('user')->orderBy('id', 'desc')->paginate($per_page))->data;
        } else {
            $experts['total_top_experts'] = ExpertsCities::where('cities_id', $obj->cities_id)->count();
            $experts['total_local_experts'] = 0;
            $experts['top_experts'] = modifyPagination(ExpertsCities::where('cities_id', $obj->cities_id)->with('user')->orderBy('id', 'desc')->paginate($per_page))->data;
            $experts['local_experts'] = [];
        }

        foreach ([$experts['top_experts'], $experts['local_experts']] as &$expertBatch) {
            $expertBatch = collect($expertBatch)->map(function ($expertUser) use ($authUser) {
                $expertUser->follow_flag = ($authUser) ? UsersFollowers::where('users_id', $expertUser->users_id)->where('followers_id', $authUser->id)->exists() : false;
                $expertUser->user->profile_picture = check_profile_picture($expertUser->user->profile_picture);
            });
        }

        $bigCount = max([$experts['total_top_experts'], $experts['total_local_experts']]);

        $data['per_page'] = $per_page;
        $data['current_page'] = $page;
        $data['total_pages'] = ceil($bigCount / $per_page);
        $data['data'] = $experts;

        return ApiResponse::create($data);
    }

    /**
     * @OA\Get(
     ** path="/place/{place_id}/trips",
     *   tags={"Places"},
     *   summary="Get Place Trips In Pagination [bearer_token is optional]",
     *   operationId="App\Http\Controllers\Api\Traits\PlaceLocationTrait@getTrips",
     *    @OA\Parameter(
     *        name="place_id",
     *        description="Place Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="page",
     *        description="pagination page number",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="per_page",
     *        description="per page | default = 5",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function getTrips(Request $request, $id)
    {
        $authUser = auth()->user();
        $obj = Place::where('id', $id)->where('active', 1)->first();
        if (!$obj) return ApiResponse::createValidationResponse(["id" => ["place not found"]]);

        $per_page   = (int) $request->get('per_page', 5);
        if ($per_page <= 0) return ApiResponse::createValidationResponse(["per_page" => ["per_page must be greater than 10"]]);

        $trips =  TripPlaces::query()
            ->whereRaw('versions_id = (SELECT trips.active_version FROM trips WHERE trips.id = trips_places.trips_id)')
            ->whereHas('trip', function ($q) use ($authUser) {
                return $q->where('users_id', ($authUser) ? $authUser->id : -1);
            })
            ->with('trip')
            ->where('places_id', $obj->id)
            ->orderBy('trips_id', 'DESC')->groupBy('trips_id')->paginate($per_page);

        $trips = modifyPagination($trips);

        $trips->data = $trips->data->map(function ($tp) {
            return [
                'id' => $tp->trips_id,
                'title' => $tp->trip->title,
            ];
        })->toArray();

        return ApiResponse::create($trips);
    }


    /**
     * @OA\GET(
     ** path="/place/{place_id}/reviews",
     *   tags={"Primary Post [ post, trip, event, report, review, discussion ]"},
     *   summary="This Api used to fetch more reviews [bearer_token is optional]",
     *   operationId="Api\PrimaryPostController@loadReview",
     *  @OA\Parameter(
     *        name="place_id",
     *        description="place_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="page",
     *        description="page",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * * @OA\Parameter(
     *        name="filter",
     *        description="1=new, 2=top, 3=worse | default = 1",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function loadReview(Request $request, $place_id)
    {
        try {
            $place = Place::find($place_id);
            if (!$place) return ApiResponse::__createBadResponse('Invalid Place ID.');

            $authUser   = auth()->user();
            $perPage    = $request->per_page ?? 10;
            $filter     = $request->filter   ?? 1;

            if (!in_array($filter, [1, 2, 3])) {
                return ApiResponse::__createBadResponse('Invalid filter.');
            }

            $reviews    = Reviews::with('author')->whereHas('author')
                ->where('places_id', $place->id)
                ->when(in_array($filter, [1, 2, 3]), function ($q) use ($filter) {
                    if ($filter == 2) {
                        $q->selectRaw('*, ((select count(*) from `reviews_votes` where `vote_type` = 1 and  `review_id`=`reviews`.`id`)) as top')->orderBy('top', 'ASC');
                    } else  if ($filter == 3) {
                        return $q->orderBy('score', 'ASC');
                    }
                    return $q->orderBy('id', 'DESC');
                })
                ->paginate($perPage);

            $reviews = modifyPagination($reviews);
            $reviews->data->map(function (&$review) use ($authUser) {
                $review->author->profile_picture = check_profile_picture($review->author->profile_picture);

                $review->total_shares = PostsShares::where('type', ShareService::TYPE_REVIEW)->where('posts_type_id', $review->id)->count();
                $review->like_flag =  $authUser ? ($review->updownvotes->where('users_id', $authUser->id)->where('vote_type', 1)->first() ? true : false) : false;
                $review->total_like = $review->updownvotes->where('vote_type', 1)->count();

                return $review;
            });

            return ApiResponse::create($reviews);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
