<?php

namespace App\Http\Controllers\Api\Traits;

use App\Http\Responses\ApiResponse;
use App\Models\Country\Countries;
use App\Models\Country\CountriesFollowers;
use App\Models\Country\CountriesMedias;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\Posts\Checkins;
use App\Models\Posts\PostsShares;
use App\Models\Reports\Reports;
use App\Models\TripPlaces\TripPlaces;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Api\ShareService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Events\Events;

trait CountryLocationTrait
{
    /**
     * @OA\Get(
     ** path="/country/{id}",
     *   tags={"Country"},
     *   summary="Fetch country [bearer_token is optional]",
     *   operationId="app\Http\Controllers\Api\Traits\CountryLocationTrait@getIndex",
     *    @OA\Parameter(
     *        name="id",
     *        description="Country Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function getIndex(Request $request, $id)
    {
        try {
            $authUser = auth()->user();
            $language_id = 1;

            $obj = Countries::with([
                'trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'region.transsingle',
                'capitals',
                'countryHolidays', 'languages', 'additional_languages', 'religions', 'emergency', 'currencies'
            ])->where('id', $id)->where('active', 1)->first();

            if (!$obj) return ApiResponse::__createBadResponse("country not found");


            // basic details & flags & counts
            $obj->image_url = url(PLACE_PLACEHOLDERS);
            $firstCityMedia = CountriesMedias::whereHas('media')->where('countries_id', $obj->id)->first();
            if ($firstCityMedia) {
                $obj->image_url = check_city_photo($firstCityMedia->media->url);
            }
            $obj->title = @$obj->trans[0]->title;
            $obj->description = strip_tags(@$obj->trans[0]->description ?? '');

            $obj->total_follower = CountriesFollowers::where('countries_id', $obj->id)->count();
            $obj->follow_status = $authUser ? ((bool) CountriesFollowers::where('countries_id', $obj->id)->where('users_id', $authUser->id)->exists()) : false;
            $obj->i_was_here_flag = $authUser ? (Checkins::where('country_id', $obj->id)->where('users_id', $authUser->id)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->count() > 0) : false;
            $obj->total_share = PostsShares::where('type', ShareService::TYPE_COUNTRY)->count();
            $obj->total_checkins = Checkins::where('country_id', $obj->id)->whereHas('user')->where('users_id', '>', 0)->count();
            $obj->past_24h_total_checkins = Checkins::where('country_id', $obj->id)->whereHas('user')->where('users_id', '>', 0)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->count();
            $obj->past_24h_top_checkins = Checkins::where('country_id', $obj->id)->whereHas('user')->with('user')->where('users_id', '>', 0)->whereDate('checkin_time', '<', Carbon::today()->toDateString())->orderBy('id', 'DESC')->limit(3)->get()->map(function ($check_in) {
                return [
                    'id' => $check_in->user->id,
                    'username' => $check_in->user->username,
                    'name' => $check_in->user->name,
                    'profile_picture' => check_profile_picture($check_in->user->profile_picture)
                ];
            });
            $region =  @$obj->region;
            unset($obj->region);
            $obj->region = @$region->transsingle->title ?? "";

            // ABOUT SECTION
            {
                $about = [];
                $about['phone_code'] = $obj->code;
                $about['capital'] = @$obj->capitals[0]->city->trans[0]->title ?? "";
                $about['iso_code'] = $obj->iso_code;
                $about['lat'] = $obj->lat;
                $about['lng'] = $obj->lng;
                $about['population'] = @$obj->trans[0]->population;
                $about['nationality'] = @$obj->trans[0]->nationality;
                $about['working_days'] = @$obj->trans[0]->working_days;

                $speedLimit = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->speed_limit);
                $speedLimits = [];
                foreach ($speedLimit as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $speedLimits[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['speed_limit'] = $speedLimits;
                preg_match_all("/\(([^\]]*)\)/", @$obj->trans[0]->metrics, $matches);
                $about['metrics'] = isset($matches[1][0]) ? explode(", ", $matches[1][0]) : [];
                $about['internet'] = @$obj->trans[0]->internet;
                $timing = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->best_time);
                $timings = [];
                foreach ($timing as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $timings[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }

                $about['general']['timing'] = $timings;
                $about['general']['transportation'] = explode(", ", @$obj->trans[0]->transportation);
                $about['general']['currencies'] = @$obj->currencies[0]->transsingle->title;

                // $about['medias'] = @$obj->getMedias;

                $socket = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->sockets);
                $sockets = [];
                foreach ($socket as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $sockets[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['sockets'] = $sockets;
                $about['cost_of_living'] = @$obj->trans[0]->cost_of_living;
                $about['crime_rate'] = @$obj->trans[0]->geo_stats;
                $about['quality_of_life'] = @$obj->trans[0]->demographics;


                $economy = preg_split("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", @$obj->trans[0]->economy);
                $economies = [];
                foreach ($economy as $value) {
                    if (count(explode(": ", $value)) >= 2) {
                        $economies[] = [
                            "type" => explode(": ", $value)[0],
                            "value" => explode(": ", $value)[1]
                        ];
                    }
                }
                $about['restrictions'] = $economies;

                $about['emergency_numbers'] = '';
                if (@$obj->emergency) {
                    $emerg = [];
                    foreach (@$obj->emergency as $emergency) {
                        $emerg[] = $emergency->transsingle->title;
                    }
                    $about['emergency_numbers'] = $emerg;
                }

                $about['general']['religions'] = '';
                if (@$obj->religions) {
                    $rel = [];
                    foreach (@$obj->religions as $religions) {
                        $rel[] = $religions->transsingle->title;
                    }
                    $about['general']['religions'] = $rel;
                }

                $about['holidays'] = '';

                if (@$obj->countryHolidays) {
                    $hol = [];
                    foreach (@$obj->countryHolidays as $holidays) {
                        if (isset($holidays->holiday->transsingle) && !empty($holidays->holiday->transsingle)) {
                            $hol[] = $holidays->holiday->transsingle->title;
                        }
                    }
                    $about['holidays'] = $hol;
                }


                $about['general']['languages_spoken'] = '';
                $lng = [];
                if (@$obj->languages) {
                    foreach (@$obj->languages as $languages) {
                        $lng[] = $languages->transsingle->title;
                    }
                }

                if (@$obj->additional_languages) {
                    foreach (@$obj->additional_languages as $additional_languages) {
                        $lng[] = $additional_languages->transsingle->title;
                    }

                    $about['general']['languages_spoken'] = $lng;
                }
            }
            $obj->about = $about;
            // END ABOUT SECTION

            // Top Experts and Local Experts[Page = 1]
            if ($authUser) {
                //where raw
                $friendRaw = 'users_id in (SELECT `uf`.`users_id` FROM `users_followers` AS `uf` INNER JOIN `users_followers` AS `uf2` ON `uf`.`users_id` = `uf2`.`followers_id` AND `uf2`.`users_id` = `uf`.`followers_id` AND `uf2`.`follow_type` = `uf`.`follow_type` WHERE `uf`.`users_id` != `uf`.`followers_id` AND `uf`.`followers_id` =' . $authUser->id . ' AND `uf`.`follow_type` = 1)';
                $topExpertsRaw = "users_id in (select id from users where nationality != $authUser->nationality)";
                $localExpertsRaw = "users_id in (select id from users where nationality = $authUser->nationality)";

                $experts['total_top_experts'] = ExpertsCountries::whereHas('user')->whereRaw($topExpertsRaw)->where('countries_id', $obj->id)->count();
                $experts['total_local_experts'] = ExpertsCountries::whereHas('user')->whereRaw($localExpertsRaw)->where('countries_id', $obj->id)->count();
                $experts['total_friends_experts'] = ExpertsCountries::whereHas('user')->whereRaw($friendRaw)->where('countries_id', $obj->id)->count();
                $experts['top_experts'] = ExpertsCountries::whereHas('user')->whereRaw($topExpertsRaw)->where('countries_id', $obj->id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
                $experts['local_experts'] = ExpertsCountries::whereHas('user')->whereRaw($localExpertsRaw)->where('countries_id', $obj->id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
                $experts['friends_experts'] = ExpertsCountries::whereHas('user')->whereRaw($friendRaw)->where('countries_id', $obj->id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
            } else {
                $experts['total_top_experts'] = ExpertsCountries::whereHas('user')->where('countries_id', $obj->id)->count();
                $experts['total_local_experts'] = 0;
                $experts['total_friends_experts'] = 0;
                $experts['top_experts'] = ExpertsCountries::whereHas('user')->where('countries_id', $obj->id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
                $experts['local_experts'] = [];
                $experts['friends_experts'] = [];
            }
            $experts['live_checkin'] = Checkins::select('id', 'users_id', DB::raw('country_id as countries_id'))->whereHas('user')->where('country_id',  $obj->id)->with('user')->orderBy('id', 'desc')->orderBy('id', 'desc')->limit(10)->get();
            $experts['total_live_checkin'] = Checkins::whereHas('user')->where('country_id', $obj->id)->count();

            foreach ([$experts['top_experts'], $experts['local_experts'], $experts['friends_experts'], $experts['live_checkin']] as &$expertBatch) {
                $expertBatch = collect($expertBatch)->map(function ($expertUser) use ($authUser) {
                    $expertUser->follow_flag = ($authUser) ? UsersFollowers::where('users_id', $expertUser->users_id)->where('followers_id', $authUser->id)->exists() : false;
                    $expertUser->user->profile_picture = check_profile_picture($expertUser->user->profile_picture);
                });
            }

            $obj->experts = $experts;
            // End Experts and Local Experts

            $_trips_query = TripPlaces::query()
                ->whereRaw('versions_id = (SELECT trips.active_version FROM trips WHERE trips.id = trips_places.trips_id)')
                ->whereHas('trip', function ($q) use ($authUser) {
                    return $q->where('users_id', ($authUser) ? $authUser->id : -1);
                })
                ->where('countries_id', $obj->id)
                ->groupBy('trips_id');

            $_trips = (clone $_trips_query)->with('trip')->orderBy('trips_id', 'DESC')->limit(5)->get();

            // Trips [Page = 1]
            $obj->trips = collect($_trips)->map(function ($tp) {
                return [
                    'id' => $tp->trips_id,
                    'title' => $tp->trip->title,
                ];
            })->toArray();
            // End Trips [Page = 1]


            // Images Box
            $_trip = (clone $_trips_query)->with('trip')->orderBy('trips_id', 'DESC')->first();
            $boxs[] = [
                "title" => "Epic Trip Plans",
                "type" => "trip-plans",
                "img" => ($_trip) ? check_cover_photo($_trip->trip->cover) : url(PATTERN_PLACEHOLDERS),
                "count" => DB::table(DB::raw('(' . _getDBQuery($_trips_query) . ') as a'))->count()
            ];

            $_trending_media = CountriesMedias::with('media', 'media.author')->whereHas('media', function ($q) {
                return $q->whereHas('author')->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4');
            })->where('countries_id', $obj->id)->orderBy('id', 'DESC')->first();
            $boxs[] = [
                "title" => "Trending Media",
                "type" => "medias",
                "img" => ($_trending_media) ? check_media_url($_trending_media->media->url) : url(PATTERN_PLACEHOLDERS),
                "count" => CountriesMedias::whereHas('media', function ($q) {
                    return $q->whereHas('author')->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4');
                })->where('countries_id', $obj->id)->count()
            ];

            $totalExperts =  ExpertsCountries::whereHas('user')->where('countries_id', $obj->id)->count();
            $boxs[] = [
                "title" => "Experts",
                "type" => "experts",
                "experts_img" => $totalExperts ?  ExpertsCountries::whereHas('user')->where('countries_id', $obj->id)->with('user')->orderBy('id', 'desc')->take(6)->get()->map(function ($expertUser) use ($authUser) {
                    return $expertUser->user->profile_picture = check_profile_picture($expertUser->user->profile_picture);
                })->toArray() : [url(MALE_PLACEHOLDERS)],
                "count" => $totalExperts
            ];

            $_report  = Reports::whereHas('cover')->where('countries_id', '=', @$obj->id)->where('flag', '=', 1)->first();
            $boxs[] = [
                "title" => "Reports",
                "type" => "reports",
                "img" =>  isset($_report->cover[0]->val) ? $_report->cover[0]->val : url(PATTERN_PLACEHOLDERS),
                "count" => Reports::where('countries_id', '=', $obj->id)->where('flag', '=', 1)->count()
            ];

            $rand = rand(1, 10);
            $category = array('community', 'concerts', 'conferences', 'expos', 'festivals', 'performing-arts', 'politics', 'school-holidays', 'sports');

            $boxs[] = [
                "title" => "Events",
                "type" => "events",
                "img" =>  url('assets2/image/events/' . $category[$rand % 9] . '/' . $rand . '.jpg'),
                "count" => Events::query()->where('countries_id', $obj->id)->count()
            ];


            // $obj->image_cards = $imageBox;
            $obj->image_cards = $boxs;
            // End Images Box

            unset(
                $obj->trans,
                $obj->countryHolidays,
                $obj->religions,
                $obj->emergency,
                $obj->currencies,
                $obj->languages,
                $obj->additional_languages,
                $obj->capitals
            );

            return ApiResponse::create($obj);

            // old code // old code // old code // old code // old code

            // $language_id = 1;
            // $country = Countries::find($country_id);

            // $random_val = $this->_getRandom(date('j'));

            // if (!$country) {
            //     return [
            //         'data' => [
            //             'error' => 400,
            //             'message' => 'Invalid Country ID',
            //         ],
            //         'success' => false
            //     ];
            // }

            // $data['search_in'] = $country;

            // $language = Languages::where('id', $language_id)->first();

            // if (!$language) {
            //     return [
            //         'data' => [
            //             'error' => 400,
            //             'message' => 'Invalid Language ID',
            //         ],
            //         'success' => false
            //     ];
            // }

            // $country = Countries::with([
            //     'trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'region.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'languages.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'holidays.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'religions.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'currencies.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'capitals.city.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'emergency.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'atms' => function ($query) {
            //         $query->where('place_type', 'like', '%atm%');
            //     },
            //     'hotels',
            //     'cities',
            //     'cities.getMedias.trans' => function ($query) use ($language_id) {
            //         $query->where('languages_id', $language_id);
            //     },
            //     'getMedias',
            //     'getMedias.users',
            //     'timezone',
            //     'followers'
            // ])
            //     ->where('id', $country_id)
            //     ->where('active', 1)
            //     ->first();

            // if (!$country) {
            //     return [
            //         'data' => [
            //             'error' => 404,
            //             'message' => 'Not found',
            //         ],
            //         'success' => false
            //     ];
            // }

            // // exit('ok');
            // $db_place_reviews = Reviews::where('countries_id', $country->id)->orderBy('created_at', 'desc')->get();
            // $data['reviews'] = $db_place_reviews;
            // $data['reviews_places'] = Reviews::where('countries_id', $country->id)->get();

            // $data['reviews_avg'] = Reviews::where('countries_id', $country->id)->whereNull('google_author_url')->avg('score');

            // $data['place'] = $country;
            // $cites = [];
            // foreach ($country->cities as $city) {
            //     $cityTrans = CitiesTranslations::where('cities_id', $city->id)->first();
            //     if (is_object($cityTrans))
            //         $cites[$city->id] = $cityTrans->title;
            // }
            // $data['countries_cities'] = $cites;
            // if (Auth::check()) {
            //     // $list_trip_plan = TripPlaces::where('countries_id', $country->id)->pluck('trips_id');
            //     $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)
            //         //    ->whereNotIn('id', $list_trip_plan)
            //         ->get();
            //     $data['me'] = Auth::user();
            // }


            // $data['travelmates'] = TravelMatesRequests::whereHas('plan_country', function ($q) use ($country) {
            //     $q->where('countries_id', '=', $country->id);
            // })
            //     ->groupBy('users_id')
            //     ->get();

            // $data['reportsinfo'] = [];
            // $data['reports'] = Reports::where('countries_id', '=', $country->id)->where('flag', '=', 1)
            //     ->get();

            // $data['pposts'] = '';

            // $data['place_discussions'] = Discussion::where('destination_type', 'Country')
            //     ->where('destination_id', $country->id)
            //     ->get();

            // // get experts into categories
            // $experts_all = $experts_top = $experts_local = $experts_friends = $live_checkins = array();
            // foreach ($country->experts as $key => $pce) {
            //     $experts_all[] = $pce;
            //     if (is_object($pce->user) && isset($data['me']) && is_object($data['me'])) {
            //         if ($pce->user->nationality != $data['me']->nationality) {
            //             $experts_top[] = $pce;
            //         } elseif ($pce->user->nationality == $data['me']->nationality) {
            //             $experts_local[] = $pce;
            //         }
            //     }
            //     if (is_object($pce->user)) {
            //         if (is_friend($pce->user->id)) {
            //             $experts_friends[] = $pce;
            //         }
            //     }
            // }

            // $data['experts_top'] = $experts_top;
            // $data['experts_local'] = $experts_local;
            // $data['experts_friends'] = $experts_friends;
            // $data['live_checkins'] = $country->checkins()->groupBy('users_id')->whereDate('checkin_time', '=', Carbon::today()->toDateString())->get();

            // $experts_ids = array();
            // foreach ($experts_all as $ea) {
            //     $experts_ids[] = $ea->users_id;
            // }


            // // Select *,count(*) as r, sum(A.replies) from 
            // // (SELECT created_at, users_id, count(*) as replies FROM `discussion_replies` GROUP BY YEAR(created_at), Month(created_at), Day(created_at)) 
            // // as A GROUP BY A.users_id ORDER BY `users_id`
            // $data['place_top_contributors'] = [];
            // if (count($experts_ids) > 0) {
            //     $last_month = date("Y-m-d", strtotime('-180 days'));

            //     $sub = DiscussionReplies::selectRaw('*, count(*) as reps, sum(num_views) as views')
            //         ->whereRaw('users_id in (' . implode(',', $experts_ids) . ')')
            //         ->whereRaw('created_at >' . $last_month)
            //         ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'));

            //     $data['place_top_contributors'] = DiscussionReplies::from(DB::raw('(' . $sub->toSql() . ') as a'))
            //         ->selectRaw('*, count(*) as count, sum(a.reps) as reply_cnt, sum(a.views) as views')
            //         ->groupBy('a.users_id')
            //         ->orderBy('count', 'desc')
            //         ->take(10)
            //         ->get();
            // }

            // $data['top_places'] = PlacesTop::where('country_id', $country->id)
            //     ->orderByRaw('FIELD(id % 32, ' . $random_val . ')')
            //     ->take(4)
            //     ->get();

            // $languages = $country->getLanguages();
            // $country->setRelation('languages', collect($languages));

            // $data['trip_plans_collection'] = TripPlaces::where('countries_id', $country->id)
            //     ->orderBy('id', 'DESC')
            //     ->get();

            // $live_users = [];
            // foreach ($data['live_checkins'] as $live_here) {
            //     $live_users[] = $live_here->users_id;
            // }
            // $temp_live_users =  User::whereIn('id', $live_users)->get();
            // $live_users = [];
            // foreach ($temp_live_users as $user) {
            //     $is_followed =  app('App\Http\Controllers\ProfileController')->postCheckFollow($user->id);
            //     $live_users[$user->id] = ['users' => $user, 'is_followed' => $is_followed['success']];
            // }
            // $data['live_users'] = $live_users;

            // $placeCount = Place::where('countries_id', '=', $country->id)
            //     ->count();





            // $citiesMediasSql = DB::table('cities_medias')
            //     ->select([
            //         'cities_id',
            //         DB::raw('max(medias_id) AS medias_id')
            //     ])
            //     ->groupBy('cities_id')
            //     ->toSql();

            // $tripPlans = Country::select(
            //     'medias.id AS media_id',
            //     'medias.url',
            //     'trips_cities.cities_id',
            //     'trips.id',
            //     'trips.title',
            //     'trips.users_id',
            //     'trips.created_at',
            //     'trips_places.budget',
            //     'trips_places.id'
            // )
            //     ->join('places', 'countries.id', '=', 'places.countries_id')
            //     ->join('trips_places', function ($join) {
            //         $join->on('trips_places.countries_id', '=', 'countries.id')
            //             ->on('trips_places.places_id', '=', 'places.id');
            //     })
            //     ->join('trips', 'trips.id', '=', 'trips_places.trips_id')
            //     ->join('trips_cities', 'trips_cities.trips_id', '=', 'trips.id')
            //     ->leftJoin(DB::raw('(' . $citiesMediasSql . ') AS cm'), 'cm.cities_id', '=', 'trips_cities.cities_id')
            //     ->leftJoin('medias', 'medias.id', '=', 'cm.medias_id')
            //     ->where('countries.id', $country_id)
            //     ->get();

            // $sumBudget = 0;
            // foreach ($tripPlans as $tripPlan) {
            //     $sumBudget += $tripPlan['budget'];
            //     $media = [
            //         'id' => $tripPlan['media_id'],
            //         'url' => $tripPlan['url']
            //     ];
            //     unset($tripPlan['media_id'], $tripPlan['url']);
            //     $tripPlan['medias'] = (object) $media;
            // }

            // $airports = Place::where('countries_id', '=', $country->id)
            //     ->where('place_type', 'like', 'airport,%')
            //     ->limit(10)
            //     ->get();


            // $data['plans'] = $tripPlans;
            // $data['plans_count'] = count($tripPlans);
            // $data['sum_budget'] = $sumBudget;

            // $data['country'] = $country;
            // $data['airports'] = $airports;
            // if (Auth::check()) {
            //     $data['user'] = Auth::user();
            //     $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->get();
            // }
            // $client = new Client();
            // //$client->getHttpClient()->setDefaultOption('verify', false);

            // $result = $client->post('https://auth.predicthq.com/token', [
            //     'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
            //     'form_params' => [
            //         'grant_type' => 'client_credentials',
            //         'scope' => 'account events signals'
            //     ],
            //     'verify' => false
            // ]);
            // $events_array = false;
            // if ($result->getStatusCode() == 200) {
            //     $json_response = $result->getBody()->read(1024);
            //     $response = json_decode($json_response);
            //     $access_token = $response->access_token;

            //     $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $country->lat . ',' . $country->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
            //         'headers' => ['Authorization' => 'Bearer ' . $access_token],
            //         'verify' => false
            //     ]);
            //     if ($get_events1->getStatusCode() == 200) {
            //         $events1 = json_decode($get_events1->getBody()->read(100024));
            //         //dd($events);
            //         $events_array1 = $events1->results;
            //         $events_final = $events_array1;
            //         if ($events1->next) {
            //             $get_events2 = $client->get($events1->next, [
            //                 'headers' => ['Authorization' => 'Bearer ' . $access_token],
            //                 'verify' => false
            //             ]);
            //             if ($get_events2->getStatusCode() == 200) {
            //                 $events2 = json_decode($get_events2->getBody()->read(100024));
            //                 //dd($events);
            //                 $events_array2 = $events2->results;
            //                 $events_final = array_merge($events_array1, $events_array2);
            //             }
            //         }
            //         $data['events'] = $events_final;
            //     }
            // }


            // $checkins = Checkins::where('country_id', $country->id)->orderBy('id', 'DESC')->get();

            // $result = array();
            // $done = array();
            // foreach ($checkins as $checkin) {
            //     if (!isset($done[$checkin->post_checkin->post->author->id])) {
            //         $result[] = $checkin;
            //         $done[$checkin->post_checkin->post->author->id] = true;
            //     }
            // }
            // $data['checkins'] = $result;


            // $data['travelmates'] = TravelMatesRequests::whereHas('plan_country', function ($q) use ($country) {
            //     $q->where('countries_id', '=', $country->id);
            // })
            //     ->get();

            // $data['reports'] = Reports::where('countries_id', '=', $country->id)
            //     ->get();

            // // return ApiResponse::create($data);

            // // $data['me'] = Auth::guard('user')->user();


            // // $data['pposts'] = collect_posts('country', $country->id);

            // $data['country_discussions'] = Discussion::where('destination_type', 'Country')
            //     ->where('destination_id', $country->id)
            //     ->get();



            // // get experts into categories
            // $experts_top = $experts_local = $experts_friends = $live_checkins = array();
            // foreach ($country->experts as $key => $pce) {
            //     if (isset($data['me']))
            //         if ($pce->user->nationality != $data['me']->nationality) {
            //             $experts_top[] = $pce;
            //         } elseif ($pce->user->nationality == $data['me']->nationality) {
            //             $experts_local[] = $pce;
            //         }
            //     if (is_friend($pce->user->id)) {
            //         $experts_friends[] = $pce;
            //     }
            // }
            // $data['experts_top']        =       $experts_top;
            // $data['experts_local']      =       $experts_local;
            // $data['experts_friends']    =       $experts_friends;
            // $data['live_checkins']      =       $country->checkins()->groupBy('users_id')->get();


            // $checkins = Checkins::whereHas("country", function ($query) use ($country) {
            //     $query->where('country_id', $country->id);
            // })
            //     ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $country->lat . ') ) * cos( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) * cos( radians( SUBSTRING(lat_lng, INSTR(lat_lng, \',\') + 1) ) - radians(' . $country->lng . ') ) + sin( radians(' . $country->lat . ') ) * sin( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) ) ) AS distance'))
            //     // ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
            //     ->having('distance', '<', 15)
            //     ->orderBy('distance')
            //     ->take(10)
            //     ->get();

            // $result = array();
            // foreach ($checkins as $checkin) {
            //     $result[] = array(
            //         // 'post_id' => $checkin->post_checkin->post->id,
            //         'lat' => $checkin->country->lat,
            //         'lng' => $checkin->country->lng,
            //         // 'photo' => @$checkin->post_checkin->post->medias[0]->media->url,
            //         'name' => @$checkin->user->name,
            //         'id' => @$checkin->user->id,
            //         'title' => $checkin->location,
            //         'profile_picture' => check_profile_picture(@$checkin->user->profile_picture),
            //         'date' => diffForHumans($checkin->created_at)
            //     );
            // }
            // $data['places_nearby'] = $result;





            // return ApiResponse::create($data);
        } catch (\Throwable $th) {
            return ApiResponse::createServerError($th);
        }
    }

    /**
     * @OA\Get(
     ** path="/country/{id}/experts",
     *   tags={"Country"},
     *   summary="Get Country Expert List In Pagination [bearer_token is optional]",
     *   operationId="App\Http\Controllers\Api\Traits\CountryLocationTrait@getExpertsList",
     *   @OA\Parameter(
     *        name="country_id",
     *        description="Country Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="page",
     *        description="pagination page number",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="per_page",
     *        description="per page | default = 5",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function getExpertsList(Request $request, $id)
    {
        $obj = Countries::where('id', $id)->where('active', 1)->first();
        if (!$obj) return ApiResponse::createValidationResponse(["id" => ["country not found"]]);

        $authUser   = auth()->user();
        $page       = (int) $request->get('page', 1);
        $per_page   = (int) $request->get('per_page', 5);

        if ($per_page <= 0) return ApiResponse::createValidationResponse(["per_page" => ["per_page must be greater than 0"]]);

        // Page = 1 ||  Top Experts and Local Experts
        if ($authUser) {
            $experts['total_top_experts'] = ExpertsCountries::whereRaw("users_id in (select id from users where nationality != $authUser->nationality)")->where('countries_id', $obj->id)->count();
            $experts['total_local_experts'] = ExpertsCountries::whereRaw("users_id in (select id from users where nationality = $authUser->nationality)")->where('countries_id', $obj->id)->count();
            $experts['top_experts'] = modifyPagination(ExpertsCountries::whereRaw("users_id in (select id from users where nationality != $authUser->nationality)")->where('countries_id', $obj->id)->with('user')->orderBy('id', 'desc')->paginate($per_page))->data;
            $experts['local_experts'] = modifyPagination(ExpertsCountries::whereRaw("users_id in (select id from users where nationality = $authUser->nationality)")->where('countries_id', $obj->id)->with('user')->orderBy('id', 'desc')->paginate($per_page))->data;
        } else {
            $experts['total_top_experts'] = ExpertsCountries::where('countries_id', $obj->id)->count();
            $experts['total_local_experts'] = 0;
            $experts['top_experts'] = modifyPagination(ExpertsCountries::where('countries_id', $obj->id)->with('user')->orderBy('id', 'desc')->paginate($per_page))->data;
            $experts['local_experts'] = [];
        }

        foreach ([$experts['top_experts'], $experts['local_experts']] as &$expertBatch) {
            $expertBatch = collect($expertBatch)->map(function ($expertUser) use ($authUser) {
                $expertUser->follow_flag = ($authUser) ? UsersFollowers::where('users_id', $expertUser->users_id)->where('followers_id', $authUser->id)->exists() : false;
                $expertUser->user->profile_picture = check_profile_picture($expertUser->user->profile_picture);
            });
        }

        $bigCount = max([$experts['total_top_experts'], $experts['total_local_experts']]);

        $data['per_page'] = $per_page;
        $data['current_page'] = $page;
        $data['total_pages'] = ceil($bigCount / $per_page);
        $data['data'] = $experts;

        return ApiResponse::create($data);
    }

    /**
     * @OA\Get(
     ** path="/country/{id}/trips",
     *   tags={"Country"},
     *   summary="Get Country Trips In Pagination [bearer_token is optional]",
     *   operationId="App\Http\Controllers\Api\Traits\CountryLocationTrait@getTrips",
     *    @OA\Parameter(
     *        name="country_id",
     *        description="Country Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="page",
     *        description="pagination page number",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="per_page",
     *        description="per page | default = 5",
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function getTrips(Request $request, $id)
    {
        $authUser = auth()->user();
        $obj = Countries::where('id', $id)->where('active', 1)->first();
        if (!$obj) return ApiResponse::createValidationResponse(["id" => ["country not found"]]);

        $per_page   = (int) $request->get('per_page', 5);
        if ($per_page <= 0) return ApiResponse::createValidationResponse(["per_page" => ["per_page must be greater than 10"]]);

        $trips =  TripPlaces::query()
            ->whereRaw('versions_id = (SELECT trips.active_version FROM trips WHERE trips.id = trips_places.trips_id)')
            ->whereHas('trip', function ($q) use ($authUser) {
                return $q->where('users_id', ($authUser) ? $authUser->id : -1);
            })
            ->with('trip')
            ->where('countries_id', $obj->id)
            ->orderBy('trips_id', 'DESC')->groupBy('trips_id')->paginate($per_page);

        $trips = modifyPagination($trips);

        $trips->data = $trips->data->map(function ($tp) {
            return [
                'id' => $tp->trips_id,
                'title' => $tp->trip->title,
            ];
        })->toArray();

        return ApiResponse::create($trips);
    }
}
