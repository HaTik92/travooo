<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsVersions;
use App\Services\PlaceChat\PlaceChatsService;
use App\Models\City\Cities;
use App\Models\Notifications\Notifications;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripCountries\TripCountries;
use Illuminate\Support\Facades\Storage;
use App\Models\Place\Place;
use App\Models\Posts\PostsPlaces;
use App\Models\TripPlans\PlanActivityLog;
use App\Models\TripPlans\TripsContributionCommits;
use App\Models\TripPlans\TripsContributionRequests;
use App\Services\Trips\PlanActivityLogService;
use App\Services\Trips\TripsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;


class AutoGenerateController extends Controller
{
    public function tripGenerate(
        PlaceChatsService $placeChatsService,
        TripsSuggestionsService $tripsSuggestionsService,
        TripsService $tripsService,
        PlanActivityLogService $planActivityLogService,
        $newsfeed_type = "random"
    ) {
        try {
            $faker = \Faker\Factory::create();

            $title          = $faker->words(rand(3, 5), true);
            $description    = "Auto generated trip description " . date('d-m-Y h:i:s A');
            $privacy        = 0; // we need public
            $memory         = rand(0, 1);
            $user_id        = auth()->user()->id;
            $date           = date('Y-m-d h:i:s', time());

            $newsfeed_type = trim(strtolower($newsfeed_type));
            $noOfTp = rand(20, 35);
            $tpLocations = [];
            $basedLoc = $this->getPlaceAndCity();

            switch ($newsfeed_type) {
                case "single-city":
                    $tpLocations[] = $basedLoc;
                    for ($i = 0; $i < $noOfTp; $i++) {
                        $tpLocations[] = $this->getPlaceAndCity($newsfeed_type, $basedLoc);
                    }
                    break;
                case "multiple-city":
                    $tpLocations = $this->getPlaceAndCity($newsfeed_type, $basedLoc);
                    break;
                case "seed":
                    $tpLocations = $this->getPlaceAndCity($newsfeed_type);
                    break;
                default:
                    $tpLocations[] = $basedLoc;
                    for ($i = 0; $i < $noOfTp; $i++) {
                        $tpLocations[] = $this->getPlaceAndCity();
                    }
                    break;
            }

            // dd($tpLocations);
            $devUsers = [
                1969 //brijesh
            ];
            $localUsers = [
                1594 //brijesh
            ];
            $prodUsers = [
                1942, // adnan
                26, // shazaib
                2387 // brijesh
            ];

            if (!count($tpLocations) > 5) {
                return ApiResponse::create(
                    [
                        'message' => ['location not found']
                    ],
                    false,
                    ApiResponse::SERVER_ERROR
                );
            }



            $finalDate = date('Y-m-d', time());

            $new_plan                   = new TripPlans;
            $new_plan->users_id         = $user_id;
            $new_plan->title            = $title;
            $new_plan->memory           = $memory;
            $new_plan->description      = $description;
            $new_plan->privacy          = $privacy;
            $new_plan->created_at       = date('Y-m-d h:i:s a', time());
            $new_plan->active           = 0;
            $new_plan->save();

            try {
                $file = $this->getRandomMedia();
                $fileName = time() . $file['file'];
                Storage::disk('s3')->put('trips/cover/' . $new_plan->id . '/' . $fileName, file_get_contents($file['path']), 'public');
                $coverUrl = 'https://s3.amazonaws.com/travooo-images2/trips/cover/' .  $new_plan->id . '/' . $fileName;
            } catch (\Throwable $e) {
                $coverUrl = null;
            }

            $trip_id = $new_plan->id;

            $version_0 = new TripsVersions;
            $version_0->plans_id = $new_plan->id;
            $version_0->authors_id = $user_id;
            $version_0->start_date = $date;
            $version_0->save();
            $new_plan->active_version = $version_0->id;
            $new_plan->cover = $coverUrl;
            $new_plan->save();
            log_user_activity('Trip', 'create', $trip_id);

            $placeChatsService->createConversation($new_plan->id, PlaceChatsService::CHAT_TYPE_MAIN, auth()->id());

            $i = 0;
            $dateIncre = 1;
            $date = $finalDate;
            $order = 0;
            foreach ($tpLocations as $tpLocation) {
                $i++;

                if (rand(0, 1) && $i != 1) {
                    $dateIncre++;
                    $date = date("Y-m-d", strtotime("+$dateIncre day", strtotime(date('Y-m-d'))));
                }

                $place_id = $tpLocation['place_id'];
                $city_id = $tpLocation['city_id'];
                $countries_id = $tpLocation['countries_id'];


                $budget = rand(20, 100);
                $known_for = '';
                $duration_days = 0;
                $duration_hours = 0;
                $duration_minutes = 0;
                $duration = ($duration_hours * 60) + $duration_minutes;
                $duration = ($duration_days * 60 * 24) + ($duration_hours * 60) + $duration_minutes;
                $time = '';
                $date_time = strtotime($date . " " . $time);
                $date_time = date("Y-m-d H:i:s", $date_time);
                $time = $date_time;

                $story = "";

                if ($new_plan->memory && rand(0, 1)) {
                    $known_for = implode(",", collect(["Time", "History", "Food", "Travel", "Unique", "Nature"])->random(rand(3, 6))->toArray());
                    if (rand(0, 1)) $story = addslashes($faker->paragraph . " " . $faker->paragraph . " " . $faker->paragraph . " " . $faker->paragraph . " " . $faker->paragraph);
                }


                $add_place = new TripPlaces;
                $add_place->trips_id = $trip_id;
                $add_place->cities_id = $city_id;
                $add_place->countries_id = $countries_id;
                $add_place->places_id = $place_id;
                $add_place->time = $time;
                $add_place->date = $date;
                $add_place->order = ++$order;
                $add_place->budget = $budget;
                $add_place->duration = $duration;
                $add_place->comment = $known_for;
                $add_place->active = 0;
                $add_place->versions_id = $new_plan->active_version;
                $add_place->story = $story;
                $add_place->save();

                if ($add_place->save() && $tripsService->recalculateOrders($trip_id, $add_place, $time)) {

                    // Create Suggestion + approve + add ActivityLog  
                    $suggestionId = $tripsSuggestionsService->createSuggestion($add_place->id, $trip_id, TripsSuggestionsService::SUGGESTION_CREATE_TYPE);
                    $tripsSuggestionsService->approveSuggestion($suggestionId);
                    $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_ADDED_PLACE, $trip_id, $add_place->getKey(), auth()->id(), [], $suggestionId);

                    $country = TripCountries::where('trips_id', $trip_id)->where('countries_id', $countries_id)->first();
                    if (!$country) {
                        $add_country = new TripCountries;
                        $add_country->trips_id = $trip_id;
                        $add_country->countries_id = $countries_id;
                        $add_country->versions_id = $new_plan->active_version;
                        $add_country->date = date("Y-m-d H:i:s");
                        $add_country->order = 0;

                        $add_country->save();
                    }

                    if ($budget) {
                        $new_plan->budget = $new_plan->budget() + $budget;
                        $new_plan->save();
                    }
                } else {
                    return ApiResponse::create(
                        [
                            'message' => ['internal server error']
                        ],
                        false,
                        ApiResponse::SERVER_ERROR
                    );
                }
            }

            $startPlaceDate    = $new_plan->trips_places()->orderBy('date')->first()->date;
            $endPlaceDate      = $new_plan->trips_places()->orderBy('date', 'desc')->first()->date;

            if ($startPlaceDate) {
                $new_plan->version->start_date = $startPlaceDate;
            }

            if ($endPlaceDate) {
                $new_plan->version->end_date = $endPlaceDate;
            }
            $new_plan->version->save();
            $version_id =  $new_plan->version->id;

            if ($new_plan->update(['active' => 1])) {

                $tripsContributionRequests = TripsContributionRequests::where('plans_id', $trip_id)->where('users_id', Auth::user()->id)->get()->first();

                if ($tripsContributionRequests) {
                    $commit = new TripsContributionCommits();
                    $commit->plans_id = $trip_id;
                    $commit->requests_id = $tripsContributionRequests->id;
                    $commit->users_id = Auth::user()->id;
                    $commit->versions_id = $version_id;
                    $commit->notes = '';
                    $commit->status = 0;
                    $commit->save();
                }

                $not = new Notifications();
                $not->authors_id = Auth::user()->id;
                $not->users_id = $new_plan->users_id;
                $not->type = 'plan_version_sent';
                $not->data_id = $new_plan->id;
                $not->notes = '';
                $not->read = 0;
                $not->save();

                $tripsSuggestionsService->publishPlan($new_plan);
                $new_plan->save();
                log_user_activity('Trip', 'activate', $trip_id);
            } else {
                return ApiResponse::create(
                    [
                        'message' => ['internal server error in version']
                    ],
                    false,
                    ApiResponse::SERVER_ERROR
                );
            }
            log_user_activity('Trip', 'plan_updated', $trip_id);
            return ApiResponse::create([
                'message'   => ['trip plans create successfully.'],
                'id'        => $trip_id
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    public function getRandomMedia()
    {
        $files = [];
        $dir = public_path('random');
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file !== '.' && $file !== '..') {
                        $files[] = [
                            'path' => $dir . "/" . $file,
                            'file' => $file
                        ];
                    }
                }
            }
            if (count($files))
                return $files[array_rand($files)];
            return null;
        } else {
            return null;
        }
    }

    private function getSomePlaces()
    {
        $allTimeFollowerTrend = collect(DB::select("SELECT places_id FROM places_followers WHERE EXISTS(SELECT * FROM places WHERE id = places_followers.`places_id`) GROUP BY places_id ORDER BY COUNT(users_id) DESC LIMIT 50"))->pluck('places_id');
        $TrendsFollowersOnly = collect(DB::select("SELECT places_id FROM places_followers WHERE created_at >  '" . Carbon::now()->subDays(7) . "' and EXISTS(SELECT * FROM places WHERE id = places_followers.`places_id`) GROUP BY places_id ORDER BY COUNT(users_id) DESC LIMIT 50"))->pluck('places_id');
        $otherFromPost = collect(DB::select("SELECT places_id FROM posts_places GROUP BY places_id ORDER BY COUNT(posts_id) DESC LIMIT 50"))->pluck('places_id');
        $otherFromTrips = collect(DB::select("SELECT places_id FROM trips_places GROUP BY places_id ORDER BY  COUNT(trips_id) DESC LIMIT 50"))->pluck('places_id');
        return $allTimeFollowerTrend->merge($TrendsFollowersOnly)->merge($otherFromPost)->merge($otherFromTrips)->unique();
    }


    public function getPlaceAndCity($newsfeed_type = null, $basedLoc = [])
    {
        if ($newsfeed_type == "seed") {
            $baseCacheKey = "CACHE_" . last(explode("\\", get_class($this)));
            $cahceKey = $baseCacheKey . "_places";

            try { // optimize
                if (Cache::has($cahceKey)) {
                    $otherPlaces = Cache::get($cahceKey);
                } else {
                    $otherPlaces = $this->getSomePlaces();
                    Cache::put($cahceKey, $otherPlaces, 100);
                }
            } catch (\Throwable $th) { // Cache folder permission issue
                $otherPlaces = $this->getSomePlaces();
            }
            $followed_places = auth()->user()->followedPlaces()->pluck('places_id');
            $ids = $followed_places->merge($otherPlaces)->unique();

            $places = Place::query()->whereIn('id', $ids->toArray())
                ->whereNotNull('cities_id')
                ->where('lat', 'not like', '0.00000%')->where('lng', 'not like', '0.00000%')
                ->whereRaw("lat between '-85.0511' and '85.0511'")
                ->whereRaw("lng between'-180' and '+180'")
                ->whereRaw('cities_id in (SELECT cities.id FROM cities WHERE cities.id = places.cities_id and lat not like "0.00000%" and lng not like "0.00000%")')
                ->take(rand(5000, 8000))->get();

            if (count($places) >= 5) {
                $l = [];
                foreach ($places->random(rand(20, 45)) as $place) {
                    if ($place->id && $place->cities_id) {
                        if ($place->cities) {
                            $l[] = collect([
                                'countries_id' => $place->cities->countries_id,
                                'place_id' => $place->id,
                                'city_id' => $place->cities_id,
                            ])->toArray();
                        }
                    }
                }
                return $l;
            }
        } else {
            $shortField = collect(['id', 'place_type', 'cities_id', 'provider_id', 'rating', 'lat', 'lng'])->random();
            $shortOrder = collect(['asc', 'desc'])->random();

            $place = Place::query()
                ->whereNotNull('cities_id')->whereNotIn('cities_id', [0])
                ->where('lat', 'not like', '0.00000%')->where('lng', 'not like', '0.00000%')
                ->whereRaw("lat between '-85.0511' and '85.0511'")
                ->whereRaw("lng between'-180' and '+180'")
                ->whereRaw('cities_id in (SELECT cities.id FROM cities WHERE cities.id = places.cities_id and lat not like "0.00000%" and lng not like "0.00000%")');
            if ($newsfeed_type) {
                if ($newsfeed_type == "single-city") {
                    $place = $place->where('cities_id', $basedLoc['city_id']);
                } else if ($newsfeed_type == "multiple-city") {
                    $place = $place->whereHas('cities', function ($cityQ) use ($basedLoc) {
                        $cityQ->where('countries_id', $basedLoc['countries_id']);
                    });
                }
            }

            if ($newsfeed_type == "multiple-city") {
                $places = $place->take(rand(5000, 8000))->get();

                if (count($places) >= 15) {
                    $l = [];
                    foreach ($places->random(rand(20, 45)) as $place) {
                        if ($place->id && $place->cities_id) {
                            if ($place->cities) {
                                $l[] = collect([
                                    'countries_id' => $place->cities->countries_id,
                                    'place_id' => $place->id,
                                    'city_id' => $place->cities_id,
                                ])->toArray();
                            }
                        }
                    }
                    return $l;
                }
                // fixed loading issue
                $notIn[] = 0;
                $notIn[] = $basedLoc['countries_id'];
                $basedLoc['countries_id'] = Cities::whereNotNull('countries_id')
                    ->whereNotIn('countries_id', $notIn)
                    ->where('lat', 'not like', '0.00000%')
                    ->where('lng', 'not like', '0.00000%')
                    ->whereRaw("lat between '-85.0511' and '85.0511'")
                    ->whereRaw("lng between'-180' and '+180'")
                    ->get()->random()->countries_id;
                return $this->getPlaceAndCity($newsfeed_type, $basedLoc);
            } else {
                $place = $place->orderBy($shortField, $shortOrder)->take(rand(100, 500))->get()->random();

                if ($place) {
                    if ($place->id && $place->cities_id) {
                        if ($place->cities) {
                            return collect([
                                'countries_id' => $place->cities->countries_id,
                                'place_id' => $place->id,
                                'city_id' => $place->cities_id,
                            ])->toArray();
                        }
                    }
                }

                return $this->getPlaceAndCity($newsfeed_type, $basedLoc);
            }
        }
    }
}
