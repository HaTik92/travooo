<?php

namespace App\Http\Controllers\Api;

use \DOMDocument;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Responses\ApiResponse;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsService;
use App\Models\TripPlans\TripsContributionRequests;
use App\Http\Requests\Api\Home\SearchLocationRequest;
use App\Http\Requests\Api\Home\SearchPlacesRequest;
use App\Http\Requests\Api\Home\SearchPOIRequest;
use App\Http\Requests\Api\Home\SuggestTopPlacesRequest;
use App\Http\Requests\Api\Home\SearchHotelsRequest;
use App\Http\Requests\Api\Home\SearchRestaurantsRequest;
use App\Http\Requests\Api\Home\SearchCountriesRequest;
use App\Http\Requests\Api\Home\SearchCitiesRequest;
use App\Http\Requests\Api\Home\SearchUsersRequest;
use App\Http\Requests\Api\Home\SearchTravelstylesRequest;
use App\Http\Requests\Api\Home\URLPreviewRequest;
use App\Http\Requests\Api\Home\SearchPOIsForTaggingRequest;
use App\Http\Requests\Api\Home\NotificationsRequest;
use App\Http\Requests\Api\Home\SearchCitiesPOIRequest;
use App\Http\Requests\Api\Home\trackRefRequest;
use App\Http\Requests\Api\Home\SearchTripsRequest;
use Illuminate\Support\Arr;
use App\Models\ActivityLog\ActivityLog;
use App\Models\PlacesTop\PlacesTop;
use App\Models\User\User;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\TripPlans\TripPlans;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\Place\Place;
use App\Models\Posts\Checkins;
use App\Http\Constants\CommonConst;
use App\Models\Hotels\Hotels;
use App\Models\Hotels\HotelsTranslations;
use App\Models\Restaurants\Restaurants;
use App\Models\Restaurants\RestaurantsTranslations;
use App\Models\Travelstyles\Travelstyles;
use App\Models\Place\PlaceTranslations;
use App\Models\Place\PlaceMedias;
use App\Models\Place\Medias;
use App\Models\Embassies\Embassies;
use App\Models\Notifications\Notifications;
use App\Models\TripPlans\TripsVersions;
use App\Models\Discussion\DiscussionReplies;
use App\Models\TripPlans\TripsSuggestion;
use App\Models\Chat\ChatConversation;
use App\Models\Referral\ReferralLinks;
use App\Models\Referral\ReferralLinksViews;
use App\Models\Referral\ReferralLinksClicks;
use App\Models\Posts\Posts;
use App\Models\Events\Events;
use App\Models\Place\PlaceFollowers;
use App\Models\Reviews\Reviews;
use App\Models\UsersFollowers\UsersFollowers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\SearchController;
use App\Services\Trends\TrendsService;

class HomeController extends Controller
{

    /*******************************************************************************************************************************
     Ohter API :: Ohter API :: Ohter API :: Ohter API :: Ohter API :: Ohter API :: Ohter API  :: Ohter API :: Ohter API :: Ohter API 
     *******************************************************************************************************************************/

    public function showCityEvents()
    {
        $user = User::find(Auth::user()->id);
        $userFavouritesCities = $user->favouritesCities;
        $returnData = $userFavouritesCitiesArr = [];

        if ($userFavouritesCities) {
            foreach ($userFavouritesCities as $item) {
                $userFavouritesCitiesArr[] = $item->id;
            }
            unset($user->favouritesCities);

            foreach ($userFavouritesCitiesArr as $place_id) {
                $events_collection = Events::where('cities_id', $place_id)
                    ->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')
                    ->orderBy('id', 'desc')->take(5)->get();

                if (count($events_collection) > 0) {
                    foreach ($events_collection as $item) {
                        $mediaFile = [];
                        if ($item->getMedias) {
                            foreach ($item->getMedias as $media) {
                                $mediaFile[] = "https://s3.amazonaws.com/travooo-images2/" . $media->url;
                            }
                        }
                        unset($item->getMedias);
                        $item->medias = $mediaFile;
                        $returnData[] = $item;
                    }
                }
            }
        }

        return $returnData;
    }

    public function showPlaceEvents()
    {
        $user = User::find(Auth::user()->id);
        $userFavouritesPlaces = $user->favouritesPlaces;
        $returnData = $userFavouritesPlacesArr  = [];

        if ($userFavouritesPlaces) {
            foreach ($userFavouritesPlaces as $item) {
                $userFavouritesPlacesArr[] = $item->id;
            }
            unset($user->favouritesPlaces);

            foreach ($userFavouritesPlacesArr as $place_id) {
                $events_collection = Events::where('places_id', $place_id)
                    ->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')
                    ->orderBy('id', 'desc')->take(5)->get();

                if (count($events_collection) > 0) {
                    foreach ($events_collection as $item) {
                        $mediaFile = [];
                        if ($item->getMedias) {
                            foreach ($item->getMedias as $media) {
                                $mediaFile[] = "https://s3.amazonaws.com/travooo-images2/" . $media->url;
                            }
                        }
                        unset($item->getMedias);
                        $item->medias = $mediaFile;
                        $returnData[] = $item;
                    }
                }
            }
        }

        return $returnData;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        try {
            $random_val = $this->_getRandom(date('j'));

            //Auth::loginUsingId(19, true);
            if (Auth::check()) {

                //GET https://api.predicthq.com/v1/events/?within=10km@28.36539800,-81.52126420
                //28.36539800,-81.52126420
                //$places = Place::searchByQuery(array('match' => array('title' => 'bank')));
                // get latest activities
                $data['activity_logs'] = ActivityLog::orderBy('id', 'DESC')->take(200)->get();

                $conditions = [
                    ['type' => 'post', 'action' => ['like', 'publish', 'comment']],
                    ['type' => 'travelmate', 'action' => ['join', 'request']],
                    ['type' => 'tripplan', 'action' => ['publish']],
                    ['type' => 'user', 'action' => ['follow']],
                    ['type' => 'country', 'action' => ['follow']],
                    ['type' => 'city', 'action' => ['follow']],
                    ['type' => 'place', 'action' => ['follow', 'review']],
                    ['type' => 'hotel', 'action' => ['follow', 'review']],
                    ['type' => 'status', 'action' => ['publish', 'like']],
                    ['type' => 'media', 'action' => ['like', 'upload', 'unlike']],
                    ['type' => 'checkin', 'action' => ['do']],
                    ['type' => 'discussion', 'action' => ['create', 'reply']],
                    ['type' => 'trip', 'action' => ['active']],
                    ['type' => 'report', 'action' => ['create']]
                ];

                $condition_items = [];
                foreach ($conditions as $condition) {
                    foreach ($condition['action'] as $action) {
                        $condition_items[] = '(lower(type) = "' . $condition['type'] . '" and lower(action) = "' . $action . '")';
                    }
                }
                $where_condition = '';
                if (count($condition_items) > 0) {
                    $where_condition .= '(' . implode(' or ', $condition_items) . ')';
                }

                if ($where_condition != '') {

                    $data['posts'] = ActivityLog::whereHas('user', function ($q) {
                        $q->whereDoesntHave('user_blocks');
                    })->groupBy(array('type', 'action', 'variable'))
                        ->whereRaw($where_condition)
                        ->orderBy('id', 'DESC')
                        ->take(10)
                        ->get();
                } else {

                    $data['posts'] = ActivityLog::groupBy(array('type', 'action', 'variable'))
                        ->orderBy('id', 'DESC')
                        ->take(10)
                        ->get();
                }

                // dd($data['posts']);
                $data['new_travellers'] = TravelMatesRequests::orderBy('created_at', 'DESC')->groupBy('users_id')->take(10)->get();
                $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->get();
                $data['me'] = Auth::user();

                return ApiResponse::create($data);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param string $key
     * @return string
     */
    public function _getRandom($key)
    {

        $init_array = [];
        for ($i = 1; $i < 32; $i++)
            $init_array[] = $i;

        $return = [];

        foreach ($init_array as $val) {
            $return[] = $val * $key % 32;
        }

        return implode(',', $return);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getSelect2()
    {
        try {
            $all_countries = Countries::all();
            $results = array();
            foreach ($all_countries as $count) {
                $results[] = array('id' => $count->id, 'text' => @$count->transsingle->title);
            }
            return ApiResponse::create([
                'results' => $results
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param  SearchLocationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect2Location(SearchLocationRequest $request)
    {
        try {
            $query = $request->get('q');

            if ($query != '') {

                $all_places = Countries::with('transsingle')
                    ->whereHas('transsingle', function ($q) use ($query) {
                        $q->where('title', 'LIKE', "%$query%");
                    })
                    ->get();
                $results = array();
                foreach ($all_places as $place) {
                    //value.id+','+value.trans[0].title+','+value.lat+","+value.lng+',Place
                    $results[] = array(
                        'id' => $place->id . ',' . $place->transsingle->title . ',' . $place->lat . "," . $place->lng . ',Country', 'text' => @$place->transsingle->title
                    );
                }

                $all_places = Cities::with('transsingle')
                    ->whereHas('transsingle', function ($q) use ($query) {
                        $q->where('title', 'LIKE', "%$query%");
                    })
                    ->get();
                foreach ($all_places as $place) {
                    //value.id+','+value.trans[0].title+','+value.lat+","+value.lng+',Place
                    $results[] = array(
                        'id' => $place->id . ',' . $place->transsingle->title . ',' . $place->lat . "," . $place->lng . ',City', 'text' => @$place->transsingle->title
                    );
                }


                $all_places = Place::searchByQuery(array('match' => array('title' => $query)));
                //$results = array();
                foreach ($all_places as $place) {
                    //value.id+','+value.trans[0].title+','+value.lat+","+value.lng+',Place
                    $results[] = array(
                        'id' => $place->id . ',' . $place->transsingle->title . ',' . $place->lat . "," . $place->lng . ',Place', 'text' => @$place->transsingle->title
                    );
                }

                $return = array('results' => $results);
                return ApiResponse::create([
                    'results' => $return
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SearchPlacesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchPlaces(SearchPlacesRequest $request)
    {
        try {
            $query = $request->get('q');
            $lat = $request->get('lat');
            $lng = $request->get('lng');

            if ($request->has('lat'))
                $lat = $request->get('lat');
            if ($request->has('lng'))
                $lng = $request->get('lng');
            if ($request->has('search_in'))
                $search_in = $request->get('search_in');
            if ($request->has('search_id')) {
                $search_id = $request->get('search_id');
                if ($search_id !== "false")
                    $search_in = $search_in . "_id";
            }
            //return json_encode($lat);
            if ($query != '') {
                $google_link = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
                    . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
                if ($lat && $lng) {
                    $google_link .= '&location=' . $lat . ',' . $lng . '&query=' . urlencode($query);
                } else {
                    $google_link .= '&query=' . $query;
                }
                $fetch_link = file_get_contents($google_link);
                $query_result = json_decode($fetch_link);
                $qr = $query_result->results;

                $final_results = array();
                foreach ($qr as $r) {

                    if ($r->types[0] !== "restaurant" && $r->types[0] !== "lodging") {

                        if (!Place::where('provider_id', '=', $r->place_id)->exists()) {

                            $p = new Place();
                            $p->place_type = @join(",", $r->types);
                            $p->safety_degrees_id = 1;
                            $p->provider_id = $r->place_id;
                            $p->countries_id = 373;
                            $p->cities_id = 2031;
                            $p->lat = $r->geometry->location->lat;
                            $p->lng = $r->geometry->location->lng;
                            $p->pluscode = $r->plus_code->compound_code;
                            $p->rating = $r->rating;
                            $p->active = 1;
                            $p->auto_import = 1;
                            $p->save();

                            $pt = new PlaceTranslations();
                            $pt->languages_id = 1;
                            $pt->places_id = $p->id;
                            $pt->title = $r->name;
                            $pt->address = $r->formatted_address;
                            //if (isset($places[$k]['phone']))
                            //    $pt->phone = $places[$k]['phone'];
                            //if (isset($places[$k]['website']))
                            //    $pt->description = $places[$k]['website'];
                            //$pt->working_days = $places[$k]['working_days'];
                            $pt->save();

                            $medium = array();
                            $medium['id'] = @$p->id;
                            $medium['place_type'] = @$r->types[0];
                            $medium['address'] = $r->formatted_address;
                            $medium['title'] = $r->name;
                            $medium['lat'] = $r->geometry->location->lat;
                            $medium['lng'] = $r->geometry->location->lng;
                            $medium['rating'] = $r->rating;
                            $medium['location'] = $r->geometry->location->lat . "," . $r->geometry->location->lng;
                        } else {
                            $p = Place::where('provider_id', '=', $r->place_id)->get()->first();
                            $medium = array();
                            $medium['id'] = @$p->id;
                            $medium['place_type'] = @explode(",", $p->place_type)[0];
                            $medium['address'] = $p->transsingle->address;
                            $medium['title'] = $p->transsingle->title;
                            $medium['lat'] = $p->lat;
                            $medium['lng'] = $p->lng;
                            $medium['rating'] = $p->rating;
                            $medium['location'] = $p->lat . "," . $p->lng;
                            if (isset($p->getMedias[0]) && is_object($p->getMedias[0]))
                                $medium['image'] = $p->getMedias[0]->url;
                        }
                        $final_results[] = $medium;
                    }
                }
                return ApiResponse::create($final_results);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SearchPOIRequest $request
     * @param TripInvitationsService $invitationsService
     * @return \Illuminate\Http\Response
     */
    public function getSearchCitiesPOIs(SearchPOIRequest $request, TripInvitationsService $invitationsService)
    {
        try {
            $query = $request->get('q');

            if ($request->has('lat'))
                $lat = $request->lat;
            if ($request->has('lng'))
                $lng = $request->lng;
            if ($request->has('search_in'))
                $search_in = $request->search_in;
            if ($request->has('search_id')) {
                $search_id = $request->search_id;
                if ($search_id !== "false") {
                    $search_in = $search_in . "_id";
                }
            }

            if ($request->has('type')) {
                $type = $request->type;
            }

            $fromExternal = false;

            if ($query != '') {

                $esResult = $this->getElSearchResult($query);
                $qr = $esResult['result'];

                // if ($qr == null || empty($qr)) {
                //     $proximity = '';

                //     if (isset($lng, $lat) && $lng && $lat && $lng !== 'false' && $lat !== 'false') {
                //         $proximity = "&proximity=$lng,$lat";
                //     }

                //     $mapboxLink = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . urlencode($query) . '.json?limit=10' . $proximity . '&routing=true&access_token=' . config('mapbox.token');

                //     if (isset($type) && $type) {
                //         $mapboxLink .= '&types=' . $type;
                //     }
                //     $mapboxResult = json_decode(file_get_contents($mapboxLink));

                //     $qr = $mapboxResult->features;
                //     $fromExternal = true;
                // }

                $result = [];

                foreach ($qr as $r) {
                    $medium = [];

                    if ($fromExternal) {
                        if (!Place::where('provider_id', '=', $r->id)->exists()) {
                            $medium['is_new'] = 1;
                            $medium['id'] = $r->id;
                            $medium['place_type'] = @$r->place_type[0]; //implode(',', $r->place_type);
                            $medium['address'] = str_replace("'", "&quot;", @$r->properties->address);
                            $medium['title'] = $r->text;
                            $medium['lat'] = $r->geometry->coordinates[1];
                            $medium['lng'] = $r->geometry->coordinates[0];
                            $medium['rating'] = @$r->rating;
                            $medium['location'] = $r->geometry->coordinates[1] . "," . $r->geometry->coordinates[0];
                            $medium['city_id'] = @$r->cities_id;
                            $medium['city_title'] = @explode(', ', $r->place_name)[2];
                        } else {
                            $p = Place::where('provider_id', '=', $r->id)->get()->first();

                            $medium['id'] = @$p->id;
                            $medium['place_type'] = @explode(",", $p->place_type)[0];
                            $medium['address'] = str_replace("'", "&quot;", $p->transsingle->address);
                            $medium['title'] = $p->transsingle->title;
                            $medium['lat'] = $p->lat;
                            $medium['lng'] = $p->lng;
                            $medium['rating'] = @$p->rating;
                            $medium['location'] = $p->lat . "," . $p->lng;
                            if (isset($p->getMedias[0]) && is_object($p->getMedias[0]))
                                $medium['image'] = $p->getMedias[0]->url;

                            $medium['city_id'] = @$p->cities_id;
                            $medium['city_title'] = @$p->city->transsingle->title;
                        }

                        $medium['type'] = '';

                        if ($medium['place_type'] != 'locality') {
                            $result[] = $medium;
                        }
                    } else {
                        $medium['id'] = @$r['place_id'];
                        $medium['address'] = $r['address'];
                        $medium['title'] = $r['place_title'];
                        $medium['lat'] = $r['lat'];
                        $medium['place_type'] = @explode(",", @$r['place_type'])[0];
                        $medium['lng'] = $r['lng'];
                        $medium['location'] = $r['lat']  . "," . $r['lng'];

                        if (isset($r['place_media']) && $r['place_media'] != null) {
                            $medium['image'] = $r['place_media'];
                        }

                        $p = Place::find($medium['id']);

                        $medium['city_id'] = @$p->cities_id;
                        $medium['city_title'] = @$p->city->transsingle->title;
                        $medium['type'] = '';

                        $result[] = $medium;
                    }
                }

                foreach ($result as &$place) {
                    $place['reviews'] = Reviews::where('places_id', $place['id'])->get();
                    $place['reviews_avg'] = Reviews::where('places_id', $place['id'])->avg('score');
                    $isTrending = PlacesTop::where('places_id', $place['id'])->exists();

                    if ($isTrending) {
                        $place['type'] = 'Trending';
                        continue;
                    }

                    $friendsAndFollowingsIds = $invitationsService->findFriends()->merge($invitationsService->findFollowings());

                    $isSuggested = Checkins::query()->where('place_id', $place['id'])->whereIn('users_id', $friendsAndFollowingsIds)->exists();

                    if ($isSuggested) {
                        $place['type'] = 'Suggested';
                    }
                }

                return ApiResponse::create($result);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/home/search/poi",
     *   tags={"Search"},
     *   summary="Search place",
     *   operationId="Api/HomeController@getSearchPOIs",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="q",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="lng",
     *        in="query",
     *        description="longitude",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *    @OA\Parameter(
     *        name="lat",
     *        in="query",
     *        description="latitude",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param SearchCitiesPOIRequest $request
     * @param TripInvitationsService $invitationsService
     * @return \Illuminate\Http\Response
     */
    public function getSearchPOIs(SearchPOIRequest $request, TripInvitationsService $invitationsService)
    {
        try {
            $query = $request->get('q');

            $lat = null;
            $lng = null;
            $cityId = null;

            if ($request->has('lat'))
                $lat = $request->get('lat');
            if ($request->has('lng'))
                $lng = $request->get('lng');
            if ($request->has('search_in'))
                $search_in = $request->get('search_in');
            if ($request->has('search_id')) {
                $search_id = $request->get('search_id');
                if ($search_id !== "false") {
                    $search_in = $search_in . "_id";
                }
            }

            if ($request->has('type')) {
                $type = $request->get('type');
            }

            if ($request->has('city_id')) {
                $cityId = $request->get('city_id');
            }

            if ($query != '') {

                $qr = $this->getElSearchResult($query)['result'];
                $result = [];

                foreach ($qr as $r) {
                    $_place_type = explode(",", $r['place_type']);
                    if (isset($_place_type[0], $r['place_id']) && in_array($_place_type[0], getAllowedPlaceTypes())) {
                        $medium = [];

                        if ($r['countries_id'] != 326 && $r['cities_id'] != 2031 && $r['countries_id'] != 373) {
                            $medium['id'] = $r['place_id'];
                            $medium['address'] = $r['address'];
                            $medium['title'] = $r['place_title'];
                            $medium['lat'] = $r['lat'];
                            $medium['place_type'] = $_place_type[0];
                            $medium['lng'] = $r['lng'];
                            $medium['location'] = $r['lat']  . "," . $r['lng'];

                            if (isset($r['place_media']) && $r['place_media'] != null) {
                                $medium['image'] = $r['place_media'];
                            }


                            $p = Place::with('city.transsingle')->where('id', $medium['id'])->first();
                            $city = null;
                            if (isset($p->city)) {
                                $city = $p->city;
                                $city->title = @$city->transsingle->title;
                                $city->subtitle = @$city->country->transsingle->title;
                                $city->image = (isset($city->medias[0]->media->path)) ? check_city_photo($city->medias[0]->media->path) : url(PLACE_PLACEHOLDERS);
                                unset($city->medias, $city->country);
                            }

                            $medium['city_id'] = @$p->cities_id;
                            $medium['city_title'] = @$p->city->transsingle->title;
                            $medium['city'] = $city;
                            $medium['type'] = '';
                            if (isset($p->city)) {
                                $result[] = $medium;
                            }
                        }
                    }
                }

                foreach ($result as $key => &$place) {
                    $place['image'] = "https://www.travooo.com/assets2/image/placeholders/place.png";
                    $place['reviews'] = Reviews::where('places_id', $place['id'])->take(3)->get();
                    $place['reviews_avg'] = Reviews::where('places_id', $place['id'])->avg('score');
                    $isTrending = PlacesTop::where('places_id', $place['id'])->exists();

                    if ($isTrending) {
                        $place['type'] = 'Trending';
                        continue;
                    }

                    $friendsAndFollowingsIds = $invitationsService->findFriends()->merge($invitationsService->findFollowings());

                    $isSuggested = Checkins::query()->where('place_id', $place['id'])
                        ->whereIn('users_id', $friendsAndFollowingsIds)->exists();

                    if ($isSuggested) {
                        $place['type'] = 'Suggested';
                    }

                    if (!$place['type']) {
                        $place['type'] = null;
                    }
                }
                return ApiResponse::create($result);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/home/suggest/top-places",
     *   tags={"Search"},
     *   summary="Suggest Top Places",
     *   operationId="Api/HomeController@getSuggestTopPlaces",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="city_id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param SuggestTopPlacesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSuggestTopPlaces(SuggestTopPlacesRequest $request)
    {
        try {
            $city_id = $request->city_id;
            return ApiResponse::create($this->__getSuggestTopPlaces($city_id));
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    public function __getSuggestTopPlaces($city_id)
    {
        if (true) {
            $trendsService = app(TrendsService::class);
            $place_suggestions = $trendsService->getMixTrendingLocations(TrendsService::LOCATION_TYPE_PLACE, $city_id, false, [], [], 3, true);
            $final = array();
            foreach ($place_suggestions as $ps) {
                $medium = array();
                $medium['id'] = $ps['location']->id;
                $medium['place_type'] = @explode(",", $ps['location']->place_type)[0];
                $medium['address'] = $ps['location']->transsingle->address;
                $medium['title'] = $ps['location']->travooo_title;
                $medium['lat'] = $ps['location']->lat;
                $medium['lng'] = $ps['location']->lng;
                $medium['rating'] = $ps['location']->rating;
                $medium['location'] = $ps['location']->lat . "," . $ps['location']->lng;
                if (isset($ps['location']->getMedias[0]) && is_object($ps['location']->getMedias[0])) {
                    $medium['image'] =  "https://s3.amazonaws.com/travooo-images2/" . $ps['location']->getMedias[0]->url;
                } else {
                    $medium['image'] = "https://www.travooo.com/assets2/image/placeholders/place.png";
                }

                $medium['city_id'] = @$ps['location']->cities_id;
                $medium['city_title'] = @$ps['location']->city->transsingle->title;
                $medium['type'] = 'Trending';
                $medium['reviews'] = Reviews::where('places_id', $ps['location']->id)->get();
                $medium['reviews_avg'] = Reviews::where('places_id', $ps['location']->id)->avg('score');
                $final[] = $medium;
            }
            return $final;
        } else {
            $random_val = $this->_getRandom(date('j'));
            $tp_array = getTopPlacesByUserPref(['city' => $city_id]);
            // dd($tp_array);
            $data['top_places'] = PlacesTop::whereIn('places_id', $tp_array)
                ->limit(6)
                ->whereNotIn('country_id', [373, 326])
                ->get();
            if (count($data['top_places']) < 20) {
                $remaining = 20 - count($data['top_places']);
                $count_remaing = PlacesTop::whereNotIn('places_id', $tp_array)
                    ->where('city_id', $city_id)
                    ->whereNotIn('country_id', [373, 326])
                    ->limit($remaining)
                    ->get();

                $data['top_places'] = $data['top_places']->merge($count_remaing);
            }
            $array_list = [];
            foreach ($data['top_places'] as $tp_place) {
                $array_list[] = $tp_place->places_id;
            }


            // $place_suggestions = Place::leftJoin('places_top', 'places_top.places_id', '=', 'places.id')
            //         ->leftJoin('places_trans', 'places.id', '=', 'places_trans.places_id')
            //         ->where('places_top.city_id', $city_id)
            //         ->whereIn('places_top.places_id',$array_list)
            //         ->take(20)
            //         ->get();

            $place_suggestions = PlacesTop::whereIn('places_id', $array_list)
                ->whereHas('place', function ($query) {
                    $query->whereHas('medias');
                })
                ->inRandomOrder()
                ->limit(50)
                ->get();
            $final = array();
            foreach ($place_suggestions as $ps) {
                $medium = array();
                $medium['id'] = $ps->places_id;
                $medium['place_type'] = @explode(",", $ps->place->place_type)[0];
                $medium['address'] = $ps->place->transsingle->address;
                $medium['title'] = $ps->travooo_title;
                $medium['lat'] = $ps->place->lat;
                $medium['lng'] = $ps->place->lng;
                $medium['rating'] = $ps->place->rating;
                $medium['location'] = $ps->place->lat . "," . $ps->place->lng;
                if (isset($ps->place->getMedias[0]) && is_object($ps->place->getMedias[0])) {
                    $medium['image'] =  "https://s3.amazonaws.com/travooo-images2/" . $ps->place->getMedias[0]->url;
                } else {
                    $medium['image'] = "https://www.travooo.com/assets2/image/placeholders/place.png";
                }

                $medium['city_id'] = @$ps->place->cities_id;
                $medium['city_title'] = @$ps->place->city->transsingle->title;
                $medium['type'] = 'Trending';
                $medium['reviews'] = Reviews::where('places_id', $ps->places_id)->get();
                $medium['reviews_avg'] = Reviews::where('places_id', $ps->places_id)->avg('score');
                $final[] = $medium;
            }
            return $final;
        }
    }

    /**
     * @param SearchHotelsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchHotels(SearchHotelsRequest $request)
    {
        try {
            if ($request->has('term')) {
                $query = $request->get('term');
            } else {
                $query = $request->get('q');
            }
            $lat = $request->get('lat');
            $lng = $request->get('lng');
            if ($request->has('lat'))
                $lat = $request->get('lat');
            if ($request->has('lng'))
                $lng = $request->get('lng');
            if ($request->has('search_in'))
                $search_in = $request->get('search_in');
            if ($request->has('search_id')) {
                $search_id = $request->get('search_id');
                if ($search_id !== "false")
                    $search_in = $search_in . "_id";
            }

            if ($query != '') {
                $query = "Hotel " . $query;
                $google_link = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
                    . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
                if ($lat && $lng) {
                    $google_link .= '&location=' . $lat . ',' . $lng . '&query=' . urlencode($query);
                } else {
                    $google_link .= '&query=' . $query;
                }

                $fetch_link = file_get_contents($google_link);
                $query_result = json_decode($fetch_link);
                $qr = $query_result->results;

                //dd($query_result->next_page_token);
                //$result = $query_result1->results;

                $final_results = array();
                foreach ($qr as $r) {

                    $medium = array();

                    if (!Hotels::where('provider_id', '=', $r->place_id)->exists()) {

                        $p = new Hotels();
                        $p->place_type = @join(",", $r->types);
                        //$p->safety_degrees_id = 1;
                        $p->provider_id = $r->place_id;
                        $p->countries_id = 373;
                        $p->cities_id = 2031;
                        $p->lat = $r->geometry->location->lat;
                        $p->lng = $r->geometry->location->lng;
                        //$p->pluscode = $r->plus_code->compound_code;
                        $p->rating = $r->rating;
                        $p->active = 1;
                        $p->auto_import = 1;
                        $p->save();

                        $pt = new HotelsTranslations();
                        $pt->languages_id = 1;
                        $pt->hotels_id = $p->id;
                        $pt->title = $r->name;
                        $pt->address = $r->formatted_address;
                        //if (isset($places[$k]['phone']))
                        //    $pt->phone = $places[$k]['phone'];
                        //if (isset($places[$k]['website']))
                        //    $pt->description = $places[$k]['website'];
                        //$pt->working_days = $places[$k]['working_days'];
                        $pt->save();

                        $medium = array();
                        $medium['id'] = @$p->id;
                        $medium['place_type'] = @$r->types[0];
                        $medium['address'] = $r->formatted_address;
                        $medium['title'] = $r->name;
                        $medium['lat'] = $r->geometry->location->lat;
                        $medium['lng'] = $r->geometry->location->lng;
                        $medium['rating'] = $r->rating;
                        $medium['location'] = $r->geometry->location->lat . "," . $r->geometry->location->lng;
                        $medium['image'] = @$photo_url;
                    } else {
                        $p = Hotels::where('provider_id', '=', $r->place_id)->get()->first();

                        $medium = array();
                        $medium['id'] = @$p->id;
                        $medium['place_type'] = @explode(",", $p->place_type)[0];
                        $medium['address'] = $p->transsingle->address;
                        $medium['title'] = $p->transsingle->title;
                        $medium['lat'] = $p->lat;
                        $medium['lng'] = $p->lng;
                        $medium['rating'] = $p->rating;
                        $medium['location'] = $p->lat . "," . $p->lng;
                        if (isset($p->getMedias[0]) && is_object($p->getMedias[0]))
                            $medium['image'] = $p->getMedias[0]->url;
                    }

                    $final_results[] = $medium;
                }
                return ApiResponse::create($final_results);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SearchHotelsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchHotelsMention(SearchHotelsRequest $request)
    {
        try {
            if ($request->has('term')) {
                $query = $request->get('term');
            } else {
                $query = $request->get('q');
            }
            if ($request->has('lat'))
                $lat = $request->get('lat');
            if ($request->has('lng'))
                $lng = $request->get('lng');
            if ($request->has('search_in'))
                $search_in = $request->get('search_in');
            if ($request->has('search_id')) {
                $search_id = $request->get('search_id');
                if ($search_id !== "false")
                    $search_in = $search_in . "_id";
            }

            if ($query != '') {

                if (isset($lat) && $lat > 0 && isset($lng) && $lng > 0) {

                    if (isset($search_in) && $search_in !== "false") {


                        $search_terms = array('bool' => array('must' => array(
                            array('match' => array($search_in => $search_id)),
                            array('match' => array('title' => $query))
                        )));

                        //var_dump(json_encode($search_terms));
                        $all_hotels = Hotels::searchByQuery(
                            $search_terms,
                            null,
                            null,
                            1000,
                            null,
                            array('_geo_distance' => array('location' => $lat . "," . $lng, 'order' => 'asc', 'unit' => 'km'))
                        );
                    } else {

                        $search_terms = array('match' => array('title' => $query));

                        $all_hotels = Hotels::searchByQuery(
                            $search_terms,
                            null,
                            null,
                            1000,
                            null,
                            array('_geo_distance' => array('location' => $lat . "," . $lng, 'order' => 'asc', 'unit' => 'km'))
                        );
                    }
                } else {
                    if (isset($search_in) && $search_in !== "false") {


                        $search_terms = array('bool' => array('must' => array(
                            array('match' => array($search_in => $search_id)),
                            array('match' => array('title' => $query))
                        )));
                        $all_hotels = Hotels::searchByQuery(
                            $search_terms,
                            null,
                            null,
                            1000
                        );
                    } else {
                        $search_terms = array('match' => array('title' => $query));

                        $all_hotels = Hotels::searchByQuery(
                            $search_terms,
                            null,
                            null,
                            1000
                        );
                    }
                }
            }

            return ApiResponse::create($all_hotels);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SearchRestaurantsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchRestaurants(SearchRestaurantsRequest $request)
    {
        try {
            $query = $request->get('q');
            $lat = $request->get('lat');
            $lng = $request->get('lng');

            if ($request->has('lat'))
                $lat = $request->get('lat');
            if ($request->has('lng'))
                $lng = $request->get('lng');
            if ($request->has('search_in'))
                $search_in = $request->get('search_in');
            if ($request->has('search_id')) {
                $search_id = $request->get('search_id');
                if ($search_id !== "false")
                    $search_in = $search_in . "_id";
            }

            if ($query != '') {
                $query = "Restaurant " . $query;
                $google_link = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
                    . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
                if ($lat && $lng) {
                    $google_link .= '&location=' . $lat . ',' . $lng . '&query=' . urlencode($query);
                } else {
                    $google_link .= '&query=' . $query;
                }

                $fetch_link = file_get_contents($google_link);
                $query_result = json_decode($fetch_link);
                $qr = $query_result->results;


                $final_results = array();
                foreach ($qr as $r) {

                    $medium = array();

                    if (!Restaurants::where('provider_id', '=', $r->place_id)->exists()) {

                        $p = new Restaurants();
                        $p->place_type = @join(",", $r->types);
                        //$p->safety_degrees_id = 1;
                        $p->provider_id = $r->place_id;
                        $p->countries_id = 373;
                        $p->cities_id = 2031;
                        $p->lat = $r->geometry->location->lat;
                        $p->lng = $r->geometry->location->lng;
                        //$p->pluscode = $r->plus_code->compound_code;
                        $p->rating = $r->rating;
                        $p->active = 1;
                        $p->auto_import = 1;
                        $p->save();

                        $pt = new RestaurantsTranslations();
                        $pt->languages_id = 1;
                        $pt->restaurants_id = $p->id;
                        $pt->title = $r->name;
                        $pt->address = $r->formatted_address;
                        //if (isset($places[$k]['phone']))
                        //    $pt->phone = $places[$k]['phone'];
                        //if (isset($places[$k]['website']))
                        //    $pt->description = $places[$k]['website'];
                        //$pt->working_days = $places[$k]['working_days'];
                        $pt->save();

                        $medium = array();
                        $medium['id'] = @$p->id;
                        $medium['place_type'] = @$r->types[0];
                        $medium['address'] = $r->formatted_address;
                        $medium['title'] = $r->name;
                        $medium['lat'] = $r->geometry->location->lat;
                        $medium['lng'] = $r->geometry->location->lng;
                        $medium['rating'] = $r->rating;
                        $medium['location'] = $r->geometry->location->lat . "," . $r->geometry->location->lng;
                    } else {
                        $p = Restaurants::where('provider_id', '=', $r->place_id)->get()->first();

                        $medium = array();
                        $medium['id'] = @$p->id;
                        $medium['place_type'] = @explode(",", $p->place_type)[0];
                        $medium['address'] = $p->transsingle->address;
                        $medium['title'] = $p->transsingle->title;
                        $medium['lat'] = $p->lat;
                        $medium['lng'] = $p->lng;
                        $medium['rating'] = $p->rating;
                        $medium['location'] = $p->lat . "," . $p->lng;
                        if (isset($p->getMedias[0]) && is_object($p->getMedias[0]))
                            $medium['image'] = $p->getMedias[0]->url;
                    }

                    $final_results[] = $medium;
                }
                return ApiResponse::create($final_results);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SearchCountriesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchCountries(SearchCountriesRequest $request)
    {
        try {
            $queryParam = $request->get('q');

            if ($queryParam != '') {

                $all_countries = Countries::with('transsingle')->with('region.transsingle')
                    ->with('getMedias')
                    ->whereHas('transsingle', function ($query) use ($queryParam) {
                        $query->where('title', 'like', "%" . $queryParam . "%");
                    })
                    ->get();
                return ApiResponse::create($all_countries);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SearchCitiesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchCities(SearchCitiesRequest $request)
    {
        try {
            $queryParam = $request->get('q');

            if ($queryParam != '') {

                $all_cities = Cities::with('transsingle')->with('country.transsingle')
                    ->with('getMedias')
                    ->whereHas('transsingle', function ($query) use ($queryParam) {
                        $query->where('title', 'like', "%" . $queryParam . "%");
                    })
                    ->get();
                return ApiResponse::create($all_cities);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SearchUsersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchUsers(SearchUsersRequest $request)
    {
        try {
            $queryParam = $request->get('q');

            if ($queryParam != '') {
                $all_users = User::where('name', 'like', "%" . $queryParam . "%")
                    ->get();

                return ApiResponse::create($all_users);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/home/search-countries-cities",
     *   tags={"Home"},
     *   summary="Get locations",
     *   operationId="Api/HomeController@getSearchCountriesCities",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *     @OA\Parameter(
     *        name="q",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param SearchCountriesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchCountriesCities(SearchCountriesRequest $request)
    {
        try {
            $queryParam = $request->get('q');
            if ($queryParam != '') {

                $all_countries = Countries::with('transsingle')
                    ->whereHas('transsingle', function ($query) use ($queryParam) {
                        $query->where('title', 'like', "%" . $queryParam . "%");
                    })
                    ->take(20)->get();

                $all_cities = Cities::with('transsingle')
                    ->whereHas('transsingle', function ($query) use ($queryParam) {
                        $query->where('title', 'like', "%" . $queryParam . "%");
                    })
                    ->take(20)->get();

                $results = array();

                foreach ($all_countries as $aco) {
                    $results[] = array(
                        'id' => 'country,' . $aco->id,
                        'text' => $aco->transsingle->title,
                        'image' => check_country_photo(@$aco->getMedias[0]->url, 180),
                        'type' => '',
                        'query' => $queryParam
                    );
                }
                foreach ($all_cities as $aci) {
                    $results[] = array(
                        'id' => 'city,' . $aci->id,
                        'text' => $aci->transsingle->title,
                        'image' => check_country_photo(@$aci->getMedias[0]->url, 180),
                        'type' => @$aci->country->transsingle->title,
                        'query' => $queryParam
                    );
                }

                return ApiResponse::create($results);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SearchTravelstylesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchTravelstyles(SearchTravelstylesRequest $request)
    {
        try {
            $queryParam = $request->get('q');

            if ($queryParam != '') {


                $all_travelstyles = Travelstyles::with('transsingle')
                    ->whereHas('transsingle', function ($query) use ($queryParam) {
                        $query->where('title', 'like', "%" . $queryParam . "%");
                    })
                    ->select('id')
                    ->get();

                $results = array();

                foreach ($all_travelstyles as $ats) {
                    $results[] = array('id' => $ats->id, 'text' => $ats->transsingle->title);
                }

                return ApiResponse::create(
                    ['results' => $results]
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/home/list_travelstyles",
     *   tags={"Home"},
     *   summary="Get travel styles",
     *   operationId="Api/HomeController@getListTravelstylesForSelect",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @return \Illuminate\Http\Response
     */
    public function getListTravelstylesForSelect()
    {
        try {
            $all_travelstyles = Travelstyles::with('transsingle')->select('id')->get();
            $results = array();
            foreach ($all_travelstyles as $ats) {
                $results[] = array('id' => $ats->id, 'text' => $ats->transsingle->title);
            }

            return ApiResponse::create(['results' => $results]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param URLPreviewRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postURLPreview(URLPreviewRequest $request)
    {
        try {
            $url = $request->get('url');
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $data = curl_exec($ch);
            curl_close($ch);

            // Load HTML to DOM Object
            $dom = new DOMDocument();
            @$dom->loadHTML($data);

            // Parse DOM to get Title
            $nodes = $dom->getElementsByTagName('title');
            $title = $nodes->item(0)->nodeValue;

            // Parse DOM to get Meta Description
            $metas = $dom->getElementsByTagName('meta');
            $body = "";
            for ($i = 0; $i < $metas->length; $i++) {
                $meta = $metas->item($i);
                if ($meta->getAttribute('name') == 'description') {
                    $body = $meta->getAttribute('content');
                }
            }

            // Parse DOM to get Images
            $image_urls = array();
            $images = $dom->getElementsByTagName('img');

            for ($i = 0; $i < $images->length; $i++) {
                $image = $images->item($i);
                $src = $image->getAttribute('src');

                if (filter_var($src, FILTER_VALIDATE_URL)) {
                    $image_src[] = $src;
                }
            }

            $output = array(
                'title' => $title,
                'image_src' => $image_src,
                'body' => $body
            );

            return ApiResponse::create($output);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/home/search_pois_for_tagging",
     *   tags={"Search"},
     *   summary="Search Tagging places or users",
     *   operationId="Api/HomeController@getSearchPOIsForTagging",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="query",
     *        description="search keyword",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="lat",
     *        description="latitude",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="lng",
     *        description="longitude",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param SearchPOIsForTaggingRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchPOIsForTagging(SearchPOIsForTaggingRequest $request)
    {
        try {
            $query = $request->get('query');
            $lat = floatval($request->get('lat'));
            $lng = floatval($request->get('lng'));

            if ($request->has('search_in'))
                $search_in = $request->get('search_in');
            if ($request->has('search_id')) {
                $search_id = $request->get('search_id');
                if ($search_id !== "false")
                    $search_in = $search_in . "_id";
            }

            $final_results['meta']['query'] = $query;
            $final_results['data']['places'] = [];
            $final_results['data']['users'] = [];

            if ($query != '') {

                // FIND PLACES

                $guzzleClient = new Client();

                $guzzleQuery = [
                    'key' => 'AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q',
                    'query' => $query
                ];
                if ($lat != 0 && $lng != 0) {
                    $guzzleQuery['location'] = $lat . ',' . $lng;
                    $guzzleQuery['radius'] = 50000;
                }

                // https://httpbin.org/get - link for test

                // Google docs
                // https://developers.google.com/places/web-service/search
                $response = $guzzleClient->get('https://maps.googleapis.com/maps/api/place/textsearch/json', [
                    'query' => $guzzleQuery
                ]);
                $body = $response->getBody();
                // $contents = $body->getContents();

                $query_result = json_decode($body);
                $qr = $query_result->results;

                // END GOOGLE REQUEST
                // START TO PROCESS RESULTS

                $place_providers = Arr::pluck($qr, 'place_id');

                $alreadyExistsPlaces = Place::whereIn('provider_id', $place_providers)->get();
                $alreadyExistsIds = $alreadyExistsPlaces->pluck('provider_id');

                $newPlacesIds = array_diff($place_providers, $alreadyExistsIds->toArray());

                $hasNewPlaces = boolval(count($newPlacesIds));

                if ($hasNewPlaces) {

                    // PROCESS PLACES
                    $newPlaces = [];
                    $newPlaceTranslations = [];

                    foreach ($qr as $r) {
                        if (in_array($r->place_id, $alreadyExistsIds->toArray()))
                            continue;

                        $model = [
                            'place_type' => @join(",", $r->types),
                            'safety_degrees_id' => 1,
                            'provider_id' => $r->place_id,
                            'countries_id' => 373,
                            'cities_id' => 2031,
                            'lat' => $r->geometry->location->lat,
                            'lng' => $r->geometry->location->lng,
                            'pluscode' => @$r->plus_code->compound_code,
                            'rating' => @$r->rating,
                            'active' => 1,
                            'auto_import' => 1,
                        ];

                        array_push($newPlaces, $model);


                        array_push($newPlaceTranslations, [
                            'provider_id' => $r->place_id,
                            'languages_id' => 1,
                            'title' => $r->name,
                            'address' => $r->formatted_address,
                            //                        'phone' => $r->phone,
                            //                        'description' => $r->website,
                            //                        'working_days' => $r->working_days,
                        ]);
                    } // foreach ($qr AS $r)

                    // insert new places
                    Place::insert($newPlaces);


                    // PROCESS TRANSLATIONS
                    $newlyAddedPlaces = Place::whereIn('provider_id', $newPlacesIds)
                        ->pluck('id', 'provider_id');

                    for ($i = 0; $i < count($newPlaceTranslations); $i++) {
                        $newPlaceTranslations[$i]['places_id'] = $newlyAddedPlaces[$newPlaceTranslations[$i]['provider_id']];
                        unset($newPlaceTranslations[$i]['provider_id']);
                    }

                    // insert new translations
                    PlaceTranslations::insert($newPlaceTranslations);
                } // END if( $hasNewPlaces )


                // at this point all new places stored in DB
                // BUILD RESPONSE

                $places = Place::whereIn('provider_id', $place_providers)->get();
                $places = $places->unique('provider_id'); // to prevent doubles
                $places_ids = $places->pluck('id');

                $transsingle = PlaceTranslations::select('places_id', 'address', 'title')
                    ->whereIn('places_id', $places_ids)
                    ->get()
                    ->keyBy('places_id');

                $placesMedias = PlaceMedias::whereIn('places_id', $places_ids)
                    ->get()
                    ->keyBy('places_id');

                $medias = [];
                foreach ($placesMedias as $place_id => $placesMedia) {
                    $media = Medias::select('url')
                        ->where('id', $placesMedia->medias_id)
                        ->first();

                    if (is_null($media))
                        continue;

                    $medias[$place_id] = $media;
                }

                $placesResponse = [];
                foreach ($places as $place) {
                    $medium = [];
                    $medium['id'] = @$place->id;
                    $medium['place_type'] = @explode(",", $place->place_type)[0];
                    $medium['address'] = @$transsingle[@$place->id]['address'];
                    $medium['title'] = @$transsingle[@$place->id]['title'];
                    $medium['lat'] = $place->lat;
                    $medium['lng'] = $place->lng;
                    $medium['rating'] = @$place->rating;
                    $medium['location'] = $place->lat . "," . $place->lng;
                    if (isset($medias[@$place->id]))
                        $medium['image'] = @$medias[@$place->id]->url;

                    array_push($placesResponse, $medium);
                }

                $placesResponse = array_slice($placesResponse, 0, 8);
                $final_results['data']['places'] = $placesResponse;


                // FIND USERS
                $users = User::where('name', 'like', "%" . $query . "%")
                    ->take(8)
                    ->get();

                $final_results['data']['users'] = $users;
            }

            return ApiResponse::create($final_results);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function tools()
    {
        try {
            $hotels = Embassies::take(2000)->get();
            foreach ($hotels as $h) {
                if (Place::where('provider_id', $h->provider_id)->count() == 0) {
                    //echo '- Add new place with id: ' . $h->provider_id . "<br />";
                    $p = new Place;
                    $p->countries_id = $h->countries_id;
                    $p->cities_id = $h->cities_id;
                    $p->place_type = $h->place_type;
                    $p->place_section = 'embassies';
                    $p->provider_id = $h->provider_id;
                    $p->lat = $h->lat;
                    $p->lng = $h->lng;
                    $p->pluscode = $h->pluscode;
                    $p->rating = $h->rating;
                    $p->active = $h->active;
                    $p->media_done = $h->media_done;
                    $p->cover_media_id = $h->cover_media_id;
                    $p->auto_import = $h->auto_import;
                    $p->visits = 0;
                    if ($p->save()) {
                        $pt = new PlaceTranslations;
                        $pt->places_id = $p->id;
                        $pt->languages_id = 1;
                        $pt->title = $h->transsingle->title;
                        $pt->description = $h->transsingle->description;
                        $pt->address = $h->transsingle->address;
                        $pt->phone = $h->transsingle->phone;
                        $pt->highlights = '';
                        $pt->working_days = $h->transsingle->working_days;
                        $pt->working_times = $h->transsingle->working_times;
                        $pt->how_to_go = $h->transsingle->how_to_go;
                        $pt->when_to_go = $h->transsingle->when_to_go;
                        $pt->num_activities = '';
                        $pt->popularity = $h->transsingle->popularity;
                        $pt->conditions = $h->transsingle->conditions;
                        $pt->price_level = $h->transsingle->price_level;
                        $pt->num_checkins = $h->transsingle->num_checkins;
                        $pt->history = $h->transsingle->history;
                        $pt->save();

                        foreach ($h->medias as $hm) {
                            //dd($hm);
                            $pm = new PlaceMedias;
                            $pm->places_id = $p->id;
                            $pm->medias_id = $hm->id;
                            $pm->save();
                        }

                        $h->delete();
                    }
                } else {
                    //echo '- Place existed before: ' . $h->provider_id . "<br />";
                    $h->delete();
                }
            }
            /*
            $some_countries = Countries::whereIn('id', array(266, 14, 21, 290, 318, 27, 267, 39, 322, 80, 330, 104, 127, 136, 358, 147, 271, 324, 360, 161, 168, 366, 263, 212, 276, 227))
            ->get();
            foreach ($some_countries AS $sc) {
            //dd($sc->countryHolidays);
            foreach ($sc->countryHolidays AS $sch) {
            foreach ($sc->cities AS $city) {
            $new_holiday = new \App\Models\City\CitiesHolidays;
            $new_holiday->cities_id = $city->id;
            $new_holiday->holidays_id = $sch->holidays_id;
            $new_holiday->date = $sch->date;
            $new_holiday->save();
            }
            }
            }
            *
            */
            return ApiResponse::create(
                [
                    'status' => true
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/notifications",
     *   tags={"Other"},
     *   summary="This Api used to fetch notifications.",
     *   operationId="app\Http\Controllers\Api\HomeController.php@postGetNotifications",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="page_no",
     *        description="page| default = 1",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="per_page",
     *        description="default = 10",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="interger"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function postGetNotifications(Request $request)
    {
        try {
            $page = (int) $request->get('page', 1);
            $per_page = (int) $request->get('per_page', 10);

            $notifications = Notifications::query()->where('users_id', Auth::user()->id)->whereHas('author');
            if ($page == 1) {
                (clone $notifications)->update(['read' => 1]);
            }
            $notifications_unseen = (clone $notifications)->where('read', 0)->count();
            $notifications = (clone $notifications)->with('author')->orderBy('created_at', 'DESC')->take(10)->paginate($per_page);


            $notifications = modifyPagination($notifications);
            $notifications->data = collect($notifications->data)->map(function ($noti) {
                $noti->_display = true;
                $meta = [];
                if ($noti->type == 'plan_invitation_received') {
                    $contrRequest = TripsContributionRequests::find($noti->data_id);
                    $meta['invite_id'] = $noti->data_id;
                    $meta['trip_id'] = $contrRequest->plans_id;
                } else if ($noti->type == 'plan_create') {
                    $meta['trip_id'] = $noti->data_id;
                } else if ($noti->type == 'plan_invitetion_accepted') {
                } else if ($noti->type == 'plan_invitetion_rejected') {
                } else if ($noti->type == 'plan_invitetion_unpublished') {
                } else if ($noti->type == 'plan_version_sent') {
                    $meta['trip_id'] = $noti->data_id;
                } else if ($noti->type == 'status_like') {
                    $post = Posts::find($noti->data_id);
                    if ($post) {
                        $meta['text'] = $post->text;
                    } else {
                        $noti->_display = false;
                    }
                } else if ($noti->type == 'discussion_upvote') {
                } else if ($noti->type == 'status_unlike') {
                    $post = Posts::find($noti->data_id);
                    if ($post) {
                        $meta['text'] = $post->text;
                    } else {
                        $noti->_display = false;
                    }
                } else if ($noti->type == 'status_comment') {
                    $post = Posts::find($noti->data_id);
                    if ($post) {
                        $meta['text'] = $post->text;
                    } else {
                        $noti->_display = false;
                    }
                } else if ($noti->type == 'approve') {
                } else if ($noti->type == 'disapprove') {
                } else if ($noti->type == 'approve_suggestion') {
                } else if ($noti->type == 'disapprove_suggestion') {
                } else if ($noti->type == 'post_delete') {
                    $noti->notes =  $noti->notes ? $noti->notes : 'Your post has been deleted';
                } else if ($noti->type == 'restore_post') {
                } else if ($noti->type == 'copyright_policy') {
                    $meta['link'] = url('copyright-policy/' . $noti->data_id);
                } else if ($noti->type == 'plan_suggestion_added') {
                    $suggestion = TripsSuggestion::find($noti->data_id);
                    if ($suggestion && isset($suggestion->plan)) {
                        $plan = $suggestion->plan;
                        $meta['trip_id'] = $plan->id;
                        $meta['trip_title'] = $plan->title;
                    } else {
                        $noti->_display = false;
                    }
                } else if ($noti->type == 'ask_experts') {
                }
                $noti->meta = $meta;
                return $noti;
            });


            $notifications->data = collect($notifications->data)->filter(function ($noti) {
                return $noti->_display == true;
            });

            return ApiResponse::create([
                'count_unseen' => $notifications_unseen,
                'notifications' => $notifications,
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function postGetMessages()
    {
        try {
            $chatmodel = new ChatConversation;
            $count = $chatmodel->unread_count(Auth::user()->id);
            // $count = ChatConversationMessage::where('to_id', '=', Auth::user()->id)->where('is_read', '=', 0)->count();
            return ApiResponse::create(
                [
                    'count' => $count
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $referral_id
     * @return \Illuminate\Http\Response
     */
    public function getReferral($referral_id)
    {
        try {
            $get_ref = ReferralLinks::findOrFail($referral_id);

            $click = new ReferralLinksViews;
            $click->links_id = $get_ref->id;
            $click->users_id = Auth::user()->id;
            $click->gender = Auth::user()->gender;
            $click->nationality = Auth::user()->nationality;
            $click->save();

            return ApiResponse::create(
                [
                    'referral_link' => $get_ref['target'] . "?ref=" . $get_ref->id
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param trackRefRequest $request
     * @return \Illuminate\Http\Response
     */
    public function trackRef(trackRefRequest $request)
    {
        try {
            $ref = $request->get('ref');
            $new_ref = new ReferralLinksClicks;
            $new_ref->links_id = $ref;
            $new_ref->users_id = Auth::user()->id;
            $new_ref->gender = '';
            $new_ref->nationality = '';
            $new_ref->save();
            if ($new_ref) {
                return ApiResponse::create(
                    [
                        'new_ref' => $new_ref
                    ]
                );
            } else {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $postId
     * @return \Illuminate\Http\Response
     */
    public function getPostView($postId)
    {
        try {
            $post = Posts::find($postId);
            $place = [];
            if (isset($post->posts_place[0]['places_id'])) {
                $place = Place::find($post->posts_place[0]['places_id']);
            }

            return ApiResponse::create(
                [
                    'post' => $post,
                    'place' => $place,
                    'checkins' => @$post->checkin[0]
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SearchTripsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchTrips(SearchTripsRequest $request)
    {
        try {
            $query = $request->get('q');

            if ($query != '') {

                $all_places = TripPlans::where('title', 'LIKE', "%$query%")->get();
                $results = array();
                foreach ($all_places as $place) {
                    //value.id+','+value.trans[0].title+','+value.lat+","+value.lng+',Place
                    $results[] = array(
                        'id' => $place->id, 'text' => @$place->title
                    );
                }

                $data = array('results' => $results);
                return ApiResponse::create($data);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getEl()
    {
        try {
            $get_restaurants = Restaurants::whereRaw('id not in (SELECT restaurants_id FROM __elastic_restaurants_done)')
                ->take(200)
                ->get();

            $data = array();
            $returnData = [];
            foreach ($get_restaurants as $restaurant) {
                $data = array(
                    'type' => 'restaurant',
                    'id' => $restaurant->id,
                    'countries_id' => $restaurant->countries_id,
                    'cities_id' => $restaurant->cities_id,
                    'title' => @$restaurant->transsingle->title,
                    'address' => @$restaurant->transsingle->address,
                    'image' => @$restaurant->getMedias[0]->url,
                    'place_type' => @$restaurant->place_type,
                    'lat' => @$restaurant->lat,
                    'lng' => @$restaurant->lng,
                    'rating' => @$restaurant->rating,
                    'location' => @$restaurant->lat . "," . @$restaurant->lng
                );


                $json_data = json_encode($data);

                //dd($json_data);


                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://search-travoooel01-2cx3vi3poxo3xalglngsszutem.us-east-2.es.amazonaws.com/restaurants/restaurants/" . $restaurant->id);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $headers = [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($json_data)
                ];

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                if (!$result = curl_exec($ch)) {
                    trigger_error(curl_error($ch));
                }

                curl_close($ch);

                $result = json_decode($result);
                if ($result->result == 'created' or $result->result == 'updated') {
                    $returnData[] = $data;
                    DB::insert('insert into __elastic_restaurants_done (restaurants_id) values (?)', [$restaurant->id]);
                }
            }
            return ApiResponse::create($returnData);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getEl2()
    {
        try {
            $get_hotels = Hotels::whereRaw('id not in (SELECT hotels_id FROM __elastic_hotels_done)')
                ->take(200)
                ->get();

            $data = array();
            $returnData = array();
            foreach ($get_hotels as $hotel) {
                $data = array(
                    'type' => 'hotel',
                    'id' => $hotel->id,
                    'countries_id' => $hotel->countries_id,
                    'cities_id' => $hotel->cities_id,
                    'title' => @$hotel->transsingle->title,
                    'address' => @$hotel->transsingle->address,
                    'image' => @$hotel->getMedias[0]->url,
                    'place_type' => @$hotel->place_type,
                    'lat' => @$hotel->lat,
                    'lng' => @$hotel->lng,
                    'rating' => @$hotel->rating,
                    'location' => @$hotel->lat . "," . @$hotel->lng
                );

                $json_data = json_encode($data);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://search-travoooel01-2cx3vi3poxo3xalglngsszutem.us-east-2.es.amazonaws.com/hotels/hotels/" . $hotel->id);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $headers = [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($json_data)
                ];

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                if (!$result = curl_exec($ch)) {
                    trigger_error(curl_error($ch));
                }

                curl_close($ch);

                $result = json_decode($result);
                if ($result->result == 'created' or $result->result == 'updated') {
                    $returnData[] = $data;
                    DB::insert('insert into __elastic_hotels_done (hotels_id) values (?)', [$hotel->id]);
                }
            }
            return ApiResponse::create($returnData);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getEl3()
    {
        try {
            $get_places = Place::whereRaw('id not in (SELECT places_id FROM __elastic_places_done)')
                ->take(200)
                ->get();

            $data = array();
            $returnData = [];
            foreach ($get_places as $place) {
                $data = array(
                    'type' => 'place',
                    'id' => $place->id,
                    'countries_id' => $place->countries_id,
                    'cities_id' => $place->cities_id,
                    'title' => @$place->transsingle->title,
                    'address' => @$place->transsingle->address,
                    'image' => @$place->getMedias[0]->url,
                    'place_type' => @$place->place_type,
                    'lat' => @$place->lat,
                    'lng' => @$place->lng,
                    'rating' => @$place->rating,
                    'location' => @$place->lat . "," . @$place->lng
                );

                $json_data = json_encode($data);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://search-travoooel01-2cx3vi3poxo3xalglngsszutem.us-east-2.es.amazonaws.com/places/places/" . $place->id);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $headers = [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($json_data)
                ];

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                if (!$result = curl_exec($ch)) {
                    trigger_error(curl_error($ch));
                }

                curl_close($ch);

                $result = json_decode($result);
                if ($result->result == 'created' or $result->result == 'updated') {
                    $returnData[] = $data;
                    DB::insert('insert into __elastic_places_done (places_id) values (?)', [$place->id]);
                }
            }
            return ApiResponse::create($returnData);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    // Helpers
    function getElSearchResult($query)
    {
        $curl = curl_init();
        $query = urlencode($query);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/places/_search?q=$query&pretty=true&size=50",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $response_array = [];
        if ($err) {
            return [];
        } else {
            $response = json_decode($response, true);
            if (isset($response['hits']) && count($response['hits']['hits']) != 0) {
                foreach ($response['hits']['hits'] as $hits) {
                    $response_array[] = $hits['_source'];
                }
                return [
                    'result' => $response_array,
                    'total' => $response['hits']['total']
                ];
            } else {
                return [
                    'result' => [],
                    'total' => 0
                ];
            }
        }
    }

    function findFriendFlag($user_id)
    {
        // follower or not
        $isMyFollower = auth()->user()->get_followers->where('followers_id', $user_id)->count();
        // following or not
        $iAmFollowing = auth()->user()->following_user->where('users_id', $user_id)->count();
        return [
            'follower'  => ($isMyFollower == 1) ? true : false,
            'following' => ($iAmFollowing == 1) ? true : false,
        ];
    }
}
