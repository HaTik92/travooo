<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Http\Constants\CommonConst;
use Illuminate\Http\Request;
use App\Http\Responses\ApiResponse;
use Illuminate\Support\Facades\Storage;
use App\Services\Ranking\RankingService;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsInfos;
use App\Models\Reports\ReportsDraft;
use App\Models\Reports\ReportsDraftInfos;
use App\Models\Reports\ReportsLikes;
use App\Models\Reports\ReportsComments;
use App\Services\Api\PrimaryPostService;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Reports\ReportsShares;
use App\Models\User\User;
use App\Models\ActivityMedia\Media;
use App\Models\Reports\ReportsCommentsMedias;
use App\Models\User\UsersMedias;
use App\Models\UsersFollowers\UsersFollowers;
use App\Models\Place\Place;
use App\Http\Requests\Api\Reports\SearchUserRequest;
use App\Http\Requests\Api\Reports\ReportListRequest;
use App\Http\Requests\Api\Reports\CreateEditFirstStepRequest;
use App\Http\Requests\Api\Reports\UploadCoverRequest;
use App\Http\Requests\Api\Reports\CreateEditSecondStepRequest;
use App\Http\Requests\Api\Reports\SearchReportPlanRequest;
use App\Http\Requests\Api\Reports\CommentRequest;
use App\Http\Requests\Api\Reports\PostShareRequest;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\TripPlans\TripPlans;
use App\Models\Country\ApiCountry as Country;
use App\Models\Reports\ReportsViews;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\Reports\ReportsCommentsLikes;
use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Posts\Checkins;
use App\Models\Reviews\Reviews;
use App\Services\Trips\TripInvitationsService;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\Traits\ReportCreation;
use App\Http\Requests\Api\Reports\NewCreateEditSecondStepRequest;
use App\Models\Place\PlaceFollowers;
use App\Models\Posts\PostsShares;
use App\Models\Reports\ReportsLocation;
use App\Models\User\UsersContentLanguages;
use App\Services\Api\ShareService;
use App\Services\Reports\ReportsService;
use App\Services\Translations\TranslationService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{

    const S3_BASE_URL = "https://s3.amazonaws.com/travooo-images2/";

    use ReportCreation;

    function __construct()
    {
        if (in_array(request()->route()->uri, optionalAuthRoutes(self::class))) {
            if (request()->bearerToken()) {
                $this->middleware('auth:api');
            }
        }
        DB::enableQueryLog();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    public function getDraftByID(Request $request)
    {
        try {
            $id = $request->get("id");
            $data = [];
            if ($id == "0") {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            } else {
                $report_info = ReportsInfos::where('reports_id', $id)->get();
                if (is_object($report_info) > 0) {
                    foreach ($report_info as $info) {
                        if ($info->var == "user") {
                            $users = explode(",", $info->val);
                            $usersdata = [];
                            foreach ($users as $user) {
                                $usermodel = User::find($user);

                                $user_info = array();

                                $user_info['id'] = $usermodel->id;
                                $user_info['text'] = $usermodel->name;
                                $user_info['country'] = ($usermodel->nationality != '') ? @$usermodel->country_of_nationality->transsingle->title : '';
                                $user_info['profile_picture'] = check_profile_picture($usermodel->profile_picture);
                                $user_info['cover_photo'] = check_cover_photo($usermodel->cover_photo);
                                $user_info['media'] = [];

                                $usermedias = $usermodel->my_medias()->pluck('medias_id');

                                $photos_list  = Media::select('id')->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
                                $medias = UsersMedias::where('users_id', $usermodel->id)
                                    ->whereIn('medias_id', $photos_list)
                                    ->orderBy('id', 'DESC')
                                    ->limit(3)->get();
                                if ($medias) {
                                    foreach ($medias as $media) {
                                        $user_info['media'][] = get_profile_media($media->media->url);
                                    }
                                }
                                $user_follow = UsersFollowers::where('users_id', $usermodel->id)
                                    ->where('followers_id', Auth::user()->id)
                                    ->first();
                                if ($user_follow) {
                                    $follow = "unfollow";
                                    $follow_class = "btn-light-grey";
                                } else {
                                    $follow = "follow";
                                    $follow_class = "btn-blue-follow";
                                }
                                $user_info['follow'] = $follow;


                                $usersdata[] = [$usermodel->id, $usermodel->name, $user_info];
                            }
                            $info['val'] = $usersdata;
                        } else if ($info->var == "place") {
                            $place = Place::find($info->val);
                            $place_info = array();

                            if (is_object($place)) {
                                $place_info[] = [
                                    'id' => $place->id,
                                    'title' => @$place->transsingle->title,
                                    'image' => (isset($place->getMedias[0]->url)) ? S3_BASE_URL . 'th1100/' . $place->getMedias[0]->url : url(PLACE_PLACEHOLDERS),
                                    'type' => do_placetype(@$place->place_type),
                                    'address' => @$place->transsingle->address,
                                    'rating' => @$place->rating,
                                    'visited_list' => $this->_getPlaceReviews($place),
                                    'place_info' => ['reaview_count' => (isset($place->reviews) ? count(@$place->reviews) : 0), 'place_city' => @$place->city->transsingle->title . ', ' . @$place->country->transsingle->title],
                                ];
                            }

                            $info['val'] = [$info->val, @$place->transsingle->title, $place_info];
                        }
                    }
                    return ApiResponse::create(
                        [
                            'report_info' => $report_info
                        ]
                    );
                } else {
                    return ApiResponse::__createBadResponse('Report Info not found');
                }
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param object $place
     * @return object
     */
    protected function _getPlaceReviews($place)
    {
        $visited_user_count = 0;
        $visited_list = [];

        if (isset($place->reviews)) {
            foreach ($place->reviews()->orderBy('created_at', 'desc')->groupBy('by_users_id')->get() as $reviews) {
                if ($reviews->author) {
                    $visited_user_count++;
                    if ($visited_user_count < 4) {
                        $visited_list['user_profile'] = check_profile_picture($reviews->author->profile_picture);
                    }
                }
            }
            if ($visited_user_count <= 3) {
                $visited_listp['count'] = $visited_user_count;
            } else {
                $visited_listp['count'] = ($visited_user_count - 3);
            }
        } else {
            $visited_listp['count'] = $visited_user_count;
        }

        return $visited_list;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getInfoByID(Request $request)
    {
        try {
            $id = $request->get("id");
            $data = [];
            if ($id == "0") {
                return ApiResponse::create($data);
            } else {
                $report = Reports::where('users_id', Auth::user()->id)->find($id);
                if (!$report) {
                    return ApiResponse::__createBadResponse('Report not found');
                }

                if (is_object($report) > 0) {
                    $locations = [];

                    if ($report->countries_id) {
                        $country_ids = explode(',', $report->countries_id);
                        foreach ($country_ids as $countries_id) {
                            $country_info = Countries::find($countries_id);
                            $locations[] = [
                                'id' => $countries_id . '-country',
                                'text' => @$country_info->transsingle->title,
                                'info' => [
                                    'id' => $countries_id,
                                    'title' => @$country_info->transsingle->title,
                                    'country_id' => @$country_info->country->id,
                                    'country_name' => @$country_info->country->transsingle->title
                                ]
                            ];
                        }
                    }
                    if ($report->cities_id) {
                        $city_ids = explode(',', $report->cities_id);
                        foreach ($city_ids as $cities_id) {
                            $city_info = Cities::find($cities_id);
                            $locations[] = [
                                'id' => $cities_id . '-city',
                                'text' => @$city_info->transsingle->title,
                                'info' => [
                                    'id' => $cities_id,
                                    'title' => @$city_info->transsingle->title,
                                    'country_id' => @$city_info->country->id,
                                    'country_name' => @$city_info->country->transsingle->title
                                ]
                            ];
                        }
                    }

                    $data["title"]              = $report->title;
                    $data["description"]        = $report->description;
                    $data["locations"]          = $locations;
                    $data["step1_report_id"]    = $report->id;

                    return ApiResponse::create($data);
                } else {
                    return ApiResponse::create($data);
                }
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/get-country",
     *   tags={"Travelogs"},
     *   summary="Get country info",
     *   operationId="get_country_info",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="countries_id",
     *        description="Country Id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getCountryByID(Request $request)
    {
        try {
            $country_id = $request->get('countries_id');
            $country = Countries::find($country_id);
            if (!$country) return ApiResponse::__createBadResponse('Country not found');
            return ApiResponse::create(preparedReportCountryDetails($country));
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/get-city",
     *   tags={"Travelogs"},
     *   summary="Get city info",
     *   operationId="get_city_info",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="cities_id",
     *        description="City Id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getCityByID(Request $request)
    {
        try {
            $city_id = $request->get('cities_id');
            $city = Cities::findOrFail($city_id);

            if (!($city && isset($city->trans[0]))) {
                return ApiResponse::__createBadResponse('City not found');
            }
            return ApiResponse::create(preparedReportCityDetails($city));
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postGeocodeCountry(Request $request)
    {
        try {
            $country_id = $request->get('country_id');
            $country = Countries::find($country_id);
            if ($country) {
                $loc = array('lat' => $country->lat, 'lng' => $country->lng);
                return ApiResponse::create($loc);
            } else {
                return ApiResponse::__createBadResponse('Country not found');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postGeocodeCity(Request $request)
    {
        try {
            $city_id = $request->get('city_id');
            $city = Cities::find($city_id);

            if ($city) {
                $loc = array('lat' => $city->lat, 'lng' => $city->lng);
                return ApiResponse::create($loc);
            } else {
                return ApiResponse::__createBadResponse('City not found');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function searchMentionUser(Request $request)
    {
        try {
            $logged_user_id = Auth::user()->id;
            $query = $request->get('q');
            if ($query) {
                $users = array();
                $i = 0;
                $get_users = User::where('name', 'like', '%' . $query . '%')->where('id', '!=', $logged_user_id)->take(30)->get();
                foreach ($get_users as $u) {
                    $users[$i]['id'] = $u->id;
                    $users[$i]['value'] = $u->name;
                    $users[$i]['link'] = "/profile/" . $u->id;
                    $users[$i]['image'] = check_profile_picture($u->profile_picture);
                    $users[$i]['nationality'] = ($u->nationality != '') ? @$u->country_of_nationality->transsingle->title : '';
                    $i++;
                }
                return ApiResponse::create($users);
            } else {
                return ApiResponse::createValidationResponse([
                    'q' => 'Search Term not provided'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }



    /**
     * @OA\Get(
     ** path="/reports/get-location-info/{location_id}",
     *   tags={"Travelogs"},
     *   summary="Get info about location",
     *   operationId="info_about_location",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="location_id",
     *        description="Location id id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getLocationInfo($location_id, Request $request)
    {
        try {
            if ($location_id) {
                $loc_info = array();
                $location_ids = explode('-', $location_id);

                $location = null;
                if ($location_ids[1] == 'country') {
                    $location = Countries::find($location_ids[0]);
                } else {
                    $location = Cities::find($location_ids[0]);
                }
                if (!$location) {
                    return ApiResponse::__createBadResponse('location not found');
                }

                $loc_info = [
                    'selected_id' => $location_id,
                    'id' => $location->id,
                    'title' => $location->transsingle->title,
                    'country_id' => @$location->country->id,
                    'country_name' => @$location->country->transsingle->title
                ];

                return ApiResponse::create($loc_info);
            } else {
                return ApiResponse::createValidationResponse([
                    'location_id' => 'Location not provided'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/get-location-list/{report_id}",
     *   tags={"Travelogs"},
     *   summary="Get Location List",
     *   operationId="get_location_list",
     *   description="Get locations list by report id",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="report_id",
     *        description="Report id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getLocationList($report_id, Request $request)
    {
        try {
            if ($report_id) {
                $locations = array();
                $report = Reports::find($report_id);
                if (!$report) {
                    return ApiResponse::__createBadResponse('Report not found');
                }

                $fetchLocations = $report->prettyLocation();

                if ($fetchLocations['total']) {
                    collect($fetchLocations[Reports::LOCATION_CITY])
                        ->each(function ($city) use (&$locations) {
                            $locations[] = [
                                'id' => $city->id,
                                'link' => url('city/' . $city->id),
                                'title' => $city->transsingle->title,
                                'image' => check_country_photo(@$city->getMedias[0]->url, 180),
                                'country_id' => $city->country->id,
                                'country_name' => $city->country->transsingle->title,
                                'country_image' => check_country_photo(@$city->country->getMedias[0]->url, 180),
                            ];
                        });
                    collect($fetchLocations[Reports::LOCATION_COUNTRY])
                        ->each(function ($country) use (&$locations) {
                            $locations[] = [
                                'id' => $country->id,
                                'link' => url('country/' . $country->id),
                                'title' => $country->transsingle->title,
                                'image' => check_country_photo(@$country->getMedias[0]->url, 180),
                                'country_id' => '',
                                'country_image' => ''
                            ];
                        });
                }

                return ApiResponse::create($locations);
            } else {
                return ApiResponse::createValidationResponse([
                    'report_id' => 'Report Id not provided'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function searchCountry(Request $request)
    {
        try {
            $query = $request->get('q');
            if ($query) {
                $countries = array();
                $i = 0;
                $queryParam = $query;
                $get_countries = Countries::whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })->get();
                foreach ($get_countries as $c) {
                    $countries[$i]['id'] = $c->id;
                    $countries[$i]['text'] = $c->transsingle->title;
                    $i++;
                }
                return ApiResponse::create([
                    'results' => $countries
                ]);
            } else {
                return ApiResponse::createValidationResponse([
                    'q' => 'Search Term not provided'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function searchCity(Request $request)
    {
        try {
            $query = $request->get('q');
            if ($query) {
                $cities = array();
                $i = 0;
                $queryParam = $query;
                $get_cities = Cities::whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })->get();
                foreach ($get_cities as $c) {
                    $cities[$i]['id'] = $c->id;
                    $cities[$i]['text'] = $c->transsingle->title;
                    $cities[$i]['ob'] = $c;
                    $i++;
                }
                return ApiResponse::create(
                    [
                        'results' => $cities
                    ]
                );
            } else {
                return ApiResponse::createValidationResponse([
                    'q' => 'Search Term not provided'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @param string $name
     * @return string
     */
    protected function _getPlaceCountryInfo($name)
    {
        $country = Countries::with('transsingle')->with('region.transsingle')
            ->with('getMedias')
            ->whereHas('transsingle', function ($query) use ($name) {
                $query->where('title', 'like', "%" . $name . "%");
            })
            ->first();

        if ($country) {
            return $name . '-:' . $country->iso_code . '-:' . str_replace(',', '.', $country->transsingle->population) . '-:' . $country->id;
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/get-user-info",
     *   tags={"Travelogs"},
     *   summary="Get user info",
     *   operationId="get_user_info",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="id",
     *        description="User id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getUserInfo(Request $request)
    {
        try {
            if ($request->id) {
                $user_id = $request->id;
                $user_info = array();
                $user = User::find($user_id);
                if (!$user) {
                    return ApiResponse::__createBadResponse("User not found");
                }
                $user_info['id'] = $user_id;
                $user_info['text'] = $user->name;
                $user_info['country'] = ($user->nationality != '') ? @$user->country_of_nationality->transsingle->title : '';
                $user_info['profile_picture'] = check_profile_picture($user->profile_picture);
                $user_info['cover_photo'] = check_cover_photo($user->cover_photo);
                $user_info['media'] = [];

                $usermedias = $user->my_medias()->pluck('medias_id');

                $photos_list  = Media::select('id')->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
                $medias = UsersMedias::where('users_id', $user_id)
                    ->whereIn('medias_id', $photos_list)
                    ->orderBy('id', 'DESC')
                    ->limit(3)->get();
                if ($medias) {
                    foreach ($medias as $media) {
                        $user_info['media'][] = get_profile_media($media->media->url);
                    }
                }
                $user_follow = UsersFollowers::where('users_id', $user_id)
                    ->where('followers_id', Auth::user()->id)
                    ->first();
                if ($user_follow) {
                    $follow = "unfollow";
                } else {
                    $follow = "follow";
                }
                $user_info['follow'] = $follow;
                return ApiResponse::create($user_info);
            } else {
                return ApiResponse::create([], false, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Get(
     ** path="/reports",
     *   tags={"Travelogs"},
     *   summary="[Login optional] : Get Travelogs : https://app.zeplin.io/project/5e8dc98975eb98226f385503/screen/5ff6e82b81a6e2707d5a44bc",
     *   operationId="app\Http\Controllers\Api\ReportsController@getList",
     *  @OA\Parameter(
     *      name="per_page",
     *      description="per page items | default : 10",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="integer"
     *      )
     * ),
     * @OA\Parameter(
     *      name="page",
     *      description="pass page no | default : 1",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="integer"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="order",
     *      description="Filter => order values [new, old, popular, my_draft] | default : new",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="type",
     *      description="my | all | default : all",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="search_text",
     *      description="Question Tab => Search query",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * @OA\Parameter(
     *        name="cities",
     *        description="comma separated",
     *        in="query",
     *        @OA\Schema(
     *           type="string",
     *        ),
     *     ),
     *  @OA\Parameter(
     *        name="countries",
     *        description="comma separated",
     *        in="query",
     *        @OA\Schema(
     *           type="string",
     *        ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param ReportListRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
        try {
            $validator  = \Validator::make($request->all(), [
                'countries'     => 'sometimes|string',
                'cities'        => 'sometimes|string',
                'type'          => 'sometimes|string',
                'search_text'   => 'sometimes|string',
                'order'         => 'sometimes|string',
            ]);
            if ($validator->fails()) {
                return ApiResponse::create($validator->errors(), false, ApiResponse::VALIDATION);
            }

            if ($request->has('langugae_id')) {
                $langugae_id = (int) $request->langugae_id;
            } else {
                $langugae_id = getDesiredLanguage();
                if (isset($langugae_id[0])) {
                    $langugae_id = $langugae_id[0];
                } else {
                    $langugae_id = UsersContentLanguages::DEFAULT_LANGUAGES;
                }
            }

            $countries             = collect(explode(",", trim(trim($request->get('countries', ''), ","))))->filter(function ($val) {
                return $val != null || $val != "";
            })->toArray();
            $cities             = collect(explode(",", trim(trim($request->get('cities', ''), ","))))->filter(function ($val) {
                return $val != null || $val != "";
            })->toArray();
            $search             = trim($request->get('search_text', ''));
            $order              = trim($request->get('order', 'popular'));
            $base_filter        = trim($request->get('type', 'all'));
            $per_page           = $request->get('per_page', 10);
            $authUser           = auth()->user();
            $page               = $request->page ?? 1;
            $skip               = ($page - 1) * $per_page;

            $per_page           = (int) $per_page;
            $page               = (int) $page;


            if (!$authUser && ($base_filter == "my" || $order == 'my_draft')) {
                return ApiResponse::__createBadResponse("login must be required for showing your reports.");
            }

            $baseQuery = Reports::query()
                ->where('language_id', $langugae_id)
                ->whereNotIn('users_id', blocked_users_list())
                ->when(strlen($search), function ($q) use ($search) {
                    return $q->where(function ($s_q) use ($search) {
                        $s_q->where('title', 'like', "%" . $search . "%");
                        $s_q->orWhere('description', 'like', "%" . $search . "%");
                    });
                });
            $with = ['author', 'cover'];
            if ($order == 'new') {
                $baseQuery->whereNotNull('published_date')->orderBy('published_date', 'DESC');
            } elseif ($order == 'old') {
                $baseQuery->whereNotNull('published_date')->orderBy('published_date', 'ASC');
            } elseif ($order == 'popular') {
                $baseQuery->withCount(['likes', 'allComments', 'postShares'])
                    ->whereNotNull('published_date')
                    ->groupBy('reports.id')
                    ->orderByDesc(DB::raw('likes_count + all_comments_count + post_shares_count'));
            } elseif ($order == 'my_draft') {
                $with = ['author', 'draft_report', 'draft_report.cover'];
                $baseQuery->where('flag', 0)->where('users_id', auth()->id())->orderBy('updated_at', 'DESC');
            }

            $baseQuery->when(count($countries) && count($cities), function ($q) use ($countries, $cities) {
                $q->whereHas('location', function ($_q) use ($countries, $cities) {
                    $_q->where(function ($query) use ($countries) {
                        $query->where('location_type', Reports::LOCATION_COUNTRY)
                            ->whereIn('location_id', $countries);
                    });
                    $_q->orWhere(function ($query) use ($cities) {
                        $query->where('location_type', Reports::LOCATION_CITY)
                            ->whereIn('location_id', $cities);
                    });
                });
            }, function ($q) use ($countries, $cities) {
                if (count($countries)) {
                    $q->whereHas('location', function ($_q) use ($countries) {
                        $_q->where('location_type', Reports::LOCATION_COUNTRY)
                            ->whereIn('location_id', $countries);
                    });
                } elseif (count($cities)) {
                    $q->whereHas('location', function ($_q) use ($cities) {
                        $_q->where('location_type', Reports::LOCATION_CITY)
                            ->whereIn('location_id', $cities);
                    });
                }
            });

            if ($base_filter == 'my' && $order != 'my_draft') {
                $baseQuery->where('flag', 1)->where('users_id', auth()->id());
            }
            $baseQuery->whereHas('author');

            $total_reports = DB::table(DB::raw('(' . _getDBQuery($baseQuery) . ') as a'))->count();
            // $total_reports = (clone $baseQuery)->get()->count();
            $reports = (clone $baseQuery)->with($with)->skip($skip)->take($per_page)->get();

            $has_more  = true;
            if (count($reports) == 0) {
                if ($langugae_id == UsersContentLanguages::DEFAULT_LANGUAGES) {
                    $has_more  = false;
                } else {
                    $newRequest = new Request;
                    $params = $request->all();
                    $params['langugae_id'] = UsersContentLanguages::DEFAULT_LANGUAGES;
                    $newRequest->merge($params);
                    return $this->getList($newRequest);
                }
            }

            $_reports = [];
            $reports = $reports->each(function (&$report) use ($authUser, $order, &$_reports) {
                $report->draft_flag = false;
                $report->is_my_report = ($authUser) ? ($report->users_id == $authUser->id) : false; // for login user
                $report->author->profile_picture = check_profile_picture($report->author->profile_picture);

                // Add Likes & Share Flag
                $report->like_flag   = $authUser ? ($report->likes->where('users_id', $authUser->id)->first() ? true : false) : false;
                $report->total_liked = $report->likes->count();
                $report->total_shares =  PostsShares::where('type', ShareService::TYPE_REPORT)->where('posts_type_id', $report->id)->count();

                $_report = $report;
                if ($order == 'my_draft') {
                    $_report = $report->draft_report;

                    $report->title = $_report->title;
                    $report->description = $_report->description;
                    $report->countries_id = $_report->countries_id;
                    $report->cities_id = $_report->cities_id;

                    $report->draft_flag = true;
                }

                $cover = asset(HOTEL_NO_PHOTO);
                if (isset($_report->cover[0]->val)) $cover = $_report->cover[0]->val;
                unset($report->cover);
                $report->cover = $cover;

                $flagsList = [];
                $prettyLocation = $_report->prettyLocation();
                collect($prettyLocation[REPORTS::LOCATION_COUNTRY])->each(function ($country) use (&$flagsList) {
                    $flagsList[] = [
                        'title' => _fieldValueFromTrans($country, 'title'),
                        'flag' => get_country_flag($country),
                    ];
                });
                collect($prettyLocation[REPORTS::LOCATION_CITY])->each(function ($city) use (&$flagsList) {
                    $country = $city->country;
                    if ($country) {
                        $flagsList[] = [
                            'title' => _fieldValueFromTrans($country, 'title'),
                            'flag' => @get_country_flag($country),
                        ];
                    }
                });
                $report->flags_list = $flagsList;

                unset($report->likes, $report->draft_report, $report->location);
                $_reports[] = $report;
            });


            $feeds = [];
            foreach ($_reports as $report) {
                if ($page >= 2 && (($page * 10 - 5) % 15 == 0 || $page % 3 == 0)) {
                    $trendingDestinations = $this->trendingDestinations($page - 1);
                    if (count($trendingDestinations)) {
                        $feeds[] =  [
                            'unique_link'   => null,
                            'type'          => PrimaryPostService::SECONDARY_TRENDING_DESTINATIONS,
                            'action'        => null,
                            'data'          => $trendingDestinations
                        ];
                    }
                }

                $feeds[] =  [
                    'unique_link'   => url("reports/" . $report->id),
                    'type'          => strtolower(PrimaryPostService::TYPE_REPORT),
                    'action'        => 'create',
                    'data'          => $report
                ];
            }

            $response['counters']['cities'] = DB::select("select count(*) as total from (SELECT location_id  FROM `reports_locations` WHERE location_type=? group by location_id) as city", [REPORTS::LOCATION_CITY])[0]->total;
            $response['counters']['countries'] = DB::select("select count(*) as total from (SELECT location_id  FROM `reports_locations` WHERE location_type=? group by location_id) as country", [REPORTS::LOCATION_COUNTRY])[0]->total;
            $response['counters']['all'] = $response['counters']['countries'] + $response['counters']['cities'];

            // if (count($feeds)) shuffle($feeds);

            $response['is_authorized_user'] = $authUser ? true : false;
            $response['total']              = $total_reports;
            $response['per_page']           = $per_page;
            $response['has_more']           = $has_more;
            $response['langugae_id']        = $langugae_id;
            // $response['total_page']         = ceil($total_reports / $per_page);
            $response['current_page']       = $page;
            $response['data']               = $feeds;

            return ApiResponse::create($response);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/top-places",
     *   tags={"Travelogs"},
     *   summary="[Login optional] : Get Top Place Slider",
     *   operationId="app\Http\Controllers\Api\ReportsController@topPlaces",
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function topPlaces(ReportsService $reportsService)
    {
        try {
            $location_ids = $reportsService->getTrendingLocations(true);
            $is_active = true;

            if (count($location_ids) == 0) {
                $location_ids = $reportsService->getTrendingLocations(false);
                $is_active = false;
            }

            $locations = [];
            foreach ($location_ids as $ids) {
                if ($ids->type == "country") {
                    $countryDetails = Countries::with('transsingle')->where('id', $ids->location_id)->first();
                    if ($countryDetails) {
                        $countryDetails["title"] = @$countryDetails->transsingle->title;
                        $countryDetails["image"] = check_country_photo(@$countryDetails->getMedias[0]->url, 180);
                        unset($countryDetails->getMedias);
                        $popular = $is_active ? $reportsService->getLocationCount($ids->location_id) : $ids->rank;
                        $locations[] = [
                            'ob' => $countryDetails,
                            'type' => 'country',
                            'report_count' => $popular,
                            'popular' => $popular
                        ];
                    }
                } elseif ($ids->type == "city") {
                    $cityDetails = Cities::with('transsingle')->where('id', $ids->location_id)->first();
                    if ($cityDetails) {
                        $cityDetails["title"] = @$cityDetails->transsingle->title;
                        $cityDetails["image"] = check_city_photo(@$cityDetails->getMedias[0]->url);
                        unset($cityDetails->getMedias);
                        $popular = $is_active ? $reportsService->getLocationCount($ids->location_id) : $ids->rank;
                        $locations[] = [
                            'ob' => $cityDetails,
                            'type' => 'city',
                            'report_count' => $popular,
                            'popular' => $popular,
                        ];
                    }
                }
            }

            if (count($locations)) {
                $locations = array_values(collect($locations)->sortByDesc('popular')->toArray());
            }

            return ApiResponse::create([
                'type' => "top_locations",
                'action' => null,
                'data' => $locations
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/{id}",
     *   tags={"Travelogs"},
     *   summary="[Login optional] : Show report by id",
     *   operationId="get_show_report",
     *    @OA\Parameter(
     *        name="id",
     *        description="Report id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     **/
    /**
     * @param integer $report_id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getShow(Request $request, $report_id)
    {
        try {
            $authUser       = Auth::user();
            $isAuthUser     = Auth::check();
            $isDraftView    = false;
            $isMyReport     = false;
            $cities         = collect([]);
            $countries      = collect([]);

            $report = Reports::with('draft_report', 'author')
                ->where('id', $report_id)->first();
            if (!$report) return ApiResponse::__createBadResponse("report not found.");

            if (!empty($report->deleted_at)) return ApiResponse::__createBadResponse("content-deleted");

            if ($isAuthUser && (($authUser->id !== $report->users_id && $report->published_date == NULL) || (user_access_denied($report->users_id, true)))) {
                return ApiResponse::__createNoPermissionResponse("access denied");
            }

            if ($isAuthUser) {
                $isMyReport     = $report->users_id == $authUser->id;
                $isDraftView    = $isMyReport;

                $reportView = new ReportsViews;
                $reportView->reports_id = $report->id;
                $reportView->users_id = $authUser->id;
                $reportView->save();
            }

            // Add draft_flag
            $report->draft_flag = !($report->flag && $report->published_date != NULL);

            $draft_report = $report->draft_report;
            unset($report->draft_report);

            if (!$draft_report) return ApiResponse::__createBadResponse("report not found");
            $report->draft_report_id = $draft_report->id;

            $_rep = $draft_report;

            if ($isDraftView) { // change report data with draft data for author view

                $reportsInfos = ReportsDraftInfos::query()->where('reports_draft_id', $draft_report->id);

                $report->title = $draft_report->title;
                $report->description = $draft_report->description;
            } else {
                $_rep = $report;
                $reportsInfos = ReportsInfos::query()->where('reports_id', $report->id);
            }

            $fetchLocations = $_rep->prettyLocation();
            if ($fetchLocations['total'] > 1) {
                $right_info = ['is_multiple' => true, 'data' => api_get_report_locations_info($fetchLocations)];
            } else {
                $right_info = ['is_multiple' => false];
                if (count($fetchLocations[Reports::LOCATION_COUNTRY])) {
                    $right_info['data'][] = [
                        'key' => trans('region.country_capital'),
                        'value' =>  @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->capitals[0]->city->transsingle->title,
                    ];
                    $right_info['data'][] = [
                        'key' => trans('region.population'),
                        'value' =>   @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->transsingle->population,
                    ];
                    $right_info['data'][] = [
                        'key' => "Currency",
                        'value' => @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->currencies[0]->transsingle->title,
                    ];
                } elseif (count($fetchLocations[Reports::LOCATION_CITY])) {
                    $right_info['data'][] = [
                        'key' => trans('region.country_capital'),
                        'value' =>  @collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->title,
                    ];
                    $right_info['data'][] = [
                        'key' => trans('region.population'),
                        'value' =>   @collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->population,
                    ];
                    $right_info['data'][] = [
                        'key' => "Currency",
                        'value' => @collect($fetchLocations[Reports::LOCATION_CITY])->first()->country->currencies[0]->transsingle->title,
                    ];
                } else {
                    $right_info['data'] = [];
                }
            }
            $report->right_info = $right_info;


            $cover = asset(HOTEL_NO_PHOTO);
            $reportsInfos = $reportsInfos->orderBy('order', 'ASC')->get()->each(function ($info) use ($isDraftView, $authUser, $isAuthUser, &$cover) {
                if ($isDraftView) {
                    unset($info->reports_draft_id);
                } else {
                    unset($info->type, $info->language_id, $info->reports_id);
                }

                $info->var = trim(strtolower($info->var));
                switch ($info->var) {
                    case ReportsService::VAR_PLACE:
                        $p = Place::find($info->val);
                        $medium['title'] = @$p->trans[0]->title;
                        $medium['address'] = @$p->trans[0]->address;
                        $medium['location'] = $p->lat . "," . $p->lng;
                        $medium['lat'] = $p->lat;
                        $medium['lng'] = $p->lng;
                        $medium['place_type'] = do_placetype($p->place_type);

                        $medium['image'] = asset(PLACE_PLACEHOLDERS);
                        if (isset($r['place_media']) && $r['place_media'] != null) {
                            $medium['image'] = check_media_url($r['place_media']);
                        }

                        $medium['city_id'] = $p->cities_id;
                        $medium['city_title'] = @$p->city->transsingle->title;
                        $medium['country_id'] = $p->countries_id;
                        $medium['country_title'] = @$p->country->transsingle->title;

                        $medium['ratting'] = Reviews::where('places_id', $p->id)->avg('score') ?? 0;
                        $medium['total_reviews'] = Reviews::where('places_id', $p->id)->count();
                        $medium['recent_reviews'] = Reviews::where('places_id', $p->id)->take(2)->get()->each(function ($placeReviewer) {
                            $placeReviewer->author;
                        });
                        $medium['total_followers'] = PlaceFollowers::where('places_id', $p->id)->count();
                        $medium['recent_followers'] = PlaceFollowers::where('places_id', $p->id)->take(4)->get()->map(function ($placeFollower) {
                            return [
                                'id' => $placeFollower->users_id,
                                'profile_picture' => check_profile_picture($placeFollower->user->profile_picture),
                            ];
                        });
                        $info->data = $medium;
                        break;

                    case ReportsService::VAR_COVER:
                        $cover = $info->val;
                        break;

                    case ReportsService::VAR_INFO:
                        $_data['id'] = $info->val;
                        $_data['html'] = view('react-native.report.info-element.show', [
                            'info' => $info
                        ])->render();
                        $_data['added_flag'] = true;
                        $info->data = $_data;
                        break;

                    case ReportsService::VAR_MAP:
                        $temp = @explode("-:", $info->val);
                        $__ =  [
                            'id' => $info->val,
                            'text' => @$temp[4],
                            'lat' =>  @$temp[0],
                            'lng' => @$temp[1],
                            'zoom' => (strpos($info->val, "-:12-:")) ? 12 : ((strpos($info->val, "-:8-:")) ? 8 : 5),
                            'image' => "https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/" . strtolower(@$temp[5]) . ".png",
                            'type' => (strpos($info->val, "-:12-:")) ? 'Place' : ((strpos($info->val, "-:8-:")) ? 'City' : 'Country'),
                            'type_desc' => '',
                            'val' => $info->val,
                            'population' => @$temp[6],
                            'country_id' => @$temp[7],
                            'country_follow_flag' => get_country_follow(@$temp[7]),
                        ];
                        $info->data = $__;
                        break;

                    case ReportsService::VAR_LOCAL_VIDEO:
                    case ReportsService::VAR_PHOTO:
                        $_ = @explode("-:", $info->val);
                        $_data['url']   = @$_[0];
                        $_data['desc']  = @$_[1];
                        $_data['link']  = @$_[2];
                        $info->data = $_data;
                        break;

                    case ReportsService::VAR_USER:
                        $info->data  = [];
                        if (strlen(trim($info->val))) {
                            $info->data = User::query()
                                ->select('id', 'name', 'username', 'email', 'cover_photo', 'profile_picture')
                                ->whereIn('id', explode(",", $info->val))
                                ->get()->each(function (&$user) use ($authUser, $isAuthUser) {
                                    $user->follow_flag = ($isAuthUser) ? UsersFollowers::where('users_id', $user->id)->where('followers_id', $authUser->id)->exists() : false;
                                    $user->cover_photo  = check_cover_photo($user->cover_photo);
                                    $user->profile_picture = check_profile_picture($user->profile_picture);
                                    $user->media = Media::select('url')
                                        ->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4')
                                        ->where('users_id', $user->id)->take(3)->pluck('url')
                                        ->map(function (&$url) {
                                            return check_media_url($url);
                                        });
                                });
                        }
                        break;

                    case ReportsService::VAR_PLAN:
                        $info->data = travelogTripObject((int)$info->val);
                        break;
                }
            });
            $report->cover_url =  $cover;
            $report->is_my_report =  $isMyReport;

            // Add All Location (step 1)

            $report->locations = _createLocationObjectWithFullDetailsForReport($fetchLocations[REPORTS::LOCATION_CITY], $fetchLocations[REPORTS::LOCATION_COUNTRY]);

            // Add author
            if ($report->author) {
                $report->author->profile_picture = check_profile_picture($report->author->profile_picture);
                if ($isAuthUser) {
                    $report->author->follow_flag = UsersFollowers::where('users_id', $report->author->id)
                        ->where('followers_id', $authUser->id)
                        ->exists();
                } else {
                    $report->author->follow_flag = false;
                }
            }

            // Add infos without cover
            $report->infos = array_values($reportsInfos->filter(function ($_info) {
                return $_info->var !== "cover";
            })->toArray());

            // Add Likes & Share Flag
            $report->like_flag   = $authUser ? ($report->likes->where('users_id', $authUser->id)->first() ? true : false) : false;
            $report->total_liked = $report->likes->count();
            $report->total_shares =  PostsShares::where('type', ShareService::TYPE_REPORT)->where('posts_type_id', $report->id)->count();
            // For Comment
            $post_comments = [];
            $report->total_comments  = $report->postComments->count();
            $report->comment_flag = $authUser ? ($report->postComments->where('users_id', $authUser->id)->count() ? true : false) : false;
            foreach ($report->postComments->take(2) as $parent_comment) {
                // For Parent Comment
                $parent_comment->like_status = $authUser ? (ReportsCommentsLikes::where('comments_id', $parent_comment->id)->where('users_id', $authUser->id)->first() ? true : false) : false;
                $parent_comment->total_likes = ReportsCommentsLikes::where('comments_id', $parent_comment->id)->count();;
                if ($parent_comment->medias) {
                    foreach ($parent_comment->medias as $media) {
                        $media->media;
                    }
                }
                if ($parent_comment->author) {
                    $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
                }
                $parent_comment->text = convert_post_text_to_tags(strip_tags($parent_comment->text), $parent_comment->tags, false);
                unset($parent_comment->tags);
                $parent_comment->comment_flag = $authUser ? ($parent_comment->sub->where('users_id', $authUser->id)->count() ? true : false) : false;

                // For Sub Comment
                if ($parent_comment->sub) {
                    foreach ($parent_comment->sub as $sub_comment) {
                        $sub_comment->like_status = $authUser ? (ReportsCommentsLikes::where('comments_id', $sub_comment->id)->where('users_id', $authUser->id)->first() ? true : false) : false;
                        $sub_comment->total_likes = ReportsCommentsLikes::where('comments_id', $sub_comment->id)->count();
                        if ($sub_comment->medias) {
                            foreach ($sub_comment->medias as $media) {
                                $media->media;
                            }
                        }
                        if ($sub_comment->author) {
                            $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
                        }
                        $sub_comment->text = convert_post_text_to_tags(strip_tags($sub_comment->text), $sub_comment->tags, false);
                        unset($sub_comment->tags);
                    }
                }

                $post_comments[] = $parent_comment;
            }
            unset($report->cover, $report->likes, $report->postComments);
            $report->post_comments = $post_comments;

            // More Travlogs
            $report->more_travlogs = Reports::where('id', '!=', $report->id)
                ->whereNotNull('published_date')->orderBy('id', 'desc')->take(6)->get()
                ->map(function ($_report) use ($authUser) {
                    $_report->cover_url = isset($_report->cover[0]) ? $_report->cover[0]->val : asset(HOTEL_NO_PHOTO);
                    if ($_report->author) {
                        $_report->author->profile_picture = check_profile_picture($_report->author->profile_picture);
                    }
                    $_report->total_liked = $_report->likes->count();
                    $_report->total_comments  = $_report->postComments->count();

                    $_report->like_flag   = $authUser ? ($_report->likes->where('users_id', $authUser->id)->first() ? true : false) : false;
                    $_report->comment_flag = $authUser ? ($_report->postComments->where('users_id', $authUser->id)->count() ? true : false) : false;

                    unset($_report->likes, $_report->postComments, $_report->cover);
                    return $_report;
                });
            unset($report->location);
            return ApiResponse::create($report);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/get-plan",
     *   tags={"Travelogs"},
     *   summary="Get plan info",
     *   operationId="get_plan_info",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="plan_id",
     *        description="Plan Id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getPlanByID(Request $request)
    {
        try {
            $plan_id = $request->get('plan_id');
            $plan = TripPlans::findOrFail($plan_id);
            if (!$plan) {
                return ApiResponse::__createBadResponse('Trip not found');
            }


            $plan_country = '';
            $place_info = '';
            foreach ($plan->places as $places) {
                if ($plan_country == $places->countries_id) {
                    $place_info = 'in';
                    $plan_country = $places->countries_id;
                } else {
                    $place_info = 'across';
                }
            }
            $data['gMapUrl'] = getStaticGmapURLForPlan($plan, '595x360', 'satellite');
            $data['plan'] =  $plan;

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @param integer $report_id
     * @param integer $user_id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getEmbedShow($report_id, $user_id, Request $request)
    {
        try {
            $user_id = url_decode($user_id);

            if ($user_id) {
                $data = array();
                $expertise = array();
                $report_info = Reports::findOrFail($report_id);
                $user_info = User::find($user_id);

                if (!$report_info) {
                    return ApiResponse::__createBadResponse("Report not found");
                }
                if (!$user_info) {
                    return ApiResponse::__createBadResponse("User not found");
                }

                if ($user_info->id == $report_info->users_id) {

                    $data['me'] = $user_info;
                    $data['report'] = $report_info;
                    $data['title'] = $data['report']->title;
                    $data['more_reports'] =  Reports::where('id', '!=', $report_id)->where('flag', 1)->orderBy('id', 'desc')->take(6)->get();
                    $data['my_plans'] = TripPlans::where('users_id', $user_info->id)->get();


                    $author_id = $report_info->users_id;

                    $cities = ExpertsCities::with('cities')->where('users_id', $author_id)->get();
                    $countries = ExpertsCountries::with('countries')->where('users_id', $author_id)->get();
                    if (count($cities) > 0) {
                        foreach ($cities as $city) {
                            $expertise[] = [
                                'id' => 'city,' . $city->cities_id,
                                'text' => $city->cities->transsingle->title
                            ];
                        }
                    }

                    if (count($countries) > 0) {
                        foreach ($countries as $country) {
                            $expertise[] = [
                                'id' => 'country,' . $country->countries_id,
                                'text' => $country->countries->transsingle->title
                            ];
                        }
                    }
                    $report_info->loadMissing('infos');
                    $data['expertise'] = $expertise;

                    return ApiResponse::create($data);
                } else {
                    return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
                }
            } else {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Delete(
     ** path="/reports/{id}",
     *   tags={"Travelogs"},
     *   summary="Report delete by id",
     *   operationId="report_delete",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="id",
     *        description="Report id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /** 
     * @param integer $report_id
     * @return \Illuminate\Http\Response
     */
    public function postDelete($report_id, ReportsService $reportsService)
    {
        try {
            $report = $reportsService->find($report_id);
            if (!$report) return ApiResponse::__createBadResponse('Report not found');

            if ($report->users_id == Auth::user()->id) {
                $reportsService->delete($report);
                return ApiResponse::__create('successfully deleted');
            } else {
                return ApiResponse::__createUnAuthorizedResponse('only author can delete report');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        try {
            $data = array();
            $data['countries'] = Countries::all();
            $data['cities'] = Cities::all();
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    // public function postCreateEditSecondStep(CreateEditSecondStepRequest $request, ReportsService $reportsService)
    // {
    // try {
    //     $authUser  = auth()->user();
    //     $title = $request->get('title');
    //     $sort = $request->get('sort');
    //     $report_id = $request->get('report_id');
    //     $report_draft = ReportsDraft::where('reports_id', $report_id)->get();
    //     $saving_mode = $request->get('saving_mode');
    //     $current_report = Reports::find($report_id);

    //     // start required validate
    //     if (!$current_report) {
    //         return ApiResponse::__createBadResponse('Report not found');
    //     }

    //     if (!in_array($saving_mode, ["draft", "publish"])) {
    //         return ApiResponse::createValidationResponse([
    //             'saving_mode' => ['invalid saving_mode']
    //         ]);
    //     }

    //     if ($current_report->users_id != $authUser->id) {
    //         return ApiResponse::__createUnAuthorizedResponse('only ' . $authUser->username . ' save report.');
    //     }
    //     // end required validate

    //     if ($title != $current_report->title) {
    //         $current_report->title = $title;
    //         $current_report->save();
    //     }

    //     ReportsInfos::where('reports_id', $report_id)->where('var', '!=', 'cover')->delete();
    //     ReportsDraftInfos::where('reports_draft_id', $report_id)->where('var', '!=', 'cover')->delete();
    //     $ssort = explode(",", $sort);

    //     $order = 1;
    //     if ($saving_mode == "draft") {

    //         foreach ($ssort as $k => $key) {
    //             if (!$request->$key)
    //                 continue;
    //             if (strpos($key, 'add_text_cont') === 0) {
    //                 $updtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();


    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $rtext = new ReportsDraftInfos;
    //                     $rtext->reports_draft_id = $report_draft[0]->id;
    //                     $rtext->var = 'text';
    //                     $rtext->val = $request->get($key);
    //                     $rtext->order = $order;
    //                     $rtext->save();
    //                 }
    //             } elseif (strpos($key, 'add_video_cont') === 0) {
    //                 $updtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $rvideo = new ReportsDraftInfos;
    //                     $rvideo->reports_draft_id = $report_draft[0]->id;
    //                     $rvideo->var = 'video';
    //                     $rvideo->val = $request->get($key);
    //                     $rvideo->order = $order;
    //                     $rvideo->save();
    //                 }
    //             } elseif (strpos($key, 'add_plan_cont') === 0) {
    //                 $updtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $rplan = new ReportsDraftInfos;
    //                     $rplan->reports_draft_id = $report_draft[0]->id;
    //                     $rplan->var = 'plan';
    //                     $rplan->val = $request->get($key);
    //                     $rplan->order = $order;
    //                     $rplan->save();
    //                 }
    //             } elseif (strpos($key, 'add_map_cont') === 0) {
    //                 $updtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $rplan = new ReportsDraftInfos;
    //                     $rplan->reports_draft_id = $report_draft[0]->id;
    //                     $rplan->var = 'map';
    //                     $rplan->val = $request->get($key);
    //                     $rplan->order = $order;
    //                     $rplan->save();
    //                 }
    //             } elseif (strpos($key, 'add_info_cont') === 0) {
    //                 $updtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $info_obj = new ReportsDraftInfos;
    //                     $info_obj->reports_draft_id = $report_draft[0]->id;
    //                     $info_obj->var = 'info';
    //                     $info_obj->val = $request->get($key);
    //                     $info_obj->order = $order;
    //                     $info_obj->save();
    //                 }
    //             } elseif (strpos($key, 'add_place_cont') === 0) {
    //                 $updtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $place = new ReportsDraftInfos;
    //                     $place->reports_draft_id = $report_draft[0]->id;
    //                     $place->var = 'place';
    //                     $place->val = $request->get($key);
    //                     $place->order = $order;
    //                     $place->save();
    //                 }
    //             } elseif (strpos($key, 'add_people_cont') === 0) {
    //                 $updtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $ruser = new ReportsDraftInfos;
    //                     $ruser->reports_draft_id = $report_draft[0]->id;
    //                     $ruser->var = 'user';
    //                     $ruser->val = implode(",", $request->get($key));
    //                     $ruser->order = $order;
    //                     $ruser->save();
    //                 }
    //             } elseif (strpos($key, 'add_image_cont') === 0) {
    //                 $info = 'info_' . $key;
    //                 $img_info_link = 'link_' . $key;
    //                 $report_photos = '';
    //                 $report_photo_url = $request->$key;

    //                 $report_photos = $report_photo_url . (($request->$info != '') ? '-:' . $request->$info : '');

    //                 if (isset($request->$info) && !empty($request->$info) && !empty($request->$img_info_link)) {
    //                     $report_photos .= '-:' . $request->$img_info_link;
    //                 }
    //                 $updtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $report_photos]);
    //                 } else {
    //                     $photo = new ReportsDraftInfos;
    //                     $photo->reports_draft_id = $report_draft[0]->id;
    //                     $photo->var = 'photo';
    //                     $photo->val = $report_photos;
    //                     $photo->order = $order;
    //                     $photo->save();
    //                 }
    //             } elseif (strpos($key, 'add_local_video_cont') === 0) {
    //                 $video_info = 'info_' . $key;
    //                 $video_info_link = 'link_' . $key;
    //                 $report_local_videos = '';

    //                 $report_video_url = $request->$key;

    //                 $report_local_videos = $report_video_url . (($request->$video_info != '') ? '-:' . $request->$video_info : '');

    //                 if (isset($request->$video_info) && !empty($request->$video_info) && !empty($request->$video_info_link)) {
    //                     $report_local_videos .= '-:' . $request->$video_info_link;
    //                 }
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $report_local_videos]);
    //                 } else {
    //                     $photo = new ReportsDraftInfos;
    //                     $photo->reports_draft_id = $report_draft[0]->id;
    //                     $photo->var = 'local_video';
    //                     $photo->val = $report_local_videos;
    //                     $photo->order = $order;
    //                     $photo->save();
    //                 }
    //             }

    //             $order++;
    //         }
    //     } elseif ($saving_mode == "publish") {
    //         foreach ($ssort as $k => $key) {

    //             if (!$request->$key)
    //                 continue;
    //             if (strpos($key, 'add_text_cont') === 0) {
    //                 $updtext = ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->get();
    //                 $upddtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $rtext = new ReportsInfos;
    //                     $rtext->reports_id = $report_id;
    //                     $rtext->var = 'text';
    //                     $rtext->val = $request->get($key);
    //                     $rtext->order = $order;
    //                     $rtext->save();
    //                 }
    //                 if (isset($upddtext)  && count($upddtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $rdtext = new ReportsDraftInfos;
    //                     $rdtext->reports_draft_id = $report_draft[0]->id;
    //                     $rdtext->var = 'text';
    //                     $rdtext->val = $request->get($key);
    //                     $rdtext->order = $order;
    //                     $rdtext->save();
    //                 }
    //             } elseif (strpos($key, 'add_video_cont') === 0) {
    //                 $updtext = ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->get();
    //                 $upddtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $rvideo = new ReportsInfos;
    //                     $rvideo->reports_id = $report_id;
    //                     $rvideo->var = 'video';
    //                     $rvideo->val = $request->get($key);
    //                     $rvideo->order = $order;
    //                     $rvideo->save();
    //                 }
    //                 if (isset($upddtext)  && count($upddtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {

    //                     $rdvideo = new ReportsDraftInfos;
    //                     $rdvideo->reports_draft_id = $report_draft[0]->id;
    //                     $rdvideo->var = 'video';
    //                     $rdvideo->val = $request->get($key);
    //                     $rdvideo->order = $order;
    //                     $rdvideo->save();
    //                 }
    //             } elseif (strpos($key, 'add_plan_cont') === 0) {
    //                 $updtext = ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->get();
    //                 $upddtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $rplan = new ReportsInfos;
    //                     $rplan->reports_id = $report_id;
    //                     $rplan->var = 'plan';
    //                     $rplan->val = $request->get($key);
    //                     $rplan->order = $order;
    //                     $rplan->save();
    //                 }
    //                 if (isset($upddtext)  && count($upddtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {

    //                     $rdplan = new ReportsDraftInfos;
    //                     $rdplan->reports_draft_id = $report_draft[0]->id;
    //                     $rdplan->var = 'plan';
    //                     $rdplan->val = $request->get($key);
    //                     $rdplan->order = $order;
    //                     $rdplan->save();
    //                 }
    //             } elseif (strpos($key, 'add_map_cont') === 0) {
    //                 $updtext = ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->get();
    //                 $upddtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $rplan = new ReportsInfos;
    //                     $rplan->reports_id = $report_id;
    //                     $rplan->var = 'map';
    //                     $rplan->val = $request->get($key);
    //                     $rplan->order = $order;
    //                     $rplan->save();
    //                 }
    //                 if (isset($upddtext)  && count($upddtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {

    //                     $rdplan = new ReportsDraftInfos;
    //                     $rdplan->reports_draft_id = $report_draft[0]->id;
    //                     $rdplan->var = 'map';
    //                     $rdplan->val = $request->get($key);
    //                     $rdplan->order = $order;
    //                     $rdplan->save();
    //                 }
    //             } elseif (strpos($key, 'add_info_cont') === 0) {
    //                 $updtext = ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->get();
    //                 $upddtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $info_obj = new ReportsInfos;
    //                     $info_obj->reports_id = $report_id;
    //                     $info_obj->var = 'info';
    //                     $info_obj->val = $request->get($key);
    //                     $info_obj->order = $order;
    //                     $info_obj->save();
    //                 }
    //                 if (isset($upddtext)  && count($upddtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {

    //                     $dinfo_obj = new ReportsDraftInfos;
    //                     $dinfo_obj->reports_draft_id = $report_draft[0]->id;
    //                     $dinfo_obj->var = 'info';
    //                     $dinfo_obj->val = $request->get($key);
    //                     $dinfo_obj->order = $order;
    //                     $dinfo_obj->save();
    //                 }
    //             } elseif (strpos($key, 'add_place_cont') === 0) {
    //                 $updtext = ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->get();
    //                 $upddtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $place = new ReportsInfos;
    //                     $place->reports_id = $report_id;
    //                     $place->var = 'place';
    //                     $place->val = $request->get($key);
    //                     $place->order = $order;
    //                     $place->save();
    //                 }
    //                 if (isset($upddtext)  && count($upddtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {

    //                     $dplace = new ReportsDraftInfos;
    //                     $dplace->reports_draft_id = $report_draft[0]->id;
    //                     $dplace->var = 'place';
    //                     $dplace->val = $request->get($key);
    //                     $dplace->order = $order;
    //                     $dplace->save();
    //                 }
    //             } elseif (strpos($key, 'add_people_cont') === 0) {
    //                 $updtext = ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->get();
    //                 $upddtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {
    //                     $ruser = new ReportsInfos;
    //                     $ruser->reports_id = $report_id;
    //                     $ruser->var = 'user';
    //                     $ruser->val = implode(",", $request->get($key));
    //                     $ruser->order = $order;
    //                     $ruser->save();
    //                 }
    //                 if (isset($upddtext)  && count($upddtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {

    //                     $druser = new ReportsDraftInfos;
    //                     $druser->reports_draft_id = $report_draft[0]->id;
    //                     $druser->var = 'user';
    //                     $druser->val = implode(",", $request->get($key));
    //                     $druser->order = $order;
    //                     $druser->save();
    //                 }
    //             } elseif (strpos($key, 'add_image_cont') === 0) {
    //                 $info = 'info_' . $key;
    //                 $img_info_link = 'link_' . $key;
    //                 $report_photos = '';
    //                 $report_photo_url = $request->$key;

    //                 $report_photos = $report_photo_url . (($request->$info != '') ? '-:' . $request->$info : '');

    //                 if (isset($request->$info) && !empty($request->$info) && !empty($request->$img_info_link)) {
    //                     $report_photos .= '-:' . $request->$img_info_link;
    //                 }
    //                 $updtext = ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->get();
    //                 $upddtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->update(['val' => $report_photos]);
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $report_photos]);
    //                 } else {
    //                     $photo = new ReportsInfos;
    //                     $photo->reports_id = $report_id;
    //                     $photo->var = 'photo';
    //                     $photo->val = $report_photos;
    //                     $photo->order = $order;
    //                     $photo->save();
    //                 }
    //                 if (isset($upddtext)  && count($upddtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {

    //                     $dphoto = new ReportsDraftInfos;
    //                     $dphoto->reports_draft_id = $report_draft[0]->id;
    //                     $dphoto->var = 'photo';
    //                     $dphoto->val = $report_photos;
    //                     $dphoto->order = $order;
    //                     $dphoto->save();
    //                 }
    //             } elseif (strpos($key, 'add_local_video_cont') === 0) {
    //                 $video_info = 'info_' . $key;
    //                 $video_info_link = 'link_' . $key;
    //                 $report_local_videos = '';

    //                 $report_video_url = $request->$key;

    //                 $report_local_videos = $report_video_url . (($request->$video_info != '') ? '-:' . $request->$video_info : '');

    //                 if (isset($request->$video_info) && !empty($request->$video_info) && !empty($request->$video_info_link)) {
    //                     $report_local_videos .= '-:' . $request->$video_info_link;
    //                 }
    //                 $updtext = ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->get();
    //                 $upddtext = ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->get();
    //                 if (isset($updtext)  && count($updtext) > 0) {
    //                     ReportsInfos::where([['reports_id', $report_id], ['order', $order]])->update(['val' => $report_local_videos]);
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $report_local_videos]);
    //                 } else {
    //                     $photo = new ReportsInfos;
    //                     $photo->reports_id = $report_id;
    //                     $photo->var = 'local_video';
    //                     $photo->val = $report_local_videos;
    //                     $photo->order = $order;
    //                     $photo->save();
    //                 }
    //                 if (isset($upddtext)  && count($upddtext) > 0) {
    //                     ReportsDraftInfos::where([['reports_draft_id', $report_draft[0]->id], ['order', $order]])->update(['val' => $request->get($key)]);
    //                 } else {

    //                     $dphoto = new ReportsDraftInfos;
    //                     $dphoto->reports_draft_id = $report_draft[0]->id;
    //                     $dphoto->var = 'local_video';
    //                     $dphoto->val = $report_local_videos;
    //                     $dphoto->order = $order;
    //                     $dphoto->save();
    //                 }
    //             }

    //             $order++;
    //         }


    //         $reportsService->publishedPlace($current_report);
    //         $reportsService->publishedLocation($report_id);
    //     }

    //     if ($saving_mode == "publish") {
    //         $action = ActivityLog::query()->where([
    //             'type' => PrimaryPostService::TYPE_REPORT,
    //             'variable' => $current_report->id,
    //             'users_id' => auth()->user()->id
    //         ])->whereIn('action', ['create', 'update'])->exists() ? 'update' : 'create';
    //         log_user_activity(PrimaryPostService::TYPE_REPORT, $action, $current_report->id);
    //     }

    //     // if ($request->saving_mode == 'draft' || $request->saving_mode == 'publish') {
    //     //     return ApiResponse::create($report_id);
    //     // } else {
    //     if ($report_id == "0" || $report_id == "") {
    //         return ApiResponse::create([
    //             "message" => ['Report created!']
    //         ]);
    //     } else {

    //         $p_report = Reports::find($report_id);
    //         $p_report->flag = 0;
    //         $p_report->save();
    //         $authUser       = Auth::user();
    //         $isAuthUser     = Auth::check();
    //         $isDraftView    = false;
    //         $isMyReport     = false;
    //         $cities         = collect([]);
    //         $countries      = collect([]);

    //         $report = Reports::with('draft_report', 'author')->where('id', $report_id)->first();

    //         if ($isAuthUser) {
    //             $isMyReport     = $report->users_id == $authUser->id;
    //             $isDraftView    = $isMyReport;

    //             $reportView = new ReportsViews;
    //             $reportView->reports_id = $report->id;
    //             $reportView->users_id = $authUser->id;
    //             $reportView->save();
    //         }

    //         // Add draft_flag
    //         $report->draft_flag = (bool) $report->flag;

    //         $draft_report = $report->draft_report;
    //         unset($report->draft_report);

    //         if (!$draft_report) return ApiResponse::__createBadResponse("report not found");
    //         $report->draft_report_id = $draft_report->id;

    //         $_rep = $draft_report;

    //         if ($isDraftView) { // change report data with draft data for author view

    //             $reportsInfos = ReportsDraftInfos::query()->where('reports_draft_id', $draft_report->id);

    //             $report->title = $draft_report->title;
    //             $report->description = $draft_report->description;
    //         } else {
    //             $_rep =  $report;
    //             $reportsInfos = ReportsInfos::query()->where('reports_id', $report->id);
    //         }
    //         $cover = asset(HOTEL_NO_PHOTO);
    //         $reportsInfos = $reportsInfos->orderBy('order', 'ASC')->get()->each(function ($info) use ($isDraftView, $authUser, $isAuthUser, &$cover) {
    //             if ($isDraftView) {
    //                 unset($info->reports_draft_id);
    //             } else {
    //                 unset($info->type, $info->language_id, $info->reports_id);
    //             }

    //             $info->var = trim(strtolower($info->var));
    //             switch ($info->var) {
    //                 case "place":
    //                     $p = Place::find($info->val);
    //                     $medium['title'] = @$p->trans[0]->title;
    //                     $medium['address'] = @$p->trans[0]->address;
    //                     $medium['location'] = $p->lat . "," . $p->lng;
    //                     $medium['lat'] = $p->lat;
    //                     $medium['lng'] = $p->lng;
    //                     $medium['place_type'] = do_placetype($p->place_type);

    //                     $medium['image'] = asset(PLACE_PLACEHOLDERS);
    //                     if (isset($r['place_media']) && $r['place_media'] != null) {
    //                         $medium['image'] = check_media_url($r['place_media']);
    //                     }

    //                     $medium['city_id'] = $p->cities_id;
    //                     $medium['city_title'] = @$p->city->transsingle->title;
    //                     $medium['country_id'] = $p->countries_id;
    //                     $medium['country_title'] = @$p->country->transsingle->title;

    //                     $medium['ratting'] = Reviews::where('places_id', $p->id)->avg('score') ?? 0;
    //                     $medium['total_reviews'] = Reviews::where('places_id', $p->id)->count();
    //                     $medium['recent_reviews'] = Reviews::where('places_id', $p->id)->take(2)->get()->each(function ($placeReviewer) {
    //                         $placeReviewer->author;
    //                     });
    //                     $medium['total_followers'] = PlaceFollowers::where('places_id', $p->id)->count();
    //                     $medium['recent_followers'] = PlaceFollowers::where('places_id', $p->id)->take(4)->get()->map(function ($placeFollower) {
    //                         return [
    //                             'id' => $placeFollower->users_id,
    //                             'profile_picture' => check_profile_picture($placeFollower->user->profile_picture),
    //                         ];
    //                     });
    //                     $info->data = $medium;
    //                     break;

    //                 case "cover":
    //                     $cover = $info->val;
    //                     break;

    //                 case "info":
    //                     $_ = @explode(":-", $info->val);
    //                     $_data['label'] = @$_[0];
    //                     $_data['value'] =  explode(' | ', (@$_[1] ?? ''));
    //                     $info->data = $_data;
    //                     break;

    //                 case "map":
    //                     $temp = @explode("-:", $info->val);
    //                     $__ = [
    //                         'id' => @$temp[7],
    //                         'title' => @$temp[4],
    //                         'url' => "https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/" . strtolower(@$temp[5]) . ".png",
    //                         'lat' => @$temp[0],
    //                         'lng' => @$temp[1],
    //                         'population' => @$temp[6],
    //                         'follow_flag' => get_country_follow(@$temp[7]),
    //                     ];
    //                     $info->data = $__;
    //                     break;

    //                 case "local_video":
    //                 case "photo":
    //                     $_ = @explode("-:", $info->val);
    //                     $_data['url']   = @$_[0];
    //                     $_data['desc']  = @$_[1];
    //                     $_data['link']  = @$_[2];
    //                     $info->data = $_data;
    //                     break;

    //                 case "user":
    //                     $info->data  = [];
    //                     if (strlen(trim($info->val))) {
    //                         $info->data = User::query()
    //                             ->select('id', 'name', 'username', 'email', 'cover_photo', 'profile_picture')
    //                             ->whereIn('id', explode(",", $info->val))
    //                             ->get()->each(function (&$user) use ($authUser, $isAuthUser) {
    //                                 $user->follow_flag = ($isAuthUser) ? UsersFollowers::where('users_id', $user->id)->where('followers_id', $authUser->id)->exists() : false;
    //                                 $user->cover_photo  = check_cover_photo($user->cover_photo);
    //                                 $user->profile_picture = check_profile_picture($user->profile_picture);
    //                                 $user->media = Media::select('url')
    //                                     ->where(DB::raw('RIGHT(url, 4)'), '!=', '.mp4')
    //                                     ->where('users_id', $user->id)->take(3)->pluck('url')
    //                                     ->map(function (&$url) {
    //                                         return check_media_url($url);
    //                                     });
    //                             });
    //                     }
    //                     break;

    //                 case "plan":
    //                     $info->data = travelogTripObject((int)$info->val);
    //                     break;
    //             }
    //         });
    //         $report->cover_url =  $cover;

    //         $fetchLocations = $_rep->prettyLocation();
    //         if ($fetchLocations['total'] > 1) {
    //             $right_info = ['is_multiple' => true, 'data' => api_get_report_locations_info($fetchLocations)];
    //         } else {
    //             $right_info = ['is_multiple' => false];
    //             if (count($fetchLocations[Reports::LOCATION_COUNTRY])) {
    //                 $right_info['data'][] = [
    //                     'key' => trans('region.country_capital'),
    //                     'value' =>  @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->capitals[0]->city->transsingle->title,
    //                 ];
    //                 $right_info['data'][] = [
    //                     'key' => trans('region.population'),
    //                     'value' =>   @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->transsingle->population,
    //                 ];
    //                 $right_info['data'][] = [
    //                     'key' => "Currency",
    //                     'value' => @collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->currencies[0]->transsingle->title,
    //                 ];
    //             } elseif (count($fetchLocations[Reports::LOCATION_CITY])) {
    //                 $right_info['data'][] = [
    //                     'key' => trans('region.country_capital'),
    //                     'value' =>  @collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->title,
    //                 ];
    //                 $right_info['data'][] = [
    //                     'key' => trans('region.population'),
    //                     'value' =>   @collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->population,
    //                 ];
    //                 $right_info['data'][] = [
    //                     'key' => "Currency",
    //                     'value' => @collect($fetchLocations[Reports::LOCATION_CITY])->first()->country->currencies[0]->transsingle->title,
    //                 ];
    //             } else {
    //                 $right_info['data'] = [];
    //             }
    //         }
    //         $report->right_info = $right_info;

    //         // Add All Location (step 1)
    //         $report->locations = _createLocationObjectWithFullDetailsForReport($fetchLocations[REPORTS::LOCATION_CITY], $fetchLocations[REPORTS::LOCATION_COUNTRY]);

    //         // Add author
    //         if ($report->author) {
    //             $report->author->profile_picture = check_profile_picture($report->author->profile_picture);
    //             if ($isAuthUser) {
    //                 $report->author->follow_flag = UsersFollowers::where('users_id', $report->author->id)
    //                     ->where('followers_id', $authUser->id)
    //                     ->exists();
    //             } else {
    //                 $report->author->follow_flag = false;
    //             }
    //         }

    //         // Add infos without cover
    //         $report->infos = array_values($reportsInfos->filter(function ($_info) {
    //             return $_info->var !== "cover";
    //         })->toArray());

    //         // Add Likes & Share Flag
    //         $report->like_flag   = $authUser ? ($report->likes->where('users_id', $authUser->id)->first() ? true : false) : false;
    //         $report->total_liked = $report->likes->count();
    //         $report->total_shares =  PostsShares::where('type', ShareService::TYPE_REPORT)->where('posts_type_id', $report->id)->count();
    //         // For Comment
    //         $post_comments = [];
    //         $report->total_comments  = $report->postComments->count();
    //         $report->comment_flag = $authUser ? ($report->postComments->where('users_id', $authUser->id)->count() ? true : false) : false;
    //         foreach ($report->postComments->take(2) as $parent_comment) {
    //             // For Parent Comment
    //             $parent_comment->like_status = $authUser ? (ReportsCommentsLikes::where('comments_id', $parent_comment->id)->where('users_id', $authUser->id)->first() ? true : false) : false;
    //             $parent_comment->total_likes = ReportsCommentsLikes::where('comments_id', $parent_comment->id)->count();;
    //             if ($parent_comment->medias) {
    //                 foreach ($parent_comment->medias as $media) {
    //                     $media->media;
    //                 }
    //             }
    //             if ($parent_comment->author) {
    //                 $parent_comment->author->profile_picture = check_profile_picture($parent_comment->author->profile_picture);
    //             }
    //             $parent_comment->text = convert_post_text_to_tags(strip_tags($parent_comment->text), $parent_comment->tags, false);
    //             unset($parent_comment->tags);
    //             $parent_comment->comment_flag = $authUser ? ($parent_comment->sub->where('users_id', $authUser->id)->count() ? true : false) : false;

    //             // For Sub Comment
    //             if ($parent_comment->sub) {
    //                 foreach ($parent_comment->sub as $sub_comment) {
    //                     $sub_comment->like_status = $authUser ? (ReportsCommentsLikes::where('comments_id', $sub_comment->id)->where('users_id', $authUser->id)->first() ? true : false) : false;
    //                     $sub_comment->total_likes = ReportsCommentsLikes::where('comments_id', $sub_comment->id)->count();
    //                     if ($sub_comment->medias) {
    //                         foreach ($sub_comment->medias as $media) {
    //                             $media->media;
    //                         }
    //                     }
    //                     if ($sub_comment->author) {
    //                         $sub_comment->author->profile_picture = check_profile_picture($sub_comment->author->profile_picture);
    //                     }
    //                     $sub_comment->text = convert_post_text_to_tags(strip_tags($sub_comment->text), $sub_comment->tags, false);
    //                     unset($sub_comment->tags);
    //                 }
    //             }

    //             $post_comments[] = $parent_comment;
    //         }
    //         unset($report->cover, $report->likes, $report->postComments);
    //         $report->post_comments = $post_comments;

    //         // More Travlogs
    //         $report->more_travlogs = Reports::where('id', '!=', $report->id)
    //             ->whereNotNull('published_date')->orderBy('id', 'desc')->take(6)->get()
    //             ->map(function ($_report) use ($authUser) {
    //                 $_report->cover_url = isset($_report->cover[0]) ? $_report->cover[0]->val : asset(HOTEL_NO_PHOTO);
    //                 if ($_report->author) {
    //                     $_report->author->profile_picture = check_profile_picture($_report->author->profile_picture);
    //                 }
    //                 $_report->total_liked = $_report->likes->count();
    //                 $_report->total_comments  = $_report->postComments->count();

    //                 $_report->like_flag   = $authUser ? ($_report->likes->where('users_id', $authUser->id)->first() ? true : false) : false;
    //                 $_report->comment_flag = $authUser ? ($_report->postComments->where('users_id', $authUser->id)->count() ? true : false) : false;

    //                 unset($_report->likes, $_report->postComments, $_report->cover);
    //                 return $_report;
    //             });

    //         return ApiResponse::create(
    //             $report
    //         );
    //     }
    //     // }
    // } catch (\Throwable $e) {
    //     return ApiResponse::createServerError($e);
    // }
    // }


    /**
     * @param CommentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postComment(CommentRequest $request)
    {
        try {
            $pair = $request['pair'];
            $report_id = $request->report_id;
            $report = Reports::find($report_id);
            if (!$report) {
                return ApiResponse::__createBadResponse("Report not found");
            }
            $com_text = $request->text;

            $text = convert_string($com_text);

            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;

            // $author_id = $report->users_id;
            // if (isset($request->comment_id)) {
            //     $ranking_title = 'report_comment_reply';
            // } else {
            //     $ranking_title = 'report_comment';
            // }

            $is_files = false;
            $file_lists = getTempFiles($pair);

            $is_files = count($file_lists) > 0 ? true : false;
            $text = is_null($text) ? '' : $text;

            if (!$is_files && $text == "")
                return 0;

            if (is_object($report)) {
                $new_comment = new ReportsComments;
                $new_comment->reports_id = $report->id;
                $new_comment->users_id = $user_id;
                $new_comment->reply_to = (isset($request->comment_id)) ? $request->comment_id : null;
                $new_comment->comment = $text;
                if ($new_comment->save()) {
                    log_user_activity('Report', 'comment', $new_comment->id);

                    foreach ($file_lists as $file) {
                        $filename = $user_id . '_' . time() . '_' . $file[1];
                        Storage::disk('s3')->put('report-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                        $path = S3_BASE_URL . 'report-comment-photo/' . $filename;

                        $media = new Media();
                        $media->url = $path;
                        $media->author_name = $user_name;
                        $media->title = $text;
                        $media->author_url = '';
                        $media->source_url = '';
                        $media->license_name = '';
                        $media->license_url = '';
                        $media->uploaded_at = date('Y-m-d H:i:s');
                        $media->users_id = $user_id;
                        $media->type  = getMediaTypeByMediaUrl($media->url);
                        $media->save();

                        $report_comments_medias = new ReportsCommentsMedias();
                        $report_comments_medias->reports_comments_id = $new_comment->id;
                        $report_comments_medias->medias_id = $media->id;
                        $report_comments_medias->save();
                    }

                    return ApiResponse::create(
                        [
                            'child' => $new_comment,
                            'report' => $report
                        ]
                    );
                } else {
                    return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
                }
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/reports/share",
     *   tags={"Travelogs"},
     *   summary="Report share",
     *   operationId="report_share",
     *   security={
     *  {"bearer_token": {}
     *    }},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="text",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="post_id",
     *                  type="integer"
     *              ),
     *              @OA\Property(
     *                   property="permission",
     *                   type="integer",
     *                   example="0: Public, 1: Followers, 2: Friends"
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     **/
    /**
     * @param PostShareRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postShare(PostShareRequest $request)
    {
        try {
            $post_id = $request->post_id;
            $user_id = Auth::user()->id;
            // $author_id = Reports::find($post_id)->users_id;
            $check_share_exists = ReportsShares::where('reports_id', $post_id)->where('users_id', $user_id)->first();
            $data = array();

            if (is_object($check_share_exists)) {
                $data['status'] = 'no';
            } else {
                $share = new ReportsShares;
                $share->reports_id = $post_id;
                $share->permissions = $request->permission;
                $share->users_id = $user_id;
                $share->text = $request->text;
                $share->save();

                log_user_activity('Report', 'share', $post_id);
                // notify($author_id, 'status_like', $post_id);

                $data['status'] = 'yes';
            }
            $data['count'] = count(ReportsShares::where('reports_id', $post_id)->get());
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postLike(Request $request)
    {
        try {
            if ($request->post_id) {
                $post_id = $request->post_id;
                $user_id = Auth::user()->id;
                $author_id = Reports::find($post_id)->users_id;
                $check_like_exists = ReportsLikes::where('reports_id', $post_id)->where('users_id', $user_id)->first();
                $data = array();

                if (is_object($check_like_exists)) {
                    $check_like_exists->delete();
                    log_user_activity('Report', 'unlike', $post_id);
                    $data['status'] = false;
                } else {
                    $like = new ReportsLikes;
                    $like->reports_id = $post_id;
                    $like->users_id = $user_id;
                    $like->save();

                    log_user_activity('Report', 'like', $post_id);

                    $data['status'] = true;
                }
                $data['count'] = count(ReportsLikes::where('reports_id', $post_id)->get());
                return ApiResponse::create($data);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postCommentDelete(Request $request)
    {
        try {
            $comment_id = $request->comment_id;

            $comment = ReportsComments::find($comment_id);
            if ($comment) {
                $comment_likes = $comment->likes;

                foreach ($comment->medias as $com_media) {
                    $medias_exist = Media::find($com_media->medias_id);

                    $com_media->delete();

                    if ($medias_exist) {
                        $amazonefilename = explode('/', $medias_exist->url);
                        Storage::disk('s3')->delete('report-comment-photo/' . end($amazonefilename));

                        $medias_exist->delete();
                    }
                }

                if (count($comment_likes) > 0) {
                    foreach ($comment_likes as $comment_like) {
                        $comment_like->delete();
                    }
                }

                if ($comment->delete()) {
                    return ApiResponse::create(['message' => ['Comment is delete']]);
                } else {
                    return ApiResponse::create(['message' => ['Comment not delete']], false, ApiResponse::BAD_REQUEST);
                }
            } else {
                return ApiResponse::create(['message' => ['Comment not found']], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CommentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCommentForPlace(CommentRequest $request)
    {
        try {
            $report_id = $request->get('report_id');
            $report = Reports::find($report_id);
            $text = $request->get('text');
            $user_id = Auth::user()->id;
            $author_id = $report->users_id;
            //$user_id = 1;

            $comment = new ReportsComments;
            $comment->reports_id = $report->id;
            $comment->users_id = $user_id;
            $comment->comment = $text;

            if ($comment->save()) {
                return ApiResponse::create($comment);
            } else {
                return ApiResponse::create(['message' => ['Comment can not add.']], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function postCommentLike(Request $request)
    {
        try {
            $comment_id = $request->comment_id;
            $user_id = Auth::user()->id;
            $reportComments = ReportsComments::find($comment_id);
            if ($reportComments) {
                $author_id = $reportComments->users_id;
                $report_id = $reportComments->reports_id;
                $report_author_id = Reports::find($report_id)->users_id;
                $check_like_exists = ReportsCommentsLikes::where('comments_id', $comment_id)->where('users_id', $user_id)->first();
                $data = array();

                if (is_object($check_like_exists)) {

                    $check_like_exists->delete();
                    log_user_activity('Report', 'commentunlike', $comment_id);
                    //notify($author_id, 'status_commentunlike', $comment_id);

                    $data['status'] = 'no';
                } else {
                    $like = new ReportsCommentsLikes;
                    $like->comments_id = $comment_id;
                    $like->users_id = $user_id;
                    $like->save();

                    log_user_activity('Report', 'commentlike', $comment_id);
                    if ($author_id != $user_id) {
                        notify($author_id, 'status_commentlike', $comment_id);
                    }

                    $data['status'] = 'yes';
                }
                $data['count'] = count(ReportsCommentsLikes::where('comments_id', $comment_id)->get());
                $data['name'] = Auth::user()->name;
                return ApiResponse::create($data);
            } else {
                return ApiResponse::create(['message' => ['Comment not found.']], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function getCommentLikeUsers(Request $request)
    {
        try {
            $comment_id = $request->comment_id;
            $comment = ReportsComments::find($comment_id);


            if ($comment->likes) {
                $likes = $comment->likes()->orderBy('created_at', 'DESC')->take(8)->get();
                return ApiResponse::create([
                    'likes' => $likes
                ]);
            } else {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CommentRequest $request
     * @return \IlluminRequestate\Http\Response
     */
    public function postCommentEdit(CommentRequest $request)
    {
        try {
            $pair = $request['pair'];
            $report_id = $request->report_id;
            $comment_id = $request->comment_id;

            $com_text = $request->text;
            $text = convert_string($com_text);

            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;

            $report = Reports::find($report_id);
            $report_comment = ReportsComments::find($comment_id);
            // $author_id = $report->users_id;

            $is_files = false;
            $file_lists = getTempFiles($pair);

            $is_files = count($file_lists) > 0 ? true : false;
            $text = is_null($text) ? '' : $text;
            if (!$is_files && $text == "")
                return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);

            if (is_object($report_comment)) {
                $report_comment->comment = $text;
                if ($report_comment->save()) {
                    log_user_activity('Status', 'comment', $report_id);


                    if (isset($request->existing_medias) && count($report_comment->medias) > 0) {

                        foreach ($report_comment->medias as $media_list) {
                            if (in_array($media_list->media->id, $request->existing_medias)) {
                                $medias_exist = Media::find($media_list->medias_id);

                                $media_list->delete();

                                if ($medias_exist) {
                                    $amazonefilename = explode('/', $medias_exist->url);
                                    Storage::disk('s3')->delete('report-comment-photo/' . end($amazonefilename));

                                    $medias_exist->delete();
                                }
                            }
                        }
                    }



                    foreach ($file_lists as $file) {
                        $filename = $user_id . '_' . time() . '_' . $file[1];
                        Storage::disk('s3')->put('report-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                        $path = S3_BASE_URL . 'report-comment-photo/' . $filename;

                        $media = new Media();
                        $media->url = $path;
                        $media->author_name = $user_name;
                        $media->title = $text;
                        $media->author_url = '';
                        $media->source_url = '';
                        $media->license_name = '';
                        $media->license_url = '';
                        $media->uploaded_at = date('Y-m-d H:i:s');
                        $media->users_id = $user_id;
                        $media->type  = getMediaTypeByMediaUrl($media->url);
                        $media->save();

                        $report_comments_medias = new ReportsCommentsMedias();
                        $report_comments_medias->reports_comments_id = $report_comment->id;
                        $report_comments_medias->medias_id = $media->id;
                        $report_comments_medias->save();
                    }

                    $report_comment->load('medias');

                    return ApiResponse::create(
                        [
                            'comment' => $report_comment,
                            'report' => $report
                        ]
                    );
                } else {
                    return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
                }
            } else {
                return ApiResponse::create(['message' => ["comment not found"]], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function getMoreComment(Request $request)
    {
        try {
            $page = $request->pagenum;
            $next = ($page - 1) * 5;

            $report_id = $request->report_id;
            $report = Reports::find($report_id);
            $report_comments = $report->comments()->orderBy('created_at', 'desc')->skip($next)->take(5)->get();

            $comment = [];
            if (count($report_comments) > 0) {
                foreach ($report_comments as $report_comment) {
                    $comment[] = array('comment' => $report_comment, 'report' => $report);
                }
            }
            return ApiResponse::create($comment);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function postLikes4Modal(Request $request)
    {
        try {
            $report_id = $request->report_id;
            $post_likes = ReportsLikes::where('reports_id', $report_id)->orderBy('created_at', 'DESC')->take(10)->get();

            $data = [];
            foreach ($post_likes as $like) {
                $data[] =  $like;
            }

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function postCommentLikes4Modal(Request $request)
    {
        try {
            $comment_id = $request->comment_id;
            $post_likes = ReportsCommentsLikes::where('comments_id', $comment_id)->get();
            $data = [];

            foreach ($post_likes as $like) {
                $data = $like;
            }

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function reportComments4Modal(Request $request)
    {
        try {
            $report_id = $request->report_id;
            $count = $request->count;

            $skip = $count;

            $report = Reports::find($report_id);
            $reportComment = [];
            $totalrows = 0;
            if (is_object($report)) {
                $comments = $report->comments()->skip($skip)
                    ->take(5)
                    ->get();

                foreach ($comments as $comment) {
                    $reportComment = array('comment' => $comment, 'report' => $report);
                }

                $totalrows = count($report->comments);
            }
            return ApiResponse::create(
                [
                    "data" => $reportComment,
                    "rows" => $totalrows
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return shuffle[Countries|Cities]
     */
    public function getCitiesCountriesTag($request, $invitationsService = null)
    {
        $queryParam = $request->get('q');
        $result = [];
        if ($queryParam != '') {

            $all_cities = Cities::limit(15)
                ->with('transsingle')
                ->whereHas('transsingle', function ($query) use ($queryParam) {
                    return  $query->where('title', 'like', $queryParam . "%");
                })
                ->with('country.trans')
                ->with('getMedias')
                ->get();

            foreach ($all_cities as $city) {
                $city->type = "city";
                if (count($city->getMedias) > 0) {
                    $city->image = S3_BASE_URL . $city->getMedias[0]->url;
                } else {
                    $city->image = url(PLACE_PLACEHOLDERS);
                }
                $city->name = $city->transsingle->title;
                $city->country_name = $city->country->trans[0]->title;

                unset(
                    $city->country,
                    $city->transsingle,
                    $city->getMedias,
                    $city->safety_degree_id,
                    $city->level_of_living_id,
                    $city->cover_media_id
                );
            }

            $all_countries = Countries::limit(15)
                ->with('transsingle')
                ->with('region.transsingle')
                ->with('getMedias')
                ->whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', $queryParam . "%");
                })
                ->get();

            foreach ($all_countries as $country) {
                $country->type = "country";
                if (count($country->getMedias) > 0) {
                    $country->image = S3_BASE_URL . $country->getMedias[0]->url;
                } else {
                    $country->image = url(PLACE_PLACEHOLDERS);
                }
                $country->name = $country->transsingle->title;
                $country->region_name = $country->region->transsingle->title;
                unset($country->region, $country->transsingle, $country->getMedias, $country->cover_media_id, $country->safety_degree_id);
            }
            // return $all_cities;
            // return $all_countries;
            $result =  $all_cities->concat($all_countries)->shuffle();
        }
        return $result;
    }

    /**
     * @param Request $request
     * @param TripInvitationsService $invitationsService
     */
    public function getPlacesTag(Request $request, TripInvitationsService $invitationsService)
    {
        $result = [];
        if ($request->q) {
            $query = $request->get('q');
            // search in place
            if ($request->has('type')) {
                $type = $request->get('type');
            }
            $fromExternal = false;
            if ($query != '') {
                $qr = app('App\Http\Controllers\HomeController')->getElSearchResult($query);
                if ($qr == null || empty($qr)) {
                    $mapboxLink = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . urlencode($query) . '.json?limit=10&routing=true&access_token=' . config('mapbox.token');
                    if (isset($type) && $type) {
                        $mapboxLink .= '&types=' . $type;
                    }
                    $mapboxResult = json_decode(file_get_contents($mapboxLink));
                    $qr = $mapboxResult->features;
                    $fromExternal = true;
                }

                foreach ($qr as $r) {
                    $medium = [];
                    if ($fromExternal) {
                        if (!Place::where('provider_id', '=', $r->id)->exists()) {
                            $medium['is_new'] = 1;
                            $medium['id'] = $r->id;
                            $medium['place_type'] = @$r->place_type[0]; //implode(',', $r->place_type);
                            $medium['address'] = str_replace("'", "&quot;", @$r->properties->address);
                            $medium['title'] = $r->text;
                            $medium['lat'] = $r->geometry->coordinates[1];
                            $medium['lng'] = $r->geometry->coordinates[0];
                            $medium['rating'] = @$r->rating;
                            $medium['location'] = $r->geometry->coordinates[1] . "," . $r->geometry->coordinates[0];
                            $medium['city_id'] = @$r->cities_id;
                            $medium['city_title'] = @explode(', ', $r->place_name)[2];
                        } else {
                            $p = Place::where('provider_id', '=', $r->id)->first();

                            $medium['id'] = @$p->id;
                            $medium['place_type'] = @explode(",", $p->place_type)[0];
                            $medium['address'] = str_replace("'", "&quot;", $p->transsingle->address);
                            $medium['title'] = $p->transsingle->title;
                            $medium['lat'] = $p->lat;
                            $medium['lng'] = $p->lng;
                            $medium['rating'] = @$p->rating;
                            $medium['location'] = $p->lat . "," . $p->lng;
                            if (isset($p->getMedias[0]) && is_object($p->getMedias[0]))
                                $medium['image'] = $p->getMedias[0]->url;

                            $medium['city_id'] = @$p->cities_id;
                            $medium['city_title'] = @$p->city->transsingle->title;
                        }

                        $medium['type'] = '';

                        if ($medium['place_type'] != 'locality') {
                            $result[] = $medium;
                        }
                    } else {
                        $medium['id'] = @$r['place_id'];
                        $medium['address'] = $r['address'];
                        $medium['title'] = $r['place_title'];
                        $medium['lat'] = $r['lat'];
                        $medium['place_type'] = @explode(",", @$r['place_type'])[0];
                        $medium['lng'] = $r['lng'];
                        $medium['location'] = $r['lat']  . "," . $r['lng'];

                        if (isset($r['place_media']) && $r['place_media'] != null) {
                            $medium['image'] = $r['place_media'];
                        }

                        $p = Place::find($medium['id']);
                        $medium['city_id'] = @$p->cities_id;
                        $medium['city_title'] = @$p->city->transsingle->title;
                        $result[] = $medium;
                    }
                }

                foreach ($result as &$place) {
                    $place['reviews'] = Reviews::where('places_id', $place['id'])->take(3)->get();
                    $place['reviews_avg'] = Reviews::where('places_id', $place['id'])->avg('score');
                    $isTrending = PlacesTop::where('places_id', $place['id'])->exists();

                    if ($isTrending) {
                        $place['type'] = 'Trending';
                        continue;
                    }

                    $friendsAndFollowingsIds = $invitationsService->findFriends()->merge($invitationsService->findFollowings());
                    $isSuggested = Checkins::query()->where('place_id', $place['id'])->whereIn('users_id', $friendsAndFollowingsIds)->exists();
                    if ($isSuggested) {
                        $place['type'] = 'Suggested';
                    }
                }
            }
            $no = 0;
            foreach ($result as $place) {
                $result[$no]['type'] = "place";
                if (!isset($result[$no]['image'])) {
                    $result[$no]['image'] = url(PLACE_PLACEHOLDERS);
                }
                $no++;
            }
        }
        return $result;
    }

    /**
     * @param Request $request
     * @param TripInvitationsService $invitationsService
     */
    public function getUserTag(Request $request)
    {
        $usersArr = [];
        if ($request->q) {
            $queryParam = $request->q;
            $all_users = User::where('name', 'like', "%" . $queryParam . "%")
                ->limit(25)
                ->orderBy('name', 'ASC')
                ->get();
            foreach ($all_users as $user) {
                if ($user->name != null) {
                    $usersArr[] = [
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'username' => $user->username,
                        'display_name' => $user->display_name,
                        'interests' => $user->interests,
                        'country' => ($user->country_of_nationality) ? $user->country_of_nationality->trans[0]->title : null,
                        'image' => check_profile_picture($user->profile_picture),
                    ];
                }
            }
        }
        return $usersArr;
    }

    /**
     * @OA\GET(
     ** path="/search/user-or-place",
     *   tags={"Search"},
     *   summary="This Api search users, places and cities_or_countries",
     *   operationId="Api\ReportsController@getSearchUsersPlaces",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="q",
     *        description="search keyword",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function getSearchUsersPlaces(Request $request, TripInvitationsService $invitationsService)
    {
        try {
            $return = [];
            $return['users']                = $this->getUserTag($request);
            $return['places']               = $this->getCitiesCountriesTag($request, $invitationsService);
            $return['cities_or_countries']  = $this->getCitiesCountriesTag($request);

            return ApiResponse::create($return);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/reports/get-place-info/{id}",
     *   tags={"Travelogs"},
     *   summary="Get place info",
     *   operationId="get_place_info",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="id",
     *        description="Place id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *       response=403,
     *       description="Forbidden"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function getPlaceInfo($id, Request $request)
    {
        try {
            if ($id) {
                $place_id = $id;
                $place_info = array();

                $place = Place::find($place_id);

                $place_info['id'] = $place_id;
                $place_info['title'] =  @$place->transsingle->title;
                $place_info['image'] = (isset($place->getMedias[0]->url)) ? S3_BASE_URL . 'th1100/' . $place->getMedias[0]->url : url(PLACE_PLACEHOLDERS);
                $place_info['type'] = do_placetype(@$place->place_type);
                $place_info['address'] = @$place->transsingle->address;
                $place_info['rating'] = @$place->rating;
                $place_info['visited_list'] = $this->_getPlaceReviews($place);
                $place_info['place_info'] = ['reaview_count' => (isset($place->reviews) ? count(@$place->reviews) : 0), 'place_city' => @$place->city->transsingle->title . ', ' . @$place->country->transsingle->title];

                return ApiResponse::create($place_info);
            } else {
                return ApiResponse::create(
                    [
                        'message' => ["Place Id not provided"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function getMoreCommentLikeUsers(Request $request)
    {
        try {
            $page = $request->pagenum;
            $next = ($page - 1) * 8;
            $comment_id = $request->comment_id;
            $comment = ReportsComments::find($comment_id);


            if ($comment->likes) {
                $likes = $comment->likes()->orderBy('created_at', 'DESC')->skip($next)->take(8)->get();
                return ApiResponse::create(
                    [
                        'likes' => $likes
                    ]
                );
            } else {
                return ApiResponse::create(['message' => ["likes not found"]], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function getMoreReportLikeUsers(Request $request)
    {
        try {
            $page = $request->pagenum;
            $next = ($page - 1) * 10;
            $report_id = $request->report_id;
            $report_likes = ReportsLikes::where('reports_id', $report_id)->orderBy('created_at', 'DESC')->skip($next)->take(10)->get();
            if (count($report_likes) > 0) {
                $data = [];
                foreach ($report_likes as $like) {
                    $data[] = $like;
                }

                return ApiResponse::create($data);
            } else {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }



    /**
     * @OA\Post(
     ** path="/reports/{report_id}/publish",
     *   tags={"Travelogs"},
     *   summary="Publish travelog",
     *   operationId="publish_travelog",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="report_id",
     *        description="report_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function postPublish(Request $request, ReportsService $reportsService, TranslationService $translationService, $report_id)
    {
        try {
            $current_report = Reports::findOrFail($report_id);
            if (!($current_report && $current_report->report_draft)) {
                return ApiResponse::__createBadResponse('Report not found');
            }
            $report_draft = $current_report->report_draft;

            ReportsInfos::where('reports_id', $current_report->id)->delete();
            $d_report_info = ReportsDraftInfos::where('reports_draft_id', $report_draft->id)->get();

            foreach ($d_report_info as $report_info) {
                $r_info = new ReportsInfos;
                $r_info->reports_id = $current_report->id;
                $r_info->var = $report_info->var;
                $r_info->val = $report_info->val;
                $r_info->order = $report_info->order;
                $r_info->language_id = $translationService->getLanguageId($report_info->val);
                $r_info->save();
            }

            // publish report
            $current_report->flag = 1;
            $current_report->users_id = $report_draft->users_id;
            $current_report->countries_id = $report_draft->countries_id;
            $current_report->cities_id = $report_draft->cities_id;
            $current_report->title = $report_draft->title;
            $current_report->description = $report_draft->description;
            $current_report->published_date = date('Y-m-d H:i:s');
            $current_report->language_id  = $translationService->getLanguageId($report_draft->title . ' ' . $report_draft->description);
            $current_report->save();

            if ($current_report->save()) {
                // publish places & locations(countries & cities)
                $reportsService->publishedPlace($current_report);
                $reportsService->publishedLocation($current_report->id);

                // publish report activity log
                $action = ActivityLog::query()->where([
                    'type' => PrimaryPostService::TYPE_REPORT,
                    'variable' => $current_report->id,
                    'users_id' => auth()->user()->id
                ])->whereIn('action', ['create', 'update'])->exists() ? 'update' : 'create';
                log_user_activity(PrimaryPostService::TYPE_REPORT, $action, $current_report->id);

                return ApiResponse::__create('successfully published');
            } else {
                return ApiResponse::__createBadResponse('please try again.');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    public function trendingDestinations($page)
    {
        $reportsService = app(ReportsService::class);
        $top_places = $reportsService->getTrendingDestination($page);
        $places = collect($top_places)->map(function ($place) {
            return [
                'id'                => $place->id,
                'title'             => @$place->transsingle->title ?? null,
                'description'       => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                'img'               => check_place_photo(@$place->getMedias[0]->url, 180),
                'total_followers'   => count($place->followers),
                'place_type'        => do_placetype(@$place->place_type ?: 'Event'),
                'city'              => @$place->city->transsingle->title ?? null,
                'country'           => @$place->city->country->transsingle->title ?? null,
                'total_reports'     => Reports::query()
                    ->whereHas('places', function ($q) use ($place) {
                        $q->where('places_id', $place->id);
                    })
                    ->orWhereHas('location', function ($q) use ($place) {
                        $q->where(['location_id' => $place->cities_id, 'location_type' => 'city']);
                        $q->orWhere(['location_id' => $place->countries_id, 'location_type' => 'country']);
                    })
                    ->count()
            ];
        })->toArray();
        return collect($places);
    }

    /**
     * @OA\Get(
     ** path="/reports/search-report-plan",
     *   tags={"Travelogs"},
     *   summary="Search report plan",
     *   operationId="saearch_report_plan",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="filter",
     *        description="Search query",
     *        required=false,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="type",
     *        description="Search reports type: mine | invited",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param SearchReportPlanRequest $request
     * @return \IlluminRequestate\Http\Response
     */
    public function searchReportPlan(SearchReportPlanRequest $request)
    {
        try {
            $filter = $request->filter;
            $type = $request->type;
            $data = [];
            $plan_count = 0;
            $authUser = auth()->user();

            $plan_query = TripPlans::with('versions');

            if (isset($type) && $type == 'invited') {
                $plan_query->whereHas('contribution_requests', function ($q) use ($authUser) {
                    $q->where('users_id', $authUser->id);
                });
            } else if (isset($type) && $type == 'mine') {
                $plan_query->where('users_id', $authUser->id)->where('active', 1);
            } else {
                $plan_query->where('users_id', $authUser->id)->where('active', 1)
                    ->orWhereHas('contribution_requests', function ($q) use ($authUser) {
                        $q->where('users_id', $authUser->id);
                    });
            }

            if ($filter) {
                $plan_query->where('title', 'like', '%' . $filter . '%');
            }

            $plan_list = (clone $plan_query)->take(10)->get();
            $plan_count = (clone $plan_query)->count();
            if (count($plan_list) > 0) {
                foreach ($plan_list as $plan) {
                    $plan->photo;
                    $plan->_attribute()->each(function ($value, $key) use (&$plan) {
                        $plan->$key = $value;
                    });
                    unset($plan->published_places, $plan->activeversion, $plan->trips_cities);
                    $data[] = $plan;
                }
            }

            return ApiResponse::create(
                [
                    'count' => $plan_count,
                    'plans' => $data
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
