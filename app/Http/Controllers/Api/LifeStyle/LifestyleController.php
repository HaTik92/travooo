<?php

namespace App\Http\Controllers\Api\LifeStyle;

/* Dependencies */
use App\Http\Requests\Api\Lifestyles\LifestylesGetRequest;
use App\Models\Access\language\Languages;
use App\Models\Lifestyle\ApiLifestyle as Lifestyle;
use Illuminate\Routing\Controller;

/**
 * Class Api/LifestyleController.
 */
class LifestyleController extends Controller {

    public function __construct() {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate','get_lifestyles']]);
    }

    /**
     * @param LifestylesGetRequest $request
     * @return array
     */
    public function get_lifestyles(LifestylesGetRequest $request){

        $lifestyles = null;
        $languageId = $request->input('language_id');
        $query      = $request->input('query') ? $request->input('query') : null;
        $limit      = $request->input('limit') ? $request->input('limit') : 20;
        $offset     = $request->input('offset') ? $request->input('offset') : 0;

        $language = Languages::where('id', $languageId)->first();
        if(!$language) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Language ID',
                ],
                'success' => false
            ];
        }

        $lifestyles = Lifestyle::select('conf_lifestyles.id as lId','conf_lifestyles_trans.title');

        $lifestyles = $lifestyles->join('conf_lifestyles_trans', 'conf_lifestyles.id', '=', 'conf_lifestyles_trans.lifestyles_id')->where(['languages_id' => $languageId]);

        if(!empty($query)){
            $lifestyles = $lifestyles->where('title', 'REGEXP', $query);
        }

        $lifestyles = $lifestyles->orderBy('title', 'asc')->offset($offset)->limit($limit);
        $lifestyles = $lifestyles->get();
        
        $lifestylesArr = [];

        foreach ($lifestyles as $key => $value) {
            $lifestylesArr[] = $value->getResponse();
        }

        return [
            'success' => true,
            'code'    => 200,
            'data'    => $lifestylesArr
        ];
    }
}