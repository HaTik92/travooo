<?php

namespace App\Http\Controllers\Api;


use App\Services\Newsfeed\HomeService;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Http\Requests\Api\GlobalTripPlannerFetchRequest;
use App\Models\TripPlans\TripPlans;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripsLikes;
use App\Models\TripPlans\TripsShares;
use App\Services\Trips\TripsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Support\Facades\DB;
use App\Models\User\UsersContentLanguages;
use App\Services\Trips\GlobalTripsService;
use App\Services\Trips\TripInvitationsService;

class GlobalTripController extends Controller
{
    public $tripsService, $tripsSuggestionsService;

    public function __construct(TripsService $tripsService, TripsSuggestionsService $tripsSuggestionsService)
    {
        if (in_array(request()->route()->uri, optionalAuthRoutes(self::class))) {
            if (request()->bearerToken()) {
                $this->middleware('auth:api');
            }
        }
        $this->tripsService = $tripsService;
        $this->tripsSuggestionsService = $tripsSuggestionsService;
    }

    /**
     * @OA\Get(
     ** path="/global-trip",
     *   tags={"Global Trip Planner"},
     *   summary="[Login optional] : Get Trips : https://app.zeplin.io/project/5e8dc98975eb98226f385503/screen/600ecb1badff3ab45e04f05d",
     *   operationId="app\Http\Controllers\Api\ReportsController@index",
     *  @OA\Parameter(
     *      name="per_page",
     *      description="per page items | default : 5",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="integer"
     *      )
     * ),
     * @OA\Parameter(
     *      name="page",
     *      description="pass page no | default : 1",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="integer"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="order",
     *      description="Filter => order values [ new, old, popular, upcoming ] | default : new",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="type",
     *      description="[ my | all ] default : all",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="trip_type",
     *      description="[ all | memory | upcoming ] default : all",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="search_text",
     *      description="Question Tab => Search query",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     * ),
     * @OA\Parameter(
     *        name="cities",
     *        description="comma separated",
     *        in="query",
     *        @OA\Schema(
     *           type="string",
     *        ),
     *     ),
     *  @OA\Parameter(
     *        name="countries",
     *        description="comma separated",
     *        in="query",
     *        @OA\Schema(
     *           type="string",
     *        ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function index(GlobalTripPlannerFetchRequest $request)
    {
        try {
            $countries             = collect(explode(",", trim(trim($request->get('countries', ''), ","))))->filter(function ($val) {
                return $val != null || $val != "";
            })->toArray();
            $cities             = collect(explode(",", trim(trim($request->get('cities', ''), ","))))->filter(function ($val) {
                return $val != null || $val != "";
            })->toArray();

            $search             = trim($request->get('search_text', ''));
            $order              = trim($request->get('order', 'popular'));
            $base_filter        = trim($request->get('type', 'all'));
            $trip_type          = trim($request->get('trip_type', 'all'));
            $per_page           = $request->get('per_page', 5);
            $authUser           = auth()->user();
            $page               = $request->page ?? 1;
            $skip               = ($page - 1) * $per_page;

            $per_page           = (int) $per_page;
            $page               = (int) $page;

            if (!$authUser && $base_filter == "my") {
                return ApiResponse::__createBadResponse("login must be required for showing your trips.");
            }

            if ($request->has('langugae_id')) {
                $langugae_id = (int) $request->langugae_id;
            } else {
                $langugae_id = getDesiredLanguage();
                if (isset($langugae_id[0])) {
                    $langugae_id = $langugae_id[0];
                } else {
                    $langugae_id = UsersContentLanguages::DEFAULT_LANGUAGES;
                }
            }

            $query = TripPlans::query()
                ->where('language_id', $langugae_id)
                ->whereNotIn('users_id', blocked_users_list())
                ->where('active', 1)
                // ->whereRaw("EXISTS (SELECT * FROM `users` WHERE `trips`.`users_id` = `users`.`id` AND `users`.`deleted_at` IS NULL) AND EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `deleted_at` IS NULL AND `trips_places`.`deleted_at` IS NULL)")
                ->when(strlen($search), function ($query) use ($search) {
                    return $query->where(function ($q) use ($search) {
                        $q->where('title', 'like', "%" . $search . "%")
                            ->orWhere('description', 'like', "%" . $search . "%")
                            ->orWhereHas('trips_places', function ($planQuery) use ($search) {
                                $planQuery->whereHas('place', function ($placeQuery) use ($search) {
                                    $placeQuery->whereHas('transsingle', function ($transQuery) use ($search) {
                                        $transQuery->where('title', 'like', "%" . $search . "%");
                                    });
                                });
                            });
                    });
                })
                ->when($authUser && $base_filter == "my", function ($q) use ($authUser) {
                    return $q->where('users_id', $authUser->id);
                }, function ($q) use ($authUser, $langugae_id) {
                    $tripInvitationsService = app(TripInvitationsService::class);
                    $friends = $tripInvitationsService->findFriends();
                    $q->where('active', 1)
                        ->where('language_id', $langugae_id)
                        ->where(function ($q) use ($friends, $authUser) {
                            $q->where(function ($q) use ($friends) {
                                $q->whereIn('users_id', $friends);
                                $q->where('privacy', TripsService::PRIVACY_FRIENDS);
                            });
                            $q->orWhere('privacy', TripsService::PRIVACY_PUBLIC);
                            $q->orWhere('users_id', $authUser->id);
                        });
                })
                ->when($trip_type != "all", function ($q) use ($trip_type) {
                    return $q->where('memory', ($trip_type == "memory") ? 1 : 0);
                })
                ->when($order == 'upcoming', function ($q) {
                    return $q->whereHas('versions', function ($_q) {
                        $_q->where('start_date', '>=', Carbon::today());
                    });
                });

            if (count($countries)) {
                $query->whereHas('trips_places', function ($query) use ($countries) {
                    $query->whereIn('countries_id', $countries);
                });
            }

            if (count($cities)) {
                $query->whereHas('trips_places', function ($query) use ($cities) {
                    $query->whereIn('cities_id', $cities);
                });
            }

            $total_trips = (clone $query)->count();

            $trips = (clone $query)->with('author')
                ->when($order != 'upcoming', function ($q) use ($order) {
                    switch ($order) {
                        case 'popular':
                            $q->select('*', DB::raw('(COALESCE(comment_count, 0) + COALESCE(steps_likes_count, 0) + COALESCE(steps_shares_count, 0) + COALESCE(medias_likes_count, 0) + COALESCE(medias_comments_count, 0) + COALESCE(medias_shares_count, 0) + COALESCE(sum(like_count), 0) + COALESCE(sum(shares_count), 0))  as avg_count'))
                                ->leftJoin(DB::raw('(SELECT `trips_id`, sum(c_count) as comment_count FROM `trips_places` LEFT JOIN (SELECT trip_place_id, count(*) as c_count FROM trips_places_comments GROUP by trip_place_id) as tps_comment ON tps_comment.trip_place_id=trips_places.id GROUP by trips_id) as tps'), function ($join) {
                                    $join->on('trips.id', '=', 'tps.trips_id');
                                })
                                ->leftJoin(DB::raw('(SELECT trips_id, count(*) as like_count FROM trips_likes GROUP by trips_id) as tpl'), function ($join) {
                                    $join->on('trips.id', '=', 'tpl.trips_id');
                                })
                                ->leftJoin(DB::raw('(SELECT trip_id, count(*) as shares_count FROM trips_shares GROUP by trip_id) as tsh'), function ($join) {
                                    $join->on('trips.id', '=', 'tsh.trip_id');
                                })
                                ->leftJoin(DB::raw('(SELECT `trips_id`, sum(l_count) as steps_likes_count FROM `trips_places` LEFT JOIN (SELECT places_id, count(*) as l_count FROM trips_likes where places_id is not NULL GROUP by places_id) as tps_likes ON tps_likes.places_id=trips_places.id GROUP by trips_id) as tsl'), function ($join) {
                                    $join->on('trips.id', '=', 'tsl.trips_id');
                                })
                                ->leftJoin(DB::raw('(SELECT `trips_id`, sum(tpsh_count) as steps_shares_count FROM `trips_places` LEFT JOIN (SELECT trip_place_id, count(*) as tpsh_count FROM trip_places_shares GROUP by trip_place_id) as tp_shares ON tp_shares.trip_place_id=trips_places.id GROUP by trips_id) as tssh'), function ($join) {
                                    $join->on('trips.id', '=', 'tssh.trips_id');
                                })
                                ->leftJoin(DB::raw('(SELECT `trips_id`, sum(ml_count) as medias_likes_count FROM `trips_medias` LEFT JOIN (SELECT medias_id, count(*) as ml_count FROM medias_likes GROUP by medias_id) as tm_likes ON tm_likes.medias_id=trips_medias.medias_id GROUP by trips_id) as tml'), function ($join) {
                                    $join->on('trips.id', '=', 'tml.trips_id');
                                })
                                ->leftJoin(DB::raw('(SELECT `trips_id`, sum(msh_count) as medias_shares_count FROM `trips_medias` LEFT JOIN (SELECT trip_media_id, count(*) as msh_count FROM trip_medias_shares GROUP by trip_media_id) as m_shares ON m_shares.trip_media_id=trips_medias.id GROUP by trips_id) as msh'), function ($join) {
                                    $join->on('trips.id', '=', 'msh.trips_id');
                                })
                                ->leftJoin(DB::raw('(SELECT `trips_id`, sum(mc_count) as medias_comments_count FROM `trips_medias` LEFT JOIN (SELECT medias_id, count(*) as mc_count FROM medias_comments GROUP by medias_id) as tm_comments ON tm_comments.medias_id=trips_medias.medias_id GROUP by trips_id) as tmc'), function ($join) {
                                    $join->on('trips.id', '=', 'tmc.trips_id');
                                })->groupBy('trips.id')->orderBy('avg_count', 'DESC');
                            break;

                        case 'old':
                            $q->orderBy('created_at', 'asc');
                            break;

                        default:
                            $q->orderBy('created_at', 'desc');
                            break;
                    }
                })->skip($skip)->take($per_page)->get();


            $has_more  = true;
            if (count($trips) == 0) {
                if ($langugae_id == UsersContentLanguages::DEFAULT_LANGUAGES) {
                    $has_more  = false;
                } else {
                    $newRequest = new GlobalTripPlannerFetchRequest;
                    $params = $request->all();
                    $params['langugae_id'] = UsersContentLanguages::DEFAULT_LANGUAGES;
                    $newRequest->merge($params);
                    return $this->index($newRequest);
                }
            }


            $trips = $trips->map(function ($trip) use ($authUser, $order) {
                if ($trip->author) {
                    $trip->author->profile_picture = check_profile_picture($trip->author->profile_picture);
                }
                if ($order == "popular") {
                    unset($trip->trips_id, $trip->comment_count, $trip->like_count, $trip->trip_id, $trip->shares_count, $trip->steps_likes_count, $trip->steps_shares_count, $trip->medias_likes_count, $trip->medias_shares_count,  $trip->medias_comments_count, $trip->avg_count);
                }

                if ($trip->published_places) {
                    $trip->_attribute()->each(function ($value, $key) use (&$trip) {
                        $trip->$key = $value;
                    });

                    $places = [];
                    $countries = [];
                    $cities = [];
                    $total_destination = 0;
                    $_totalDestination = ['city_wise' => [], 'country_wise' => []];

                    foreach ($trip->published_places as $tripPlace) {
                        if (!$tripPlace->place or !$tripPlace->country or !$tripPlace->city) {
                            continue;
                        }
                        if (!isset($places[$tripPlace->places_id])) {
                            $total_destination++;
                            $_totalDestination['city_wise'][$tripPlace->cities_id][$tripPlace->places_id] = $tripPlace->place;
                            $_totalDestination['country_wise'][$tripPlace->countries_id][$tripPlace->places_id] = $tripPlace->place;
                            $places[$tripPlace->places_id] = [
                                'id' => $tripPlace->places_id,
                                'country_id' => $tripPlace->countries_id,
                                'city_id' => $tripPlace->cities_id,
                                'title' => isset($tripPlace->place->transsingle->title) ? $tripPlace->place->transsingle->title : null,
                                'lat' => $tripPlace->place->lat,
                                'lng' => $tripPlace->place->lng,
                                'image' => check_place_photo($tripPlace->place),
                            ];
                        }
                        if (!isset($countries[$tripPlace->countries_id])) {
                            $countries[$tripPlace->countries_id] = [
                                'id' => $tripPlace->countries_id,
                                'title' => isset($tripPlace->country->trans[0]->title) ? $tripPlace->country->trans[0]->title : null,
                                'lat' => $tripPlace->country->lat,
                                'lng' => $tripPlace->country->lng,
                                'image' => get_country_flag($tripPlace->country),
                                'total_destination' => 0,
                            ];
                        }
                        if (!isset($cities[$tripPlace->cities_id])) {
                            $cities[$tripPlace->cities_id] = [
                                'id' => $tripPlace->cities_id,
                                'country_id' => $tripPlace->countries_id,
                                'title' => isset($tripPlace->city->trans[0]->title) ? $tripPlace->city->trans[0]->title : null,
                                'lat' => $tripPlace->city->lat,
                                'lng' => $tripPlace->city->lng,
                                'image' => isset($tripPlace->city->medias[0]->media->path) ?
                                    check_city_photo($tripPlace->city->medias[0]->media->path) :
                                    url(PLACE_PLACEHOLDERS),
                                'total_destination' => 0,
                            ];
                        }
                    }
                    try {
                        $trip->static_map_image = tripPlanThumb($trip, 615, 400);
                    } catch (\Throwable $e) {
                        $trip->static_map_image = tripPlanStaticImage($trip);
                    }


                    foreach ($countries as $country_id => &$country) {
                        $country['total_destination'] = isset($_totalDestination['country_wise'][$country_id]) ? count($_totalDestination['country_wise'][$country_id]) : 0;
                    }
                    foreach ($cities as $city_id =>  &$city) {
                        $city['total_destination'] = isset($_totalDestination['city_wise'][$city_id]) ? count($_totalDestination['city_wise'][$city_id]) : 0;
                    }
                    $country_title =  isset(array_values($countries)[0]['title']) ? array_values($countries)[0]['title'] : null;
                    $trip->total_destination = $total_destination;
                    if (count($countries) > 1) {
                        $trip->type = "multiple-country";
                        $trip->data = array_values($countries);
                    } else {
                        if (count($cities) > 1) {
                            $trip->type = "multiple-city";
                            $trip->country_title = $country_title;
                            $trip->data = array_values($cities);
                        } else {
                            $trip->type = "single-city";
                            $trip->country_title = $country_title;
                            $trip->data = array_values($places);
                        }
                    }

                    $trip->like_flag   = $authUser ? (TripsLikes::where('trips_id', $trip->id)->where('users_id', $authUser->id)->exists()) : false;
                    $trip->total_likes = $trip->likes()->count();
                    $trip->total_shares = TripsShares::where('trip_id', $trip->id)->count();

                    // For Comment
                    $trip = app(HomeService::class)->__addPostCommentSection($trip);
                    unset($trip->published_places, $trip->contribution_requests, $trip->likes);
                    // $trip_plan->_totalDestination = $_totalDestination;
                    // $trip_plan->_places = $places;
                    // $trip_plan->_countries = $countries;
                    // $trip_plan->_cities = $cities;
                }
                unset($trip->activeversion, $trip->trips_cities);

                return $trip;
            });

            $feeds =  $trips;

            $result = DB::select("SELECT 
                    (SELECT COUNT(DISTINCT countries_id) FROM trips_places WHERE versions_id = (SELECT trips.active_version FROM trips WHERE trips.id = trips_places.trips_id AND deleted_at IS NULL) AND deleted_at IS NULL AND countries_id IS NOT NULL)  AS total_countries,
                    (SELECT COUNT(DISTINCT cities_id) AS countries FROM trips_places WHERE versions_id = (SELECT trips.active_version FROM trips WHERE trips.id = trips_places.trips_id AND deleted_at IS NULL) AND deleted_at IS NULL AND cities_id IS NOT NULL) AS total_cities")[0];

            $response['counters'] = ['countries' => $result->total_countries, 'cities' => $result->total_cities];
            $response['counters']['all'] = $response['counters']['countries'] + $response['counters']['cities'];

            $response['is_authorized_user'] = $authUser ? true : false;
            $response['total']              = $total_trips;
            $response['has_more']           = $has_more;
            $response['langugae_id']        = $langugae_id;
            $response['per_page']           = $per_page;
            // $response['total_page']         = ceil($total_trips / $per_page);
            $response['current_page']       = $page;
            $response['data']               = $feeds;

            return ApiResponse::create($response);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/global-trip/trending-trips",
     *   tags={"Global Trip Planner"},
     *   summary="[Login optional] : Get Trending Trips Slider",
     *   operationId="app\Http\Controllers\Api\GlobalTripController@topTrips",
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function topTrips(GlobalTripsService $globalTripsService)
    {
        try {

            $top_trips = $globalTripsService->getTopTripPlans(true);

            if ($top_trips->count() == 0) {
                $top_trips = $globalTripsService->getTopTripPlans(false);
            }

            $data = [];
            foreach ($top_trips as $trip) {
                $flag = false;
                if ($trip->privacy == TripsService::PRIVACY_PRIVATE && $trip->contribution_requests()->where('users_id', auth()->id())->where('status', TripsSuggestionsService::SUGGESTION_APPROVED_STATUS)->exists()) {
                    $flag = true;
                } elseif ($trip->privacy == TripsService::PRIVACY_FRIENDS || $trip->privacy == TripsService::PRIVACY_PUBLIC) {
                    $flag = true;
                }
                if ($flag) {
                    if ($trip->author) {
                        $trip->author->profile_picture = check_profile_picture($trip->author->profile_picture);
                    }
                    if ($trip->published_places) {
                        $trip->_attribute()->each(function ($value, $key) use (&$trip) {
                            $trip->$key = $value;
                        });
                        // $trip->total_distance = $trip->_attribute(TripPlans::ATTRIBUTE_DISTANCE);
                        // $trip->total_duration = $trip->_attribute(TripPlans::ATTRIBUTE_DURATION);
                        // $trip->total_budget = $trip->_attribute(TripPlans::ATTRIBUTE_BUDGET);
                        // $trip->start_date = $trip->_attribute(TripPlans::ATTRIBUTE_STARTING_DATE);


                        // $trip->total_travalers = $trip->join_requests->going->count();
                        // $trip->latest_travalers = $trip->join_requests->going->with('author')->whereHas('author')->orderBy('id', 'DESC')->limit(3)->get()->map(function ($tmu) {
                        //     return check_profile_picture($tmu->author->profile_picture);
                        // });

                        $trip->total_travalers = 0;
                        $trip->latest_travalers = [];


                        $places = [];
                        $countries = [];
                        $cities = [];
                        $total_destination = 0;
                        $_totalDestination = ['city_wise' => [], 'country_wise' => []];

                        foreach ($trip->published_places as $tripPlace) {
                            if (!$tripPlace->place or !$tripPlace->country or !$tripPlace->city) {
                                continue;
                            }
                            if (!isset($places[$tripPlace->places_id])) {
                                $total_destination++;
                                $_totalDestination['city_wise'][$tripPlace->cities_id][$tripPlace->places_id] = $tripPlace->place;
                                $_totalDestination['country_wise'][$tripPlace->countries_id][$tripPlace->places_id] = $tripPlace->place;
                                $places[$tripPlace->places_id] = [
                                    'id' => $tripPlace->places_id,
                                    'country_id' => $tripPlace->countries_id,
                                    'city_id' => $tripPlace->cities_id,
                                    'title' => isset($tripPlace->place->transsingle->title) ? $tripPlace->place->transsingle->title : null,
                                    'lat' => $tripPlace->place->lat,
                                    'lng' => $tripPlace->place->lng,
                                    'image' => check_place_photo($tripPlace->place),
                                ];
                            }
                            if (!isset($countries[$tripPlace->countries_id])) {
                                $countries[$tripPlace->countries_id] = [
                                    'id' => $tripPlace->countries_id,
                                    'title' => isset($tripPlace->country->trans[0]->title) ? $tripPlace->country->trans[0]->title : null,
                                    'lat' => $tripPlace->country->lat,
                                    'lng' => $tripPlace->country->lng,
                                    'image' => get_country_flag($tripPlace->country),
                                    'total_destination' => 0,
                                ];
                            }
                            if (!isset($cities[$tripPlace->cities_id])) {
                                $cities[$tripPlace->cities_id] = [
                                    'id' => $tripPlace->cities_id,
                                    'country_id' => $tripPlace->countries_id,
                                    'title' => isset($tripPlace->city->trans[0]->title) ? $tripPlace->city->trans[0]->title : null,
                                    'lat' => $tripPlace->city->lat,
                                    'lng' => $tripPlace->city->lng,
                                    'image' => isset($tripPlace->city->medias[0]->media->path) ?
                                        check_city_photo($tripPlace->city->medias[0]->media->path) :
                                        url(PLACE_PLACEHOLDERS),
                                    'total_destination' => 0,
                                ];
                            }
                        }
                        try {
                            $trip->static_map_image = tripPlanThumb($trip, 615, 400);
                        } catch (\Throwable $e) {
                            $trip->static_map_image = tripPlanStaticImage($trip);
                        }

                        foreach ($countries as $country_id => &$country) {
                            $country['total_destination'] = isset($_totalDestination['country_wise'][$country_id]) ? count($_totalDestination['country_wise'][$country_id]) : 0;
                        }
                        foreach ($cities as $city_id =>  &$city) {
                            $city['total_destination'] = isset($_totalDestination['city_wise'][$city_id]) ? count($_totalDestination['city_wise'][$city_id]) : 0;
                        }
                        $country_title =  isset(array_values($countries)[0]['title']) ? array_values($countries)[0]['title'] : null;
                        $trip->total_destination = $total_destination;
                        if (count($countries) > 1) {
                            $trip->type = "multiple-country";
                            $trip->data = array_values($countries);
                        } else {
                            if (count($cities) > 1) {
                                $trip->type = "multiple-city";
                                $trip->country_title = $country_title;
                                $trip->data = array_values($cities);
                            } else {
                                $trip->type = "single-city";
                                $trip->country_title = $country_title;
                                $trip->data = array_values($places);
                            }
                        }

                        unset($trip->activeversion, $trip->published_places, $trip->trips_cities, $trip->join_requests, $trip->_trend);
                        $data[] = $trip;
                    }
                }
            }
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Get(
     ** path="/global-trip/trending-destinations",
     *   tags={"Global Trip Planner"},
     *   summary="[Login optional] : Get Trending Destinations Slider",
     *   operationId="app\Http\Controllers\Api\GlobalTripController@trendingDestinations",
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function trendingDestinations()
    {
        try {
            $countriesIds = TripPlans::query()->select('trips_places.countries_id')
                ->selectRaw('count(DISTINCT CONCAT(trips_places.countries_id, \' \', trips.id)) as countries_count')
                ->join('trips_places', function ($q) {
                    $from = Carbon::now()->subDay(90);
                    $to = Carbon::now()->addDay(90);
                    $q->on('trips.active_version', 'trips_places.versions_id')
                        ->where('trips_places.time', '<=', $to)
                        ->where('trips_places.time', '>=', $from)
                        ->where('trips_places.cities_id', '!=', 2031)
                        ->whereNull('trips_places.deleted_at');
                })
                ->where('active_version', '!=', 0)
                ->groupBy('countries_id')
                ->orderByDesc('countries_count')->take(15)->get();

            $citiesIds = TripPlans::query()->select('trips_places.cities_id')
                ->selectRaw('count(DISTINCT CONCAT(trips_places.cities_id, \' \', trips.id)) as cities_count')
                ->join('trips_places', function ($q) {
                    $from = Carbon::now()->subDay(90);
                    $to = Carbon::now()->addDay(90);
                    $q->on('trips.active_version', 'trips_places.versions_id')
                        ->where('trips_places.time', '<=', $to)
                        ->where('trips_places.time', '>=', $from)
                        ->where('trips_places.cities_id', '!=', 2031)
                        ->whereNull('trips_places.deleted_at');
                })
                ->where('active_version', '!=', 0)
                ->groupBy('cities_id')
                ->orderByDesc('cities_count')->take(15)->get();

            $locations = [];
            $countries_ids = [];
            $cities_ids = [];

            foreach ($countriesIds as $co) {
                if (!in_array($co->countries_id, $countries_ids)) {
                    $countryDetails = Countries::with('transsingle')->where('id', $co->countries_id)->first();
                    if ($countryDetails) {
                        $locations[] = [
                            'id'  => $countryDetails->id,
                            'type' => 'country',
                            'total_no_of_trips' => TripPlaces::where('countries_id', $countryDetails->id)->groupBy('trips_id')->count(),
                            'image' => get_country_flag($countryDetails),
                            'title' => @$countryDetails->transsingle->title,
                        ];
                        $countries_ids[] = $co->countries_id;
                    }
                }
            }

            foreach ($citiesIds as $ci) {
                if (!in_array($ci->cities_id, $cities_ids)) {
                    $cityDetails = Cities::with('transsingle', 'first_city_medias')->where('id', $ci->cities_id)->first();
                    if ($cityDetails) {
                        $locations[] = [
                            'id'  => $cityDetails->id,
                            'type' => 'city',
                            'total_no_of_trips' => TripPlaces::where('cities_id', $cityDetails->id)->groupBy('trips_id')->count(),
                            'image' =>  check_city_photo(@$cityDetails->first_city_medias[0]->url, 180),
                            'title' => @$cityDetails->transsingle->title,
                        ];
                        $cities_ids[] = $ci->cities_id;
                    }
                }
            }

            if (count($locations)) $locations = array_values(collect($locations)->sortByDesc('total_no_of_trips')->toArray());

            return ApiResponse::create($locations);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    // /**
    //  * @return \Illuminate\Http\Response
    //  */
    // public function getIndex()
    // {
    //     try {
    //         $data = array();
    //         $top_cities = [];

    //         $top_trips = TripPlans::with('versions')
    //             ->whereHas('versions', function ($q) {
    //                 $q->where('start_date', '>=', Carbon::today());
    //             })->where('active', 1)->whereNull('deleted_at')->inRandomOrder()->take(5)->get();

    //         $data['top_trips'] = $top_trips;

    //         $trip_cities = DB::table('trips')
    //             ->select('tp.cities_id', DB::raw('count(tp.trips_id) as total_cities'))
    //             ->join(DB::raw('(SELECT trips_id, cities_id,  count(trips_places.trips_id) as total FROM `trips_places` GROUP BY cities_id, trips_id) as tp'), function ($join) {
    //                 $join->on('tp.trips_id', '=', 'trips.id');
    //             })
    //             ->where('trips.active', 1)
    //             ->whereNull('trips.deleted_at')
    //             ->groupBy('tp.cities_id')
    //             ->orderBy('total_cities', 'desc')
    //             ->where('created_at', '>=', Carbon::now()->subDays(7))
    //             ->take(20)->get();


    //         if (count($trip_cities) > 7) {
    //             foreach ($trip_cities as $trip_city) {
    //                 $top_cities[] = [
    //                     'city_info' => Cities::findorfail($trip_city->cities_id),
    //                     'count' => $trip_city->total_cities
    //                 ];
    //             }
    //         }


    //         $data['top_cities'] = $top_cities;
    //         $all_plans = $this->_getTripPlans('', 10, []);
    //         $data['all_plans'] = $all_plans['plans'];

    //         $data['all_trip_count'] = $all_plans['count'];

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // /**
    //  * @param int $skip
    //  * @param int $take
    //  * @param arr $filters
    //  * @param string $sort
    //  * @return Array
    //  */
    // protected function _getTripPlans($skip, $take, $filters = [], $sort = 'newest')
    // {
    //     $query = TripPlans::query();

    //     if (isset($filters['type']) && $filters['type'] == 'mine') {
    //         $query->where('users_id', Auth::user()->id);
    //     }
    //     if (isset($filters['countries'])) {
    //         $countries = $filters['countries'];
    //         $query->whereHas('trips_places', function ($query) use ($countries) {
    //             return $query->whereIn('countries_id', $countries);
    //         });
    //     }

    //     if (isset($filters['cities'])) {
    //         $cities = $filters['cities'];
    //         $query->whereHas('trips_places', function ($query) use ($cities) {
    //             $query->whereIn('cities_id', $cities);
    //         });
    //     }


    //     $query->with('versions')->where('active', 1);

    //     switch ($sort) {
    //         case 'oldest':
    //             $query->orderBy('created_at', 'asc');
    //             break;
    //         case 'creation_date':
    //             $query->whereHas('versions', function ($q) {
    //                 $q->orderBy('start_date', 'DESC');
    //             })->groupBy('id');
    //             break;
    //         case 'upcoming':
    //             $query->whereHas('versions', function ($q) {
    //                 $q->where('start_date', '>=', Carbon::today());
    //             });
    //             break;
    //         default:
    //             $query->orderBy('created_at', 'desc');
    //     }

    //     $count = $query->count();

    //     if ($skip != '') {
    //         $query->skip($skip);
    //     }
    //     if ($take != '') {
    //         $query->take($take);
    //     }

    //     return ['count' => $count, 'plans' => $query->get()];
    // }

    // /**
    //  * @param MoreTripPlanRequest $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function getMoreTripPlan(MoreTripPlanRequest $request)
    // {
    //     try {
    //         $page = $request->pagenum;
    //         $skip = ($page - 1) * 10;
    //         $sort = $request->order;
    //         $filters = $request->filters;
    //         $all_plans = $this->_getTripPlans($skip, 10, $filters, $sort);

    //         foreach ($all_plans['plans'] as $inv) {
    //             $inv->author_name =  $inv->author->name;
    //             $inv->author_id =  $inv->author->id;
    //             $inv->duration = calculate_duration($inv->id, 'all');
    //             $inv->distance =  calculate_distance($inv->id);
    //             unset($inv->author);
    //             $place_photo = [];
    //             $trips_places = $inv->trips_places()->where('versions_id', '!=', 0)->get();
    //             foreach ($trips_places as $place) {
    //                 $place_photo[] = check_place_photo($place->place);
    //             }
    //             $inv->trips_places = $place_photo;
    //             // $inv->trips_places()->where('versions_id', '!=', 0)->get();
    //             // weatherDate($inv->version->start_date);
    //             // weatherDate($inv->version->end_date);
    //             get_plan_map($inv, 140, 150);
    //         }

    //         return ApiResponse::create([
    //             'total' => $all_plans['count'],
    //             'all_plans' => $all_plans['plans'],
    //         ]);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // /**
    //  * @param Request $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function getFilterTripPlan(Request $request)
    // {
    //     try {
    //         $sort = ($request->order != '') ? $request->order : '';

    //         $filters = $request->filters;
    //         $all_plans = $this->_getTripPlans('', 10, $filters, $sort);

    //         $count = $all_plans['count'];

    //         return ApiResponse::create(
    //             [
    //                 'count' => $count,
    //                 'all_plans' => $all_plans['plans']
    //             ]
    //         );
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // /**
    //  * @param Request $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function getSearchCountries(Request $request)
    // {
    //     try {
    //         $queryParam = $request->get('search');
    //         if ($queryParam) {
    //             $countries = array();

    //             $get_cuntries = Country::whereHas('transsingle', function ($query) use ($queryParam) {
    //                 $query->where('title', 'like', "%" . $queryParam . "%");
    //             })->take(20)->get();

    //             foreach ($get_cuntries as $country) {
    //                 $countries[] = [
    //                     'id' => $country->id,
    //                     'country_name' => $country->transsingle->title,
    //                     'image' => check_country_photo(@$country->getMedias[0]->url, 180)
    //                 ];
    //             }

    //             return ApiResponse::create([
    //                 'countries' => $countries
    //             ]);
    //         } else {
    //             return ApiResponse::create(
    //                 [
    //                     'search' => ['Search Term not provided']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }
}
