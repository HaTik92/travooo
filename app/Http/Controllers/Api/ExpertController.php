<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Http\Responses\ApiResponse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\TripPlans\TripPlans;

class ExpertController extends Controller
{
    public function getIndex() {
        try {
            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->get();
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
