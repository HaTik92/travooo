<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Responses\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Http\Requests\Api\User\ChangePasswordRequest;
use App\Http\Requests\Api\User\StepFourExpertRequest;
use App\Models\User\User;
use App\Http\Requests\Api\Settings\AccountSettingRequest;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\TripPlans\TripPlans;
use App\Models\Country\Countries;
use App\Models\DeleteAccount\DeleteReasons;
use App\Models\Ranking\RankingUsersTransactions;
use App\Models\User\DeleteAccountRequest;
use App\Models\User\UsersContentLanguages;
use App\Models\User\UsersPrivacy;
use App\Models\User\UsersTravelStyles;
use App\Notifications\DeleteAccountRequestNotification;
use App\Services\Api\UserProfileService;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Agent\Agent;

class SettingsController extends Controller
{

    public function __construct()
    {
    }

    /**
     * @OA\GET(
     ** path="/setting/{user_id}/agent-info",
     *   tags={"Setting"},
     *   summary="agentInfo",
     *   operationId="Api\SettingsController@agentInfo",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function agentInfo(Request $request, $user_id)
    {
        try {
            $agent = new Agent();
            $user = auth()->user();
            $data['aget_info'] = [
                'browser' => $agent->browser(),
                'platform' => $agent->platform(),
                'user_ip' => $request->ip(),
                'last_accessed' => ($user->last_login) ? \Carbon\Carbon::parse($user->last_login)->format('d M, Y') : \Carbon\Carbon::parse($user->updated_at)->format('d M, Y'),
                'signed_in' => \Carbon\Carbon::parse($user->created_at)->format('d M, Y')
            ];
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Put(
     ** path="/setting/{user_id}/social-links",
     *   tags={"Setting"},
     *   summary="Update Auth User Social Links",
     *   operationId="Api\SettingsController@postSocial",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\RequestBody(
     *        @OA\MediaType(
     *        mediaType="application/json",
     *        @OA\Schema(
     *              example={
     *                  "facebook" : "",
     *                  "twitter" : "",
     *                  "instagram" : "",
     *                  "pinterest" : "",
     *                  "tripadvisor" : "",
     *                  "youtube" : "",
     *                  "medium" : "",
     *                  "website" : "",
     *                  "vimeo" : "",
     *              },
     *        )
     *     )
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param StepFourExpertRequest $request
     * @return ApiResponse
     */
    public function postSocial(StepFourExpertRequest $request, UserProfileService $userProfileService, $user_id)
    {
        try {
            if ($userProfileService->saveSocialLinks(auth()->user()->id, $request)) {
                return ApiResponse::__create("Links save successfully.");
            } else {
                return ApiResponse::__createBadResponse("Social link not saved!");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     * path="/setting/{user_id}/security",
     *   tags={"Setting"},
     *   summary="Change password",
     *   operationId="Api\SettingsController@postSecurity",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="old_password",
     *        description="old password",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="new_password",
     *        description="new password",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="new_confirm_password",
     *        description="new confirm password",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Change user Password.
     *
     * @param  ChangePasswordRequest  $request
     * @return Response
     */
    public function postSecurity(Request $request, $user_id)
    {
        try {
            $user = auth()->user();
            $validator = Validator::make($request->all(), [
                'old_password' => ['required', function ($attribute, $value, $fail) use ($user) {
                    if (!\Hash::check($value, $user->password)) {
                        return $fail(__('The current password is incorrect.'));
                    }
                }],
                'new_password' => 'required|min:6|max:32|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])(?=\S+$).{6,255}$/',
                'new_confirm_password' => 'required|same:new_password'
            ], [
                'new_password.regex'                       => 'New password should contain 6 minimum characters consisting of at least 1 letter and number.',
                'new_password.min'                         => 'New password should contain 6 minimum characters consisting of at least 1 letter and number.',
            ]);
            if ($validator->fails()) {
                return ApiResponse::__createBadResponse($validator->errors()->first());
            }

            $user->forceFill([
                'password' => bcrypt($request->new_password),
                'remember_token' => Str::random(60),
            ])->save();

            return ApiResponse::__create('Password Successfully Updated!');
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/setting/{user_id}/social-links",
     *   tags={"Setting"},
     *   summary="Fetch Auth User Social Links",
     *   operationId="Api\SettingsController@getSocial",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @return ApiResponse
     */
    public function getSocial(Request $request, UserProfileService $userProfileService, $user_id)
    {
        try {
            return ApiResponse::create(
                $userProfileService->findSocialLinks(auth()->user()->id)
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Put(
     ** path="/setting/{user_id}/my-account",
     *   tags={"Setting"},
     *   summary="Update Auth User Profile Details",
     *   operationId="Api\SettingsController@postAccount",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\RequestBody(
     *        @OA\MediaType(
     *        mediaType="application/json",
     *        @OA\Schema(
     *              example={
     *                  "name" : "briz",
     *                  "username" : "briz",
     *                  "email" : "brijesh+15@travooo.com",
     *                  "nationality" : "98",
     *                  "about" : "",
     *                  "mobile" : "",
     *                  "interests" : { "Music", "Movie" },
     *                  "travelstyles" : { 2, 5 },
     *                  "content_languages" : { 1, 2 },
     *                  "expertise" : { 
     *                      { "type":"country" , "value": 98 },
     *                      { "type":"city" , "value": 78 },
     *                      { "type":"city" , "value": 15 }
     *                  }
     *              },
     *        )
     *     )
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function postAccount(Request $request, UserProfileService $userProfileService, $user_id)
    {
        try {
            $user = auth()->user();

            $validator = Validator::make($request->all(), [
                'nationality'               => 'required',
                'username'                  => 'required|string|min:4|max:15|regex:/^([A-Za-z0-9_])+$/|unique:users,username,' . $user->id,
                'email'                     => 'required|email|unique:users,email,' . $user->id,
                'interests'                 => 'sometimes|array',
                'expertise'                 => 'sometimes|array',
                'travelstyles'              => 'sometimes|array',
                'content_languages'         => 'sometimes|array',
            ], [
                'username.unique'           => 'username already exist',
                'username.regex'           =>  'invalid format of username',
                'email.unique'              => 'email already exist',
                'interests.array'           => 'invalid format of interests',
                'expertise.array'           => 'invalid format of expertise',
                'travelstyles.array'        => 'invalid format of travel styles',
                'content_languages.array'   => 'invalid format of content languages',
            ]);

            if ($validator->fails()) return ApiResponse::createValidationResponse($validator->errors());

            if ($userProfileService->saveProfile($user->id, $request)) {
                return ApiResponse::__create('Your Account Update Successfully.');
            } else {
                return ApiResponse::__createBadResponse('Your Account Can not Update!');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/setting/{user_id}/my-account",
     *   tags={"Setting"},
     *   summary="Fetch Auth User Profile Details",
     *   operationId="Api\SettingsController@getMyProfile",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getMyProfile(Request $request, UserProfileService $userProfileService, $user_id)
    {
        try {
            return ApiResponse::create(
                $userProfileService->findProfile(auth()->user()->id)
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/setting/{user_id}/block/users",
     *   tags={"Setting"},
     *   summary="Fetch Block Users List",
     *   operationId="Api\SettingsController@getBlockUsers",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getBlockUsers(Request $request, UserProfileService $userProfileService, $user_id)
    {
        try {
            return ApiResponse::create(
                $userProfileService->fetchBlockUsers(auth()->user()->id)
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Put(
     ** path="/setting/{user_id}/block-unblock/{blocking_user_id}",
     *   tags={"Setting"},
     *   summary="Block Api : Block any user",
     *   operationId="Api\SettingsController@blockUnBlockUser",
     *   security={
     *      {"bearer_token": {}
     *   }},
     *  @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="blocking_user_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function blockUnBlockUser(Request $request, UserProfileService $userProfileService, $user_id, $blocking_user_id)
    {
        try {
            if ($user_id == $blocking_user_id) {
                return ApiResponse::__createBadResponse("you can not block you");
            }
            $isAlreadyBlock = $userProfileService->isAlreadyBlock($user_id, $blocking_user_id);
            $blockOrUnBlock = $userProfileService->blockOrUnBlock($user_id, $blocking_user_id);
            if ($isAlreadyBlock && $blockOrUnBlock) {
                return ApiResponse::__create("unblock successfully.");
            } else if (!$isAlreadyBlock && $blockOrUnBlock) {
                return ApiResponse::__create("block successfully.");
            } else {
                return ApiResponse::__createBadResponse("something went wrong.");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/account-delete-reasons",
     *   tags={"Common API"},
     *   summary="Fetch Account Delete Reasons List",
     *   operationId="Api\SettingsController@getAccontDeleteReason",
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getAccontDeleteReason(Request $request)
    {
        try {
            return ApiResponse::create(
                DeleteReasons::select('id', 'title')->where('status', 1)->get()
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/setting/{user_id}/account-delete/request",
     *   tags={"Setting"},
     *   summary="Send Mail for delete account",
     *   operationId="Api\SettingsController@sendMailOfDeleteRequest",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="reason",
     *        description="reason id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function sendMailOfDeleteRequest(Request $request, $user_id)
    {
        try {
            $reason = $request->reason;
            if ($reason) {
                $deleteReasons = DeleteReasons::find($reason);
                if (!$deleteReasons) return ApiResponse::__createBadResponse('reason not found');
                $user = auth()->user();
                $deleteAccountRequest = DeleteAccountRequest::updateOrCreate(
                    [
                        'status' => DeleteAccountRequest::STATUS_SEND_MAIL,
                        'users_id' => $user->id,
                    ],
                    [
                        'delete_reason' => $reason,
                        'status' => DeleteAccountRequest::STATUS_SEND_MAIL,
                        'users_id' => $user->id,
                        'token' => str_random(DeleteAccountRequest::TOKEN_LENGTH)
                    ]
                );
                try {
                    $user->notify(
                        new DeleteAccountRequestNotification($deleteAccountRequest->token, false)
                    );
                } catch (\Throwable $e) {
                }
                return ApiResponse::__create('Mail send successfully!');
            } else {
                return ApiResponse::__createBadResponse('reason must be required');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Delete(
     ** path="/setting/{user_id}/account-delete/{token}",
     *   tags={"Setting"},
     *   summary="Delete User Account Data",
     *   operationId="Api\SettingsController@sendMailOfDeleteRequest",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="token",
     *        description="token get from deeplink",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function finalDeleteAccount(Request $request, UserProfileService $userProfileService, $user_id, $token)
    {
        try {
            $deleteAccountRequest = DeleteAccountRequest::where('token', $token)->where('status', DeleteAccountRequest::STATUS_SEND_MAIL)->first();
            if ($deleteAccountRequest) {
                $deleteAccountRequest->status = DeleteAccountRequest::STATUS_CONFIRM;
                $deleteAccountRequest->save();
                $userProfileService->deleteAccount($deleteAccountRequest->users_id);
                return ApiResponse::__create('Account Delete Successfully!');
            } else {
                return ApiResponse::__createBadResponse('Invalid Token!');
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Get(
     ** path="/setting/{user_id}/privacy",
     *   tags={"Setting"},
     *   summary="Fetch Privacy",
     *   operationId="Api\SettingsController@fetchPrivacy",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function fetchPrivacy(Request $request, $user_id)
    {
        try {
            $user = auth()->user();
            $user_privacy = UsersPrivacy::where('users_id', $user->id)->first();

            if (!$user_privacy) {
                $user_privacy = new UsersPrivacy();
                $user_privacy->users_id = $user->id;
                $user_privacy->see_my_following = UsersPrivacy::EVERYONE;
                $user_privacy->message_me = UsersPrivacy::EVERYONE;
                $user_privacy->follow_me = UsersPrivacy::EVERYONE;
                $user_privacy->find_me = UsersPrivacy::EVERYONE;
                $user_privacy->save();
            }

            return ApiResponse::create($user_privacy);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Put(
     ** path="/setting/{user_id}/privacy",
     *   tags={"Setting"},
     *   summary="Update Privacy",
     *   operationId="Api\SettingsController@putPrivacy",
     *   security={
     *      {"bearer_token": {}
     *   }},
     * @OA\Parameter(
     *        name="user_id",
     *        description="user_id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="message_me",
     *        description="message me [0 = EVERYONE, 1 = FRIENDS, 2 = FOLLOWERS]",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="follow_me",
     *        description="follow me [0 = EVERYONE, 1 = FRIENDS, 2 = FOLLOWERS]",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="find_me",
     *        description="find me [0 = EVERYONE, 1 = FRIENDS, 2 = FOLLOWERS]",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="see_my_following",
     *        description="Who Can See My Followings [0 = EVERYONE, 1 = ONLY_PEOPLE_I_FOLLOWING]",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function putPrivacy(Request $request, $user_id)
    {
        try {
            $user = auth()->user();

            $validator = Validator::make($request->all(), [
                'message_me'                    => 'required|integer|in:' . implode(",", UsersPrivacy::COMMON_PRIVACY_MAPPING),
                'follow_me'                     => 'required|integer|in:' . implode(",", UsersPrivacy::COMMON_PRIVACY_MAPPING),
                'find_me'                       => 'required|integer|in:' . implode(",", UsersPrivacy::COMMON_PRIVACY_MAPPING),
                'see_my_following'              => 'required|integer|in:' . UsersPrivacy::EVERYONE . "," . UsersPrivacy::ONLY_PEOPLE_I_FOLLOWING,
            ], [
                'message_me.in'  => 'message me value in must be list ' . implode(",", UsersPrivacy::COMMON_PRIVACY_MAPPING),
                'follow_me.in'  => 'follow me value in must be list ' . implode(",", UsersPrivacy::COMMON_PRIVACY_MAPPING),
                'find_me.in'  => 'find me value in must be list ' . implode(",", UsersPrivacy::COMMON_PRIVACY_MAPPING),
                'see_my_following.in'  => 'see my following me value in must be list ' . UsersPrivacy::EVERYONE . "," . UsersPrivacy::ONLY_PEOPLE_I_FOLLOWING,
            ]);

            if ($validator->fails()) return ApiResponse::createValidationResponse($validator->errors());

            $user_privacy = UsersPrivacy::where('users_id', $user->id)->first();

            if (!$user_privacy) {
                $user_privacy = new UsersPrivacy();
                $user_privacy->users_id = $user->id;
            }

            $user_privacy->see_my_following = $request->see_my_following;
            $user_privacy->message_me = $request->message_me;
            $user_privacy->follow_me = $request->follow_me;
            $user_privacy->find_me = $request->find_me;

            $user_privacy->save();
            return ApiResponse::__create('Save Successfully.');
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
