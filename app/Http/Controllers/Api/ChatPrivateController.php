<?php

namespace App\Http\Controllers\Api;

use App\Events\Api\TripChat\ChatNewConversationApiEvent;
use App\Events\ChatNewConversationEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Models\User\User;
use App\Models\Chat\ChatConversation;
use App\Models\Chat\ChatConversationMessage;
use App\Models\Chat\ChatConversationParticipant;
use App\Events\ChatSendPrivateMessageEvent;
use App\Services\Trips\TripInvitationsService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use stdClass;

class ChatPrivateController extends Controller
{
    /**
     * @OA\Get(
     ** path="/chat/{chat_conversation_id}/message/",
     *   tags={"Trip Chat"},
     *   summary="(FETCH_CHAT_MESSAGE_URL)",
     *   operationId="Api/ChatPrivateController@getMessages",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="chat_conversation_id",
     *        description="",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getMessages(
        Request $request,
        TripInvitationsService $tripInvitationsService,
        $chat_conversation_id
    ) {
        //Check is conversation exists
        $conversation = ChatConversation::find($chat_conversation_id);
        if (!$conversation) {
            return ApiResponse::__createBadResponse('Chat conversation not found.');
        }

        //Check is user allowed to use this conversation
        $participant = $conversation->participants->where('user_id', Auth::id())->count();
        if ($participant == 0) {
            return ApiResponse::__createBadResponse('You are not allow to read this conversation.');
        }

        //Mark all as read
        $conversation->markAllAsRead(Auth::id());
        $perPage = 10;
        $totalPages = ceil((ChatConversationMessage::where('chat_conversation_id', $chat_conversation_id)
            ->where('to_id', Auth::id())->count()) / $perPage);

        $history = $conversation->myMessages(Auth::id(), $perPage);
        $holdHistory = [];
        foreach ($history as $message) {
            $item = new stdClass;
            $item->id = $message->id;
            $item->type = $message->type;
            $item->send_id = $message->send_id;
            $item->chat_conversation_id = $message->chat_conversation_id;
            $item->from_id = $message->from_id;
            $item->to_id = $message->to_id;
            $item->is_read = $message->is_read;
            $item->plans_id = $message->plans_id;
            $item->places_id = $message->places_id;
            // $item->created_at = $message->created_at;
            // $item->updated_at = $message->updated_at;
            // $item->from_user_name = $message->from_user_name;
            // $item->from_user_thumb = $message->from_user_thumb;
            $item->file_link = $message->file_link;
            $item->message_parsed = $message->message_parsed;
            $item->conv_title = $message->conv_title;
            $item->last_updated_at = $message->last_updated_at;
            $item->unread_chat_conversations_count = $message->unread_chat_conversations_count;
            $item->fromuser = $message->fromuser;
            $item->touser =  $message->touser;
            $holdHistory[] = $item;
        }

        return ApiResponse::create(
            [
                'total_pages' => $totalPages,
                'history' => $holdHistory
            ]
        );
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function conversations()
    {
        $conversations = ChatConversation::whereHas(
            "participants",
            function ($query) {
                $query->where("user_id", "=", Auth::id());
            }
        )->orderByDesc('updated_at')->get();

        return ApiResponse::create(
            $conversations
        );
    }

    /**
     * Show all of the message threads to the user.
     *
     * @param integer $chat_conversation_id
     * @return \Illuminate\Http\Response
     */
    public function getConversations($chat_conversation_id)
    {
        try {
            //Check is conversation exists
            $conversation = ChatConversation::find($chat_conversation_id);
            if (!$conversation) {
                return ApiResponse::__createBadResponse("Chat conversation not found.");
            }

            //Check is user allowed to use this conversation
            $participant = $conversation->participants->where('user_id', Auth::id())->count();
            if ($participant == 0) {
                return ApiResponse::__createBadResponse("You are not allow to read this conversation.");
            }

            $conversation->markAllAsRead(Auth::id());
            return ApiResponse::create(
                $conversation->myMessages(Auth::id())
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Post(
     ** path="/chat/send",
     *   tags={"Global Chat"},
     *   summary="send message to X user",
     *   operationId="app\Http\Controllers\Api\ChatPrivateController.php@send",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="participants",
     *        description="user_id of X user",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="message",
     *        description="message",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function send(Request $request)
    {
        if (!$request->has('chat_conversation_id')) {
            $request->merge(['chat_conversation_id' => 0]);
        }

        // Define the rules for the input
        $rules['chat_conversation_id'] = 'required|integer|min:0';

        // Define the error messagges
        $messages = [
            'chat_conversation_id.required' => 'Chat conversation id  must required',
            'chat_conversation_id.integer'  => 'Chat conversation id  must be an integer.',
            'chat_conversation_id.min'      => 'Chat conversation id  must be greater then 0',
            'message.required'              => 'Message body required',
            'message.string'                => 'Message body must be a string',
            'participants.required'         => 'Participants list required.',
            'participants.string'           => 'Participants list must be a string format.'
        ];

        $rules['message'] = ($request->hasFile('documents')) ? 'required|string' : 'string|sometimes|nullable';
        ($request->chat_conversation_id == 0) ? $rules['participants'] =  'required|string' : '';

        // Validate the request
        $validator = \Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return ApiResponse::create($validator->errors(), false, ApiResponse::VALIDATION);
        }

        $select_conversation_id = null;
        try {
            if ($request->file('documents')) {

                //Save file to Storage
                $files = $request->file('documents');

                foreach ($files as $file) {
                    $file_name = $file->getClientOriginalName();
                    $file_type = $file->getClientOriginalExtension();
                    $file_size = $file->getClientSize();
                    $name = uniqid() . $file_name;

                    Storage::disk('s3')->put(
                        'chat/' . $name,
                        file_get_contents($file),
                        'public'
                    );

                    $file_path = 'chat/' . $name;

                    $multi_file_name = $file_name . "|";
                    $multi_file_type = $file_type . "|";
                    $multi_file_size = $file_size . "|";
                    $multi_file_path = $file_path . "|";
                }
            }

            // It means new conversation must be created
            if ($request->chat_conversation_id == 0) {
                $participants_received = explode(',', $request->participants);
                $result = $this->createNewChatConversation($participants_received, Auth::id(), true);

                if (!$result['status']) {
                    return ApiResponse::create(
                        [
                            'message' => [$result['message']]
                        ],
                        $result['status']
                    );
                }
                if ($result['select_conversation'] == true) {
                    $select_conversation_id = $result['conversation']->id;

                    //Set new conversation as current
                    $request['chat_conversation_id'] = $result['conversation']->id;
                }
            }

            $response = $request->all();

            //Check is conversation exists
            if (!$conversation = ChatConversation::find($request->chat_conversation_id)) {
                return ApiResponse::create(
                    [
                        'message' => ["Chat conversation not found."]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            //Check is user allowed to use this conversation
            if (!$participant = $conversation->participants->where('user_id', Auth::id())->count()) {
                return ApiResponse::create(
                    [
                        'message' => ["You are not allow to use this conversation."]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            //Save message to database
            $participants = $conversation->participants;

            //Check is there at least 2 participants
            if (count($participants) < 2) {
                return ApiResponse::create(
                    [
                        'message' => ["At least 2 participants are needed for conversation."]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $response['from_id'] = Auth::id();
            $response['file_path'] = isset($multi_file_path) ?: '';
            $response['file_name'] = isset($multi_file_name) ?: '';
            $response['file_size'] = isset($multi_file_size) ?: '';
            $response['file_type'] = isset($multi_file_type) ?: '';

            //Sending batch
            $response['send_id'] = ChatConversationMessage::max('send_id') + 1;

            //Update conversation updated_at
            date_default_timezone_set("UTC");
            $conversation->touch();
            date_default_timezone_set("Asia/Riyadh");

            foreach ($participants as $participant) {

                //Save even when from_id == to_id in order to avoid getting duplicates in getMessages()
                $response['to_id'] = $participant->user_id;

                if ($response['to_id'] == $response['from_id']) {
                    $response['is_read'] = true;
                } else {
                    $response['is_read'] = false;
                }

                $response['message'] = processString($response['message']);

                $message = ChatConversationMessage::create($response);

                broadcast(new ChatSendPrivateMessageEvent($participant->user_id, $message, $select_conversation_id));
            }
            $message_new = $this->getMessage_last($conversation->id);
            $data = [
                'conversation' => $conversation,
                'message_new'  => $message_new,
            ];

            return ApiResponse::create([
                'message_send_status' => true
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    private function getMessage_last($chat_conversation_id)
    {
        //Check is conversation exists
        $conversation = ChatConversation::find($chat_conversation_id);

        if ($conversation === null) {
            return response(['message' => 'Chat conversation not found.'], 422);
        }

        //Check is user allowed to use this conversation
        $participant = $conversation->participants->where('user_id', Auth::id())->count();

        if ($participant == 0) {
            return response(['message' => 'You are not allow to read this conversation.'], 422);
        }

        //Mark all as read
        $conversation->markAllAsRead(Auth::id());

        return $conversation->myMessages(Auth::id())->last();
    }

    /**
     * @param array $user_ids
     * @param integer $created_by
     * @param boolean $broadcast_to_owner
     * @return array
     */
    private function createNewChatConversation(array $user_ids, $created_by, $broadcast_to_owner = false)
    {
        //Check does user exists
        foreach ($user_ids as $user_id) {
            if (User::find($user_id) === null) {
                return [
                    "status" => false,
                    "message" => "Non existing user.",
                    "conversation" => null,
                    "select_conversation" => false
                ];
            }
        }

        //Check does conversation exists
        $check_conversation = $this->checkConversation($user_ids, $created_by);

        if ($check_conversation === -1) {
            return [
                "status" => false,
                "message" => "At least 2 participants are needed for conversation.",
                "conversation" => null,
                "select_conversation" => false
            ];
        }

        if ($check_conversation) {
            return [
                "status" => true,
                "message" => "",
                "conversation" => $check_conversation,
                "select_conversation" => true
            ];
        }

        //Create new conversation
        $conversation = ChatConversation::create(
            [
                'created_by_id' => $created_by,
                'subject' => ''
            ]
        );

        //Add participants
        ChatConversationParticipant::create(
            [
                'chat_conversation_id' => $conversation->id,
                'user_id' => Auth::id()
            ]
        );

        foreach ($user_ids as $user_id) {
            ChatConversationParticipant::create(
                [
                    'chat_conversation_id' => $conversation->id,
                    'user_id' => $user_id
                ]
            );

            //Broadcast event only to user
            try {
                broadcast(new ChatNewConversationEvent($user_id, $conversation));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new ChatNewConversationApiEvent($user_id, $conversation));
            } catch (\Throwable $e) {
            }
        }

        if ($broadcast_to_owner === true) {
            try {
                broadcast(new ChatNewConversationEvent(Auth::id(), $conversation));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new ChatNewConversationApiEvent(Auth::id(), $conversation));
            } catch (\Throwable $e) {
            }
        }

        return [
            "status" => true,
            "message" => "",
            "conversation" => $conversation,
            "select_conversation" => false
        ];
    }

    /**
     * @param array $participants_id
     * @param integer $created_by
     * @return false|array
     */
    private function checkConversation(array $participants_id, $created_by)
    {
        // Get conversations by created_by field
        $conv_ids = ChatConversation::select('id')
            ->whereHas(
                "participants",
                function ($query) use ($created_by) {
                    $query->where("user_id", $created_by);
                }
            )->pluck('id', 'id')->toArray();

        if (!count($conv_ids)) {
            return false;
        }
        // Get conversations participant by conversation ids
        $only_conversations = ChatConversationParticipant::whereIn('chat_conversation_id', $conv_ids)
            ->get();

        // Collect convaersation ids and user ids to a array
        $conv_id = [];
        foreach ($only_conversations as $val) {
            if ($created_by == $val->user_id) {
                continue;
            }
            $conv_id[$val->chat_conversation_id][] = $val->user_id;
        }

        // Check if $participants in array of users, and also is same length
        // it means if chat is group and contains same people as in array($participants_id), or individual chat
        foreach ($conv_id as $key => $val) {
            if (in_array($participants_id, [$val]) && count($val) == count($participants_id)) {
                return ChatConversation::where('id', $key)->first();
            }
        }
        return false;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getParticipants()
    {
        $participants = User::where('id', '<>', Auth::id())->where('status', 1)->pluck('name', 'id');
        return ApiResponse::create($participants);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getParticipantImages(Request $request)
    {
        try {
            $participants = User::select('id', 'profile_picture')
                ->where('id', '<>', Auth::id())
                ->where('status', 1)
                ->where('name', 'like', '%' . $request->term . '%')
                ->get()->toarray();
            return ApiResponse::create($participants);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $number
     * @return \Illuminate\Http\Response
     */
    public function getLastFive($number)
    {
        if ($number > 20) {
            return ApiResponse::create(
                [
                    'message' => ["Max. 20 is allowed."]
                ],
                false,
                ApiResponse::BAD_REQUEST
            );
        }
        $userId = Auth::id();
        $conversations = ChatConversation::whereHas(
            "participants",
            function ($query) use ($userId) {
                $query->where("user_id", $userId);
            }
        )->orderByDesc('updated_at')->paginate($number);

        return ApiResponse::create($conversations);
    }

    /**
     * @param integer $conversation_id
     * @return \Illuminate\Http\Response
     */
    public function deleteConversation($conversation_id)
    {
        //This is not deleting conversation in total but only removing participant
        //Other participants will still be able to check messages and continue conversation
        ChatConversationParticipant::where('user_id', Auth::id())
            ->where('chat_conversation_id', $conversation_id)
            ->delete();
        return ApiResponse::create(
            [
                'message' => ["User removed from participants list."]
            ]
        );
    }

    /**
     * @param integer $conversation_id
     * @return \Illuminate\Http\Response
     */
    public function createNewConversation($user_id)
    {
        if ($user_id == Auth::id()) {
            return ApiResponse::create(
                [
                    'message' => ["You can't create chat with yourself."]
                ],
                false,
                ApiResponse::BAD_REQUEST
            );
        }

        $result = $this->createNewChatConversation([$user_id], Auth::id(), true);

        if (!$result['status']) {
            return ApiResponse::create(
                [
                    'message' => [$result['message']]
                ],
                $result['status']
            );
        }
        return ApiResponse::create(
            [
                'conversation_id' => $result['conversation']->id
            ]
        );
    }
}
