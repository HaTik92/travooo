<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Auth;
use DB;
use App\Http\Responses\ApiResponse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests\Api\Media\CommentMediasRequest;
use App\Http\Requests\Api\Media\CommentReplyRequest;
use App\Http\Requests\Api\Media\CommentEditRequest;
use App\Http\Requests\Api\Media\LikeUnlikeRequest;
use App\Http\Requests\Api\Media\TripPlaceLikeUnlikeRequest;
use App\Http\Requests\Api\Media\CommentDeleteRequest;
use App\Http\Requests\Api\Media\CommentLikeUnlikeRequest;
use App\Http\Requests\Api\Media\CommentLikeUsersRequest;
use App\Http\Requests\Api\Media\Comment4PlaceRequest;
use App\Http\Requests\Api\Media\UpdateViewsRequest;

use App\Models\ActivityMedia\Media;
use App\Models\ActivityMedia\MediasComments;
use App\Models\ActivityMedia\MediascommentsMedias;
use App\Models\Posts\PostsMedia;
use App\Models\ActivityMedia\MediasLikes;
use App\Models\Posts\Posts;
use App\Models\TripMedias\TripMedias;
use App\Models\ActivityMedia\MediasCommentsLikes;

class MediasController extends Controller
{
    /**
     * @param CommentMediasRequest $request
     * @param integer $medias_id
     * @return \Illuminate\Http\Response
     */
    public function postComment(CommentMediasRequest $request, $medias_id)
    {
        try {
            $comment = $request->text;
            $comment = is_null($comment) ? '' : $comment;
            $pair = $request->pair;
            @$author_id = Media::find($medias_id)->mediaUser->users_id;

            $is_files = false;
            $file_lists = $this->getTempFiles($pair);

            $is_files = count($file_lists) > 0 ? true : false;

            if (!$is_files && $comment == "")
                return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);

            $text = convert_string($comment);

            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;

            $new_comment = new MediasComments;
            $new_comment->medias_id = $medias_id;
            $new_comment->users_id = $user_id;
            $new_comment->reply_to = 0;
            $new_comment->comment = $text;
            $data = array();
            if ($new_comment->save()) {
                log_user_activity('Media', 'comment', $medias_id);

                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('media-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/media-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $text;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->type  = getMediaTypeByMediaUrl($media->url);
                    $media->users_id = $user_id;
                    $media->save();

                    $posts_comments_medias = new MediascommentsMedias();
                    $posts_comments_medias->medias_comments_id = $new_comment->id;
                    $posts_comments_medias->medias_id = $media->id;
                    $posts_comments_medias->save();
                }

                $post_object = Media::find($medias_id);
                return ApiResponse::create(
                    [
                        'comment' => $new_comment,
                        'post_object' => $post_object
                    ]
                );
            } else {
                return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param pair for tempfile prefix
     * @return fileNameArray(filepath, filename)
     */
    public function getTempFiles($pair)
    {
        $temp_dir = public_path() . '/assets2/upload_tmp/';
        $auth_user_id = Auth::user()->id;

        $file_lists = [];

        if (is_dir($temp_dir)) {
            if ($handle = opendir($temp_dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    if (strpos($file, $pair . '_' . $auth_user_id) !== FALSE) {
                        $filename_split = explode($pair . '_' . $auth_user_id . '_', $file, 2);
                        $file_lists[] = [$temp_dir . $file, $filename_split[1]];
                    }
                }
                closedir($handle);
            }
        }

        return $file_lists;
    }

    /**
     * @param CommentReplyRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCommentReply(CommentReplyRequest $request)
    {
        try {
            $comment = $request->text;
            $comment = is_null($comment) ? '' : $comment;
            $comment_id = $request->comment_id;
            $medias_id = $request->media_id;
            $pair = $request->pair;
            @$author_id = Media::find($medias_id)->mediaUser->users_id;

            $is_files = false;
            $file_lists = $this->getTempFiles($pair);

            $is_files = count($file_lists) > 0 ? true : false;

            if (!$is_files && $comment == "")
                return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);


            $text = convert_string($comment);
            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;

            $new_comment = new MediasComments;
            $new_comment->medias_id = $medias_id;
            $new_comment->users_id = $user_id;
            $new_comment->reply_to = $comment_id;
            $new_comment->comment = $text;
            $data = array();
            if ($new_comment->save()) {
                log_user_activity('Media', 'comment', $medias_id);

                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('media-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/media-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $text;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->type  = getMediaTypeByMediaUrl($media->url);
                    $media->users_id = $user_id;
                    $media->save();

                    $posts_comments_medias = new MediascommentsMedias();
                    $posts_comments_medias->medias_comments_id = $new_comment->id;
                    $posts_comments_medias->medias_id = $media->id;
                    $posts_comments_medias->save();
                }

                $post_object = Media::find($medias_id);
                return ApiResponse::create(
                    [
                        'child' => $new_comment,
                        'post_object' => $post_object
                    ]
                );
            } else {
                return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CommentEditRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCommentEdit(CommentEditRequest $request)
    {
        try {
            $pair = $request['pair'];
            $medias_id = $request->media_id;
            $comment_id = $request->comment_id;

            $media_comment_edit = $request->text;
            $user_id = Auth::user()->id;
            @$author_id = Media::find($medias_id)->mediaUser->users_id;
            $media_comment = MediasComments::find($comment_id);

            $user_name = Auth::user()->name;

            $is_files = false;
            $file_lists = $this->getTempFiles($pair);

            $is_files = count($file_lists) > 0 ? true : false;
            $media_comment_edit = is_null($media_comment_edit) ? '' : $media_comment_edit;
            if (!$is_files && $media_comment_edit == "")
                return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);

            $post_object = Media::find($medias_id);

            $text = convert_string($media_comment_edit);

            if (is_object($post_object)) {
                $media_comment->comment = $text;
                if ($media_comment->save()) {
                    log_user_activity('Status', 'comment', $medias_id);


                    if (isset($request->existing_medias) && count($media_comment->medias) > 0) {
                        foreach ($media_comment->medias as $media_list) {
                            if (in_array($media_list->media->id, $request->existing_medias)) {
                                $medias_exist = Media::find($media_list->medias_id);

                                $media_list->delete();

                                if ($medias_exist) {
                                    $amazonefilename = explode('/', $medias_exist->url);
                                    Storage::disk('s3')->delete('post-comment-photo/' . end($amazonefilename));

                                    $medias_exist->delete();
                                }
                            }
                        }
                    }



                    foreach ($file_lists as $file) {
                        $filename = $user_id . '_' . time() . '_' . $file[1];
                        Storage::disk('s3')->put('media-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                        $path = 'https://s3.amazonaws.com/travooo-images2/media-comment-photo/' . $filename;

                        $media = new Media();
                        $media->url = $path;
                        $media->author_name = $user_name;
                        $media->title = $text;
                        $media->author_url = '';
                        $media->source_url = '';
                        $media->license_name = '';
                        $media->license_url = '';
                        $media->uploaded_at = date('Y-m-d H:i:s');
                        $media->type  = getMediaTypeByMediaUrl($media->url);
                        $media->users_id = $user_id;
                        $media->save();

                        $posts_comments_medias = new MediascommentsMedias();
                        $posts_comments_medias->medias_comments_id = $media_comment->id;
                        $posts_comments_medias->medias_id = $media->id;
                        $posts_comments_medias->save();
                    }

                    $media_comment->load('medias');

                    if ($request->comment_type == 1) {
                        return ApiResponse::create(
                            [
                                'comment' => $media_comment,
                                'post_object' => $post_object
                            ]
                        );
                    } else {
                        return ApiResponse::create(
                            [
                                'child' => $media_comment,
                                'post_object' => $post_object
                            ]
                        );
                    }
                } else {
                    return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
                }
            } else {
                return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param LikeUnlikeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postLikeUnlike(LikeUnlikeRequest $request)
    {
        try {
            $media_id = $request->media_id;
            $post_media = PostsMedia::where('medias_id', $media_id)->first();
            if (!$post_media) {
                return ApiResponse::create(
                    [
                        "message" => ["Invalid Media Id"]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            $user_id = Auth::user()->id;
            $post_id = PostsMedia::where('medias_id', $media_id)->get()->first()->posts_id;
            $author_id = Posts::find($post_id)->users_id;

            // $author_id = UsersMedias::where('medias_id', $media_id)->get()->first()->user->id;

            $check_like_exists = MediasLikes::where('medias_id', $media_id)->where('users_id', $user_id)->get()->first();
            $data = array();

            if (is_object($check_like_exists)) {
                //dd($check_like_exists);
                $check_like_exists->delete();
                log_user_activity('Media', 'unlike', $media_id);
                //notify($author_id, 'status_unlike', $post_id);

                $data['status'] = false;
            } else {
                $like = new MediasLikes;
                $like->medias_id = $media_id;
                $like->users_id = $user_id;
                $like->save();

                log_user_activity('Media', 'like', $media_id);
                //notify($author_id, 'media_like', $media_id);

                if ($author_id && $user_id != $author_id) {
                    notify($author_id, 'status_like', $post_id);
                }
                $data['status'] = true;
            }
            $data['count'] = count(MediasLikes::where('medias_id', $media_id)->get());

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param TripPlaceLikeUnlikeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function tripPlaceLikeUnlike(TripPlaceLikeUnlikeRequest $request)
    {
        try {
            if ($request->media_id) {
                $mediaId = $request->media_id;
                $tripPlaceMedia = TripMedias::where('medias_id', $mediaId)->get()->first();
                if (!$tripPlaceMedia) {
                    return ApiResponse::create(
                        [
                            "message" => ["Invalid Medias Id"]
                        ],
                        false,
                        ApiResponse::BAD_REQUEST
                    );
                }

                $userId = auth()->id();

                $check_like_exists = MediasLikes::where('medias_id', $mediaId)->where('users_id', $userId)->first();

                $data = [];

                if ($check_like_exists) {
                    $check_like_exists->delete();
                    log_user_activity('Media', 'unlike', $mediaId);
                    $data['status'] = 'no';
                } else {

                    $like = new MediasLikes();
                    $like->medias_id = $mediaId;
                    $like->users_id = $userId;
                    $like->save();

                    log_user_activity('Media', 'like', $mediaId);

                    $authorId = $tripPlaceMedia->trip->users_id;

                    if ($authorId && $userId != $authorId) {
                        notify($authorId, 'trip_place_like', $tripPlaceMedia->trips_id);
                    }

                    $data['status'] = 'yes';
                }

                $data['count'] = MediasLikes::where('medias_id', $mediaId)->count();

                return ApiResponse::create($data);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CommentDeleteRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCommentDelete(CommentDeleteRequest $request)
    {
        try {
            $comment_id = $request->comment_id;
            $comment = MediasComments::find($comment_id);
            if (!$comment) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Comment Id']
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            foreach ($comment->medias as $com_media) {
                $medias_exist = Media::find($com_media->medias_id);

                $com_media->delete();

                if ($medias_exist) {
                    $amazonefilename = explode('/', $medias_exist->url);
                    Storage::disk('s3')->delete('media-comment-photo/' . end($amazonefilename));

                    $medias_exist->delete();
                }
            }

            if ($comment->delete()) {
                return ApiResponse::create(
                    [],
                    true,
                    ApiResponse::OK
                );
            } else {
                return ApiResponse::create(
                    [],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CommentLikeUnlikeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCommentLikeUnlike(CommentLikeUnlikeRequest $request)
    {
        try {
            $comment_id = $request->comment_id;
            $user_id = Auth::user()->id;

            $author_id = MediasComments::find($comment_id)->users_id;
            $media_id = MediasComments::find($comment_id)->medias_id;

            @$post_author_id = Media::find($media_id)->mediaUser->users_id;
            // $post_id = PostsMedia::where('medias_id', $media_id)->get()->first()->posts_id;
            // $post_author_id = Posts::find($post_id)->users_id;

            $check_like_exists = MediasCommentsLikes::where('medias_comments_id', $comment_id)->where('users_id', $user_id)->get()->first();
            $data = array();

            if (is_object($check_like_exists)) {
                //dd($check_like_exists);
                $check_like_exists->delete();
                log_user_activity('Media', 'commentunlike', $comment_id);
                //notify($author_id, 'status_unlike', $post_id);

                $data['status'] = 'no';
            } else {
                $like = new MediasCommentsLikes;
                $like->medias_comments_id = $comment_id;
                $like->users_id = $user_id;
                $like->save();

                log_user_activity('Media', 'commentlike', $comment_id);
                //notify($author_id, 'media_like', $media_id);

                $data['status'] = 'yes';
            }
            $data['count'] = count(MediasCommentsLikes::where('medias_comments_id', $comment_id)->get());
            $data['name'] = Auth::user()->name;

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $medias_id
     * @return \Illuminate\Http\Response
     */
    public function postShowLikes($medias_id)
    {
        try {
            $data['likes'] = MediasLikes::where('medias_id', $medias_id)->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param CommentLikeUsersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getCommentLikeUsers(CommentLikeUsersRequest $request)
    {
        try {
            $comment_id = $request->comment_id;
            $comment = MediasComments::find($comment_id);

            if ($comment->likes) {
                return ApiResponse::create(
                    [
                        'likes' => $comment->likes()->orderBy('created_at', 'DESC')->get()
                    ]
                );
            } else {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param Comment4PlaceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postComment4Place(Comment4PlaceRequest $request, $medias_id)
    {
        try {
            $comment = processString($request->text);
            $comment = is_null($comment) ? '' : $comment;
            @$author_id = Media::find($medias_id)->mediaUser->users_id;

            // $post_id = PostsMedia::where('medias_id', $medias_id)->get()->first()->posts_id;
            // $author_id = Posts::find($post_id)->users_id;


            $user_id = Auth::user()->id;
            $user_name = Auth::user()->name;

            $new_comment = new MediasComments;
            $new_comment->medias_id = $medias_id;
            $new_comment->users_id = $user_id;
            $new_comment->reply_to = 0;
            $new_comment->comment = $comment;
            $data = array();
            if ($new_comment->save()) {
                log_user_activity('Media', 'comment', $medias_id);

                // $post_object = Media::find($medias_id);
                return ApiResponse::create(
                    [
                        'comment' => $new_comment
                    ]
                );
            } else {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $medias_id
     * @return \Illuminate\Http\Response
     */
    public function postUpdateViews($medias_id)
    {
        try {
            $media = Media::find($medias_id)->increment('views');
            if ($media) {
                return ApiResponse::create(
                    [],
                    true,
                    ApiResponse::OK
                );
            } else {
                return ApiResponse::create(
                    [],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
