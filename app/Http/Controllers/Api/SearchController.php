<?php

namespace App\Http\Controllers\Api;

use App\Http\Constants\CommonConst;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\HomeController;
use App\Http\Responses\ApiResponse;
use App\Models\Place\Place;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Posts\Checkins;
use App\Models\Reviews\Reviews;
use App\Services\Trips\TripInvitationsService;
use App\Http\Requests\Api\Home\GlobalSearchRequest;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\User\SettingUsersBlocks;
use App\Models\User\User;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    /**
     * @OA\GET(
     ** path="/search/global",
     *   tags={"Search"},
     *   summary="This Api global search api",
     *   operationId="Api\SearchController@globalSearch",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="q",
     *        description="search keyword",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param GlobalSearchRequest $request
     * @param TripInvitationsService $invitationsService
     * @return ApiResponse
     */
    public function globalSearch(GlobalSearchRequest $request, TripInvitationsService $invitationsService)
    {
        try {
            $query = $request->get('q');
            $result = [];
            $lat = null;
            $lng = null;
            $cityId = null;

            if ($request->has('lat'))
                $lat = $request->get('lat');
            if ($request->has('lng'))
                $lng = $request->get('lng');
            if ($request->has('search_in'))
                $search_in = $request->get('search_in');
            if ($request->has('search_id')) {
                $search_id = $request->get('search_id');
                if ($search_id !== "false") {
                    $search_in = $search_in . "_id";
                }
            }

            if ($request->has('city_id')) {
                $cityId = $request->get('city_id');
            }
            if ($request->has('type')) {
                $type = $request->get('type');
            }

            $fromExternal = false;

            if ($query != '') {

                $esResult = app(HomeController::class)->getElSearchResult($query);
                $qr = $esResult['result'];
                $result = [];

                foreach ($qr as $r) {
                    $medium = [];
                    $_place_type = explode(",", $r['place_type']);
                    if (isset($_place_type[0], $r['place_id']) && in_array($_place_type[0], getAllowedPlaceTypes())) {
                        // if ($r['countries_id'] != 326 && $r['cities_id'] != 2031 && $r['countries_id'] != 373) {
                        $medium['id'] = $r['place_id'];
                        $medium['address'] = $r['address'];
                        $medium['title'] = $r['place_title'];
                        $medium['lat'] = $r['lat'];
                        $medium['place_type'] = $_place_type[0];
                        $medium['lng'] = $r['lng'];
                        $medium['location'] = $r['lat']  . "," . $r['lng'];

                        if (isset($r['place_media']) && $r['place_media'] != null) {
                            $medium['image'] = $r['place_media'];
                        }

                        $p = Place::find($medium['id']);

                        $medium['city_id'] = @$p->cities_id;
                        $medium['city_title'] = @$p->city->transsingle->title;
                        $medium['type'] = '';
                        $result[] = $medium;
                        // }
                    }
                }

                foreach ($result as $key =>  &$place) {
                    if (!($place['city_id'] && $place['city_title']) || ($cityId && $place['city_id'] != $cityId)) {
                        unset($result[$key]);
                        continue;
                    }

                    $place['reviews'] = Reviews::where('places_id', $place['id'])->take(3)->get();
                    $place['reviews_avg'] = Reviews::where('places_id', $place['id'])->avg('score');
                    $place['type'] = CommonConst::SEARCH_PLACE;
                    // $isTrending = PlacesTop::where('places_id', $place['id'])->exists();

                    // if ($isTrending) {
                    //     $place['type'] = 'Trending';
                    //     continue;
                    // }

                    // $friendsAndFollowingsIds = $invitationsService->findFriends()->merge($invitationsService->findFollowings());

                    // $isSuggested = Checkins::query()->where('place_id', $place['id'])->whereIn('users_id', $friendsAndFollowingsIds)->exists();

                    // if ($isSuggested) {
                    //     $place['type'] = 'Suggested';
                    // }
                }
            }
            $no = 0;
            foreach ($result as $place) {
                $result[$no]['type'] = "place";
                if (!isset($result[$no]['image'])) {
                    $result[$no]['image'] = url(PLACE_PLACEHOLDERS);
                } else {
                    $photo_url  = str_replace(S3_BASE_URL, "", $result[$no]['image']);
                    $result[$no]['image'] =  S3_BASE_URL . $photo_url;
                }
                $no++;
            }

            $list = [];
            $list['users']      = $this->getSearchUsersGlobal($request);
            $list['cities']     = $this->getSearchCitiesGlobal($request);
            $list['countries']  = $this->getSearchCountriesGlobal($request);
            $list['places']     = $result;
            return ApiResponse::create($list);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/search/tags",
     *   tags={"Search"},
     *   summary="This Api search tags from users, places, cities, and countries",
     *   operationId="Api\SearchController@tagsSearch",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="q",
     *        description="search keyword",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * @param Request $request
     * @return \IlluminRequestate\Http\Response
     */
    public function tagsSearch(GlobalSearchRequest $request, TripInvitationsService $invitationsService)
    {
        return $this->globalSearch($request, $invitationsService);
    }

    /**
     * @OA\GET(
     ** path="/search/location",
     *   tags={"Search"},
     *   summary="This Api search location",
     *   operationId="Api\SearchController@getLocationSearchResult",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\Parameter(
     *        name="q",
     *        description="search keyword",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="lat",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="lng",
     *        description="",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getLocationSearchResult(Request $request)
    {
        $query = $request->get('q');
        $results = array();

        if ($query != '') {
            $all_places = app(HomeController::class)->getElSearchResult($query);
            $counter = 0;
            foreach ($all_places['result'] as $place) {
                $counter++;
                if ($counter <= 20) {
                    $results[] = array(
                        'id' => $place['place_id'],
                        'title' => @$place['place_title'],
                        'description' => !empty(@$place['address']) ? @$place['address'] : '',
                        'img' => isset($place['place_media']) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place['place_media'] : asset(PATTERN_PLACEHOLDERS),
                        'type' => CommonConst::SEARCH_PLACE
                    );
                }
            }

            $all_places = Countries::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($query) {
                    $q->where('title', 'LIKE', "%$query%");
                })
                ->limit(20)
                ->get();
            foreach ($all_places as $place) {
                $results[] = array(
                    'id' => $place->id,
                    'title' => @$place->transsingle->title,
                    'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                    'img' => get_country_flag($place),
                    'lng' => $place->lng,
                    'lat' => $place->lat,
                    'type' => CommonConst::SEARCH_COUNTRY
                );
            }

            $all_places = Cities::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($query) {
                    $q->where('title', 'LIKE', "%$query%");
                })
                ->limit(20)
                ->get();
            foreach ($all_places as $place) {
                $results[] = array(
                    'id' => $place->id,
                    'title' => @$place->transsingle->title,
                    'description' => !empty(@$place->transsingle->description) ? @$place->transsingle->description : '',
                    'img' => isset($place->getMedias[0]->url) ? check_city_photo($place->getMedias[0]->url) : asset(PATTERN_PLACEHOLDERS),
                    'lng' => $place->lng,
                    'lat' => $place->lat,
                    'type' => CommonConst::SEARCH_CITY
                );
            }
        } else {
            if (!empty($request->lat) && $request->lat != "false" && !empty($request->lng) && $request->lng != "false") {
                $lat = $request->lat;
                $lon = $request->lng;
                $all_places = Place::with('trans')
                    ->select('places.id', 'places.lat', 'places.lng', DB::raw("6371 * acos(cos(radians(" . $lat . "))
                        * cos(radians(places.lat))
                        * cos(radians(places.lng) - radians(" . $lon . "))
                        + sin(radians(" . $lat . "))
                        * sin(radians(places.lat))) AS distance"))
                    ->groupBy("distance")
                    ->limit(15)
                    ->get();

                foreach ($all_places as $place) {
                    $results[] = array(
                        'id' => $place->id,
                        'title' => @$place->transsingle->title,
                        'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                        'img' => check_place_photo($place),
                        'lng' => $place->lng,
                        'lat' => $place->lat,
                        'type' => CommonConst::SEARCH_PLACE
                    );
                }
            } else {
                $details = ip_details(getClientIP());
                if (array_key_exists('city', $details)) {
                    $cityName = $details['city'];
                    $city = Cities::with('transsingle')
                        ->whereHas('transsingle', function ($q) use ($cityName) {
                            $q->where('title', 'LIKE', "%$cityName%");
                        })
                        ->first();

                    if (!empty($city)) {
                        $results[] = array(
                            'id' => $city->id,
                            'title' => @$city->transsingle->title,
                            'description' => !empty(@$city->transsingle->description) ? @$city->transsingle->description : '',
                            'img' => isset($city->getMedias[0]->url) ? check_city_photo($city->getMedias[0]->url) : asset(PATTERN_PLACEHOLDERS),
                            'lng' => $city->lng,
                            'lat' => $city->lat,
                            'type' => CommonConst::SEARCH_CITY
                        );
                        $cityId = $city->id;
                        $all_places = PlacesTop::where('city_id', $cityId)
                            ->limit(5)->get();

                        foreach ($all_places as $place) {
                            $results[] = array(
                                'id' => $place->id,
                                'title' => @$place->place->transsingle->title,
                                'description' => @$place->place->transsingle->description,
                                'img' => check_place_photo(@$place->place),
                                'lng' => $place->place->lng,
                                'lat' => $place->place->lat,
                                'type' => CommonConst::SEARCH_PLACE
                            );
                        }
                    }
                }
            }
        }
        return ApiResponse::create($results);
    }

    /**
     * @OA\GET(
     ** path="/countries",
     *   tags={"Common API"},
     *   summary="Fetch all country list",
     *   operationId="Api\SearchController@getCountryList",
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getCountryList()
    {
        try {
            $all_countries = Countries::with('transsingle')->get()
                ->map(function ($country) {
                    return [
                        'id'   => $country->id,
                        'name' => $country->transsingle->title,
                        'iso_code' => $country->iso_code,
                        'code' => $country->code,
                        'lat' => $country->lat,
                        'lng' => $country->lng,
                        'flag' => get_country_flag($country)
                    ];
                });

            $all_countries = collect($all_countries)->sortBy('name')->values();
            return ApiResponse::create($all_countries);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\GET(
     ** path="/languages",
     *   tags={"Common API"},
     *   summary="Fetch all languages list",
     *   operationId="Api\SearchController@getLanguages",
     * @OA\Parameter(
     *        name="search",
     *        description="search for searching | optional",
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    public function getLanguages()
    {
        $search = trim(request('search') ?? '');
        $regional_languages = LanguagesSpoken::query()->whereNotIn('id', [28, 19])
            ->with('transsingle')
            ->when(strlen($search), function ($query) use ($search) {
                $query->whereHas('transsingle', function ($query) use ($search) {
                    $query->where('title', 'like', "%$search%");
                });
            })
            ->get()->map(function ($language) {
                return  [
                    'id' => $language->id,
                    'text' => $language->transsingle->title,
                ];
            });
        $regional_languages = collect($regional_languages)->sortBy('text')->values();
        return ApiResponse::create($regional_languages);
    }

    /**
     * @OA\GET(
     ** path="/search/users",
     *   tags={"Search"},
     *   summary="Search User",
     *   operationId="Api\SearchController@getUsers",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *  @OA\Parameter(
     *        name="q",
     *        description="q",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Search users for blocking.
     *
     * @param  Request  $request
     * @return ApiResponse
     */
    public function getUsers(Request $request)
    {
        try {
            $authUser = auth()->user();
            $query = $request->get('q', null);

            $blocked_user_list = SettingUsersBlocks::where('users_id', $authUser->id)->pluck('blocked_users_id')->toArray();

            $userQuery = User::query();
            if ($query) {
                $userQuery = $userQuery->where(function ($q) use ($query) {
                    $q->where('name', 'like', '%' . $query . '%');
                    $q->orWhere('username', 'like', '%' . $query . '%');
                });
            }

            $users = $userQuery->whereNotIn('id', array_merge($blocked_user_list, [$authUser->id]))
                ->take(50)->get()
                ->map(function ($user) {
                    return [
                        'id'            => $user->id,
                        'text'          => $user->name,
                        'image'         => check_profile_picture($user->profile_picture),
                        'nationality'   => $user->nationality ? $user->country_of_nationality->transsingle->title : '',
                    ];
                });

            return ApiResponse::create($users);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    // Helpers
    public function getSearchUsersGlobal($request)
    {
        $queryParam = $request->get('q');
        $usersArr = [];
        if ($queryParam != '') {
            $all_users = User::where('name', 'like',  $queryParam . "%")
                ->limit(50)
                ->orderBy('name', 'ASC')
                ->get();

            foreach ($all_users as $user) {
                $usersArr[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'username' => $user->username,
                    'display_name' => $user->display_name,
                    'interests' => $user->interests,
                    'country' => ($user->country_of_nationality) ? $user->country_of_nationality->trans[0]->title : null,
                    'profile_picture' => check_profile_picture($user->profile_picture),
                    'type' => CommonConst::SEARCH_USER
                ];
            }
        }
        return $usersArr;
    }

    public function getSearchCitiesGlobal($request)
    {
        $queryParam = $request->get('q');

        if ($queryParam != '') {

            $all_cities = Cities::limit(50)
                ->with('transsingle')
                ->whereHas('transsingle', function ($query) use ($queryParam) {
                    return  $query->where('title', 'like', $queryParam . "%");
                })
                ->with('country.trans')
                ->with('getMedias')
                ->get();

            foreach ($all_cities as $city) {
                if (count($city->getMedias) > 0) {
                    $city->image = check_city_photo($city->getMedias[0]->url);
                } else {
                    $city->image = url(PLACE_PLACEHOLDERS);
                }

                $city->type = CommonConst::SEARCH_CITY;
                $city->name = $city->transsingle->title;
                $city->country_name = $city->country->trans[0]->title;
                unset($city->country, $city->transsingle, $city->getMedias);
            }

            return $all_cities;
        } else {
            return [];
        }
    }

    public function getSearchCountriesGlobal($request)
    {
        $queryParam = $request->get('q');

        if ($queryParam != '') {
            $all_countries = Countries::limit(50)
                ->with('transsingle')
                ->with('region.transsingle')
                ->whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', $queryParam . "%");
                })
                ->get();

            foreach ($all_countries as $country) {
                $country->image = get_country_flag($country);
                $country->type =  CommonConst::SEARCH_COUNTRY;
                $country->name = $country->transsingle->title;
                $country->region_name = $country->region->transsingle->title;
                unset($country->region, $country->transsingle);
            }

            return $all_countries;
        } else {
            return [];
        }
    }

    /**
     * @OA\GET(
     ** path="/search/global/countries",
     *   tags={"Search"},
     *   summary="Search Countries",
     *   operationId="Api\SearchController@searchCountries",
     *  @OA\Parameter(
     *        name="q",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Search users for blocking.
     *
     * @param  Request  $request
     * @return ApiResponse
     */
    public function searchCountries(Request $request)
    {
        try {
            return ApiResponse::create($this->getSearchCountriesGlobal($request));
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\GET(
     ** path="/search/global/cities",
     *   tags={"Search"},
     *   summary="Search Countries",
     *   operationId="Api\SearchController@searchCities",
     *  @OA\Parameter(
     *        name="q",
     *        description="",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=422,
     *       description="Unprocessable Entity"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *       response=500,
     *       description="Internal Server Error"
     *   )
     *)
     **/
    /**
     * Search users for blocking.
     *
     * @param  Request  $request
     * @return ApiResponse
     */
    public function searchCities(Request $request)
    {
        try {
            return ApiResponse::create($this->getSearchCitiesGlobal($request));
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
