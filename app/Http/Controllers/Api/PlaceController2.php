<?php

namespace App\Http\Controllers\Api;

use App\Http\Constants\CommonConst;
use Illuminate\Support\Facades\Auth;
use App\Http\Responses\ApiResponse;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Api\Place2\ReviewRequest;
use App\Http\Requests\Api\Place2\Base64ImageRequest;
use App\Http\Requests\Api\Place2\PostByIdRequest;

use App\Models\Place\Place;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Place\PlaceTranslations;
use App\Models\Reviews\Reviews;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\Reports\ReportsInfos;
use App\Models\Reports\Reports;
use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionReplies;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Posts\Checkins;
use App\Models\Place\PlaceFollowers;
use App\Models\Posts\PostsCheckins;
use App\Models\Posts\PostsPlaces;
use App\Models\User\UsersMedias;
use App\Models\Posts\PostsMedia;
use App\Models\Place\PlaceMedias;
use App\Models\Place\PlacesShares;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCommentsMedias;
use App\Models\Posts\Posts;
use App\Models\ActivityMedia\Media;
use App\Models\Reviews\ReviewsMedia;
use App\Models\Reviews\ReviewsVotes;
use App\Models\Reviews\ReviewsShares;
use App\Models\User\User;
use App\Models\ActivityMedia\MediasLikes;
use App\Models\Events\EventsLikes;
use App\Models\Events\EventsShares;
use App\Models\Events\Events;
use App\Models\Events\EventsComments;
use App\Models\Events\EventsCommentsMedias;
use App\Models\Events\EventsCommentsLikes;
use App\Http\Controllers\Api\Traits\PlaceLocationTrait;
use App\Http\Requests\Api\CCPNewsfeedRequest;
use App\Models\ExpertsPlaces\ExpertsPlaces;
use App\Models\UsersFollowers\UsersFollowers;
use App\Models\Holidays\Holidays;
use App\Models\Holidays\HolidayFollower;
use App\Services\Newsfeed\CCPNewsfeed;
use App\Services\Newsfeed\HomeService;
use App\Services\Ranking\RankingService;

class PlaceController2 extends Controller
{
    public function __construct()
    {
        if (request()->route()) {
            if (in_array(request()->route()->uri, optionalAuthRoutes(self::class))) {
                if (request()->bearerToken()) {
                    $this->middleware('auth:api');
                }
            }
        }
    }

    use PlaceLocationTrait;

    /**
     * @OA\Post(
     ** path="/place/{place_id}/review",
     *   tags={"Places"},
     *   summary="Publis post review",
     *   operationId="publish_review",
     *   security={
     *       {"bearer_token": {}
     *   }},
     *   @OA\Parameter(
     *        name="place_id",
     *        description="Place Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="text",
     *        description="review text",
     *        in="query",
     *        required=true,
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Parameter(
     *        name="score",
     *        description="score =>  min 0 | max 5",
     *        in="query",
     *        required=true,
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function postReview(ReviewRequest $request, $place_id)
    {
        try {
            $rankingService = app(RankingService::class);
            $place = Place::find($place_id);
            if (!$place) return ApiResponse::__createBadResponse('Invalid Place ID');

            $review_text = $request->get('text');
            $review_score = $request->get('score');
            $review_author = Auth::user();
            $review_place_id = $place->id;

            $review = Reviews::where([
                'places_id' => $review_place_id,
                'by_users_id' => $review_author->id,
            ])->first();

            if ($review) {
                return ApiResponse::__createBadResponse("You have already reviewed this place!");
            }

            $review = new Reviews();
            $review->places_id = $review_place_id;
            $review->by_users_id = $review_author->id;
            $review->score = $review_score;
            $review->text = $review_text;

            if ($review->save()) {
                $rankingService->addPointsEarners($review->id, get_class($review), $review->by_users_id);
                log_user_activity('Place', 'review', $review->id);

                // testing seed only calling from laravel command
                if (session('seed', false)) {
                    return $review->id;
                }

                return ApiResponse::create(app(HomeService::class)->__preparedPlaceReview($review->id));
            }
            return ApiResponse::__createBadResponse('review can not save, please try again');
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    /**
     * @OA\Get(
     ** path="/place/{id}/newsfeed",
     *   tags={"Places"},
     *   summary="Follow the city",
     *   operationId="app\Http\Controllers\Api\PlaceController2@showNewsfeed",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="id",
     *        description="City Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="page",
     *        description="pagination page number",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     * @OA\Parameter(
     *        name="tab",
     *        description="new-posts | top-posts | discussions | trip-plans | reports | reviews | events | medias",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *  @OA\Parameter(
     *        name="sub_type",
     *        description="country | city | place -> according to algorithm",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="string"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function showNewsfeed(CCPNewsfeedRequest $request, CCPNewsfeed $newsfeed, $id)
    {
        $obj = Place::find($id);
        if (!$obj) return ApiResponse::__createBadResponse("Place not found");

        $page       = (int) $request->get('page', 1);
        $per_page   = 10;
        $tab        = strtolower(trim($request->get('tab', CommonConst::CCP_TAB_NEW_POSTS)));
        $sub_type   = request()->get('sub_type', CCPNewsfeed::SUB_TYPES[HomeService::TYPE_PLACE][0]);
        $filter     = request()->except(['page', 'per_page', 'tab']);

        if (!in_array($tab, CommonConst::CCP_TAB_MAPPING)) return ApiResponse::createValidationResponse(["tab" => ["invalid tab"]]);

        $newsfeed->set($obj, $tab, $sub_type, $filter, $page, $per_page);
        return ApiResponse::create($newsfeed->get());
    }


    /**
     * @param integer $providerId
     * @return integer
     */
    private function addNewPlaceByProviderId($providerId)
    {
        $geo_link = 'https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
            . '&place_id=' . urlencode($providerId);
        $fetch_geo_link = file_get_contents($geo_link);
        $geo_result = json_decode($fetch_geo_link);
        $geo_city = @$geo_result->result->address_components;
        $geo_city_guess = '';
        $geo_country = '';
        $geo_state = '';
        $country_code = '';
        $city_found_flag = false;
        if (isset($geo_city)) {
            foreach ($geo_city as $gc) {
                if (isset($gc->types) && !empty($gc->types)) {
                    if (($gc->types[0] == 'locality' ||  $gc->types[0] == 'administrative_area_level_3') && !$city_found_flag) {
                        $city_found_flag = true;
                        $geo_city_guess = $gc->long_name;
                    }
                    if ($gc->types[0] == 'country') {
                        $country_code = $gc->short_name;
                        $geo_country = $gc->long_name;
                    }
                    if ($gc->types[0] == 'administrative_area_level_1') {
                        $geo_state = $gc->long_name;
                    }
                }
            }
        }
        if (isset($geo_city_guess) && !empty($geo_city_guess)) {
            $get_city = Cities::whereHas('transsingle', function ($query) use ($geo_city_guess) {
                $query->where('title', 'like', '%' . $geo_city_guess . '%');
            })->get()->first();
        }

        $p = new Place();
        $p->place_type = @join(",", $geo_result->result->types);
        $p->safety_degrees_id = 1;
        $p->provider_id = $providerId;

        if (isset($get_city) && is_object($get_city)) {

            $p->countries_id = $get_city->countries_id;
            $p->cities_id = $get_city->id;
            $p->states_id = $get_city->states_id;
        } else {
            $p->cities_id = '2031';
            $p->countries_id = '373';
            $p->states_id  = '0';

            // $geo_country_info=  Countries::whereHas('transsingle', function ($query) use ( $geo_country) {
            //     $query->where('title','like', '%'.$geo_country.'%');
            // })->get()->first();

            // $country_id =326;
            // // check if country exists or else add new 
            // if(isset($geo_country_info)){
            // $country_id  = $geo_country_info->id;
            // }else {
            //         $country  = new Countries();
            //         $country->lat = $geo_result->result->geometry->location->lat;
            //         $country->lng = $geo_result->result->geometry->location->lng;
            //         $country->iso_code = $country_code;
            //         $country->regions_id = 22;
            //         $country->safety_degree_id = 1;
            //         $country->active =1;
            //         if($country->save()){
            //             $country_id = $country->id;
            //             $country_trans=  new CountriesTranslations();
            //             $country_trans->countries_id =$country->id;
            //             $country_trans->title = $geo_country;
            //             $country_trans->languages_id = 1;
            //             $country_trans->save();
            //         }
            // }


            // //check if state exists or add new
            // if(isset($geo_state) && !empty($geo_state)){
            //     $state = States::whereHas('transsingle',function($query) use ($geo_state){
            //         $query->where('title','like', '%'.$geo_state.'%');
            //     })->get()->first();

            // }
            // $states_id =0;
            // if(isset($state) && is_object($state)){
            //     $states_id =  $state->id;
            // }else {
            //     $state  = new States();
            //     $state->lat = $geo_result->result->geometry->location->lat;
            //     $state->lng = $geo_result->result->geometry->location->lng;
            //     $state->countries_id  = $country_id;
            //     $state->active = 1;
            //     if($country_id != 227){
            //         $state->is_province =1;
            //     }
            //     if($state->save()){
            //         $states_id = $state->id;
            //         $state_trans=  new StateTranslation();
            //         $state_trans->states_id =$state->id;
            //         $state_trans->title = $geo_state;
            //         $state_trans->languages_id = 1;
            //         $state_trans->save();
            //     }
            // }
            // $city_id = 2031;
            // // add City as it wasnt added before
            // if(!empty($geo_city_guess)){
            //     $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'.'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($geo_city_guess);
            //     $fetch_link_for_city = file_get_contents($google_link_for_city);
            //     $query_result_for_city = json_decode($fetch_link_for_city);
            //     $qr_city = $query_result_for_city->results;

            //     $city  = new Cities();
            //     $city->lat = $qr_city[0]->geometry->location->lat;
            //     $city->lng = $qr_city[0]->geometry->location->lng;
            //     $city->countries_id  = $country_id;
            //     $city->states_id  = $states_id;
            //     $city->safety_degree_id  = 1;
            //     $city->level_of_living_id  = 0;
            //     $city->active  = 1;
            //     if($city->save()){
            //         $city_id = $city->id;
            //             $city_trans=  new CitiesTranslations();
            //             $city_trans->cities_id =$city->id;
            //             $city_trans->title = $geo_city_guess;
            //             $city_trans->languages_id = 1;
            //             $city_trans->save();
            //     }
            // }   
            // $p->countries_id =$country_id;
            // $p->states_id = $states_id;
            // $p->cities_id =$city_id;
        }
        $p->lat = $geo_result->result->geometry->location->lat;
        $p->lng = $geo_result->result->geometry->location->lng;
        $p->pluscode = @$geo_result->result->plus_code->compound_code;
        $p->rating = @$geo_result->result->rating;
        $p->active = 1;
        $p->auto_import = 1;
        $p->save();

        $pt = new PlaceTranslations();
        $pt->languages_id = 1;
        $pt->places_id = $p->id;
        $pt->title = $geo_result->result->name;
        $pt->address = $geo_result->result->formatted_address;
        $pt->save();
        return $p->id;
    }

    /**
     * @param string $key
     * @return string
     */
    private function _getRandom($key)
    {

        $init_array = [];
        for ($i = 1; $i < 32; $i++)
            $init_array[] = $i;

        $return = [];

        foreach ($init_array as $val) {
            $return[] = $val * $key % 32;
        }

        return implode(',', $return);
    }


    // public function postCheckCheckin($placeId)
    // {
    //     try {

    //         $userId = Auth::user()->id;
    //         $place = Place::find($placeId);
    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place ID']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }

    //         $checkin = Checkins::where('place_id', $placeId)
    //             ->where('users_id', $userId)
    //             ->first();

    //         if (empty($checkin)) {
    //             return ApiResponse::create(
    //                 [],
    //                 false,
    //                 ApiResponse::NO_CONTENT
    //             );
    //         } else {
    //             return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function postCheckin($placeId)
    // {
    //     try {

    //         session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

    //         if (strpos(session('place_country'), 'country') !== false)
    //             return app('App\Http\Controllers\CountryController2')->postCheckin($placeId);

    //         if (strpos(session('place_country'), 'city') !== false)
    //             return app('App\Http\Controllers\CityController')->postCheckin($placeId);

    //         $userId = auth()->user()->id;
    //         $place = Place::find($placeId);
    //         if (!$place) {
    //             return ApiResponse::create([
    //                 'data' => [
    //                     'error' => 400,
    //                     'message' => 'Invalid Place ID',
    //                 ],
    //                 'success' => false
    //             ]);
    //         }


    //         $check = new Checkins();
    //         $postCheckedIn =     Input::get('checkin_time');
    //         if (isset($postCheckedIn)) {
    //             $check->checkin_time = date("Y-m-d", strtotime(Input::get('checkin_time')));
    //         }
    //         $check->users_id = $userId;
    //         $check->place_id = $placeId;
    //         $check->location = $place->transsingle->title ?? 'unnamed';
    //         $check->lat_lng = $place->lat . ',' . $place->lng;
    //         $check->save();
    //         //$this->updateStatistic($country, 'followers', count($country->followers));

    //         log_user_activity('Place', 'checkin', $placeId);

    //         if (strtotime($check->checkin_time) == strtotime(date('Y-m-d'))) {
    //             return ApiResponse::create([
    //                 'success' => true,
    //                 'live' => 1,
    //                 'checkin_id' => $check->id,
    //                 'message' => 'Thank you for checking in! You are live on this page now.'
    //             ]);
    //         } else if (strtotime($check->checkin_time) < strtotime(date('Y-m-d'))) {
    //             return ApiResponse::create([
    //                 'success' => true,
    //                 'live' => 0,
    //                 'checkin_id' => $check->id,
    //                 'message' => 'Thank you for recording your past check-in!'
    //             ]);
    //         } else if (strtotime($check->checkin_time) > strtotime(date('Y-m-d'))) {
    //             return ApiResponse::create([
    //                 'success' => true,
    //                 'live' => 0,
    //                 'checkin_id' => $check->id,
    //                 'message' => 'Thank you for checking in! You will appear live on this page on your check-in date.'
    //             ]);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function postCheckout($placeId)
    // {
    //     try {
    //         $userId = Auth::user()->id;
    //         $place = Place::find($placeId);
    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place ID']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }

    //         $follower = PlaceFollowers::where('places_id', $placeId)
    //             ->where('users_id', $userId);

    //         if (!$follower->first()) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['You are not following this Place.']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         } else {
    //             $follower->delete();
    //             //$this->updateStatistic($country, 'followers', count($country->followers));

    //             log_user_activity('Place', 'checkout', $placeId);
    //             return ApiResponse::create(['message' => [true]]);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    /**
     * @param array $media
     * @return boolean
     */
    public function _create_thumbs($media)
    {
        $complete_url = $media->url;
        $url = explode("/", $complete_url);

        $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

        $options = [
            'region' => 'us-east-1',
            'version' => 'latest',
            'http' => ['verify' => false],
            'credentials' => $credentials,
            'endpoint' => 'https://s3.amazonaws.com'
        ];

        $s3Client = new S3Client($options);
        //$s3 = AWS::createClient('s3');
        $result = $s3Client->getObject([
            'Bucket' => 'travooo-images2', // REQUIRED
            'Key' => $complete_url, // REQUIRED
            'ResponseContentType' => 'text/plain',
        ]);

        $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        //return $img_700->response('jpg');
        //echo $complete_url . '<br />';
        //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

        $put_1100 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_1100->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_700 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_700->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_230 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_230->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);

        $put_180 = $s3Client->putObject([
            'ACL' => 'public-read',
            'Body' => $img_180->encode(),
            'Bucket' => 'travooo-images2',
            'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
        ]);


        $media->thumbs_done = 1;
        $media->save();
        return true;
    }

    /**
     * @OA\Post(
     ** path="/place/media",
     *   tags={"Places"},
     *   summary="Upload media to post",
     *   operationId="upload_media",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="mediafiles",
     *                  type="string",
     *                  format="binary"
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function uploadMedia(Request $request)
    {
        try {
            $media  = $request['mediafiles'];
            if ($media) {
                list($baseType, $media) = explode(';', $media);
                list(, $media) = explode(',', $media);
                $media = base64_decode($media);
                if (strpos($baseType, 'image') !== false)
                    $filename = sha1(microtime()) . "_place_added_file.png";
                else if (strpos($baseType, 'video') !== false)
                    $filename = sha1(microtime()) . "_place_added_file.mp4";

                $userId = Auth::user()->id;
                $userName = Auth::user()->name;
                Storage::disk('s3')->put(
                    $userId . '/medias/' . $filename,
                    $media,
                    'public'
                );
                $media_url = $userId . '/medias/' . $filename;

                return ApiResponse::create(
                    [
                        'media_url' => $media_url
                    ]
                );
            } else {
                return ApiResponse::create(['message' => ["file not found"]], false, ApiResponse::BAD_REQUEST);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    // public function postComment(CommentRequest $request)
    // {
    //     try {
    //         $post_id = $request->post_id;
    //         $pair = $request->pair;
    //         $post_comment = processString($request->text);

    //         $user_id = Auth::user()->id;
    //         $user_name = Auth::user()->name;

    //         $author_id = Posts::find($post_id)->users_id;

    //         $post_comment = is_null($post_comment) ? '' : $post_comment;

    //         $is_files = false;
    //         $file_lists = getTempFiles($pair);

    //         $is_files = count($file_lists) > 0 ? true : false;

    //         if (!$is_files && $post_comment == "")
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);

    //         $post = Posts::find($post_id);
    //         if (is_object($post)) {
    //             $new_comment = new PostsComments;
    //             $new_comment->users_id = $user_id;
    //             $new_comment->posts_id = $post_id;
    //             $new_comment->text = $post_comment;
    //             if ($new_comment->save()) {
    //                 switch (session('place_country')) {
    //                     case 'country':
    //                         log_user_activity('Country_Post', 'comment', $post_id);
    //                         break;
    //                     case 'city':
    //                         log_user_activity('City_Post', 'comment', $post_id);
    //                         break;

    //                     default:
    //                         log_user_activity('Place_Post', 'comment', $post_id);
    //                         break;
    //                 }

    //                 // if($user_id != $author_id)
    //                 //     notify($author_id, 'status_comment', $post_id);

    //                 foreach ($file_lists as $file) {
    //                     $filename = $user_id . '_' . time() . '_' . $file[1];
    //                     Storage::disk('s3')->put('place-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

    //                     $path = 'https://s3.amazonaws.com/travooo-images2/place-comment-photo/' . $filename;

    //                     $media = new Media();
    //                     $media->url = $path;
    //                     $media->author_name = $user_name;
    //                     $media->title = $post_comment;
    //                     $media->author_url = '';
    //                     $media->source_url = '';
    //                     $media->license_name = '';
    //                     $media->license_url = '';
    //                     $media->uploaded_at = date('Y-m-d H:i:s');
    //                     $media->save();

    //                     $posts_comments_medias = new PostsCommentsMedias();
    //                     $posts_comments_medias->posts_comments_id = $new_comment->id;
    //                     $posts_comments_medias->medias_id = $media->id;
    //                     $posts_comments_medias->save();
    //                 }

    //                 return ApiResponse::create(
    //                     [
    //                         'ppc' => $new_comment,
    //                         'post' => $post
    //                     ]
    //                 );
    //             } else {
    //                 return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //             }
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function postCommentEdit(CommentEditRequest $request)
    // {
    //     try {
    //         $pair = $request['pair'];
    //         $post_id = $request->post_id;
    //         $comment_id = $request->comment_id;

    //         $post_comment_edit = processString($request->text);
    //         $user_id = Auth::user()->id;
    //         $author_id = Posts::find($post_id)->users_id;
    //         $post_comment = PostsComments::find($comment_id);

    //         $user_name = Auth::user()->name;

    //         $is_files = false;
    //         $file_lists = getTempFiles($pair);

    //         $is_files = count($file_lists) > 0 ? true : false;
    //         $post_comment_edit = is_null($post_comment_edit) ? '' : $post_comment_edit;
    //         if (!$is_files && $post_comment_edit == "")
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);

    //         $post = Posts::find($post_id);
    //         if (is_object($post)) {
    //             $post_comment->text = $post_comment_edit;
    //             if ($post_comment->save()) {
    //                 switch (session('place_country')) {
    //                     case 'country':
    //                         log_user_activity('Country_Post', 'comment', $post_id);
    //                         break;
    //                     case 'city':
    //                         log_user_activity('City_Post', 'comment', $post_id);
    //                         break;

    //                     default:
    //                         log_user_activity('Place_Post', 'comment', $post_id);
    //                         break;
    //                 }
    //                 if ($user_id != $post_comment->users_id) {
    //                     notify($author_id, 'status_comment', $post_id);
    //                 }

    //                 if (isset($request->existing_media) && count($post_comment->medias) > 0) {
    //                     foreach ($post_comment->medias as $media_list) {
    //                         if (in_array($media_list->media->id, $request->existing_media)) {
    //                             $medias_exist = Media::find($media_list->medias_id);

    //                             $media_list->delete();

    //                             if ($medias_exist) {
    //                                 $amazonefilename = explode('/', $medias_exist->url);
    //                                 Storage::disk('s3')->delete('post-comment-photo/' . end($amazonefilename));

    //                                 $medias_exist->delete();
    //                             }
    //                         }
    //                     }
    //                 }

    //                 foreach ($file_lists as $file) {
    //                     $filename = $user_id . '_' . time() . '_' . $file[1];
    //                     Storage::disk('s3')->put('place-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

    //                     $path = 'https://s3.amazonaws.com/travooo-images2/place-comment-photo/' . $filename;

    //                     $media = new Media();
    //                     $media->url = $path;
    //                     $media->author_name = $user_name;
    //                     $media->title = $post_comment_edit;
    //                     $media->author_url = '';
    //                     $media->source_url = '';
    //                     $media->license_name = '';
    //                     $media->license_url = '';
    //                     $media->uploaded_at = date('Y-m-d H:i:s');
    //                     $media->save();


    //                     $posts_comments_medias = new PostsCommentsMedias();
    //                     $posts_comments_medias->posts_comments_id = $post_comment->id;
    //                     $posts_comments_medias->medias_id = $media->id;
    //                     $posts_comments_medias->save();
    //                 }

    //                 $post_comment->load('medias');

    //                 return ApiResponse::create(
    //                     [
    //                         'ppc' => $post_comment,
    //                         'post' => $post
    //                     ]
    //                 );
    //             } else {
    //                 return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //             }
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function postCommentReply(CommentReplyRequest $request)
    // {
    //     try {
    //         $post_id = $request->post_id;
    //         $comment_id = $request->comment_id;
    //         $pair = $request->pair;

    //         $post_comment_reply = processString($request->text);
    //         $user_id = Auth::user()->id;

    //         $author_id = Posts::find($post_id)->users_id;
    //         $comment_author_id = PostsComments::find($comment_id)->users_id;

    //         $user_name = Auth::user()->name;

    //         $is_files = false;
    //         $file_lists = getTempFiles($pair);

    //         $is_files = count($file_lists) > 0 ? true : false;

    //         $post_comment_reply = is_null($post_comment_reply) ? '' : $post_comment_reply;
    //         if (!$is_files && $post_comment_reply == "")
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);

    //         $post = Posts::find($post_id);
    //         if (is_object($post)) {
    //             $new_comment = new PostsComments;
    //             $new_comment->users_id = $user_id;
    //             $new_comment->posts_id = $post_id;
    //             $new_comment->parents_id = $comment_id;
    //             $new_comment->text = $post_comment_reply;
    //             if ($new_comment->save()) {
    //                 switch (session('place_country')) {
    //                     case 'country':
    //                         log_user_activity('Country_Post', 'commentreply', $post_id);
    //                         break;
    //                     case 'city':
    //                         log_user_activity('City_Post', 'commentreply', $post_id);
    //                         break;

    //                     default:
    //                         log_user_activity('Place_Post', 'commentreply', $post_id);
    //                         break;
    //                 }
    //                 // if($user_id != $comment_author_id)
    //                 //     notify($author_id, 'status_comment', $post_id);

    //                 foreach ($file_lists as $file) {
    //                     $filename = $user_id . '_' . time() . '_' . $file[1];
    //                     Storage::disk('s3')->put('place-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

    //                     $path = 'https://s3.amazonaws.com/travooo-images2/place-comment-photo/' . $filename;

    //                     $media = new Media();
    //                     $media->url = $path;
    //                     $media->author_name = $user_name;
    //                     $media->title = $post_comment_reply;
    //                     $media->author_url = '';
    //                     $media->source_url = '';
    //                     $media->license_name = '';
    //                     $media->license_url = '';
    //                     $media->uploaded_at = date('Y-m-d H:i:s');
    //                     $media->save();

    //                     $posts_comments_medias = new PostsCommentsMedias();
    //                     $posts_comments_medias->posts_comments_id = $new_comment->id;
    //                     $posts_comments_medias->medias_id = $media->id;
    //                     $posts_comments_medias->save();
    //                 }

    //                 return ApiResponse::create(
    //                     [
    //                         'child' => $new_comment,
    //                         'post' => $post
    //                     ]
    //                 );
    //             } else {
    //                 return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //             }
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function postCheckFollow($placeId)
    // {
    //     try {
    //         $userId = Auth::user()->id;
    //         $place = Place::find($placeId);
    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place ID']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }

    //         $follower = PlaceFollowers::where('places_id', $placeId)
    //             ->where('users_id', $userId)
    //             ->first();

    //         if (empty($follower)) {
    //             return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
    //         } else {
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    /**
     * @OA\Get(
     ** path="/place/{place_id}/follow",
     *   tags={"Places"},
     *   summary="Follow the place",
     *   operationId="follow_the_place",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="place_id",
     *        description="Place Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param integer $placeId
     * @return \Illuminate\Http\Response
     */
    public function postFollow($placeId)
    {
        try {
            $userId = Auth::user()->id;
            $place = Place::find($placeId);
            if (!$place) {
                return ApiResponse::createValidationResponse([
                    'message' => ['Invalid Place ID']
                ]);
            }

            $follower = PlaceFollowers::where('places_id', $placeId)
                ->where('users_id', $userId)
                ->exists();

            if (!$follower) {
                $place->followers()->create([
                    'users_id' => $userId
                ]);
                //$this->updateStatistic($country, 'followers', count($country->followers));
                log_user_activity('Place', 'follow', $placeId);
            }


            return ApiResponse::create(
                [
                    'total' =>  PlaceFollowers::where('places_id', $placeId)->count(),
                    'follow_flag' =>  PlaceFollowers::where('places_id', $placeId)
                        ->where('users_id', $userId)
                        ->exists()
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @OA\Get(
     ** path="/place/{place_id}/unfollow",
     *   tags={"Places"},
     *   summary="Unfollow the place",
     *   operationId="unfollow_the_place",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="place_id",
     *        description="Place Id",
     *        required=true,
     *        in="path",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param integer $placeId
     * @return \Illuminate\Http\Response
     */
    public function postUnFollow($placeId)
    {
        try {
            $userId = Auth::user()->id;

            $place = Place::find($placeId);
            if (!$place) {
                return ApiResponse::createValidationResponse([
                    'message' => ['Invalid Place ID']
                ]);
            }

            $follower = PlaceFollowers::where('places_id', $placeId)
                ->where('users_id', $userId);

            if (!$follower->first()) {
                return ApiResponse::__createBadResponse('You are not following this Place yet, so you can not unfollow this place.');
            } else {
                $follower->delete();
                //$this->updateStatistic($country, 'followers', count($country->followers));
                log_user_activity('Place', 'unfollow', $placeId);
            }

            return ApiResponse::create(
                [
                    'total' => PlaceFollowers::where('places_id', $placeId)->count(),
                    'follow_flag' =>  PlaceFollowers::where('places_id', $placeId)
                        ->where('users_id', $userId)
                        ->exists()
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    // public function postTalkingAbout($placeId)
    // {
    //     try {
    //         $place = Place::find($placeId);
    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place ID']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }

    //         $shares = $place->shares;

    //         foreach ($shares as $share) {
    //             $data['shares'][] = array(
    //                 'user_id' => $share->user->id,
    //                 'user_profile_picture' => check_profile_picture($share->user->profile_picture)
    //             );
    //         }

    //         $data['num_shares'] = count($shares);
    //         $data['success'] = true;
    //         return ApiResponse::create($data);

    //         //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function postNowInPlace($placeId)
    // {
    //     try {
    //         $place = Place::find($placeId);
    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place ID']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }

    //         $checkins = Checkins::where('place_id', $place->id)
    //             ->orderBy('id', 'DESC')
    //             ->take(5)
    //             ->get();

    //         $result = array();
    //         $done = array();
    //         foreach ($checkins as $checkin) {
    //             if (!isset($done[$checkin->post_checkin->post->author->id])) {
    //                 if (time() - strtotime($checkin->post_checkin->post->date) < (24 * 60 * 60)) {
    //                     $live = 1;
    //                 } else {
    //                     $live = 0;
    //                 }
    //                 $result[] = array(
    //                     'name' => $checkin->post_checkin->post->author->name,
    //                     'id' => $checkin->post_checkin->post->author->id,
    //                     'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
    //                     'live' => $live
    //                 );
    //                 $done[$checkin->post_checkin->post->author->id] = true;
    //             }
    //         }
    //         $data['live_checkins'] = $result;

    //         $data['success'] = true;
    //         return ApiResponse::create($data);

    //         //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function editReview(Request $request, $id)
    // {
    //     try {
    //         $data['success'] = false;

    //         $review = Reviews::find($id);
    //         $review->text = $request->input('text');
    //         $review->save();

    //         $data['success'] = true;

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function deleteReview($id)
    // {
    //     try {
    //         $data['success'] = false;

    //         try {
    //             $review = Reviews::find($id);
    //             $review->delete();
    //         } catch (\Exception $e) {
    //             return ApiResponse::create($data);
    //         }

    //         $data['success'] = true;

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function happeningToday($placeId)
    // {
    //     try {
    //         $place = Place::find($placeId);
    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place ID']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }

    //         $checkins = Checkins::whereHas("place", function ($query) use ($place) {
    //             $query->where('cities_id', $place->cities_id);
    //             $query->where('id', '!=', $place->id);
    //         })
    //             ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) * cos( radians( SUBSTRING(lat_lng, INSTR(lat_lng, \',\') + 1) ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( SUBSTRING(lat_lng, 1, INSTR(lat_lng, \',\') -1 ) ) ) ) ) AS distance'))
    //             // ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
    //             ->having('distance', '<', 15)
    //             ->orderBy('distance')
    //             ->take(10)
    //             ->get();

    //         //$checkins = Checkins::where('place_id', $place->id)->orderBy('id', 'DESC')->take(5)->get();

    //         $result = array();
    //         foreach ($checkins as $checkin) {
    //             $result[] = array(
    //                 // 'post_id' => $checkin->post_checkin->post->id,
    //                 'lat' => $checkin->place->lat,
    //                 'lng' => $checkin->place->lng,
    //                 // 'photo' => @$checkin->post_checkin->post->medias[0]->media->url,
    //                 'name' => @$checkin->user->name,
    //                 'id' => @$checkin->user->id,
    //                 'title' => $checkin->location,
    //                 'profile_picture' => check_profile_picture(@$checkin->user->profile_picture),
    //                 'date' => diffForHumans($checkin->created_at)
    //             );
    //         }
    //         $data['happenings'] = $result;

    //         $data['success'] = true;
    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function postMorePhoto($placeId, Request $request)
    // {
    //     try {
    //         $language_id = 1;
    //         $num = $request->pagenum;
    //         $skip = ($num) * 6;
    //         $place = Place::find($placeId);

    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place ID']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }

    //         $medias = $place->getMedias;

    //         if (!$medias) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place ID']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //         $data = array();
    //         $c = (count($medias) <= ($skip + 6)) ? count($medias) : $skip + 6;
    //         for ($i = $skip; $i < $c; $i++) {
    //             $data[] = 'https://s3.amazonaws.com/travooo-images2/th230/' . @$medias[$i]->url;
    //         }

    //         return ApiResponse::create(
    //             [
    //                 'medias' => $data,
    //             ]
    //         );
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function discussionTipLoadmore(DiscussionTipLoadmoreRequest $request)
    // {
    //     try {
    //         $discussion_id = $request->dis_id;
    //         $page_num = $request->page_num;

    //         $skip = ($page_num - 1) * 3 + 1;

    //         $discussion = Discussion::find($discussion_id);
    //         if (is_object($discussion)) {
    //             $replies = $discussion->replies()
    //                 ->orderBy('created_at', 'desc')
    //                 ->skip($skip)
    //                 ->take(3)
    //                 ->get();

    //             $totalrows = count($discussion->replies);
    //             $has_next = ($totalrows > ($page_num * 3 + 1)) ? true : false;
    //             $data = [];

    //             foreach ($replies as $re) {
    //                 $reply = $re;
    //                 $reply['profile_picture'] = check_profile_picture($re->author->profile_picture);
    //                 $reply['reply_date'] = diffForHumans($re->created_at);
    //                 $data[] = $reply;
    //             }

    //             return ApiResponse::create(
    //                 [
    //                     "replies" => $data,
    //                 ]
    //             );
    //         } else {
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function mediaComments(MediaCommentsRequest $request)
    // {
    //     try {
    //         $media_id = $request->media_id;

    //         $media = Media::find(1);
    //         if (is_object($media)) {
    //             $comments = $media->comments;

    //             $totalrows = count($comments);
    //             return ApiResponse::create(
    //                 [
    //                     "comments" => $comments,
    //                     "totalrows" => $totalrows,
    //                 ]
    //             );
    //         } else {
    //             return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function postReviewUpDownVote(ReviewUpDownVoteRequest $request)
    // {
    //     try {
    //         $review_id = $request->review_id;
    //         $user_id = Auth::user()->id;

    //         $review = Reviews::find($review_id);

    //         $author_id = $review->by_users_id;

    //         if (count($review->updownvotes()->where('users_id', $user_id)->where('vote_type', $request->vote_type)->get()) == 0) {
    //             $reviewUpvote = new ReviewsVotes;
    //             $reviewUpvote->review_id = $review_id;
    //             $reviewUpvote->users_id = $user_id;
    //             $reviewUpvote->vote_type = $request->vote_type;
    //             $reviewUpvote->save();

    //             if ($request->vote_type == 1) {
    //                 if (count($review->updownvotes()->where('users_id', $user_id)->get()) != 0) {
    //                     $review->updownvotes()->where('users_id', $user_id)->where('vote_type', 2)->delete();
    //                 }
    //                 log_user_activity('Review', 'upvote', $review_id);
    //             } else {
    //                 if (count($review->updownvotes()->where('users_id', $user_id)->get()) != 0) {
    //                     $review->updownvotes()->where('users_id', $user_id)->where('vote_type', 1)->delete();
    //                 }
    //                 log_user_activity('Review', 'downvote', $review_id);
    //             }

    //             return ApiResponse::create(
    //                 [
    //                     'count_upvotes' => count($review->updownvotes()->where('vote_type', 1)->get()),
    //                     'count_downvotes' => count($review->updownvotes()->where('vote_type', 2)->get())
    //                 ]
    //             );
    //         } else {
    //             $review->updownvotes()->where('users_id', $user_id)->where('vote_type', $request->vote_type)->delete();

    //             if ($request->vote_type == 1) {
    //                 log_user_activity('Review', 'upvote', $review_id);
    //             } else {
    //                 log_user_activity('Review', 'downvote', $review_id);
    //             }
    //             return ApiResponse::create(
    //                 [
    //                     'count_upvotes' => count($review->updownvotes()->where('vote_type', 1)->get()),
    //                     'count_downvotes' => count($review->updownvotes()->where('vote_type', 2)->get())
    //                 ]
    //             );
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function shareReview($review_id)
    // {
    //     try {
    //         $user_id = Auth::user()->id;
    //         $review = ReviewsShares::where('users_id', $user_id)->where('review_id', $review_id)->get()->first();

    //         if (is_object($review)) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['You had already shared this place.']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         } else {
    //             $share = new ReviewsShares;
    //             $share->users_id = $user_id;
    //             $share->review_id = $review_id;

    //             log_user_activity('Review', 'share', $review_id);
    //             if ($share->save())
    //                 return ApiResponse::create(
    //                     [
    //                         'message' => ['You have just shared this post.']
    //                     ]
    //                 );
    //             else
    //                 return ApiResponse::create(
    //                     [
    //                         'message' => ['You can not share this post now. Please try to it later.']
    //                     ],
    //                     false,
    //                     ApiResponse::BAD_REQUEST
    //                 );
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function searchAllDatas(SearchAllDatasRequest $request)
    // {
    //     try {
    //         $query = $request->get('q');
    //         // $search_in = $request->get('search_in');
    //         $search_id = $request->get('search_id');

    //         $place = Place::find($search_id);

    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place Id']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         } else {
    //             $data = [];

    //             // getting discussion for place
    //             $discussions = Discussion::where('destination_type', 'Place')
    //                 ->where('destination_id', $place->id)
    //                 ->whereRaw('(question like "%' . $query . '%" or description like "%' . $query . '%")')
    //                 ->paginate(10);

    //             $data['discussions'] = $discussions;
    //             $data['place'] = $place;

    //             // getting text post for place
    //             $post_ary = PostsPlaces::where('places_id', $place->id)->pluck('posts_id')->toArray();
    //             if (count($post_ary) > 0) {
    //                 $posts = Posts::whereIn('id', $post_ary)
    //                     ->whereRaw('text like "%' . $query . '%"')
    //                     ->paginate(10);

    //                 $data['post'] = $posts;
    //             }

    //             // getting media post for place
    //             $media_ary = $place->medias->pluck('id')->toArray();
    //             $post_ary = PostsMedia::whereIn('medias_id', $media_ary)->pluck('posts_id')->toArray();
    //             if (count($post_ary) > 0) {
    //                 $media = Posts::whereIn('id', $post_ary)
    //                     ->whereRaw('text like "%' . $query . '%"')
    //                     ->paginate(10);

    //                 $data['media'] = $media;
    //             }

    //             // getting travelmates for place
    //             $mates_ary = User::whereRaw('(name like "%' . $query . '%" or interests like "%' . $query . '%")')
    //                 ->pluck('id');

    //             $travelmates = TravelMatesRequests::whereHas('plan_place', function ($q) use ($place) {
    //                 $q->where('places_id', '=', $place->id);
    //             })
    //                 ->whereIn('users_id', $mates_ary)
    //                 ->paginate(10);
    //             if (count($travelmates) == 0) {
    //                 $travelmates = TravelMatesRequests::whereHas('plan_city', function ($q) use ($place) {
    //                     $q->where('cities_id', '=', $place->city->id);
    //                 })
    //                     ->whereIn('users_id', $mates_ary)
    //                     ->paginate(10);
    //             }

    //             $data['travelmate_request'] = $travelmates;
    //             // getting reports for place
    //             $reports_ary = ReportsInfos::where('var', 'place')
    //                 ->where('val', $place->id)
    //                 ->whereHas('report', function ($q) {
    //                     $q->where('flag', '=', 1);
    //                 })
    //                 ->pluck('reports_id')->toArray();
    //             if (count($reports_ary) == 0) {
    //                 $reports = Reports::where('cities_id', '=', $place->city->id)
    //                     ->whereRaw('(title like "%' . $query . '%" or description like "%' . $query . '%")')
    //                     ->paginate(10);
    //             } else {
    //                 $reports = Reports::whereIn('id', $reports_ary)
    //                     ->whereRaw('(title like "%' . $query . '%" or description like "%' . $query . '%")')
    //                     ->paginate(10);
    //             }

    //             $data['report'] = $reports;
    //             // getting reviews for place
    //             $reviews = Reviews::where('places_id', $place->id)
    //                 ->whereRaw('text like "%' . $query . '%"')
    //                 ->paginate(10);

    //             $data['review'] = $reviews;

    //             //getting trip plans for place
    //             $trip_plans = TripPlaces::where('places_id', $place->id)->pluck('trips_id')->toArray();

    //             if (count($trip_plans) > 0) {
    //                 $trips = TripPlans::whereIn('id', $trip_plans)
    //                     ->whereRaw('title like "%' . $query . '%"')
    //                     ->paginate(10);

    //                 $data['plan'] = $trips;
    //             }

    //             //getting events for place
    //             $events = array();
    //             try {
    //                 $client = new Client();
    //                 $result = @$client->post('https://auth.predicthq.com/token', [
    //                     'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
    //                     'form_params' => [
    //                         'grant_type' => 'client_credentials',
    //                         'scope' => 'account events signals'
    //                     ],
    //                     'verify' => false
    //                 ]);
    //                 $events_array = false;
    //                 if ($result->getStatusCode() == 200) {
    //                     $json_response = $result->getBody()->read(1024);
    //                     $response = json_decode($json_response);
    //                     $access_token = $response->access_token;

    //                     $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
    //                         'headers' => ['Authorization' => 'Bearer ' . $access_token],
    //                         'verify' => false
    //                     ]);
    //                     if ($get_events1->getStatusCode() == 200) {
    //                         $events1 = json_decode($get_events1->getBody()->read(100024));
    //                         $events_array1 = $events1->results;
    //                         $events_final = $events_array1;
    //                         if ($events1->next) {
    //                             $get_events2 = $client->get($events1->next, [
    //                                 'headers' => ['Authorization' => 'Bearer ' . $access_token],
    //                                 'verify' => false
    //                             ]);
    //                             if ($get_events2->getStatusCode() == 200) {
    //                                 $events2 = json_decode($get_events2->getBody()->read(100024));
    //                                 $events_array2 = $events2->results;
    //                                 $events_final = array_merge($events_array1, $events_array2);
    //                             }
    //                         }
    //                         $events = $events_final;
    //                     }
    //                 }
    //                 if (count($events) > 0) {
    //                     foreach ($events as $evt) {
    //                         if (strpos($evt->title, $query) || strpos($evt->description, $query) || strpos(@$evt->entities[0]->formatted_address, $query)) {
    //                             $data['events'][] = $evt;
    //                         }
    //                     }
    //                 }
    //             } catch (\Throwable $th) {
    //                 return ApiResponse::createServerError($th);
    //             }
    //             return ApiResponse::create($data);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    public function get64Image(Base64ImageRequest $request)
    {
        try {
            $url = $request->url;
            return ApiResponse::create(
                [
                    'base64_image' => "data:image/png;base64," . base64_encode(file_get_contents($url))
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    // public function mediaLikes(MediaLikesRequest $request)
    // {
    //     try {
    //         $media_id = $request->media_id;
    //         $user_id = Auth::user()->id;

    //         $check_like_exists = MediasLikes::where('medias_id', $media_id)->where('users_id', $user_id)->get()->first();
    //         $data = array();

    //         if (is_object($check_like_exists)) {
    //             $check_like_exists->delete();

    //             $data['status'] = 'no';
    //         } else {
    //             $like = new MediasLikes;
    //             $like->medias_id = $media_id;
    //             $like->users_id = $user_id;
    //             $like->save();

    //             switch (session('place_country')) {
    //                 case 'country':
    //                     log_user_activity('Country_Media', 'like', $media_id);
    //                     break;
    //                 case 'city':
    //                     log_user_activity('City_Media', 'like', $media_id);
    //                     break;

    //                 default:
    //                     log_user_activity('Place_Media', 'like', $media_id);
    //                     break;
    //             }

    //             $data['status'] = 'yes';
    //         }
    //         $data['count'] = count(MediasLikes::where('medias_id', $media_id)->get());
    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function eventLikes($event_id)
    // {
    //     try {
    //         $user_id = Auth::user()->id;

    //         $check_like_exists = EventsLikes::where('events_id', $event_id)->where('users_id', $user_id)->get()->first();
    //         $data = array();

    //         if (is_object($check_like_exists)) {
    //             $check_like_exists->delete();

    //             $data['status'] = 'no';
    //         } else {
    //             $like = new EventsLikes;
    //             $like->events_id = $event_id;
    //             $like->users_id = $user_id;
    //             $like->save();

    //             log_user_activity('Event', 'like', $event_id);

    //             $data['status'] = 'yes';
    //         }
    //         $data['count'] = count(EventsLikes::where('events_id', $event_id)->get());
    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function eventShares($event_id)
    // {
    //     try {
    //         $user_id = Auth::user()->id;
    //         $shared = EventsShares::where('users_id', $user_id)->where('events_id', $event_id)->get()->first();

    //         $data = array();
    //         if (is_object($shared)) {
    //             $data['status'] = 'no';
    //             $data['msg'] = 'You had already shared this event.';
    //         } else {
    //             $share = new EventsShares;
    //             $share->users_id = $user_id;
    //             $share->events_id = $event_id;
    //             $share->created_at = date("Y-m-d H:i:s");

    //             if ($share->save()) {
    //                 log_user_activity('Event', 'share', $event_id);
    //                 $data['status'] = 'yes';
    //                 $data['count'] = EventsShares::where('users_id', $user_id)->where('events_id', $event_id)->count();
    //                 $data['msg'] = 'You have just shared this event.';
    //             }
    //         }
    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function eventComments(EventCommentsRequest $request)
    // {
    //     try {
    //         $evt_id = $request->event_id;
    //         $evt_comment = processString($request->text);
    //         $pair = $request->pair;

    //         $user_id = Auth::user()->id;
    //         $user_name = Auth::user()->name;

    //         $evt_comment = is_null($evt_comment) ? '' : $evt_comment;

    //         $is_files = false;
    //         $file_lists = getTempFiles($pair);

    //         $is_files = count($file_lists) > 0 ? true : false;

    //         if (!$is_files && $evt_comment == "")
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);

    //         $event = Events::find($evt_id);
    //         if (is_object($event)) {
    //             $new_comment = new EventsComments;
    //             $new_comment->users_id = $user_id;
    //             $new_comment->events_id = $evt_id;
    //             $new_comment->text = $evt_comment;
    //             if ($new_comment->save()) {
    //                 log_user_activity('Event', 'comment', $evt_id);
    //                 // if($user_id != $author_id)
    //                 //     notify($author_id, 'status_comment', $post_id);
    //                 // ranking_add_points($author_id, 1, 0.2, 'event_comment', $evt_id, $user_id);
    //                 // ranking_add_points($author_id, 5, 0.05, 'event_comment', $evt_id, $user_id);

    //                 foreach ($file_lists as $file) {
    //                     $filename = $user_id . '_' . time() . '_' . $file[1];
    //                     Storage::disk('s3')->put('event-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

    //                     $path = 'https://s3.amazonaws.com/travooo-images2/event-comment-photo/' . $filename;

    //                     $media = new Media();
    //                     $media->url = $path;
    //                     $media->author_name = $user_name;
    //                     $media->title = $evt_comment;
    //                     $media->author_url = '';
    //                     $media->source_url = '';
    //                     $media->license_name = '';
    //                     $media->license_url = '';
    //                     $media->uploaded_at = date('Y-m-d H:i:s');
    //                     $media->save();

    //                     $event_comments_medias = new EventsCommentsMedias();
    //                     $event_comments_medias->events_comments_id = $new_comment->id;
    //                     $event_comments_medias->medias_id = $media->id;
    //                     $event_comments_medias->save();
    //                 }

    //                 return ApiResponse::create(
    //                     [
    //                         'comment' => $new_comment,
    //                         'event' => $event
    //                     ]
    //                 );
    //             } else {
    //                 return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //             }
    //         } else {
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function eventCommentsReply(EventCommentsReplyRequest $request)
    // {
    //     try {
    //         $event_id = $request->event_id;
    //         $comment_id = $request->comment_id;
    //         $pair = $request->pair;

    //         $event_comment_reply = processString($request->text);
    //         $user_id = Auth::user()->id;

    //         $user_name = Auth::user()->name;

    //         $is_files = false;
    //         $file_lists = getTempFiles($pair);

    //         $is_files = count($file_lists) > 0 ? true : false;

    //         $event_comment_reply = is_null($event_comment_reply) ? '' : $event_comment_reply;
    //         if (!$is_files && $event_comment_reply == "")
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);

    //         $event = Events::find($event_id);
    //         if (is_object($event)) {
    //             $new_comment = new EventsComments;
    //             $new_comment->users_id = $user_id;
    //             $new_comment->events_id = $event_id;
    //             $new_comment->parents_id = $comment_id;
    //             $new_comment->text = $event_comment_reply;
    //             if ($new_comment->save()) {
    //                 log_user_activity('Event', 'commentreply', $event_id);
    //                 // if($user_id != $comment_author_id)
    //                 //     notify($author_id, 'status_comment', $post_id);


    //                 foreach ($file_lists as $file) {
    //                     $filename = $user_id . '_' . time() . '_' . $file[1];
    //                     Storage::disk('s3')->put('event-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

    //                     $path = 'https://s3.amazonaws.com/travooo-images2/event-comment-photo/' . $filename;

    //                     $media = new Media();
    //                     $media->url = $path;
    //                     $media->author_name = $user_name;
    //                     $media->title = $event_comment_reply;
    //                     $media->author_url = '';
    //                     $media->source_url = '';
    //                     $media->license_name = '';
    //                     $media->license_url = '';
    //                     $media->uploaded_at = date('Y-m-d H:i:s');
    //                     $media->save();

    //                     $event_comments_medias = new EventsCommentsMedias();
    //                     $event_comments_medias->events_comments_id = $new_comment->id;
    //                     $event_comments_medias->medias_id = $media->id;
    //                     $event_comments_medias->save();
    //                 }

    //                 return ApiResponse::create(
    //                     [
    //                         'child' => $new_comment,
    //                         'event' => $event
    //                     ]
    //                 );
    //             } else {
    //                 return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //             }
    //         } else {
    //             return ApiResponse::create(['message' => ["event not found"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function eventCommentsEdit(EventCommentsEditRequest $request)
    // {
    //     try {
    //         $pair = $request['pair'];
    //         $event_id = $request->event_id;
    //         $comment_id = $request->comment_id;

    //         $event_comment_edit = processString($request->text);
    //         $user_id = Auth::user()->id;
    //         $event_comment = EventsComments::find($comment_id);

    //         $user_name = Auth::user()->name;

    //         $is_files = false;
    //         $file_lists = getTempFiles($pair);

    //         $is_files = count($file_lists) > 0 ? true : false;
    //         $event_comment_edit = is_null($event_comment_edit) ? '' : $event_comment_edit;
    //         if (!$is_files && $event_comment_edit == "")
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);

    //         $event = Events::find($event_id);
    //         if (is_object($event)) {
    //             $event_comment->text = $event_comment_edit;
    //             if ($event_comment->save()) {

    //                 if (isset($request->existing_media) && count($event_comment->medias) > 0) {
    //                     foreach ($event_comment->medias as $media_list) {
    //                         if (in_array($media_list->media->id, $request->existing_media)) {
    //                             $medias_exist = Media::find($media_list->medias_id);

    //                             $media_list->delete();

    //                             if ($medias_exist) {
    //                                 $amazonefilename = explode('/', $medias_exist->url);
    //                                 Storage::disk('s3')->delete('event-comment-photo/' . end($amazonefilename));

    //                                 $medias_exist->delete();
    //                             }
    //                         }
    //                     }
    //                 }

    //                 foreach ($file_lists as $file) {
    //                     $filename = $user_id . '_' . time() . '_' . $file[1];
    //                     Storage::disk('s3')->put('event-comment-photo/' . $filename, fopen($file[0], 'r+'), 'public');

    //                     $path = 'https://s3.amazonaws.com/travooo-images2/event-comment-photo/' . $filename;

    //                     $media = new Media();
    //                     $media->url = $path;
    //                     $media->author_name = $user_name;
    //                     $media->title = $event_comment_edit;
    //                     $media->author_url = '';
    //                     $media->source_url = '';
    //                     $media->license_name = '';
    //                     $media->license_url = '';
    //                     $media->uploaded_at = date('Y-m-d H:i:s');
    //                     $media->save();

    //                     $event_comments_medias = new EventsCommentsMedias();
    //                     $event_comments_medias->events_comments_id = $event_comment->id;
    //                     $event_comments_medias->medias_id = $media->id;
    //                     $event_comments_medias->save();
    //                 }

    //                 $event_comment->load('medias');

    //                 return ApiResponse::create(
    //                     [
    //                         'event_comment' => $event_comment,
    //                         'event' => $event
    //                     ]
    //                 );
    //             } else {
    //                 return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //             }
    //         } else {
    //             return ApiResponse::create(['message' => ["event not found"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function eventCommentsLike($comment_id)
    // {
    //     try {
    //         $user_id = Auth::user()->id;
    //         $event_commment = EventsComments::find($comment_id);
    //         if (!$event_commment) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Comment Id']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //         $author_id = EventsComments::find($comment_id)->users_id;
    //         $event_id = EventsComments::find($comment_id)->events_id;
    //         $check_like_exists = EventsCommentsLikes::where('events_comments_id', $comment_id)->where('users_id', $user_id)->get()->first();
    //         $data = array();

    //         if (is_object($check_like_exists)) {
    //             $check_like_exists->delete();
    //             log_user_activity('Event', 'commentunlike', $comment_id);
    //             //notify($author_id, 'status_commentunlike', $comment_id);

    //             $data['status'] = 'no';
    //         } else {
    //             $like = new EventsCommentsLikes;
    //             $like->events_comments_id = $comment_id;
    //             $like->users_id = $user_id;
    //             $like->save();

    //             log_user_activity('Event', 'commentlike', $comment_id);

    //             $data['status'] = 'yes';
    //         }
    //         $data['count'] = count(EventsCommentsLikes::where('events_comments_id', $comment_id)->get());
    //         $data['name'] = Auth::user()->name;

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function eventCommentsDelete($comment_id)
    // {
    //     try {
    //         $user_id = Auth::user()->id;
    //         // $author_id = Events::find($post_id)->users_id;
    //         $comment = EventsComments::where('users_id', $user_id)->find($comment_id);
    //         if (!$comment) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['comment not found']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }

    //         foreach ($comment->medias as $com_media) {
    //             $medias_exist = Media::find($com_media->medias_id);

    //             $com_media->delete();

    //             if ($medias_exist) {
    //                 $amazonefilename = explode('/', $medias_exist->url);
    //                 Storage::disk('s3')->delete('event-comment-photo/' . end($amazonefilename));

    //                 $medias_exist->delete();
    //             }
    //         }

    //         if ($comment->delete()) {
    //             return ApiResponse::create(['message' => ["Delete successfully"]], false, ApiResponse::BAD_REQUEST);
    //         } else {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['You can not delete this comment now. Please try to it later.']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function eventComments4Modal(EventComments4ModalRequest $request)
    // {
    //     try {
    //         $event_id = $request->event_id;
    //         $count = $request->skip;

    //         $skip = $count;

    //         $event = Events::find($event_id);
    //         $totalrows = 0;
    //         if (is_object($event)) {
    //             $comments = $event->comments()->skip($skip)
    //                 ->take(5)
    //                 ->get();

    //             $totalrows = count($event->comments);
    //             return ApiResponse::create(
    //                 [
    //                     "comments" => $comments,
    //                     "event" => $event,
    //                     "rows" => $totalrows
    //                 ]
    //             );
    //         } else {
    //             return ApiResponse::create(['message' => ["event not found"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function getCommentLikeUsers($comment_id)
    // {
    //     try {
    //         $comment = EventsComments::find($comment_id);
    //         if (!$comment) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Comment Id']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //         if ($comment->likes) {
    //             return ApiResponse::create(
    //                 [
    //                     'likes' => $comment->likes()->orderBy('created_at', 'DESC')->get()
    //                 ]
    //             );
    //         } else {
    //             return ApiResponse::create(
    //                 [
    //                     'likes' => 0
    //                 ]
    //             );
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function checkEvent($place_id)
    // {
    //     try {
    //         $place = Place::find($place_id);
    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place Id']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //         $client = new Client();
    //         //$client->getHttpClient()->setDefaultOption('verify', false);

    //         $data = array();
    //         $data['data'] = '';
    //         $data['cnt'] = 0;
    //         try {

    //             $result = @$client->post('https://auth.predicthq.com/token', [
    //                 'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
    //                 'form_params' => [
    //                     'grant_type' => 'client_credentials',
    //                     'scope' => 'account events signals'
    //                 ],
    //                 'verify' => false
    //             ]);
    //             $events_array = false;
    //             if ($result->getStatusCode() == 200) {
    //                 $json_response = $result->getBody()->read(1024);
    //                 $response = json_decode($json_response);
    //                 $access_token = $response->access_token;

    //                 $get_events1 = $client->get('https://api.predicthq.com/v1/events/?end.gte=' . date('Y-m-d') . '&within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
    //                     'headers' => ['Authorization' => 'Bearer ' . $access_token],
    //                     'verify' => false
    //                 ]);
    //                 if ($get_events1->getStatusCode() == 200) {
    //                     $events1 = json_decode($get_events1->getBody()->read(100024));
    //                     $events_array1 = $events1->results;
    //                     $events_final = $events_array1;
    //                     if ($events1->next) {
    //                         $get_events2 = $client->get($events1->next, [
    //                             'headers' => ['Authorization' => 'Bearer ' . $access_token],
    //                             'verify' => false
    //                         ]);
    //                         if ($get_events2->getStatusCode() == 200) {
    //                             $events2 = json_decode($get_events2->getBody()->read(100024));
    //                             $events_array2 = $events2->results;
    //                             $events_final = array_merge($events_array1, $events_array2);
    //                         }
    //                     }
    //                     $events = $events_final;

    //                     foreach ($events as $event) {
    //                         $check_envent = Events::where('provider_id', $event->id)->where('places_id', $place->id)->get()->first();
    //                         if (!is_object($check_envent)) {
    //                             $new_event = new Events();
    //                             $new_event->places_id = $place->id;
    //                             $new_event->provider_id = @$event->id;
    //                             $new_event->category = @$event->category;
    //                             $new_event->title = @$event->title;
    //                             $new_event->description = @$event->description;
    //                             $new_event->address = @$event->entities[0]->formatted_address;
    //                             $new_event->labels = @join(",", $event->labels);
    //                             $new_event->lat = @$event->location[1];
    //                             $new_event->lng = @$event->location[0];
    //                             $new_event->start = @$event->start;
    //                             $new_event->end = @$event->end;
    //                             $new_event->save();
    //                         }
    //                     }
    //                     $event_datas = $place->events()->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')->get();
    //                     $data['cnt'] = count($event_datas);
    //                     $data['event_datas'] = $event_datas;
    //                 }
    //             }
    //         } catch (\Throwable $e) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => $e->getMessage()
    //                 ],
    //                 false
    //             );
    //         }

    //         $postIds = get_postlist4placemedia($place->id, 'place');
    //         foreach ($place->getMedias as $media) {
    //             $posts = PostsMedia::where('medias_id', $media->id)->pluck('posts_id')->toArray();
    //             if (!count(array_intersect($posts, $postIds))) {
    //                 $post = new Posts();
    //                 $post->updated_at = $media->uploaded_at;
    //                 $post->permission = 0;
    //                 $post->save();
    //                 $postplace = new PostsPlaces();
    //                 $postplace->posts_id = $post->id;
    //                 $postplace->places_id = $place->id;
    //                 $postplace->save();
    //                 $postmedia = new PostsMedia();
    //                 $postmedia->posts_id = $post->id;
    //                 $postmedia->medias_id = $media->id;
    //                 $postmedia->save();
    //             }
    //         }

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    /**
     * @OA\Get(
     ** path="/place/get_media",
     *   tags={"Places"},
     *   summary="Get place media",
     *   operationId="get_place_media",
     *   security={
     *       {"bearer_token": {}
     *    }},
     *    @OA\Parameter(
     *        name="place_id",
     *        description="Place Id",
     *        required=true,
     *        in="query",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/

    // public function getMedia(MediaRequest $request)
    // {
    //     try {
    //         $place_id = $request->get('place_id');
    //         $final_results = array();
    //         $h = Place::find($place_id);
    //         if (is_object($h)) {
    //             $details_link = file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?'
    //                 . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
    //                 . '&placeid=' . $h->provider_id);
    //             $details = json_decode($details_link);
    //             if (isset($details->result)) {
    //                 $details = $details->result;
    //                 if (isset($details->photos)) {
    //                     $place_photos = $details->photos;
    //                     $raw_photos = array();
    //                     $i = 1;

    //                     foreach ($place_photos as $pp) {
    //                         $file_contents = file_get_contents('https://maps.googleapis.com/maps/api/place/photo?'
    //                             . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
    //                             . '&maxwidth=1600'
    //                             . '&photoreference=' . $pp->photo_reference);

    //                         $sha1 = sha1($file_contents);
    //                         $md5 = md5($file_contents);
    //                         $size = strlen($file_contents);
    //                         $raw_photos[$i]['sha1'] = $sha1;
    //                         $raw_photos[$i]['md5'] = $md5;
    //                         $raw_photos[$i]['size'] = $size;
    //                         $raw_photos[$i]['contents'] = $file_contents;
    //                         $i++;


    //                         $check_media_exists = Media::where('sha1', $sha1)
    //                             ->where('md5', $md5)
    //                             ->where('filesize', $size)
    //                             ->get()
    //                             ->count();
    //                         if ($check_media_exists == 0) {
    //                             $media_file = 'places/' . $h->provider_id . '/' . sha1(microtime()) . '.jpg';
    //                             Storage::disk('s3')->put($media_file, $file_contents, 'public');


    //                             $media = new Media;
    //                             $media->url = $media_file;
    //                             $media->sha1 = $sha1;
    //                             $media->filesize = $size;
    //                             $media->md5 = $md5;
    //                             $media->html_attributions = join(",", $pp->html_attributions);
    //                             $media->save();
    //                             if ($media->save()) {
    //                                 $place_media = new PlaceMedias;
    //                                 $place_media->places_id = $h->id;
    //                                 $place_media->medias_id = $media->id;
    //                                 $place_media->save();

    //                                 $complete_url = $media->url;
    //                                 $url = explode("/", $complete_url);
    //                                 //$data = file_get_contents('https://s3.amazonaws.com/travooo-images2/'.$complete_url);
    //                                 //$s3 = App::make('aws')->createClient('s3');

    //                                 $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

    //                                 $options = [
    //                                     'region' => 'us-east-1',
    //                                     'version' => 'latest',
    //                                     'http' => ['verify' => false],
    //                                     'credentials' => $credentials,
    //                                     'endpoint' => 'https://s3.amazonaws.com'
    //                                 ];

    //                                 $s3Client = new S3Client($options);
    //                                 //$s3 = AWS::createClient('s3');
    //                                 $result = $s3Client->getObject([
    //                                     'Bucket' => 'travooo-images2', // REQUIRED
    //                                     'Key' => $complete_url, // REQUIRED
    //                                     'ResponseContentType' => 'text/plain',
    //                                 ]);

    //                                 $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
    //                                     $constraint->aspectRatio();
    //                                 });
    //                                 $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
    //                                     $constraint->aspectRatio();
    //                                 });
    //                                 $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
    //                                     $constraint->aspectRatio();
    //                                 });
    //                                 $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
    //                                     $constraint->aspectRatio();
    //                                 });

    //                                 //return $img_700->response('jpg');
    //                                 //echo $complete_url . '<br />';
    //                                 //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

    //                                 $put_1100 = $s3Client->putObject([
    //                                     'ACL' => 'public-read',
    //                                     'Body' => $img_1100->encode(),
    //                                     'Bucket' => 'travooo-images2',
    //                                     'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
    //                                 ]);

    //                                 $put_700 = $s3Client->putObject([
    //                                     'ACL' => 'public-read',
    //                                     'Body' => $img_700->encode(),
    //                                     'Bucket' => 'travooo-images2',
    //                                     'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
    //                                 ]);

    //                                 $put_230 = $s3Client->putObject([
    //                                     'ACL' => 'public-read',
    //                                     'Body' => $img_230->encode(),
    //                                     'Bucket' => 'travooo-images2',
    //                                     'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
    //                                 ]);

    //                                 $put_180 = $s3Client->putObject([
    //                                     'ACL' => 'public-read',
    //                                     'Body' => $img_180->encode(),
    //                                     'Bucket' => 'travooo-images2',
    //                                     'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
    //                                 ]);

    //                                 $media->thumbs_done = 1;
    //                                 $media->save();
    //                             }
    //                         }
    //                     }

    //                     if (isset($put_180))
    //                         return ApiResponse::create(
    //                             [
    //                                 'url' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2]
    //                             ]
    //                         );
    //                     else
    //                         return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //                 }
    //             }
    //         } else {
    //             return ApiResponse::create(['message' => ["place not found"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function getPOIMedia(Request $request)
    // {
    //     try {
    //         $place_id = $request->get('place_id');
    //         $is_new = $request->get('is_new');
    //         $final_results = array();
    //         $h = Place::find($place_id);
    //         if (!$h) {
    //             return ApiResponse::create(
    //                 [
    //                     "message" => ["Invalid Place Id"]
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //         if (isset($is_new) && $is_new == 1 && $h->is_media_added == 1) {
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //         if (is_object($h)) {
    //             $details_link = file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?'
    //                 . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
    //                 . '&placeid=' . $h->provider_id);
    //             $details = json_decode($details_link);
    //             if (isset($details->result)) {
    //                 $details = $details->result;
    //                 if (isset($details->photos)) {
    //                     $place_photos = $details->photos;
    //                     $raw_photos = array();
    //                     $i = 1;

    //                     foreach ($place_photos as $pp) {
    //                         $file_contents = file_get_contents('https://maps.googleapis.com/maps/api/place/photo?'
    //                             . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
    //                             . '&maxwidth=1600'
    //                             . '&photoreference=' . $pp->photo_reference);

    //                         $sha1 = sha1($file_contents);
    //                         $md5 = md5($file_contents);
    //                         $size = strlen($file_contents);
    //                         $raw_photos[$i]['sha1'] = $sha1;
    //                         $raw_photos[$i]['md5'] = $md5;
    //                         $raw_photos[$i]['size'] = $size;
    //                         $raw_photos[$i]['contents'] = $file_contents;
    //                         $i++;


    //                         $check_media_exists = Media::where('sha1', $sha1)
    //                             ->where('md5', $md5)
    //                             ->where('filesize', $size)
    //                             ->get();


    //                         if ($check_media_exists->count() <= 0) {
    //                             $media_file = 'places/' . $h->provider_id . '/' . sha1(microtime()) . '.jpg';
    //                             Storage::disk('s3')->put($media_file, $file_contents, 'public');


    //                             $media = new Media;
    //                             $media->url = $media_file;
    //                             $media->sha1 = $sha1;
    //                             $media->filesize = $size;
    //                             $media->md5 = $md5;
    //                             $media->html_attributions = join(",", $pp->html_attributions);
    //                             $media->save();

    //                             if ($media->save()) {
    //                                 $place_media = new PlaceMedias;
    //                                 $place_media->places_id = $h->id;
    //                                 $place_media->medias_id = $media->id;
    //                                 $place_media->save();

    //                                 // create new post
    //                                 $po = new Posts();
    //                                 $po->save();

    //                                 // link post to place
    //                                 $pp = new PostsPlaces();
    //                                 $pp->posts_id = $po->id;
    //                                 $pp->places_id = $h->id;
    //                                 $pp->save();

    //                                 // link media to post
    //                                 $posts_medias = new PostsMedia();
    //                                 $posts_medias->posts_id = $po->id;
    //                                 $posts_medias->medias_id = $media->id;
    //                                 $posts_medias->save();

    //                                 $complete_url = $media->url;
    //                                 $url = explode("/", $complete_url);
    //                                 //$data = file_get_contents('https://s3.amazonaws.com/travooo-images2/'.$complete_url);
    //                                 //$s3 = App::make('aws')->createClient('s3');

    //                                 $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

    //                                 $options = [
    //                                     'region' => 'us-east-1',
    //                                     'version' => 'latest',
    //                                     'http' => ['verify' => false],
    //                                     'credentials' => $credentials,
    //                                     'endpoint' => 'https://s3.amazonaws.com'
    //                                 ];

    //                                 $s3Client = new S3Client($options);
    //                                 //$s3 = AWS::createClient('s3');
    //                                 $result = $s3Client->getObject([
    //                                     'Bucket' => 'travooo-images2', // REQUIRED
    //                                     'Key' => $complete_url, // REQUIRED
    //                                     'ResponseContentType' => 'text/plain',
    //                                 ]);

    //                                 $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
    //                                     $constraint->aspectRatio();
    //                                 });
    //                                 $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
    //                                     $constraint->aspectRatio();
    //                                 });
    //                                 $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
    //                                     $constraint->aspectRatio();
    //                                 });
    //                                 $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
    //                                     $constraint->aspectRatio();
    //                                 });

    //                                 //return $img_700->response('jpg');
    //                                 //echo $complete_url . '<br />';
    //                                 //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

    //                                 $put_1100 = $s3Client->putObject([
    //                                     'ACL' => 'public-read',
    //                                     'Body' => $img_1100->encode(),
    //                                     'Bucket' => 'travooo-images2',
    //                                     'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
    //                                 ]);

    //                                 $put_700 = $s3Client->putObject([
    //                                     'ACL' => 'public-read',
    //                                     'Body' => $img_700->encode(),
    //                                     'Bucket' => 'travooo-images2',
    //                                     'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
    //                                 ]);

    //                                 $put_230 = $s3Client->putObject([
    //                                     'ACL' => 'public-read',
    //                                     'Body' => $img_230->encode(),
    //                                     'Bucket' => 'travooo-images2',
    //                                     'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
    //                                 ]);

    //                                 $put_180 = $s3Client->putObject([
    //                                     'ACL' => 'public-read',
    //                                     'Body' => $img_180->encode(),
    //                                     'Bucket' => 'travooo-images2',
    //                                     'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
    //                                 ]);

    //                                 $media->thumbs_done = 1;
    //                                 $media->save();
    //                             }
    //                         }
    //                     }

    //                     if (isset($is_new) && $is_new == 1 && $h->is_media_added == 0) {
    //                         $h->is_media_added = 1;
    //                         $h->save();
    //                         $place = Place::with([
    //                             'getMedias',
    //                             'getMedias.users',
    //                         ])
    //                             ->where('id', $place_id)
    //                             ->where('active', 1)
    //                             ->first();
    //                         return ApiResponse::create($place);
    //                     } else {
    //                         if (isset($put_180))
    //                             return ApiResponse::create(
    //                                 [
    //                                     'url' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2]
    //                                 ]
    //                             );
    //                         else
    //                             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //                     }
    //                 }
    //             }
    //         } else {
    //             return ApiResponse::create(['message' => ["place not found"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function setPOIMedia(POIMediaRequest $request)
    // {
    //     try {
    //         ignore_user_abort();
    //         $request = new Request();
    //         $request->request->add([
    //             'place_id' => $request->place_id,
    //         ]);
    //         $images  = app(PlaceController2::class)->getPOIMedia($request);
    //         return $images;
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function getTripData($id)
    // {
    //     try {
    //         $plan = TripPlans::find($id);
    //         $data = [];

    //         if (is_object($plan)) {
    //             $points = [];
    //             $polylines = "";
    //             $center = [];
    //             $zoom = 8;
    //             $markers = [];
    //             $waypoint = [];
    //             $lats = [];
    //             $lngs = [];

    //             foreach ($plan->places as $pl) :
    //                 $markers[] = "markers=anchor:center|icon:" . asset("assets2/image/marker_s.png") . "|" . $pl->lat . "," . $pl->lng;

    //                 $points[] = $pl->lat . "," . $pl->lng;
    //                 $waypoint[] = "via:" . $pl->lat . "," . $pl->lng;
    //                 $lats[] = $pl->lat;
    //                 $lngs[] = $pl->lng;
    //             endforeach;

    //             $newwaypoint = $waypoint;
    //             @$origin = $points[0];
    //             @$destin = $points[count($points) - 1];

    //             $url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' . $origin . '&destination=' . $destin . '&waypoints=optimize:true|' . implode("|", $newwaypoint) . '&mode=driving&key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
    //             $ch = curl_init();
    //             curl_setopt($ch, CURLOPT_URL, $url);
    //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //             curl_setopt($ch, CURLOPT_POST, false);
    //             $result = curl_exec($ch);
    //             curl_close($ch);
    //             $googleDirection = json_decode($result, true);
    //             try {
    //                 $polyline = urlencode($googleDirection['routes'][0]['overview_polyline']['points']);
    //                 $polylines = "enc:" . $polyline;

    //                 $lat = ($googleDirection['routes'][0]['bounds']['southwest']['lat'] + $googleDirection['routes'][0]['bounds']['northeast']['lat']) / 2;
    //                 $lng = ($googleDirection['routes'][0]['bounds']['southwest']['lng'] + $googleDirection['routes'][0]['bounds']['northeast']['lng']) / 2;
    //                 $center[0] = isset($center[0]) ? ($center[0] + $lat) / 2 : $lat;
    //                 $center[1] = isset($center[1]) ? ($center[1] + $lng) / 2 : $lng;

    //                 $west = $googleDirection['routes'][0]['bounds']['southwest']['lng'];
    //                 $east = $googleDirection['routes'][0]['bounds']['northeast']['lng'];

    //                 $angle = $east - $west;
    //                 if ($angle < 0) {
    //                     $angle += 360;
    //                 }
    //                 $zoom = @floor(log(712 * 360 / $angle / 256) / log(2)) - 1;
    //             } catch (\Throwable $th) {
    //                 if (count($lats) > 0) {
    //                     $center[0] = array_sum($lats) / count($lats);
    //                     $center[1] = array_sum($lngs) / count($lngs);

    //                     $angle = max($lngs) - min($lngs);
    //                     if ($angle < 0) {
    //                         $angle += 360;
    //                     }
    //                     $zoom = @floor(log(712 * 360 / $angle / 256) / log(2)) - 2;
    //                 }
    //                 $polylines = "geodesic:true|" . implode('|', $points);
    //             }

    //             @$center = count($center) > 0 ? $center : [$plan->places[0]->lat, $plan->places[0]->lng];
    //             $zoom = $zoom === INF ? 14 : $zoom;
    //             $markers = implode("&", $markers);
    //             $data['msg'] = 'ok';
    //             $data['content'] = ['plan' => $plan, 'center' => $center, 'zoom' => $zoom, 'markers' => $markers, 'polylines' => $polylines];
    //         } else {
    //             $data['msg'] = "Try later.";
    //         }

    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function discussion_replies_24hrs($place_id, DiscussionRepliesRequest $request)
    // {
    //     try {
    //         session()->put('place_country', str_replace('/', '', \Request::route()->getPrefix()));

    //         $place = Place::find($place_id);
    //         $user_id = $request->get('user_id');

    //         $discussions = DiscussionReplies::whereHas('discussion', function ($q) use ($place_id) {
    //             $q->where('destination_type', 'place')
    //                 ->where('destination_id', $place_id);
    //         })
    //             ->where('created_at', '>', date("Y-m-d", strtotime('-24 hours')))
    //             ->where('users_id', $user_id)
    //             ->orderBy('id', 'DESC')
    //             ->get();
    //         if (count($discussions) > 0) {
    //             return ApiResponse::create(
    //                 [
    //                     'discussions' => $discussions,
    //                     'place' => $place
    //                 ]
    //             );
    //         } else {
    //             return ApiResponse::create(['message' => ["discussions not found"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function newsfeed_show_all($place_id, NewsfeedShowAllRequest $request)
    // {
    //     try {

    //         $place = Place::find($place_id);
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');

    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();
    //         $data = [];
    //         //$all_collection = collect_posts('place', $place_id, $page);
    //         $filter = $request->all();
    //         $all_collection  = getPostsIdForAllFeed($place_id, $page, 'place', $filter);
    //         if (count($all_collection) > 0) {
    //             foreach ($all_collection as $ppost) {
    //                 $collections = [];

    //                 if ($ppost['type'] == 'regular') {
    //                     $post = Posts::find($ppost['id']);

    //                     // if(is_object($post)){
    //                     //     if ($post->text != '' && count($post->medias) > 0) {
    //                     //         $more = View::make('site.place2.partials.post_media_with_text', array());
    //                     //     } elseif ($post->text == '' && count($post->medias) > 0) {
    //                     //         if(!is_object($post->author)){
    //                     //             if(isset($post->medias[0]->media))
    //                     //                 $more = View::make('site.place2.partials.post_media_without_text', array());
    //                     //         }else
    //                     //             $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));

    //                     //     } else {
    //                     //         $more = View::make('site.place2.partials.post_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));

    //                     //     }
    //                     // }
    //                     if (is_object($post)) {
    //                         $collections[] = ['post' => $post, 'place' => $place, 'checkins' => @$post->checkin[0]];
    //                     }
    //                 } elseif ($ppost['type'] == 'discussion') {
    //                     $discussion = Discussion::find($ppost['id']);
    //                     if (is_object($discussion))
    //                         $collections = array('discussion' => $discussion, 'place' => $place);
    //                 } elseif ($ppost['type'] == 'plan') {
    //                     $plan = TripPlaces::find($ppost['id']);
    //                     if (isset($plan->trip) && is_object($plan->trip))
    //                         $collections = array('plan' => $plan->trip, 'place' => $place);
    //                 } elseif ($ppost['type'] == 'report') {
    //                     if ((isset($filter['location']) || isset($filter['people']))) {
    //                         $followers = getUsersByFilters($filter);
    //                         $report = Reports::whereId($ppost['id'])->whereIn('users_id', $followers)->first();
    //                     } else {
    //                         $report = Reports::find($ppost['id']);
    //                     }
    //                     if (is_object($report)) {
    //                         $collections = array('report' => $report, 'place' => $place);
    //                     }
    //                 } elseif ($ppost['type'] == 'review') {
    //                     $reviews = Reviews::find($ppost['id']);
    //                     if (is_object($reviews))
    //                         $collections = ['place' => $place, 'review' => $reviews];
    //                 }
    //                 $timestamp = (new Carbon($ppost['timestamp']))->timestamp;
    //                 // if (!array_key_exists($timestamp, $output)) {
    //                 //     $output[$timestamp] = '';
    //                 // }
    //                 // if(isset( $more) && $more !='')
    //                 //     $output[$timestamp] = $collections;
    //                 if (!empty($collections)) {
    //                     $data['collections'][] = $collections;
    //                 }
    //             }
    //         }

    //         //** dont append in the series  */
    //         // foreach ($this->newsfeed_show_travelmates($place_id, $request, true) as $key => $travelmates) {
    //         //     $output[$key] = $travelmates;
    //         // }
    //         if (!isset($filter['q']) && !isset($filter['people']) && !isset($filter['location'])) {
    //             foreach ($this->newsfeed_show_events($place_id, $request, true, true) as $key => $events) {
    //                 $data[$key] = $events;
    //             }
    //         }

    //         // foreach ($this->newsfeed_show_reports($place_id, $request, true) as $key => $reports) {
    //         //     $output[$key] = $reports;
    //         // }

    //         // foreach ($this->newsfeed_show_reviews($place_id, $request, true) as $key => $reviews) {
    //         //     $output[$key] = $reviews;
    //         // }

    //         $this->utf8_encode_deep($data);
    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    function utf8_encode_deep(&$input)
    {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                self::utf8_encode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                self::utf8_encode_deep($input->$var);
            }
        }
    }


    // public function newsfeed_show_events($place_id, Request $request)
    // {
    //     try {
    //         $place = Place::find($place_id);
    //         if ($place) {
    //             $page = $request->get('page');
    //             $skip = ($page - 1) * 5;

    //             $place->title = $place->trans[0]->title;
    //             unset($place->trans);

    //             $events_collection = Events::where('places_id', $place_id)
    //                 ->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')
    //                 ->orderBy('id', 'desc');

    //             $events_collection_total    = $events_collection->count();
    //             $events_collection          = $events_collection->skip($skip)->take(5)->get();

    //             return ApiResponse::create([
    //                 'place' => $place,
    //                 'total' => $events_collection_total,
    //                 'events' => $events_collection,
    //             ]);
    //         } else {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ["place not found"]
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function newsfeed_show_discussions($place_id, NewsfeedShowDiscussionRequest $request)
    // {
    //     try {
    //         $place = Place::find($place_id);
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');
    //         $skip = ($page - 1) * 5;
    //         $filter = $request->all();
    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();

    //         // $get_posts_discussions_collection ='';
    //         if (isset($filter['location']) || isset($filter['people']) || isset($filter['q']) || isset($filter['sort'])) {
    //             if (isset($filter['location']) || isset($filter['people']))
    //                 $followers = getUsersByFilters($filter);
    //             else
    //                 $followers = [];
    //             $query_search = isset($filter['q']) ? $filter['q'] : "";
    //             $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
    //                 ->where('destination_type', 'place')
    //                 ->where(function ($get_posts_discussions_collection) use ($query_search) {
    //                     if (isset($query_search)) {
    //                         $get_posts_discussions_collection->where('question', 'like', '%' . $query_search . '%');
    //                     }
    //                 })
    //                 ->orderBy('id', 'desc')
    //                 ->where(function ($get_posts_discussions_collection) use ($followers) {
    //                     if (!empty($followers)) {
    //                         $get_posts_discussions_collection->where('users_id', $followers);
    //                     }
    //                 })
    //                 ->skip($skip)
    //                 ->take(5)
    //                 ->get();
    //         } else {
    //             $get_posts_discussions_collection = Discussion::where('destination_id', $place_id)
    //                 ->where('destination_type', 'place')
    //                 ->orderBy('id', 'desc')
    //                 ->skip($skip)
    //                 ->take(5)
    //                 ->get();
    //         }
    //         if (count($get_posts_discussions_collection) > 0) {
    //             return ApiResponse::create(
    //                 [
    //                     'place' => $place,
    //                     'discussions' => $get_posts_discussions_collection
    //                 ]
    //             );
    //         } else {
    //             return ApiResponse::create(['message' => ["discussions not found"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function newsfeed_show_top($place_id, NewsfeedShowTopRequest $request)
    // {
    //     try {
    //         $place = Place::find($place_id);
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');
    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();

    //         $data = [];
    //         $filter = $request->all();
    //         $all_collection = collect_posts('place', $place_id, $page, true, false, $filter);
    //         if (count($all_collection) > 0) {
    //             foreach ($all_collection as $ppost) {
    //                 $collection = [];

    //                 if ($ppost['type'] == 'regular') {
    //                     $post = Posts::find($ppost['id']);
    //                     // if ($post->text != '' && count($post->medias) > 0) {
    //                     //     $more = View::make('site.place2.partials.post_media_with_text', array('post' => $post, 'place' => $place));
    //                     // } elseif ($post->text == '' && count($post->medias) > 0) {
    //                     //     if(!is_object($post->author)){
    //                     //         if(isset($post->medias[0]->media))
    //                     //             $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));
    //                     //     }else
    //                     //         $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));
    //                     // } else {
    //                     //     $more = View::make('site.place2.partials.post_text', array('post' => $post, 'place' => $place));
    //                     // }
    //                     if ($post) {
    //                         $collection = array('post' => $post, 'place' => $place, 'checkins' => @$post->checkin[0]);
    //                     }
    //                 } elseif ($ppost['type'] == 'discussion') {
    //                     $discussion = Discussion::find($ppost['id']);
    //                     // $more = View::make('site.place2.partials.post_discussion', array('discussion' => $discussion, 'place' => $place));
    //                     if ($discussion) {
    //                         $collection = array('discussion' => $discussion, 'place' => $place);
    //                     }
    //                 } elseif ($ppost['type'] == 'plan') {
    //                     $plan = TripPlaces::find($ppost['id']);
    //                     // if(isset($plan->trip)){
    //                     //     if(isset($filter['q']) ){
    //                     //         if(strpos(strtolower($plan->trip->title), strtolower($filter['q'])) !== false){
    //                     //             $more = View::make('site.place2.partials.post_plan', array('plan' => $plan->trip, 'place' => $place));
    //                     //         }
    //                     //     }else{
    //                     //         $more = View::make('site.place2.partials.post_plan', array('plan' => $plan->trip, 'place' => $place));
    //                     //     }
    //                     // }
    //                     if ($plan) {
    //                         $collection = array('plan' => $plan->trip, 'place' => $place);
    //                     }
    //                 } elseif ($ppost['type'] == 'report') {
    //                     $report = Reports::find($ppost['id']);
    //                     // if ($report) {
    //                     //     if(isset($filter['q']) ){
    //                     //         if(strpos(strtolower($report->title), strtolower($filter['q'])) !== false){
    //                     //             $more = View::make('site.place2.partials.post_report', array('report' => $report, 'place' => $place));
    //                     //         }
    //                     //     }else
    //                     //         $more = View::make('site.place2.partials.post_report', array('report' => $report, 'place' => $place));
    //                     // }
    //                     if ($plan) {
    //                         $collection = array('report' => $report, 'place' => $place);
    //                     }
    //                 }
    //                 if (!empty($collection)) {
    //                     $data['collection'][] = $collection;
    //                 }
    //             }
    //         }

    //         // $nf_travelmates = $this->newsfeed_show_travelmates($place_id, $request, true, true);
    //         // foreach($nf_travelmates as $nf_item){
    //         //     $output .= $nf_item;
    //         // }

    //         // $nf_events = $this->newsfeed_show_events($place_id, $request, true);
    //         // foreach($nf_events as $nf_item){
    //         //     $output .= $nf_item;
    //         // }

    //         // $nf_reports = $this->newsfeed_show_reports($place_id, $request, true, true);
    //         // foreach($nf_reports as $nf_item){
    //         //     $output .= $nf_item;
    //         // }

    //         // $nf_reviews = $this->newsfeed_show_reviews($place_id, $request, true, true);
    //         // foreach($nf_reviews as $nf_item){
    //         //     $output .= $nf_item;
    //         // }

    //         $this->utf8_encode_deep($data);
    //         return ApiResponse::create($data);
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function newsfeed_show_media($place_id, NewsfeedShowMediaRequest $request)
    // {
    //     try {
    //         $place = Place::find($place_id);
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');
    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();

    //         $data = [];
    //         $filter = $request->all();
    //         $all_collection = collect_posts('place', $place_id, $page, false, true, $filter);
    //         if (count($all_collection) > 0) {
    //             foreach ($all_collection as $ppost) {
    //                 $collection = [];
    //                 if ($ppost['type'] == 'regular') {
    //                     $post = Posts::find($ppost['id']);

    //                     // if ($post->text != '' && count($post->medias) > 0) {
    //                     //     $more = View::make('site.place2.partials.post_media_with_text', array('post' => $post));
    //                     //     $output .= $more->render();
    //                     // } elseif ($post->text == '' && count($post->medias) > 0) {
    //                     //     if(!is_object($post->author)){
    //                     //         if(isset($post->medias[0]->media))
    //                     //             $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));
    //                     //     }else
    //                     //         $more = View::make('site.place2.partials.post_media_without_text', array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]));

    //                     //     $output .= $more->render();
    //                     //     $collection = array('post' => $post, 'place' => $place,'checkins'=>@$post->checkin[0]);
    //                     // } else {

    //                     // }

    //                     if ($post) {
    //                         $collection = array('post' => $post, 'place' => $place, 'checkins' => @$post->checkin[0]);
    //                     }
    //                 }
    //                 if (!empty($collection)) {
    //                     $data['collection'][] = $collection;
    //                 }
    //             }
    //             return ApiResponse::create($data);
    //         } else {
    //             return ApiResponse::create(['message' => ["something went wrong"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function newsfeed_show_travelmates($place_id, NewsfeedShowTravelmatesRequest $request, $internal = false, $top = false)
    // {
    //     try {
    //         $place = Place::find($place_id);
    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place Id']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');
    //         $skip = ($page - 1) * 5;
    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();

    //         $output = '';
    //         $get_posts_mates_collection = TravelMatesRequests::whereHas('plan_place', function ($q) use ($place) {
    //             $q->where('places_id', '=', $place->id);
    //         })
    //             ->groupBy('users_id')
    //             ->orderBy('id', 'desc')
    //             ->paginate(5);
    //         // ->skip($skip)
    //         // ->take(5)
    //         // ->get();
    //         if ($get_posts_mates_collection->total() == 0) {
    //             $get_posts_mates_collection = TravelMatesRequests::whereHas('plan_city', function ($q) use ($place) {
    //                 $q->where('cities_id', '=', $place->city->id);
    //             })
    //                 ->groupBy('users_id')
    //                 ->orderBy('id', 'desc')
    //                 ->paginate(5);
    //             // ->skip($skip)
    //             // ->take(5)
    //             // ->get();
    //         }
    //         $internalOutput = [];

    //         if ($get_posts_mates_collection->count() > 0) {
    //             return ApiResponse::create(
    //                 [
    //                     'travel_mates' => $get_posts_mates_collection
    //                 ]
    //             );
    //         } else {
    //             return ApiResponse::create(['message' => ["not found"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function newsfeed_show_reports($place_id, NewsfeedShowReportsRequest $request, $internal = false, $top = false)
    // {
    //     try {

    //         $place = Place::find($place_id);
    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place Id']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');
    //         $skip = ($page - 1) * 5;
    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();
    //         $output = '';
    //         $flag = false;
    //         $filter = $request->all();
    //         if (!$top) {
    //             if (isset($filter['location']) || isset($filter['people']) || isset($filter['q']) || isset($filter['sort'])) {
    //                 if (isset($filter['location']) || isset($filter['people']))
    //                     $followers = getUsersByFilters($filter);
    //                 else
    //                     $followers = [];
    //             }
    //             $query_search = isset($filter['q']) ? $filter['q'] : "";
    //             $followers = getUsersByFilters($filter);
    //             $get_posts_reports_collection = ReportsInfos::where('var', 'place')
    //                 ->where('val', $place_id)

    //                 ->whereHas('report', function ($q) use ($followers) {
    //                     $q->where('flag', '=', 1);
    //                     if (!empty($followers))
    //                         $q->whereIn('users_id', $followers);
    //                 })
    //                 ->paginate(5);
    //             // ->skip($skip)
    //             // ->take(5)
    //             // ->get();
    //             if ($get_posts_reports_collection->total() == 0 && (!isset($filter['location']) && !isset($filter['people']))) {
    //                 $flag = true;
    //                 $get_posts_reports_collection = Reports::where('cities_id', '=', $place->city->id)
    //                     ->paginate(5);
    //                 // ->skip($skip)
    //                 // ->take(5)
    //                 // ->get();
    //             }
    //         } else {
    //             $get_posts_reports_collection = ReportsInfos::where('var', 'place')
    //                 ->selectRaw('*, ((select count(*) from `reports_comments` where `reports_id`=`reports_infos`.`reports_id`) 
    //                                     + (select count(*) from `reports_likes` where `reports_id`=`reports_infos`.`reports_id`) 
    //                                     + (select count(*) from `reports_views` where `reports_id`=`reports_infos`.`reports_id`) 
    //                                     + (select count(*) from `reports_shares` where `reports_id`=`reports_infos`.`reports_id`)) as rank')
    //                 ->where('val', $place_id)
    //                 ->orderBy('rank', 'DESC')
    //                 ->paginate(5);
    //             // ->skip($skip)
    //             // ->take(5)
    //             // ->get();
    //             if ($get_posts_reports_collection->total() == 0) {
    //                 $flag = true;
    //                 $get_posts_reports_collection = Reports::where('cities_id', '=', $place->city->id)
    //                     ->selectRaw('*, ((select count(*) from `reports_comments` where `reports_id`=`reports`.`id`) 
    //                                         + (select count(*) from `reports_likes` where `reports_id`=`reports`.`id`) 
    //                                         + (select count(*) from `reports_views` where `reports_id`=`reports`.`id`) 
    //                                         + (select count(*) from `reports_shares` where `reports_id`=`reports`.`id`)) as rank')
    //                     ->orderBy('rank', 'DESC')
    //                     ->paginate(5);
    //                 // ->skip($skip)
    //                 // ->take(5)
    //                 // ->get();
    //             }
    //         }

    //         $internalOutput = [];

    //         if ($get_posts_reports_collection->count() > 0) {
    //             // foreach ($get_posts_reports_collection AS $gpdc) {
    //             //     if(isset($filter['q'])){
    //             //         if (isset($gpdc->title) && strpos(strtolower($gpdc->title), strtolower($query_search)) !== false) {
    //             //             $report = $flag ? $gpdc : $gpdc->report;
    //             //             $view = View::make('site.place2.partials.post_report', ['place' => $place,
    //             //                         'report' => $report]);
    //             //             $output .= $view->render();
    //             //             if (!array_key_exists($report->created_at->timestamp, $internalOutput)) {
    //             //                 $internalOutput[$report->created_at->timestamp] = '';
    //             //             }
    //             //             $internalOutput[$report->created_at->timestamp] .= $view->render(); 
    //             //         }
    //             //     }else{
    //             //         $report = $flag ? $gpdc : $gpdc->report;
    //             //             $view = View::make('site.place2.partials.post_report', ['place' => $place,
    //             //                         'report' => $report]);
    //             //             $output .= $view->render();
    //             //             if (!array_key_exists($report->created_at->timestamp, $internalOutput)) {
    //             //                 $internalOutput[$report->created_at->timestamp] = '';
    //             //             }
    //             //             $internalOutput[$report->created_at->timestamp] .= $view->render();
    //             //     }
    //             // }

    //             // krsort($internalOutput);
    //             return ApiResponse::create(
    //                 [
    //                     'reports' => $get_posts_reports_collection
    //                 ]
    //             );
    //         } else {
    //             return ApiResponse::create(['message' => ["reports not found"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    // public function newsfeed_show_reviews($place_id, NewsfeedShowReportsReviews $request, $internal = false, $top = false)
    // {
    //     try {
    //         $place = Place::find($place_id);
    //         if (!$place) {
    //             return ApiResponse::create(
    //                 [
    //                     'message' => ['Invalid Place Id']
    //                 ],
    //                 false,
    //                 ApiResponse::BAD_REQUEST
    //             );
    //         }
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');
    //         $skip = ($page - 1) * 5;
    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();
    //         $filter = $request->all();
    //         if (!$top) {
    //             if (isset($filter['location']) || isset($filter['people']) || isset($filter['q']) || isset($filter['sort'])) {
    //                 if (isset($filter['location']) || isset($filter['people']))
    //                     $followers = getUsersByFilters($filter);
    //                 else
    //                     $followers = [];
    //                 $query_search = isset($filter['q']) ? $filter['q'] : "";
    //                 $get_posts_review_collection = Reviews::where('places_id', $place_id)
    //                     ->orderBy('id', 'desc')
    //                     ->where(function ($get_posts_review_collection) use ($query_search) {
    //                         if (isset($query_search)) {
    //                             $get_posts_review_collection->where('text', 'like', '%' . $query_search . '%');
    //                         }
    //                     })
    //                     ->where(function ($get_posts_review_collection) use ($followers) {
    //                         if (!empty($followers))
    //                             $get_posts_review_collection->whereIn('users_id', $followers);
    //                     })
    //                     ->skip($skip)
    //                     ->take(5)
    //                     ->get();
    //             } else {
    //                 $get_posts_review_collection = Reviews::where('places_id', $place_id)
    //                     ->orderBy('id', 'desc')
    //                     ->skip($skip)
    //                     ->take(5)
    //                     ->get();
    //             }
    //         } else {
    //             $get_posts_review_collection = Reviews::where('places_id', $place_id)
    //                 ->selectRaw('*, ((select count(*) from `reviews_votes` where `review_id`=`reviews`.`id` and `vote_type`=1) 
    //                                     + (select count(*) from `reviews_shares` where `review_id`=`reviews`.`id`)) as rank')
    //                 ->orderBy('rank', 'desc')
    //                 ->skip($skip)
    //                 ->take(5)
    //                 ->get();
    //         }
    //         $internalOutput = [];

    //         if (count($get_posts_review_collection) > 0) {
    //             return ApiResponse::create(
    //                 [
    //                     'reviews' => $get_posts_review_collection
    //                 ]
    //             );
    //         } else {
    //             return ApiResponse::create(['message' => ["reviews not found"]], false, ApiResponse::BAD_REQUEST);
    //         }
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function newsfeed_show_tripplan($place_id, Request $request)
    // {
    //     try {
    //         $place = Place::find($place_id);
    //         $user_id = $request->get('user_id');
    //         $page = $request->get('page');
    //         $skip = ($page - 1) * 5;
    //         //$filters = View::make('site.place2.partials._posts_filters_block');
    //         //$output = $filters->render();

    //         $output = '';
    //         $filter = $request->all();

    //         if (isset($filter['location']) || isset($filter['people']) || isset($filter['q']) || isset($filter['sort'])) {
    //             if (isset($filter['location']) || isset($filter['people']))
    //                 $triplist = get_triplist4me($filter);
    //             else
    //                 $triplist = get_triplist4me();
    //         } else
    //             $triplist = get_triplist4me();
    //         $get_posts_plans_collection = TripPlaces::where('places_id', $place_id)
    //             ->whereIn('trips_id', $triplist)
    //             ->orderBy('id', 'DESC')
    //             ->paginate(5);
    //         // ->skip($skip)
    //         // ->take(5)
    //         // ->get();
    //         if ($get_posts_plans_collection->total() == 0) {
    //             $get_posts_plans_collection = TripPlaces::where('cities_id', $place->city->id)
    //                 ->whereIn('trips_id', $triplist)
    //                 ->orderBy('id', 'DESC')
    //                 ->paginate(5);
    //         }
    //         if ($get_posts_plans_collection->count() > 0 || $output == '') {
    //             $output = 'no';
    //         }
    //         return ApiResponse::create($output);
    //     } catch (\Throwable $th) {
    //         return ApiResponse::createServerError($th);
    //     }
    // }

    /**
     * @param integer $place_id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getPlaceAboutInfo($place_id, Request $request)
    {
        try {
            $pagenum = Input::get('pagnum');
            $data = [];
            $place = Place::find($place_id);
            $countries_id = isset($place->countries_id) ? $place->countries_id : 0;
            $cities_id = isset($place->cities_id) ? $place->cities_id : 0;
            $language_id  = 1;
            $city = '';
            $country = '';
            if ($cities_id != 0) {
                $city = Cities::with([
                    'trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages_spoken.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias'

                ])
                    ->where('id', $cities_id)
                    ->where('active', 1)
                    ->first();
            }
            if ($countries_id != 0) {
                $country = Countries::with([
                    'trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'languages.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'religions.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    }, 'region.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'currencies.trans' => function ($query) use ($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',

                ])
                    ->where('id', $countries_id)
                    ->where('active', 1)
                    ->first();
            }
            // get Etiquettes of city 
            $etiquette = [];
            if (isset($city->trans[0]->etiquette)) {
                $etiquette_array = explode('|', $city->trans[0]->etiquette);
                if (count($etiquette_array) > 0) {
                    foreach ($etiquette_array as $et) {
                        $temp  = explode(':-', $et);
                        if (isset($temp[1])) {
                            $temp[0] = str_replace("\r", "", $temp[0]);
                            $temp[0] = str_replace("\n", "", $temp[0]);
                            $etiquette[$temp[0]] = $temp[1];
                        }
                    }
                }
            }
            // if not exist get from counrty
            if (!empty($etiquette)) {
                $data[] = ['Ettiquette' => $etiquette];
            } else {
                if (isset($country->trans[0]->etiquette)) {
                    $etiquette_array = explode('|', $country->trans[0]->etiquette);
                    if (count($etiquette_array) > 0) {
                        foreach ($etiquette_array as $et) {
                            $temp  = explode(':-', $et);
                            if (isset($temp[1])) {
                                $temp[0] = str_replace("\r", "", $temp[0]);
                                $temp[0] = str_replace("\n", "", $temp[0]);
                                $etiquette[$temp[0]] = $temp[1];
                            }
                        }
                    }
                }
                if (!empty($etiquette))
                    $data[] = ['Ettiquette' => $etiquette];
            }
            // get Potential Dangers of City
            $potential_dangers = [];
            if (isset($city->trans[0]->potential_dangers)) {
                $potential_dangers_array = explode('|', $city->trans[0]->potential_dangers);
                if (count($potential_dangers_array) > 0) {
                    foreach ($potential_dangers_array as $et) {
                        $temp  = explode(':-', $et);
                        if (isset($temp[1])) {
                            $temp[0] = str_replace("\r", "", $temp[0]);
                            $temp[0] = str_replace("\n", "", $temp[0]);
                            $potential_dangers[$temp[0]] = $temp[1];
                        }
                    }
                }
            }
            // if not exist get from counrty
            if (!empty($potential_dangers)) {
                $data[] = ['Potential Dangers' => $potential_dangers];
            } else {
                if (isset($country->trans[0]->potential_dangers)) {
                    $potential_dangers_array = explode('|', $country->trans[0]->potential_dangers);
                    if (count($potential_dangers_array) > 0) {
                        foreach ($potential_dangers_array as $et) {
                            $temp  = explode(':-', $et);
                            if (isset($temp[1])) {
                                $temp[0] = str_replace("\r", "", $temp[0]);
                                $temp[0] = str_replace("\n", "", $temp[0]);
                                $potential_dangers[$temp[0]] = $temp[1];
                            }
                        }
                    }
                }
                if (!empty($potential_dangers))
                    $data[] = ['Potential Dangers' => $potential_dangers];
            }

            // get restrictions for City

            $restrictions = [];
            if (isset($city->trans[0]->economy)) {
                $restrictions_array = explode('|', $city->trans[0]->economy);
                if (count($restrictions_array) > 0) {
                    foreach ($restrictions_array as $et) {
                        $temp  = explode(':-', $et);
                        if (isset($temp[1])) {
                            $temp[0] = str_replace("\r", "", $temp[0]);
                            $temp[0] = str_replace("\n", "", $temp[0]);
                            $restrictions[$temp[0]] = $temp[1];
                        }
                    }
                }
            }
            // if not exist get from counrty
            if (!empty($restrictions)) {
                $data[] = ['Restrictions' => $restrictions];
            } else {
                if (isset($country->trans[0]->economy)) {
                    $restrictions_array = explode('|', $country->trans[0]->economy);
                    if (count($restrictions_array) > 0) {
                        foreach ($restrictions_array as $et) {
                            $temp  = explode(':-', $et);
                            if (isset($temp[1])) {
                                $temp[0] = str_replace("\r", "", $temp[0]);
                                $temp[0] = str_replace("\n", "", $temp[0]);
                                $restrictions[$temp[0]] = $temp[1];
                            }
                        }
                    }
                }
                if (!empty($restrictions))
                    $data[] = ['Restrictions' => $restrictions];
            }


            // get packing trips for city

            $packing_tips = [];
            if (isset($city->trans[0]->planning_tips)) {
                $packing_tips_array = explode('|', $city->trans[0]->planning_tips);
                if (count($packing_tips_array) > 0) {
                    foreach ($packing_tips_array as $et) {
                        $temp  = explode(':-', $et);
                        if (isset($temp[1])) {
                            $temp[0] = str_replace("\r", "", $temp[0]);
                            $temp[0] = str_replace("\n", "", $temp[0]);
                            $packing_tips[$temp[0]] = $temp[1];
                        }
                    }
                }
            }
            // if not exist get from counrty
            if (!empty($packing_tips)) {
                $data[] = ['Packing Tips' => $packing_tips];
            } else {
                if (isset($country->trans[0]->planning_tips)) {
                    $packing_tips_array = explode('|', $country->trans[0]->planning_tips);
                    if (count($packing_tips_array) > 0) {
                        foreach ($packing_tips_array as $et) {
                            $temp  = explode(':-', $et);
                            if (isset($temp[1])) {
                                $temp[0] = str_replace("\r", "", $temp[0]);
                                $temp[0] = str_replace("\n", "", $temp[0]);
                                $packing_tips[$temp[0]] = $temp[1];
                            }
                        }
                    }
                }
                if (!empty($packing_tips))
                    $data[] = ['Packing Tips' => $packing_tips];
            }


            // get description +image of City
            if (isset($city->getMedias[0]->url) && isset($city->trans[0]->description)) {
                $data[] = ['Description' => ['image' => $city->getMedias[0]->url, 'description' => strip_tags($city->trans[0]->description)]];
            } else {
                $data[] = ['Description' => ['image' => @$country->getMedias[0]->url, 'description' => strip_tags(@$country->trans[0]->description)]];
            }


            // get population map
            if (isset($city->trans[0]->population) && isset($city->lng) && isset($city->lat)) {
                $data[] = ['population' => ['population' => $city->trans[0]->population, 'lat' => $city->lat, 'lng' => $city->lng]];
            } else {
                $data[] = ['population' => ['population' => $country->trans[0]->population, 'lat' => $country->lat, 'lng' => $country->lng]];
            }


            // get languages with language code
            $religions = [];
            if (count($city->religions) > 0) {
                foreach ($city->religions as $lang) {
                    if (isset($lang->trans[0]->title)) {
                        $religions[$lang->trans[0]->title] = $lang->trans[0]->title;
                    }
                }
            }
            // if not exist get from country
            if (isset($religions)) {
                $data[] = ['Religions' => $religions];
            } else {

                if (count($country->religions) > 0) {
                    foreach ($country->religions as $lang) {
                        if (isset($lang->trans[0]->title)) {
                            $religions[$lang->trans[0]->title] = $lang->trans[0]->title;
                        }
                    }
                }
                $data[] = ['Religions' => $religions];
            }

            // get relegions of city

            $languages = [];
            if (count($city->languages_spoken) > 0) {
                foreach ($city->languages_spoken as $lang) {
                    if (isset($lang->trans)) {
                        $languages[$lang->trans->iso_code] = $lang->trans->title;
                    }
                }
            }
            // if not exist get from country
            if (isset($languages)) {
                $data[] = ['Languages' => $languages];
            } else {

                if (count($country->languages) > 0) {
                    foreach ($country->languages as $lang) {
                        if (isset($lang->trans)) {
                            $languages[$lang->trans->iso_code] = $lang->trans->title;
                        }
                    }
                }
                if (isset($languages))
                    $data[] = ['Languages' => $languages];
            }

            // get currencies  of city

            // get Indexes of the city
            $indexes = [];
            if (isset($city->trans[0]->cost_of_living)) {
                $index['Cost of Living Index'] = $city->trans[0]->cost_of_living;
            } else {
                $index['Cost of Living Index'] = $country->trans[0]->cost_of_living;
            }
            if (isset($city->trans[0]->geo_stats)) {
                $index['Crime Rate Index'] = $city->trans[0]->geo_stats;
            } else {
                $index['Crime Rate Index'] = $country->trans[0]->geo_stats;
            }

            if (isset($city->trans[0]->demographics)) {
                $index['Quality of Life Index'] = $city->trans[0]->demographics;
            } else {
                $index['Quality of Life Index'] = $country->trans[0]->demographics;
            }

            if (isset($city->trans[0]->pollution_index)) {
                $index['Pollution Index'] = $city->trans[0]->pollution_index;
            } else {
                $index['Pollution Index'] = $country->trans[0]->pollution_index;
            }
            if (isset($city->trans[0]->title)) {
                $title = $city->trans[0]->title;
            } else {
                $title = $country->trans[0]->title;
            }
            //$data[]  =['index'=>$index];
            if (isset($data[$pagenum]) && count($data[$pagenum]) > 0) {
                $placeInfo = ['data' => $data[$pagenum], 'pagenum' => $pagenum, 'title' => $title, 'flag' => strtolower($country->iso_code), 'country' => @$country->trans[0]->title, 'region' => @$country->region->trans[0]->title];

                return ApiResponse::create($placeInfo);
            } else {
                return ApiResponse::create([], true, ApiResponse::NO_CONTENT);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    // public function mediaComments4Modal(MediaCommentsModelRequest $request)
    // {
    //     try {
    //         $media_id = $request->media_id;
    //         $count = $request->skip;

    //         $skip = $count;

    //         $media = Media::find($media_id);
    //         $totalrows = 0;
    //         $author = '';
    //         $comments = [];
    //         if (is_object($media)) {
    //             $comments = $media->comments()->skip($skip)
    //                 ->take(5)
    //                 ->get();

    //             // foreach ($comments as $comment) {
    //             //     $rowcontent .= view('site.home.partials.media_comment_block', array('comment' => $comment, 'post_object' => $media));
    //             // }

    //             $totalrows = count($media->comments);
    //             $author = is_object($media->mediaUser) ? $media->mediaUser->user : '';
    //             if ($author != '') {
    //                 $author->profile_picture = $author->profile_picture == '' ? '' : check_profile_picture($author->profile_picture);
    //                 $author->name = $author->name == null ? '' : $author->name;
    //             }
    //         }

    //         return ApiResponse::create(
    //             [
    //                 "data" => ['comment' => $comments, 'post_object' => $media],
    //                 "author" => $author,
    //                 "uploaded_at" => diffForHumans($media->uploaded_at),
    //                 "caption" => $media->title,
    //                 "rows" => $totalrows,
    //                 "media" => $media,
    //                 "media_likes" => count($media->likes),
    //                 "like_class" => i_like('media', $media->id) ? 'red' : ''
    //             ]
    //         );
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }


    // public function tripComments4Modal(TripCommentsModelRequest $request)
    // {
    //     try {
    //         $trip_id = $request->trip_id;
    //         $count = $request->skip;

    //         $skip = $count;

    //         $plan = TripPlans::find($trip_id);
    //         $totalrows = 0;
    //         $author = '';
    //         $comments = [];
    //         if (is_object($plan)) {
    //             $comments = $plan->comments()->skip($skip)
    //                 ->take(5)
    //                 ->get();

    //             $totalrows = count($plan->comments);
    //         }

    //         return ApiResponse::create(
    //             [
    //                 "comments" => $comments,
    //                 "rows" => $totalrows
    //             ]
    //         );
    //     } catch (\Throwable $e) {
    //         return ApiResponse::createServerError($e);
    //     }
    // }

    /**
     * @OA\Post(
     ** path="/place/get-post-detail",
     *   tags={"Places"},
     *   summary="Get post detail",
     *   operationId="ge_post_detail",
     *   security={
     *       {"bearer_token": {}
     *           }},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="postId",
     *                  type="integer",
     *              ),
     *          )
     *      )
     *  ),
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * @param PostByIdRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getPostById(PostByIdRequest $request)
    {
        try {
            $post = Posts::find($request->postId);
            $place = [];
            if (isset($post->posts_place[0]['places_id'])) {
                $place = Place::find($post->posts_place[0]['places_id']);
            }

            return ApiResponse::create(
                [
                    'post' => $post,
                    'place' => $place,
                    'checkins' => @$post->checkin[0]
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
