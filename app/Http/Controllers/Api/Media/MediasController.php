<?php

namespace App\Http\Controllers\Api\Media;

/* Dependencies */

use App\Helpers\UrlGenerator;
use App\Http\Requests\Api\Medias\ActivityMediasRequest;
use App\Http\Requests\Api\Medias\CommentMediaRequest;
use App\Http\Requests\Api\Medias\CreateMediaRequest;
use App\Http\Requests\Api\Medias\DeleteMediasRequest;
use App\Http\Requests\Api\Medias\ShareMediasRequest;
use App\Http\Requests\Api\Medias\UnlikeMediasRequest;
use App\Http\Requests\Api\Medias\UpdateDescriptionMediasRequest;
use App\Models\Access\language\Languages;
use App\Models\ActivityLog\ActivityLog;
use App\Models\ActivityMedia\MediasCommentsLikes;
use App\Models\User\ApiUser as User;
use App\Models\ActivityMedia\Media;
use App\Models\ActivityMedia\MediasComments;
use App\Models\ActivityMedia\MediasHides;
use App\Models\ActivityMedia\MediasLikes;
use App\Models\ActivityMedia\MediasReports;
use App\Models\ActivityMedia\MediasShares;
use App\Models\ActivityMedia\MediaTranslations;
use App\Models\System\Session;
use App\Models\User\UsersHiddenContent;
use App\Models\User\UsersMedias;
use App\Transformers\Media\MediaReactionsTransformer;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Resource\Item;

/**
 * @resource Media
 *
 * Media (images & videos) uploaded by users
 */
class MediasController extends Controller
{
    /**
     * Create a new Media
     *
     * This function will add new Media uploaded by user
     *
     * @param  integer $user_id         ID of user creating the Media
     * @param  string  $session_token   authentication session token for the user
     * @param  integer $language_id     ID of Language
     * @param  string  $media_file      Uploaded file for Media
     * @param  string  $media_description   short description about Media
     * @return boolean  returns true if the sql row does exist
     */
    public function create(CreateMediaRequest $request) {
        $userId = Auth::Id();
        /* Check That Uploads Directory Exists, If Not Create It */
        $path = storage_path() . DIRECTORY_SEPARATOR . 'uploads';
        if( !is_dir( $path ) ){
            mkdir($path, 0755);
        }

        /* Check That Medias Directory Exists, If Not Create It */
        $path .= DIRECTORY_SEPARATOR . 'medias';
        if( !is_dir( $path ) ){
            mkdir($path, 0755);
        }

        /* Check That Users Directory Exists, If Not Create It */
        $path .= DIRECTORY_SEPARATOR . 'users';
        if( !is_dir( $path ) ){
            mkdir($path, 0755);
        }

        /* Check That User's Id Directory Exists, If Not Create It */
        $path .= DIRECTORY_SEPARATOR . $userId;
        if( !is_dir( $path ) ){
            mkdir($path, 0755);
        }

        $path .= DIRECTORY_SEPARATOR;

        /* Upload File */
        $new_file_name = time() . '_media.' . $request->file('media_file')->extension();
        $new_path = '/uploads/medias/users/' . $userId;
        $request->media_file->storeAs( $new_path , $new_file_name);
        /* Upload File End */


        $media = new Media();
        $media_translation = new MediaTranslations;
        $user_media = new UsersMedias;

        $media_path = 'medias/users/' . $userId;
        /* New Url Of Uploaded Image */
        $media_url = UrlGenerator::GetUploadsUrl() .  $media_path . '/' . $new_file_name;
        // $media_url = asset('/storage' . $media_path . '/' . $new_file_name);

        $media->url = $media_url;
        $media->uploaded_at = Date('Y-m-d h:i:s');
        $media->save();

        $media_translation->medias_id = $media->id;

        /* If Description Of Media Is Set, Save It */
        if( isset($post['media_description']) && !empty($post['media_description']) ){
            $media_translation->description = $post['media_description'];
        }else{
            $media_translation->description = 'No description provided.';
        }

        $media_translation->languages_id = $request->input('language_id');

        /* Save Updated Image Name In User's Table */
        $media_translation->title = $new_file_name;
        $media_translation->save();

        $user_media->users_id  = $userId;
        $user_media->medias_id = $media->id;
        $user_media->save();

        /* Return Success Status, And Successfull Message */
        return [
            'success' => true,
            'data' => [
                'message' => 'Media Created Successfully',
                'url' => $media->url
            ]
        ];
    }

    /* Comment On Media Api */
    /**
     * @param CommentMediaRequest $request
     * @param $mediasId
     * @return array
     */
    public function comment(CommentMediaRequest $request, $mediasId) {
        $userId = Auth::Id();

        /* Find Media For The Provided Media Id */
        $media = Media::find($mediasId);

        /* if Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }
        /* Create New Comment */
        $comment = new MediasComments;

        /* Load Information In Comment */
        $comment->users_id   = $userId;
        $comment->medias_id  = $mediasId;
        $comment->comment    = "comment_test: " . $request->input('comment') . ',';
        $comment->created_at = Date('Y-m-d h:i:s');

        $parentCommentId = $request->input('reply_to');
        /* If Parent Comment Id Is Set, Save Parent Id Info, Else Put 0 In The Parent Id Field. */
        if(!isset($parentCommentId) || empty($parentCommentId) ){
            $comment->reply_to = 0;
        }else{
            /* If Parent Id Field Id Not Empty, Find Parent Comment For Provided Parent Id */
            $parent_comment = MediasComments::where(['id' => $parentCommentId])->first();

            /* If Parent Is Not Found, Return Error */
            if(!empty($parent_comment)){
                $comment->reply_to = $parentCommentId;
            }else{
                return [
                    'data' => [
                        'error'     => 400,
                        'message'   => 'Wrong Parent Comment Id Provided.',
                    ],
                    'status'    => false
                ];
            }
        }

        /* Save Comment Information To Database */
        $comment->save();
        log_user_activity('Media', 'comment', $mediasId);

        return [
            'status' => true,
            'data' => [
                'message' => 'Comment Added Successfully.',
                'MediasCommentsId' => $comment->id
            ]
        ];
    }

    /* Like Media Api */

    /**
     * @param $mediasId
     * @return array
     */
    public function like($mediasId) {
        $userId = Auth::Id();

        /* Find Media For The Provided Media Id */
        $media = Media::find($mediasId);

        /* if Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }

        $mediasLikeCheck = MediasLikes::where('medias_id', $mediasId)
                                      ->where('users_id', $userId)
                                      ->first();

        if (empty($mediasLikeCheck)) {
            /* Create New Media Like */
            $like = new MediasLikes;

            /* Save Information In MediasLikes Model */
            $like->users_id  = $userId;
            $like->medias_id = $mediasId;

            /* Save Information To Database */
            $like->save();
            $liked = true;
            log_user_activity('Media', 'like', $mediasId);
        } else {
            /*Remove the MediasLikes record when MediasLikes object exists for this Media for the same User*/
            $mediasLikeCheck->delete();
            log_user_activity('Media', 'unlike', $mediasId);
            $liked = false;
        }

        /* Return Success Status, With Success Liked Message */
        return [
            'status' => true,
            'liked'  => $liked
        ];
    }

    /* Unlike Media Api */
    /**
     * @param UnlikeMediasRequest $request
     * @return array
     */
    public function unlike(UnlikeMediasRequest $request) {
        /* Get Post Arguments From Request Input */
        $mediaId = $request->input('medias_id');
        $userId  = Auth::Id();

        /* Find Media For The Provided Media Id */
        $media = Media::find($mediaId);

        /* If Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }

        /* Find Media Like For The Provided Media id And User Id */
        $like = MediasLikes::where(['users_id' => $userId , 'medias_id' => $mediaId ])->first();

        /* If Media Like Found For Provided User, Delete Record */
        if(! empty($like) ){
            $like->delete();
        }

        /* Return Success Status, With Success Message In Data */
        return [
            'status' => true,
            'data'   => [
                'message' => 'Media Like Deleted Successfully.'
            ]
        ];
    }

    /* Share Media Api */
    /**
     * @param $mediaId
     * @param ShareMediasRequest $request
     * @return array
     */
    public function share($mediaId, ShareMediasRequest $request) {
        $userId  = Auth::Id();
        $scope   = $request->input('scope');
        $comment = $request->input('comment');

        /* Find Media For The Provided Media Id */
        $media = Media::find($mediaId);

        /* If Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }

        /* Find Media For The Provided Medias Id */
        $report = MediasReports::where(['medias_id' => $mediaId ])->first();

        if(!empty($report)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Media has been reported and cannot be shared.'
                ]
            ];
        }

        /* Create New MediaShare Object */
        $mediasShare = new MediasShares;

        /* Load Information In Media Share Object*/
        $mediasShare->medias_id = $mediaId;
        $mediasShare->users_id  = $userId;
        $mediasShare->scope     = $scope;
        $mediasShare->comment   = $comment;
        $mediasShare->save();

        /* Return Status True, With Success Message */
        return [
            'Success' => true
        ];
    }

    /* Delete Media Api */
    /**
     * @param DeleteMediasRequest $request
     * @return array
     */
    public function delete(DeleteMediasRequest $request) {
        /* Get Post Array For Arguments Sent In Request */
        $mediaId = $request->input('medias_id');
        $userId  = Auth::Id();

        /* Find Media For The Provided Media Id */
        $media = Media::find($mediaId);

        /* If Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }

        /* Find Media For The Provided Medias Id */
        $report = MediasReports::where(['medias_id' => $mediaId ])->first();

        if(!empty($report)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Media has been reported and cannot be shared.'
                ]
            ];
        }

        /* Find Relation For User Media */
        $user_media = UsersMedias::where([ 'users_id' => $userId, 'medias_id' => $mediaId ])->first();

        /* If Relation Not Found, Return Access Permission Error */
        if(empty($user_media)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'You don\'t have permission to perform this action.'
                ]
            ];
        }

        /* Delete Relation And Media */
        $user_media->delete();
        $media->delete();

        /* Return Success Status, And Success Message */
        return [
            'status' => true,
            'data'   => [
                'message' => 'Media Deleted Successfully.'
            ]
        ];

    }

    /* Hide Media Api */
    /**
     * @param $mediasId
     * @return array
     */
    public function hide($mediasId) {
        $userId = Auth::Id();

        /* Find Media For The Provided Media Id */
        $media = Media::find($mediasId);

        /* if Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }

        /* Find Previous Entry For Provided User Id And Media Id. */
        $media_hide = MediasHides::where(['users_id' => $userId, 'medias_id' => $mediasId ])->first();
        $user_hide  = UsersHiddenContent::where(['users_id' => $userId, 'content_id' => $mediasId, 'content_type' => UsersHiddenContent::CONTENT_MEDIA ])->first();

        /* If Previous Entry Not Found, Create New Entry */
        if(empty($media_hide) || empty($user_hide)){

            if(empty($media_hide)){
                $media_hide = new MediasHides;

                /* Load Data In MediasHides Model */
                $media_hide->users_id   = $userId;
                $media_hide->medias_id  = $mediasId;

                /* Save MediasHides */
                $media_hide->save();
            }

            if(empty($user_hide)){
                $user_hide = new UsersHiddenContent;

                $user_hide->users_id     = $userId;
                $user_hide->content_id   = $mediasId;
                $user_hide->content_type = UsersHiddenContent::CONTENT_MEDIA;

                $user_hide->save();
            }

            $hidden = true;
            log_user_activity('Media', 'hide', $mediasId);
        }else{
            $hidden = false;
        }

        /* Return Status True, And Success Message */
        return [
            'success' => $hidden
        ];
    }

    /* Report Media Api */
    /**
     * @param Request $request
     * @param $mediasId
     * @return array
     */
    public function report(Request $request, $mediasId) {
        $userId = Auth::Id();

        /* Find Media For The Provided Media Id */
        $media = Media::find($mediasId);

        /* if Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }

        /* If Reason For Reporting Is Not An Integer, Return Error */
        if( $request->input('reason') || !empty($request->input('reason')) ){
            /* If Reason For Reporting Is Not An Integer, Return Error */
            if(! is_numeric($request->input('reason'))){
                return [
                    'status' => false,
                    'data' => [
                        'error'   => 400,
                        'message' => 'Reason should be an integer.'
                        ]
                    ];
            } else {
                $reason = $request->input('reason');
            }
        } else {
            $reason = 0;
        }

        /* If Provided User Already Reported This Media, Find The Record */
        $media_report = MediasReports::where(['medias_id' => $mediasId, 'users_id' => $userId])->first();

        /* If Previous Record Of Reporting Not Found, Create New Record */
        if( empty($media_report) ) {
            $media_report = new MediasReports;

            /* Load Information In MediasReports Model */
            $media_report->reason    = $reason;
            $media_report->medias_id = $mediasId;
            $media_report->users_id  = $userId;

            /* Save MediasReports Model */
            $media_report->save();
            log_user_activity('Media', 'report', $mediasId);

            $message = 'Media reported successfully.';
            $status  = true;
        } else {
            $message = 'Media is already reported before.';
            $status  = false;
        }

        /* Return Success Status, Along With Success Message */
        return [
            'status' => $status,
            'data'   => [
                'message' => $message
            ]
        ];
    }

    /* Display Media Information Api */
    /**
     * @param ActivityMediasRequest $request
     * @return array
     */
    public function activity(ActivityMediasRequest $request) {
        /* Get Arguments From Post Request */
        $mediaId = $request->input('medias_id');

        /* Find Media For The Provided Media Id */
        $media = Media::find($mediaId);

        /* If Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }

        /* Container For Array Format Response Of Media's Likes */
        $medias_likes_arr = [];

        /* If Media Has Likes, Get Array Format Of Likes Information */
        if(!empty($media->likes)){

            foreach ($media->likes as $key => $value) {
                array_push($medias_likes_arr,[
                    'user_id'    => $value->users_id,
                    'media_id'   => $value->medias_id,
                    'created_at' => $value->created_at
                ]);
            }
        }

        /* Container For Media Comments Information */
        $medias_comments_arr = [];

        /* If Media has Comments, Get Array Format Of Comments Information */
        if(!empty($media->comments)){
            foreach ($media->comments as $key => $value) {
                array_push($medias_comments_arr,[
                    'user_id'    => $value->users_id,
                    'media_id'   => $value->medias_id,
                    'comment'    => $value->comment,
                    'reply_to'   => $value->reply_to,
                    'created_at' => $value->created_at
                ]);
            }
        }

        /* Return Status True, Along With Success Message */
        return [
            'status' => true,
            'data'   => [
                'medias_likes'    => $medias_likes_arr,
                'medias_comments' => $medias_comments_arr
            ]
        ];
    }

    /* Update Media Description Api */
    /**
     * @param UpdateDescriptionMediasRequest $request
     * @return array
     */
    public function update_description(UpdateDescriptionMediasRequest $request) {
        /* Get Arguments From Post Request */
        $mediaId     = $request->input('medias_id');
        $languageId  = $request->input('language_id', 1);
        $description = $request->input('description');

        /* Find Media For Provided Medias Id */
        $media = Media::find($mediaId);

        /* If Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }

        // Need Language Id To Decide Which Description To Update, For Not English Is Being Updated

        /* Find Translation Model For Medias With Provided Language Id And Medias id */
        $media_trans = MediaTranslations::where(['languages_id' => $languageId, 'medias_id' => $mediaId ])->first();

        /* If Translation Model Not Found,  */
        if(!empty($media_trans)) {
            $media_trans->description = $description;
            $media_trans->save();
        } else {
            //Create New Model If Required
        }

        /* Return Success Status, Along With Success Message */
        return [
            'status' => true,
            'data'   => [
                'message' => 'Description of media updated successfully'
            ]
        ];
    }

    /* List User's Media Api */
    /**
     * @param $user_id
     * @param $session_token
     * @param $media_user_id
     * @return array
     */
    public function listbyuser($user_id, $session_token, $media_user_id) {
        /* If User Id Is Not An Integer, Return Error */
        if(!is_numeric($user_id)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'User id should be an integer.'
                ]
            ];
        }

        /* Find User For The Provided User Id */
        $user = User::find($user_id);

        /* If User Not Found, Return Error */
        if(empty($user)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.'
                ]
            ];
        }

        /* FInd Session For The Provided Session Token */
        $session = Session::find($session_token);

        /* If Session Not Found, Return Error */
        if(empty($session)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong session token provided.'
                ]
            ];
        }

        /* If Session's User Id Doesn't Matches Provided User Id, Return Error */
        if($session->user_id != $user_id){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong user id provided.'
                ]
            ];
        }

        /* If Medias User Id Is Not An Integer, Return Error */
        if(!is_numeric($media_user_id)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Media user id should be an integer.'
                ]
            ];
        }

        /* Find User For The Provided Medias User Id */
        $media_user = User::find($media_user_id);

        /* If User Not Found For The Provided Medias User Id, Return Error */
        if(empty($media_user)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong media user id provided.'
                ]
            ];
        }

        /* Container To Hold The List Of Medias Of Provided User In Array Format */
        $medias_arr = [];

        /* Find All Languages In The System */
        $languages = Languages::all();

        /* if Media User Has Medias, Get Array Response */
        if(!empty($media_user->my_medias)){
            /* For Each Media, Get Its Translation And Group By The Languages Ids */
            foreach ($media_user->my_medias as $key => $value) {
                $media = $value->media;
                if(!empty($media)){
                    $trans_temp_arr = [];
                    if(!empty($languages)){
                        foreach ($languages as $key_language => $value_language) {
                            $temp_trans = MediaTranslations::where(['medias_id' => $media->id, 'languages_id' => $value_language->id])->first();

                            if(!empty($temp_trans)){
                                $trans_temp_arr[$value_language->id] = [
                                    'title' => $temp_trans->title,
                                    'description' => $temp_trans->description
                                ];
                            }
                        }
                    }
                    /*
                    * Push Medias Information ALong With All Available Translations Found For This Media In "medias_arr"
                    */
                    array_push($medias_arr,[
                        'id' => $media->id,
                        'url' => $media->url,
                        'translations' => $trans_temp_arr
                    ]);
                }
            }
        }

        /* Return Success Status, ALong With Medias Array */
        return [
            'status' => true,
            'data' => [
                'medias' => $medias_arr
            ]
        ];
    }

    /* Display Media reactions */
    /**
     * @param $mediaId
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getMediaReactions($mediaId) {
        /* Find Media For The Provided Media Id */
        $media = Media::find($mediaId);

        /* if Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data' => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }

        $media = Media::with([
            'mediaUser',
            'likes',
            'comments',
            'comments.user',
            'comments.likes',
            'comments.replyTo.user'
        ])->where('id', $mediaId)->first();

        $resource = new Item($media, new MediaReactionsTransformer());

        return apiResponse(
            $resource,
            [
                'comments',
                'comments.user:name|username|profile_picture',
                'comments.reply_to:id',
                'comments.reply_to.user:id|name|username'
            ]
        );
    }

    /* Liking Media Comment Api */
    /**
     * @param $mediaId
     * @param $commentId
     * @return array
     */
    public function commentLike($mediaId, $commentId) {
        $userId = Auth::Id();

        /* Find Media For The Provided Media Id */
        $media = Media::find($mediaId);

        /* if Media Not Found, Return Error */
        if(empty($media)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong Media Id Provided.'
                ]
            ];
        }

        /* Find Media For The Provided Media Id */
        $comment = MediasComments::find($commentId);

        /* if Media Not Found, Return Error */
        if(empty($comment)){
            return [
                'status' => false,
                'data'   => [
                    'error'   => 400,
                    'message' => 'Wrong Comment Id Provided.'
                ]
            ];
        }

        $commentLikeCheck = MediasCommentsLikes::where('medias_comments_id', $commentId)
            ->where('users_id', $userId)
            ->first();

        if (empty($commentLikeCheck)) {
            /* Create New Media Comment Like */
            $commentLike = new MediasCommentsLikes;

            /* Save Information In MediasCommentsLikes Model */
            $commentLike->medias_comments_id = $commentId;
            $commentLike->users_id           = $userId;
            $commentLike->created_at         = Date('Y-m-d h:i:s');;

            /* Save Information To Database */
            $commentLike->save();
            $liked = true;
        } else {
            /*Remove the MediasCommentsLikes record when MediasCommentsLikes object exists for this Media for the same User*/
            $commentLikeCheck->delete();
            $liked = false;
        }

        /* Return Success Status, With Success Liked Message */
        return [
            'status' => true,
            'liked'  => $liked
        ];
    }
    
    
    public function likeunlike(Request $request) {

        $medias_id = $request->medias_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = Posts::find($medias_id)->users_id;
        $check_like_exists = MediasLikes::where('posts_id', $post_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            //dd($check_like_exists);
            $check_like_exists->delete();
            log_user_activity('Post', 'unlike', $post_id);
            notify($author_id, 'status_unlike', $post_id);

            $data['status'] = 'no';
        } else {
            $like = new PostsLikes;
            $like->posts_id = $post_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Post', 'like', $post_id);
            notify($author_id, 'status_like', $post_id);

            $data['status'] = 'yes';
        }
        $data['count'] = count(PostsLikes::where('posts_id', $post_id)->get());
        return json_encode($data);
    }
}
