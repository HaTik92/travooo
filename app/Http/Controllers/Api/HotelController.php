<?php

namespace App\Http\Controllers\Api;

use DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Responses\ApiResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Models\Hotels\Hotels;
use App\Models\Reviews\Reviews;
use App\Models\Posts\Checkins;
use App\Models\TripPlans\TripPlans;
use App\Models\Hotels\HotelFollowers;

use App\Http\Requests\Api\Hotel\TrackRefRequest;
use App\Http\Requests\Api\Hotel\MediaRequest;
use App\Http\Requests\Api\Hotel\ReviewRequest;

use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use GuzzleHttp\Client;
use Intervention\Image\ImageManagerStatic as Image;

class HotelController extends Controller
{
     /**
     * @param MediaRequest $request
     * @return \Illuminate\Http\Response
     */
    public function getMedia(MediaRequest $request) {
        try {
            $hotel_id = $request->get('hotel_id');
            $final_results = array();
            $h = Hotels::find($hotel_id);
            if (is_object($h)) {
                $details_link = file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?'
                        . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
                        . '&placeid=' . $h->provider_id);
                $details = json_decode($details_link);
                //dd($details);
                if (isset($details->result)) {
                    $details = $details->result;
                    if (isset($details->photos)) {
                        $place_photos = $details->photos;
                        $raw_photos = array();
                        $i = 1;

                        foreach ($place_photos AS $pp) {
                            $file_contents = file_get_contents('https://maps.googleapis.com/maps/api/place/photo?'
                                    . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
                                    . '&maxwidth=1600'
                                    . '&photoreference=' . $pp->photo_reference);

                            $sha1 = sha1($file_contents);
                            $md5 = md5($file_contents);
                            $size = strlen($file_contents);
                            $raw_photos[$i]['sha1'] = $sha1;
                            $raw_photos[$i]['md5'] = $md5;
                            $raw_photos[$i]['size'] = $size;
                            $raw_photos[$i]['contents'] = $file_contents;
                            $i++;

                            
                                $check_media_exists = \App\Models\ActivityMedia\Media::where('sha1', $sha1)
                                        ->where('md5', $md5)
                                        ->where('filesize', $size)
                                        ->get()
                                        ->count();
                                if ($check_media_exists == 0) {
                                    $media_file = 'hotels/' . $h->provider_id . '/' . sha1(microtime()) . '.jpg';
                                    \Illuminate\Support\Facades\Storage::disk('s3')->put($media_file, $file_contents, 'public');

                                    
                                    $media = new \App\Models\ActivityMedia\Media;
                                    $media->url = $media_file;
                                    $media->sha1 = $sha1;
                                    $media->filesize = $size;
                                    $media->md5 = $md5;
                                    $media->html_attributions = join(",", $pp->html_attributions);
                                    $media->save();
                                    if ($media->save()) {
                                        $place_media = new \App\Models\Hotels\HotelsMedias;
                                        $place_media->hotels_id = $h->id;
                                        $place_media->medias_id = $media->id;
                                        $place_media->save();
                                        
                                        
                                        $complete_url = $media->url;
                                        $url = explode("/", $complete_url);
                                        //$data = file_get_contents('https://s3.amazonaws.com/travooo-images2/'.$complete_url);
                                        //$s3 = App::make('aws')->createClient('s3');
                                    
                                        $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

                                        $options = [
                                            'region' => 'us-east-1',
                                            'version' => 'latest',
                                            'http' => ['verify' => false],
                                            'credentials' => $credentials,
                                            'endpoint' => 'https://s3.amazonaws.com'
                                        ];

                                        $s3Client = new S3Client($options);
                                        //$s3 = AWS::createClient('s3');
                                        $result = $s3Client->getObject([
                                            'Bucket' => 'travooo-images2', // REQUIRED
                                            'Key' => $complete_url, // REQUIRED
                                            'ResponseContentType' => 'text/plain',
                                        ]);

                                        $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
                                            $constraint->aspectRatio();
                                        });
                                        $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
                                            $constraint->aspectRatio();
                                        });
                                        $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
                                            $constraint->aspectRatio();
                                        });
                                        $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
                                            $constraint->aspectRatio();
                                        });

                                        //return $img_700->response('jpg');
                                        //echo $complete_url . '<br />';
                                        //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

                                        $put_1100 = $s3Client->putObject([
                                            'ACL' => 'public-read',
                                            'Body' => $img_1100->encode(),
                                            'Bucket' => 'travooo-images2',
                                            'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                        ]);

                                        $put_700 = $s3Client->putObject([
                                            'ACL' => 'public-read',
                                            'Body' => $img_700->encode(),
                                            'Bucket' => 'travooo-images2',
                                            'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                        ]);

                                        $put_230 = $s3Client->putObject([
                                            'ACL' => 'public-read',
                                            'Body' => $img_230->encode(),
                                            'Bucket' => 'travooo-images2',
                                            'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                        ]);

                                        $put_180 = $s3Client->putObject([
                                            'ACL' => 'public-read',
                                            'Body' => $img_180->encode(),
                                            'Bucket' => 'travooo-images2',
                                            'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                        ]);
                                        $media->thumbs_done = 1;
                                        $media->save();
                                    }
                                }
                        }
                        
                        // if(isset($put_180)) return 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2];

                        //return serialize($raw_photos);
                        //return $raw_photos;
                        if(isset($put_180)){
                            $mediaGalary =  'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2];
                            return ApiResponse::create(
                                [
                                    'media' => $mediaGalary
                                ]
                            );
                        }else{
                            return ApiResponse::create( [], true, ApiResponse::NO_CONTENT );
                        }
                    }
                }
            }else{
                return ApiResponse::create( [], true, ApiResponse::NO_CONTENT );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $hotel_id
     * @return \Illuminate\Http\Response
     */
    public function getIndex($hotel_id) {
        try {
            $language_id = 1;
            $hotel = Hotels::find($hotel_id);

            if (!$hotel) {
                return ApiResponse::create([
                        'message' => ['Invalid Hotel ID',]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $place = Hotels::with([
                        'trans' => function ($query) use($language_id) {
                            $query->where('languages_id', $language_id);
                        },
                        'getMedias',
                        'getMedias.users',
                        'getMedias.comments',
                        'followers'
                    ])
                    ->where('id', $hotel_id)
                    ->where('active', 1)
                    ->first();

            $db_hotel_reviews = Reviews::where('hotels_id', $hotel->id)->get();
            if (!count($db_hotel_reviews)) {
                $hotel_reviews = get_google_reviews($hotel->provider_id);
                if (is_array($hotel_reviews)) {
                    foreach ($hotel_reviews AS $review) {
                        $r = new Reviews;
                        $r->hotels_id = $hotel->id;
                        $r->google_author_name = $review->author_name;
                        $r->google_author_url = $review->author_url;
                        $r->google_language = $review->language;
                        $r->google_profile_photo_url = $review->profile_photo_url;
                        $r->score = $review->rating;
                        $r->google_relative_time_description = $review->relative_time_description;
                        $r->text = $review->text;
                        $r->google_time = $review->time;
                        $r->save();
                    }
                }
            }

            $db_hotel_reviews = Reviews::where('hotels_id', $hotel->id)->get();
            $data['reviews'] = $db_hotel_reviews;

            $data['reviews_avg'] = Reviews::where('hotels_id', $hotel->id)->avg('score');

            // get hotels nearby
            $data['hotels_nearby'] = Hotels::with('medias')
                    ->whereHas("medias", function ($query) {
                        $query->where("medias_id", ">", 0);
                    })
                    ->where('cities_id', $hotel->cities_id)
                    ->where('id', '!=', $hotel->id)
                    ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $hotel->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $hotel->lng . ') ) + sin( radians(' . $hotel->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
                    ->having('distance', '<', 15)
                    ->orderBy('distance')
                    ->take(10)
                    ->get();


            $checkins = Checkins::where('hotels_id', $hotel->id)->orderBy('id', 'DESC')->get();

            $result = array();
            $done = array();
            foreach ($checkins AS $checkin) {
                if (!isset($done[$checkin->post_checkin->post->author->id])) {
                    $result[] = $checkin;
                    $done[$checkin->post_checkin->post->author->id] = true;
                }
            }
            $data['checkins'] = $result;
            $data['hotel'] = $hotel;
            $data['events'] = array();

            $data['my_plans'] = TripPlans::where('users_id', Auth::user()->id)->get();
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $hotelId
     * @return \Illuminate\Http\Response
     */
    public function postCheckFollow($hotelId) {
        try {
            $userId = Auth::user()->id;
            $hotel = Hotels::find($hotelId);
            if (!$hotel) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Hotel ID',]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $follower = HotelFollowers::where('hotels_id', $hotelId)
                    ->where('users_id', $userId)
                    ->first();

            if(empty($follower)){
                return ApiResponse::create(
                    [
                        'success' => false
                    ]
                );
            }else{
                return ApiResponse::create(
                    [
                        'success' => true
                    ]
                );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
    
    /**
     * @param integer $hotelId
     * @return \Illuminate\Http\Response
     */
    public function postFollow($hotelId) {
        try {
            $userId = Auth::user()->id;
            $hotel = Hotels::find($hotelId);
            if (!$hotel) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Hotel ID',]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }


            $follower = HotelFollowers::where('hotels_id', $hotelId)
                    ->where('users_id', $userId)
                    ->first();

            if ($follower) {
                return ApiResponse::create([
                        'message' => ['This user is already following that Hotel',]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $new_follower = new HotelFollowers;
            $new_follower->hotels_id = $hotelId;
            $new_follower->users_id = $userId;
            $new_follower->save();

            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('Hotel', 'follow', $hotelId);
            return ApiResponse::create(
                [
                    'success' => true
                ]
            );
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $hotelId
     * @return \Illuminate\Http\Response
     */
    public function postUnFollow($hotelId) {
        try {
            $userId = Auth::user()->id;

            $hotel = Hotels::find($hotelId);
            if (!$hotel) {
                return ApiResponse::create([
                        'message' => ['Invalid Hotel ID',]
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $follower = HotelFollowers::where('hotels_id', $hotelId)
                    ->where('users_id', $userId);

            if (!$follower->first()) {
                return ApiResponse::create([
                        'message' => ['You are not following this Hotel.'],
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            } else {
                $follower->delete();
                //$this->updateStatistic($country, 'followers', count($country->followers));

                log_user_activity('Hotel', 'unfollow', $hotelId);
                return ApiResponse::create(['message' => ["unfollowing success"]]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $hotelId
     * @return \Illuminate\Http\Response
     */
    public function postTalkingAbout($hotelId) {
        try {
            $hotel = Hotels::find($hotelId);
            if (!$hotel) {
                return ApiResponse::create([
                        'message' => ['Invalid Hotel ID'],
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
    
            $shares = $hotel->shares;
    
            foreach ($shares AS $share) {
                $data['shares'][] = array(
                    'user_id' => $share->user->id,
                    'user_profile_picture' => check_profile_picture($share->user->profile_picture)
                );
            }
    
            $data['num_shares'] = count($shares);
            $data['success'] = true;
            
            return ApiResponse::create($data);
            //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $hotelId
     * @return \Illuminate\Http\Response
     */
    public function postNowInPlace($hotelId) {
        try {
            $hotel = Hotels::find($hotelId);
            if (!$hotel) {
                return ApiResponse::create([
                        'message' => ['Invalid Hotel ID'],
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
    
            $checkins = Checkins::where('hotels_id', $hotel->id)
                    ->orderBy('id', 'DESC')
                    ->take(5)
                    ->get();
    
            $result = array();
            $done = array();
            foreach ($checkins AS $checkin) {
                if (!isset($done[$checkin->post_checkin->post->author->id])) {
                    $result[] = array(
                        'name' => $checkin->post_checkin->post->author->name,
                        'id' => $checkin->post_checkin->post->author->id,
                        'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture)
                    );
                    $done[$checkin->post_checkin->post->author->id] = true;
                }
            }
            $data['live_checkins'] = $result;
    
            $data['success'] = true;
            //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $hotelId
     * @param ReviewRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postReview($hotelId,ReviewRequest $request) {
        try {
            $hotel = Hotels::find($hotelId);
            if (!$hotel) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Hotel ID'],
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }
            if ($request->has('text')) {
                $review_text = $request->get('text');
                $review_score = $request->get('score');
                $review_author = Auth::user();
                $review_place_id = $hotel->id;

                $review = new Reviews;
                $review->hotels_id = $review_place_id;
                $review->by_users_id = $review_author->id;
                $review->score = $review_score;
                $review->text = $review_text;
                $review->save();

                if ($review->save()) {
                    log_user_activity('Hotel', 'review', $hotel->id);
                    $data['success'] = true;
                    $data['review']['author_profile'] =  check_profile_picture(Auth::user()->profile_picture);
                    $data['review']['review_author'] =  Auth::user()->name;
                    $data['review']['review_text'] =  $review_text;
                    $data['review']['review_score'] =  $review->score;
                    $data['review']['review_date'] =  diffForHumans(date("Y-m-d", time()));
                } else {
                    $data['success'] = false;
                }
            } else {
                $data['success'] = false;
            }
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param integer $hotelId
     * @return \Illuminate\Http\Response
     */
    public function happeningToday($hotelId) {
        try {
            $hotel = Hotels::find($hotelId);
            if (!$hotel) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Hotel ID'],
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }

            $checkins = Checkins::whereHas("place", function ($query) use ($hotel) {
                        $query->where('cities_id', $hotel->cities_id);
                        $query->where('id', '!=', $hotel->id);
                    })
                    ->orderBy('id', 'DESC')
                    ->get();

            /*
            ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
            ->having('distance', '<', 15)
            ->orderBy('distance')
            ->take(10)
            ->get();
            * 
            */

            //$checkins = Checkins::where('place_id', $place->id)->orderBy('id', 'DESC')->take(5)->get();

            $result = array();
            foreach ($checkins AS $checkin) {
                $result[] = array(
                    'post_id' => $checkin->post_checkin->post->id,
                    'lat' => $checkin->hotel->lat,
                    'lng' => $checkin->hotel->lng,
                    'photo' => @$checkin->post_checkin->post->medias[0]->media->url,
                    'name' => $checkin->post_checkin->post->author->name,
                    'id' => $checkin->post_checkin->post->author->id,
                    'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                    'date' => diffForHumans($checkin->post_checkin->post->created_at)
                );
            }
            $data['happenings'] = $result;

            $data['success'] = true;
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
    /**
     * @param integer $hotelId
     * @return \Illuminate\Http\Response
     */
    public function hotelRates($hotelId) {
        try {
            $hotel = Hotels::find($hotelId);
            if (!$hotel) {
                return ApiResponse::create(
                    [
                        'message' => ['Invalid Hotel ID'],
                    ],
                    false,
                    ApiResponse::BAD_REQUEST
                );
            }


            $city_name = $hotel->city->transsingle->title;
            $data['city_code'] = 0;
            $res = file_get_contents('https://srv.wego.com/places/search?query=' . $city_name);
            $results = json_decode($res);
            $cities = array();
            $i = 0;
            if (isset($results[0]) AND is_object($results[0]) AND $results[0]->type == "city") {
                $data['city_code'] = $results[0]->code;
            }


            // start search for that Hotel
            //city_select=DXB&daterange=05%2F24%2F2019+-+05%2F25%2F2019
            $input['daterange'] = date('m/d/Y', time()) . ' - ' . date('m/d/Y', time() + 86400);
            $input['city_select'] = $data['city_code'];


            /// start search hotels
            $drange = explode("-", $input['daterange']);

            $date_from = date("Y-n-d", strtotime(trim($drange[0])));
            $date_to = date("Y-n-d", strtotime(trim($drange[1])));

            //dd($date_from);


            $token = $this->doAPIAuthorization();
            if ($token) {
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => "Bearer $token"]
                ]);

                $response = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                            [
                                'search' => [
                                    'locale' => 'en',
                                    'cityCode' => $input['city_select'],
                                    'siteCode' => 'US',
                                    'currencyCode' => 'USD',
                                    'guestsCount' => 1,
                                    'roomsCount' => 1,
                                    'checkIn' => $date_from,
                                    'checkOut' => $date_to,
                                    'deviceType' => 'desktop'
                                ]
                            ]
                    )]
                );

                if ($response->getStatusCode() == 200) {
                    $result = json_decode($response->getBody()->getContents());
                    $search_id = $result->search->id;
                    //die();
                    $offset = count($result->hotels);

                    $rates_response = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                                [
                                    'search' => [
                                        'id' => $search_id
                                    ]
                                ]
                        )]
                    );

                    if ($rates_response->getStatusCode() == 200) {
                        $rates_result = json_decode($rates_response->getBody()->getContents());
                        $rates = $rates_result->rates;
                        //echo $token;
                    }


                    sleep(1);

                    $response2 = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                                [
                                    'search' => [
                                        'id' => $search_id,
                                        'locale' => 'en',
                                        'currencyCode' => 'USD',
                                        'offset' => $offset
                                    ]
                                ]
                        )]
                    );


                    if ($response2->getStatusCode() == 200) {
                        $result2 = json_decode($response2->getBody()->getContents());
                        $search_id = $result2->search->id;
                        //die();
                        $offset = count($result2->hotels);


                        $rates_response2 = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                                    [
                                        'search' => [
                                            'id' => $search_id
                                        ]
                                    ]
                            )]
                        );

                        if ($rates_response2->getStatusCode() == 200) {
                            $rates_result2 = json_decode($rates_response2->getBody()->getContents());
                            $rates2 = $rates_result2->rates;
                            //echo $token;
                        }

                        $data['hotels'] = $result2->hotels;
                        $data['rates'] = array_merge($rates, $rates2);
                    }
                }
            }
            // end search hotels

            $data['city_name'] = $result->search->city->name;

            $data['hotel_city'] = \App\Models\City\Cities::with('transsingle')->with('medias')
                            ->whereHas('transsingle', function ($query) use ($data) {
                                $query->where('title', 'LIKE', '%' . $data['city_name'] . '%');
                            })->get();

            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    
    /**
     * @return null|string
     */
    private function doAPIAuthorization() {
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json', 'X-Wego-Version' => 1]
        ]);

        $response = $client->post('https://srv.wego.com/users/oauth/token', ['body' => json_encode(
                    [
                        'grant_type' => 'client_credentials',
                        'client_id' => 'ac9ac5c6c1c603fb33e61f01',
                        'client_secret' => '4ebe013c772c81b0ff3f764e',
                        'scope' => 'affiliate'
                    ]
            )]
        );

        if ($response->getStatusCode() == 200) {
            $result = json_decode($response->getBody()->getContents());
            $token = $result->access_token;
            //echo $token;
            return $token;
        } else {
            return null;
        }
    }
    
    /**
     * @param TrackRefRequest $request
     * @return \Illuminate\Http\Response
     */
    public function trackRef(TrackRefRequest $request) {
        try {
            $ref = $request->get('ref');
            $new_ref = new \App\Models\Referral\ReferralLinksClicks;
            $new_ref->links_id = $ref;
            $new_ref->users_id = Auth::user()->id;
            $new_ref->gender = '';
            $new_ref->nationality = '';
            $new_ref->save();
            if($new_ref){
                return ApiResponse::create(
                    [
                        'new_ref' => $new_ref
                    ]
                );
            }else{
                return ApiResponse::create( [], true, ApiResponse::NO_CONTENT );
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
