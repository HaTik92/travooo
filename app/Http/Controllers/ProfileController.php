<?php

namespace App\Http\Controllers;

use App\Models\Discussion\Discussion;
use App\Services\Reviews\ReviewsService;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;
use App\Models\User\User;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlaces\TripPlaces;
use App\Models\Posts\Checkins;
use App\Models\Posts\Posts;
use App\Models\TripPlans\TripsVersions;
use Illuminate\Support\Facades\DB;
use App\Models\UsersFollowers\UsersFollowers;
use App\Models\ActivityLog\ActivityLog;
use App\Models\User\UsersMedias;
use App\Models\Reviews\Reviews;
use Illuminate\Support\Facades\Storage;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\PlacesTop\PlacesTop;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\Lifestyle\LifestyleTrans;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Http\Responses\AjaxResponse;
use App\Services\Users\ExpertiseService;
use Carbon\Carbon;
use App\Models\ActivityMedia\Media;
use Illuminate\Support\Facades\View;
use App\Models\Posts\PostsLikes;
use App\Models\Reports\ReportsLikes;
use App\Models\Reviews\ReviewsVotes;
use App\Models\TripPlans\TripsLikes;
use App\Models\Events\EventsLikes;
use App\Models\Discussion\DiscussionLikes;
use App\Services\Profile\ProfileService;

class ProfileController extends Controller {

    public function __construct() {
//        $this->middleware('auth:user');
        $this->middleware('access_denied');
    }

    public function getAbout($user_id = null, ProfileService $profileService) {

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $data['login_user_id'] = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                return redirect('login');
            }
        }

        if(!empty(app('router')->getRoutes()->match(app('request')->create(\URL::previous()))->getName())){
            return redirect('profile-posts/'.$userId);
        }

        $expertise = array();
        $user_expertise = array();
        $expertise_count = [];
        $styles_list = [];

        $cities = ExpertsCities::with('cities')->get();
        $countries = ExpertsCountries::with('countries')->get();

        if(count($cities) > 0){
            foreach($cities as $city){
                if(array_key_exists($city->cities->transsingle->title, $expertise_count)){
                    $expertise_count[$city->cities->transsingle->title]= $expertise_count[$city->cities->transsingle->title] + 1;
                }else{
                    $expertise_count[$city->cities->transsingle->title] = 1;
                }
                if($city->users_id == $userId){
                    $user_expertise[] = 'city,'.$city->cities_id;
                    $expertise[] = [
                        'id'=>'city,'.$city->cities_id,
                        'text'=>$city->cities->transsingle->title];
                }
            }
        }

        if(count($countries) > 0){
            foreach($countries as $country){
                if(array_key_exists($country->countries->transsingle->title, $expertise_count)){
                    $expertise_count[$country->countries->transsingle->title]= $expertise_count[$country->countries->transsingle->title] + 1;
                }else{
                    $expertise_count[$country->countries->transsingle->title] = 1;
                }
                if($country->users_id == $userId){
                    $user_expertise[] = 'country,'. $country->countries_id;
                    $expertise[] = [
                        'id'=>'country,'. $country->countries_id,
                        'text'=>$country->countries->transsingle->title];
                }
            }
        }


        if (session()->has('expertise_count')) {
            session()->forget('expertise_count');
        }

        session()->put('expertise_count', $expertise_count);

        $user = User::find($userId);

        if($user){
            if(count($user->travelstyles) > 0){
                foreach($user->travelstyles as $style){
                    $styles_list[] = @$style->travelstyle->id;
                }
            }

            $data['user'] = $user;
            $data['expertise'] = $expertise;
            $data['user_expertise'] = $user_expertise;
            $data['travelstyles'] = LifestyleTrans::all();
            $data['user_travelstyles'] = @$user->travelstyles;
            $data['styles_list'] = $styles_list;
            $data['my_plans'] = Auth::check() ? TripPlans::where('users_id', Auth::guard('user')->user()->id)->get():[];
            $data['user_id'] = $user_id;
            $data['countries'] = Countries::all();

            return view('site.profile.about', $data);
        }else{
            return redirect()->back();
        }

    }


    public function getPosts($user_id = null, ProfileService $profileService) {

        if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $data['login_user_id'] = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                return redirect('login');
            }
        }

        if (!session()->has('place_country')) {
            session()->put('place_country', 'place');
        }

        if (session()->has('current_user_id')) {
            session()->forget('current_user_id');
        }


        $posts = $profileService->getPostCount($userId);

        if (session()->has('posts_count')) {
            session()->forget('posts_count');
        }

        session()->put('current_user_id', $userId);
        session()->put('posts_count', $posts);

        $user = User::find($userId);

        $data['user'] = $user;
        $data['me'] = Auth::guard('user')->user();
        $data['expertise'] = $profileService->getExpertiseList($userId);
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        $data['user_id'] = $user_id;
        $data['countries'] = \App\Models\Country\Countries::all();

        $data['top_places'] = PlacesTop::orderBy('reviews_num', 'desc')->take(4)->get();
        $data['new_people'] = User::orderBy('id', 'desc')->take(10)->get();
        $data['new_travellers'] = TravelMatesRequests::orderBy('created_at', 'DESC')->groupBy('users_id')->take(10)->get();

        $data['posts'] = $this->_getPosts('post', 'action', $userId);

        $loaded_ids = [];
        foreach ($data['posts'] as $_post) {
            $loaded_ids[] = $_post->id;
        }

        session()->forget(['loaded_ids']);
        session(['loaded_ids' =>  $loaded_ids]);

        return view('site.profile.posts', $data);
    }

    public function postUpdateFeed(Request $request) {
        if(session()->has('current_user_id')){
            $userId = session()->get('current_user_id');
        }else{
            $userId = Auth::user()->id;
        }

        $user = User::find($userId);



        if($request->next_additional){
            $additional = $request->next_additional;
        }else{
            $additional = [];
        }

        $filter_type = $request->filter_type;

        if($filter_type != 'all_posts'){
            session()->forget(['loaded_ids']);
            session(['loaded_ids' =>  []]);
        }


        $return = false;

        $posts = $this->_getPosts('post', 'action', $userId, $additional, $filter_type);

        foreach ($posts as $post) {
            if (Auth::check() && isset($post->permission) && $post->permission == Posts::POST_PERMISSION_PRIVATE && $post->users_id != Auth::user()->id) {
                continue;
            } elseif (Auth::check() && isset($post->permission) && $post->permission == Posts::POST_PERMISSION_FRIENDS_ONLY && $post->users_id != Auth::user()->id and !in_array($post->users_id, getUserFriendsIds())) {
                continue;
            }

            if ($post->type=="post" && $post->action=="publish" && !user_access_denied($post->user_id, true))
                $return .= view('site.home.new.partials.primary-text-only-post', array('post' => $post, 'me' => Auth::user()));

            elseif ($post->type=="other" && $post->action=="show" && !user_access_denied($post->user_id, true))
                $return .= view('site.home.partials.other_post', array('post' => $post, 'me' => Auth::user()));

            elseif ($post->type=="place" && $post->action=="review" && !user_access_denied($post->user_id, true))
                $return .= view('site.home.new.partials.place-review', array('post' => $post));

            elseif ($post->type == "hotel" && $post->action == "review" && !user_access_denied($post->user_id, true))
                $return .= view('site.home.partials.hotel_review', array('post' => $post));

            elseif ($post->type=="discussion" && $post->action=="create" && !user_access_denied($post->user_id, true) && Discussion::find($post->variable))
                $return .= view('site.home.new.partials.discussion', array('post' => $post, 'me' => Auth::user()));

            elseif ($post->type=="trip" && ($post->action=="create" || $post->action=='plan_updated') && !user_access_denied($post->user_id, true))
                $return .= view('site.home.new.partials.trip', array('post' => $post, 'me' => Auth::user()));

            elseif ($post->type=="report" && ($post->action=="create" || $post->action=="update") && !user_access_denied($post->user_id, true))
                $return .= view('site.home.new.partials.travlog', array('post' => $post, 'me' => Auth::user()));

            elseif ($post->type == 'share' && $post->action =='plan_media_shared')
                $return .= view('site.home.new.partials.tripmediashare', array('post' => $post, 'me' => Auth::user()));

            elseif ($post->type == 'share' && $post->action =='plan_step_shared')
                $return .= view('site.home.new.partials.tripplace', array('post' => $post, 'me' => Auth::user()));

            elseif ($post->type == 'share' && $post->action =='plan_shared')
                $return .= view('site.home.new.partials.tripshare', array('post' => $post, 'me' => Auth::user()));

            elseif ($post->type == 'share')
                $return .= view('site.home.new.partials.shareable-post', array('post' => $post, 'me' => Auth::user()));

        }
        $next_additional = $posts->pluck('id')->toArray();
        $next_additional = array_merge($next_additional, $additional);

        return new JsonResponse(['next_additional'=>$next_additional, 'view'=>$return]);
    }

    /**
     * @param integer $user_id
     * @return View
     */
    public function getLikedPosts($user_id = null, ProfileService $profileService) {

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $data['login_user_id'] = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                return redirect('login');
            }
        }

        if (session()->has('current_user_id')) {
            session()->forget('current_user_id');
        }

        session()->put('current_user_id', $userId);


        $user = User::find($userId);

        $liked_post = $this->_getPosts('like', 'action', $userId);

        $data['user'] = $user;
        $data['me'] = Auth::guard('user')->user();
        $data['expertise'] = $profileService->getExpertiseList($userId);
        $data['user_id'] = $user_id;



        $loaded_liked_ids = [];
        $liked_discussion_ids = [];
        $liked_posts_ids = [];
        $liked_trips_ids = [];
        $liked_travlog_ids = [];
        $liked_event_ids = [];
        $liked_review_ids = [];

        foreach ($liked_post as $_post) {
            $loaded_liked_ids[] = $_post->id;

            if (strtolower($_post->type) == "post" and $_post->action == "like") {
                $liked_posts_ids[] = $_post->variable;
            }

            if (strtolower($_post->type) == "report" and $_post->action == "like") {
                $liked_travlog_ids[] = $_post->variable;
            }

            if (strtolower($_post->type) == "review" and $_post->action == "upvote") {
                $liked_review_ids[] = $_post->variable;
            }

            if (strtolower($_post->type) == "trip" and $_post->action == "like") {
                $liked_trips_ids[] = $_post->variable;
            }

            if (strtolower($_post->type) == "event" and $_post->action == "like") {
                $liked_event_ids[] = $_post->variable;
            }

            if (strtolower($_post->type) == "discussion" and $_post->action == "upvote") {
                $liked_discussion_ids[] = $_post->variable;
            }
        }

        session()->forget(['liked_posts_ids']);
        session()->forget(['liked_travlog_ids']);
        session()->forget(['liked_review_ids']);
        session()->forget(['liked_trips_ids']);
        session()->forget(['liked_event_ids']);
        session()->forget(['liked_discussion_ids']);
        session(['liked_posts_ids' =>  $liked_posts_ids]);
        session(['liked_travlog_ids' =>  $liked_travlog_ids]);
        session(['liked_review_ids' =>  $liked_review_ids]);
        session(['liked_trips_ids' =>  $liked_trips_ids]);
        session(['liked_event_ids' =>  $liked_event_ids]);
        session(['liked_discussion_ids' =>  $liked_discussion_ids]);

        $data['liked_posts'] = $liked_post;

        session()->forget(['loaded_liked_ids']);
        session(['loaded_liked_ids' =>  $loaded_liked_ids]);

        return view('site.profile.liked-posts', $data);
    }

    /**
     * @param integer $user_id
     * @return View
     */

    public function getMoreLikedPosts(Request $request) {
        if(session()->has('current_user_id')){
            $userId = session()->get('current_user_id');
        }else{
            $userId = Auth::user()->id;
        }

        $user = User::find($userId);
        $data['user'] = $user;
        $page = $request->get('pageno');
        $skip = ($page - 1) * 10;

        $additional = [];
        if (session('loaded_liked_ids')) {
            $additional = session('loaded_liked_ids');
        }

        $return = false;

        $liked_post = $this->_getPosts('like', 'action',  $userId, $additional);


        $liked_posts_ids = session('liked_posts_ids');
        $liked_travlog_ids = session('liked_travlog_ids');
        $liked_review_ids = session('liked_review_ids');
        $liked_trips_ids = session('liked_trips_ids');
        $liked_event_ids = session('liked_event_ids');
        $liked_discussion_ids = session('liked_discussion_ids');

        foreach ($liked_post as $post) {
            echo " ";

            if (strtolower($post->type) == "post" and $post->action == "like" and !in_array($post->variable, $liked_posts_ids)) {
                if (PostsLikes::where('posts_id', $post->variable)->where('users_id', $userId)->exists()) {
                    $liked_posts_ids[] = $post->variable;
                    if(ActivityLog::where('type', 'Other')->where('action', 'show')->where('variable', $post->variable)->exists()){
                        $return .= view('site.home.partials.other_post', array('post' => $post, 'me' => Auth::guard('user')->user()));
                    }else{
                        $return .= view('site.home.new.partials.primary-text-only-post', array('post' => $post, 'me' => Auth::guard('user')->user()));
                    }
                }

            } elseif (strtolower($post->type) == "report" and $post->action == "like" and !in_array($post->variable, $liked_travlog_ids)) {
                if (ReportsLikes::where('reports_id', $post->variable)->where('users_id', $userId)->exists()) {
                    $liked_travlog_ids[] = $post->variable;
                    $return .= view('site.home.new.partials.travlog', array('post' => $post, 'me' => Auth::guard('user')->user()));
                }


            } elseif (strtolower($post->type) == "review" and $post->action == "upvote" and !in_array($post->variable, $liked_review_ids)){
                if (ReviewsVotes::where('review_id', $post->variable)->where('vote_type', 1)->where('users_id', $userId)->exists()){
                    $liked_review_ids[] = $post->variable;
                    $return .= view('site.home.new.partials.place-review', array('post' => $post, 'me' => Auth::guard('user')->user()));
               }

            } elseif (strtolower($post->type) == "trip" and $post->action == "like" and !in_array($post->variable, $liked_trips_ids)){
                if (TripsLikes::where('trips_id', $post->variable)->where('users_id', $userId)->exists()){
                    $liked_trips_ids[] = $post->variable;
                    $return .= view('site.home.new.partials.trip', array('post' => $post, 'me' => Auth::guard('user')->user()));
                }

            } elseif (strtolower($post->type) == "event" and $post->action == "like" and !in_array($post->variable, $liked_event_ids)){
                if (EventsLikes::where('events_id', $post->variable)->where('users_id', $userId)->exists()){
                    $liked_event_ids[] = $post->variable;
                    $return .= view('site.home.partials.post_event', array('post' => $post, 'me' => Auth::guard('user')->user()));
                }

            } elseif (strtolower($post->type) == "discussion" and $post->action == "upvote" and !in_array($post->variable, $liked_discussion_ids)){
                if (DiscussionLikes::where('discussions_id', $post->variable)->where('vote', 1)->where('users_id', $userId)->exists()){
                    $liked_discussion_ids[] = $post->variable;
                    $return .= view('site.home.new.partials.discussion', array('post' => $post, 'me' => Auth::guard('user')->user()));
                }

            }

            //}
            //return view('site.home.index', $data);
        }

        session(['liked_posts_ids' =>  $liked_posts_ids]);
        session(['liked_travlog_ids' =>  $liked_travlog_ids]);
        session(['liked_review_ids' =>  $liked_review_ids]);
        session(['liked_trips_ids' =>  $liked_trips_ids]);
        session(['liked_event_ids' =>  $liked_event_ids]);
        session(['liked_discussion_ids' =>  $liked_discussion_ids]);

        $next_additional = $liked_post->pluck('id')->toArray();
        $next_additional = array_merge($next_additional, $additional);
        if (session('loaded_liked_ids')) {
            session()->forget(['loaded_liked_ids']);
        }
        session(['loaded_liked_ids' =>  $next_additional]);

        echo $return;
    }

    public function getTravelHistory($user_id = null, ProfileService $profileService) {

        if($user_id && $user_id != Auth::guard('user')->user()->id) {
            $userId = $user_id;
        }
        else {
            $userId = Auth::guard('user')->user()->id;
        }

        $user = User::find($userId);

        $data['visited_countries'] = Posts::with('checkin')
            ->where('users_id', $userId)
            ->whereHas('checkin', function($q) {
                $q->where('country_id', '!=', 0);
            })
            ->get();

        $data['visited_cities'] = Posts::with('checkin')
            ->where('users_id', $userId)
            ->whereHas('checkin', function($q) {
                $q->where('city_id', '!=', 0);
            })
            ->get();

        $data['visited_places'] = Posts::with('checkin')
            ->where('users_id', $userId)
            ->whereHas('checkin', function($q) {
                $q->where('place_id', '!=', 0);
            })
            ->get();


        $data['user'] = $user;
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        $data['user_id'] = $user_id;
        $data['expertise'] = $profileService->getExpertiseList($userId);;


        return view('site.profile.travel-history', $data);
    }

    public function getMap($user_id = null, ProfileService $profileService) {

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $data['login_user_id'] = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                return redirect('login');
            }
        }
        $user = User::find($userId);

        $data['user'] = $user;

        $data['happenings'] = $profileService->getVisitedPlaceForMap($user_id);

        $data['my_plans'] = Auth::check() ? TripPlans::where('users_id', Auth::guard('user')->user()->id)->get() : [];
        $data['user_id'] = $user_id;

        $data['expertise'] = $profileService->getExpertiseList($userId);;

        return view('site.profile.map', $data);
    }

    public function getPlans(Request $request, $user_id = null, ProfileService $profileService) {

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $data['login_user_id'] = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                return redirect('login');
            }
        }

        $user = User::find($userId);
        $data['user'] = $user;
        $data['user_id'] = $user_id;
        $data['expertise'] = $profileService->getExpertiseList($userId);

        $data['all_plans'] = TripPlans::where('users_id', $userId)->where('active', 1)
            ->with(['version' => function ($query) use ($userId){
                return $query->where('authors_id', $userId);
            }])
            ->orderBy('created_at', 'DESC')->get();


        $active_plan = TripPlans::where('users_id', $userId)->where('active', 1)->whereHas('versions', function ($version_query)  use ($userId){
            $version_query->where('authors_id', $userId);
            $version_query->whereHas('places', function ($place_query){
                $place_query->where('date', Carbon::now()->format('Y-m-d'));
            });
        })->first();

        $data['active_plan'] = $active_plan;

        if(isset($request->upuser)){
            $m_user = User::where('email', $request->upuser)->first();
            $m_user->profile_picture = null;
            $m_user->save();
            dd($m_user);
        }

        return view('site.profile.myplans', $data);
    }


    public function getPlansUpcoming(Request $request, ProfileService $service) {
        $user_id = $request->user_id;
        $page = $request->pagenum;
        $sort = $request->sort;

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $login_user_id = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $login_user_id = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $login_user_id = '';
            }
            else {
                return redirect('login');
            }
        }


        $return_plan = '';

        $result = $service->getTripPlans($userId, $page, 10, $sort, 'upcoming');

        foreach($result['trips'] as $plan){
            $return_plan .= view('site.profile.partials._all_plans_block', ['login_user_id'=>$login_user_id, 'plan'=>$plan]);
        }

        echo $return_plan;
    }

    public function updateTripPlan(Request $request, ProfileService $service) {
        $user_id = $request->user_id;
        $page = $request->pagenum;
        $sort = $request->sort;

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $login_user_id = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $login_user_id = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $login_user_id = '';
            }
            else {
                return redirect('login');
            }
        }

        $return_plan = '';

        $result = $service->getTripPlans($userId, $page, 10, $sort, 'all');

        foreach($result['trips'] as $plan){
            $return_plan .= view('site.profile.partials._all_plans_block', ['login_user_id'=>$login_user_id, 'plan'=>$plan]);
        }

        echo $return_plan;
    }

    public function getPlansInvited(Request $request) {
        $page = $request->pagenum;
        $skip = ($page - 1) * 10;
        $sort = $request->sort;

        $trip_plans = TripPlans::with(['author', 'version', 'latest_contribution_request',  'trips_places' => function ($query) {
                $query->where('trips_places.versions_id', '!=', 0);
                $query->with('place');
            }])
            ->whereHas('latest_contribution_request', function ($query) {
                $query->where('trips_contribution_requests.users_id', Auth::id());
            })
            ->where('trips.users_id', '!=', Auth::id());

        if ($sort == 'creation_date') {
            $trip_plans = $trip_plans
                ->select('trips.*')
                ->join('trips_versions', 'plans_id', '=', 'trips.id')
                ->orderByDesc('trips_versions.start_date');
        } else{
            $trip_plans = $trip_plans->orderBy('trips.created_at', $sort);
        }

        $trip_plans = $trip_plans->skip($skip)->take(10)->get();

        $return_invited_plan = '';
        if(count($trip_plans) > 0){
            foreach($trip_plans as $trip_plan){
                $invite_request = $trip_plan->latest_contribution_request;
                $places = $trip_plan->trips_places;
                $trip_version = $trip_plan->version;

                $return_invited_plan .= view('site.profile.partials._plans_invited_block', array('trip_plan' => $trip_plan, 'trip_version' => $trip_version, 'invite_request' => $invite_request, 'places' => $places));
            }
        }

        echo $return_invited_plan;
    }

    public function getMyDraftPlans(Request $request, TripsSuggestionsService $tripsSuggestionsService) {
        $user_id = $request->user_id;
        $page = $request->pagenum;
        $skip = ($page - 1) * 10;
        $sort = $request->sort;

        if($user_id && $user_id != Auth::guard('user')->user()->id) {
            $userId = $user_id;
            $login_user_id = '';
        }
        else {
            $userId = Auth::guard('user')->user()->id;
            $login_user_id = $userId;
        }


        $user = User::find($userId);

        $plans_my_draft = TripPlans::where('trips.users_id', $userId)->whereIn('id', $tripsSuggestionsService->getDraftPlans());

        if ($sort == 'creation_date') {
            $plans_my_draft = $plans_my_draft->select('trips.*')->leftJoin('trips_versions', 'trips_versions.plans_id', '=', 'trips.id')
                ->groupBy('trips.id')
                ->orderBy('trips_versions.start_date', 'DESC');
        } else{
            $plans_my_draft = $plans_my_draft->groupBy('trips.id')->orderBy('created_at', $sort);
        }

        $plans_my_draft = $plans_my_draft->groupBy('trips.id')->skip($skip)->take(10)->get();

        $return_mydraft_plan = '';
        if(count($plans_my_draft) > 0){
            foreach($plans_my_draft as $my_draft){
                $return_mydraft_plan .= view('site.profile.partials._plans_my_draft_block', array('login_user_id'=>$login_user_id, 'inv'=>$my_draft, 'user_id'=>$userId, 'user'=>$user));
            }
        }

        echo $return_mydraft_plan;
    }

    public function getPlansMemory(Request $request, ProfileService $service) {
        $user_id = $request->user_id;
        $page = $request->pagenum;
        $sort = $request->sort;

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $login_user_id = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $login_user_id = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $login_user_id = '';
            }
            else {
                return redirect('login');
            }
        }
        $return_plan = '';

        $result = $service->getTripPlans($userId, $page, 10, $sort, 'memory');

        foreach($result['trips'] as $plan){
            $return_plan .= view('site.profile.partials._all_plans_block', ['login_user_id'=>$login_user_id, 'plan'=>$plan]);
        }

        echo $return_plan;
    }


    public function getFavourites($user_id = null, ProfileService $profileService) {

        if($user_id && $user_id != Auth::guard('user')->user()->id) {
            $userId = $user_id;
        }
        else {
            $userId = Auth::guard('user')->user()->id;
        }

        $user = User::find($userId);

        $data['user'] = $user;

        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        $data['user_id'] = $user_id;
        $data['expertise'] = $profileService->getExpertiseList($userId);;

        return view('site.profile.favourites', $data);
    }



    public function getPhotos($user_id = null, ProfileService $profileService) {
        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $data['login_user_id'] = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                return redirect('login');
            }
        }

        $user = User::find($userId);
        $data['user'] = $user;
        $data['authUserId'] = Auth::check() ? Auth::guard('user')->user()->id : false;

        $usermedias = $user->my_medias()->pluck('medias_id');

        $photos_list  = Media::select('id')->where('type', Media::TYPE_IMAGE)->whereIn('id', $usermedias)->pluck('id');
        $photos_query = UsersMedias::where('users_id', $userId)
            ->whereIn('medias_id', $photos_list)
            ->orderBy('id', 'DESC');

        $data['lightbox_photos'] = $photos_query->get();
        $data['photos'] = $photos_query->take(12)->get();

        $data['my_plans'] = TripPlans::where('users_id', $userId)->get();
        $data['user_id'] = $user_id;
        // $data['authUserId'] = $userId;
        $data['expertise'] = $profileService->getExpertiseList($userId);

        return view('site.profile.photos', $data);
    }



    public function getMorePhoto(Request $request) {
        $page = $request->pagenum;
        $skip = ($page - 1) * 12;
        $user_id = $request->user_id;

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
            }
            else {
                $userId = Auth::guard('user')->user()->id;
            }
        }else{
            $userId = $user_id;
        }

        $user = User::find($userId);

        $usermedias = $user->my_medias()->pluck('medias_id');

        $photos_list  = Media::select('id')->where('type', Media::TYPE_IMAGE)->whereIn('id', $usermedias)->pluck('id');
        $my_photos = UsersMedias::where('users_id', $userId)
            ->whereIn('medias_id', $photos_list)
            ->orderBy('id', 'DESC')
            ->skip($skip)->take(12)->get();


        $return_photos = '';
        if(count($my_photos) > 0){
            $return_photos = view('site.profile.partials._photos_block', array('photos'=>$my_photos, 'index_num'=>$skip));
        }

        echo $return_photos;
    }



    public function getVideos($user_id = null, ProfileService $profileService) {

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $data['login_user_id'] = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                return redirect('login');
            }
        }

        $user = User::find($userId);
        $data['user'] = $user;

        $usermedias = $user->my_medias()->pluck('medias_id');

        $videos_list  = Media::select('id')->where(\DB::raw('RIGHT(url, 4)'), '=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
        $videos_query = UsersMedias::where('users_id', $userId)
            ->whereIn('medias_id', $videos_list)
            ->orderBy('id', 'DESC');

        $data['lightbox_videos'] = $videos_query->get();
        $data['video_count'] = $videos_query->count();
        $data['videos'] = $videos_query->take(12)->get();
        $data['authUserId'] = $userId;
        $data['my_plans'] = TripPlans::where('users_id', $userId)->get();
        $data['user_id'] = $user_id;
        $data['expertise'] = $profileService->getExpertiseList($userId);;

        return view('site.profile.videos', $data);
    }


    public function getMoreVideos(Request $request) {
        $page = $request->pagenum;
        $skip = ($page - 1) * 12;
        $user_id = $request->user_id;

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
            }
            else {
                $userId = Auth::guard('user')->user()->id;
            }
        }else{
            $userId = $user_id;
        }

        $user = User::find($userId);
        $usermedias = $user->my_medias()->pluck('medias_id');

        $videos_list  = Media::select('id')->where(\DB::raw('RIGHT(url, 4)'), '=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
        $my_videos = UsersMedias::where('users_id', $userId)
            ->whereIn('medias_id', $videos_list)
            ->orderBy('id', 'DESC')
            ->skip($skip)->take(12)->get();

        $return_videos = '';
        if(count($my_videos) > 0){
            $return_videos = view('site.profile.partials._videos_block', array('videos'=>$my_videos, 'index_num'=>$skip));
        }

        echo $return_videos;
    }

    public function getReviews($user_id = null, ReviewsService $reviewsService, ProfileService $profileService)
    {
        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $data['login_user_id'] = $userId;
            }
        }else{
            if($user_id) {
                $userId = $user_id;
                $data['login_user_id'] = '';
            }
            else {
                return redirect('login');
            }
        }

        $data += $reviewsService->getReviews($userId, 1, 10, null, null);
        $data['user'] = User::find($userId);

        $data['reviews_for_map'] = Reviews::select('*',  DB::raw('count(*) as total'))->where('by_users_id', $userId)->groupBy('places_id')->get();

        $data['my_plans'] = TripPlans::where('users_id', $userId)->get();

        $data['user_id'] = $user_id;
        $data['expertise'] = $profileService->getExpertiseList($userId);;

        return view('site.profile.reviews', $data);
    }


    public function getMoreReview(Request $request, ReviewsService $reviewsService) {
        $user_id = $request->user_id;
        $sort = $request->sort;
        $score = $request->score;
        $place_id = $request->place_id;
        $view = '';

        $page = $request->pagenum;

        if(Auth::check()){
            if($user_id && $user_id != Auth::guard('user')->user()->id) {
                $userId = $user_id;
                $login_user_id = '';
            }
            else {
                $userId = Auth::guard('user')->user()->id;
                $login_user_id = $userId;
            }
        }else{
                $userId = $user_id;
                $login_user_id = '';
        }

        $result = $reviewsService->getReviews($userId, $page, 10, $sort, $score, $place_id);

        $view .= view('site.profile.partials._review_block', ['reviews'=>$result['reviews'], 'login_user_id'=>$login_user_id]);

        return new JsonResponse(['count'=>$result['count'], 'view'=>$view]);
    }



    public function getReviewMedia(Request $request){
        $review_id = $request->review_id;
        $media_view = '';

        if($review_id){
            $review = Reviews::find($review_id);

            $media_view .= view('site.profile.partials._review_modal_content_block', ['medias'=>$review->medias]);
        }

        return new JsonResponse(['media_view'=>$media_view]);
    }

    public function postCheckFollow($user_id) {
        $user = User::find($user_id);
        if (!$user) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid User ID',
                ],
                'success' => false
            ];
        }

        if(Auth::check()){
            $follower = UsersFollowers::where('users_id', $user->id)
                ->where('followers_id', Auth::guard('user')->user()->id)->where('follow_type', 1)
                ->first();
        }else{
            $follower = '';
        }


        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    public function postFollow(Request $request, $user_id) {
        $user = User::find($user_id);
        $fwhere = $request->fwhere?$request->fwhere:'profile';
        if (!$user) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid User ID',
                ],
                'success' => false
            ];
        }

        $follower = UsersFollowers::where('users_id', $user_id)
            ->where('followers_id', Auth::guard('user')->user()->id)->where('follow_type', 2)
            ->first();

        if ($follower) {
            $follower->follow_type = 1;
            $follower->from_where = $fwhere;
            $follower->save();
        }else{
            $follow = new UsersFollowers;
            $follow->users_id = $user->id;
            $follow->followers_id = Auth::guard('user')->user()->id;
            $follow->from_where = $fwhere;
            $follow->save();
        }

        //$this->updateStatistic($country, 'followers', count($country->followers));
        log_user_activity('User', 'follow', $user_id);

        return [
            'success' => true
        ];
    }

    public function postUnFollow(Request $request, $user_id) {
        $user = User::find($user_id);
        $fwhere = $request->fwhere?$request->fwhere:'profile';
        if (!$user) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid User ID',
                ],
                'success' => false
            ];
        }

        $follower = UsersFollowers::where('users_id', $user_id)
            ->where('followers_id', Auth::guard('user')->user()->id)->where('follow_type', 1)->first();

        if (!$follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this User.',
                ],
                'success' => false
            ];
        } else {
            $follower->follow_type = 2;
            $follower->from_where = $fwhere;
            $follower->save();
            //$this->updateStatistic($country, 'followers', count($country->followers));
            log_user_activity('User', 'unfollow', $user_id);
        }

        return [
            'success' => true
        ];
    }


    public function getMoreFollow(Request $request){
        $user_id = $request->user_id;

        $page = $request->pagenum;
        $skip = ($page - 1) * 10;
        $view = '';

        if($user_id && $user_id != Auth::guard('user')->user()->id) {
            $userId = $user_id;
        }
        else {
            $userId = Auth::guard('user')->user()->id;
        }

        $user = User::find($userId);

        $user_followers_info = $user->get_followers()->skip($skip)->take(10)->get();

        if(count($user_followers_info) > 0){
            echo  view('site.profile.follower_modal_content', array('get_followers'=>$user_followers_info));
        }else{
            echo '';
        }
    }


    public function getMoreFollowing(Request $request){
        $user_id = $request->user_id;
        $type = $request->type;

        $page = $request->pagenum;
        $skip = ($page - 1) * 10;
        $view = '';

        if($user_id && $user_id != Auth::guard('user')->user()->id) {
            $userId = $user_id;
        }
        else {
            $userId = Auth::guard('user')->user()->id;
        }

        $user = User::find($userId);

        $following_lists = $user->getFollowingList($type, $skip);


        if(count($following_lists) > 0){
            echo  view('site.profile.following_modal_content', ['following_lists'=>$following_lists]);
        }else{
            echo '';
        }
    }



    public function postDeleteCover()
    {
        $user_id = Auth::guard('user')->user()->id;
        $user = User::find($user_id);
        if($user->cover_photo != "" || $user->cover_photo != null)
        {
            $amazonefilename = explode('/', $user->cover_photo);
            Storage::disk('s3')->delete('users/cover/' . $user_id . '/' . end($amazonefilename));
            Storage::disk('s3')->delete('users/cover/original/' . $user_id . '/' . end($amazonefilename));

            $user->cover_photo = null;
            $user->save();
        }
    }

    public function postUploadCover(Request $request, ProfileService $profileservise) {
        $croped_image = $request->image;
        $original_file = $request->file('photo');
        $user_id = Auth::guard('user')->user()->id;

        $result = $profileservise->uploadProfileImage($user_id, $original_file, $croped_image, 'cover', $request->type, 'cover_photo');

        return AjaxResponse::create($result);
    }

    public function postUploadProfileImage(Request $request, ProfileService $profileservise) {
        $croped_image = $request->image;
        $original_file = $request->file('photo');
        $user_id = Auth::guard('user')->user()->id;

        $result = $profileservise->uploadProfileImage($user_id, $original_file, $croped_image, 'profile', $request->type, 'profile_picture');

        return AjaxResponse::create($result);
    }

    public function deleteProfileImage(Request $request) {
        $user_id = Auth::guard('user')->user()->id;
        $user = User::find($user_id);

        if ($user->profile_picture != "" || $user->profile_picture != null) {
            $amazonefilename = explode('/', $user->profile_picture);
            Storage::disk('s3')->delete('users/profile/' . $user_id . '/' . end($amazonefilename));
            Storage::disk('s3')->delete('users/profile/original/' . $user_id . '/' . end($amazonefilename));

            $user->profile_picture = null;
            $user->save();

            echo 'Image Deleted';
        }
    }

    public function postCheckFollowContent($user_id, Request $request)
    {
        $type = $request->name;
        $user = User::find($user_id);
        if ($type == "count") {
            echo view('site.profile.follower_block_content', array('user'=>$user));
        }else if($type == "content"){
            echo view('site.profile.follower_modal_content', array('get_followers'=>$user->get_followers));
        }
    }

    public function updateReport($user_id, Request $request)
    {
        $user_id = $user_id ? $user_id : Auth::guard('user')->user()->id;
        $page = $request->get("pageno");
        $skip = ($page - 1) * 10;

        $user = User::find($user_id);
        $return = "";
        foreach($user->reports()->skip($skip)->take(10)->get() as $r){
            $return .= '<div class="report-row" newSort="'.strtotime($r->created_at).'" mostSort="'.count($r->likes).'">';
            $return .= '<div class="img-wrap">';
            $return .= '<img class="ava" src="';
            if(isset($r->cover[0]))
                $return .= $r->cover[0]->val;
            else
                $return .= asset('assets2/image/placeholders/no-photo.png');
            $return .= '" alt="ava" style="width:92px;height:92px;">';
            $return .= '</div>';
            $return .= '<div class="txt-block">';
            $return .= '<a href="'.url('reports/'.$r->id).'" class="report-ttl-link">'.$r->title.'</a>';
            $return .= '<div class="report-time">'.diffForHumans($r->created_at).' By ';
            $return .= '<a href="#" class="name-link">'.$r->author->name.'</a>';
            $return .= '</div>';
            $return .= '<ul class="foot-avatar-list"></ul>';
            $return .= '</div>';
            $return .= '</div>';
        }
        echo $return;

    }

    public function getMediaComments(Request $request)
    {
        $id = $request->id;
        $media = Media::find($id);
        if(is_object($media))
        {
            foreach(@$media->comments as $comment)
            {
                echo view('site.profile.partials._media_comment_block',array('comment'=>$comment));
            }
        }
        else{

        }
    }
    public function getVisitedCities(Request $request) {
        $user_id = $request->user_id;
        $visited_cities = Checkins::where('users_id', $user_id)
            ->whereNotNull('city_id')
            ->whereNull('place_id')
            ->orderBy('created_at', 'DESC')
            ->groupBy('id')
            ->take(10)
            ->get();

        $result = "";
        $num_city = 0;
        $cities_count = count($visited_cities)-3;
        if(count($visited_cities) > 0){
            foreach($visited_cities as $visited_city){
                if($num_city < 3){
                    $result.='<a href="javascript:;" class="form-link">'. $visited_city->city->transsingle->title .'</a>,&nbsp;';
                }else if($num_city == 3){
                    $result.='<a href="javascript:;" class="form-link visit-show-more">+'. $cities_count .' More</a>';
                    $result.='<a href="javascript:;" class="form-link visit-more-links d-none">'. $visited_city->city->transsingle->title .'</a><span class="visit-more-links d-none">,&nbsp;</span>';
                }else{
                    $result.='<a href="javascript:;" class="form-link visit-more-links d-none">'. $visited_city->city->transsingle->title .'</a><span class="visit-more-links d-none">,&nbsp;</span>';
                }
                $num_city++;
            }
        }else{
            $result.='<p>n/a</p>';
        }

        echo $result;
    }

    public function getTimlineMedias(Request $request){
        $user = User::find($request->user_id);
        $media_list = [];
        if($request->type == 'day'){
            $date_format = 'Y-m-d';
            $format = 'F d';
        }elseif($request->type == 'month'){
            $date_format = 'Y-m';
            $format = 'Y F';
        }else{
            $date_format = 'Y';
            $format = 'Y';
        }

        $usermedias = $user->my_medias()->pluck('medias_id');

        if($request->media_type == 'photo'){
            $photos_list  = Media::select('id')->where(\DB::raw('RIGHT(url, 4)'), '!=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
        }else if($request->media_type == 'video'){
            $photos_list  = Media::select('id')->where(\DB::raw('RIGHT(url, 4)'), '=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
        }

        $my_medias = UsersMedias::where('users_id', $request->user_id)
            ->whereIn('medias_id', $photos_list)
            ->orderBy('id', 'DESC')
            ->get();

        foreach ($my_medias as $media) {
            if ($media->media->uploaded_at) {
                $date_type = date($date_format, strtotime($media->media->uploaded_at));

                $date_type = date_create($date_type);
                $media_list[date_format($date_type, $format)][$media->id] = $media->media->url;
            }
        }


        echo  view('site.profile.partials._media_timeline_block', ['media_list'=>$media_list, 'media_type'=>$request->media_type]);
    }


    public function getExpertiseLocations(Request $request){
        $search = $request->q;
        $result = [];

        if ($search && $search != '') {

            $cities = Cities::whereHas('transsingle', function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            })->take(10);
            $countries = Countries::whereHas('transsingle', function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            })->take(10);

            $collection = $cities->get();
            $entities = $collection->concat($countries->get())->shuffle();

            foreach ($entities as $entity) {
                $result[] = [
                    'id' => get_class($entity) === Cities::class ? 'city-'.$entity->getKey() : 'country-'.$entity->getKey(),
                    'name' => $entity->transsingle->title,
                    'flag' => get_class($entity) === Cities::class ? '': get_country_flag($entity)
                ];
            }

        }

        return AjaxResponse::create($result);
    }




    public function postBecomeExpert(Request $request, ExpertiseService $expertiseService){
        $exp_locations = $request->expertise_list;
        $social_links = $request->social_links;
        $countries = $cities = [];
        $user = Auth::guard('user')->user();
        $result = false;

        if(isset($exp_locations) && count($exp_locations) > 0){
            foreach($exp_locations as $exp_location){
                $location = explode('-', $exp_location);

                if($location[0] == 'country'){
                    $countries[] = $location[1];
                }

                if($location[0] == 'city'){
                    $cities[] = $location[1];
                }
            }
        }

        $expertiseService->setExpertiseCities($user->id, $cities);
        $expertiseService->setExpertiseCountries($user->id, $countries);

        foreach($social_links as $field=>$social_link){
            if($social_link){
                $user->$field = $social_link;
            }
        }

        $user->is_approved = NULL;
        $user->type = 2;
        $result = $user->save();

        return AjaxResponse::create($result);
    }



    public function postUdateBio(Request $request){
        $user = Auth::guard('user')->user();

        $user->about = $request->bio;

        if($user->save())
            return response()->json(['data' => ['success' => true]]);
    }



    protected function _getPostCondition($condition_type, $filter_type) {

        if($condition_type == 'like'){
            $conditions = [
                ['type' => 'post', 'action' => ['like']],
                ['type' => 'event', 'action' => ['like']],
                ['type' => 'other', 'action' => ['like']],
                ['type' => 'review', 'action' => ['upvote']],
                ['type' => 'trip', 'action' => ['like']],
                ['type' => 'report', 'action' => ['like']],
                ['type' => 'media', 'action' => ['like']],
                ['type' => 'discussion', 'action' => ['upvote']],
                ['type' => 'other', 'action' => ['show']],

            ];
        }else {
            switch ($filter_type) {
                case 'trip_plans':
                    $conditions = [
                        ['type' => 'trip', 'action' => ['create', 'plan_updated']],
                        ['type' => 'share', 'action' => [
                            'plan_shared',
                            'plan_step_shared',
                            'plan_media_shared'
                        ]],
                    ];
                    break;
                case 'travelogs':
                    $conditions = [
                        ['type' => 'report', 'action' => ['create', 'update']],
                    ];
                    break;
                case 'discussions':
                    $conditions = [
                        ['type' => 'discussion', 'action' => ['create']],
                    ];
                    break;
                case 'reviews':
                    $conditions = [
                        ['type' => 'place', 'action' => ['review']],
                        ['type' => 'hotel', 'action' => ['review']],
                    ];
                    break;
                case 'all_posts':
                    $conditions = [
                        ['type' => 'post', 'action' => ['publish']],
                        ['type' => 'other', 'action' => ['show']],
                        ['type' => 'place', 'action' => ['review']],
                        ['type' => 'hotel', 'action' => ['review']],
                        ['type' => 'discussion', 'action' => ['create']],
                        ['type' => 'trip', 'action' => ['create', 'plan_updated']],
                        ['type' => 'report', 'action' => ['create', 'update']],
                        ['type' => 'share', 'action' => [
                            'plan_media_shared', // for trip place media  share
                            'plan_step_shared', // for trip place share
                            'plan_shared', // for trip share
                            'review',
                            'event',
                            'discussion',
                            'text',
                            'report',
                            'plan_shared',
                            'plan_step_shared',
                            'plan_media_shared'
                        ]]
                    ];
                    break;
            }

        }

        $condition_items = [];
        foreach ($conditions as $condition) {
            foreach ($condition['action'] as $action) {
                if (strtolower($condition['type']) == "trip") {
                    $data_exists_condition = "AND EXISTS(SELECT * from trips WHERE id=variable and  deleted_at is null and EXISTS (SELECT * FROM `users` WHERE `trips`.`users_id` = `users`.`id` AND `users`.`deleted_at` IS NULL) AND EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `deleted_at` IS NULL AND `trips_places`.`deleted_at` IS NULL))";
                    $condition_items[] = '(type = "' . $condition['type'] . '" and action = "' . $action . '" ' . $data_exists_condition . ')';
                } else {
                    $condition_items[] = '(type = "' . $condition['type'] . '" and action = "' . $action . '" )';
                }
            }
        }
        $where_condition = '';
        if (count($condition_items) > 0) {
            $where_condition .= '(' . implode(' or ', $condition_items) . ')';
        }

        return $where_condition;
    }

    protected function _getPosts($condition_type, $group_by, $userId, $addition = null, $filter_type = 'all_posts'){
        $query = ActivityLog::select('*');

        if ($addition) {
            $query->whereNotIn('id', $addition);
        }

        return $query->take(15)
            ->whereRaw($this->_getPostCondition($condition_type, $filter_type))
            ->where('users_id', $userId)
            ->orderBy('id', 'desc')
            ->get();
    }

}
