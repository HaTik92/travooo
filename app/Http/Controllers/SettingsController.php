<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Models\User\User;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsCities\ExpertsCities;
use App\Models\TripPlans\TripPlans;
use App\Models\Ranking\RankingUsersTransactions;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\Country\Countries;
use App\Models\DeleteAccount\DeleteReasons;
use App\Models\DeleteAccount\DeletedUsers;
use App\Models\Chat\ChatConversationMessage;
use App\Models\UsersFollowers\UsersFollowers;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsCheckins;
use App\Models\Posts\PostsLikes;
use App\Models\Posts\PostsShares;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsCommentsLikes;
use App\Models\Posts\Checkins;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsInfos;
use App\Models\Reports\ReportsDraft;
use App\Models\Reports\ReportsDraftInfos;
use App\Models\Reports\ReportsComments;
use App\Models\Reports\ReportsCommentsMedias;
use App\Models\Reports\ReportsLikes;
use App\Models\Reports\ReportsShares;
use App\Models\Reports\ReportsViews;
use App\Models\Reports\ReportsCommentsLikes;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionMedias;
use App\Models\Discussion\DiscussionExperts;
use App\Models\Discussion\DiscussionRepliesUpvotes;
use App\Models\Discussion\DiscussionRepliesDownvotes;
use App\Models\Discussion\DiscussionViews;
use App\Models\Discussion\DiscussionLikes;
use App\Models\Discussion\DiscussionShares;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\TravelMates\TravelMatesRequestUsers;
use App\Models\TripPlans\TripsContributionRequests;
use App\Models\TripPlans\TripsSuggestion;
use App\Models\TripCities\TripCities;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripCountries\TripCountries;
use App\Models\TripPlans\TripsVersions;
use App\Models\TripPlans\TripsLikes;
use App\Models\TripPlans\TripsComments;
use App\Models\TripPlans\TripsShares;
use App\Models\TripPlans\TripsContributionCommits;
use App\Models\Notifications\Notifications;
use App\Models\Reviews\Reviews;
use App\Models\Reviews\ReviewsVotes;
use App\Models\Reviews\ReviewsShares;
use App\Models\User\UsersPrivacy;
use App\Models\User\SettingUsersBlocks;
use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\User\UsersContentLanguages;
use App\Models\Ranking\RankingPointsEarners;
use App\Models\ActivityLog\ActivityLog;
use App\Models\User\DeleteAccountRequest;
use App\Notifications\DeleteAccountRequestNotification;
use App\Services\Api\UserProfileService;
use Jenssegers\Agent\Agent;

class SettingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:user', ['except' => [
            'finalDeleteAccount'
        ]]);
    }

    public function getIndex(Request $request)
    {
        $agent = new Agent();

        if (isset($request->checkexp) && $request->checkexp == 20) {
            $users = User::all();
            foreach ($users as $user) {
                if ($user->type == NULL) {
                    if ($user->expert == 1) {
                        $user->type = 2;
                        $user->expert = 0;
                    } else {
                        $user->type = 1;
                    }

                    $user->save();
                }
            }
            dd(count($users));
        }

        $user = Auth::guard('user')->user();
        $data['user'] = $user;
        $expertise = array();
        $user_expertise = array();
        $styles_list = array();

        $cities = ExpertsCities::with('cities')->where('users_id', $user->id)->get();
        $exp_countries = ExpertsCountries::with('countries')->where('users_id', $user->id)->get();
        if (count($cities) > 0) {
            foreach ($cities as $city) {
                $user_expertise[] = 'city,' . $city->cities_id;
                $expertise[] = [
                    'id' => 'city,' . $city->cities_id,
                    'text' => $city->cities->transsingle->title
                ];
            }
        }

        if (count($exp_countries) > 0) {
            foreach ($exp_countries as $country) {
                $user_expertise[] = 'country,' . $country->countries_id;
                $expertise[] = [
                    'id' => 'country,' . $country->countries_id,
                    'text' => $country->countries->transsingle->title
                ];
            }
        }


        if (count($user->travelstyles) > 0) {
            foreach ($user->travelstyles as $style) {
                $styles_list[] = @$style->travelstyle->id;
            }
        }


        $data['expertise'] = $expertise;

        $countries = Countries::with('transsingle')->get()
            ->sortBy(function ($countries) {
                return $countries->transsingle->title;
            });
        $data['countries'] = $countries;
        $data['styles_list'] = $styles_list;
        $data['user_expertise'] = $user_expertise;
        $data['del_reason_list'] = DeleteReasons::all();
        $data['aget_info'] = [
            'browser' => $agent->browser(),
            'platform' => $agent->platform(),
            'user_ip' => $request->ip(),
        ];

        $data['chat_count'] = ChatConversationMessage::where('to_id', $user->id)->where('from_id', '<>', $user->id)->where('is_read', false)->count();
        $data['notification_count'] = Notifications::where('users_id', $user->id)->where('read', 0)->count();

        return view('site.settings.index', $data);
    }

    public function postAccount(Request $request)
    {
        $user = Auth::user();

        $name = $request->get('name');
        $username = $request->get('username');
        $contact_email = $request->get('contact_email');
        $nationality = $request->get('nationality');
        $phone = $request->get('full_mobile_number');
        $about = $request->get('about');
        $interests = $request->get('interest_list');

        Validator::make($request->all(), [
            'username' => 'required|string|min:4|max:15|regex:/^([A-Za-z0-9_])+$/|unique:users,username,' . $user->id,
        ])->validate();


        $user->name = $name;
        $user->username = $username;
        $user->contact_email = $contact_email;
        $user->nationality = $nationality;
        $user->about = $about;
        $user->mobile = $phone;
        $user->interests = $interests;

        $user->save();

        $expertise = explode('-:', $request->get('expertise'));

        ExpertsCities::where('users_id', $user->id)->delete();
        ExpertsCountries::where('users_id', $user->id)->delete();

        if (isset($expertise) && count($expertise) > 0) {
            foreach ($expertise as $ex) {
                $exp = explode(",", $ex);

                if ($exp[0] == "country") {
                    $new_ex = new ExpertsCountries;
                    $new_ex->users_id = $user->id;
                    $new_ex->countries_id = $exp[1];
                    $new_ex->save();
                } elseif ($exp[0] == "city") {
                    $new_ex = new ExpertsCities;
                    $new_ex->users_id = $user->id;
                    $new_ex->cities_id = $exp[1];
                    $new_ex->save();
                }
            }
        }



        $travelstyles = explode(',', $request->get('travelstyles'));

        \App\Models\User\UsersTravelStyles::where('users_id', $user->id)->delete();

        if (isset($travelstyles) && count($travelstyles) > 0 && $travelstyles[0] != '') {
            foreach ($travelstyles as $tr) {
                $new_tr = new \App\Models\User\UsersTravelStyles;
                $new_tr->users_id = $user->id;
                $new_tr->conf_lifestyles_id = $tr;
                $new_tr->save();
            }
        }


        //if all fields filled, give the user 1000 points, just for one time
        if ($request->get('name') != '' && $request->get('nationality') != '' && $request->get('about') != '' && $request->get('profession') != '') {
            if (RankingUsersTransactions::where('users_id', $user->id)->where('action', 'profile_complete')->get()->count() == 0) {
                ranking_add_points($user->id, 5, 1000, 'profile_complete', $user->id, $user->id);
            }
        }

        $xp_cities = ExpertsCities::with('cities')->get();
        $exp_countries = ExpertsCountries::with('countries')->get();
        $expertise_count = [];
        $expertise = [];
        $exp_content = '';

        if (count($xp_cities) > 0) {
            foreach ($xp_cities as $city) {
                if (array_key_exists($city->cities->transsingle->title, $expertise_count)) {
                    $expertise_count[$city->cities->transsingle->title] = $expertise_count[$city->cities->transsingle->title] + 1;
                } else {
                    $expertise_count[$city->cities->transsingle->title] = 1;
                }

                if ($city->users_id == $user->id) {
                    $expertise[] = ['text' => $city->cities->transsingle->title];
                }
            }
        }

        if (count($exp_countries) > 0) {
            foreach ($exp_countries as $country) {
                if (array_key_exists($country->countries->transsingle->title, $expertise_count)) {
                    $expertise_count[$country->countries->transsingle->title] = $expertise_count[$country->countries->transsingle->title] + 1;
                } else {
                    $expertise_count[$country->countries->transsingle->title] = 1;
                }

                if ($country->users_id == $user->id) {
                    $expertise[] = ['text' => $country->countries->transsingle->title];
                }
            }
        }

        if (count($expertise) > 0) {
            foreach ($expertise as $k => $ex) {
                $scope = '';
                if ($k < count($expertise) - 2) {
                    $scope = ', ';
                }

                if ($k === count($expertise) - 2) {
                    $scope = 'and ';
                }

                $exp_content .= '<strong>' . $ex['text'] . '</strong>
                                  <sup class="profile-sup-text">' . optimize_counter(($expertise_count[$ex['text']]) ? $expertise_count[$ex['text']] : 0) . '
                                    </sup>
                                    <span class="profile-sup-seprate">' . $scope . '</span>';
            }
        }
        $country_iso = '';
        if (isset($user->country_of_nationality->iso_code)) {
            $country_iso = '<img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/' . strtolower($user->country_of_nationality->iso_code) . '.png" alt="flag" style="width:40px;height:26px;margin-bottom: 7px;">';
        }

        if (isset($request->content_languages)) {
            UsersContentLanguages::where('users_id', $user->id)->delete();
            foreach (explode(',', $request->content_languages) as $language) {
                $lng = new UsersContentLanguages();
                $lng->users_id = $user->id;
                $lng->language_id = $language;

                $lng->save();
            }
        }


        if (isset($request->is_ajax)) {
            return response()->json(['data' => ['success' => true, 'exp_content' => $exp_content, 'country_iso' => $country_iso, 'name_size' => calculate_charackter_font_size($user->name, 'header'), 'about_name_size' => calculate_charackter_font_size($user->name, 'about')]]);
        } else {
            return redirect()->back()->withFlashSuccess('Your Account Successfully Updated!');
        }
    }

    /**
     * Add or Update social profiles links.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postSocial(Request $request)
    {
        $user = Auth::guard('user')->user();

        $website = $request->get('website');
        $facebook = $request->get('facebook');
        $twitter = $request->get('twitter');
        $instagram = $request->get('instagram');
        $youtube = $request->get('youtube');
        $tumblr = $request->get('tumblr');
        $pinterest = $request->get('pinterest');


        $user->website = $website;
        $user->facebook = $facebook;
        $user->twitter = $twitter;
        $user->instagram = $instagram;
        $user->youtube = $youtube;
        $user->tumblr = $tumblr;
        $user->pinterest = $pinterest;

        $user->save();

        if (isset($request->is_ajax)) {
            return response()->json(['data' => ['success' => true]]);
        } else {
            return redirect()->back()->withFlashSuccess('Successfully Updated!');
        }
    }

    /**
     * Change user Password.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postSecurity(Request $request)
    {
        $user = Auth::user();

        Validator::make($request->all(), [
            'current_password' => ['required', function ($attribute, $value, $fail) use ($user) {
                if (!\Hash::check($value, $user->password)) {
                    return $fail(__('The current password is incorrect.'));
                }
            }],
            'new_password' => 'required|same:new_confirm_password|string|min:6|regex:/^(?=.*[A-Za-z])(?=.*\d)[a-zA-Z0-9!@#$%^&*]{6,}$/',
            'new_confirm_password' => 'required'
        ])->validate();

        $user->forceFill([
            'password' => bcrypt($request->new_password),
            'remember_token' => Str::random(60),
        ])->save();

        return redirect()->back()->withFlashSuccess('Password Successfully Updated!');
    }

    /**
     * Add Privacy settings.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postPrivacy(Request $request)
    {
        $user = Auth::guard('user')->user();

        $user_privacy = UsersPrivacy::where('users_id', $user->id)->first();

        if (!$user_privacy) {
            $user_privacy = new UsersPrivacy();
            $user_privacy->users_id = $user->id;
        }

        $user_privacy->see_my_following = $request->see_my_following;
        $user_privacy->message_me = $request->message_me;
        $user_privacy->follow_me = $request->follow_me;
        $user_privacy->find_me = $request->find_me;

        $user_privacy->save();

        return redirect()->back()->withFlashSuccess('Privacy Setting Successfully Updated!');
    }

    /**
     * Add blocking users.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postBlockingUsers(Request $request)
    {
        $user = Auth::guard('user')->user();
        $blocked_user_id = $request->blocked_user;

        if ($blocked_user_id) {
            $blocked_user_info = User::find($blocked_user_id);

            $setting_block_users = new SettingUsersBlocks();

            $setting_block_users->users_id = $user->id;
            $setting_block_users->blocked_users_id = $blocked_user_id;

            if ($setting_block_users->save()) {
                if (isset($request->is_ajax)) {
                    return response()->json(['success' => true]);
                } else {
                    return redirect()->back()->withFlashSuccess('You are blocked ' . $blocked_user_info->name . '!');
                }
            }
        }
    }

    /**
     * Delete blocking users.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postUnBlockingUsers(Request $request)
    {
        $user = Auth::guard('user')->user();
        $blocked_user_id = $request->id;

        if ($blocked_user_id) {
            $blocked_user_info = User::find($blocked_user_id);

            $setting_block_users = SettingUsersBlocks::where('users_id', $user->id)->where('blocked_users_id', $blocked_user_id)->delete();

            if ($setting_block_users) {
                \Session::flash('flash_success', 'You are unblocked ' . $blocked_user_info->name . '!');
                return ['success' => true];
            }
        }
    }

    /**
     * get More blocking users.
     *
     * @param  Request  $request
     * @return view
     */
    public function getMoreBlockingUser(Request $request)
    {
        $page = $request->pagenum;
        $skip = ($page - 1) * 10;
        $user = Auth::guard('user')->user();
        $return = '';

        $setting_block_users = SettingUsersBlocks::where('users_id', $user->id)->orderBy('created_at', 'desc')->skip($skip)->take(10)->get();

        if (count($setting_block_users) > 0) {
            $return .= view('site.settings.partials._blocked-list-block', ['setting_block_users' => $setting_block_users]);

            return $return;
        } else {
            return '';
        }
    }

    /**
     * Delete user account.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postDeleteAccount(Request $request)
    {
        $reason = $request->delete_reason;
        $user = Auth::guard('user')->user();
        if ($reason) {
            $deleteAccountRequest = DeleteAccountRequest::updateOrCreate(
                [
                    'status' => DeleteAccountRequest::STATUS_SEND_MAIL,
                    'users_id' => $user->id,
                ],
                [
                    'delete_reason' => $reason,
                    'status' => DeleteAccountRequest::STATUS_SEND_MAIL,
                    'users_id' => $user->id,
                    'token' => str_random(DeleteAccountRequest::TOKEN_LENGTH)
                ]
            );
            try {
                $user->notify(
                    new DeleteAccountRequestNotification($deleteAccountRequest->token)
                );
            } catch (\Throwable $e) {
            }

            return redirect()->back()->withFlashSuccess('Check your mail!');
        } else {
            return redirect()->back();
        }
    }

    public function finalDeleteAccount(Request $request, UserProfileService $userProfileService, $token)
    {
        try {
            $deleteAccountRequest = DeleteAccountRequest::where('token', $token)->where('status', DeleteAccountRequest::STATUS_SEND_MAIL)->first();
            if ($deleteAccountRequest) {
                $deleteAccountRequest->status = DeleteAccountRequest::STATUS_CONFIRM;
                $deleteAccountRequest->save();
                $userProfileService->deleteAccount($deleteAccountRequest->users_id);
            }
            Auth::logout();
            $request->session()->flush();
            $request->session()->invalidate();
            return redirect('/login')->withFlashSuccess('Account Delete Successfully!');
        } catch (\Throwable $e) {
            return redirect('/login')->withFlashSuccess($e->getMessage());
        }
    }

    /**
     * Search users for blocking.
     *
     * @param  Request  $request
     * @return Response
     */
    public function ajaxSearchUser(Request $request)
    {
        $logged_user_id = Auth::user()->id;
        $query = $request->get('q');
        if ($query) {
            $users = array();

            $blocked_user_list = SettingUsersBlocks::where('users_id', $logged_user_id)->pluck('blocked_users_id');

            $get_users = User::where(function ($q) use ($query) {
                $q->where('name', 'like', '%' . $query . '%');
                $q->orWhere('username', 'like', '%' . $query . '%');
            })->where('id', '!=', $logged_user_id)->whereNotIn('id', $blocked_user_list)->get();

            foreach ($get_users as $u) {
                $users[] = [
                    'id' => $u->id,
                    'text' => $u->name,
                    'image' => check_profile_picture($u->profile_picture),
                    'nationality' => $u->nationality ? $u->country_of_nationality->transsingle->title : '',
                    'query' => $query,
                ];
            }
            return json_encode($users);
        }
    }

    /**
     * Search regional languages.
     *
     * @param  Request  $request
     * @return Response
     */
    public function ajaxSearchRegionalLanguage(Request $request)
    {
        $search_query = $request->get('q');
        $lng_list = array();

        if ($search_query != '') {
            $regional_languages = LanguagesSpoken::whereHas('transsingle', function ($q) use ($search_query) {
                $q->where('title', 'like', '%' . $search_query . '%');
            })->take(20)->get();
        } else {
            $regional_languages = LanguagesSpoken::with('transsingle')->take(20)->get()
                ->sortBy(function ($regional_languages) {
                    return $regional_languages->transsingle->title;
                });
        }

        foreach ($regional_languages as $lng) {
            $lng_list[] = [
                'id' => $lng->id,
                'text' => $lng->transsingle->title,
                'query' => $search_query,
            ];
        }
        return json_encode($lng_list);
    }
}
