<?php

namespace App\Http\Controllers;

use App\Http\Constants\CommonConst;
use App\Models\Discussion\SpamsReplies;
use App\Services\Discussions\SpamsRepliesService;
use App\Services\Discussions\DiscussionVotesService;
use App\Services\Discussions\DiscussionsService;
use App\Services\EnlargedViews\EnlargedViewsService;
use App\Services\Ranking\RankingService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use App\Models\Discussion\DiscussionReplies;
use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionMedias;
use App\Models\Discussion\DiscussionExperts;
use App\Models\Discussion\DiscussionRepliesUpvotes;
use App\Models\Discussion\DiscussionRepliesDownvotes;
use App\Models\Discussion\DiscussionViews;
use App\Models\Discussion\DiscussionLikes;
use App\Models\Discussion\DiscussionShares;
use App\Models\Discussion\DiscussionTopics;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsCities\ExpertsCities;

use App\Models\TripPlans\TripPlans;
use App\Models\Place\Place;
use App\Models\Country\ApiCountry as Country;
use App\Models\City\Cities;
use App\Services\Translations\TranslationService;


class DiscussionController extends Controller
{
    public function __construct()
    {
        //        $this->middleware('auth:user');
    }

    public function getIndex(DiscussionsService $discussionsService)
    {
        $blocked_users = blocked_users_list();
        $data['trending_topics'] =  $discussionsService->getTrendingTopic();


        $data['most_helpful'] = DB::table('discussion_replies')
            ->selectRaw('users_id, count(*) as total');

        if (Auth::check()) {
            $data['most_helpful'] = $data['most_helpful']->where('users_id', '!=', Auth::user()->id);
        }

        $data['most_helpful'] = $data['most_helpful']->whereNotIn('users_id', $blocked_users)
            ->groupBy('users_id')
            ->orderBy('total', 'desc')
            ->where('created_at', '>=', Carbon::now()->subDays(7))->take(10)->get();

        if (Auth::check()) {
            $data['my_questions'] = Discussion::where('users_id', Auth::user()->id)->orderBy('created_at', 'desc')->take(2)->get();
            $data['answers'] = Discussion::where('users_id', Auth::user()->id)->count();
            $data['suggestions'] = DiscussionReplies::where('users_id', Auth::user()->id)->count();
        }


        return view('site.discussions.index', $data);
    }


    public function getSearchDiscussion(Request $request, DiscussionsService $discussionsService)
    {
        $queryParam = $request->get('q');

        $page = $request->pagenum;

        $skip = ($page - 1) * 10;
        $destination_type = '';
        $destination_id = '';
        $order = $request->order;
        $type = $request->type ? $request->type : null;
        $trending_type = $request->trending_type ? $request->trending_type : '';
        $answer_user_id = $request->answers_user_id ? $request->answers_user_id : '';
        $most_discussed = [];

        $view = false;
        $trending_view = false;
        $from_last = false;

        if (isset($request->dest_type)) {
            $disc_type = explode(',', $request->dest_type);
            $destination_type = $disc_type[1];
            $destination_id = $disc_type[0];
        }

        if($request->has('first_load')){
            $limit = 5;
        }else {
            $limit = 10;
        }

        $onlyAuthUser = $type ? true : false;
        $onlyAnswerUser = $answer_user_id ? $answer_user_id : false;

        $discussions = Discussion::getDiscussions($queryParam, $skip, $limit, $destination_type, $destination_id, $order, $type, $type ? true : false, $trending_type, $answer_user_id);

        if (count($discussions) > 0) {
            if ($page >= 2 && (($page - 1) % 3 == 0 || ($skip + 5) % 15 == 0)) {
                $most_discussed = $discussionsService->getTrendingDiscussions($page - 1);

                if(count($most_discussed) > 0){
                    $trending_view = true;
                }

                if(($skip + 5) % 15 == 0){
                    $from_last = true;
                }
            }

            $view .= view('site.discussions.partials._discussion-block', ['discussions' => $discussions, 'most_discussed' => $most_discussed, 'from_last' => $from_last, 'onlyAuthUser' => $onlyAuthUser, 'search_param' => $queryParam, 'onlyAnswerUser' => $onlyAnswerUser]);
        }

        return new JsonResponse(['view' => $view, 'trending_view' => $trending_view, 'from_last'=>$from_last]);
    }


    public function getAjaxView(Request $request)
    {
        $discussion_id = $request->discussion_id;
        $discussion = Discussion::find($discussion_id);

        if ($discussion) {
            return view('site.discussions.partials._view-block', ['dis' => $discussion]);
        } else {
            return '';
        }
    }


    public function getMoreHelpful(Request $request)
    {
        $most_helpful = DB::table('discussion_replies')
            ->select('users_id', DB::raw('count(*) as total'))
            ->where('created_at', '>=', Carbon::now()->subDays(7))
            ->groupBy('users_id')
            ->orderBy('total', 'desc');

        if ($request->destination_type != '') {
            $destination_type = explode(',', $request->destination_type);

            switch ($destination_type[1]) {
                case 'place':
                    $place = Place::find($destination_type[0]);

                    $plase_ids = $destination_type[0];
                    $city_id = $place->cities_id;
                    $country_id = $place->countries_id;
                    break;
                case 'country':
                    $country = Country::find($destination_type[0]);
                    $country_id = $destination_type[0];
                    $place_ids = Place::where('countries_id', $country_id)->pluck('id')->toArray();
                    $city_id = Cities::where('countries_id', $country_id)->pluck('id')->toArray();
                    break;
                case 'city':
                    $city = Cities::find($destination_type[0]);
                    $city_id = $destination_type[0];
                    $place_ids = Place::where('cities_id', $city_id)->pluck('id')->toArray();
                    $country_id = $city->countries_id;

                    break;
            }

            $discussion_list = Discussion::where(function ($pq) use ($place_ids) {
                $pq->where('destination_type', 'Place');
                if (is_array($place_ids)) {
                    $pq->whereIn('destination_id', $place_ids);
                } else {
                    $pq->where('destination_id', $place_ids);
                }
            })
                ->orWhere(function ($ci_q) use ($city_id) {
                    $ci_q->where('destination_type', 'city');
                    if (is_array($city_id)) {
                        $ci_q->whereIn('destination_id', $city_id);
                    } else {
                        $ci_q->where('destination_id', $city_id);
                    }
                })
                ->orWhere(function ($co_q) use ($country_id) {
                    $co_q->where('destination_type', 'country');
                    $co_q->where('destination_id', $country_id);
                })->pluck('id');

            $most_helpful = $most_helpful->whereIn('discussions_id', $discussion_list);
        }

        $most_helpful = $most_helpful->get();

        if (count($most_helpful) > 0) {
            return view('site.discussions.partials._helpful-block', ['most_helpful' => $most_helpful]);
        } else {
            return '';
        }
    }


    public function getTrendingTopic(Request $request)
    {
        $place = $request->destination_place;
        if ($place != '') {
            $place_type = explode(',', $place);

            $trending_topics = Discussion::whereNotNull('topic')->where('destination_type', $place_type[1])->where('destination_id', $place_type[0])->get();
        } else {
            $trending_topics = Discussion::whereNotNull('topic')->where('created_at', '>=', Carbon::now()->subDays(7))->orderBy('created_at', 'desc')->get();
        }

        $topic_list = [];
        $walked_arr = [];
        if (count($trending_topics) > 0) {
            foreach ($trending_topics as $trending_topic) {
                $topics = explode(',', $trending_topic->topic);
                foreach ($topics as $topic) {
                    $topic_low = strtolower($topic);
                    array_walk($walked_arr, function (&$value) {
                        $value = strtolower($value);
                    });

                    if (!in_array($topic_low, $walked_arr)) {
                        $topic_list[] = $walked_arr[] = $topic;
                    }
                }
            }
        }

        if (count($topic_list) > 0) {
            return view('site.discussions.partials._topic-block', ['trending_topics' => $topic_list]);
        } else {
            return '';
        }
    }



    public function getMoreReplies(Request $request)
    {
        $page = $request->pagenum;
        $skip = ($page - 1) * 10;
        $disc_id = $request->discussion_id;
        $return = '';

        $discussion_info = Discussion::find($disc_id);

        $disc_replies = $discussion_info->replies()->orderBy('num_upvotes', 'desc')->skip($skip)->take(10)->get();

        if (count($disc_replies) > 0) {
            foreach ($disc_replies as $disc_reply) {
                $return .= view('site.discussions.partials._reply-block', ['reply' => $disc_reply]);
            }

            return $return;
        } else {
            return '';
        }
    }


    public function postCreate(Request $request, RankingService $rankingService, TranslationService $translationService)
    {

        $validator = Validator::make($request->all(), [
            'question' => 'max:150',
            'description' => 'max:1000'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $pair = $request->disc_pair;
        $user_id = Auth::guard('user')->user()->id;
        $file_lists = getTempFiles($pair);

        $type = 1;

        $destination = @explode(",", $request->get('destination_id'));

        $destination_id = $destination[0];
        $destination_type = $destination[1];

        switch ($destination_type) {
            case 'city':
                $country_id = Cities::find($destination_id)->country->id;
                break;
            case 'place':
                $country_id = Place::find($destination_id)->country->id;
                break;
            default:
                $country_id = $destination_id;
        }

        $question = $request->get('question');
        $description = processString($request->get('description'));

        $disc = new Discussion;
        $disc->users_id = $user_id;
        $disc->type = $type;
        $disc->question = $question;
        $disc->description = $description;
        $disc->destination_id = $destination_id;
        $disc->countries_id = $country_id;
        $disc->destination_type = $destination_type;
        $disc->language_id = $translationService->getLanguageId($question . ' ' . $description);

        if ($disc->save()) {
            $rankingService->addPointsEarners($disc->id, get_class($disc), $user_id);

            if ($request->topics) {
                $topics = explode(',', $request->topics);
                foreach ($topics as $topic) {
                    $disc_topic = new DiscussionTopics();

                    $disc_topic->discussions_id = $disc->id;
                    $disc_topic->topics = $topic;

                    $disc_topic->save();
                }
            }

            if (count($file_lists) > 0) {
                foreach ($file_lists as $k => $file) {
                    $filename = $user_id . '_' . $k . '_' . time() . '_' .  preg_replace("/([#]|[%]|[+]|[$])/", "_", $file[1]);
                    Storage::disk('s3')->put('discussion-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                    $path = S3_BASE_URL . 'discussion-photo/' . $filename;

                    $discussion_media = new DiscussionMedias();
                    $discussion_media->discussion_id = $disc->id;
                    $discussion_media->media_url = $path;
                    $discussion_media->save();

                    $filepath = './assets2/upload_tmp/' . $pair . '_' . $user_id . '_' . $file[1];
                    gc_collect_cycles();
                    @unlink($filepath);
                }
            }

            if ($request->experts) {
                $experts = explode(',', $request->experts);

                foreach ($experts as $expert) {
                    $exp = new DiscussionExperts();
                    $exp->discussion_id =  $disc->id;
                    $exp->expert_id = $expert;

                    if ($exp->save()) {
                        notify($expert, 'ask_experts', $disc->id);
                    }
                }
            }

            log_user_activity('Discussion', 'create', $disc->id);

            return Redirect::back()->with('success', 'Discussion sent successfully!');
        }
    }


    public function postDelete($discussion_id)
    {

        $user_id = Auth::guard('user')->user()->id;
        $disc = Discussion::where('users_id', $user_id)->where('id', $discussion_id)->get()->first();
        if (is_object($disc)) {

            $disc_medias = $disc->medias;

            if ($disc_medias) {
                foreach ($disc_medias as $disc_media) {
                    $disc_media->delete();
                }
            }

            $disc_experts = $disc->experts;

            if ($disc_experts) {
                foreach ($disc_experts as $disc_expert) {
                    $disc_expert->delete();
                }
            }

            $disc_replies = $disc->replies;

            if ($disc_replies) {
                foreach ($disc_replies as $disc_reply) {
                    $disc_reply->delete();
                }
            }
            $disc->forceDelete();
            del_user_activity('Discussion', 'create', $discussion_id, $user_id);
            return 'success';
        } else {
            return 'error';
        }
    }
    public function getEdit($discussionId)
    {
    }

    public function postUpVoteReply(Request $request, RankingService $rankingService)
    {
        $reply_id = $request->get('replies_id');
        $user_id = Auth::guard('user')->user()->id;

        $reply = DiscussionReplies::find($reply_id);
        $author_id = Discussion::find($reply->discussions_id)->users_id;

        if (count($reply->upvotes()->where('users_id', $user_id)->get()) == 0) {
            $replyUpvote = new DiscussionRepliesUpvotes;
            $replyUpvote->discussions_id = $reply->discussions_id;
            $replyUpvote->replies_id = $reply_id;
            $replyUpvote->users_id = $user_id;
            $replyUpvote->save();

            $reply->num_upvotes = count($reply->upvotes);
            $reply->save();

            if (count($reply->downvotes()->where('users_id', $user_id)->get()) != 0) {
                $reply->downvotes()->where('users_id', $user_id)->delete();
            }

            log_user_activity('Discussion', 'upvote', $reply_id);

            if ($user_id != $reply->users_id) {
                notify($reply->users_id, 'discussion_upvote', $reply_id);
            }

            $rankingService->addPointsToEarners($reply);

            return new JsonResponse([
                'status' => 1,
                'count_upvotes' => optimize_counter(count($reply->upvotes)),
                'count_downvotes' => optimize_counter(count($reply->downvotes)),
            ]);
        } else {
            $reply->upvotes()->where('users_id', $user_id)->delete();

            $reply->num_upvotes = count($reply->upvotes);
            $reply->save();

            $rankingService->subPointsFromEarners($reply);

            unnotify($reply->users_id, 'discussion_upvote', $reply_id);

            log_user_activity('Discussion', 'upvote', $reply_id);
            return new JsonResponse([
                'status' => 1,
                'count_upvotes' => optimize_counter(count($reply->upvotes)),
                'count_downvotes' => optimize_counter(count($reply->downvotes)),
            ]);
        }
    }

    public function postDownVoteReply(Request $request)
    {
        $reply_id = $request->get('replies_id');
        $user_id = Auth::guard('user')->user()->id;

        $reply = DiscussionReplies::find($reply_id);
        if (count($reply->downvotes()->where('users_id', $user_id)->get()) == 0) {
            $replyDownvote = new DiscussionRepliesDownvotes();
            $replyDownvote->discussions_id = $reply->discussions_id;
            $replyDownvote->replies_id = $reply_id;
            $replyDownvote->users_id = $user_id;
            $replyDownvote->save();

            $reply->num_downvotes = count($reply->downvotes);
            $reply->save();

            if (count($reply->upvotes()->where('users_id', $user_id)->get()) != 0) {
                $reply->upvotes()->where('users_id', $user_id)->delete();
            }

            log_user_activity('Discussion', 'downvote', $reply_id);
            return new JsonResponse(
                [
                    'status' => 1,
                    'count_upvotes' => optimize_counter(count($reply->upvotes)),
                    'count_downvotes' => optimize_counter(count($reply->downvotes))
                ]
            );
        } else {
            $reply->downvotes()->where('users_id', $user_id)->delete();

            $reply->num_downvotes = count($reply->downvotes);
            $reply->save();

            log_user_activity('Discussion', 'downvote', $reply_id);
            return new JsonResponse(
                [
                    'status' => 1,
                    'count_upvotes' => optimize_counter(count($reply->upvotes)),
                    'count_downvotes' => optimize_counter(count($reply->downvotes))
                ]
            );
        }
    }

    public function postReply(Request $request, RankingService $rankingService, DiscussionsService $discussionsService)
    {
        $discussion_id = $request->get('discussion_id');
        $discussion = Discussion::find($discussion_id);
        $text = $request->get('text');
        $user_id = Auth::guard('user')->user()->id;
        $view = '';
        //$user_id = 1;

        $text = convert_string($text);

        $reply = new DiscussionReplies;
        $reply->discussions_id = $discussion->id;
        $reply->users_id = $user_id;
        $reply->type = $discussion->type;
        $reply->reply = $text;
        $reply->num_upvotes = 0;
        $reply->num_downvotes = 0;
        $reply->num_views = 0;
        $reply->num_follows = 0;
        $reply->medias_type = $request->media_type;
        $reply->medias_url = $request->media_url;
        if ($request->has('parents_id')) {
            $reply->parents_id = $request->parents_id;
        }

        if ($reply->save()) {
            $rankingService->addPointsToEarners($reply->discussion);
            $rankingService->addPointsEarners($reply->id, get_class($reply), $user_id);
            $discussionsService->updatePopularCount($discussion->id);

            $data['error'] = 0;
            $data['reply_id'] = $reply->id;

            $_ = ['reply' => $reply];
            if ($request->has('parents_id')) {
                $_['is_sub'] = true;
            }

            $view .= view('site.discussions.partials._reply-block', $_);
            log_user_activity('Discussion', 'reply', $discussion->id);
            updatePostRanks($discussion->id, CommonConst::TYPE_DISCUSSION, CommonConst::ACTION_REPLY);
        }
        echo $view;
    }

    public function putEditReply(Request $request, DiscussionReplies $reply)
    {
        $view = '';
        if ($reply->users_id == Auth::id()) {
            $reply->reply = processString($request->get('text'));
            $reply->medias_url = $request->media_url;
            $reply->medias_type = $request->media_type;
            $reply->save();
            log_user_activity('Discussion', 'edit_reply', $reply->discussion->id);

            $_ = ['reply' => $reply];
            if ($reply->parents_id) {
                $_['is_sub'] = true;
            }

            if (isset($request->is_single_page)) {
                $_['is_single_page'] = true;
            }


            $view .= view('site.discussions.partials._reply-block', $_);
        }

        echo $view;
    }

    public function deleteReply(Request $request, DiscussionReplies $reply, DiscussionsService $discussionsService)
    {
        if ($reply->users_id !== Auth::id()) {
            $data['error'] = 1;
        } else {
            $reply->delete();

            $discussionsService->updatePopularCount($reply->discussion->id, false);

            $data['error'] = 0;
            log_user_activity('Discussion', 'delete_reply', $reply->discussion->id);
            updatePostRanks($reply->discussion->id, CommonConst::TYPE_DISCUSSION, CommonConst::ACTION_DELETE_REPLY);
        }

        return new JsonResponse($data);
    }

    /*Add count of post Views*/

    public function postDiscussionViewCount(Request $request)
    {
        if (Auth::check()) {
            $user_id = Auth::guard('user')->user()->id;
            $discussion_id = $request->discussion_id;
            if ($discussion_id) {
                $discussion = Discussion::find($discussion_id);
                if (count($discussion->views()->where('users_id', $user_id)->get()) == 0) {
                    $discussion_view = new DiscussionViews;
                    $discussion_view->discussions_id = $discussion_id;
                    $discussion_view->users_id = $user_id;

                    $discussion_view->save();

                    //log_user_activity('Discussion', 'view', $discussion_id);
                    return json_encode(array('status' => 1, 'count_views' => $discussion->views()->count()));
                } else {
                    return json_encode(array('status' => 2));
                }
            }
        }
    }

    public function getFilterAnswers(Request $request)
    {
        $responce = '';
        if (!empty($request->discussion_id)) {
            $filter_type = $request->type;
            $discussion = Discussion::find($request->discussion_id);
            if ($discussion) {
                switch ($filter_type) {
                    case 'top_first':
                        foreach ($discussion->replies()->where('parents_id', 0)->orderBy('num_upvotes', 'desc')->get() as $reply) {
                            $responce .= view('site.discussions.partials._reply-block', ['reply' => $reply]);
                        }
                        break;
                    case 'new_first':
                        foreach ($discussion->replies()->where('parents_id', 0)->orderBy('created_at', 'desc')->get() as $reply) {
                            $responce .= view('site.discussions.partials._reply-block', ['reply' => $reply]);
                        }
                        break;
                    case 'old_first':
                        foreach ($discussion->replies()->where('parents_id', 0)->orderBy('created_at', 'asc')->get() as $reply) {
                            $responce .= view('site.discussions.partials._reply-block', ['reply' => $reply]);
                        }
                        break;
                }
            }
        }
        return json_encode($responce);
    }


    public function postAjaxSearchDestination(Request $request)
    {

        $query = $request->get('q');
        if ($query) {
            $res = file_get_contents('https://srv.wego.com/places/search?query=' . $query);
            $results = json_decode($res);
            $cities = array();
            $i = 0;
            foreach ($results as $result) {
                if ($result->type == "city") {
                    $cities[$i]['id'] = $result->code;
                    $cities[$i]['text'] = $result->code . " - " . $result->name;
                    $i++;
                }
            }
            return json_encode(array('results' => $cities));
        }
    }

    protected function _orderTitle($str)
    {
        $title_arr = array(
            'dateasc' => trans('discussion.strings.date_down_up'),
            'datedesc' => trans('discussion.strings.date_up_down'),
            'liked' => trans('discussion.strings.most_liked'),
            'commented' => trans('discussion.strings.most_commented')
        );

        return $title_arr[$str];
    }


    public function postUpDownVotes(Request $request, DiscussionVotesService $discussionVotesService)
    {
        $user_id = Auth::user()->id;
        $discussion_id = $request->discussion_id;
        $type = $request->type;
        $is_add = $request->is_add;

        $discussion = Discussion::find($discussion_id);

        if ($discussionVotesService->setVotes($discussion_id, $type, $user_id, $is_add)) {
            return new JsonResponse(
                [
                    'status' => 1,
                    'count_upvotes' => optimize_counter(count($discussion->discussion_upvotes)),
                    'count_downvotes' => optimize_counter(count($discussion->discussion_downvotes))
                ]
            );
        }
    }

    public function postLikeUnlike(Request $request)
    {

        $post_id = $request->post_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = Discussion::find($post_id)->users_id;
        $check_like_exists = DiscussionLikes::where('discussions_id', $post_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            //dd($check_like_exists);
            $check_like_exists->delete();
            log_user_activity('Discussion', 'unlike', $post_id);
            //notify($author_id, 'status_unlike', $post_id);

            $data['status'] = 'no';
        } else {
            $like = new DiscussionLikes;
            $like->discussions_id = $post_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Discussion', 'like', $post_id);
            notify($author_id, 'status_like', $post_id);

            $data['status'] = 'yes';
        }
        $data['count'] = count(DiscussionLikes::where('discussions_id', $post_id)->get());
        return json_encode($data);
    }

    public function postShareUnshare(Request $request)
    {

        $post_id = $request->post_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = Discussion::find($post_id)->users_id;
        $check_share_exists = DiscussionShares::where('discussions_id', $post_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_share_exists)) {
            $shareid = $check_share_exists->id;
            $check_share_exists->delete();
            log_user_activity('Discussion', 'unshare', $post_id);
            // notify($author_id, 'status_unlike', $post_id);

            $data['status'] = 'no';
        } else {
            $share = new DiscussionShares;
            $share->discussions_id = $post_id;
            $share->users_id = $user_id;
            $share->save();

            log_user_activity('Discussion', 'share', $post_id);
            // notify($author_id, 'status_share', $post_id);

            $data['status'] = 'yes';
        }

        $data['count'] = count(DiscussionShares::where('discussions_id', $post_id)->get());
        return json_encode($data);
    }



    public function uploadRepliesMedia(Request $request)
    {
        $replies_media = $request->replies_media;
        $type = $request->type;
        $filename = time() . '-replies-media.' . $replies_media->getClientOriginalExtension();


        Storage::disk('s3')->putFileAs(
            'replies_media/',
            $replies_media,
            $filename,
            'public'
        );

        $media_url = 'https://s3.amazonaws.com/travooo-images2/replies_media/' . $filename;


        return new JsonResponse(['type' => $type, 'replies_media' => $media_url]);
    }




    public function postAjaxReply(Request $request)
    {
        $discussion_id = $request->get('discussion_id');
        $discussion = Discussion::find($discussion_id);
        $text = processString($request->get('text'));
        $user_id = Auth::guard('user')->user()->id;
        //$user_id = 1;

        $reply = new DiscussionReplies;
        $reply->discussions_id = $discussion->id;
        $reply->users_id = $user_id;
        $reply->type = $discussion->type;
        $reply->reply = $text;
        $reply->num_upvotes = 0;
        $reply->num_downvotes = 0;
        $reply->num_views = 0;
        $reply->num_follows = 0;

        $str = "";
        if ($reply->save()) {

            if ($reply->type == 1)
                $str .= '<h3 class="post__tips-title">' . trans('discussion.strings.top_recommendations_by_users') . '</h3>';
            else if ($reply->type == 2)
                $str .= '<h3 class="post__tips-title">' . trans('discussion.strings.top_tips_by_users') . '</h3>';
            else if ($reply->type == 3)
                $str .= '<h3 class="post__tips-title">' . trans('discussion.strings.top_planning_tips_by_users') . '</h3>';
            else if ($reply->type == 4)
                $str .= '<h3 class="post__tips-title">' . trans('discussion.strings.top_answers_by_users') . '</h3>';

            $str .= '<div class="post__tip">
                    <div class="post__tip-main">
                        <img class="post__tip-avatar" src="' . check_profile_picture($reply->author->profile_picture) . '" alt="" role="presentation">
                        <div class="post__tip-content">
                            <div class="post__tip-header">
                                <a href="#">' . $reply->author->name . '</a> said<span class="post__tip-posted">' . diffForHumans($reply->created_at) . '</span>
                            </div>
                            <div class="post__tip-text">' . $reply->reply . '</div>
                        </div>
                    </div>
                    <div class="post__tip-footer">
                        <button class="post__tip-upvotes" type="button" style="outline:none;" reply-id="' . $reply->id . '">
                            <svg class="icon icon--angle-up-solid">
                                <use xlink:href="' . asset('assets3/img/sprite.svg#angle-up-solid') . '"></use>
                            </svg>
                            <span><strong>' . @count($reply->upvotes) . '</strong>  Upvotes</span>
                        </button>';

            if (@count($discussion->replies) > 1) :
                $str .= '<button class="post__tip-more" type="button" style="outline:none;" onclick="discuss_loadmore(this)">
                                <div class="user-list">
                                    <div class="user-list__item">';
                $count = 0;
                foreach ($discussion->replies as $rep) :
                    if ($count == 3)
                        break;
                    $str .= '<div class="user-list__user"><img class="user-list__avatar" src="' . check_profile_picture($rep->author->profile_picture) . '" alt="" role="presentation"></div>';
                    $count++;
                endforeach;
                $str .= '</div>
                                </div>
                                <span><strong>' . (count($discussion->replies) - 1) . '</strong> more tips</span>
                            </button>';
            endif;
            $str .= '</div>
                </div>';

            log_user_activity('Discussion', 'reply', $reply->id);
        } else {
            $str = "error";
        }

        echo $str;
    }

    /**
     * @param Request $request
     * @param DiscussionReplies $reply
     * @param SpamsRepliesService $spamsRepliesService
     * @return JsonResponse
     */
    public function reportSpam(Request $request, DiscussionReplies $reply, SpamsRepliesService $spamsRepliesService)
    {
        if (!$reply) {
            return new JsonResponse("Wrong reply id", 400);
        }

        $type = $request->input('type');
        $text = $request->input('text');

        try {
            $spamsRepliesService->report($reply, $type, $text);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new JsonResponse("Wrong report type", 400);
        }

        return new JsonResponse("Thanks for your report.");
    }



    public function ajaxGetDiscussion(Request $request)
    {
        $query = $request->get('q');

        if ($query) {
            $locations = array();
            $queryParam = $query;
            $get_countries = Country::whereHas('transsingle', function ($query) use ($queryParam) {
                $query->where('title', 'like', "%" . $queryParam . "%");
            })->take(20)->get();
            foreach ($get_countries as $c) {
                $locations['info'][] = [
                    'id' => $c->id . ',country',
                    'text' => $c->transsingle->title,
                    'image' => check_country_photo(@$c->getMedias[0]->url, 180),
                    'type' => 'Country in ' . @$c->region->transsingle->title,
                    'select_type' => 'country',
                    'query' => $query
                ];
            }

            $countries_count = count($get_countries);

            $get_cities = Cities::whereHas('transsingle', function ($query) use ($queryParam) {
                $query->where('title', 'like', "%" . $queryParam . "%");
            })->take(20)->get();
            foreach ($get_cities as $city) {
                $locations['info'][] = [
                    'id' => $city->id . ',city',
                    'text' => $city->transsingle->title,
                    'image' => check_country_photo(@$city->getMedias[0]->url, 180),
                    'type' => 'City in ' . $city->country->transsingle->title,
                    'select_type' => 'city',
                    'query' => $query
                ];
            }

            $cities_count = count($get_cities);

            $request->lat = 37.5442706;
            $request->lng = -4.727752;

            $home_cnt = new HomeController();
            $places = $home_cnt->getSearchPOIs($request);
            $get_places = json_decode($places);
            $places_count = 0;


            if (count($get_places) > 0 && is_object($get_places[0])) {
                foreach ($get_places as $place) {
                    $locations['info'][] = [
                        'id' => @$place->id . ',place',
                        'text' => @$place->title,
                        'image' => isset($place->image) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->image : asset('assets2/image/placeholders/place.png'),
                        'type' => 'Place',
                        'select_type' => 'place',
                        'query' => $query
                    ];
                }

                $places_count = count($get_places);
            }

            $locations['count_info'] = [
                'all_count' => $places_count + $cities_count + $countries_count,
                'countries_count' => $countries_count,
                'cities_count' => $cities_count,
                'place_count' => $places_count,
            ];

            return json_encode($locations);
        }
    }


    public function getExperts(Request $request)
    {

        $queryParam = $request->get('q');
        $experts = [];
        $exp_list = [];
        $existing_users = [];
        if ($request->type) {
            $destination_type = explode(',', $request->type);

            switch ($destination_type[1]) {
                case 'place':
                    $place = Place::find($destination_type[0]);
                    $exp_countries = ExpertsCountries::with('user')->whereHas('user', function ($query) use ($queryParam) {
                        $query->where('name', 'like', "%" . $queryParam . "%");
                        $query->where('id', '!=', Auth::guard('user')->user()->id);
                    })->where('countries_id', $place->countries_id)->take(20)->get();

                    $exp_cities = ExpertsCities::with('user')->whereHas('user', function ($query) use ($queryParam) {
                        $query->where('name', 'like', "%" . $queryParam . "%");
                        $query->where('id', '!=', Auth::guard('user')->user()->id);
                    })->where('cities_id', $place->cities_id)->take(20)->get();
                    $exp_list =  $exp_cities->toBase()->merge($exp_countries);

                    break;
                case 'country':
                    $cities_id = Cities::where('countries_id', $destination_type[0])->pluck('id');

                    $exp_countries = ExpertsCountries::with('user')->whereHas('user', function ($query) use ($queryParam) {
                        $query->where('name', 'like', "%" . $queryParam . "%");
                        $query->where('id', '!=', Auth::guard('user')->user()->id);
                    })->where('countries_id', $destination_type[0])->take(20)->get();

                    if (count($cities_id) > 0) {
                        $exp_cities = ExpertsCities::with('user')->whereHas('user', function ($query) use ($queryParam) {
                            $query->where('name', 'like', "%" . $queryParam . "%");
                            $query->where('id', '!=', Auth::guard('user')->user()->id);
                        })->whereIn('cities_id', $cities_id)->take(20)->get();

                        $exp_list =  $exp_cities->toBase()->merge($exp_countries);
                    } else {
                        $exp_list = $exp_countries;
                    }
                    break;
                case 'city':
                    $city = Cities::find($destination_type[0]);
                    $country_id = $city ? $city->countries_id : '';

                    $exp_cities = ExpertsCities::with('user')->whereHas('user', function ($query) use ($queryParam) {
                        $query->where('name', 'like', "%" . $queryParam . "%");
                        $query->where('id', '!=', Auth::guard('user')->user()->id);
                    })->where('cities_id', $destination_type[0])->take(20)->get();

                    if ($country_id != '') {
                        $exp_countries = ExpertsCountries::with('user')->whereHas('user', function ($query) use ($queryParam) {
                            $query->where('name', 'like', "%" . $queryParam . "%");
                            $query->where('id', '!=', Auth::guard('user')->user()->id);
                        })->where('countries_id', $country_id)->take(20)->get();

                        $exp_list =  $exp_countries->toBase()->merge($exp_cities);
                    } else {
                        $exp_list = $exp_cities;
                    }
                    break;
            }
        }

        foreach ($exp_list as $exp) {
            if (!in_array($exp->user->id, $existing_users) && !user_access_denied($exp->user->id, true)) {
                $experts[] = [
                    'id' => $exp->user->id,
                    'text' => $exp->user->name,
                    'query' => $queryParam,
                    'image' => check_profile_picture($exp->user->profile_picture),
                    'ask_count' => DiscussionReplies::where('users_id', $exp->users_id)->count(),
                    'title' => rtrim($this->_getUserExpertiseList($exp->user->id), ', ')
                ];

                $existing_users[] =  $exp->user->id;
            }
        }


        return new JsonResponse($experts);
    }



    protected function _getUserExpertiseList($userId)
    {
        $expertise = '';

        $cities = ExpertsCities::with('cities')->where('users_id', $userId)->get();
        $countries = ExpertsCountries::with('countries')->where('users_id', $userId)->get();
        if (count($cities) > 0) {
            foreach ($cities as $city) {
                $expertise .= $city->cities->transsingle->title . ', ';
            }
        }

        if (count($countries) > 0) {
            foreach ($countries as $country) {
                $expertise .= $country->countries->transsingle->title . ', ';
            }
        }

        return $expertise;
    }

    public function postUpDownvoteDiscussion(Request $request)
    {
        $existing = DiscussionLikes::where(['discussions_id' => $request->discussion_id, 'users_id' => Auth::user()->id])->first();

        if ($existing) {
            $existing->delete();
        }
        $newFeed = new DiscussionLikes;
        $newFeed->discussions_id = $request->discussion_id;
        $newFeed->users_id = Auth::user()->id;
        $newFeed->vote = ($request->type == 'up') ? 1 : 0;
        $newFeed->save();
        return json_encode(['count_upvotes' => DiscussionLikes::where(['discussions_id' => $request->discussion_id, 'vote' => 1])->count(), 'count_downvotes' => DiscussionLikes::where(['discussions_id' => $request->discussion_id, 'vote' => 0])->count()]);
    }
}
