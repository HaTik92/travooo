<?php

namespace App\Http\Controllers;

use App\Http\Responses\AjaxResponse;
use App\Services\Checkins\CheckinsService;

class CheckinsController
{
    /**
     * @param $id
     * @param CheckinsService $checkinsService
     * @return \Illuminate\Http\Response
     */
    public function getPlaceCheckins($id, CheckinsService $checkinsService)
    {
        $checkins = $checkinsService->getFriendsAndFollowingsCheckins([$id]);
        $checkinUsers = [
            'friend' => [],
            'following' => []
        ];

        foreach ($checkins as $checkin) {
            if (!$checkin->user) {
                continue;
            }

            $checkinUsers[$checkin->person_type][] = [
                'id' => $checkin->users_id,
                'name' => $checkin->user->name,
                'src' => check_profile_picture($checkin->user->profile_picture)
            ];
        }

        return AjaxResponse::create($checkinUsers);
    }
}