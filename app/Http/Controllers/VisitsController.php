<?php

namespace App\Http\Controllers;

use App\Http\Responses\AjaxResponse;
use App\Services\Checkins\CheckinsService;
use App\Services\Visits\VisitsService;
use Illuminate\Http\Request;

class VisitsController
{
    public function getVisitsThumbs($id, Request $request, VisitsService $visitsService)
    {
        $locationType = $request->get('location-type');
        return AjaxResponse::create($visitsService->getRelatedVisits([$id], false, null, $locationType));
    }

    public function getPlaceVisitsCount($id, Request $request, VisitsService $visitsService)
    {
        $locationType = $request->get('location-type');
        $visits = $visitsService->getRelatedVisits([$id], false, null, $locationType);

        $data = [
            'follower' => $visits->where('person_type', 'follower')->count(),
            'friend' => $visits->where('person_type', 'friend')->count(),
            'expert' => $visits->where('person_type', 'expert')->count(),
            'other' => $visits->where('person_type', 'other')->count()
        ];

        return AjaxResponse::create($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @param VisitsService $visitsService
     * @return \Illuminate\Http\Response
     */
    public function getPlaceVisits(Request $request, $id, VisitsService $visitsService)
    {
        $type = $request->get('type');
        $locationType = $request->get('location-type');

        $visits = $visitsService->getRelatedVisits([$id], true, $type, $locationType);
        $visitUsers = [
            'friend' => [],
            'follower' => [],
            'expert' => [],
            'other' => []
        ];

        foreach ($visits as $visit) {
            if (!$visit->author) {
                continue;
            }

            $visitUsers[$visit->person_type][] = [
                'id' => $visit->users_id,
                'name' => $visit->author->name,
                'src' => check_profile_picture($visit->author->profile_picture),
                'visit' => diffForHumans($visit->time),
                'plan_id' => $visit->id,
                'trip_place_id' => $visit->trip_place_id
            ];
        }

        return AjaxResponse::create($visitUsers);
    }
}