<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Socialite\SocialiteService;

class SocialAuthGoogleController extends Controller
{
    /**
     * Redirect the user to the social network authentication page.
     * @param String $provider
     * @return \Illuminate\Http\JsonResponse
     */
    public function redirectToProvider(String $provider , SocialiteService $socialiteService)
    {

        return redirect($socialiteService->getRedirectUrlByProvider($provider));
    }

    
    /**
     * @param String $provider
     * @param SocialiteService $socialiteService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback(String $provider ,SocialiteService $socialiteService)
    {
        $result = $socialiteService->loginWithSocialite($provider);
        return redirect($result['redirectUrl']);
    }


}
