<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\SocialLogin;


class SocialAuthTwitterController extends Controller
{
    protected function guard()
    {
        return Auth::guard('user');
    }

    protected $guard = 'user';

    public function redirect()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Return a callback method from twitter api.
     *
     * @return callback URL from twitter
     */
    public function callback(Request $request)
    {
        if (!$request->has('error')) {
            $providerUser = Socialite::driver('twitter')->user();
            $email = $providerUser->email;
            if(!$email){
                return redirect()->to('/login')->with('error', 'Twitter email is required.');
            }
            
            $findUser = User::where(['email' => $email])->first();
            if ($findUser) {
                $user_id = $findUser->id;
                $user = $findUser;
                User::where('email', $email)->update([
                    'login_type' => 2,
                    'social_key' => $providerUser->id,
                ]);

            } else {
                $user = User::create([
                    'email' => $providerUser->email,
                    'name' => $providerUser->name,
                    'password' => Hash::make(rand(1, 10000)),
                    'login_type' => 2,
                    'profile_picture' => $providerUser->avatar,
                    'social_key' => $providerUser->id,
                ]);
                $user_id = $user->id;
            }
            $socialLogin = SocialLogin::where(['provider_id' => $providerUser->id])->first();
            if ($socialLogin) {
                \App\SocialLogin::where('provider_id', $providerUser->id)->update([
                    'user_id' => $user_id,
                    'provider_id' => $providerUser->id,
                    'provider' => 'TW',
                    'token' => $providerUser->token,
                    'avatar' => $providerUser->avatar,
                ]);
            } else {
                \App\SocialLogin::create([
                    'user_id' => $user_id,
                    'provider_id' => $providerUser->id,
                    'provider' => 'TW',
                    'token' => $providerUser->token,
                    'avatar' => $providerUser->avatar,
                ]);
            }
            Auth::guard('user')->login($user);
            return redirect()->to('/home');
        } else {
            return redirect()->to('/login');
        }
    }
}
