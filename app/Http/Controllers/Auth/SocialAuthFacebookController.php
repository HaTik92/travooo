<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\SocialLogin;


class SocialAuthFacebookController extends Controller
{
    protected function guard()
    {
        return Auth::guard('user');
    }

    protected $guard = 'user';

    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback(Request $request)
    {
        if(!$request->has('error')){
            $providerUser = Socialite::driver('facebook')->user();

            $email = $providerUser->email;
            if(!$email){
                return redirect()->to('/login')->with('error', 'Facebook email is required.');
            }
            
            $findUser = User::where(['email' => $email])->first();
             
            if ($findUser) {
               
                $user_id = $findUser->id;
                $user = $findUser;
                $email = $user->email;
                $password = $user->password;
                User::where('email', $email)->update([
                    'login_type' => 1,
                    'social_key' => $providerUser->expiresIn,
                ]);
            } else {
                $password = Hash::make(rand(1, 10000));
                $user = User::create([
                    'email' => $providerUser->email,
                    'name' => $providerUser->name,
                    'password' => $password,
                    'login_type' => 1,
                    'profile_picture' => $providerUser->avatar,
                    'social_key' => $providerUser->id,
                ]);
                $user_id = $user->id;
                $email = $user->email;
            }
            $socialLogin = SocialLogin::where(['provider_id' => $providerUser->id])->first();
            if ($socialLogin) {
                \App\SocialLogin::where('provider_id', $providerUser->id)->update([
                    'user_id' => $user_id,
                    'provider_id' => $providerUser->id,
                    'provider' => 'FB',
                    'token' => $providerUser->token,
                    'avatar' => $providerUser->avatar,
                ]);
            } else {
                \App\SocialLogin::create([
                    'user_id' => $user_id,
                    'provider_id' => $providerUser->id,
                    'provider' => 'FB',
                    'token' => $providerUser->token,
                    'avatar' => $providerUser->avatar,
                ]);
            }
            Auth::guard('user')->login($user);
            return redirect()->to('/home');
        }else{
            return redirect()->to('/login');
        }
    }
}
