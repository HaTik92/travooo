<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Frontend\User\Registration\StepConfirmationEmailRequest;
use App\Http\Requests\Frontend\User\Registration\StepFiveRequest;
use App\Http\Requests\Frontend\User\Registration\StepFourExpertRequest;
use App\Http\Requests\Frontend\User\Registration\StepFiveExpertRequest;
use App\Http\Requests\Frontend\User\Registration\StepFourRequest;
use App\Http\Requests\Frontend\User\Registration\StepOneRequest;
use App\Http\Requests\Frontend\User\Registration\StepSevenRequest;
use App\Http\Requests\Frontend\User\Registration\StepSixRequest;
use App\Http\Requests\Frontend\User\Registration\StepThreeCompanyRequest;
use App\Http\Requests\Frontend\User\Registration\StepThreeRequest;
use App\Http\Requests\Frontend\User\Registration\StepTwoRequest;
use App\Http\Responses\AjaxResponse;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\Place\Place;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Travelstyles\Travelstyles;
use App\Services\Experts\ExpertsService;
use App\Services\Users\ExpertiseService;
use App\Services\Users\FollowingService;
use App\Services\Users\RegistrationService;
use App\Services\Users\TravelStylesService;
use App\Services\Users\LanguagesService;
use App\Services\Users\UsersService;
use App\Tokens\RegistrationToken;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use App\Models\User\UsersTravelStyles;
use App\Models\ExpertsCountries\ExpertsCountries;
use App\Models\ExpertsCities\ExpertsCities;
use Carbon\Carbon;
use App\Models\User\UsersFavourites;
use App\Models\Place\PlaceFollowers;
use App\Models\Destinations\GenericTopPlaces;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    protected function guard()
    {
        return Auth::guard('user');
    }

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $guard = 'user';

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param StepOneRequest $request
     * @param UsersService $usersService
     * @param RegistrationService $registrationService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepOne(StepOneRequest $request, UsersService $usersService, RegistrationService $registrationService, ExpertsService $expertsService)
    {
        $user = $usersService->create($request->onlyValidated(), false);

        if (!$registrationService->sendConfirmationEmail($user)) {
            $usersService->forceDelete($user);

            return AjaxResponse::create(['errors' => [
                'send_mail' => "There's a server error. Please try again later."
            ]], false, 422);
        }

        if (session()->has('current_user_nationality')) {
            session()->forget('current_user_nationality');
        }

        session()->put('current_user_nationality', $request->nationality);

        $usersService->setSteps($user->id, '2');

        $content = [
            'token' => $registrationService->generateRegistrationToken($user, $request->onlyValidated()['password']),
            'is_invited' => $user->type === \App\Models\User\User::TYPE_INVITED_EXPERT,
            'badge' => $expertsService->getPresetBadge($user),
            'user_name' => $user->name
        ];

        return AjaxResponse::create($content);
    }

    /**
     * @param Request $request
     * @param RegistrationService $registrationService
     * @param UsersService $usersService
     * @return \Illuminate\Http\Response
     */
    public function resendConfirmationCode(Request $request, RegistrationService $registrationService, UsersService $usersService)
    {
        $userId = $request->get('user_id');

        $timeout = Cache::get('resend_confirmation_code_' . $userId);

        if ($timeout) {
            return AjaxResponse::create([], false);
        }

        $attributes = [];

        $usersService->generateConfirmationCode($attributes);
        $usersService->update($userId, $attributes);

        $user = $usersService->find($userId);

        $registrationService->sendConfirmationEmail($user);

        Cache::put('resend_confirmation_code_' . $userId, 1, 1);

        return AjaxResponse::create();
    }

    /**
     * @param StepConfirmationEmailRequest $request
     * @param RegistrationService $registrationService
     * @param UsersService $usersService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepConfirmationEmail(StepConfirmationEmailRequest $request, RegistrationService $registrationService, UsersService $usersService)
    {
        $code = $request->get('confirmation_code');
        $userId = $request->get('user_id');

        if (!$registrationService->checkCode($userId, $code)) {
            return AjaxResponse::create([
                'errors' => [
                    'confirmation_code' => [
                        trans('validation.exists', ['attribute' => 'confirmation code'])
                    ]
                ]
            ], false, 422);
        };

        $token = $registrationService->getRegistrationToken($request->get('token'));

        $usersService->create([
            'email'    => $token->getEmail(),
            'password' => $token->getPassword()
        ], false);

        $usersService->setSteps($userId, '3');

        return AjaxResponse::create([], true);
    }

    /**
     * @param StepTwoRequest $request
     * @param UsersService $usersService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepTwo(StepTwoRequest $request, UsersService $usersService)
    {
        $stepTwoResult = $usersService->update($request->get('user_id'), $request->onlyValidated());
        $usersService->setIpAddress($request->get('user_id'), $request->ip());
        $usersService->setSteps($request->get('user_id'), '3');

        return AjaxResponse::create([], $stepTwoResult);
    }

    /**
     * @param StepFourRequest $request
     * @param RegistrationService $registrationService
     * @param FollowingService $followingService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepThree(StepThreeRequest $request, RegistrationService $registrationService, FollowingService $followingService, UsersService $usersService)
    {
        if (!$registrationService->validateStepThreeRequest($request)) {
            return AjaxResponse::create([], false);
        }

        $userId = $request->get('user_id');
        $followingService->setFollowingCities($userId, $request->input('cities', []));
        $followingService->setFollowingCountries($userId, $request->input('countries', []));

        if ($request->get('type') == 1) {
            $usersService->setSteps($request->get('user_id'), '4');
        } else {
            $usersService->setSteps($request->get('user_id'), '5');
        }

        return AjaxResponse::create();
    }


    /**
     * @param StepFourExpertRequest $request
     * @param UsersService $usersService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepFourExpert(StepFourExpertRequest $request, UsersService $usersService, ExpertsService $expertsService)
    {
        $userId = $request->get('user_id');

        $updateResult = $usersService->update($userId, $request->onlyValidated());
        $usersService->setSteps($userId, '4');

        return AjaxResponse::create([], $updateResult);
    }

    /**
     * @param StepFourRequest $request
     * @param RegistrationService $registrationService
     * @param ExpertiseService $expertiseService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepFiveExpert(StepFiveExpertRequest $request, RegistrationService $registrationService, ExpertiseService $expertiseService, UsersService $usersService)
    {
        if (!$registrationService->validateStepFiveExpertRequest($request)) {
            return AjaxResponse::create([], false);
        }

        $userId = $request->get('user_id');
        $expertiseService->setExpertiseCities($userId, $request->input('cities', []));
        $expertiseService->setExpertiseCountries($userId, $request->input('countries', []));
        $expertiseService->setExpertisePlaces($userId, $request->input('places', []));
        $usersService->setSteps($userId, '6');

        return AjaxResponse::create();
    }

    /**
     * @param StepFiveRequest $request
     * @param RegistrationService $registrationService
     * @param ExpertiseService $expertiseService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepSixExpert(StepFiveRequest $request, RegistrationService $registrationService, ExpertiseService $expertiseService, UsersService $usersService)
    {
        if (!$registrationService->validateStepFiveRequest($request)) {
            return AjaxResponse::create([], false);
        }

        $expertiseService->setExpertisePlaces($request->get('user_id'), $request->input('places', []));
        $usersService->setSteps($request->get('user_id'), '7');

        return AjaxResponse::create();
    }

    /**
     * @param StepFourRequest $request
     * @param RegistrationService $registrationService
     * @param FollowingService $followingService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepFour(StepFourRequest $request, RegistrationService $registrationService, FollowingService $followingService, UsersService $usersService)
    {
        if (!$registrationService->validateStepFourRequest($request)) {
            return AjaxResponse::create([], false);
        }

        $followingService->setFollowingPlaces($request->get('user_id'), $request->input('places', []));

        $usersService->setSteps($request->get('user_id'), '5');

        return AjaxResponse::create();
    }

    /**
     * @param StepFiveRequest $request
     * @param RegistrationService $registrationService
     * @param UsersService $usersService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepFive(StepFiveRequest $request, RegistrationService $registrationService, UsersService $usersService)
    {
        if (!$registrationService->validateStepFiveRequest($request)) {
            return AjaxResponse::create([], false);
        }

        $dataChange = [
            'interests' => implode(',', $request->input('interests'))
        ];

        $stepSixResult = $usersService->update($request->get('user_id'), $dataChange);

        if ($request->get('type') == 1) {
            $usersService->setSteps($request->get('user_id'), '6');
        } else {
            $usersService->setSteps($request->get('user_id'), '7');
        }

        return AjaxResponse::create([], $stepSixResult);
    }

    /**
     * @param StepSixRequest $request
     * @param RegistrationService $registrationService
     * @param TravelStylesService $travelStylesService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepSix(StepSixRequest $request, RegistrationService $registrationService, TravelStylesService $travelStylesService, UsersService $usersService)
    {
        if (!$registrationService->validateStepSixRequest($request)) {
            return AjaxResponse::create([], false);
        }

        $travelStylesService->setTravelStyles($request->get('user_id'), $request->input('travel_styles'));

        $usersService->setSteps($request->get('user_id'), '7');

        return AjaxResponse::create();
    }


    /**
     * @param StepSevenRequest $request
     * @param RegistrationService $registrationService
     * @param ExpertiseService $expertiseService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepEightExpert(StepSixRequest $request, RegistrationService $registrationService, TravelStylesService $travelStylesService, UsersService $usersService)
    {
        if (!$registrationService->validateStepSixRequest($request)) {
            return AjaxResponse::create([], false);
        }

        $travelStylesService->setTravelStyles($request->get('user_id'), $request->input('travel_styles'));

        $usersService->setSteps($request->get('user_id'), '8');

        return AjaxResponse::create();
    }


    /**
     * @param Request $request
     * @param LanguagesService $languagesService
     * @param UsersService $usersService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepTen(Request $request, LanguagesService $languagesService, UsersService $usersService)
    {

        $languagesService->addContentLanguages($request->get('user_id'), $request->get('lng_content_list'));

        $usersService->setSteps($request->get('user_id'), '9');

        return AjaxResponse::create();
    }


    /**
     * @param Request $request
     * @param RegistrationService $registrationService
     * @return \Illuminate\Http\Response
     */
    public function registrationStepFinal(Request $request, RegistrationService $registrationService, ExpertsService $expertsService, UsersService $usersService)
    {
        $userId = $request->get('user_id');

        $registrationService->activate($userId);

        $expertsService->addPresetPoints($userId);

        $usersService->setSteps($userId, 'complated');

        return AjaxResponse::create([], $registrationService->loginById($userId));
    }

    /**
     * @param Request $request
     * @param RegistrationService $registrationService
     * @return \Illuminate\Http\Response
     */
    public function registrationUpdateSteps(Request $request, UsersService $usersService, ExpertiseService $expertiseService, TravelStylesService $travelStylesService, FollowingService $followingService)
    {
        $userId = $request->get('user_id');
        $step = $request->get('step');
        $type = $request->get('type');
        $step_type = $request->get('step_type');

        if ($step_type == 'skip') {
            if ($type == 1) {
                switch ($step) {
                    case 4:
                        $followingService->deleteFollowing($userId, 'city');
                        $followingService->deleteFollowing($userId, 'country');
                        break;
                    case 5:
                        $followingService->deleteFollowing($userId, 'place');
                        break;
                    case 6:
                        $usersService->update($request->get('user_id'), ['interests' => NULL]);
                        break;
                    case 7:
                        $travelStylesService->deleteTravelStyles($userId);
                        break;
                }
            } else {
                switch ($step) {
                    case 5:
                        $followingService->deleteFollowing($userId, 'city');
                        $followingService->deleteFollowing($userId, 'country');
                        break;
                    case 6:
                        $expertiseService->deleteExpertiseCities($userId);
                        $expertiseService->deleteExpertiseCountries($userId);
                        $expertiseService->deleteExpertisePlaces($userId);
                        break;
                    case 7:
                        $usersService->update($request->get('user_id'), ['interests' => NULL]);
                        break;
                    case 8:
                        $travelStylesService->deleteTravelStyles($userId);
                        break;
                }
            }
        }

        if ($step == 2) {
            $usersService->update($request->get('user_id'), ['confirmed' => 0]);
        }

        if ($step >= 2 && $step <= 9) {
            $usersService->setSteps($userId, $step);
        }

        return AjaxResponse::create();
    }

    /**
     * @param Request $request
     * @param UsersService $usersService
     * @return \Illuminate\Http\Response
     */
    public function registrationCheckUsername(Request $request, UsersService $usersService)
    {
        $username = $request->get('username');
        $user = $usersService->findByUsername($username);

        if ($user) {
            return AjaxResponse::create(['type' => 'exit'], true);
        } else {
            return AjaxResponse::create(['type' => 'available'], true);
        }
    }


    public function searchCitiesOrCountries(Request $request, UsersService $usersService, RegistrationService $registrationService)
    {
        $search = $request->input('search');
        $token  = $request->input('token');
        $result = [];
        $user_nationality = session()->get('current_user_nationality');

        if ($search && $search != '') {
            $cities = Cities::whereHas('transsingle', function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            })->take(10);
            $countries = Countries::whereHas('transsingle', function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            })->take(10);

            $collection = $cities->get();
            $entities = $collection->concat($countries->get())->shuffle();

            foreach ($entities as $entity) {
                $result[] = [
                    'id' => get_class($entity) === Cities::class ? 'city-' . $entity->getKey() : 'country-' . $entity->getKey(),
                    'type' => 'location',
                    'name' => $entity->transsingle->title,
                    'desc' => get_class($entity) === Cities::class ? $entity->country->transsingle->title : '',
                    'url' => get_class($entity) === Cities::class ? @check_city_photo($entity->getMedias[0]->url, 180) : @check_country_photo($entity->getMedias[0]->url, 180)
                ];
            }
        } else {
            $result = $usersService->getUserSuggestionLocations($user_nationality);
        }


        return AjaxResponse::create($result);
    }


    public function searchAreaExpertise(Request $request, UsersService $usersService, RegistrationService $registrationService)
    {
        $search = $request->input('search');
        $result = [];

        if ($search && $search != '') {

            $cities = Cities::whereHas('transsingle', function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            })->take(10);
            $countries = Countries::whereHas('transsingle', function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            })->take(10);

            $collection = $cities->get();
            $entities = $collection->concat($countries->get())->shuffle();

            foreach ($entities as $entity) {
                $result[] = [
                    'id' => get_class($entity) === Cities::class ? 'city-' . $entity->getKey() : 'country-' . $entity->getKey(),
                    'name' => $entity->transsingle->title,
                    'flag' => get_class($entity) === Cities::class ?  get_country_flag($entity->country) : get_country_flag($entity)
                ];
            }
        }


        return AjaxResponse::create($result);
    }

    public function searchPlaces(Request $request, RegistrationService $registrationService)
    {
        $query = $request->input('search');
        $user_nationality = session()->get('current_user_nationality');

        $fromExternal = false;
        $result = [];

        if ($query != '') {
            $qr = $this->getElSearchResult($query);

            if ($qr == null || empty($qr)) {
                $mapboxLink = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . urlencode($query) . '.json?limit=10&routing=true&access_token=' . config('mapbox.token');
                $mapboxResult = json_decode(file_get_contents($mapboxLink));
                $qr = $mapboxResult->features;
                $fromExternal = true;
            }

            foreach ($qr as $r) {
                if ($fromExternal) {
                    if (Place::where('provider_id', '=', $r->id)->exists()) {
                        $p = Place::where('provider_id', '=', $r->id)->first();

                        $medium['id'] = @$p->id;
                        $medium['type'] = 'place';
                        $medium['name'] = $p->transsingle->title;
                        $medium['desc'] = @$p->country->transsingle->title ? @$p->country->transsingle->title : '';
                        $medium['url']  = @check_place_photo($p->getMedias[0]->url);

                        $result[] = $medium;
                    }
                } else {
                    $medium['id'] = @$r['place_id'];
                    $p = Place::find($medium['id']);
                    $medium['type'] = 'place';
                    $medium['name'] = $r['place_title'];
                    $medium['desc'] = @$p->country->transsingle->title ? @$p->country->transsingle->title : '';

                    if (isset($r['place_media']) && $r['place_media'] != null) {
                        $medium['url'] = $r['place_media'];
                    } else {
                        $medium['url'] = asset('assets2/image/placeholders/place.png');
                    }

                    $result[] = $medium;
                }
            }
        } else {
            $place_followers = collect([]);
            $user_list = User::where('nationality', $user_nationality)->pluck('id');
            if (count($user_list) > 100) {
                $place_followers_list = PlaceFollowers::select('*', DB::raw('count(*) as total'))
                    ->whereHas('place', function ($query) {
                        $query->whereHas('medias');
                    })
                    ->whereIn('users_id', $user_list)->where('created_at', '>=', Carbon::now()->subDays(30))->orderBy('id', 'DESC')->groupBy('places_id')->pluck('total', 'places_id');

                foreach ($place_followers_list as $place_id => $total_count) {
                    if ($total_count >= 100 && count($place_followers) < 10) {
                        $place_followers[] = $place_id;
                    }
                }
            }

            $countries_for_places = isset($request->countries) ? $request->countries : [];
            $cities_for_places = isset($request->cities) ? $request->cities : [];

            if(isset($request->check_follower) && $request->check_follower == 1){
                dd($place_followers);
            }

            if (count($place_followers) < 20) {
                $place_count = 20 - count($place_followers);
                $t1 = microtime(true);
                $places_by_location = PlacesTop::whereNotNull('places_id')->whereNotIn('places_id', $place_followers)
                    ->where(function($_q) use($countries_for_places, $cities_for_places){
                        if(count($countries_for_places) > 0 && count($cities_for_places) > 0){
                            $_q->whereIn('country_id', $countries_for_places);
                            $_q->orWhereIn('city_id', $cities_for_places);
                        }
                        if(count($countries_for_places) > 0 && count($cities_for_places) == 0){
                            $_q->whereIn('country_id', $countries_for_places);
                        }
                        if(count($countries_for_places) == 0 && count($cities_for_places) > 0){
                            $_q->whereIn('country_id', $cities_for_places);
                        }
                    })

                    ->orderBy('rating', 'DESC')->take($place_count)->get()->pluck('places_id');


                $t2 = microtime(true);
                if(isset($request->check_sql) && $request->check_sql == 1){
                    dd($t2-$t1);
                }

                $final_by_location = $place_followers->concat($places_by_location)->shuffle();

                if(isset($request->check_loc) && $request->check_loc == 1){
                    dd($final_by_location);
                }

                if (count($final_by_location) < 20) {
                    $final_place_count = 20 - count($final_by_location);
                    $generic_places = GenericTopPlaces::whereNotIn('places_id', $final_by_location)->take($final_place_count)->pluck('places_id');

                    $places =  $generic_places->concat($final_by_location)->shuffle();

                    if(isset($request->check_gen) && $request->check_gen == 1){
                        dd($generic_places);
                    }
                } else {
                    $places = $final_by_location;
                }
            } else {
                $places = $place_followers->shuffle();
            }

            if(isset($request->place_inf) && $request->place_inf == 1){
                dd($places);
            }

            $places_info = Place::whereHas('medias')->whereIn('id', $places->toArray())->get();

            foreach ($places_info as $place_info) {
                $result[] = [
                    'id' => @$place_info->id,
                    'type' => 'place',
                    'name' => @$place_info->transsingle->title,
                    'desc' => @$place_info->country->transsingle->title ? @$place_info->country->transsingle->title : '',
                    'url' => @check_place_photo($place_info)
                ];
            }
        }

        return AjaxResponse::create($result);
    }


    /**
     * Get suggestion interests.
     *
     * @param  Request  $request
     * @return AjaxResponse
     */
    public function getInterestSuggestion(Request $request)
    {
        $result = [];
        $token = $request->input('token');
        $user_nationality = session()->get('current_user_nationality');

        $user_lists = User::where('nationality', $user_nationality)->where('created_at', '>=', Carbon::now()->subDays(30))->get();

        $list_result = $this->_getInterestList($user_lists);

        if (count($list_result) < 10) {
            $user_lists = User::where('created_at', '>=', Carbon::now()->subDays(30))->get();
            $list_result = $this->_getInterestList($user_lists);
        }

        $result = $list_result;

        if (count($result) < 10) {
            $result = ['Traveling Websites', 'Flights', 'Football', 'Hiking', 'Swimming', 'Photography', 'Cheap Air', 'Condimentum', 'Travel Sites', 'Fringilla Risus'];
        }

        return AjaxResponse::create($result);
    }


    /**
     * Search suggestion languages.
     *
     * @param  Request  $request
     * @return Response
     */
    public function getLanguageSuggestion(Request $request)
    {
        $user_nationality = session()->get('current_user_nationality');
        $suggestion_lng = [];
        $country = Countries::find($user_nationality);

        if ($country) {
            if (@$country->languages) {
                foreach (@$country->languages as $languages) {
                    if ($languages->id != 1) {
                        $suggestion_lng[] = [
                            'id' => $languages->id,
                            'text' => $languages->transsingle->title
                        ];
                    }
                }
            }

            if (@$country->additional_languages) {
                foreach (@$country->additional_languages as $additional_languages) {
                    if ($additional_languages->id != 1) {
                        $suggestion_lng[] = [
                            'id' => $additional_languages->id,
                            'text' => $additional_languages->transsingle->title
                        ];
                    }
                }
            }
        }

        return AjaxResponse::create($suggestion_lng);
    }

    /**
     * Search current languages.
     *
     * @param  Request  $request
     * @return Response
     */
    public function getCurrentLanguage(Request $request)
    {
        $search_query = $request->get('q');
        $lng_list = array();

        if ($search_query != '') {
            $regional_languages = LanguagesSpoken::whereHas('transsingle', function ($q) use ($search_query) {
                $q->where('title', 'like', '%' . $search_query . '%');
            })->take(20)->get();
        } else {
            $regional_languages = LanguagesSpoken::with('transsingle')->take(20)->get()
                ->sortBy(function ($regional_languages) {
                    return $regional_languages->transsingle->title;
                });
        }

        foreach ($regional_languages as $lng) {
            $lng_list[] = [
                'id' => $lng->id,
                'text' => $lng->transsingle->title,
                'query' => $search_query,
            ];
        }
        return json_encode($lng_list);
    }

    public function searchTravelStyles(Request $request)
    {
        $search = $request->input('search');
        $sortDirection = 'asc';

        $styles = Travelstyles::with('transsingle')->whereHas('transsingle', function ($query) use ($sortDirection) {
            $query->orderBy('title', $sortDirection);
        })->limit(20);

        if ($search) {
            $styles->whereHas('transsingle', function ($query) use ($search) {
                $query->where('title', 'like', $search . '%');
            });
        }

        $result = [];
        $other_result = [];

        foreach ($styles->get()->sortBy('transsingle.title', SORT_REGULAR, false) as $entity) {
            $entity_content = [
                'id' => $entity->id,
                'type' => 'travel_style',
                'name' => @$entity->transsingle->title,
                'desc' => '',
                'url' => @check_lifestyles_photo(@$entity->getMedias[0]->url)
            ];
            if (@$entity->transsingle->title != 'Other') {
                $result[] = $entity_content;
            } else {
                $other_result = $entity_content;
            }
        }

        array_push($result, $other_result);

        return AjaxResponse::create($result);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ]);
    }

    protected function step2validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|string',
        ]);
    }


    protected function _getInterestList($user_list)
    {
        $return = [];

        foreach ($user_list as $user) {
            $interests = explode(',', $user->interests);
            foreach ($interests as $interest) {
                if (!in_array($interest, $return) && count($return) < 10 && $interest != '') {
                    $return[] = $interest;
                }
            }
        }

        return $return;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    public function registerstep1(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $user;
    }

    public function registerstep2(Request $request)
    {

        $this->step2validator($request->all())->validate();
        User::where('email', $request->email)->update([
            'name' => $request->username,
            'age' => $request->age,
            'gender' => $request->gender,
            'expert' => $request->expert,
        ]);

        //dd($request->get('expertise'));


        $created_user = User::where('email', $request->email)->first();

        // update new user travelstyles
        $travelstyles_array = $request->travelstyles;

        foreach ($travelstyles_array as $ts) {
            $created_user_travelstyle = new UsersTravelStyles;
            $created_user_travelstyle->users_id = $created_user->id;
            $created_user_travelstyle->conf_lifestyles_id = $ts;
            $created_user_travelstyle->save();
        }

        // update new user expertise countries & cities
        $expertise = $request->get('expertise');
        //  dd(isset($expertise));
        if (isset($expertise) && count($expertise) > 0) {
            foreach ($expertise as $ex) {
                $exp = explode(",", $ex);
                //dd($exp);
                if ($exp[0] == "country") {
                    $new_ex = new ExpertsCountries;
                    $new_ex->users_id = $created_user->id;
                    $new_ex->countries_id = $exp[1];
                    $new_ex->save();
                } elseif ($exp[0] == "city") {
                    $new_ex = new ExpertsCities;
                    $new_ex->users_id = $created_user->id;
                    $new_ex->cities_id = $exp[1];
                    $new_ex->save();
                }
            }
        }

        event(new Registered($user = User::where('email', $request->email)->first()));
        $this->guard()->login($user);
        $this->registered($request, $user);
        return $user;
    }

    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }


    function getElSearchResult($query)
    {
        $curl = curl_init();
        $query = urlencode($query);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/places/_search?q=$query&pretty=true&size=50",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: 7f74e27e-ce46-404b-b0a7-fe5b542d7862",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $response_array = [];
        if ($err) {
            return [];
        } else {
            $response = json_decode($response, true);
            if (isset($response['hits']) && count($response['hits']['hits']) != 0) {
                foreach ($response['hits']['hits'] as $hits) {
                    $response_array[] = $hits['_source'];
                }
                return $response_array;
            } else {
                return [];
            }
        }
    }
}
