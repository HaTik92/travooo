<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function send_ResetLinkEmail(Request $request)
    {
        $validator = Validator::make($this->credentials($request), [
            'email' => 'required_if:username,""|email',
            'username' => 'required_if:email,""|string|min:4|max:15|regex:/^([A-Za-z0-9_])+$/'
        ], [
            'email.required_if'    => 'passwords.required',
            'username.required_if' => 'passwords.required',

            'email.email'          => 'passwords.invalid',
            'username.string'      => 'passwords.invalid',
            'username.min'         => 'passwords.invalid',
            'username.max'         => 'passwords.invalid',
            'username.regex'       => 'passwords.invalid',
        ]);

        if ($validator->fails()) {
            $response = $validator->messages()->first();
        } else {
            $response = $this->broker()->sendResetLink(
                $this->credentials($request)
            );
        }

        return $response == Password::RESET_LINK_SENT
            ? $this->send_ResetLinkResponse($request, $response)
            : $this->send_ResetLinkFailedResponse($request, $response);
    }


    /**
     * Get the response for a successful password reset link.
     *
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function send_ResetLinkResponse(Request $request, $response)
    {
        $email = $request->get('email');

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email = User::where('username', $email)->first()->email;
        }

        return back()->with(['status' => trans($response), 'email' => $email]);
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function send_ResetLinkFailedResponse(Request $request, $response)
    {
        return back()->withErrors(
            ['email' => trans($response)]
        );
    }

    protected function credentials(Request $request)
    {
        $field = filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)
            ? 'email'
            : 'username';

        return [
            $field => $request->get('email'),
            'password' => $request->password
        ];
    }
}
