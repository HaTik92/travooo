<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ActivityMedia\Media;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Aws\S3\S3Client;
use Aws\Credentials\Credentials;
use Illuminate\Support\Facades\Storage;
use App\Models\Place\PlaceMedias;
use Intervention\Image\ImageManagerStatic as Image;

class CronsController extends Controller {
    public function getPlacesMedia($page) {
        $page = intval(str_replace('media', "", $page));
        getPlacesMediaPerCities($page);
    }

    public function getFixLostImages(Request $request) {
        $f = new Filesystem;
        $files = $f->directories('/home/travooo/public_html/storage/app/public/restaurants_media');
        echo count($files) . "<br />";
        //$files = array_slice($files);

        foreach ($files as $file) {
            $provider_id = basename($file);
            echo (string) $provider_id . "<br />";

            $p = \App\Models\Restaurants\Restaurants::where('provider_id', $provider_id)->update(['media_done' => NULL]);
        }
    }

    public function getHotelsMedia($page) {
        $page = intval(str_replace('media', "", $page));
        getHotelsMediaPerCities($page);
    }

    public function getRestaurantsMedia($page) {
        $page = intval(str_replace('media', "", $page));
        getRestaurantsMediaPerCities($page);
    }

    public function getPlacesDetails($field, Request $request) {

        $places_missing_details = \App\Models\Place\Place::whereNull($field)
                ->orderBy('id', 'ASC')
                ->take(100)
                ->get();

        foreach ($places_missing_details AS $pmd) {
            if (time() % 4 == 0) {
                $json = file_get_contents('http://db.travooodev.com/public/places/details/go/' . $pmd->provider_id);
                echo 'http://db.travooodev.com/public/places/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 1) {
                $json = file_get_contents('http://db.travooodev.com/public/places/details/go/' . $pmd->provider_id);
                echo 'http://db.travooodev.com/public/places/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 2) {
                $json = file_get_contents('http://db.travoooapi.com/public/places/details/go/' . $pmd->provider_id);
                echo 'http://db.travoooapi.com/public/places/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 3) {
                $json = file_get_contents('http://db.travoooapi.net/public/places/details/go/' . $pmd->provider_id);
                echo 'http://db.travoooapi.net/public/places/details/go/' . $pmd->provider_id . ' ';
            }

            $details = json_decode($json);
            echo $pmd->provider_id . ' ';

            if (is_object($details)) {
                $types = @join(",", $details->types);
                echo $types;
            }
            if (!isset($types)) {
                $types = '';
            }
            echo '<br />';

            $pmd->place_type = $types;
            $pmd->save();
        }
    }

    public function getEmbassiesDetails($field, Request $request) {

        $places_missing_details = \App\Models\Embassies\Embassies::whereNull($field)
                ->orderBy('id', 'ASC')
                ->take(100)
                ->get();

        foreach ($places_missing_details AS $pmd) {
            if (time() % 4 == 0) {
                $json = file_get_contents('http://db.travooodev.com/public/embassies/details/go/' . $pmd->provider_id);
                echo 'http://db.travooodev.com/public/embassies/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 1) {
                $json = file_get_contents('http://db.travooodev.com/public/embassies/details/go/' . $pmd->provider_id);
                echo 'http://db.travooodev.com/public/embassies/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 2) {
                $json = file_get_contents('http://db.travoooapi.com/public/embassies/details/go/' . $pmd->provider_id);
                echo 'http://db.travoooapi.com/public/embassies/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 3) {
                $json = file_get_contents('http://db.travoooapi.net/public/embassies/details/go/' . $pmd->provider_id);
                echo 'http://db.travoooapi.net/public/embassies/details/go/' . $pmd->provider_id . ' ';
            }

            $details = json_decode($json);
            echo $pmd->provider_id . ' ';

            if (is_object($details)) {
                $types = @join(",", $details->types);
                echo $types;
            }
            if (!isset($types)) {
                $types = '';
            }
            echo '<br />';

            $pmd->place_type = $types;
            $pmd->save();
        }
    }

    public function getHotelsDetails($field, Request $request) {


        $places_missing_details = \App\Models\Hotels\Hotels::whereNull($field)
                ->orderBy('id', 'ASC')
                ->take(100)
                ->get();

        foreach ($places_missing_details AS $pmd) {
            if (time() % 4 == 0) {
                $json = file_get_contents('http://db.travooodev.com/public/hotels/details/go/' . $pmd->provider_id);
                echo 'http://db.travooodev.com/public/hotels/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 1) {
                $json = file_get_contents('http://db.travooodev.com/public/hotels/details/go/' . $pmd->provider_id);
                echo 'http://db.travooodev.com/public/hotels/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 2) {
                $json = file_get_contents('http://db.travoooapi.com/public/hotels/details/go/' . $pmd->provider_id);
                echo 'http://db.travoooapi.com/public/hotels/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 3) {
                $json = file_get_contents('http://db.travoooapi.net/public/hotels/details/go/' . $pmd->provider_id);
                echo 'http://db.travoooapi.net/public/hotels/details/go/' . $pmd->provider_id . ' ';
            }

            $details = json_decode($json);
            echo $pmd->provider_id . ' ';

            if (is_object($details)) {
                $types = @join(",", $details->types);
                echo $types;
            }
            if (!isset($types)) {
                $types = '';
            }
            echo '<br />';

            $pmd->place_type = $types;
            $pmd->save();
        }
    }

    public function getCountryLatLng(Request $request) {


        $places_missing_details = \App\Models\Restaurants\Restaurants::whereNull($field)
                ->orderBy('id', 'ASC')
                ->take(100)
                ->get();

        foreach ($places_missing_details AS $pmd) {
            if (time() % 4 == 0) {
                $json = file_get_contents('http://db.travooodev.com/public/restaurants/details/go/' . $pmd->provider_id);
                echo 'http://db.travooodev.com/public/restaurants/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 1) {
                $json = file_get_contents('http://db.travooodev.com/public/restaurants/details/go/' . $pmd->provider_id);
                echo 'http://db.travooodev.com/public/restaurants/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 2) {
                $json = file_get_contents('http://db.travoooapi.com/public/restaurants/details/go/' . $pmd->provider_id);
                echo 'http://db.travoooapi.com/public/restaurants/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 3) {
                $json = file_get_contents('http://db.travoooapi.net/public/restaurants/details/go/' . $pmd->provider_id);
                echo 'http://db.travoooapi.net/public/restaurants/details/go/' . $pmd->provider_id . ' ';
            }

            $details = json_decode($json);
            echo $pmd->provider_id . ' ';

            if (is_object($details)) {
                $types = @join(",", $details->types);
                echo $types;
            }
            if (!isset($types)) {
                $types = '';
            }
            echo '<br />';

            $pmd->place_type = $types;
            $pmd->save();
        }
    }

    public function getRestaurantsDetails($field, Request $request) {


        $places_missing_details = \App\Models\Restaurants\Restaurants::whereNull($field)
                ->orderBy('id', 'ASC')
                ->take(100)
                ->get();

        foreach ($places_missing_details AS $pmd) {
            if (time() % 4 == 0) {
                $json = file_get_contents('http://db.travooodev.com/public/restaurants/details/go/' . $pmd->provider_id);
                echo 'http://db.travooodev.com/public/restaurants/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 1) {
                $json = file_get_contents('http://db.travooodev.com/public/restaurants/details/go/' . $pmd->provider_id);
                echo 'http://db.travooodev.com/public/restaurants/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 2) {
                $json = file_get_contents('http://db.travoooapi.com/public/restaurants/details/go/' . $pmd->provider_id);
                echo 'http://db.travoooapi.com/public/restaurants/details/go/' . $pmd->provider_id . ' ';
            } elseif (time() % 4 == 3) {
                $json = file_get_contents('http://db.travoooapi.net/public/restaurants/details/go/' . $pmd->provider_id);
                echo 'http://db.travoooapi.net/public/restaurants/details/go/' . $pmd->provider_id . ' ';
            }

            $details = json_decode($json);
            echo $pmd->provider_id . ' ';

            if (is_object($details)) {
                $types = @join(",", $details->types);
                echo $types;
            }
            if (!isset($types)) {
                $types = '';
            }
            echo '<br />';

            $pmd->place_type = $types;
            $pmd->save();
        }
    }

    public function getCreatePlaceThumbs(Request $request) {

        $get_media = Media::where('thumbs_done', 0)->take(10)->get();
        foreach ($get_media AS $media) {
            $complete_url = $media->url;
            $url = explode("/", $complete_url);
            //$data = file_get_contents('https://s3.amazonaws.com/travooo-images2/'.$complete_url);
            //$s3 = App::make('aws')->createClient('s3');

            $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

            $options = [
                'region' => 'us-east-1',
                'version' => 'latest',
                'http' => ['verify' => false],
                'credentials' => $credentials,
                'endpoint' => 'https://s3.amazonaws.com'
            ];

            $s3Client = new S3Client($options);
            //$s3 = AWS::createClient('s3');
            $result = $s3Client->getObject([
                'Bucket' => 'travooo-images2', // REQUIRED
                'Key' => $complete_url, // REQUIRED
                'ResponseContentType' => 'text/plain',
            ]);

            $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            //return $img_700->response('jpg');
            echo $complete_url . '<br />';
            echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

            $put_1100 = $s3Client->putObject([
                'ACL' => 'public-read',
                'Body' => $img_1100->encode(),
                'Bucket' => 'travooo-images2',
                'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
            ]);

            $put_700 = $s3Client->putObject([
                'ACL' => 'public-read',
                'Body' => $img_700->encode(),
                'Bucket' => 'travooo-images2',
                'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
            ]);

            $put_230 = $s3Client->putObject([
                'ACL' => 'public-read',
                'Body' => $img_230->encode(),
                'Bucket' => 'travooo-images2',
                'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
            ]);

            $put_180 = $s3Client->putObject([
                'ACL' => 'public-read',
                'Body' => $img_180->encode(),
                'Bucket' => 'travooo-images2',
                'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
            ]);


            $media->thumbs_done = 1;
            $media->save();
        }
    }

    public function getTopPlaces(Request $request) {

        $places_top = \App\Models\PlacesTop\PlacesTop::where('place_exists', 0)
                ->where('place_added', 0)
                ->where('place_notfound', 0)
                ->take(100)
                ->get();

        foreach ($places_top AS $pt) {
            $find_place = \App\Models\Place\Place::leftJoin('places_trans', 'places.id', '=', 'places_trans.places_id')
                    ->where('places.countries_id', $pt->country_id)
                    ->where('places.cities_id', $pt->city_id)
                    ->where('places_trans.title', $pt->title)
                    ->get()
                    ->first();

            //echo $find_place->id . "<br />";
            if (count($find_place) > 0) {
                ////////////////////// place is already inside our database, just update places_top.places_id and places_top.place_exists
                //dd($find_place);
                $pt->places_id = $find_place->id;
                $pt->place_exists = 1;
                $pt->save();

                echo "Place existed before @ ID " . $find_place->id . "<br />";
            } else {

                ///////////////////// place isnot in our database, let us add it, add its media
                $google_link = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
                        . 'key=AIzaSyDazlrQc906mR7ZpqZeWRI_pER0z1upOPI';
                $google_link .= '&query=' . urlencode($pt->city . " " . $pt->title);
                //var_dump($google_link);

                $fetch_link = file_get_contents($google_link);
                $query_result = json_decode($fetch_link);
                $qr = $query_result->results;

                $r = $qr[0];
                if ($r->place_id) {

                    $final_results = array();

                    $details_link = file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?'
                            . 'key=AIzaSyDazlrQc906mR7ZpqZeWRI_pER0z1upOPI'
                            . '&placeid=' . $r->place_id);

                    $details = json_decode($details_link);
                    $details = $details->result;

                    $r->working_days = @join('<br />', $details->opening_hours->weekday_text);
                    $r->working_times = @join('<br />', $details->opening_hours->weekday_text);
                    if (isset($details->rating))
                        $r->rating = $details->rating;
                    if (isset($details->price_level))
                        $r->price_level = $details->price_level;
                    if (isset($details->international_phone_number))
                        $r->international_phone_number = $details->international_phone_number;
                    if (isset($details->website))
                        $r->website = $details->website;



                    $p = new \App\Models\Place\Place;
                    $p->place_type = @join(',', $r->types);
                    $p->safety_degrees_id = 1;
                    $p->provider_id = $r->place_id;
                    $p->countries_id = $pt->country_id;
                    $p->cities_id = $pt->city_id;
                    $p->lat = $r->geometry->location->lat;
                    $p->lng = $r->geometry->location->lng;
                    $p->rating = $r->rating;
                    $p->active = 1;

                    if ($p->save()) {

                        echo "Place saved @ ID " . $p->id . "<br />";

                        $ptr = new \App\Models\Place\PlaceTranslations;
                        $ptr->languages_id = 1;
                        $ptr->places_id = $p->id;
                        $ptr->title = $r->name;
                        $ptr->address = $r->formatted_address;
                        if (isset($r->phone))
                            $ptr->phone = $r->phone;
                        if (isset($r->website))
                            $ptr->description = $r->website;
                        $ptr->working_days = $r->working_days;
                        //dd($pt);
                        if ($ptr->save()) {
                            echo "Place Translation saved @ ID " . $ptr->id . "<br />";

                            $json = file_get_contents('http://db2.travoooapi.com/public/places/media/go/' . $r->place_id);

                            $response = unserialize($json);
                            $error = $response['error'];
                            $photos = $response['photos'];
                            //var_dump($photos);


                            if (is_array($photos)) {
                                foreach ($photos AS $ph) {

                                    $media_file = 'places/' . $r->place_id . '/' . sha1(microtime()) . '.jpg';
                                    Storage::disk('s3')->put($media_file, $ph['contents'], 'public');

                                    $media = new Media;
                                    $media->url = $media_file;
                                    $media->sha1 = $ph['sha1'];
                                    $media->filesize = $ph['size'];
                                    $media->md5 = $ph['md5'];
                                    $media->html_attributions = join(",", $ph['html_attributions']);
                                    //dd($media);

                                    if ($media->save()) {
                                        echo "Media saved @ ID " . $media->id . "<br />";
                                        $place_media = new PlaceMedias;
                                        $place_media->places_id = $p->id;
                                        $place_media->medias_id = $media->id;
                                        if ($place_media->save()) {
                                            echo "Place Media saved @ ID " . $place_media->id . "<br />";
                                        }
                                    }
                                }
                            }

                            $p->media_done = 1;
                            $p->save();

                            $pt->places_id = $p->id;
                            $pt->place_added = 1;
                            $pt->save();
                        }
                    }
                } else {
                    $pt->place_notfound = 1;
                    $pt->save();
                    echo "Place NOT FOUND<br />";
                }



                //echo '<br />';
            }
            //echo count($find_place) . "<br />";
        }
    }

    //http://db.travoooapi.com/public/places/pluscode/go/ChIJA-mdOLaIJRMRDVHFjx0Gtu0
    public function getPlusCodes(Request $request) {
        $places_missing_pluscode = \App\Models\Place\Place::where('pluscode', '=', '')
                ->where(function ($query) {
                    $query->whereIn('countries_id', array(365, 17, 275, 128, 265, 370, 212))
                    ->orWhereIn('cities_id', array(1428, 54, 810, 1470, 813, 811, 1029));
                })
                ->take(200)
                ->get();


        foreach ($places_missing_pluscode AS $pmp) {

            //dd($pmp);
            $json = file_get_contents('http://db.travoooapi.com/public/places/pluscode/go/' . $pmp->provider_id);

            $details = json_decode($json);

            if (isset($details->status) && $details->status == "NOT_FOUND") {

                echo $pmp->provider_id . ' ';
                echo $details->status;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->status;
                $pmp->save();
            } elseif (isset($details->status) && $details->status == "PLACE_NOT_FOUND") {

                echo $pmp->provider_id . ' ';
                echo $details->status;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->status;
                $pmp->save();
            } else {
                echo $pmp->provider_id . ' ';
                echo $details->compound_code;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->compound_code;
                $pmp->save();
            }
        }
    }
    
    public function getHotelsPlusCodes(Request $request) {
        $places_missing_pluscode = \App\Models\Hotels\Hotels::where('pluscode', '=', '')
                ->where(function ($query) {
                    $query->whereIn('countries_id', array(365, 17, 275, 128, 265, 370, 212))
                    ->orWhereIn('cities_id', array(1428, 54, 810, 1470, 813, 811, 1029));
                })
                
                ->take(200)
                ->get();
                //660
                //dd($places_missing_pluscode);


        foreach ($places_missing_pluscode AS $pmp) {

            //dd($pmp);
            $json = file_get_contents('http://db.travoooapi.com/public/places/pluscode/go/' . $pmp->provider_id);

            $details = json_decode($json);

            if (isset($details->status) && $details->status == "NOT_FOUND") {

                echo $pmp->provider_id . ' ';
                echo $details->status;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->status;
                $pmp->save();
            } elseif (isset($details->status) && $details->status == "PLACE_NOT_FOUND") {

                echo $pmp->provider_id . ' ';
                echo $details->status;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->status;
                $pmp->save();
            } else {
                echo $pmp->provider_id . ' ';
                echo $details->compound_code;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->compound_code;
                $pmp->save();
            }
        }
    }
    
    public function getRestaurantsPlusCodes(Request $request) {
        $places_missing_pluscode = \App\Models\Restaurants\Restaurants::where('pluscode', '=', '')
                ->where(function ($query) {
                    $query->whereIn('countries_id', array(365, 17, 275, 128, 265, 370, 212))
                    ->orWhereIn('cities_id', array(1428, 54, 810, 1470, 813, 811, 1029));
                })
                ->take(200)
                ->get();
                //17000
                //dd($places_missing_pluscode);


        foreach ($places_missing_pluscode AS $pmp) {

            //dd($pmp);
            $json = file_get_contents('http://db.travoooapi.com/public/places/pluscode/go/' . $pmp->provider_id);

            $details = json_decode($json);

            if (isset($details->status) && $details->status == "NOT_FOUND") {

                echo $pmp->provider_id . ' ';
                echo $details->status;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->status;
                $pmp->save();
            } elseif (isset($details->status) && $details->status == "PLACE_NOT_FOUND") {

                echo $pmp->provider_id . ' ';
                echo $details->status;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->status;
                $pmp->save();
            } else {
                echo $pmp->provider_id . ' ';
                echo $details->compound_code;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->compound_code;
                $pmp->save();
            }
        }
    }
    
    public function getEmbassiesPlusCodes(Request $request) {
        $places_missing_pluscode = \App\Models\Embassies\Embassies::where('pluscode', '=', '')
                ->where(function ($query) {
                    $query->whereIn('location_country_id', array(365, 17, 275, 128, 265, 370, 212))
                    ->orWhereIn('cities_id', array(1428, 54, 810, 1470, 813, 811, 1029));
                })
                //->take(200)
                ->get();
                
                dd($places_missing_pluscode);


        foreach ($places_missing_pluscode AS $pmp) {

            //dd($pmp);
            $json = file_get_contents('http://db.travoooapi.com/public/places/pluscode/go/' . $pmp->provider_id);

            $details = json_decode($json);

            if (isset($details->status) && $details->status == "NOT_FOUND") {

                echo $pmp->provider_id . ' ';
                echo $details->status;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->status;
                $pmp->save();
            } elseif (isset($details->status) && $details->status == "PLACE_NOT_FOUND") {

                echo $pmp->provider_id . ' ';
                echo $details->status;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->status;
                $pmp->save();
            } else {
                echo $pmp->provider_id . ' ';
                echo $details->compound_code;
                //dd($details->compound_code);

                echo '<br />';

                $pmp->pluscode = $details->compound_code;
                $pmp->save();
            }
        }
    }

}
