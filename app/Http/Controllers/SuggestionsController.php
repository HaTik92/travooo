<?php

namespace App\Http\Controllers;

use App\Http\Responses\AjaxResponse;
use App\Models\TripPlaces\TripPlaces;
use App\Services\Trips\SuggestionUserLogsService;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SuggestionsController
{
    public function suggest(Request $request, TripsSuggestionsService $tripsSuggestionsService, TripInvitationsService $invitationsService)
    {
        $request->validate([
            'reason' => 'string|max:200|nullable',
            'plan_id' => 'required|numeric',
            'place_id' => 'required|numeric',
            'suggestion_edit_type' => 'required|string',
            'values' => 'required|array',
        ]);

        $planId = $request->get('plan_id');
        $placeId = $request->get('place_id');
        $suggestionEditType = $request->get('suggestion_edit_type');
        $values = $request->get('values');
        $reason = $request->get('reason');

        $userId = auth()->id();

        if (!$userId || !in_array($suggestionEditType, TripsSuggestionsService::SUGGESTION_EDIT_TYPES)) {
            throw new BadRequestHttpException();
        }

        $role = $invitationsService->getUserRole($planId, $userId);

        if (!$role || $role !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]) {
            throw new AccessDeniedHttpException();
        }

        $placeId = $tripsSuggestionsService->getRootTripPlace($placeId, true);

        /** @var TripPlaces $place */
        $place = TripPlaces::query()->where('id', $placeId)->withTrashed()->first();

        if (!$place) {
            throw new ModelNotFoundException();
        }

        if ($tripsSuggestionsService->isDeletedTripPlace($tripsSuggestionsService->getLastTripPlace($placeId))) {
            return AjaxResponse::create([], false, 400);
        }

        $resolver = $tripsSuggestionsService->getSuggestionEditResolver($suggestionEditType, $place, $values);

        $place = $resolver->resolvePlace();

        if ($place->save()) {
            $tripsSuggestionsService->createSuggestion($place->id, $planId, TripsSuggestionsService::SUGGESTION_EDIT_BY_EDITOR_TYPE, $placeId, $reason, $suggestionEditType, false);

            return AjaxResponse::create();
        }

        return AjaxResponse::create([], false, 400);
    }

    public function read(Request $request, TripInvitationsService $invitationsService, TripsSuggestionsService $tripsSuggestionsService, SuggestionUserLogsService $suggestionUserLogsService)
    {
        $planId = $request->get('plan_id');
        $placeId = $request->get('place_id');
        $type = $request->get('type');
        $userId = auth()->id();

        if (!$planId || !$userId || !$placeId || !$type) {
            throw new BadRequestHttpException();
        }

        $role = $invitationsService->getUserRole($planId, $userId);

        if (!$role || ($role !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN] && $role !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR])) {
            throw new AccessDeniedHttpException();
        }

        $suggestions = $tripsSuggestionsService->getPlaceSuggestions($tripsSuggestionsService->getRootTripPlace($placeId, true), $role);

        $readSuggetionsIds = [];

        foreach ($suggestions as $suggestion) {
            if (!isset($suggestion->meta['edit_type']) || $suggestion->meta['edit_type'] !== $type) {
                continue;
            }

            $readSuggetionsIds[] = $suggestion->id;
        }

        $suggestionUserLogsService->read($readSuggetionsIds);

        return new AjaxResponse();
    }

    public function getSuggestions(Request $request, TripInvitationsService $invitationsService, TripsSuggestionsService $tripsSuggestionsService, SuggestionUserLogsService $suggestionUserLogsService)
    {
        $planId = $request->get('plan_id');
        $placeId = $request->get('place_id');
        $userId = auth()->id();

        if (!$planId || !$userId || !$placeId) {
            throw new BadRequestHttpException();
        }

        $role = $invitationsService->getUserRole($planId, $userId);

        if (!$role || ($role !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN] && $role !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR])) {
            throw new AccessDeniedHttpException();
        }

        $suggestions = $tripsSuggestionsService->getPlaceSuggestions($tripsSuggestionsService->getRootTripPlace($placeId, true), $role);

        $result = [];

        $meta = [];

        foreach ($suggestions as $suggestion) {
            if (!isset($suggestion->meta['edit_type'])) {
                continue;
            }

            $type = $suggestion->meta['edit_type'];

            $isUnread = $suggestionUserLogsService->isUnread($suggestion->id);

            $result[$type][] = [
                'suggested_place' => $suggestion->suggestedPlace()->with(['medias', 'medias.media'])->first(),
                'status' => $suggestion->status,
                'id' => $suggestion->id,
                'reason' => $suggestion->reason,
                'date' => diffForHumans($suggestion->created_at),
                'user' => [
                    'name' => $suggestion->user->name,
                    'img' => check_profile_picture($suggestion->user->profile_picture),
                    'role' => $invitationsService->getUserRole($planId, $suggestion->users_id)
                ],
                'unread' => $isUnread
            ];

            if (!isset($meta[$type]['unread_count'])) {
                $meta[$type]['unread_count'] = 0;
            }

            $meta[$type]['unread_count'] += $isUnread;
        }

        return new AjaxResponse([
            'suggestions' => $result,
            'meta' => $meta
        ]);
    }
}


























