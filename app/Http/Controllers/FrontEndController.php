<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontEndController extends Controller
{

    //Return expert landing page
    public function getExperts(){
        return view ('frontend.pages.experts');
    }

    //Return Trip Plan and Trip Planner landing page
    public function getTripPlanner(){
        return view ('frontend.pages.trip-planner');
    }



    public function design(Request $request){
        return view ('site.design.newsfeed');
    }
    public function popups(Request $request){
        return view ('site.design.popups');
    }
    public function pages(Request $request){
        return view ('site.design.pages');
    }
    public function userboard(Request $request){
        return view ('site.design.userboard');
    }
}
