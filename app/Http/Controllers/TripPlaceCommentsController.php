<?php

namespace App\Http\Controllers;

use App\Events\Api\LikeUnLike\TripPlaceCommentLikeUnlike;
use App\Events\Api\Plan\TripPlaceCommentAddedApiEvent;
use App\Events\Api\Plan\TripPlaceCommentChangedApiEvent;
use App\Events\Api\Plan\TripPlaceCommentRemovedApiEvent;
use App\Events\Plan\TripPlaceCommentAddedEvent;
use App\Events\Plan\TripPlaceCommentChangedEvent;
use App\Events\Plan\TripPlaceCommentRemovedEvent;
use App\Http\Responses\AjaxResponse;
use App\Models\ActivityLog\ActivityLog;
use App\Models\ActivityMedia\Media;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlaces\TripPlacesComments;
use App\Models\TripPlaces\TripPlacesCommentsLikes;
use App\Models\TripPlaces\TripPlacesCommentsMedias;
use App\Models\User\User;
use App\Services\Ranking\RankingService;
use App\Services\Trips\TripPlaceCommentsService;
use App\Services\Trips\TripsSuggestionsService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TripPlaceCommentsController extends Controller
{
    /**
     * @param Request $request
     * @param TripPlaceCommentsService $tripPlaceCommentsService
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function create(Request $request, TripPlaceCommentsService $tripPlaceCommentsService)
    {
        $comment = $request->get('comment');
        //todo same trip place id in suggestions
        $tripPlaceId = $request->get('trip_place_id');

        $tripPlaceCommentsService->create($tripPlaceId, $comment);

        return AjaxResponse::create();
    }

    /**
     * @param Request $request
     * @param TripPlaceCommentsService $tripPlaceCommentsService
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function getList(Request $request, TripPlaceCommentsService $tripPlaceCommentsService)
    {
        $tripPlaceId = $request->get('trip_place_id');
        $list = $tripPlaceCommentsService->getList($tripPlaceId, $request->get('per-page', null));

        $data = [
            'meta' => [
                'total' => $list->total(),
                'current_page' => $list->currentPage(),
                'from' => $list->firstItem(),
                'last_page' => $list->lastPage(),
                'next_page_url' => $list->nextPageUrl(),
                'per_page' => $list->perPage(),
                'prev_page_url' => $list->previousPageUrl(),
                'to' => $list->lastItem(),
            ],
            'comments' => []
        ];

        foreach ($list->items() as $comment) {
            $user = User::query()->find($comment->user_id);

            if (!$user) {
                continue;
            }

            $data['comments'][] = [
                'comment' => $comment->comment,
                'user' => [
                    'img' => check_profile_picture($user->profile_picture),
                    'name' => $user->name,
                    'username' => $user->username,
                ],
                'created' => $comment->created_at->diffForHumans()
            ];
        }

        return AjaxResponse::create($data);
    }

    /**
     * @param string $pair
     * @return array
     */
    public function getTempFiles($pair)
    {
        $temp_dir = public_path() . '/assets2/upload_tmp/';
        $auth_user_id = Auth::guard('user')->user()->id;

        $file_lists = [];

        if (is_dir($temp_dir)) {
            if ($handle = opendir($temp_dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }

                    if (strpos($file, $pair . '_' . $auth_user_id) !== FALSE) {
                        $filename_split = explode($pair . '_' . $auth_user_id . '_', $file, 2);
                        $file_lists[] = [$temp_dir . $file, $filename_split[1]];
                    }
                }
                closedir($handle);
            }
        }

        return $file_lists;
    }

    public function postComment(Request $request, $trip_place_id, RankingService $rankingService, TripsSuggestionsService $tripsSuggestionsService)
    {
        $comment = $request->text;
        $comment = is_null($comment) ? '' : $comment;

        $pair = $request->pair;

        $is_files = false;
        $file_lists = [];

        if ($pair) {
            $file_lists = $this->getTempFiles($pair);
            $is_files = count($file_lists) > 0 ? true : false;
        }

        if (!$is_files && $comment == "") {
            return 0;
        }

        $text = convert_string($comment);

        $user_id = Auth::id();
        $user_name = Auth::guard('user')->user()->name;

        $trip_place_id = $tripsSuggestionsService->getRootTripPlace($trip_place_id);

        $new_comment = new TripPlacesComments();
        $new_comment->trip_place_id = $trip_place_id;
        $new_comment->user_id = $user_id;
        $new_comment->reply_to = $request->comment_id ? $request->comment_id : 0;
        $new_comment->comment = $text;

        if ($new_comment->save()) {
            $rankingService->addPointsToEarners($new_comment->tripPlace);

            $uniqueActivityLogData = [
                'users_id' => $user_id,
                'type' => array_last(explode('\\', TripPlacesComments::class)),
                'action' => 'plan_step_comment'
            ];

            $activityLogData = array_merge($uniqueActivityLogData, ['time' => now(), 'variable' => $new_comment->id]);

            ActivityLog::query()->updateOrCreate($uniqueActivityLogData, $activityLogData);

            foreach ($file_lists as $file) {
                $filename = $user_id . '_' . time() . '_' . $file[1];
                Storage::disk('s3')->put('trip-place-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                $path = 'https://s3.amazonaws.com/travooo-images2/trip-place-comment-photo/' . $filename;

                $media = new Media();
                $media->url = $path;
                $media->author_name = $user_name;
                $media->title = $text;
                $media->author_url = '';
                $media->source_url = '';
                $media->license_name = '';
                $media->license_url = '';
                $media->uploaded_at = date('Y-m-d H:i:s');
                $media->users_id = $user_id;
                $media->type  = getMediaTypeByMediaUrl($media->url);
                $media->save();

                $posts_comments_medias = new TripPlacesCommentsMedias();
                $posts_comments_medias->trip_place_comment_id = $new_comment->id;
                $posts_comments_medias->medias_id = $media->id;
                $posts_comments_medias->save();
            }

            $params = [
                'comment' => $new_comment,
                'authUserId' => $user_id,
                'tp' => TripPlaces::find($trip_place_id),
                'auth_picture' => check_profile_picture(auth()->user()->profile_picture)
            ];

            if (!$new_comment->reply_to) {
                try {
                    broadcast(new TripPlaceCommentAddedEvent($new_comment->id));
                } catch (\Throwable $e) {
                }
                try {
                    broadcast(new TripPlaceCommentAddedApiEvent($new_comment->id));
                } catch (\Throwable $e) {
                }
            } else {
                try {
                    broadcast(new TripPlaceCommentChangedEvent($new_comment->reply_to));
                } catch (\Throwable $e) {
                }
                try {
                    broadcast(new TripPlaceCommentChangedApiEvent($new_comment->reply_to));
                } catch (\Throwable $e) {
                }
            }

            if (isset($request->comment_id)) {
                $params['child'] = $new_comment;
                return view('site.trip2.partials.trip_place_comment_reply_block', $params);
            } else {
                return view('site.trip2.partials.trip_place_comment_block', $params);
            }
        }
    }

    public function postCommentReply(Request $request)
    {
        $comment = $request->text;
        $comment = is_null($comment) ? '' : $comment;
        $comment_id = $request->comment_id;
        $tripPlaceId = $request->trip_place_id;
        $pair = $request->pair;

        $file_lists = $this->getTempFiles($pair);
        $is_files = count($file_lists) > 0 ? true : false;

        if (!$is_files && $comment == "") {
            return 0;
        }

        $text = convert_string($comment);
        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;

        $new_comment = new TripPlacesComments();
        $new_comment->trip_place_id = $tripPlaceId;
        $new_comment->user_id = $user_id;
        $new_comment->reply_to = $comment_id;
        $new_comment->comment = $text;

        if ($new_comment->save()) {
            foreach ($file_lists as $file) {
                $filename = $user_id . '_' . time() . '_' . $file[1];
                Storage::disk('s3')->put('trip-place-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                $path = 'https://s3.amazonaws.com/travooo-images2/trip-place-comment-photo/' . $filename;

                $media = new Media();
                $media->url = $path;
                $media->author_name = $user_name;
                $media->title = $text;
                $media->author_url = '';
                $media->source_url = '';
                $media->license_name = '';
                $media->license_url = '';
                $media->uploaded_at = date('Y-m-d H:i:s');
                $media->users_id = $user_id;
                $media->type  = getMediaTypeByMediaUrl($media->url);
                $media->save();

                $posts_comments_medias = new TripPlacesCommentsMedias();
                $posts_comments_medias->trip_place_comment_id = $new_comment->id;
                $posts_comments_medias->medias_id = $media->id;
                $posts_comments_medias->save();
            }

            $params = [
                'child' => $new_comment,
                'authUserId' => $user_id,
                'tp' => TripPlaces::find($tripPlaceId),
                'auth_picture' => check_profile_picture(auth()->user()->profile_picture)
            ];

            return view('site.trip2.partials.trip_place_comment_reply_block', $params);
        }
    }

    public function postCommentEdit(Request $request)
    {
        $pair = $request->pair;
        $tripPlaceId = $request->trip_place_id;
        $comment_id = $request->comment_id;

        $media_comment_edit = $request->text;

        $user_id = Auth::guard('user')->user()->id;
        $user_name = Auth::guard('user')->user()->name;

        $tripPlaceComment = TripPlacesComments::find($comment_id);

        $file_lists = $this->getTempFiles($pair);
        $is_files = count($file_lists) > 0 ? true : false;

        $media_comment_edit = is_null($media_comment_edit) ? '' : $media_comment_edit;

        if (!$is_files && $media_comment_edit == "") {
            return 0;
        }

        $post_object = TripPlaces::find($tripPlaceId);

        $text = convert_string($media_comment_edit);

        if (is_object($post_object)) {
            $tripPlaceComment->comment = $text;
            if ($tripPlaceComment->save()) {
                if (isset($request->existing_medias) && count($tripPlaceComment->medias) > 0) {
                    foreach ($tripPlaceComment->medias as $media_list) {
                        if (in_array($media_list->media->id, $request->existing_medias)) {
                            $medias_exist = Media::find($media_list->medias_id);

                            $media_list->delete();

                            if ($medias_exist) {
                                $amazonefilename = explode('/', $medias_exist->url);
                                Storage::disk('s3')->delete('trip-place-comment-photo/' . end($amazonefilename));

                                $medias_exist->delete();
                            }
                        }
                    }
                }

                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('trip-place-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/trip-place-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $text;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->users_id = $user_id;
                    $media->type  = getMediaTypeByMediaUrl($media->url);
                    $media->save();

                    $posts_comments_medias = new TripPlacesCommentsMedias();
                    $posts_comments_medias->trip_place_comment_id = $tripPlaceComment->id;
                    $posts_comments_medias->medias_id = $media->id;
                    $posts_comments_medias->save();
                }

                $tripPlaceComment->load('medias');

                $params = [
                    'comment' => $tripPlaceComment,
                    'authUserId' => Auth::id(),
                    'tp' => TripPlaces::find($tripPlaceId),
                    'auth_picture' => check_profile_picture(auth()->user()->profile_picture)
                ];

                if ($request->comment_type == 1) {
                    try {
                        broadcast(new TripPlaceCommentChangedEvent($tripPlaceComment->id));
                    } catch (\Throwable $e) {
                    }
                    try {
                        broadcast(new TripPlaceCommentChangedApiEvent($tripPlaceComment->id));
                    } catch (\Throwable $e) {
                    }
                    return view('site.trip2.partials.trip_place_comment_block', $params);
                } else {
                    try {
                        broadcast(new TripPlaceCommentChangedEvent($tripPlaceComment->reply_to));
                    } catch (\Throwable $e) {
                    }
                    try {
                        broadcast(new TripPlaceCommentChangedApiEvent($tripPlaceComment->reply_to));
                    } catch (\Throwable $e) {
                    }
                    $params['child'] = $tripPlaceComment;
                    return view('site.trip2.partials.trip_place_comment_reply_block', $params);
                }
            } else {
                return 0;
            }
        }
    }

    public function postCommentLikeUnlike(Request $request)
    {
        $comment_id = $request->comment_id;
        $user_id = Auth::id();

        $comment = TripPlacesComments::query()->find($comment_id);

        if (!$comment) {
            throw new NotFoundHttpException();
        }

        $check_like_exists = TripPlacesCommentsLikes::where('trip_place_comment_id', $comment_id)->where('user_id', $user_id)->get()->first();
        $data = [];

        if (is_object($check_like_exists)) {
            $check_like_exists->delete();
            $data['status'] = 'no';
        } else {
            $like = new TripPlacesCommentsLikes();
            $like->trip_place_comment_id = $comment_id;
            $like->user_id = $user_id;
            $like->save();

            $data['status'] = 'yes';
        }

        $data['count'] = count(TripPlacesCommentsLikes::where('trip_place_comment_id', $comment_id)->get());
        $data['name'] = Auth::guard('user')->user()->name;

        if ($comment->reply_to) {
            $updateCommentId = $comment->reply_to;
        } else {
            $updateCommentId = $comment_id;
        }


        try {
            broadcast(new TripPlaceCommentChangedEvent($updateCommentId));
        } catch (\Throwable $e) {
        }
        try {
            broadcast(new TripPlaceCommentLikeUnlike($updateCommentId));
        } catch (\Throwable $e) {
        }

        return json_encode($data);
    }

    public function postCommentDelete(Request $request)
    {
        $user_id = Auth::id();
        $comment_id = $request->comment_id;
        $comment = TripPlacesComments::find($comment_id);

        foreach ($comment->medias as $com_media) {
            $medias_exist = Media::find($com_media->medias_id);
            $com_media->delete();

            if ($medias_exist) {
                $amazonefilename = explode('/', $medias_exist->url);
                Storage::disk('s3')->delete('trip-place-comment-photo/' . end($amazonefilename));

                $medias_exist->delete();
            }
        }

        if ($comment->delete()) {
            if ($comment->reply_to) {
                try {
                    broadcast(new TripPlaceCommentChangedEvent($comment->reply_to));
                } catch (\Throwable $e) {
                }
                try {
                    broadcast(new TripPlaceCommentRemovedApiEvent($comment->reply_to, $comment->trip_place_id, $user_id));
                } catch (\Throwable $e) {
                }
            } else {
                try {
                    broadcast(new TripPlaceCommentRemovedEvent($comment->id, $comment->trip_place_id));
                } catch (\Throwable $e) {
                }
                try {
                    broadcast(new TripPlaceCommentRemovedApiEvent($comment->id, $comment->trip_place_id, $user_id));
                } catch (\Throwable $e) {
                }
            }

            return 1;
        } else {
            return 0;
        }
    }

    public function getCommentLikeUsers(Request $request)
    {
        $comment_id = $request->comment_id;
        $comment = TripPlacesComments::find($comment_id);

        if ($comment->likes) {
            return view('site.home.partials.modal_comments_like_block', [
                'likes' => $comment->likes()->orderBy('created_at', 'DESC')->get()
            ]);
        }
    }

    public function getMoreComments(Request $request, TripPlaceCommentsService $tripPlaceCommentsService)
    {
        $page = $request->pagenum;
        $tripPlaceId = $request->trip_place_id;

        return $tripPlaceCommentsService->loadMoreComments($page, $tripPlaceId);
    }

    public function getComment($commentId, TripPlaceCommentsService $tripPlaceCommentsService)
    {
        $comment = TripPlacesComments::query()->find($commentId);

        //todo add access check
        $hasAccess = true;

        if (!$comment || !$hasAccess) {
            throw new ModelNotFoundException();
        }

        return $tripPlaceCommentsService->renderComment($comment);
    }
}
