<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Models\TripPlans\TripPlans;
use App\Models\City\Cities;

class BookController extends Controller {

    public function __construct() {
        $this->middleware('auth:user');
    }

    public function getHotels() {
        $data = array();
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();


        return view('site.book.hotels', $data);
    }
    
    public function getSearchHotels(Request $request) {
        $data = array();
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        


        $results = $this->doSearchHotel($request->all());
        $data['search_id'] = $results->search->id;
        $data['city_name'] = $results->search->city->name;
        
        $data['hotel_city'] = Cities::with('transsingle')->with('medias')
                ->whereHas('transsingle', function ($query) use ($data) {
                    $query->where('title', 'LIKE', '%'.$data['city_name'].'%');
                })->get();
                
                
        
        
        //die();
        //sleep(1);
        
        $results2 = $this->doCompleteSearchHotel($data['search_id']);
        $rates = $results2->rates;
        
        $frates = array();
        foreach($rates AS $rate) {
            $frates[$rate->hotelId][] = $rate;
        }

        
        
        $data['hotels'] = $results->hotels;
        $data['rates'] = $frates;
        
        return view('site.book.search-hotel-results', $data);
    }

    public function postSearchHotels(Request $request) {
        $data = array();
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();


        $results = $this->doSearchHotel($request->all());
        $data['search_id'] = $results->search->id;
        $data['city_name'] = $results->search->city->name;
        
        $data['city'] = Cities::with('transsingle')->with('medias')
                ->whereHas('transsingle', function ($query) use ($data) {
                    $query->where('title', 'LIKE', '%'.$data['city_name'].'%');
                })->get();
                
        
        
        //die();
        //sleep(1);
        
        $results2 = $this->doCompleteSearchHotel($data['search_id']);
        $rates = $results2->rates;
        
        $frates = array();
        foreach($rates AS $rate) {
            $frates[$rate->hotelId][] = $rate;
        }

        
        
        $data['hotels'] = $results->hotels;
        $data['rates'] = $frates;
        
        return view('site.book.search-hotel-results', $data);
    }
    
    public function ajaxCompleteSearchHotels(Request $request) {
        $data = array();
        $search_id = $request->get('search_id');
        
        return $this->doCompleteSearchHotel($search_id);

        
    }

    private function doSearchHotel($input) {
        $drange = explode("-", $input['daterange']);
        
        $date_from = date("Y-n-d", strtotime(trim($drange[0])));
        $date_to = date("Y-n-d", strtotime(trim($drange[1])));

        

        $token = $this->doAPIAuthorization();
        if ($token) {
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => "Bearer $token"]
            ]);

            $response = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                        [
                            'search' => [
                                'locale' => 'en',
                                'cityCode' => $input['city_select'],
                                'siteCode' => 'US',
                                'currencyCode' => 'USD',
                                'guestsCount' => 1,
                                'roomsCount' => 1,
                                'checkIn' => $date_from,
                                'checkOut' => $date_to,
                                'deviceType' => 'desktop'
                            ]
                        ]
                )]
            );

            if ($response->getStatusCode() == 200) {
                $result = json_decode($response->getBody()->getContents());
                return $result;
                //echo $token;
            }
        }
    }
    
    private function doCompleteSearchHotel($search_id) {
        $token = $this->doAPIAuthorization();
        if ($token) {
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => "Bearer $token"]
            ]);

            $response = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                        [
                            'search' => [
                                'id' => $search_id
                            ]
                        ]
                )]
            );

            if ($response->getStatusCode() == 200) {
                $result = json_decode($response->getBody()->getContents());
                return $result;
                //echo $token;
            }
        }
    }

    private function doAPIAuthorization() {
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json', 'X-Wego-Version' => 1]
        ]);

        $response = $client->post('https://srv.wego.com/users/oauth/token', ['body' => json_encode(
                    [
                        'grant_type' => 'client_credentials',
                        'client_id' => 'ac9ac5c6c1c603fb33e61f01',
                        'client_secret' => '4ebe013c772c81b0ff3f764e',
                        'scope' => 'affiliate'
                    ]
            )]
        );

        if ($response->getStatusCode() == 200) {
            $result = json_decode($response->getBody()->getContents());
            $token = $result->access_token;
            //echo $token;
            return $token;
        } else {
            return null;
        }
    }

    public function getFlights() {
        $data = array();
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        return view('site.book.flights', $data);
    }
    
    public function postSearchFlights(Request $request) {
         $data = array();
        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();

        $this->validate($request, [
            'city_from' => 'required',
            'city_to' => 'required',
            'date_1' => 'required',
            'date_2' => 'required',
        ]);

        $results = $this->doSearchFlights($request->all());

        $data['search_id'] = $results->search->id;

        $results2 = $this->doCompleteSearchFlights($data['search_id'], $request->all());


        $data['search_id'] = $results->search->id;
        //$data['flights'] = $results->flights;
        
        
        return view('site.book.search-flight-results', $data);
    }
    
    private function doSearchFlights($input) {


        $token = $this->doAPIAuthorization();
        if ($token) {
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => "Bearer $token"]
            ]);
            
            $legs = array();
            if($input['city_from']) {
                $legs[0] = array(
                    "departureAirportCode" => $input['city_from'],
                    "arrivalCityCode" => $input['city_to'],
                    "outboundDate" => date("Y-m-d", strtotime($input['date_1']))

                    );
                
            }
            if($input['city_to']) {
                $legs[1] = array(
                    "departureAirportCode" => $input['city_to'],
                    "arrivalCityCode" => $input['city_from'],
                    "outboundDate" => date("Y-m-d", strtotime($input['date_2']))

                    );
            }


            $response = $client->post('https://srv.wego.com/metasearch/flights/searches', ['body' => json_encode(
                        [
                            'search' => [
                                "cabin" => "economy",
                                "adultsCount" => 1,
                                "childrenCount" => 0,
                                "infantsCount" => 0,
                                "locale" => "ar",
                                "siteCode" => "SG",
                                "currencyCode" => "SGD",
                                'legs'=> $legs,
                                "deviceType" => "desktop",
                                "appType" => "WEB_APP"

                            ]
                        ]
                )]
            );

            if ($response->getStatusCode() == 200) {
                $result = json_decode($response->getBody()->getContents());
                return $result;
                //echo $token;
            }
        }
    }
    private function doCompleteSearchFlights($search_id, $input) {

        $token = $this->doAPIAuthorization();
        if ($token) {
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => "Bearer $token"]
            ]);



            /*$response = $client->post('https://srv.wego.com/metasearch/flights/searches', ['body' => json_encode(
                        [
                            'search' => [
                                'id' => $search_id,
                            ]
                        ]
                )]
            );*/
            $response = $client->get('https://srv.wego.com/metasearch/flights/searches/'.$search_id."?offset=0");

            if ($response->getStatusCode() == 200) {
                $result = json_decode($response->getBody()->getContents());dd($result);
                return $result;
                //echo $token;
            }
        }
    }

    public function ajaxSearchCity(Request $request) {
        $query = $request->get('q');
        if ($query) {
            $res = file_get_contents('https://srv.wego.com/places/search?query=' . $query);
            $results = json_decode($res);
            $cities = array();
            $i = 0;
            foreach ($results AS $result) {
                if ($result->type == "city") {
                    $cities[$i]['id'] = $result->code;
                    $cities[$i]['text'] = $result->code . " - " . $result->name;
                    $i++;
                }
            }
            return json_encode(array('results' => $cities));
        }
    }

}
