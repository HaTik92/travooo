<?php

namespace App\Http\Controllers;

use App\Events\Api\Plan\PlanDeletedApiEvent;
use App\Events\Api\Plan\TripPlaceStepLikeApiEvent;
use App\Events\Api\PlanLogs\PlanPublicApiEvent;
use App\Events\Plan\PlanDeletedEvent;
use App\Events\Plan\TripPlaceStepLikeEvent;
use App\Events\PlanLogs\PlanPublicEvent;
use App\Exceptions\PlanNotExistsException;
use App\Exceptions\PlanPermissionsException;
use App\Http\Constants\CommonConst;
use App\Http\Responses\AjaxResponse;
use App\Models\Reviews\Reviews;
use App\Models\TripMedias\TripMedias;
use App\Models\TripPlans\PlanActivityLog;
use App\Models\User\UsersMedias;
use App\Services\Checkins\CheckinsService;
use App\Services\PlaceChat\PlaceChatsService;
use App\Services\Places\PlacesService;
use App\Services\Ranking\RankingService;
use App\Services\Translations\TranslationService;
use App\Services\Trends\TrendsService;
use App\Services\Trips\PlanActivityLogService;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsService;
use App\Services\Trips\TripsSuggestionsService;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\TripPlans\TripPlans;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\TripCities\TripCities;
use App\Models\TripPlaces\TripPlaces;
use Illuminate\Support\Facades\View;
use App\Models\Place\Place;
use App\Models\TripCountries\TripCountries;
use App\Models\TripPlans\TripsVersions;
use App\Models\TripPlans\TripsDescriptions;
use App\Models\User\User;
use App\Models\TripPlans\TripsContributionRequests;
use App\Models\TripPlans\TripsContributionCommits;
use App\Models\TripPlans\TripsContributionVersions;
use App\Models\Notifications\Notifications;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Models\PlacesTop\PlacesTop;
use Illuminate\Support\Facades\Storage;
use App\Models\ActivityMedia\Media;
use App\Models\Reports\ReportsDraftInfos;
use App\Models\TripPlans\TripsLikes;
use App\Models\TripPlans\TripsComments;
use App\Models\TripPlans\TripsCommentsMedias;
use App\Models\TripPlans\TripsCommentsLikes;
use App\Models\TripPlans\TripsShares;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\TravelMates\TravelMatesRequestUsers;
use App\Models\Reports\ReportsInfos;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use function Sodium\randombytes_uniform;

class TripController extends Controller
{

    public function __construct()
    {
        //        $this->middleware('auth:user');
    }

    public function getCreateTrip($id, Request $request, TripsSuggestionsService $tripsSuggestionsService, TripInvitationsService $tripInvitationsService, PlaceChatsService $placeChatsService, PlanActivityLogService $planActivityLogService, TripsService $tripService, TrendsService $trendsService)
    {
        $time_start1 = microtime(true);

        $data = array();
        $data['add'] = null;
        $data['add_type'] = null;
        $add_city_action = true;
        $data['trip_id'] = $id;
        if ($request->has('memory') && $request->get('memory') == 1) {
            $data['memory'] = 1;
        }

        if (Auth::check()) {
            $data['me'] = Auth::guard('user')->user();
            $data['me_user'] = User::find($data['me']->id);
        }

        $data['pre_selected_place'] = false;
        if (isset($request->place) && isset($request->city)) {
            $place = place::find($request->place);
            $data['selected_place'] = [
                'id' => $place->id,
                'title' => $place->trans[0]->title,
                'media' => $place->getMedias[0]->url
            ];
            $data['selected_city'] = [
                'id' => $place->city->id,
                'title' => $place->city->trans[0]->title,
                'media' => $place->city->getMedias[0]->url
            ];
            $data['pre_selected_place'] = true;
        }

        $data['is_editor'] = 0;
        $data['open_affiliate_hint'] = 0;
        $data['is_admin'] = 0;
        $data['editable'] = 0;
        $data['is_invited'] = false;
        $data['convertedToMemory'] = 0;
        $data['content'] = '';
        $time_start2 = microtime(true);
        if ($id == 0) {
            if (Auth::check()) {
                $data['goto_first_step'] = true;
                $data['my_version_id'] = 1;
                $data['im_invited'] = false;
                $data['show_accept_buttons'] = false;
            } else {
                return redirect('/login');
            }
        } else {
            $time_start3 = microtime(true);
            $editMode = $request->has('do') && $request->get('do') == 'edit';
            $previewMode = $request->has('do') && $request->get('do') == 'preview';

            if ($previewMode) {
                $data['do'] = 'preview';
            }

            $data['trip'] = TripPlans::find($id);

            if (!$data['trip']) {
                throw new PlanNotExistsException('The plan does not exist', 404);
            }

            $data['convertedToMemory'] = $data['trip']->memory;

            if (user_access_denied($data['trip']->users_id, true)) {
                return redirect('access-denied');
            }

            $invitedRequest = $tripsSuggestionsService->getInvitedUserRequest($id, auth()->id(), false);
            $cantEdit = !$invitedRequest || $invitedRequest->status === TripInvitationsService::STATUS_PENDING || ($invitedRequest->role !== TripInvitationsService::ROLE_EDITOR && $invitedRequest->role !== TripInvitationsService::ROLE_ADMIN);

            if ($cantEdit && $editMode && $data['trip']->users_id !== auth()->id()) {
                return redirect()->route('trip.plan', ['id' => $id]);
            }

            if (Auth::check() && !session()->has('admin_can_visit_private_pages')) {
                if ($data['trip']->privacy === TripsService::PRIVACY_PRIVATE && $data['trip']->users_id !== auth()->id()) {
                    if (!$invitedRequest || $invitedRequest->status === TripInvitationsService::STATUS_REJECTED) {
                        throw new PlanPermissionsException('No permissions', 403);
                    }

                    if (($invitedRequest->role !== TripInvitationsService::ROLE_EDITOR && $invitedRequest->role !== TripInvitationsService::ROLE_ADMIN) && $editMode) {
                        return redirect()->route('trip.plan', ['id' => $id]);
                    }
                } elseif ($data['trip']->privacy === TripsService::PRIVACY_PUBLIC && $data['trip']->users_id !== auth()->id() && $editMode) {
                    if ($cantEdit) {
                        return redirect()->route('trip.plan', ['id' => $id]);
                    }
                } elseif ($data['trip']->active === 0 && $data['trip']->users_id !== auth()->id() && !$editMode) {
                    if (!$invitedRequest) {
                        return redirect('/plan-access-denied');
                    }
                } elseif (Auth::check() && $data['trip']->privacy === TripsService::PRIVACY_FRIENDS && $data['trip']->users_id !== auth()->id()) {
                    $isFriend = in_array($data['trip']->users_id, $tripInvitationsService->findFriends()->toArray());

                    if (!$invitedRequest && !$isFriend || ($editMode && $cantEdit)) {
                        throw new PlanPermissionsException('No permissions', 403);
                    }

                    if ($editMode && $cantEdit) {
                        throw new PlanPermissionsException('No permissions', 403);
                    }
                }

                if (!$data['trip']->active && $data['trip']->users_id !== auth()->id() && (!$invitedRequest || ($invitedRequest->role !== TripInvitationsService::ROLE_EDITOR && $invitedRequest->role !== TripInvitationsService::ROLE_ADMIN))) {
                    throw new PlanPermissionsException('No permissions', 403);
                }
            } elseif (!Auth::check() && ($data['trip']->privacy !== 0 || $data['trip']->active !== 1)) {
                throw new PlanPermissionsException('No permissions', 403);
            }

            $data['show_draft_buttons'] = $tripsSuggestionsService->isUnsavedChanges($id);
            $data['log_unread_count'] = $planActivityLogService->getPlanActivityLogUnreadCount($id);

            $data['is_admin'] = $data['trip']->users_id === auth()->id() || ($invitedRequest && $invitedRequest->status === TripInvitationsService::STATUS_ACCEPTED && $invitedRequest->role === TripInvitationsService::ROLE_ADMIN);
            $data['is_editor'] = $invitedRequest && $invitedRequest->status === TripInvitationsService::STATUS_ACCEPTED && $invitedRequest->role === TripInvitationsService::ROLE_EDITOR;
            $data['is_invited'] = $invitedRequest && $invitedRequest->status === TripInvitationsService::STATUS_ACCEPTED;
            $data['draft_view_mode'] = $invitedRequest && $invitedRequest->status === TripInvitationsService::STATUS_PENDING && ($invitedRequest->role === TripInvitationsService::ROLE_EDITOR || $invitedRequest->role === TripInvitationsService::ROLE_ADMIN);
            $data['show_accept_buttons'] = $invitedRequest && $invitedRequest->status === TripInvitationsService::STATUS_PENDING;

            if ($data['show_accept_buttons']) {
                $data['invite_desc_role'] = $invitedRequest->role;
                $data['invite_id'] = $invitedRequest->id;
                $data['inviter'] = $invitedRequest->author;
            }

            $my_version_id = $data['trip']->versions[0]->id;
            if (Auth::check() && Auth::guard('user')->user()->id != $data['trip']->author->id) {
                $data['im_invited'] = true;
            } else {
                $data['im_invited'] = false;
            }

            $data['my_version_id'] = $my_version_id;
            $ftrip_places = array();
            $map_places = array();
            $trip_places_arr = array();
            $time_start4 = microtime(true);

            $time_start5 = microtime(true);
            $data['trip_places'] = $ftrip_places;

            $editMode = $request->has('do') && $request->get('do') == 'edit';

            if (Auth::check() && $editMode || $data['draft_view_mode']) {

                if ($editMode) {
                    $data['do'] = 'edit';

                    if ($invitedRequest && $invitedRequest->user->expert) {
                        $data['open_affiliate_hint'] = 1;
                    }
                }

                $data['editable'] = 1;
            }

            $trip_places_arr = collect(array_values($trip_places_arr))->sortBy('time')->values()->all();
            $map_places = collect($map_places)->sortBy('time')->toArray();

            if (Auth::check()) {
                foreach ($trip_places_arr as &$tripPlace) {
                    $tripPlace->chat_users = [];
                }
            }

            $data['trip_places_arr'] = $trip_places_arr;
            $data['trip_arr'] = $map_places;

            $data['role'] = $tripInvitationsService->getUserRole($id, auth()->id());
            $time_start6 = microtime(true);
            $data['content'] = $tripsSuggestionsService->getPlanContents($data['editable'], $data['trip_id'], auth()->id(), $previewMode);
            $time_start7 = microtime(true);
        }

        $time_start8 = microtime(true);
        $data['planMember'] = $data['is_invited'] || $data['is_admin'];
        $data['plan_chats'] = [];

        if ($id) {
            $data['plan_chats'] = [
                'data' => [],
                'unread_count' => 0
            ];
        }

        $data['add_data'] = json_encode([]);

        if ((isset($data['do']) && $data['do'] == 'edit' && ($data['is_admin'] || $data['is_editor'])) || $id == 0) {
            $data['add'] = $request->get('add', null);
            $data['add_type'] = $request->get('add-type', null);
            if ($data['add']) {
                switch ($data['add_type']) {
                    case 'place':
                        $data['add_data'] = json_encode($tripService->getPlaceDataById($data['add']));
                        break;
                    case 'city':
                        $data['add_data'] = json_encode($tripService->getCityDataById($data['add']));
                        break;
                    case 'country':
                        $data['add_data'] = json_encode($tripService->getCountryDataById($data['add']));
                        break;
                    default:
                        break;
                }
            }
        }
        $log = [];
        $data['plan_people'] = $tripInvitationsService->getAnotherPlanPeople($id);
        $time_start9 = microtime(true);
        if (isset($editMode) && $editMode && !$data['convertedToMemory']) {
            ini_set('memory_limit', '512M');

            $except = [];

            if (isset($data['trip_places_arr'])) {
                $except = collect($data['trip_places_arr'])->pluck('id');
            }

            $time_start = microtime(true);

            //            \DB::enableQueryLog();
            $data['trends'] = $trendsService->getRecursiveTrendingLocations(2, null, false, $except, []);
            //$log = \DB::getQueryLog();
            $log = ''; //collect($log)->sum('time');

            $time_end = microtime(true);

            $data['trends_time'] = [$trendsService->debugData, $time_end - $time_start];
        } else {
            $data['trends'] = [];
            $data['trends_time'] = 0;
        }

        $data['countries_iso'] = Countries::query()->get()->pluck('id', 'iso_code');
        $time_start10 = microtime(true);

        //      \DB::enableQueryLog();
        $view = view('site.trip2.create', $data)->render();
        //dd(\DB::getQueryLog());
        $time_start11 = microtime(true);

        if ($id) {
            $debugTiming = [
                'time1' => $time_start2 - $time_start1,
                'time2' => $time_start3 - $time_start2,
                'time3' => $time_start4 - $time_start3,
                'time4' => $time_start5 - $time_start4,
                'time5' => $time_start6 - $time_start5,
                'time6' => $time_start7 - $time_start6,
                'time7' => $time_start8 - $time_start7,
                'time8' => $time_start9 - $time_start8,
                'time9' => $time_start10 - $time_start9,
                'time10' => $time_start11 - $time_start10,
                'all_time' => $time_start11 - $time_start1,
                'log' => $log
            ];
            //dd($debugTiming);
            $view .= '<div style="display: none;">' . json_encode($debugTiming) . '</div>';
        }

        return $view;
    }

    public function ajaxGetPlaceById(Request $request, $placeId, TripsService $tripService)
    {
        return AjaxResponse::create($tripService->getPlaceDataById($placeId));
    }

    public function ajaxGetPlanData(Request $request, $planId, TripsService $tripsService)
    {
        return AjaxResponse::create($tripsService->getPlanDataById($planId));
    }

    public function getNearbyHotels(Request $request)
    {
        $latlng = $request->get('latlng');
        $latlng = @explode(",", $latlng);

        $nearby_places = DB::table('places')
            ->where('place_section', 'hotels')
            ->select(DB::raw('id, ((ACOS(SIN(' . $latlng[0] . ' * PI() / 180) * SIN(lat * PI() / 180) + COS(' . $latlng[0] . ' * PI() / 180) * COS(lat * PI() / 180) * COS((' . $latlng[1] . ' - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance'))
            ->havingRaw('distance <= ?', [2])
            ->orderBy('distance', 'ASC')
            ->get();

        $content = '<markers>';

        foreach ($nearby_places as $np) {
            $p = Place::find($np->id);

            if (isset($p->getMedias[0]->url)) {
                if ($p->getMedias[0]->thumbs_done == 1) {
                    $media = 'https://s3.amazonaws.com/travooo-images2/th180/' . $p->getMedias[0]->url;
                } else {
                    $media = 'https://s3.amazonaws.com/travooo-images2/' . $p->getMedias[0]->url;
                }
            } else {
                $media = asset('assets2/image/placeholders/pattern.png');
            }
            $content .= '<marker id="' . $p->id . '" name="' . htmlspecialchars($p->trans[0]->title) . '" address="' . htmlspecialchars($p->trans[0]->address) . '" lat="' . $p->lat . '" lng="' . $p->lng . '" '
                . 'image="' . $media . '" />';
        }

        $content .= '</markers>';

        return response($content, 200, [
            'Content-Type' => 'application/xml'
        ]);
    }

    public function getNearbyPlaces(Request $request, TripInvitationsService $invitationsService, PlacesService $placesService, PlaceChatsService $placeChatsService, CheckinsService $checkinsService)
    {
        $latlng = $request->get('latlng');
        $latlng = @explode(",", $latlng);

        $except = $request->get('except', []);

        $nearbyPlaces = $placesService->getNearbyPlaces($latlng[0], $latlng[1]);

        $checkins = $checkinsService->getFriendsAndFollowingsCheckins($nearbyPlaces->pluck('id'), $except);

        $suggestedPlaces = [];
        $exceptPlaceIds = [];

        foreach ($checkins as $checkin) {
            $place = Place::findOrFail($checkin->place_id);
            $user = User::findOrFail($checkin->users_id);

            if (!isset($suggestedPlaces[$place->id]['unread_count'])) {
                $suggestedPlaces[$place->id]['unread_count'] = 0;
            }

            $suggestedPlaces[$place->id]['place'] = $place;
            $suggestedPlaces[$place->id]['unread_count'] += $placeChatsService->getUnreadMessagesCount($user->getKey());
            $suggestedPlaces[$place->id]['title'] = $place->transsingle->title;
            $suggestedPlaces[$place->id]['src'] = check_place_photo($place);
            $suggestedPlaces[$place->id]['users'][] = [
                'src' => check_profile_picture($user->profile_picture)
            ];

            $exceptPlaceIds[] = $place->id;
        }

        $nearbyPlacesId = $nearbyPlaces->pluck('id');

        $topPlaces = PlacesTop::whereIn('places_id', $nearbyPlacesId)->whereNotIn('places_id', $exceptPlaceIds)->whereNotIn('places_id', $except)->limit(5)->get();
        $topSuggestedPlaces = [];

        foreach ($topPlaces as $topPlace) {
            $place = Place::findOrFail($topPlace->places_id);

            $topSuggestedPlaces[] = [
                'place' => $place,
                'title' => $place->transsingle->title,
                'image' => check_place_photo($place)
            ];
        }

        return $this->jsonResponse([
            'top' => $topSuggestedPlaces,
            'nearby' => array_values($suggestedPlaces)
        ]);
    }

    /**
     * UNUSED
     *
     * @param Request $request
     * @param PlacesService $placesService
     * @return JsonResponse
     */
    public function getNearbyTopPlaces(Request $request, PlacesService $placesService)
    {
        $latlng = $request->get('latlng');
        $latlng = @explode(",", $latlng);

        $nearbyPlacesId = $placesService->getNearbyPlaces($latlng[0], $latlng[1])->pluck('id');

        $topPlaces = PlacesTop::whereIn('places_id', $nearbyPlacesId)->limit(5)->get();
        $topSuggestedPlaces = [];

        foreach ($topPlaces as $topPlace) {
            $place = Place::findOrFail($topPlace->places_id);

            $topSuggestedPlaces[] = [
                'place' => $place,
                'title' => $place->transsingle->title,
                'image' => check_place_photo($place)
            ];
        }

        return $this->jsonResponse($topSuggestedPlaces);
    }

    public function ajaxGetTripPlaceByProviderId(Request $request, TripsService $tripsService)
    {
        $providerId = $request->get('provider_id');
        $placeId = $request->get('place_id');

        if (!$providerId && !$placeId) {
            return AjaxResponse::create([], false, 400);
        }

        $where = ['id' => $placeId];

        if ($providerId) {
            $where = ['provider_id' => $providerId];
        }

        $place = Place::query()->where($where)->with(['city', 'trans', 'city.trans', 'country', 'country.trans'])->first();

        if (!$place) {
            $placeId = $tripsService->addNewPlaceByProviderId($providerId);
            $place = Place::query()->with(['city', 'trans', 'city.trans', 'country', 'country.trans'])->find($placeId);
        }

        $place->src = check_place_photo($place);

        $place->reviews = Reviews::where('places_id', $place->id)->get();
        $place->reviews_avg = Reviews::where('places_id', $place->id)->avg('score');

        return AjaxResponse::create($place);
    }

    /**
     * @param Request $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @return mixed
     */
    public function ajaxGetPlanContents(Request $request, TripsSuggestionsService $tripsSuggestionsService, PlanActivityLogService $planActivityLogService)
    {
        $editable = false;
        $trip_id = $request->get('trip_id');

        $editMode = $request->has('do') && $request->get('do') == 'edit';
        $previewMode = $request->has('do') && $request->get('do') == 'preview';

        $inviteRequest = $tripsSuggestionsService->getInvitedUserRequest($trip_id, auth()->id(), false);

        if ($editMode || ($inviteRequest && $inviteRequest->status === TripInvitationsService::STATUS_PENDING && ($inviteRequest->role === TripInvitationsService::ROLE_EDITOR || $inviteRequest->role === TripInvitationsService::ROLE_ADMIN))) {
            $editable = true;
        }

        $content = $tripsSuggestionsService->getPlanContents($editable, $trip_id, auth()->id(), $previewMode);
        $invited = $tripsSuggestionsService->getInvitedUserRequest($trip_id, auth()->id());

        if (!$editable && $invited && $invited->role === 'viewer') {
            $planActivityLogService->getPlanActivityLogList($trip_id);
        }

        return $content;
    }

    /**
     * @param Request $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return AjaxResponse
     * @throws AuthorizationException
     */
    public function ajaxApproveSuggestion(Request $request, TripsSuggestionsService $tripsSuggestionsService, PlanActivityLogService $planActivityLogService)
    {
        //todo custom request
        $suggestionId = $request->get('suggestion_id');

        $suggestion = $tripsSuggestionsService->approveSuggestion($suggestionId);
        $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_APPROVED_SUGGESTION, $suggestion->trips_id, $suggestion->id, auth()->id());

        return new AjaxResponse();
    }

    /**
     * @param Request $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return AjaxResponse
     * @throws AuthorizationException
     */
    public function ajaxDisapproveSuggestion(Request $request, TripsSuggestionsService $tripsSuggestionsService, PlanActivityLogService $planActivityLogService)
    {
        //todo custom request
        $suggestionId = $request->get('suggestion_id');

        $suggestion = $tripsSuggestionsService->disapproveSuggestion($suggestionId);
        $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_DISAPPROVED_SUGGESTION, $suggestion->trips_id, $suggestion->id, auth()->id());
        $tripsSuggestionsService->dismissSuggestedPlace($suggestion->suggested_trips_places_id);

        return new AjaxResponse();
    }

    public function ajaxGetTripPlace(Request $request, PlanActivityLogService $planActivityLogService, TripsSuggestionsService $tripsSuggestionsService)
    {
        $data = array();

        $place_id = $request->get('place_id');
        $place_id = $tripsSuggestionsService->getLastTripPlace($place_id);

        $tripPlace = TripPlaces::findOrFail($place_id);
        $place = @Place::find($tripPlace->places_id);
        $city = @Cities::find($tripPlace->place->cities_id);
        $tripPlace->dateForPicker = strtotime($tripPlace->date . ' UTC') * 1000;
        $tripPlace->timeForPicker = (new Carbon($tripPlace->time))->format('g:ia');
        $tripPlace->cities_name = $city ? $city->transsingle->title : null;
        $tripPlace->countries_name = Countries::find($tripPlace->countries_id)->transsingle->title;
        @$tripPlace->places_name = Place::find($tripPlace->places_id)->transsingle->title;
        $tripPlace->cities_id = $city->id;

        if ($place) {
            if (!isset($place->getMedias[0])) {
                $tripPlace->places_img = asset('assets2/image/placeholders/pattern.png');
            } elseif ($place->getMedias[0]->thumbs_done == 1) {
                $tripPlace->places_img = 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url;
            } else {
                $tripPlace->places_img = 'https://s3.amazonaws.com/travooo-images2/' . $place->getMedias[0]->url;
            }
        }
        $tripPlace->trip_media = @TripMedias::where('places_id', $place_id)->first()->media->url;

        $tripPlace->original_cities_name = $tripPlace->cities_name;
        $tripPlace->cities_name = $tripPlace->cities_name . ', city in ' . $tripPlace->countries_name;
        $tripPlace->cities_lat = $city ? $city->lat : null;
        $tripPlace->cities_lng = $city ? $city->lng : null;

        $tripPlace->cities_img =  @check_city_photo(Cities::find($tripPlace->cities_id)->getMedias[0]->url, 180);

        $tripPlace->images = TripMedias::where([
            'trips_id' => $tripPlace->trips_id,
            'trip_place_id' => $place_id
        ])
            ->with('media')
            ->get()
            ->unique('medias_id')
            ->values();

        $tripPlace->updated_attributes = $planActivityLogService->getPlaceLogUpdatedAttributes($place_id);

        return json_encode($tripPlace);
    }

    public function ajaxSearchCitiesForSelect(Request $request)
    {
        $query = $request->get('term');
        $return = false;
        $suggestions = Cities::with('trans')
            ->whereHas('trans', function ($q) use ($query) {
                $q->where('title', 'LIKE', '%' . $query . '%');
            })
            ->orderBy('id', 'desc')->get();
        $res = array();
        foreach ($suggestions as $suggestion) {
            $img = @check_city_photo($suggestion->getMedias[0]->url, 180);
            $res[] = [
                'img' => $img,
                'id' => $suggestion->id,
                'text' => $suggestion->transsingle->title,
                'country' => 'city in ' . $suggestion->country->transsingle->title,
                'lat' => $suggestion->lat,
                'lng' => $suggestion->lng
            ];
        }
        $country_suggestions = Countries::with('trans')
            ->whereHas('trans', function ($q) use ($query) {
                $q->where('title', 'LIKE', '%' . $query . '%');
            })->first();

        $language_id = 1;
        if (isset($country_suggestions) && is_object($country_suggestions)) {
            $additional_suggestion =  Cities::with([
                'trans' => function ($query) use ($language_id) {
                    $query->where('languages_id', $language_id);
                },
                'top'
            ])
                ->where('countries_id', $country_suggestions->id)->get();
            foreach ($additional_suggestion as $suggestion) {
                $img = @check_city_photo($suggestion->getMedias[0]->url, 180);
                $res[(int) isset($suggestion->top->no_of_reviews) ? $suggestion->top->no_of_reviews : 0] = [
                    'img' => $img,
                    'id' => $suggestion->id,
                    'text' => $suggestion->transsingle->title,
                    'country' => 'city in ' . $suggestion->country->transsingle->title,
                    'lat' => $suggestion->lat,
                    'lng' => $suggestion->lng,
                    'reviews' => isset($suggestion->top->no_of_reviews) ? $suggestion->top->no_of_reviews : 0
                ];
            }
        }
        krsort($res);
        $return = array('results' => array_values($res));
        return json_encode($return);
    }

    public function getIndex($id, Request $request, TripsSuggestionsService $tripsSuggestionsService)
    {
        $trip_info = TripPlans::findorfail($id);

        return redirect('trip/plan/' . $id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function postVersionRespond(Request $request)
    {

        $trip_id = $request->get('trip_id');
        $version_id = $request->get('version_id');
        $action = $request->get('action');

        $request_id = TripsContributionRequests::where('plans_id', $trip_id)->where('authors_id', Auth::guard('user')->user()->id)->get()->first()->id;
        $user_id = TripsContributionRequests::where('plans_id', $trip_id)->where('authors_id', Auth::guard('user')->user()->id)->get()->first()->users_id;

        if ($request_id && $user_id) {
            $request_id = $request_id->id;
            $user_id = $user_id->users_id;
            if ($action == "accept") {
                $ver = TripsContributionVersions::where('plans_id', $trip_id)->where('requests_id', $request_id)->get()->first();
                $ver->status = 1;
                $ver->save();

                $trip = TripPlans::find($trip_id);
                $trip->active_version = $version_id;
                $trip->save();

                log_user_activity('Trip', 'accept_version', $version_id);
            } elseif ($action == "reject") {
                $ver = TripsContributionVersions::where('plans_id', $trip_id)->where('requests_id', $request_id)->get()->first();
                $ver->status = 2;
                $ver->save();

                log_user_activity('Trip', 'reject_version', $version_id);
            }
        }

        return Redirect::to('trip/view/' . $trip_id);
    }

    public function postAjaxFirstStep(Request $request, PlaceChatsService $placeChatsService, RankingService $rankingService, TranslationService $translationService)
    {
        $this->validate($request, [
            'title' => 'string|required|max:100',
            'description' => 'string|nullable|max:260',
            'cover' => 'file|nullable'
        ]);

        $title = $request->get('title');
        $description = $request->get('description');
        $date = $request->get('actual_start_date');
        $privacy = $request->get('privacy');
        $user_id = Auth::guard('user')->user()->id;
        $memory = $request->get('plan_type');
        $cover = $request->file('cover');

        if (!$cover && $memory) {
            throw new \Exception();
        }

        $new_plan = new TripPlans;
        $new_plan->users_id = $user_id;
        $new_plan->title = $title;
        $new_plan->description = $description;
        $new_plan->privacy = $privacy;
        $new_plan->memory = $memory;
        $new_plan->created_at = date('Y-m-d h:i:s a', time());
        $new_plan->active = 0;
        $new_plan->language_id = $translationService->getLanguageId($title . ' ' . $description);

        if ($new_plan->save()) {
            $rankingService->addPointsEarners($new_plan->id, get_class($new_plan), $user_id);

            $coverUrl = null;
            if ($cover) {
                $filename = time() . '_' . bin2hex(random_bytes(10)) . '.' . $cover->getClientOriginalExtension();

                Storage::disk('s3')->putFileAs(
                    'trips/cover/' . $new_plan->id . '/',
                    $cover,
                    $filename,
                    'public'
                );

                $coverUrl = 'https://s3.amazonaws.com/travooo-images2/trips/cover/' . $new_plan->id . '/' . $filename;
            }

            $version_0 = new TripsVersions;
            $version_0->plans_id = $new_plan->id;
            $version_0->authors_id = $user_id;
            $version_0->start_date = $date;
            $version_0->save();
            $new_plan->active_version = $version_0->id;
            $new_plan->cover = $coverUrl;
            $new_plan->save();

            $placeChatsService->createConversation($new_plan->id, PlaceChatsService::CHAT_TYPE_MAIN, auth()->id());

            $ret = array('status' => 'success', 'trip_id' => $new_plan->id);
        } else {
            $ret = array('status' => 'error');
        }

        return json_encode($ret);
    }

    public function postAjaxEditTrip(Request $request, TripsSuggestionsService $tripsSuggestionsService, PlanActivityLogService $planActivityLogService, TripInvitationsService $tripInvitationsService)
    {
        $this->validate($request, [
            'title' => 'string|required|max:100',
            'description' => 'string|nullable|max:260',
            'cover' => 'file|nullable'
        ]);

        $trip_id = $request->get('trip_id');
        $title = $request->get('title');
        $description = $request->get('description');
        $date = $request->get('actual_start_date');
        $privacy = $request->get('privacy');
        $cover = $request->file('cover');

        $user_id = Auth::guard('user')->user()->id;

        $old_plan = TripPlans::findOrFail($trip_id);

        if ($tripInvitationsService->getUserRole($trip_id, auth()->id()) !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN]) {
            throw new AuthorizationException('', 403);
        }

        $old_plan->active = 0;
        if ($old_plan->save()) {
            $planSuggestions = $tripsSuggestionsService->getEditPlanSuggestions($trip_id);

            foreach ($planSuggestions as $planSuggestion) {
                $meta = $planSuggestion->meta;
                $old_plan->privacy = Arr::get($meta, 'privacy');
                $old_plan->description = Arr::get($meta, 'description');
                $old_plan->title = Arr::get($meta, 'name');
                $old_plan->cover = Arr::get($meta, 'cover', $old_plan->cover);
            }

            $suggestion = $tripsSuggestionsService->createEditPlanSuggestion($trip_id, $title, $description, $privacy, $cover);

            $cover = $old_plan->cover;

            if (isset($suggestion->meta['cover'])) {
                $cover = $suggestion->meta['cover'];
            }

            $version_0 = TripsVersions::find($old_plan->active_version);
            $version_0->start_date = $date;
            $version_0->save();

            log_user_activity('Trip', 'edit', $old_plan->id);
            $ret = array('status' => 'success', 'trip_id' => $old_plan->id, 'data' => $suggestion->meta);
        } else {
            $ret = array('status' => 'error');
        }

        if ($old_plan->privacy != $privacy) {
            $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_CHANGED_PRIVACY_SETTINGS, $trip_id, null, auth()->id(), ['privacy' => $privacy]);
        }

        if ($old_plan->title != $title) {
            $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_CHANGED_PLAN_TITLE, $trip_id, null, auth()->id());
        }

        if ($old_plan->description != $description) {
            $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_CHANGED_PLAN_DESCRIPTION, $trip_id, null, auth()->id());
        }

        if ($old_plan->cover != $cover) {
            $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_CHANGED_PLAN_COVER, $trip_id, null, auth()->id());
        }

        return json_encode($ret);
    }

    public function postAjaxSearchCities(Request $request)
    {
        $query = $request->get('query');
        $return = false;
        $selected_cities = json_decode($request->selected_cities);

        $suggestions = Cities::with('trans')
            ->whereHas('trans', function ($q) use ($query) {
                $q->where('title', 'LIKE', '%' . $query . '%');
            })
            ->orderBy('id', 'desc')->get();
        //dd($suggestions);

        foreach ($suggestions as $suggestion) {
            $view = View::make('site.trip.partials.city-suggestion-row', array('suggestion' => $suggestion, 'selected_cities' => $selected_cities));
            //dd((string) $view);
            $return .= $view->render();
        }
        //dd($return);
        return json_encode($return);
    }

    public function postAjaxSearchPlaces(Request $request)
    {
        $query = $request->get('term');
        $city_id = $request->get('cities_id');
        $return = false;

        $suggestions = Place::with('trans')
            ->where('cities_id', $city_id)
            ->whereHas('trans', function ($q) use ($query) {
                $q->where('title', 'LIKE', '%' . $query . '%');
            })
            ->orderBy('rating', 'desc')
            ->take(100)
            ->get();

        $res = array();
        foreach ($suggestions as $suggestion) {
            $res[] = array('id' => $suggestion->id, 'text' => $suggestion->transsingle->title);
        }

        $return = array('results' => $res);

        return json_encode($return);
    }

    public function postAjaxAddCityToTrip(Request $request)
    {
        $city_id = $request->get('city_id');
        $trip_id = $request->get('trip_id');
        $version_id = $request->get('version_id');

        $add_city = new TripCities;
        $add_city->trips_id = $trip_id;
        $add_city->cities_id = $city_id;
        $add_city->versions_id = $version_id;
        $add_city->date = date("Y-mm-dd", time());
        $add_city->order = 0;
        $add_city->active = 0;
        $add_city->transportation = '';
        if ($add_city->save()) {
            $add_country = new TripCountries;
            $add_country->trips_id = $trip_id;
            $add_country->countries_id = Cities::find($city_id)->countries_id;
            $add_country->versions_id = $version_id;
            $add_country->date = date("Y-mm-dd", time());
            $add_country->order = 0;
            if ($add_country->save()) {
                $ret = array('status' => 'success', 'trip_city_id' => $add_city->id);
            }
        } else {
            $ret = array('status' => 'error');
        }
        return json_encode($ret);

        //return view('site.trip.view', $data);
    }

    /**
     * @param Request $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param TripsService $tripsService
     * @param PlanActivityLogService $planActivityLogService
     * @return false|string
     * @throws AuthorizationException
     * @throws \App\Exceptions\MaxRejectedTimesException
     */
    public function postAjaxAddPlaceToTrip(Request $request, TripsSuggestionsService $tripsSuggestionsService, TripsService $tripsService, PlanActivityLogService $planActivityLogService, TripInvitationsService $tripInvitationsService)
    {
        $trip_id = $request->get('trip_id');
        $trip = TripPlans::find($trip_id);

        if (!$trip) {
            //todo error
        }

        $authUserId = auth()->id();
        $tripUserId = $trip->users_id;
        $invitedUserRequest = null;

        if (!in_array($tripInvitationsService->getUserRole($trip_id, auth()->id()), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN],
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
        ])) {
            throw new AuthorizationException('', 403);
        }

        if ($tripUserId !== $authUserId) {
            $invitedUserRequest = $tripsSuggestionsService->getInvitedUserRequest($trip_id, $authUserId);
        }

        $tripVersion = TripsVersions::where([
            'authors_id' => $authUserId,
            'plans_id' => $trip_id
        ])->latest()->first();

        if (!$tripVersion) {
            //todo error
        }

        $city_id = $request->get('city_id');
        $place_id = $request->get('place_id');
        $budget = $request->get('budget');
        $known_for = $request->get('known_for');
        $duration_days = $request->get('duration_days');
        $duration_hours = $request->get('duration_hours');
        $duration_minutes = $request->get('duration_minutes');
        $date = $request->get('date');
        $time = $request->get('time');

        $version_id = 0;
        $order = $request->get('order');
        $medias = array_slice(array_unique($request->get('medias', [])), 0, 10);

        if ($order === null) {
            $order = TripPlaces::where('trips_id', $trip_id)->where('date', $date)->max('order');
        }

        $duration = ($duration_hours * 60) + $duration_minutes;
        $duration = ($duration_days * 60 * 24) + ($duration_hours * 60) + $duration_minutes;

        $date_time = strtotime($date . " " . $time);
        $date_time = date("Y-m-d H:i:s", $date_time);

        if ($trip->memory && Carbon::parse($date_time)->greaterThan(now())) {
            throw new AuthorizationException('', 403);
        }

        $withoutTime = 0;

        if (!$time) {
            $withoutTime = 1;
        }

        $time = $date_time;
        $is_new_place = 0;
        if (
            !Place::where('provider_id', '=', $place_id)->exists()
            && !Place::where('id', '=', $place_id)->exists()
        ) {
            $place_id = $tripsService->addNewPlaceByProviderId($place_id);
            $is_new_place = $place_id;
        } elseif (Place::where('provider_id', '=', $place_id)->exists()) {
            $place_id = Place::where('provider_id', '=', $place_id)->first()->id;
        }

        $add_place = new TripPlaces;
        $add_place->trips_id = $trip_id;
        $add_place->versions_id = 0;
        $add_place->cities_id = $city_id;
        $city_object = Cities::find($city_id);
        $add_place->countries_id = $city_object->countries_id;
        $add_place->places_id = $place_id;
        $add_place->time = $time;
        $add_place->date = $time;
        $add_place->without_time = $withoutTime;
        $add_place->order = $order === null ? 1 : $order + 1;
        $add_place->budget = $budget;
        $add_place->duration = $duration;
        $add_place->comment = $known_for;
        $add_place->active = 0;
        $add_place->story = $request->get('story', '');

        $isPlaceExists = $tripsSuggestionsService->isPlaceOrTimeExistsInPlan($add_place);

        if ($isPlaceExists) {
            $ret = [
                'status' => 'error',
                'data' => 'The place already exists or there\'s already a place with that time.'
            ];

            return json_encode($ret);
        }

        if ($add_place->save() && $tripsService->recalculateOrders($trip_id, $add_place, $request->get('time'))) {
            $isAdmin = !$invitedUserRequest || $invitedUserRequest->role === 'admin';

            $suggestionId = $tripsSuggestionsService->createSuggestion(
                $add_place->id,
                $trip_id,
                TripsSuggestionsService::SUGGESTION_CREATE_TYPE,
                null,
                null,
                null,
                $isAdmin
            );

            if ($isAdmin) {
                $tripsSuggestionsService->approveSuggestion($suggestionId);
                $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_ADDED_PLACE, $trip_id, $add_place->getKey(), auth()->id(), [], $suggestionId);
            }

            $country = TripCountries::where('trips_id', $trip_id)->where('countries_id', $city_object->countries_id)->first();
            if (!$country) {
                $add_country = new TripCountries;
                $add_country->trips_id = $trip_id;
                $add_country->countries_id = $city_object->countries_id;
                $add_country->versions_id = $version_id;
                $add_country->date = date("Y-m-d H:i:s");
                $add_country->order = 0;

                $add_country->save();
            }

            if ($budget) {
                $trip->budget = $trip->budget + $budget;

                $trip->save();
            }

            foreach ($medias as $mediaId) {
                $tm = new TripMedias();
                $tm->medias_id = $mediaId;
                $tm->trips_id = $trip_id;
                $tm->places_id = $place_id;
                $tm->trip_place_id = $add_place->id;
                $tm->save();

                $um = new UsersMedias();
                $um->users_id = auth()->id();
                $um->medias_id = $mediaId;
                $um->save();
            }

            $place = $add_place->place()->with(['city', 'trans', 'city.trans', 'country', 'country.trans'])->first();
            $place->src = check_place_photo($place);

            $ret = [
                'status' => 'success',
                'trip_place_id' => $add_place->id,
                'is_new' => $is_new_place,
                'place' => $place
            ];
        } else {
            $ret = [
                'status' => 'error',
                'data' => 'internal error'
            ];
        }
        return json_encode($ret);
    }


    /**
     * @param Request $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @return false|string
     * @throws AuthorizationException
     */
    public function postAjaxEditPlaceInTrip(Request $request, TripsSuggestionsService $tripsSuggestionsService, TripsService $tripsService, PlanActivityLogService $planActivityLogService, TripInvitationsService $tripInvitationsService)
    {
        $trip_id = $request->get('trip_id');
        $trip = TripPlans::find($trip_id);
        $medias = array_slice(array_unique($request->get('medias', [])), 0, 10);

        if (!$trip) {
            //todo error
        }

        $authUserId = auth()->id();
        $tripUserId = $trip->users_id;

        $invitedUserRequest = null;

        if (!in_array($tripInvitationsService->getUserRole($trip_id, auth()->id()), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN],
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
        ])) {
            throw new AuthorizationException('', 403);
        }

        if ($tripUserId !== $authUserId) {
            $invitedUserRequest = $tripsSuggestionsService->getInvitedUserRequest($trip_id, $authUserId);
        }

        $tripVersion = TripsVersions::where([
            'authors_id' => $authUserId,
            'plans_id' => $trip_id
        ])->latest()->first();

        if (!$tripVersion) {
            //todo error
        }

        $trip_place_id = $request->get('trip_place_id');

        $city_id = $request->get('city_id');
        $place_id = $request->get('place_id');
        $budget = $request->get('budget');
        $duration_days = $request->get('duration_days');
        $duration_hours = $request->get('duration_hours');
        $duration_minutes = $request->get('duration_minutes');
        $date = $request->get('date');
        $time = $request->get('time');
        $known_for = $request->get('known_for');

        $duration = ($duration_days * 60 * 24) + ($duration_hours * 60) + $duration_minutes;

        $date_time = strtotime($date . " " . $time);
        $date_time = date("Y-m-d H:i:s", $date_time);

        $withoutTime = 0;

        if (!$time) {
            $withoutTime = 1;
        }

        $time = $date_time;

        if ($trip->memory && Carbon::parse($time)->greaterThan(now()->endOfDay())) {
            throw new BadRequestHttpException();
        }

        if (!$trip->memory && Carbon::parse($time)->lessThan(now()->startOfDay())) {
            throw new BadRequestHttpException();
        }

        $trip_place_id = $tripsSuggestionsService->getLastTripPlace($trip_place_id);

        if ($tripsSuggestionsService->isDeletedTripPlace($trip_place_id)) {
            $ret = [
                'status' => 'error',
                'data' => 'The place was removed'
            ];

            return json_encode($ret);
        }

        $add_place = TripPlaces::query()->findOrFail($trip_place_id);

        $current_place = $add_place;
        $add_place = $add_place->replicate();

        $add_place->trips_id = $trip_id;
        $add_place->versions_id = 0;
        $add_place->cities_id = $city_id;
        $add_place->without_time = $withoutTime;
        $city_object = Cities::find($city_id);
        $add_place->countries_id = $city_object->countries_id;
        $add_place->places_id = $place_id;
        $add_place->time = $time;
        $add_place->date = $time;
        $add_place->budget = $budget;
        $add_place->duration = $duration;
        $add_place->comment = $known_for;
        $add_place->active = 0;
        $add_place->story = $request->get('story', '');

        $isPlaceExists = $tripsSuggestionsService->isPlaceOrTimeExistsInPlan($add_place, $trip_place_id);

        if ($isPlaceExists) {
            $ret = [
                'status' => 'error',
                'data' => 'The place already exists or there\'s already a place with that time.'
            ];

            return json_encode($ret);
        }

        if ($add_place->save() && $tripsService->recalculateOrders($trip_id, $add_place, $request->get('time'))) {
            $isAdmin = !$invitedUserRequest || $invitedUserRequest->role === 'admin';

            $suggestionId = $tripsSuggestionsService->createSuggestion(
                $add_place->id,
                $trip_id,
                TripsSuggestionsService::SUGGESTION_EDIT_TYPE,
                $trip_place_id,
                null,
                null,
                $isAdmin
            );

            $meta = $planActivityLogService->prepareLogMeta($add_place, $current_place);

            if ($isAdmin) {
                $tripsSuggestionsService->approveSuggestion($suggestionId);
                $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_EDITED_PLACE, $trip_id, $add_place->getKey(), $authUserId, $meta, $suggestionId);
            }

            foreach ($medias as $mediaId) {
                $attributes = [
                    'medias_id' => $mediaId,
                    'trips_id' => $trip_id,
                    'places_id' => $place_id,
                    'trip_place_id' => $add_place->id
                ];

                TripMedias::query()->firstOrCreate($attributes);
                UsersMedias::query()->firstOrCreate(['users_id' => auth()->id(), 'medias_id' => $mediaId]);
            }

            $ret = array('status' => 'success', 'trip_place_id' => $add_place->id);
        } else {
            $ret = array('status' => 'error', 'data' => 'internal error');
        }
        return json_encode($ret);
    }

    public function postAjaxDismissPlaceFromTrip(Request $request, TripsSuggestionsService $tripsSuggestionsService)
    {
        $tripPlaceId = $request->get('place_id');

        /** @var TripPlaces $tripPlace */
        $tripPlace = TripPlaces::query()->withTrashed()->find($tripPlaceId);

        if (!$tripPlace) {
            throw new \Exception('Trip place not found', 404);
        }

        if ($tripsSuggestionsService->dismissSuggestedPlace($tripPlaceId)) {
            return AjaxResponse::create();
        }

        return AjaxResponse::create([], false, 400);
    }

    /**
     * @param Request $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @param PlanActivityLogService $planActivityLogService
     * @return false|string
     * @throws AuthorizationException
     * @throws \App\Exceptions\MaxRejectedTimesException
     */
    public function postAjaxRemovePlaceFromTrip(Request $request, TripsSuggestionsService $tripsSuggestionsService, PlanActivityLogService $planActivityLogService)
    {
        $tripPlaceId = $request->get('place_id');

        $tripPlaceId = $tripsSuggestionsService->getLastTripPlace($tripPlaceId);

        /** @var TripPlaces $tripPlace */
        $tripPlace = TripPlaces::find($tripPlaceId);

        if (!$tripPlace) {
            throw new \Exception('Trip place not found', 404);
        }

        $trip = $tripPlace->trip;
        $authUserId = auth()->id();

        $invitedUserRequest = null;
        $ret = ['status' => 'success'];

        $invitedUserRequest = $tripsSuggestionsService->getInvitedUserRequest($trip->id, $authUserId);

        $isAdmin = !$invitedUserRequest || $invitedUserRequest->role === 'admin';

        $suggestionId = $tripsSuggestionsService->createSuggestion(
            $tripPlaceId,
            $trip->id,
            TripsSuggestionsService::SUGGESTION_DELETE_TYPE,
            $tripPlaceId,
            null,
            null,
            $isAdmin
        );

        if ($isAdmin) {
            $tripsSuggestionsService->approveSuggestion($suggestionId);
            $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_REMOVED_PLACE, $trip->id, $tripPlaceId, auth()->id());
            $tripsSuggestionsService->dismissSuggestedPlace($tripPlaceId);
        }

        return json_encode($ret);
    }

    public function postAjaxRemoveCityFromTrip(Request $request)
    {
        $city_id = $request->get('city_id');
        $trip_id = $request->get('trip_id');
        $version_id = $request->get('version_id');

        $remove_city = TripCities::where('cities_id', $city_id)
            ->where('versions_id', $version_id)
            ->where('trips_id', $trip_id);

        if ($remove_city->delete()) {
            $remove_places = TripPlaces::where('cities_id', $city_id)
                ->where('versions_id', $version_id)
                ->where('trips_id', $trip_id)->delete();
            $existing_cities = TripCities::where('trips_id', $trip_id)->orderBy('id', 'desc')->first();

            if ($existing_cities) {
                $last_city_id = $existing_cities->cities_id;
            } else {
                $last_city_id = 0;
            }
            $ret = array('status' => 'success', 'last_city_id' => $last_city_id);
        } else {
            $ret = array('status' => 'error');
        }
        return json_encode($ret);
    }

    public function postAjaxActivateTripCity(Request $request)
    {
        $trip_city_id = $request->get('trip_city_id');
        $transportation = $request->get('transportation');

        $trip_city = TripCities::find($trip_city_id);
        $trip_city->transportation = $transportation;
        $trip_city->active = 1;

        if ($trip_city->save()) {
            $ret = array('status' => 'success', 'trip_city_id' => $trip_city->id);
        } else {
            $ret = array('status' => 'error');
        }
        return json_encode($ret);
    }

    public function postAjaxDeActivateTripCity(Request $request)
    {
        $trip_city_id = $request->get('trip_city_id');

        $trip_city = TripCities::find($trip_city_id);
        $trip_city->active = 0;

        if ($trip_city->save()) {
            $ret = array('status' => 'success', 'trip_city_id' => $trip_city->id);
        } else {
            $ret = array('status' => 'error');
        }
        return json_encode($ret);
    }

    public function postAjaxActivateTripPlace(Request $request)
    {
        $trip_place_id = $request->get('trip_place_id');
        $trip_id = $request->get('trip_id');
        $version_id = $request->get('version_id');
        $place_id = $request->get('place_id');
        $city_id = $request->get('city_id');
        $date = $request->get('actual_place_date');
        $time = $request->get('time');
        $duration_hours = $request->get('duration_hours');
        $duration_minutes = $request->get('duration_minutes');
        $budget = $request->get('budget');
        $duration = ($duration_hours * 60) + $duration_minutes;
        //date=30%20November%202018&actual_place_date=2018-11-30&time=2%3A30am&duration_hours=1&duration_minutes=30&budget%5B%5D=40&trip_place_id=108&trip_id=196&city_id=909&place_id=2

        $date_time = strtotime($date . " " . $time);
        $date_time = date("Y-n-d H:i:s", $date_time);
        $time = $date_time;

        //return json_encode($time);

        $trip_place = TripPlaces::find($trip_place_id);
        $trip_place->date = $date;
        $trip_place->time = $time;
        $trip_place->budget = $budget;
        $trip_place->duration = $duration;
        $trip_place->active = 1;

        if ($trip_place->save()) {

            // update city & country visit datetime
            TripCities::where('trips_id', $trip_id)->where('versions_id', $version_id)->where('cities_id', $city_id)->update(['date' => $date_time]);
            $_city = Cities::find($city_id);
            TripCountries::where('trips_id', $trip_id)->where('versions_id', $version_id)->where('countries_id', $_city->countries_id)->update(['date' => $date_time]);

            $trip_place_update = $trip_place->trip->myversion()[0];
            $trip_place_update->end_date = $date;
            $trip_place_update->save();

            $ret = array('status' => 'success', 'trip_place_id' => $trip_place->id);
        } else {
            $ret = array('status' => 'error');
        }
        return json_encode($ret);
    }

    public function postAjaxDeActivateTripPlace(Request $request)
    {
        $trip_place_id = $request->get('trip_place_id');

        $trip_place = TripPlaces::find($trip_place_id);
        $trip_place->active = 0;

        if ($trip_place->save()) {

            $ret = array('status' => 'success', 'trip_place_id' => $trip_place->id);
        } else {
            $ret = array('status' => 'error');
        }
        return json_encode($ret);
    }


    public function postAjaxDeleteTrip(Request $request, PlaceChatsService $placeChatsService)
    {
        $trip_id = $request->get('trip_id');
        $ret = array('status' => null);
        $travel_mates = TravelMatesRequests::where('plans_id', $trip_id)->first();
        if ($travel_mates) {
            TravelMatesRequestUsers::where('requests_id', $travel_mates->id)->delete();
            $travel_mates->delete();
        }

        ReportsInfos::where('var', 'plan')->where('val', $trip_id)->delete();
        ReportsDraftInfos::where('var', 'plan')->where('val', $trip_id)->delete();

        TripCities::where('trips_id', $trip_id)->delete();
        TripCountries::where('trips_id', $trip_id)->delete();
        TripPlaces::where('trips_id', $trip_id)->delete();
        TripsVersions::where('plans_id', $trip_id)->delete();
        $placeChatsService->removeAllPlanChats($trip_id);
        TripPlans::destroy($trip_id);

        try {
            broadcast(new PlanDeletedEvent($trip_id));
        } catch (\Throwable $e) {
        }
        try {
            broadcast(new PlanDeletedApiEvent($trip_id));
        } catch (\Throwable $e) {
        }

        log_user_activity('Trip', 'delete', $trip_id);
        $ret['status'] = 'success';

        Notifications::whereIn('data_id', TripsContributionRequests::where('plans_id', $trip_id)->pluck('id'))
            ->where('type', 'plan_invitation_received')
            ->delete();

        return json_encode($ret);
    }

    public function postAjaxActivateTrip(Request $request, TripsSuggestionsService $tripsSuggestionsService, PlanActivityLogService $planActivityLogService, TripInvitationsService $tripInvitationsService)
    {
        $trip_id = $request->get('trip_id');
        $version_id = $request->get('version_id');
        $send_back = $request->get('send_back');
        $title = $request->get('title');
        $description = $request->get('description');
        $privacy = $request->get('privacy');

        $my_trip = TripPlans::find($trip_id);

        @$startPlaceDate = $my_trip->trips_places()->orderBy('date')->first()->date;
        @$endPlaceDate = $my_trip->trips_places()->orderBy('date', 'desc')->first()->date;

        if ($startPlaceDate) {
            $my_trip->version->start_date = $startPlaceDate;
        }

        if ($endPlaceDate) {
            $my_trip->version->end_date = $endPlaceDate;
        }

        if ($tripInvitationsService->getUserRole($trip_id, auth()->id()) !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN]) {
            throw new AuthorizationException('', 403);
        }

        $my_trip->version->save();

        if ($my_trip->update(['title' => $title, 'description' => $description, 'active' => 1])) {
            if ($send_back) {
                $tripsContributionRequests = TripsContributionRequests::where('plans_id', $trip_id)->where('users_id', Auth::guard('user')->user()->id)->get()->first();

                if ($tripsContributionRequests) {
                    $commit = new TripsContributionCommits;
                    $commit->plans_id = $trip_id;
                    $commit->requests_id = $tripsContributionRequests->id;
                    $commit->users_id = Auth::guard('user')->user()->id;
                    $commit->versions_id = $version_id;
                    $commit->notes = '';
                    $commit->status = 0;
                    $commit->save();
                }

                $not = new Notifications;
                $not->authors_id = Auth::guard('user')->user()->id;
                $not->users_id = $my_trip->users_id;
                $not->type = 'plan_version_sent';
                $not->data_id = $my_trip->id;
                $not->notes = '';
                $not->read = 0;
                $not->save();

                log_user_activity('Trip', 'activate', $trip_id);
            }


            $published = $tripsSuggestionsService->publishPlan($my_trip);

            $my_trip->title = $title;
            $my_trip->description = $description;
            $my_trip->privacy = $privacy;
            $my_trip->save();

            if ($published) {
                $log = $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_PUBLISHED_PLAN, $trip_id, null, auth()->id());
                $tempLog = $planActivityLogService->prepareLogForResponse($log);
                try {
                    broadcast(new PlanPublicEvent($tempLog, false));
                } catch (\Throwable $e) {
                }
                try {
                    broadcast(new PlanPublicApiEvent($tempLog, false));
                } catch (\Throwable $e) {
                }
            }

            $ret['status'] = 'success';
        } else {
            $ret['status'] = 'error_unknown';
        }

        return json_encode($ret);
    }

    public function postAjaxPublishTripPlace(Request $request, TripInvitationsService $tripInvitationsService, TripsSuggestionsService $tripsSuggestionsService, PlanActivityLogService $planActivityLogService)
    {
        $this->validate($request, [
            'trip_id' => 'numeric|required',
            'place_id' => 'numeric|required'
        ]);

        $planId = $request->get('trip_id');
        $placeId = $request->get('place_id');

        /** @var TripPlans $plan */
        $plan = TripPlans::query()->findOrFail($planId);

        if ($tripInvitationsService->getUserRole($planId, auth()->id()) !== TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN]) {
            throw new AuthorizationException('', 403);
        }

        $published = $tripsSuggestionsService->publishPlan($plan, $placeId);

        if ($published) {
            $log = $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_PUBLISHED_PLAN, $planId, null, auth()->id());
            $tempLog = $planActivityLogService->prepareLogForResponse($log);
            try {
                broadcast(new PlanPublicEvent($tempLog, false));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new PlanPublicApiEvent($tempLog, false));
            } catch (\Throwable $e) {
            }
        }

        return new AjaxResponse($published);
    }

    /**
     * @param Request $request
     * @param TripInvitationsService $tripInvitationsService
     * @return AjaxResponse
     */
    public function getAjaxInvitedPeople(Request $request, TripInvitationsService $tripInvitationsService)
    {
        $planId = $request->get('trip_id');
        return new AjaxResponse($tripInvitationsService->getInvitedPeople($planId, true, true));
    }

    /**
     * @param Request $request
     * @param TripInvitationsService $tripInvitationsService
     * @return AjaxResponse
     */
    public function getAjaxPeopleToInvite(Request $request, TripInvitationsService $tripInvitationsService)
    {
        $search = $request->get('search');
        $withStyles = $request->get('with_styles');
        $planId = $request->get('trip_id');

        return new AjaxResponse($tripInvitationsService->getPeopleToInvite($search, $planId, $withStyles));
    }

    /**
     * @param Request $request
     * @param TripInvitationsService $tripInvitationsService
     * @return AjaxResponse
     * @throws \Exception
     */
    public function postAjaxInviteFriends(Request $request, TripInvitationsService $tripInvitationsService)
    {
        $planId = $request->get('trip_id');
        $userId = $request->get('users_id');
        $notes = $request->get('notes');
        $role = $request->get('role');
        $w_plans_id = ($request->get('w_plans_id') !== null) ? $request->get('w_plans_id') : NULL;

        if ($userId == Auth::guard('user')->user()->id) {
            return new AjaxResponse([], false);
        }

        if (!in_array($tripInvitationsService->getUserRole($planId, auth()->id()), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN]
        ])) {
            throw new AuthorizationException('', 403);
        }

        return new AjaxResponse([], $tripInvitationsService->invite($planId, $userId, $notes, $role, $w_plans_id));
    }

    public function postAjaxAcceptInvitation(Request $request, PlanActivityLogService $planActivityLogService, PlaceChatsService $placeChatsService)
    {
        $invitation_id = $request->get('invitation_id');
        $user_id = Auth::guard('user')->user()->id;

        $invite = TripsContributionRequests::find($invitation_id);
        $invite->status = 1;
        if ($invite->save()) {
            notify($invite->authors_id, 'plan_invitetion_accepted', $invite->id, 'travel-mate');

            $placeChatsService->addToPlanChats($invite->plans_id, $invite->users_id);

            $logMeta = [
                'role' => TripInvitationsService::ROLES_MAPPER[$invite->role]
            ];

            $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_JOINED_THE_PLAN, $invite->plans_id, $invite->id, auth()->id(), $logMeta);

            Notifications::where('users_id', $user_id)->where('data_id', $invite->id)->delete();

            $from_version = TripsVersions::where('plans_id', $invite->plans_id)->where('authors_id', $invite->authors_id)->get()->first();
            $to_version = new TripsVersions;
            $to_version->authors_id = $user_id;
            $to_version->start_date = $from_version->start_date;
            $to_version->plans_id = $from_version->plans_id;
            if ($to_version->save()) {
                log_user_activity('Trip', 'accept_invitation', $invitation_id);

                $v = new TripsContributionVersions;
                $v->plans_id = $invite->plans_id;
                $v->requests_id = $invite->id;
                $v->version_number = 1;
                $v->status = 0;
                $v->save();

                // copy cities
                $old_cities = TripCities::where('trips_id', $invite->plans_id)->where('versions_id', $from_version->id)->get();
                //dd($old_cities);
                foreach ($old_cities as $old_city) {
                    $new_city = new TripCities;
                    $new_city->trips_id = $invite->plans_id;
                    $new_city->cities_id = $old_city->cities_id;
                    $new_city->date = $old_city->date;
                    $new_city->order = $old_city->order;
                    $new_city->active = $old_city->active;
                    $new_city->transportation = $old_city->transportation;
                    $new_city->versions_id = $to_version->id;
                    $new_city->save();
                }
                // copy countries
                $old_countries = TripCountries::where('trips_id', $invite->plans_id)->where('versions_id', $from_version->id)->get();
                foreach ($old_countries as $old_country) {
                    $new_country = new TripCountries;
                    $new_country->trips_id = $invite->plans_id;
                    $new_country->countries_id = $old_country->countries_id;
                    $new_country->date = $old_country->date;
                    $new_country->order = $old_country->order;
                    $new_country->versions_id = $to_version->id;
                    $new_country->save();
                }
                // copy places
                $old_places = TripPlaces::where('trips_id', $invite->plans_id)->where('versions_id', $from_version->id)->get();
                foreach ($old_places as $old_place) {
                    $new_place = new TripPlaces;
                    $new_place->trips_id = $invite->plans_id;
                    $new_place->countries_id = $old_place->countries_id;
                    $new_place->cities_id = $old_place->cities_id;
                    $new_place->places_id = $old_place->places_id;
                    $new_place->date = $old_place->date;
                    $new_place->order = $old_place->order;
                    $new_place->time = $old_place->time;
                    $new_place->duration = $old_place->duration;
                    $new_place->budget = $old_place->budget;
                    $new_place->active = $old_place->active;
                    $new_place->versions_id = $to_version->id;
                    $new_place->save();
                }


                $ret = array('status' => 'success', 'trip_id' => $invite->plans_id);
            }
        }


        return json_encode($ret);
    }

    /**
     * @param Request $request
     * @param TripInvitationsService $tripInvitationsService
     * @return false|string
     * @throws \Exception
     */
    public function postAjaxCancelInvitation(Request $request, TripInvitationsService $tripInvitationsService)
    {
        $tripId = $request->get('trip_id');
        $userId = $request->get('users_id');

        if ($userId !== auth()->id() && !in_array($tripInvitationsService->getUserRole($tripId, auth()->id()), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN]
        ])) {
            throw new AuthorizationException('', 403);
        }

        return json_encode([
            'status' => $tripInvitationsService->cancelInvitation($tripId, $userId) ? 'success' : 'fail'
        ]);
    }

    /**
     * @param Request $request
     * @param TripInvitationsService $tripInvitationsService
     * @return false|string
     * @throws \Exception
     */
    public function postAjaxLeavePlan(Request $request, TripInvitationsService $tripInvitationsService)
    {
        $tripId = $request->get('trip_id');

        return json_encode([
            'status' => $tripInvitationsService->leavePlan($tripId) ? 'success' : 'fail'
        ]);
    }

    public function postAjaxRejectInvitation(Request $request, PlanActivityLogService $planActivityLogService)
    {
        $invitation_id = $request->get('invitation_id');
        $user_id = Auth::guard('user')->user()->id;

        $invite = TripsContributionRequests::find($invitation_id);
        $invite->status = 2;
        if ($invite->save()) {
            $logMeta = [
                'role' => TripInvitationsService::ROLES_MAPPER[$invite->role]
            ];

            $planActivityLogService->createPlanActivityLog(PlanActivityLog::TYPE_REJECT_INVITATION, $invite->plans_id, $invite->id, auth()->id(), $logMeta);

            log_user_activity('Trip', 'reject_invitation', $invitation_id);
            notify($invite->authors_id, 'plan_invitetion_rejected', $invitation_id, 'travel-mate');

            $ret = array('status' => 'success');
            Notifications::where('users_id', $user_id)->where('data_id', $invite->id)->delete();
        }


        return json_encode($ret);
    }

    public function postUploadMedia(Request $request, TripInvitationsService $tripInvitationsService, RankingService $rankingService)
    {
        $media_file = $request->file('file');
        $trip_id = $request->get('trip_id');
        $place_id = $request->get('place_id');
        $tripPlaceId = $request->get('trip_place_id');
        $user_id = Auth::guard('user')->user()->id;

        $media_file_url = '';
        $thumb_file_url = null;

        if (!in_array($tripInvitationsService->getUserRole($trip_id, auth()->id()), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN],
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
        ])) {
            throw new AuthorizationException('', 403);
        }

        if ($media_file) {
            $filename = time() . md5($media_file->getClientOriginalName()) . '.' . $media_file->getClientOriginalExtension();

            $type = Media::TYPE_IMAGE;

            Storage::disk('s3')->putFileAs('trips/' . $trip_id . '/', $media_file, $filename, 'public');
            $media_file_url = 'https://s3.amazonaws.com/travooo-images2/trips/' . $trip_id . '/' . $filename;

            if (strpos($media_file->getMimeType(), 'video/') !== false) {
                $type = Media::TYPE_VIDEO;
                $ffmpeg = \FFMpeg\FFMpeg::create();

                $video = $ffmpeg->open($media_file->getRealPath());

                $duration = @$video->getStreams()->videos()->first()->get('duration');

                if ($duration) {
                    $frame = $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds($duration * 0.01));
                    $name = bin2hex(openssl_random_pseudo_bytes(10)) . '.png';
                    $path = storage_path() . '/' . $name;
                    $frame->save($path);

                    Storage::disk('s3')->putFileAs(
                        'trips/' . $trip_id . '/thumb/',
                        new UploadedFile($path, $name),
                        $name,
                        'public'
                    );

                    $thumb_file_url = 'https://s3.amazonaws.com/travooo-images2/trips/' . $trip_id . '/thumb/' . $name;

                    unlink($path);
                }
            } else {
                //$originalImg = Storage::disk('s3')->get('trips/' . $trip_id . '/' . $filename);

                $cropped = \Intervention\Image\ImageManagerStatic::make($media_file)->orientate()->resize(180, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                Storage::disk('s3')->put(
                    'trips/' . $trip_id . '/th180/' . $filename,
                    $cropped->encode(),
                    'public'
                );

                $thumb_file_url = 'https://s3.amazonaws.com/travooo-images2/trips/' . $trip_id . '/th180/' . $filename;
            }

            $trip_media = new Media();
            $trip_media->url = $media_file_url;
            $trip_media->type = $type;
            $trip_media->source_url = $thumb_file_url;
            $trip_media->users_id = $user_id;

            if ($type === Media::TYPE_VIDEO) {
                $trip_media->video_thumbnail_url = $thumb_file_url;
            }

            if (!$trip_media->save()) {
                return json_encode([
                    'status' => 'failed'
                ]);
            }
        }

        $rankingService->addPointsEarners($trip_media->id, get_class($trip_media), $user_id);

        return new JsonResponse([
            'url' => $thumb_file_url,
            'id' => $trip_media->getKey()
        ]);
    }

    public function deleteTripPlaceMedia(Request $request, TripInvitationsService $tripInvitationsService)
    {
        $mediaId = $request->get('media_id');
        $tripId = $request->get('trip_id');
        $placeId = $request->get('place_id');

        if (!in_array($tripInvitationsService->getUserRole($tripId, auth()->id()), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN],
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
        ])) {
            throw new AuthorizationException('', 403);
        }

        if (!$mediaId) {
            throw new \Exception('', 400);
        }

        $tripMedia = TripMedias::query()->where([
            'places_id' => $placeId,
            'trips_id' => $tripId,
            'medias_id' => $mediaId
        ]);

        if ($tripMedia) {
            $tripMedia->delete();
        }

        $media = Media::find($mediaId);

        if (!$media) {
            throw new ModelNotFoundException('');
        }

        $media->delete();

        return json_encode([
            'status' => 'success'
        ]);
    }

    public function postLikeUnLike(Request $request, RankingService $rankingService)
    {
        $post_id = $request->id;
        // $type = $request->type;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = TripPlans::find($post_id)->users_id;
        if (isset($request->place_id)) {
            $check_like_exists = TripsLikes::where('trips_id', $post_id)->where('places_id', $request->place_id)->where('users_id', $user_id)->get()->first();
        } else {
            $check_like_exists = TripsLikes::where('trips_id', $post_id)->where('users_id', $user_id)->whereNull('places_id')->get()->first();
        }
        $data = array();

        if (is_object($check_like_exists)) {
            $check_like_exists->delete();
            log_user_activity('Trip', 'unlike', $post_id);
            updatePostRanks($post_id, CommonConst::TYPE_TRIP, CommonConst::ACTION_UNLIKE);

            $data['status'] = 'no';
            $rankingService->subPointsFromEarners($check_like_exists->trip);
        } else {
            $like = new TripsLikes;
            $like->trips_id = $post_id;
            $like->users_id = $user_id;
            if (isset($request->place_id)) {
                $like->places_id = $request->place_id;
            }
            $like->save();

            log_user_activity('Trip', 'like', $post_id);
            updatePostRanks($post_id, CommonConst::TYPE_TRIP, CommonConst::ACTION_LIKE);
            // notify($author_id, 'status_like', $post_id);

            $rankingService->addPointsToEarners($like->trip);
            $data['status'] = 'yes';
        }

        if (isset($request->place_id)) {
            $data['count'] = count(TripsLikes::where('trips_id', $post_id)->where('places_id', $request->place_id)->get());
        } else {
            $data['count'] = count(TripsLikes::where('trips_id', $post_id)->whereNull('places_id')->get());
        }

        return json_encode($data);
    }

    public function postComment(Request $request)
    {
        $post_id = $request->plan_id;
        $pair = $request->pair;
        $post_comment = processString($request->text);
        $user_id = Auth::guard('user')->user()->id;
        $author_id = TripPlans::find($post_id)->users_id;
        $user_name = Auth::guard('user')->user()->name;

        $post_comment = is_null($post_comment) ? '' : $post_comment;

        $is_files = false;
        $file_lists = getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;

        if (!$is_files && $post_comment == "")
            return 0;

        $post = TripPlans::find($post_id);
        if (is_object($post)) {
            $new_comment = new TripsComments;
            $new_comment->users_id = $user_id;
            $new_comment->trips_id = $post_id;
            $new_comment->text = $post_comment;
            if ($new_comment->save()) {
                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('trip-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/trip-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $post_comment;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->users_id = $user_id;
                    $media->type  = getMediaTypeByMediaUrl($media->url);
                    $media->save();

                    $trip_comments_medias = new TripsCommentsMedias();
                    $trip_comments_medias->trips_comments_id = $new_comment->id;
                    $trip_comments_medias->medias_id = $media->id;
                    $trip_comments_medias->save();
                }

                return view('site.home.partials.trip_comment_block', array('comment' => $new_comment, 'plan' => $post));
            } else {
                return 0;
            }
        }
    }

    public function postCommentReply(Request $request)
    {
        $post_id = $request->post_id;
        $pair = $request->pair;
        $comment_id = $request->comment_id;

        $post_comment = processString($request->text);
        $user_id = Auth::guard('user')->user()->id;
        $author_id = TripPlans::find($post_id)->users_id;
        $user_name = Auth::guard('user')->user()->name;

        $post_comment = is_null($post_comment) ? '' : $post_comment;

        $is_files = false;
        $file_lists = getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;

        if (!$is_files && $post_comment == "")
            return 0;

        $post = TripPlans::find($post_id);
        if (is_object($post)) {
            $new_comment = new TripsComments;
            $new_comment->users_id = $user_id;
            $new_comment->trips_id = $post_id;
            $new_comment->parents_id = $comment_id;
            $new_comment->text = $post_comment;
            if ($new_comment->save()) {
                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('trip-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/trip-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $post_comment;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->users_id = $user_id;
                    $media->type  = getMediaTypeByMediaUrl($media->url);
                    $media->save();

                    $trip_comments_medias = new TripsCommentsMedias();
                    $trip_comments_medias->trips_comments_id = $new_comment->id;
                    $trip_comments_medias->medias_id = $media->id;
                    $trip_comments_medias->save();
                }

                return view('site.home.partials.trip_comment_reply_block', array('child' => $new_comment, 'plan' => $post));
            } else {
                return 0;
            }
        }
    }

    public function postCommentEdit(Request $request)
    {
        $pair = $request->pair;
        $trip_id = $request->post_id;
        $comment_id = $request->comment_id;

        $trip_comment_edit = processString($request->text);
        $user_id = Auth::guard('user')->user()->id;
        $author_id = TripPlans::find($trip_id)->users_id;
        $user_name = Auth::guard('user')->user()->name;

        $trip_comment = TripsComments::find($comment_id);

        $is_files = false;
        $file_lists = getTempFiles($pair);

        $is_files = count($file_lists) > 0 ? true : false;
        $trip_comment_edit = is_null($trip_comment_edit) ? '' : $trip_comment_edit;
        if (!$is_files && $trip_comment_edit == "")
            return 0;

        $trip = TripPlans::find($trip_id);
        if (is_object($trip)) {
            $trip_comment->text = $trip_comment_edit;
            if ($trip_comment->save()) {
                log_user_activity('Status', 'comment', $trip_id);
                if ($user_id != $trip_comment->users_id) {
                    notify($author_id, 'status_comment', $trip_id);
                }

                if (isset($request->existing_media) && count($trip_comment->medias) > 0) {
                    foreach ($trip_comment->medias as $media_list) {
                        if (in_array($media_list->media->id, $request->existing_media)) {
                            $medias_exist = Media::find($media_list->medias_id);

                            $media_list->delete();

                            if ($medias_exist) {
                                $amazonefilename = explode('/', $medias_exist->url);
                                Storage::disk('s3')->delete('trip-comment-photo/' . end($amazonefilename));

                                $medias_exist->delete();
                            }
                        }
                    }
                }



                foreach ($file_lists as $file) {
                    $filename = $user_id . '_' . time() . '_' . $file[1];
                    Storage::disk('s3')->put('trip-comment-photo/' . $filename, fopen($file[0], 'r+'),  'public');

                    $path = 'https://s3.amazonaws.com/travooo-images2/trip-comment-photo/' . $filename;

                    $media = new Media();
                    $media->url = $path;
                    $media->author_name = $user_name;
                    $media->title = $trip_comment_edit;
                    $media->author_url = '';
                    $media->source_url = '';
                    $media->license_name = '';
                    $media->license_url = '';
                    $media->uploaded_at = date('Y-m-d H:i:s');
                    $media->users_id = $user_id;
                    $media->type  = getMediaTypeByMediaUrl($media->url);
                    $media->save();


                    $trip_comments_medias = new TripsCommentsMedias();
                    $trip_comments_medias->trips_comments_id = $trip_comment->id;
                    $trip_comments_medias->medias_id = $media->id;
                    $trip_comments_medias->save();
                }

                $trip_comment->load('medias');

                if ($request->comment_type == 1) {
                    return view('site.home.partials.trip_comment_block', array('comment' => $trip_comment, 'plan' => $trip));
                } else {
                    return view('site.home.partials.trip_comment_reply_block', array('child' => $trip_comment, 'plan' => $trip));
                }
            } else {
                return 0;
            }
        }
    }

    public function postComment4Tripbox(Request $request)
    {
        $post_id = $request->post_id;
        $post_comment = processString($request->text);
        $user_id = Auth::guard('user')->user()->id;
        $author_id = TripPlans::find($post_id)->users_id;

        $post_comment = is_null($post_comment) ? '' : $post_comment;

        if ($post_comment == "")
            return 0;

        $post = TripPlans::find($post_id);
        if (is_object($post)) {
            $new_comment = new TripsComments;
            $new_comment->users_id = $user_id;
            $new_comment->trips_id = $post_id;
            $new_comment->text = $post_comment;
            if ($new_comment->save()) {

                return '<div class="comment">
                <img class="comment__avatar" src="' . check_profile_picture($new_comment->author->profile_picture) . '" alt="" role="presentation" />
                <div class="comment__content">
                  <div class="comment__header"><a class="comment__username" href="' . url_with_locale('profile/' . $new_comment->author->id) . '">' . $new_comment->author->name . '</a>
                    <div class="comment__posted">' . diffForHumans($new_comment->created_at) . '</div>
                  </div>
                  <div class="comment__text">' . $new_comment->text . '</div>
                  <div class="comment__footer" posttype="Tripcomment">
                    <button class="comment__flag-btn" type="button" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(' . $new_comment->id . ',this)"><svg class="icon icon--flag">
                        <use xlink:href="' . asset('assets3/img/sprite.svg#flag') . '"></use>
                      </svg></button>
                  </div>
                </div>
              </div>';
            } else {
                return 0;
            }
        }
    }

    public function postCommentDelete(Request $request)
    {
        $comment_id = $request->comment_id;

        $comment = TripsComments::find($comment_id);

        foreach ($comment->medias as $com_media) {
            $medias_exist = Media::find($com_media->medias_id);

            $com_media->delete();

            if ($medias_exist) {
                $amazonefilename = explode('/', $medias_exist->url);
                Storage::disk('s3')->delete('trip-comment-photo/' . end($amazonefilename));

                $medias_exist->delete();
            }
        }


        if ($comment->delete()) {
            return 1;
        } else {
            return 0;
        }
    }


    public function postCommentLikeUnlike(Request $request)
    {

        $comment_id = $request->comment_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = TripsComments::find($comment_id)->users_id;
        $trip_id = TripsComments::find($comment_id)->trips_id;
        $trip_author_id = TripPlans::find($trip_id)->users_id;
        $check_like_exists = TripsCommentsLikes::where('trips_comments_id', $comment_id)->where('users_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_like_exists)) {
            //dd($check_like_exists);
            $check_like_exists->delete();
            log_user_activity('Trip', 'commentunlike', $comment_id);

            $data['status'] = 'no';
        } else {
            $like = new TripsCommentsLikes;
            $like->trips_comments_id = $comment_id;
            $like->users_id = $user_id;
            $like->save();

            log_user_activity('Trip', 'commentlike', $comment_id);
            if ($user_id != $author_id) {
                notify($author_id, 'status_commentlike', $comment_id);
            }

            $data['status'] = 'yes';
        }
        $data['count'] = count(TripsCommentsLikes::where('trips_comments_id', $comment_id)->get());
        $data['name'] = Auth::guard('user')->user()->name;
        return json_encode($data);
    }



    public function postShareUnshare(Request $request)
    {

        $trip_id = $request->trip_id;
        $user_id = Auth::guard('user')->user()->id;
        $author_id = TripPlans::find($trip_id)->users_id;
        $check_share_exists = TripsShares::where('trip_id', $trip_id)->where('user_id', $user_id)->get()->first();
        $data = array();

        if (is_object($check_share_exists)) {
            $shareid = $check_share_exists->id;
            $check_share_exists->delete();
            log_user_activity('TripPlan', 'unshare', $trip_id);
            // notify($author_id, 'status_unlike', $trip_id);

            $data['status'] = 'no';
            // $data['rowId'] = $shareid;
        } else {
            $share              = new TripsShares;
            $share->trip_id     = $trip_id;
            $share->user_id     = $user_id;
            $share->created_at  = date("Y-m-d H:i:s");
            $share->save();

            log_user_activity('TripPlan', 'share', $trip_id);

            $data['status'] = 'yes';
        }

        $data['count'] = count(TripsShares::where('trip_id', $trip_id)->get());
        return json_encode($data);
    }

    public function postTripDistance(Request $request)
    {
        $trip_place_info = TripPlaces::where('trips_id', $request->trip_id)->get();
        if (count($trip_place_info) > 0) {
            foreach ($trip_place_info as $place) {
                $place->update(['travel_duration' => null, 'distance' => null]);
            }
        }

        $trip_city_info = TripCities::where('trips_id', $request->trip_id)->get();
        if (count($trip_place_info) > 0) {
            foreach ($trip_city_info as $city) {
                $city->update(['duration' => null, 'distance' => null]);
            }
        }

        if (isset($request->trip_places)) {
            foreach ($request->trip_places as $trip_places) {
                foreach ($trip_places as $place_id => $distance) {
                    $tripPlace = TripPlaces::where('trips_id', $request->trip_id)->where('places_id', $place_id)->first();
                    $tripPlace->travel_duration = ($request->traval_mode == 'PLANE') ? $distance['duration'] : $distance['duration']['value'];
                    $tripPlace->distance = ($request->traval_mode == 'PLANE') ? $distance['distance'] : $distance['distance']['value'];
                    $tripPlace->save();
                }
            }
        }

        if (isset($request->trip_cities)) {
            foreach ($request->trip_cities as $trip_cities) {
                foreach ($trip_cities as $city_id => $city_distance) {
                    $tripCity = TripCities::where('trips_id', $request->trip_id)->where('cities_id', $city_id)->first();
                    $tripCity->duration = $city_distance['duration'];
                    $tripCity->distance = $city_distance['distance'];
                    $tripCity->transportation = $request->traval_mode;
                    $tripCity->save();
                }
            }
        }

        echo 'ok';
    }

    public function postTripDescription(Request $request)
    {

        $description_photos = [];
        if (isset($request->places_photo) && !empty($request->places_photo)) {
            foreach ($request->places_photo as $k => $photo) {
                $filename = time() . $k . $photo->getClientOriginalName();
                Storage::disk('s3')->putFileAs(
                    'trip_description_photo/',
                    $photo,
                    $filename,
                    'public'
                );
                $description_photos[] = 'https://s3.amazonaws.com/travooo-images2/trip_description_photo/' . $filename;
            }
        }
        if (isset($request->selected_places_photo) && !empty($request->selected_places_photo)) {
            foreach ($request->selected_places_photo as $selected_photo) {
                $description_photos[] = $selected_photo;
            }
        }
        if ($request->description_id != '') {
            $trip_description = TripsDescriptions::find($request->description_id);
        } else {
            $trip_description = new TripsDescriptions;
        }

        $trip_description->trips_id = $request->trips_id;
        $trip_description->places_id = $request->place_id;
        $trip_description->users_id = Auth::guard('user')->user()->id;
        $trip_description->description = processString($request->description);
        $trip_description->images = implode(",", $description_photos);
        $trip_description->save();

        return redirect()->back();
    }

    public function posAjaxTripDescription(Request $request)
    {
        if (isset($request->description_id)) {
            $description_info = TripsDescriptions::find($request->description_id);
            $description_photos = explode(',', $description_info->images);
            $result = "";
            $i = 1;
            foreach ($description_photos as $photo) {
                $image_div_id = 'add_image_cont' . $i;
                $result .= '<div class="create-row ui-state-default add_image_cont" id="' . $image_div_id . '" style="border:none;background:none;margin-top:20px;margin-bottom:20px;float:left;width:33%;">
                                <div class="content">
                                    <input type="file" class="file-input tripDescImage" data-id="' . $i . '" id="tripDescImage' . $i . '" name="places_photo[]"  style="display:none;">
                                    <input type="hidden" name="selected_places_photo[]" value="' . $photo . '">
                                            <img id="add_image_src' . $i . '" src="' . $photo . '" alt="image" width="188px" max-heght="183px" />
                                </div>
                                <div class="side right" style="left: 65%;top: 10px;">
                                    <ul class="video-upload-control">
                                        <li>
                                            <a href="#" onclick="document.getElementById(&apos;' . $image_div_id . '&apos;).remove();">
                                                <div class="close-handle red">
                                                    <i class="trav-close-icon"></i>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>';
                $i++;
            }

            $ret = array('status' => 'success', 'description' => $description_info->description, 'result' => $result);

            return json_encode($ret);
        }
    }

    public function postAjaxAddComment(Request $request)
    {
        $user_id = Auth::guard('user')->user()->id;
        $author_id = TripPlans::find($request->trip_id)->users_id;

        $trip_comment = new TripsComments;
        $trip_comment->trips_id = $request->trip_id;
        $trip_comment->places_id = $request->place_id;
        $trip_comment->descriptions_id = $request->description_id;
        $trip_comment->users_id = Auth::guard('user')->user()->id;
        $trip_comment->text = $request->comment;

        if ($trip_comment->save()) {
            $data['error'] = 0;
            $data['trip_comment_id'] = $trip_comment->id;
            $data['trip_comment'] = $this->_construct_comment($trip_comment->id);
        } else {
            $data['error'] = 1;
        }

        return json_encode($data);
    }

    public function _construct_comment($trip_comment_id)
    {
        $trip_comment = TripsComments::find($trip_comment_id);

        $str = '<div class="post-comment-row comment-content" topsort="0" newsort="' . strtotime($trip_comment->created_at) . '">
                    <div class="post-com-avatar-wrap">
                        <img src="' . check_profile_picture($trip_comment->author->profile_picture) . '" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="' . url("profile/" . $trip_comment->author->id) . '" class="comment-name">' . $trip_comment->author->name . '</a>
                            <span class="com-time">' . diffForHumans($trip_comment->created_at) . '</span>
                        </div>
                        <div class="comment-txt">
                            <p>' . $trip_comment->text . '</p>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="com-reaction">
                                <div class="tips-footer updownvote">
                                  <a href="#" class="upvote-link trip-comment-vote up disabled" id="' . $trip_comment->id . '">
                                    <span class="arrow-icon-wrap"><i class="trav-angle-up"></i></span>
                                </a>
                                <span class="upvote-' . $trip_comment->id . '" style="font-size:85%"><b>0</b> Upvotes</span>
                                &nbsp;&nbsp;
                                <a href="#" class="upvote-link trip-comment-vote down disabled" id="' . $trip_comment->id . '">
                                    <span class="arrow-icon-wrap"><i class="trav-angle-down"></i></span>
                                </a>
                                <span class="downvote-' . $trip_comment->id . '" style="font-size:85%"><b>0</b> Downvotes</span>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>';

        return $str;
    }

    public function postCommentUpDownVotes(Request $request)
    {
        $comment_id = $request->comment_id;
        $user_id = Auth::guard('user')->user()->id;

        $comment = TripsComments::find($comment_id);
        if (count($comment->updownvotes()->where('users_id', $user_id)->where('vote_type', $request->vote_type)->get()) == 0) {
            $commentUpvote = new TripsCommentsVotes;
            $commentUpvote->comment_id = $comment_id;
            $commentUpvote->users_id = $user_id;
            $commentUpvote->vote_type = $request->vote_type;
            $commentUpvote->save();




            if ($request->vote_type == 1) {
                if (count($comment->updownvotes()->where('users_id', $user_id)->get()) != 0) {
                    $comment->updownvotes()->where('users_id', $user_id)->where('vote_type', 2)->delete();
                }
            } else {
                if (count($comment->updownvotes()->where('users_id', $user_id)->get()) != 0) {
                    $comment->updownvotes()->where('users_id', $user_id)->where('vote_type', 1)->delete();
                }
            }
            return json_encode(array(
                'status' => 1,
                'count_upvotes' => count($comment->updownvotes()->where('vote_type', 1)->get()),
                'count_downvotes' => count($comment->updownvotes()->where('vote_type', 2)->get())
            ));
        } else {
            $comment->updownvotes()->where('users_id', $user_id)->where('vote_type', $request->vote_type)->delete();

            return json_encode(array(
                'status' => 1,
                'count_upvotes' => count($comment->updownvotes()->where('vote_type', 1)->get()),
                'count_downvotes' => count($comment->updownvotes()->where('vote_type', 2)->get())
            ));
        }
    }

    public function getCommentLikeUsers(Request $request)
    {
        $comment_id = $request->comment_id;
        $comment = TripsComments::find($comment_id);


        if ($comment->likes) {
            return view('site.home.partials.modal_comments_like_block', array('likes' => $comment->likes()->orderBy('created_at', 'DESC')->get()));
        }
    }

    /**
     * @param Request $request
     * @param TripsService $tripsService
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @return AjaxResponse
     */
    public function saveDraft(Request $request, TripsService $tripsService, TripsSuggestionsService $tripsSuggestionsService)
    {
        $planId = $request->get('plan_id');

        $plan = $tripsService->findPlan($planId);

        if (!$plan) {
            return new AjaxResponse(['error' => 'Plan not found'], false, 404);
        }

        $tripsSuggestionsService->saveChanges($plan);

        return new AjaxResponse();
    }



    /**
     * @param Request $request
     * @param TripsService $tripsService
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @return AjaxResponse
     * @throws \Exception
     */
    public function cancelChanges(Request $request, TripsService $tripsService, TripsSuggestionsService $tripsSuggestionsService, TripInvitationsService $tripInvitationsService)
    {
        $planId = $request->get('plan_id');

        $plan = $tripsService->findPlan($planId);

        if (!$plan) {
            return new AjaxResponse(['error' => 'Plan not found'], false, 404);
        }

        if (!in_array($tripInvitationsService->getUserRole($planId, auth()->id()), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN],
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
        ])) {
            throw new AuthorizationException('', 403);
        }

        $tripsSuggestionsService->cancelChanges($plan);

        return new AjaxResponse();
    }

    /**
     * @param Request $request
     * @param TripsService $tripsService
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @return AjaxResponse
     * @throws \Exception
     */
    public function undoChanges(Request $request, TripsService $tripsService, TripsSuggestionsService $tripsSuggestionsService, TripInvitationsService $tripInvitationsService)
    {
        $planId = $request->get('plan_id');

        $plan = $tripsService->findPlan($planId);

        if (!$plan) {
            return new AjaxResponse(['error' => 'Plan not found'], false, 404);
        }

        if (!in_array($tripInvitationsService->getUserRole($planId, auth()->id()), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN],
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
        ])) {
            throw new AuthorizationException('', 403);
        }

        $result = $tripsSuggestionsService->undoChanges($plan);

        if ($result === false) {
            return new AjaxResponse(['error' => 'No changes'], false, 404);
        }

        return new AjaxResponse([
            'count' => $result
        ]);
    }

    /**
     * @param Request $request
     * @param TripInvitationsService $tripInvitationsService
     * @return AjaxResponse
     * @throws \Exception
     */
    public function ajaxChangeInvitationRequestRole(Request $request, TripInvitationsService $tripInvitationsService)
    {
        $userId = $request->get('user_id');
        $planId = $request->get('plan_id');
        $role = $request->get('role');

        if (!in_array($tripInvitationsService->getUserRole($planId, auth()->id()), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN]
        ])) {
            throw new AuthorizationException('', 403);
        }

        $tripInvitationsService->changeRole($userId, $planId, $role);

        return new AjaxResponse();
    }

    /**
     * @param Request $request
     * @param TripsService $tripsService
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @return AjaxResponse
     */
    public function sendSuggestions(Request $request, TripsService $tripsService, TripsSuggestionsService $tripsSuggestionsService, TripInvitationsService $tripInvitationsService)
    {
        $planId = $request->get('plan_id');

        $plan = $tripsService->findPlan($planId);

        if (!$plan) {
            return new AjaxResponse(['error' => 'Plan not found'], false, 404);
        }

        if (!in_array($tripInvitationsService->getUserRole($planId, auth()->id()), [
            TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]
        ])) {
            throw new AuthorizationException('', 403);
        }

        $tripsSuggestionsService->sendSuggestions($planId);

        return new AjaxResponse();
    }

    /**
     * @param Request $request
     * @param PlanActivityLogService $planActivityLogService
     * @return AjaxResponse
     */
    public function getPlanActivityLog(Request $request, PlanActivityLogService $planActivityLogService, TripInvitationsService $invitationsService)
    {
        $planId = $request->get('plan_id');

        $role = $invitationsService->getUserRole($planId, auth()->id());

        if (!$role || !in_array($role, [TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_ADMIN], TripInvitationsService::ROLES_MAPPER[TripInvitationsService::ROLE_EDITOR]])) {
            throw new AuthorizationException('', 403);
        }

        return new AjaxResponse($planActivityLogService->getPlanActivityLogList($planId));
    }

    /**
     * @param Request $request
     * @param TripsSuggestionsService $tripsSuggestionsService
     * @return AjaxResponse
     */
    public function postTripPlaceLikeUnlike(Request $request, TripsSuggestionsService $tripsSuggestionsService, RankingService $rankingService)
    {
        $data['status'] = 'yes';

        $tripPlaceId = $request->place_id;
        $userId = Auth::id();

        $allLikes = TripsLikes::query()->whereIn('places_id', $tripsSuggestionsService->getAllSuggestionPackTripPlacesIds($tripPlaceId))->get();
        $userLikes = $allLikes->where('users_id', $userId)->first();

        if ($userLikes) {
            $userLikes->delete();
            $rankingService->subPointsFromEarners($userLikes->tripPlace);

            $data['status'] = 'no';
            $data['count'] = $allLikes->count() - 1;


            try {
                broadcast(new TripPlaceStepLikeEvent($tripPlaceId));
            } catch (\Throwable $e) {
            }
            try {
                broadcast(new TripPlaceStepLikeApiEvent($tripPlaceId));
            } catch (\Throwable $e) {
            }

            return new AjaxResponse($data);
        }

        $like = new TripsLikes();
        $like->users_id = $userId;
        $like->places_id = $tripPlaceId;
        $like->save();

        $rankingService->addPointsToEarners($like->tripPlace);

        $data['count'] = $allLikes->count() + 1;


        try {
            broadcast(new TripPlaceStepLikeEvent($tripPlaceId));
        } catch (\Throwable $e) {
        }
        try {
            broadcast(new TripPlaceStepLikeApiEvent($tripPlaceId));
        } catch (\Throwable $e) {
        }

        return new AjaxResponse($data);
    }

    public function getTripPlaceLikesData($tripPlaceId, TripsSuggestionsService $tripsSuggestionsService)
    {
        $userId = Auth::id();
        $allLikes = TripsLikes::query()->whereIn('places_id', $tripsSuggestionsService->getAllSuggestionPackTripPlacesIds($tripPlaceId))->get();

        return [
            'count' => $allLikes->count(),
            'is_my_like' => $allLikes->contains('users_id', $userId)
        ];
    }

    public function getTrendingLocations(Request $request, TrendsService $trendsService)
    {
        $this->validate($request, [
            'location' => 'numeric',
            'parent_id' => 'numeric',
            'except' => 'array',
            'nearby' => 'bool',
            'latlng' => 'array'
        ]);

        $location = (int)$request->get('location');
        $parentId = $request->get('parent_id');
        $except = $request->get('except', []);
        $nearby = $request->get('nearby', false);
        $latlng = $request->get('latlng', []);
        $distance = $request->get('distance', 3);
        $withNotTrending = $request->get('with_not_trendings', false);

        ini_set('memory_limit', '512M');

        $trendsService->withNotTrendingCities = $withNotTrending;
        $locations = $trendsService->getRecursiveTrendingLocations($location, $parentId, $nearby, $except, $latlng, $distance);

        return new AjaxResponse($locations);
    }
}
