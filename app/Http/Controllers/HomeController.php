<?php

namespace App\Http\Controllers;

use App\Models\Activity\Activity;
use App\Models\City\CitiesTranslations;
use App\Models\Place\PlaceFollowers;
use App\Models\Place\PlaceTranslations;
use App\Models\Posts\Checkins;
use App\Models\Posts\PostsMedia;
use App\Models\Posts\PostsPlaces;
use App\Models\Reviews\Reviews;
use App\Models\TripPlans\TripsContributionRequests;
use App\Models\TripPlans\TripsSuggestion;
use App\Models\User\UsersFriends;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Trends\TrendsService;
use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsService;
use App\Services\Trips\TripsSuggestionsService;
use App\Models\Reports\ReportsInfos;
use App\Services\Reports\ReportsService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Place\Place;
use App\Models\Posts\Posts;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\City\CitiesFollowers;
use App\Models\Notifications\Notifications;
use App\Models\TripPlans\TripPlans;
use App\Models\TripPlans\TripsVersions;
use App\Models\PlacesTop\PlacesTop;
use Illuminate\Support\Facades\DB;
use elegisandi\AWSElasticsearchService\Facades\ElasticSearch;
use App\Models\User\User;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\Hotels\Hotels;
use App\Models\Restaurants\Restaurants;
use App\Models\Referral\ReferralLinks;
use App\Models\Referral\ReferralLinksViews;
use App\Models\Discussion\Discussion;
use App\Models\Discussion\DiscussionReplies;
use App\Models\Chat\ChatConversation;
use \DOMDocument;
use App\Models\TripMedias\TripMedias;
use Illuminate\Support\Arr;
use App\Models\ActivityMedia\Media;
use App\Models\City\CitiesMedias;
use App\Models\City\CitiesShares;
use App\Models\Country\CountriesCapitals;
use App\Models\Country\CountriesFollowers;
use App\Models\Country\CountriesTranslations;
use App\Models\States\States;
use App\Models\States\StateTranslation;
use App\Models\Place\PlaceMedias;
use App\Models\Hotels\HotelsMedias;
use App\Models\Restaurants\RestaurantsMedias;
use App\Models\TripPlaces\TripPlaces;
use App\Models\Posts\PostsCountries;
use App\Models\Posts\PostsCities;
use App\Models\TopCities\TopCities;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use Google\Cloud\Translate\V2\TranslateClient;
use App\Models\Events\Events;
use App\Models\Events\EventCategories;
use App\Models\Events\EventCities;
use App\Models\TripCities\TripCities;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsLikes;
use App\Models\AdminLogs\AdminLogs;
use App\Models\User\UsersContentLanguages;
use App\Services\Translations\TranslationService;
use App\Models\Posts\PostsLikes;
use App\Models\Discussion\DiscussionLikes;
use App\Models\TripPlans\TripsLikes;
use App\Models\City\CitiesHolidays;
use App\Models\Holidays\HolidaysTranslations;
use App\Models\Holidays\HolidaysMedias;
use App\Models\Country\CountriesHolidays;
use App\Models\User\UserWatchHistory;
use App\Services\Api\PrimaryPostService;
use App\Models\TripPlans\SuggestionUserLog;
use Response;
use App\Http\Responses\AjaxResponse;
use App\Services\Newsfeed\HomeService;

class HomeController extends Controller
{

    protected  $placeTypesArray = [
        'point_of_interest',
        'airport',
        'amusement_park',
        'aquarium',
        'art_gallery',
        'bakery',
        'bar',
        'cafe',
        'campground',
        'casino',
        'cemetery',
        'church',
        'city_hall',
        'embassy',
        'hindu_temple',
        'library',
        'light_rail_station',
        'lodging',
        'meal_delivery',
        'meal_takeaway',
        'mosque',
        'movie_theater',
        'museum',
        'night_club',
        'park',
        'restaurant',
        'shopping_mall',
        'stadium',
        'subway_station',
        'synagogue',
        'tourist_attraction',
        'train_station',
        'transit_station',
        'zoo',
        'food',
        'place_of_worship',
        'natural_feature'

    ];

    public $newsfeedService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        HomeService $newsfeedService
    ) {
        $this->newsfeedService = $newsfeedService;
    }

    public function _getHotelsMedia($provider_id)
    {
        $json = file_get_contents('http://db.travoooapi.com/public/places/media/go/' . $provider_id);


        $response = unserialize($json);
        $error = $response['error'];
        if (isset($response['photos'])) {
            $photos = $response['photos'];
        } else {
            $photos = false;
        }

        $pwm = Hotels::where('provider_id', $provider_id)->get()->first();


        //echo $pwm->provider_id . ' ';

        if ($error == 1) {
            echo $error . "<br />";
            $pwm->media_done = -1;
            $pwm->save();
        } else {
            if ($photos && is_array($photos)) {
                foreach ($photos as $p) {

                    $check_media_exists = Media::where('sha1', $p['sha1'])
                        ->where('md5', $p['md5'])
                        ->where('filesize', $p['size'])
                        ->get()
                        ->count();
                    if ($check_media_exists == 0) {
                        $media_file = 'hotels/' . $pwm->provider_id . '/' . sha1(microtime()) . '.jpg';
                        Storage::disk('s3')->put($media_file, $p['contents'], 'public');

                        $media = new Media;
                        $media->url = $media_file;
                        $media->sha1 = $p['sha1'];
                        $media->filesize = $p['size'];
                        $media->md5 = $p['md5'];
                        $media->html_attributions = join(",", $p['html_attributions']);

                        if ($media->save()) {
                            $place_media = new HotelsMedias;
                            $place_media->hotels_id = $pwm->id;
                            $place_media->medias_id = $media->id;
                            $place_media->save();
                        }
                    }
                }
                //echo count($photos);
            }

            $pwm->media_done = 1;
            $pwm->save();
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showHome()
    {
        return redirect('/home');
    }

    private function _aasort(&$array, $key)
    {
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
    }

    private function _getRandom($key)
    {

        $init_array = [];
        for ($i = 1; $i < 32; $i++)
            $init_array[] = $i;

        $return = [];

        foreach ($init_array as $val) {
            $return[] = $val * $key % 32;
        }

        return implode(',', $return);
    }

    public function getIndex()
    {
        if (Auth::check()) {
            $data['old_content_flag'] = false;
            $events_activities_list = session('events_activites', []);
            //following for events
            $followed_cities_ids = [];
            $ip = getClientIP();
            $geoLoc = ip_details($ip);
            $getLocCity = $geoLoc['city'];
            $getLocCity = Cities::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($getLocCity) {
                    $q->where('title', 'LIKE', "%$getLocCity%");
                })
                ->first();

            if (isset($getLocCity)) {
                $followed_cities_ids[] = $getLocCity->id;
                $data['event_activity'] = $this->getEventForUser($followed_cities_ids);
            }
            if (empty($data['event_activity'])) {
                $followed_cities_ids = getUserFollwoingForEvents(Auth::user()->id);
                $data['event_activity'] = $this->getEventForUser($followed_cities_ids);
            }
            if (isset($data['event_activity']->id)) {
                $events_activities_list[] = $data['event_activity']->id;
            }

            // session()->forget(['events_activites']);
            $value2 = session(['events_activites' =>  $events_activities_list]);
            $scondary_post_collection = ['new_travellers', 'weather_for_trip', 'weather', 'following_locations', 'collective_following_location', 'video_you_might_like', 'recommended_places', 'people_you_might_know'];
            shuffle($scondary_post_collection);
            session()->forget(['secondary_post_style']);
            $value1 = session(['secondary_post_style' =>  $scondary_post_collection]);


            //language script 
            $user_lang  = UsersContentLanguages::where('users_id', Auth::user()->id)->orderBy('id', 'desc')->get();
            $getLangids = [];
            $counter =  $user_lang->count();
            if ($user_lang) {
                foreach ($user_lang as $u_lang) {
                    $getLangids[] = $u_lang->languages->id;
                }
            }
            if (session('ratio_language')) {
                session()->forget(['ratio_language']);
            }
            $value = session(['ratio_language' =>  $getLangids]);


            $places = PlaceFollowers::query()
                ->where('users_id', auth()->user()->id)
                ->with('place')
                ->whereHas('place')
                ->get();

            $countries = CountriesFollowers::query()
                ->where('users_id', auth()->user()->id)
                ->with('country')
                ->whereHas('country')
                ->get();

            $cities = CitiesFollowers::query()
                ->where('users_id', auth()->user()->id)
                ->with('city')
                ->whereHas('city')
                ->get();
            $places_ids = $places->pluck('places_id');
            $countries_ids = $countries->pluck('countries_id');
            $cities_ids = $cities->pluck('cities_id');

            $following_locations = [
                'places_ids' => $places_ids,
                'countries_ids' => $countries_ids,
                'cities_ids' => $cities_ids,
            ];

            $data['following_locations'] = $following_locations;

            // $data['activity_logs'] = ActivityLog::orderBy('id', 'DESC')
            //     ->with(['event' => function ($q) use ($places_ids, $countries_ids, $cities_ids) {
            //         $q->where(function ($query) use ($places_ids, $countries_ids, $cities_ids) {
            //             $query->whereIn('places_id', $places_ids->all())
            //                 ->orWhereIn('countries_id', $countries_ids->all())
            //                 ->orWhereIn('cities_id', $cities_ids->all());
            //         })
            //             ->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')
            //             ->with(['place.reviews', 'country.reviews', 'city.reviews'])
            //             ->orderBy('id', 'desc')
            //             ->get();
            //     }])
            //     ->take(200)
            //     ->get();
            $data['activity_logs'] = [];

            $data['posts'] = $this->newsfeedService->getPrimaryPosts(1, 15)->shuffle();

            $cities_followers = $cities->pluck('cities_id')->toArray();
            $citiesHolidays = CitiesHolidays::whereIn('cities_id', $cities_followers)->where([['date', '>', date('U')], ['date', '<=', date('U', strtotime('+7 days'))]])->inRandomOrder()->first();
            if (isset($citiesHolidays->cities_id)) {
                if (in_array($citiesHolidays->cities_id, $cities_followers)) {
                    $cityInCountry_id = Cities::where('id', $citiesHolidays->cities_id)->first()->countries_id;
                    $city_trans = CitiesTranslations::where('cities_id', $citiesHolidays->cities_id)->first(['title'])->toArray();
                    $country_trans = CountriesTranslations::where('countries_id', $cityInCountry_id)->first(['countries_id', 'title AS country_title'])->toArray();
                    $citiesHolidays['count_city_folower'] = $cities->where('cities_id', $citiesHolidays->cities_id)->count();
                    $citiesHolidays['iso_code'] = Countries::where('id', $cityInCountry_id)->first()->iso_code;
                    $citiesHolidays['city_trans'] = array_merge($city_trans, $country_trans);
                    $citiesHolidays['trans'] = HolidaysTranslations::where('holidays_id', $citiesHolidays->holidays_id)->first()->toArray();
                    $citiesHolidays['type'] = "city";
                    $medias_id = HolidaysMedias::where('holidays_id', $citiesHolidays->holidays_id)->first();
                    if (!$medias_id) {
                        $citiesHolidays['media'] = "https://s3.amazonaws.com/travooo-images2/th700/places/ChIJVYmnYM9aDDUR4jnTKUd1pwk/13faa83ee5565ad8017c41d68677ea1e829d4f3b.jpg";
                    } else {
                        $citiesHolidays['media'] = Media::where('id', $medias_id->medias_id)->first()->url;
                    }
                    $data['posts']->push($citiesHolidays);
                }
            }
            $countries_followers = $countries->pluck('countries_id')->toArray();
            $countriesHolidays = CountriesHolidays::whereIn('countries_id', $countries_followers)->where([['date', '>', date('U')], ['date', '<=', date('U', strtotime('+7 days'))]])->inRandomOrder()->first();
            if (isset($countriesHolidays->countries_id)) {
                if (in_array($countriesHolidays->countries_id, $countries_followers)) {
                    $countriesHolidays['count_country_folower'] = $countries->where('countries_id', $countriesHolidays->countries_id)->count();
                    $countriesHolidays['iso_code'] = Countries::where('id', $countriesHolidays->countries_id)->first()->iso_code;
                    $countriesHolidays['country_trans'] = CountriesTranslations::where('countries_id', $countriesHolidays->countries_id)->first(['title'])->toArray();
                    $countriesHolidays['trans'] = HolidaysTranslations::where('holidays_id', $countriesHolidays->holidays_id)->first()->toArray();
                    $countriesHolidays['type'] = "country";
                    $medias_id = HolidaysMedias::where('holidays_id', $countriesHolidays->holidays_id)->first();
                    if (!$medias_id) {
                        $countriesHolidays['media'] = "https://s3.amazonaws.com/travooo-images2/th700/places/ChIJVYmnYM9aDDUR4jnTKUd1pwk/13faa83ee5565ad8017c41d68677ea1e829d4f3b.jpg";
                    } else {
                        $countriesHolidays['media'] = Media::where('id', $medias_id->medias_id)->first()->url;
                    }
                    $data['posts']->push($countriesHolidays);
                }
            }

            if (count($data['posts']) > 0) {
                $frst_post_time = @$data['posts'][0]->time;
                $frst_post_time = Carbon::parse($frst_post_time);
                $now = Carbon::now()->toDateString();
                $different_days = $frst_post_time->diffInDays($now);
                if ($different_days > 1)
                    $data['old_content_flag'] = true;
            }

            $data['secondary_post_suggestion'] = '';
            if (count($data['posts']) == 0) {
                // new user signup suggestion posts time
                $second_suggestion = '';
                $final_array = $this->newsfeedService->userPlacesContent();
                if (!empty($final_array)) {
                    if (isset($post_to_print[HomeService::TYPE_POST])) {
                        $second_suggestion .= view('site.home.posts.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST], 'type' => 'Post'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_TRIP])) {
                        $second_suggestion .= view('site.home.posts.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP], 'type' => 'Trip'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_REPORT])) {
                        $second_suggestion .= view('site.home.posts.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT], 'type' => 'Report'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_DISCUSSION])) {
                        $second_suggestion .= view('site.home.posts.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION], 'type' => 'Discussion'], 'me' => Auth::guard('user')->user()]);
                    }
                }
                $data['secondary_post_suggestion'] .= $second_suggestion;
                //trending post;
                $post_to_print = $this->newsfeedService->getTrendingPosts(true);
                $shuffled_array = array();
                $keys = array_keys($post_to_print);
                shuffle($keys);
                foreach ($keys as $key) {
                    $shuffled_array[$key] = $post_to_print[$key];
                }
                $post_to_print = $shuffled_array;
                if (!empty($post_to_print)) {
                    if (isset($post_to_print[HomeService::TYPE_POST]->id)) {
                        $second_suggestion .= view('site.home.trendings.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST]->id, 'type' => 'Post'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_TRIP]->id)) {
                        $second_suggestion .= view('site.home.trendings.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP]->id, 'type' => 'Trip'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_REPORT]->id)) {
                        $second_suggestion .= view('site.home.trendings.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT]->id, 'type' => 'Report'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_DISCUSSION]->id)) {
                        $second_suggestion .= view('site.home.trendings.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION]->id, 'type' => 'Discussion'], 'me' => Auth::guard('user')->user()]);
                    }
                }
                $data['secondary_post'] = $second_suggestion;

                //All time trending 
                $second_suggestion = '';
                $post_to_print = $this->newsfeedService->getTrendingPosts(true, true);
                $shuffled_array = array();
                $keys = array_keys($post_to_print);
                shuffle($keys);
                foreach ($keys as $key) {
                    $shuffled_array[$key] = $post_to_print[$key];
                }
                $post_to_print = $shuffled_array;
                if (!empty($post_to_print)) {
                    if (isset($post_to_print[HomeService::TYPE_POST]->id)) {
                        $second_suggestion .= view('site.home.trendings.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST]->id, 'type' => 'Post'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_TRIP]->id)) {
                        $second_suggestion .= view('site.home.trendings.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP]->id, 'type' => 'Trip'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_REPORT]->id)) {
                        $second_suggestion .= view('site.home.trendings.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT]->id, 'type' => 'Report'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_DISCUSSION]->id)) {
                        $second_suggestion .= view('site.home.trendings.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION]->id, 'type' => 'Discussion'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                }
                $data['secondary_post'] .= $second_suggestion;
            } else {
                $second_suggestion = '';
                // random 'city', 'place', 'country'
                $rand_following_suggestion  = collect([HomeService::TYPE_CITY, HomeService::TYPE_PLACE, HomeService::TYPE_COUNTRY])->random();
                if ($rand_following_suggestion == HomeService::TYPE_CITY) {
                    $post_to_print = $this->newsfeedService->userCitiesContent();
                } else if ($rand_following_suggestion == HomeService::TYPE_COUNTRY) {
                    $post_to_print = $this->newsfeedService->userCountriesContent();
                } else {
                    $post_to_print = $this->newsfeedService->userPlacesContent();
                }
                if (!empty($post_to_print)) {
                    if (isset($post_to_print[HomeService::TYPE_POST])) {
                        $second_suggestion .= view('site.home.posts.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST], 'type' => 'Post'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_TRIP])) {
                        $second_suggestion .= view('site.home.posts.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP], 'type' => 'Trip'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_REPORT])) {
                        $second_suggestion .= view('site.home.posts.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT], 'type' => 'Report'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_DISCUSSION])) {
                        $second_suggestion .= view('site.home.posts.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION], 'type' => 'Discussion'], 'me' => Auth::guard('user')->user()]);
                    }
                }

                $post_to_print = $this->newsfeedService->getTrendingPosts();
                if (!empty($post_to_print)) {
                    if (isset($post_to_print[HomeService::TYPE_POST]->id)) {
                        $second_suggestion .= view('site.home.trendings.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST]->id, 'type' => 'Post'], 'me' => Auth::guard('user')->user()]);
                    } else if (isset($post_to_print[HomeService::TYPE_TRIP]->id)) {
                        $second_suggestion .= view('site.home.trendings.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP]->id, 'type' => 'Trip'], 'me' => Auth::guard('user')->user()]);
                    } else if (isset($post_to_print[HomeService::TYPE_REPORT]->id)) {
                        $second_suggestion .= view('site.home.trendings.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT]->id, 'type' => 'Report'], 'me' => Auth::guard('user')->user()]);
                    } else if (isset($post_to_print[HomeService::TYPE_DISCUSSION]->id)) {
                        $second_suggestion .= view('site.home.trendings.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION]->id, 'type' => 'Discussion'], 'me' => Auth::guard('user')->user()]);
                    }
                }

                $post_to_print = $this->newsfeedService->getTrendingPosts(false, true);
                if (!empty($post_to_print)) {
                    if (isset($post_to_print[HomeService::TYPE_POST]->id)) {
                        $second_suggestion .= view('site.home.trendings.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST]->id, 'type' => 'Post'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_TRIP]->id)) {
                        $second_suggestion .= view('site.home.trendings.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP]->id, 'type' => 'Trip'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_REPORT]->id)) {
                        $second_suggestion .= view('site.home.trendings.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT]->id, 'type' => 'Report'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_DISCUSSION]->id)) {
                        $second_suggestion .= view('site.home.trendings.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION]->id, 'type' => 'Discussion'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                }
                $data['secondary_post'] = $second_suggestion;
            }

            $data['new_people'] = User::orderBy('id', 'desc')->take(10)->get();
            $data['my_plans'] = [];
            $data['me'] = Auth::guard('user')->user();

            // session()->forget(['loaded_post_ids']);
            // $value = session(['loaded_post_ids' =>  $data['posts']->pluck('id')->toArray()]);
            $data['my_friends_trip'] = $this->getFriendsTrips('');
            $data['my_friend_started_trip'] =  $this->getFriendsTrips('start');

            //top places script updated //
            $data['top_places'] = $this->getTopPlace();
            // $data['top_places'] = [];
            return view('site.home.index', $data);
        } else {
            return redirect('login');
        }
    }

    public function postUpdateFeed(Request $request)
    {
        $authUser = auth()->user();
        $page = $request->get('pageno');
        $per_page = 15;
        $is_suggestion = $request->get('is_suggestion');
        $return = false;
        $posts = [];
        $loop_countert = 1;

        if (Auth::check()) {
            if ($is_suggestion) {
                $data['secondary_post_suggestion'] = '';
                $user_place_content = $this->newsfeedService->userPlacesContent();
                $post_to_print = $user_place_content;
                if (!empty($post_to_print)) {
                    if (isset($post_to_print[HomeService::TYPE_POST])) {
                        $return .= view('site.home.posts.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST], 'type' => 'Post'], 'me' => $authUser]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_TRIP])) {
                        $return .= view('site.home.posts.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP], 'type' => 'Trip'], 'me' => $authUser]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_REPORT])) {
                        $return .= view('site.home.posts.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT], 'type' => 'Report'], 'me' => $authUser]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_DISCUSSION])) {
                        $return .= view('site.home.posts.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION], 'type' => 'Discussion'], 'me' => $authUser]);
                    }
                }
                $post_to_print = $this->newsfeedService->getTrendingPosts();
                if (!empty($post_to_print)) {
                    if (isset($post_to_print[HomeService::TYPE_POST]->id)) {
                        $return .= view('site.home.trendings.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST]->id, 'type' => 'Post'], 'me' => $authUser]);
                    } else if (isset($post_to_print[HomeService::TYPE_TRIP]->id)) {
                        $return .= view('site.home.trendings.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP]->id, 'type' => 'Trip'], 'me' => $authUser]);
                    } else if (isset($post_to_print[HomeService::TYPE_REPORT]->id)) {
                        $return .= view('site.home.trendings.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT]->id, 'type' => 'Report'], 'me' => $authUser]);
                    } else if (isset($post_to_print[HomeService::TYPE_DISCUSSION]->id)) {
                        $return .= view('site.home.trendings.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION]->id, 'type' => 'Discussion'], 'me' => $authUser]);
                    }
                }
                $post_to_print = $this->newsfeedService->getTrendingPosts(false, true);
                if (!empty($post_to_print)) {
                    if (isset($post_to_print[HomeService::TYPE_POST]->id)) {
                        $return .= view('site.home.trendings.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST]->id, 'type' => 'Post'], 'me' => $authUser, 'all' => true]);
                    } else if (isset($post_to_print[HomeService::TYPE_TRIP]->id)) {
                        $return .= view('site.home.trendings.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP]->id, 'type' => 'Trip'], 'me' => $authUser, 'all' => true]);
                    } else if (isset($post_to_print[HomeService::TYPE_REPORT]->id)) {
                        $return .= view('site.home.trendings.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT]->id, 'type' => 'Report'], 'me' => $authUser, 'all' => true]);
                    } else if (isset($post_to_print[HomeService::TYPE_DISCUSSION]->id)) {
                        $return .= view('site.home.trendings.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION]->id, 'type' => 'Discussion'], 'me' => $authUser, 'all' => true]);
                    }
                }
            } else {
                $posts = $this->newsfeedService->getPrimaryPosts($page, $per_page)->shuffle();
            }

            $secondary_post_list = session('secondary_post_style');
            if ($page % 2 == 0 && !empty($secondary_post_list)) {

                // if(empty($secondary_post_list)){
                //     $secondary_post_list = ['new_travellers', 'weather_for_trip', 'weather', 'following_locations', 'collective_following_location', 'video_you_might_like', 'recommended_places', 'people_you_might_know'];
                //     shuffle($secondary_post_list);
                //     session()->forget(['secondary_post_style']);
                //     $value1 = session(['secondary_post_style' =>  $secondary_post_list]);
                // }
                //code need to move 
                //                session()->forget(['secondary_post_style']);

                $events_activities_list = session('events_activites');
                $followed_cities_ids = [];
                $ip = getClientIP();
                $geoLoc = ip_details($ip);
                $getLocCity = $geoLoc['city'];
                $getLocCity = Cities::with('transsingle')
                    ->whereHas('transsingle', function ($q) use ($getLocCity) {
                        $q->where('title', 'LIKE', "%$getLocCity%");
                    })
                    ->first();

                if (isset($getLocCity)) {
                    $followed_cities_ids[] = $getLocCity->id;
                    $data['event_activity'] = $this->getEventForUser($followed_cities_ids);
                }
                if (empty($data['event_activity'])) {
                    $followed_cities_ids = getUserFollwoingForEvents(Auth::user()->id);
                    $data['event_activity'] = $this->getEventForUser($followed_cities_ids);
                }
                if (session('events_activites')) {
                    session()->forget(['events_activites']);
                }
                $events_activities_list[] = isset($data['event_activity']->id) ? $data['event_activity']->id : 0;
                session(['events_activites' =>  $events_activities_list]);
                $return  .= view('site.home.partials.post_event', array('post' => $data['event_activity'], 'me' => Auth::guard('user')->user()));

                if (isset($secondary_post_list[0])  && $secondary_post_list[0] == 'new_travellers') {
                    $data['new_travellers'] = $this->discoverNewTravellers(Auth::user());
                    // $data['new_travellers'] = User::whereDate('created_at', '>', Carbon::now()->subDays(15))->orderBy('created_at', 'DESC')->take(10)->get();
                    $return .= view('site.home.partials.discover_new_travellers', $data);
                } elseif (isset($secondary_post_list[0])  && $secondary_post_list[0] == 'weather_for_trip') {
                    if (Auth::check()) {
                        $data['weather_for_trip'] = $this->getWeatherUpdateForMyTrips();
                        if (count($data['weather_for_trip']) > 0) {
                            echo " ";
                            $return .= view('site.home.new.partials.trip-weather', $data);
                        }
                    }
                } elseif (isset($secondary_post_list[0])  && $secondary_post_list[0] == 'weather') {
                    if (Auth::check()) {
                        $data['weather'] = $this->getWeatherUpdateForCheckins();
                        if (count($data['weather']) > 0) {
                            echo " ";
                            $return .= view('site.home.new.partials.weather-forcast-checkin', $data);
                        }
                    }
                } else if (isset($secondary_post_list[0])  && $secondary_post_list[0] == 'collective_following_location') {
                    $return .= view('site.home.partials.collective_followers_places');
                } else if (isset($secondary_post_list[0])  && $secondary_post_list[0] == 'video_you_might_like') {
                    $return .= view('site.home.partials.videos_you_might_like');
                    // if(Auth::check()){
                    //     $places = PlaceFollowers::query()
                    //         ->where('users_id', auth()->user()->id)
                    //         ->with('place')
                    //         ->whereHas('place')
                    //         ->get();

                    //     $countries = CountriesFollowers::query()
                    //         ->where('users_id', auth()->user()->id)
                    //         ->with('country')
                    //         ->whereHas('country')
                    //         ->get();

                    //     $cities = CitiesFollowers::query()
                    //         ->where('users_id', auth()->user()->id)
                    //         ->with('city')
                    //         ->whereHas('city')
                    //         ->get();

                    //     $places_ids = $places->pluck('places_id');
                    //     $countries_ids = $countries->pluck('countries_id');
                    //     $cities_ids = $cities->pluck('cities_id');

                    //     $following_locations = [
                    //         'places_ids' => $places_ids,
                    //         'countries_ids' => $countries_ids,
                    //         'cities_ids' => $cities_ids,
                    //     ];

                    //     $data['following_locations'] = $following_locations;
                    //     //  $return .= view('site.home.partials.trending_events', $data);
                    // }
                } else if (isset($secondary_post_list[0])  && $secondary_post_list[0] == 'recommended_places')
                    $return .= view('site.home.partials.recommended_places');
                else if (isset($secondary_post_list[0])  && $secondary_post_list[0] == 'trending_places')
                    $return .= view('site.home.partials.recommended_places');
                // else if ($page == 7)
                //     $return .= view('site.home.partials.recommended_countries_cities');
                // else if (isset($secondary_post_list[$page-2])  && $secondary_post_list[$page-2] == 'people_you_might_know')
                //     $return .= view('site.home.partials.people_you_might_know');

                unset($secondary_post_list[0]);
                session(['secondary_post_style' =>  $secondary_post_list]);
            }

            if (count($posts) == 0 && !$is_suggestion) {

                //trending post;
                $post_to_print = $this->newsfeedService->getTrendingPosts(true);
                $shuffled_array = array();

                $keys = array_keys($post_to_print);
                shuffle($keys);

                foreach ($keys as $key) {
                    $shuffled_array[$key] = $post_to_print[$key];
                }
                $post_to_print = $shuffled_array;
                if (!empty($post_to_print)) {
                    if (isset($post_to_print[HomeService::TYPE_POST]->id)) {
                        $return .= view('site.home.trendings.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST]->id, 'type' => 'Post'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_TRIP]->id)) {
                        $return .= view('site.home.trendings.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP]->id, 'type' => 'Trip'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_REPORT]->id)) {
                        $return .= view('site.home.trendings.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT]->id, 'type' => 'Report'], 'me' => Auth::guard('user')->user()]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_DISCUSSION]->id)) {
                        $return .= view('site.home.trendings.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION]->id, 'type' => 'Discussion'], 'me' => Auth::guard('user')->user()]);
                    }
                }
                $post_to_print = $this->newsfeedService->getTrendingPosts(true, true);
                $shuffled_array = array();

                $keys = array_keys($post_to_print);
                shuffle($keys);

                foreach ($keys as $key) {
                    $shuffled_array[$key] = $post_to_print[$key];
                }
                $post_to_print = $shuffled_array;
                if (!empty($post_to_print)) {
                    if (isset($post_to_print[HomeService::TYPE_POST]->id)) {
                        $return .= view('site.home.trendings.primary-text-only-post', ['post' => ['variable' => $post_to_print[HomeService::TYPE_POST]->id, 'type' => 'Post'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_TRIP]->id)) {
                        $return .= view('site.home.trendings.trip', ['post' => ['variable' => $post_to_print[HomeService::TYPE_TRIP]->id, 'type' => 'Trip'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_REPORT]->id)) {
                        $return .= view('site.home.trendings.travlog', ['post' => ['variable' => $post_to_print[HomeService::TYPE_REPORT]->id, 'type' => 'Report'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                    if (isset($post_to_print[HomeService::TYPE_DISCUSSION]->id)) {
                        $return .= view('site.home.trendings.discussion', ['post' => ['variable' => $post_to_print[HomeService::TYPE_DISCUSSION]->id, 'type' => 'Discussion'], 'me' => Auth::guard('user')->user(), 'all' => true]);
                    }
                }
                if ($page == 2) {
                    $data['weather_for_trip'] = $this->getWeatherUpdateForMyTrips();
                    if (count($data['weather_for_trip']) > 0) {
                        echo " ";
                        $return .= view('site.home.new.partials.trip-weather', $data);
                    }
                } else if ($page == 3) {
                    $data['weather'] = $this->getWeatherUpdateForCheckins();
                    if (count($data['weather']) > 0) {
                        echo " ";
                        $return .= view('site.home.new.partials.weather-forcast-checkin', $data);
                    }
                } else if ($page == 4) {
                    $places = PlaceFollowers::query()
                        ->where('users_id', auth()->user()->id)
                        ->with('place')
                        ->whereHas('place')
                        ->get();

                    $countries = CountriesFollowers::query()
                        ->where('users_id', auth()->user()->id)
                        ->with('country')
                        ->whereHas('country')
                        ->get();

                    $cities = CitiesFollowers::query()
                        ->where('users_id', auth()->user()->id)
                        ->with('city')
                        ->whereHas('city')
                        ->get();

                    $places_ids = $places->pluck('places_id');
                    $countries_ids = $countries->pluck('countries_id');
                    $cities_ids = $cities->pluck('cities_id');

                    $following_locations = [
                        'places_ids' => $places_ids,
                        'countries_ids' => $countries_ids,
                        'cities_ids' => $cities_ids,
                    ];

                    $data['following_locations'] = $following_locations;
                    // $return .= view('site.home.partials.trending_events', $data);
                } else if ($page == 5) {
                }
                // $return .= view('site.home.partials.collective_followers_places');
                else if ($page == 6)
                    $return .= view('site.home.partials.videos_you_might_like');
                // else if ($page == 7)
                //     $return .= view('site.home.partials.recommended_countries_cities');
                else if ($page == 8)
                    $return .= view('site.home.partials.recommended_places');
                else if ($page == 9)
                    $return .= view('site.home.partials.people_you_might_know');
                else if ($page == 10) {

                    $data['new_travellers'] = $this->discoverNewTravellers(Auth::user());
                    // $data['new_travellers'] = User::whereDate('created_at', '>', Carbon::now()->subDays(15))->orderBy('created_at', 'DESC')->take(10)->get();
                    $return .= view('site.home.partials.discover_new_travellers', $data);
                }
            }
        } else {
            $posts = $this->newsfeedService->getPrimaryPosts($page, $per_page)->shuffle();
        }

        if (count($posts) != 0) {
            foreach ($posts as $post) {
                echo " ";
                if (Auth::check() && isset($post->permission) and $post->permission == Posts::POST_PERMISSION_PRIVATE and $post->users_id != $authUser->id) {
                    continue;
                } elseif (Auth::check() && isset($post->permission) and $post->permission == Posts::POST_PERMISSION_FRIENDS_ONLY and $post->users_id != $authUser->id and !in_array($post->users_id, getUserFriendsIds())) {
                    continue;
                }
                $loop_countert++;

                if ($post->type == "post" and $post->action == "publish" and !user_access_denied($post->user_id, true)) {
                    // $return .= view('site.home.partials.post_regular', array('post' => $post, 'me' => Auth::guard('user')->user()));
                    $return .= view('site.home.new.partials.primary-text-only-post', array('post' => $post, 'me' => $authUser));
                }
                // else if ($post->type == "post" and $post->action == "like" and !user_access_denied($post->user_id, true)){
                //     $return .= view('site.home.partials.post_like', array('post' => $post));
                // }
                // elseif ($post->type == "post" and $post->action == "comment" and !user_access_denied($post->user_id, true)){
                //     $return .= view('site.home.partials.post_comment', array('post' => $post));
                // }
                else if ($post->type == 'trips_medias_shares') {
                    $return .= view('site.home.new.partials.tripmediashare', array('post' => $post));
                } else if ($post->type == 'trips_places_shares') {
                    $return .= view('site.home.new.partials.tripplace', array('post' => $post));
                }
                // else if ($post->type == "post" and $post->action == "unlike") {
                // } elseif ($post->type == "travelmate" and $post->action == "join") {
                //     $return .= view('site.home.partials.travelmate_join', array('post' => $post));
                // } elseif ($post->type == "travelmate" and $post->action == "request") {
                //     $return .= view('site.home.partials.travelmate_request', array('post' => $post));
                // } 
                else if ($post->type == "trip" and ($post->action == "create" || $post->action == 'plan_updated') and !user_access_denied($post->user_id, true)) {
                    $return .= view('site.home.new.partials.trip', array('post' => $post));
                }

                // elseif ($post->type == "user" and $post->action == "follow" and !user_access_denied($post->user_id, true))
                //     $return .= view('site.home.partials.user_follow', array('post' => $post));

                // elseif ($post->type == "user" and $post->action == "unfollow") {
                // } elseif ($post->type == "users" and $post->action == "registrate") {
                // } elseif ($post->type == "country" and $post->action == "follow" and !user_access_denied($post->user_id, true))
                //     $return .= view('site.home.partials.country_follow', array('post' => $post));

                // elseif ($post->type == "country" and $post->action == "unfollow") {
                // } elseif ($post->type == "city" and $post->action == "follow" and !user_access_denied($post->user_id, true))
                //     $return .= view('site.home.partials.city_follow', array('post' => $post));

                // elseif ($post->type == "city" and $post->action == "unfollow") {
                // } elseif ($post->type == "place" and $post->action == "follow" and !user_access_denied($post->user_id, true))
                //     $return .= view('site.home.partials.place_follow', array('post' => $post));

                else if ($post->type == "place" and $post->action == "review" and !user_access_denied($post->user_id, true)) {
                    // $return .= view('site.home.partials.place_review', array('post' => $post));
                    $return .= view('site.home.new.partials.place-review', array('post' => $post));
                }
                // elseif ($post->type == "place" and $post->action == "unfollow") {
                // } elseif ($post->type == "hotel" and $post->action == "follow" and !user_access_denied($post->user_id, true)){
                //     $return .= view('site.home.partials.hotel_follow', array('post' => $post));
                // }
                else if ($post->type == "hotel" and $post->action == "review" and !user_access_denied($post->user_id, true)) {
                    $return .= view('site.home.partials.hotel_review', array('post' => $post));
                }
                // elseif ($post->type == "hotel" and $post->action == "unfollow") {
                // }
                // elseif(strtolower($post->type) == "Status" AND $post->action == "comment")
                //     $return .= view('site.home.partials.status_comment', array('post'=>$post, 'me'=>$authUser));
                else if ($post->type == "status" and $post->action == "publish" and !user_access_denied($post->user_id, true)) {
                    $return .= view('site.home.partials.status_publish', array('post' => $post));
                }

                // elseif ($post->type == "status" and $post->action == "like")
                //     $return .= view('site.home.partials.status_like', array('post' => $post));

                // elseif ($post->type == "status" and $post->action == "unlike") {
                // } 
                // elseif ($post->type == "media" and $post->action == "upload")
                //     $return .= view('site.home.new.partials.primary-media-post', array('post' => $post));

                // elseif($post->type == "media" AND $post->action == "comment")
                //     $return .= view('site.home.partials.media_comment', array('post'=>$post));
                // elseif(($post->type == "Medias" OR $post->type == "Media") AND $post->action == "comment")
                //     $return .= view('site.home.partials.media_comment', array('post'=>$post));

                // elseif ($post->type == "media" and $post->action == "like")
                //     $return .= view('site.home.partials.media_like', array('post' => $post));

                // elseif ($post->type == "media" and $post->action == "unlike")
                //     $return .= view('site.home.partials.media_unlike', array('post' => $post));

                // elseif ($post->type == "checkin" and $post->action == "do" and !user_access_denied($post->user_id, true))
                //     $return .= view('site.home.partials.checkin_do', array('post' => $post));

                // elseif ($post->type == "discussion" and $post->action == "reply" and !user_access_denied($post->user_id, true))
                //     $return .= view('site.home.partials.discussion_reply', array('post' => $post));

                // elseif ($post->type == "discussion" and $post->action == "upvote") {
                // } elseif ($post->type == "discussion" and $post->action == "downvote") {
                // } 
                else if ($post->type == "discussion" and $post->action == "create" and !user_access_denied($post->user_id, true)) {
                    // @include('site.home.new.partials.discussion')
                    $return .= view('site.home.new.partials.discussion', array('post' => $post, 'me' => $authUser));
                } else if ($post->type == "trip" and $post->action == "invite_friends") {
                } else if ($post->type == "trip" and $post->action == "activate" and !user_access_denied($post->user_id, true)) {
                    $return .= view('site.home.partials.trip_activate', array('post' => $post, 'me' => $authUser));
                } else if ($post->type == "trip" and $post->action == "create") {
                }
                // elseif ($post->type == "trip" and $post->action == "accept_invitation") {
                // } elseif ($post->type == "trip" and $post->action == "reject_invitation") {
                // } elseif ($post->type == "trip" and $post->action == "delete") {
                // } 
                else if ($post->type == "report" and ($post->action == "create" || $post->action == "update") and !user_access_denied($post->user_id, true)) {
                    $return .= view('site.home.new.partials.travlog', array('post' => $post, 'me' => $authUser));
                }
                //  elseif ($post->type == "report" and $post->action == "comment") {
                // } 
                else if ($post->type == "event" and $post->action == "show" and !user_access_denied($post->user_id, true)) {
                    $return .= view('site.home.partials.post_event', array('post' => $post, 'me' => $authUser));
                } elseif ($post->type == 'share' and !user_access_denied($post->user_id, true)) {
                    $return .= view('site.home.new.partials.shareable-post', array('post' => $post, 'me' => $authUser));
                }
                //}
                //return view('site.home.index', $data);
            }
        }
        echo $return;
    }

    private function getTopPlace()
    {
        $topPlaces = [];
        $follow_list = [];
        $countries_suggested = '';
        $same_country_flag = false;

        $current_user_nationality = Auth::user()->nationality;
        $user_country = Countries::find($current_user_nationality);

        $ip = getClientIP();

        $geoLoc = ip_details($ip);
        $details['country']  = $geoLoc['iso_code'];
        $p = Auth::user()->id;
        $follows_places = DB::select('SELECT * FROM `places_followers` WHERE users_id   =' . $p);

        foreach ($follows_places as $placex) {
            $follow_list[$placex->places_id] = $placex->places_id;
        }

        if (isset($details['country']) && isset($user_country->iso_code) && $details['country'] == $user_country->iso_code) {
            $tp_array = getTopPlacesByUserPref();
            $countries_suggested = Countries::where('iso_code', $details['country'])->first();
            $same_country_flag = true;
        } else {
            if (isset($details['country'])) {
                $countries_suggested = Countries::where('iso_code', $details['country'])->first();
                $tp_array = [];
                // $tp_array = getTopPlacesByUserPref(['country' => $countries_suggested->id]);
            } else {
                $tp_array = getTopPlacesByUserPref();
            }
        }


        if (!empty($follow_list)) {
            $tp_array = array_diff($tp_array, array_values($follow_list));
        }

        if ($same_country_flag && !empty($tp_array)) {
            $topPlaces = PlacesTop::whereIn('places_id', $tp_array)
                ->whereHas('place', function ($query) {
                    $query->whereHas('medias');
                })
                ->whereNotIn('country_id', [373, 326])
                ->whereNotIn('city_id', [2031])
                ->inRandomOrder()
                ->limit(20)
                ->get()->shuffle();
        } else {
            $topPlaces = PlacesTop::where('country_id', @$countries_suggested->id)
                ->whereHas('place', function ($query) {
                    $query->whereHas('medias');
                })
                ->whereNotIn('country_id', [373, 326])
                ->whereNotIn('city_id', [2031])
                ->orderBy('reviews_num', 'DESC')
                ->orderBy('rating', 'DESC')
                ->limit(20)
                ->get()->shuffle();
        }
        if (!empty($follow_list)) {
            $tp_array = array_merge($tp_array, $follow_list);
        }

        if (count($topPlaces) < 20) {
            if ($same_country_flag) {
                $remaining = 20 - count($topPlaces);
                $count_remaing = PlacesTop::whereNotIn('places_id', $tp_array)
                    ->whereHas('place', function ($query) {
                        $query->whereHas('medias');
                    })
                    ->whereNotIn('country_id', [373, 326])
                    ->whereNotIn('city_id', [2031])
                    ->where('country_id', @$countries_suggested->id)
                    ->orderBy('reviews_num', 'DESC')
                    ->orderBy('rating', 'DESC')
                    ->limit($remaining)
                    ->get()->shuffle();;
            } else {
                $remaining = 20 - count($topPlaces);
                $count_remaing = PlacesTop::whereNotIn('places_id', $tp_array)
                    ->whereHas('place', function ($query) {
                        $query->whereHas('medias');
                    })
                    ->whereNotIn('country_id', [373, 326])
                    ->whereNotIn('city_id', [2031])
                    ->orderBy('reviews_num', 'DESC')
                    ->orderBy('rating', 'DESC')
                    ->limit($remaining)
                    ->get()->shuffle();;
            }
            $topPlaces = $topPlaces->merge($count_remaing);
        }
        return $topPlaces;
    }

    public function getSelect2()
    {
        $all_countries = Countries::all();
        $results = array();
        foreach ($all_countries as $count) {
            $results[] = array('id' => $count->id, 'text' => @$count->transsingle->title);
        }
        $return = array('results' => $results);
        return json_encode($return);
    }

    public function getSelect2Location(Request $request)
    {
        $query = $request->get('q');

        if ($query != '') {
            $results = array();
            $cityId = '';
            //here should be topPlaces
            $all_places = PlacesTop::where('city_id', $cityId)
                ->take(10)
                ->get();

            foreach ($all_places as $place) {
                //value.id+','+value.trans[0].title+','+value.lat+","+value.lng+',Place
                $results[] = array(
                    'id' => $place->id . ',' . $place->transsingle->title . ',' . $place->lat . "," . $place->lng . ',Country', 'text' => @$place->transsingle->title,
                    'lng' => $place->lng,
                    'lat' => $place->lat
                );
            }


            $all_places = Countries::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($query) {
                    $q->where('title', 'LIKE', "%$query%");
                })
                ->get();
            foreach ($all_places as $place) {
                $results[] = array(
                    'id' => $place->id . ',' . $place->transsingle->title . ',' . $place->lat . "," . $place->lng . ',Country', 'text' => @$place->transsingle->title,
                    'lng' => $place->lng,
                    'lat' => $place->lat
                );
            }

            $all_places = Cities::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($query) {
                    $q->where('title', 'LIKE', "%$query%");
                })
                ->get();
            foreach ($all_places as $place) {
                //value.id+','+value.trans[0].title+','+value.lat+","+value.lng+',Place
                $results[] = array(
                    'id' => $place->id . ',' . $place->transsingle->title . ',' . $place->lat . "," . $place->lng . ',City', 'text' => @$place->transsingle->title,
                    'lng' => $place->lng,
                    'lat' => $place->lat
                );
            }


            $all_places = Place::whereHas('transsingle', function ($q) use ($query) {
                $q->where('title', 'LIKE', "%$query%");
            })->get();

            //$results = array();
            foreach ($all_places as $place) {
                $results[] = array(
                    'id' => $place->id . ',' . $place->transsingle->title . ',' . $place->lat . "," . $place->lng . ',Place', 'text' => @$place->transsingle->title,
                    'lng' => $place->lng,
                    'lat' => $place->lat
                );
            }

            $return = array('results' => $results);
            return json_encode($return);
        }
    }

    public function ajaxCheckToursCities()
    {
        $client = new Client();

        $cities = CitiesFollowers::query()
            ->where('users_id', auth()->user()->id)
            ->with('city')
            ->whereHas('city')
            ->limit(1)
            ->orderBy('id', 'DESC')
            ->get();

        $cities_query = $cities->map(function ($city) {
            return str_replace(' ', '_', $city->city->transsingle->title);
        })
            ->filter(function ($value) {
                return !empty($value);
            })
            ->implode(',');
        try {
            $get_tours1 = @$client->get('https://www.triposo.com/api/20201111/tour.json?account=YUW6AYM4&token=6dl4ll059uu2shfj9lncfrt8ucpq8giz&fields=all&count=10&location_ids=' . $cities_query);

            if ($get_tours1->getStatusCode() == 200) {
                $tours1 = json_decode($get_tours1->getBody()->read(10000024));
                $tours_array1 = $tours1->results;
                $tours = $tours_array1;

                foreach ($tours as $tour) {
                    $check_tours = Events::where('provider_id', $tour->id)->get()->first();

                    if (!is_object($check_tours)) {

                        $new_tour = new Events();
                        $new_tour->cities_id = @$city->city->id;
                        $new_tour->provider_id = @$tour->id;
                        $new_tour->category = 'event_tour';
                        $new_tour->title = @$tour->content->title;
                        $new_tour->description = @$tour->intro;
                        $new_tour->labels = @join(",", $tour->tag_labels);
                        $new_tour->lat = @$tour->coordinates->lat ?: '0.0';
                        $new_tour->lng = @$tour->coordinates->lng ?: '0.0';
                        $new_tour->variable = json_encode([
                            'score' => $tour->score,
                            'price' => $tour->price,
                            'price_is_per_person' => $tour->price_is_per_person,
                            'properties' => $tour->properties,
                            'images' => $tour->images,
                            'duration' => $tour->duration,
                            'duration_unit' => $tour->duration_unit,
                            'highlights' => $tour->highlights,
                            'tags' => $tour->tags,
                            'booking_info' => $tour->booking_info,
                            'structured_content' => $tour->structured_content,
                            'content' => $tour->content,
                            'poi_ids' => $tour->poi_ids
                        ]);
                        $new_tour->save();
                        log_user_activity('Event', 'show', $new_tour->id);
                    }
                }

                // $this->createPostsForEvents($city->city, 'city');
            }
        } catch (\Throwable $th) {
        }

        return json_encode(['success' => true]);
    }

    public function ajaxCheckToursCountries()
    {
        $client = new Client();

        $countries = CountriesFollowers::query()
            ->where('users_id', auth()->user()->id)
            ->with('country')
            ->whereHas('country')
            ->get();

        try {

            $countries->each(function ($country) use ($client) {
                $get_tours1 = @$client->get('https://www.triposo.com/api/20201111/tour.json?account=YUW6AYM4&token=6dl4ll059uu2shfj9lncfrt8ucpq8giz&fields=all&count=10&countrycode=' . strtolower($country->country->iso_code));

                if ($get_tours1->getStatusCode() == 200) {
                    $tours1 = json_decode($get_tours1->getBody()->read(10000024));
                    $tours_array1 = $tours1->results;

                    $tours = $tours_array1;

                    foreach ($tours as $tour) {
                        $check_tours = Events::where('provider_id', $tour->id)->get()->first();
                        if (!is_object($check_tours)) {
                            $new_tour = new Events();
                            $new_tour->countries_id = $country->country->id;
                            $new_tour->provider_id = @$tour->id;
                            $new_tour->category = 'event_tour';
                            $new_tour->title = @$tour->content->title;
                            $new_tour->description = @$tour->intro;
                            $new_tour->labels = @join(",", $tour->tag_labels);
                            $new_tour->lat = @$tour->coordinates->lat ?: '0.0';
                            $new_tour->lng = @$tour->coordinates->lng ?: '0.0';
                            $new_tour->variable = json_encode([
                                'score' => $tour->score,
                                'price' => $tour->price,
                                'price_is_per_person' => $tour->price_is_per_person,
                                'properties' => $tour->properties,
                                'images' => $tour->images,
                                'duration' => $tour->duration,
                                'duration_unit' => $tour->duration_unit,
                                'highlights' => $tour->highlights,
                                'tags' => $tour->tags,
                                'booking_info' => $tour->booking_info,
                                'structured_content' => $tour->structured_content,
                                'content' => $tour->content,
                                'poi_ids' => $tour->poi_ids
                            ]);
                            $new_tour->save();

                            log_user_activity('Tour', 'publish', $new_tour->id);
                        }
                    }

                    // $this->createPostsForEvents($country->country, 'country');
                }
            });
        } catch (\Throwable $th) {
        }

        return json_encode(['success' => true]);
    }

    public function ajaxCheckActivitiesCities()
    {
        $client = new Client();

        $cities = CitiesFollowers::query()
            ->where('users_id', auth()->user()->id)
            ->with('city')
            ->whereHas('city')
            ->get();
        // try {
        $cities->each(function ($city) use ($client) {

            $get_pois1 = @$client->get('https://www.triposo.com/api/20201111/poi.json?account=YUW6AYM4&token=6dl4ll059uu2shfj9lncfrt8ucpq8giz&fields=all&count=10&location_id=' . str_replace(' ', '_', $city->city->transsingle->title));

            if ($get_pois1->getStatusCode() == 200) {
                $pois1 = json_decode($get_pois1->getBody()->read(10000024));
                dd('https://www.triposo.com/api/20201111/poi.json?account=YUW6AYM4&token=6dl4ll059uu2shfj9lncfrt8ucpq8giz&fields=all&count=10&location_id=' . str_replace(' ', '_', $city->city->transsingle->title));
                $tours_array1 = $pois1->results;
                $pois = $tours_array1;

                foreach ($pois as $poi) {
                    $check_pois = Events::where('provider_id', $poi->id)->get()->first();
                    if (!is_object($check_pois)) {
                        $new_poi = new Events();
                        $new_poi->cities_id = $city->city->id;
                        $new_poi->provider_id = @$poi->id;
                        $new_poi->category = 'event_activity';
                        $new_poi->title = @$poi->content->title;
                        $new_poi->description = @$poi->intro;
                        $new_poi->labels = @join(",", $poi->tag_labels);
                        $new_poi->lat = @$poi->coordinates->lat ?: '0.0';
                        $new_poi->lng = @$poi->coordinates->lng ?: '0.0';
                        $new_poi->variable = json_encode([
                            'score' => $poi->score,
                            'images' => $poi->images,
                            'properties' => $poi->properties,
                            'best_for' => $poi->best_for,
                            'tags' => $poi->tags,
                            'booking_info' => $poi->booking_info,
                            'attribution' => $poi->attribution,
                            'price_tier' => $poi->price_tier,
                            'structured_content' => $poi->structured_content,
                            'opening_hours' => $poi->opening_hours,
                            'content' => $poi->content,
                            'snippet' => $poi->snippet,
                            'location_ids' => $poi->location_ids,
                            'tour_ids' => $poi->tour_ids
                        ]);
                        $new_poi->save();

                        log_user_activity('Event', 'show', $new_poi->id);
                    }
                }

                // $this->createPostsForEvents($city->city, 'city');
            }
        });
        // } catch (\Throwable $th) {
        // }

        return json_encode(['success' => true]);
    }

    public function ajaxCheckActivitiesCountries()
    {
        $client = new Client();

        $countries = CountriesFollowers::query()
            ->where('users_id', auth()->user()->id)
            ->with('country')
            ->whereHas('country')
            ->get();

        try {
            $countries->each(function ($country) use ($client) {

                $get_pois1 = @$client->get('https://www.triposo.com/api/20201111/poi.json?account=YUW6AYM4&token=6dl4ll059uu2shfj9lncfrt8ucpq8giz&fields=all&count=10&countrycode=' . strtolower($country->country->iso_code));

                if ($get_pois1->getStatusCode() == 200) {
                    $pois1 = json_decode($get_pois1->getBody()->read(10000024));
                    $pois_array1 = $pois1->results;

                    $pois = $pois_array1;

                    foreach ($pois as $poi) {
                        $check_pois = Events::where('provider_id', $poi->id)->get()->first();
                        if (!is_object($check_pois)) {
                            $new_poi = new Events();
                            $new_poi->countries_id = $country->country->id;
                            $new_poi->provider_id = @$poi->id;
                            $new_poi->category = 'event_activity';
                            $new_poi->title = @$poi->content->title;
                            $new_poi->description = @$poi->intro;
                            $new_poi->labels = @join(",", $poi->tag_labels);
                            $new_poi->lat = @$poi->coordinates->lat ?: '0.0';
                            $new_poi->lng = @$poi->coordinates->lng ?: '0.0';
                            $new_poi->variable = json_encode([
                                'score' => $poi->score,
                                'images' => $poi->images,
                                'properties' => $poi->properties,
                                'best_for' => $poi->best_for,
                                'tags' => $poi->tags,
                                'booking_info' => $poi->booking_info,
                                'attribution' => $poi->attribution,
                                'price_tier' => $poi->price_tier,
                                'structured_content' => $poi->structured_content,
                                'opening_hours' => $poi->opening_hours,
                                'content' => $poi->content,
                                'snippet' => $poi->snippet,
                                'location_ids' => $poi->location_ids,
                                'tour_ids' => $poi->tour_ids
                            ]);
                            $new_poi->save();

                            log_user_activity('Event', 'show', $new_poi->id);
                        }
                    }

                    // $this->createPostsForEvents($country->country, 'country');
                }
            });
        } catch (\Throwable $th) {
        }

        return json_encode(['success' => true]);
    }

    private function ajaxCheckEvent()
    {
        $client = new Client();

        $places = PlaceFollowers::query()
            ->where('users_id', auth()->user()->id)
            ->with('place')
            ->whereHas('place')
            ->get();

        $countries = CountriesFollowers::query()
            ->where('users_id', auth()->user()->id)
            ->with('country')
            ->whereHas('country')
            ->get();

        $cities = CitiesFollowers::query()
            ->where('users_id', auth()->user()->id)
            ->with('city')
            ->whereHas('city')
            ->get();

        $countries_query = $countries->map(function ($country) {
            return $country->country->iso_code;
        })
            ->filter(function ($value) {
                return !empty($value);
            })
            ->implode(',');

        $data = array();
        $data['data'] = '';
        $data['cnt'] = 0;
        try {
            $result = @$client->post('https://auth.predicthq.com/token', [
                'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'LWd1o9GZnlFsjf7ZgJffC-V4v6CEsljPaUuNaqz3_7-TFz_B2PpCOA'],
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'scope' => 'account events signals'
                ],
                'verify' => false
            ]);

            $events_array = false;
            if ($result->getStatusCode() == 200) {
                $json_response = $result->getBody()->read(1024);
                $response = json_decode($json_response);
                $access_token = $response->access_token;

                $places->each(function ($place) use ($client, $access_token) {
                    $get_events1 = $client->get('https://api.predicthq.com/v1/events/?end.gte=' . date('Y-m-d') . '&within=10km@' . $place->place->lat . ',' . $place->place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                        'headers' => ['Authorization' => 'Bearer ' . $access_token],
                        'verify' => false
                    ]);
                    if ($get_events1->getStatusCode() == 200) {
                        $events1 = json_decode($get_events1->getBody()->read(100024));
                        $events_array1 = $events1->results;
                        $events_final = $events_array1;
                        if ($events1->next) {
                            $get_events2 = $client->get($events1->next, [
                                'headers' => ['Authorization' => 'Bearer ' . $access_token],
                                'verify' => false
                            ]);
                            if ($get_events2->getStatusCode() == 200) {
                                $events2 = json_decode($get_events2->getBody()->read(100024));
                                //dd($events);
                                $events_array2 = $events2->results;
                                $events_final = array_merge($events_array1, $events_array2);
                            }
                        }
                        $events = $events_final;

                        foreach ($events as $event) {
                            $check_envent = Events::where('provider_id', $event->id)->where('places_id', $place->place->id)->get()->first();
                            if (!is_object($check_envent)) {
                                $new_event = new Events();
                                $new_event->places_id = $place->place->id;
                                $new_event->provider_id = @$event->id;
                                $new_event->category = @$event->category;
                                $new_event->title = @$event->title;
                                $new_event->description = @$event->description;
                                $new_event->address = @$event->entities[0]->formatted_address;
                                $new_event->labels = @join(",", $event->labels);
                                $new_event->lat = @$event->location[1];
                                $new_event->lng = @$event->location[0];
                                $new_event->start = @$event->start;
                                $new_event->end = @$event->end;
                                $new_event->save();

                                log_user_activity('Event', 'show', $new_event->id);
                            }
                        }

                        // $this->createPostsForEvents($place, 'place');
                    }
                });

                $cities->each(function ($city) use ($client, $access_token) {
                    $get_events1 = $client->get('https://api.predicthq.com/v1/events/?end.gte=' . date('Y-m-d') . '&within=10km@' . $city->city->lat . ',' . $city->city->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                        'headers' => ['Authorization' => 'Bearer ' . $access_token],
                        'verify' => false
                    ]);

                    if ($get_events1->getStatusCode() == 200) {
                        $events1 = json_decode($get_events1->getBody()->read(100024));
                        $events_array1 = $events1->results;
                        $events_final = $events_array1;
                        if ($events1->next) {
                            $get_events2 = $client->get($events1->next, [
                                'headers' => ['Authorization' => 'Bearer ' . $access_token],
                                'verify' => false
                            ]);
                            if ($get_events2->getStatusCode() == 200) {
                                $events2 = json_decode($get_events2->getBody()->read(100024));

                                $events_array2 = $events2->results;
                                $events_final = array_merge($events_array1, $events_array2);
                            }
                        }
                        $events = $events_final;

                        foreach ($events as $event) {
                            $check_envent = Events::where('provider_id', $event->id)->where('cities_id', $city->city->id)->get()->first();
                            if (!is_object($check_envent)) {
                                $new_event = new Events();
                                $new_event->cities_id = $city->city->id;
                                $new_event->provider_id = @$event->id;
                                $new_event->category = @$event->category;
                                $new_event->title = @$event->title;
                                $new_event->description = @$event->description;
                                $new_event->address = @$event->entities[0]->formatted_address;
                                $new_event->labels = @join(",", $event->labels);
                                $new_event->lat = @$event->location[1];
                                $new_event->lng = @$event->location[0];
                                $new_event->start = @$event->start;
                                $new_event->end = @$event->end;
                                $new_event->save();

                                log_user_activity('Event', 'show', $new_event->id);
                            }
                        }

                        // $this->createPostsForEvents($city, 'city');
                    }
                });

                $countries->each(function ($country) use ($client, $access_token, $countries_query) {
                    $get_events1 = $client->get('https://api.predicthq.com/v1/events/?end.gte=' . date('Y-m-d') . '&country=' . $countries_query . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                        'headers' => ['Authorization' => 'Bearer ' . $access_token],
                        'verify' => false
                    ]);
                    if ($get_events1->getStatusCode() == 200) {
                        $events1 = json_decode($get_events1->getBody()->read(100024));
                        $events_array1 = $events1->results;
                        $events_final = $events_array1;
                        if ($events1->next) {
                            $get_events2 = $client->get($events1->next, [
                                'headers' => ['Authorization' => 'Bearer ' . $access_token],
                                'verify' => false
                            ]);
                            if ($get_events2->getStatusCode() == 200) {
                                $events2 = json_decode($get_events2->getBody()->read(100024));
                                //dd($events);
                                $events_array2 = $events2->results;
                                $events_final = array_merge($events_array1, $events_array2);
                            }
                        }
                        $events = $events_final;
                        foreach ($events as $event) {
                            $check_envent = Events::where('provider_id', $event->id)->where('countries_id', $country->country->id)->get()->first();
                            if (!is_object($check_envent)) {
                                $new_event = new Events();
                                $new_event->countries_id = $country->country->id;
                                $new_event->provider_id = @$event->id;
                                $new_event->category = @$event->category;
                                $new_event->title = @$event->title;
                                $new_event->description = @$event->description;
                                $new_event->address = @$event->entities[0]->formatted_address;
                                $new_event->labels = @join(",", $event->labels);
                                $new_event->lat = @$event->location[1];
                                $new_event->lng = @$event->location[0];
                                $new_event->start = @$event->start;
                                $new_event->end = @$event->end;
                                $new_event->save();

                                log_user_activity('Event', 'show', $new_event->id);
                            }
                        }

                        // $this->createPostsForEvents($country, 'country');
                    }
                });
                //                $event_datas = $place->events()->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')->get();
                //                $data['cnt'] = count($event_datas);
            }
        } catch (\Throwable $th) {
        }

        //        $data['data'] = $this->getEvents($places_ids, $countries_ids, $cities_ids);

        return $data;
    }

    private function createPostsForEvents($place, $place_type)
    {
        $postIds = get_postlist4placemedia($place->id, $place_type);
        foreach ($place->getMedias as $media) {
            $posts = PostsMedia::where('medias_id', $media->id)->pluck('posts_id')->toArray();
            if (!count(array_intersect($posts, $postIds))) {
                $post = new Posts();
                $post->updated_at = $media->uploaded_at;
                $post->permission = 0;
                $post->save();
                $postplace = new PostsPlaces();
                $postplace->posts_id = $post->id;
                $postplace->places_id = $place->id;
                $postplace->save();
                $postmedia = new PostsMedia();
                $postmedia->posts_id = $post->id;
                $postmedia->medias_id = $media->id;
                $postmedia->save();
            }
        }
    }

    public function getEvents($places_ids, $countries_ids, $cities_ids)
    {
        $get_posts_events_collection = Events::where(function ($query) use ($places_ids, $countries_ids, $cities_ids) {
            $query->whereIn('places_id', $places_ids->all())
                ->orWhereIn('countries_id', $countries_ids->all())
                ->orWhereIn('cities_id', $cities_ids->all());
        })
            ->whereRaw('TIMESTAMP(end) > "' . date("Y-m-d H:i:s") . '"')
            ->with(['place.reviews', 'country.reviews', 'city.reviews'])
            ->orderBy('id', 'desc')
            ->get();

        //        $output = '';

        //        if (count($get_posts_events_collection) > 0) {
        //            foreach ($get_posts_events_collection AS $gpec) {
        //                $view = View::make('site.home.partials.post_event',
        //                    [
        //                        'place' => $gpec->place ?: ($gpec->country ?: ($gpec->city ?: '')),
        //                        'evt' => $gpec
        //                    ]);
        //                $output .= $view->render();
        //            }
        //        }
        //
        //        return json_encode($output);
    }

    public function getLocationSearchResult(Request $request)
    {
        $query = $request->get('q');

        if ($query != '') {
            $results = array();

            $all_places = $this->getElSearchResult($query);
            // $all_places = Place::with('transsingle')
            //     ->whereHas('transsingle', function ($q) use ($query) {
            //         $q->where('title', 'LIKE', "%$query%");
            //     })
            //     ->limit(20)
            //     ->get();
            $counter = 0;
            foreach ($all_places['result'] as $place) {
                $counter++;
                if ($counter <= 20) {
                    $results[] = array(
                        'id' => $place['place_id'],
                        'title' => @$place['place_title'],
                        'description' => !empty(@$place['address']) ? @$place['address'] : '',
                        'img' => isset($place['place_media']) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place['place_media'] : asset('assets2/image/placeholders/pattern.png'),
                        'type' => 'Place'
                    );
                }
            }


            $all_places = Countries::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($query) {
                    $q->where('title', 'LIKE', "%$query%");
                })
                ->limit(20)
                ->get();
            foreach ($all_places as $place) {
                $results[] = array(
                    'id' => $place->id,
                    'title' => @$place->transsingle->title,
                    'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                    'img' => isset($place->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                    'lng' => $place->lng,
                    'lat' => $place->lat,
                    'type' => 'Country'
                );
            }

            $all_places = Cities::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($query) {
                    $q->where('title', 'LIKE', "%$query%");
                })
                ->limit(20)
                ->get();
            foreach ($all_places as $place) {
                $results[] = array(
                    'id' => $place->id,
                    'title' => @$place->transsingle->title,
                    'description' => !empty(@$place->transsingle->description) ? @$place->transsingle->description : '',
                    'img' => isset($place->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                    'lng' => $place->lng,
                    'lat' => $place->lat,
                    'type' => 'City'
                );
            }
        } else {
            $results = array();

            if (!empty($request->lat) && $request->lat != "false" && !empty($request->lng) && $request->lng != "false") {
                $lat = $request->lat;
                $lon = $request->lng;
                $all_places = Place::with('trans')
                    ->select('places.id', 'places.lat', 'places.lng', DB::raw("6371 * acos(cos(radians(" . $lat . "))
                        * cos(radians(places.lat))
                        * cos(radians(places.lng) - radians(" . $lon . "))
                        + sin(radians(" . $lat . "))
                        * sin(radians(places.lat))) AS distance"))
                    ->groupBy("distance")
                    ->limit(3)
                    ->get();

                //$results = array();
                foreach ($all_places as $place) {
                    $results[] = array(
                        'id' => $place->id,
                        'title' => @$place->transsingle->title,
                        'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                        'img' => isset($place->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                        'lng' => $place->lng,
                        'lat' => $place->lat,
                        'type' => 'Place'
                    );
                }

                $all_places = Cities::with('trans')
                    ->select('cities.id', 'cities.lat', 'cities.lng', DB::raw("6371 * acos(cos(radians(" . $lat . "))
                        * cos(radians(cities.lat))
                        * cos(radians(cities.lng) - radians(" . $lon . "))
                        + sin(radians(" . $lat . "))
                        * sin(radians(cities.lat))) AS distance"))
                    ->groupBy("distance")
                    ->limit(3)
                    ->get();
                foreach ($all_places as $place) {
                    $results[] = array(
                        'id' => $place->id,
                        'title' => @$place->transsingle->title,
                        'description' => !empty(@$place->transsingle->description) ? @$place->transsingle->description : '',
                        'img' => isset($place->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                        'lng' => $place->lng,
                        'lat' => $place->lat,
                        'type' => 'City'
                    );
                }

                $all_places = Countries::with('trans')
                    ->select('countries.id', 'countries.lat', 'countries.lng', DB::raw("6371 * acos(cos(radians(" . $lat . "))
                        * cos(radians(countries.lat))
                        * cos(radians(countries.lng) - radians(" . $lon . "))
                        + sin(radians(" . $lat . "))
                        * sin(radians(countries.lat))) AS distance"))
                    ->groupBy("distance")
                    ->limit(3)
                    ->get();
                foreach ($all_places as $place) {
                    $results[] = array(
                        'id' => $place->id . ',' . @$place->transsingle->title . ',Country',
                        'title' => @$place->transsingle->title,
                        'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                        'img' => isset($place->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                        'lng' => $place->lng,
                        'lat' => $place->lat,
                        'type' => 'Country'
                    );
                }
            } else {
                $details = ip_details(getClientIP());
                if (array_key_exists('city', $details)) {
                    $cityName = $details['city'];
                    $city = Cities::with('transsingle')
                        ->whereHas('transsingle', function ($q) use ($cityName) {
                            $q->where('title', 'LIKE', "%$cityName%");
                        })
                        ->first();

                    if (!empty($city)) {
                        $results[] = array(
                            'id' => $city->id,
                            'title' => @$city->transsingle->title,
                            'description' => !empty(@$city->transsingle->description) ? @$city->transsingle->description : '',
                            'img' => isset($city->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $city->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                            'lng' => $city->lng,
                            'lat' => $city->lat,
                            'type' => 'City'
                        );
                        $cityId = $city->id;
                        $all_places = PlacesTop::where('city_id', $cityId)
                            ->limit(5)->get();

                        foreach ($all_places as $place) {
                            $results[] = array(
                                'id' => $place->id,
                                'title' => @$place->place->transsingle->title,
                                'description' => @$place->place->transsingle->description,
                                'img' => !empty(@$place->place->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . @$place->place->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                                'lng' => $place->place->lng,
                                'lat' => $place->place->lat,
                                'type' => 'Place'
                            );
                        }
                    }
                }
            }
        }

        return json_encode($results);
    }

    public function getTaggingSearchResult(Request $request)
    {
        $query = $request->get('q');
        $return = array();

        if ($query != '') {
            $results = array();

            $all_people = User::where('name', 'LIKE', "%$query%")
                ->limit(20)->get();
            foreach ($all_people as $user) {
                $results[] = array(
                    'id' => $user->id,
                    'title' => $user->name,
                    'description' => !empty($user->address) ? $user->address : '',
                    'img' => user_profile_picture($user),
                    'type' => 'People'
                );
            }
            $all_places = $this->getElSearchResult($query);
            // $all_places = Place::with('transsingle')
            //     ->whereHas('transsingle', function ($q) use ($query) {
            //         $q->where('title', 'LIKE', "%$query%");
            //     })
            //     ->limit(20)
            //     ->get();
            $counter = 0;
            foreach ($all_places['result'] as $place) {
                $counter++;
                if ($counter <= 20) {
                    $results[] = array(
                        'id' => $place['place_id'],
                        'title' => @$place['place_title'],
                        'description' => !empty(@$place['address']) ? @$place['address'] : '',
                        'img' => isset($place['place_media']) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place['place_media'] : asset('assets2/image/placeholders/pattern.png'),
                        'type' => 'Place'
                    );
                }
            }


            $all_countries = Countries::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($query) {
                    $q->where('title', 'LIKE', "%$query%");
                })
                ->limit(20)
                ->get();
            foreach ($all_countries as $place) {
                $results[] = array(
                    'id' => $place->id,
                    'title' => @$place->transsingle->title,
                    'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                    'img' => isset($place->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                    'type' => 'Country'
                );
            }

            $all_cities = Cities::with('transsingle')
                ->whereHas('transsingle', function ($q) use ($query) {
                    $q->where('title', 'LIKE', "%$query%");
                })
                ->limit(20)
                ->get();
            foreach ($all_cities as $place) {
                $results[] = array(
                    'id' => $place->id,
                    'title' => @$place->transsingle->title,
                    'description' => !empty(@$place->transsingle->description) ? @$place->transsingle->description : '',
                    'img' => isset($place->getMedias[0]->url) ? 'https://s3.amazonaws.com/travooo-images2/th1100/' . $place->getMedias[0]->url : asset('assets2/image/placeholders/pattern.png'),
                    'type' => 'City'
                );
            }

            $return = [
                'results' => $results,
                'counts' => [
                    'places' => count(@$all_places['result']),
                    'cities' => count($all_cities),
                    'countries' => count($all_countries),
                    'people' => count($all_people),
                ]
            ];
        }
        return json_encode($return);
    }


    public function getSearchPlaces(Request $request)
    {
        $query = $request->get('q');

        if ($request->has('lat'))
            $lat = $request->get('lat');
        if ($request->has('lng'))
            $lng = $request->get('lng');
        if ($request->has('search_in'))
            $search_in = $request->get('search_in');
        if ($request->has('search_id')) {
            $search_id = $request->get('search_id');
            if ($search_id !== "false")
                $search_in = $search_in . "_id";
        }

        $allowed_types = array(
            'airport', 'amusement_park', 'aquarium', 'art_gallery', 'bakery', 'bar', 'cafe', 'campground', 'casino', 'cemetery', 'church',
            'city_hall', 'embassy', 'hindu_temple', 'library', 'light_rail_station', 'lodging', 'meal_delivery', 'meal_takeaway', 'mosque', 'movie_theater', 'museum',
            'airport', 'night_club', 'park', 'restaurant', 'shopping_mall', 'stadium', 'subway_station', 'synagogue', 'tourist_attraction', 'train_station', 'transit_station',
            'zoo', 'food', 'place_of_worship', 'point_of_interest', 'poi'
        );
        //return json_encode($lat);
        if ($query != '') {


            $google_link = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
                . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
            if ($lat && $lng) {
                $google_link .= '&location=' . $lat . ',' . $lng . '&query=' . urlencode($query);
            } else {
                $google_link .= '&query=' . $query;
            }

            $fetch_link = file_get_contents($google_link);
            $query_result = json_decode($fetch_link);
            $qr = $query_result->results;


            //dd($query_result->next_page_token);
            //$result = $query_result1->results;

            $final_results = array();
            foreach ($qr as $r) {

                if ($r->types[0] !== "restaurant" && $r->types[0] !== "lodging") {

                    if (!Place::where('provider_id', '=', $r->place_id)->exists()) {

                        $p = new Place();
                        $p->place_type = @join(",", $r->types);
                        $p->safety_degrees_id = 1;
                        $p->provider_id = $r->place_id;
                        $p->countries_id = 373;
                        $p->cities_id = 2031;
                        $p->lat = $r->geometry->location->lat;
                        $p->lng = $r->geometry->location->lng;
                        $p->pluscode = $r->plus_code->compound_code;
                        $p->rating = $r->rating;
                        $p->active = 1;
                        $p->auto_import = 1;
                        $p->save();

                        $pt = new \App\Models\Place\PlaceTranslations();
                        $pt->languages_id = 1;
                        $pt->places_id = $p->id;
                        $pt->title = $r->name;
                        $pt->address = $r->formatted_address;
                        //if (isset($places[$k]['phone']))
                        //    $pt->phone = $places[$k]['phone'];
                        //if (isset($places[$k]['website']))
                        //    $pt->description = $places[$k]['website'];
                        //$pt->working_days = $places[$k]['working_days'];
                        $pt->save();

                        $medium = array();
                        $medium['id'] = @$p->id;
                        $medium['place_type'] = @$r->types[0];
                        $medium['address'] = $r->formatted_address;
                        $medium['title'] = $r->name;
                        $medium['lat'] = $r->geometry->location->lat;
                        $medium['lng'] = $r->geometry->location->lng;
                        $medium['rating'] = $r->rating;
                        $medium['location'] = $r->geometry->location->lat . "," . $r->geometry->location->lng;
                    } else {
                        $p = Place::where('provider_id', '=', $r->place_id)->get()->first();

                        $medium = array();
                        $medium['id'] = @$p->id;
                        $medium['place_type'] = @explode(",", $p->place_type)[0];
                        $medium['address'] = $p->transsingle->address;
                        $medium['title'] = $p->transsingle->title;
                        $medium['lat'] = $p->lat;
                        $medium['lng'] = $p->lng;
                        $medium['rating'] = $p->rating;
                        $medium['location'] = $p->lat . "," . $p->lng;
                        if (isset($p->getMedias[0]) && is_object($p->getMedias[0]))
                            $medium['image'] = $p->getMedias[0]->url;
                    }


                    $final_results[] = $medium;
                }
            }
            return json_encode($final_results);
        }
    }


    function srt_results($a, $title)
    {
        return strcmp($a["title"], $title);
    }

    function getElSearchResult($query, $lat = null, $lng = null)
    {
        $curl = curl_init();
        $query = urlencode($query);
        $geo_distance = '';
        if (!is_null($lat) and !is_null($lng)) {

            $geo_distance = '
            {
                "query": {
                    "bool": {
                      "should": [
                        {
                          "match": {
                            "title": {
                              "query": "' . $query . '",
                              "operator": "and"
                            }
                          }
                        }
                      ]
                    }
                  },
                  "size" : 5,
                  "sort" : [
                    {
                      "_score" : {
                        "order" : "desc"
                      }
                    }
                  ]
            }';
        }
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/pois/_search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => $geo_distance,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json;charset=utf-8',
                'Connection: Keep-Alive'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $response_array = [];
        if ($err) {
            return [];
        } else {
            $response = json_decode($response, true);
            //dd($response);
            if (isset($response['hits']) && count($response['hits']['hits']) != 0) {
                foreach ($response['hits']['hits'] as $hits) {
                    // we need only to show exact matches
                    if ($hits['_source']['title'] == $query) {
                        $response_array[] = $hits['_source'];
                    }
                }
                return [
                    'result' => $response_array,
                    'total' => $response['hits']['total']
                ];
            } else {
                return [
                    'result' => [],
                    'total' => 0
                ];
            }
        }
    }

    function getElSearchResult2($query, $lat = null, $lng = null)
    {
        $curl = curl_init();
        $query = urlencode($query);
        $geo_distance = '';
        if (!is_null($lat) and !is_null($lng)) {

            $geo_distance = '
            {
                "query": {
                    "bool": {
                      "should": [
                        {
                          "match": {
                            "title": {
                              "query": "' . $query . '",
                              "operator": "and"
                            }
                          }
                        }
                      ]
                    }
                  },
                  "size" : 100,
                  "sort" : [
                    {
                      "_geo_distance" : {
                        "location" : {
                          "lat" : ' . $lat . ',
                          "lon" : ' . $lng . '
                        },
                        "order" : "asc",
                        "unit" : "km"
                      }
                    },
                    {
                      "_score" : {
                        "order" : "desc"
                      }
                    }
                  ]
            }';

            /*
            $geo_distance = '{
                "sort" : [
                  {
                    "_geo_distance" : {
                      "location" : {
                        "lat" : ' . $lat . ',
                        "lon" : ' . $lng . '
                      },
                      "order" : "asc",
                      "unit" : "km"
                    }
                  },
                  {
                    "_score" : {
                      "order" : "desc"
                    }
                  }
                ]
              }
            ';
            */
        }

        //dd($geo_distance);

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/pois/_search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => $geo_distance,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json;charset=utf-8',
                'Connection: Keep-Alive'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $response_array = [];
        if ($err) {
            return [];
        } else {
            $response = json_decode($response, true);
            //dd($response);
            if (isset($response['hits']) && count($response['hits']['hits']) != 0) {
                foreach ($response['hits']['hits'] as $hits) {
                    $response_array[] = $hits['_source'];
                }
                return [
                    'result' => $response_array,
                    'total' => $response['hits']['total']
                ];
            } else {
                return [
                    'result' => [],
                    'total' => 0
                ];
            }
        }
    }


    function _doElSearch($query, $lat = null, $lng = null, $place_type = null, $city_id = null, $country = null, $size = 100)
    {
        $curl = curl_init();
        //$query = urlencode($query);
        $geo_distance = '';
        $el_data = false;

        if (!is_null($place_type) and !is_null($city_id)) {

            $el_data = array(
                'query' => array(
                    'bool' => array(
                        'must' => array(
                            'match' => array(
                                'place_type' => $place_type
                            )
                        ),
                        'filter' => array(
                            'term' => array(
                                'cities_id' => $city_id
                            )
                        )
                    )

                ),
                'size' => $size,
                'sort' => array(
                    'top' => array(
                        'order' => 'desc'
                    ),
                    'rating.keyword' => array(
                        'order' => 'desc'
                    ),
                    '_score' => array(
                        'order' => 'desc'
                    )
                )
            );
        } elseif (is_null($place_type) and !is_null($city_id)) {

            $el_data = array(
                'query' => array(
                    'bool' => array(
                        'must' => array(
                            'match' => array(
                                'title' => array(
                                    'query' => $query,
                                    'operator' => 'and'
                                )
                            )
                        ),
                        'filter' => array(
                            'term' => array(
                                'cities_id' => $city_id
                            )
                        )
                    )

                ),
                'size' => $size,
                'sort' => array(
                    'top' => array(
                        'order' => 'desc'
                    ),
                    'rating.keyword' => array(
                        'order' => 'desc'
                    ),
                    '_score' => array(
                        'order' => 'desc'
                    )
                )
            );
        } elseif (!is_null($place_type) and is_null($city_id)) {
            //dd($place_type);

            $el_data = array(
                'query' => array(
                    'bool' => array(
                        'must' => array(
                            'match' => array(
                                'place_type' => $place_type
                            )
                        )
                    )

                ),
                'size' => $size,
                'sort' => array(

                    '_geo_distance' => array(
                        'location' => array(
                            'lat' => $lat,
                            'lon' => $lng
                        ),
                        'order' => 'asc',
                        'unit' => 'km'
                    ),
                    'rating.keyword' => array(
                        'order' => 'desc'
                    ),
                    '_score' => array(
                        'order' => 'desc'
                    )
                )
            );
        } elseif (!is_null($query)) {
            $el_data = array(
                'query' => array(
                    'bool' => array(
                        'should' => array(
                            'match' => array(
                                'title' => array(
                                    'query' => $query,
                                    'operator' => 'and'
                                )
                            )
                        )
                    )
                ),
                'size' => $size,
                'sort' => array(

                    'top' => array(
                        'order' => 'desc'
                    ),
                    'rating.keyword' => array(
                        'order' => 'desc'
                    ),

                    '_score' => array(
                        'order' => 'desc'
                    )
                )
            );
        }

        // if lat & lng are passed, order results by distance
        if (!is_null($lat) and !is_null($lng)) {
            $el_data['sort'] = array(


                '_geo_distance' => array(
                    'location' => array(
                        'lat' => $lat,
                        'lon' => $lng
                    ),
                    'order' => 'asc',
                    'unit' => 'km'
                ),
                'top' => array(
                    'order' => 'desc'
                ),
                'rating.keyword' => array(
                    'order' => 'desc'
                ),

                '_score' => array(
                    'order' => 'desc'
                )
            );
        }


        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/pois/_search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => json_encode($el_data),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json;charset=utf-8',
                'Connection: Keep-Alive'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        //dd($response);
        curl_close($curl);
        $response_array = [];
        if ($err) {
            return [];
        } else {
            $response = json_decode($response, true);
            //dd($response);
            if (isset($response['hits']) && count($response['hits']['hits']) != 0) {
                foreach ($response['hits']['hits'] as $hits) {
                    $response_array[] = $hits['_source'];
                }
                return [
                    'result' => $response_array,
                    'total' => $response['hits']['total']
                ];
            } else {
                return [
                    'result' => [],
                    'total' => 0
                ];
            }
        }
    }

    public function getSearchPOIs(Request $request)
    {

        $query = $request->get('q');

        $lat = null;
        $lng = null;
        $city_id = null;
        $result_for_return = array();

        if ($request->has('lat'))
            $lat = $request->get('lat');
        if ($request->has('lng'))
            $lng = $request->get('lng');
        if ($request->hasHeader('cf-ipcountry') && (($lat == "null" && $lng == "null") or ($lat == "false" && $lng == "false"))) {
            $ip_country = $request->header('cf-ipcountry');
            $loc_country = \App\Models\Country\Countries::where('iso_code', $ip_country)->get()->first();
            $lat = $loc_country->lat;
            $lng = $loc_country->lng;
        }
        if ($request->has('search_in'))
            $search_in = $request->get('search_in');
        if ($request->has('search_id')) {
            $search_id = $request->get('search_id');
            if ($search_id !== "false") {
                $search_in = $search_in . "_id";
            }
        }

        if ($request->has('city_id')) {
            $city_id = $request->get('city_id');
        }

        if ($request->has('type')) {
            $type = $request->get('type');
        }

        $withoutUsers = $request->get('without_users', false);

        $fromExternal = false;

        $countries = [];
        $cities = [];
        $place_query = '';
        $all_results = array();

        $types_conversion = array(
            "restaurants" => "restaurant",
            "restaurant" => "restaurant",
            "food" => "restaurant",
            "hotels" => "lodging",
            "hotel" => "lodging"
        );

        $allowed_types = array(
            'premise', 'poi', 'pharmacy', 'airport', 'amusement_park', 'aquarium', 'art_gallery', 'bakery', 'bar', 'cafe', 'campground', 'casino', 'cemetery', 'church',
            'city_hall', 'embassy', 'hindu_temple', 'library', 'light_rail_station', 'lodging', 'meal_delivery', 'meal_takeaway', 'mosque', 'movie_theater', 'museum',
            'airport', 'night_club', 'park', 'restaurant', 'shopping_mall', 'stadium', 'subway_station', 'synagogue', 'tourist_attraction', 'train_station', 'transit_station',
            'zoo', 'food', 'place_of_worship', 'point_of_interest', 'bus_station'
        );

        if ($query != '') {
            $queryParam = $query;

            $queryParam = $query;
            $translate_query = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($queryParam) . '&sensor=false&key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&language=en');
            $tq = json_decode($translate_query);


            $medium = [];
            $result = [];

            // SEARCH USERS //
            if (!$withoutUsers) {
                $get_users = User::where('name', 'like', "%" . $query . "%")->get();
                foreach ($get_users as $u) {
                    $medium['id'] = @$u->id;
                    $medium['place_type'] = 'user';
                    $medium['address'] = '';
                    $medium['title'] = @$u->name;
                    $medium['lat'] = '';
                    $medium['lng'] = '';
                    $medium['rating'] = 0;
                    $medium['location'] = '';
                    $medium['image'] = $u->profile_picture;
                    $medium['cities_id'] = 0;
                    $medium['city_title'] = '';

                    $result[] = $medium;
                }
            }

            // SEARCH CASE 1 ----------//
            //  search query for cities & countries: ex: "Riyadh" or "الرياض"  or "Saudi Arabia"
            // this  should list matched country or city then top places inside them
            if (
                $translate_query and !str_contains(strtolower($query), ' in ') and !str_contains(strtolower($query), ' near ')
                and is_array($tq->results) and count($tq->results) and ($tq->results[0]->types[0] == "locality" or $tq->results[0]->types[0] == "country")
            ) {

                $ct = $tq->results[0]->address_components[0]->long_name;

                $place_query = $ct;

                $get_countries = Countries::with('transsingle')->whereHas('transsingle', function ($query) use ($queryParam, $ct) {
                    $query->where('title', 'like', "%" . $queryParam . "%")
                        ->orWhere('title', 'like', "%" . $ct . "%");
                })
                    ->get();

                //var_dump($get_countries);


                foreach ($get_countries as $cnt) {

                    $medium['id'] = "country_" . $cnt->id;
                    $medium['place_type'] = 'country';
                    $medium['address'] = @$cnt->region->transsingle->title;
                    $medium['title'] = $cnt->transsingle->title;
                    $medium['lat'] = $cnt->lat;
                    $medium['lng'] = $cnt->lng;
                    $medium['rating'] = @$cnt->rating;
                    $medium['location'] = $cnt->lat . "," . $cnt->lng;
                    if (isset($cnt->getMedias[0]) && is_object($cnt->getMedias[0]))
                        $medium['image'] = $cnt->getMedias[0]->url;
                    $medium['city_id'] = 0;
                    $medium['city_title'] = '';

                    $result[] = $medium;
                    $countries[] = $cnt->id;
                }



                $get_cities = Cities::with('transsingle')->whereHas('transsingle', function ($query) use ($queryParam, $ct) {
                    $query->where('title', 'like', "%" . $queryParam . "%")
                        ->orWhere('title', 'like', "%" . $ct . "%");
                })
                    ->get();

                //dd($get_countries);

                foreach ($get_cities as $cit) {
                    $medium = array();

                    $medium['id'] = "city_" . $cit->id;
                    $medium['place_type'] = 'city';
                    $medium['address'] = @$cit->country->transsingle->title;
                    $medium['title'] = $cit->transsingle->title;
                    $medium['lat'] = $cit->lat;
                    $medium['lng'] = $cit->lng;
                    $medium['rating'] = @$cit->rating;
                    $medium['location'] = $cit->lat . "," . $cit->lng;
                    if (isset($cit->getMedias[0]) && is_object($cit->getMedias[0]))
                        $medium['image'] = $cit->getMedias[0]->url;
                    $medium['cities_id'] = 0;
                    $medium['city_title'] = '';

                    $result[] = $medium;
                    $cities[] = $cit->id;
                }

                if ($countries) {

                    $country_tp = PlacesTop::whereIn("country_id", $countries)
                        //->orderBy('reviews_num', 'desc')
                        //->orderBy('rating', 'desc')
                        ->orderBy('order_by_city', 'desc')
                        ->limit(10)
                        ->get();

                    foreach ($country_tp as $cnt) {

                        $p = Place::find($cnt->places_id);

                        $medium['id'] = @$p->id;
                        $medium['place_type'] = @explode(",", $p->place_type)[0];
                        $medium['address'] = str_replace("'", "&quot;", $p->transsingle->address);
                        $medium['title'] = $p->transsingle->title;
                        $medium['lat'] = $p->lat;
                        $medium['lng'] = $p->lng;
                        $medium['rating'] = @$p->rating;
                        $medium['location'] = $p->lat . "," . $p->lng;
                        if (isset($p->getMedias[0]) && is_object($p->getMedias[0]))
                            $medium['image'] = $p->getMedias[0]->url;

                        $medium['cities_id'] = @$p->cities_id;
                        $medium['city_title'] = @$p->city->transsingle->title;

                        $result[] = $medium;
                    }
                }
                if ($cities) {

                    $city_tp = PlacesTop::whereIn("city_id", $cities)
                        //->orderBy('reviews_num', 'desc')
                        //->orderBy('rating', 'desc')
                        ->orderBy('order_by_city', 'desc')
                        ->limit(10)
                        ->get();

                    foreach ($city_tp as $cit) {

                        $p = Place::find($cit->places_id);

                        $medium['id'] = @$p->id;
                        $medium['place_type'] = @explode(",", $p->place_type)[0];
                        $medium['address'] = str_replace("'", "&quot;", $p->transsingle->address);
                        $medium['title'] = $p->transsingle->title;
                        $medium['lat'] = $p->lat;
                        $medium['lng'] = $p->lng;
                        $medium['rating'] = @$p->rating;
                        $medium['location'] = $p->lat . "," . $p->lng;
                        if (isset($p->getMedias[0]) && is_object($p->getMedias[0]))
                            $medium['image'] = $p->getMedias[0]->url;

                        $medium['cities_id'] = @$p->cities_id;
                        $medium['city_title'] = @$p->city->transsingle->title;

                        $result[] = $medium;
                    }
                }

                $all_results = $result;

                //dd($all_results);
            }
            // SEARCH CASE 3 ----------//
            // search query using "in", ex: "Restraurants in Riyadh"

            elseif (str_contains(strtolower($query), ' in ')) {
                $exp = explode(" in ", $query);
                $query_subject = strtolower(trim($exp[0]));
                $query_location = strtolower(trim($exp[1]));
                if (isset($types_conversion[$query_subject])) {
                    $required_type = $types_conversion[$query_subject];
                    $in_city = CitiesTranslations::where('title', 'like', "%$query_location%")->get()->first();
                    $temp_in = $this->_doElSearch($query_subject, $lat, $lng, $required_type, $in_city->cities_id);
                    $all_results = $temp_in['result'];
                }
                //dd($all_results);

            }
            // SEARCH CASE 4 ----------//
            // search query using "near", ex: "Restaurants near Riyadh Front"
            elseif (str_contains(strtolower($query), 'near')) {
                $exp = explode(" near ", $query);
                $query_subject = strtolower(trim($exp[0]));
                $query_location = strtolower(trim($exp[1]));
                if (isset($types_conversion[$query_subject])) {
                    $required_type = $types_conversion[$query_subject];
                    $near_place = \App\Models\Place\PlaceTranslations::where('title', 'like', "%$query_location%")->get()->first()->place;
                    $temp_near = $this->_doElSearch($query_subject, $near_place->lat, $near_place->lng, $required_type);
                    $all_results = $temp_near['result'];
                }
            } else {
                // exact match
                $temp_exact = $this->_doElSearch($query, null, null, null, $city_id, null, 5);
                //dd($temp_exact);

                //$all_results[] = $temp_exact['result'];
                // other matches
                $temp_other = $this->_doElSearch($query, $lat, $lng, null, $city_id);
                //$all_results[] = $temp_other['result'];
                //dd($temp_other);

                $all_results = array_unique(array_merge($result, $temp_exact['result'], $temp_other['result']), SORT_REGULAR);
                //dd($all_results);
            }

            if ($place_query != '') {
                $el_query = $place_query;
            } else {
                $el_query = $query;
            }

            $qr = $all_results;


            foreach ($qr as $k => $r) {
                $rpt = explode(",", $r['place_type']);
                if (in_array($rpt[0], $allowed_types) or $rpt[0] == "city" or $rpt[0] == "user") {
                    //if ($r['countries_id'] != 326 && $r['cities_id'] != 2031 && $r['countries_id'] != 373) {

                    $medium['id'] = @$r['id'];
                    $medium['address'] = $r['address'];
                    $medium['title'] = $r['title'];
                    $medium['lat'] = $r['lat'];
                    $medium['place_type'] = @explode(",", @$r['place_type'])[0];
                    $medium['lng'] = $r['lng'];
                    $medium['location'] = $r['lat']  . "," . $r['lng'];
                    $medium['image'] = isset($r['image']) ? $r['image'] : '';

                    $medium['city_id'] = $r['cities_id'];

                    $city = @Cities::find($r['cities_id']);

                    $cityImg = @$city->first_city_medias[0]->url;
                    $citySrc = S3_BASE_URL . 'th180/' . $cityImg;

                    if (!$cityImg) {
                        $citySrc = url(PLACE_PLACEHOLDERS);
                    }

                    $medium['city_title'] = @$city->transsingle->title;
                    $medium['city_img'] = $citySrc;

                    $medium['reviews'] = Reviews::where('places_id', $medium['id'])->get();
                    $medium['reviews_avg'] = $medium['reviews']->avg('score');
                    //$medium['type'] = '';

                    $result_for_return[] = $medium;
                }
            }

            if (count($result_for_return) == 0) {
                $result_for_return = $this->_searchGoogle($query, 0, 0, $city_id);
            }


            return json_encode(array_values($result_for_return));
        }
    }


    public function _searchGoogle($query, $lat = 0, $lng = 0, $city_id = null)
    {
        if ($query != '') {
            $google_link = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?inputtype=textquery&fields=types,photos,formatted_address,name,opening_hours,rating,place_id,geometry&key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
            if ($lat && $lng) {
                $google_link .= '&locationbias=circle:2000@47.6918452,-122.2226413&location=' . $lat . ',' . $lng . '&input=' . urlencode($query);
            } else {
                $google_link .= '&input=' . urlencode($query);
            }

            $fetch_link = file_get_contents($google_link);
            $query_result = json_decode($fetch_link);
            $qr = $query_result->candidates;

            //dd($qr);


            $final_results = array();
            foreach ($qr as $r) {
                $placeQuery = Place::query();
                $medium = array();

                if (count($placeQuery->where('provider_id', $r->place_id)->get()) == 0) {

                    $p = new Place();
                    $p->place_type = @join(",", $r->types);
                    //$p->safety_degrees_id = 1;
                    $p->provider_id = $r->place_id;
                    $p->countries_id = 326;
                    $p->cities_id = 2031;
                    $p->lat = $r->geometry->location->lat;
                    $p->lng = $r->geometry->location->lng;
                    //$p->pluscode = $r->plus_code->compound_code;
                    $p->rating = isset($r->rating) ? $r->rating : 0;
                    $p->active = 1;
                    $p->auto_import = 1;
                    $p->save();

                    $pt = new \App\Models\Place\PlaceTranslations();
                    $pt->languages_id = 1;
                    $pt->places_id = $p->id;
                    $pt->title = $r->name;
                    $pt->address = $r->formatted_address;
                    //if (isset($places[$k]['phone']))
                    //    $pt->phone = $places[$k]['phone'];
                    //if (isset($places[$k]['website']))
                    //    $pt->description = $places[$k]['website'];
                    //$pt->working_days = $places[$k]['working_days'];
                    $pt->save();


                    $medium = array();
                    $medium['id'] = @$p->id;
                    $medium['place_type'] = @$r->types[0];
                    $medium['address'] = $r->formatted_address;
                    $medium['title'] = $r->name;
                    $medium['lat'] = $r->geometry->location->lat;
                    $medium['lng'] = $r->geometry->location->lng;
                    $medium['rating'] = isset($r->rating) ? $r->rating : 0;
                    $medium['location'] = $r->geometry->location->lat . "," . $r->geometry->location->lng;


                    // sync to EL
                    $data = array(
                        'type' => 'poi',
                        'id' => $p->id,
                        'countries_id' => $p->countries_id,
                        'cities_id' => $p->cities_id,
                        'title' => @$p->transsingle->title,
                        'address' => @$p->transsingle->address,
                        'image' => @$p->getMedias[0]->url,
                        'place_type' => @$p->place_type,
                        'lat' => @$p->lat,
                        'lng' => @$p->lng,
                        'location' => @$p->lat . "," . @$p->lng,
                        'rating' => @$p->rating
                    );

                    $json_data = json_encode($data);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/pois/_doc/" . $p->id);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    $headers = [
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($json_data)
                    ];

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    if (!$result = curl_exec($ch)) {
                        trigger_error(curl_error($ch));
                    }

                    curl_close($ch);
                } else {
                    if ($city_id) {
                        $placeQuery->where('cities_id', $city_id);
                    }

                    $ap = $placeQuery->where('provider_id', $r->place_id)->get()->first();

                    if (!$ap) {
                        continue;
                    }

                    $medium = array();
                    $medium['id'] = @$ap->id;
                    $medium['place_type'] = @$ap->place_type;
                    $medium['address'] = $ap->transsingle->address;
                    $medium['title'] = $ap->transsingle->title;
                    $medium['lat'] = $ap->lat;
                    $medium['lng'] = $ap->lng;
                    $medium['rating'] = @$ap->rating;
                    $medium['location'] = $ap->lat . "," . $ap->lng;
                }

                $final_results[] = $medium;
            }
            return $final_results;
        }
    }

    public function getSuggestTopPlaces(Request $request, TrendsService $trendsService)
    {
        $city_id = $request->get('city_id');

        $final = [];
        $place_suggestions = $trendsService->getRecursiveTrendingLocations(TrendsService::LOCATION_TYPE_PLACE, $city_id, false, [], [], 3, false);

        foreach ($place_suggestions as $ps) {
            $medium = [];
            $medium['id'] = $ps['location']->id;
            $medium['place_type'] = @explode(",", $ps['location']->place_type)[0];
            $medium['address'] = $ps['location']->transsingle->address;
            $medium['title'] = $ps['location']->transsingle->title;
            $medium['lat'] = $ps['location']->lat;
            $medium['lng'] = $ps['location']->lng;
            $medium['location'] = $ps['location']->lat . "," . $ps['location']->lng;
            if (isset($ps['location']->getMedias[0]) && is_object($ps['location']->getMedias[0])) {
                $medium['image'] = $ps['location']->getMedias[0]->url;
            }
            $medium['city_id'] = @$ps['location']->cities_id;
            $medium['city_title'] = @$ps['location']->city->transsingle->title;
            $medium['type'] = $ps['place_type'];
            $medium['reviews'] = Reviews::where('places_id', $ps['location']->id)->get();
            $medium['reviews_avg'] = Reviews::where('places_id', $ps['location']->id)->avg('score');

            $final[] = $medium;
        }

        return json_encode($final);
    }


    public function getSearchRestaurants(Request $request)
    {
        $query = $request->get('q');
        if ($request->has('lat'))
            $lat = $request->get('lat');
        if ($request->has('lng'))
            $lng = $request->get('lng');
        if ($request->has('search_in'))
            $search_in = $request->get('search_in');
        if ($request->has('search_id')) {
            $search_id = $request->get('search_id');
            if ($search_id !== "false")
                $search_in = $search_in . "_id";
        }

        if ($query != '') {
            $query = "Restaurant " . $query;
            $google_link = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
                . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
            if ($lat && $lng) {
                $google_link .= '&location=' . $lat . ',' . $lng . '&query=' . urlencode($query);
            } else {
                $google_link .= '&query=' . $query;
            }

            $fetch_link = file_get_contents($google_link);
            $query_result = json_decode($fetch_link);
            $qr = $query_result->results;


            $final_results = array();
            foreach ($qr as $r) {

                $medium = array();

                if (!Restaurants::where('provider_id', '=', $r->place_id)->exists()) {

                    $p = new Restaurants();
                    $p->place_type = @join(",", $r->types);
                    //$p->safety_degrees_id = 1;
                    $p->provider_id = $r->place_id;
                    $p->countries_id = 373;
                    $p->cities_id = 2031;
                    $p->lat = $r->geometry->location->lat;
                    $p->lng = $r->geometry->location->lng;
                    //$p->pluscode = $r->plus_code->compound_code;
                    $p->rating = $r->rating;
                    $p->active = 1;
                    $p->auto_import = 1;
                    $p->save();

                    $pt = new \App\Models\Restaurants\RestaurantsTranslations();
                    $pt->languages_id = 1;
                    $pt->restaurants_id = $p->id;
                    $pt->title = $r->name;
                    $pt->address = $r->formatted_address;
                    //if (isset($places[$k]['phone']))
                    //    $pt->phone = $places[$k]['phone'];
                    //if (isset($places[$k]['website']))
                    //    $pt->description = $places[$k]['website'];
                    //$pt->working_days = $places[$k]['working_days'];
                    $pt->save();

                    // get media for restaurant




                    $medium = array();
                    $medium['id'] = @$p->id;
                    $medium['place_type'] = @$r->types[0];
                    $medium['address'] = $r->formatted_address;
                    $medium['title'] = $r->name;
                    $medium['lat'] = $r->geometry->location->lat;
                    $medium['lng'] = $r->geometry->location->lng;
                    $medium['rating'] = $r->rating;
                    $medium['location'] = $r->geometry->location->lat . "," . $r->geometry->location->lng;
                } else {
                    $p = Restaurants::where('provider_id', '=', $r->place_id)->get()->first();

                    $medium = array();
                    $medium['id'] = @$p->id;
                    $medium['place_type'] = @explode(",", $p->place_type)[0];
                    $medium['address'] = $p->transsingle->address;
                    $medium['title'] = $p->transsingle->title;
                    $medium['lat'] = $p->lat;
                    $medium['lng'] = $p->lng;
                    $medium['rating'] = $p->rating;
                    $medium['location'] = $p->lat . "," . $p->lng;
                    if (isset($p->getMedias[0]) && is_object($p->getMedias[0]))
                        $medium['image'] = $p->getMedias[0]->url;
                }



                $final_results[] = $medium;
            }
            return json_encode($final_results);
        }
    }

    public function getSearchHotels(Request $request)
    {
        if ($request->has('term')) {
            $query = $request->get('term');
        } else {
            $query = $request->get('q');
        }
        if ($request->has('lat'))
            $lat = $request->get('lat');
        if ($request->has('lng'))
            $lng = $request->get('lng');
        if ($request->has('search_in'))
            $search_in = $request->get('search_in');
        if ($request->has('search_id')) {
            $search_id = $request->get('search_id');
            if ($search_id !== "false")
                $search_in = $search_in . "_id";
        }

        if ($query != '') {
            $query = "Hotel " . $query;
            $google_link = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
                . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
            if ($lat && $lng) {
                $google_link .= '&location=' . $lat . ',' . $lng . '&query=' . urlencode($query);
            } else {
                $google_link .= '&query=' . $query;
            }

            $fetch_link = file_get_contents($google_link);
            $query_result = json_decode($fetch_link);
            $qr = $query_result->results;


            //dd($query_result->next_page_token);
            //$result = $query_result1->results;

            $final_results = array();
            foreach ($qr as $r) {

                $medium = array();

                if (!Hotels::where('provider_id', '=', $r->place_id)->exists()) {

                    $p = new Hotels();
                    $p->place_type = @join(",", $r->types);
                    //$p->safety_degrees_id = 1;
                    $p->provider_id = $r->place_id;
                    $p->countries_id = 373;
                    $p->cities_id = 2031;
                    $p->lat = $r->geometry->location->lat;
                    $p->lng = $r->geometry->location->lng;
                    //$p->pluscode = $r->plus_code->compound_code;
                    $p->rating = $r->rating;
                    $p->active = 1;
                    $p->auto_import = 1;
                    $p->save();

                    $pt = new \App\Models\Hotels\HotelsTranslations();
                    $pt->languages_id = 1;
                    $pt->hotels_id = $p->id;
                    $pt->title = $r->name;
                    $pt->address = $r->formatted_address;
                    //if (isset($places[$k]['phone']))
                    //    $pt->phone = $places[$k]['phone'];
                    //if (isset($places[$k]['website']))
                    //    $pt->description = $places[$k]['website'];
                    //$pt->working_days = $places[$k]['working_days'];
                    $pt->save();


                    $medium = array();
                    $medium['id'] = @$p->id;
                    $medium['place_type'] = @$r->types[0];
                    $medium['address'] = $r->formatted_address;
                    $medium['title'] = $r->name;
                    $medium['lat'] = $r->geometry->location->lat;
                    $medium['lng'] = $r->geometry->location->lng;
                    $medium['rating'] = $r->rating;
                    $medium['location'] = $r->geometry->location->lat . "," . $r->geometry->location->lng;
                    $medium['image'] = @$photo_url;
                } else {
                    $p = Hotels::where('provider_id', '=', $r->place_id)->get()->first();

                    $medium = array();
                    $medium['id'] = @$p->id;
                    $medium['place_type'] = @explode(",", $p->place_type)[0];
                    $medium['address'] = $p->transsingle->address;
                    $medium['title'] = $p->transsingle->title;
                    $medium['lat'] = $p->lat;
                    $medium['lng'] = $p->lng;
                    $medium['rating'] = $p->rating;
                    $medium['location'] = $p->lat . "," . $p->lng;
                    if (isset($p->getMedias[0]) && is_object($p->getMedias[0]))
                        $medium['image'] = $p->getMedias[0]->url;
                }

                $final_results[] = $medium;
            }
            return json_encode($final_results);
        }
    }

    public function getSearchHotelsMention(Request $request)
    {
        if ($request->has('term')) {
            $query = $request->get('term');
        } else {
            $query = $request->get('q');
        }
        if ($request->has('lat'))
            $lat = $request->get('lat');
        if ($request->has('lng'))
            $lng = $request->get('lng');
        if ($request->has('search_in'))
            $search_in = $request->get('search_in');
        if ($request->has('search_id')) {
            $search_id = $request->get('search_id');
            if ($search_id !== "false")
                $search_in = $search_in . "_id";
        }

        if ($query != '') {

            if (isset($lat) && $lat > 0 && isset($lng) && $lng > 0) {

                if (isset($search_in) && $search_in !== "false") {


                    $search_terms = array('bool' => array('must' => array(
                        array('match' => array($search_in => $search_id)),
                        array('match' => array('title' => $query))
                    )));

                    //var_dump(json_encode($search_terms));
                    $all_hotels = Hotels::searchByQuery(
                        $search_terms,
                        null,
                        null,
                        1000,
                        null,
                        array('_geo_distance' => array('location' => $lat . "," . $lng, 'order' => 'asc', 'unit' => 'km'))
                    );
                } else {

                    $search_terms = array('match' => array('title' => $query));

                    $all_hotels = Hotels::searchByQuery(
                        $search_terms,
                        null,
                        null,
                        1000,
                        null,
                        array('_geo_distance' => array('location' => $lat . "," . $lng, 'order' => 'asc', 'unit' => 'km'))
                    );
                }
            } else {
                if (isset($search_in) && $search_in !== "false") {


                    $search_terms = array('bool' => array('must' => array(
                        array('match' => array($search_in => $search_id)),
                        array('match' => array('title' => $query))
                    )));
                    $all_hotels = Hotels::searchByQuery(
                        $search_terms,
                        null,
                        null,
                        1000
                    );
                } else {
                    $search_terms = array('match' => array('title' => $query));

                    $all_hotels = Hotels::searchByQuery(
                        $search_terms,
                        null,
                        null,
                        1000
                    );
                }
            }
        }


        return json_encode($all_hotels);
    }

    public function getSearchCountries(Request $request)
    {
        $queryParam = $request->get('q');

        if ($queryParam != '') {

            $all_countries = Countries::with('transsingle')->with('region.transsingle')
                ->with('getMedias')
                ->whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })
                ->get();
            return json_encode($all_countries);
        }
    }

    public function getSearchCities(Request $request)
    {
        $queryParam = $request->get('q');

        if ($queryParam != '') {

            $all_cities = Cities::with('transsingle')->with('country.transsingle')
                ->with('getMedias')
                ->whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })
                ->get();
            return json_encode($all_cities);
        }
    }

    public function getSearchUsers(Request $request)
    {
        $queryParam = $request->get('q');
        $user_id = Auth::user()->id;

        if ($queryParam != '') {
            $all_users = User::where('name', 'like', "%" . $queryParam . "%")
                ->whereDoesntHave('setting_users_block', function ($query) use ($user_id) {
                    $query->where('blocked_users_id', $user_id);
                })
                ->get();
            return json_encode($all_users);
        }
    }

    public function getSearchCountriesCities(Request $request)
    {
        $queryParam = $request->get('q');

        if ($queryParam != '') {

            $all_countries = Countries::with('transsingle')
                ->whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })
                ->take(20)->get();

            $all_cities = Cities::with('transsingle')
                ->whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })
                ->take(20)->get();

            $results = array();

            foreach ($all_countries as $aco) {
                $results[] = array(
                    'id' => 'country,' . $aco->id,
                    'text' => $aco->transsingle->title,
                    'image' => check_country_photo(@$aco->getMedias[0]->url, 180),
                    'type' => '',
                    'query' => $queryParam
                );
            }
            foreach ($all_cities as $aci) {
                $results[] = array(
                    'id' => 'city,' . $aci->id,
                    'text' => $aci->transsingle->title,
                    'image' => check_country_photo(@$aci->getMedias[0]->url, 180),
                    'type' => @$aci->country->transsingle->title,
                    'query' => $queryParam
                );
            }


            return json_encode($results);
        }
    }

    public function getSearchTravelstyles(Request $request)
    {
        $queryParam = $request->get('q');

        if ($queryParam != '') {


            $all_travelstyles = \App\Models\Travelstyles\Travelstyles::with('transsingle')
                ->whereHas('transsingle', function ($query) use ($queryParam) {
                    $query->where('title', 'like', "%" . $queryParam . "%");
                })
                ->select('id')
                ->get();

            $results = array();

            foreach ($all_travelstyles as $ats) {
                $results[] = array('id' => $ats->id, 'text' => $ats->transsingle->title);
            }


            return json_encode(['results' => $results]);
        }
    }

    public function getListTravelstylesForSelect(Request $request)
    {

        $all_travelstyles = \App\Models\Travelstyles\Travelstyles::with('transsingle')->select('id')
            ->get();

        $results = array();

        foreach ($all_travelstyles as $ats) {
            $results[] = array('id' => $ats->id, 'text' => $ats->transsingle->title);
        }


        return json_encode(['results' => $results]);
    }

    public function getSelect2Trips(Request $request)
    {
        $query = $request->get('q');

        if ($query != '') {

            $all_places = TripPlans::where('title', 'LIKE', "%$query%")->get();
            $results = array();
            foreach ($all_places as $place) {
                //value.id+','+value.trans[0].title+','+value.lat+","+value.lng+',Place
                $results[] = array(
                    'id' => $place->id, 'text' => @$place->title
                );
            }



            $return = array('results' => $results);
            return json_encode($return);
        }
    }

    public function postAjaxGetMessages(Request $request)
    {
        $chatmodel = new ChatConversation;
        $count = $chatmodel->unread_count(Auth::guard('user')->user()->id);
        // $count = ChatConversationMessage::where('to_id', '=', Auth::guard('user')->user()->id)->where('is_read', '=', 0)->count();
        return json_encode(array('count' => $count));
    }

    public function postAjaxGetNotifications(Request $request, TripsService $tripsService)
    {
        if (Auth::guard('user')->check()) {
            $mark_as_seen = $request->get('view');

            $notifications = Notifications::where('users_id', Auth::guard('user')->user()->id);
            if ($mark_as_seen == 'yes') {
                $notifications->update(['read' => 1]);
            }
            $notifications = $notifications->with('author')->orderBy('created_at', 'DESC')->take(10)->get();

            $notifications_unseen = Notifications::where('users_id', Auth::guard('user')->user()->id)
                ->where('read', 0)
                ->orderBy('created_at', 'DESC')
                ->get();

            $notifications_html = '';

            foreach ($notifications as $not) {

                $beforeHtml = $notifications_html;
                $notifications_html .= '<div class="conv-block not-block">
                                        <div class="img-wrap">
                                            <img src="' . check_profile_picture($not->author->profile_picture) . '" alt="image" style="width:50px;height:50px;">
                                        </div>
                                        <div class="conv-txt">
                                            <div class="name">
                                                <a href="' . url('profile/' . $not->author->id) . '" class="inner-link not-profile-link">' . $not->author->name . '</a>';

                if ($not->type == 'plan_create') {
                    $notifications_html .= ' created a <a href="' . url('plan/' . $not->data_id) . '" class="inner-link">Trip Plan</a>';
                } elseif ($not->type == 'plan_invitation_received') {
                    $contrRequest = TripsContributionRequests::find($not->data_id);
                    if ($tripsService->isEventsExpired($contrRequest->plans_id)) {
                        $notifications_html = $beforeHtml;
                        continue;
                    }
                    $notifications_html .= ' invited you to participate in their <a href="' . url('trip/plan/' . $contrRequest->plans_id) . '" class="inner-link">Trip Plan</a>'
                        . ' <span>(<a href="" class="inner-link acceptPlanInvitation" data-id="' . $not->data_id . '">Accept</a> - <a href="" class="inner-link rejectPlanInvitation" data-id="' . $not->data_id . '">Reject</a>)</span>';
                } elseif ($not->type == 'plan_invitetion_accepted') {
                    $notifications_html .= ' accepted your inivtation';
                } elseif ($not->type == 'plan_invitetion_rejected') {
                    $notifications_html .= ' rejected your invitation';
                } elseif ($not->type == 'plan_invitetion_unpublished') {
                    $notifications_html .= ' unpublished ' . (($not->author->gender == 1) ? 'his' : 'her') . ' request.';
                } elseif ($not->type == 'plan_version_sent') {
                    $plan = TripPlans::find($not->data_id);
                    if (is_object($plan)) {
                        $notifications_html .= ' sent you their suggestion for your <a href="' . url('trip/plan/' . $plan->id) . '" class="inner-link">Trip Plan</a>'
                            . ' <span>(<a href="' . url('trip/plan/' . $plan->id) . '" class="inner-link">Review now</a>)</span>';
                    }
                } elseif ($not->type == 'status_like') {
                    $post = Posts::find($not->data_id);
                    if (is_object($post)) {
                        $notifications_html .= ' liked your post <i>' . strip_tags($post->text) . '</i>';
                    }
                } elseif ($not->type == 'status_commentlike') {
                    $post = Posts::find($not->data_id);
                    if (is_object($post)) {
                        $notifications_html .= ' liked your comment ';
                    }
                } elseif ($not->type == 'discussion_upvote') {
                    $reply = DiscussionReplies::find($not->data_id);
                    if (is_object($reply)) {
                        $notifications_html .= ' upvoted your reply <a href="' . url('discussions/#-' . $reply->discussions_id) . '" class="not-disc-link"></a>';
                    }
                } elseif ($not->type == 'status_unlike') {
                    $post = Posts::find($not->data_id);
                    if (is_object($post)) {
                        $notifications_html .= ' unliked your post <i>' . strip_tags($post->text) . '</i>';
                    }
                } elseif ($not->type == 'status_comment') {
                    $post = Posts::find($not->data_id);
                    if (is_object($post)) {
                        $notifications_html .= ' commented on your post <i>' . strip_tags($post->text) . '</i>';
                    }
                } elseif ($not->type == 'ask_to_join') {
                    $notifications_html .= ' wants to become your Travel Mate.</i>';
                } elseif ($not->type == 'approved_request') {
                    $notifications_html .= ' Join request accepted! You are now Travel Mates with <a href="' . url('profile/' . $not->author->id) . '" class="inner-link">' . $not->author->name . '</a>.</i>';
                } elseif ($not->type == 'approve') {
                    $notifications_html .= ' Your account approved. You got ' . $not->notes . ' Points';
                } elseif ($not->type == 'disapprove') {
                    $notifications_html .= ' Sorry, you didn\'t meet our criteria to be qualified as an expert.';
                } elseif ($not->type == 'approve_suggestion') {
                    $notifications_html .= ' Your suggestion was approved by plan admin';
                } elseif ($not->type == 'disapprove_suggestion') {
                    $notifications_html .= ' Your suggestion was disapproved by plan admin';
                } elseif ($not->type == 'post_delete') {
                    $notifications_html .=  $not->notes ?: 'Your post has been deleted';
                } elseif ($not->type == 'restore_post') {
                    $notifications_html .= $not->notes;
                } elseif ($not->type == 'copyright_policy') {
                    $notifications_html .=  '<a class="text-lowercase p-0" href="' . url('/copyright-policy/' . $not->data_id) . '">' .  $not->notes  . '</a>';
                } elseif ($not->type == 'plan_suggestion_added') {
                    $suggestion = TripsSuggestion::find($not->data_id);
                    if (!$suggestion) {
                        $notifications_html = $beforeHtml;
                        continue;
                    }

                    $plan = $suggestion->plan;
                    $notifications_html .= ' made a suggestion in <a href="' . url('trip/plan/' . $plan->id) . '" class="inner-link">' . $plan->title . '</a> plan';
                } elseif ($not->type == 'ask_experts') {

                    $notifications_html .= ' tagged you in ' . (($not->author->gender == 1) ? 'his' : 'her') . ' discussion.<a href="' . url('discussions/#-' . $not->data_id) . '" class="not-disc-link"></a>';
                } elseif ($not->type == 'plan_suggestion_added') {

                    $plan = @TripsSuggestion::find($not->data_id)->plan;
                    if (!$plan) {
                        $notifications_html = $beforeHtml;
                        continue;
                    }

                    $notifications_html .= ' made a suggestion in <a href="' . url('trip/plan/' . $plan->id) . '" class="inner-link">' . $plan->title . '</a> plan';
                }

                $notifications_html .= '</div>
                                            <div class="time">
                                                <p>' . diffForHumans($not->created_at) . '</p>
                                            </div>
                                        </div>
                                    </div>';
            }
            $return = array(
                'notifications' => $notifications_html,
                'count_unseen' => count($notifications_unseen),
                'view' => $mark_as_seen
            );
            return json_encode($return);
        }
    }



    public function getEl()
    {

        $get_places = Place::whereRaw('id not in (SELECT places_id FROM __elastic_places_done)')
            ->take(500)
            ->get();



        $data = array();

        foreach ($get_places as $place) {
            $data = array(
                'type' => 'poi',
                'id' => $place->id,
                'countries_id' => $place->countries_id,
                'cities_id' => $place->cities_id,
                'title' => @$place->transsingle->title,
                'address' => @$place->transsingle->address,
                'image' => @$place->getMedias[0]->url,
                'place_type' => @$place->place_type,
                'lat' => @$place->lat,
                'lng' => @$place->lng,
                'location' => @$place->lat . "," . @$place->lng,
                'rating' => @$place->rating
            );

            $json_data = json_encode($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/pois/_doc/" . $place->id);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $headers = [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data)
            ];

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            if (!$result = curl_exec($ch)) {
                trigger_error(curl_error($ch));
            }

            curl_close($ch);

            //dd($result);
            $result = json_decode($result);
            if ($result->result == 'created' or $result->result == 'updated') {
                DB::insert('insert into __elastic_places_done (places_id) values (?)', [$place->id]);
            }
        }
    }


    public function getElTop()
    {

        $get_places = PlacesTop::whereRaw('places_id not in (SELECT places_id FROM __elastic_topplaces_done)')
            ->take(500)
            ->get();



        $data = array();

        foreach ($get_places as $p) {
            $place = Place::find($p->places_id);
            $data = array(
                'type' => 'poi',
                'id' => $place->id,
                'countries_id' => $place->countries_id,
                'cities_id' => $place->cities_id,
                'title' => @$place->transsingle->title,
                'address' => @$place->transsingle->address,
                'image' => @$place->getMedias[0]->url,
                'place_type' => @$place->place_type,
                'lat' => @$place->lat,
                'lng' => @$place->lng,
                'location' => @$place->lat . "," . @$place->lng,
                'rating' => @$place->rating,
                'top' => 1
            );

            $json_data = json_encode($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/pois/_doc/" . $place->id);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $headers = [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data)
            ];

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            if (!$result = curl_exec($ch)) {
                trigger_error(curl_error($ch));
            }

            curl_close($ch);

            //dd($result);
            $result = json_decode($result);
            if ($result->result == 'created' or $result->result == 'updated') {
                DB::insert('insert into __elastic_topplaces_done (places_id) values (?)', [$place->id]);
            }
            //dd($place->id);
        }
    }

    public function getEl4()
    {

        $get_countries = Countries::all();


        $data = array();

        foreach ($get_countries as $country) {
            $data = array(
                'type' => 'country',
                'id' => $country->id,
                'countries_id' => 0,
                'cities_id' => 0,
                'title' => @$country->transsingle->title,
                'address' => @$country->transsingle->address,
                'image' => @$country->getMedias[0]->url,
                'place_type' => 'country',
                'lat' => @$country->lat,
                'lng' => @$country->lng,
                'rating' => @$country->rating,
                'location' => @$country->lat . "," . @$country->lng
            );

            //dd($data);

            $json_data = json_encode($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/places/_doc/c_" . $country->id);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $headers = [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data)
            ];

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            if (!$result = curl_exec($ch)) {
                trigger_error(curl_error($ch));
            }

            curl_close($ch);

            var_dump($result);
            $result = json_decode($result);
            //if ($result->result == 'created' or $result->result == 'updated') {
            //DB::insert('insert into __elastic_places_done (places_id) values (?)', [$place->id]);
            //}
        }
    }

    public function getReferral(Request $request, $referral_id)
    {

        $get_ref = ReferralLinks::findOrFail($referral_id);

        $click = new ReferralLinksViews;
        $click->links_id = $get_ref->id;
        $click->users_id = Auth::guard('user')->user()->id;
        $click->gender = Auth::guard('user')->user()->gender;
        $click->nationality = Auth::guard('user')->user()->nationality;
        $click->save();

        return redirect()->away($get_ref['target'] . "?ref=" . $get_ref->id);
    }

    public function ajaxTrackRef(Request $request)
    {
        $ref = $request->get('ref');
        $new_ref = new \App\Models\Referral\ReferralLinksClicks;
        $new_ref->links_id = $ref;
        $new_ref->users_id = Auth::guard('user')->user()->id;
        $new_ref->gender = '';
        $new_ref->nationality = '';
        $new_ref->save();
    }

    public function postURLPreview(Request $request)
    {


        $url = $request->get('url');
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $data = curl_exec($ch);
        curl_close($ch);

        // Load HTML to DOM Object
        $dom = new DOMDocument();
        @$dom->loadHTML($data);

        // Parse DOM to get Title
        $nodes = $dom->getElementsByTagName('title');
        $title = $nodes->item(0)->nodeValue;

        // Parse DOM to get Meta Description
        $metas = $dom->getElementsByTagName('meta');
        $body = "";
        for ($i = 0; $i < $metas->length; $i++) {
            $meta = $metas->item($i);
            if ($meta->getAttribute('name') == 'description') {
                $body = $meta->getAttribute('content');
            }
        }

        // Parse DOM to get Images
        $image_urls = array();
        $images = $dom->getElementsByTagName('img');

        for ($i = 0; $i < $images->length; $i++) {
            $image = $images->item($i);
            $src = $image->getAttribute('src');

            if (filter_var($src, FILTER_VALIDATE_URL)) {
                $image_src[] = $src;
            }
        }

        $output = array(
            'title' => $title,
            'image_src' => $image_src,
            'body' => $body
        );
        echo json_encode($output);

        //dd($request->all());
    }

    public function getSearchPOIsForTagging(Request $request)
    {

        $query = $request->get('query');
        //dd($query);

        $lat = floatval($request->get('lat'));
        $lng = floatval($request->get('lng'));

        if ($request->has('search_in'))
            $search_in = $request->get('search_in');
        if ($request->has('search_id')) {
            $search_id = $request->get('search_id');
            if ($search_id !== "false")
                $search_in = $search_in . "_id";
        }

        $final_results['meta']['query'] = $query;
        $final_results['data']['places'] = [];
        $final_results['data']['users'] = [];

        if ($query != '') {

            // FIND PLACES

            $guzzleClient = new Client();

            $guzzleQuery = [
                'key' => 'AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q',
                'query' => $query
            ];
            if ($lat != 0 && $lng != 0) {
                $guzzleQuery['location'] = $lat . ',' . $lng;
                $guzzleQuery['radius'] = 50000;
            }

            // https://httpbin.org/get - link for test

            // Google docs
            // https://developers.google.com/places/web-service/search
            $response = $guzzleClient->get('https://maps.googleapis.com/maps/api/place/textsearch/json', [
                'query' => $guzzleQuery
            ]);
            $body = $response->getBody();
            // $contents = $body->getContents();

            $query_result = json_decode($body);
            $qr = $query_result->results;

            // END GOOGLE REQUEST
            // START TO PROCESS RESULTS

            $place_providers = Arr::pluck($qr, 'place_id');

            $alreadyExistsPlaces = Place::whereIn('provider_id', $place_providers)->get();
            $alreadyExistsIds = $alreadyExistsPlaces->pluck('provider_id');

            $newPlacesIds = array_diff($place_providers, $alreadyExistsIds->toArray());

            $hasNewPlaces = boolval(count($newPlacesIds));

            if ($hasNewPlaces) {

                // PROCESS PLACES
                $newPlaces = [];
                $newPlaceTranslations = [];

                foreach ($qr as $r) {
                    if (in_array($r->place_id, $alreadyExistsIds->toArray()))
                        continue;

                    $model = [
                        'place_type' => @join(",", $r->types),
                        'safety_degrees_id' => 1,
                        'provider_id' => $r->place_id,
                        'countries_id' => 373,
                        'cities_id' => 2031,
                        'lat' => $r->geometry->location->lat,
                        'lng' => $r->geometry->location->lng,
                        'pluscode' => @$r->plus_code->compound_code,
                        'rating' => @$r->rating,
                        'active' => 1,
                        'auto_import' => 1,
                    ];

                    array_push($newPlaces, $model);


                    array_push($newPlaceTranslations, [
                        'provider_id' => $r->place_id,
                        'languages_id' => 1,
                        'title' => $r->name,
                        'address' => $r->formatted_address,
                        //                        'phone' => $r->phone,
                        //                        'description' => $r->website,
                        //                        'working_days' => $r->working_days,
                    ]);
                } // foreach ($qr AS $r)

                // insert new places
                Place::insert($newPlaces);


                // PROCESS TRANSLATIONS
                $newlyAddedPlaces = Place::whereIn('provider_id', $newPlacesIds)
                    ->pluck('id', 'provider_id');

                for ($i = 0; $i < count($newPlaceTranslations); $i++) {
                    $newPlaceTranslations[$i]['places_id'] = $newlyAddedPlaces[$newPlaceTranslations[$i]['provider_id']];
                    unset($newPlaceTranslations[$i]['provider_id']);
                }

                // insert new translations
                \App\Models\Place\PlaceTranslations::insert($newPlaceTranslations);
            } // END if( $hasNewPlaces )


            // at this point all new places stored in DB
            // BUILD RESPONSE

            $places = Place::whereIn('provider_id', $place_providers)->get();
            $places = $places->unique('provider_id'); // to prevent doubles
            $places_ids = $places->pluck('id');

            $transsingle = \App\Models\Place\PlaceTranslations::select('places_id', 'address', 'title')
                ->whereIn('places_id', $places_ids)
                ->get()
                ->keyBy('places_id');

            $placesMedias = PlaceMedias::whereIn('places_id', $places_ids)
                ->get()
                ->keyBy('places_id');

            $medias = [];
            foreach ($placesMedias as $place_id => $placesMedia) {
                $media = \App\Models\Place\Medias::select('url')
                    ->where('id', $placesMedia->medias_id)
                    ->first();

                if (is_null($media))
                    continue;

                $medias[$place_id] = $media;
            }

            $placesResponse = [];
            foreach ($places as $place) {
                $medium = [];
                $medium['id'] = @$place->id;
                $medium['place_type'] = @explode(",", $place->place_type)[0];
                $medium['address'] = @$transsingle[@$place->id]['address'];
                $medium['title'] = @$transsingle[@$place->id]['title'];
                $medium['lat'] = $place->lat;
                $medium['lng'] = $place->lng;
                $medium['rating'] = @$place->rating;
                $medium['location'] = $place->lat . "," . $place->lng;
                if (isset($medias[@$place->id]))
                    $medium['image'] = @$medias[@$place->id]->url;

                array_push($placesResponse, $medium);
            }

            $placesResponse = array_slice($placesResponse, 0, 8);
            $final_results['data']['places'] = $placesResponse;


            // FIND USERS
            $users = User::where('name', 'like', "%" . $query . "%")
                ->take(8)
                ->get();

            $final_results['data']['users'] = $users;
        }

        return response()->json($final_results);
    }

    public function tools(Request $request)
    {


        $hotels = \App\Models\Embassies\Embassies::take(2000)->get();
        foreach ($hotels as $h) {
            if (Place::where('provider_id', $h->provider_id)->count() == 0) {
                //echo '- Add new place with id: ' . $h->provider_id . "<br />";
                $p = new Place;
                $p->countries_id = $h->countries_id;
                $p->cities_id = $h->cities_id;
                $p->place_type = $h->place_type;
                $p->place_section = 'embassies';
                $p->provider_id = $h->provider_id;
                $p->lat = $h->lat;
                $p->lng = $h->lng;
                $p->pluscode = $h->pluscode;
                $p->rating = $h->rating;
                $p->active = $h->active;
                $p->media_done = $h->media_done;
                $p->cover_media_id = $h->cover_media_id;
                $p->auto_import = $h->auto_import;
                $p->visits = 0;
                if ($p->save()) {
                    $pt = new PlaceTranslations;
                    $pt->places_id = $p->id;
                    $pt->languages_id = 1;
                    $pt->title = $h->transsingle->title;
                    $pt->description = $h->transsingle->description;
                    $pt->address = $h->transsingle->address;
                    $pt->phone = $h->transsingle->phone;
                    $pt->highlights = '';
                    $pt->working_days = $h->transsingle->working_days;
                    $pt->working_times = $h->transsingle->working_times;
                    $pt->how_to_go = $h->transsingle->how_to_go;
                    $pt->when_to_go = $h->transsingle->when_to_go;
                    $pt->num_activities = '';
                    $pt->popularity = $h->transsingle->popularity;
                    $pt->conditions = $h->transsingle->conditions;
                    $pt->price_level = $h->transsingle->price_level;
                    $pt->num_checkins = $h->transsingle->num_checkins;
                    $pt->history = $h->transsingle->history;
                    $pt->save();

                    foreach ($h->medias as $hm) {
                        //dd($hm);
                        $pm = new PlaceMedias;
                        $pm->places_id = $p->id;
                        $pm->medias_id = $hm->id;
                        $pm->save();
                    }

                    $h->delete();
                }
            } else {
                //echo '- Place existed before: ' . $h->provider_id . "<br />";
                $h->delete();
            }
        }
        /*
          $some_countries = Countries::whereIn('id', array(266, 14, 21, 290, 318, 27, 267, 39, 322, 80, 330, 104, 127, 136, 358, 147, 271, 324, 360, 161, 168, 366, 263, 212, 276, 227))
          ->get();
          foreach ($some_countries AS $sc) {
          //dd($sc->countryHolidays);
          foreach ($sc->countryHolidays AS $sch) {
          foreach ($sc->cities AS $city) {
          $new_holiday = new \App\Models\City\CitiesHolidays;
          $new_holiday->cities_id = $city->id;
          $new_holiday->holidays_id = $sch->holidays_id;
          $new_holiday->date = $sch->date;
          $new_holiday->save();
          }
          }
          }
         *
         */
    }
    public function getExvelCountries()
    {
        $row = 1;
        $cities = [];
        $countries = [];
        if (($handle = fopen(public_path("citiescsv/allcities.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                if ($row > 2) {
                    $countries[] = utf8_decode(utf8_encode($data[0]));
                }
            }
        }

        $ind_countries = [];
        $existing = [];
        $new = [];
        foreach ($countries as $country) {
            $country_id = Countries::whereHas('transsingle', function ($query) use ($country) {
                $query->where('title', 'like', '%' . $country . '%');
            })->get()->first();
            if (isset($country_id->id)) {
                $country_trans = CountriesTranslations::where('countries_id', $country_id->id)->get()->first();
                $existing[$country_id->id] = $country_trans->title;
            } else {
                $new[$country] = $country;
            }
        }
        //for new and existing

        // foreach( $new as $key=>$country){
        //     $txt = $country. "\n";
        //     fwrite($myfile1,$txt);

        // }   


        //for to be deleted records 
        //$myfile1 = fopen("to_be_deleted_countries.txt", "w") or die("Unable to open file!");

        $cities = Countries::select('id')->whereNotIn('id', $existing)->get();
        foreach ($cities as $city) {
            $city_trans = CountriesTranslations::where('countries_id', $city->id)->get()->first();
            $tb_deleted[] = $city->id;
            if (isset($city_trans['title'])) {
                $txt = $city_trans['title'] . "\n";
            } else {
                $txt = 'city title not find country_id:' . $city_trans['countries_id'] . "\n";
            }
            //fwrite($myfile1, $txt);
        }


        //fclose($myfile1);   
        $final_result = ['new' => $new, 'existing' => $existing, 'to_be_deleted' => $tb_deleted];

        dd($final_result);
    }
    public function updatePlacesCities(Request $request)
    {


        /* reading excel file containing Cities information */
        $row = 1;
        $cities = [];
        $countries = [];
        if (($handle = fopen(public_path("citiescsv/allcities1.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                if ($row > 2) {
                    $countries[] = utf8_decode(utf8_encode($data[0]));
                    $cities[utf8_decode(utf8_encode($data[0]))][] = $data[1];
                }
            }
            fclose($handle);
        }
        $new_country = [];
        $ind_countries = [];
        foreach ($countries as $country) {
            $country_id = Countries::whereHas('transsingle', function ($query) use ($country) {
                $query->where('title', $country);
            })->get()->first();
            if (isset($country_id->id)) {
                if (array_key_exists($country, $cities))
                    $ind_countries[$country_id->id] = $cities[$country];
            } else {
                $new_country[] = $country;
            }
        }
        //dd($ind_countries, $new_country);
        /* identify existing and new and to be deleted cities */
        $existing = [];
        $existing_list = [];
        $new = [];
        $tb_deleted = [];
        $repeat = [];
        $city_counter = 0;
        foreach ($ind_countries as $key => $country) {
            $city_counter += count($country);
            foreach ($country as $city) {

                $temp_city = Cities::whereHas('transsingle', function ($query) use ($city) {
                    $query->where('title', $city);
                })->where('countries_id', $key)->get()->first();
                if (isset($temp_city->id)) {
                    $existing[] = $temp_city->id;
                    if (array_key_exists($temp_city->id, $existing_list))
                        $repeat[] = $city;
                    $existing_list[$temp_city->id] = $city;
                } else {
                    $new[]  = $city;
                }
            }
        }
        // dd($existing_list,$existing,$new,$city_counter,$repeat);
        /* to be deleted from DB */
        $cities = Cities::select('id')->whereNotIn('id', $existing)->get();
        foreach ($cities as $city) {
            $city_trans = CitiesTranslations::where('cities_id', $city->id)->get()->first();
            if (isset($city_trans['title'])) {

                $tb_deleted[$city->id] = $city_trans['title'];
                $txt = $city_trans['title'] . "\n";
            } else {
                $tb_deleted[$city->id] = 'city title not find cityid:' . $city_trans['cities_id'];
                $txt = 'city title not find cityid:' . $city_trans['cities_id'] . "\n";
            }
        }

        /* actaul deleted record from DB */
        $dnt_delete = [];
        if (($handle = fopen(public_path("citiescsv/tbd_cities.txt"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, "\n")) !== FALSE) {
                $num = count($data);
                $row++;
                if ($row > 2) {
                    $dnt_delete[$data[0]] = $data[0];
                }
            }
            fclose($handle);
        }
        $act_delete_list = [];
        $act_delete = [];
        foreach ($tb_deleted as $key => $sh_delet) {
            if (!array_key_exists($sh_delet, $dnt_delete)) {
                $act_delete[] = $key;
                $act_delete_list[$key] = $sh_delet;
            }
        }
        /* identify places to move */
        $act_place = [];
        $places_to_be_changed = [];
        $places = Place::whereIn('cities_id', $act_delete)->get();
        foreach ($places  as $place) {
            $act_place[] = $place->cities_id;
            $places_to_be_changed[] = $place;
        }
        if ($request->update_place == 1) {
            $updated =  Place::whereIn('cities_id', $act_delete)->update(['cities_id' => '2031', 'countries_id' => '373']);
            if ($updated) {
                echo $updated . ' places updated \n';
            }
            /* delete city code */
            $deleted_cites = 0;
            if ($request->delete_city == 1) {
                foreach ($act_delete as $to_del) {
                    if ($to_del != '2043' && $to_del != '127' && $to_del != '2051' && $to_del != '2080' && $to_del != '2119' && $to_del != '1420' && $to_del != '1691') {

                        CitiesTranslations::where('cities_id', $to_del)->delete();
                        CountriesCapitals::where('cities_id', $to_del)->delete();
                        CitiesMedias::where('cities_id', $to_del)->delete();
                        TripPlaces::where('cities_id', $to_del)->delete();
                        CitiesShares::where('city_id', $to_del)->delete();
                        $delete = Cities::whereId($to_del)->delete();
                        if ($delete) {
                            $deleted_cites++;
                        }
                    }
                }
            }
            $final_result = ['new' => $new, 'existing' => $existing_list, 'to_be_deleted' => $tb_deleted, 'final_delete' => $act_delete_list, 'actual_places_to_be changed' => $places_to_be_changed, 'repeative_cities' => $repeat, 'deleted' => $deleted_cites];
            dd($final_result);
        } else {
            $final_result = ['new' => $new, 'existing' => $existing_list, 'to_be_deleted' => $tb_deleted, 'final_delete' => $act_delete_list, 'actual_places_to_be changed' => $places_to_be_changed, 'repeative_cities' => $repeat];
            dd($final_result);
        }
        $final_result = ['new' => $new, 'existing' => $existing_list, 'to_be_deleted' => $tb_deleted, 'final_delete' => $act_delete_list, 'actual_places_to_be changed' => $places_to_be_changed, 'repeative_cities' => $repeat];

        dd($final_result);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function copyright()
    {
        return view('site.home.partials.copyright');
    }

    public function getPostView($postId, Request $request)
    {
        $post = Posts::find($postId);
        $place = [];
        if (isset($post->posts_place[0]['places_id'])) {
            $place = Place::find($post->posts_place[0]['places_id']);
        }

        $more = View::make('site.home.partials.post-text-popup', array('post' => $post, 'place' => $place, 'checkins' => @$post->checkin[0]));
        return view('site.home.post-view')->with(array('post' => $post, 'place' => $place, 'checkins' => @$post->checkin[0]));
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function userBlocked(Request $request)
    {
        return view('site.home.partials.user-blocked')->withMessage($request->message);
    }


    public function bulkUpdateInstance(Request $request)
    {
        $offset = $request->page;
        ignore_user_abort();
        set_time_limit(0);
        $limit  = 10000;
        if ($offset != 0) {
            $offset = $offset * $limit;
        }
        $language_id = 1;
        $counter = 0;
        $str = '';
        $arr_count = 0;
        $places = [];
        $places = Place::with([
            'trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'getMedias',
        ])
            ->limit($limit)
            ->offset($offset)
            ->get();
        $offset += $limit;
        foreach ($places as $place) {
            $arr_count++;
            if (isset($place->trans[0]->title)) {
                $str .= json_encode(['index' => ['_index' => 'places', '_id' => $place->id],], JSON_PRESERVE_ZERO_FRACTION) . "\r\n";
                $str .= json_encode([
                    'place_id' => $place->id,
                    'place_title' => @$place->trans[0]->title,
                    'place_type' => @$place->place_type,
                    'place_media' => @$place->getMedias[0]->url,
                    'place_description' => @$place->trans[0]->description,
                    'lat' => @$place->lat,
                    'lng' => @$place->lng,
                    'city' => @$place->city->trans[0]->title,
                    'country' => @$place->country->trans[0]->title,
                    'cities_id' => @$place->cities_id,
                    'countries_id' => @$place->countries_id,
                    'address' => @$place->trans[0]->address
                ], JSON_PRESERVE_ZERO_FRACTION) . "\r\n";
                $counter++;
            }
            if ($counter == 100) {
                if ($str != "" && $str != "{}" && $str != "[]") {
                    $this->pushBulkData($str);
                    $str = "";
                }
                $counter = 0;
            }
            if ($arr_count == count($places)) {
                if ($str != "" && $str != "{}" && $str != "[]") {
                    $this->pushBulkData($str);
                    $str = "";
                }
                $counter = 0;
            }
        }
    }
    function pushBulkData($str)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://search-travooo-dev-ehpbhzcgywfnqic75w6gdxrwmq.eu-west-3.es.amazonaws.com/_bulk",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $str,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
        }
    }
    public function getTopPlaces(Request $request)
    {
        $page = $request->page;
        // reading file 
        dd(Place::count());
        $places_array  = [];
        $row = 0;
        if (($handle = fopen(public_path("citiescsv/newplace.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                if ($row >= 2) {
                    $places_array[] = [
                        'old_id' => $data[0],
                        'place_name' => $data[4],
                        'city_id' => $data[5]
                    ];
                }
            }
            fclose($handle);
        }
        $places_return = [];
        $exists = Storage::disk('local')->exists('tp_place3.txt');
        if ($exists) {
            $str = Storage::disk('local')->get('tp_place3.txt');
        } else {
            $str  = "old_id| Place Name|Travooo Id|Place Type|Address|Not Found|Multiple" . "\n";
        }

        $places_array = array_chunk($places_array, 1000);
        foreach ($places_array[$page] as $key => $place) {
            $needle = $place['place_name'];
            $place_X = DB::table('places')
                ->leftjoin('places_trans', 'places.id', '=', 'places_trans.places_id')
                ->where('cities_id', $place['city_id'])
                ->where('title', 'like', '%' . $needle . '%')
                ->select('places_trans.title', 'places_trans.places_id', 'places_trans.address', 'places.place_type')
                ->get();
            if (isset($place_X[0]->title) && count($place_X) == 1) {
                $str .= $place['old_id'] . "|" . $place_X[0]->title . "|" . $place_X[0]->places_id . "|" . $place_X[0]->place_type . "|" . $place_X[0]->address . "| no | no" . " \n";
            } else if (isset($place_X[0]->title) && count($place_X) > 1) {
                $str .= $place['old_id'] . "|" . $place_X[0]->title . "|" . $place_X[0]->places_id . "|" . $place_X[0]->place_type . " |" . $place_X[0]->address . " | no |yes" . " \n";
            } else {
                $str .= $place['old_id'] . "| " . $place['place_name'] . "| ----|---| ------|yes|no" . "\n ";
            }
        }
        $headers = array(
            'Content-Type' => 'text/csv',
        );
        Storage::disk('local')->put('tp_place3.txt', $str);
        if (count($places_array) - 1 == $page) {

            dd('done');
        } else {
            echo 'done ' . $page;
        }
    }
    function extractFile()
    {
        $exists = Storage::disk('local')->exists('tp_place3.txt');
        $str = '';
        if ($exists) {
            $str = Storage::disk('local')->get('tp_place3.txt');
            $str = preg_split("/\r?\n|\r/", $str);
        } else {
            echo 'file not exist';
            die;
        }
        $output = fopen('tp_place3.csv', 'w');


        foreach ($str as $file) {
            $data  = explode('|', $file);
            fputcsv($output, $data);
        }
        fclose($output);
        $headers = array(
            'Content-Type: application/pdf',
        );

        return Response::download('tp_place3.csv', 'top place.csv', $headers);
    }


    function getCountries()
    {
        $row = 0;

        // $results = [];
        // if (($handle = fopen(public_path("citiescsv/countries.csv"),"r")) !== FALSE) {
        //     while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        //         $num = count($data);
        //         $row++;
        //         if($row>=2){
        //            $results[] = $data;
        //         }
        //     }
        //     fclose($handle);
        // }
        // $array = [];
        // foreach($results as $result){
        //     $country= Countries::with('transsingle')
        //             ->whereHas('transsingle', function($country) use ($result) {
        //                 $country->where('title', 'LIKE', "%$result[0]%");
        //             })
        //             ->first();
        //     if(!is_object($country)){
        //         $array[] = $result;
        //     }
        // }
        // foreach($array as $val){
        //         $country  = new Countries();
        //         $country->iso_code =$val[1];
        //         $country->regions_id = 22;
        //         $country->safety_degree_id = 1;
        //         $country->active =1;
        //         if($country->save()){
        //             $country_trans=  new CountriesTranslations();
        //             $country_trans->countries_id =$country->id;
        //             $country_trans->title = $val[0];
        //             $country_trans->languages_id = 1;
        //             $country_trans->save();
        //         }
        // }
        $countries =  Countries::with('transsingle')->get();

        $countries_not_equal = [];
        foreach ($countries as $result) {
            $google_link = 'https://maps.googleapis.com/maps/api/place/textsearch/json?key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
            $google_link .= '&query=' . urlencode($result->transsingle->title);

            $fetch_link = file_get_contents($google_link);
            $query_result = json_decode($fetch_link);
            $qr = $query_result->results;

            if (isset($qr[0])) {

                if (strcasecmp($result->transsingle->title, $qr[0]->name) != 0) {
                    $countries_not_equal[$result->id]  = $result->transsingle->title;
                }
                $trans_C = CountriesTranslations::where('title', 'like', '%' . $result->transsingle->title . '%')->first();
                $country_to_update = Countries::find($trans_C->countries_id);
                $country_to_update->lat = @$qr[0]->geometry->location->lat;
                $country_to_update->lng = @$qr[0]->geometry->location->lng;
                $country_to_update->save();
            }
        }
        dd($countries_not_equal);
    }
    function getUsaCiies(Request $request)
    {

        $query = Place::where(function ($query) {
            foreach ($this->placeTypesArray as $key => $placeType) {
                $query->whereRaw('SUBSTRING_INDEX( place_type, ",", 1 ) NOT like "%' . $placeType . '%"');
            }
        });
        dd($query->count());



        if (!isset($request->page)) {
            return 'Please enter Page number from 0';
        }
        $language_id = 1;
        $cities = Cities::with([
            'trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'countryTitle'
        ])

            ->where('countries_id', 227)
            ->limit(50)
            ->offset($request->page * 50)
            ->get();
        dd($cities);
        $resultant_array = [];
        $differnt_level = [];
        foreach ($cities as $city) {
            $query = $city->trans[0]->title . ' in ' . $city->countryTitle->title;
            $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?' . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($query);
            $fetch_link_for_city = file_get_contents($google_link_for_city);
            $query_result_for_city = json_decode($fetch_link_for_city);
            $qr_city = $query_result_for_city->results;
            if (isset($qr_city[0])) {
                if ($qr_city[0]->types[0] == 'locality' || $qr_city[0]->types[0] == 'administrative_area_level_3') {
                    $update = CitiesTranslations::where('cities_id', $city->id)->update(['title' => $qr_city[0]->name]);
                    $resultant_array[$city->id] = ['provider_id' => $qr_city[0]->place_id, 'id' => $city->id, 'country' => $city->countryTitle->countries_id];
                } else
                    $differnt_level[] = [$city->id => $qr_city[0]->types[0]];
            }
        }
        foreach ($resultant_array as $arry) {
            $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&place_id=' . $arry['provider_id'];
            $fetch_geo_link = file_get_contents($google_link_for_city);
            $geo_result = json_decode($fetch_geo_link);
            $geo_city = @$geo_result->result->address_components;
            $city_found_flag = false;
            $geo_states = [];
            if (isset($geo_city)) {
                foreach ($geo_city as $gc) {
                    if (isset($gc->types) && !empty($gc->types)) {
                        if ($gc->types[0] == 'administrative_area_level_1') {
                            $geo_state = $gc->long_name;
                        }
                    }
                }
            }
            if (isset($geo_state) && !empty($geo_state)) {
                $state = States::whereHas('transsingle', function ($query) use ($geo_state) {
                    $query->where('title', 'like', '%' . $geo_state . '%');
                })->get()->first();
            }
            $states_id = 0;
            if (isset($state) && is_object($state)) {
                Cities::whereId($arry['id'])->update(['states_id' => $state->id]);
            } else {
                $state  = new States();
                $state->lat = $geo_result->result->geometry->location->lat;
                $state->lng = $geo_result->result->geometry->location->lng;
                $state->countries_id  = $arry['country'];
                $state->active = 1;
                if ($arry['country'] != 227) {
                    $state->is_province = 1;
                }
                if ($state->save()) {
                    $states_id = $state->id;
                    $state_trans =  new StateTranslation();
                    $state_trans->states_id = $state->id;
                    $state_trans->title = $geo_state;
                    $state_trans->languages_id = 1;
                    $state_trans->save();
                }
            }
        }
        dd($differnt_level, $resultant_array);
    }
    function getStates(Request $request)
    {
        $row  = 1;
        $states = [];
        if (($handle = fopen(public_path("citiescsv/states.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                if ($row > 2) {
                    $states[]  = $data[0];
                }
            }
            fclose($handle);
        }
        dd($states[101]);
        foreach ($states[101][4009] as $states) {
        }
        $language_id = 1;

        $states = Cities::with([
            'trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'countryTitle'
        ])

            ->whereIn('id', $states)
            ->get();
        $final_list = [];
        $differnt_level = [];
        $already_added = [];
        $not_found = [];
        foreach ($states as $state) {
            $query = $state->trans[0]->title . ' in ' . $state->countryTitle->title;

            $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?' . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($query);
            $fetch_link_for_city = file_get_contents($google_link_for_city);
            $query_result_for_city = json_decode($fetch_link_for_city);
            $qr_city = $query_result_for_city->results;
            if (isset($qr_city[0])) {

                if ($qr_city[0]->types[0] == 'administrative_area_level_1') {
                    $state_temp = States::whereHas('transsingle', function ($query) use ($qr_city) {
                        $query->where('title', 'like', '%' . $qr_city[0]->name . '%');
                    })->get()->first();
                    if (!isset($state_temp) || !is_object($state_temp)) {
                        $state_temp  = new States();
                        $state_temp->lat = $qr_city[0]->geometry->location->lat;
                        $state_temp->lng = $qr_city[0]->geometry->location->lng;
                        $state_temp->countries_id  = $state->countries_id;
                        $state_temp->active = 1;
                        if ($state->countries_id != 227) {
                            $state_temp->is_province = 1;
                        }
                        if ($state_temp->save()) {
                            $states_id = $state_temp->id;
                            $state_trans =  new StateTranslation();
                            $state_trans->states_id = $state_temp->id;
                            $state_trans->title = $qr_city[0]->name;
                            $state_trans->languages_id = 1;
                            $state_trans->save();
                        }
                        $final_list[] = ['Travooo ID' => $state->id, 'State ID' => $state_temp->id, 'Google Name' => $qr_city[0]->name, 'Google Level' => $qr_city[0]->types[0], 'Travooo Name' => $state->trans[0]->title];
                    } else {
                        $already_added[$state_temp->id] = $qr_city[0]->name;
                    }
                } else {
                    $differnt_level[] = ['Travoo Id' => $state->id, 'Google Level' => $qr_city[0]->types[0], 'Google Name' => $qr_city[0]->name, 'Travooo Name' => $state->trans[0]->title];
                }
            } else {
                $not_found[] = $state->trans[0]->title;
            }
        }
        dd($final_list, $differnt_level, $not_found, $already_added);
    }
    function getCities(Request $request)
    {
        $page  = $request->page;
        if (!isset($page)) {
            return 'please enter page';
        }
        $row = 1;
        $cities = [];
        if (($handle = fopen(public_path("citiescsv/Missing Cities.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $row++;
                if ($row > 2) {
                    if (!empty($data[0]))
                        $cities[] = $data[0]  . '_' . $data[1];
                }
            }
            fclose($handle);
        }
        $resultant_array = [];
        $not_found = [];
        $cities = array_chunk($cities, 50);

        foreach ($cities[$page] as $city) {
            if (isset($city)) {
                try {
                    $temp_query = explode('_', $city);
                    $query = $temp_query[0] . ' in ' . $temp_query[1];
                    $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?' . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($query);
                    $fetch_link_for_city = file_get_contents($google_link_for_city);
                    $query_result_for_city = json_decode($fetch_link_for_city);
                    $qr_city = $query_result_for_city->results;
                    if (isset($qr_city[0])) {
                        if ($qr_city[0]->types[0] == 'locality' || $qr_city[0]->types[0] == 'administrative_area_level_3') {

                            //find country 
                            $country_id = Countries::whereHas('transsingle', function ($query) use ($temp_query) {
                                $query->where('title', 'like', '%' . $temp_query[1] . '%');
                            })->pluck('id')->first();
                            if (isset($country_id)) {

                                // create city
                                $city_temp = Cities::create(['countries_id' => $country_id, 'active' => 1, 'lat' => $qr_city[0]->geometry->location->lat, 'lng' => $qr_city[0]->geometry->location->lng, 'safety_degree_id' => 19, 'level_of_living_id' => 0, 'is_capital' => 0]);

                                //create its translations 

                                CitiesTranslations::create([
                                    'cities_id' => $city_temp->id,
                                    'title' => $qr_city[0]->name,
                                    'languages_id' => 1,
                                ]);
                            } else {
                                $not_found[] = [$temp_query[0],  $temp_query[1], 'city not found in our db'];
                            }
                            //create new city if not exist 

                        } else {
                            $not_found[] = [$temp_query[0],  $temp_query[1], 'google type is different ' . $qr_city[0]->types[0]];
                        }
                    } else {
                        $not_found[] = [$temp_query[0],  $temp_query[1], 'not found on google'];
                    }
                } catch (\Exception $ex) {
                    $not_found[] = [$temp_query[0],  $temp_query[1], 'Special characters may b'];
                }
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="newly_added_cities_' . $request->page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($not_found as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }

    function getStatesData(Request $request)
    {
        $page = $request->page;
        $countries_id = 107;
        $json = file_get_contents(public_path("citiescsv/states.json"));
        // Converts it into a PHP object
        $data = json_decode($json, true);
        $counter = 0;
        //$data = array_chunk( $data [108]['states'], 10);
        $val = 0;
        // $places = place::where('countries_id',107)->
        // where('cities_id',2456)->get();
        dd($data);
        $cities = [];
        $staess = [0 => 'San José Province', 1 => 'Melaka'];
        // || $city_X['name'] =='Arizona' || $city_X['name'] =='California' ||  $city_X['name'] =='Kentucky' || $city_X['name'] =='' || $city_X == 'Michigan's
        // foreach($data [101]['states'] as $city_X){
        //     foreach($city_X['cities'] as $citx){
        //         $cities[] = $citx['name'].'_'.$city_X['name'];
        //     }
        // }
        // dd( $cities);

        // dd($data [115]['states']);
        foreach ($data[52]['states'] as $city_X) {

            if ($city_X['name'] == $staess[$page]) {
                // dd($city_X );
                $val++;
                $state = '';
                $state = States::whereHas('transsingle', function ($query) use ($city_X) {
                    $query->where('title', 'like',  '%' . $city_X['name'] . '%');
                })->get()->first();

                if (!isset($state) || !is_object($state)) {
                    $state_l  = new States();
                    $state_l->lat = 0.0;
                    $state_l->lng = 0.0;
                    $state_l->countries_id  = 51;
                    $state_l->active = 1;
                    if ($state_l->save()) {
                        $state = $state_l;
                        $state_trans =  new StateTranslation();
                        $state_trans->states_id = $state_l->id;
                        $state_trans->title = $city_X['name'];
                        $state_trans->languages_id = 1;
                        $state_trans->save();
                    }
                }
                foreach ($city_X['cities'] as $citx) {
                    //    dd($citx['name']);
                    // if($citx['name'] == 'Malacca'){
                    //     $temp = str_replace('Malacca','Malacca City',$citx['name']);
                    //     $citx['name'] = $temp;
                    // }
                    $get_city = Cities::whereHas('transsingle', function ($query) use ($citx) {
                        $query->where('title', 'like', '%' . $citx['name'] . '%');
                    })->where('countries_id', 51)->first();
                    if (isset($get_city) || is_object($get_city)) {
                    } else {
                        $city  = new Cities();
                        $city->countries_id  = 51;
                        $city->states_id  = $state->id;
                        $city->safety_degree_id  = 1;
                        $city->level_of_living_id  = 0;
                        $city->active  = 1;
                        $counter++;
                        if ($city->save()) {
                            $city_trans =  new CitiesTranslations();
                            $city_trans->cities_id = $city->id;
                            $city_trans->title = $citx['name'];
                            $city_trans->languages_id = 1;
                            $city_trans->save();
                        }
                    }
                }
            }
        }
        dd($val, $counter);
        // $counter = 0;
        // $val= 0;
        // dd($data [233]['states']);
        // foreach($data [108]['states'] as $city_X){


        //     $counter++;
        //         $state = '';
        //         $state = States::whereHas('transsingle',function($query) use ($city_X){
        //             $query->where('title','like', '%'.$city_X['name'].'%');
        //         })->get()->first();

        //         if(!isset($state) || !is_object($state)){

        //                     $state_l  = new States();
        //                     $state_l->lat =0.00000;
        //                     $state_l->lng =0.00000;
        //                     $state_l->countries_id  = $countries_id;
        //                     $state_l->active = 1;
        //                     if($countries_id != 227){
        //                         $state_l->is_province =1;
        //                     }
        //                     if($state_l->save()){
        //                         $state = $state_l;
        //                         $state_trans=  new StateTranslation();
        //                         $state_trans->states_id =$state_l->id;
        //                         $state_trans->title =$city_X['name'] ;
        //                         $state_trans->languages_id = 1;
        //                         $state_trans->save();
        //                     }
        //         }
        //         foreach($city_X['cities'] as $citx){
        //             $val++;
        //             $queryx = $citx['name'];
        //             $get_city = Cities::whereHas('transsingle', function ($query) use ($queryx) {
        //                 $query->where('title','like', '%'.$queryx.'%');
        //             })->get()->first();
        //             if(isset($get_city) && is_object($get_city)){
        //             }else{
        //                 $city  = new Cities();
        //                 $city->lat = 0.00000;
        //                 $city->lng = 0.00000;
        //                 $city->countries_id  = $countries_id;
        //                 $city->states_id  = $state->id;
        //                 $city->safety_degree_id  = 1;
        //                 $city->level_of_living_id  = 0;
        //                 $city->active  = 1;
        //                 $counter++;
        //                 if($city->save()){
        //                     $city_id = $city->id;
        //                         $city_trans=  new CitiesTranslations();
        //                         $city_trans->cities_id =$city->id;
        //                         $city_trans->title = $queryx;
        //                         $city_trans->languages_id = 1;
        //                         $city_trans->save();
        //                 }
        //             }   

        //         }

        // }
        dd($counter, $val);
    }

    function updatePlacesInformationByStates(Request $request)
    {

        $row = 1;
        $places = [];
        if (($handle = fopen(public_path("citiescsv/final_list_places.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $row++;
                if ($row > 2) {
                    if (!empty($data[0]))
                        $places[] = $data;
                }
            }
            fclose($handle);
        }
        // dd($places);
        $not_found = [
            'City ID',
            'Address',
            'Travoo Country'
        ];
        $page = $request->page;
        $limit = $request->limit;
        if (!isset($page)) {
            return 'page is required';
        }
        $language_id = 1;
        $cities = Cities::with([
            'trans'
        ])
            ->get();
        // dd( $cities );
        // $places = Place::with([
        //     'trans' => function ($query) {
        //         $query->whereNotNull('address');
        //     },
        // ])->where('countries_id',107)
        // ->limit(2000)
        // ->offset($page*2000)->get();
        // dd($cities);
        $counter  = 0;
        $found = false;
        $valid = false;
        $not_found = [];
        $places = array_chunk($places, 1000);

        foreach ($places[$page] as $place) {
            if (!is_null($place[1]) && isset($place[1])) {
                foreach ($cities as $city) {
                    if ((isset($city->trans[0]->title) && isset($place[1])) && strpos(strtolower($place[1]), strtolower($city->trans[0]->title)) !== false  && ($place[2] == $city->countries_id)) {
                        Place::whereId($place[0])->update(['cities_id' => $city->id]);
                        $found = true;
                    }
                }
                if (!$found) {
                    $not_found[] = [$place[0], isset($place[1]) ? $place[1] : '', $place[2]];
                }
                $found = false;
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="place_final_' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($not_found as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    function getTranslate(Request $request)
    {
        $dummy_places = [];
        if (($handle = fopen(public_path("citiescsv/dummy.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $dummy_places[] = $data[0];
            }
            fclose($handle);
        }
        $translate = new TranslateClient([
            'key' => 'AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
        ]);
        $places_ids = array_chunk($dummy_places, 1000);

        $page = $request->page;
        if (!isset($page)) {
            return 'enter page no.';
        }
        $counter = 0;
        $places = PlaceTranslations::whereIn('places_id', $places_ids[$page])->get();

        foreach ($places as $place) {
            if (isset($place->address)) {
                $result = $translate->translate($place->address, [
                    'target' => 'en'
                ]);
                if (strcmp($place->address, $result['text']) != 0) {
                    PlaceTranslations::where('places_id', $place->id)->update(['address' => $result['text']]);
                    $counter++;
                }
            }
        }

        echo $counter . ' items updated' . "\n";
    }
    function getTopCities(Request $request)
    {
        $row = 1;
        if (($handle = fopen(public_path("citiescsv/topplaces.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                if ($row > 2) {
                    if (isset($places[$data[14]])) {

                        $review = isset($data[8]) ? $data[8] : 0;
                        $places[$data[14]] += (int) $review;
                    } else {
                        $places[$data[14]]  = (int) $data[8];
                    }
                }
            }
            fclose($handle);
        }
        foreach ($places as $key => $new) {
            $tc = new TopCities();
            $tc->cities_id = $key;
            $tc->no_of_reviews = $new;
            $tc->save();
        }
    }
    function getTranslateGlobal(Request $request)
    {
        $row = 1;
        $countries = [];
        if (($handle = fopen(public_path("citiescsv/data_country.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $row++;
                if ($row > 2) {
                    if (!empty($data[0]))
                        $countries[] = $data[0];
                }
            }
            fclose($handle);
        }

        $translate = new TranslateClient([
            'key' => 'AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
        ]);

        $page = $request->page;
        if (!isset($page)) {
            return 'enter page no.';
        }
        $counter = 0;
        // $places =  PlaceTranslations::with([
        //     'place' => function ($query) use($countries){
        //         $query->whereIn('countries_id', $countries);
        //     },
        // ])->whereNotNull('address')->limit(2000)->offset($page*2000)->get();
        // dd($places);
        $query_string = '';
        foreach ($this->placeTypesArray as $key => $placeType) {
            if ($key == 0)
                $query_string .= "SUBSTRING_INDEX( place_type, ',', 1 )  like '%" . $placeType . "%'";
            else
                $query_string .= "or SUBSTRING_INDEX( place_type, ',', 1 )  like '%" . $placeType . "%'";
        }

        $places = DB::select("select places.id, address,place_type from places_trans left join places on places.id = places_trans.places_id where countries_id in (" . implode(',', $countries) . ") 
    
        limit 1000
        offset " . ($page * 1000));

        foreach ($places as $place) {
            $flag = false;
            foreach ($this->placeTypesArray as $key => $placeType) {
                $dd = explode(',', $place->place_type);

                if (strcmp($dd[0], $placeType) == 0) {
                    $flag = true;
                }
            }
            if ($flag) {
                $result = $translate->translate($place->address, [
                    'target' => 'en'
                ]);
                if (strcmp($place->address, $result['text']) != 0) {
                    PlaceTranslations::where('places_id', $place->id)->update(['address' => $result['text']]);
                    $counter++;
                }
            }
        }

        echo $counter . ' items updated' . "\n";
    }
    function processCsv(Request $request)
    {
        $page = $request->page;
        $places = [];
        if (($handle = fopen(public_path("citiescsv/final_cities_csv.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $places[$data[0]]  = $data[1];
            }
        }
        fclose($handle);
        dd($places);
        $places = [
            377 => "Hong Kong",
            13977 => "Virgil",
        ];
        $places = [
            377 => "Hong Kong",
            920 => "Macau",
        ];
        foreach ($places  as  $key => $place) {

            CitiesTranslations::where('cities_id', $key)->update(['title' => $place]);
            //$citu = Cities::find($key);
            //$updatd = Place::where('cities_id',$key)->update(['countries_id'=>$citu->countries_id]);
            // dd($updatd);
        }
        dd('done');
        $moved = [
            14182 => 804,
            14216 => 1601,
            15786 => 1112,
            15822 => 2204,
            15826 => 2204,
            15830 => 2204,
            16215 => 54,
            16216 => 54,
            2388 => 405,
        ];
        $place = 0;
        $delete_c = 0;
        $delete_xt = 0;
        foreach ($moved  as  $key => $move) {
            //    dd($key,$move);
            $place  += Place::where('cities_id', $key)->update(['cities_id' => $move]);
            $delete_c += CitiesTranslations::where('cities_id', $key)->delete();
            $delete_xt += Cities::whereId($key)->delete();
        }
        $tb_deleted = [
            12848 => "Patla",
            13782 => "Azilda",
            14707 => "Łagiewniki",
            14827 => "Twardogóra",
            14852 => "Złotniki",
            15163 => "Bodzanów",
            15317 => "Otrębusy",
            15461 => "Wieliszew",
            15630 => "Chwaszczyno",
            15792 => "Poli",
            15801 => "Sector 4",
            15836 => "Linton Park",
            16193 => "Kampung Bukit Baharu",
            16208 => "Goicoechea",
        ];

        foreach ($tb_deleted  as  $key => $delete) {
            $place  += Place::where('cities_id', $key)->delete();
            $delete_c += CitiesTranslations::where('cities_id', $key)->delete();
            $delete_xt += Cities::whereId($key)->delete();
        }
        dd($place, $delete_c, $delete_xt);
        dd('stop');
        $language_id = 1;
        $cities = Cities::with([
            'trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
        ])
            ->where('countries_id', 107)
            ->get();
        $found = false;
        $places = Place::with([
            'trans' => function ($query) {
                $query->whereNotNull('address');
            },
        ])->where('countries_id', 107)
            ->whereIn('id', $places)
            ->limit(500)
            ->offset($page * 500)->get();
        foreach ($places as $place) {
            if (isset($place->trans[0]->address)) {

                foreach ($cities as $city) {
                    if ((isset($city->trans[0]->title) && isset($place->trans[0]->address)) && strpos(strtolower($place->trans[0]->address), strtolower($city->trans[0]->title)) !== false) {
                        if ($place->cities_id != $city->id) {
                            Place::whereId($place->id)->update(['cities_id' => $city->id]);
                        }
                        $found = true;
                    }
                }
                if (!$found) {
                    $not_found[] = [$place->id, isset($place->trans[0]->address) ? $place->trans[0]->address : ''];
                }
                $found = false;
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="data_' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($not_found as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function getTopCitiesCountries()
    {
        $flag = false;
        $t_cities = TopCities::all();
        $counter = 0;
        foreach ($t_cities as $city) {
            $city_x = Cities::whereId($city->cities_id)->first();
            $flag = TopCities::where('cities_id', $city->cities_id)->update(['countries_id' => isset($city_x->countries_id) ? $city_x->countries_id : 0]);
            if ($flag)
                $counter++;
        }
        dd($counter);
    }
    public function movePlacePostToCitiesCountries(Request $request)
    {
        $page = $request->page;
        if (!isset($page)) {
            return 'Page is required';
        }
        $ppost = DB::select('SELECT  countries_id,cities_id,posts_id as posts_id,places.id as places_id FROM posts_places LEFT JOIN places  ON posts_places.`places_id` = places.`id`  limit 1000 offset ' . (1000 * $page));
        $records = 0;
        foreach ($ppost  as $post) {
            $data_city = [
                'places_id' => isset($post->places_id) ? $post->places_id : 0,
                'posts_id' => isset($post->posts_id) ? $post->posts_id : 0,
                'cities_id' => isset($post->cities_id) ? $post->cities_id : 0
            ];
            $data_country = [
                'places_id' => isset($post->places_id) ? $post->places_id : 0,
                'posts_id' => isset($post->posts_id) ? $post->posts_id : 0,
                'countries_id' => isset($post->countries_id) ? $post->countries_id : 0,
            ];
            PostsCountries::create($data_country);
            PostsCities::create($data_city);
            $records++;
        }
        return $records . 'Updated';
    }
    public function getCountryRecords(Request $request)
    {
        if (!isset($request->page)) {
            return 'page no required';
        }
        $row = 1;
        $places = [];
        $notfound = [];
        $cities_X = Cities::with(['trans'])->where('countries_id', 275)->get();
        if (($handle = fopen(public_path("citiescsv/275.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                // $data = array_map("utf8_encode", $data); //added
                $row++;
                if ($row > 2) {
                    if (!empty($data[0]))
                        $places[] = $data;
                }
            }
            fclose($handle);
        }
        // dd($places);
        //update places address from DB 


        // $places_new = [];
        // foreach($places as $place){

        //    $places_temp =  Place::find($place[0]);
        //    $places_new[] = [$places[0],$places_temp->trans[0]->address,$place[2]];

        // }
        // dd($places_new);

        //end script
        $cities = [];
        $not_c  = [];

        foreach ($cities_X as $city) {
            // dd($city);
            $temp = str_replace('Copenhagen', 'København', $city->trans[0]->title);
            $cities[$city->id] = $temp;
        }
        dd($cities);
        $places = array_chunk($places, 1000);
        $counter = 0;
        $notfound_flag = true;

        foreach ($places[$request->page] as $place) {
            foreach ($cities as $id => $city) {
                // dd($place[1],$city);
                if ((isset($city) && isset($place[1])) && strpos(strtolower($place[1]), strtolower($city)) !== false) {
                    Place::whereId($place[0])->update(['cities_id' => $id]);
                    $notfound_flag = false;
                    $counter++;
                }
            }
            if ($notfound_flag) {
                $notfound[] = [$place[0], $place[1], $place[2]];
            }
            $notfound_flag = true;
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="denmark_' . $request->page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($notfound as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function joinCsv(Request $request)
    {

        $name = $request->name;
        $val = $request->val;
        // $places= [];
        // $places_x= [];
        // if (($handle = fopen(public_path("citiescsv/final_places_dummy_csv.csv"),"r")) !== FALSE) {
        //     while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        //            // if(strlen($data[1]) >20)
        //                 $places[] = [$data[0],$data[1],$data[2]];
        //            // else
        //               //  $places_x[] = [$data[0],$data[1],$data[2]];
        //     }
        //     fclose($handle);
        // }
        // dd($places);
        // $fp = fopen('final_places_collect_csv.csv', 'w');
        // foreach ( $places as $line ) {
        //     fputcsv($fp, $line);
        // }
        // fclose($fp);
        // $fp = fopen('final_places_collect_addr_csv.csv', 'w');
        // foreach ( $places_x as $line ) {
        //     fputcsv($fp, $line);
        // }
        // fclose($fp);
        $name = $request->name;
        $val = $request->val;
        $places = [];
        for ($int = 0; $int <= 124; $int++) {
            if (file_exists(public_path("citiescsv/dummy_finalist/dummy_here_fin_" . $int . ".csv"))) {
                if (($handle = fopen(public_path("citiescsv/dummy_finalist/dummy_here_fin_" . $int . ".csv"), "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $places[] = [$data[0], $data[1], $data[2]];
                    }
                    fclose($handle);
                }
            }
        }
        $fp = fopen('dummy_remaing_file.csv', 'w');
        foreach ($places as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function getEmptyCities(Request $request)
    {

        $cities = DB::select('SELECT * FROM experts_cities ');
        $cities_id_X = [];
        foreach ($cities as $city) {
            $cities_id_X[] = $city->cities_id;
        }
        $cities = DB::select('SELECT * FROM cities t1 WHERE NOT EXISTS (SELECT cities_id FROM places t2 WHERE t1.id = t2.cities_id)');
        $cities_id = [];
        foreach ($cities as $city) {

            if (!in_array($city->id, $cities_id_X))
                $cities_id[] = $city->id;
        }
        $delted =  Cities::whereIn('id', $cities_id)->delete();
        $delted_X =  CitiesTranslations::whereIn('cities_id', $cities_id)->delete();

        dd($delted, $delted_X);
    }
    public function getWrongCities()
    {

        $cities = Cities::with([
            'trans',
            'countryTitle'
        ])
            ->get();
        foreach ($cities as $city) {
            if (isset($city->trans[0]))
                $citiess[]  = [$city->id, $city->trans[0]->title, $city->countryTitle->title];
        }
        // dd($citiess );
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="cities.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($citiess as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function dummyRecordsData(Request $request)
    {
        $row = 1;
        $places = [];
        if (($handle = fopen(public_path("citiescsv/dummy_k_response_file.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $row++;
                if ($row > 2) {
                    if (!empty($data[0]))
                        $places[] = $data;
                }
            }
            fclose($handle);
        }
        // dd($places);
        $not_found = [
            'City ID',
            'Address',
            'Travoo Country'
        ];
        $page = $request->page;
        $limit = $request->limit;
        if (!isset($page)) {
            return 'page is required';
        }
        $language_id = 1;
        $cities = Cities::with([
            'trans',
            'countryTitle'
        ])
            ->get();
        // dd( $cities );
        // $places = Place::with([
        //     'trans' => function ($query) {
        //         $query->whereNotNull('address');
        //     },
        // ])->where('countries_id',107)
        // ->limit(2000)
        // ->offset($page*2000)->get();
        // dd($cities);
        $counter  = 0;
        $found = false;
        $valid = false;
        $not_found = [];
        $places = array_chunk($places, 1000);
        foreach ($places[$page] as $place) {
            if (!is_null($place[1]) && isset($place[1])) {

                foreach ($cities as $city) {
                    if ((isset($city->trans[0]->title) && isset($place[1]) && isset($city->countryTitle->title)) && strpos(strtolower($place[1]), strtolower($city->trans[0]->title)) !== false  && strpos(strtolower($place[1]), strtolower($city->countryTitle->title)) !== false) {
                        Place::whereId($place[0])->update(['cities_id' => $city->id]);
                        $found = true;
                    }
                }
                if (!$found) {
                    $not_found[] = [$place[0], isset($place[1]) ? $place[1] : '', $place[2]];
                }
                $found = false;
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="dummy_here_fin_' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($not_found as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function getRemainingCountriesData(Request $request)
    {
        $page = $request->page;
        if (!isset($page)) {
            return 'énter page';
        }
        $countries = [];
        if (($handle = fopen(public_path("citiescsv/data_country.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $countries[] = $data[0];
            }
        }
        fclose($handle);
        // dd($countries);
        $new_place = [];
        $places = Place::with([
            'trans' => function ($query) {
                $query->whereNotNull('address');
            },
        ])
            ->whereNotIn('countries_id', $countries)
            ->limit(30000)
            ->offset($page * 30000)
            ->get();
        $flag = false;
        foreach ($places as $place) {
            if (isset($place->place_type)) {
                foreach ($this->placeTypesArray as $key => $placeType) {
                    $dd = explode(',', $place->place_type);
                    if (strcmp($dd[0], $placeType) == 0) {
                        $flag = true;
                    }
                }
                if ($flag) {
                    if (isset($place->trans[0]->address)) {
                        $new_place[] = [$place->id, $place->trans[0]->address, $place->countries_id];
                        $flag = false;
                    }
                }
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="leftover_' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($new_place as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function getOldCountries(Request $request)
    {
        $places = [];
        $country = [];
        if (($handle = fopen(public_path("citiescsv/new_chuss.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $places[] = $data;
            }
        }
        fclose($handle);
        if (($handle = fopen(public_path("citiescsv/old_country_list.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $country[] = $data[0];
            }
        }
        fclose($handle);
        // dd($country);
        $new_list = [];
        $new_other_country = [];
        foreach ($places as $place) {
            if (in_array($place[2], $country)) {
                $new_list[$place[0]] = $place[2];
            } else {
                $new_other_country[$place[0]] = $place[2];
            }
        }
        dd($new_other_country);
    }
    public function get95kPlace(Request $request)
    {
        $places = [];
        if (($handle = fopen(public_path("citiescsv/95kitem/new_final_places_csv.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($data[0] != '')
                    $places[] = $data;
            }
        }
        $not_found = [
            'City ID',
            'Address',
            'Travoo Country'
        ];
        $page = $request->page;
        if (!isset($page)) {
            return 'page is required';
        }
        $language_id = 1;
        $cities = Cities::with([
            'trans'
        ])
            ->get();
        $not_found = [];
        $found  = false;
        $places = array_chunk($places, 1000);
        foreach ($places[$page] as $place) {
            if (!is_null($place[1]) && isset($place[1])) {
                foreach ($cities as $city) {
                    if ((isset($city->trans[0]->title) && isset($place[1])) && strpos(strtolower($place[1]), strtolower($city->trans[0]->title)) !== false  && ($place[2] == $city->countries_id)) {
                        Place::whereId($place[0])->update(['cities_id' => $city->id]);
                        $found = true;
                    }
                }
                if (!$found) {
                    $not_found[] = [$place[0], isset($place[1]) ? $place[1] : '', $place[2]];
                }
                $found = false;
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="place_95k_' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($not_found as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }


    public function getOriginalDummy(Request $request)
    {
        $page = $request->page;
        if (!isset($page)) {
            return 'page missing';
        }
        $dummy_places = Place::with(['trans'])->where('cities_id', 2031)->orWhereIn('countries_id', [326])->limit(30000)->offset($page * 30000)->get();
        $dummy  = [];
        $flag = false;
        foreach ($dummy_places as $place) {
            foreach ($this->placeTypesArray as $key => $placeType) {
                $dd = explode(',', $place->place_type);

                if (strcmp($dd[0], $placeType) == 0) {
                    $flag = true;
                }
            }
            if ($flag) {
                $dummy[] = [$place->id, isset($place->trans[0]->address) ? $place->trans[0]->address : "", $place->countries_id];
                $flag = false;
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="dummy_restarting_' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($dummy as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function getDuplicateCity()
    {
        $cities = Cities::with([
            'trans',
            'countryTitle',
            'stateTitle'
        ])
            ->get();
        $cities_x = [];
        foreach ($cities as $city) {
            if (isset($city->trans[0]->title))
                $cities_x[$city->trans[0]->title . '_' . $city->countries_id][] =  ['city_id' => $city->id, 'city_name' => $city->trans[0]->title, 'countries' => $city->countryTitle->title, 'states' => isset($city->stateTitle[0]->title) ? $city->stateTitle[0]->title : ""];
        }
        $repeat = [];
        $state_flag = true;
        foreach ($cities_x as $key => $city) {
            if (count($city) > 1) {
                foreach ($city as $citx) {
                    if ($citx['states'] == '')
                        $state_flag = false;
                }
                if (!$state_flag)
                    $repeat[] = $city;
                $state_flag = true;
            }
        }
        $all_deleted = [];
        $all_data = [];
        dd($repeat);
        foreach ($repeat as $rep) {
            $original = 0;
            $others = [];
            // dd($original,$others);


            foreach ($rep as $key => $city) {
                if ($city['city_name']  != 'Columbia City') {
                    if ($key == 0)
                        $original = $city['city_id'];
                    else {
                        $others[] = $city['city_id'];
                        $all_deleted[] = [$city['city_id']];
                    }
                    // if($city['states'] == '' && $rep[1] !='')
                    //     Cities::whereId($original)->update(['states_id'=>$rep[1]['states']]);
                    // }
                }
            }


            // dd($original,$others);
            $total = Place::whereIn('cities_id', $others)->update(['cities_id' => $original]);
            $total = TripCities::whereIn('cities_id', $others)->update(['cities_id' => $original]);
            $total = CitiesMedias::whereIn('cities_id', $others)->update(['cities_id' => $original]);
        }
        // $places = Place::whereIn('cities_id', $all_deleted)->       count();
        // dd($all_deleted);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="tb_deleted.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($all_deleted as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }

    public function getUniquePlacesList()
    {
        $places = [];
        $places_dummy = [];
        if (($handle = fopen(public_path("citiescsv/95k_response_file.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $places[$data[0]] = $data;
            }
        }
        fclose($handle);
        if (($handle = fopen(public_path("citiescsv/dummy_remaing_file.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $places_dummy[$data[0]] = $data;
            }
        }
        fclose($handle);
        $final_list = [];
        $count = 0;
        foreach ($places_dummy as $key => $place) {
            if (!array_key_exists($key, $places)) {
                $places[$key] = $place;
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="done_final_list.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($places as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function topPlacesScript(Request $request)
    {
        $page = $request->page;
        $del_me = $request->del_me;
        $save_me = $request->save_me;
        if ($del_me == 1) {
            $dd =  PlacesTop::all();
            dd($dd);
        }
        $array = [];
        if ($save_me == 1) {
            $all = PlacesTop::all();
            dd($all);
            foreach ($all as $d) {

                $array[] = [
                    $d['id'],
                    $d['places_id'],
                    $d['country'],
                    $d['country_id'],
                    $d['city'],
                    $d['city_id'],
                    $d['title'],
                    $d['travooo_title'],
                    $d['reviews_num'],
                    $d['rating'],
                    $d['country_exists'],
                    $d['city_exists'],
                    $d['place_exists'],
                    $d['place_added'],
                    $d['place_notfound'],
                    $d['has_media'],
                    $d['destination_type'],
                ];
            }
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename="done_all_elem.csv";');
            $fp = fopen('php://output', 'w');
            foreach ($array as $line) {
                fputcsv($fp, $line);
            }
            fclose($fp);
        }

        if (!isset($page)) {
            return 'page not found';
        }
        $row = 1;
        $places = [];
        $header = [];

        if (($handle = fopen(public_path("citiescsv/top_places/Top Restaurants (Cleared).csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $row++;
                if ($row > 2)
                    $places[$data[3]] = $data;
                else
                    $header  =  $data;
            }
            fclose($handle);
        }
        dd($places);
        $places = array_chunk($places, 2000);
        $not_found = [];
        foreach ($places[$page] as $place) {
            // dd($place,$header);
            $placexs = Place::with(['trans'])
                ->whereId($place[3])->first();
            if (is_object($placexs) && isset($placexs->trans[0]->title)) {
                $datum = [
                    'places_id' => $placexs->id,
                    'city' => isset($placexs->city->trans[0]->title) ? $placexs->city->trans[0]->title : "",
                    'city_id' => $placexs->cities_id,
                    'country' => isset($placexs->country->trans[0]->title) ? $placexs->country->trans[0]->title : "",
                    'country_id' => $placexs->countries_id,
                    'title' => $place[4],
                    'travooo_title' => $placexs->trans[0]->title,
                    'rating' => ($place[6] == '' || $place[6] == '-') ? 0 : $place[6],
                    'reviews_num' => ($place[7] == '' || $place[7] == '-') ? 0 : $place[7],
                    'destination_type' => 'restaurant'
                ];
                // dd( $datum);
                PlacesTop::create($datum);
            } else {
                $not_found[] = $place;
            }
        }

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="not_matching_csv_top_places' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($not_found as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function setMislData()
    {
        $cities = Cities::where('countries_id', 161)->get()->pluck('id');

        $cities = Place::whereIn('cities_id', $cities)->update(['countries_id' => 161]);
        dd($cities);
    }
    public function setLatLongCity(Request $request)
    {
        $cities = [];
        $row = 1;
        if (($handle = fopen(public_path("citiescsv/lat long page 2.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($row > 1)
                    $cities[$data[0]] = $data;
                $row++;
            }
        }
        fclose($handle);
        $counter = 0;
        // dd($cities);
        foreach ($cities as $city) {

            $updated = cities::whereId($city)->update(['lat' => $city[1], 'lng' => $city[2]]);
            if ($updated) {
                $counter++;
            }
        }
        dd($counter);
    }
    public function deleteDuplicate()
    {
        $cities = [];
        if (($handle = fopen(public_path("citiescsv/to_be_deleted.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $cities[] = $data;
            }
        }
        fclose($handle);
        // dd($cities);
        $suggestion = SuggestionUserLog::whereIn('cities_id', $cities)->delete();
        $cities_trans = CitiesTranslations::whereIn('cities_id', $cities)->delete();
        $cities = Cities::whereIn('id', $cities)->delete();
        dd($cities_trans, $cities, $suggestion);
    }

    public function getDuplicateCountryWise()
    {
        $cities = Cities::with([
            'trans',
            'countryTitle',
            'stateTitle'
        ])
            ->get();
        $cities_x = [];
        foreach ($cities as $city) {
            if (isset($city->trans[0]->title))
                $cities_x[$city->trans[0]->title . '_' . $city->countryTitle->title][] =  [$city->id, $city->trans[0]->title, $city->countryTitle->title, isset($city->stateTitle[0]->title) ? $city->stateTitle[0]->title : ""];
        }
        $repeat = [];
        // dd( $cities_x);
        foreach ($cities_x as $city) {
            if (count($city) > 1) {
                foreach ($city as $citx) {
                    $repeat[] = $citx;
                }
            }
        }
        // dd($repeat);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="repeat_city.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($repeat as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }

    public function getUSDuplicateCoordinate()
    {

        $cities = [];
        if (($handle = fopen(public_path("citiescsv/repeat_city.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $cities[] = $data;
            }
        }
        $counter = 0;
        $res = false;
        if (isset($cities) && count($cities) > 0) {
            foreach ($cities as $city) {
                if ($city[1] == 'Columbia City') {
                } else {
                    $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?' . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($city[1] . ' in ' . $city[3] . ' In ' . $city[2]);
                    $fetch_link_for_city = file_get_contents($google_link_for_city);
                    $query_result_for_city = json_decode($fetch_link_for_city);
                    $qr_city = $query_result_for_city->results;
                    if (isset($qr_city[0]->geometry->location->lat) && $qr_city[0]->geometry->location->lng) {
                        $res = Cities::whereId($city[0])->update(['lat' => $qr_city[0]->geometry->location->lat, 'lng' => $qr_city[0]->geometry->location->lng]);
                        if ($res) {
                            $counter++;
                            $res = false;
                        }
                    }
                }
            }
        }
        echo $counter . ' Cities updated';
    }
    public function getzerolatlongCities()
    {
        $language_id = 1;
        $cities = Cities::with([
            'trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'countryTitle'
        ])
            ->where('lat', '0.00000000')
            ->where('lng', '0.00000000')
            ->where('active', 1)
            ->orderBy('id', 'ASC')
            ->get();
        $result = [];
        foreach ($cities  as $city) {
            if (isset($city->id, $city->trans[0]->title))
                $result[] = [$city->id, $city->trans[0]->title, $city->countryTitle->title, $city->lat, $city->lng];
        }
        // dd($result);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="zerolatlong.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($result as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function deleteEmptyPlaceCities()
    {
        $cities = [];
        if (($handle = fopen(public_path("citiescsv/zerolatlong.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $cities[] = $data;
            }
        }

        $places_no_city = [];
        foreach ($cities as $city) {
            $places_no_city[] = [$city[0], Place::where('cities_id', $city[0])->count()];
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="leftover_empty.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($places_no_city as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
        // dd($places_no_city);
    }
    public function getUserByLikes(Request $request)
    {
        $postId = $request->get('id');
        $postType = $request->get('type');
        $result = [];
        $post = null;
        if ($postType && $postId) {
            switch ($postType) {
                case 'report':
                    $post = Reports::with('likes.user')->find($postId);
                    break;
                case 'trip':
                    $post = TripPlans::with('likes.user')->find($postId);
                    break;
                case 'trip-media':
                    $post = TripMedias::with('media.likes.user')->where('medias_id', $postId)->first();
                    break;
                case 'event':
                    $post = Events::with('likes.user')->find($postId);
                    break;
                default:
                    if ($postType !== 'review') {
                        $post = Posts::with('likes.user')->find($postId);
                    }
            }

            if ($post) {
                if ($postType == 'trip-media') {
                    $likes = $post->media ? $post->media->likes : null;
                } else {
                    $likes = $post->likes;
                }

                if ($likes) {
                    foreach ($likes as $like) {
                        if ($like->user) {
                            $user = $like->user;
                            $result[] = ['user_id' => $user->id, 'name' => $user->name, 'profile' => $user->profile_picture];
                        }
                    }
                }
            }
        }

        if (!empty($result))
            $result = ['html' => view::make('site.home.new.partials.user_who_like', ['data' => $result])->render(), 'count' => count($result)];

        return json_encode($result);
    }
    public function getFriendsTrips($param)
    {

        $me = Auth::user()->id;
        $output = getMyFriendsOnGoingTrips($me, $param);
        if (isset($output) && !empty($output) && count($output) > 0) {
            $random_keys = array_rand($output, 1);
            return $output[$random_keys];
        }

        return [];
    }
    public function getCitiesList()
    {

        $cities = Cities::all();
        $_city = [];
        foreach ($cities as $city) {
            if (isset($city->trans[0]->title))
                $_city[] = [$city->trans[0]->title, @$city->country->trans[0]->title, $city->id];
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="ccountries_city_list.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($_city as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function getWeatherUpdateForMyTrips()
    {
        $trips = TripPlans::where('users_id', Auth::user()->id)->whereIn('id', get_triplist4me())->pluck('id')->toarray();
        $givenTrips = TripPlaces::where('time', '>=', Carbon::now()->subDay())->whereIn('trips_id', $trips)->orderBy('id', 'DESC')->first();
        if (isset($givenTrips) && isset($givenTrips->city) && isset($givenTrips->trip->places)) {
            $get_place_key = curl_init();
            curl_setopt_array($get_place_key, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q=' . $givenTrips->city->lat . ',' . $givenTrips->city->lng,
            ));
            $get_current_weather = '';
            $resp = curl_exec($get_place_key);
            curl_close($get_place_key);
            if ($resp && $resp != "null") {
                $resp = json_decode($resp);
                $place_key = $resp->Key;
                $get_current_weather = curl_init();
                curl_setopt_array($get_current_weather, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://dataservice.accuweather.com/currentconditions/v1/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
                ));
                $current_weather = curl_exec($get_current_weather);
                curl_close($get_current_weather);
                $get_current_weather = json_decode($current_weather);
                $is_multiple_place = false;
                $cities_array  = [];
                foreach ($givenTrips->trip->places as $place) {
                    $cities_array[$place->cities_id] = $place->cities_id;
                }
                if (count($cities_array) > 1)
                    $is_multiple_place = true;
                return ['weather' => $get_current_weather, 'multiple' => $is_multiple_place, 'trip' => @$givenTrips->trip, 'city' => $givenTrips->city, 'current_trip' => $givenTrips];
            }
        }
        return [];
    }
    public function getWeatherUpdateForCheckins()
    {
        $my_checkins = Checkins::where('users_id', Auth::user()->id)->where('checkin_time', '<=', Carbon::now()->addDay(2))->where('checkin_time', '>=', Carbon::now()->addDay(1))->first();

        if (isset($my_checkins->place) && isset($my_checkins)) {
            $get_place_key = curl_init();
            curl_setopt_array($get_place_key, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&q=' . $my_checkins->place->lat . ',' . $my_checkins->place->lng,
            ));
            $get_current_weather = '';
            $resp = curl_exec($get_place_key);
            curl_close($get_place_key);
            if ($resp && $resp != "null") {
                $resp = json_decode($resp);
                $place_key = $resp->Key;
                $get_daily_weather = curl_init();
                curl_setopt_array($get_daily_weather, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://dataservice.accuweather.com/forecasts/v1/daily/10day/' . $place_key . '?apikey=lE5kt8vgADp9EAtvv5cD0hqOfGG3QlS9&metric=true',
                ));
                $daily_weather = curl_exec($get_daily_weather);
                curl_close($get_daily_weather);
                return ['weather' => @json_decode($daily_weather)->DailyForecasts, 'place' => $my_checkins->place, 'checkin' => $my_checkins];
            }
        }
        return [];
    }
    // public function getQueryForHome($where_condition, $addition = null, $followings = [])
    // {
    //     if ($addition) {

    //         return ActivityLog::selectRaw('SUBSTRING_INDEX (
    //             GROUP_CONCAT(id ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `id`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(users_id ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `user_id`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(type ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `type`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(action ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `action`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(variable ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `variable`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(time ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `time`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(permission ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `permission`')
    //             ->groupBy('action')
    //             ->orderByRaw('CAST(SUBSTRING_INDEX (GROUP_CONCAT(id ORDER BY id DESC),",",1) AS UNSIGNED) DESC')
    //             ->limit(12)
    //             ->whereNotIn('id', $addition)
    //             ->whereRaw($where_condition)
    //             ->whereIn('users_id', $followings)
    //             ->get();
    //     } else {
    //         return ActivityLog::selectRaw('SUBSTRING_INDEX (
    //             GROUP_CONCAT(id ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `id`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(users_id ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `user_id`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(type ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `type`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(action ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `action`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(variable ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `variable`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(time ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `time`,
    //             SUBSTRING_INDEX (
    //             GROUP_CONCAT(permission ORDER BY id DESC),
    //             ",",
    //             1
    //             ) AS `permission`')
    //             ->groupBy('action')
    //             ->orderByRaw('CAST(SUBSTRING_INDEX (GROUP_CONCAT(id ORDER BY id DESC),",",1) AS UNSIGNED) DESC')
    //             ->limit(12)
    //             ->whereRaw($where_condition)
    //             ->whereIn('users_id', $followings)
    //             ->get();
    //     }
    // }

    public function deleteTripPlan()
    {

        $trip = ActivityuserLog::where(['action' => 'publish', 'type' => 'Post', 'variable' => '670203'])->delete();

        dd($trip);

        // $trip =  AdminLogs::where(['item_type' => 'places', 'item_id' => '146513'])->get();

        // Place::find(146513);
        dd($trip);
    }
    public function getAllTopPlaces()
    {

        $all = PlacesTop::count();
        dd($all);
        foreach ($all as $d) {

            $array[] = [
                $d['id'],
                $d['places_id'],
                $d['country'],
                $d['country_id'],
                $d['city'],
                $d['city_id'],
                $d['title'],
                $d['travooo_title'],
                $d['reviews_num'],
                $d['rating'],
                $d['country_exists'],
                $d['city_exists'],
                $d['place_exists'],
                $d['place_added'],
                $d['place_notfound'],
                $d['has_media'],
                $d['destination_type'],
            ];
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="done_all_elem.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($array as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    // private function getTrendingPosts_Old($new = false)
    // {
    //     $already_posted_ids = ['posts_ids' => session('loaded_posts_ids'), 'discussions_ids' => session('loaded_discussion_ids'), 'trips_ids' => session('loaded_trips_ids'), 'travlogs_ids' => session('loaded_travlog_ids')];
    //     $topWeeklyPost = [];
    //     $chosenType = ['report' => 'report', 'discussion' => 'discussion', 'post' => 'post', 'trip' => 'trip'];

    //     $chosenType = array_rand($chosenType);
    //     $final_outcome = getDesiredLanguage();
    //     if (isset($final_outcome[0])) {
    //         $final_outcome = $final_outcome[0];
    //     } else {
    //         $final_outcome = UsersContentLanguages::DEFAULT_LANGUAGES;
    //     }
    //     if (!$new) {
    //         if ($chosenType == 'post') {
    //             $whereCondition = '';
    //             if (!empty($already_posted_ids['posts_ids']))
    //                 $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['posts_ids']) . ')';

    //             $watchedCheck = $this->getWatchedPost('post', 1);
    //             $whereWatched = '';
    //             if (!empty($watchedCheck))
    //                 $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';

    //             $topWeeklyPost['trending_post']  = DB::select(DB::raw('
    //                                                                 SELECT
    //                                                     t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
    //                                                     FROM
    //                                                     `posts` AS t
    //                                                     LEFT JOIN
    //                                                     (SELECT
    //                                                         likes.`posts_id` AS r_id,
    //                                                         COUNT(*) AS counts
    //                                                         FROM
    //                                                         `posts_likes` AS likes
    //                                                         WHERE likes.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                                                         GROUP BY likes.posts_id) AS l
    //                                                         ON l.r_id = t.id
    //                                                     LEFT JOIN
    //                                                     (SELECT
    //                                                         shares.`posts_type_id` AS r_id,
    //                                                         COUNT(*) AS counts
    //                                                         FROM
    //                                                         `posts_shares` AS shares
    //                                                         WHERE shares.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                                                         and type ="text"
    //                                                         GROUP BY shares.posts_type_id) AS s
    //                                                         ON s.r_id = t.id
    //                                                     LEFT JOIN
    //                                                     (SELECT
    //                                                         views.`posts_id` AS r_id,
    //                                                         COUNT(*) AS counts
    //                                                         FROM
    //                                                         `posts_comments` AS views
    //                                                         WHERE views.created_at > "' . Carbon::now()->subDays(7) . '"
    //                                                         and type="post"
    //                                                         GROUP BY views.posts_id) AS v
    //                                                         ON v.r_id = t.id
    //                                                     WHERE deleted_at IS NULL
    //                                                         ' . $whereCondition . '
    //                                                         ' . $whereWatched . '
    //                                                         and language_id = ' . $final_outcome . '
    //                                                         AND 
    //                                                         (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
    //                                                         > 5   and  t.users_id <> ' . Auth::user()->id . ' 
    //                                                     GROUP BY t.id
    //                                                     ORDER BY popular DESC
    //                                                     LIMIT 1
    //             '));
    //             if (empty($topWeeklyPost['trending_post']) && $final_outcome != UsersContentLanguages::DEFAULT_LANGUAGES) {
    //                 $topWeeklyPost['trending_post']  = DB::select(DB::raw('
    //                                     SELECT
    //                         t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
    //                         FROM
    //                         `posts` AS t
    //                         LEFT JOIN
    //                         (SELECT
    //                             likes.`posts_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `posts_likes` AS likes
    //                             WHERE likes.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                             GROUP BY likes.posts_id) AS l
    //                             ON l.r_id = t.id
    //                         LEFT JOIN
    //                         (SELECT
    //                             shares.`posts_type_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `posts_shares` AS shares
    //                             WHERE shares.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                             and type ="text"
    //                             GROUP BY shares.posts_type_id) AS s
    //                             ON s.r_id = t.id
    //                         LEFT JOIN
    //                         (SELECT
    //                             views.`posts_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `posts_comments` AS views
    //                             WHERE views.created_at > "' . Carbon::now()->subDays(7) . '"
    //                             and type="post"
    //                             GROUP BY views.posts_id) AS v
    //                             ON v.r_id = t.id
    //                         WHERE deleted_at IS NULL
    //                             ' . $whereCondition . '
    //                             ' . $whereWatched . '
    //                         and language_id = ' . UsersContentLanguages::DEFAULT_LANGUAGES . '
    //                             AND 
    //                             (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
    //                             > 5   and  t.users_id <> ' . Auth::user()->id . ' 
    //                         GROUP BY t.id
    //                         ORDER BY popular DESC
    //                         LIMIT 1
    //             '));
    //             }
    //         } else if ($chosenType == 'discussion') {
    //             $whereCondition = '';
    //             if (!empty($already_posted_ids['discussions_ids']))
    //                 $whereCondition = 'and  discussions_id not in (' . implode(',', @$already_posted_ids['discussions_ids']) . ')';
    //             $watchedCheck = $this->getWatchedPost('discussion', 1);
    //             $whereWatched = '';
    //             if (!empty($watchedCheck))
    //                 $whereWatched = 'and discussions_id not in (' . implode(',', @$watchedCheck) . ')';

    //             $topWeeklyPost['trending_discussion'] = DB::select(DB::raw('SELECT discussions_id as id FROM `discussion_replies` left join discussions on discussions.id = discussion_replies.discussions_id  where discussion_replies.created_at > "' . Carbon::now()->subDays(7) . '" and language_id= ' . $final_outcome . ' ' . $whereCondition . ' ' . $whereWatched . '  GROUP BY discussions_id HAVING
    //             COUNT(discussions_id) >= 5 ORDER BY COUNT(discussions_id) DESC LIMIT 1'));
    //             if (empty($topWeeklyPost['trending_discussion']) && $final_outcome != 1) {
    //                 $topWeeklyPost['trending_discussion'] =  DB::select(DB::raw('SELECT discussions_id as id FROM `discussion_replies` left join discussions on discussions.id = discussion_replies.discussions_id  where discussion_replies.created_at > "' . Carbon::now()->subDays(7) . '" and language_id= 1 ' . $whereCondition . ' ' . $whereWatched . '   GROUP BY discussions_id HAVING
    //             COUNT(discussions_id) >= 5 ORDER BY COUNT(discussions_id) DESC LIMIT 1'));
    //             }
    //         } else if ($chosenType == 'trip') {

    //             $whereCondition = '';
    //             if (!empty($already_posted_ids['trips_ids']))
    //                 $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['trips_ids']) . ')';
    //             $myTrips = get_triplist4me();
    //             $additionalWhere = '';
    //             if (!empty($myTrips))
    //                 $whereCondition = 'and t.id in (' . implode(',', $myTrips) . ')';

    //             $watchedCheck = $this->getWatchedPost('trip', 1);
    //             $whereWatched = '';
    //             if (!empty($watchedCheck))
    //                 $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';
    //             $topWeeklyPost['trending_tripplan'] = DB::select(DB::raw('
    //                         SELECT
    //                         t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0)  + IFNULL(k.counts, 0))  AS popular
    //                         FROM
    //                         `trips` AS t
    //                         LEFT JOIN
    //                         (SELECT
    //                             likes.`trips_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `trips_likes` AS likes
    //                             WHERE likes.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                             GROUP BY likes.trips_id) AS l
    //                             ON l.r_id = t.id
    //                         LEFT JOIN
    //                         (SELECT
    //                             shares.`posts_type_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `posts_shares` AS shares
    //                             WHERE shares.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                             and type = "trip"
    //                             GROUP BY shares.posts_type_id) AS s
    //                             ON s.r_id = t.id
    //                         LEFT JOIN
    //                         (SELECT
    //                             views.`posts_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `posts_comments` AS views
    //                             WHERE views.created_at > "' . Carbon::now()->subDays(7) . '"
    //                             and type = "trip"
    //                             GROUP BY views.posts_id) AS v
    //                             ON v.r_id = t.id
    //                         LEFT JOIN 
    //                         (SELECT
    //                             tp_views.`trips_id` AS r_id,
    //                             COUNT(trip_place_id) AS counts
    //                             FROM
    //                             `trips_places` AS tp_views LEFT JOIN
    //                             trips_places_comments tps  ON  tps.`trip_place_id` =  tp_views.`id`
    //                             WHERE tps.created_at > "' . Carbon::now()->subDays(7) . '"
    //                             GROUP BY tp_views.trips_id) as k
    //                             ON k.r_id = t.id
    //                         WHERE deleted_at IS NULL
    //                             ' . $whereCondition . '
    //                             ' . $additionalWhere . '
    //                             ' . $whereWatched . '
    //                            and  language_id = ' . $final_outcome . '
    //                             AND 
    //                             (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0)  + IFNULL(k.counts, 0))
    //                             > 5 and  t.users_id <> ' . Auth::user()->id . ' 
    //                         GROUP BY t.id
    //                         ORDER BY popular DESC
    //                         LIMIT 1
    //             '));
    //             if (empty($topWeeklyPost['trending_tripplan'])) {
    //                 $topWeeklyPost['trending_tripplan'] = DB::select(DB::raw('
    //                         SELECT
    //                         t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) + IFNULL(k.counts, 0) )  AS popular
    //                         FROM
    //                         `trips` AS t
    //                         LEFT JOIN
    //                         (SELECT
    //                             likes.`trips_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `trips_likes` AS likes
    //                             WHERE likes.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                             GROUP BY likes.trips_id) AS l
    //                             ON l.r_id = t.id
    //                         LEFT JOIN
    //                         (SELECT
    //                             shares.`posts_type_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `posts_shares` AS shares
    //                             WHERE shares.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                             and type = "trip"
    //                             GROUP BY shares.posts_type_id) AS s
    //                             ON s.r_id = t.id
    //                         LEFT JOIN
    //                         (SELECT
    //                             views.`posts_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `posts_comments` AS views
    //                             WHERE views.created_at > "' . Carbon::now()->subDays(7) . '"
    //                             and type = "trip"
    //                             GROUP BY views.posts_id) AS v
    //                             ON v.r_id = t.id
    //                         LEFT JOIN 
    //                         (SELECT
    //                             tp_views.`trips_id` AS r_id,
    //                             COUNT(trip_place_id) AS counts
    //                             FROM
    //                             `trips_places` AS tp_views LEFT JOIN
    //                             trips_places_comments tps  ON  tps.`trip_place_id` =  tp_views.`id`
    //                             WHERE tps.created_at > "' . Carbon::now()->subDays(7) . '"
    //                             GROUP BY tp_views.trips_id) as k
    //                             ON k.r_id = t.id
    //                         WHERE deleted_at IS NULL
    //                             ' . $whereCondition . '
    //                             ' . $additionalWhere . '
    //                             ' . $whereWatched . '
    //                            and  language_id = 1
    //                             AND 
    //                             (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) + IFNULL(k.counts, 0))
    //                             > 5 and  t.users_id <> ' . Auth::user()->id . ' 
    //                         GROUP BY t.id
    //                         ORDER BY popular DESC
    //                         LIMIT 1
    //                 '));
    //             }
    //         } else if ($chosenType == 'report') {
    //             $whereCondition = '';
    //             if (!empty($already_posted_ids['travlogs_ids']))
    //                 $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['travlogs_ids']) . ')';

    //             $watchedCheck = $this->getWatchedPost('travlog', 1);
    //             $whereWatched = '';
    //             if (!empty($watchedCheck))
    //                 $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';
    //             $topWeeklyPost['trending_report'] = DB::select(DB::raw('
    //                         SELECT
    //                     t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
    //                     FROM
    //                     `reports` AS t
    //                     LEFT JOIN
    //                     (SELECT
    //                         likes.`reports_id` AS r_id,
    //                         COUNT(*) AS counts
    //                         FROM
    //                         `reports_likes` AS likes
    //                         WHERE likes.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                         GROUP BY likes.reports_id) AS l
    //                         ON l.r_id = t.id
    //                     LEFT JOIN
    //                     (SELECT
    //                         shares.`posts_type_id` AS r_id,
    //                         COUNT(*) AS counts
    //                         FROM
    //                         `posts_shares` AS shares
    //                         WHERE type ="report" and shares.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                         and type ="report"
    //                         GROUP BY shares.posts_type_id) AS s
    //                         ON s.r_id = t.id
    //                     LEFT JOIN
    //                     (SELECT
    //                         views.`posts_id` AS r_id,
    //                         COUNT(*) AS counts
    //                         FROM
    //                         `posts_comments` AS views
    //                         WHERE views.created_at > "' . Carbon::now()->subDays(7) . '"
    //                         and type ="report"
    //                         GROUP BY views.posts_id) AS v
    //                         ON v.r_id = t.id
    //                     WHERE 
    //                         EXISTS(SELECT * FROM reports_infos WHERE reports_infos.reports_id=t.id and var="cover") 
    //                         AND deleted_at IS NULL
    //                         ' . $whereCondition . '
    //                         ' . $whereWatched . '
    //                        and  language_id = ' . $final_outcome . '
    //                        AND published_date IS NOT NULL and flag = 1
    //                         AND 
    //                         (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
    //                         > 5 and  t.users_id <> ' . Auth::user()->id . ' 
    //                     GROUP BY t.id
    //                     ORDER BY popular DESC
    //                     LIMIT 1
    //             '));
    //             if (empty($topWeeklyPost['trending_report'])) {
    //                 $topWeeklyPost['trending_report'] = DB::select(DB::raw('
    //                         SELECT
    //                     t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
    //                     FROM
    //                     `reports` AS t
    //                     LEFT JOIN
    //                     (SELECT
    //                         likes.`reports_id` AS r_id,
    //                         COUNT(*) AS counts
    //                         FROM
    //                         `reports_likes` AS likes
    //                         WHERE likes.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                         GROUP BY likes.reports_id) AS l
    //                         ON l.r_id = t.id
    //                     LEFT JOIN
    //                     (SELECT
    //                         shares.`posts_type_id` AS r_id,
    //                         COUNT(*) AS counts
    //                         FROM
    //                         `posts_shares` AS shares
    //                         WHERE type ="report" and shares.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                         and type ="report"
    //                         GROUP BY shares.posts_type_id) AS s
    //                         ON s.r_id = t.id
    //                     LEFT JOIN
    //                     (SELECT
    //                         views.`posts_id` AS r_id,
    //                         COUNT(*) AS counts
    //                         FROM
    //                         `posts_comments` AS views
    //                         WHERE views.created_at > "' . Carbon::now()->subDays(7) . '"
    //                         and type ="report"
    //                         GROUP BY views.posts_id) AS v
    //                         ON v.r_id = t.id
    //                     WHERE 
    //                         EXISTS(SELECT * FROM reports_infos WHERE reports_infos.reports_id=t.id and var="cover") 
    //                         AND deleted_at IS NULL
    //                         ' . $whereCondition . '
    //                         ' . $whereWatched . '
    //                        and  language_id = 1
    //                        AND published_date IS NOT NULL and flag = 1
    //                         AND 
    //                         (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
    //                         > 5 and  t.users_id <> ' . Auth::user()->id . ' 
    //                     GROUP BY t.id
    //                     ORDER BY popular DESC
    //                     LIMIT 1
    //                 '));
    //             }
    //         }
    //     } else {
    //         $Type = ['trip' => 'trip', 'post' => 'post', 'report' => 'report', 'discussion' => 'discussion'];
    //         foreach ($Type as $chosenType) {
    //             if ($chosenType == 'post') {
    //                 $whereCondition = '';
    //                 if (!empty($already_posted_ids['posts_ids']))
    //                     $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['posts_ids']) . ')';
    //                 $watchedCheck = $this->getWatchedPost('post', 1);
    //                 $whereWatched = '';
    //                 if (!empty($watchedCheck))
    //                     $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';
    //                 $topWeeklyPost['trending_post']  = DB::select(DB::raw('
    //                                                                     SELECT
    //                                                         t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
    //                                                         FROM
    //                                                         `posts` AS t
    //                                                         LEFT JOIN
    //                                                         (SELECT
    //                                                             likes.`posts_id` AS r_id,
    //                                                             COUNT(*) AS counts
    //                                                             FROM
    //                                                             `posts_likes` AS likes
    //                                                             WHERE likes.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                                                             GROUP BY likes.posts_id) AS l
    //                                                             ON l.r_id = t.id
    //                                                         LEFT JOIN
    //                                                         (SELECT
    //                                                             shares.`posts_type_id` AS r_id,
    //                                                             COUNT(*) AS counts
    //                                                             FROM
    //                                                             `posts_shares` AS shares
    //                                                             WHERE shares.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                                                             and type ="text"
    //                                                             GROUP BY shares.posts_type_id) AS s
    //                                                             ON s.r_id = t.id
    //                                                         LEFT JOIN
    //                                                         (SELECT
    //                                                             views.`posts_id` AS r_id,
    //                                                             COUNT(*) AS counts
    //                                                             FROM
    //                                                             `posts_comments` AS views
    //                                                             WHERE views.created_at > "' . Carbon::now()->subDays(7) . '"
    //                                                             and type ="post"
    //                                                             GROUP BY views.posts_id) AS v
    //                                                             ON v.r_id = t.id
    //                                                         WHERE deleted_at IS NULL
    //                                                             ' . $whereCondition . '
    //                                                             ' . $whereWatched . '
    //                                                             AND language_id = ' . $final_outcome . '
    //                                                             AND 
    //                                                             (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
    //                                                             > 5   and  t.users_id <> ' . Auth::user()->id . ' 
    //                                                         GROUP BY t.id
    //                                                         ORDER BY popular DESC
    //                                                         LIMIT 1
    //                 '));
    //             } else if ($chosenType == 'discussion') {
    //                 $whereCondition = '';
    //                 if (!empty($already_posted_ids['discussions_ids']))
    //                     $whereCondition = 'and  discussions_id not in (' . implode(',', @$already_posted_ids['discussions_ids']) . ')';


    //                 $watchedCheck = $this->getWatchedPost('discussion', 1);
    //                 $whereWatched = '';
    //                 if (!empty($watchedCheck))
    //                     $whereWatched = 'and discussions_id not in (' . implode(',', @$watchedCheck) . ')';

    //                 $topWeeklyPost['trending_discussion'] = DB::select(DB::raw('SELECT discussions_id as id FROM `discussion_replies` left join discussions on discussions.id = discussion_replies.discussions_id where discussion_replies.created_at > "' . Carbon::now()->subDays(7) . '"  AND language_id = ' . $final_outcome . ' ' . $whereCondition . ' ' . $whereWatched . '   GROUP BY discussions_id HAVING
    //                 COUNT(discussions_id) >= 5 ORDER BY COUNT(discussions_id) DESC LIMIT 1'));
    //             } else if ($chosenType == 'trip') {

    //                 $whereCondition = '';
    //                 if (!empty($already_posted_ids['trips_ids']))
    //                     $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['trips_ids']) . ')';
    //                 $myTrips = get_triplist4me();
    //                 $additionalWhere = '';
    //                 if (!empty($myTrips))
    //                     $additionalWhere = 'and t.id in (' . implode(',', @$myTrips) . ')';


    //                 $watchedCheck = $this->getWatchedPost('trip', 1);
    //                 $whereWatched = '';
    //                 if (!empty($watchedCheck))
    //                     $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';
    //                 // $additionalWhere = 'and t.privacy =0';

    //                 $topWeeklyPost['trending_tripplan'] = DB::select(DB::raw('
    //                             SELECT
    //                             t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) + IFNULL(k.counts, 0) )  AS popular
    //                             FROM
    //                             `trips` AS t
    //                             LEFT JOIN
    //                             (SELECT
    //                                 likes.`trips_id` AS r_id,
    //                                 COUNT(*) AS counts
    //                                 FROM
    //                                 `trips_likes` AS likes
    //                                 WHERE likes.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                                 GROUP BY likes.trips_id) AS l
    //                                 ON l.r_id = t.id
    //                             LEFT JOIN
    //                             (SELECT
    //                                 shares.`posts_type_id` AS r_id,
    //                                 COUNT(*) AS counts
    //                                 FROM
    //                                 `posts_shares` AS shares
    //                                 WHERE shares.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                                 and type ="trip"
    //                                 GROUP BY shares.posts_type_id) AS s
    //                                 ON s.r_id = t.id
    //                             LEFT JOIN
    //                             (SELECT
    //                                 views.`posts_id` AS r_id,
    //                                 COUNT(*) AS counts
    //                                 FROM
    //                                 `posts_comments` AS views
    //                                 WHERE views.created_at > "' . Carbon::now()->subDays(7) . '"
    //                                 and type ="trip"
    //                                 GROUP BY views.posts_id) AS v
    //                                 ON v.r_id = t.id
    //                             LEFT JOIN 
    //                             (SELECT
    //                                 tp_views.`trips_id` AS r_id,
    //                                 COUNT(trip_place_id) AS counts
    //                                 FROM
    //                                 `trips_places` AS tp_views LEFT JOIN
    //                                 trips_places_comments tps  ON  tps.`trip_place_id` =  tp_views.`id`
    //                                 WHERE tps.created_at > "' . Carbon::now()->subDays(7) . '"
    //                                 GROUP BY tp_views.trips_id
    //                             ) as k
    //                             ON k.r_id = t.id

    //                             WHERE deleted_at IS NULL
    //                             AND language_id = ' . $final_outcome . '
    //                                 ' . $whereCondition . '
    //                                 ' . $additionalWhere . '
    //                                 ' . $whereWatched . '
    //                                 AND 
    //                                 (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) + IFNULL(k.counts, 0))
    //                                 > 5 and  t.users_id <> ' . Auth::user()->id . ' 
    //                             GROUP BY t.id
    //                             ORDER BY popular DESC
    //                             LIMIT 1
    //                 '));
    //             } else if ($chosenType == 'report') {
    //                 $whereCondition = '';
    //                 if (!empty($already_posted_ids['travlogs_ids']))
    //                     $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['travlogs_ids']) . ')';

    //                 $watchedCheck = $this->getWatchedPost('travlog', 1);
    //                 $whereWatched = '';
    //                 if (!empty($watchedCheck))
    //                     $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';

    //                 $topWeeklyPost['trending_report'] = DB::select(DB::raw('
    //                             SELECT
    //                         t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
    //                         FROM
    //                         `reports` AS t
    //                         LEFT JOIN
    //                         (SELECT
    //                             likes.`reports_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `reports_likes` AS likes
    //                             WHERE likes.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                             GROUP BY likes.reports_id) AS l
    //                             ON l.r_id = t.id
    //                         LEFT JOIN
    //                         (SELECT
    //                             shares.`posts_type_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `posts_shares` AS shares
    //                             WHERE shares.created_at >  "' . Carbon::now()->subDays(7) . '"
    //                             and type="report"
    //                             GROUP BY shares.posts_type_id) AS s
    //                             ON s.r_id = t.id
    //                         LEFT JOIN
    //                         (SELECT
    //                             views.`posts_id` AS r_id,
    //                             COUNT(*) AS counts
    //                             FROM
    //                             `posts_comments` AS views
    //                             WHERE views.created_at > "' . Carbon::now()->subDays(7) . '"
    //                             and type ="report"
    //                             GROUP BY views.posts_id) AS v
    //                             ON v.r_id = t.id
    //                         WHERE 
    //                             deleted_at IS NULL
    //                             ' . $whereCondition . '
    //                             ' . $whereWatched . '
    //                             AND language_id = ' . $final_outcome . '
    //                             AND published_date IS NOT NULL and flag = 1
    //                             AND EXISTS(SELECT * FROM reports_infos WHERE reports_infos.reports_id=t.id and var="cover") 
    //                             AND 
    //                             (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
    //                             > 5 and  t.users_id <> ' . Auth::user()->id . ' 
    //                         GROUP BY t.id
    //                         ORDER BY popular DESC
    //                         LIMIT 1
    //                 '));
    //             }
    //         }
    //     }
    //     return  $topWeeklyPost;
    // }
    private function getPublicTrips()
    {
        return TripPlans::whereRaw('privacy=0')->pluck('id')->toArray();
    }
    public function getCitiesTypes(Request $request)
    {
        $page  = $request->page;
        if (!isset($page)) {
            return 'enter page';
        }
        $language_id = 1;
        $cities = Cities::with([
            'trans' => function ($query) use ($language_id) {
                $query->where('languages_id', $language_id);
            },
            'countryTitle'
        ])
            ->offset($page * 50)
            ->limit(50)
            ->get();
        $differnt_level = [];
        $resultant_array = [];
        $not_found = [];
        foreach ($cities as $city) {
            if (isset($city->trans[0]->title)) {
                $query = $city->trans[0]->title . ' City in ' . $city->countryTitle->title;
                $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?' . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($query);
                $fetch_link_for_city = file_get_contents($google_link_for_city);
                $query_result_for_city = json_decode($fetch_link_for_city);
                $qr_city = $query_result_for_city->results;
                if (isset($qr_city[0]) && count($query_result_for_city->results) > 0) {
                    if ($qr_city[0]->types[0] == 'locality' || $qr_city[0]->types[0] == 'administrative_area_level_3') {
                        $cityx = Cities::whereId($city->id)->update(['lat' => $qr_city[0]->geometry->location->lat, 'lng' => $qr_city[0]->geometry->location->lng]);
                        $cities_trans = CitiesTranslations::where('cities_id', $city->id)->update(['title' => $qr_city[0]->name]);
                    } else {
                        $resultant_array[] = ['city_id' => $city->id, 'provider_id' => $qr_city[0]->place_id, 'DB title' => @$city->trans[0]->title, 'Google Title' => $qr_city[0]->name, 'type' => @$qr_city[0]->types[0]];
                    }
                } else {
                    $resultant_array[] = ['city_id' => $city->id, 'provider_id' => 0, 'DB title' => @$city->trans[0]->title, 'Google Title' => 'City not found google 0 result', 'type' => 'unspecified'];
                }
            } else {
                $resultant_array[] = ['city_id' => $city->id, 'provider_id' => 0, 'DB title' => @$city->trans[0]->title, 'Google Title' => 'DB result empty', 'type' => 'unspecified'];
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="cities_with_type' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($resultant_array as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function fixTopPlaceCountry(Request $request)
    {
        // dd($request->all());

        $city = $request->city;
        $query = $request->country;

        $country = Countries::with('transsingle')
            ->whereHas('transsingle', function ($q) use ($query) {
                $q->where('title', 'LIKE', "%$query%");
            })
            ->orderBy('id')
            ->first();
        $city = Cities::with('transsingle')
            ->whereHas('transsingle', function ($q) use ($city) {
                $q->where('title', 'LIKE', "%$city%");
            })
            ->orderBy('id')
            ->first();
        $countryTitle = $country->transsingle->title;
        $resultant_array = [];
        $places = Place::with('transsingle')->whereHas('transsingle', function ($q) use ($countryTitle) {
            $q->where('address', 'LIKE', "%', $countryTitle%");
        })->where('cities_id', $city->id)->where('countries_id', '<>', $country->id)->get();
        foreach ($places as $place) {
            $resultant_array[] = [@$place->transsingle->title, @$place->id, @$city->transsingle->title, @$city->id, @$place->countries_id];
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="places of ' . @$city->transsingle->title . ' with ' . @$country->transsingle->title . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($resultant_array as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    public function updateWrongCountryPlaces(Request $request)
    {
    }
    public function updatePlacesWithCountries(Request $request)
    {
        $page = $request->page;
        if (!isset($page)) {
            return 'Page number is missing';
        }
        $places = Place::with('transsingle')->limit(10000)->offset($page * 10000)->get();
        $resultant_places = [];
        foreach ($places as $place) {
            $flag = false;
            foreach ($this->placeTypesArray as $key => $placeType) {
                $dd = explode(',', $place->place_type);

                if (strcmp($dd[0], $placeType) == 0) {
                    $flag = true;
                }
            }
            if ($flag && isset($place->transsingle->title) && isset($place->country->transsingle->title)) {

                if (!(strpos($place->transsingle->address, trim($place->country->transsingle->title)) !== false)) {
                    $resultant_places[] = [$place->id, $place->transsingle->title, $place->transsingle->address, $place->country->transsingle->title];
                }
            }
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="top places of differnt country with pagination No:' . $page . '.csv";');
        $fp = fopen('php://output', 'w');
        foreach ($resultant_places as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
    private function discoverNewTravellers($authUser)
    {
        $userLoc = userLoc($authUser, TRUE);
        $ip = get_client_ip();
        $myTravelStyles = $authUser->travelstyles->pluck('conf_lifestyles_id')->toArray();

        // #1 & #2
        $users = User::whereNotNull('profile_picture')
            ->whereNotIn('id', [1109])
            ->whereIn('nationality', $userLoc)
            ->orderBy('created_at', 'DESC')
            ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                    $travelstylesSubQuery->whereIn('conf_lifestyles_id', $myTravelStyles);
                });
            })->take(50)->get();
        if (count($users) == 0) {
            // #3 & #4
            $otherUser = userLoc($authUser, TRUE);
            $users = User::whereNotNull('profile_picture')
                ->whereNotIn('id', [1109])
                ->whereNotIn('nationality', [$authUser->nationality])
                ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                    $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                        $travelstylesSubQuery->whereIn('conf_lifestyles_id', $myTravelStyles);
                    });
                })
                ->when(count($otherUser), function ($q) use ($otherUser) {
                    $q->orWhereIn('nationality', $otherUser);
                })
                ->orderBy('created_at', 'DESC')->take(50)->get();
            if (count($users) == 0) {
                // #3 & #4
                $users = User::whereNotNull('profile_picture')
                    ->whereNotIn('id', [1109])
                    ->whereIn('nationality', [$authUser->nationality])
                    ->when(count($otherUser), function ($q) use ($otherUser) {
                        $q->orWhereIn('nationality', $otherUser);
                    })
                    ->when((count($myTravelStyles) > 0), function ($travelStylesQuery)  use ($myTravelStyles) {
                        $travelStylesQuery->whereHas('travelstyles', function ($travelstylesSubQuery) use ($myTravelStyles) {
                            $travelstylesSubQuery->orWhereIn('conf_lifestyles_id', $myTravelStyles);
                        });
                    })
                    ->orderBy('created_at', 'DESC')->take(50)->get();
            }
        }
        $users = $users->each(function ($user) use ($authUser) {
            $user->follow_flag      = (UsersFollowers::where('followers_id', $authUser->id)->where('users_id', $user->id)->where('follow_type', 1)->first())  ? true : false;
            $user->total_follower   = $user->get_followers()->whereHas('follower')->count();
            $user->latest_followers = $user->get_followers()->whereHas('follower')->take(3)->get()->map(function ($follower) {
                return [
                    'id' => $follower->followers_id,
                    'name' => $follower->follower->name,
                ];
            })->toArray();
            unset($user->get_followers);
        });
        return $users;
    }
    public function fetchAllCategoriesFromMusement()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.musement.com/api/v3/categories-tree",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);
            dd($response);
            foreach ($response['children']  as $children) {
                $this->saveEventCategories($children, 0);
            }
        }
    }
    private function saveEventCategories($categoryData, $parent_id)
    {
        if (empty($categoryData['children']))
            return false;
        else {
            foreach ($categoryData['children'] as $children) {
                $eventCat = EventCategories::where('tp_id', $children['id'])->first();
                if (!is_object($eventCat))
                    $eventCat =  EventCategories::create(['p_id' => $parent_id, 'name' => $children['name'], 'code' => $children['code'], 'type' => $categoryData['name'], 'tp_id' => $children['id']]);
                $this->saveEventCategories($children, $eventCat->id);
            }
        }
    }
    public function fetchAllCitiesFromMusemnt()
    {


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.musement.com/api/v3/cities",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: a6325019-ad34-4851-9e29-847781bf4f29",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $countr = 0;
            $response = json_decode($response, true);
            foreach ($response as $data) {
                $country_id = Countries::whereHas('transsingle', function ($query) use ($data) {
                    $query->where('title', 'like', '%' . $data['country']['name'] . '%');
                })->get()->first();
                if (isset($country_id->id)) {
                    $temp_city = Cities::whereHas('transsingle', function ($query) use ($data) {
                        $query->where('title', $data['name']);
                    })->where('countries_id', $country_id->id)->get()->first();
                }
                if (!EventCities::where('tp_id', $data['id'])->exists()) {
                    $ec = EventCities::create([
                        'tp_id' => $data['id'],
                        'tp_country_id' => $data['country']['id'],
                        'cities_id' => isset($temp_city->id) ? $temp_city->id : 0,
                        'countries_id' => isset($country_id->id) ? $country_id->id : 0,
                        'name' => $data['name'],
                        'country' => $data['country']['name']
                    ]);
                    if ($ec)
                        $countr++;
                }
            }
            dd($countr . ' new cities added');
        }
    }
    public function fetchTestEvents()
    {
        ignore_user_abort(true);
        set_time_limit(0);
        $EventCities = EventCities::all();
        dd($EventCities);
        foreach ($EventCities as $ec) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.musement.com/api/v3/cities/" . $ec->tp_id . "/activities",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                continue;
            } else {
                $response = json_decode($response, true);
                foreach ($response['data'] as $event) {

                    if (!Events::where(['provider_id' => $event['uuid']])->exists()) {
                        $ev_cr =  Events::create([
                            'countries_id' => $ec->countries_id,
                            'cities_id' => $ec->cities_id,
                            'city_id' => $ec->cities_id,
                            'provider_id' => $event['uuid'],
                            'category' => @$event['categories'][0]['name'],
                            'title' => $event['title'],
                            'description' => @$event['description'],
                            'lat' => isset($event['latitude']) ? $event['latitude'] : 0,
                            'lng' => isset($event['longitude']) ? $event['longitude'] : 0,
                            'variable' => json_encode($event),
                            'ratings' => $event['reviews_avg'],
                            'reviews' => $event['reviews_number']
                        ]);
                    }
                }
            }
        }
    }
    private function getEventForUser($cities_array = [])
    {
        $event_activity = session('events_activites');
        if (isset($cities_array) && !empty($cities_array)) {
            if (isset($event_activity))
                $event = Events::whereIn('cities_id', $cities_array)->whereNotIn('id', $event_activity)->orderBy('ratings', 'DESC')->whereDate('created_at', '>', Carbon::now()->subDays(7))->orderBy('reviews', 'DESC')->first();
            else
                $event = Events::whereIn('cities_id', $cities_array)->orderBy('ratings', 'DESC')->whereDate('created_at', '>', Carbon::now()->subDays(7))->orderBy('reviews', 'DESC')->first();
            return $event;
        }
    }
    public function removeTags(Request $request)
    {
        $page = $request->page;
        if (!isset($page)) {
            return 'enter page ';
        }
        $posts = Posts::limit(10000)->offset($page * 10000)->orderBy('id', 'DESC')->get();
        foreach ($posts as $post) {
            $post = Posts::where('id', $post->id)->update(['text' => strip_tags($post->text)]);
        }
    }

    private function getAlltimeTrendings($new = false)
    {

        $already_posted_ids = ['posts_ids' => session('loaded_posts_ids'), 'discussions_ids' => session('loaded_discussion_ids'), 'trips_ids' => session('loaded_trips_ids'), 'travlogs_ids' => session('loaded_travlog_ids')];
        $topWeeklyPost = [];
        $chosenType = ['report' => 'report', 'discussion' => 'discussion', 'post' => 'post', 'trip' => 'trip'];
        $chosenType = array_rand($chosenType);
        $final_outcome = getDesiredLanguage();
        if (isset($final_outcome[0])) {
            $final_outcome = $final_outcome[0];
        } else {
            $final_outcome = UsersContentLanguages::DEFAULT_LANGUAGES;
        }

        if (!$new) {
            if ($chosenType == 'post') {
                $whereCondition = '';
                if (!empty($already_posted_ids['posts_ids']))
                    $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['posts_ids']) . ')';


                $watchedCheck = $this->getWatchedPost('post', 180);
                $whereWatched = '';
                if (!empty($watchedCheck))
                    $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';
                $topWeeklyPost['trending_post']  = DB::select(DB::raw('
                                                                    SELECT
                                                        t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular,l.counts as likes,s.counts as shares,v.counts as comments
                                                        FROM
                                                        `posts` AS t
                                                        LEFT JOIN
                                                        (SELECT
                                                            likes.`posts_id` AS r_id,
                                                            COUNT(*) AS counts
                                                            FROM
                                                            `posts_likes` AS likes
                                                            GROUP BY likes.posts_id) AS l
                                                            ON l.r_id = t.id
                                                        LEFT JOIN
                                                        (SELECT
                                                            shares.`posts_type_id` AS r_id,
                                                            COUNT(*) AS counts
                                                            FROM
                                                            `posts_shares` AS shares
                                                            where type = "text"
                                                            GROUP BY shares.posts_type_id) AS s
                                                            ON s.r_id = t.id
                                                        LEFT JOIN
                                                        (SELECT
                                                            views.`posts_id` AS r_id,
                                                            COUNT(*) AS counts
                                                            FROM
                                                            `posts_comments` AS views
                                                            where type= "post"
                                                            GROUP BY views.posts_id) AS v
                                                            ON v.r_id = t.id
                                                        WHERE deleted_at IS NULL
                                                        and Date(t.created_at) > "2019-01-01"
                                                            ' . $whereCondition . '
                                                            ' . $whereWatched . '
                                                            and language_id  = ' . $final_outcome . '
                                                            AND 
                                                            (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
                                                            > 5   and  t.users_id <> ' . Auth::user()->id . ' 
                                                        GROUP BY t.id
                                                        ORDER BY popular DESC
                                                        LIMIT 1
                '));
                if (empty($topWeeklyPost['trending_post'])) {

                    $topWeeklyPost['trending_post']  = DB::select(DB::raw('
                                        SELECT
                            t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular,l.counts as likes,s.counts as shares,v.counts as comments
                            FROM
                            `posts` AS t
                            LEFT JOIN
                            (SELECT
                                likes.`posts_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `posts_likes` AS likes
                                GROUP BY likes.posts_id) AS l
                                ON l.r_id = t.id
                            LEFT JOIN
                            (SELECT
                                shares.`posts_type_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `posts_shares` AS shares
                                where type = "text"
                                GROUP BY shares.posts_type_id) AS s
                                ON s.r_id = t.id
                            LEFT JOIN
                            (SELECT
                                views.`posts_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `posts_comments` AS views
                                where type= "post"
                                GROUP BY views.posts_id) AS v
                                ON v.r_id = t.id
                            WHERE deleted_at IS NULL
                            and Date(t.created_at) > "2019-01-01"
                                ' . $whereCondition . '
                                ' . $whereWatched . '
                                and language_id  =1
                                AND 
                                (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
                                > 5   and  t.users_id <> ' . Auth::user()->id . ' 
                            GROUP BY t.id
                            ORDER BY popular DESC
                            LIMIT 1
                        '));
                }
            } else if ($chosenType == 'discussion') {
                $whereCondition = '';
                if (!empty($already_posted_ids['discussions_ids']))
                    $whereCondition = ' and  discussions_id not in (' . implode(',', @$already_posted_ids['discussions_ids']) . ')';

                $watchedCheck = $this->getWatchedPost('discussion', 180);
                $whereWatched = '';
                if (!empty($watchedCheck))
                    $whereWatched = 'and discussions_id not in (' . implode(',', @$watchedCheck) . ')';
                $topWeeklyPost['trending_discussion'] = DB::select(DB::raw('SELECT discussions_id as id FROM `discussion_replies` left join discussions on discussions.id = discussion_replies.discussions_id where  language_id  = ' . $final_outcome . ' and  Date(discussions.created_at) > "2019-01-01"   ' . $whereCondition . ' ' . $whereWatched . '    GROUP BY discussions_id HAVING
                COUNT(discussions_id) >= 5 ORDER BY COUNT(discussions_id) DESC LIMIT 1'));
                if ($topWeeklyPost['trending_discussion']) {
                    $topWeeklyPost['trending_discussion'] = DB::select(DB::raw('SELECT discussions_id as id FROM `discussion_replies` left join discussions on discussions.id = discussion_replies.discussions_id where  language_id  = 1 and  Date(discussions.created_at) > "2019-01-01"   ' . $whereCondition . '  ' . $whereWatched . '   GROUP BY discussions_id HAVING
                    COUNT(discussions_id) >= 5 ORDER BY COUNT(discussions_id) DESC LIMIT 1'));
                }
            } else if ($chosenType == 'trip') {

                $whereCondition = '';
                if (!empty($already_posted_ids['trips_ids']))
                    $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['trips_ids']) . ')';
                $myTrips = get_triplist4me();
                $additionalWhere = '';
                if (!empty($myTrips))
                    $whereCondition = 'and t.id in (' . implode(',', $myTrips) . ')';

                $watchedCheck = $this->getWatchedPost('trip', 180);
                $whereWatched = '';
                if (!empty($watchedCheck))
                    $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';
                $topWeeklyPost['trending_tripplan'] = DB::select(DB::raw('
                            SELECT
                            t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0)+ IFNULL(k.counts, 0) )  AS popular
                            FROM
                            `trips` AS t
                            LEFT JOIN
                            (SELECT
                                likes.`trips_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `trips_likes` AS likes
                                GROUP BY likes.trips_id) AS l
                                ON l.r_id = t.id
                            LEFT JOIN
                            (SELECT
                                shares.`posts_type_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `posts_shares` AS shares
                                where type ="trip"
                                GROUP BY shares.posts_type_id) AS s
                                ON s.r_id = t.id
                            LEFT JOIN
                            (SELECT
                                views.`posts_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `posts_comments` AS views
                                where type = "trip"
                                GROUP BY views.posts_id) AS v
                                ON v.r_id = t.id
                            LEFT JOIN
                            (SELECT
                                tp_views.`trips_id` AS r_id,
                                COUNT(trip_place_id) AS counts
                                FROM
                                `trips_places` AS tp_views LEFT JOIN
                                trips_places_comments tps  ON  tps.`trip_place_id` =  tp_views.`id`
                                GROUP BY tp_views.trips_id) as k
                                ON k.r_id = t.id
                            WHERE deleted_at IS NULL
                                ' . $whereCondition . '
                                ' . $additionalWhere . '
                                ' . $whereWatched . '
                                and language_id  = ' . $final_outcome . '
                                and Date(t.created_at) > "2020-01-01"
                                AND 
                                (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0)+ IFNULL(k.counts, 0))
                                > 5 and  t.users_id <> ' . Auth::user()->id . ' 
                            GROUP BY t.id
                            ORDER BY popular DESC
                            LIMIT 1
                '));
                if (empty($topWeeklyPost['trending_tripplan'])) {
                    $topWeeklyPost['trending_tripplan'] = DB::select(DB::raw('
                    SELECT
                    t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) + IFNULL(k.counts, 0))  AS popular
                    FROM
                    `trips` AS t
                    LEFT JOIN
                    (SELECT
                        likes.`trips_id` AS r_id,
                        COUNT(*) AS counts
                        FROM
                        `trips_likes` AS likes
                        GROUP BY likes.trips_id) AS l
                        ON l.r_id = t.id
                    LEFT JOIN
                    (SELECT
                        shares.`posts_type_id` AS r_id,
                        COUNT(*) AS counts
                        FROM
                        `posts_shares` AS shares
                        where type ="trip"
                        GROUP BY shares.posts_type_id) AS s
                        ON s.r_id = t.id
                    LEFT JOIN
                    (SELECT
                        views.`posts_id` AS r_id,
                        COUNT(*) AS counts
                        FROM
                        `posts_comments` AS views
                        where type = "trip"
                        GROUP BY views.posts_id) AS v
                        ON v.r_id = t.id
                    LEFT JOIN
                    (SELECT
                        tp_views.`trips_id` AS r_id,
                        COUNT(trip_place_id) AS counts
                        FROM
                        `trips_places` AS tp_views LEFT JOIN
                        trips_places_comments tps  ON  tps.`trip_place_id` =  tp_views.`id`
                        GROUP BY tp_views.trips_id) as k
                        ON k.r_id = t.id
                    WHERE deleted_at IS NULL
                        ' . $whereCondition . '
                        ' . $additionalWhere . '
                        ' . $whereWatched . '
                        and language_id  =1
                        and Date(t.created_at) > "2020-01-01"
                        AND 
                        (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0)+ IFNULL(k.counts, 0))
                        > 5 and  t.users_id <> ' . Auth::user()->id . ' 
                    GROUP BY t.id
                    ORDER BY popular DESC
                    LIMIT 1
                    '));
                }
            } else if ($chosenType == 'report') {
                $whereCondition = '';
                if (!empty($already_posted_ids['travlogs_ids']))
                    $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['travlogs_ids']) . ')';
                $watchedCheck = $this->getWatchedPost('travlog', 180);
                $whereWatched = '';
                if (!empty($watchedCheck))
                    $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';
                $topWeeklyPost['trending_report'] = DB::select(DB::raw('
                            SELECT
                        t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
                        FROM
                        `reports` AS t
                        LEFT JOIN
                        (SELECT
                            likes.`reports_id` AS r_id,
                            COUNT(*) AS counts
                            FROM
                            `reports_likes` AS likes
                            GROUP BY likes.reports_id) AS l
                            ON l.r_id = t.id
                        LEFT JOIN
                        (SELECT
                            shares.`posts_type_id` AS r_id,
                            COUNT(*) AS counts
                            FROM
                            `posts_shares` AS shares
                            where type ="report"
                            GROUP BY shares.posts_type_id) AS s
                            ON s.r_id = t.id
                        LEFT JOIN
                        (SELECT
                            views.`posts_id` AS r_id,
                            COUNT(*) AS counts
                            FROM
                            `posts_comments` AS views
                            where type="report"
                            GROUP BY views.posts_id) AS v
                            ON v.r_id = t.id
                        WHERE 
                            EXISTS(SELECT * FROM reports_infos WHERE reports_infos.reports_id=t.id and var="cover") 
                            AND deleted_at IS NULL
                            ' . $whereCondition . '
                             ' . $whereWatched . '
                            and language_id  = ' . $final_outcome . '
                            and Date(t.created_at) > "2019-01-01"
                            and published_date IS NOT NULL
                            AND 
                            (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
                            > 5 and  t.users_id <> ' . Auth::user()->id . ' 
                        GROUP BY t.id
                        ORDER BY popular DESC
                        LIMIT 1
                '));
                if (empty($topWeeklyPost['trending_report'])) {
                    $topWeeklyPost['trending_report'] = DB::select(DB::raw('
                                SELECT
                            t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
                            FROM
                            `reports` AS t
                            LEFT JOIN
                            (SELECT
                                likes.`reports_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `reports_likes` AS likes
                                GROUP BY likes.reports_id) AS l
                                ON l.r_id = t.id
                            LEFT JOIN
                            (SELECT
                                shares.`posts_type_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `posts_shares` AS shares
                                where type ="report"
                                GROUP BY shares.posts_type_id) AS s
                                ON s.r_id = t.id
                            LEFT JOIN
                            (SELECT
                                views.`posts_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `posts_comments` AS views
                                where type="report"
                                GROUP BY views.posts_id) AS v
                                ON v.r_id = t.id
                            WHERE 
                                deleted_at IS NULL
                                ' . $whereCondition . '
                                ' . $whereWatched . '
                                and language_id  = 1
                                and Date(t.created_at) > "2020-01-01"
                                and published_date IS NOT NULL
                                AND EXISTS(SELECT * FROM reports_infos WHERE reports_infos.reports_id=t.id and var="cover") 
                                AND 
                                (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
                                > 5 and  t.users_id <> ' . Auth::user()->id . ' 
                            GROUP BY t.id
                            ORDER BY popular DESC
                            LIMIT 1
                    '));
                }
            }
        } else {
            $Type = ['trip' => 'trip', 'post' => 'post', 'report' => 'report', 'discussion' => 'discussion'];
            foreach ($Type as $chosenType) {
                if ($chosenType == 'post') {
                    $whereCondition = '';
                    if (!empty($already_posted_ids['posts_ids']))
                        $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['posts_ids']) . ')';
                    $watchedCheck = $this->getWatchedPost('post', 180);
                    $whereWatched = '';
                    if (!empty($watchedCheck))
                        $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';
                    $topWeeklyPost['trending_post']  = DB::select(DB::raw('
                                                                        SELECT
                                                            t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
                                                            FROM
                                                            `posts` AS t
                                                            LEFT JOIN
                                                            (SELECT
                                                                likes.`posts_id` AS r_id,
                                                                COUNT(*) AS counts
                                                                FROM
                                                                `posts_likes` AS likes
                                                                GROUP BY likes.posts_id) AS l
                                                                ON l.r_id = t.id
                                                            LEFT JOIN
                                                            (SELECT
                                                                shares.`posts_type_id` AS r_id,
                                                                COUNT(*) AS counts
                                                                FROM
                                                                `posts_shares` AS shares
                                                                where type = "text"
                                                                GROUP BY shares.posts_id) AS s
                                                                ON s.r_id = t.id
                                                            LEFT JOIN
                                                            (SELECT
                                                                views.`posts_id` AS r_id,
                                                                COUNT(*) AS counts
                                                                FROM
                                                                `posts_comments` AS views
                                                                where type = "post"
                                                                GROUP BY views.posts_id) AS v
                                                                ON v.r_id = t.id
                                                            WHERE deleted_at IS NULL
                                                            and Date(t.created_at) > "2019-01-01"

                                                                ' . $whereCondition . '
                                                                ' . $whereWatched . '
                                                                and language_id  = ' . $final_outcome . '
                                                                AND 
                                                                (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
                                                                > 5   and  t.users_id <> ' . Auth::user()->id . ' 
                                                            GROUP BY t.id
                                                            ORDER BY popular DESC
                                                            LIMIT 1
                    '));
                } else if ($chosenType == 'discussion') {
                    $whereCondition = '';
                    if (!empty($already_posted_ids['discussions_ids']))
                        $whereCondition = 'and discussions_id not in (' . implode(',', @$already_posted_ids['discussions_ids']) . ')';
                    $watchedCheck = $this->getWatchedPost('discussion', 180);
                    $whereWatched = '';
                    if (!empty($watchedCheck))
                        $whereWatched = 'and discussions_id not in (' . implode(',', @$watchedCheck) . ')';
                    $topWeeklyPost['trending_discussion'] = DB::select(DB::raw('SELECT discussions_id as id FROM `discussion_replies` left join discussions on discussions.id = discussion_replies.discussions_id where language_id = ' . $final_outcome . ' ' . $whereWatched . ' and Date(discussions.created_at) > "2019-01-01"  ' . $whereCondition . '   GROUP BY discussions_id HAVING
                    COUNT(discussions_id) >= 5 ORDER BY COUNT(discussions_id) DESC LIMIT 1'));
                } else if ($chosenType == 'trip') {

                    $whereCondition = '';
                    if (!empty($already_posted_ids['trips_ids']))
                        $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['trips_ids']) . ')';
                    $myTrips = get_triplist4me();
                    $additionalWhere = '';
                    if (!empty($myTrips))
                        $additionalWhere = 'and t.id  in (' . implode(',', @$myTrips) . ')';
                    $watchedCheck = $this->getWatchedPost('trip', 180);
                    $whereWatched = '';
                    if (!empty($watchedCheck))
                        $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';
                    $topWeeklyPost['trending_tripplan'] = DB::select(DB::raw('
                                SELECT
                                t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
                                FROM
                                `trips` AS t
                                LEFT JOIN
                                (SELECT
                                    likes.`trips_id` AS r_id,
                                    COUNT(*) AS counts
                                    FROM
                                    `trips_likes` AS likes
                                    GROUP BY likes.trips_id) AS l
                                    ON l.r_id = t.id
                                LEFT JOIN
                                (SELECT
                                    shares.`posts_type_id` AS r_id,
                                    COUNT(*) AS counts
                                    FROM
                                    `posts_shares` AS shares
                                    where type = "trip"
                                    GROUP BY shares.posts_type_id) AS s
                                    ON s.r_id = t.id
                                LEFT JOIN
                                (SELECT
                                    views.`posts_id` AS r_id,
                                    COUNT(*) AS counts
                                    FROM
                                    `posts_comments` AS views
                                    where type ="trip"
                                    GROUP BY views.posts_id) AS v
                                    ON v.r_id = t.id
                                WHERE deleted_at IS NULL
                                    ' . $whereCondition . '
                                    ' . $additionalWhere . '
                                    ' . $whereWatched . '
                                    and language_id  = ' . $final_outcome . '
                                    and Date(t.created_at) > "2019-01-01"
                                    AND 
                                    (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
                                    > 5 and  t.users_id <> ' . Auth::user()->id . ' 
                                GROUP BY t.id
                                ORDER BY popular DESC
                                LIMIT 1
                    '));
                } else if ($chosenType == 'report') {
                    $whereCondition = '';
                    if (!empty($already_posted_ids['travlogs_ids']))
                        $whereCondition = 'and t.id not in (' . implode(',', @$already_posted_ids['travlogs_ids']) . ')';
                    $watchedCheck = $this->getWatchedPost('travlog', 180);
                    $whereWatched = '';
                    if (!empty($watchedCheck))
                        $whereWatched = 'and t.id not in (' . implode(',', @$watchedCheck) . ')';
                    $topWeeklyPost['trending_report'] = DB::select(DB::raw('
                                SELECT
                            t.id, SUM( IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0) )  AS popular
                            FROM
                            `reports` AS t
                            LEFT JOIN
                            (SELECT
                                likes.`reports_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `reports_likes` AS likes
                                GROUP BY likes.reports_id) AS l
                                ON l.r_id = t.id
                            LEFT JOIN
                            (SELECT
                                shares.`posts_type_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `posts_shares` AS shares
                                where type ="report"
                                GROUP BY shares.posts_type_id) AS s
                                ON s.r_id = t.id
                            LEFT JOIN
                            (SELECT
                                views.`posts_id` AS r_id,
                                COUNT(*) AS counts
                                FROM
                                `posts_comments` AS views
                                where type= "report"
                                GROUP BY views.posts_id) AS v
                                ON v.r_id = t.id
                            WHERE deleted_at IS NULL
                                ' . $whereCondition . '
                                ' . $whereWatched . '
                                and language_id  = ' . $final_outcome . '
                                and Date(t.created_at) > "2019-01-01"
                                and published_date IS NOT NULL
                                AND EXISTS(SELECT * FROM reports_infos WHERE reports_infos.reports_id=t.id and var="cover")
                                AND 
                                (IFNULL(l.counts, 0) + IFNULL(s.counts, 0) + IFNULL(v.counts, 0))
                                > 5 and  t.users_id <> ' . Auth::user()->id . ' 
                            GROUP BY t.id
                            ORDER BY popular DESC
                            LIMIT 1
                    '));
                }
            }
        }
        return  $topWeeklyPost;
    }
    public function setContentsLanguage(Request $request)
    {
        $page = $request->page;
        $type = $request->type;
        if ($type == 'report') {
            $reports = Reports::offset($page * 500)->limit(500)->get();

            foreach ($reports as $report) {
                $translationService = new TranslationService();
                $text = $report['title'];
                $language_id = $translationService->getLanguageId($text);
                $report->language_id = isset($language_id) ? $language_id : 1;
                $report->Save();
            }
        } else if ($type == 'post') {
            $reports = Posts::offset($page * 500)->limit(500)->get();

            foreach ($reports as $report) {
                $translationService = new TranslationService();
                $text = $report['text'];
                $language_id = $translationService->getLanguageId($text);
                $report->language_id = isset($language_id) ? $language_id : 1;
                $report->Save();
            }
        } else if ($type == 'discussion') {
            $reports = Discussion::offset($page * 500)->limit(500)->get();

            foreach ($reports as $report) {
                $translationService = new TranslationService();
                $text = $report['question'];
                $language_id = $translationService->getLanguageId($text);
                $report->language_id = isset($language_id) ? $language_id : 1;
                $report->Save();
            }
        } else if ($type == 'trip') {
            $reports = TripPlans::offset($page * 500)->limit(500)->get();

            foreach ($reports as $report) {
                $translationService = new TranslationService();
                $text = $report['title'];
                $language_id = $translationService->getLanguageId($text);
                $report->language_id = isset($language_id) ? $language_id : 1;
                $report->Save();
            }
        }
        dd('donee');
    }
    public function addLikeEngagement(Request $request)
    {
        $users = [];
        if (($handle = fopen(public_path("citiescsv/users.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $users[] = utf8_decode(utf8_encode($data[0]));
            }
        }
        $users = User::Where(function ($query) use ($users) {
            for ($i = 0; $i < count($users); $i++) {
                $query->orwhere('email', 'like',  '%' . $users[$i] . '%');
            }
        })->pluck('id')->toArray();
        $page = $request->page;
        $type = $request->type;


        if ($type == 'post') {

            $posts = Posts::where('language_id', 4)->offset($page * 1000)->limit(1000)->pluck('id');
            $bulkdata = [];
            foreach ($posts as $post) {
                $bulkdata = [];
                shuffle($users);
                $temp_users = array_slice($users, 20);
                foreach ($temp_users as $user) {
                    $bulkdata[] = ['users_id' => $user, 'posts_id' => $post];
                }
                PostsLikes::insert($bulkdata);
            }
        } elseif ($type == 'report') {
            $posts = Reports::where('language_id', 4)->offset($page * 1000)->limit(1000)->pluck('id');

            foreach ($posts as $post) {
                $bulkdata = [];
                shuffle($users);
                $temp_users = array_slice($users, 20);
                foreach ($temp_users as $user) {
                    $bulkdata[] = ['users_id' => $user, 'reports_id' => $post, 'created_at' => gmdate('Y-m-d H:m:s'), 'updated_at' => gmdate('Y-m-d H:m:s')];
                }
                ReportsLikes::insert($bulkdata);
            }
        } elseif ($type == 'discussion') {
            $posts = Discussion::where('language_id', 4)->offset($page * 1000)->limit(1000)->pluck('id');
            $bulkdata = [];
            foreach ($posts as $post) {
                $bulkdata = [];
                shuffle($users);
                $temp_users = array_slice($users, 20);
                foreach ($temp_users as $user) {
                    $bulkdata[] = ['users_id' => $user, 'discussions_id' => $post, 'vote' => 1];
                }
                DiscussionLikes::insert($bulkdata);
            }
        } elseif ($type == 'trip') {
            $posts = TripPlans::where('language_id', 4)->offset($page * 1000)->limit(1000)->pluck('id');
            $bulkdata = [];
            foreach ($posts as $post) {
                $bulkdata = [];
                shuffle($users);
                $temp_users = array_slice($users, 20);
                foreach ($temp_users as $user) {
                    $bulkdata[] = ['users_id' => $user, 'trips_id' => $post];
                }
                TripsLikes::insert($bulkdata);
            }
        }
        dd('page done ' . $page);
    }

    /**
     * remove content based on following critereia
     * discussion with thitle and description withe test keyword and equal or less then 2 words 
     * post with test words 
     * trip plan with test title
     * report just like above criteria
     * 
     */
    public function removeJunkContent(Request $request)
    {

        $type = $request->type;

        $page = $request->page;

        $match_string = ' test ';
        $dump_ids = [];
        $test_Array  = [];
        if ($type == 'post') {

            // where text contains test or testing only 
            $posts = Posts::where('text', 'like', '%test%')->get();
            foreach ($posts as $post) {
                $testcreation = ' ' . $post->text . ' ';
                if (strpos(strtolower($testcreation), $match_string) !== false || strpos(strtolower($testcreation), '>test<') !== false || strpos(strtolower($testcreation), 'testing') !== false || strpos(strtolower($testcreation), '>testing<') !== false)
                    $dump_ids[] = $post->id;
                else
                    $test_Array[$post->id] = $post->text;
            }
            $ps = ActivityLog::whereIn('variable', $dump_ids)->where('type', 'Post')->delete();
            $detled  = Posts::whereIn('id', $dump_ids)->delete();

            //whhere text contains less then 2 words and test  and no image
            // $dump_ids = [];
            // $test_Array  = [];
            // $posts = Posts::whereRaw('length(text)<20')->whereNotNull('text')->whereRaw('text <> "" ')->get();
            // foreach($posts as $post ){
            //     if(count($post->medias) ==0)
            //         $test_Array[$post->id] = $post->text;
            // }
            // dd($test_Array );
        }
        if ($type == 'trip') {
            $tps = Tripplans::where('title', 'like', '%test%')->get();
            foreach ($tps as $post) {
                $testcreation = ' ' . $post->title . ' ';
                if (strpos(strtolower($testcreation), $match_string) !== false || strpos(strtolower($testcreation), '>test<') !== false || strpos(strtolower($testcreation), 'testing') !== false || strpos(strtolower($testcreation), '>testing<') !== false)
                    $test_Array[] = $post->id;
            }
            $ps = ActivityLog::whereIn('variable', $test_Array)->where('type', 'Trip')->delete();
            $detled  = Tripplans::whereIn('id', $test_Array)->delete();
            dd($test_Array);
        }
        if ($type == 'discussion') {
            $data = Discussion::whereRaw(" question regexp '^[^ ]+$' and description regexp '^[^ ]+$'")->get();
            foreach ($data as $post) {
                $test_Array[] = $post->id;
            }
            $ps = ActivityLog::whereIn('variable', $test_Array)->where('type', 'Discussion')->delete();
            $detled  = Discussion::whereIn('id', $test_Array)->delete();
            dd($detled);
        }
        if ($type == 'report') {
            $data = Reports::whereRaw(" (title regexp '^[^ ]+$' and description regexp '^[^ ]+$') or title like  '%test%'")->get();
            foreach ($data as $post) {
                $test_Array[] = $post->id;
            }

            $ps = ActivityLog::whereIn('variable', $test_Array)->where('type', 'Report')->delete();
            $detled  = Reports::whereIn('id', $test_Array)->delete();
            dd($detled);
        }
    }
    public function checkTriplanEngagement(Request $reqeust)
    {
        $conditions = [
            ['type' => 'post', 'action' => ['publish']],
            // ['type' => 'event', 'action' => ['show']],
            ['type' => 'other', 'action' => ['show']],
            // ['type' => 'travelmate', 'action' => ['join', 'request']],
            // ['type' => 'tripplan', 'action' => ['publish']],
            // ['type' => 'user', 'action' => ['follow']],
            // ['type' => 'country', 'action' => ['follow']],
            // ['type' => 'city', 'action' => ['follow']],
            ['type' => 'place', 'action' => ['review']],
            ['type' => 'hotel', 'action' => ['review']],
            ['type' => 'discussion', 'action' => ['create']],
            ['type' => 'trip', 'action' => ['create', 'plan_updated']],
            ['type' => 'report', 'action' => ['create']],
            ['type' => 'trips_medias_shares', 'action' => ['plan_media_shared']],
            ['type' => 'trips_places_shares', 'action' => ['plan_step_shared']],
            // ['type' => 'tripplacescomments', 'action' => ['plan_step_comment']],
            ['type' => 'report', 'action' => ['update']],
            ['type' => 'share', 'action' => [
                'review',
                'event',
                'trip',
                'discussion',
                'text',
                'report'
            ]]
        ];

        $condition_items = [];
        foreach ($conditions as $condition) {
            foreach ($condition['action'] as $action) {
                if (strtolower($condition['type']) == "trip") {
                    $data_exists_condition = "AND EXISTS(SELECT * from trips WHERE id=variable and  deleted_at is null and EXISTS (SELECT * FROM `users` WHERE `trips`.`users_id` = `users`.`id` AND `users`.`deleted_at` IS NULL) AND EXISTS (SELECT * FROM `trips_places` WHERE `trips`.`id` = `trips_places`.`trips_id` AND `versions_id` = `trips`.active_version AND `deleted_at` IS NULL AND `trips_places`.`deleted_at` IS NULL))";
                    $condition_items[] = '(type = "' . $condition['type'] . '" and action = "' . $action . '" ' . $data_exists_condition . ')';
                } else {
                    $condition_items[] = '(type = "' . $condition['type'] . '" and action = "' . $action . '" )';
                }
            }
        }
        $where_condition = '';
        if (count($condition_items) > 0) {
            $where_condition .= '(' . implode(' or ', $condition_items) . ')';
        }
        $activity =  ActivityLog::selectRaw('SUBSTRING_INDEX (
            GROUP_CONCAT(id ORDER BY id DESC),
            ",",
            1
            ) AS `id`,
            SUBSTRING_INDEX (
            GROUP_CONCAT(users_id ORDER BY id DESC),
            ",",
            1
            ) AS `user_id`,
            SUBSTRING_INDEX (
            GROUP_CONCAT(type ORDER BY id DESC),
            ",",
            1
            ) AS `type`,
            SUBSTRING_INDEX (
            GROUP_CONCAT(action ORDER BY id DESC),
            ",",
            1
            ) AS `action`,
            SUBSTRING_INDEX (
            GROUP_CONCAT(variable ORDER BY id DESC),
            ",",
            1
            ) AS `variable`,
            SUBSTRING_INDEX (
            GROUP_CONCAT(time ORDER BY id DESC),
            ",",
            1
            ) AS `time`,
            SUBSTRING_INDEX (
            GROUP_CONCAT(permission ORDER BY id DESC),
            ",",
            1
            ) AS `permission`')
            ->groupBy('action')
            ->orderByRaw('CAST(SUBSTRING_INDEX (GROUP_CONCAT(id ORDER BY id DESC),",",1) AS UNSIGNED) DESC')
            ->limit(12)
            ->whereRaw($where_condition)
            // ->where('users_id', '<>', Auth::user()->id)
            ->get();
        dd($activity);
    }
    public function getGoogleNamesFromCSV()
    {
        $cities = [];
        if (($handle = fopen(public_path("citiescsv/repeat_city.csv"), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $cities[] = $data;
            }
        }
        $resultant = [];
        foreach ($cities as $city) {
            $google_link_for_city = 'https://maps.googleapis.com/maps/api/place/textsearch/json?' . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q&query=' . urlencode($city[0] . ' City in Saudi Arabia');
            $fetch_link_for_city = file_get_contents($google_link_for_city);
            $query_result_for_city = json_decode($fetch_link_for_city);
            $qr_city = $query_result_for_city->results;
            if (isset($qr_city[0]->name)) {
                $resultant[] = ['orginal_name' => $city, 'google name' => $qr_city[0]->name, 'type' => @$qr_city[0]->types[0]];
            }
        }
        dd($resultant);
    }
    public function updateTopPlaceSort(Request $request)
    {
        $page = $request->page;
        if (!isset($page)) {
            return 'enter page';
        }
        $tp = PlacesTop::orderBy('rating', 'DESC')->orderBy('reviews_num', 'DESC')->get();
        $final_list = [];
        foreach ($tp  as $top) {
            $final_list[$top->city_id][] = $top;
        }
        $final_list = array_chunk($final_list, 160);
        foreach ($final_list[$page]  as $key => $places) {
            $sort = 1;
            foreach ($places as $place) {
                $place->order_by_city = $sort;
                $place->save();
                $sort++;
            }
        }
        dd('done page ' . $page);
    }
    public function checkWatchhistory()
    {

        $user  = UserWatchHistory::all();
        dd($user);
    }


    public function checkCronStatus()
    {
        // $media = Media::whereIn(DB::raw('lower(RIGHT(url, 3))'), ["jpeg", "jpg", "png", "gif", "tiff", "heic", "mp4", "mov", "ogg", "wmv"])->whereNull('type')->whereNotNull('url');
        // $discussion =  Discussion::whereHas('author')->whereNull('language_id');
        // $report =  Reports::whereHas('author')->whereNull('language_id');
        // $tripPlan =  TripPlans::whereHas('author')->whereNull('language_id');
        // $post = Posts::whereHas('author')->whereNull('language_id')->whereNull('text');
        // $reportsInfo = ReportsInfos::whereHas('report')->whereIn('var', [ReportsService::VAR_TEXT, ReportsService::VAR_INFO])->whereNull('language_id')->whereNotNull('val');
        // return AjaxResponse::create([
        //     'Media' => (request()->has('count')) ? $media->count() : $media->exists(),
        //     'Discussion' => (request()->has('count')) ? $discussion->count() : $discussion->exists(),
        //     'Reports' => (request()->has('count')) ? $report->count() : $report->exists(),
        //     'TripPlans' => (request()->has('count')) ? $tripPlan->count() : $tripPlan->exists(),
        //     'Posts' => (request()->has('count')) ? $post->count() : $post->exists(),
        //     'ReportsInfos' => (request()->has('count')) ? $reportsInfo->count() : $reportsInfo->exists(),
        //     'video_thumb' => Media::query() ->where('type', Media::TYPE_VIDEO)->whereNotNull('url')->whereNull('video_thumbnail_url')->count()
        // ]);

        $medias =  Media::query()
            ->select(['id', 'url', 'video_thumbnail_url', 'type', 'users_id', 'uploaded_at'])
            ->where('type', Media::TYPE_VIDEO)
            ->orderBy('id', 'ASC')
            ->get();

        return AjaxResponse::create([
            'total_videos' => $medias->count(),
            'pending_thumb' => $medias->where('video_thumbnail_url', NULL)->count(),
            'completed_thumb' => $medias->where('video_thumbnail_url', '!=', NULL)->count(),
            'medias' => $medias->toArray(),
        ]);
    }
}
