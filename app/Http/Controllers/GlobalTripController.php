<?php

namespace App\Http\Controllers;

use App\Services\Trips\TripInvitationsService;
use App\Services\Trips\TripsService;
use App\Services\Trips\GlobalTripsService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\TripPlans\TripPlans;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Country\ApiCountry as Country;
use App\Models\TripCities\TripCities;
use App\Models\TripPlaces\TripPlaces;
use App\Models\Place\Place;
use App\Models\TripCountries\TripCountries;
use App\Models\TripPlans\TripsVersions;
use App\Models\TripPlans\TripsDescriptions;
use App\Models\User\User;
use App\Models\Reports\Reports;
use App\Models\TravelMates\TravelMatesRequests;
use App\Models\TravelMates\TravelMatesWithoutPlan;
use App\Models\User\UsersContentLanguages;

class GlobalTripController extends Controller
{

    public function __construct()
    {
        //        $this->middleware('auth:user');
    }

    public function getIndex(Request $request, GlobalTripsService $globalTripsService)
    {
        $data = array();

        $top_trips = $globalTripsService->getTopTripPlans(true);

        if ($top_trips->count() == 0) {
            $top_trips = $globalTripsService->getTopTripPlans(false);
        }

        $data['top_trips'] = $top_trips;

        $trip_cities = $globalTripsService->getTrendingLocations(true);

        if ($trip_cities->count() == 0) {
            $trip_cities = $globalTripsService->getTrendingLocations(false);
        }

        foreach ($trip_cities as $trip_city) {
            $top_cities[] = [
                'city_info' => Cities::query()->with(['getMedias', 'transsingle'])->findOrFail($trip_city->cities_id),
                'count' => $trip_city->total_cities,
            ];
        }

        $data['top_cities'] = $top_cities;

        return view('site.globaltrips.list', $data);
    }


    /**
     * @param Request $request
     * @return View
     */
    public function getMoreTripPlan(Request $request)
    {
        $page = $request->pagenum;
        $skip = ($page - 1) * 10;
        $sort = $request->order;
        $filters = $request->filters;
        $searched_text = '';

        if (isset($filters['search_text'])) {
            $searched_text = $filters['search_text'];
        }

        $all_plans = $this->_getTripPlans($skip, 10, $filters, $sort);

        echo view('site.globaltrips.partials._trip-plan-block', array('all_plans' => $all_plans['plans'], 'searched_text' => $searched_text));
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getFilterTripPlan(Request $request)
    {
        $sort = ($request->order != '') ? $request->order : '';
        $return = '';
        $searched_text = '';

        $filters = $request->filters;
        $all_plans = $this->_getTripPlans('', 10, $filters, $sort);

        if (isset($filters['search_text'])) {
            $searched_text = $filters['search_text'];
        }

        $return .= view('site.globaltrips.partials._trip-plan-block', array('all_plans' => $all_plans['plans'], 'searched_text' => $searched_text));

        $count = $all_plans['count'];

        return new JsonResponse(['count' => $count, 'view' => $return]);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getSearchCountries(Request $request)
    {
        $query = $request->get('q');
        if ($query) {
            $countries = array();
            $queryParam = $query;
            $get_cuntries = Country::whereHas('transsingle', function ($query) use ($queryParam) {
                $query->where('title', 'like', "%" . $queryParam . "%");
            })->take(20)->get();
            foreach ($get_cuntries as $country) {
                $countries[] = [
                    'id' => $country->id,
                    'text' => $country->transsingle->title,
                    'image' => check_country_photo(@$country->getMedias[0]->url, 180),
                    'country_name' => null,
                    'query' => $query
                ];
            }

            return new JsonResponse($countries);
        }
    }


    /**
     * @param int $skip
     * @param int $take
     * @param arr $filters
     * @param string $sort
     * @return Array
     */
    protected function _getTripPlans($skip, $take, $filters, $sort = 'popular')
    {

        $query = TripPlans::query();
        $langugae_id = getDesiredLanguage();
        if (isset($langugae_id[0])) {
            $langugae_id = $langugae_id;
        } else {
            $langugae_id = [UsersContentLanguages::DEFAULT_LANGUAGES];
        }

        $authUserId = auth()->id();

        if (isset($filters['type']) && $filters['type'] == 'mine') {
            $query->where('users_id', $authUserId);
        } else {
            $tripInvitationsService = app(TripInvitationsService::class);

            $friends = $tripInvitationsService->findFriends();

            $query
                ->where('active', 1)
                ->whereIn('language_id',$langugae_id)
                ->where(function($q) use ($friends, $authUserId) {
                    $q->where(function($q) use ($friends) {
                        $q->whereIn('users_id', $friends);
                        $q->where('privacy', TripsService::PRIVACY_FRIENDS);
                    });
                    $q->orWhere('privacy', TripsService::PRIVACY_PUBLIC);
                    $q->orWhere('users_id', $authUserId);
                });
        }

        if (isset($filters['countries'])) {
            $countries = $filters['countries'];
            $query->whereHas('trips_places', function ($query) use ($countries) {
                $query->whereIn('countries_id', $countries);
            });
        }

        if (isset($filters['cities'])) {
            $cities = $filters['cities'];
            $query->whereHas('trips_places', function ($query) use ($cities) {
                $query->whereIn('cities_id', $cities);
            });
        }

        if (isset($filters['places'])) {
            $places = $filters['places'];
            $query->whereHas('trips_places', function ($query) use ($places) {
                $query->whereIn('places_id', $places);
            });
        }

        if (isset($filters['search_text'])) {
            $search_text = $filters['search_text'];
            $query->where(function ($q) use ($search_text) {
                $q->where('title', 'like', "%" . $search_text . "%")
                    ->orWhere('description', 'like', "%" . $search_text . "%")
                    ->orWhereHas('trips_places', function ($planQuery) use ($search_text) {
                        $planQuery->whereHas('place', function ($placeQuery) use ($search_text) {
                            $placeQuery->whereHas('transsingle', function ($transQuery) use ($search_text) {
                                $transQuery->where('title', 'like', "%" . $search_text . "%");
                            });
                        });
                    });
            });
        }

        $query->whereNotIn('users_id', blocked_users_list());

        $query->with('versions')->with(['version', 'trips_cities']);

        switch ($sort) {
            case 'popular':
                $query->select('*', DB::raw('(COALESCE(comment_count, 0) + COALESCE(steps_likes_count, 0) + COALESCE(steps_shares_count, 0) + COALESCE(medias_likes_count, 0) + COALESCE(medias_comments_count, 0) + COALESCE(medias_shares_count, 0) + COALESCE(sum(like_count), 0) + COALESCE(sum(shares_count), 0))  as avg_count'))
                    ->leftJoin(DB::raw('(SELECT `trips_id`, sum(c_count) as comment_count FROM `trips_places` LEFT JOIN (SELECT trip_place_id, count(*) as c_count FROM trips_places_comments GROUP by trip_place_id) as tps_comment ON tps_comment.trip_place_id=trips_places.id GROUP by trips_id) as tps'), function ($join) {
                        $join->on('trips.id', '=', 'tps.trips_id');
                    })
                    ->leftJoin(DB::raw('(SELECT trips_id, count(*) as like_count FROM trips_likes GROUP by trips_id) as tpl'), function ($join) {
                        $join->on('trips.id', '=', 'tpl.trips_id');
                    })
                    ->leftJoin(DB::raw('(SELECT trip_id, count(*) as shares_count FROM trips_shares GROUP by trip_id) as tsh'), function ($join) {
                        $join->on('trips.id', '=', 'tsh.trip_id');
                    })
                    ->leftJoin(DB::raw('(SELECT `trips_id`, sum(l_count) as steps_likes_count FROM `trips_places` LEFT JOIN (SELECT places_id, count(*) as l_count FROM trips_likes where places_id is not NULL GROUP by places_id) as tps_likes ON tps_likes.places_id=trips_places.id GROUP by trips_id) as tsl'), function ($join) {
                        $join->on('trips.id', '=', 'tsl.trips_id');
                    })
                    ->leftJoin(DB::raw('(SELECT `trips_id`, sum(tpsh_count) as steps_shares_count FROM `trips_places` LEFT JOIN (SELECT trip_place_id, count(*) as tpsh_count FROM trip_places_shares GROUP by trip_place_id) as tp_shares ON tp_shares.trip_place_id=trips_places.id GROUP by trips_id) as tssh'), function ($join) {
                        $join->on('trips.id', '=', 'tssh.trips_id');
                    })
                    ->leftJoin(DB::raw('(SELECT `trips_id`, sum(ml_count) as medias_likes_count FROM `trips_medias` LEFT JOIN (SELECT medias_id, count(*) as ml_count FROM medias_likes GROUP by medias_id) as tm_likes ON tm_likes.medias_id=trips_medias.medias_id GROUP by trips_id) as tml'), function ($join) {
                        $join->on('trips.id', '=', 'tml.trips_id');
                    })
                    ->leftJoin(DB::raw('(SELECT `trips_id`, sum(msh_count) as medias_shares_count FROM `trips_medias` LEFT JOIN (SELECT trip_media_id, count(*) as msh_count FROM trip_medias_shares GROUP by trip_media_id) as m_shares ON m_shares.trip_media_id=trips_medias.id GROUP by trips_id) as msh'), function ($join) {
                        $join->on('trips.id', '=', 'msh.trips_id');
                    })
                    ->leftJoin(DB::raw('(SELECT `trips_id`, sum(mc_count) as medias_comments_count FROM `trips_medias` LEFT JOIN (SELECT medias_id, count(*) as mc_count FROM medias_comments GROUP by medias_id) as tm_comments ON tm_comments.medias_id=trips_medias.medias_id GROUP by trips_id) as tmc'), function ($join) {
                        $join->on('trips.id', '=', 'tmc.trips_id');
                    })->groupBy('trips.id')->orderBy('avg_count', 'DESC');

                break;
            case 'oldest':
                $query->orderBy('created_at', 'asc');
                break;
            case 'upcoming':
                $query->whereHas('versions', function ($q) {
                    $q->where('start_date', '>=', Carbon::today());
                });
                break;
            default:
                $query->orderBy('created_at', 'desc');
        }

        $count = count($query->get());


        if ($skip != '') {
            $query->skip($skip);
        }
        if ($take != '') {
            $query->take($take);
        }

        $plans = $query->get();
        return ['count' => $count, 'plans' => $plans];
    }
}
