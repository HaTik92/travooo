<?php

namespace App\Http\Controllers;

use App\Http\Responses\AjaxResponse;
use App\Services\Ranking\RankingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

use App\Models\TripPlans\TripPlans;
use App\Models\Referral\ReferralLinks;
use App\Models\Posts\Posts;
use App\Models\Posts\PostsViews;
use App\Models\Posts\PostsComments;
use App\Models\Posts\PostsLikes;
use App\Models\Posts\PostsShares;
use App\Models\UsersFollowers\UsersFollowers;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsComments;
use App\Models\Reports\ReportsShares;
use App\Models\Reports\ReportsViews;
use App\Models\Reports\ReportsLikes;
use App\Models\Chat\ChatConversationMessage;
use App\Models\Notifications\Notifications;
use App\Services\Users\ExpertiseService;
use App\Models\Country\Countries;
use App\Services\Experts\DashboardService as ExpertDashboardService;

use Carbon\Carbon;

class DashboardController extends Controller
{

    /**
     * @var ExpertDashboardService
     */
    private $service;

    public function __construct(ExpertDashboardService $service)
    {
        $this->service = $service;
        $this->middleware('auth:user');
    }

    public function getIndex()
    {
        if (Auth::guard('user')->user()->expert == 0) {
            return abort(404);
        }

        $user_id = Auth::guard('user')->user()->id;
        $data['chat_count'] = ChatConversationMessage::where('to_id', $user_id)->where('from_id', '<>', $user_id)->where('is_read', false)->count();
        $data['notification_count'] = Notifications::where('users_id', $user_id)->where('read', 0)->count();

        return view('site.dashboard.index', $data);
    }

    public function ajaxGetMoreAffiliate(Request $request)
    {
        $page = $request->pagenum;
        $skip = ($page - 1) * 20;
        $order = $request->order;

        $my_links = ReferralLinks::getReferralLinks(20, $skip, $order);

        if (count($my_links) > 0) {
            return view('site.dashboard.partials._affiliate-block', ['my_links' => $my_links]);
        } else {
            return '';
        }
    }

    /**
     * @param Request $request
     * @return view
     */
    public function getOverview(Request $request)
    {
        $filter = $request->filter;

        echo  view('site.dashboard.partials._overview-block', $this->service->getOverview($filter));
    }


    /**
     * @param Request $request
     * @return view
     */
    public function getEngagement(Request $request)
    {
        $filter = $request->filter;
        $filter = $filter - 1;

        $data['engag_view'] = $this->_getEngagementByType('view', $filter);
        $data['engag_comment'] = $this->_getEngagementByType('comment', $filter);
        $data['engag_like'] = $this->_getEngagementByType('like', $filter);
        $data['engag_share'] = $this->_getEngagementByType('share', $filter);


        echo  view('site.dashboard.partials._engagement-block', $data);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getEngagementFilter(Request $request)
    {
        $filter = $request->filter;
        $filter = $filter - 1;
        $filter_type = $request->filter_type;

        switch ($filter_type) {
            case 'view-engagement':
                $engag_view = $this->_getEngagementByType('view', $filter);
                return new JsonResponse($engag_view);
                break;
            case 'comment-engagement':
                $comment_view = $this->_getEngagementByType('comment', $filter);
                return new JsonResponse($comment_view);
                break;
            case 'likes-engagement':
                $like_view = $this->_getEngagementByType('like', $filter);
                return new JsonResponse($like_view);
                break;
            case 'share-engagement':
                $share_view = $this->_getEngagementByType('share', $filter);
                return new JsonResponse($share_view);
                break;
        }
    }

    /**
     * @param Request $request
     * @return View
     */
    public function getEngagementDigInfo(Request $request)
    {
        $filter = $request->filter;
        $dig_type = $request->dig_type;
        $progress_color = '';

        $posts = Posts::where('users_id', Auth::guard('user')->user()->id)
            ->pluck('id');

        switch ($dig_type) {
            case 'Views':
                $dig_infos = PostsViews::select('*', DB::raw('count(*) as total'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('posts_id')->get();
                $progress_color = 'orange';
                break;
            case 'Comments':
                $dig_infos = PostsComments::select('*', DB::raw('count(*) as total'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('posts_id')->get();
                $progress_color = 'primary';
                break;
            case 'Likes':
                $dig_infos = PostsLikes::select('*', DB::raw('count(*) as total'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('posts_id')->get();
                $progress_color = 'red';
                break;
            case 'Shares':
                $dig_infos = PostsShares::select('*', DB::raw('count(*) as total'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('posts_id')->get();
                $progress_color = 'green';
                break;
        }

        return view('site.dashboard.partials._engagement-modal-block', ['dig_infos' => $dig_infos, 'dig_type' => $dig_type, 'progress_color' => $progress_color]);
    }


    /**
     * @param Request $request
     * @param RankingService $rankingService
     * @return view
     */
    public function getRanking(Request $request, RankingService $rankingService)
    {
        $user = auth()->user();

        $data = $rankingService->getProgressData($user->id);
        $data['user'] = $user;

        return view('site.dashboard.partials._ranking-block', $data);
    }

    public function ajaxApplyToBadge(Request $request, RankingService $rankingService)
    {
        $user = auth()->user();

        $progress = $rankingService->getProgressData($user->id);

        if (
            !$user->expert ||
            !$user->is_approved ||
            !$progress ||
            !$progress['has_cover'] ||
            !$progress['has_picture'] ||
            $progress['interactions_progress'] < 100 ||
            $progress['followers_progress'] < 100
        ) {
            return AjaxResponse::create([], false);
        }

        $user->apply_to_badge = 1;
        $user->save();

        return AjaxResponse::create();
    }

    /**
     * @param Request $request
     * @return view
     */
    public function getInteraction(Request $request)
    {
        $filter = $request->filter;
        $filter = $filter - 1;
        $interaction_men_ages = $interaction_women_ages = ['18-22' => 0, '23-30' => 0, '31-36' => 0, '37-48' => 0, '49-58' => 0, '59+' => 0];
        $user_ids  = $world_countries = $countries_list = [];
        $man_count = $woman_count = 0;
        $posts = Posts::where('users_id', Auth::guard('user')->user()->id)
            ->pluck('id');

        $post_comment_query = PostsComments::whereIn('posts_id', $posts)->with('author.country_of_nationality');
        $post_like_query = PostsLikes::whereIn('posts_id', $posts)->with('user.country_of_nationality');
        $post_shere_query = PostsShares::whereIn('posts_id', $posts)->with('author.country_of_nationality');

        $post_commant_users = $post_comment_query->groupBy('users_id')->get();
        foreach ($post_commant_users as $post_commant_user) {
            if (!in_array($post_commant_user->author->id, $user_ids)) {
                $user_age = get_users_age($post_commant_user->author->id);
                $user_ids[] = $post_commant_user->author->id;
                if ($post_commant_user->author->country_of_nationality) {
                    if (array_key_exists($post_commant_user->author->country_of_nationality->iso_code, $world_countries)) {
                        $world_countries[$post_commant_user->author->country_of_nationality->iso_code]++;
                    } else {
                        $world_countries[$post_commant_user->author->country_of_nationality->iso_code] = 1;
                    }
                }

                if ($user_age >= 18) {
                    if ($post_commant_user->author->gender == 1) {
                        $interaction_men_ages = $this->_getUserAges($interaction_men_ages, $user_age);
                        $man_count++;
                    } else {
                        $interaction_women_ages = $this->_getUserAges($interaction_women_ages, $user_age);
                        $woman_count++;
                    }
                }
            }
        }



        $post_like_users = $post_like_query->groupBy('users_id')->get();
        foreach ($post_like_users as $post_like_user) {
            if (!in_array($post_like_user->user->id, $user_ids)) {
                $user_age = get_users_age($post_like_user->user->id);
                $user_ids[] = $post_like_user->user->id;
                if ($post_like_user->user->country_of_nationality) {
                    if (array_key_exists($post_like_user->user->country_of_nationality->iso_code, $world_countries)) {
                        $world_countries[$post_like_user->user->country_of_nationality->iso_code]++;
                    } else {
                        $world_countries[$post_like_user->user->country_of_nationality->iso_code] = 1;
                    }
                }

                if ($user_age >= 18) {
                    if ($post_like_user->user->gender == 1) {
                        $interaction_men_ages = $this->_getUserAges($interaction_men_ages, $user_age);
                        $man_count++;
                    } else {
                        $interaction_women_ages = $this->_getUserAges($interaction_women_ages, $user_age);
                        $woman_count++;
                    }
                }
            }
        }

        $post_share_users = $post_shere_query->groupBy('users_id')->get();
        foreach ($post_share_users as $post_share_user) {
            if (!in_array($post_share_user->author->id, $user_ids)) {
                $user_age = get_users_age($post_share_user->author->id);
                $user_ids[] = $post_share_user->author->id;

                if ($post_share_user->author->country_of_nationality) {
                    if (array_key_exists($post_share_user->author->country_of_nationality->iso_code, $world_countries)) {
                        $world_countries[$post_share_user->author->country_of_nationality->iso_code]++;
                    } else {
                        $world_countries[$post_share_user->author->country_of_nationality->iso_code] = 1;
                    }
                }
                if ($user_age >= 18) {
                    if ($post_share_user->author->gender == 1) {
                        $interaction_men_ages = $this->_getUserAges($interaction_men_ages, $user_age);
                        $man_count++;
                    } else {
                        $interaction_women_ages = $this->_getUserAges($interaction_women_ages, $user_age);
                        $woman_count++;
                    }
                }
            }
        }

        $world_countries =  array_map(
            function ($key, $value) {
                return array($this->service->_convertCountryIsoCode($key), $value);
            },
            array_keys($world_countries),
            $world_countries
        );

        $data['man_count'] = $man_count;
        $data['woman_count'] = $woman_count;
        $data['total_count'] = $man_count + $woman_count;
        $data['interaction_men_ages'] = $interaction_men_ages;
        $data['interaction_women_ages'] = $interaction_women_ages;

        $location_info = $this->_getInteractionCountries($filter);

        $data['interaction_countries'] = $location_info['interaction_countries'];

        $data['world_countries'] = json_encode($world_countries);


        echo  view('site.dashboard.partials._interaction-source-block', $data);
    }


    /**
     * @param Request $request
     * @return view
     */
    public function getInteractionFilter(Request $request)
    {
        $filter = $request->filter;
        $filter = $filter - 1;
        $countries_view = $cities_view = '';
        $location_info = $this->_getInteractionCountries($filter);
        $interaction_countries = $location_info['interaction_countries'];

        foreach ($interaction_countries as $country) {
            $countries_view .= '<tr>
                        <td> <img class="flag" src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/' . $country['iso_code'] . '.png" style="width: 26px !important;" alt="flag"> ' . $country['country_name'] . '</td>
                        <td>' . (isset($country['engagements']) ? $country['engagements'] : 0) . '</td>
                        <td>' . (isset($country['comments']) ? $country['comments'] : 0) . '</td>
                        <td>' . (isset($country['likes']) ? $country['likes'] : 0) . '</td>
                        <td>' . (isset($country['shares']) ? $country['shares'] : 0) . '</td>
                    </tr>';
        }

        return new JsonResponse(['countries_view' => $countries_view]);
    }


    /**
     * @param Request $request
     * @return view
     */
    public function getPosts(Request $request)
    {
        $filter = $request->filter;
        $filter = $filter - 1;
        $type = $request->type;

        switch ($type) {
            case 'all':
                $data['summary'] = $this->_getPostsByType('summary', $filter);
                $posts_reports = $this->_getPostsByType('reports', $filter);
                ksort($posts_reports);
                $data['post_reports'] = $posts_reports;

                echo  view('site.dashboard.partials._posts-block', $data);
                break;
            case 'summary':
                $data['summary'] = $this->_getPostsByType('summary', $filter);

                echo  view('site.dashboard.partials._posts-summary-block', $data);
                break;
            case 'reports':
                $posts_reports = $this->_getPostsByType('reports', $filter);
                ksort($posts_reports);
                $data['post_reports'] = $posts_reports;

                echo  view('site.dashboard.partials._posts-reports-block', $data);
                break;
        }
    }

    /**
     * @param Request $request
     * @return view
     */
    public function getReports(Request $request)
    {
        $filter = $request->filter;
        $filter = $filter - 1;

        $reports_views = $reports_comments = $reports_shares = $reports_likes = $reports_ids = [];
        $reports = Reports::where('users_id', Auth::guard('user')->user()->id)
            ->pluck('id');

        $views = ReportsViews::whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
        $comments = ReportsComments::whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
        $shares = ReportsShares::whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
        $likes = ReportsLikes::whereIn('reports_id', $reports)->where('created_at', '>=', Carbon::now()->subDays($filter))->get();

        foreach ($views as $view) {
            if (!in_array($view->reports_id, $reports_ids)) {
                $reports_ids[] = $view->reports_id;
            }
            if (array_key_exists($view->created_at->format('Y-m-d'), $reports_views)) {
                $reports_views[$view->created_at->format('Y-m-d')]++;
            } else {
                $reports_views[$view->created_at->format('Y-m-d')] = 1;
            }
        }

        foreach ($comments as $comment) {
            if (!in_array($comment->reports_id, $reports_ids)) {
                $reports_ids[] = $comment->reports_id;
            }
            if (array_key_exists($comment->created_at->format('Y-m-d'), $reports_comments)) {
                $reports_comments[$comment->created_at->format('Y-m-d')]++;
            } else {
                $reports_comments[$comment->created_at->format('Y-m-d')] = 1;
            }
        }

        foreach ($shares as $share) {
            if (!in_array($share->reports_id, $reports_ids)) {
                $reports_ids[] = $share->reports_id;
            }
            if (array_key_exists($share->created_at->format('Y-m-d'), $reports_shares)) {
                $reports_shares[$share->created_at->format('Y-m-d')]++;
            } else {
                $reports_shares[$share->created_at->format('Y-m-d')] = 1;
            }
        }

        foreach ($likes as $like) {
            if (!in_array($like->reports_id, $reports_ids)) {
                $reports_ids[] = $like->reports_id;
            }
            if (array_key_exists($like->created_at->format('Y-m-d'), $reports_likes)) {
                $reports_likes[$like->created_at->format('Y-m-d')]++;
            } else {
                $reports_likes[$like->created_at->format('Y-m-d')] = 1;
            }
        }


        for ($day = $filter - 1; $day >= 0; $day--) {
            $reports_label[] = Carbon::now()->subDays($day)->format('d');
            if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $reports_views)) {
                $view_values[] = $reports_views[Carbon::now()->subDays($day)->format('Y-m-d')];
            } else {
                $view_values[] = 0;
            }

            if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $reports_comments)) {
                $comment_values[] = $reports_comments[Carbon::now()->subDays($day)->format('Y-m-d')];
            } else {
                $comment_values[] = 0;
            }

            if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $reports_shares)) {
                $share_values[] = $reports_shares[Carbon::now()->subDays($day)->format('Y-m-d')];
            } else {
                $share_values[] = 0;
            }

            if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $reports_likes)) {
                $like_values[] = $reports_likes[Carbon::now()->subDays($day)->format('Y-m-d')];
            } else {
                $like_values[] = 0;
            }
        }

        $last_view = ReportsViews::whereHas('repots', function ($q) use ($filter) {
            $q->where('users_id', Auth::guard('user')->user()->id);
        })->where('created_at', '>=', Carbon::now()->subDays($filter))->orderBy('created_at', 'DESC')->first();

        $all_reports = Reports::whereIn('id', $reports_ids)
            ->withCount(['views' => function ($q) use ($filter) {
                $q->where('created_at', '>=', Carbon::now()->subDays($filter));
            }])
            ->withCount(['comments' => function ($q) use ($filter) {
                $q->where('created_at', '>=', Carbon::now()->subDays($filter));
            }])
            ->withCount(['shares' => function ($q) use ($filter) {
                $q->where('created_at', '>=', Carbon::now()->subDays($filter));
            }])
            ->withCount(['likes' => function ($q) use ($filter) {
                $q->where('created_at', '>=', Carbon::now()->subDays($filter));
            }])->get();

        $data['reports_label'] = json_encode($reports_label);
        $data['reports_views'] = json_encode($view_values);
        $data['reports_comments'] = json_encode($comment_values);
        $data['reports_shares'] = json_encode($share_values);
        $data['reports_likes'] = json_encode($like_values);

        $data['all_reports'] = $all_reports;
        $data['last_view'] = $last_view;


        echo  view('site.dashboard.partials._reports-block', $data);
    }


    /**
     * @param Request $request
     * @return view
     */
    public function getFollowers(Request $request)
    {
        $filter = $request->filter;
        $filter = $filter - 1;

        $followers = $this->service->_getFollowByType('follow', $filter);
        $unfollowers = $this->service->_getFollowByType('unfollow', $filter);

        $data['followers'] = $followers;
        $data['unfollowers'] = $unfollowers;


        echo  view('site.dashboard.partials._followers-block', $data);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getFollowersFilter(Request $request)
    {
        $filter = $request->filter;
        $filter = $filter - 1;
        $filter_type = $request->filter_type;
        $view_list = '';
        switch ($filter_type) {
            case 'followers':
                $followers = $this->service->_getFollowByType('follow', $filter);
                $view_list .= view('site.dashboard.partials._follow-modal-block', ['follows' => $followers['followers_list'], 'type' => 'follow']);
                return new JsonResponse(['follow' => $followers, 'view_list' => $view_list]);
            case 'unfollowers':
                $unfollowers = $this->service->_getFollowByType('unfollow', $filter);
                $view_list .= view('site.dashboard.partials._follow-modal-block', ['follows' => $unfollowers['followers_list'], 'type' => 'unfollow']);
                return new JsonResponse(['follow' => $unfollowers, 'view_list' => $view_list]);
        }
    }

    /**
     * @param Request $request
     * @return view
     */
    public function getMoreFollowers(Request $request)
    {
        $filter = $request->filter;
        $filter = $filter - 1;
        $type = $request->type;
        $page = $request->pagenum;
        $skip = ($page - 1) * 10;

        switch ($type) {
            case 'followers':
                $followers_list = UsersFollowers::where('users_id', Auth::guard('user')->user()->id)->where('follow_type', 1)->where('created_at', '>=', Carbon::now()->subDays($filter))->skip($skip)->take(10)->get();
                echo  view('site.dashboard.partials._follow-modal-block', ['follows' => $followers_list]);
                break;
            case 'unfollowers':
                $followers_list = UsersFollowers::where('users_id', Auth::guard('user')->user()->id)->where('follow_type', 2)->where('created_at', '>=', Carbon::now()->subDays($filter))->skip($skip)->take(10)->get();
                echo  view('site.dashboard.partials._follow-modal-block', ['follows' => $followers_list]);
                break;
        }
    }


    /**
     * @param Request $request
     * @return view
     */
     public function getGlobalImpact(Request $request)
     {
        echo view('site.dashboard.partials._global-impact-block', $this->service->getGlobalImpact());
     }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getMoreLeader(Request $request)
    {
        $page = $request->pagenum;
        $skip = ($page - 1) * 10;
        $view_list = '';
        $expertise_country_lists = [];
        $filter_country_id = $request->country_id;

        $leader_badge_lists = collect();

        if (count($leader_badge_lists) > 0) {
            foreach ($leader_badge_lists as $leader_badge_list) {
                if ($leader_badge_list->point_author->expert == 1 && $leader_badge_list->point_author->nationality != 0) {
                    $expertise_country_lists[$leader_badge_list->point_author->nationality] = @$leader_badge_list->point_author->country_of_nationality->transsingle->title;
                }
            }

            $view_list .= view('site.dashboard.partials._global-impact-leader-block', ['leader_badge_lists' => $leader_badge_lists]);
        }

        return new JsonResponse(['expertise_country_lists' => $expertise_country_lists, 'view_list' => $view_list]);
    }


    /**
     * @param string $post_type
     * @param int $filter
     * @return array
     */
    protected function _getPostsByType($post_type, $filter)
    {
        $post_reports = [];
        $post_images_list = [];
        $post_videos_list = [];
        $post_texts = $this->service->_getOverviewByType('post_text', $filter, 'post');
        $post_images = $this->service->_getOverviewByType('post_image', $filter, 'post');
        $post_videos = $this->service->_getOverviewByType('post_video', $filter, 'post');
        $trip_plans = $this->service->_getOverviewByType('trip_plan', $filter, 'post');
        $reports = $this->service->_getOverviewByType('reports', $filter, 'post');
        $links = $this->service->_getOverviewByType('links', $filter, 'post');

        if ($post_type == 'summary') {
            $data['post_view'] = $this->service->_getOverviewByType('post_view', $filter);
            $data['texts_count'] = $post_texts['count'];
            $data['images_count'] = $post_images['count'];
            $data['videos_count'] = $post_videos['count'];
            $data['trip_plans_count'] = $trip_plans['count'];
            $data['reports_count'] = $reports['count'];
            $data['links_count'] = $links['count'];

            return $data;
        } else {
            foreach ($post_texts['post_info'] as $post_text) {
                $post_reports[strtotime($post_text->created_at)] = [
                    'type' => 'text',
                    'title' => $post_text->text,
                    'icon' => 'trav-post-icon',
                    'views_count' => $post_text->views_count,
                    'engag_count' => $post_text->comments_count + $post_text->shares_count + $post_text->likes_count,
                    'click_count' => 0
                ];
            }

            foreach ($post_images['post_info'] as $post_image) {
                if ($post_image->medias[0] && !in_array($post_image->medias[0]->media->url, $post_images_list)) {
                    $post_reports[strtotime($post_image->created_at)] = [
                        'type' => 'image',
                        'title' => ' <img src="' . ($post_image->medias[0] ? @$post_image->medias[0]->media->url : '') . '" alt="image" class="dash-posts-report-img"> ' . ($post_image->text ? $post_image->text : ''),
                        'icon' => 'trav-camera',
                        'views_count' => $post_image->views_count,
                        'engag_count' => $post_image->comments_count + $post_image->shares_count + $post_image->likes_count,
                        'click_count' => 0
                    ];

                    $post_images_list[] = $post_image->medias[0]->media->url;
                }
            }

            foreach ($post_videos['post_info'] as $post_video) {
                if ($post_video->medias[0] && !in_array($post_video->medias[0]->media->url, $post_videos_list)) {
                    $post_reports[strtotime($post_video->created_at)] = [
                        'type' => 'video',
                        'title' => '<span class="dash-post-video-wrap"><i class="fa fa-play" aria-hidden="true"></i></span> ' . ($post_video->text ? $post_video->text : ''),
                        'icon' => 'trav-video-icon',
                        'views_count' => $post_video->views_count,
                        'engag_count' => $post_video->comments_count + $post_video->shares_count + $post_video->likes_count,
                        'click_count' => 0
                    ];

                    $post_videos_list[] = $post_video->medias[0]->media->url;
                }
            }

            foreach ($trip_plans['post_info'] as $trip_plan) {
                $post_reports[strtotime($trip_plan->created_at)] = [
                    'type' => 'plan',
                    'title' => '<img src="' . get_plan_map($trip_plan, 140, 150) . '" alt="image" class="dash-posts-report-img"> ' . $trip_plan->title,
                    'icon' => 'trav-trip-plan-loc-icon',
                    'views_count' => 0,
                    'engag_count' => $trip_plan->comments_count + $trip_plan->shares_count + $trip_plan->likes_count,
                    'click_count' => 0
                ];
            }

            foreach ($reports['post_info'] as $report) {
                $post_reports[strtotime($report->created_at)] = [
                    'type' => 'report',
                    'title' => ' <img src="' . (isset($report->cover[0]) ? $report->cover[0]->val : asset('assets2/image/placeholders/no-photo.png')) . '" alt="image" class="dash-posts-report-img"> ' . $report->title,
                    'icon' => 'trav-report-icon',
                    'views_count' => $report->views_count,
                    'engag_count' => $report->comments_count + $report->shares_count + $report->likes_count,
                    'click_count' => 0
                ];
            }

            foreach ($links['post_info'] as $link) {
                $post_reports[strtotime($link->created_at)] = [
                    'type' => 'link',
                    'title' => $link->target,
                    'icon' => 'trav-link',
                    'views_count' => $link->clicks_count,
                    'engag_count' => $link->exits_count,
                    'click_count' => $link->exits_count
                ];
            }

            return $post_reports;
        }
    }


    /**
     * @param string $engagment_type
     * @param int $filter
     * @return array
     */
    protected function _getEngagementByType($engagment_type, $filter)
    {
        $daily_type = [];

        $posts = Posts::where('users_id', Auth::guard('user')->user()->id)
            ->pluck('id');

        switch ($engagment_type) {
            case 'view':
                //Engagement views
                $daily_count = PostsViews::whereIn('posts_id', $posts)->where('created_at', '>', Carbon::now()->subDays(1))->count();
                $total_count = PostsViews::whereIn('posts_id', $posts)->where('created_at', '<', Carbon::now()->subDays($filter))->count();
                $engagments = PostsViews::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('day')->get();
                break;
            case 'comment':
                //Engagement comment
                $daily_count = PostsComments::whereIn('posts_id', $posts)->where('created_at', '>', Carbon::now()->subDays(1))->count();
                $total_count = PostsComments::whereIn('posts_id', $posts)->where('created_at', '<', Carbon::now()->subDays($filter))->count();
                $engagments = PostsComments::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('day')->get();
                break;
            case 'like':
                //Engagement like
                $daily_count = PostsLikes::whereIn('posts_id', $posts)->where('created_at', '>', Carbon::now()->subDays(1))->count();
                $total_count = PostsLikes::whereIn('posts_id', $posts)->where('created_at', '<', Carbon::now()->subDays($filter))->count();
                $engagments = PostsLikes::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('day')->get();
                break;
            case 'share':
                //Engagement share
                $daily_count = PostsShares::whereIn('posts_id', $posts)->where('created_at', '>', Carbon::now()->subDays(1))->count();
                $total_count = PostsShares::whereIn('posts_id', $posts)->where('created_at', '<', Carbon::now()->subDays($filter))->count();
                $engagments = PostsShares::select(DB::raw('count(*) as total'), DB::raw('DATE(created_at) as day'))->whereIn('posts_id', $posts)->where('created_at', '>=', Carbon::now()->subDays($filter))->groupBy('day')->get();
                break;
        }



        foreach ($engagments as $engagment) {
            $daily_type[$engagment->day] = $engagment->total;
        }

        for ($day = $filter; $day >= 0; $day--) {
            $engagment_label[] = Carbon::now()->subDays($day)->format('d');
            if (array_key_exists(Carbon::now()->subDays($day)->format('Y-m-d'), $daily_type)) {
                $daily_values[] = $daily_type[Carbon::now()->subDays($day)->format('Y-m-d')];
                $total_values[] = $total_count + $daily_type[Carbon::now()->subDays($day)->format('Y-m-d')];
            } else {
                $daily_values[] = 0;
                $total_values[] = $total_count;
            }
        }

        $data['engagement_label'] = json_encode($engagment_label);
        $data['daily_value'] = json_encode($daily_values);
        $data['total_value'] = json_encode($total_values);
        $data['daily_count'] = $daily_count;

        return $data;
    }

    protected  function _getInteractionCountries($filter)
    {
        $interaction_countries = [];
        $posts = Posts::where('users_id', Auth::guard('user')->user()->id)
            ->pluck('id');

        $post_comment_query = PostsComments::whereIn('posts_id', $posts);
        $post_like_query = PostsLikes::whereIn('posts_id', $posts);
        $post_shere_query = PostsShares::whereIn('posts_id', $posts);

        $post_commant_countries = $post_comment_query->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
        foreach ($post_commant_countries as $post_commant_country) {
            if ($post_commant_country->author && $post_commant_country->author->country_of_nationality) {
                if (array_key_exists($post_commant_country->author->country_of_nationality->id, $interaction_countries)) {
                    $interaction_countries[$post_commant_country->author->country_of_nationality->id]['engagements']++;
                    $interaction_countries[$post_commant_country->author->country_of_nationality->id]['comments']++;
                } else {
                    $interaction_countries[$post_commant_country->author->country_of_nationality->id] = [
                        'engagements' => 1,
                        'comments' => 1,
                        'country_name' => $post_commant_country->author->country_of_nationality->transsingle->title,
                        'iso_code' => strtolower($post_commant_country->author->country_of_nationality->iso_code),
                    ];
                }
            }
        }



        $post_like_countries = $post_like_query->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
        foreach ($post_like_countries as $post_like_country) {
            if ($post_like_country->user && $post_like_country->user->country_of_nationality) {
                if (array_key_exists($post_like_country->user->country_of_nationality->id, $interaction_countries)) {
                    $interaction_countries[$post_like_country->user->country_of_nationality->id]['engagements']++;
                    if (array_key_exists('like', $interaction_countries[$post_like_country->user->country_of_nationality->id])) {
                        $interaction_countries[$post_like_country->user->country_of_nationality->id]['likes']++;
                    } else {
                        $interaction_countries[$post_like_country->user->country_of_nationality->id]['likes'] = 1;
                    }
                } else {
                    $interaction_countries[$post_like_country->user->country_of_nationality->id] = [
                        'engagements' => 1,
                        'likes' => 1,
                        'country_name' => $post_like_country->user->country_of_nationality->transsingle->title,
                        'iso_code' => strtolower($post_like_country->user->country_of_nationality->iso_code),
                    ];
                }
            }
        }

        $post_share_countries = $post_shere_query->where('created_at', '>=', Carbon::now()->subDays($filter))->get();
        foreach ($post_share_countries as $post_share_country) {
            if ($post_share_country->author && $post_share_country->author->country_of_nationality) {
                if (array_key_exists($post_share_country->author->country_of_nationality->id, $interaction_countries)) {
                    $interaction_countries[$post_share_country->author->country_of_nationality->id]['engagements']++;

                    if (array_key_exists('shares', $interaction_countries[$post_share_country->author->country_of_nationality->id])) {
                        $interaction_countries[$post_share_country->author->country_of_nationality->id]['shares']++;
                    } else {
                        $interaction_countries[$post_share_country->author->country_of_nationality->id]['shares'] = 1;
                    }
                } else {
                    $interaction_countries[$post_share_country->author->country_of_nationality->id] = [
                        'engagement' => 1,
                        'shares' => 1,
                        'country_name' => $post_share_country->author->country_of_nationality->transsingle->title,
                        'iso_code' => strtolower($post_share_country->author->country_of_nationality->iso_code),
                    ];
                }
            }
        }

        return ['interaction_countries' => $interaction_countries];
    }

    protected function _getUserAges($age_arr, $user_age)
    {
        if (18 <= $user_age && $user_age < 22) {
            $age_arr['18-22']++;
        } else if (23 <= $user_age && $user_age < 30) {
            $age_arr['23-30']++;
        } else if (31 <= $user_age && $user_age < 36) {
            $age_arr['31-36']++;
        } else if (37 <= $user_age && $user_age < 48) {
            $age_arr['37-48']++;
        } else if (49 <= $user_age && $user_age < 58) {
            $age_arr['49-58']++;
        } else if (59 <= $user_age) {
            $age_arr['59+']++;
        }

        return $age_arr;
    }
}
