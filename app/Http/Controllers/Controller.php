<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Travooo Api",
     *      description="
     * Country, City, Place             : https://docs.google.com/document/d/1jSTU6OptGh81yEVFTAGPR86tduRGPMnVQ1hawpXWX3g/edit
     * Flight, Hotel & Cars Booking     : https://docs.google.com/document/d/1-PmrMtmRj-iAqdYam5_aZcAxVRCCHy5IA0DuHN0iGx0/edit 
     * Report Creation                  : https://docs.google.com/document/d/1TP4Q3YTHg4KnDUDFpUh0Mqve1A5w2mDwPbMik96a11w/edit",
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="API Server"
     * )
     *
     *
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Returns JSON Response that specified by project standards
     *
     * @param array $data
     * @param array $errors
     * @param null $message
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponse($data = [], $errors = [], $message = null, $status = 200)
    {
        return response()
            ->json([
                'data' => $data,
                'errors' => $errors,
                'message' => $message,
                'code' => $status
            ], $status);
    }
}
