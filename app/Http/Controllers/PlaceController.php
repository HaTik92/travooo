<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Access\language\Languages;
use League\Fractal\Resource\Item;
use App\Transformers\Country\CountryTransformer;
use Illuminate\Support\Facades\DB;
use App\Models\Country\ApiCountry as Country;
use App\Models\Country\CountriesFollowers;
use Illuminate\Support\Facades\Auth;
use App\Models\ActivityLog\ActivityLog;
use App\CountriesContributions;
use Illuminate\Support\Facades\Input;
use App\Models\Place\Place;
use App\Models\Place\PlaceFollowers;
use App\Models\PlacesTop\PlacesTop;
use App\Models\Reviews\Reviews;
use App\Models\Posts\Checkins;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Models\TripPlans\TripPlans;

class PlaceController extends Controller {

    public function __construct() {
        $this->middleware('auth:user');
    }

    public function getIndex($place_id) {
        $language_id = 1;
        $place = Place::find($place_id);

        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $place = Place::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'followers'
                ])
                ->where('id', $place_id)
                ->where('active', 1)
                ->first();

        $top_places = PlacesTop::where('country_id', $place->country->id)
                ->orderBy('reviews_num', 'desc')
                ->take(20)
                ->get();

        if (!$place) {
            return [
                'data' => [
                    'error' => 404,
                    'message' => 'Not found',
                ],
                'success' => false
            ];
        }

        $db_place_reviews = Reviews::where('places_id', $place->id)->get();
        if (!count($db_place_reviews)) {
            $place_reviews = get_google_reviews($place->provider_id);
            if (is_array($place_reviews)) {
                foreach ($place_reviews AS $review) {
                    $r = new Reviews;
                    $r->places_id = $place->id;
                    $r->google_author_name = $review->author_name;
                    $r->google_author_url = $review->author_url;
                    $r->google_language = $review->language;
                    $r->google_profile_photo_url = $review->profile_photo_url;
                    $r->score = $review->rating;
                    $r->google_relative_time_description = $review->relative_time_description;
                    $r->text = $review->text;
                    $r->google_time = $review->time;
                    $r->save();
                }
            }
        }

        $db_place_reviews = Reviews::where('places_id', $place->id)->get();
        $data['reviews'] = $db_place_reviews;

        $data['reviews_avg'] = Reviews::where('places_id', $place->id)->avg('score');

        // get places nearby
        $data['places_nearby'] = Place::with('medias')
                ->whereHas("medias", function ($query) {
                    $query->where("medias_id", ">", 0);
                })
                ->where('cities_id', $place->cities_id)
                ->where('id', '!=', $place->id)
                ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
                ->having('distance', '<', 15)
                ->orderBy('distance')
                ->take(10)
                ->get();

                
        $checkins = Checkins::where('place_id', $place->id)->orderBy('id', 'DESC')->get();
        
        $result = array();
        $done = array();
        foreach ($checkins AS $checkin) {
            if (is_object($checkin->post_checkin) && !isset($done[$checkin->post_checkin->post->author->id])) {
                $result[] = $checkin;
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['checkins'] = $result;


        $data['place'] = $place;
        $data['top_places'] = $top_places;



        $client = new Client();
        //$client->getHttpClient()->setDefaultOption('verify', false);

        $result = @$client->post('https://api.predicthq.com/oauth2/token', [
            'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'TZUhMoqSkz0JGuiBjL22c8x5Bsm9zgymv7AhZgg3'],
            'form_params' => [
                'grant_type' => 'client_credentials',
                'scope' => 'account events signals'
            ],
            'verify' => false
        ]);
        $events_array = false;
        if ($result->getStatusCode() == 200) {
            $json_response = $result->getBody()->read(1024);
            $response = json_decode($json_response);
            $access_token = $response->access_token;

            $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $place->lat . ',' . $place->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
                'headers' => ['Authorization' => 'Bearer ' . $access_token],
                'verify' => false
            ]);
            if ($get_events1->getStatusCode() == 200) {
                $events1 = json_decode($get_events1->getBody()->read(100024));
                //dd($events);
                $events_array1 = $events1->results;
                $events_final = $events_array1;
                if ($events1->next) {
                    $get_events2 = $client->get($events1->next, [
                        'headers' => ['Authorization' => 'Bearer ' . $access_token],
                        'verify' => false
                    ]);
                    if ($get_events2->getStatusCode() == 200) {
                        $events2 = json_decode($get_events2->getBody()->read(100024));
                        //dd($events);
                        $events_array2 = $events2->results;
                        $events_final = array_merge($events_array1, $events_array2);
                    }
                }
                $data['events'] = $events_final;
            }
        }
        
        
        $city_name = $place->city->transsingle->title;
        $data['city_code'] = 0;
        $res = file_get_contents('https://srv.wego.com/places/search?query=' . $city_name);
            $results = json_decode($res);
            $cities = array();
            $i = 0;
            if(isset($results[0]) AND is_object($results[0]) AND $results[0]->type=="city") {
                $data['city_code'] = $results[0]->code;
            }

            //dd($city_code);

        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();
        
        return view('site.place.index', $data);
    }

    public function postFollow($placeId) {
        $userId = Auth::guard('user')->user()->id;
        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }


        $follower = PlaceFollowers::where('places_id', $placeId)
                ->where('users_id', $userId)
                ->first();

        if ($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that Place',
                ],
                'success' => false
            ];
        }

        $place->followers()->create([
            'users_id' => $userId
        ]);

        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('Place', 'follow', $placeId);


        return [
            'success' => true
        ];
    }

    public function postUnFollow($placeId) {
        $userId = Auth::guard('user')->user()->id;

        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $follower = PlaceFollowers::where('places_id', $placeId)
                ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this Place.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('Place', 'unfollow', $placeId);
        }

        return [
            'success' => true
        ];
    }

    public function postCheckFollow($placeId) {
        $userId = Auth::guard('user')->user()->id;
        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $follower = PlaceFollowers::where('places_id', $placeId)
                ->where('users_id', $userId)
                ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    public function postTalkingAbout($placeId) {
        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $shares = $place->shares;

        foreach ($shares AS $share) {
            $data['shares'][] = array(
                'user_id' => $share->user->id,
                'user_profile_picture' => check_profile_picture($share->user->profile_picture)
            );
        }

        $data['num_shares'] = count($shares);
        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function postNowInPlace($placeId) {
        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        $checkins = Checkins::where('place_id', $place->id)
                ->orderBy('id', 'DESC')
                ->take(5)
                ->get();

        $result = array();
        $done = array();
        foreach ($checkins AS $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                if(time()-strtotime($checkin->post_checkin->post->date) < (24*60*60)) {
                    $live = 1;
                }
                else {
                    $live = 0;
                }
                $result[] = array(
                    'name' => $checkin->post_checkin->post->author->name,
                    'id' => $checkin->post_checkin->post->author->id,
                    'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                    'live' => $live
                );
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['live_checkins'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function ajaxPostReview($placeId) {
        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }

        if (Input::has('text')) {
            $review_text = Input::get('text');
            $review_score = Input::get('score');
            $review_author = Auth::guard('user')->user();
            $review_place_id = $place->id;

            $review = new Reviews;
            $review->places_id = $review_place_id;
            $review->by_users_id = $review_author->id;
            $review->score = $review_score;
            $review->text = $review_text;

            if ($review->save()) {
                log_user_activity('Place', 'review', $place->id);
                $data['success'] = true;
                $data['prepend'] = '<div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="' . check_profile_picture(Auth::guard('user')->user()->profile_picture) . '" alt="" style="width:45px;height:45px;">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">' . Auth::guard('user')->user()->name . '</a>
                                    <a href="#" class="comment-nickname"></a>
                                </div>
                                <div class="comment-txt">
                                    <p>' . $review_text . '</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-star-block">
                                        <select class="rating_stars">
                                            <option value="1" ' . ($review->score == 1 ? "selected" : "") . '>1</option>
                                            <option value="2" ' . ($review->score == 2 ? "selected" : "") . '>2</option>
                                            <option value="3" ' . ($review->score == 3 ? "selected" : "") . '>3</option>
                                            <option value="4" ' . ($review->score == 4 ? "selected" : "") . '>4</option>
                                            <option value="5" ' . ($review->score == 5 ? "selected" : "") . '>5</option>
                                        </select>
                                        <span class="count">
                                            <b>' . $review->score . '</b> / 5
                                        </span>
                                    </div>
                                    <div class="dot">·</div>
                                    <div class="com-time">' . diffForHumans(date("Y-m-d", time())) . '</div>
                                </div>
                            </div>
                        </div>';
            } else {
                $data['success'] = false;
            }
        } else {
            $data['success'] = false;
        }
        return json_encode($data);
    }

    public function ajaxHappeningToday($placeId) {
        $place = Place::find($placeId);
        if (!$place) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Place ID',
                ],
                'success' => false
            ];
        }


        $checkins = Checkins::whereHas("place", function ($query) use ($place) {
                    $query->where('cities_id', $place->cities_id);
                    $query->where('id', '!=', $place->id);
                })
                ->orderBy('id', 'DESC')
                ->get();

        /*
          ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
          ->having('distance', '<', 15)
          ->orderBy('distance')
          ->take(10)
          ->get();
         * 
         */

        //$checkins = Checkins::where('place_id', $place->id)->orderBy('id', 'DESC')->take(5)->get();

        $result = array();
        foreach ($checkins AS $checkin) {
            $result[] = array(
                'post_id' => $checkin->post_checkin->post->id,
                'lat' => $checkin->place->lat,
                'lng' => $checkin->place->lng,
                'photo' => @$checkin->post_checkin->post->medias[0]->media->url,
                'name' => $checkin->post_checkin->post->author->name,
                'id' => $checkin->post_checkin->post->author->id,
                'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                'date' => diffForHumans($checkin->post_checkin->post->created_at)
            );
        }
        $data['happenings'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

}
