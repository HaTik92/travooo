<?php

namespace App\Http\Controllers\Backend\SafetyDegrees;

use App\Models\Access\Language\Languages;
use App\Models\SafetyDegree\SafetyDegree;
use App\Models\SafetyDegree\SafetyDegreeTrans;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\SafetyDegrees\ManageSafetyDegreesRequest;
use App\Http\Requests\Backend\SafetyDegrees\StoreSafetyDegreesRequest;
use App\Repositories\Backend\SafetyDegrees\SafetyDegreesRepository;
use App\Http\Requests\Backend\SafetyDegrees\UpdateSafetyDegreesRequest;
use Yajra\DataTables\Facades\DataTables;

class SafetyDegreesController extends Controller
{
    protected $degrees;
    protected $languages;

    /**
     * SafetyDegreesController constructor.
     * @param SafetyDegreesRepository $degrees
     */
    public function __construct(SafetyDegreesRepository $degrees)
    {
        $this->degrees = $degrees;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManageSafetyDegreesRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageSafetyDegreesRequest $request)
    {
        return view('backend.safety-degrees.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->degrees->getForDataTable())
            ->addColumn('action', function ($degrees) {
                return view('backend.buttons', ['params' => $degrees, 'route' => 'safety-degrees.safety']);
            })
            ->make(true);
    }

    /**
     * @param ManageSafetyDegreesRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(ManageSafetyDegreesRequest $request)
    {
        return view('backend.safety-degrees.create');
    }

    /**
     * @param $id
     * @param ManageSafetyDegreesRequest $request
     * @return mixed
     */
    public function edit($id, ManageSafetyDegreesRequest $request)
    {   
        $data = [];
        $safetydegree = SafetyDegree::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = SafetyDegreeTrans::where([
                'languages_id' => $language->id,
                'safety_degrees_id'   => $id
            ])->first();

            if (!empty($model)) {
                $data['title_'.$language->id]       = $model->title ?: null;
                $data['description_'.$language->id] = $model->description ?: null;
            }
        }

        return view('backend.safety-degrees.edit')
            ->withLanguages($this->languages)
            ->withSafetydegrees($safetydegree)
            ->withSafetyid($id)
            ->withData($data);
    }

    /**
     * @param $id
     * @param UpdateSafetyDegreesRequest $request
     * @return mixed
     */
    public function update($id, UpdateSafetyDegreesRequest $request)
    {   
        $safetydegree = SafetyDegree::findOrFail($id);
        
        $data = [];

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }
        
        $this->degrees->update($id , $safetydegree, $data);

        return redirect()->route('admin.safety-degrees.safety.index')
            ->withFlashSuccess('Safety Degree updated Successfully!');
    }

    /**
     * @param StoreSafetyDegreesRequest $request
     * @return mixed
     */
    public function store(StoreSafetyDegreesRequest $request)
    {   
        $data = [];
       
        foreach ($this->languages as $key => $language) {

            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $this->degrees->create($data);

        return redirect()->route('admin.safety-degrees.safety.index')->withFlashSuccess('Safety Degree Created Successfully');
    }

    /**
     * @param $id
     * @param ManageSafetyDegreesRequest $request
     * @return mixed
     */
    public function destroy($id, ManageSafetyDegreesRequest $request)
    {
        $item = SafetyDegree::findOrFail($id);
        /* Delete Children Tables Data of this country */
        $child = SafetyDegreeTrans::where(['safety_degrees_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.safety-degrees.safety.index')->withFlashSuccess('Safety Degree Deleted Successfully');
    }

    /**
     * @param Languages $language
     * @param $status
     * @param ManageSafetyDegreesRequest $request
     * @return mixed
     */
    public function mark(Languages $language, $status, ManageSafetyDegreesRequest $request)
    {
        $language->active = $status;
        $language->save();
        return redirect()->route('admin.access.languages.index')->withFlashSuccess(trans('alerts.backend.language.updated'));
    }

    /**
     * @param $degree
     * @param ManageSafetyDegreesRequest $request
     * @return mixed
     */
    public function show($degree, ManageSafetyDegreesRequest $request)
    {   $id = $degree;
        $degree = SafetyDegree::findOrFail($id);
        $degreeTrans = SafetyDegreeTrans::where(['safety_degrees_id' => $id])->get();
        
        return view('backend.safety-degrees.show')
            ->withDegree($degree)
            ->withDegreetrans($degreeTrans);
    }
}