<?php

namespace App\Http\Controllers\Backend\UnapprovedItem;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\City\CitiesTranslations;
use App\Models\Country\CountriesTranslations;
use App\Models\Place\Place;
use App\Models\Place\Medias;
use App\Models\Place\PlaceMedias;
use App\Models\Place\PlaceTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Place\ManagePlaceRequest;
use App\Http\Requests\Backend\Place\StorePlaceRequest;
use App\Http\Requests\Backend\Place\UpdatePlaceRequest;
use App\Models\Place\UnapprovedItem;
use App\Models\PlacesTop\PlacesTop;
use App\Repositories\Backend\Place\PlaceRepository;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\AdminLogs\AdminLogs;
use App\Models\PlaceSearchHistory\PlaceSearchHistory;
use App\Models\ActivityMedia\Media;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Utilities\Request;

class UnapprovedItemController extends Controller {

    use CreateUpdateStatisticTrait;

    protected $unapprovedItems;
    private $placeTypesArray = [
        'airport',
        'amusement_park',
        'aquarium',
        'art_gallery',
        'bakery',
        'bar',
        'cafe',
        'campground',
        'casino',
        'cemetery',
        'church',
        'city_hall',
        'embassy',
        'hindu_temple',
        'library',
        'light_rail_station',
        'lodging',
        'meal_delivery',
        'meal_takeaway',
        'mosque',
        'movie_theater',
        'museum',
        'night_club',
        'park',
        'restaurant',
        'shopping_mall',
        'stadium',
        'subway_station',
        'synagogue',
        'tourist_attraction',
        'train_station',
        'transit_station',
        'zoo',
        'food',
        'place_of_worship',
        'natural_feature' 

    ];

    /**
     * PlaceController constructor.
     * @param PlaceRepository $unapprovedItems
     */
    public function __construct(PlaceRepository $unapprovedItems) {

        $this->places = $unapprovedItems;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManagePlaceRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManagePlaceRequest $request) {
        $term = trim($request->q);
        if (!empty($term)) {
            $unapprovedItems_trans = PlaceTranslations::where('title', 'LIKE', $term . '%')
                    ->join('places', function ($query) {
                        $query->on('places_trans.places_id', '=', 'places.id')
                        ->where('places.active', 1);
                    })
                    ->skip(($request->page - 1 ) * 10)
                    ->take(10)
                    ->get();
            $paginate = true;
            if ($unapprovedItems_trans->isEmpty()) {
                $unapprovedItems_trans = PlaceTranslations::where('title', 'LIKE', $term . '%')
                        ->join('places', function ($query) {
                            $query->on('places_trans.places_id', '=', 'places.id')
                            ->where('places.active', 1);
                        })
                        ->get();
                $paginate = false;
            }
            $formatted_places = [];

            foreach ($unapprovedItems_trans as $value) {
                $formatted_places[] = ['id' => $value->places_id, 'text' => $value->title];
            }
            return response()->json(['unapprovedItems' => $formatted_places, 'paginate' => $paginate]);
        }
        return view('backend.unapproved_item.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table(Request $request) {
        foreach ($request->columns as $column) {
            if ($column['data'] == 'address') {
                $matchValue     = @$column['search']['value1']['matchValue'];
                $notMatchValue  = @$column['search']['value2']['notMatchValue'];
            }

            if ($column['data'] == 'pluscode') {
                $matchValue2     = @$column['search']['value1']['matchValue2'];
                $notMatchValue2  = @$column['search']['value2']['notMatchValue2'];
            }

            if ($column['data'] == '') {
                $countryId = $column['search']['value'];
            }

            if ($column['data'] == 'city_title') {
                $cities_id = $column['search']['value'];
            }

            if ($column['data'] == 'unapproved_item_id_title') {
                $unapprovedItemType = $column['search']['value'];
            }

            if ($column['data'] == 'title') {
                $unapprovedItemTitle = $column['search']['value'];
            }
        }

        $orderData = current($request->order);
        $orderColumn = $request->columnName($orderData['column']);
        $orderDir = $orderData['dir'];

        $query = UnapprovedItem::select([
            config('locations.place_table') . '.id',
            DB::raw('MAX(' . config('locations.place_table') . '.cities_id)'),
            DB::raw('MAX(' . config('locations.place_table') . '.countries_id)'),
            DB::raw('MAX(' . config('locations.place_table') . '.place_type) as unapproved_item_id_title'),
            DB::raw('MAX(' . config('locations.place_table') . '.media_done)'),
            DB::raw('MAX(' . config('locations.place_table') . '.active) as active'),
            DB::raw('MAX(' . config('locations.place_table') . '.media_count) as media_count'),
            DB::raw('MAX(' . config('locations.place_table') . '.pluscode) as pluscode'),
            DB::raw('MAX(transsingle.title) as title'),
            DB::raw('MAX(transsingle.address) as address'),
            DB::raw('MAX(cities_trans.title) as city_title'),
            DB::raw('MAX(countries_trans.title) as country_title')
        ]) ->where('places.auto_import', '=', 1)//unapproved flag
//        ->where(function ($query) {
//            foreach($this->placeTypesArray as $key=>$placeType){
//                if($key == 0)
//                    $query->whereRaw('SUBSTRING_INDEX( place_type, ",", 1 ) like "%'.$placeType.'%"');
//                else
//                    $query->OrwhereRaw('SUBSTRING_INDEX( place_type, ",", 1 ) like "%'.$placeType.'%"');
//            }
//        })
            ->leftJoin(DB::raw('places_trans AS transsingle FORCE INDEX (places_id)'), function ($query) {
                $query->on('transsingle.places_id', '=', 'places.id')
                    ->where('transsingle.languages_id', '=', 1);
                })
                ->leftJoin('cities_trans', function ($query) {
                    $query->on('cities_trans.cities_id', '=', 'places.cities_id')
                    ->where('cities_trans.languages_id', '=', 1);
                })
                ->Leftjoin('countries_trans', function ($query) {
                    $query->on('countries_trans.countries_id', '=', 'places.countries_id')
                    ->where('countries_trans.languages_id', '=', 1);
            })
            ->when($matchValue, function ($query) use ($matchValue) {
                $this->filtered = true;
                $matchValues = explode("/", $matchValue );
                return $query->where(function ($query) use ($matchValues) {
                    foreach ($matchValues as $key => $matchValue) {
                        if ($key)
                            $query->orWhere('transsingle.address', 'LIKE', "%".$matchValue."%");
                        else
                            $query->where('transsingle.address', 'LIKE', "%".$matchValue."%");
                    }
                });
            })
            ->when($notMatchValue, function ($query) use ($notMatchValue) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue );
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue) {
                        $query->where('transsingle.address', 'NOT LIKE', "%" . $notMatchValue . "%");
                    }
                });
            })
            ->when($matchValue2, function ($query) use ($matchValue2) {
                $this->filtered = true;
                return $query->where('places.pluscode', 'LIKE', "%".$matchValue2."%");
            })
            ->when($notMatchValue2, function ($query) use ($notMatchValue2) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue2 );
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue2) {
                        $query->where('places.pluscode', 'NOT LIKE', "%" . $notMatchValue2 . "%");
                    }
                });
            })
            ->when($cities_id, function ($query) use ($cities_id) {
                $this->filtered = true;
                return $query->where('places.cities_id', $cities_id);
            })
            ->when($unapprovedItemType, function ($query) use ($unapprovedItemType) {
                $this->filtered = true;

                return $query->where('places.place_type', $unapprovedItemType);
            })
            ->when($unapprovedItemTitle, function ($query) use ($unapprovedItemTitle) {
                $this->filtered = true;
                return $query->where(function($qr) use($unapprovedItemTitle){
                    $qr->where('transsingle.title', 'LIKE', '%'.$unapprovedItemTitle.'%')
                        ->orWhere('transsingle.address', 'LIKE', '%'.$unapprovedItemTitle.'%')
                        ->orWhere('places.pluscode', 'LIKE', '%'.$unapprovedItemTitle.'%');
                    });
            })
            ->groupBy('places.id');

//        $response['recordsTotal'] = $query->count();

        /*this is a wrong value for $response['recordsFiltered'] count, as $query->count() does not work for large data,
        will fix it after ElasticSearch instalation*/
        $response['recordsFiltered'] = 1000000;;

        $unapprovedItems = $query->offset($request->start)->limit($request->length)->orderBy($orderColumn, $orderDir)->get();

        foreach ($unapprovedItems as $unapprovedItem) {
            $unapprovedItem->empty = '';
            $unapprovedItem->action = $unapprovedItem->action_buttons;
            $unapprovedItem->media_done_new = 'Yes';

            if(!isset($unapprovedItem->pluscode)){
                $unapprovedItem->pluscode = '-';
            }
        }

        $response['data'] = $unapprovedItems;
        $response['draw'] = $request->draw;
        $response['input'] = $request->all();

        return response()->json($response);
    }

    public function approve(Request $request)
    {
        $placeId = $request->id;

        Place::where('id', $placeId)->update(array('auto_import' => 0));

        return redirect()->route('admin.location.unapproved-items.index')->withFlashSuccess('Successfully Approved!');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function markTopPlace(Request $request)
    {
        $placeId = $request->id;
        $hotelType = [
            'lodging'
        ];

        $restourantType = [
            'bakery',
            'bar',
            'cafe',
            'meal_delivery',
            'meal_takeaway',
            'restaurant',
            'food'
        ];

        $ifExist = PlacesTop::where('places_id', '=', $placeId)->exists();

        if (!$ifExist){
            $topPlaceData = Place::select(
                'places.id as places_id',
                'places_trans.title as title',
                'places.countries_id as country_id',
                'places.cities_id as city_id',
                'places.rating as rating',
                'cities_trans.title as city',
                'countries_trans.title as country',
                'places.place_type as destination_type'
            )
                ->leftJoin('cities_trans', 'cities_trans.cities_id', '=', 'places.cities_id')
                ->leftJoin('places_trans', 'places_trans.places_id', '=', 'places.id')
                ->leftJoin('countries_trans', 'countries_trans.countries_id', '=', 'places.countries_id')
                ->where('places.id', '=', $placeId)
                ->get()->first()
            ;

            $topPlaceData['rating'] = $topPlaceData['rating'] ?: 0;

            $ptype = explode(",", $topPlaceData['destination_type']);

            if(in_array($ptype[0], $hotelType)){
                $topPlaceData['destination_type'] = 'hotel';
            }elseif (in_array($ptype[0], $restourantType)){
                $topPlaceData['destination_type'] = 'restaurant';
            }else{
                $topPlaceData['destination_type'] = 'place';
            }

            $order = 0 ;
            $tp_for_order = PlacesTop::where('city_id', $topPlaceData['city_id'])->where('destination_type', $topPlaceData['destination_type'])->orderBy('order_by_city','DESC')->first();
            if(is_object($tp_for_order)){
                $order= $tp_for_order->order_by_city+1;
            }

            $topPlaceData['order_by_city'] = $order;

            PlacesTop::create(
                $topPlaceData->toArray()
            );

            Place::where('id', $placeId)->update(array('auto_import' => 0));

            return redirect()->route('admin.location.unapproved-items.index')->withFlashSuccess('Place successfully mark as a top!');
        } else {
            return redirect()->route('admin.location.unapproved-items.index')->withErrors('Place already mark as a top!');
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function unmarkTopPlace(Request $request)
    {
        $placeId = $request->id;

        $ifExist = PlacesTop::where('places_id', '=', $placeId)->exists();

        if ($ifExist){
            PlacesTop::where('places_id', '=', $placeId)->delete();

            return redirect()->route('admin.location.unapproved-items.index')->withFlashSuccess('Place successfully unmark as a top!');
        } else {
            return redirect()->route('admin.location.unapproved-items.index')->withErrors('Place not marked as a top!');
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function massApprove(Request $request)
    {
        $placeIds = $request->ids;
        Place::whereIn('id', $placeIds)->update(array('auto_import' => 0));
        return response()->json([
            'data' => [
                'success' => true,
                'message' => 'Successfully Approved!',
            ]
        ]);
    }
}
