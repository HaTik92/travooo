<?php

namespace App\Http\Controllers\Backend\Destinations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Destinations\CitiesNationalityRequest;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Repositories\Backend\Destinations\CitiesNationalityRepository;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Utilities\Request;

class CitiesNationalityController extends Controller
{
    protected $cities_nationality;

    /**
     * CountriesNationality constructor.
     */
    public function __construct(CitiesNationalityRepository $cities_nationality)
    {
        $this->cities_nationality = $cities_nationality;
    }

    public function index(Request $request)
    {
        $top_cities = [
            'Hong Kong',
            'Bangkok',
            'London',
            'Macau',
            'Singapore',
            'Paris',
            'Dubai',
            'New York',
            'Kuala Lumpur',
            'Istanbul',
            'Delhi',
            'Antalya',
            'Shenzhen',
            'Mumbai',
            'Phuket',
            'Rome',
            'Tokyo',
            'Pattaya',
            'Taipei',
            'Mecca',
            'Guangzhou',
            'Prague',
            'Medina',
            'Seoul',
            'Amsterdam',
            'Agra',
            'Miami',
            'Osaka',
            'Los Angeles',
            'Shanghai',
            'Ho Chi Minh City',
            'Denpasar',
            'Barcelona',
            'Las Vegas',
            'Cairo',
            'Athens',
            'Moscow',
            'Riyadh',

        ];

        if($request->add_city && $request->add_city == 1){
            $countries = Countries::all();

            foreach($countries as $country){
                shuffle($top_cities);
                $i =0;
                $list_ids = [];

                foreach($top_cities as $top_city){
                    $get_city = Cities::with('transsingle')->whereHas('transsingle', function ($query) use ($top_city) {
                        $query->where('title', $top_city);
                    })->first();

                    if($get_city && $get_city->country->id != $country->id && $i<10){

                        $list_ids[]= $get_city->id;
                        $i++;
                    }
                }

                $extra = [
                    'nationality_id' => $country->id,
                    'cities_id' => $list_ids,
                ];
                $this->cities_nationality->create($extra);
            }

            dd('done');
        }

        return view('backend.destinations.cities-nationality.cities-by-nationality');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table() {
        return Datatables::of($this->cities_nationality->getForDataTable())
            ->addColumn('action', function ($cities_nationality) {
                return $cities_nationality->actions;
            })
            ->make(true);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create()
    {
        /* Get All Countries */
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->with('transsingle')->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        return view('backend.destinations.cities-nationality.create-cities-by-nationality', [
                        'countries' => $countries_arr,
                        'cities' => $cities_arr,
                        ]);
    }

    /**
     * @param CitiesNationalityRequest $request
     * @return mixed
     */
    public function store(CitiesNationalityRequest $request)
    {
        $extra = [
            'nationality_id' => $request->nationality_id,
            'cities_id' => $request->cities_id,
        ];

        $this->cities_nationality->create($extra);

        return redirect()->route('admin.cities-by-nationality.index')->withFlashSuccess('Cities by Nationality Created!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $cities = $this->cities_nationality->findByNationalityId($id);
        $all_cities_list = Cities::where(['active' => 1])->with('transsingle')->get();
        $cities_arr = [];

        foreach ($all_cities_list as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        $get_country = Countries::find($id);
        $selected_cities = [];
        if($cities){
            foreach($cities as $city){
                $selected_cities[] = $city->cities_id;
            }
        }

        $data['selected_nationality'] = $get_country->transsingle->title;
        $data['selected_cities'] = $selected_cities;
        $data['all_cities'] = $cities_arr;


        return view('backend.destinations.cities-nationality.edit-cities-by-nationality', $data)
            ->withCities($cities)
            ->withNationalityid($id);
    }

    /**
     * @param  $id
     * @param CitiesNationalityRequest $request
     *
     * @return mixed
     */
    public function update($id, CitiesNationalityRequest $request)
    {

        $extra = [
            'nationality_id' => $id,
            'cities_id' => $request->cities_id,
        ];

        $this->cities_nationality->update($extra);

        return redirect()->back()
            ->withFlashSuccess('Cities updated Successfully!');
    }


    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {

    }

}
