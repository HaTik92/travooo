<?php

namespace App\Http\Controllers\Backend\Destinations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Destinations\GenericPlacesRequest;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Destinations\GenericTopPlaces;
use App\Models\PlacesTop\PlacesTop;
use App\Repositories\Backend\Destinations\GenericTopPlacesRepository;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Utilities\Request;

class GenericTopPlacesController extends Controller
{
    protected $generic_top_places;

    /**
     * GenericTopPlaces constructor.
     */
    public function __construct(GenericTopPlacesRepository $generic_top_places)
    {
        $this->generic_top_places = $generic_top_places;
    }

    public function index()
    {
        return view('backend.destinations.generic-places.generic-top-places');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table() {
        return Datatables::of($this->generic_top_places->getForDataTable())
            ->addColumn('action', function ($generic_top_places) {
                return $generic_top_places->actions;
            })
            ->make(true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlacesByCountry(Request $request) {
        /* Get Cities by Country id*/
        $country_id = $request->countryId;
        $cities = Cities::where(['active' => 1])->where('countries_id', $country_id)->with('transsingle')->get();
        $cities_arr = [];
        $places_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[] = [
                    'id' => $value->id,
                    'text' => $value->transsingle->title
                ];
            }
        }

        $all_places = PlacesTop::where('country_id', $country_id)
            ->orderBy('reviews_num', 'desc')
            ->orderBy('rating', 'desc')
            ->get();

        foreach ($all_places as  $place) {
                $places_arr[] = [
                    'id' => $place->places_id,
                    'text' => $place->title
                ];
        }

        return new JsonResponse(['cities' => $cities_arr, 'places' => $places_arr]);
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlacesByCity(Request $request) {
        $city_id = $request->cityId;
        $places_arr = [];

        /* Get Places by Country and City*/
        $all_places = PlacesTop::where('city_id', $city_id)
            ->orderBy('reviews_num', 'desc')
            ->orderBy('rating', 'desc')
            ->get();

        foreach ($all_places as  $place) {
            $places_arr[] = [
                'id' => $place->places_id,
                'text' => $place->title
            ];
        }

        return new JsonResponse($places_arr);
    }


    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create()
    {
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->where('countries_id', 1)->with('transsingle')->get();
        $cities_arr = [];
        $places_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }
        /* Get Places by Country and City*/
        $all_places = PlacesTop::where('city_id', 19399)
            ->orderBy('reviews_num', 'desc')
            ->orderBy('rating', 'desc')
            ->get();

        foreach ($all_places as  $place) {
            $places_arr[$place->places_id] = $place->title;
        }

        return view('backend.destinations.generic-places.create-generic-top-places', [
                        'countries' => $countries_arr,
                        'cities' => $cities_arr,
                        'places' => $places_arr,
                        ]);
    }

    /**
     * @param GenericPlacesRequest $request
     * @return mixed
     */
    public function store(GenericPlacesRequest $request)
    {
        $extra = [
            'city_id' => $request->city_id,
            'country_id' => $request->country_id,
            'place_id' => $request->place_id,
        ];

        $generic_place = $this->generic_top_places->findByPlaceId($request->place_id);

        if ($generic_place) {
            return redirect()->back()->withErrors('The Place Already Selected!');
        }

        $all_generic_places = GenericTopPlaces::all();

        if(count($all_generic_places) == 20){
            return redirect()->back()->withErrors('Maximum Places List Are 20');
        }

        $this->generic_top_places->create($extra);

        return redirect()->route('admin.generic-top-places.index')->withFlashSuccess('Generic Top Place Created!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $generic_place = $this->generic_top_places->findById($id);
        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->where('countries_id', $generic_place->countries_id)->with('transsingle')->get();
        $cities_arr = [];
        $places_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Countries */
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get Selected Places */
        $top_places = PlacesTop::where('city_id', $generic_place->cities_id)
        ->orderBy('reviews_num', 'desc')
        ->orderBy('rating', 'desc')
        ->get();

        foreach ($top_places as $place) {
              $places_arr[$place->places_id] = $place->title;
        }

        $data['selected_city'] = $generic_place->cities_id;
        $data['selected_country'] = $generic_place->countries_id;
        $data['selected_place'] = $generic_place->places_id;
        $data['all_cities'] = $cities_arr;
        $data['countries'] = $countries_arr;
        $data['places'] = $places_arr;


        return view('backend.destinations.generic-places.edit-generic-top-places', $data)
            ->withCities($generic_place)
            ->withId($id);
    }

    /**
     * @param  $id
     * @param GenericPlacesRequest $request
     *
     * @return mixed
     */
    public function update($id, GenericPlacesRequest $request)
    {
        $extra = [
            'city_id' => $request->city_id,
            'country_id' => $request->country_id,
            'place_id' => $request->place_id,
        ];

        $generic_place = $this->generic_top_places->findByPlaceId($request->place_id);

        if ($generic_place) {
                    return redirect()->back()->withErrors('The Place Already Selected!');
                }

        $this->generic_top_places->update($id, $extra);

        return redirect()->back()
            ->withFlashSuccess('Generic Top Cities updated Successfully!');
    }


    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->generic_top_places->delete($id);

        return redirect()->back()
            ->withFlashSuccess('Place Deleted Successfully!');
    }

}
