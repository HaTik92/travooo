<?php

namespace App\Http\Controllers\Backend\Destinations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Destinations\GenericCitiesRequest;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Destinations\GenericTopCities;
use App\Repositories\Backend\Destinations\GenericTopCitiesRepository;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Utilities\Request;

class GenericTopCitiesController extends Controller
{
    protected $generic_top_cities;

    /**
     * GenericTopCities constructor.
     */
    public function __construct(GenericTopCitiesRepository $generic_top_cities)
    {
        $this->generic_top_cities = $generic_top_cities;
    }

    public function index()
    {
        return view('backend.destinations.generic-cities.generic-top-cities');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table() {
        return Datatables::of($this->generic_top_cities->getForDataTable())
            ->addColumn('action', function ($generic_top_cities) {
                return $generic_top_cities->actions;
            })
            ->make(true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCitiesByCountry(Request $request) {
        /* Get Cities by Country id*/
        $country_id = $request->countryId;
        $cities = Cities::where(['active' => 1])->where('countries_id', $country_id)->with('transsingle')->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[] = [
                    'id' => $value->id,
                    'text' => $value->transsingle->title
                ];
            }
        }

        return new JsonResponse($cities_arr);
    }


    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create()
    {
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->where('countries_id', 1)->with('transsingle')->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        return view('backend.destinations.generic-cities.create-generic-top-cities', [
                        'countries' => $countries_arr,
                        'cities' => $cities_arr,
                        ]);
    }

    /**
     * @param GenericCitiesRequest $request
     * @return mixed
     */
    public function store(GenericCitiesRequest $request)
    {
        $extra = [
            'city_id' => $request->city_id,
            'country_id' => $request->country_id,
        ];

        $generic_city = $this->generic_top_cities->findByCityId($request->city_id);

        if ($generic_city) {
            return redirect()->back()->withErrors('The City Already Selected!');
        }

        $all_generic_cities = GenericTopCities::all();

        if(count($all_generic_cities) == 10){
            return redirect()->back()->withErrors('Maximum Cities List Are 10');
        }

        $this->generic_top_cities->create($extra);

        return redirect()->route('admin.generic-top-cities.index')->withFlashSuccess('Generic Top City Created!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $generic_city = $this->generic_top_cities->findById($id);
        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->where('countries_id', $generic_city->countries_id)->with('transsingle')->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Countries */
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }

        $data['selected_city'] = $generic_city->cities_id;
        $data['selected_country'] = $generic_city->countries_id;
        $data['all_cities'] = $cities_arr;
        $data['countries'] = $countries_arr;


        return view('backend.destinations.generic-cities.edit-generic-top-cities', $data)
            ->withCities($generic_city)
            ->withId($id);
    }

    /**
     * @param  $id
     * @param GenericCitiesRequest $request
     *
     * @return mixed
     */
    public function update($id, GenericCitiesRequest $request)
    {
        $extra = [
            'city_id' => $request->city_id,
            'country_id' => $request->country_id,
        ];

        $generic_city = $this->generic_top_cities->findByCityId($request->city_id);

        if ($generic_city) {
                    return redirect()->back()->withErrors('The City Already Selected!');
                }

        $this->generic_top_cities->update($id, $extra);

        return redirect()->back()
            ->withFlashSuccess('Generic Top Cities updated Successfully!');
    }


    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->generic_top_cities->delete($id);

        return redirect()->back()
            ->withFlashSuccess('City Deleted Successfully!');
    }

}
