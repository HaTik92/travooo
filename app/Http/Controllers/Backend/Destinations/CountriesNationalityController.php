<?php

namespace App\Http\Controllers\Backend\Destinations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Destinations\CountriesNationalityRequest;
use App\Models\Country\Countries;
use App\Models\Access\Language\Languages;
use App\Repositories\Backend\Destinations\CountriesNationalityRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Utilities\Request;

class CountriesNationalityController extends Controller
{
    protected $languages;
    protected $countries_nationality;

    /**
     * CountriesNationality constructor.
     */
    public function __construct(CountriesNationalityRepository $countries_nationality)
    {
        $this->countries_nationality = $countries_nationality;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    public function index(Request $request)
    {
        $top_countries = [
                    'France',
                    'United States',
                    'Spain',
                    'China',
                    'Italy',
                    'United Kingdom',
                    'Germany',
                    'Mexico',
                    'Thailand',
                    'Turkey',
                    'Austria',
                    'Malaysia',
                    'Hong Kong',
                    'Greece',
                    'Russia',
                    'Japan',
                    'Canada',
                    'Saudi Arabia',
                    'Poland',
                    'South Korea',
                    'Netherlands',
                    'Macao',
                    'Hungary',
                    'United Arab Emirates',
                    'India',
                    'Croatia',
                    'Ukraine',
                    'Singapore',
                    'Indonesia',
                    'Czech Republic'
        ];

        if($request->add_country && $request->add_country == 1){
                $countries = Countries::all();

                foreach($countries as $country){
                    shuffle($top_countries);
                    $i =0;
                    $list_ids = [];

                    foreach($top_countries as $top_counttry){
                        $get_country_id = Countries::with('transsingle')->whereHas('transsingle', function ($query) use ($top_counttry) {
                            $query->where('title', $top_counttry);
                        })->first();

                        if($get_country_id && $get_country_id->id != $country->id && $i<10){

                            $list_ids[]= $get_country_id->id;
                            $i++;
                        }
                    }

                    $extra = [
                        'nationality_id' => $country->id,
                        'countries_id' => $list_ids,
                    ];
                    $this->countries_nationality->create($extra);
                }

            dd('done');
        }

        return view('backend.destinations.countries-nationality.countries-by-nationality');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table() {
        return Datatables::of($this->countries_nationality->getForDataTable())
            ->addColumn('action', function ($countries_nationality) {
                return $countries_nationality->actions;
            })
            ->make(true);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create()
    {
        /* Get All Countries */
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }

        return view('backend.destinations.countries-nationality.create-countries-by-nationality', [
                        'countries' => $countries_arr
                        ]);
    }

    /**
     * @param CountriesNationalityRequest $request
     * @return mixed
     */
    public function store(CountriesNationalityRequest $request)
    {
        $extra = [
            'nationality_id' => $request->nationality_id,
            'countries_id' => $request->countries_id,
        ];

        $this->countries_nationality->create($extra);

        return redirect()->route('admin.countries-by-nationality.index')->withFlashSuccess('Countries by Nationality Created!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $countries = $this->countries_nationality->findByNationalityId($id);
        $all_countries_list = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];

        foreach ($all_countries_list as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }

        $get_country = Countries::find($id);
        $selected_countries = [];
        if($countries){
            foreach($countries as $country){
                    $selected_countries[] = $country->countries_id;
            }
        }

        $data['selected_nationality'] = $get_country->transsingle->title;
        $data['selected_countries'] = $selected_countries;
        $data['all_countries'] = $countries_arr;


        return view('backend.destinations.countries-nationality.edit-countries-by-nationality', $data)
            ->withCountries($countries)
            ->withNationalityid($id);
    }

    /**
     * @param  $id
     * @param CountriesNationalityRequest $request
     *
     * @return mixed
     */
    public function update($id, CountriesNationalityRequest $request)
    {

        $extra = [
            'nationality_id' => $id,
            'countries_id' => $request->countries_id,
        ];

        $this->countries_nationality->update($extra);

        return redirect()->back()
            ->withFlashSuccess('Countries updated Successfully!');
    }


    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {

    }

}
