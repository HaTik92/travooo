<?php

namespace App\Http\Controllers\Backend\Destinations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Destinations\GenericCountriesRequest;
use App\Models\Country\Countries;
use App\Models\Destinations\GenericTopCountries;
use App\Repositories\Backend\Destinations\GenericTopCountriesRepository;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Utilities\Request;

class GenericTopCountriesController extends Controller
{
    protected $generic_top_countries;

    /**
     * GenericTopCountries constructor.
     */
    public function __construct(GenericTopCountriesRepository $generic_top_countries)
    {
        $this->generic_top_countries = $generic_top_countries;
    }

    public function index()
    {
        return view('backend.destinations.generic-countries.generic-top-countries');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table() {
        return Datatables::of($this->generic_top_countries->getForDataTable())
            ->addColumn('action', function ($generic_top_countries) {
                return $generic_top_countries->actions;
            })
            ->make(true);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create()
    {
        /* Get All Countries */
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }

        return view('backend.destinations.generic-countries.create-generic-top-countries', [
                        'countries' => $countries_arr,
                        ]);
    }

    /**
     * @param GenericCountriesRequest $request
     * @return mixed
     */
    public function store(GenericCountriesRequest $request)
    {
        $extra = [
            'country_id' => $request->country_id,
        ];

        $generic_country = $this->generic_top_countries->findByCountryId($request->country_id);

        if ($generic_country) {
            return redirect()->back()->withErrors('The Country Already Selected!');
        }

        $all_generic_countries = GenericTopCountries::all();

        if(count($all_generic_countries) == 10){
            return redirect()->back()->withErrors('Maximum Countries List Are 10');
        }

        $this->generic_top_countries->create($extra);

        return redirect()->route('admin.generic-top-countries.index')->withFlashSuccess('Generic Top Country Created!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $generic_country = $this->generic_top_countries->findById($id);
        /* Get All Countries */
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }

        $data['selected_country'] = $generic_country->countries_id;
        $data['all_countries'] = $countries_arr;


        return view('backend.destinations.generic-countries.edit-generic-top-countries', $data)
            ->withCountries($generic_country)
            ->withId($id);
    }

    /**
     * @param  $id
     * @param GenericCountriesRequest $request
     *
     * @return mixed
     */
    public function update($id, GenericCountriesRequest $request)
    {
        $extra = $request->country_id;

        $generic_country = $this->generic_top_countries->findByCountryId($extra);

        if ($generic_country) {
                    return redirect()->back()->withErrors('The Country Already Selected!');
                }

        $this->generic_top_countries->update($id, $extra);

        return redirect()->back()
            ->withFlashSuccess('Generic Top Countries updated Successfully!');
    }


    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->generic_top_countries->delete($id);

        return redirect()->back()
            ->withFlashSuccess('Country Deleted Successfully!');
    }

}
