<?php

namespace App\Http\Controllers\Backend\ActivityTypes;

use App\Http\Requests\Backend\ActivityTypes\UpdateActivityTypesRequest;
use App\Models\ActivityTypes\ActivityTypes;
use App\Models\ActivityTypes\ActivityTypesTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\ActivityTypes\ManageActivityTypesRequest;
use App\Http\Requests\Backend\ActivityTypes\StoreActivityTypesRequest;
use App\Repositories\Backend\ActivityTypes\ActivityTypesRepository;
use Yajra\DataTables\Facades\DataTables;

class ActivityTypesController extends Controller
{
    protected $activitytypes;

    /**
     * ActivityTypesController constructor.
     * @param ActivityTypesRepository $activitytypes
     */
    public function __construct(ActivityTypesRepository $activitytypes)
    {
        $this->activitytypes = $activitytypes;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

     /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.activity-types.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->activitytypes->getForDataTable())
            ->addColumn('action', function ($activitytypes) {
                return view('backend.buttons', ['params' => $activitytypes, 'route' => 'activities.activitytypes']);
            })
            ->withTrashed()
            ->make(true);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.activity-types.create',[]);
    }

    /**
     * @param StoreActivityTypesRequest $request
     * @return mixed
     */
    public function store(StoreActivityTypesRequest $request)
    {   
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id); 
        }

        $this->activitytypes->create($data);

        return redirect()->route('admin.activities.activitytypes.index')->withFlashSuccess('Activity type Created!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $item = ActivityTypes::findOrFail($id);
        /* Delete Children Tables Data of this country */
        $child = ActivityTypesTranslations::where(['activities_types_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.activities.activitytypes.index')->withFlashSuccess('Actiivty Type Deleted Successfully');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $data = [];
        $activitytype = ActivityTypes::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = ActivityTypesTranslations::where([
                'languages_id' => $language->id,
                'activities_types_id'   => $id
            ])->first();

            if(!empty($model)){
                $data['title_'.$language->id]       = $model->title ?: null;
                $data['description_'.$language->id] = $model->description ?: null;
            }
        }

        return view('backend.activity-types.edit')
            ->withLanguages($this->languages)
            ->withActivitytype($activitytype)
            ->withActivitytypeid($id)
            ->withData($data);
    }

    /**
     * @param $id
     * @param ManageActivityTypesRequest $request
     * @return mixed
     */
    public function update($id, UpdateActivityTypesRequest $request)
    {   
        $activitytype = ActivityTypes::findOrFail($id);

        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $this->activitytypes->update($id , $activitytype, $data);

        return redirect()->route('admin.activities.activitytypes.index')
            ->withFlashSuccess('Activity Type updated Successfully!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {   
        $activitytype = ActivityTypes::findOrFail($id);
        $activitytypeTrans = ActivityTypesTranslations::where(['activities_types_id' => $id])->get();

        return view('backend.activity-types.show')
            ->withActivitytype($activitytype)
            ->withActivitytypetrans($activitytypeTrans);
    }

    /**
     * @param Countries $country
     * @param $status
     * @return mixed
     */
    public function mark(Countries $country, $status)
    {
        $country->active = $status;
        $country->save();
        return redirect()->route('admin.location.country.index')
            ->withFlashSuccess('Country Status Updated!');
    }
}