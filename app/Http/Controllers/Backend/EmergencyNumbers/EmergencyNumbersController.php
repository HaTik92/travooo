<?php

namespace App\Http\Controllers\Backend\EmergencyNumbers;

use App\Http\Requests\Backend\EmergencyNumbers\UpdateEmergencyNumbersRequest;
use App\Models\EmergencyNumbers\EmergencyNumbers;
use App\Models\EmergencyNumbers\EmergencyNumbersTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\EmergencyNumbers\ManageEmergencyNumbersRequest;
use App\Http\Requests\Backend\EmergencyNumbers\StoreEmergencyNumbersRequest;
use App\Repositories\Backend\EmergencyNumbers\EmergencyNumbersRepository;
use Yajra\DataTables\Facades\DataTables;


class EmergencyNumbersController extends Controller
{
    protected $emergencynumbers;

    /**
     * EmergencyNumbersController constructor.
     * @param EmergencyNumbersRepository $emergencynumbers
     */
    public function __construct(EmergencyNumbersRepository $emergencynumbers)
    {
        $this->emergencynumbers = $emergencynumbers;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

     /**
     * @param ManageEmergencyNumbersRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageEmergencyNumbersRequest $request)
    {
        $term = trim($request->q);
        if (!empty($term)) {

            $emergencyNumbers = EmergencyNumbers::whereHas('transsingle', function ($query) use ($term) {
                $query->where('title', 'LIKE', $term.'%');
            })->with('transsingle')->offset(($request->page -1 ) * 10)->limit(10)->get();

            $formatted_numbers = [];
            $paginate = (count($emergencyNumbers) < 10) ? false : true;

            foreach ($emergencyNumbers as $number) {
                $formatted_numbers[] = ['id' => $number->id, 'text' => $number->transsingle->title];
            }
            return response()->json(['data' => $formatted_numbers, 'paginate' => $paginate]);
        }
        return view('backend.emergencynumbers.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->emergencynumbers->getForDataTable())
            ->addColumn('action', function ($emergencynumbers) {
                return view('backend.buttons', ['params' => $emergencynumbers, 'route' => 'emergencynumbers']);
            })
            ->make(true);
    }

    /**
     * @param ManageEmergencyNumbersRequest $request
     * @return mixed
     */
    public function create(ManageEmergencyNumbersRequest $request)
    {
        return view('backend.emergencynumbers.create');
    }

    /**
     * @param StoreEmergencyNumbersRequest $request
     *
     * @return mixed
     */
    public function store(StoreEmergencyNumbersRequest $request)
    {   
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $this->emergencynumbers->create($data);

        return redirect()->route('admin.emergencynumbers.index')->withFlashSuccess('Emergency Numbers Created!');
    }

    /**
     * @param $id
     *
     * @param ManageEmergencyNumbersRequest $request
     * @return mixed
     */
    public function destroy($id, ManageEmergencyNumbersRequest $request)
    {      
        $item = EmergencyNumbers::findOrFail($id);
        /* Delete Children Tables Data of this emergencynumbers */
        $child = EmergencyNumbersTranslations::where(['emergency_numbers_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.emergencynumbers.index')->withFlashSuccess('Emergency Numbers Deleted Successfully');
    }

    /**
     * @param $id
     *
     * @param ManageEmergencyNumbersRequest $request
     * @return mixed
     */
    public function edit($id, ManageEmergencyNumbersRequest $request)
    {
        $data = [];
        $emergencynumber = EmergencyNumbers::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = EmergencyNumbersTranslations::where([
                'languages_id' => $language->id,
                'emergency_numbers_id'   => $id
            ])->first();

            if(!empty($model)){
                $data['title_'.$language->id]       = $model->title ?: null;
                $data['description_'.$language->id] = $model->description ?: null;
            }
        }

        return view('backend.emergencynumbers.edit')
            ->withLanguages($this->languages)
            ->withEmergencynumbers($emergencynumber)
            ->withEmergencynumbersid($id)
            ->withData($data);
    }

    /**
     * @param EmergencyNumbers $id
     * @param ManageEmergencyNumbersRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateEmergencyNumbersRequest $request)
    {   
        $emergencynumbers = EmergencyNumbers::findOrFail($id);
        
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $this->emergencynumbers->update($id , $emergencynumbers, $data);
        
        return redirect()->route('admin.emergencynumbers.index')
            ->withFlashSuccess('Emergency Numbers updated Successfully!');
    }

    /**
     * @param $id
     *
     * @param ManageEmergencyNumbersRequest $request
     * @return mixed
     */
    public function show($id, ManageEmergencyNumbersRequest $request)
    {   
        $emergencynumber = EmergencyNumbers::findOrFail($id);
        $emergencynumbersTrans = EmergencyNumbersTranslations::where(['emergency_numbers_id' => $id])->get();
       
        return view('backend.emergencynumbers.show')
            ->withEmergencynumbers($emergencynumber)
            ->withEmergencynumberstrans($emergencynumbersTrans);
    }
}