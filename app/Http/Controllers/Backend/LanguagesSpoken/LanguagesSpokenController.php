<?php

namespace App\Http\Controllers\Backend\LanguagesSpoken;

use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\Access\Language\Languages;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\LanguagesSpoken\ManageLanguagesSpokenRequest;
use App\Http\Requests\Backend\LanguagesSpoken\StoreLanguagesSpokenRequest;
use App\Repositories\Backend\LanguagesSpoken\LanguagesSpokenRepository;
use App\Models\LanguagesSpoken\LanguagesSpokenTranslation;
use App\Http\Requests\Backend\LanguagesSpoken\UpdateLanguagesSpokenRequest;
use Yajra\DataTables\Facades\DataTables;

class LanguagesSpokenController extends Controller
{
    protected $languages_spoken;
    protected $languages;

    /**
     * LanguagesSpokenController constructor.
     * @param LanguagesSpokenRepository $languages_spoken
     */
    public function __construct(LanguagesSpokenRepository $languages_spoken)
    {
        $this->languages_spoken = $languages_spoken;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->languages_spoken->getForDataTable())
            ->addColumn('action', function ($languages_spoken) {
                return view('backend.buttons', ['params' => $languages_spoken, 'route' => 'languagesspoken']);
            })
            ->make(true);
    }

     /**
     * @param ManageLanguagesSpokenRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageLanguagesSpokenRequest $request)
    {
        $term = trim($request->q);
        if (!empty($term)) {

            $languageSpokens = LanguagesSpoken::whereHas('transsingle', function ($query) use ($term) {
                $query->where('title', 'LIKE', $term.'%');
            })->where('active', 1)->with('transsingle')->offset(($request->page -1 ) * 10)->limit(10)->get();

            $formattedLanguageSpokens = [];
            $paginate = (count($languageSpokens) < 10) ? false : true;

            foreach ($languageSpokens as $languageSpoken) {
                $formattedLanguageSpokens[] = ['id' => $languageSpoken->id, 'text' => $languageSpoken->transsingle->title];
            }
            return response()->json(['data' => $formattedLanguageSpokens, 'paginate' => $paginate]);
        }
        return view('backend.languages_spoken.index');
    }

    /**
     * @param ManageLanguagesSpokenRequest $request
     * @return mixed
     */
    public function create(ManageLanguagesSpokenRequest $request)
    {
        return view('backend.languages_spoken.create')
            ->withLanguages($this->languages);
    }

    /**
     * @param LanguagesSpoken $language_spoken
     * @param ManageLanguagesSpokenRequest $request
     *
     * @return mixed
     */
    public function edit($id, ManageLanguagesSpokenRequest $request)
    {
        $language_spoken = LanguagesSpoken::findOrFail($id);
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $model = LanguagesSpokenTranslation::where([
                'languages_id' => $language->id,
                'languages_spoken_id'   => $language_spoken->id
            ])->first();

            if(!empty($model)){
                $data['title_'.$language->id]       = $model->title ?: null;
                $data['description_'.$language->id] = $model->description ?: null;
            }
        }
        
        $data['active'] = $language_spoken->active;

        return view('backend.languages_spoken.edit')
            ->withLanguages($this->languages)
            ->withLanguageSpoken($language_spoken)
            ->withLanguageSpokenId($id)
            ->withData($data);
    }

    /**
     * @param LanguagesSpoken $id
     * @param UpdateLanguagesSpokenRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateLanguagesSpokenRequest $request)
    {
        $language_spoken = LanguagesSpoken::findOrFail($id);
        $data = [];

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }
        $active = 2;

        if (!empty($request->input('active'))) {
            $active = 1;
        }

        $this->languages_spoken->update($id, $language_spoken, $data, $active);

        return redirect()->route('admin.languagesspoken.index')
            ->withFlashSuccess('Languages Spoken updated Successfully!');
    }

    /**
     * @param StoreLanguagesSpokenRequest $request
     *
     * @return mixed
     */
    public function store(StoreLanguagesSpokenRequest $request)
    {
        $data = [];

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $active = 2;
        if (!empty($request->input('active'))) {
            $active = 1;
        }

        $this->languages_spoken->create($data, $active );

        return redirect()->route('admin.languagesspoken.index')->withFlashSuccess('Languages Spoken Created!');
    }

    /**
     * @param $id
     * @param ManageLanguagesSpokenRequest $request
     * @return mixed
     */
    public function destroy($id, ManageLanguagesSpokenRequest $request)
    {
        $item = LanguagesSpoken::findOrFail($id);
        if(!empty($item)) {
            $model = LanguagesSpokenTranslation::where("languages_spoken_id", $id)->get();
            foreach ($model as $key => $trans) {
                $trans->delete();
            }
            $item->delete();

            return redirect()->route('admin.languagesspoken.index')
                ->withFlashSuccess("Languages Spoken Successfully Removed!");
        }

        throw new GeneralException('Invalid Request');
    }

    /**
     * @param $id
     * @param $status
     *
     * @param ManageLanguagesSpokenRequest $request
     * @return mixed
     * @internal param Languages $languagesspoken
     */
    public function mark($id, $status, ManageLanguagesSpokenRequest $request)
    {   
        $languagesspoken = LanguagesSpoken::findOrFail($id);
        $languagesspoken->active = $status;
        $languagesspoken->save();
        return redirect()->route('admin.languagesspoken.index')
            ->withFlashSuccess('Region Status Updated!');
    }

    /**
     * @param $id
     * @param ManageLanguagesSpokenRequest $request
     * @return mixed
     * @internal param Languages $language
     *
     */
    public function show($id, ManageLanguagesSpokenRequest $request)
    {   
        $languagesspoken = LanguagesSpoken::findOrFail($id);
        $languagespokenTrans = LanguagesSpokenTranslation::where(['languages_spoken_id' => $id])->get();
       

        return view('backend.languages_spoken.show')
            ->withLanguagespoken($languagesspoken)
            ->withLanguagespokentrans($languagespokenTrans);
    }
}