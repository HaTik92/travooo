<?php

namespace App\Http\Controllers\Backend\TopPlaces;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\City\CitiesTranslations;
use App\Models\Country\CountriesTranslations;
use App\Models\Place\Place;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Place\ManagePlaceRequest;
use App\Http\Requests\Backend\TopPlaces\StoreTopPlaceRequest;
use App\Http\Requests\Backend\TopPlaces\UpdateTopPlaceRequest;
use App\Models\PlacesTop\PlacesTop;
use App\Repositories\Backend\TopPlaces\TopPlaceRepository;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\AdminLogs\AdminLogs;
use App\Models\PlaceSearchHistory\PlaceSearchHistory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Utilities\Request;

class TopRestaurantsController extends Controller {

    use CreateUpdateStatisticTrait;

    protected $top_restaurants;
    private $placeTypesArray = [
        'bakery',
        'bar',
        'cafe',
        'meal_delivery',
        'meal_takeaway',
        'restaurant',
        'food'
    ];

    /**
     * PlaceController constructor.
     * @param TopPlaceRepository $top_restaurants
     */
    public function __construct(TopPlaceRepository $top_restaurants) {
        $this->top_restaurants = $top_restaurants;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        return view('backend.top_places.restaurant.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table(Request $request) {
        foreach ($request->columns as $column) {
            if ($column['data'] == 'address') {
                $matchValue     = @$column['search']['value1']['matchValue'];
                $notMatchValue  = @$column['search']['value2']['notMatchValue'];
            }
            if ($column['data'] == 'country_title') {
                $countryId = $column['search']['value'];
            }
            if ($column['data'] == 'city_title') {
                $cities_id = $column['search']['value'];
            }
            if ($column['data'] == 'place_id_title') {
                $placeType = $column['search']['value'];
            }
            if ($column['data'] == 'title') {
                $placeTitle = $column['search']['value'];
            }
        }

        $orderData = current($request->order);
        $orderColumn = $request->columnName($orderData['column']);
        $orderDir = $orderData['dir'];

        $query = PlacesTop::select([
            'places_top.id',
            DB::raw('MAX(places_top.places_id) as places_id'),
            DB::raw('MAX(places_top.city_id)'),
            DB::raw('MAX(places_top.country_id)'),
            DB::raw('MAX(places_top.order_by_city) as order_num'),
            DB::raw('MAX(places.place_type) as place_id_title'),
            DB::raw('MAX(places_top.reviews_num) as reviews_num'),
            DB::raw('MAX(places_top.rating) as rating'),
            DB::raw('MAX(places.active) as active'),
            DB::raw('MAX(places.media_count) as media_count'),
            DB::raw('MAX(places_top.title) as title'),
            DB::raw('MAX(transsingle.address) as address'),
            DB::raw('MAX(cities_trans.title) as city_title'),
            DB::raw('MAX(countries_trans.title) as country_title')
        ])
            ->where('places_top.places_id', '!=', 0)
            ->where('places_top.destination_type', 'restaurant')
            ->whereNotNull('places_top.places_id')
            ->leftJoin(DB::raw('places_trans AS transsingle FORCE INDEX (places_id)'), function ($query) {
                $query->on('transsingle.places_id', '=', 'places_top.places_id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->leftJoin('cities_trans', function ($query) {
                $query->on('cities_trans.cities_id', '=', 'places_top.city_id')
                    ->where('cities_trans.languages_id', '=', 1);
            })
            ->Leftjoin('countries_trans', function ($query) {
                $query->on('countries_trans.countries_id', '=', 'places_top.country_id')
                    ->where('countries_trans.languages_id', '=', 1);
            })
            ->Leftjoin('places', function ($query) {
                $query->on('places.id', '=', 'places_top.places_id')
                    ->where('countries_trans.languages_id', '=', 1);
            })
            ->when($matchValue, function ($query) use ($matchValue) {
                $this->filtered = true;
                $matchValues = explode("/", $matchValue );
                return $query->where(function ($query) use ($matchValues) {
                    foreach ($matchValues as $key => $matchValue) {
                        if ($key)
                            $query->orWhere('transsingle.address', 'LIKE', "%".$matchValue."%");
                        else
                            $query->where('transsingle.address', 'LIKE', "%".$matchValue."%");

                    }
                });
            })
            ->when($notMatchValue, function ($query) use ($notMatchValue) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue );
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue) {
                        $query->where('transsingle.address', 'NOT LIKE', "%" . $notMatchValue . "%");
                    }
                });
            })
            ->when($countryId, function ($query) use ($countryId) {
                $this->filtered = true;
                return $query->where('places_top.country_id', $countryId);
            })
            ->when($cities_id, function ($query) use ($cities_id) {
                $this->filtered = true;
                $query->orderBy('places_top.order_by_city', 'ASC');
                return $query->where('places_top.city_id', $cities_id);
            })
            ->when($placeType, function ($query) use ($placeType) {
                $this->filtered = true;
                return $query->where('places.place_type', $placeType);
            })
            ->when($placeTitle, function ($query) use ($placeTitle) {
                $this->filtered = true;
                return $query->where(function ($query) use ($placeTitle) {
                    $query->where('transsingle.title', 'LIKE', '%'.$placeTitle.'%')
                        ->orWhere('transsingle.address', 'LIKE', '%'.$placeTitle.'%');
                });
            });

        $query->groupBy('places_top.id');


        if (isset($mediaDoneNew))
        $response['recordsTotal'] = $query->get()->count();

        /*this is a wrong value for $response['recordsFiltered'] count, as $query->count() does not work for large data,
        will fix it after ElasticSearch instalation*/
        $response['recordsFiltered'] = isset($response) ? $response['recordsTotal'] : 1000000;
        $places = $query->offset($request->start)->limit($request->length)->orderBy($orderColumn, $orderDir)->get();

        foreach ($places as $place) {
            $place->empty = '';
            $place->action = $place->restaurant_action_buttons;
        }
        $response['data'] = $places;
        $response['draw'] = $request->draw;
        $response['input'] = $request->all();

        return response()->json($response);
    }

    /**
     * @return mixed
     */
    public function create() {

        return view('backend.top_places.restaurant.create');
    }

    /**
     * @param StoreTopPlaceRequest $request
     *
     * @return mixed
     */
    public function store(StoreTopPlaceRequest $request) {

        $city_id = $request->cities_id;
        $place_id = $request->place_id;

        $place_info = Place::findOrFail($place_id);
        if(!PlacesTop::where('places_id', $place_id)->exists()){

            $order  =0 ;
            $tp_for_order = PlacesTop::where('city_id',@$place_info->city->id)->where('destination_type', 'restaurant')->orderBy('order_by_city','DESC')->first();
            if(is_object($tp_for_order)){
                // $order = (isset($tp_for_order->order_by_city) && $tp_for_order->order_by_city !=null) ? $tp_for_order->order_by_city:0;
                $order= $tp_for_order->order_by_city+1;
            }
            $extra = [
                'places_id' => $place_id,
                'country' => @$place_info->country->transsingle->title,
                'country_id' => @$place_info->country->id,
                'city' => @$place_info->city->transsingle->title,
                'city_id' =>  @$place_info->city->id,
                'title' => @$place_info->transsingle->title,
                'travooo_title' => @$place_info->transsingle->title,
                'reviews_num' => @$place_info->reviews()->count(),
                'rating' => @$place_info->rating?$place_info->rating:0,
                'country_exists' => $place_info->country?1:0,
                'city_exists' => $place_info->city?1:0,
                'place_exists' => 1,
                'place_added' => 1,
                'place_notfound' => 0,
                'destination_type' => 'restaurant',
                'order_by_city'=>$order
            ];

            if($this->top_restaurants->create($extra)){
                return redirect()->route('admin.restaurants.index')->withFlashSuccess('Top Restaurant added!');
            }
        }else {
            return redirect()->route('admin.restaurants.create')->withFlashDanger('Restaurant already added');
        }
    }

    public function getAddedCountries(Request $request) {
        $q = null;
        $json = [];
        if ($request->has('q')) {
            $q = $request->q;
        }

        $places = PlacesTop::distinct()->select('country_id', 'country')->whereNotNull('country_id')
                             ->where('country', 'LIKE', '%' . $q . '%')->where('destination_type', 'restaurant')->take(30)->get();

        if (!empty($places)) {
            foreach ($places as $value) {
                $json[] = ['id' => $value->country_id, 'text' => $value->country];
            }
        }
        echo json_encode($json);
    }


    public function getAddedCities(Request $request) {
        $q = '';
        $country_id = $request->country;
        $json = [];

        if ($request->has('q')) {
            $q = $request->q;
        }

        $places = PlacesTop::distinct()->select('city_id', 'city')->whereNotNull('city_id')
                           ->where('city', 'LIKE', '%' . $q . '%')
                           ->where('destination_type', 'restaurant')
                           ->where(function($_q) use($country_id){
                               if($country_id != ''){
                                   $_q->where('country_id', $country_id);
                               }
                           })->take(30)->get();


        if (!empty($places)) {
            foreach ($places as $value) {
                    $json[] = ['id' => $value->city_id, 'text' => $value->city];
            }
        }

        echo json_encode(['data' => $json]);
    }

    /**
     * @param Request $request
     * @return response json
     */
    public function getRestaurantsByCity(Request $request)
    {
        $city_id = $request->city_id;
        $paginate = false;
        $selected_restaurants = [];

        if($city_id != ''){
            $restaurants = $this->top_restaurants->getPlacesById($request->q, $city_id);

            if(count($restaurants) > 0){
                foreach($restaurants as $r){
                    $rpt = explode(",", $r['place_type']);
                    if (in_array($rpt[0], $this->placeTypesArray)) {
                        $selected_restaurants[] = ['id' => @$r['id'], 'text' => $r['title']];
                    }
                }
            }

            $paginate = (count($restaurants) < 10) ? false : true;
        }

        return response()->json(['data' => $selected_restaurants, 'paginate' => $paginate]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function unmarkSelectedTopRestaurants(Request $request)
    {
        $ids = $request->ids;

        if (!empty($ids)) {
            $top_places_ids = explode(',', $ids);
            foreach ($top_places_ids as $top_place) {
                if(PlacesTop::find($top_place)->exists()){

                    PlacesTop::find($top_place)->delete();
                }
            }
        }

        echo json_encode([
            'result' => true
        ]);
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function unmarkTopRestaurants(Request $request)
    {
        $top_placeId = $request->id;

        $ifExist = PlacesTop::find($top_placeId)->exists();

        if ($ifExist){
            PlacesTop::where('id', '=', $top_placeId)->delete();

            return redirect()->route('admin.restaurants.index')->withFlashSuccess('Restaurant successfully unmark as a top!');
        } else {
            return redirect()->route('admin.restaurants.index')->withErrors('Restaurant not marked as a top!');
        }
    }
    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function updateSort(Request $request)
    {
        $top_placeId = $request->id;
        $new_order    = $request->val;
        $original    = PlacesTop::find($top_placeId);
        $current_order = $original->order_by_city;
        $increment = $new_order < $current_order  ? true : false;

        if($new_order < 0){
            return 0;
        }

        $last_order_number = PlacesTop::where('city_id', $original->city_id)->where('destination_type' , 'restaurant')->orderBy('order_by_city', 'DESC')->first()->order_by_city;

        if($new_order > $last_order_number){
            $new_order = $last_order_number;
        }


        if(is_object($original)){
            PlacesTop::where('city_id', $original->city_id)->where('destination_type' , 'restaurant')
                ->where(function($q) use ($increment, $new_order, $current_order) {
                    if($increment){
                        $q->where('order_by_city', '>=', $new_order);
                        $q->where('order_by_city', '<', $current_order);
                    }else{
                        $q->where('order_by_city', '<=', $new_order);
                        $q->where('order_by_city', '>', $current_order);
                    }
                })->get()->each(function ($hotel) use ($increment) {
                    $hotel->order_by_city = $increment ? $hotel->order_by_city + 1 : $hotel->order_by_city - 1;
                    $hotel->save();
                });

            $original->order_by_city = $new_order;
            $original->save();

            return 1;
        }

        return 0 ;
    }
}
