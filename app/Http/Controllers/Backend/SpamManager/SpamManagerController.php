<?php

namespace App\Http\Controllers\Backend\SpamManager;

use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Models\Reports\ConfirmedReports;
use App\Repositories\Backend\SpamsPosts\SpamsPostsRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Utilities\Request;
class SpamManagerController extends Controller
{
    protected $languages;
    protected $spamsPostsRepository;

    /**
     * SpamReportController constructor.
     */
    public function __construct(SpamsPostsRepository $spamsPostsRepository)
    {
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();

        $this->spamsPostsRepository = $spamsPostsRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request) {
        return view('backend.spam_manager.index', ['type' => $request->type]);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function history() {
        return view('backend.spam_manager.history');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table(Request $request) {
        return Datatables::of($this->spamsPostsRepository->getForDataTable($request->filter, $request->type))
            ->addColumn('',function(){
                return null;
            })
            ->addColumn('user', function ($spamsPostsRepository) {
                $user = $spamsPostsRepository->user;
                $data = [];
                if ($user) {
                    $data['id'] = $user->id;
                    $data['name'] = $user->name;
                    $data['expert'] = '';
                    if ($user && ((int)$user->type === 2 || (int)$user->expert === 1)) {
                        $data['expert'] = $user->expert;
                    }
                }
                return $data;
            })
            ->addColumn('author', function ($spamsPostsRepository) {
                $spamsPostsAuthor = $this->spamsPostsRepository->getPostOwnerById($spamsPostsRepository->id);
                if ($spamsPostsAuthor) {
                    $author['id'] = $spamsPostsAuthor->id;
                    $author['blocked'] = user_is_blocked($spamsPostsAuthor->id);
                    $author['history'] = ConfirmedReports::where('owner_id', $spamsPostsAuthor->id)->get()->toArray();
                    //flagged user if report count > 5
                    $author['flagged'] = ConfirmedReports::select(DB::raw('count(*) as count'))->where('created_at', '>', Carbon::now()->subMonth())
                        ->where('type', 'mark_as_safe')->where('owner_id', $spamsPostsAuthor->id)->
                        select('type', DB::raw('count(*) as count'))->having('count', '>', 5)->groupBy('owner_id')->get()->toArray();

                    $author['name'] = $spamsPostsAuthor->name;
                    $author['postsId'] = $spamsPostsRepository->posts_id;
                } else {
                    $author = ['id' => null ,'blocked'=> null, 'history' => null,  'name' => null, 'postsId' => null];
                }
                return $author;
            })
            ->addColumn('report_users', function ($spamsPostsRepository) {
                return $this->spamsPostsRepository->getPostReportUsers($spamsPostsRepository->posts_id);
            })
            ->make(true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function historyTable(Request $request)
    {
        return Datatables::of($this->spamsPostsRepository->historyTable($request))
            ->addColumn('',function(){
                return null;
            })
            ->addColumn('post', function ($spamsPostsRepository) {
                if($spamsPostsRepository->spamsPost) {
                    return $this->spamsPostsRepository->getPostById($spamsPostsRepository->spamsPost->id);
                }
                return null;
            })
            ->addColumn('posts_id', function ($spamsPostsRepository) {
                if($spamsPostsRepository->spamsPost) {
                    return $spamsPostsRepository->spamsPost->posts_id;
                }
                return null;
            })
            ->addColumn('post_type', function ($spamsPostsRepository) {
                if($spamsPostsRepository->spamsPost) {
                    return $spamsPostsRepository->spamsPost->post_type;
                }
                return null;
            })
            ->addColumn('date', function ($spamsPostsRepository) {
                 return Carbon::parse($spamsPostsRepository->created_at)->format('Y-m-d');
            })
            ->addColumn('report_type', function ($spamsPostsRepository) {
                return trans('report.' . $spamsPostsRepository->type . '');
            })
            ->make(true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsSafe(Request $request) {
        return $this->spamsPostsRepository->markAsSafe((int)$request->id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeReportPost(Request $request) {
        return $this->spamsPostsRepository->removeReportPost((int)$request->id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeReportPostAndBlockUser(Request $request) {
        return $this->spamsPostsRepository->removeReportPostAndBlockUser((int)$request->id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function restoreRemovedPost(Request $request) {
        return $this->spamsPostsRepository->restoreRemovedPost((int)$request->id);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getApiReports() {
        return view('backend.spam_manager.api-reports');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiReportsTable(Request $request)
    {
        return Datatables::of($this->spamsPostsRepository->apiReportsTable())
            ->addColumn('actions', function ($user) {
                return 'actions';
            })
            ->make(true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function restoreApiReportPost(Request $request)
    {
        return $this->spamsPostsRepository->restoreApiReportPost($request->input('id'), $request->input('type'));
    }

    /**
     * @param $id
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function show($id)
    {
        return $this->spamsPostsRepository->show($id);
    }
}