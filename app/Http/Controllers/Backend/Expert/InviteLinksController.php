<?php

namespace App\Http\Controllers\Backend\Expert;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\Expert\InviteLinksRepository;
use App\Services\Experts\InviteLinksService;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class InviteLinksController extends Controller
{
    /**
     * @var InviteLinksRepository
     */
    protected $inviteLinksRepository;

    /**
     * ExpertsController constructor.
     * @param InviteLinksRepository $inviteLinksRepository
     */
    public function __construct(InviteLinksRepository $inviteLinksRepository)
    {
        $this->inviteLinksRepository = $inviteLinksRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function table(Request $request)
    {
        return Datatables::of($this->inviteLinksRepository->getForDataTable())
            ->addColumn('actions', function ($user) {
                return 'actions';
            })
            ->make(true);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index(Request $request)
    {
        return view('backend.access.invite-links');
    }

    public function create(Request $request)
    {
        return view('backend.access.invite-links-create');
    }

    public function store(Request $request, InviteLinksService $inviteLinksService)
    {
        $name = $request->get('name');
        $badge = $request->get('badge');
        //todo unique name validator
        $inviteLinksService->create($name, $badge);

        return redirect()->route('admin.invite-links.index');
    }
}
