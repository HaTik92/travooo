<?php

namespace App\Http\Controllers\Backend\Expert;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use App\Repositories\Backend\Access\Expert\ExpertsRepository;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Services\Experts\ExpertsService;
use App\Services\Ranking\RankingService;
use App\Services\Users\UsersService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ExpertsController extends Controller
{
    /**
     * @var ExpertsRepository
     */
    protected $expertsRepository;

    /**
     * ExpertsController constructor.
     * @param ExpertsRepository $expertsRepository
     */
    public function __construct(ExpertsRepository $expertsRepository)
    {
        $this->expertsRepository = $expertsRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function table(Request $request, RankingService $rankingService)
    {
        $filters = $request->get('filters', []);

        $query = Datatables::of($this->expertsRepository->getForDataTable($request->get('status'), $request->get('approved'), $request->get('invited'), $request->get('trashed'), $filters))
            ->escapeColumns(['name', 'email'])
            ->addColumn('',function(){
                return null;
            })
            ->editColumn('confirmed', function ($user) {
                return $user->confirmed_label;
            })
            ->addColumn('roles', function ($user) {
                return $user->roles->count() ?
                    implode('<br/>', $user->roles->pluck('name')->toArray()) :
                    trans('labels.general.none');
            });

        if (isset($filters['applied']) && $filters['applied']) {
            $query->addColumn('next_badge', function($user) use ($rankingService) {
                return $rankingService->getNextBudge($user->id);
            });

            $query->addColumn('actions', function ($user) {
                return $user->expert_action_apply_buttons;
            });
        } else {
            $query->addColumn('actions', function ($user) {
                return $user->expert_action_buttons;
            });
        }

        return $query
            ->make(true);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index(Request $request)
    {
        return view('backend.access.experts');
    }

    /**
     * @param User $expert
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function show(User $expert, Request $request)
    {
        return view('backend.access.edit-experts', ['user' => $expert]);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create(Request $request)
    {
        return view('backend.access.create-experts');
    }

    public function store(Request $request, UserRepository $userRepository)
    {

        $request->validate([
            'email' => 'required|unique:users',
        ]);

        $userRepository->create([
            'data' => [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'status' => $request->input('status', \App\Models\User\User::STATUS_INACTIVE),
                'confirmed' => $request->input('confirmed', 0),
                'confirmation_email' => $request->input('confirmation_email'),
                'address' => $request->input('address'),
                'single' => $request->input('single'),
                'gender' => $request->input('gender'),
                'children' => $request->input('children'),
                'birth_date' => $request->input('birth_date'),
                'age' => $request->input('age'),
                'mobile' => $request->input('server_phone'),
                'nationality' => $request->input('nationality'),
                'public_profile' => $request->input('public_profile'),
                'notifications' => $request->input('notifications'),
                'messages' => $request->input('messages'),
                'username' => $request->input('username'),
                'sms' => $request->input('sms'),
                'type' => User::TYPE_INVITED_EXPERT,
                'website' => $request->input('website'),
                'twitter' => $request->input('twitter'),
                'facebook' => $request->input('facebook'),
                'youtube' => $request->input('youtube'),
                'instagram' => $request->input('instagram'),
                'pinterest' => $request->input('pinterest'),
                'tumblr' => $request->input('tumblr'),
                'interests' => $request->input('interests'),
                'is_approved' => 1,
                'register_steps' => 2,
            ],
            'roles' => [],
            'relations' => [
                'cities_countries' => $request->input('cities-countries', []),
                'places' => $request->input('places', []),
                'travel_type' => $request->input('travel_type'),
                'badge' => $request->input('badge'),
            ]
        ]);

        return redirect()->route('admin.experts.invited');
    }

    /**
     * @param User $expert
     * @param Request $request
     * @param ExpertsService $expertsService
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function update(User $expert, Request $request, ExpertsService $expertsService)
    {
        $points = $request->get('add-points');

        $expertsService->approve($expert->getKey(), $points);

        $expert->refresh();

        return redirect()->route('admin.experts.show', ['expert' => $expert]);
    }


    /**
     * @param int  $id
     * @return mixed
     */
    public function destroy($id) {
        $user = User::findOrFail($id);

        $user->forceDelete();

        return redirect()->back()->withFlashSuccess('The Expert was successfully deleted.');
    }

    /**
     * @param User $expert
     * @param Request $request
     * @param RankingService $rankingService
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function changeBadge(User $expert, Request $request, RankingService $rankingService)
    {
        $badge = $request->get('badge');

        $rankingService->changeBadge($expert->getKey(), $badge);

        return JsonResponse::create(['status' => 'success']);
    }

    /**
     * @param User $expert
     * @param Request $request
     * @param ExpertsService $expertsService
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function approve(User $expert, Request $request, ExpertsService $expertsService)
    {
        $badge = $request->get('badge');

        $expertsService->approve($expert->getKey(), $badge);

        $expert->refresh();

        return JsonResponse::create(['status' => 'success']);
    }

    public function disapprove(User $expert, Request $request, ExpertsService $expertsService)
    {
        $message = $request->get('message');

        $expertsService->disapprove($expert->getKey(), $message);

        $expert->refresh();

        return JsonResponse::create(['status' => 'success']);
    }

    public function apply(User $expert, RankingService $rankingService)
    {
        $nextBadge = $rankingService->getNextBudge($expert->id);

        if ($nextBadge) {
            $currentBadge = $expert->badge;

            if (!$currentBadge) {
                $rankingService->changeBadge($expert->id, $nextBadge->id, false);
            } else {
                $currentBadge->badges_id = $nextBadge->id;
                $currentBadge->save();
            }

            $expert->apply_to_badge = 0;
            $expert->save();

            return JsonResponse::create(['status' => 'success']);
        }

        return JsonResponse::create(['status' => 'fail']);
    }

    public function disapply(User $expert, RankingService $rankingService)
    {
        $expert->apply_to_badge = 0;
        $expert->save();

        return JsonResponse::create(['status' => 'success']);
    }

    /**
     * @param Request $request
     * @param ExpertsService $expertsService
     * @return JsonResponse
     */
    public function approveExperts(Request $request, ExpertsService $expertsService)
    {
        $ids = explode(',', $request->ids);

        foreach ($ids as $id) {
            $expert = $this->expertsRepository->getExpertById($id);
            $expertsService->approve($expert->getKey(), $request->points);
            $expert->refresh();
        }

        return JsonResponse::create(['status' => 'success']);
    }

    /**
     * @param Request $request
     * @param ExpertsService $expertsService
     * @return JsonResponse
     */
    public function disapproveExperts(Request $request, ExpertsService $expertsService)
    {
        $ids = explode(',', $request->ids);

        foreach ($ids as $id) {
            $expert = $this->expertsRepository->getExpertById($id);

            $expertsService->disapprove($expert->getKey(), $request->get('message'));
            $expert->refresh();
        }

        return JsonResponse::create(['status' => 'success']);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function notApproved(Request $request)
    {
        $data = [
            'badges' => RankingService::getAvailableBadgesList(),
            'filters' => [
                'badge' => $request->get('badge'),
                'invited' => $request->get('invited')
            ]
        ];

        return view('backend.access.not-approved-experts', $data);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function pendingApproval(Request $request)
    {
        return view('backend.access.pending-approval-experts');
    }

    public function approved(Request $request)
    {
        $data = [
            'badges' => RankingService::getAvailableBadgesList(),
            'filters' => [
                'badge' => $request->get('badge'),
                'invited' => $request->get('invited')
            ]
        ];

        return view('backend.access.approved-experts', $data);
    }

    public function specialCaseExpertsapproved(Request $request)
    {
        $data = [
            'badges' => RankingService::getAvailableBadgesList(),
            'filters' => [
                'badge' => $request->get('badge'),
                'invited' => $request->get('invited')
            ]
        ];

        return view('backend.access.special-approved-experts', $data);
    }

    public function invited(Request $request)
    {
        $data = [
            'badges' => RankingService::getAvailableBadgesList(),
            'filters' => [
                'badge' => $request->get('badge'),
                'invited' => $request->get('invited'),
                'joined' => $request->get('joined')
            ]
        ];

        return view('backend.access.invited-experts', $data);
    }

    public function applied(Request $request)
    {
        $data = [
            'badges' => RankingService::getAvailableBadgesList(),
            'filters' => [
                'badge' => $request->get('badge'),
                'next-badge' => $request->get('next-badge'),
                'applied' => 1,
            ]
        ];

        return view('backend.access.applied-experts', $data);
    }

}
