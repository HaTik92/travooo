<?php

namespace App\Http\Controllers\Backend\Expert;

use App\Http\Controllers\Controller;
use App\Models\Ranking\RankingBadges;
use App\Repositories\Backend\Access\Expert\BadgesRepository;
use App\Services\Experts\InviteLinksService;
use App\Services\Ranking\RankingService;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class BadgesController extends Controller
{
    /**
     * @var BadgesRepository
     */
    private $badgesRepository;

    public function __construct(BadgesRepository $badgesRepository)
    {
        $this->badgesRepository = $badgesRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function table(Request $request)
    {
        return Datatables::of($this->badgesRepository->getForDataTable())
            ->addColumn('actions', function ($badge) {
                return $badge->actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index(Request $request)
    {
        return view('backend.access.badges');
    }

    /**
     * @param RankingBadges $badge
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function edit(RankingBadges $badge, Request $request)
    {
        return view('backend.access.edit-badges', ['badge' => $badge]);
    }

    /**
     * @param RankingBadges $badge
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create(RankingBadges $badge, Request $request)
    {
        return view('backend.access.create-badges');
    }

    public function update(RankingBadges $badge, Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'url' => 'required',
            'order' => 'required',
            'followers' => 'required',
            'points' => 'required'
        ]);

        $data['active'] = 1;

        $badge->update($data);

        return redirect()->route('admin.badges.index');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'url' => 'required',
            'order' => 'required',
            'followers' => 'required',
            'points' => 'required'
        ]);

        $data['active'] = 1;

        RankingBadges::query()->create($data);

        return redirect()->route('admin.badges.index');
    }
}
