<?php

namespace App\Http\Controllers\Backend\Country;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Country\ManageCountryRequest;
use App\Http\Requests\Backend\Country\StoreCountryRequest;
use App\Http\Requests\Backend\Country\UpdateCountryRequest;
use App\Models\Access\Language\Languages;
use App\Models\ActivityMedia\Media;
use App\Models\AdminLogs\AdminLogs;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Country\CountriesMedias;
use App\Models\Country\CountriesTranslations;
use App\Models\Country\CountriesAbouts;
use App\Models\Currencies\Currencies;
use App\Models\Lifestyle\Lifestyle;
use App\Models\Regions\Regions;
use App\Models\Religion\Religion;
use App\Repositories\Backend\Country\CountryRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

use App\Models\EmergencyNumbers\EmergencyNumbers;
use App\Models\Holidays\Holidays;
use App\Models\LanguagesSpoken\LanguagesSpoken;

class CountryController extends Controller
{
    protected $countries;

    public function __construct(CountryRepository $countries)
    {
        $this->countries = $countries;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param Countries $country
     * @param Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeCountryMedia(Countries $country, Media $media)
    {
        if ($country instanceof Countries) {
            if ($media instanceof Media) {
                $repository = new BaseRepository();

                /** @var CountriesMedias $image */
                $image = CountriesMedias::where(['countries_id' => $country->id, 'medias_id' => $media->id])->first();
                Media::destroy($image->medias_id);

                $image->delete();

                return $this->jsonResponse(['result' => $repository->deleteFromS3($media->url)]);
            }

            return $this->jsonResponse([], ['This Media is not an instance of Media'], null, 422);
        }

        return $this->jsonResponse([], ['This Country is not an instance of Countries'], null, 422);
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->countries->getForDataTable())
            ->escapeColumns(['code'])
            ->addColumn('action', function ($countries) {
                return view('backend.buttons', ['params' => $countries, 'route' => 'location.country']);
            })
            ->addColumn('',function(){
                return null;
            })
            ->make(true);
    }

    /**
     * @param ManageCountryRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageCountryRequest $request)
    {
        $term = trim($request->q);
        if (!empty($term)) {

            $countries = Countries::whereHas('transsingle', function ($query) use ($term) {
                $query->where('title', 'LIKE', $term.'%');
            })->where('active', 1)->with('transsingle')->offset(($request->page -1 ) * 10)->limit(10)->get();

            $formatted_countries = [];
            $paginate = (count($countries) < 10) ? false : true;

            foreach ($countries as $country) {
                $formatted_countries[] = ['id' => $country->id, 'text' => $country->transsingle->title];
            }
            return response()->json(['data' => $formatted_countries, 'paginate' => $paginate]);

        }
        return view('backend.country.index');
    }

    /**
     * @param ManageCountryRequest $request
     * @return mixed
     */
    public function create(ManageCountryRequest $request)
    {
        /* Get All Regions */
        $regions = Regions::where(['active' => 1])->with('transsingle')->get();
        $regions_arr = [];

        foreach ($regions as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $regions_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Lifestyles */
        $lifestyles = Lifestyle::with('transsingle')->get();
        $lifestyles_arr = [];

        foreach ($lifestyles as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $lifestyles_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Religions */
        $religions = Religion::where(['active' => 1])->with('transsingle')->get();
        $religions_arr = [];

        foreach ($religions as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $religions_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Currencies */
        $currencies = Currencies::where(['active' => 1])->with('transsingle')->get();
        $currencies_arr = [];

        foreach ($currencies as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $currencies_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->with('transsingle')->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All EmergencyNumbers */
        $emergency_numbers = EmergencyNumbers::with('transsingle')->get();
        $emergency_numbers_arr = [];

        foreach ($emergency_numbers as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $emergency_numbers_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Holidays */
        $holidays = Holidays::with('transsingle')->get();
        $holidays_arr = [];

        foreach ($holidays as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $holidays_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All LanguagesSpoken */
        $languages_spoken = LanguagesSpoken::where(['active' => 1])->with('transsingle')->get();
        $languages_spoken_arr = [];

        foreach ($languages_spoken as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $languages_spoken_arr[$value->id] = $value->transsingle->title;
            }
        }

        return view('backend.country.create', [
            'regions' => $regions_arr,
            'lifestyles' => $lifestyles_arr,
            'religions' => $religions_arr,
            'currencies' => $currencies_arr,
            'cities' => $cities_arr,
            'emergency_numbers' => $emergency_numbers_arr,
            'holidays' => $holidays_arr,
            'languages_spoken' => $languages_spoken_arr,
        ]);
    }

    /**
     * @param StoreCountryRequest $request
     *
     * @return mixed
     */
    public function store(StoreCountryRequest $request)
    {
        // dd($request->input());
        $lifestyles_id = (array)$request->input('lifestyles_id');
        if (!empty($lifestyles_id) && count($lifestyles_id) !== count(array_unique($lifestyles_id))) {
            return redirect()->back()->withErrors('Travel style should not  be duplicated on a country page.');
        }

        if (!empty($request->input('lifestyles_rating'))) {
            foreach ($request->input('lifestyles_rating') as $key => $rating) {
                if (($key && is_null($rating)) || !($rating >= 0) || !($rating <= 10)) {
                    return redirect()->back()->withErrors('Travel Style\'s rating should be mandatory and need to be 1-10');
                }
            }
        }

        if (!$this->_holidaysDateValidate($request->input('holidays_date'))) {
            return redirect()->back()->withErrors('Date of the Holiday is required');
        }

        $location = explode(',', $request->input('lat_lng'));
        /* Check if active field is enabled or disabled */
        $active = 1;
        if (empty($request->input('active')) || $request->input('active') == 0) {
            $active = 2;
        }

        /* Send All Relations and Common Fields Through $extra array */
        $extra = [
            'active' => $active,
            'region_id' => $request->input('region_id'),
            'code' => $request->input('code'),
            'iso_code' => $request->input('iso_code'),
            'lat' => isset($location[0]) && $location[0] > 0 ? $location[0] : 0,
            'lng' => isset($location[1]) && $location[1] > 0 ? $location[1] : 0,
            'currencies' => $request->input('currencies_id'),
            'cities' => $request->input('cities_id'),
            'emergency_numbers' => $request->input('emergency_numbers_id'),
            'holidays' => $request->input('holidays_id'),
            'holidays_date' => $request->input('holidays_date'),
            'languages_spoken' => $request->input('languages_spoken_id'),
            'additional_languages_spoken' => $request->input('additional_languages_spoken_id'),
            'lifestyles' => $request->input('lifestyles_id'),
            'lifestyles_rating' => $request->input('lifestyles_rating'),
            'religions' => $request->input('religions_id'),
            'license_images' => $request->license_images,
            'abouts' => $request->input('abouts')
        ];

        $this->countries->create($request->translations, $extra);

        return redirect()->route('admin.location.country.index')->withFlashSuccess('Country Created!');
    }

    /**
     * @param Countries $id
     *
     * @param ManageCountryRequest $request
     * @return mixed
     */
    public function destroy($id, ManageCountryRequest $request)
    {
        $item = Countries::findOrFail($id);
        $item->deleteTrans();
        $item->deleteMedias();
        $item->delete();

        return redirect()->route('admin.location.country.index')->withFlashSuccess('Country Deleted Successfully');
    }

    /**
     * @param Countries $id
     *
     * @param ManageCountryRequest $request
     * @return mixed
     */
    public function edit($id, ManageCountryRequest $request)
    {
        $data = [];
        $country = Countries::with('countryHolidays','transsingle')->find($id);
        $data['translations'] = [];
        foreach ($this->languages as $language) {

            if ( $translation = CountriesTranslations::where(['countries_id' => $id, 'languages_id' => $language->id])->first() ) {
                $data['translations'][] = $translation;
            } else {
                $empty_translation = new CountriesTranslations;
                foreach ($country->transsingle->toArray() as $key => $value) {
                    $empty_translation->$key = null;
                }
                unset($empty_translation->id);
                $empty_translation->languages_id    = $language->id;
                $empty_translation->countries_id    = $country->transsingle->countries_id;
                $empty_translation->transsingle     = $language;
                $data['translations'][]             = $empty_translation;
            }
        }
        $data['abouts'] = [];
        if( $abouts = CountriesAbouts::where(['countries_id' => $id]) ) {
            $data['abouts'] = CountriesAbouts::where(['countries_id' => $id])->get()->toArray();
        }

        $data['lat_lng'] = $country['lat'] . ',' . $country['lng'];
        $data['code'] = $country['code'];
        $data['iso_code'] = $country['iso_code'];
        $data['active'] = $country['active'];
        $data['regions_id'] = $country['regions_id'];
        $data['safety_degree_id'] = $country['safety_degree_id'];

        $regions = Regions::where(['active' => 1])->with('transsingle')->get();
        $regions_arr = [];

        foreach ($regions as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $regions_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Selected Currencies */
        $selected_currencies = $country->currencies;
        $selected_currencies_arr = [];

        if (!empty($selected_currencies)) {
            foreach ($selected_currencies as $key => $value) {
                $selected_currencies_arr['ids'][] = $value->id;
                $currency = $value->currency;

                if (!empty($currency)) {
                    array_push($selected_currencies_arr, $currency->id);
                }
            }
        }
        $data['selected_currencies'] = $selected_currencies_arr;

        /* Get All Currencies For Dropdown */
        $currencies = Currencies::where(['active' => 1])->get();
        $currencies_arr = [];

        foreach ($currencies as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $currencies_arr[$value->id] = $value->transsingle->title;
            }
        }

        $data['selected_currencies']['all_currencies'] = $currencies_arr;

        /* Get Selected Cities */
        $selected_capitals = $country->capitals;
        $selected_capitals_arr = [];

        if (!empty($selected_capitals)) {
            foreach ($selected_capitals as $key1 => $value1) {
                $data['selected_capitals']['ids'][] = $value1->cities_id;
            }
        }

        /* Get All Cities For Dropdown*/
        $cities = Cities::where(['active' => 1])->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        $data['selected_capitals']['capital'] = $cities_arr;

        /* Get Selected Numbers */
        $selected_numbers = $country->emergency;
        $selected_numbers_arr = [];

        if (!empty($selected_numbers)) {
            foreach ($selected_numbers as $key => $value) {
                $selected_numbers_arr['ids'][] = $value->id;
            }
        }

        $data['selected_numbers'] = $selected_numbers_arr;

        /* Get All EmergencyNumbers For Dropdown */
        $emergency_numbers = EmergencyNumbers::get();
        $emergency_numbers_arr = [];

        foreach ($emergency_numbers as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $emergency_numbers_arr[$value->id] = $value->transsingle->title;
            }
        }

        $data['selected_numbers']['all_numbers'] = $emergency_numbers_arr;

        /* Get All Selected Holidays */
        $selected_holidays = $country->countryHolidays;
        $selected_holidays_arr = [];


        if (!empty($selected_holidays)) {
            foreach ($selected_holidays as $key => $value) {
                $selected_holidays_arr['ids'][] = $value->holidays_id;
                $selected_holidays_arr['holiday_date'][$value->holidays_id] = $value->date;
            }
        }

        $data['selected_holidays'] = $selected_holidays_arr;

        /* Get All Holidays For Dropdown*/
        $holidays = Holidays::get();
        $holidays_arr = [];

        foreach ($holidays as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $holidays_arr[$value->id] = $value->transsingle->title;
            }
        }

        $data['selected_holidays']['all_holidays'] = $holidays_arr;
        $data['selected_holidays']['all_holidays_date'] = $holidays_arr;

        /* Get Selected Languages Spoken */
        $selected_languages_spoken = $country->languages;
        $selected_languages_spoken_arr = [];

        if (!empty($selected_languages_spoken)) {
            foreach ($selected_languages_spoken as $key => $value) {
                $selected_languages_spoken_arr['ids'][]                 = $value->id;
            }
        }

        $data['selected_languages_spoken'] = $selected_languages_spoken_arr;

        /* Get Selected Additional Languages Spoken */
        $selected_additional_languages_spoken = $country->additional_languages;
        $selected_additional_languages_spoken_arr = [];

        if (!empty($selected_additional_languages_spoken)) {
            foreach ($selected_additional_languages_spoken as $key => $value) {
                $selected_additional_languages_spoken_arr['ids'][]                 = $value->id;
            }
        }

        $data['selected_additional_languages_spoken_arr'] = $selected_additional_languages_spoken_arr;

        /* Get All LanguagesSpoken For Dropdown */
        $languages_spoken = LanguagesSpoken::where(['active' => 1])->get();
        $languages_spoken_arr = [];

        foreach ($languages_spoken as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $languages_spoken_arr[$value->id] = $value->transsingle->title;
            }
        }

        $data['all_languages'] = $languages_spoken_arr;

        /* Get Selected Lifestyles */
        $selected_lifestyles = $country->lifestyles;
        $selected_lifestyles_arr = [];

        if (!empty($selected_lifestyles)) {
            foreach ($selected_lifestyles as $key => $value) {
                $lifestyle = $value->lifestyle;

                if (!empty($lifestyle)) {
                    $selected_lifestyles_arr['ids'][] = $lifestyle->id;
                    $selected_lifestyles_arr['title'][$lifestyle->id] = $lifestyle->transsingle->title;
                    $selected_lifestyles_arr['rating'][$lifestyle->id] = $value->rating;
                }
            }
        }

        $data['selected_lifestyles'] = $selected_lifestyles_arr;

        /* Get All Lifestyles For Dropdown*/
        $lifestyles = Lifestyle::with('transsingle')->get();
        $lifestyles_arr = [];

        foreach ($lifestyles as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $lifestyles_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['selected_lifestyles']['all_lifestyles'] = $lifestyles_arr;
        $data['selected_lifestyles']['all_lifestyles_rating'] = $lifestyles_arr;
        $media_results = ($country->getMedias->isEmpty())? [] : $country->getMedias;

        /* Get Selected Religions */
        $selected_religions = $country->religions;
        $selected_religions_arr = [];

        if (!empty($selected_religions)) {
            foreach ($selected_religions as $key => $value) {
                $selected_religions_arr[] =  $value->id;
            }
        }

        $data['selected_religions'] = $selected_religions_arr;

        /* Get All Religions For Dropdown*/
        $religions = Religion::where(['active' => 1])->with('transsingle')->get();
        $religions_arr = [];

        foreach ($religions as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $religions_arr[$value->id] = $value->transsingle->title;
            }
        }
        
        $data['media_results'] = $media_results;
        $data['country'] = $country;

        return view('backend.country.edit', $data)
            ->withLanguages($this->languages)
            ->withCountry($country)
            ->withCountryid($id)
            ->withData($data)
            ->withRegions($regions_arr)
            ->withAdditional_languages_spoken($selected_additional_languages_spoken_arr)
            ->withLifestyles($lifestyles_arr)
            ->withReligions($religions_arr);
    }

    /**
     * @param Countries $id
     * @param ManageCountryRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateCountryRequest $request)
    {
        // dd($request->input());
        $lifestyles_id = (array)$request->input('lifestyles_id');
        if (!empty($lifestyles_id) && count($lifestyles_id) !== count(array_unique($lifestyles_id))) {
            return redirect()->back()->withErrors('Travel style should not  be duplicated on a country page.');
        }

        if (!empty($request->input('lifestyles_rating'))) {
            foreach ($request->input('lifestyles_rating') as $key => $rating) {
                if (is_null($rating) || !($rating >= 0) || !($rating <= 10)) {
                    return redirect()->back()->withErrors('Travel Style\'s rating should be mandatory and need to be 1-10');
                }
            }
        }

        if (!$this->_holidaysDateValidate($request->input('holidays_date'))) {
            return redirect()->back()->withErrors('Date of the Holiday is required');
        }

        $country        = Countries::findOrFail(['id' => $id]);
        $translations   = $request->translations;
        $location       = explode(',', $request->input('lat_lng'));

        /* Check if active field is enabled or disabled */
        $active = 1;
        if (empty($request->input('active')) || $request->input('active') == 0) {
            $active = 2;
        }
        $extra = [
            'active' => $active,
            'region_id' => $request->input('region_id'),
            'code' => $request->input('code'),
            'iso_code' => $request->input('iso_code'),
            'lat' => $location[0],
            'lng' => $location[1],
            'currencies' => $request->input('currencies_id'),
            'cities' => $request->input('cities_id'),
            'holidays' => $request->input('holidays_id'),
            'holidays_date' => $request->input('holidays_date'),
            'emergency_numbers' => $request->input('emergency_numbers_id'),
            'languages_spoken' => $request->input('languages_spoken_id'),
            'additional_languages_spoken' => $request->input('additional_languages_spoken_id'),
            'lifestyles' => $request->input('lifestyles_id'),
            'lifestyles_rating' => $request->input('lifestyles_rating'),
            'religions' => $request->input('religions_id'),
            'license_images' => [
                'images'    => $request->license_images,
                'deleted'   => $request->del
            ],
            'abouts' => $request->input('abouts'),
            'best_time' => $request->input('best_time'),
            'daily_costs' => $request->input('daily_costs'),
            'transportation' => $request->input('transportation'),
            'sockets_plugs' => $request->input('sockets_plugs')
        ];


        $this->countries->update($id, $country, $translations, $extra);

        return redirect()->route('admin.location.country.index')
            ->withFlashSuccess('Country updated Successfully!');
    }

    /**
     * @param Countries $id
     * @param ManageCountryRequest $request
     *
     * @return mixed
     */
    public function show($id, ManageCountryRequest $request)
    {
        $country = Countries::with( 'countryHolidays')->findOrFail($id);
        if($request->jsRequest) {
            return response()->json(data);
        }
        $countryTrans = CountriesTranslations::where(['countries_id' => $id])->get();
        $countryAbouts = CountriesAbouts::where(['countries_id' => $id])->get();
        /* Get Regions Information */
        $region = $country->region;
        $region = $region->transsingle;

        /* Get Safety Degrees Information */
        $safety_degree_temp = $country->degree;
        $safety_degree = null;
        if (!empty($safety_degree_temp)) {
            $safety_degree = $safety_degree_temp->transsingle;
        }

        /* Get All Selected Places For Airports */
        $airports = $country->airports;
        $airports_arr = [];

        if (!empty($airports)) {
            foreach ($airports as $key => $value) {
                $place = $value->place;

                if (!empty($place)) {
                    $place = $place->transsingle;

                    if (!empty($place)) {
                        array_push($airports_arr, $place->title);
                    }
                }
            }
        }

        /* Get All Selected Currencies */
        $currencies = $country->currencies;
        $currencies_arr = [];

        if (!empty($currencies)) {
            foreach ($currencies as $key => $value) {
                $currency = $value;

                if (!empty($currency)) {
                    $currency = $currency->transsingle;

                    if (!empty($currency)) {
                        array_push($currencies_arr, $currency->title);
                    }
                }
            }
        }

        /* Get All Selected Cities As Capitals */
        $capitals = $country->capitals;
        $capitals_arr = [];

        if (!empty($capitals)) {
            foreach ($capitals as $key => $value) {
                $capital = $value->city;

                if (!empty($capital)) {
                    $capital = $capital->transsingle;

                    if (!empty($capital)) {
                        array_push($capitals_arr, $capital->title);
                    }
                }
            }
        }

        /* Get All Emergency Numbers */
        $emergency_numbers = $country->emergency;
        $emergency_numbers_arr = [];

        if (!empty($emergency_numbers)) {
            foreach ($emergency_numbers as $key => $value) {
                $emergency_number = $value;

                if (!empty($emergency_number)) {
                    $emergency_number = $emergency_number->transsingle;

                    if (!empty($emergency_number)) {
                        array_push($emergency_numbers_arr, $emergency_number->title);
                    }
                }
            }
        }

        /* Get All Selected Holidays */
        $holidays = $country->holidays;
        $holidays_arr = [];

        if (!empty($holidays)) {
            foreach ($holidays as $key => $value) {
                $holiday = $value;

                if (!empty($holiday)) {
                    $holiday = $holiday->transsingle;

                    if (!empty($holiday)) {
                        array_push($holidays_arr, $holiday->title);
                    }
                }
            }
        }

        /* Get Selected LanguageSpoken */
        $languages_spoken = $country->languages;
        $languages_spoken_arr = [];

        if (!empty($languages_spoken)) {
            foreach ($languages_spoken as $key => $value) {
                $language_spoken = $value;

                if (!empty($language_spoken)) {
                    $language_spoken = $language_spoken->transsingle;

                    if (!empty($language_spoken)) {
                        array_push($languages_spoken_arr, $language_spoken->title);
                    }
                }
            }
        }

        /* Get Selected LanguageSpoken */
        $additional_languages_spoken = $country->additional_languages_spoken;
        $additional_languages_spoken_arr = [];

        if (!empty($additional_languages_spoken)) {
            foreach ($additional_languages_spoken as $key => $value) {
                $language_spoken = $value->language_spoken;

                if (!empty($language_spoken)) {
                    $language_spoken = $language_spoken->transsingle;

                    if (!empty($language_spoken)) {
                        array_push($additional_languages_spoken_arr, $language_spoken->title);
                    }
                }
            }
        }

        /* Get Selected CountriesLifestyles */
        $lifestyles = $country->lifestyles;
        $lifestyles_arr = [];

        if (!empty($lifestyles)) {
            foreach ($lifestyles as $key => $value) {
                $lifestyle = $value->lifestyle;

                if (!empty($lifestyle)) {
                    $lifestyle = $lifestyle->transsingle;

                    if (!empty($lifestyle)) {
                        $lifestyles_arr[] = [
                            'rating' => $value->rating,
                            'title' => $lifestyle->title
                        ];
                    }
                }
            }
        }

        /* Get Selected Medias */
        $medias = $country->medias;
        $image_urls = [];
        $medias_arr = [];

        if (!empty($medias)) {
            foreach ($medias as $key => $value) {
                $media = $value->media;

                if (!empty($media)) {
                    if ($media->type != Media::TYPE_IMAGE) {
                        $media = $media->transsingle;

                        if (!empty($media)) {
                            array_push($medias_arr, $media->title);
                        }
                    } else {
                        array_push($image_urls, $media->url);
                    }
                }
            }
        }

        $media_results = ($country->getMedias->isEmpty())? [] : $country->getMedias;

        /* Get Selected Religions */
        $religions = $country->religions;
        $religions_arr = [];

        if (!empty($religions)) {
            foreach ($religions as $key => $value) {
                $religion = $value;

                if (!empty($religion)) {
                    $religion = $religion->transsingle;

                    if (!empty($religion)) {
                        array_push($religions_arr, $religion->title);
                    }
                }
            }
        }

        /* Get Cover Image Of Country */
        $cover = null;
        if (!empty($country->cover)) {
            $cover = $country->cover;
        }

        return view('backend.country.show')
            ->withCountry($country)
            ->withCountrytrans($countryTrans)
            ->withCountryabouts($countryAbouts)
            ->withRegion($region)
            ->withDegree($safety_degree)
            ->withAirports($airports_arr)
            ->withCurrencies($currencies_arr)
            ->withCapitals($capitals_arr)
            ->withEmergencyNumbers($emergency_numbers_arr)
            ->withHolidays($holidays_arr)
            ->withHolidaysDate($country->countryHolidays)
            ->withLanguagesSpoken($languages_spoken_arr)
            ->withAdditionalLanguagesSpoken($additional_languages_spoken_arr)
            ->withLifestyles($lifestyles_arr)
            ->withMedias($medias_arr)
            ->withMediasImage($media_results)
            ->withImages($image_urls)
            ->withReligions($religions_arr)
            ->withCover($cover);
    }

    /**
     * @param Countries $country
     * @param $status
     *
     * @param ManageCountryRequest $request
     * @return mixed
     * @internal param Countries $countries
     */
    public function mark(Countries $country, $status, ManageCountryRequest $request)
    {
        $country->active = $status;
        $country->save();
        return redirect()->route('admin.location.country.index')
            ->withFlashSuccess('Country Status Updated!');
    }

    /**
     * @param ManageCountryRequest $request
     * @return string
     */
    public function jsoncities(ManageCountryRequest $request)
    {
        $country_id = $request->get('countryID');

        $cities_list = Cities::leftJoin('cities_trans', 'cities.id', '=', 'cities_trans.cities_id')
            ->where('cities.countries_id', $country_id)
            ->where('cities_trans.languages_id', 1)
            ->select('cities.id', 'cities_trans.title')
            ->orderBy('cities_trans.title', 'ASC')
            ->get()
            ->toArray();

        return json_encode($cities_list);
    }

    /**
     * @param ManageCountryRequest $request
     */
    public function delete_ajax(ManageCountryRequest $request)
    {
        $ids = $request->input('ids');
        if (!empty($ids)) {
            $ids = explode(',', $request->input('ids'));
            foreach ($ids as $key => $value) {
                $this->delete_single_ajax($value);
            }
        }

        echo json_encode([
            'result' => true
        ]);
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete_single_ajax($id)
    {
        $item = Countries::find($id);

        if (empty($item)) {
            return false;
        }

        $item->deleteTrans();
        $item->delete();

        AdminLogs::create(['item_type' => 'countries', 'item_id' => $id, 'action' => 'delete', 'time' => time(), 'admin_id' => Auth::user()->id]);
    }

    /**
     * @param $holidaysDate
     * @return bool
     */
    private function _holidaysDateValidate($holidaysDate)
    {
        if (!empty($holidaysDate)) {
            foreach ($holidaysDate as $key => $holidayDate) {
                if (is_null($holidayDate) && $key > 0) {
                    return false;
                }
            }
        }

        return true;
    }
}