<?php

namespace App\Http\Controllers\Backend\CommonPages;

use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Pages\ManagePagesRequest;
use App\Http\Requests\Backend\Pages\UpdateCommonPagesRequest;
use App\Models\CommonPage\CommonPage;
use App\Models\CommonPage\CommonPageTranslations;
use App\Repositories\Backend\CommonPages\CommonPagesRepository;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\DB;

use function PHPSTORM_META\type;
use Yajra\DataTables\Facades\DataTables;

class CommonPagesController extends Controller
{
    protected $pages;

    /**
     * PagesController constructor.
     * @param CommonPagesRepository $pages
     */
    public function __construct(CommonPagesRepository $pages)
    {
        $this->pages = $pages;
        $this->languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @return mixed
     */
    public function show($slug)
    {
        $data  = [];
        $slug = trim(strtolower($slug));
        $page = CommonPage::where('slug', $slug)->first();
        if ($page && $page->trans) {

            foreach ($this->languages as $key => $language) {
                $model = CommonPageTranslations::where([
                    'languages_id' => $language->id,
                    'common_page_id'   => $page->id
                ])->first();

                if (!empty($model)) {
                    $data['title_' . $language->id] = $model->title ?: null;
                    $data['description_' . $language->id] = $model->description ?: null;
                }
            }

            $data['active'] = $page->active;
            $data['slug']   = $page->slug;
            $data['id']     = $page->id;

            return view('backend.common-pages.index', [
                'page_title' => CommonPage::SLUG_MAPPING[$slug],
                'languages' => $this->languages,
                'data' => $data,
                'page' => $page
            ]);
        } else {
            return redirect()->back()->withFlashDanger('Sorry, page not found');
        }
    }

    /**
     * @param Pages $id
     * @param ManagePagesRequest $request
     * @return mixed
     */
    public function update($id, UpdateCommonPagesRequest $request)
    {
        $page = CommonPage::findOrFail($id);

        $active = CommonPage::DEACTIVE;
        $data = [];

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_' . $language->id] = $request->input('title_' . $language->id);
            $data[$language->id]['description_' . $language->id] = $request->input('description_' . $language->id);
        }

        if (!empty($request->input('active'))) {
            $active = CommonPage::ACTIVE;
        }

        /* Send All Relation and Common Fields Through $extra Array */
        $extra = [
            'active' => $active,
        ];

        $this->pages->update($id, $page, $data, $extra);

        return redirect()->route('admin.commonpages.show', $page->slug)
            ->withFlashSuccess('Pages updated Successfully!');
    }
}
