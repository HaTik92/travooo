<?php

namespace App\Http\Controllers\Backend\Currencies;

use App\Http\Requests\Backend\Currencies\UpdateCurrenciesRequest;
use App\Models\Currencies\Currencies;
use App\Models\Currencies\CurrenciesTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Currencies\ManageCurrenciesRequest;
use App\Http\Requests\Backend\Currencies\StoreCurrenciesRequest;
use App\Repositories\Backend\Currencies\CurrenciesRepository;
use Yajra\DataTables\Facades\DataTables;

class CurrenciesController extends Controller
{
    protected $currencies;

    public function __construct(CurrenciesRepository $currencies)
    {
        $this->currencies = $currencies;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

     /**
     * @param ManageCurrenciesRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageCurrenciesRequest $request)
    {
        $term = trim($request->q);
        if (!empty($term)) {
            $currencies = Currencies::whereHas('transsingle', function ($query) use ($term) {
                $query->where('title', 'LIKE', $term.'%')->where('languages_id', 1);
            })->where('active', 1)->with('transsingle')->offset(($request->page -1 ) * 10)->limit(10)->get();

            $formatted_currencies = [];
            $paginate = (count($currencies) < 10) ? false : true;

            foreach ($currencies as $currency) {
                $formatted_currencies[] = ['id' => $currency->id, 'text' => $currency->transsingle->title];
            }
            return response()->json(['data' => $formatted_currencies, 'paginate' => $paginate]);

        }
        return view('backend.currencies.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->currencies->getForDataTable())
            ->addColumn('action', function ($currencies) {
                return view('backend.buttons', ['params' => $currencies, 'route' => 'currencies']);
            })
            ->make(true);
    }

    /**
     * @param ManageCurrenciesRequest $request
     * @return mixed
     */
    public function create(ManageCurrenciesRequest $request)
    {
        return view('backend.currencies.create');
    }

    /**
     * @param StoreCurrenciesRequest $request
     *
     * @return mixed
     */
    public function store(StoreCurrenciesRequest $request)
    {   
        $data = [];

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
        }

        $active = 2;
        if($request->input('active') == 1){
            $active = 1;
        }

        $this->currencies->create($data,$active);

        return redirect()->route('admin.currencies.index')->withFlashSuccess('Currencies Created!');
    }

    /**
     * @param Currencies $id
     *
     * @param ManageCurrenciesRequest $request
     * @return mixed
     */
    public function destroy($id, ManageCurrenciesRequest $request)
    {      
        $item = Currencies::findOrFail($id);
        /* Delete Children Tables Data of this currency */
        $child = CurrenciesTranslations::where(['currencies_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.currencies.index')->withFlashSuccess('Currencies Deleted Successfully');
    }

    /**
     * @param Currencies $id
     *
     * @param ManageCurrenciesRequest $request
     * @return mixed
     */
    public function edit($id, ManageCurrenciesRequest $request)
    {
        $data = [];
        $currencies = Currencies::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = CurrenciesTranslations::where([
                'languages_id' => $language->id,
                'currencies_id'   => $id
            ])->first();

            if(!empty($model)){
                $data['title_'.$language->id] = $model->title ?: null;
            }
        }

        $data['active'] = $currencies->active;

        return view('backend.currencies.edit')
            ->withLanguages($this->languages)
            ->withCurrencies($currencies)
            ->withCurrenciesid($id)
            ->withData($data);
    }

    /**
     * @param Currencies $id
     * @param ManageCurrenciesRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateCurrenciesRequest $request)
    {   
        $currencies = Currencies::findOrFail($id);
        $post = $request->input();
        
        $data = [];
        $active = 2;
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
        }

        if( isset($post['active'])){
            $active = 1;
        }

        $this->currencies->update($id , $currencies, $data , $active);
        
        return redirect()->route('admin.currencies.index')
            ->withFlashSuccess('Currencies updated Successfully!');
    }

    /**
     * @param Currencies $id
     *
     * @param ManageCurrenciesRequest $request
     * @return mixed
     */
    public function show($id, ManageCurrenciesRequest $request)
    {   
        $currencies = Currencies::findOrFail($id);
        $currenciesTrans = CurrenciesTranslations::where(['currencies_id' => $id])->get();
       
        return view('backend.currencies.show')
            ->withCurrencies($currencies)
            ->withCurrenciestrans($currenciesTrans);
    }

    /**
     * @param Currencies $currencies
     * @param $status
     *
     * @param ManageCurrenciesRequest $request
     * @return mixed
     * @internal param Currencies $currency
     */
    public function mark(Currencies $currencies, $status, ManageCurrenciesRequest $request)
    {
        $currencies->active = $status;
        $currencies->save();
        return redirect()->route('admin.currencies.index')
            ->withFlashSuccess('Currencies Status Updated!');
    }
}