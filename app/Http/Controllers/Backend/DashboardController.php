<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Access\Language\ConfTranslationsProgress;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(ConfTranslationsProgress $confTranslationsProgress)
    {
        return view('backend.dashboard')->with(['progresses' => $confTranslationsProgress->get()]);
    }
}
