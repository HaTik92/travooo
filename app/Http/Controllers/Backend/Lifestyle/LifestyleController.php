<?php

namespace App\Http\Controllers\Backend\Lifestyle;

use App\Models\Access\Language\Languages;
use App\Models\Lifestyle\Lifestyle;
use App\Models\Lifestyle\LifestyleTrans;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Lifestyle\ManageLifestyleRequest;
use App\Http\Requests\Backend\Lifestyle\StoreLifestyleRequest;
use App\Repositories\Backend\Lifestyle\LifestyleRepository;
use App\Http\Requests\Backend\Lifestyle\UpdateLifestyleRequest;
use App\Models\ActivityMedia\Media;
use Yajra\DataTables\Facades\DataTables;

class LifestyleController extends Controller
{
    protected $lifestyle;
    protected $languages;

    /**
     * LifestyleController constructor.
     * @param LifestyleRepository $lifestyle
     */
    public function __construct(LifestyleRepository $lifestyle)
    {
        $this->lifestyle = $lifestyle;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManageLifestyleRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageLifestyleRequest $request)
    {
        return view('backend.lifestyle.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->lifestyle->getForDataTable())
            ->addColumn('action', function ($lifestyle) {
                return view('backend.buttons', ['params' => $lifestyle, 'route' => 'lifestyle']);
            })
            ->make(true);

    }

    /**
     * @param ManageLifestyleRequest $request
     * @return mixed
     */
    public function create(ManageLifestyleRequest $request)
    {
        return view('backend.lifestyle.create');
    }


    /**
     * @param $id
     * @param ManageLifestyleRequest $request
     * @return mixed
     * @internal param SafetyDegres $safetudegree
     *
     */
    public function edit($id, ManageLifestyleRequest $request)
    {
        $data = [];
        $lifestyle = Lifestyle::findOrFail($id);
        
        foreach ($this->languages as $key => $language) {
            $model = LifestyleTrans::where([
                'languages_id' => $language->id,
                'lifestyles_id'   => $id
            ])->first();

            if (!empty($model)) {
                $data['title_'.$language->id]       = $model->title ?: null;
            }
        }

        /* Get Selected Medias */
        $selected_medias     = $lifestyle->medias;
        $selected_images     = [];
        $selected_medias_arr = [];

        $media_results = [];

        if(isset($lifestyle->medias)){
            foreach($lifestyle->medias as $media){
                $medias_id = $media->medias_id;

                $media_results[] = Media::find($medias_id);
            }
        }
        
        $data['selected_medias'] = $selected_medias_arr;

        return view('backend.lifestyle.edit', compact('media_results'))
            ->withLanguages($this->languages)
            ->withLifestyle($lifestyle)
            ->withLifestyleid($id)
            ->withData($data)
            ->withImages($selected_images);
    }

    /**
     * @param $id
     * @param UpdateLifestyleRequest $request
     * @return mixed
     */
    public function update($id, UpdateLifestyleRequest $request)
    {
        $lifestyle = Lifestyle::findOrFail($id);

        $data = [];

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
        }

        $extra = [
            'media_cover_image'     => $request->input('media-cover-image'),
            'remove-cover-image'    => $request->input('remove-cover-image'),
            'delete-images'         => $request->input('delete-images'),
            'license_images' => [
                'images'    => $request->license_images,
                'deleted'   => $request->del
            ]
        ];

        $this->lifestyle->update($id , $lifestyle, $data, $extra);

        return redirect()->route('admin.lifestyle.index')
            ->withFlashSuccess('Life Style updated Successfully!');
    }

    /**
     * @param StoreLifestyleRequest $request
     *
     * @return mixed
     */
    public function store(StoreLifestyleRequest $request)
    {   
        $data = [];
       
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
        }

        $files = null;
        if($request->hasFile('pictures')){
            $files = $request->file('pictures');
        }

        $extra = [];
        if (!empty($files)){
            foreach($files as $key => $file){
                $extra[$key]['image'] = $file;
            }
        }

        $this->lifestyle->create($data, $extra);

        return redirect()->route('admin.lifestyle.index')->withFlashSuccess('Lifestyle Created Successfully');
    }

    /**
     * @param Lifestyle $id
     *
     * @param ManageLifestyleRequest $request
     * @return mixed
     */
    public function destroy($id, ManageLifestyleRequest $request)
    {
        $item = Lifestyle::findOrFail($id);
        /* Delete Children Tables Data of this country */
        $child = LifestyleTrans::where(['lifestyles_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.lifestyle.index')->withFlashSuccess('Life Style Deleted Successfully');
    }

    /**
     * @param $id
     * @param ManageLifestyleRequest $request
     * @return mixed
     * @internal param Lifestyle $degree
     *
     */
    public function show($id, ManageLifestyleRequest $request)
    {   
        $lifestyle = Lifestyle::findOrFail($id);
        $lifestyleTrans = LifestyleTrans::where(['lifestyles_id' => $id])->get();
        
        /* Get Selected Medias */
        $medias     = $lifestyle->medias;
        $image_urls = [];
        $medias_arr = [];

        if(!empty($medias)){
            foreach ($medias as $key => $value) {
                $media = $value->media;

                if(!empty($media)){
                    if($media->type != Media::TYPE_IMAGE){
                        $media = $media->transsingle;

                        if(!empty($media)){
                            array_push($medias_arr,$media->title);
                        }
                    }else{
                        array_push($image_urls,$media->url);
                    }
                }
            }
        }

        return view('backend.lifestyle.show')
            ->withLifestyle($lifestyle)
            ->withLifestyletrans($lifestyleTrans);
    }
}
