<?php

namespace App\Http\Controllers\Backend\Embassies;

use App\Models\City\CitiesTranslations;
use App\Models\Country\CountriesTranslations;
use App\Models\Embassies\Embassies;
use App\Models\City\Cities;
use App\Models\AdminLogs\AdminLogs;
use Illuminate\Support\Facades\Auth;
use App\Models\Embassies\EmbassiesTranslations;
use App\Models\EmbassiesSearchHistory\EmbassiesSearchHistory;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Embassies\ManageEmbassiesRequest;
use App\Http\Requests\Backend\Embassies\StoreEmbassiesRequest;
use App\Http\Requests\Backend\Embassies\UpdateEmbassiesRequest;
use App\Repositories\Backend\Embassies\EmbassiesRepository;
use App\Models\Country\Countries;
use Yajra\DataTables\Facades\DataTables;

class EmbassiesController extends Controller
{
    protected $embassies;

    public function __construct(EmbassiesRepository $embassies)
    {
        $this->embassies = $embassies;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

     /**
     * @param ManageEmbassiesRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageEmbassiesRequest $request)
    {
        return view('backend.embassies.index');
    }

    /**
     * @param ManageEmbassiesRequest $request
     * @return mixed
     */
    public function table(ManageEmbassiesRequest $request)
    {
        $embassiesDatatable = Datatables::of($this->embassies->getForDataTable($request))
            ->addColumn('', function () {
                return null;
            })
            ->addColumn('action', function ($embassies) {
                return view('backend.buttons', ['params' => $embassies, 'route' => 'embassies']);
            })
            ->filterColumn(config('embassies.embassies_table').'.cities_id', function($query, $keyword) {
                $query->where(config('embassies.embassies_table').'.cities_id', $keyword);
            })
            ->make(true);

        return $embassiesDatatable;
    }

    /**
     * @param ManageEmbassiesRequest $request
     *
     * @return mixed
     */
    public function create(ManageEmbassiesRequest $request)
    {
        /* Get All Countries */
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];
        $countries_location_arr = [];
        /* Get All Regions */

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat'] = $value->lat;
                $countries_location_arr[$value->id]['lng'] = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        return view('backend.embassies.create',[
            'countries' => $countries_arr,
            'cities' => $cities_arr,
            'countries_location' => $countries_location_arr
        ]);
    }

    /**
     * @param StoreEmbassiesRequest $request
     *
     * @return mixed
     */
    public function store(StoreEmbassiesRequest $request)
    {
        $location = explode(',',$request->input('lat_lng') );

        /* Check if active field is enabled or disabled */
        $active = 1;

        if(empty($request->input('active')) || $request->input('active') == 0){
            $active = 2;
        }

        /* Send All Relations and Common fields Through $extra array */
        $extra = [
            'active'        => $active,
            'location_country_id' => $request->input('location_country_id'),
            'cities_id'     => $request->input('cities_id'),
            'lat'           => $location[0],
            'lng'           => $location[1],
            'license_images' => $request->license_images
        ];

        $this->embassies->create($request->translations, $extra);

        return redirect()->route('admin.embassies.index')->withFlashSuccess('Embassy Created!');
    }

    /**
     * @param Embassies $id
     *
     * @return mixed
     */
    public function destroy($id, ManageEmbassiesRequest $request)
    {
        $item = Embassies::findOrFail($id);
        $item->deleteTrans();
        if(!$item->getMedias->isEmpty()){
            foreach ($item->getMedias as $media) {
                $media->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.embassies.index')->withFlashSuccess('Embassies Deleted Successfully');
    }

    /**
     * @param Embassies $id
     * @param ManageEmbassiesRequest $request
     *
     * @return mixed
     */
    public function edit($id, ManageEmbassiesRequest $request)
    {
        $data = [];
        $embassies = Embassies::findOrFail($id);
        $data['translations'] = [];
        foreach ($this->languages as $language) {
            if ( $translation = EmbassiesTranslations::where(['embassies_id' => $id, 'languages_id' => $language->id])->first() ) {
                $data['translations'][] = $translation;
            } else {;
                $empty_translation = new EmbassiesTranslations;
                foreach ($embassies->transsingle->toArray() as $key => $value) {
                    $empty_translation->$key = null;
                }
                unset($empty_translation->id);
                $empty_translation->languages_id    = $language->id;
                $empty_translation->embassies_id    = $embassies->transsingle->embassies_id;
                $empty_translation->transsingle     = $language;
                $data['translations'][]             = $empty_translation;
            }
        }
        $data['lat_lng'] = $embassies['lat'] . ',' . $embassies['lng'];
        $data['active'] = $embassies['active'];
        $data['location_country_id'] = $embassies['location_country_id'];
        $data['origin_country_id'] = $embassies['origin_country_id'];
        $data['cities_id'] = $embassies['cities_id'];
        $data['place_types_ids'] = $embassies['place_type_ids'];
        $data['safety_degrees_id'] = $embassies['safety_degrees_id'];
        $data['city_id'] = $embassies['cities_id'];
        $city_title = Cities::find($embassies->cities_id)->transsingle->title;
        $data['selected_city_arr '] = [$embassies->cities_id => $city_title];


        /* Find All Active Countries */
        $countries              = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr          = [];
        $countries_location_arr = [];

        /* Get Title Id Pair For Each Model */
        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id]                      = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat']      = $value->lat;
                $countries_location_arr[$value->id]['lng']      = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }
        $media_results = ($embassies->getMedias->isEmpty())? [] : $embassies->getMedias;


        return view('backend.embassies.edit', compact('media_results'))
            ->withLanguages($this->languages)
            ->withEmbassies($embassies)
            ->withCountriesLocation($countries_location_arr)
            ->withEmbassiesid($id)
            ->withCountries($countries_arr)
            ->withData($data);
    }

    /**
     * @param Embassies $id
     * @param UpdateEmbassiesRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateEmbassiesRequest $request)
    {
        $embassies =  Embassies::findOrFail($id);

        $location = explode( ',' , $request->input('lat_lng') );

        /* Check if active field is enabled or disabled */
        $active = 1;

        if(empty($request->input('active')) || $request->input('active') == 0){
            $active = 2;
        }
        /* Send All Relation and Common Fields Through $extra Array */
        $extra = [
            'active'            => $active,
            'location_country_id' => $request->input('location_country_id'),
            'origin_country_id'   => $request->input('origin_country_id'),
            'cities_id'         => $request->input('cities_id'),
            'lat'               => $location[0],
            'lng'               => $location[1],
            'license_images' => [
                'images'    => $request->license_images,
                'deleted'   => $request->del
            ]
        ];

        $this->embassies->update($id , $embassies, $request->translations , $extra);

        return redirect()->route('admin.embassies.index')
            ->withFlashSuccess('Embassies updated Successfully!');
    }

    /**
     * @param Embassies $id
     * @param ManageEmbassiesRequest $request
     *
     * @return mixed
     */
    public function show($id, ManageEmbassiesRequest $request)
    {
        $embassies = Embassies::findOrFail($id);
        $embassiesTrans = EmbassiesTranslations::where(['embassies_id' => $id])->get();
        if ($embassies->origin_country_id > 0){
            $origin_country = $embassies->originCountry;
            $origin_country = $origin_country->transsingle;
        }else{
            $origin_country = '';
        }
        /* Get Regions Information */

        $cities = CitiesTranslations::where(['cities_id' => $embassies->cities_id])->first();
        $country = $embassies->country;
        $country = $country->transsingle;
        return view('backend.embassies.show')
            ->withEmbassies($embassies)
            ->withEmbassiestrans($embassiesTrans)
            ->withOriginCountry($origin_country)
            ->withCities($cities->title)
            ->withCountry($country);
    }

    /**
     * @param Embassies $embassies
     * @param $status
     *
     * @return mixed
     */
    public function mark(Embassies $embassies, $status)
    {
        $embassies->active = $status;
        $embassies->save();
        return redirect()->route('admin.embassies.index')
            ->withFlashSuccess('Embassies Status Updated!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function import() {
        $countries = Countries::where(['active' => 1])->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['countries'] = $countries_arr;

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->get();
        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['cities'] = $cities_arr;

        return view('backend.embassies.import', $data);
    }

    /**
     * @param int $admin_logs_id
     * @param int $country_id
     * @param int $city_id
     * @param int $latlng
     * @param ManageEmbassiesRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search($admin_logs_id = 0, $country_id = 0, $city_id = 0, $latlng = 0, ManageEmbassiesRequest $request) {
        $data['countries_id'] = $country_id ?: $request->input('countries_id');
        $data['cities_id'] = $city_id ?: $request->input('cities_id');

        $city = urlencode($request->input('address'));

        if ($admin_logs_id) {
            list($lat, $lng) = @explode(",", $latlng);

            $admin_logs = AdminLogs::find($admin_logs_id);
            $qu = $admin_logs->query;
            $queries = array_values(unserialize($qu));

            if (count($queries)) {
                $query = urlencode($queries[0]);
                unset($queries[0]);
                $queries = serialize($queries);
                $ad = AdminLogs::find($admin_logs_id);
                $ad->query = $queries;
                $ad->save();
            }
        } else {
            $queries = explode(",", $request->input('query'));
            $query = urlencode($queries[0]);
            unset($queries[0]);
            $queries = serialize($queries);

            list($lat, $lng) = @explode(",", $request->input('latlng'));

            $admin_logs = new AdminLogs();
            $admin_logs->item_type = 'embassies';
            $admin_logs->item_id = 0;
            $admin_logs->action = 'search';
            $admin_logs->query = $queries;
            $admin_logs->time = time();
            $admin_logs->admin_id = Auth::user()->id;
            $admin_logs->save();
        }

        if (isset($query)) {
            $provider_ids = array();
            /*
            $get_provider_ids = Embassies::where('id', '>', 0)->select('provider_id')->get()->toArray();
            foreach ($get_provider_ids AS $gpi) {
                $provider_ids[] = $gpi['provider_id'];
            }
            $data['provider_ids'] = $provider_ids;
             * 
             */

//            $i = time() % 4;
//
//            if ($i == 0) {
//                $json = file_get_contents('http://db.travooodev.com/public/embassies/go/' . ($city ? $city : 0) . '/' . $lat . '/' . $lng . '/' . $query);
//            } elseif ($i == 1)  {
//                $json = file_get_contents('http://db.travooodev.com/public/embassies/go/' . ($city ? $city : 0) . '/' . $lat . '/' . $lng . '/' . $query);
//            } elseif ($i == 2)  {
//                $json = file_get_contents('http://db.travoooapi.com/public/embassies/go/' . ($city ? $city : 0) . '/' . $lat . '/' . $lng . '/' . $query);
//            } elseif ($i == 3)  {
                $json = file_get_contents('http://db.travoooapi.com/public/embassies/go/' . ($city ? $city : 0) . '/' . $lat . '/' . $lng . '/' . $query);
//            }
            $result = json_decode($json);

            EmbassiesSearchHistory::create([
                'lat' => $lat,
                'lng' => $lng,
                'time' => time(),
                'admin_id' => Auth::user()->id
            ]);

            $data['results'] = $result;
            $data['queries'] = $queries;
            if (isset($admin_logs) && is_object($admin_logs)) {
                $data['admin_logs_id'] = $admin_logs->id;
                $data['latlng'] = $lat . ',' . $lng;
            }

            return view('backend.embassies.importresults', $data);
        } else {
            return redirect()->route('admin.embassies.index')
                            ->withFlashSuccess('Done!');
        }
    }

    /**
     * @param ManageEmbassiesRequest $request
     * @return mixed
     */
    public function savesearch(ManageEmbassiesRequest $request) {
        $data['countries_id'] = $request->input('countries_id');
        $data['cities_id'] = $request->input('cities_id');
        $to_save = $request->input('save');
        $places = $request->input('place');

        if (is_array($to_save)) {
            foreach ($to_save AS $k => $v) {
                if (!Embassies::where('provider_id', '=', $places[$k]['provider_id'])->exists()) {
                    $p = new Embassies();
                    $p->place_type = $places[$k]['types'];
                    $p->provider_id = $places[$k]['provider_id'];
                    $p->location_country_id = $data['location_country_id'];
                    $p->cities_id = $data['cities_id'];
                    $p->lat = $places[$k]['lat'];
                    $p->lng = $places[$k]['lng'];
                    $p->pluscode = $places[$k]['plus_code'];
                    $p->rating = $places[$k]['rating'];
                    $p->active = 1;
                    $p->save();

                    $pt = new EmbassiesTranslations();
                    $pt->languages_id = 1;
                    $pt->embassies_id = $p->id;
                    $pt->title = $places[$k]['name'];
                    $pt->address = $places[$k]['address'];
                    if (isset($places[$k]['phone']))
                        $pt->phone = $places[$k]['phone'];
                    if (isset($places[$k]['website']))
                        $pt->description = $places[$k]['website'];
                    $pt->working_days = $places[$k]['working_days'] ? $places[$k]['working_days'] : '';
                    $pt->save();
                    AdminLogs::create(['item_type' => 'embassies', 'item_id' => $p->id, 'action' => 'import', 'query' => '', 'time' => time(), 'admin_id' => Auth::user()->id]);
                }
            }

            $num = count($to_save);

            if ($request->input('admin_logs_id')) {
                return redirect()->route('admin.embassies.search', array($request->get('admin_logs_id'),
                                    $request->get('countries_id'),
                                    $request->get('cities_id'),
                                    $request->get('latlng')))
                                ->withFlashSuccess($num . ' Embassies imported successfully!');
            } else {
                return redirect()->route('admin.embassies.index')
                                ->withFlashSuccess($num . ' Embassies imported successfully!');
            }
        } else {
            if ($request->input('admin_logs_id')) {
                return redirect()->route('admin.embassies.search', array($request->get('admin_logs_id'),
                                    $request->get('countries_id'),
                                    $request->get('cities_id'),
                                    $request->get('latlng')))
                                ->withFlashSuccess('You didnt select any items to import!');
            } else {
            return redirect()->route('admin.embassies.index')
                            ->withFlashError('You didnt select any items to import!');
            }
        }
    }

    /**
     * @param ManageEmbassiesRequest $request
     * @return string
     */
    public function return_search_history(ManageEmbassiesRequest $request) {
        $ne_lat = $request->get('ne_lat');
        $sw_lat = $request->get('sw_lat');
        $ne_lng = $request->get('ne_lng');
        $sw_lng = $request->get('sw_lat');

        $markers = EmbassiesSearchHistory::whereBetween('lat', array($sw_lat, $ne_lat))
                ->whereBetween('lng', array($sw_lng, $ne_lng))
                ->groupBy('lat', 'lng')
                ->select('lat', 'lng')
                ->get()
                ->toArray();
        return json_encode($markers);
    }

    /**
     * @param ManageEmbassiesRequest $request
     */
    public function delete_ajax(ManageEmbassiesRequest $request){

        $ids = $request->input('ids');

        if(!empty($ids)){
            $ids = explode(',',$request->input('ids'));
            foreach ($ids as $key => $value) {
                $this->delete_single_ajax($value);
            }
        }

        echo json_encode([
            'result' => true
        ]);
    }

    /**
     * @param Embassies $id
     *
     * @return mixed
     */
    public function delete_single_ajax($id) {
        $item = Embassies::find($id);
        if(empty($item)){
            return false;
        }
        $item->trans()->delete();
        $item->delete();

        AdminLogs::create(['item_type' => 'embassies', 'item_id' => $id, 'action' => 'delete', 'time' => time(), 'admin_id' => Auth::user()->id]);
    }

    public function getAddedCountries(){
        $q = null;
        if(isset($_GET['q'])){
            $q = $_GET['q'];
        }

        $embassies = Embassies::distinct()->select('location_country_id')->get();
        $temp_country = [];
        $filter_html = null;
        $json = [];

        if(!empty($embassies)){
            foreach ($embassies as $key => $value) {
                $country = null;
                if(empty($q)){
                    $country = Countries::find($value->location_country_id);

                }else{
                    $country = Countries::leftJoin('countries_trans', function($join){
                        $join->on('countries_trans.countries_id', '=', 'countries.id');
                    })->where('countries_trans.title', 'LIKE', '%'.$q.'%')->where(['countries.id' => $value->location_country_id])->first();

                }
                if(!empty($country)){
                    $transingle = CountriesTranslations::where(['countries_id' => $value->location_country_id])->first();

                    if(!empty($transingle)){
                        $filter_html .= '<option value="'.$value->location_country_id.'">'.$transingle->title.'</option>';
                        array_push($temp_country,$transingle->title);
                        $json[] = ['id' => $value->location_country_id, 'text' => $transingle->title];
                    }
                }
            }
        }
        echo json_encode($json);
    }

    public function getAddedCities(){
        $q = null;
        if(isset($_GET['q'])){
            $q = $_GET['q'];
        }

        $embassies = Embassies::distinct()->select('cities_id')->get();
        $temp_city = [];
        $city_filter_html = null;
        $json = [];

        if(!empty($embassies)){
            foreach ($embassies as $key => $value) {
                if(empty($q)){
                    $city = Cities::find($value->cities_id);

                }else{
                    $city = Cities::leftJoin('cities_trans', function($join){
                        $join->on('cities_trans.cities_id', '=', 'cities.id');
                    })->where('cities_trans.title', 'LIKE', '%'.$q.'%')->where(['cities.id' => $value->cities_id])->first();
                }
                if(!empty($city)){
                    $transingle = CitiesTranslations::where(['cities_id' => $value->cities_id])->first();

                    if(!empty($transingle)){
                        $city_filter_html .= '<option value="'.$value->cities_id.'">'.$transingle->title.'</option>';
                        array_push($temp_city,$transingle->title);
                        $json[] = ['id' => $value->cities_id, 'text' => $transingle->title];
                    }
                }
            }
        }

        echo json_encode($json);
    }

    public function getPlaceTypes(){
        $q = null;
        if(isset($_GET['q'])){
            $q = $_GET['q'];
        }

        if(empty($q)){
            $embassies = Embassies::distinct()->select('place_type')->get();
        }else{
            $embassies = Embassies::distinct()->select('place_type')->where('place_type', 'LIKE', '%'.$q.'%')->get();
        }

        $city_filter_html = null;
        $json = [];

        if(!empty($embassies)){
            foreach ($embassies as $key => $value) {
                $json[] = ['id' => $value->place_type, 'text' => $value->place_type];
            }
        }

        echo json_encode($json);
    }
}