<?php

namespace App\Http\Controllers\Backend\Logs;

use App\Http\Controllers\Controller;
use App\Models\Logs\RankCronLog;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\DataTables;

class LogsReaderController extends Controller
{
    public function index()
    {
        return view('backend.logs.index');
    }

    public function table()
    {
        return DataTables::of(RankCronLog::query())
            ->addColumn('r_type', function ($log) {
                return $log->rank_type === RankCronLog::RANK_TYPE_ALL ? RankCronLog::RANK_TYPE_ALL_TEXT : RankCronLog::RANK_TYPE_7_TEXT;
            })
            ->filterColumn('r_type', function($query, $keyword) {
                if (strpos(strtolower(RankCronLog::RANK_TYPE_7_TEXT), strtolower($keyword)) !== false) {
                    $query->where('rank_type', RankCronLog::RANK_TYPE_7);
                } elseif (strpos(strtolower(RankCronLog::RANK_TYPE_ALL_TEXT), strtolower($keyword)) !== false) {
                    $query->where('rank_type', RankCronLog::RANK_TYPE_ALL);
                }
            })->make(true);
    }

    public function truncateRankCronTable()
    {
        if (RankCronLog::truncate()) {
            return new JsonResponse(['success' => true]);
        }

        return new JsonResponse(['success' => false]);
    }
}
