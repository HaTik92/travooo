<?php

namespace App\Http\Controllers\Backend\Levels;

use App\Http\Requests\Backend\Levels\UpdateLevelsRequest;
use App\Models\Levels\Levels;
use App\Models\Levels\LevelsTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Levels\ManageLevelsRequest;
use App\Http\Requests\Backend\Levels\StoreLevelsRequest;
use App\Repositories\Backend\Levels\LevelsRepository;
use Yajra\DataTables\Facades\DataTables;

class LevelsController extends Controller
{
    protected $levels;

    /**
     * LevelsController constructor.
     * @param LevelsRepository $levels
     */
    public function __construct(LevelsRepository $levels)
    {
        $this->levels = $levels;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

     /**
     * @param ManageLevelsRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageLevelsRequest $request)
    {
        return view('backend.levels.index');
    }

    /**
     * @param ManageLevelsRequest $request
     *
     * @return mixed
     */
    public function create(ManageLevelsRequest $request)
    {
        return view('backend.levels.create',[]);
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->levels->getForDataTable())
            ->addColumn('action', function ($levels) {
                return view('backend.buttons', ['params' => $levels, 'route' => 'levels']);
            })
            ->withTrashed()
            ->make(true);
    }

    /**
     * @param StoreLevelsRequest $request
     *
     * @return mixed
     */
    public function store(StoreLevelsRequest $request)
    {   
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
        }

        $this->levels->create($data);

        return redirect()->route('admin.levels.index')->withFlashSuccess('Levels Created!');
    }

    /**
     * @param levels $id
     * @param ManageLevelsRequest $request
     *
     * @return mixed
     */
    public function destroy($id, ManageLevelsRequest $request)
    {      
        $item = Levels::findOrFail($id);
        /* Delete Children Tables Data of this country */
        $child = levelsTranslations::where(['levels_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.levels.index')->withFlashSuccess('Levels Deleted Successfully');
    }

    /**
     * @param activitytypes $id
     * @param ManageLevelsRequest $request
     *
     * @return mixed
     */
    public function edit($id, ManageLevelsRequest $request)
    {
        $data = [];
        $level = Levels::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = LevelsTranslations::where([
                'languages_id' => $language->id,
                'levels_id'   => $id
            ])->first();

            if(!empty($model)){
                $data['title_'.$language->id] = $model->title ?: null;
            }
        }

        
        return view('backend.levels.edit')
            ->withLanguages($this->languages)
            ->withLevels($level)
            ->withLevelsid($id)
            ->withData($data);
    }

    /**
     * @param levels              $id
     * @param ManageLevelsRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateLevelsRequest $request)
    {   
        $levels = Levels::findOrFail($id);
        
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
        }

        $this->levels->update($id , $levels, $data);

        return redirect()->route('admin.levels.index')
            ->withFlashSuccess('Level updated Successfully!');
    }

    /**
     * @param Levels              $id
     * @param ManageLevelsRequest $request
     *
     * @return mixed
     */
    public function show($id, ManageLevelsRequest $request)
    {   
        $levels = Levels::findOrFail($id);
        $levelsTrans = LevelsTranslations::where(['levels_id' => $id])->get();

        return view('backend.levels.show')
            ->withLevels($levels)
            ->withLevelstrans($levelsTrans);
    }
}