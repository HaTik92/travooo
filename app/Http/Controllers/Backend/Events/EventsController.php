<?php

namespace App\Http\Controllers\Backend\Events;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Events\ManageEventsRequest;
use App\Http\Requests\Backend\Events\StoreEventsRequest;
use App\Models\ActivityMedia\Media;
use App\Models\AdminLogs\AdminLogs;
use App\Models\City\CitiesTranslations;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Events\Events;
use App\Repositories\Backend\Events\EventsRepository;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Utilities\Request;
use Carbon\Carbon;

class EventsController extends Controller
{

    protected $eventsRepository;

    public function __construct(EventsRepository $eventsRepository)
    {
        $this->eventsRepository = $eventsRepository;
    }

    public function index(ManageEventsRequest $request)
    {
        return view('backend.events.index');
    }

    /**
     * @param ManageEventsRequest $request
     * @return mixed
     */
    public function api_index(ManageEventsRequest $request)
    {
        $eventUrlType = true;
        return view('backend.events.index')
            ->withEventUrlType($eventUrlType);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create()
    {
        /* Get All Countries */
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];
        $countries_location_arr = [];
        /* Get All Regions */

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat'] = $value->lat;
                $countries_location_arr[$value->id]['lng'] = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        return view('backend.events.create', [
            'countries' => $countries_arr,
            'cities' => $cities_arr,
            'countries_location' => $countries_location_arr
        ]);
    }

    /**
     * @param StoreEventsRequest $request
     * @return mixed
     */
    public function store(StoreEventsRequest $request)
    {
        $location = explode(',', $request->input('lat_lng'));
        /* Send All Relations and Common fields Through $extra array */
        $extra = [
            'title' => $request->input('name'),
            'category' => $request->input('category'),
            'description' => $request->input('description'),
            'start' => $request->input('start') ? Carbon::parse($request->input('start'))->toIso8601ZuluString() : null,
            'end' => $request->input('end') ? Carbon::parse($request->input('end'))->toIso8601ZuluString() : null,
            'countries_id' => $request->input('location_country_id'),
            'city_id'     => $request->input('cities_id'),
            'lat'           => $location[0],
            'lng'           => $location[1],
            'license_images' => $request->license_images
        ];

        $this->eventsRepository->create($request->translations, $extra);

        return redirect()->route('admin.events.index')->withFlashSuccess('Event Created!');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function table(Request $request)
    {
        return Datatables::of($this->eventsRepository->getForDataTable($request))
            ->escapeColumns(['code'])
            ->addColumn('action', function ($eventsRepository) {
                return view('backend.buttons', ['params' => $eventsRepository, 'route' => 'events']);
            })
            ->addColumn('',function(){
                return null;
            })
            ->make(true);
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function api_table(Request $request){
        return Datatables::of($this->eventsRepository->getForEventAPIDataTable($request))
            ->escapeColumns(['code'])
            ->addColumn('action', function ($eventsRepository) {
                return view('backend.buttons', ['params' => $eventsRepository, 'route' => 'events']);
            })
            ->addColumn('',function(){
                return null;
            })
            ->make(true);
    }

    /**
     * @param ManageEventsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_ajax(ManageEventsRequest $request){

        $ids = $request->input('ids');
        if (!empty($ids)) {
            $ids = explode(',', $request->input('ids'));
            foreach ($ids as $key => $value) {
                $this->delete_single_ajax($value);
            }
        }
        echo json_encode([
            'result' => true
        ]);
    }

    /**
     * @param $id
     * @param ManageEventsRequest $request
     * @return mixed
     */
    public function show($id, ManageEventsRequest $request)
    {
        $event = Events::find($id);

        $city = Cities::find($event->city_id);
        $cityTrans = CitiesTranslations::with('translanguage')->where(['cities_id' => $city->id])->first();
        $event->city = $cityTrans->title;
        $event->start = Carbon::parse()->toDateString($event->start);
        $event->end = Carbon::parse()->toDateString($event->end);
        $medias = $event->getMedias;
        $image_urls = [];
        $medias_arr = [];

        if (!empty($medias)) {
            foreach ($medias as $key => $media) {
                if (!empty($media)) {
                    if ($media->type != Media::TYPE_IMAGE) {
                        if (!empty($media)) {
                            array_push($medias_arr, $media->title);
                        }
                    } else {
                        array_push($image_urls, $media->url);
                    }
                }
            }
        }
        $data['media_results'] = ($event->getMedias->isEmpty())? [] : $event->getMedias;
        return view('backend.events.show', $data)
            ->withEvent($event)
            ->withMedias($medias_arr)
            ->withImages($image_urls)
            ->withCityTrans($cityTrans);
    }

    /**
     * @param $id
     * @param ManageEventsRequest $request
     * @return mixed
     */
    public function edit($id, ManageEventsRequest $request)
    {
//        /* $data array to pass data to view */
        $data = [];
        $event = Events::find($id);
        /* Get All Countries */
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];
        $countries_location_arr = [];
        $event->start = Carbon::parse()->toDateString($event->start);
        $event->end = Carbon::parse()->toDateString($event->end);
        /* Get All Regions */

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat'] = $value->lat;
                $countries_location_arr[$value->id]['lng'] = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->get();
        $cities_arr = [];
        $data['lat_lng'] = $event->lat . ',' . $event->lng;

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['media_results'] = ($event->getMedias->isEmpty())? [] : $event->getMedias;
        return view('backend.events.edit', $data)
            ->withData($data)
            ->withEvent($event)
            ->withCities($cities_arr)
            ->withCountries($countries_arr)
            ->withCountriesLocation($countries_location_arr);
    }

    /**
     * @param Cities $id
     * @param ManageCityRequest $request
     *
     * @return mixed
     */
    public function update($id, Request $request)
    {
        $location = explode(',', $request->input('lat_lng'));
        $extra = [
            'title' => $request->input('name'),
            'category' => $request->input('category'),
            'description' => $request->input('description'),
            'start' => $request->input('start') ? Carbon::parse($request->input('start'))->toIso8601ZuluString() : null,
            'end' => $request->input('end') ? Carbon::parse($request->input('end'))->toIso8601ZuluString() : null,
            'location_country_id' => $request->input('location_country_id'),
            'city_id'     => $request->input('cities_id'),
            'lat'           => $location[0],
            'lng'           => $location[1],
            'license_images' => [
                'images'    => $request->license_images,
                'deleted'   => $request->del
            ]
        ];

        $this->eventsRepository->update($id, $request->translations, $extra);

        return redirect()->back()
            ->withFlashSuccess('Event updated Successfully!');
    }
    /**
     * @param Events $id
     *
     * @return mixed
     */
    public function delete_single_ajax($id) {
        $item = Events::find($id);
        if(empty($item)){
            return false;
        }

        $item->delete();

        AdminLogs::create(['item_type' => 'events', 'item_id' => $id, 'action' => 'delete', 'time' => time(), 'admin_id' => Auth::user()->id]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = Events::findOrFail($id);
        if(!$item->getMedias->isEmpty()){
            foreach ($item->getMedias as $media) {
                $media->delete();
            }
        }
        $item->delete();
        return redirect()->back()->withFlashSuccess('Event Deleted!');
    }
}