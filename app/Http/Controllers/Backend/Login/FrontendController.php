<?php

namespace App\Http\Controllers\Backend\Login;

use App\Http\Controllers\Controller;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {

        if(access()->allow('view-backend')){
            return redirect('/admin/dashboard');
        }else{
            return redirect('/admin/login');
        }
    }

    /**
     * @return \Illuminate\View\View
     */
    public function macros()
    {
        return view('frontend.macros');
    }
}
