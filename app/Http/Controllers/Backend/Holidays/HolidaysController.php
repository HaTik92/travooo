<?php

namespace App\Http\Controllers\Backend\Holidays;

use App\Http\Requests\Backend\Holidays\UpdateHolidaysRequest;
use App\Models\Holidays\Holidays;
use App\Models\Holidays\HolidaysTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Holidays\ManageHolidaysRequest;
use App\Http\Requests\Backend\Holidays\StoreHolidaysRequest;
use App\Repositories\Backend\Holidays\HolidaysRepository;
use Yajra\DataTables\Facades\DataTables;

class HolidaysController extends Controller
{
    protected $holidays;

    /**
     * HolidaysController constructor.
     * @param HolidaysRepository $holidays
     */
    public function __construct(HolidaysRepository $holidays)
    {
        $this->holidays = $holidays;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

     /**
     * @param ManageHolidaysRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageHolidaysRequest $request)
    {
        $term = trim($request->q);
        if (!empty($term)) {

            $holidays = Holidays::whereHas('transsingle', function ($query) use ($term) {
                $query->where('title', 'LIKE', $term.'%');
            })->with('transsingle')->offset(($request->page -1 ) * 10)->limit(10)->get();

            $formatted_holidays = [];
            $paginate = (count($holidays) < 10) ? false : true;

            foreach ($holidays as $holiday) {
                $formatted_holidays[] = ['id' => $holiday->id, 'text' => $holiday->transsingle->title];
            }
            return response()->json(['data' => $formatted_holidays, 'paginate' => $paginate]);
        }
        return view('backend.holidays.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->holidays->getForDataTable())
            ->addColumn('action', function ($holiday) {
                return view('backend.buttons', ['params' => $holiday, 'route' => 'holidays']);
            })
            ->make(true);
    }

    /**
     * @param ManageHolidaysRequest $request
     * @return mixed
     */
    public function create(ManageHolidaysRequest $request)
    {
        return view('backend.holidays.create');
    }

    /**
     * @param StoreHolidaysRequest $request
     *
     * @return mixed
     */
    public function store(StoreHolidaysRequest $request)
    {   
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['slug_'.$language->id] = $request->input('slug_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $image = $request->license_images;

        $this->holidays->create($data, $image);

        return redirect()->route('admin.holidays.index')->withFlashSuccess('Holidays Created!');
    }

    /**
     * @param Holidays $id
     * @param ManageHolidaysRequest $request
     *
     * @return mixed
     */
    public function destroy($id, ManageHolidaysRequest $request)
    {      
        $item = Holidays::findOrFail($id);
        /* Delete Children Tables Data of this weekday */
        $child = HolidaysTranslations::where(['holidays_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.holidays.index')->withFlashSuccess('Holidays Deleted Successfully');
    }

    /**
     * @param Holidays $id
     * @param ManageHolidaysRequest $request
     *
     * @return mixed
     */
    public function edit($id, ManageHolidaysRequest $request)
    {
        $data = [];
        $holiday = Holidays::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = HolidaysTranslations::where([
                'languages_id' => $language->id,
                'holidays_id'   => $id
            ])->first();

            if(!empty($model)){
                $data['title_'.$language->id] = $model->title ?: null;
                $data['slug_'.$language->id]  = $model->slug ?: null;
                $data['description_'.$language->id] = $model->description ?: null;
            }
        }

        $media_results = ($holiday->getMedias->isEmpty())? [] : $holiday->getMedias;
        $rowmedia = $media_results;
        if(isset($rowmedia[0])){
            $rowmedia = $rowmedia[0];
        }

        return view('backend.holidays.edit')
            ->withLanguages($this->languages)
            ->withHolidays($holiday)
            ->withHolidaysid($id)
            ->withRowmedia($rowmedia)
            ->withData($data);
    }

    /**
     * @param Holidays $id
     * @param ManageHolidaysRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateHolidaysRequest $request)
    {   
        $holiday = Holidays::findOrFail($id);
        
        $data = [];
        $image = [];

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['slug_'.$language->id]  = $request->input('slug_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }


        $image['license_images'] = [
            'image'    => $request->license_images,
            'deleted'   => $request->del
        ];

        $this->holidays->update($id , $holiday, $data, $image);
        
        return redirect()->route('admin.holidays.index')
            ->withFlashSuccess('Holidays updated Successfully!');
    }

    /**
     * @param Holidays  $id
     * @param ManageHolidaysRequest $request
     *
     * @return mixed
     */
    public function show($id, ManageHolidaysRequest $request)
    {   
        $holiday = Holidays::findOrFail($id);
        $holidaysTrans = HolidaysTranslations::where(['holidays_id' => $id])->get();
       
        return view('backend.holidays.show')
            ->withHolidays($holiday)
            ->withHolidaystrans($holidaysTrans);
    }
}