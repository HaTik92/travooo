<?php

namespace App\Http\Controllers\Backend\Hotels;

/* Hotels Models */
use App\Models\Country\CountriesTranslations;
use App\Models\Hotels\Hotels;
use App\Models\Hotels\HotelsTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Models\HotelsSearchHistory\HotelsSearchHistory;
use App\Models\AdminLogs\AdminLogs;
use App\Models\Place\PlaceTranslations;
use Illuminate\Support\Facades\Auth;

/* Store and Update Requests*/
use App\Http\Requests\Backend\Hotels\ManageHotelsRequest;
use App\Http\Requests\Backend\Hotels\StoreHotelsRequest;
use App\Http\Requests\Backend\Hotels\UpdateHotelsRequest;

/*Repositories*/
use App\Repositories\Backend\Hotels\HotelsRepository;

/* Dropdown Models*/
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\Place\Place;
use App\Models\ActivityMedia\Media;

use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Utilities\Request;

class HotelsController extends Controller
{
    protected $hotels;

    /**
     * HotelsController constructor.
     * @param HotelsRepository $hotels
     */
    public function __construct(HotelsRepository $hotels)
    {
        $this->hotels = $hotels;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManageHotelsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageHotelsRequest $request)
    {
        return view('backend.hotels.index');
    }

    /**
     * @return mixed
     */
    public function table(Request $request)
    {
        $hotelsDatatable = Datatables::of($this->hotels->getForDataTable($request))
            ->addColumn('',function(){
                return null;
            })
            ->addColumn('action', function ($hotels) {
                return view('backend.buttons', ['params' => $hotels, 'route' => 'hotels']);
            })
            ->filterColumn(config('hotels.hotels_table').'.cities_id', function($query, $keyword) {
                $query->where(config('hotels.hotels_table').'.cities_id', $keyword);
            })
            ->make(true);

        return $hotelsDatatable;
    }

    /**hcj jn bdc bhjbh  windowsdd 7 with xp out of track
     * @param ManageHotelsRequest $request
     * @return mixed
     */
    public function create(ManageHotelsRequest $request)
    {
        /* Get All Active Countries For Dropdown */
        $countries = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr = [];
        $countries_location_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat'] = $value->lat;
                $countries_location_arr[$value->id]['lng'] = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }

        /* Get All Active Cities For Dropdown */
        $cities = Cities::where(['active' => 1])->with('transsingle')->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            /* If Translation Exists, Get Title */
            if(isset($value->transsingle) && !empty($value->transsingle)){
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Places For Dropdown */
        $places = Place::with('transsingle')->limit(30000)->get();;
        $places_arr = [];

        foreach ($places as $key => $value) {
            /* If Translation Exists, Get Title */
            if(isset($value->transsingle) && !empty($value->transsingle)){
                $places_arr[$value->id] = $value->transsingle->title;
            }
        }

        return view('backend.hotels.create',[
            'countries' => $countries_arr,
            'countries_location' => $countries_location_arr,
            'cities' => $cities_arr,
            'places' => $places_arr
        ]);
    }

    /**
     * @param StoreHotelsRequest $request
     *
     * @return mixed
     */
    public function store(StoreHotelsRequest $request)
    {
        /* Seperate Latitude and Longitude */
        $location = explode(',',$request->input('lat_lng') );

        /* Check if active field is enabled or disabled */
        $active = 1;

        if(empty($request->input('active')) || $request->input('active') == 0){
            $active = 2;
        }

        /* Send All Relation and Common Fields Through $extra Array */
        $extra = [
            'active'        => $active,
            'country_id'    => $request->input('country_id'),
            'city_id'       => $request->input('cities_id'),
            'place_id'      => $request->input('place_id'),
            'lat'           => $location[0],
            'lng'           => $location[1],
            'license_images' => $request->license_images
        ];

        $this->hotels->create($request->translations, $extra);

        return redirect()->route('admin.hotels.index')->withFlashSuccess('Hotel Created!');
    }

    /**
     * @param Hotels $id
     *
     * @param ManageHotelsRequest $request
     * @return mixed
     */
    public function destroy($id, ManageHotelsRequest $request)
    {
        $item = Hotels::findOrFail($id);
        /* Delete Children Tables Data of this Hotel */
        $child = HotelsTranslations::where(['hotels_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        if (!$item->getMedias->isEmpty()) {
            foreach ($item->getMedias as $media) {
                $media->delete();
            }
        }

        $item->delete();

        return redirect()->route('admin.hotels.index')->withFlashSuccess('Hotels Deleted Successfully');
    }

    /**
     * @param Hotels $id
     *
     * @param ManageHotelsRequest $request
     * @return mixed
     */
    public function edit($id, ManageHotelsRequest $request)
    {
        $hotel = Hotels::findOrFail($id);
        $data = [];
        $data['translations'] = [];
        foreach ($this->languages as $language) {

            if ( $translation = HotelsTranslations::where(['hotels_id' => $id, 'languages_id' => $language->id])->first() ) {
                $data['translations'][] = $translation;
            } else {
                $empty_translation = new HotelsTranslations;
                foreach ($hotel->transsingle->toArray() as $key => $value) {
                    $empty_translation->$key = null;
                }
                unset($empty_translation->id);
                $empty_translation->languages_id    = $language->id;
                $empty_translation->hotels_id    = $hotel->transsingle->hotels_id;
                $empty_translation->transsingle     = $language;
                $data['translations'][]             = $empty_translation;
            }
        }

        $data['lat_lng'] = $hotel['lat'] . ',' . $hotel['lng'];
        $data['country_id'] = $hotel['countries_id'];
        $data['city_id'] = $hotel['cities_id'];
        $data['place_id'] = $hotel['places_id'];
        $data['active'] = $hotel['active'];

        /* Get Active Countries For Dropdown */
        $countries              = Countries::where(['active' => 1])->with('transsingle')->get();
        $countries_arr          = [];
        $countries_location_arr = [];

        /* Get Title Id Pair For Each Model */
        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat'] = $value->lat;
                $countries_location_arr[$value->id]['lng'] = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }

        $cities_arr[$hotel->cities_id] = $hotel->city->transsingle->title;

        /* Get Places For Dropdown */
        $place = PlaceTranslations::where('places_id', $hotel['places_id'])->where('languages_id', 1)->first();

        if(isset($place->places_id) && isset($place->title)){
            $data['place'] = [$place->places_id => $place->title];
        } else {
            $data['place'] = null;
        }
        $media_results = ($hotel->getMedias->isEmpty())? [] : $hotel->getMedias;

        return view('backend.hotels.edit', compact('media_results'))
            ->withLanguages($this->languages)
            ->withHotel($hotel)
            ->withHotelid($id)
            ->withCountriesLocation($countries_location_arr)
            ->withData($data)
            ->withCountries($countries_arr)
            ->withCities($cities_arr);
    }

    /**
     * @param Hotels $id
     * @param ManageHotelsRequest|UpdateHotelsRequest $request
     * @return mixed
     */
    public function update($id, UpdateHotelsRequest $request)
    {
        $hotel = Hotels::findOrFail($id);

        $location = explode(',',$request->input('lat_lng') );

        /* Check if active field is enabled or disabled */
        $active = 1;

        if(empty($request->input('active')) || $request->input('active') == 0){
            $active = 2;
        }

        /* Send All Relation and Extra Fields Through $extra Array */
        $extra = [
            'active'            => $active,
            'country_id'        => $request->input('country_id'),
            'city_id'           => $request->input('cities_id'),
            'place_id'          => $request->input('place_id'),
            'lat'               => $location[0],
            'lng'               => $location[1],
            'license_images' => [
                'images'    => $request->license_images,
                'deleted'   => $request->del
            ]
        ];

        $this->hotels->update($hotel, $request->translations , $extra);

        return redirect()->route('admin.hotels.index')
            ->withFlashSuccess('Hotels updated Successfully!');
    }

    /**
     * @param Hotels $id
     * @param ManageHotelsRequest $request
     *
     * @return mixed
     */
    public function show($id, ManageHotelsRequest $request)
    {
        $hotel = Hotels::findOrFail($id);
        $hotelTrans = HotelsTranslations::where(['hotels_id' => $id])->get();

        /* Get Country Relation */
        if(isset($hotel->city)){
            $country = $hotel->country;
            $country = $country->transsingle;
        } else {
            $country = null;
        }

        /* Get City Relation */
        if(isset($hotel->city)){
            $city = $hotel->city;
            $city = $city->transsingle;
        } else {
            $city = null;
        }

        /* Get Place Relation */
        if(isset($hotel->place)){
            $place = $hotel->place;
            $place = $place->transsingle;
        } else {
            $place = null;
        }

        $medias = $hotel->medias;
        $medias_arr = [];

        if(!empty($medias)){
            foreach ($medias as $key => $value) {
                if(!empty($value->media)){
                    $media = $value->media;
                    if(!empty($media->transsingle)){
                        array_push($medias_arr, $media->transsingle->title);
                    }
                }
            }
        }

        /* Get Selected Medias */
        $medias = $country->medias;
        $image_urls = [];
        $medias_arr_2 = [];

        if(!empty($medias)){
            foreach ($medias as $key => $value) {
                $media = $value->media;

                if(!empty($media)){
                    if($media->type != Media::TYPE_IMAGE){
                        $media = $media->transsingle;

                        if(!empty($media)){
                            array_push($medias_arr_2,$media->title);
                        }
                    }else{
                        array_push($image_urls,$media->url);
                    }
                }
            }
        }

        return view('backend.hotels.show')
            ->withHotel($hotel)
            ->withHoteltrans($hotelTrans)
            ->withCountry($country)
            ->withCity($city)
            ->withPlace($place)
            ->withImages($image_urls)
            ->withMedias($medias_arr);
    }

    /**
     * @param Hotels $id
     * @param $status
     * @param ManageHotelsRequest $request
     *
     * @return mixed
     */
    public function mark($id, $status, ManageHotelsRequest $request)
    {
        $hotel = Hotels::findOrFail($id);

        $hotel->active = $status;
        $hotel->save();

        return redirect()->route('admin.hotels.index')
            ->withFlashSuccess('Hotels Status Updated!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function import() {
        $countries = Countries::where(['active' => 1])->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['countries'] = $countries_arr;

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->get();
        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['cities'] = $cities_arr;

        return view('backend.hotels.import', $data);
    }

    /**
     * @param int $admin_logs_id
     * @param int $country_id
     * @param int $city_id
     * @param int $latlng
     * @param ManageHotelsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search($admin_logs_id = 0, $country_id = 0, $city_id = 0, $latlng = 0, ManageHotelsRequest $request) {
        $data['countries_id'] = $country_id ?: $request->input('countries_id');;
        $data['cities_id'] = $city_id ?: $request->input('cities_id');;

        $city = urlencode($request->input('address'));

        if ($admin_logs_id) {
            list($lat, $lng) = @explode(",", $latlng);

            $admin_logs = AdminLogs::find($admin_logs_id);
            $qu = $admin_logs->query;
            $queries = array_values(unserialize($qu));

            if (count($queries)) {
                $query = urlencode($queries[0]);
                unset($queries[0]);
                $queries = serialize($queries);
                $ad = AdminLogs::find($admin_logs_id);
                $ad->query = $queries;
                $ad->save();
            }
        } else {
            $queries = explode(",", $request->input('query'));
            $query = urlencode($queries[0]);
            unset($queries[0]);
            $queries = serialize($queries);

            list($lat, $lng) = @explode(",", $request->input('latlng'));

            $admin_logs = new AdminLogs();
            $admin_logs->item_type = 'hotels';
            $admin_logs->item_id = 0;
            $admin_logs->action = 'search';
            $admin_logs->query = $queries;
            $admin_logs->time = time();
            $admin_logs->admin_id = Auth::user()->id;
            $admin_logs->save();
        }

        if (isset($query)) {
            $provider_ids = array();
            /*
            $get_provider_ids = Hotels::where('id', '>', 0)->select('provider_id')->get()->toArray();
            foreach ($get_provider_ids AS $gpi) {
                $provider_ids[] = $gpi['provider_id'];
            }
            $data['provider_ids'] = $provider_ids;
             * 
             */

            $json = file_get_contents('http://db.travoooapi.com/public/hotels/go/' . ($city ? $city : 0) . '/' . $lat . '/' . $lng . '/' . $query);
            $result = json_decode($json);
//echo 'http://db.travoooapi.com/public/hotels/go/' . ($city ? $city : 0) . '/' . $lat . '/' . $lng . '/' . $query;
//dd();
            HotelsSearchHistory::create([
                'lat' => $lat,
                'lng' => $lng,
                'time' => time(),
                'admin_id' => Auth::user()->id
            ]);

            $data['results'] = $result;
            $data['queries'] = $queries;
            if (isset($admin_logs) && is_object($admin_logs)) {
                $data['admin_logs_id'] = $admin_logs->id;
                $data['latlng'] = $lat . ',' . $lng;
            }

            return view('backend.hotels.importresults', $data);
        } else {
            return redirect()->route('admin.hotels.index')
                            ->withFlashSuccess('Done!');
        }
    }

    /**
     * @param ManageHotelsRequest $request
     * @return mixed
     */
    public function savesearch(ManageHotelsRequest $request) {
        $data['countries_id'] = $request->input('countries_id');
        $data['cities_id'] = $request->input('cities_id');
        $to_save = $request->input('save');
        $places = $request->input('place');

        if (is_array($to_save)) {
            foreach ($to_save AS $k => $v) {
                if (!Hotels::where('provider_id', '=', $places[$k]['provider_id'])->exists()) {
                $p = new Hotels();
                $p->place_type = $places[$k]['types'];

                $p->provider_id = $places[$k]['provider_id'];
                $p->countries_id = $data['countries_id'];
                $p->cities_id = $data['cities_id'];
                $p->lat = $places[$k]['lat'];
                $p->lng = $places[$k]['lng'];
                $p->pluscode = $places[$k]['plus_code'];
                $p->rating = $places[$k]['rating'];
                $p->places_id = 1;
                $p->active = 1;
                $p->save();

                $pt = new HotelsTranslations();
                $pt->languages_id = 1;
                $pt->hotels_id = $p->id;
                $pt->title = $places[$k]['name'];
                $pt->address = $places[$k]['address'];
                if (isset($places[$k]['phone']))
                    $pt->phone = $places[$k]['phone'];
                if (isset($places[$k]['website']))
                    $pt->description = $places[$k]['website'];
                $pt->working_days = $places[$k]['working_days'];
                $pt->save();
                AdminLogs::create(['item_type' => 'hotels', 'item_id' => $p->id, 'action' => 'import', 'query' => '', 'time' => time(), 'admin_id' => Auth::user()->id]);
                }
            }
            $num = count($to_save);

            if ($request->input('admin_logs_id')) {
                return redirect()->route('admin.hotels.search', array($request->get('admin_logs_id'),
                                    $request->get('countries_id'),
                                    $request->get('cities_id'),
                                    $request->get('latlng')))
                                ->withFlashSuccess($num . ' Hotels imported successfully!');
            } else {
                return redirect()->route('admin.hotels.index')
                                ->withFlashSuccess($num . ' Hotels imported successfully!');
            }
        } else {
            if ($request->input('admin_logs_id')) {
                return redirect()->route('admin.hotels.search', array($request->get('admin_logs_id'),
                                    $request->get('countries_id'),
                                    $request->get('cities_id'),
                                    $request->get('latlng')))
                                ->withFlashSuccess('You didnt select any items to import!');
            } else {
            return redirect()->route('admin.hotels.index')
                            ->withFlashError('You didnt select any items to import!');
            }
        }
    }

    /**
     * @param ManageHotelsRequest $request
     * @return string
     */
    public function return_search_history(ManageHotelsRequest $request) {
        $ne_lat = $request->get('ne_lat');
        $sw_lat = $request->get('sw_lat');
        $ne_lng = $request->get('ne_lng');
        $sw_lng = $request->get('sw_lat');

        $markers = HotelsSearchHistory::whereBetween('lat', array($sw_lat, $ne_lat))
                ->whereBetween('lng', array($sw_lng, $ne_lng))
                ->groupBy('lat', 'lng')
                ->select('lat', 'lng')
                ->get()
                ->toArray();
        return json_encode($markers);
    }

    /**
     * @param ManageHotelsRequest $request
     */
    public function delete_ajax(ManageHotelsRequest $request){
        $ids = $request->input('ids');

        if(!empty($ids)){
            $ids = explode(',',$request->input('ids'));
            foreach ($ids as $key => $value) {
                $this->delete_single_ajax($value);
            }
        }

        echo json_encode([
            'result' => true
        ]);
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete_single_ajax($id) {
        $item = Hotels::find($id);
        if(empty($item)){
            return false;
        }
        $item->delete();

        AdminLogs::create(['item_type' => 'hotels', 'item_id' => $id, 'action' => 'delete', 'time' => time(), 'admin_id' => Auth::user()->id]);
    }

    public function getAddedCountries(){
        $q = null;
        if(isset($_GET['q'])){
            $q = $_GET['q'];
        }

        $hotel = Hotels::distinct()->select('countries_id')->get();
        $temp_country = [];
        $filter_html = null;
        $json = [];

        if(!empty($hotel)){
            foreach ($hotel as $key => $value) {
                $country = null;
                if(empty($q)){
                    $country = Countries::find($value->countries_id);

                }else{
                    $country = Countries::leftJoin('countries_trans', function($join){
                        $join->on('countries_trans.countries_id', '=', 'countries.id');
                    })->where('countries_trans.title', 'LIKE', '%'.$q.'%')->where(['countries.id' => $value->countries_id])->first();
                }
                if(!empty($country)){

                    $transingle = CountriesTranslations::where(['countries_id' => $value->countries_id])->first();

                    if(!empty($transingle)){
                        $filter_html .= '<option value="'.$value->countries_id.'">'.$transingle->title.'</option>';
                        array_push($temp_country,$transingle->title);
                        $json[] = ['id' => $value->countries_id, 'text' => $transingle->title];
                    }
                }
            }
        }
        echo json_encode($json);
    }

    public function getAddedCities(){
        $q = null;
        if(isset($_GET['q'])){
            $q = $_GET['q'];
        }

        $place = Hotels::distinct()->select('cities_id')->get();
        $temp_city = [];
        $city_filter_html = null;
        $json = [];

        if(!empty($place)){
            foreach ($place as $key => $value) {
                if(empty($q)){
                    $city = Cities::find($value->cities_id);

                }else{
                    $city = Cities::leftJoin('cities_trans', function($join){
                        $join->on('cities_trans.cities_id', '=', 'cities.id');
                    })->where('cities_trans.title', 'LIKE', '%'.$q.'%')->where(['cities.id' => $value->cities_id])->first();
                }
                if(!empty($city)){

                    $transingle = CitiesTranslations::where(['cities_id' => $value->cities_id])->first();

                    if(!empty($transingle)){
                        $city_filter_html .= '<option value="'.$value->cities_id.'">'.$transingle->title.'</option>';
                        array_push($temp_city,$transingle->title);
                        $json[] = ['id' => $value->cities_id, 'text' => $transingle->title];
                    }
                }
            }
        }

        echo json_encode($json);
    }

    public function getPlaceTypes(){
        $q = null;
        if(isset($_GET['q'])){
            $q = $_GET['q'];
        }

        if(empty($q)){
            $place = Hotels::distinct()->select('place_type')->get();
        }else{
            $place = Hotels::distinct()->select('place_type')->where('place_type', 'LIKE', '%'.$q.'%')->get();
        }
        $city_filter_html = null;
        $json = [];

        if(!empty($place)){
            foreach ($place as $key => $value) {
                $json[] = ['id' => $value->place_type, 'text' => $value->place_type];
            }
        }

        echo json_encode($json);
    }
}

/*
 * db.travoooapi.com    AIzaSyD62QDGpGcCLFiVGc-fYDvKL-PRqzBKnf8   done
 * db2.travoooapi.com   AIzaSyDazlrQc906mR7ZpqZeWRI_pER0z1upOPI   done
 * db.travoooapi.net    AIzaSyCUUi3SIwrA9L_CMmPGM2DqfuMCijYZg_g   done
 * db2.travoooapi.net   AIzaSyDD-FC1mJ5a26kDIYEBB2QSG95j2ByNE7U   done
 * db.travooodev.com    AIzaSyBkEfyhR3NFzoOQDBCjmZU-lPVrPSbUErA   done
 * www.travooodev2.com  AIzaSyBOoJ92xmBXGMg5x8RIV7Flu8t7ln1grkg   done
 */