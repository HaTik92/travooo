<?php

namespace App\Http\Controllers\Backend\Pages;

use App\Http\Requests\Backend\Pages\UpdatePagesRequest;
use App\Models\Pages\Pages;
use App\Models\Pages\PagesTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Pages\ManagePagesRequest;
use App\Http\Requests\Backend\Pages\StorePagesRequest;
use App\Repositories\Backend\Pages\PagesRepository;
use App\Models\ActivityMedia\Media;
use App\Models\User\User;
use App\Models\Role\RoleUser;
use Illuminate\Http\Request;
use function PHPSTORM_META\type;
use Yajra\DataTables\Facades\DataTables;

class PagesController extends Controller
{
    protected $pages;

    /**
     * PagesController constructor.
     * @param PagesRepository $pages
     */
    public function __construct(PagesRepository $pages)
    {
        $this->pages = $pages;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

     /**
     * @param ManagePagesRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManagePagesRequest $request)
    {
        return view('backend.pages.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->pages->getForDataTable())
            ->addColumn('action', function ($pages) {
                return view('backend.buttons', ['params' => $pages, 'route' => 'pages']);
            })
            ->make(true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function medias(Request $request)
    {
        $term = trim($request->q);
        $medias = [];

        if (!empty($term)) {
            $matchPhraseParams['title'] = $term;
            $medias = Media::where(['title'=> $term])->get()->toArray();
        }

        $dataArray = [];

        $paginate = count($medias) < 10 ? false : true;

        if(!empty($medias)){
            foreach ($medias as $media) {
                $dataArray[] = ['id' => $media['id'], 'text' => $media['title']];
            }
        }

        return response()->json(['data' => $dataArray, 'paginate' => $paginate]);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        /* Get All Medias For Dropdown */
        $medias = Media::limit(30)->get();
        $medias_arr = [];

        if(!empty($medias)){
            foreach ($medias as $key => $value) {
                $medias_arr[$value['id']] = $value->transsingle['title'];
            }
        }

        /* Get All Admins For Dropdown */
        $admins = RoleUser::where(['role_id' => 1])->get();
        $admins_arr = [];

        if(!empty($admins)){
            foreach ($admins as $key => $value) {
                $admins_arr[$value->user['id']] = $value->user['name'];
            }
        }

        /* Get All Users */
        $users = User::get();
        $users_arr = [];

        if(!empty($users)){
            foreach ($users as $key => $value) {
                $users_arr[$value->id] = $value['name'];
            }
        }

        $filtred_admins = [];
        foreach($admins_arr as $key => $value){
            if(!empty($key) && !empty($value)){
                $filtred_admins[$key] = $value;
            }
        }

        $filtred_users = [];
        foreach($users_arr as $key => $value){
            if(!empty($key) && !empty($value)){
                $filtred_users[$key] = $value;
            }
        }

        $filtred_medias = [];
        foreach($medias_arr as $key => $value){
            if(!empty($key) && !empty($value)){
                $filtred_medias[$key] = $value;
            }
        }

        return view('backend.pages.create',[
            'admins' => $filtred_admins,
            'followers' => $filtred_users,
            'medias' => $filtred_medias
        ]);
    }

    /**
     * @param StorePagesRequest $request
     * @return mixed
     */
    public function store(StorePagesRequest $request)
    {   
        $data = [];
        $active = 2;

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        if(!empty($request->input('active'))){
            $active = 1;
        }

        /* Send All Relations and Common Fields Through $extra Array */
        $extra = [
            'active' => $active,
            'url' => $request->input('url'),
            'medias' => $request->input('medias_id'),
            'admins' => $request->input('admins_id'),
            'followers' => $request->input('followers_id'),
        ];
        
        $this->pages->create($data , $extra);

        return redirect()->route('admin.pages.index')->withFlashSuccess('Page Created!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {      
        $item = Pages::findOrFail($id);
        /* Delete Children Tables Data of this weekday */
        $child = PagesTranslations::where(['pages_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        /* Delete All Relations */
        $item->deleteAdmins();
        $item->deleteFollowers();
        $item->deleteMedias();
        $item->delete();

        return redirect()->route('admin.pages.index')->withFlashSuccess('Page Deleted Successfully');
    }

    /**
     * @param Pages $id
     *
     * @return mixed
     */
    public function edit($id)
    {
        $data  = [];
        $pages = Pages::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = PagesTranslations::where([
                'languages_id' => $language->id,
                'pages_id'   => $id
            ])->first();

            if(!empty($model)){
                $data['title_'.$language->id] = $model->title ?: null;
                $data['description_'.$language->id] = $model->description ?: null;
            }
        }

        $data['active'] = $pages->active;
        $data['url']    = $pages->url;

        $selected_medias = $pages->medias;
        $selected_medias_arr = [];

        /* Get Selected Medias */
        if(!empty($selected_medias)){
            foreach ($selected_medias as $key => $value) {
                if(!empty($value->media)){
                    if(!empty($value->media->transsingle)){
                        $selected_medias_arr[] = $value->media->transsingle->medias_id;
                    }
                }
            }
        }

        /* Get All Medias For Dropdown */
        $medias = Media::limit(30)->get();
        $medias_arr = [];

        if(!empty($medias)){
            foreach ($medias as $key => $value) {
                $medias_arr[$value->id] = $value->transsingle['title'];
            }
        }

        $data['selectedMedia'] = $medias_arr;
        $data['selectedMediaIds'] = $selected_medias_arr;

        /* Get Selected Admins For Dropdown */
        $selected_admins = $pages->admins;
        $selected_admins_arr = [];

        if(!empty($selected_admins)){
            foreach ($selected_admins as $key => $value) {
                array_push($selected_admins_arr,$value->users_id);
            }
        }

        $data['selected_admins'] = $selected_admins_arr;
        
        /* Get All Admins For Dropdown */
        $admins = RoleUser::where(['role_id' => 1])->get();
        $admins_arr = [];

        if(!empty($admins)){
            foreach ($admins as $key => $value) {
                if(!empty($value->user['id']) && !empty($value->user['name'])){
                    $admins_arr[$value->user['id']] = $value->user['name'];
                }
            }
        }

        /* Get Selected Followers */
        $selected_followers = $pages->followers;
        $selected_followers_arr = [];

        if(!empty($selected_followers)){
            foreach ($selected_followers as $key => $value) {
                if(!empty($value->user)){
                    array_push($selected_followers_arr,$value->user->id);
                }
            }
        }

        $data['selected_followers'] = $selected_followers_arr;

        /* Get All Users */
        $users = User::get();
        $users_arr = [];

        if(!empty($users)){
            foreach ($users as $key => $value) {
            if(!empty($value->id) && !empty($value->name))
                $users_arr[$value->id] = $value->name;
            }
        }
        return view('backend.pages.edit')
            ->withLanguages($this->languages)
            ->withPages($pages)
            ->withPagesid($id)
            ->withData($data)
            ->withAdmins($admins_arr)
            ->withFollowers($users_arr);
    }

    /**
     * @param Pages $id
     * @param ManagePagesRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdatePagesRequest $request)
    {   
        $pages = Pages::findOrFail($id);

        $active = 2;
        $data = [];

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        if(!empty($request->input('active'))){
            $active = 1;
        }

        /* Send All Relation and Common Fields Through $extra Array */
        $extra = [
            'active' => $active,
            'url' => $request->input('url'),
            'medias' => $request->input('medias_id'),
            'admins' => $request->input('admins_id'),
            'followers' => $request->input('followers_id'),
        ];

        $this->pages->update($id , $pages, $data , $extra);
        
        return redirect()->route('admin.pages.index')
            ->withFlashSuccess('Pages updated Successfully!');
    }

    /**
     * @param Pages $id
     *
     * @return mixed
     */
    public function show($id)
    {   
        $pages = Pages::findOrFail($id);
        $pagesTrans = PagesTranslations::where(['pages_id' => $id])->get();
        
        $selected_medias = $pages->medias;
        $selected_medias_arr = [];

        /* Get Selected Medias */
        if(!empty($selected_medias)){
            foreach ($selected_medias as $key => $value) {               
                if(!empty($value->media)){
                    if(!empty($value->media->transsingle)){
                        array_push($selected_medias_arr,$value->media->transsingle->title);
                    }
                }
            }
        }

        /* Get Selected Admins */
        $selected_admins = $pages->admins;
        $selected_admins_arr = [];

        if(!empty($selected_admins)){
            foreach ($selected_admins as $key => $value) {               
                if(!empty($value->user)){
                        array_push($selected_admins_arr,$value->user->name);
                }
            }
        }

        /* Det Selected Followers */
        $selected_followers = $pages->followers;
        $selected_followers_arr = [];

        if(!empty($selected_followers)){
            foreach ($selected_followers as $key => $value) {               
                if(!empty($value->user)){
                        array_push($selected_followers_arr,$value->user->name);
                }
            }
        }
        
        return view('backend.pages.show')
            ->withPages($pages)
            ->withPagestrans($pagesTrans)
            ->withMedias($selected_medias_arr)
            ->withAdmins($selected_admins_arr)
            ->withFollowers($selected_followers_arr);
    }

    /**
     * @param $id
     * @param $status
     * @return mixed
     */
    public function mark($id, $status)
    {   
        $pages = Pages::findOrFail($id);
        
        $pages->active = $status;
        $pages->save();
        
        return redirect()->route('admin.pages.index')
            ->withFlashSuccess('Page Status Updated!');
    }
}