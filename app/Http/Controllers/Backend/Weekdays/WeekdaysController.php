<?php

namespace App\Http\Controllers\Backend\Weekdays;

use App\Http\Requests\Backend\Weekdays\UpdateWeekdaysRequest;
use App\Models\Weekdays\Weekdays;
use App\Models\Weekdays\WeekdaysTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Weekdays\ManageWeekdaysRequest;
use App\Http\Requests\Backend\Weekdays\StoreWeekdaysRequest;
use App\Repositories\Backend\Weekdays\WeekdaysRepository;
use Yajra\DataTables\Facades\DataTables;

class WeekdaysController extends Controller
{
    protected $weekdays;

    /**
     * WeekdaysController constructor.
     * @param WeekdaysRepository $weekdays
     */
    public function __construct(WeekdaysRepository $weekdays)
    {
        $this->weekdays = $weekdays;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManageWeekdaysRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageWeekdaysRequest $request)
    {
        return view('backend.weekdays.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->weekdays->getForDataTable())
            ->addColumn('action', function ($weekdays) {
                return view('backend.buttons', ['params' => $weekdays, 'route' => 'weekdays']);
            })
            ->make(true);
    }

    /**
     * @param ManageWeekdaysRequest $request
     *
     * @return mixed
     */
    public function create(ManageWeekdaysRequest $request)
    {
        return view('backend.weekdays.create');
    }

    /**
     * @param StoreWeekdaysRequest $request
     * @return mixed
     */
    public function store(StoreWeekdaysRequest $request)
    {   
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $this->weekdays->create($data);

        return redirect()->route('admin.weekdays.index')->withFlashSuccess('Weekday Created!');
    }

    /**
     * @param $id
     * @param ManageWeekdaysRequest $request
     * @return mixed
     */
    public function destroy($id, ManageWeekdaysRequest $request)
    {      
        $item = Weekdays::findOrFail($id);
        /* Delete Children Tables Data of this weekday */
        $child = WeekdaysTranslations::where(['weekdays_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.weekdays.index')->withFlashSuccess('Weekday Deleted Successfully');
    }

    /**
     * @param Weekdays $id
     *
     * @param ManageWeekdaysRequest $request
     * @return mixed
     */
    public function edit($id, ManageWeekdaysRequest $request)
    {
        $data = [];
        $weekdays = Weekdays::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = WeekdaysTranslations::where([
                'languages_id' => $language->id,
                'weekdays_id'   => $id
            ])->first();

            if(!empty($model)){
                $data['title_'.$language->id]       = $model->title ?: null;
                $data['description_'.$language->id] = $model->description ?: null;
            }
        }

        return view('backend.weekdays.edit')
            ->withLanguages($this->languages)
            ->withWeekdays($weekdays)
            ->withWeekdaysid($id)
            ->withData($data);
    }

    /**
     * @param Weekdays $id
     * @param ManageWeekdaysRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateWeekdaysRequest $request)
    {   
        $weekdays = Weekdays::findOrFail($id);
        
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $this->weekdays->update($id , $weekdays, $data);
        
        return redirect()->route('admin.weekdays.index')
            ->withFlashSuccess('Weekdays updated Successfully!');
    }

    /**
     * @param Weekdays $id
     *
     * @param ManageWeekdaysRequest $request
     * @return mixed
     */
    public function show($id, ManageWeekdaysRequest $request)
    {   
        $weekdays = Weekdays::findOrFail($id);
        $weekdaysTrans = WeekdaysTranslations::where(['weekdays_id' => $id])->get();

        return view('backend.weekdays.show')
            ->withWeekdays($weekdays)
            ->withWeekdaystrans($weekdaysTrans);
    }
}