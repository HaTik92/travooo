<?php

namespace App\Http\Controllers\Backend\SpamReport;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Models\Place\UnapprovedItem;
use App\Models\Posts\Posts;
use App\Models\Posts\SpamsPosts;
use App\Models\Reports\Reports;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Utilities\Request;


class SpamReportController extends Controller
{
    use CreateUpdateStatisticTrait;

    protected $languages;

    /**
     * SpamReportController constructor.
     */
    public function __construct()
    {
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('backend.spam_reports.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table(Request $request) {
        $orderData = current($request->order);
        $orderColumn = $request->columnName($orderData['column']);
        $orderDir = $orderData['dir'];

        $query = SpamsPosts::select('id', 'report_text');

        $response['recordsTotal'] = count($query->get());
        $response['recordsFiltered'] = $response['recordsTotal'];

        $spamReports = $query->offset($request->start)->limit($request->length)->orderBy($orderColumn, $orderDir)->get();
        $spamReports = $spamReports->map(function($report) {
            $report->action = null;
            return $report;
        });
        $response['data'] = $spamReports;

        return response()->json($response);
    }

    public function ignore($spamPostId)
    {
        SpamsPosts::findOrFail($spamPostId)->delete();

        return redirect()->back()->withFlashSuccess('Spam report ignored successfully');
    }

    public function resolve($spamPostId)
    {
        $spamPost = SpamsPosts::find($spamPostId);


        if (is_object($spamPost)) {
            $post = Posts::find($spamPost->post_id);

            if (is_object($post)) {
                $post->delete();
            }

            $spamPost->delete();

            return redirect()->back()->withFlashSuccess('Spam report resolved successfully');
        }

        return redirect()->back();

    }

    public function show()
    {

    }
}