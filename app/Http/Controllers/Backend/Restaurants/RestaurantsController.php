<?php

namespace App\Http\Controllers\Backend\Restaurants;

use App\Models\AdminLogs\AdminLogs;
use App\Models\Country\CountriesTranslations;
use App\Models\Place\PlaceTranslations;
use App\Models\RestaurantsSearchHistory\RestaurantsSearchHistory;
use Illuminate\Support\Facades\Auth;
use App\Models\Restaurants\Restaurants;
use App\Models\Restaurants\RestaurantsTranslations;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Repositories\Backend\Restaurants\RestaurantsRepository;
use App\Http\Requests\Backend\Restaurants\StoreRestaurantsRequest;
use App\Http\Requests\Backend\Restaurants\ManageRestaurantsRequest;
use App\Http\Requests\Backend\Restaurants\UpdateRestaurantsRequest;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Place\Place;
use App\Models\ActivityMedia\Media;
use Yajra\DataTables\Utilities\Request;

class RestaurantsController extends Controller
{
    /* RestaurantsRepository $restaurants */
    protected $restaurants;
    protected $languages;

    /**
     * RestaurantsController constructor.
     * @param RestaurantsRepository $restaurants
     */
    public function __construct(RestaurantsRepository $restaurants)
    {
        $this->restaurants = $restaurants;
        /* Get All Active Language */
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManageRestaurantsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageRestaurantsRequest $request)
    {
        return view('backend.restaurants.index');
    }

    /**
     * @return mixed
     */
    public function table(Request $request)
    {
        return Datatables::of($this->restaurants->getForDataTable($request))
            ->addColumn('',function(){
                return null;
            })
            ->addColumn('action', function ($restaurants) {
                return view('backend.buttons', ['params' => $restaurants, 'route' => 'restaurants']);
            })
            ->filterColumn(config('restaurants.restaurants_table').'.cities_id', function($query, $keyword) {
                $query->where(config('restaurants.restaurants_table').'.cities_id', $keyword);
            })
            ->make(true);
    }

    /**
     * @param ManageRestaurantsRequest $request
     * @return mixed
     */
    public function create(ManageRestaurantsRequest $request)
    {
        /* Get All Countries */
        $countries = Countries::where(['active' => Countries::ACTIVE])->with('transsingle')->get();
        $countries_arr = [];
        $countries_location_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat'] = $value->lat;
                $countries_location_arr[$value->id]['lng'] = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }

        /* Get All Cities */
        $cities = Cities::where([ 'active' => 1 ])->with('transsingle')->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            if(isset($value->transsingle) && !empty($value->transsingle)){
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Places For Dropdown */
        $places = Place::with('transsingle')->limit(30000)->get();;
        $places_arr = [];

        foreach ($places as $key => $value) {
            /* If Translation Exists, Get Title */
            if(isset($value->transsingle) && !empty($value->transsingle)){
                $places_arr[$value->id] = $value->transsingle->title;
            }
        }

        return view('backend.restaurants.create',[
            'countries' => $countries_arr,
            'countries_location' => $countries_location_arr,
            'cities' => $cities_arr,
            'places' => $places_arr
        ]);
    }

    /**
     * @param StoreRestaurantsRequest $request
     *
     * @return mixed
     */
    public function store(StoreRestaurantsRequest $request)
    {
        $location = explode(',',$request->input('lat_lng') );

        /* Check if active field is enabled or disabled */
        $active = Cities::ACTIVE;
        if(empty($request->input('active')) || $request->input('active') == 0){
            $active = Cities::DEACTIVE;
        }
        /* Pass All Relations Through $extra Array */
        $extra = [
            'active'        => $active,
            'countries_id'  => $request->input('countries_id'),
            'cities_id'     => $request->input('cities_id'),
            'places_id'     => $request->input('places_id'),
            'lat'           => $location[0],
            'lng'           => $location[1],
            'license_images' => $request->license_images
        ];

        $this->restaurants->create($request->translations, $extra);

        return redirect()->route('admin.restaurants.index')->withFlashSuccess('Restaurant Created!');
    }

    /**
     * @param Restaurants $id
     *
     * @param ManageRestaurantsRequest $request
     * @return mixed
     */
    public function edit($id, ManageRestaurantsRequest $request)
    {
        /* $data array to pass data to view */
        $data = [];

        $restaurants            = Restaurants::findOrFail($id);
        $data['translations'] = [];
        foreach ($this->languages as $language) {

            if ( $translation = RestaurantsTranslations::where(['restaurants_id' => $id, 'languages_id' => $language->id])->first() ) {
                $data['translations'][] = $translation;
            } else {
                $empty_translation = new RestaurantsTranslations;
                foreach ($restaurants->transsingle->toArray() as $key => $value) {
                    $empty_translation->$key = null;
                }
                unset($empty_translation->id);
                $empty_translation->languages_id    = $language->id;
                $empty_translation->restaurants_id  = $restaurants->transsingle->restaurants_id;
                $empty_translation->transsingle     = $language;
                $data['translations'][]             = $empty_translation;
            }
        }

        /* Put All Common Fields In $data Array To Be Used In Edit Form */
        $data['lat_lng']        = $restaurants['lat'] . ',' . $restaurants['lng'];
        $data['active']         = $restaurants['active'];
        $data['countries_id']   = $restaurants['countries_id'];
        $data['cities_id']      = $restaurants['cities_id'];
        $data['places_id']      = $restaurants['places_id'];

        /* Find All Active Countries */
        $countries              = Countries::where(['active' => Countries::ACTIVE])->with('transsingle')->get();
        $countries_arr          = [];
        $countries_location_arr = [];

        /* Get Title Id Pair For Each Model */
        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat'] = $value->lat;
                $countries_location_arr[$value->id]['lng'] = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }

        $cities_arr[$restaurants->cities_id] = Cities::find($restaurants->cities_id)->transsingle->title;
        $place = PlaceTranslations::where('places_id', $restaurants['places_id'])->where('languages_id', 1)->first();
        if(isset($place->places_id) && isset($place->title)){
            $data['place'] = [$place->places_id => $place->title];
        } else {
            $data['place'] = null;
        }

        if(isset($restaurants->medias)){
            foreach($restaurants->medias as $media){
                $medias_id = $media->medias_id;

                $media_results[] = Media::find($medias_id);
            }
        }

        return view('backend.restaurants.edit', compact('media_results'))
            ->withLanguages($this->languages)
            ->withRestaurant($restaurants)
            ->withRestaurantid($id)
            ->withCountries($countries_arr)
            ->withCountriesLocation($countries_location_arr)
            ->withCities($cities_arr)
            ->withData($data);
    }

    /**
     * @param Restaurants $id
     * @param ManageRestaurantsRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateRestaurantsRequest $request)
    {
        $restaurants = Restaurants::findOrFail($id);

        $location = explode(',',$request->input('lat_lng') );

        /* Check if active field is enabled or disabled */
        $active = Cities::ACTIVE;
        if(empty($request->input('active')) || $request->input('active') == 0){
            $active = Cities::DEACTIVE;
        }

        /* Pass All Relations Through $extra Array */
        $extra = [
            'active'            => $active,
            'countries_id'      => $request->input('countries_id'),
            'cities_id'         => $request->input('cities_id'),
            'places_id'         => $request->input('places_id'),
            'lat'               => $location[0],
            'lng'               => $location[1],
            'license_images' => [
                'images'    => $request->license_images,
                'deleted'   => $request->del
            ]
        ];


        $this->restaurants->update($id , $restaurants, $request->translations , $extra);

        return redirect()->route('admin.restaurants.index')
            ->withFlashSuccess('Restaurants updated Successfully!');
    }

    /**
     * @param Restaurants        $id
     * @param ManageRestaurantsRequest $request
     *
     * @return mixed
     */
    public function show($id, ManageRestaurantsRequest $request)
    {
        $restaurants = Restaurants::findOrFail($id);
        $restaurantsTrans = RestaurantsTranslations::where(['restaurants_id' => $id])->get();

        /* Get Country Information */
        $country = $restaurants->country;
        $country = $country->transsingle;

        /* Get City Information */
        $city = $restaurants->city;
        $city = $city->transsingle;

        /* Get Place Information */
        $place = $restaurants->place;
        if(!empty($place) && !empty($place->transsingle)){
            $place = $place->transsingle;
        } else {
            $place = null;
        }

        /* Get All Selected Medias */
        $medias     = $restaurants->medias;
        $medias_arr = [];

        if(!empty($medias)){
            foreach ($medias as $key => $value) {
                if(!empty($value->media)){
                    if(!empty($value->media->transsingle)){
                        array_push($medias_arr, $value->media->transsingle->title);
                    }
                }
            }
        }

        return view('backend.restaurants.show')
            ->withRestaurant($restaurants)
            ->withRestaurant_trans($restaurantsTrans)
            ->withCountry($country)
            ->withCity($city)
            ->withPlace($place)
            ->withMedias($medias_arr);
    }

    /**
     * @param Restaurants $id
     *
     * @param ManageRestaurantsRequest $request
     * @return mixed
     */
    public function destroy($id, ManageRestaurantsRequest $request)
    {
        $item = Restaurants::findOrFail($id);
        $item->deleteTrans();
        if (!$item->getMedias->isEmpty()) {
            foreach ($item->getMedias as $media) {
                $media->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.restaurants.index')->withFlashSuccess('Restaurants Deleted Successfully');
    }

    /**
     * @param $id
     * @param $status
     *
     * @param ManageRestaurantsRequest $request
     * @return mixed
     * @internal param Restaurants $restaurants
     */
    public function mark($id, $status, ManageRestaurantsRequest $request)
    {
        $restaurants = Restaurants::findOrFail($id);
        $restaurants->active = $status;
        $restaurants->save();
        return redirect()->route('admin.restaurants.index')
            ->withFlashSuccess('Restaurants Status Updated!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function import() {
        $countries = Countries::where(['active' => 1])->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['countries'] = $countries_arr;

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->get();
        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['cities'] = $cities_arr;

        return view('backend.restaurants.import', $data);
    }

    /**
     * @param int $admin_logs_id
     * @param int $country_id
     * @param int $city_id
     * @param int $latlng
     * @param ManageRestaurantsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search($admin_logs_id = 0, $country_id = 0, $city_id = 0, $latlng = 0, ManageRestaurantsRequest $request)
    {
        $data['countries_id'] = $country_id ?: $request->input('countries_id');;
        $data['cities_id'] = $city_id ?: $request->input('cities_id');

        $city = urlencode($request->input('address'));

        if ($admin_logs_id) {
            list($lat, $lng) = @explode(",", $latlng);

            $admin_logs = AdminLogs::find($admin_logs_id);
            $qu = $admin_logs->query;
            $queries = array_values(unserialize($qu));

            if (count($queries)) {
                $query = urlencode($queries[0]);
                unset($queries[0]);
                $queries = serialize($queries);
                $ad = AdminLogs::find($admin_logs_id);
                $ad->query = $queries;
                $ad->save();
            }
        } else {
            $queries = explode(",", $request->input('query'));
            $query = urlencode($queries[0]);
            unset($queries[0]);
            $queries = serialize($queries);

            $latlng = @explode(",", $request->input('latlng'));
            $lat = $latlng[0];
            $lng = $latlng[1];

            $admin_logs = new AdminLogs();
            $admin_logs->item_type = 'restaurants';
            $admin_logs->item_id = 0;
            $admin_logs->action = 'search';
            $admin_logs->query = $queries;
            $admin_logs->time = time();
            $admin_logs->admin_id = Auth::user()->id;
            $admin_logs->save();
        }

        if (isset($query)) {

            /*
             $provider_ids = array();
             
            $get_provider_ids = Restaurants::where('id', '>', 0)->select('provider_id')->get()->toArray();
            foreach ($get_provider_ids AS $gpi) {
                $provider_ids[] = $gpi['provider_id'];
            }
            $data['provider_ids'] = $provider_ids;
             * 
             */

            $data['provider_ids'] = array();

            $json = file_get_contents('http://db.travoooapi.com/public/restaurants/go/' . ($city ? $city : 0) . '/' . $lat . '/' . $lng . '/' . $query);

            $result = json_decode($json);

            RestaurantsSearchHistory::create([
                'lat' => $lat,
                'lng' => $lng,
                'time' => time(),
                'admin_id' => Auth::user()->id
            ]);

            $data['results'] = $result;
            $data['queries'] = $queries;
            if (isset($admin_logs) && is_object($admin_logs)) {
                $data['admin_logs_id'] = $admin_logs->id;
                $data['latlng'] = $lat . ',' . $lng;
            }

            return view('backend.restaurants.importresults', $data);
        } else {
            return redirect()->route('admin.restaurants.index')
                            ->withFlashSuccess('Done!');
        }
    }

    /**
     * @param ManageRestaurantsRequest $request
     * @return mixed
     */
    public function savesearch(ManageRestaurantsRequest $request) {
        $data['countries_id'] = $request->input('countries_id');
        $data['cities_id'] = $request->input('cities_id');
        $to_save = $request->input('save');
        $places = $request->input('place');

        if (is_array($to_save)) {
            foreach ($to_save AS $k => $v) {
                if (!Restaurants::where('provider_id', '=', $places[$k]['provider_id'])->exists()) {
                    $p = new Restaurants();
                    $p->provider_id = $places[$k]['provider_id'];
                    $p->countries_id = $data['countries_id'];
                    $p->cities_id = $data['cities_id'];
                    $p->place_type = $places[$k]['types'];

                    $p->places_id = 1;
                    $p->lat = $places[$k]['lat'];
                    $p->lng = $places[$k]['lng'];
                    $p->pluscode = $places[$k]['plus_code'];
                    $p->rating = $places[$k]['rating'];
                    $p->active = 1;
                    $p->save();

                    $pt = new RestaurantsTranslations();
                    $pt->languages_id = 1;
                    $pt->restaurants_id = $p->id;
                    $pt->title = $places[$k]['name'];
                    $pt->address = $places[$k]['address'];
                    if (isset($places[$k]['phone']))
                        $pt->phone = $places[$k]['phone'];
                    if (isset($places[$k]['website']))
                        $pt->description = $places[$k]['website'];
                    $pt->working_days = $places[$k]['working_days'] ? $places[$k]['working_days'] : '';
                    $pt->save();
                    AdminLogs::create(['item_type' => 'restaurants', 'item_id' => $p->id, 'action' => 'import', 'query' => '', 'time' => time(), 'admin_id' => Auth::user()->id]);
                }
            }
            $num = count($to_save);

            if ($request->input('admin_logs_id')) {
                return redirect()->route('admin.restaurants.search', array($request->get('admin_logs_id'),
                                    $request->get('countries_id'),
                                    $request->get('cities_id'),
                                    $request->get('latlng')))
                                ->withFlashSuccess($num . ' Restaurants imported successfully!');
            } else {
                return redirect()->route('admin.restaurants.index')
                                ->withFlashSuccess($num . ' Restaurants imported successfully!');
            }
        } else {
            if ($request->input('admin_logs_id')) {
                return redirect()->route('admin.restaurants.search', array($request->get('admin_logs_id'),
                                    $request->get('countries_id'),
                                    $request->get('cities_id'),
                                    $request->get('latlng')))
                                ->withFlashSuccess('You didnt select any items to import!');
            } else {
            return redirect()->route('admin.restaurants.index')
                            ->withFlashError('You didnt select any items to import!');
            }
        }
    }

    /**
     * @param ManageRestaurantsRequest $request
     * @return string
     */
    public function return_search_history(ManageRestaurantsRequest $request) {
        $ne_lat = $request->get('ne_lat');
        $sw_lat = $request->get('sw_lat');
        $ne_lng = $request->get('ne_lng');
        $sw_lng = $request->get('sw_lat');

        $markers = RestaurantsSearchHistory::whereBetween('lat', array($sw_lat, $ne_lat))
                ->whereBetween('lng', array($sw_lng, $ne_lng))
                ->groupBy('lat', 'lng')
                ->select('lat', 'lng')
                ->get()
                ->toArray();
        return json_encode($markers);
    }

    /**
     * @param ManageRestaurantsRequest $request
     */
    public function delete_ajax(ManageRestaurantsRequest $request){

        $ids = $request->input('ids');
        if(!empty($ids)){
            $ids = explode(',',$request->input('ids'));
            foreach ($ids as $key => $value) {
                $this->delete_single_ajax($value);
            }
        }

        echo json_encode([
            'result' => true
        ]);
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete_single_ajax($id) {
        $item = Restaurants::find($id);
        if(empty($item)){
            return false;
        }
        $item->delete();

        AdminLogs::create(['item_type' => 'restaurants', 'item_id' => $id, 'action' => 'delete', 'time' => time(), 'admin_id' => Auth::user()->id]);
    }

    public function getAddedCountries(){
        $q = null;
        if(isset($_GET['q'])){
            $q = $_GET['q'];
        }

        $hotel = Restaurants::distinct()->select('countries_id')->get();
        $temp_country = [];
        $filter_html = null;
        $json = [];

        if(!empty($hotel)){
            foreach ($hotel as $key => $value) {
                $country = null;
                if(empty($q)){
                    $country = Countries::find($value->countries_id);

                }else{
                    $country = Countries::leftJoin('countries_trans', function($join){
                        $join->on('countries_trans.countries_id', '=', 'countries.id');
                    })->where('countries_trans.title', 'LIKE', '%'.$q.'%')->where(['countries.id' => $value->countries_id])->first();
                }
                if(!empty($country)){

                    $transingle = CountriesTranslations::where(['countries_id' => $value->countries_id])->first();

                    if(!empty($transingle)){
                        $filter_html .= '<option value="'.$value->countries_id.'">'.$transingle->title.'</option>';
                        array_push($temp_country,$transingle->title);
                        $json[] = ['id' => $value->countries_id, 'text' => $transingle->title];
                    }
                }
            }
        }
        echo json_encode($json);
    }

    public function getAddedCities(){
        $q = null;
        if(isset($_GET['q'])){
            $q = $_GET['q'];
        }

        $place = Restaurants::distinct()->select('cities_id')->get();
        $temp_city = [];
        $city_filter_html = null;
        $json = [];

        if(!empty($place)){
            foreach ($place as $key => $value) {
                if(empty($q)){
                    $city = Cities::find($value->cities_id);

                }else{
                    $city = Cities::leftJoin('cities_trans', function($join){
                        $join->on('cities_trans.cities_id', '=', 'cities.id');
                    })->where('cities_trans.title', 'LIKE', '%'.$q.'%')->where(['cities.id' => $value->cities_id])->first();
                }
                if(!empty($city)){

                    $transingle = CitiesTranslations::where(['cities_id' => $value->cities_id])->first();

                    if(!empty($transingle)){
                        $city_filter_html .= '<option value="'.$value->cities_id.'">'.$transingle->title.'</option>';
                        array_push($temp_city,$transingle->title);
                        $json[] = ['id' => $value->cities_id, 'text' => $transingle->title];
                    }
                }
            }
        }

        echo json_encode($json);
    }

    public function getPlaceTypes(){
        $q = null;
        if(isset($_GET['q'])){
            $q = $_GET['q'];
        }

        if(empty($q)){
            $place = Restaurants::distinct()->select('place_type')->get();
        }else{
            $place = Restaurants::distinct()->select('place_type')->where('place_type', 'LIKE', '%'.$q.'%')->get();
        }
        $city_filter_html = null;
        $json = [];

        if(!empty($place)){
            foreach ($place as $key => $value) {
                $json[] = ['id' => $value->place_type, 'text' => $value->place_type];
            }
        }

        echo json_encode($json);
    }
}