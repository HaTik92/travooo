<?php

namespace App\Http\Controllers\Backend\Cultures;

use App\Http\Requests\Backend\Cultures\UpdateCulturesRequest;
use App\Models\Cultures\Cultures;
use App\Models\Cultures\CulturesTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Cultures\ManageCulturesRequest;
use App\Http\Requests\Backend\Cultures\StoreCulturesRequest;
use App\Repositories\Backend\Cultures\CulturesRepository;
use Yajra\DataTables\Facades\DataTables;

class CulturesController extends Controller
{
    protected $cultures;

    public function __construct(CulturesRepository $cultures)
    {
        $this->cultures = $cultures;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

     /**
     * @param ManageCulturesRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageCulturesRequest $request)
    {
        return view('backend.cultures.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->cultures->getForDataTable())
            ->addColumn('action', function ($cultures) {
                return view('backend.buttons', ['params' => $cultures, 'route' => 'cultures']);
            })
            ->make(true);
    }

    /**
     * @param ManageCulturesRequest $request
     * @return mixed
     */
    public function create(ManageCulturesRequest $request)
    {   
        return view('backend.cultures.create');
    }

    /**
     * @param StoreCulturesRequest $request
     *
     * @return mixed
     */
    public function store(StoreCulturesRequest $request)
    {   
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $this->cultures->create($data);

        return redirect()->route('admin.cultures.index')->withFlashSuccess('Culture Created!');
    }

    /**
     * @param Cultures $id
     *
     * @param ManageCulturesRequest $request
     * @return mixed
     */
    public function destroy($id, ManageCulturesRequest $request)
    {      
        $item = Cultures::findOrFail($id);
        /* Delete Children Tables Data of this Culture */
        $child = CulturesTranslations::where(['cultures_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.cultures.index')->withFlashSuccess('Culture Deleted Successfully');
    }

    /**
     * @param Cultures $id
     *
     * @param ManageCulturesRequest $request
     * @return mixed
     */
    public function edit($id, ManageCulturesRequest $request)
    {
        $data = [];
        $cultures = Cultures::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = CulturesTranslations::where([
                'languages_id' => $language->id,
                'cultures_id'   => $id
            ])->first();

            if(!empty($model)){
                $data['title_'.$language->id]       = $model->title ?: null;
                $data['description_'.$language->id] = $model->description ?: null;
            }
        }

        return view('backend.cultures.edit')
            ->withLanguages($this->languages)
            ->withCultures($cultures)
            ->withCulturesid($id)
            ->withData($data);
    }

    /**
     * @param Cultures $id
     * @param ManageCulturesRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateCulturesRequest $request)
    {   
        $cultures = Cultures::findOrFail($id);
        
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $this->cultures->update($id , $cultures, $data);
        
        return redirect()->route('admin.cultures.index')
            ->withFlashSuccess('Cultures updated Successfully!');
    }

    /**
     * @param Cultures $id
     *
     * @param ManageCulturesRequest $request
     * @return mixed
     */
    public function show($id, ManageCulturesRequest $request)
    {   
        $cultures = Cultures::findOrFail($id);
        $culturesTrans = CulturesTranslations::where(['cultures_id' => $id])->get();
       
        return view('backend.cultures.show')
            ->withCultures($cultures)
            ->withCulturestrans($culturesTrans);
    }
}