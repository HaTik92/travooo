<?php

namespace App\Http\Controllers\Backend\City;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Models\City\Cities;
use App\Models\Country\CountriesTranslations;
use App\Models\Country\Countries;
use App\Models\Religion\Religion;
use App\Models\Lifestyle\Lifestyle;
use App\Models\ActivityMedia\Media;
use App\Models\City\CitiesAbouts;
use App\Http\Controllers\Controller;
use App\Models\City\CitiesTranslations;
use App\Models\Access\Language\Languages;
use App\Repositories\Backend\City\CityRepository;
use App\Http\Requests\Backend\City\StoreCityRequest;
use App\Http\Requests\Backend\City\ManageCityRequest;
use App\Http\Requests\Backend\City\UpdateCityRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\AdminLogs\AdminLogs;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Currencies\Currencies;
use App\Models\LanguagesSpoken\LanguagesSpoken;
use App\Models\Holidays\Holidays;
use App\Models\EmergencyNumbers\EmergencyNumbers;

class CityController extends Controller
{
    use CreateUpdateStatisticTrait;
    /* CityRepository $cities */
    protected $cities;
    protected $languages;

    /**
     * CityController constructor.
     * @param CityRepository $cities
     */
    public function __construct(CityRepository $cities)
    {
        $this->cities = $cities;
        /* Get All Active Language */
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManageCityRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(ManageCityRequest $request)
    {
        $term = trim($request->q);
        if (!empty($term)) {
            if (!empty($request->countryId)) {
                $cities = Cities::whereHas('transsingle', function ($query) use ($term) {
                    $query->where('title', 'LIKE', "%$term%");
                })->where('active', 1)->where('countries_id', $request->countryId)->with('transsingle')->offset(($request->page -1 ) * 10)->limit(10)->get();
            } else {
                $cities = Cities::whereHas('transsingle', function ($query) use ($term) {
                    $query->where('title', 'LIKE', "%$term%");
                })->where('active', 1)->with('transsingle')->offset(($request->page -1 ) * 10)->limit(10)->get();
            }

            $formatted_cities = [];
            $paginate = (count($cities) < 10) ? false : true;

            foreach ($cities as $city) {
                $formatted_cities[] = ['id' => $city->id, 'text' => $city->transsingle->title];
            }
            return response()->json(['data' => $formatted_cities, 'paginate' => $paginate]);

        }
        return view('backend.city.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->cities->getForDataTable())
            ->escapeColumns(['code'])
            ->addColumn('action', function ($cities) {
                return view('backend.buttons', ['params' => $cities, 'route' => 'location.city']);
            })
            ->addColumn('',function(){
                return null;
            })
            ->make(true);

    }

    /**
     * @param ManageCityRequest $request
     *
     * @return mixed
     */
    public function create(ManageCityRequest $request)
    {
        /* Get All Regions */
        $countries = Countries::where(['active' => Countries::ACTIVE])->get();
        $countries_arr = [];
        $countries_location_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat'] = $value->lat;
                $countries_location_arr[$value->id]['lng'] = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }
        /* Find All Lifestyles In The System */
        $lifestyles = Lifestyle::with('transsingle')->get();
        $lifestyles_arr = [];

        foreach ($lifestyles as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $lifestyles_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Find All Active Religions In The System */
        $religion = Religion::where(['active' => Religion::ACTIVE])->with('transsingle')->get();
        $religion_arr = [];

        foreach ($religion as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $religion_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Active Currencies */
        $currencies = Currencies::where(['active' => Currencies::ACTIVE])->get();
        $currencies_arr = [];

        foreach ($currencies as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $currencies_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Religions */
        $religions = Religion::where(['active' => 1])->with('transsingle')->get();
        $religions_arr = [];

        foreach ($religions as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $religions_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Find All LanguagesSpoken In The System */
        $languages_spoken = LanguagesSpoken::get();
        $languages_spoken_arr = [];

        foreach ($languages_spoken as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $languages_spoken_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Find All Holidays In The System */
        $holidays = Holidays::get();
        $holidays_arr = [];

        foreach ($holidays as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $holidays_arr[$value->id] = $value->transsingle->title;
            }
        }

        /* Get All Emergency Numbers */
        $emergency_numbers = EmergencyNumbers::get();
        $emergency_numbers_arr = [];

        foreach ($emergency_numbers as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $emergency_numbers_arr[$value->id] = $value->transsingle->title;
            }
        }


        return view('backend.city.create', [
            'countries' => $countries_arr,
            'countries_location' => $countries_location_arr,
            'lifestyles' => $lifestyles_arr,
            'religions' => $religion_arr,
            'currencies' => $currencies_arr,
            'religions' => $religions_arr,
            'languages_spoken' => $languages_spoken_arr,
            'holidays' => $holidays_arr,
            'emergency_numbers' => $emergency_numbers_arr
        ]);
    }

    /**
     * @param StoreCityRequest $request
     *
     * @return mixed
     */
    public function store(StoreCityRequest $request)
    {
        // dd($request->input());
        $best_time = $request->input('best_time');
        if($best_time['low_season']['low_start'] != '' && $best_time['low_season']['low_end'] == '') {
            return redirect()->back()->withErrors('Low Season the field is not filled in correctly');
        }
        if($best_time['shoulder']['shoulder_start'] != '' && $best_time['shoulder']['shoulder_end'] == '') {
            return redirect()->back()->withErrors('Shoulder the field is not filled in correctly');
        }
        if($best_time['high_season']['high_start'] != '' && $best_time['high_season']['high_end'] == '') {
            return redirect()->back()->withErrors('High Season the field is not filled in correctly');
        }
        $lifestyles_id = (array)$request->input('lifestyles_id');
        if (!empty($lifestyles_id) && count($lifestyles_id) !== count(array_unique($lifestyles_id))) {
            return redirect()->back()->withErrors('Travel style should not  be duplicated on a city page.');
        }

        if (!empty($request->input('lifestyles_rating'))) {
            foreach ($request->input('lifestyles_rating') as $key => $rating) {
                if (($key && is_null($rating)) || !($rating >= 0) || !($rating <= 10)) {
                    return redirect()->back()->withErrors('Travel Style\'s rating should be mandatory and need to be 1-10');
                }
            }
        }

        if (!$this->_holidaysDateValidate($request->input('holidays_date'))) {
            return redirect()->back()->withErrors('Date of the Holiday is required');
        }

        $location = explode(',', $request->input('lat_lng'));
        /* Check if active field is enabled or disabled */
        $active = Cities::ACTIVE;
        if (empty($request->input('active')) || $request->input('active') == 0) {
            $active = Cities::DEACTIVE;
        }

        /* Check if capital field is enabled or disabled */
        $is_capital = Cities::IS_CAPITAL;
        if (empty($request->input('is_capital')) || $request->input('is_capital') == 0) {
            $is_capital = Cities::IS_NOT_CAPITAL;
        }

        /* Pass All Relation and Common Fields Through $extra Array */
        $extra = [
            'active' => $active,
            'is_capital' => $is_capital,
            'countries_id' => $request->input('countries_id'),
            'code' => $request->input('code') ? $request->input('code') : 0,
            'lat' => $location[0] ? $location[0] : 0,
            'lng' => isset($location[1]) ? $location[1] : 0,
            'currencies' => $request->input('currencies_id') ? $request->input('currencies_id') : '',
            'emergency_numbers' => $request->input('emergency_numbers_id') ? $request->input('emergency_numbers_id') : '',
            'holidays' => $request->input('holidays_id') ? $request->input('holidays_id') : '',
            'holidays_date' => $request->input('holidays_date'),

            'languages_spoken' => $request->input('languages_spoken_id') ? $request->input('languages_spoken_id') : '',
            'additional_languages_spoken' => $request->input('additional_languages_spoken_id') ? $request->input('additional_languages_spoken_id') : '',
            'lifestyles' => $request->input('lifestyles_id') ? $request->input('lifestyles_id') : '',
            'lifestyles_rating' => $request->input('lifestyles_rating'),
            'religions' => $request->input('religions_id') ? $request->input('religions_id') : '',
            'level_of_living_id' => $request->input('level_of_living_id') ? $request->input('level_of_living_id') : 0,
            'license_images' => $request->license_images,
            'best_time' => $request->input('best_time')
        ];

        $this->cities->create($request->translations, $extra);

        return redirect()->route('admin.location.city.index')->withFlashSuccess('City Created!');
    }

    /**
     * @param Cities $id
     * @param ManageCityRequest $request
     *
     * @return mixed
     */
    public function edit($id, ManageCityRequest $request)
    {
        /* $data array to pass data to view */
        $data = [];
        $city = Cities::with('cityHolidays')->find($id);

        $data['translations'] = [];
        foreach ($this->languages as $language) {
            if ( $translation = CitiesTranslations::where(['cities_id' => $id, 'languages_id' => $language->id])->first() ) {
                $data['translations'][] = $translation;
            } else {
                $empty_translation = new CitiesTranslations;
                if(!is_null($city->transsingle)) {
                    foreach ($city->transsingle->toArray() as $key => $value) {
                        $empty_translation->$key = null;
                    }
                }
                unset($empty_translation->id);
                $empty_translation->languages_id  = $language->id;
                $empty_translation->cities_id     = $city->transsingle->cities_id;
                $empty_translation->transsingle = $language;
                $data['translations'][] = $empty_translation;
            }
        }

        $data['abouts'] = [];
        if( $abouts = CitiesAbouts::where(['cities_id' => $id]) ) {
            $data['abouts'] = CitiesAbouts::where(['cities_id' => $id])->get()->toArray();
        }

        /* Put All Common Fields In $data Array To Be Used In Edit Form */
        $data['lat_lng'] = $city['lat'] . ',' . $city['lng'];
        $data['code'] = $city['code'];
        $data['active'] = $city['active'];
        $data['is_capital'] = $city['is_capital'];
        $data['countries_id'] = $city['countries_id'];

        /* Find All Active Countries */
        $countries              = Countries::where(['active' => Countries::ACTIVE])->get();
        $countries_arr          = [];
        $countries_location_arr = [];

        /* Get Title Id Pair For Each Model */
        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat'] = $value->lat;
                $countries_location_arr[$value->id]['lng'] = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }

        /* Get Selected Currencies */
        $selected_currencies = $city->currencies;
        $selected_currencies_arr = [];

            foreach ($selected_currencies as $key => $value) {
                $selected_currencies_arr['ids'][] = $value->id;
                $selected_currencies_arr['currencies'][$value->id] = $value->transsingle->title;
            }
        
        if(isset($selected_currencies_arr['ids'])) $data['selected_currencies'] = $selected_currencies_arr['ids'];
        else $data['selected_currencies'] = array();
        
        //dd($data['selected_currencies']);
        
        /* Find All Currencies In The System */
        
        $currencies = \App\Models\Currencies\Currencies::where(['active' => Religion::ACTIVE])->get();
        $currencies_arr = [];
        /* Get Title Id Pair For Each Model */
        foreach ($currencies as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $currencies_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['currencies'] = $currencies_arr;

        /* Get Selected Emergency Numbers */
        $selected_numbers = $city->emergency;
        $selected_numbers_arr = [];
        foreach ($selected_numbers as $key => $value) {
            $selected_numbers_arr['numbers'][$value->id] = $value->transsingle->title;
            $selected_numbers_arr['ids'][] = $value->id;
        }

        if(isset($selected_numbers_arr['ids'])) $data['emergency_numbers'] = $selected_numbers_arr['ids'];
        else $data['emergency_numbers'] = array();


        $selected_holidays = $city->cityHolidays;
        $selected_holidays_arr = [];

        if (!empty($selected_holidays)) {
            foreach ($selected_holidays as $key => $value) {
                $selected_holidays_arr['ids'][] = $value->holidays_id;
                $selected_holidays_arr['holiday_date'][$value->holidays_id] = $value->date;
            }
        }

        $data['selected_holidays'] = $selected_holidays_arr;

        /* Get All Holidays For Dropdown*/
        $holidays = Holidays::get();
        $holidays_arr = [];

        foreach ($holidays as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $holidays_arr[$value->id] = $value->transsingle->title;
            }
        }

        $data['selected_holidays']['all_holidays'] = $holidays_arr;
        $data['selected_holidays']['all_holidays_date'] = $holidays_arr;


        /* Get Selected Languages Spoken */
        $selected_languages_spoken = $city->languages_spoken;
        $selected_languages_spoken_arr = [];
        /* Get Selected Id Pair From Each Model */
        foreach ($selected_languages_spoken as $key => $value) {
            $selected_languages_spoken_arr['languages'][$value->id] = $value->transsingle->title;
            $selected_languages_spoken_arr['ids'][]                 = $value->id;
        }

        if(isset($selected_languages_spoken_arr['ids'])) $data['selected_languages_spoken'] = $selected_languages_spoken_arr['ids'];
        else $data['selected_languages_spoken'] = array();

        /* Get Selected Languages Spoken */
        $additional_selected_languages_spoken = $city->additional_languages;
        $additional_selected_languages_spoken_arr = [];
        /* Get Selected Id Pair From Each Model */
        foreach ($additional_selected_languages_spoken as $key => $value) {
            $additional_selected_languages_spoken_arr['languages'][$value->id] = $value->transsingle->title;
            $additional_selected_languages_spoken_arr['ids'][]                 = $value->id;
        }

        if(isset($additional_selected_languages_spoken_arr['ids'])) $data['additional_selected_languages_spoken'] = $additional_selected_languages_spoken_arr['ids'];
        else $data['additional_selected_languages_spoken'] = array();
        
        /* Find All Languages In The System */
        
        $spoken_languages = \App\Models\LanguagesSpoken\LanguagesSpoken::where(['active' => Religion::ACTIVE])->get();
        $spoken_languages_arr = [];
        /* Get Title Id Pair For Each Model */
        foreach ($spoken_languages as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $spoken_languages_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['spoken_languages'] = $spoken_languages_arr;
       // dd($data['languages']);

        /* Get Selected Lifestyles */
        $selected_lifestyles = $city->lifestyles;
        $selected_lifestyles_arr = [];
        /* Get Selected Id Pair From Each Model */
        if (!empty($selected_lifestyles)) {
            foreach ($selected_lifestyles as $key => $value) {
                $lifestyle = $value->lifestyle;
                if (!empty($lifestyle)) {
                    $selected_lifestyles_arr['ids'][] = $lifestyle->id;
                    $selected_lifestyles_arr['title'][$lifestyle->id] = $lifestyle->transsingle->title;
                    $selected_lifestyles_arr['rating'][$lifestyle->id] = $value->rating;
                }
            }
        }
        $data['selected_lifestyles'] = $selected_lifestyles_arr;

        /* Find All CitiesLifestyles In The System */
        $lifestyles = Lifestyle::with('transsingle')->get();
        $lifestyles_arr = [];
        /* Get Title Id Pair For Each Model */
        foreach ($lifestyles as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $lifestyles_arr[$value->id] = $value->transsingle->title;
            }
        }
        
        
        $numbers = \App\Models\EmergencyNumbers\EmergencyNumbers::get();
        $numbers_arr = [];
        /* Get Title Id Pair For Each Model */
        foreach ($numbers as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $numbers_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['numbers'] = $numbers_arr;

        /* Get Selected Religions */
        $selected_religions = $city->religions;
        $selected_religions_arr = [];
        /* Get Selected Id Pair From Each Model */
        foreach ($selected_religions as $key => $value) {
            array_push($selected_religions_arr, $value->id);
        }

        $data['selected_lifestyles']['all_lifestyles'] = $lifestyles_arr;
        $data['selected_lifestyles']['all_lifestyles_rating'] = $lifestyles_arr;
        /* Find All Religions In The System */
        $religion = Religion::where(['active' => Religion::ACTIVE])->get();
        $religion_arr = [];
        /* Get Title Id Pair For Each Model */
        foreach ($religion as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $religion_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['media_results'] = ($city->getMedias->isEmpty())? [] : $city->getMedias;

        return view('backend.city.edit', $data)
            ->withLanguages($this->languages)
            ->withCity($city)
            ->withCityid($id)
            ->withData($data)
            ->withCountries($countries_arr)
            ->withCountriesLocation($countries_location_arr)
            ->withLifestyles($lifestyles_arr)
            //->withCurrencies($currencies)
            ->withReligions($religion_arr);
    }

    /**
     * @param Cities $id
     * @param ManageCityRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateCityRequest $request)
    {
        // dd($request->input());
        // $best_time = $request->input('best_time');
        // if($best_time['low_season']['low_start'] != '' && $best_time['low_season']['low_end'] == '') {
        //     return redirect()->back()->withErrors('Low Season the field is not filled in correctly');
        // }
        // if($best_time['shoulder']['shoulder_start'] != '' && $best_time['shoulder']['shoulder_end'] == '') {
        //     return redirect()->back()->withErrors('Shoulder the field is not filled in correctly');
        // }
        // if($best_time['high_season']['high_start'] != '' && $best_time['high_season']['high_end'] == '') {
        //     return redirect()->back()->withErrors('High Season the field is not filled in correctly');
        // }
        // dd($best_time);
        $lifestyles_id = (array)$request->input('lifestyles_id');
        if (!empty($lifestyles_id) && count($lifestyles_id) !== count(array_unique($lifestyles_id))) {
            return redirect()->back()->withErrors('Travel style should not  be duplicated on a city page.');
        }

        if (!empty($request->input('lifestyles_rating'))) {
            foreach ($request->input('lifestyles_rating') as $key => $rating) {
                if (is_null($rating) || !($rating >= 0) || !($rating <= 10)) {
                    return redirect()->back()->withErrors('Travel Style\'s rating should be mandatory and need to be 1-10');

                }
            }
        }

        if (!$this->_holidaysDateValidate($request->input('holidays_date'))) {
            return redirect()->back()->withErrors('Date of the Holiday is required');
        }

        $city = Cities::find($id);
        $location = explode(',', $request->input('lat_lng'));

        /* Check if active field is enabled or disabled */
        $active = Cities::ACTIVE;
        if (empty($request->input('active')) || $request->input('active') == 0) {
            $active = Cities::DEACTIVE;
        }

        /* Check if is_capital field is enabled or disabled */
        $is_capital = Cities::IS_CAPITAL;
        if (empty($request->input('is_capital')) || $request->input('is_capital') == 0) {
            $is_capital = Cities::IS_NOT_CAPITAL;
        }

        $extra = [
            'active' => $active,
            'is_capital' => $is_capital,
            'countries_id' => $request->input('countries_id'),
            'code' => $request->input('code'),
            'lat' => $location[0],
            'lng' => $location[1],
            'currencies' => $request->input('currencies_id'),
            'holidays' => $request->input('holidays_id'),
            'holidays_date' => $request->input('holidays_date'),
            'lifestyles' => $request->input('lifestyles_id'),
            'lifestyles_rating' => $request->input('lifestyles_rating'),
            'emergency_numbers' => $request->input('emergency_numbers_id'),
            'languages_spoken' => $request->input('languages_spoken_id'),
            'additional_languages_spoken' => $request->input('additional_languages_spoken_id'),
            'religions' => $request->input('religions_id'),
            'license_images' => [
                'images'    => $request->license_images,
                'deleted'   => $request->del
            ],
            'abouts' => $request->input('abouts'),
            'best_time' => $request->input('best_time'),
            'daily_costs' => $request->input('daily_costs'),
            'transportation' => $request->input('transportation'),
            'sockets_plugs' => $request->input('sockets_plugs')
        ];

        $this->cities->update($id, $city, $request->translations, $extra);

        return redirect()->route('admin.location.city.index')
            ->withFlashSuccess('City updated Successfully!');
    }

    /**
     * @param $id
     * @param ManageCityRequest $request
     * @return mixed
     */
    public function show($id, ManageCityRequest $request)
    {
        $city = Cities::findOrFail($id);
        $cityTrans = CitiesTranslations::where(['cities_id' => $id])->get();
        $cityAbouts = CitiesAbouts::where(['cities_id' => $id])->get();

        /* Get Country Information */
        $country = $city->country;
        $country = $country->transsingle;

        /* Get Safety Degrees Information */
        $safety_degree_temp = $city->degree;
        $safety_degree = null;
        if (!empty($safety_degree_temp)) {
            $safety_degree = $safety_degree_temp->transsingle;
        }

        /*Get Airport Locations*/
        $airports = $city->airports;
        $airports_arr = [];

        /* If Model Exist, Get Translated Title For Each Model */
        if (!empty($airports)) {
            foreach ($airports as $key => $value) {

                $place = $value->place;
                if (!empty($place)) {

                    $place = $place->transsingle;
                    if (!empty($place)) {
                        array_push($airports_arr, $place->title);
                    }
                }
            }
        }

        /*Get Currencies*/
        $currencies = $city->currencies;
        $currencies_arr = [];

        /* If Model Exist, Get Translated Title For Each Model */
        if (!empty($currencies)) {
            foreach ($currencies as $key => $value) {

                $currency = $value;
                if (!empty($currency)) {

                    $currency = $currency->transsingle;
                    if (!empty($currency)) {
                        array_push($currencies_arr, $currency->title);
                    }
                }
            }
        }

        /*Get EmergencyNumbers*/
        $emergency_numbers = $city->emergency;
        $emergency_numbers_arr = [];

        /* If Model Exist, Get Translated Title For Each Model */
        if (!empty($emergency_numbers)) {
            foreach ($emergency_numbers as $key => $value) {

                $number = $value;

                if (!empty($number)) {

                    $number = $number->transsingle;
                    if (!empty($number)) {
                        array_push($emergency_numbers_arr, $number->title);
                    }
                }
            }
        }

        /* Get Holidays */
        $holidays = $city->holidays;
        $holidays_arr = [];
        /* If Model Exist, Get Translated Title For Each Model */
        if (!empty($holidays)) {
            foreach ($holidays as $key => $value) {

                $holiday = $value;

                if (!empty($holiday)) {

                    $holiday = $holiday->transsingle;
                    if (!empty($holiday)) {
                        array_push($holidays_arr, $holiday->title);
                    }
                }
            }
        }

        /* Get Languages Spoken */
        $languages_spoken = $city->languages_spoken;
        $languages_spoken_arr = [];

        /* If Model Exist, Get Translated Title For Each Model */
        if (!empty($languages_spoken)) {
            foreach ($languages_spoken as $key => $value) {

                $language_spoken = $value;

                if (!empty($language_spoken)) {

                    $language_spoken = $language_spoken->transsingle;
                    if (!empty($language_spoken)) {
                        array_push($languages_spoken_arr, $language_spoken->title);
                    }
                }
            }
        }

        /* Get Additional Languages Spoken */
        $additional_languages_spoken = $city->additional_languages_spoken;
        $additional_languages_spoken_arr = [];

        /* If Model Exist, Get Translated Title For Each Model */
        if (!empty($additional_languages_spoken)) {
            foreach ($additional_languages_spoken as $key => $value) {

                $language_spoken = $value->languages_spoken;

                if (!empty($language_spoken)) {

                    $language_spoken = $language_spoken->transsingle;
                    if (!empty($language_spoken)) {
                        array_push($additional_languages_spoken_arr, $language_spoken->title);
                    }
                }
            }
        }

        /* Get Languages Spoken */
        $lifestyles = $city->lifestyles;
        $lifestyles_arr = [];

        /* If Model Exist, Get Translated Title For Each Model */
        if (!empty($lifestyles)) {
            foreach ($lifestyles as $key => $value) {

                $lifestyle = $value->lifestyle;

                if (!empty($lifestyle)) {

                    $lifestyle = $lifestyle->transsingle;
                    if (!empty($lifestyle)) {
                        $lifestyles_arr[] = [
                            'rating' => $value->rating,
                            'title' => $lifestyle->title
                        ];
                    }
                }
            }
        }

        /* Get All Added Medias */
        $medias = $city->medias;
        $medias_arr = [];
        $images_arr = [];

        /* If Model Exist, Get Translated Title For Each Model */
        if (!empty($medias)) {
            foreach ($medias as $key => $value) {

                $media = $value->medias;

                if (!empty($media)) {
                    if ($media->type != Media::TYPE_IMAGE) {
                        $media = $media->transsingle;

                        if (!empty($media)) {
                            array_push($medias_arr, $media->title);
                        }
                    } else {
                        array_push($images_arr, $media->url);
                    }
                }
            }
        }

        /* Get All Added Religions */
        $religions = $city->religions;
        $religions_arr = [];

        /* If Model Exist, Get Translated Title For Each Model */
        if (!empty($religions)) {
            foreach ($religions as $key => $value) {

                $religion = $value;

                if (!empty($religion)) {

                    $religion = $religion->transsingle;

                    if (!empty($religion)) {
                        array_push($religions_arr, $religion->title);
                    }
                }
            }
        }

        /* Get Cover Image Of Country */
        $cover = null;
        if (!empty($city->cover)) {
            $cover = $city->cover;
        }

        return view('backend.city.show')
            ->withCity($city)
            ->withCitytrans($cityTrans)
            ->withCityabouts($cityAbouts)
            ->withCountry($country)
            ->withDegree($safety_degree)
            ->withAirports($airports_arr)
            ->withCurrencies($currencies_arr)
            ->withHolidays($holidays_arr)
            ->withHolidaysDate($city->cityHolidays)
            ->withLifestyles($lifestyles_arr)
            ->withLanguagesSpoken($languages_spoken_arr)
            ->withAdditionalLanguagesSpoken($additional_languages_spoken_arr)
            ->withMedias($medias_arr)
            ->withImages($images_arr)
            ->withEmergencynumbers($emergency_numbers_arr)
            ->withReligions($religions_arr)
            ->withCover($cover);
    }

    /**
     * @param $id
     * @param ManageCityRequest $request
     * @return mixed
     */
    public function destroy($id, ManageCityRequest $request)
    {
        $item = Cities::findOrFail($id);
        $item->deleteTrans();
        $item->deleteMedias();
        $country = Countries::find($item->countries_id);
        $item->delete();
        if(!is_null($country)) {

            $this->updateStatistic($country, 'cities', count($country->cities));
        }
        return redirect()->route('admin.location.city.index')->withFlashSuccess('City Deleted Successfully');
    }

    /**
     * @param Cities $city
     * @param $status
     *
     * @param ManageCityRequest $request
     * @return mixed
     */
    public function mark(Cities $city, $status, ManageCityRequest $request)
    {
        $city->active = $status;
        $city->save();
        return redirect()->route('admin.location.city.index')
            ->withFlashSuccess('City Status Updated!');
    }

    public function delete_ajax(ManageCityRequest $request)
    {
        $ids = $request->input('ids');
        if (!empty($ids)) {
            $ids = explode(',', $request->input('ids'));
            foreach ($ids as $key => $value) {
                $this->delete_single_ajax($value);
            }
        }

        echo json_encode([
            'result' => true
        ]);
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete_single_ajax($id)
    {
        $item = Cities::find($id);

        if (empty($item)) {
            return false;
        }

        $item->deleteTrans();
        $country = Countries::find($item->countries_id);
        $item->delete();
        if (!is_null($country)) {
            $this->updateStatistic($country, 'cities', count($country->cities));
        }

        AdminLogs::create(['item_type' => 'cities', 'item_id' => $id, 'action' => 'delete', 'time' => time(), 'admin_id' => Auth::user()->id]);
    }

    public function getAddedCountries(){
        $q = null;
        if(isset($_GET['q'])){
            $q = $_GET['q'];
        }

        $place = Cities::distinct()->select('countries_id')->get();
        $temp_country = [];
        $filter_html = null;
        $json = [];

        if(!empty($place)){
            foreach ($place as $key => $value) {
                $country = null;
                if(empty($q)){
                    $country = Countries::find($value->countries_id);

                }else{
                    $country = Countries::leftJoin('countries_trans', function($join){
                        $join->on('countries_trans.countries_id', '=', 'countries.id');
                    })->where('countries_trans.title', 'LIKE', '%'.$q.'%')->where(['countries.id' => $value->countries_id])->first();
                }
                if(!empty($country)){
                    $transingle = CountriesTranslations::where(['countries_id' => $value->countries_id])->first();

                    if(!empty($transingle)){
                        $filter_html .= '<option value="'.$value->countries_id.'">'.$transingle->title.'</option>';
                        array_push($temp_country,$transingle->title);
                        $json[] = ['id' => $value->countries_id, 'text' => $transingle->title];
                    }
                }
            }
        }
        echo json_encode($json);
    }

    /**
     * @param $holidaysDate
     * @return bool
     */
    private function _holidaysDateValidate($holidaysDate)
    {
        if (!empty($holidaysDate)) {
            foreach ($holidaysDate as $key => $holidayDate) {
                if (is_null($holidayDate) && $key > 0) {
                    return false;
                }
            }
        }

        return true;
    }
}
