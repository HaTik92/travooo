<?php

namespace App\Http\Controllers\Backend\Place;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Http\Requests\Backend\Place\PlaceMassApplyFiltersRequest;
use App\Http\Requests\Backend\Place\PlaceMassApplyMoveToRequest;
use App\Http\Requests\Backend\Place\PlaceMassPageRequest;
use App\Models\City\CitiesTranslations;
use App\Models\Country\CountriesTranslations;
use App\Models\Place\Place;
use App\Models\Place\Medias;
use App\Models\Place\PlaceMedias;
use App\Models\Place\PlaceTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Place\ManagePlaceRequest;
use App\Http\Requests\Backend\Place\StorePlaceRequest;
use App\Http\Requests\Backend\Place\UpdatePlaceRequest;
use App\Models\PlacesTop\PlacesTop;
use App\Repositories\Backend\Place\PlaceRepository;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\AdminLogs\AdminLogs;
use App\Models\PlaceSearchHistory\PlaceSearchHistory;
use App\Models\ActivityMedia\Media;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Utilities\Request;

class PlaceController extends Controller {

    use CreateUpdateStatisticTrait;

    protected $places;
    private $placeTypesArray = [
        'airport',
        'amusement_park',
        'aquarium',
        'art_gallery',
        'bakery',
        'bar',
        'cafe',
        'campground',
        'casino',
        'cemetery',
        'church',
        'city_hall',
        'embassy',
        'hindu_temple',
        'library',
        'light_rail_station',
        'lodging',
        'meal_delivery',
        'meal_takeaway',
        'mosque',
        'movie_theater',
        'museum',
        'night_club',
        'park',
        'restaurant',
        'shopping_mall',
        'stadium',
        'subway_station',
        'synagogue',
        'tourist_attraction',
        'train_station',
        'transit_station',
        'zoo',
        'food',
        'place_of_worship',
        'natural_feature',
        'point_of_interest',
        'premise',
        'poi',

    ];

    /**
     * PlaceController constructor.
     * @param PlaceRepository $places
     */
    public function __construct(PlaceRepository $places) {
        $this->places = $places;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManagePlaceRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManagePlaceRequest $request) {
        $term = trim($request->q);
        if (!empty($term)) {
            $places_trans = PlaceTranslations::where('title', 'LIKE', $term . '%')
                    ->join('places', function ($query) {
                        $query->on('places_trans.places_id', '=', 'places.id')
                        ->where('places.active', 1);
                    })
                    ->skip(($request->page - 1 ) * 10)
                    ->take(10)
                    ->get();
            $paginate = true;
            if ($places_trans->isEmpty()) {
                $places_trans = PlaceTranslations::where('title', 'LIKE', $term . '%')
                        ->join('places', function ($query) {
                            $query->on('places_trans.places_id', '=', 'places.id')
                            ->where('places.active', 1);
                        })
                        ->get();
                $paginate = false;
            }
            $formatted_places = [];

            foreach ($places_trans as $value) {
                $formatted_places[] = ['id' => $value->places_id, 'text' => $value->title];
            }
            return response()->json(['places' => $formatted_places, 'paginate' => $paginate]);
        }
        return view('backend.place.index');
    }

    /**
     * @param PlaceMassPageRequest $request
     * @return mixed
     */
    public function mass(PlaceMassPageRequest $request) {
        $action =  $request->input('action');
        return view('backend.place.mass')->withAction($action);
    }

    /**
     * @param PlaceMassApplyFiltersRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyMassFilter(PlaceMassApplyFiltersRequest $request){

        $query = Place::query();

        $query->when( $request->input('city_id'), function ($query, $city_id) {
            return $query->where('cities_id', $city_id);
        });

        $query->when( $request->input('country_id'), function ($query, $country_id) {
            return $query->where('countries_id', $country_id);
        });

        $query->when( $request->input('action'), function ($query, $action) {
            if ($action === 'unapproved') {
                return $query->where('auto_import', '=', 1);
            } else {
                return $query->where(function ($query) {
                    $query->where('auto_import', '!=', 1)
                        ->orWhere('auto_import', null);
                });
            }
        });

        $addressMatch = $request->input('address');
        $addressNotMatch = $request->input('address_not_match');

        if( ! is_null($addressMatch) || ! is_null($addressNotMatch) ) {
            $query->join('places_trans', function ($join) use ($addressMatch, $addressNotMatch) {
                $join->on('places.id', '=', 'places_trans.places_id')
                    ->when($addressMatch, function($query) use($addressMatch){
                        $addressValues = explode("/", $addressMatch );
                        return $query->where(function ($query) use ($addressValues) {
                            foreach ($addressValues as $key => $address) {
                                if ($key)
                                    $query->orWhere('places_trans.address', 'LIKE', "%" . $address . "%");
                                else
                                    $query->where('places_trans.address', 'LIKE', "%" . $address . "%");
                            }
                        });
                    })
                    ->when($addressNotMatch, function($query) use($addressNotMatch){
                        $notMatchValues = explode("/", $addressNotMatch );
                        return $query->where(function ($query) use ($notMatchValues) {
                            foreach ($notMatchValues as $key => $notMatchValue) {
                                $query->where('places_trans.address', 'NOT LIKE', "%" . $notMatchValue . "%");
                            }
                        });
                    });
            });
        }

        $pluscode = $request->input('pluscode');
        $pluscodeNotMatch = $request->input('pluscode_not_match');

        $query->when( $pluscode, function ($query) use ($pluscode) {
            return $query->where('places.pluscode', 'LIKE', "%".$pluscode."%");
        });

        $query->when( $pluscodeNotMatch, function ($query) use ($pluscodeNotMatch) {
            $notMatchValues = explode("/", $pluscodeNotMatch );
            return $query->where(function ($query) use ($notMatchValues) {
                foreach ($notMatchValues as $key => $notMatchValue2) {
                    $query->where('places.pluscode', 'NOT LIKE', "%" . $notMatchValue2 . "%");
                }
            });
        });

        $ids = $query
            ->pluck('places.id');

        return response()->json($ids);
    }

    public function applyMoveToAction(PlaceMassApplyMoveToRequest $request){

        Place::whereIn('id', $request->input('ids'))->update([
                'cities_id' => $request->input('city_id'),
                'countries_id' => $request->input('country_id'),
                'auto_import'  => 0
            ]
        );

        return response()->json([]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table(Request $request) {
        foreach ($request->columns as $column) {
            if ($column['data'] == 'address') {
                $matchValue     = @$column['search']['value1']['matchValue'];
                $notMatchValue  = @$column['search']['value2']['notMatchValue'];
            }
            if ($column['data'] == 'pluscode') {
                $matchValue2     = @$column['search']['value1']['matchValue2'];
                $notMatchValue2  = @$column['search']['value2']['notMatchValue2'];
            }
            if ($column['data'] == 'section') {
                $section = $column['search']['value'];
            }
            if ($column['data'] == 'country_title') {
                $countryId = $column['search']['value'];
            }
            if ($column['data'] == 'city_title') {
                $cities_id = $column['search']['value'];
            }
            if ($column['data'] == 'place_id_title') {
                $placeType = $column['search']['value'];
            }
            if ($column['data'] == 'title') {
                $placeTitle = $column['search']['value'];
            }
            if($column['data'] == 'media_done_new') {
                $mediaDoneNew = $column['search']['value'];
            }
        }

        $orderData = current($request->order);
        $orderColumn = $request->columnName($orderData['column']);
        $orderDir = $orderData['dir'];

//        $query = Place::select([
//            config('locations.place_table') . '.id',
//            DB::raw('MAX(' . config('locations.place_table') . '.cities_id)'),
//            DB::raw('MAX(' . config('locations.place_table') . '.countries_id)'),
//            DB::raw('MAX(' . config('locations.place_table') . '.place_type) as place_id_title'),
//            DB::raw('MAX(' . config('locations.place_table') . '.media_done)'),
//            DB::raw('MAX(' . config('locations.place_table') . '.active) as active'),
//            DB::raw('MAX(' . config('locations.place_table') . '.media_count) as media_count'),
//            DB::raw('MAX(transsingle.title) as title'),
//            DB::raw('MAX(transsingle.address) as address'),
//            DB::raw('MAX(cities_trans.title) as city_title'),
//            DB::raw('MAX(countries_trans.title) as country_title')
//        ])
//            ->join(DB::raw('places_trans AS transsingle FORCE INDEX (places_id)'), function ($query) use ($placeTitle, $matchValue, $notMatchValue) {
//                $query->on('transsingle.places_id', '=', 'places.id')
//                    ->on('transsingle.languages_id', '=', DB::raw(1));
//                if ($placeTitle) {
//                    $this->filtered = true;
//                    $query->on('transsingle.title', 'LIKE', '%' . $placeTitle . '%');
//                }
//
//                if ($matchValue) {
//                    $this->filtered = true;
//                    $query->where('transsingle.address', 'LIKE', '%' . $matchValue . '%');
//                }
//
//                if ($notMatchValue) {
//                    $this->filtered = true;
//                    $notMatchValues = explode("/", $notMatchValue);
//                    foreach ($notMatchValues as $key => $notMatchValue) {
//                        $query->where('transsingle.address', 'NOT LIKE', "%" . $notMatchValue . "%");
//                    }
//                }
//            })
//            ->leftJoin('cities_trans', function ($query) {
//                $query->on('cities_trans.cities_id', '=', 'places.cities_id')
//                    ->on('cities_trans.languages_id', '=', DB::raw(1));
//            })
//            ->leftJoin('countries_trans', function ($query) {
//                $query->on('countries_trans.countries_id', '=', 'places.countries_id')
//                    ->on('countries_trans.languages_id', '=', DB::raw(1));
//            })
////            ->when($matchValue, function ($query) use ($matchValue) {
////                $this->filtered = true;
////                return $query->where('transsingle.address', 'LIKE', "%" . $matchValue . "%");
////            })
////            ->when($notMatchValue, function ($query) use ($notMatchValue) {
////                $this->filtered = true;
////                $notMatchValues = explode("/", $notMatchValue);
////                $query->where(function ($query) use ($notMatchValues) {
////                    foreach ($notMatchValues as $key => $notMatchValue) {
////                        $query->where('transsingle.address', 'NOT LIKE', "%" . $notMatchValue . "%");
////                    }
////                });
////            })
//            ->when($countryId, function ($query) use ($countryId) {
//                $this->filtered = true;
//                return $query->where('places.countries_id', $countryId);
//            })
//            ->when($cities_id, function ($query) use ($cities_id) {
//                $this->filtered = true;
//                return $query->where('places.cities_id', $cities_id);
//            })
//            ->when($placeType, function ($query) use ($placeType) {
//                $this->filtered = true;
//                return $query->where('places.place_type', $placeType);
//            })
////            ->when($placeTitle, function ($query) use ($placeTitle) {
////                $this->filtered = true;
////                return $query->where('transsingle.title', 'LIKE', '%' . $placeTitle . '%');
////            })
//            ->groupBy('places.id');
        
        $query = Place::select([
            config('locations.place_table') . '.id',
            DB::raw('MAX(' . config('locations.place_table') . '.cities_id)'),
            DB::raw('MAX(' . config('locations.place_table') . '.countries_id)'),
            DB::raw('MAX(' . config('locations.place_table') . '.place_type) as place_id_title'),
            DB::raw('MAX(' . config('locations.place_table') . '.media_done)'),
            DB::raw('MAX(' . config('locations.place_table') . '.active) as active'),
            DB::raw('MAX(' . config('locations.place_table') . '.media_count) as media_count'),
            DB::raw('MAX(' . config('locations.place_table') . '.place_section) as section'),
            DB::raw('MAX(' . config('locations.place_table') . '.pluscode) as pluscode'),
            DB::raw('MAX(transsingle.title) as title'),
            DB::raw('MAX(transsingle.address) as address'),
            DB::raw('MAX(cities_trans.title) as city_title'),
            DB::raw('MAX(countries_trans.title) as country_title')
        ])
            ->where(function ($query) {
                $query->where('auto_import', '!=', 1)
                      ->orWhere('auto_import', null);
//                foreach($this->placeTypesArray as $key=>$placeType){
//                    if($key == 0)
//                        $query->whereRaw('SUBSTRING_INDEX( place_type, ",", 1 ) like "%'.$placeType.'%"');
//                    else
//                        $query->OrwhereRaw('SUBSTRING_INDEX( place_type, ",", 1 ) like "%'.$placeType.'%"');
//                }
            })
            ->leftJoin(DB::raw('places_trans AS transsingle FORCE INDEX (places_id)'), function ($query) {
                $query->on('transsingle.places_id', '=', 'places.id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->leftJoin('cities_trans', function ($query) {
                $query->on('cities_trans.cities_id', '=', 'places.cities_id')
                    ->where('cities_trans.languages_id', '=', 1);
            })
            ->Leftjoin('countries_trans', function ($query) {
                $query->on('countries_trans.countries_id', '=', 'places.countries_id')
                    ->where('countries_trans.languages_id', '=', 1);
            })
            ->when($matchValue, function ($query) use ($matchValue) {
                $this->filtered = true;
                $matchValues = explode("/", $matchValue );
                return $query->where(function ($query) use ($matchValues) {
                    foreach ($matchValues as $key => $matchValue) {
                        if ($key)
                            $query->orWhere('transsingle.address', 'LIKE', "%".$matchValue."%");
                        else
                            $query->where('transsingle.address', 'LIKE', "%".$matchValue."%");

                    }
                });
            })
            ->when($notMatchValue, function ($query) use ($notMatchValue) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue );
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue) {
                        $query->where('transsingle.address', 'NOT LIKE', "%" . $notMatchValue . "%");
                    }
                });
            })
            ->when($matchValue2, function ($query) use ($matchValue2) {
                $this->filtered = true;
                return $query->where('places.pluscode', 'LIKE', "%".$matchValue2."%");
            })
            ->when($notMatchValue2, function ($query) use ($notMatchValue2) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue2 );
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue2) {
                        $query->where('places.pluscode', 'NOT LIKE', "%" . $notMatchValue2 . "%");
                    }
                });
            })
            ->when($section, function ($query) use ($section) {
                $this->filtered = true;
                return $query->where('places.place_section', $section);
            })
            ->when($countryId, function ($query) use ($countryId) {
                $this->filtered = true;
                return $query->where('places.countries_id', $countryId);
            })
            ->when($cities_id, function ($query) use ($cities_id) {
                $this->filtered = true;
                return $query->where('places.cities_id', $cities_id);
            })
            ->when($placeType, function ($query) use ($placeType) {
                $this->filtered = true;
                return $query->where('places.place_type', $placeType);
            })
            ->when($placeTitle, function ($query) use ($placeTitle) {
                $this->filtered = true;
                return $query->where(function ($query) use ($placeTitle) {
                    $query->where('transsingle.title', 'LIKE', '%'.$placeTitle.'%')
                        ->orWhere('transsingle.address', 'LIKE', '%'.$placeTitle.'%')
                        ->orWhere('places.pluscode', 'LIKE', '%'.$placeTitle.'%');
                });
            });
            if (isset($mediaDoneNew)) {
                $this->filtered = true;
                $query =  $query->where(config('locations.place_table') . '.media_done',$mediaDoneNew);
            }
        $query->groupBy('places.id');
        if (isset($mediaDoneNew))
        $response['recordsTotal'] = $query->count();

        /*this is a wrong value for $response['recordsFiltered'] count, as $query->count() does not work for large data,
        will fix it after ElasticSearch instalation*/
        $response['recordsFiltered'] = isset($response) ? $response['recordsTotal'] : 1000000;
        $places = $query->offset($request->start)->limit($request->length)->orderBy($orderColumn, $orderDir)->get();
        foreach ($places as $place) {
            $place->empty = '';
            $place->action = $place->action_buttons;
            $place->media_done_new = isset($mediaDoneNew) ? $mediaDoneNew ? 'Yes' : 'No' : 'Yes';
            if(!isset($place->pluscode)){
                $place->pluscode = '-';
            }
        }
        $response['data'] = $places;
        $response['draw'] = $request->draw;
        $response['input'] = $request->all();

        return response()->json($response);
    }

    /**
     * @param ManagePlaceRequest $request
     *
     * @return mixed
     */
    public function create(ManagePlaceRequest $request) {
        /* Get All Countries */
        $countries = Countries::where(['active' => Countries::ACTIVE])->get();
        $countries_arr = [];
        $countries_location_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
                $countries_location_arr[$value->id]['lat'] = $value->lat;
                $countries_location_arr[$value->id]['lng'] = $value->lng;
                $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
            }
        }

        return view('backend.place.create', [
            'countries' => $countries_arr,
            'countries_location' => $countries_location_arr
        ]);
    }

    /**
     * @param StorePlaceRequest $request
     *
     * @return mixed
     */
    public function store(StorePlaceRequest $request) {
        $location = explode(',', $request->input('lat_lng'));
        $active = 1;

        if (empty($request->input('active')) || $request->input('active') == 0) {
            $active = 2;
        }

        /* Send All Relation and Common Fields Through $extra Array */
        $extra = [
            'active' => $active,
            'countries_id' => $request->input('countries_id'),
            'cities_id' => $request->input('cities_id'),
            'lat' => $location[0],
            'lng' => $location[1],
            'medias' => $request->input('medias_id'),
            'license_images' => $request->license_images
        ];

        $this->places->create($request->translations, $extra);  /// save place data
        $this->saveAdditionalImgsInfo($request);

        $country = Countries::find($request->input('countries_id'));
        $this->updateStatistic($country, 'places', count($country->places));

        $city = Cities::find($request->input('cities_id'));
        $this->updateStatistic($city, 'places', count($city->places));

        AdminLogs::create(['item_type' => 'places', 'item_id' => 0, 'action' => 'create', 'time' => time(), 'admin_id' => Auth::user()->id]);

        return redirect()->route('admin.location.place.index')->withFlashSuccess('Place Created!');
    }

    /**
     * @param $request
     * @param int $id
     */
    public function saveAdditionalImgsInfo($request, $id = 0) {
        $rowMedias = DB::table('medias')->orderBy('id', 'desc')->first(); //get last media id

        $mediaID = $rowMedias->id;
        $media_id = $mediaID + 1;

        if ($id > 0) {
            $placeID = $id;
            $this->deletePlacesMedia($id);
        } else {
            $rowPlace = DB::table('places')->orderBy('id', 'desc')->first(); //get last media id
            $placeID = $rowPlace->id;
        }

        $file_title = $request->input('file_title');
        $data_fields = array();

        if (!empty($file_title)) {
            $i = 0;

            foreach ($file_title as $rowfield) {
                if (!empty($rowfield)) {
                    $author_name = $request->input('author_name')[$i];
                    $author_link = $request->input('author_link')[$i];
                    $source_link = $request->input('source_link')[$i];
                    $license_name = $request->input('license_name')[$i];
                    $license_link = $request->input('license_link')[$i];
                    $filename = $this->saveAdditionalImageIntoFolder($i);
                    $mediasID = $media_id++;

                    if (!empty($filename)) {
                        if (!empty($_POST['image_name_field'][$i])) {
                            File::delete('uploads_images/' . $_POST['image_name_field'][$i]);
                        }

                        $data_fields[] = array('title' => $rowfield, 'url' => $filename, 'author_name' => $author_name, 'author_url' => $author_link, 'source_url' => $source_link, 'license_name' => $license_name, 'license_url' => $license_link);
                    } else {
                        $imageFileName = $_POST['image_name_field'][$i];
                        $data_fields[] = array('title' => $rowfield, 'url' => $imageFileName, 'author_name' => $author_name, 'author_url' => $author_link, 'source_url' => $source_link, 'license_name' => $license_name, 'license_url' => $license_link);
                    }
                    $mediaPlace[] = array('places_id' => $placeID, 'medias_id' => $mediasID);

                    if (!empty($image)) {
                        $imageFileName = time() . '.' . $image->getClientOriginalExtension();

                        /**
                         * @var Storage $s3
                         */
                        $s3 = Storage::disk('s3');
                        $filePath = '/places/more/' . $imageFileName;

                        $s3->put($filePath, file_get_contents($image), 'public');
                    }
                }
                $i++;
            }
            if (!empty($data_fields)) {
                Medias::insert($data_fields);
                PlaceMedias::insert($mediaPlace);
            }
        }
    }

    /**
     * @param int $index
     * @return string
     */
    public function saveAdditionalImageIntoFolder($index = 0) {
        $filesValue = Input::file('upload_imgfile');
        $file = $filesValue[$index];
        $uploaded_file = '';

        if (!empty($file)) {
            $fileName = $file->getClientOriginalName();
            $uploaded_file = strtolower(str_replace(' ', '_', $fileName));
            $getimageName = time() . '_' . $uploaded_file;
            $file->move(public_path('uploads_images'), $getimageName);   /// img saved in: public/uploads_images
        }

        return $getimageName;
    }

    /**
     * @param int $id
     * @param string $delete
     * @return bool
     */
    public function deletePlacesMedia($id = 0, $delete = '') {
        $mediasRows = PlaceMedias::where(['places_id' => $id])->get();
        if (!empty($mediasRows)) {
            foreach ($mediasRows as $mdkey => $mdValue) {
                if (!empty($mdValue->medias_id)) {
                    DB::table('medias')->where('id', $mdValue->medias_id)->delete();
                    $this->delete_media_imgs($mdValue->medias_id, $delete);
                }
            }
        }
        DB::table('places_medias')->where('places_id', $id)->delete();
        return true;
    }

    /**
     * @param int $id
     * @param string $image_name
     * @return string
     */
    public function delete_place_img($id = 0, $image_name = '') {
        Media::where('id', $id)
                ->update(['image_src' => '']);

        if (!empty($image_name)) {
            File::delete('uploads_images/' . $image_name);
        }

        return 'Image Deleted successfully!';
    }

    /**
     * @param int $media_id
     * @param string $delete
     */
    public function delete_media_imgs($media_id = 0, $delete = '') {
        if ($media_id > 0 && !empty($delete)) {
            $mediaRow = Medias::where(['id' => $media_id])->get()->last();
            File::delete('uploads_images/' . $mediaRow->image_src);
        }
    }

    /**
     * @param int $place_id
     * @return array
     */
    public function getMediaPlacesRecord($place_id = 0) {
        $mediaPlace = array();

        $medias_rows = Medias::where(['placesID' => $place_id])->get();
        if (!empty($medias_rows) > 0) {
            foreach ($medias_rows as $mdkey => $mdValue) {
                $mediaPlace[] = array('places_id' => $place_id, 'medias_id' => $mdValue->id);
            }
        }

        return $mediaPlace;
    }

    /**
     * @param Place $id
     * @param ManagePlaceRequest $request
     *
     * @return mixed
     */
    public function destroy($id, ManagePlaceRequest $request) {
        $item = Place::findOrFail($id);
        /* Delete Children Tables Data of this country */
        $child = PlaceTranslations::where(['places_id' => $id])->get();
        if (!empty($child)) {
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }

        //$this->deletePlacesMedia($id, 'deleted');  /// delete data in medias and places_medias table
        //$item->deleteMedias();
        $country = Countries::find($item->countries_id);
        $city = Cities::find($item->cities_id);
        $item->delete();
        if (!is_null($country->places)) {
            $this->updateStatistic($country, 'places', count($country->places));
        }
        if (!is_null($city->places)) {
            $this->updateStatistic($city, 'places', count($city->places));
        }

        AdminLogs::create(['item_type' => 'places', 'item_id' => $id, 'action' => 'delete', 'time' => time(), 'admin_id' => Auth::user()->id]);

        return redirect()->route('admin.location.place.index')->withFlashSuccess('Place Deleted Successfully');
    }

    /**
     * @param Place $id
     * @param ManagePlaceRequest $request
     *
     * @return mixed
     */
    public function edit($id, ManagePlaceRequest $request) {
        $data = [];
        $place = Place::findOrFail($id);
        $data['translations'] = [];
        foreach ($this->languages as $language) {

            if ($translation = PlaceTranslations::where(['places_id' => $id, 'languages_id' => $language->id])->first()) {
                $data['translations'][] = $translation;
            } else {
                $empty_translation = new PlaceTranslations;
                foreach ($place->transsingle->toArray() as $key => $value) {
                    $empty_translation->$key = null;
                }
                unset($empty_translation->id);
                $empty_translation->languages_id = $language->id;
                $empty_translation->places_id = $place->transsingle->places_id;
                $empty_translation->transsingle = $language;
                $data['translations'][] = $empty_translation;
            }
        }

        $data['url_path'] = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        $data['lat_lng'] = $place['lat'] . ',' . $place['lng'];
        $data['countries_id'] = $place['countries_id'];
        $data['cities_id'] = $place['cities_id'];
        $data['place_types_ids'] = $place['place_type_ids'];
        $data['active'] = $place['active'];
        $data['safety_degrees_id'] = $place['safety_degrees_id'];

        $media_results = ($place->getMedias->isEmpty()) ? [] : $place->getMedias;

        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->where('countries_id', $place->countries_id)->with('transsingle')->get();
        $cities_arr = [];

        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }

        $countries = Countries::where(['active' => 1])->get();
        $countries_arr = [];
        $countries_location_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                if (isset($value->transsingle) && !empty($value->transsingle)) {
                    $countries_arr[$value->id] = $value->transsingle->title;
                    $countries_location_arr[$value->id]['lat'] = $value->lat;
                    $countries_location_arr[$value->id]['lng'] = $value->lng;
                    $countries_location_arr[$value->id]['iso_code'] = $value->iso_code;
                }
            }
        }
        return view('backend.place.edit', $data, compact('media_results'))
                        ->withLanguages($this->languages)
                        ->withPlace($place)
                        ->withPlaceid($id)
                        ->withData($data)
                        ->withCities($cities_arr)
                        ->withCountries($countries_arr)
                        ->withCountriesLocation($countries_location_arr);
    }

    /**
     * @param Place $id
     * @param ManagePlaceRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdatePlaceRequest $request) {
        $place = Place::findOrFail($id);
        $active = $request->input('active');
        $location = explode(',', $request->input('lat_lng'));

        if (empty($active)) {
            $active = 2;
        }

        /* Send All Relation and Common fields through $extra Array */
        $extra = [
            'active' => $active,
            'countries_id' => $request->input('countries_id'),
            'cities_id' => $request->input('cities_id'),
            //'place_types_ids' => $request->input('place_types_ids'),
            'medias' => $request->input('medias_id'),
            'lat' => $location[0],
            'lng' => $location[1],
            'remove-cover-image' => $request->input('remove-cover-image'),
            'delete-images' => $request->input('delete-images'),
            'license_images' => [
                'images' => $request->license_images,
                'deleted' => $request->del
            ]
        ];

        $this->places->update($id, $place, $request->translations, $extra);

        AdminLogs::create(['item_type' => 'places', 'item_id' => $id, 'action' => 'edit', 'time' => time(), 'admin_id' => Auth::user()->id]);

        return redirect()->route('admin.location.place.index')
                        ->withFlashSuccess('Place updated Successfully!');
    }

    /**
     * @param Place $id
     * @param ManagePlaceRequest $request
     *
     * @return mixed
     */
    public function show($id, ManagePlaceRequest $request) {
        $place = Place::findOrFail($id);
        $placeTrans = PlaceTranslations::where(['places_id' => $id])->get();

        /* Get Country Information */
        $country = $place->country;
        $country = $country->transsingle;

        /* Get Country Information */
        $city = $place->city;
        $city = $city->transsingle;

        /* Get Country Information */
        $type = $place->type;
        if (!empty($type)) {
            $type = $type->transsingle;
        }

        /* Get Safety Degrees Information */
        $safety_degree = $place->degree;
        if (!empty($safety_degrees)) {
            $safety_degree = $safety_degree->transsingle;
        }

        /* Get All Selected Medias */
        $selected_medias = $place->medias;
        $image_urls = [];
        $selected_medias_arr = [];

        foreach ($selected_medias as $key => $value) {
            $value = $value->media;
            if (!empty($value) && $value->type == null) {
                if (isset($value->transsingle) && !empty($value->transsingle)) {
                    array_push($selected_medias_arr, $value->transsingle->title);
                }
            } else if (!empty($value->url)) {
                array_push($image_urls, $value->url);
            }
        }

        /* Get Cover Image Of Country */
        $cover = null;
        if (!empty($place->cover)) {
            $cover = $place->cover;
            // $cover->url = str_replace('storage.travooo.com', 'https://localhost/travoo-api/storage/uploads', $cover->url);
        }

        return view('backend.place.show')
                        ->withPlace($place)
                        ->withPlacetrans($placeTrans)
                        ->withCountry($country)
                        ->withCity($city)
                        ->withType($type)
                        ->withDegree($safety_degree)
                        ->withImages($image_urls)
                        ->withMedias($selected_medias_arr)
                        ->withCover($cover);
    }

    /**
     * @param Place $place
     * @param $status
     * @param ManagePlaceRequest $request
     *
     * @return mixed
     */
    public function mark(Place $place, $status, ManagePlaceRequest $request) {
        $place->active = $status;
        $place->save();
        return redirect()->route('admin.location.place.index')
                        ->withFlashSuccess('Place Status Updated!');
    }

    /**
     * @param ManagePlaceRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function import(ManagePlaceRequest $request) {
        $countries = Countries::where(['active' => 1])->get();
        $countries_arr = [];

        foreach ($countries as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $countries_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['countries'] = $countries_arr;


        /* Get All Cities */
        $cities = Cities::where(['active' => 1])->get();
        foreach ($cities as $key => $value) {
            if (isset($value->transsingle) && !empty($value->transsingle)) {
                $cities_arr[$value->id] = $value->transsingle->title;
            }
        }
        $data['cities'] = $cities_arr;

        return view('backend.place.import', $data);
    }

    /**
     * @param int $admin_logs_id
     * @param int $country_id
     * @param int $city_id
     * @param int $latlng
     * @param ManagePlaceRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search($admin_logs_id = 0, $country_id = 0, $city_id = 0, $latlng = 0, ManagePlaceRequest $request) {
        $data['countries_id'] = $country_id ?: $request->input('countries_id');
        $data['cities_id'] = $city_id ?: $request->input('cities_id');

        $city = urlencode($request->input('address'));

        if ($admin_logs_id) {
            list($lat, $lng) = @explode(",", $latlng);

            $admin_logs = AdminLogs::find($admin_logs_id);
            $qu = $admin_logs->query;
            $queries = array_values(unserialize($qu));

            if (count($queries)) {
                $query = urlencode($queries[0]);
                unset($queries[0]);
                $queries = serialize($queries);
                $ad = AdminLogs::find($admin_logs_id);
                $ad->query = $queries;
                $ad->save();
            }
        } else {
            $queries = explode(",", $request->input('query'));
            $query = urlencode($queries[0]);
            unset($queries[0]);
            $queries = serialize($queries);

            list($lat, $lng) = @explode(",", $request->input('latlng'));

            $admin_logs = new AdminLogs();
            $admin_logs->item_type = 'places';
            $admin_logs->item_id = 0;
            $admin_logs->action = 'search';
            $admin_logs->query = $queries;
            $admin_logs->time = time();
            $admin_logs->admin_id = Auth::user()->id;
            $admin_logs->save();
        }

        if (isset($query)) {

            /* $provider_ids = array();
              $get_provider_ids = Place::where('countries_id', $data['countries_id'])
              ->where('cities_id', $data['cities_id'])
              ->select('provider_id')->get()->toArray();

              foreach ($get_provider_ids AS $gpi) {
              $provider_ids[] = $gpi['provider_id'];
              }

              $data['provider_ids'] = $provider_ids;
              //dd($data['provider_ids']);
             * 
             */

            $data['provider_ids'] = array();

            $json = file_get_contents('http://db.travoooapi.com/public/places/go/' . ($city ? $city : 0) . '/' . $lat . '/' . $lng . '/' . $query);

            $result = json_decode($json);

            PlaceSearchHistory::create([
                'lat' => $lat,
                'lng' => $lng,
                'time' => time(),
                'admin_id' => Auth::user()->id
            ]);

            $data['results'] = $result;
            $data['queries'] = $queries;
            if (isset($admin_logs) && is_object($admin_logs)) {
                $data['admin_logs_id'] = $admin_logs->id;
                $data['latlng'] = $lat . ',' . $lng;
            }

            return view('backend.place.importresults', $data);
        } else {
            return redirect()->route('admin.location.place.index')
                            ->withFlashSuccess('Done!');
        }
    }

    /**
     * @param ManagePlaceRequest $request
     * @return mixed
     */
    public function savesearch(ManagePlaceRequest $request) {
        $data['countries_id'] = $request->input('countries_id');
        $data['cities_id'] = $request->input('cities_id');
        $to_save = $request->input('save');
        $places = $request->input('place');

        if (is_array($to_save)) {
            foreach ($to_save AS $k => $v) {
                if (!Place::where('provider_id', '=', $places[$k]['provider_id'])->exists()) {

                    $p = new Place();
                    $p->place_type = $places[$k]['types'];
                    $p->safety_degrees_id = 1;
                    $p->provider_id = $places[$k]['provider_id'];
                    $p->countries_id = $data['countries_id'];
                    $p->cities_id = $data['cities_id'];
                    $p->lat = $places[$k]['lat'];
                    $p->lng = $places[$k]['lng'];
                    $p->pluscode = $places[$k]['plus_code'];
                    $p->rating = $places[$k]['rating'];
                    $p->active = 1;
                    $p->save();

                    $pt = new PlaceTranslations();
                    $pt->languages_id = 1;
                    $pt->places_id = $p->id;
                    $pt->title = $places[$k]['name'];
                    $pt->address = $places[$k]['address'];
                    if (isset($places[$k]['phone']))
                        $pt->phone = $places[$k]['phone'];
                    if (isset($places[$k]['website']))
                        $pt->description = $places[$k]['website'];
                    $pt->working_days = $places[$k]['working_days'];
                    $pt->save();
                    AdminLogs::create(['item_type' => 'places', 'item_id' => $p->id, 'action' => 'import', 'query' => '', 'time' => time(), 'admin_id' => Auth::user()->id]);

                    $data = array(
                        'type' => 'place',
                        'id' => $p->id,
                        'countries_id' => $p->countries_id,
                        'cities_id' => $p->cities_id,
                        'title' => @$pt->title,
                        'address' => @$pt->address,
                        'image' => '',
                        'place_type' => @$p->place_type,
                        'lat' => @$p->lat,
                        'lng' => @$p->lng,
                        'rating' => @$p->rating,
                        'location' => @$p->lat . "," . @$p->lng
                    );

                }
            }

            $num = count($to_save);

            if ($request->input('admin_logs_id')) {

                return redirect()->route('admin.location.place.search', array($request->get('admin_logs_id'),
                                    $request->get('countries_id'),
                                    $request->get('cities_id'),
                                    $request->get('latlng')))
                                ->withFlashSuccess($num . ' Places imported successfully!');
            } else {
                return redirect()->route('admin.location.place.index')
                                ->withFlashSuccess($num . ' Places imported successfully!');
            }
        } else {
            if ($request->input('admin_logs_id')) {
                return redirect()->route('admin.location.place.search', array($request->get('admin_logs_id'),
                                    $request->get('countries_id'),
                                    $request->get('cities_id'),
                                    $request->get('latlng')))
                                ->withFlashSuccess('You didnt select any items to import!');
            } else {
                return redirect()->route('admin.location.place.index')
                                ->withFlashError('You didnt select any items to import!');
            }
        }
    }

    /**
     * @param ManagePlaceRequest $request
     * @return string
     */
    public function return_search_history(ManagePlaceRequest $request) {
        $ne_lat = $request->get('ne_lat');
        $sw_lat = $request->get('sw_lat');
        $ne_lng = $request->get('ne_lng');
        $sw_lng = $request->get('sw_lat');

        $markers = PlaceSearchHistory::whereBetween('lat', array($sw_lat, $ne_lat))
                ->whereBetween('lng', array($sw_lng, $ne_lng))
                ->groupBy('lat', 'lng')
                ->select('lat', 'lng')
                ->get()
                ->toArray();
        return json_encode($markers);
    }

    /**
     * @param ManagePlaceRequest $request
     */
    public function delete_ajax(ManagePlaceRequest $request) {
        $ids = $request->input('ids');

        if (!empty($ids)) {
            $ids = explode(',', $request->input('ids'));
            foreach ($ids as $key => $value) {
                $this->delete_single_ajax($value);
            }
        }

        echo json_encode([
            'result' => true
        ]);
    }

    /**
     * @param Place $id
     * @param ManagePlaceRequest $request
     *
     * @return mixed
     */
    public function delete_single_ajax($id) {
        $item = Place::find($id);
        if (empty($item)) {
            return false;
        }

        $child = PlaceTranslations::where(['places_id' => $item->id])->get();
        if (!empty($child)) {
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }

        //$this->deletePlacesMedia($id, 'deleted');  /// delete data in medias and places_medias table
        //$item->deleteMedias();
        $country = Countries::find($item->countries_id);
        $city = Cities::find($item->cities_id);
        $item->delete();

        /*
          if(!is_null($country->places)) {


          $this->updateStatistic($country, 'places', count($country->places));
          }

          if(!is_null($city->places)) {

          $this->updateStatistic($city, 'places', count($city->places));
          }
         * 
         */

        AdminLogs::create(['item_type' => 'places', 'item_id' => $id, 'action' => 'delete', 'time' => time(), 'admin_id' => Auth::user()->id]);
    }

    public function getAddedCountries() {
        $q = null;
        if (isset($_GET['q'])) {
            $q = $_GET['q'];
        }

        $place = Place::distinct()->select('countries_id')->get();
        $temp_country = [];
        $filter_html = null;
        $json = [];

        if (!empty($place)) {
            foreach ($place as $key => $value) {
                $country = null;
                if (empty($q)) {
                    $country = Countries::find($value->countries_id);
                } else {
                    $country = Countries::leftJoin('countries_trans', function ($join) {
                                $join->on('countries_trans.countries_id', '=', 'countries.id');
                            })->where('countries_trans.title', 'LIKE', '%' . $q . '%')->where(['countries.id' => $value->countries_id])->first();
                }
                if (!empty($country)) {
                    $transingle = CountriesTranslations::where(['countries_id' => $value->countries_id])->first();

                    if (!empty($transingle)) {
                        $filter_html .= '<option value="' . $value->countries_id . '">' . $transingle->title . '</option>';
                        array_push($temp_country, $transingle->title);
                        $json[] = ['id' => $value->countries_id, 'text' => $transingle->title];
                    }
                }
            }
        }
        echo json_encode($json);
    }

    public function getAddedSections(){
        $q = request()->input('q');

        $places = Place::distinct()->select('place_section')
            ->where('place_section', 'LIKE', '%' . $q . '%')
            ->get();

        $data = [];

        foreach($places as $index => $place){
            array_push($data, [
                'id'=>$index,
                'text'=>$place->place_section,
            ]);
        }

        return response()->json([
            'data'=>$data
        ]);
    }

    public function getAddedCities(Request $request) {
        $formatted_cities = [];
        $paginate = false;
        $country_id = $request->countryId;

        $term = trim($request->q);
        if (!empty($term)) {

            $places = Place::distinct()->select('cities_id')->whereHas('cities', function ($query) use ($term) {
                $query->whereHas('transsingle', function ($qr) use ($term) {
                    $qr->where('title', 'LIKE', "%$term%");
                });
            })
                ->where(function($_q) use($country_id){
                    if($country_id != ''){
                        $_q->where('countries_id', $country_id);
                    }
                })->with('cities')->offset(($request->page -1 ) * 10)->limit(10)->get();

            $paginate = (count($places) < 10) ? false : true;

            foreach ($places as $place) {
                $formatted_cities[] = ['id' => $place->city->id, 'text' => $place->city->transsingle->title];
            }


        }

        return response()->json(['data' => $formatted_cities, 'paginate' => $paginate]);
    }

    public function getPlaceTypes(Request $request) {
        $term = trim($request->q);
        if (!empty($term)) {
            $places = Place::distinct()
                    ->select('place_type')
                    ->where('place_type', 'LIKE', '%' . $term . '%')
                    ->offset(($request->page - 1) * 10)
                    ->limit(10)
                    ->get();
        }

        $dataArray = [];
        $paginate = (count($places) < 10) ? false : true;

        if (!empty($places)) {
            foreach ($places as $key => $value) {
                $dataArray[] = ['id' => $value->place_type, 'text' => $value->place_type];
            }
        }

        return response()->json(['data' => $dataArray, 'paginate' => $paginate]);
    }

    /**
     * @return false|string
     */
    public function getAllCountries() {
        $query = null;

        if (isset($_GET['q'])) {
            $query = $_GET['q'];
        }

        $countries = CountriesTranslations::where('title', 'LIKE', '%' . $query . '%')
            ->orderBy('title', 'ASC')
            ->get();

        $json = [];

        foreach ($countries as $country) {
            $json[] = [
                'id' => $country->countries_id,
                'text' => $country->title
            ];
        }

        return response()->json($json);
    }

    /**
     * @return false|string
     */
    public function getAllCities() {
        $query = null;
        $countryId = request('country_id');

        if (isset($_GET['q'])) {
            $query = $_GET['q'];
        }

        $dbQuery = DB::table('cities_trans')
            ->select(
                'cities_trans.title as city_title',
                'countries_trans.title as country_title',
                'cities.id as city_id',
                'cities.countries_id as country_id'
            )
            ->leftJoin('cities', 'cities_trans.cities_id', '=', 'cities.id')
            ->join('countries_trans', 'cities.countries_id', '=', 'countries_trans.countries_id')
            ->where('cities_trans.title', 'LIKE', '%' . $query . '%');

        if( ! is_null($countryId) ){
            $dbQuery->where('cities.countries_id', intval($countryId));
        }

        $cities = $dbQuery->get()->all();



        $citiesJson = [];

        foreach ($cities as $city) {
            $citiesJson[] = ['id' => $city->city_id, 'text' => $city->city_title . ' (' . $city->country_title . ')'];
        }

        return json_encode($citiesJson);
    }

    /**
     * @param ManagePlaceRequest $request
     * @return false|string
     */
    public function changePlacesToNewCity(ManagePlaceRequest $request)
    {
        $ids = $request->input('ids');
        $cityId = $request->input('cityId');
        $countryId = $request->input('countryId');
//        $countryId = Cities::select('countries_id')->where('id', $cityId)->get()->first()->countries_id;
//        dd($ids , $cityId , $countryId);
        $response = [
            'result' => true
        ];

        // country is required
        if(is_null($countryId) || empty($ids))
            return response()->json($response);


        $idsArray = explode(',', $ids);
        foreach ($idsArray as $id) {
            if(is_null($cityId)){
                // only countryId exists
                Place::where('id', $id)->update([
                        'countries_id' => $countryId,
                        'auto_import'  => 0
                    ]
                );
            }else{
                // cityId and countryId exists
                Place::where('id', $id)->update([
                        'cities_id' => $cityId,
                        'countries_id' => $countryId,
                        'auto_import'  => 0
                    ]
                );
            }
        }
        //update top places based on bulk places city change
        if(!is_null($countryId))
            $updateTopPlaces = PlacesTop::whereIn('places_id', $idsArray)->update(['country_id' => $countryId]);
        if(!is_null($cityId))
            $updateTopPlaces = PlacesTop::whereIn('places_id', $idsArray)->update(['city_id'=> $cityId]);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function markTopPlace(Request $request)
    {
        $placeId = $request->id;
        $hotelType = [
            'lodging'
        ];

        $restourantType = [
            'bakery',
            'bar',
            'cafe',
            'meal_delivery',
            'meal_takeaway',
            'restaurant',
            'food'
        ];

        $ifExist = PlacesTop::where('places_id', '=', $placeId)->exists();

        if (!$ifExist){
            $topPlaceData = Place::select(
                'places.id as places_id',
                'places_trans.title as title',
                'places.countries_id as country_id',
                'places.cities_id as city_id',
                'places.rating as rating',
                'cities_trans.title as city',
                'countries_trans.title as country',
                'places.place_type as destination_type'
            )
                ->leftJoin('cities_trans', 'cities_trans.cities_id', '=', 'places.cities_id')
                ->leftJoin('places_trans', 'places_trans.places_id', '=', 'places.id')
                ->leftJoin('countries_trans', 'countries_trans.countries_id', '=', 'places.countries_id')
                ->where('places.id', '=', $placeId)
                ->get()->first()
            ;

            $topPlaceData['rating'] = $topPlaceData['rating'] ?: 0;

            $ptype = explode(",", $topPlaceData['destination_type']);

            if(in_array($ptype[0], $hotelType)){
                $topPlaceData['destination_type'] = 'hotel';
            }elseif (in_array($ptype[0], $restourantType)){
                $topPlaceData['destination_type'] = 'restaurant';
            }else{
                $topPlaceData['destination_type'] = 'place';
            }

            $order = 0 ;
            $tp_for_order = PlacesTop::where('city_id', $topPlaceData['city_id'])->where('destination_type', $topPlaceData['destination_type'])->orderBy('order_by_city','DESC')->first();
            if(is_object($tp_for_order)){
                $order= $tp_for_order->order_by_city+1;
            }

            $topPlaceData['order_by_city'] = $order;

            PlacesTop::create(
                $topPlaceData->toArray()
            );

            return redirect()->route('admin.location.place.index')->withFlashSuccess('Place successfully mark as a top!');
        } else {
            return redirect()->route('admin.location.place.index')->withErrors('Place already mark as a top!');
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function unmarkTopPlace(Request $request)
    {
        $placeId = $request->id;

        $ifExist = PlacesTop::where('places_id', '=', $placeId)->exists();

        if ($ifExist){
            PlacesTop::where('places_id', '=', $placeId)->delete();

            return redirect()->route('admin.location.place.index')->withFlashSuccess('Place successfully unmark as a top!');
        } else {
            return redirect()->route('admin.location.place.index')->withErrors('Place not marked as a top!');
        }
    }
}
