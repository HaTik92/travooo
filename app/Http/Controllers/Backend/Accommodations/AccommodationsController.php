<?php

namespace App\Http\Controllers\Backend\Accommodations;

use App\Http\Requests\Backend\Accommodations\UpdateAccommodationsRequest;
use App\Models\Accommodations\Accommodations;
use App\Models\Accommodations\AccommodationsTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Accommodations\ManageAccommodationsRequest;
use App\Http\Requests\Backend\Accommodations\StoreAccommodationsRequest;
use App\Repositories\Backend\Accommodations\AccommodationsRepository;
use Yajra\DataTables\Facades\DataTables;

class AccommodationsController extends Controller
{
    protected $accommodations;

    /**
     * AccommodationsController constructor.
     * @param AccommodationsRepository $accommodations
     */
    public function __construct(AccommodationsRepository $accommodations)
    {
        $this->accommodations = $accommodations;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManageAccommodationsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageAccommodationsRequest $request)
    {
        return view('backend.accommodations.index');
    }

    /**
     * @param ManageAccommodationsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(ManageAccommodationsRequest $request)
    {
        return view('backend.accommodations.create',[]);
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->accommodations->getForDataTable())
            ->addColumn('action', function ($accommodations) {
                return view('backend.buttons', ['params' => $accommodations, 'route' => 'accommodations']);
            })
            ->withTrashed()
            ->make(true);
    }

    /**
     * @param StoreAccommodationsRequest $request
     * @return mixed
     */
    public function store(StoreAccommodationsRequest $request)
    {   
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
        }

        $this->accommodations->create($data);

        return redirect()->route('admin.accommodations.index')->withFlashSuccess('Accommodations Created!');
    }

    /**
     * @param $id
     * @param ManageAccommodationsRequest $request
     * @return mixed
     */
    public function destroy($id, ManageAccommodationsRequest $request)
    {      
        $item = Accommodations::findOrFail($id);
        /* Delete Children Tables Data of this country */
        $child = AccommodationsTranslations::where(['Accommodations_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.accommodations.index')->withFlashSuccess('Accommodations Deleted Successfully');
    }

    /**
     * @param $id
     * @param ManageAccommodationsRequest $request
     * @return mixed
     */
    public function edit($id, ManageAccommodationsRequest $request)
    {
        $data = [];
        $accommodations = Accommodations::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = AccommodationsTranslations::where([
                'languages_id' => $language->id,
                'accommodations_id'   => $id
            ])->first();

            $data['title_'.$language->id] = null;

            if(!empty($model)){
                $data['title_'.$language->id] = $model->title;
            }
        }

        return view('backend.accommodations.edit')
            ->withLanguages($this->languages)
            ->withAccommodations($accommodations)
            ->withAccommodationsid($id)
            ->withData($data);
    }

    /**
     * @param $id
     * @param ManageAccommodationsRequest $request
     * @return mixed
     */
    public function update($id, UpdateAccommodationsRequest $request)
    {   
        $accommodations = Accommodations::findOrFail($id);
        
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
        }

        $this->accommodations->update($id , $accommodations, $data);

        return redirect()->route('admin.accommodations.index')
            ->withFlashSuccess('Accommodations updated Successfully!');
    }

    /**
     * @param $id
     * @param ManageAccommodationsRequest $request
     * @return mixed
     */
    public function show($id, ManageAccommodationsRequest $request)
    {   
        $accommodations = Accommodations::findOrFail($id);
        $accommodationsTrans = AccommodationsTranslations::where(['accommodations_id' => $id])->get();

        return view('backend.accommodations.show')
            ->withAccommodations($accommodations)
            ->withAccommodationstrans($accommodationsTrans);
    }
}