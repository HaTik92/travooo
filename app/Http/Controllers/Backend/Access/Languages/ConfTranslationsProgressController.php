<?php

namespace App\Http\Controllers\Backend\Access\Languages;

use App\Http\Controllers\Controller;
use App\Models\Access\Language\ConfTranslationsProgress;
use Yajra\DataTables\Facades\DataTables;

class ConfTranslationsProgressController extends Controller
{
    /**
     * @param ConfTranslationsProgress $confTranslationsProgress
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUncompleted(ConfTranslationsProgress $confTranslationsProgress)
    {
        return Datatables::of($confTranslationsProgress->where('completed_at', '=', null))
            ->addColumn('refresh', function ($progress) {
                return view('backend.access.language.parts.progress-refresh-button', ['id' => $progress->id]);
            })
            ->rawColumns(['refresh'])
            ->make(true);
    }

    /**
     * @param $id
     * @param ConfTranslationsProgress $confTranslationsProgress
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id, ConfTranslationsProgress $confTranslationsProgress)
    {
        return response()->json($confTranslationsProgress->find($id));
    }
}
