<?php

namespace App\Http\Controllers\Backend\Access\Languages;

use App\Models\Access\Language\Languages;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Access\Language\ManageLanguagesRequest;
use App\Http\Requests\Backend\Access\Language\StoreLanguageRequest;
use App\Repositories\Backend\Access\Languages\LanguagesRepository;
use App\Http\Requests\Backend\Access\Language\UpdateLanguageRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Yajra\DataTables\Facades\DataTables;

class LanguagesController extends Controller
{
    use DispatchesJobs;

    protected $languages;

    public function __construct(LanguagesRepository $languages)
    {
        $this->languages = $languages;
    }

     /**
     * @param ManageLanguagesRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageLanguagesRequest $request)
    {
        return view('backend.access.language.index');
    }

    /**
     * @param ManageLanguagesRequest $request
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->languages->getForDataTable())
            ->addColumn('action', function ($languages) {
                return view('backend.buttons', ['params' => $languages, 'route' => 'access.languages']);
            })
            ->make(true);
    }

    /**
     * @param ManageLanguagesRequest $request
     *
     * @return mixed
     */
    public function create(ManageLanguagesRequest $request)
    {
        return view('backend.access.language.create');
    }


    /**
     * @param Languages              $language
     * @param ManageLanguagesRequest $request
     *
     * @return mixed
     */
    public function edit(Languages $language, ManageLanguagesRequest $request)
    {
        $data['active'] = $language->active;

        return view('backend.access.language.edit')
            ->withLanguages($language)
            ->withData($data);
    }

    /**
     * @param Languages             $language
     * @param UpdateLanguageRequest $request
     *
     * @return mixed
     */
    public function update(Languages $language, UpdateLanguageRequest $request)
    {
        $active = 2;

        if(!empty($request->input('active'))){
            $active = 1;
        }

        $this->languages->update($language, [
            'title'  => $request->input('title'),
            'code'   => $request->input('code'),
            'active' => $active,
        ]);

        return redirect()->route('admin.access.languages.index')->withFlashSuccess(trans('alerts.backend.language.updated'));
    }

    /**
     * @param StoreLanguageRequest $request
     *
     * @return mixed
     */
    public function store(StoreLanguageRequest $request)
    {
        $this->languages->create($request->only('title', 'code', 'active'));

        return redirect()->route('admin.access.languages.index')->withFlashSuccess(trans('alerts.backend.language.created'));
    }

    /**
     * @param $id
     * @param ManageLanguagesRequest $request
     * @return mixed
     */
    public function destroy($id, ManageLanguagesRequest $request)
    {
        $item = Languages::findOrFail($id);
        $item->delete();
        return redirect()->route('admin.access.languages.index')->withFlashSuccess(trans('alerts.backend.language.deleted'));
    }

    /**
     * @param Languages $language
     * @param $status
     * @param ManageLanguagesRequest $request
     *
     * @return mixed
     */
    public function mark(Languages $language, $status, ManageLanguagesRequest $request)
    {
        $language->active = $status;
        $language->save();
        return redirect()->route('admin.access.languages.index')->withFlashSuccess(trans('alerts.backend.language.updated'));
    }
}