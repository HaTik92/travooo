<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Http\Responses\AjaxResponse;
use App\Models\Place\Place;
use Illuminate\Http\Request;
use App\Models\Access\User\User;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\User\User as Users;
use App\Models\User\ApiUser;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Http\Requests\Backend\Access\User\StoreUserRequest;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserRequest;
use App\Models\AdminLogs\AdminLogs;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class UserController.
 */
class UserController extends Controller {

    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * @param UserRepository $users
     * @param RoleRepository $roles
     */
    public function __construct(UserRepository $users, RoleRepository $roles) {
        $this->users = $users;
        $this->roles = $roles;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageUserRequest $request) {
        return view('backend.access.index');
    }

    /**
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function table(ManageUserRequest $request)
    {
        return Datatables::of($this->users->getForDataTable($request->get('status'), $request->get('trashed'),$request->get('nonConfirmed'),$request->get('suspended')))
            ->escapeColumns(['name', 'email'])
            ->addColumn('',function(){
                return null;
            })
            ->editColumn('confirmed', function ($user) {
                return $user->confirmed_label;
            })
            ->editColumn('status', function ($user) {
                $status = 'Active';
                if ($user->status  === ApiUser::STATUS_INACTIVE) {
                    $status = 'Inactive';
                } else if ($user->status  === ApiUser::STATUS_DEACTIVE) {
                    $status = 'Suspended';
                } elseif($user->deleted_at) {
                    $status = 'Deleted';
                }
                return $status;
            })
            ->addColumn('roles', function ($user) {
                return $user->roles->count() ?
                    implode('<br/>', $user->roles->pluck('name')->toArray()) :
                    trans('labels.general.none');
            })
            ->addColumn('actions', function ($user) {
                return $user->action_buttons;
            })
            ->make(true);
    }

    public function getlogs(ManageUserRequest $request) {

        if ($request->has('id')) {
            $admin_id = $request->get('id');
            $data['logs'] = DB::table('admin_logs')
                    ->leftJoin('users', 'admin_logs.admin_id', '=', 'users.id')
                    ->where('admin_logs.admin_id', $admin_id)
                    ->select('admin_logs.admin_id', 'admin_logs.item_type', 'admin_logs.action', 'users.email', DB::raw('count(*) as total'))
                    ->groupBy('admin_logs.admin_id', 'users.email', 'admin_logs.item_type', 'admin_logs.action')
                    ->get();

            return view('backend.access.logsbyid', $data);

        } else {
            $data['logs'] = DB::table('admin_logs')
                    ->leftJoin('users', 'admin_logs.admin_id', '=', 'users.id')
                    ->select('admin_logs.admin_id', 'users.email', DB::raw('count(*) as total'))
                    ->groupBy('admin_logs.admin_id', 'users.email')
                    ->get();

            return view('backend.access.logs', $data);
        }
    }

    public function postlogs(ManageUserRequest $request) {
        $date_from = $date_to = false;
        if ($request->has('date_from')) {
            $date_from = strtotime($request->get('date_from'));
        }
        if ($request->has('date_to')) {
            $date_to = strtotime($request->get('date_to'));
        }

        $logs = DB::table('admin_logs')
                ->leftJoin('users', 'admin_logs.admin_id', '=', 'users.id')
                ->select('admin_logs.admin_id', 'users.email', DB::raw('count(*) as total'));
        if ($date_from) {
            $logs = $logs->where('admin_logs.time', '>', $date_from);
        }
        if ($date_to) {
            $logs = $logs->where('admin_logs.time', '<', $date_to);
        }

        $logs = $logs->groupBy('admin_logs.admin_id', 'users.email')
                ->get();

        $data['logs'] = $logs;
        return view('backend.access.logs', $data);
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function create(ManageUserRequest $request) {
        return view('backend.access.create')
                        ->withRoles($this->roles->getAll());
    }

    /**
     * @param StoreUserRequest $request
     *
     * @return mixed
     */
    public function store(StoreUserRequest $request) {
        if (!empty($request->file('profile_picture'))) {
            $imageName = time() . '_' . rand(10, 10000000) . '.' . $request->file('profile_picture')->getClientOriginalExtension();
            $request->file('profile_picture')->move(
                    base_path() . '/public/img/users/', $imageName
            );
        } else {
            $imageName = "";
        }

        $assigneesRoles =  $request->only('assignees_roles');
        //add admin if super admin added

        $superAdminRole = DB::table(config('access.roles_table'))->where('id', User::SUPER_ADMIN_ID)->first();
        if ($superAdminRole && $assigneesRoles['assignees_roles'] && array_key_exists($superAdminRole->id, $assigneesRoles['assignees_roles']) && !array_key_exists(1, $assigneesRoles['assignees_roles'])) {
            $assigneesRoles['assignees_roles']['1'] = '1';
        }

        $this->users->create([
            'data' => [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'status' => $request->input('status', \App\Models\User\User::STATUS_INACTIVE),
                'confirmed' => $request->input('confirmed', 0),
                'confirmation_email' => $request->input('confirmation_email'),
                'address' => $request->input('address'),
                'single' => $request->input('single'),
                'gender' => $request->input('gender'),
                'children' => $request->input('children'),
                'birth_date' => $request->input('date_of_birth'),
                'mobile' => $request->input('server_phone'),
                'nationality' => $request->input('nationality'),
                'public_profile' => $request->input('public_profile'),
                'notifications' => $request->input('notifications'),
                'messages' => $request->input('messages'),
                'username' => $request->input('username'),
                'profile_picture' => $imageName,
                'sms' => $request->input('sms'),
                'type' => $request->input('type'),
                'website' => $request->input('website'),
                'twitter' => $request->input('twitter'),
                'facebook' => $request->input('facebook'),
                'youtube' => $request->input('youtube'),
                'instagram' => $request->input('instagram'),
                'pinterest' => $request->input('pinterest'),
                'tumblr' => $request->input('tumblr'),
                'interests' => $request->input('interests'),
            ],
            'roles' => $assigneesRoles,
            'relations' => [
                'cities_countries' => $request->input('cities-countries', []),
                'places' => $request->input('places', []),
                'travel_type' => $request->input('travel_type'),
            ]
        ]);

        return redirect()->route('admin.access.user.index')
                        ->withFlashSuccess(trans('alerts.backend.users.created'));
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function show(User $user, ManageUserRequest $request) {
        $users = Users::with(['confirmedReports.spamsPost', 'copyrightInfringement' => function ($query) {
            $query->whereNotNull('deleted_at');
        },
        ])->where('id', $user->id)->first();
        return view('backend.access.show')
            ->withUser($users);
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function edit(User $user, ManageUserRequest $request) {
        return view('backend.access.edit')
                        ->withUser($user)
                        ->withUserRoles($user->roles->pluck('id')->all())
                        ->withRoles($this->roles->getAll());
    }

    /**
     * @param User              $user
     * @param UpdateUserRequest $request
     *
     * @return mixed
     */
    public function update(User $user, UpdateUserRequest $request) {
        if (!empty($request->file('profile_picture'))) {
            $imageName = time() . '_' . rand(10, 10000000) . '.' . $request->file('profile_picture')->getClientOriginalExtension();

            $amazonefilename = explode('/', $user->profile_picture);

            $user_id = $user->id;

            $original_file = $request->file('profile_picture');

            Storage::disk('s3')->putFileAs(
                'users/profile/' . $user_id . '/', $original_file, $imageName, 'public'
            );

            $cropped = Image::make($original_file)->resize(180, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            //Move original image
            Storage::disk('s3')->putFileAs(
                'users/profile/original/' . $user_id . '/', $original_file, $imageName, 'public'
            );

            Storage::disk('s3')->put(
                'users/profile/th180/' . $user_id . '/' . $imageName, $cropped->encode(), 'public'
            );

            $imageName = 'https://s3.amazonaws.com/travooo-images2/users/profile/' . $user_id . '/' . $imageName;

            if ($user->profile_picture != "" || $user->profile_picture != null) {
                Storage::disk('s3')->delete('users/profile/' . $user_id . '/' . end($amazonefilename));

                Storage::disk('s3')->delete('users/profile/original/' . $user_id . '/' . end($amazonefilename));
            }
        } else {
            $imageName = null;
        }

        $assigneesRoles =  $request->only('assignees_roles');

        $superAdminRole = DB::table(config('access.roles_table'))->where('id', User::SUPER_ADMIN_ID)->first();
        if ($superAdminRole && $assigneesRoles['assignees_roles'] && array_key_exists($superAdminRole->id, $assigneesRoles['assignees_roles']) && !array_key_exists(1, $assigneesRoles['assignees_roles'])) {
            $assigneesRoles['assignees_roles']['1'] = '1';
        }

        $this->users->update($user, [
            'data' => [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'status' => $request->input('status', \App\Models\User\User::STATUS_INACTIVE),
                'confirmed' => $request->input('confirmed', 0),
                'confirmation_email' => $request->input('confirmation_email'),
                'address' => $request->input('address'),
                'single' => $request->input('single'),
                'gender' => $request->input('gender'),
                'children' => $request->input('children'),
                'birth_date' => $request->input('date_of_birth'),
                'mobile' => $request->input('mobile') ? $request->input('server_phone') : '',
                'nationality' => $request->input('nationality'),
                'public_profile' => $request->input('public_profile'),
                'notifications' => $request->input('notifications'),
                'messages' => $request->input('messages'),
                'username' => $request->input('username'),
                'profile_picture' => $imageName,
                'sms' => $request->input('sms'),
                'type' => $request->input('type'),
                'website' => $request->input('website'),
                'twitter' => $request->input('twitter'),
                'facebook' => $request->input('facebook'),
                'youtube' => $request->input('youtube'),
                'instagram' => $request->input('instagram'),
                'pinterest' => $request->input('pinterest'),
                'tumblr' => $request->input('tumblr'),
                'interests' => $request->input('interests'),
            ],
            'roles' => $assigneesRoles,
            'relations' => [
                'cities_countries' => $request->input('cities-countries', []),
                'places' => $request->input('places', []),
                'travel_type' => $request->input('travel_type'),
            ]
        ]);

        return redirect()->route('admin.access.user.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function destroy(User $user, ManageUserRequest $request) {
        $this->users->delete($user);

        return redirect()->route('admin.access.user.deleted')->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }

    public function delete_ajax(ManageUserRequest $request){

        $ids = $request->input('ids');

        if(!empty($ids)){
            $ids = explode(',',$request->input('ids'));
            foreach ($ids as $key => $value) {
                $this->delete_single_ajax($value);
            }
        }
        
        echo json_encode([
            'result' => true
        ]);
    }

    /**
     * @param User $id
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function delete_single_ajax($id) {
        $item = User::findOrFail($id);
        if(empty($item)){
            return false;
        }
        $item->forceDelete();

        AdminLogs::create(['item_type' => 'user', 'item_id' => $id, 'action' => 'delete', 'time' => time(), 'admin_id' => Auth::user()->id]);
    }

    /**
     * @param User $id
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function citiesCountries(Request $request) {
        $locs = $request->locs;
        $locs = collect($locs)->map(function($loc) {
            $pieces  = explode('-', $loc);
            $newLoc = [];
            $newLoc['type'] = $pieces[0];
            $newLoc['id'] = $pieces[1];

            return $newLoc;
        });
        $result = [];

        $locs = $locs->groupBy('type')->toArray();

        $cities = collect([]);
        $countries = collect([]);

        if(isset($locs['city'])) {
            $cities = Cities::whereIn('id', collect($locs['city'])->pluck('id')->all())->get();
        }

        if(isset($locs['country'])) {
            $countries = Countries::whereIn('id', collect($locs['country'])->pluck('id')->all())->get();
        }

        $entities = $cities->concat($countries)->shuffle();

        foreach ($entities as $entity) {
            $result[] = [
                'id' => get_class($entity) === Cities::class ? 'city-'.$entity->getKey() : 'country-'.$entity->getKey(),
                'type' => 'location',
                'name' => $entity->transsingle->title,
                'desc' => get_class($entity) === Cities::class ? $entity->country->transsingle->title : '',
                'url' => get_class($entity) === Cities::class ? @check_city_photo($entity->getMedias[0]->url, 700) : @check_country_photo($entity->getMedias[0]->url, 700)
            ];
        }

        return AjaxResponse::create($result);
    }

    /**
     * @param User $id
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function places(Request $request) {
        $locs = $request->locs;
        $locs = collect($locs)->map(function($loc) {
            $pieces  = explode('-', $loc);
            $newLoc = [];
            $newLoc['type'] = $pieces[0];
            $newLoc['id'] = $pieces[1];

            return $newLoc;
        });
        $result = [];

        $locs = $locs->groupBy('type')->toArray();

        $places = collect([]);

        if(isset($locs['place'])) {
            $places = Place::whereIn('id', collect($locs['place'])->pluck('id')->all())->get();
        }

        $entities = $places->shuffle();

        foreach ($entities as $entity) {
            $result[] = [
                'id' => 'place-'.$entity->getKey(),
                'type' => 'location',
                'name' => $entity->transsingle->title,
                'desc' => get_class($entity) === Cities::class ? $entity->country->transsingle->title : '',
                'url' => get_class($entity) === Cities::class ? @check_city_photo($entity->getMedias[0]->url, 700) : @check_country_photo($entity->getMedias[0]->url, 700)
            ];
        }

        return AjaxResponse::create($result);
    }

    /**
     * @param ManageUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function block(ManageUserRequest $request) {
        return $this->users->block($request->input('id'));
    }

    /**
     * @param ManageUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function unBlockUser(ManageUserRequest $request) {
        return $this->users->unBlockUser($request->input('id'));
    }
}
