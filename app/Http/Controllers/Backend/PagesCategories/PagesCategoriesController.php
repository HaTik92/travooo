<?php

namespace App\Http\Controllers\Backend\PagesCategories;

use App\Models\PagesCategories\PagesCategories;
use App\Models\PagesCategories\PagesCategoriesTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\PagesCategories\ManagePagesCategoriesRequest;
use App\Http\Requests\Backend\PagesCategories\StorePagesCategoriesRequest;
use App\Repositories\Backend\PagesCategories\PagesCategoriesRepository;
use Yajra\DataTables\Facades\DataTables;

class PagesCategoriesController extends Controller
{
    protected $pages_categories;

    /**
     * PagesCategoriesController constructor.
     * @param PagesCategoriesRepository $pages_categories
     */
    public function __construct(PagesCategoriesRepository $pages_categories)
    {
        $this->pages_categories = $pages_categories;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

     /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.pages_categories.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->pages_categories->getForDataTable())
            ->addColumn('action', function ($pages_categories) {
                return view('backend.buttons', ['params' => $pages_categories, 'route' => 'pages_categories']);
            })
            ->make(true);
    }

    /**
     *
     * @return mixed
     */
    public function create()
    {   
        return view('backend.pages_categories.create');
    }

    /**
     * @param StorePagesCategoriesRequest $request
     *
     * @return mixed
     */
    public function store(StorePagesCategoriesRequest $request)
    {   
        $data = [];
    
        $extra = [
            'title' => $request->input('title')
        ];
        
        $this->pages_categories->create($data , $extra);

        return redirect()->route('admin.pages_categories.index')->withFlashSuccess('Page Category Created!');
    }

    /**
     * @param PagesCategories $id
     *
     * @return mixed
     */
    public function destroy($id)
    {      
        $item = PagesCategories::findOrFail($id);
        /* Delete Children Tables Data of this PageCategory */
        $item->delete();

        return redirect()->route('admin.pages_categories.index')->withFlashSuccess('Page Category Deleted Successfully');
    }

    /**
     * @param PagesCategories $id
     *
     * @return mixed
     */
    public function edit($id)
    {
        $data = [];
        $pages_categories = PagesCategories::findOrFail($id);

        $data['title'] = $pages_categories->title;

        return view('backend.pages_categories.edit')
            ->withLanguages($this->languages)
            ->withPages_categories($pages_categories)
            ->withPages_categories_id($id)
            ->withData($data);
    }

    /**
     * @param PagesCategories $id
     * @param ManagePagesCategoriesRequest $request
     *
     * @return mixed
     */
    public function update($id, ManagePagesCategoriesRequest $request)
    {   
        $pages_categories = PagesCategories::findOrFail($id);
        
        $data = [];

        $extra = [
            'title' => $request->input('title')
        ];

        $this->pages_categories->update($id , $pages_categories, $data , $extra);
        
        return redirect()->route('admin.pages_categories.index')
            ->withFlashSuccess('Pages Categories updated Successfully!');
    }

    /**
     * @param PagesCategories $id
     *
     * @return mixed
     */
    public function show($id)
    {   
        $pages_categories = PagesCategories::findOrFail($id);
       
        return view('backend.pages_categories.show')
            ->withPages_categories($pages_categories); 
    }

    /**
     * @param $id
     * @param $status
     * @return mixed
     */
    public function mark($id, $status)
    {   
        $pages_categories = PagesCategories::findOrFail($id);
        $pages_categories->active = $status;
        $pages_categories->save();
        
        return redirect()->route('admin.pages_categories.index')
            ->withFlashSuccess('Page Category Status Updated!');
    }
}