<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ErrorLogReaderController extends Controller
{

    public function __construct()
    {
        $this->logDir = storage_path('logs') . "/";
    }

    public function index()
    {
        $prefixs = [
            'chat_list', 'debug_api', 'restapi', 'laravel'
        ];
        $arr = [];
        if (is_dir($this->logDir)) {
            if ($handle = opendir($this->logDir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..' || $file == ".gitignore") continue;
                    foreach ($prefixs as $prefix) {
                        if (strpos($file, $prefix) !== false) {
                            $arr[] = [
                                'path' => $this->logDir . $file,
                                'file' =>  $file
                            ];
                        }
                    }
                }
                closedir($handle);
            }
        }
        return view('backend.common-pages.load-reader', [
            'page_title' => "Load Reader",
            "arr" => collect($arr)->sortByDesc('file')->take(150)
        ]);
    }

    public function download($file)
    {
        $path = $this->logDir . $file;
        return Response::download($path, $file, [
            'Content-Type: text/x-log'
        ]);
    }
}
