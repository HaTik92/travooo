<?php

namespace App\Http\Controllers\Backend\IsolatedPlaces;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Http\Requests\Backend\Place\PlaceMassApplyFiltersRequest;
use App\Http\Requests\Backend\Place\PlaceMassApplyMoveToRequest;
use App\Http\Requests\Backend\Place\PlaceMassPageRequest;
use App\Models\City\CitiesTranslations;
use App\Models\Country\CountriesTranslations;
use App\Models\Place\Place;
use App\Models\Place\Medias;
use App\Models\Place\PlaceMedias;
use App\Models\Place\PlaceTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Place\ManagePlaceRequest;
use App\Http\Requests\Backend\Place\StorePlaceRequest;
use App\Http\Requests\Backend\Place\UpdatePlaceRequest;
use App\Models\PlacesTop\PlacesTop;
use App\Repositories\Backend\Place\PlaceRepository;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\AdminLogs\AdminLogs;
use App\Models\PlaceSearchHistory\PlaceSearchHistory;
use App\Models\ActivityMedia\Media;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Utilities\Request;

class IsolatedPlacesController extends Controller {


    protected $places;
    private $placeTypesArray = [
        'airport',
        'amusement_park',
        'aquarium',
        'art_gallery',
        'bakery',
        'bar',
        'cafe',
        'campground',
        'casino',
        'cemetery',
        'church',
        'city_hall',
        'embassy',
        'hindu_temple',
        'library',
        'light_rail_station',
        'lodging',
        'meal_delivery',
        'meal_takeaway',
        'mosque',
        'movie_theater',
        'museum',
        'night_club',
        'park',
        'restaurant',
        'shopping_mall',
        'stadium',
        'subway_station',
        'synagogue',
        'tourist_attraction',
        'train_station',
        'transit_station',
        'zoo',
        'food',
        'place_of_worship',
        'natural_feature' 

    ];

    /**
     * PlaceController constructor.
     * @param PlaceRepository $places
     */
    public function __construct(PlaceRepository $places) {
        $this->places = $places;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManagePlaceRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManagePlaceRequest $request) {
       
        return view('backend.isolatedpalces.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table(Request $request) {
        foreach ($request->columns as $column) {
            if ($column['data'] == 'address') {
                $matchValue     = @$column['search']['value1']['matchValue'];
                $notMatchValue  = @$column['search']['value2']['notMatchValue'];
            }
            if ($column['data'] == 'pluscode') {
                $matchValue2     = @$column['search']['value1']['matchValue2'];
                $notMatchValue2  = @$column['search']['value2']['notMatchValue2'];
            }
            if ($column['data'] == 'section') {
                $section = $column['search']['value'];
            }
            if ($column['data'] == 'country_title') {
                $countryId = $column['search']['value'];
            }
            if ($column['data'] == 'city_title') {
                $cities_id = $column['search']['value'];
            }
            if ($column['data'] == 'place_id_title') {
                $placeType = $column['search']['value'];
            }
            if ($column['data'] == 'title') {
                $placeTitle = $column['search']['value'];
            }
        }

        $orderData = current($request->order);
        $orderColumn = $request->columnName($orderData['column']);
        $orderDir = $orderData['dir'];
        $query = Place::select([
            config('locations.place_table') . '.id',
            DB::raw('MAX(' . config('locations.place_table') . '.cities_id)'),
            DB::raw('MAX(' . config('locations.place_table') . '.countries_id)'),
            DB::raw('MAX(' . config('locations.place_table') . '.place_type) as place_id_title'),
            DB::raw('MAX(' . config('locations.place_table') . '.media_done)'),
            DB::raw('MAX(' . config('locations.place_table') . '.active) as active'),
            DB::raw('MAX(' . config('locations.place_table') . '.media_count) as media_count'),
            DB::raw('MAX(' . config('locations.place_table') . '.place_section) as section'),
            DB::raw('MAX(' . config('locations.place_table') . '.pluscode) as pluscode'),
            DB::raw('MAX(transsingle.title) as title'),
            DB::raw('MAX(transsingle.address) as address'),
            DB::raw('MAX(cities_trans.title) as city_title'),
            DB::raw('MAX(countries_trans.title) as country_title')
        ])
            ->where(function ($query) {
                $query->where('auto_import', '!=', 1)
                      ->orWhere('auto_import', null);
                foreach($this->placeTypesArray as $key=>$placeType){
                        $query->whereRaw('SUBSTRING_INDEX( place_type, ",", 1 ) NOT like "%'.$placeType.'%"');
                }
            })
            ->leftJoin(DB::raw('places_trans AS transsingle FORCE INDEX (places_id)'), function ($query) {
                $query->on('transsingle.places_id', '=', 'places.id')
                    ->where('transsingle.languages_id', '=', 1);
            })
            ->leftJoin('cities_trans', function ($query) {
                $query->on('cities_trans.cities_id', '=', 'places.cities_id')
                    ->where('cities_trans.languages_id', '=', 1);
            })
            ->Leftjoin('countries_trans', function ($query) {
                $query->on('countries_trans.countries_id', '=', 'places.countries_id')
                    ->where('countries_trans.languages_id', '=', 1);
            })
            ->when($matchValue, function ($query) use ($matchValue) {
                $this->filtered = true;
                $matchValues = explode("/", $matchValue );
                return $query->where(function ($query) use ($matchValues) {
                    foreach ($matchValues as $key => $matchValue) {
                        if ($key)
                            $query->orWhere('transsingle.address', 'LIKE', "%".$matchValue."%");
                        else
                            $query->where('transsingle.address', 'LIKE', "%".$matchValue."%");

                    }
                });
            })
            ->when($notMatchValue, function ($query) use ($notMatchValue) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue );
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue) {
                        $query->where('transsingle.address', 'NOT LIKE', "%" . $notMatchValue . "%");
                    }
                });
            })
            ->when($matchValue2, function ($query) use ($matchValue2) {
                $this->filtered = true;
                return $query->where('places.pluscode', 'LIKE', "%".$matchValue2."%");
            })
            ->when($notMatchValue2, function ($query) use ($notMatchValue2) {
                $this->filtered = true;
                $notMatchValues = explode("/", $notMatchValue2 );
                $query->where(function ($query) use ($notMatchValues) {
                    foreach ($notMatchValues as $key => $notMatchValue2) {
                        $query->where('places.pluscode', 'NOT LIKE', "%" . $notMatchValue2 . "%");
                    }
                });
            })
            ->when($section, function ($query) use ($section) {
                $this->filtered = true;
                return $query->where('places.place_section', $section);
            })
            ->when($countryId, function ($query) use ($countryId) {
                $this->filtered = true;
                return $query->where('places.countries_id', $countryId);
            })
            ->when($cities_id, function ($query) use ($cities_id) {
                $this->filtered = true;
                return $query->where('places.cities_id', $cities_id);
            })
            ->when($placeType, function ($query) use ($placeType) {
                $this->filtered = true;
                return $query->where('places.place_type', $placeType);
            })
            ->when($placeTitle, function ($query) use ($placeTitle) {
                $this->filtered = true;
                return $query->where(function ($query) use ($placeTitle) {
                    $query->where('transsingle.title', 'LIKE', '%'.$placeTitle.'%')
                        ->orWhere('transsingle.address', 'LIKE', '%'.$placeTitle.'%')
                        ->orWhere('places.pluscode', 'LIKE', '%'.$placeTitle.'%');
                });
            })
            ->groupBy('places.id');

        $response['recordsTotal'] = 10000000;

        /*this is a wrong value for $response['recordsFiltered'] count, as $query->count() does not work for large data,
        will fix it after ElasticSearch instalation*/
        $response['recordsFiltered'] = $response['recordsTotal'];
        $places = $query->offset($request->start)->limit($request->length)->orderBy($orderColumn, $orderDir)->get();
        foreach ($places as $place) {
            $place->empty = '';
            $place->action = $place->action_buttons;
            $place->media_done_new = 'Yes';
            if(!isset($place->pluscode)){
                $place->pluscode = '-';
            }
        }
        $response['data'] = $places;
        $response['draw'] = $request->draw;
        $response['input'] = $request->all();

        return response()->json($response);
    }

}
