<?php

namespace App\Http\Controllers\Backend\ActivityMedia;

use App\Http\Requests\Backend\ActivityMedia\UpdateActivityMediaRequest;
use App\Models\ActivityMedia\Media;
use App\Models\ActivityMedia\MediaTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\ActivityMedia\ManageActivityMediaRequest;
use App\Http\Requests\Backend\ActivityMedia\StoreActivityMediaRequest;
use App\Models\AdminLogs\AdminLogs;
use App\Repositories\Backend\ActivityMedia\ActivityMediaRepository;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ActivityMediaController extends Controller
{
    protected $activitymedia;

    /**
     * ActivityMediaController constructor.
     * @param ActivityMediaRepository $activitymedia
     */
    public function __construct(ActivityMediaRepository $activitymedia)
    {
        $this->activitymedia = $activitymedia;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->activitymedia->getForDataTable())
            ->addColumn('',function(){
                return null;
            })
            ->addColumn('action', function ($activitymedia) {
                return $activitymedia->action_buttons;
            })
            ->make(true);
    }


    /**
     * @param ManageActivityMediaRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(ManageActivityMediaRequest $request)
    {
        $term = trim($request->q);

        if (!empty($term)) {
            $medias_trans       = MediaTranslations::where('title', 'LIKE' ,$term.'%')->skip(($request->page - 1) * 10)->take(10)->get();
            $paginate           = true;
            if ($medias_trans->isEmpty()) {
                $medias_trans       = MediaTranslations::where('title', 'LIKE' ,$term.'%')->get();
                $paginate           = false;
            }
            $formatted_medias   = [];
            foreach ($medias_trans as $value) {
                $formatted_medias[] = ['id' => $value->medias_id, 'text' => $value->title];
            }
            return response()->json(['medias' => $formatted_medias, 'paginate' => $paginate]);
        }

        return view('backend.activitymedia.index');
    }

    /**
     * @param ManageActivityMediaRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(ManageActivityMediaRequest $request)
    {
        return view('backend.activitymedia.create',[]);
    }

    /**
     * @param StoreActivityMediaRequest $request
     * @return mixed
     */
    public function store(StoreActivityMediaRequest $request)
    {
        $data = [];

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        /* Pass All Relation and Common fields through $extra Array */
        $extra = [
            'url' => $request->input('url')
        ];
        $this->activitymedia->create($data,$extra);

        return redirect()->route('admin.activitymedia.index')->withFlashSuccess('Media Created!');
    }

    /**
     * @param $id
     * @param ManageActivityMediaRequest $request
     * @return mixed
     */
    public function destroy($id, ManageActivityMediaRequest $request)
    {
        $item = Media::findOrFail($id);
        /* Delete Children Tables Data of this ActivityMedia */
        $child = MediaTranslations::where(['medias_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.activitymedia.index')->withFlashSuccess('Media Deleted Successfully');
    }

    /**
     * @param $id
     * @param ManageActivityMediaRequest $request
     * @return mixed
     */
    public function edit($id, ManageActivityMediaRequest $request)
    {
        $data = [];
        $activitymedia = Media::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = MediaTranslations::where([
                'languages_id' => $language->id,
                'medias_id'   => $id
            ])->first();

            if(!empty($model)){
                $data['title_'.$language->id]       = $model->title ?: null;
                $data['description_'.$language->id] = $model->description ?: null;
            }
        }

        $data['url'] = $activitymedia->url;

        return view('backend.activitymedia.edit')
            ->withLanguages($this->languages)
            ->withMedia($activitymedia)
            ->withMediaid($id)
            ->withData($data);
    }

    /**
     * @param $id
     * @param ManageActivityMediaRequest $request
     * @return mixed
     */
    public function update($id, UpdateActivityMediaRequest $request)
    {
        $activitymedia = Media::findOrFail($id);

        $data = [];

        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
            $data[$language->id]['description_'.$language->id] = $request->input('description_'.$language->id);
        }

        $extra = [
            'url' => $request->input('url')
        ];

        $this->activitymedia->update($id , $activitymedia, $data , $extra);

        return redirect()->route('admin.activitymedia.index')
            ->withFlashSuccess('Media updated Successfully!');
    }

    /**
     * @param $id
     * @param ManageActivityMediaRequest $request
     * @return mixed
     */
    public function show($id, ManageActivityMediaRequest $request)
    {
        $activitymedia = Media::findOrFail($id);
        $activitymediaTrans = MediaTranslations::where(['medias_id' => $id])->get();

        return view('backend.activitymedia.show')
            ->withMedia($activitymedia)
            ->withMediatrans($activitymediaTrans);
    }

    public function delete_ajax(ManageActivityMediaRequest $request)
    {
        $ids = $request->input('ids');
        if (!empty($ids)) {
            $ids = explode(',', $request->input('ids'));
            foreach ($ids as $key => $value) {
                $this->delete_single_ajax($value);
            }
        }

        echo json_encode([
            'result' => true
        ]);
    }

    /**
     * @param $id
     * @return bool
     *
     */
    public function delete_single_ajax($id)
    {
        $item = Media::find($id);

        if (empty($item)) {
            return false;
        }

        $item->deleteTrans();
        $item->delete();

        AdminLogs::create(['item_type' => 'medias', 'item_id' => $id, 'action' => 'delete', 'time' => time(), 'admin_id' => Auth::user()->id]);
    }
}