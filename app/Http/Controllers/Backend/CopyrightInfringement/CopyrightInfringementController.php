<?php

namespace App\Http\Controllers\Backend\CopyrightInfringement;

use App\Http\Requests\Backend\CopyrightInfringement\CopyrightRequest;
use App\Repositories\Backend\CopyrightInfringement\CopyrightInfringementRepository;
use Yajra\DataTables\Utilities\Request;
use App\Http\Controllers\Controller;

class CopyrightInfringementController extends Controller
{
    protected $copyrightInfringementRepository;


    /**
     * CopyrightInfringementController constructor.
     * @param CopyrightInfringementRepository $copyrightInfringementRepository
     */
    public function __construct(CopyrightInfringementRepository $copyrightInfringementRepository)
    {
        $this->copyrightInfringementRepository = $copyrightInfringementRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->copyrightInfringementRepository->index($request->input('blacklist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CopyrightRequest $request)
    {
        return $this->copyrightInfringementRepository->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->copyrightInfringementRepository->show($id);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function table(Request $request)
    {
        return $this->copyrightInfringementRepository->table($request);
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function confirm(Request $request)
    {
        return $this->copyrightInfringementRepository->confirm($request->get('id'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reply(Request $request)
    {
        return $this->copyrightInfringementRepository->reply($request->all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        return $this->copyrightInfringementRepository->delete($request->id);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getCopyrightPolicy(Request $request)
    {
        return $this->copyrightInfringementRepository->getCopyrightPolicy((int)$request->id);
    }

    /**
     * @return mixed
     */
    public function getSuspendedAccounts()
    {
        return $this->copyrightInfringementRepository->getSuspendedAccounts();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSuspendedAccountsTable(Request $request)
    {
        return $this->copyrightInfringementRepository->getSuspendedAccountsTable($request);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getHistory()
    {
        return $this->copyrightInfringementRepository->getHistory();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHistoryTable(Request $request)
    {
        return $this->copyrightInfringementRepository->getHistoryTable($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function restorePost(Request $request)
    {
        return $this->copyrightInfringementRepository->restorePost((int)$request->id);
    }
}
