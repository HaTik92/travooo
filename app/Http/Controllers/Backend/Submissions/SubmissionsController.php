<?php

namespace App\Http\Controllers\Backend\Submissions;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Models\Place\UnapprovedItem;
use App\Models\Submissions\Submissions;
use App\Models\User\User;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Utilities\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Country\Countries;
use App\Models\Country\CountriesTranslations;
use App\Models\City\Cities;
use App\Models\City\CitiesTranslations;
use App\Models\States\States;
use App\Models\States\StateTranslation;
use App\Models\Place\Place;
use App\Models\Place\PlaceTranslations;

class SubmissionsController extends Controller
{
    // use CreateUpdateStatisticTrait;

    protected $languages;

    /**
     * SpamReportController constructor.
     */
    public function __construct()
    {
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('backend.submissions.index');
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function table(Request $request) {
        $allSubmissions = Submissions::select('users.name as user_name', 'users.email as user_email', 'search_submissions.*')->join('users', 'user_id', '=', 'users.id')->where("search_submissions.status", "=", 0);
        return Datatables::of($allSubmissions)
            ->addColumn('actions', function ($submission) {
                return $submission->actions;
            })
            ->addColumn('place_hyperlink', function($submission) {
                return "https://www.google.com/maps/place/".$submission->lat.",".$submission->lng;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function getApprovedSubmissions() {
        $allSubmissions = Submissions::select('users.name as user_name', 'users.email as user_email', 'search_submissions.*')->join('users', 'user_id', '=', 'users.id')->where("search_submissions.status", "=", 1);
        return Datatables::of($allSubmissions)
            ->addColumn('actions', function ($submission) {
                return $submission->actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function getDisApprovedSubmissions()
    {
        $allSubmissions = Submissions::select('users.name as user_name', 'users.email as user_email', 'search_submissions.*')->join('users', 'user_id', '=', 'users.id')->where("search_submissions.status", "=", 2);
        return Datatables::of($allSubmissions)
            ->addColumn('actions', function ($submission) {
                return $submission->actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function approve($id) {

        $submission = Submissions::where('id', $id)->first();
        $countryId = '';
        $cityId = '';
        $stateId = '';
        if($submission->place_country) {
            $country = CountriesTranslations::where("title", $submission->place_country)->first();
            if($country) {
                $countryId = $country->countries_id;
                // dd($country);
            } else {
                $countryDetails = json_decode(file_get_contents("https://api.mapbox.com/geocoding/v5/mapbox.places/".$submission->place_country.".json?access_token=".config('mapbox.token')), true)['features'][0];
                // dd($countryDetails);
                $newCountry = new Countries();
                $newCountry->iso_code = $submission->iso_code;
                $newCountry->lat = $countryDetails['center'][1];
                $newCountry->lng = $countryDetails['center'][0];
                $newCountry->save();
                $countryId = $newCountry->id;

                $countryTrans = new CountriesTranslations();
                $countryTrans->countries_id = $countryId;
                $countryTrans->languages_id = 1;
                $countryTrans->title = $submission->place_country; 
                $countryTrans->save();
            }
        }
        
        if($submission->place_city) {
            $city = CitiesTranslations::where("title", $submission->place_city)->first();
            if($city) {
                $cityId = $city->cities_id;
            } else {
                $cityDetails = json_decode(file_get_contents("http://api.geonames.org/search?name=".$submission->place_city."&country=".$submission->iso_code."&type=json&username=routi&style=SHORT"), true)['geonames'][0];
                // dd($cityDetails, $countryId);
                $newCity = new Cities();
                $newCity->iso_code = $submission->iso_code;
                $newCity->countries_id = $countryId;
                $newCity->lat = $cityDetails['center'][1];
                $newCity->lng = $cityDetails['center'][0];
                $newCity->active = true;
                $newCity->is_capital = $cityDetails['fcode'] === "PPLC" ? 1 : 2;
                $newCity->save();
                $cityId = $newCity->id;

                $cityTrans = new CitiesTranslations();
                $cityTrans->cities_id = $countryId;
                $cityTrans->languages_id = 1;
                $cityTrans->title = $submission->place_city; 
                $cityTrans->save();
            }
        }
        
        if($submission->places_state) {
            $state = StateTranslation::where("title", $submission->place_state)->first();
            if($state) {
                $stateId = $state->id;
            } else {
                $stateDetails = json_decode(file_get_contents("https://api.mapbox.com/geocoding/v5/mapbox.places/".$submission->place_state.".json?access_token=".config('mapbox.token')), true)['features'][0];
                $newState = new States();
                $newState->countries_id = $countryId;
                $newState->lat = $stateDetails['center'][1];
                $newState->lng = $stateDetails['center'][0];
                $newState->active = true;
                $newState->save();
                $stateId = $newState->id;

                $cityTrans = new StateTranslation();
                $cityTrans->states_id = $stateId;
                $cityTrans->languages_id = 1;
                $cityTrans->title = $submission->place_state; 
                $cityTrans->save();
            }
        }

        $newPlace = new Place();
        $newPlace->countries_id = $countryId;
        $newPlace->cities_id = $cityId;
        $newPlace->states_id = $stateId ? $stateId : false;
        $newPlace->place_type = $submission->place_category;
        $newPlace->active = true;
        $newPlace->lat = $submission->lat;
        $newPlace->lng = $submission->lng;
        $newPlace->save();

        $placeTrans = new PlaceTranslations();
        $placeTrans->title = $submission->place_name;
        $placeTrans->places_id = $newPlace->id;
        $placeTrans->languages_id = 1;
        $placeTrans->address = $submission->place_address;
        $placeTrans->save();

        $submission->update([
            "status" => 1
        ]);
        return redirect()->back();
    }

    public function disapprove($id) {
        Submissions::where('id', $id)->update([
            "status" => 2
        ]);
        return redirect()->back();
    }

    public function create(Request $request) {
        // dd($request->all());
        // \Mail::raw('New Submission', function($message) {
        //     $message->subject('Thank you for your suggestion, our admins will look into your submission soon.')->to(auth()->user()->email);
        //  });
        $category;
        switch ($request->placeCategory) {
            case 1:
                $category = "Point of Interest";
                break;
            case 2:
                $category = "Restaurant";
                break;
            case 3:
                $category = "Hotel";
                break;
        }

        $submission = new Submissions();
        $submission->user_id = auth()->user()->id;
        $submission->place_name = $request->placeName;
        $submission->place_address = $request->placeAddress;;
        $submission->place_category = $category;
        $submission->place_city = $request->placeCity;
        //  $submission->place_hyperlink = "https://www.google.com/maps/place/".$request->newPosition[1].",".$request->newPosition[0];
        $submission->place_country = $request->placeCountry;
        $submission->place_state = $request->placeState;
        $submission->lat =$request->newPosition[1];
        $submission->lng =$request->newPosition[0];
        $submission->iso_code = $request->isoCode;
        $submission->save();
        return '1';
    }

    
}