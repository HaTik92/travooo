<?php

namespace App\Http\Controllers\Backend\Submissions\Approved;

use App\Helpers\Traits\CreateUpdateStatisticTrait;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Models\Place\UnapprovedItem;
use App\Models\Submissions\Submissions;
use App\Models\User\User;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Utilities\Request;
use Yajra\DataTables\Facades\DataTables;

class ApprovedController extends Controller
{
    // use CreateUpdateStatisticTrait;

    protected $languages;

    /**
     * SpamReportController constructor.
     */
    public function __construct()
    {
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('backend.submissions-approved.index');
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function table(Request $request) {
        $allSubmissions = Submissions::select('users.name as user_name', 'users.email as user_email', 'search_submissions.*')->join('users', 'user_id', '=', 'users.id')->where("search_submissions.status", "=", 1);
        return Datatables::of($allSubmissions)
            // ->addColumn('actions', function ($submission) {
            //     return $submission->actions;
            // })
            // ->rawColumns(['actions'])
            ->make(true);
    }
}