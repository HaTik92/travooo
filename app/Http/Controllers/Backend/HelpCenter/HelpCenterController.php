<?php

namespace App\Http\Controllers\Backend\HelpCenter;

use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Utilities\Request;
use App\Models\Help\HelpCenter;
use App\Models\Help\HelpCenterContent;
use App\Models\Help\HelpCenterLIkeDislike;
use App\Models\Help\HelpCenterSubMenu;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;
// use App\Helpers\Traits\CreateUpdateStatisticTrait;
// use App\Models\Country\Countries;
// use App\Models\Country\CountriesTranslations;
// use App\Models\City\Cities;
// use App\Models\City\CitiesTranslations;
// use App\Models\States\States;
// use App\Models\States\StateTranslation;
// use App\Models\Place\Place;
// use App\Models\Place\PlaceTranslations;

class HelpCenterController extends Controller
{
    protected $languages;

    /**
     * SpamReportController constructor.
     */
    public function __construct()
    {
        $this->languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }


    public function menuesView() {
        return view('backend.help_center.menues.index');
    }

    public function subMenuesView() {
        return view('backend.help_center.sub_menues.index');
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function menuesTable(Request $request) {
        $allMenues = HelpCenter::select("name", "id", "created_at", "updated_at");
        return Datatables::of($allMenues)
            ->addColumn('actions', function ($menu) {
                return '<a href="'.route('admin.help-center.menu.edit', $menu).'" class="btn btn-xs btn-info"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i></a>
                <a href="'.route('admin.help-center.menu.delete', $menu).'" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i></a>';
            })
            ->rawColumns(['actions'])
            ->make(true);
    }


    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function subMenuesTable(Request $request) {
        $allSubMenues = HelpCenterSubMenu::select('help_center_sub_menues.id as id', 'help_center_categories.name as category_name', 'help_center_sub_menues.title as title', 'help_center_sub_menues.position as position', 'help_center_sub_menues.created_at', 'help_center_sub_menues.updated_at')->join('help_center_categories', 'help_center_sub_menues.category_id', '=', 'help_center_categories.id');
        return Datatables::of($allSubMenues)
            ->addColumn('actions', function ($subMenu) {
                return '<a href="'.route('admin.help-center.subMenu.edit', $subMenu).'" class="btn btn-xs btn-info"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i></a>
                <a href="'.route('admin.help-center.subMenu.delete', $subMenu).'" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i></a>';
            })
            ->rawColumns(['actions'])
            ->make(true);
    }


    public function deleteMenu($id) {
        HelpCenter::findOrFail($id)->delete();
    }


    public function deleteSubMenu($id) {
        HelpCenterSubMenu::findOrFail($id)->delete();
        return back();
    }


    public function menuEditView($id) {
        $menu = HelpCenter::findOrFail($id);
        return view('backend.help_center.menues.edit', [
                'menu' => $menu, 
                'page_title' => "Edit menu - ".$menu->name,
                'languages' => $this->languages
            ]);
    }

    public function menuUpdate(Request $request, $id) {
        HelpCenter::where("id", $id)->update(['name' => $request->title]);
        return redirect()->route("admin.help-center.menu.edit", $id);
        // dd($request->all());
    }

    public function subMenuEditView($id) {
        $subMenu = HelpCenterSubMenu::where("id", $id)->with(["content", "category"])->first();
        $categories = HelpCenter::all();
        return view('backend.help_center.sub_menues.edit', [
            'subMenu' => $subMenu,
            'page_title' => "Edit sub menu - ".$subMenu->title,
            'categories' => $categories,
            'languages' => $this->languages
        ]);
    }

    function subMenuUpdate(Request $request, $id) {
        $data = $request->all();
        foreach ($data as $key => $value) {
            if (str_contains($key, "content_")) {
                preg_match('/\d+/',$key, $content_id);
                HelpCenterContent::where([["sub_menu_id", $id], ["id", $content_id[0]]])->update([
                    "content" => $value,
                ]);
            } else if (str_contains($key, "mobile_faq") || str_contains($key, "desktop_faq")) {
                preg_match('/\d+/',$key, $content_id);
                preg_match('/[a-z]+_[a-z]+/',$key, $field_key);
                $field = $field_key[0];
                HelpCenterContent::where([["sub_menu_id", $id], ["id", $content_id[0]]])->update([
                    $field => $value,
                ]);
            } else if (str_contains($key, "position_")) {
                preg_match('/\d+/',$key, $content_id);
                HelpCenterContent::where([["sub_menu_id", $id], ["id", $content_id[0]]])->update([
                    "position" => $data["position_".$content_id[0]]
                ]);
            }
        }
        HelpCenterSubMenu::where("id", $id)->update([
            'category_id' => $data['category'],
            'title' => $data['title']
        ]);
        return redirect()->route("admin.help-center.subMenu.edit", $id);
    }

    public function newSubMenu() {
        $title = "Sub menu".now();
        $newSubMenu = new HelpCenterSubMenu();
        $newSubMenu->category_id = HelpCenter::all()[0]->id;
        $newSubMenu->title = $title;
        $newSubMenu->position = "";
        $newSubMenu->save();
        $categories = HelpCenter::all();
        return view('backend.help_center.sub_menues.edit', [
            'subMenu' => $newSubMenu,
            'page_title' => $title,
            'categories' => $categories,
            'languages' => $this->languages
        ]);
    }


    public function newSubMenuContent(Request $request, $type) {
        if (HelpCenterContent::where([
            ["sub_menu_id", $request->submenu_id],
            ["type", $type]
        ])->exists() && $type == "device_faq") {
            return response()->json([
                "message" => "Already exists"
            ], 409);
        }
        $newSubMenuContent = new HelpCenterContent();
        $newSubMenuContent->sub_menu_id = $request->submenu_id;
        $newSubMenuContent->type = $type;
        $newSubMenuContent->content = " ";
        $newSubMenuContent->desktop_faq = " ";
        $newSubMenuContent->mobile_faq = " ";
        $newSubMenuContent->save();

        return response()->json($newSubMenuContent);
    }

    public function mediaUploadSubMenuContent(Request $request, $id) {
        $content_media = $request->media;
        $allowedMimeTypes = ["jpeg", "jpg", "png"];
        $mimeType = $content_media->getClientOriginalExtension();
        if(in_array($mimeType, $allowedMimeTypes)) {
            
            $filename = microtime(true) . '-' . auth()->user()->id . '-help-center-content-image.' . $content_media->getClientOriginalExtension();

            Storage::disk('s3')->putFileAs(
                'help_center_media/',
                $content_media,
                $filename,
                'public'
            );
            $content_media_url = 'https://s3.amazonaws.com/travooo-images2/help_center_media/' . $filename;
            HelpCenterContent::where("id", $id)->update([
                "media_url" => $content_media_url
            ]);
            return response()->json([
                "media_url" => $content_media_url
            ], 200);
        }

        return response()->json([
            "message" => "Wrong mime type of file"
        ], 405);
    }
    

    public function deleteSubMenuContent($id) {
        if (HelpCenterContent::where("id", $id)->delete() ) {
            return response()->json(["id" => $id], 200);
        } else {
            return response()->json([], 500);
        }

    }

}
