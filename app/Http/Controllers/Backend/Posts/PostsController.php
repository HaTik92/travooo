<?php

namespace App\Http\Controllers\Backend\Posts;

use App\Http\Constants\CommonConst;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function index()
    {
        $types = CommonConst::postTypes(true);

        return view('backend.posts.index', compact('types'));
    }

    public function search(Request $request)
    {
        $post = (CommonConst::postModels()[$request->get('type')])::whereId($request->get('id'))->first();

        return new JsonResponse(['data' => $post]);
    }
}
