<?php

namespace App\Http\Controllers\Backend\Religion;

use App\Http\Requests\Backend\Religion\UpdateReligionRequest;
use App\Models\Religion\Religion;
use App\Models\Religion\ReligionTranslations;
use App\Http\Controllers\Controller;
use App\Models\Access\Language\Languages;
use App\Http\Requests\Backend\Religion\ManageReligionRequest;
use App\Http\Requests\Backend\Religion\StoreReligionRequest;
use App\Repositories\Backend\Religion\ReligionRepository;
use Yajra\DataTables\Facades\DataTables;

class ReligionController extends Controller
{
    protected $religions;

    /**
     * ReligionController constructor.
     * @param ReligionRepository $religions
     */
    public function __construct(ReligionRepository $religions)
    {
        $this->religions = $religions;
        $this->languages = \DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
    }

    /**
     * @param ManageReligionRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageReligionRequest $request)
    {
        return view('backend.religion.index');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return Datatables::of($this->religions->getForDataTable())
            ->addColumn('action', function ($religions) {
                return view('backend.buttons', ['params' => $religions, 'route' => 'religion']);
            })
            ->make(true);
    }

    /**
     * @param ManageReligionRequest $request
     * @return mixed
     */
    public function create(ManageReligionRequest $request)
    {
        return view('backend.religion.create');
    }

    /**
     * @param StoreReligionRequest $request
     *
     * @return mixed
     */
    public function store(StoreReligionRequest $request)
    {   
        $data = [];
        $active = 2;
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
        }

        if($request->input('active') == 1){
            $active = 1;
        }
        
        /* Pass All Relation and Common Fields Through $extra Array */
        $extra = [
            'active' => $active
        ];

        $this->religions->create($data,$extra);

        return redirect()->route('admin.religion.index')->withFlashSuccess('Religion Created!');
    }

    /**
     * @param Religion $id
     *
     * @param ManageReligionRequest $request
     * @return mixed
     */
    public function destroy($id, ManageReligionRequest $request)
    {      
        $item = Religion::findOrFail($id);
        /* Delete Children Tables Data of this country */
        $child = ReligionTranslations::where(['religions_id' => $id])->get();
        if(!empty($child)){
            foreach ($child as $key => $value) {
                $value->delete();
            }
        }
        $item->delete();

        return redirect()->route('admin.religion.index')->withFlashSuccess('Religion Deleted Successfully');
    }

    /**
     * @param Religion $id
     *
     * @param ManageReligionRequest $request
     * @return mixed
     */
    public function edit($id, ManageReligionRequest $request)
    {
        $data = [];
        $religion = Religion::findOrFail($id);

        foreach ($this->languages as $key => $language) {
            $model = ReligionTranslations::where([
                'languages_id' => $language->id,
                'religions_id'   => $id
            ])->first();

            if (!empty($model)) {
                $data['title_'.$language->id] = $model->title ?: null;
            }
        }

        $data['active'] = $religion->active;

        return view('backend.religion.edit')
            ->withLanguages($this->languages)
            ->withReligion($religion)
            ->withReligionid($id)
            ->withData($data);
    }

    /**
     * @param Religion $id
     * @param ManageReligionRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateReligionRequest $request)
    {   
        $religion = Religion::findOrFail($id);
        $active = 2;
        $data = [];
        
        foreach ($this->languages as $key => $language) {
            $data[$language->id]['title_'.$language->id] = $request->input('title_'.$language->id);
        }

        if(!empty($request->input('active'))){
            $active = 1;
        }
        
        /* Send All Relation and Common Fields Through $extra Array */     
        $extra = [
            'active' => $active
        ];

        $this->religions->update($id , $religion, $data , $extra);

        return redirect()->route('admin.religion.index')
            ->withFlashSuccess('Religion updated Successfully!');
    }

    /**
     * @param $id
     *
     * @param ManageReligionRequest $request
     * @return mixed
     */
    public function show($id, ManageReligionRequest $request)
    {   
        $religion = Religion::findOrFail($id);
        $religionTrans = ReligionTranslations::where(['religions_id' => $id])->get();

        return view('backend.religion.show')
            ->withReligion($religion)
            ->withReligiontrans($religionTrans);
    }

    /**
     * @param Religion $religion
     * @param $status
     *
     * @param ManageReligionRequest $request
     * @return mixed
     */
    public function mark(Religion $religion, $status, ManageReligionRequest $request)
    {
        $religion->active = $status;
        $religion->save();
        return redirect()->route('admin.religion.index')
            ->withFlashSuccess('Religion Status Updated!');
    }
}