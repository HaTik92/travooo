<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Access\language\Languages;
use League\Fractal\Resource\Item;
use App\Transformers\Country\CountryTransformer;
use Illuminate\Support\Facades\DB;
use App\Models\Country\ApiCountry as Country;
use App\Models\Country\CountriesFollowers;
use Illuminate\Support\Facades\Auth;
use App\Models\ActivityLog\ActivityLog;
use App\CountriesContributions;
use Illuminate\Support\Facades\Input;
use App\Models\Hotels\Hotels;
use App\Models\Hotels\HotelFollowers;
use App\Models\Reviews\Reviews;
use App\Models\Posts\Checkins;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Models\TripPlans\TripPlans;
use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use Intervention\Image\ImageManagerStatic as Image;

class HotelController extends Controller {

    public function __construct() {
        $this->middleware('auth:user');
    }

    public function getIndex($hotel_id, Request $request) {
        $language_id = 1;
        $hotel = \App\Models\Hotels\Hotels::find($hotel_id);

        if (!$hotel) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        $place = Hotels::with([
                    'trans' => function ($query) use($language_id) {
                        $query->where('languages_id', $language_id);
                    },
                    'getMedias',
                    'getMedias.users',
                    'getMedias.comments',
                    'followers'
                ])
                ->where('id', $hotel_id)
                ->where('active', 1)
                ->first();

        $db_hotel_reviews = Reviews::where('hotels_id', $hotel->id)->get();
        if (!count($db_hotel_reviews)) {
            $hotel_reviews = get_google_reviews($hotel->provider_id);
            if (is_array($hotel_reviews)) {
                foreach ($hotel_reviews AS $review) {
                    $r = new Reviews;
                    $r->hotels_id = $hotel->id;
                    $r->google_author_name = $review->author_name;
                    $r->google_author_url = $review->author_url;
                    $r->google_language = $review->language;
                    $r->google_profile_photo_url = $review->profile_photo_url;
                    $r->score = $review->rating;
                    $r->google_relative_time_description = $review->relative_time_description;
                    $r->text = $review->text;
                    $r->google_time = $review->time;
                    $r->save();
                }
            }
        }

        $db_hotel_reviews = Reviews::where('hotels_id', $hotel->id)->get();
        $data['reviews'] = $db_hotel_reviews;

        $data['reviews_avg'] = Reviews::where('hotels_id', $hotel->id)->avg('score');

        // get hotels nearby
        $data['hotels_nearby'] = Hotels::with('medias')
                ->whereHas("medias", function ($query) {
                    $query->where("medias_id", ">", 0);
                })
                ->where('cities_id', $hotel->cities_id)
                ->where('id', '!=', $hotel->id)
                ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $hotel->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $hotel->lng . ') ) + sin( radians(' . $hotel->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
                ->having('distance', '<', 15)
                ->orderBy('distance')
                ->take(10)
                ->get();


        $checkins = Checkins::where('hotels_id', $hotel->id)->orderBy('id', 'DESC')->get();

        $result = array();
        $done = array();
        foreach ($checkins AS $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                $result[] = $checkin;
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['checkins'] = $result;


        $data['hotel'] = $hotel;
        /*

          $client = new Client();
          //$client->getHttpClient()->setDefaultOption('verify', false);

          $result = $client->post('https://api.predicthq.com/oauth2/token', [
          'auth' => ['phq.PuhqixYddpOryyBIuLfEEChmuG5BT6DZC0vKMlhw', 'lZJvumnBXIELLxfWKW6EOw98T8Ia4-LrhHitxJYFwhGwm0c7n8jWgg'],
          'form_params' => [
          'grant_type' => 'client_credentials',
          'scope' => 'account events signals'
          ],
          'verify' => false
          ]);
          $events_array = false;
          if ($result->getStatusCode() == 200) {
          $json_response = $result->getBody()->read(1024);
          $response = json_decode($json_response);
          $access_token = $response->access_token;

          $get_events1 = $client->get('https://api.predicthq.com/v1/events/?within=10km@' . $hotel->lat . ',' . $hotel->lng . '&sort=start&category=school-holidays,politics,conferences,expos,concerts,festivals,performing-arts,sports,community', [
          'headers' => ['Authorization' => 'Bearer ' . $access_token],
          'verify' => false
          ]);
          if ($get_events1->getStatusCode() == 200) {
          $events1 = json_decode($get_events1->getBody()->read(100024));
          //dd($events);
          $events_array1 = $events1->results;
          $events_final = $events_array1;
          if ($events1->next) {
          $get_events2 = $client->get($events1->next, [
          'headers' => ['Authorization' => 'Bearer ' . $access_token],
          'verify' => false
          ]);
          if ($get_events2->getStatusCode() == 200) {
          $events2 = json_decode($get_events2->getBody()->read(100024));
          //dd($events);
          $events_array2 = $events2->results;
          $events_final = array_merge($events_array1, $events_array2);
          }
          }
          $data['events'] = $events_final;
          }
          }
         * 
         */
        $data['events'] = array();


        $data['my_plans'] = TripPlans::where('users_id', Auth::guard('user')->user()->id)->get();


        /*
          $city_name = $hotel->city->transsingle->title;
          $data['city_code'] = 0;
          $res = file_get_contents('https://srv.wego.com/places/search?query=' . $city_name);
          $results = json_decode($res);
          $cities = array();
          $i = 0;
          if (isset($results[0]) AND is_object($results[0]) AND $results[0]->type == "city") {
          $data['city_code'] = $results[0]->code;
          }

          if($request->has('ref') AND intval($request->get('ref'))) {
          $data['referrer_is_there'] = intval($request->get('ref'));
          }
         * 
         */






        //dd($data);
        return view('site.hotel.index', $data);
    }

    public function postFollow($hotelId) {
        $userId = Auth::guard('user')->user()->id;
        $hotel = Hotels::find($hotelId);
        if (!$hotel) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }


        $follower = HotelFollowers::where('hotels_id', $hotelId)
                ->where('users_id', $userId)
                ->first();

        if ($follower) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'This user is already following that Hotel',
                ],
                'success' => false
            ];
        }

        $new_follower = new HotelFollowers;
        $new_follower->hotels_id = $hotelId;
        $new_follower->users_id = $userId;
        $new_follower->save();

        //$this->updateStatistic($country, 'followers', count($country->followers));

        log_user_activity('Hotel', 'follow', $hotelId);


        return [
            'success' => true
        ];
    }

    public function postUnFollow($hotelId) {
        $userId = Auth::guard('user')->user()->id;

        $hotel = Hotels::find($hotelId);
        if (!$hotel) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        $follower = HotelFollowers::where('hotels_id', $hotelId)
                ->where('users_id', $userId);

        if (!$follower->first()) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'You are not following this Hotel.',
                ],
                'success' => false
            ];
        } else {
            $follower->delete();
            //$this->updateStatistic($country, 'followers', count($country->followers));

            log_user_activity('Hotel', 'unfollow', $hotelId);
        }

        return [
            'success' => true
        ];
    }

    public function postCheckFollow($hotelId) {
        $userId = Auth::guard('user')->user()->id;
        $hotel = Hotels::find($hotelId);
        if (!$hotel) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        $follower = HotelFollowers::where('hotels_id', $hotelId)
                ->where('users_id', $userId)
                ->first();

        return empty($follower) ? ['success' => false] : ['success' => true];
    }

    public function postTalkingAbout($hotelId) {
        $hotel = Hotels::find($hotelId);
        if (!$hotel) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        $shares = $hotel->shares;

        foreach ($shares AS $share) {
            $data['shares'][] = array(
                'user_id' => $share->user->id,
                'user_profile_picture' => check_profile_picture($share->user->profile_picture)
            );
        }

        $data['num_shares'] = count($shares);
        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function postNowInPlace($hotelId) {
        $hotel = Hotels::find($hotelId);
        if (!$hotel) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        $checkins = Checkins::where('hotels_id', $hotel->id)
                ->orderBy('id', 'DESC')
                ->take(5)
                ->get();

        $result = array();
        $done = array();
        foreach ($checkins AS $checkin) {
            if (!isset($done[$checkin->post_checkin->post->author->id])) {
                $result[] = array(
                    'name' => $checkin->post_checkin->post->author->name,
                    'id' => $checkin->post_checkin->post->author->id,
                    'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture)
                );
                $done[$checkin->post_checkin->post->author->id] = true;
            }
        }
        $data['live_checkins'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function ajaxPostReview($hotelId) {
        $hotel = Hotels::find($hotelId);
        if (!$hotel) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }

        if (Input::has('text')) {
            $review_text = Input::get('text');
            $review_score = Input::get('score');
            $review_author = Auth::guard('user')->user();
            $review_place_id = $hotel->id;

            $review = new Reviews;
            $review->hotels_id = $review_place_id;
            $review->by_users_id = $review_author->id;
            $review->score = $review_score;
            $review->text = $review_text;

            if ($review->save()) {
                log_user_activity('Hotel', 'review', $hotel->id);
                $data['success'] = true;
                $data['prepend'] = '<div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="' . check_profile_picture(Auth::guard('user')->user()->profile_picture) . '" alt="" style="width:45px;height:45px;">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">' . Auth::guard('user')->user()->name . '</a>
                                    <a href="#" class="comment-nickname"></a>
                                </div>
                                <div class="comment-txt">
                                    <p>' . $review_text . '</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-star-block">
                                        <select class="rating_stars">
                                            <option value="1" ' . ($review->score == 1 ? "selected" : "") . '>1</option>
                                            <option value="2" ' . ($review->score == 2 ? "selected" : "") . '>2</option>
                                            <option value="3" ' . ($review->score == 3 ? "selected" : "") . '>3</option>
                                            <option value="4" ' . ($review->score == 4 ? "selected" : "") . '>4</option>
                                            <option value="5" ' . ($review->score == 5 ? "selected" : "") . '>5</option>
                                        </select>
                                        <span class="count">
                                            <b>' . $review->score . '</b> / 5
                                        </span>
                                    </div>
                                    <div class="dot">·</div>
                                    <div class="com-time">' . diffForHumans(date("Y-m-d", time())) . '</div>
                                </div>
                            </div>
                        </div>';
            } else {
                $data['success'] = false;
            }
        } else {
            $data['success'] = false;
        }
        return json_encode($data);
    }

    public function ajaxHappeningToday($hotelId) {
        $hotel = Hotels::find($hotelId);
        if (!$hotel) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }


        $checkins = Checkins::whereHas("place", function ($query) use ($hotel) {
                    $query->where('cities_id', $hotel->cities_id);
                    $query->where('id', '!=', $hotel->id);
                })
                ->orderBy('id', 'DESC')
                ->get();

        /*
          ->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $place->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $place->lng . ') ) + sin( radians(' . $place->lat . ') ) * sin( radians( lat ) ) ) ) AS distance'))
          ->having('distance', '<', 15)
          ->orderBy('distance')
          ->take(10)
          ->get();
         * 
         */

        //$checkins = Checkins::where('place_id', $place->id)->orderBy('id', 'DESC')->take(5)->get();

        $result = array();
        foreach ($checkins AS $checkin) {
            $result[] = array(
                'post_id' => $checkin->post_checkin->post->id,
                'lat' => $checkin->hotel->lat,
                'lng' => $checkin->hotel->lng,
                'photo' => @$checkin->post_checkin->post->medias[0]->media->url,
                'name' => $checkin->post_checkin->post->author->name,
                'id' => $checkin->post_checkin->post->author->id,
                'profile_picture' => check_profile_picture($checkin->post_checkin->post->author->profile_picture),
                'date' => diffForHumans($checkin->post_checkin->post->created_at)
            );
        }
        $data['happenings'] = $result;

        $data['success'] = true;
        return json_encode($data);

        //https://api.joinsherpa.com/v2/entry-requirements/CA-BR
    }

    public function ajaxHotelRates($hotelId) {
        $hotel = Hotels::find($hotelId);
        if (!$hotel) {
            return [
                'data' => [
                    'error' => 400,
                    'message' => 'Invalid Hotel ID',
                ],
                'success' => false
            ];
        }


        $city_name = $hotel->city->transsingle->title;
        $data['city_code'] = 0;
        $res = file_get_contents('https://srv.wego.com/places/search?query=' . $city_name);
        $results = json_decode($res);
        $cities = array();
        $i = 0;
        if (isset($results[0]) AND is_object($results[0]) AND $results[0]->type == "city") {
            $data['city_code'] = $results[0]->code;
        }


        // start search for that Hotel
        //city_select=DXB&daterange=05%2F24%2F2019+-+05%2F25%2F2019
        $input['daterange'] = date('m/d/Y', time()) . ' - ' . date('m/d/Y', time() + 86400);
        $input['city_select'] = $data['city_code'];


        /// start search hotels
        $drange = explode("-", $input['daterange']);

        $date_from = date("Y-n-d", strtotime(trim($drange[0])));
        $date_to = date("Y-n-d", strtotime(trim($drange[1])));

        //dd($date_from);


        $token = $this->doAPIAuthorization();
        if ($token) {
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => "Bearer $token"]
            ]);

            $response = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                        [
                            'search' => [
                                'locale' => 'en',
                                'cityCode' => $input['city_select'],
                                'siteCode' => 'US',
                                'currencyCode' => 'USD',
                                'guestsCount' => 1,
                                'roomsCount' => 1,
                                'checkIn' => $date_from,
                                'checkOut' => $date_to,
                                'deviceType' => 'desktop'
                            ]
                        ]
                )]
            );

            if ($response->getStatusCode() == 200) {
                $result = json_decode($response->getBody()->getContents());
                $search_id = $result->search->id;
                //die();
                $offset = count($result->hotels);

                $rates_response = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                            [
                                'search' => [
                                    'id' => $search_id
                                ]
                            ]
                    )]
                );

                if ($rates_response->getStatusCode() == 200) {
                    $rates_result = json_decode($rates_response->getBody()->getContents());
                    $rates = $rates_result->rates;
                    //echo $token;
                }


                sleep(1);

                $response2 = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                            [
                                'search' => [
                                    'id' => $search_id,
                                    'locale' => 'en',
                                    'currencyCode' => 'USD',
                                    'offset' => $offset
                                ]
                            ]
                    )]
                );


                if ($response2->getStatusCode() == 200) {
                    $result2 = json_decode($response2->getBody()->getContents());
                    $search_id = $result2->search->id;
                    //die();
                    $offset = count($result2->hotels);


                    $rates_response2 = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                                [
                                    'search' => [
                                        'id' => $search_id
                                    ]
                                ]
                        )]
                    );

                    if ($rates_response2->getStatusCode() == 200) {
                        $rates_result2 = json_decode($rates_response2->getBody()->getContents());
                        $rates2 = $rates_result2->rates;
                        //echo $token;
                    }

                    $data['hotels'] = $result2->hotels;
                    $data['rates'] = array_merge($rates, $rates2);
                }
            }
        }
        // end search hotels

        $data['city_name'] = $result->search->city->name;

        $data['hotel_city'] = \App\Models\City\Cities::with('transsingle')->with('medias')
                        ->whereHas('transsingle', function ($query) use ($data) {
                            $query->where('title', 'LIKE', '%' . $data['city_name'] . '%');
                        })->get();



        //$data['hotels'] = $results->hotels;
        //$data['count'] = count($results->hotels);
        //$data['hotels'] = $results;
        //$data['rates'] = $frates;

        return json_encode($data);
    }

    private function doSearchHotel($input) {
        $drange = explode("-", $input['daterange']);

        $date_from = date("Y-n-d", strtotime(trim($drange[0])));
        $date_to = date("Y-n-d", strtotime(trim($drange[1])));

        //dd($date_from);


        $token = $this->doAPIAuthorization();
        if ($token) {
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => "Bearer $token"]
            ]);

            $response = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                        [
                            'search' => [
                                'locale' => 'en',
                                'cityCode' => $input['city_select'],
                                'siteCode' => 'US',
                                'currencyCode' => 'USD',
                                'guestsCount' => 1,
                                'roomsCount' => 1,
                                'checkIn' => $date_from,
                                'checkOut' => $date_to,
                                'deviceType' => 'desktop'
                            ]
                        ]
                )]
            );

            if ($response->getStatusCode() == 200) {
                $result = json_decode($response->getBody()->getContents());
                return $result;
                $offset = $result->count;
                //echo $token;

                $response2 = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                            [
                                'search' => [
                                    'id' => $search_id,
                                    'locale' => 'en',
                                    'currencyCode' => 'USD',
                                    'offset' => $offset
                                ]
                            ]
                    )]
                );

                if ($response2->getStatusCode() == 200) {
                    $result = json_decode($response2->getBody()->getContents());
                    $offset = $result->count;
                    //echo $token;

                    $response2 = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                                [
                                    'search' => [
                                        'id' => $search_id,
                                        'locale' => 'en',
                                        'currencyCode' => 'USD',
                                        'offset' => $offset
                                    ]
                                ]
                        )]
                    );

                    return array_merge($response, $response2);
                }
            }
        }
    }

    private function doCompleteSearchHotel($search_id) {
        //dd($input);
        $token = $this->doAPIAuthorization();
        if ($token) {
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => "Bearer $token"]
            ]);

            $response = $client->post('https://srv.wego.com/metasearch/hotels/searches', ['body' => json_encode(
                        [
                            'search' => [
                                'id' => $search_id
                            ]
                        ]
                )]
            );

            if ($response->getStatusCode() == 200) {
                $result = json_decode($response->getBody()->getContents());
                return $result;
                //echo $token;
            }
        }
    }

    private function doAPIAuthorization() {
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json', 'X-Wego-Version' => 1]
        ]);

        $response = $client->post('https://srv.wego.com/users/oauth/token', ['body' => json_encode(
                    [
                        'grant_type' => 'client_credentials',
                        'client_id' => 'ac9ac5c6c1c603fb33e61f01',
                        'client_secret' => '4ebe013c772c81b0ff3f764e',
                        'scope' => 'affiliate'
                    ]
            )]
        );

        if ($response->getStatusCode() == 200) {
            $result = json_decode($response->getBody()->getContents());
            $token = $result->access_token;
            //echo $token;
            return $token;
        } else {
            return null;
        }
    }

    public function ajaxTrackRef($hotel_id, Request $request) {
        $ref = $request->get('ref');
        $new_ref = new \App\Models\Referral\ReferralLinksClicks;
        $new_ref->links_id = $ref;
        $new_ref->users_id = Auth::guard('user')->user()->id;
        $new_ref->gender = '';
        $new_ref->nationality = '';
        $new_ref->save();
    }

    public function ajaxGetMedia(Request $request) {
        $hotel_id = $request->get('hotel_id');
        $final_results = array();
        $h = Hotels::find($hotel_id);
        if (is_object($h)) {
            $details_link = file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?'
                    . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
                    . '&placeid=' . $h->provider_id);
            $details = json_decode($details_link);
            //dd($details);
            if (isset($details->result)) {
                $details = $details->result;
                if (isset($details->photos)) {
                    $place_photos = $details->photos;
                    $raw_photos = array();
                    $i = 1;

                    foreach ($place_photos AS $pp) {
                        $file_contents = file_get_contents('https://maps.googleapis.com/maps/api/place/photo?'
                                . 'key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q'
                                . '&maxwidth=1600'
                                . '&photoreference=' . $pp->photo_reference);

                        $sha1 = sha1($file_contents);
                        $md5 = md5($file_contents);
                        $size = strlen($file_contents);
                        $raw_photos[$i]['sha1'] = $sha1;
                        $raw_photos[$i]['md5'] = $md5;
                        $raw_photos[$i]['size'] = $size;
                        $raw_photos[$i]['contents'] = $file_contents;
                        $i++;

                        
                            $check_media_exists = \App\Models\ActivityMedia\Media::where('sha1', $sha1)
                                    ->where('md5', $md5)
                                    ->where('filesize', $size)
                                    ->get()
                                    ->count();
                            if ($check_media_exists == 0) {
                                $media_file = 'hotels/' . $h->provider_id . '/' . sha1(microtime()) . '.jpg';
                                \Illuminate\Support\Facades\Storage::disk('s3')->put($media_file, $file_contents, 'public');

                                
                                $media = new \App\Models\ActivityMedia\Media;
                                $media->url = $media_file;
                                $media->sha1 = $sha1;
                                $media->filesize = $size;
                                $media->md5 = $md5;
                                $media->html_attributions = join(",", $pp->html_attributions);
                                $media->save();
                                if ($media->save()) {
                                    $place_media = new \App\Models\Hotels\HotelsMedias;
                                    $place_media->hotels_id = $h->id;
                                    $place_media->medias_id = $media->id;
                                    $place_media->save();
                                    
                                    
                                    $complete_url = $media->url;
                                    $url = explode("/", $complete_url);
                                    //$data = file_get_contents('https://s3.amazonaws.com/travooo-images2/'.$complete_url);
                                    //$s3 = App::make('aws')->createClient('s3');
                                
                                    $credentials = new Credentials('AKIAJ3AVI5LZTTRH42VA', 'S0UoecYnugvd/cVhYT7spHWZe8OBMyIJHQWL0n4z');

                                    $options = [
                                        'region' => 'us-east-1',
                                        'version' => 'latest',
                                        'http' => ['verify' => false],
                                        'credentials' => $credentials,
                                        'endpoint' => 'https://s3.amazonaws.com'
                                    ];

                                    $s3Client = new S3Client($options);
                                    //$s3 = AWS::createClient('s3');
                                    $result = $s3Client->getObject([
                                        'Bucket' => 'travooo-images2', // REQUIRED
                                        'Key' => $complete_url, // REQUIRED
                                        'ResponseContentType' => 'text/plain',
                                    ]);

                                    $img_1100 = Image::make($result['Body'])->resize(1100, null, function ($constraint) {
                                        $constraint->aspectRatio();
                                    });
                                    $img_700 = Image::make($result['Body'])->resize(700, null, function ($constraint) {
                                        $constraint->aspectRatio();
                                    });
                                    $img_230 = Image::make($result['Body'])->resize(230, null, function ($constraint) {
                                        $constraint->aspectRatio();
                                    });
                                    $img_180 = Image::make($result['Body'])->resize(180, null, function ($constraint) {
                                        $constraint->aspectRatio();
                                    });

                                    //return $img_700->response('jpg');
                                    //echo $complete_url . '<br />';
                                    //echo 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2];

                                    $put_1100 = $s3Client->putObject([
                                        'ACL' => 'public-read',
                                        'Body' => $img_1100->encode(),
                                        'Bucket' => 'travooo-images2',
                                        'Key' => 'th1100/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                    ]);

                                    $put_700 = $s3Client->putObject([
                                        'ACL' => 'public-read',
                                        'Body' => $img_700->encode(),
                                        'Bucket' => 'travooo-images2',
                                        'Key' => 'th700/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                    ]);

                                    $put_230 = $s3Client->putObject([
                                        'ACL' => 'public-read',
                                        'Body' => $img_230->encode(),
                                        'Bucket' => 'travooo-images2',
                                        'Key' => 'th230/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                    ]);

                                    $put_180 = $s3Client->putObject([
                                        'ACL' => 'public-read',
                                        'Body' => $img_180->encode(),
                                        'Bucket' => 'travooo-images2',
                                        'Key' => 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2],
                                    ]);


                                    $media->thumbs_done = 1;
                                    $media->save();
                                }
                            }
                        
                    }
                    
                    if(isset($put_180)) return 'th180/' . $url[0] . '/' . $url[1] . '/' . $url[2];

                    //return serialize($raw_photos);
                    //return $raw_photos;
                }
            }
        }
    }

}
