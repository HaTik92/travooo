<?php

namespace App\Http\Controllers;

use App\Models\User\User;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\ErrorsHandler\ErrorsHandlerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ErrorsHandlerController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Get Access Denied Page.
     * @param ErrorsHandlerService $errorsHandlerService
     * @return View
     */
    public function getAccessDeniedPage(ErrorsHandlerService $errorsHandlerService)
    {
        $user_lists = $errorsHandlerService->getUsersList();

        return view('errors.access-denied', ['user_lists' => $user_lists]);
    }

    /**
     * Get Account Deleted Page.
     * @param ErrorsHandlerService $errorsHandlerService
     * @return View
     */
    public function getAccountDeletedPage(ErrorsHandlerService $errorsHandlerService)
    {
        $content_list = $errorsHandlerService->getUsersList();

        return view('errors.account-deleted', ['content_list' => $content_list]);
    }



    /**
     * Get Private Trip Plan Page.
     * @param ErrorsHandlerService $errorsHandlerService
     * @return View
     */
    public function getPrivateTripPlanPage(ErrorsHandlerService $errorsHandlerService)
    {
        $plans_list = $errorsHandlerService->getPlanList();

        return view('errors.private-trip-plan', ['plans_list' => $plans_list]);
    }

    /**
     * Get Plan Draft Access Denied Page.
     * @param ErrorsHandlerService $errorsHandlerService
     * @return View
     */
    public function getPlanAccessDeniedPage(ErrorsHandlerService $errorsHandlerService)
    {
        $plans_list = $errorsHandlerService->getPlanList();

        return view('errors.draft-access-denied', ['content_list' => $plans_list, 'type' => 'plan']);
    }


    /**
     * Get Travelog Draft Access Denied Page.
     * @param ErrorsHandlerService $errorsHandlerService
     * @return View
     */
    public function getRepertAccessDeniedPage(ErrorsHandlerService $errorsHandlerService)
    {
        $report_list = $errorsHandlerService->getReportList();

        return view('errors.draft-access-denied', ['content_list' => $report_list, 'type' => 'report']);
    }

    /**
     * Get Permission Denied Page.
     * @param ErrorsHandlerService $errorsHandlerService
     * @return View
     */
    public function getPermissionDeniedPage(ErrorsHandlerService $errorsHandlerService)
    {
        $mixed_list = $errorsHandlerService->getAllContentList();

        return view('errors.permission-denied', ['content_list' => $mixed_list['entity'], 'type' => $mixed_list['type']]);
    }

    /**
     * Get Content Deleted Page.
     * @param ErrorsHandlerService $errorsHandlerService
     * @return View
     */
    public function getContentDeletedPage(ErrorsHandlerService $errorsHandlerService)
    {
        $mixed_list = $errorsHandlerService->getAllContentList();

        return view('errors.content-deleted', ['content_list' => $mixed_list['entity'], 'type' => $mixed_list['type']]);
    }
}
