<?php

namespace App\Http\Controllers;

use App\Http\Responses\AjaxResponse;
use App\Models\ActivityLog\ActivityLog;
use App\Models\TripPlans\TripsMediasShares;
use App\Models\TripPlans\TripsPlacesShares;
use App\Models\TripPlans\TripsShares;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class TripShareController extends Controller
{
    public function sharePlan(Request $request)
    {
        $request->validate([
            'plan_id' => 'required|numeric|exists:trips,id',
            'comment' => 'string|nullable'
        ]);

        $planId = $request->get('plan_id');
        $comment = $request->get('comment') ?? '';
        $userId = auth()->id();

        if (!$userId) {
            throw new AuthenticationException();
        }

        $planShare = new TripsShares();
        $planShare->user_id = $userId;
        $planShare->comment = $comment;
        $planShare->trip_id = $planId;
        $planShare->created_at = now();

        if ($planShare->save()) {
            log_user_activity('share', 'plan_shared', $planShare->id);
        }

        return AjaxResponse::create();
    }

    public function shareMedia(Request $request)
    {
        $request->validate([
            'trip_media_id' => 'required|numeric|exists:trips_medias,id',
            'comment' => 'string|nullable'
        ]);

        $tripMediaId = $request->get('trip_media_id');
        $comment = $request->get('comment') ?? '';
        $userId = auth()->id();

        if (!$userId) {
            throw new AuthenticationException();
        }

        $tripMediaShare = new TripsMediasShares();
        $tripMediaShare->user_id = $userId;
        $tripMediaShare->comment = $comment;
        $tripMediaShare->trip_media_id = $tripMediaId;

        if ($tripMediaShare->save()) {
            log_user_activity('share', 'plan_media_shared', $tripMediaShare->id);
        }

        return AjaxResponse::create();
    }

    public function sharePlace(Request $request)
    {
        $request->validate([
            'trip_place_id' => 'required|numeric|exists:trips_places,id',
            'comment' => 'string|nullable'
        ]);

        $tripPlaceId = $request->get('trip_place_id');
        $comment = $request->get('comment') ?? '';
        $userId = auth()->id();

        if (!$userId) {
            throw new AuthenticationException();
        }

        $tripPlaceShare = new TripsPlacesShares();
        $tripPlaceShare->user_id = $userId;
        $tripPlaceShare->comment = $comment;
        $tripPlaceShare->trip_place_id = $tripPlaceId;

        if ($tripPlaceShare->save()) {
            log_user_activity('share', 'plan_step_shared', $tripPlaceShare->id);
        }

        return AjaxResponse::create();
    }
}
