<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Help\HelpCenter;
use App\Models\Help\HelpCenterContent;
use App\Models\Help\HelpCenterSubMenu;
use App\Models\Help\HelpCenterLIkeDislike;
use HelpCenterSubMenues;
use Illuminate\Support\Facades\Input;

class HelpCenterController extends Controller
{
    
    function index () {
        $menues = HelpCenter::with('subMenues')->get();
        return view("site.help.index", ["menues" => $menues]);
    }

    public function likeDislike(Request $request, $id) {
        $type = $request->type;
        $sub_menu_id  = $id;
        $data = HelpCenterLIkeDislike::where([["user_id", auth()->user()->id], ["sub_menu_id", $sub_menu_id]])->first();
        if (!isset($data)) {
            $data = new HelpCenterLIkeDislike();
            $data->user_id = auth()->user()->id;
            $data->sub_menu_id = $sub_menu_id;
            $data->save();
        }

        if($type == 'like') {
            $data->update([
                "like" => !$data->like,
                "dislike" => false
            ]);
        } else if ($type == 'dislike') {
            $data->update([
                "dislike" => !$data->dislike,
                "like" => false
            ]);
        } else {
            return "Error";
        }

        return HelpCenterSubMenu::find($sub_menu_id)->LikesDislikesCount();
    }

    public function helpSearch() {
        $request = Input::get('q');
        $searchResult = HelpCenterSubMenu::whereRaw('(title like "%' . $request . '%")')->get()->groupBy("category_id")->map(function($grouped, $key) {
            return [
                "category_id" => $key,
                "menuName" => $grouped[0]->category->name,
                "iconURL" => url("assets2/image/icons/".strtolower(explode(" ", $grouped[0]->category->name)[0]).".svg"),
                "result" => $grouped->each(function($subMenu) {
                    $subMenu->content;
                    unset($subMenu->category);
                }),
            ];
        })->toArray();
        return response()->json(array_values($searchResult), 200);
    }
}
