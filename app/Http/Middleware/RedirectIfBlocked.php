<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request->user_id)) {
            if (user_is_blocked($request->user_id)) {
                return redirect()->route('user_is_blocked', ['message' => 'Sorry, this account has been suspended.']);
            }
        } elseif(isset($request->id)) {
            if (user_is_blocked($request->id)) {
                return redirect()->route('user_is_blocked', ['message' => 'Sorry, this account has been suspended.']);
            }
        }

        return $next($request);
    }
}
