<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;

class SessionCount
{
    /**
     * @var Store
     */
    protected $session;

    /**
     * @var int
     */
    protected $period;

    /**
     * @param Store $session
     */
    public function __construct(Store $session)
    {
        $this->session = $session;
        $this->period = config('session.period');
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authUserId = auth()->id();

        if ($authUserId) {
            $currentTime = time();
            $lastActivityTime = session('lastActivityTime');
            if (user_is_blocked($authUserId)) {
                clear_user_sessions($authUserId);
                return redirect()->route('user_is_blocked',['message' => 'Sorry, your account has been suspended.']);
            }

            if ($lastActivityTime && ($currentTime - $lastActivityTime) > $this->period) {
                log_user_activity('User', 'session_start', $authUserId);
            }

            $this->session->put('lastActivityTime', time());
        }

        return $next($request);
    }
}
