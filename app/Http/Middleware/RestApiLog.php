<?php

namespace App\Http\Middleware;

use Closure;

class RestApiLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $endTime = microtime(true);
        \Log::useDailyFiles(storage_path() . '/logs/laravel.log');
        \Log::info(json_encode([
            'responseTime' => number_format($endTime - LARAVEL_START, 3),
            'method'    => $request->method(),
            'url'       => $request->fullUrl(),
            'header'    => getallheaders(),
            'request'   => $request->all(),
            'response'  => $response->original
        ], JSON_PRETTY_PRINT));
    }
}
