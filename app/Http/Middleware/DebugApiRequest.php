<?php

namespace App\Http\Middleware;

use Closure;

class DebugApiRequest
{
    public function handle($request, Closure $next, $module)
    {
        _ApiRequest($module, '', []);
        return $next($request);
    }
}
