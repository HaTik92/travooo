<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HandleLink
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isMobileDevice = (isLiveSite() && isMobileDevice());
        $this->addParamToRequest($request, $isMobileDevice);
        return $next($request);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return Request
     */
    protected function addParamToRequest(Request &$request, $isMobileDevice)
    {
        $input = $request->input();
        $input['isMobileDevice'] = $isMobileDevice;
        $request->replace($input);
        return $request;
    }
}
