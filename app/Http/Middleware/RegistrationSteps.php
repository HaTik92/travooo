<?php

namespace App\Http\Middleware;

use App\Http\Responses\ApiResponse;
use App\Services\Users\RegistrationService;
use Carbon\Carbon;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class RegistrationSteps
{
    /**
     * @var RegistrationService
     */
    protected $registrationService;

    public function __construct(RegistrationService $registrationService)
    {
        $this->registrationService = $registrationService;
    }

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->get('token');
        if (strpos(url()->current(), '/api/v1/')) {
            if (!is_string($token)) return ApiResponse::createValidationResponse(['token' => ["token type is invliad"]]);
            if ($request->has('token') && $request->token == "undefined") return ApiResponse::createValidationResponse(['token' => ["undefined is not valid token"]]);
            if ($request->has('search') && $request->search == "undefined") return ApiResponse::createValidationResponse(['search' => ["undefined is not valid search value"]]);
            if ($request->has('user_id')) return ApiResponse::createValidationResponse(['user_id' => ["user_id not required in this api."]]);
        }

        $token = $this->registrationService->getRegistrationToken($token);

        if (!$token || !$token->getUserId() || $token->getExpiredDate() < Carbon::create()) {
            throw new AuthenticationException();
        }

        $this->addUserIdParamToRequest($request, $token->getUserId());

        return $next($request);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return Request
     */
    protected function addUserIdParamToRequest(Request &$request, $userId)
    {
        $input = $request->input();
        $input['user_id'] = $userId;
        $request->replace($input);

        return $request;
    }
}
