<?php

namespace App\Http\Middleware;

use App\Http\Responses\ApiResponse;
use Closure;
use Illuminate\Support\Facades\Auth;

class ExpertDashbaordMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $user = Auth::user();
            if (($user->type == 1 && $user->expert == 0) || ($user->type == 2 && $user->expert == 0 && $user->is_approved === 0)) {
                return ApiResponse::__createBadResponse("your account is not an expert account, only expert user can access dashbaord module, please switch your account and then check dashbaord module.");
            } else {
                if (Auth::user()->is_approved === 1) {
                    return $next($request);
                } else {
                    return ApiResponse::__createBadResponse("You have already submitted your application, and it's under review. The Support Team will get back to you soon.");
                }
            }
        }
        return ApiResponse::__createUnAuthorizedResponse();
    }
}
