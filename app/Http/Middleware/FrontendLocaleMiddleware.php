<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class FrontendLocaleMiddleware
{

    public static function getLocale()
    {
        $uri = \Request::path(); //получаем URI


        $segmentsURI = explode('/', $uri); //делим на части по разделителю "/"


        //Проверяем метку языка  - есть ли она среди доступных языков
        if (!empty($segmentsURI[0]) && in_array($segmentsURI[0], ['ar', 'fr', 'en'])) {

            if ($segmentsURI[0] != config('locale.frontend_main_language')) return $segmentsURI[0];

        }
        return null;
    }

    public static function setLocale($locale)
    {
        app()->setLocale($locale);

        /*
         * setLocale for php. Enables ->formatLocalized() with localized values for dates
         */
        setlocale(LC_TIME, config('locale.languages')[$locale][1]);

        /*
         * setLocale to use Carbon source locales. Enables diffForHumans() localized
         */
        Carbon::setLocale(config('locale.languages')[$locale][0]);

        /*
         * Set the session variable for whether or not the app is using RTL support
         * for the current language being selected
         * For use in the blade directive in BladeServiceProvider
         */
        if (config('locale.languages')[$locale][2]) {
            session(['lang-rtl' => true]);
        } else {
            session()->forget('lang-rtl');
        }
    }

    public function handle($request, Closure $next)
    {
        $locale = self::getLocale();

        if ($locale) {
            self::setLocale($locale);
        } else {
            self::setLocale(config('locale.frontend_main_language'));
        }

        return $next($request);
    }

}
