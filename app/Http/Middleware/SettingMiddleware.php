<?php

namespace App\Http\Middleware;

use App\Http\Responses\ApiResponse;
use App\Models\User\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class SettingMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $user = User::find($request->route('user_id'));
            if ($user) {
                if (Auth::user()->id == $user->id) {
                    return $next($request);
                }
                return ApiResponse::__createBadResponse("You can not change anything in " . ($user->username != "" ? $user->username : 'other') . " setting.");
            }
            return ApiResponse::__createBadResponse("user not found.");
        }
        return ApiResponse::__createUnAuthorizedResponse();
    }
}
