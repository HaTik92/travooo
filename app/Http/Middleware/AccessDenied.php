<?php

namespace App\Http\Middleware;

use Closure;

class AccessDenied
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request->id)) {
            if (user_access_denied($request->id, true)) {
                return redirect('access-denied');
            }
        } 

        return $next($request);
    }
}
