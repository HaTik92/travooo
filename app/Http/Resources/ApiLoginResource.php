<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class ApiLoginResource extends Resource
{
    public function toArray($request)
    {
        $this['user']->profile_picture = check_profile_picture($this['user']->profile_picture);
        unset($this['user']->confirmation_code, $this['user']->password_reset_token);
        return [
            'user'          => $this['user'],
            'access_token'  => $this['token']->accessToken,
            'token_type'    => 'Bearer',
            'expires_at'    => Carbon::parse(
                $this['token']->token->expires_at
            )->toDateTimeString(),
            'message'       => isset($this['msg']) ? $this['msg'] : "User Logged In"
        ];
    }
}
