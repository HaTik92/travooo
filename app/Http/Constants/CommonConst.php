<?php

namespace App\Http\Constants;

use App\Models\Discussion\Discussion;
use App\Models\Posts\Posts;
use App\Models\Reports\Reports;
use App\Models\TripPlans\TripPlans;

class CommonConst
{

    const OAUTH_CLIENT = "TRAVOOO";

    static function isTestMode()
    {
        return (auth()->check() && auth()->user()->id == 1969);
    }

    // Country, City, Place Tabs ['top-posts', 'new-posts', 'discussions', 'trip-plans', 'reports', 'reviews', 'events' , 'medias']
    const CCP_TAB_DISCUSSIONS   = 'discussions';
    const CCP_TAB_REPORTS       = 'reports';
    const CCP_TAB_REVIEWS       = 'reviews';
    const CCP_TAB_NEW_POSTS     = 'new-posts';
    const CCP_TAB_TOP_POSTS     = 'top-posts';
    const CCP_TAB_TRIP_PLANS    = 'trip-plans';
    const CCP_TAB_EVENTS        = 'events';
    const CCP_TAB_MEDIAS        = 'medias';

    const CCP_TAB_MAPPING = [
        self::CCP_TAB_DISCUSSIONS,
        self::CCP_TAB_REPORTS,
        self::CCP_TAB_REVIEWS,
        self::CCP_TAB_NEW_POSTS,
        self::CCP_TAB_TOP_POSTS,
        self::CCP_TAB_TRIP_PLANS,
        self::CCP_TAB_EVENTS,
        self::CCP_TAB_MEDIAS,
    ];

    // Post Checkin
    const POST_CHECKIN_COUNTRY = "country";
    const POST_CHECKIN_CITY    = "city";
    const POST_CHECKIN_PLACE   = "place";

    // Search Checkin
    const SEARCH_COUNTRY       = "country";
    const SEARCH_CITY          = "city";
    const SEARCH_PLACE         = "place";
    const SEARCH_USER          = "user";

    // Post Types
    const TYPE_POST            = "post";
    const TYPE_TRIP            = "trip";
    const TYPE_REPORT          = "report";
    const TYPE_DISCUSSION      = "discussion";

    // Post Actions
    const ACTION_LIKE          = "like";
    const ACTION_UNLIKE        = "unlike";
    const ACTION_SHARE         = "share";
    const ACTION_UNSHARE       = "unshare";
    const ACTION_COMMENT       = "comment";
    const ACTION_UNCOMMENT     = "uncomment";
    const ACTION_VIEW          = "view";
    const ACTION_REPLY         = "reply";
    const ACTION_DELETE_REPLY  = "delete_reply";

    public static function postModels()
    {
        return [
            CommonConst::TYPE_POST => Posts::class,
            CommonConst::TYPE_REPORT => Reports::class,
            CommonConst::TYPE_TRIP => TripPlans::class,
            CommonConst::TYPE_DISCUSSION => Discussion::class
        ];
    }

    public static function postTypes($discussion = false)
    {
        $typesArray = [
            CommonConst::TYPE_POST,
            CommonConst::TYPE_REPORT,
            CommonConst::TYPE_TRIP,
        ];

        if ($discussion) {
            array_push($typesArray, CommonConst::TYPE_DISCUSSION);
        }

        return $typesArray;
    }
}
