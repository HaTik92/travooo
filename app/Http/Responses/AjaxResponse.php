<?php

namespace App\Http\Responses;

use Illuminate\Http\Response;

class AjaxResponse extends Response
{
    /**
     * AjaxResponse constructor.
     * @param bool $success
     * @param array $data
     * @param int $status
     * @param array $headers
     */
    public function __construct($data = [], $success = true, $status = 200, array $headers = [])
    {
        $content = $data;

        if ($status === 200) {
            $content = $this->generateContent($success, $data);
        }

        parent::__construct($content, $status, $headers);
    }

    /**
     * @param bool $success
     * @param array $data
     * @param int $status
     * @param array $headers
     *
     * @return Response
     */
    public static function create($data = [], $success = true, $status = 200, array $headers = [])
    {
        return new static($data, $success, $status, $headers);
    }

    /**
     * @param bool $success
     * @param mixed $data
     *
     * @return array
     */
    protected function generateContent($success, $data)
    {
        return [
            'status' => $success ? 'success' : 'fail',
            'data' => $data
        ];
    }
}
