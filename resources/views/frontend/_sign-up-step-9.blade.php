<div class="modal fade sign-up search res-scroll" id="createAccount9" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style w-628" role="document">
        <div class="modal-content final-m-content">
            <div class="modal-body border--helper">
                <div class="header">
                    <img src="{{asset('frontend_assets/image/reg-expert-cover.png')}}" class='succes-image'>
                </div>
                <div class="description">
                    <h1 class="desc-title">Congratulations!</h1>
                    <div class="content">You are a Travooo Expert now and you can use all the tools and features provided to you to make the most of your membership.</div>
                    <div class="continue">Get Started <i class="fa fa-long-arrow-right" aria-hidden="true"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>