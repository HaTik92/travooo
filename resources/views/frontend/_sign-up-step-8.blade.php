<div class="modal fade sign-up search pt-70" id="createAccount8" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body body-create">
                <div class="header">
                    <h1>{{__('Level Up!')}}</h1>
                </div>
                <div class="description">
                    {{__('You can star earning badges by leveling up in diffrent arear, bellow you can find all the badges and how you can earn them and level up.')}}
                </div>
            </div>
            <div class="modal-footer" >
                <div class="sup-badges">
                    <div class="sup-badge">
                        <div class="img">
                            <img src="{{asset('assets2/image/sign_up/badge_5.png')}}" alt="">
                        </div>
                        <div class="badge-body">
                            <div class="title">{{__('Socializer Badge:')}}</div>
                            <div class="desc">{{__('Points for this badge will be granted if a person performs actions
such as liking, commenting, sharing, adding people, etc.')}}</div>
                        </div>
                        <div class="arrow">
                            <img src="{{asset('assets2/image/sign_up/arrow-icon.png')}}" alt="">
                        </div>
                    </div>
                    <div class="sup-badge">
                        <div class="img">
                            <img src="{{asset('assets2/image/sign_up/badge_2.png')}}" alt="">
                        </div>
                        <div class="badge-body">
                            <div class="title">{{__('Explorer Badge:')}}</div>
                            <div class="desc">{{__('Points for this badge will be granted when a person checks in, or creates trip plans for a specific place.')}}</div>
                        </div>
                        <div class="arrow">
                            <img src="{{asset('assets2/image/sign_up/arrow-icon.png')}}" alt="">
                        </div>
                    </div>
                    <div class="sup-badge">
                        <div class="img">
                            <img src="{{asset('assets2/image/sign_up/badge_1.png')}}" alt="">
                        </div>
                        <div class="badge-body">
                            <div class="title">{{__('Philanthropist Badge:')}}</div>
                            <div class="desc">{{__('Points for this badge will be granted by making contribution to Travooo’s content such as adding information on pages, adding
media, reporting errors, reviewing places, etc.')}}</div>
                        </div>
                        <div class="arrow">
                            <img src="{{asset('assets2/image/sign_up/arrow-icon.png')}}" alt="">
                        </div>
                    </div>
                    <div class="sup-badge">
                        <div class="img">
                            <img src="{{asset('assets2/image/sign_up/badge_4.png')}}" alt="">
                        </div>
                        <div class="badge-body">
                            <div class="title">{{__('Advisor Badge:')}}</div>
                            <div class="desc">{{__('Points for this badge will be granted for answering questions in the discussion panel, and generally giving out tips.')}}</div>
                        </div>
                        <div class="arrow">
                            <img src="{{asset('assets2/image/sign_up/arrow-icon.png')}}" alt="">
                        </div>
                    </div>
                    <div class="sup-badge">
                        <div class="img">
                            <img src="{{asset('assets2/image/sign_up/badge_3.png')}}" alt="">
                        </div>
                        <div class="badge-body">
                            <div class="title">{{__('Personality Badge:')}}</div>
                            <div class="desc">{{__('Points for this badge will be given out according to the popularity
of a person. Gaining more followers and friends will contribute to
the level of this badge.')}}</div>
                        </div>
                        <div class="arrow">
                            <img src="{{asset('assets2/image/sign_up/arrow-icon.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="sup-badge-details" id="badge-1" style="display: none">
                <div class="breadcrumbs">
                    <div class="breadcrumbs-back"><img src="{{asset('assets2/image/sign_up/arrow-back.png')}}"></div>
                    <div class="path">All Badges / <span>Socializer Badge</span></div>
                </div>
                <div class="sup-badge-header">
                    <div class="img"><img src="{{asset('assets2/image/badges/5_6.png')}}"></div>
                    <div class="level">
                        <div class="title">Level 6/10</div>
                        <div class="desc">87 users like you have this badge level</div>
                    </div>
                </div>
                <div class="pb"><div class="pb-wrapper"><div class="progress-bar"></div></div><div class="count">75 / 150</div></div>
                <div class="body">
                    <div class="title">{{__('How to level up?')}}</div>
                    <div class="desc">{{__('To level up your Socializer badge you will need to collect points by performing different actions like writing helpful comments and reviews and receiving likes on your posts.')}}</div>
                    <div class="desc-table">
                        <div class="header-row">
                            <div>Action</div>
                            <div>Granted Points</div>
                        </div>
                        <div class="row">
                            <div>1 Written Review</div>
                            <div>10 Points</div>
                        </div>
                        <div class="row">
                            <div>1 Helpful Comment</div>
                            <div>5 Points</div>
                        </div>
                        <div class="row">
                            <div>100 Received Likes</div>
                            <div>20 Points</div>
                        </div>
                        <div class="row">
                            <div>1 Posted Media</div>
                            <div>1 Points</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('div.sup-badge').on('click', function() {
            $('#badge-1').show();

            $('#createAccount8 .modal-body div.description').hide();
            $('#createAccount8 .modal-footer').hide();
        });

        $('#createAccount8 div.breadcrumbs-back').on('click', function() {
            $('#badge-1').hide();

            $('#createAccount8 .modal-body div.description').show();
            $('#createAccount8 .modal-footer').show();
        });

    });
</script>























