<div class="modal fade sign-up responsive-modal res-scroll" id="createAccount2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
   <div class="">

   </div>
    <div class="modal-dialog sign-up-style" role="document">
        <button type="button" class="close-signup-btn">
            <i class="trav-close-icon"></i>
        </button>
        <div class="modal-content step-content">
            <div class="modal-body body-create">
                <div class="top-layer">
                        <p class="login-title dynamic-top-layer">{{ __('Create a free account') }}</p>
                        <p class="login-descripton">{{ __('Get started now in few steps.') }}</p>
                    <div class="error-messages"></div>
                </div>
                 <form class="login-form" method="post" action="" id="step2_form">
                    <div class="step-2">
                       <div class="form-group">
                            <input type="email" class="form-control mobile--input" id="email" autocomplete="off" name="email" aria-describedby="emailHelp" placeholder="{{ __('Email address')}}" required>
                        </div>
                        <div class="form-group uname-group">
                            <input type="text" class="form-control mobile--input" id="username" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" name="username" placeholder="{{ __('Username')}}" required>
                            <input type="hidden" class="form-control mobile--input" name="is_back" id="is_back"  value="1">
                            <div class="uname-info"></div>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control mobile--input" id="password" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false"  name="password" aria-describedby="pass" placeholder="{{ __('Password')}}" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control mobile--input" id="confirmation_password" name="confirmation_password" aria-describedby="pass" placeholder="{{ __('Repeat Password')}}" required>
                        </div>
                            <div class="form-group flex-custom">
                                <div class="flex-input w-100">
                                    <input type="text" class="form-control mobile--input" id="name" name="name" aria-describedby="full name" placeholder="{{ __('Full Name')}}"></div>
                                <div class="flex-input-right position-relative">
                                    <input type="text" class="form-control mobile--input" id="dob" name="age" placeholder="{{ __('Age')}}" required readonly>
                                    <span class="input-input__arrow" dir="auto"><b dir="auto"></b></span>
                                </div>
                        </div>
                        <div class="form-group">
                            <select class="custom-select" name="nationality" id="nationality" onchange="changeNationlity(this)">
                                <option selected value="">{{ __('Nationality')}}</option>
                                @php
                                    $countries = \App\Models\Country\Countries::with('trans')->get()->pluck('trans.0.title', 'id')->toArray();
                                    asort($countries);
                                @endphp
                                @foreach($countries as $key => $val)
                                    <option value="{{$key}}">{{$val}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-check flex-custom mobile--gender">
                            <div class="custom-check-label">
                                <input type="radio" class="custom-check-input gender mobile--input"  name="gender" id="male" value="1">
                                <label for="male">{{ __('Male')}}</label>
                            </div>
                            <div class="custom-check-label">
                                <input type="radio" class="custom-check-input gender mobile--input" name="gender" id="female" value="2">
                                <label for="female">{{ __('Female')}}</label>
                            </div>
                        </div>

                         @if(config('captcha.key'))
                            <div class="g-recaptcha form-group"
                                 data-sitekey="{{config('captcha.key')}}">
                            </div>
                        @endif
                        
                        <div class="form-group continue-wrapper mt-2">
                            <button type="button" class="btn createAccount2_continue continue">
                                <span class="spinner-border spinner-border-sm d-none createAccount1_spiner" role="status" aria-hidden="true"></span>  {{ __('Sign Up')}}</button>
                        </div>
                        
                        <div class="form-group">
                            <div class="signup-back-btn back-btn">
                                <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back
                            </div>
                        </div>
                      
                        <div class="form-group bottom-txt-group mobile--border">
                            <p class="bottom-txt reg-bottom-text">{{ __('Creating an account means you\'re okay with')}}</p>
                            <ul class="link-list reg-term-text">
                                <li><b>{{ __('Travooo\'s')}}</b></li>
                                <li><a href="{{route('page.terms_of_service')}}"> {{ __('Terms of Service')}}</a>,</li>
                                <li><a href="{{route('page.privacy_policy')}}"> {{ __('Privacy Policy')}}</a></li>
                            </ul>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <div>
                        <span class="foot-txt">{{ __('Already a member?')}}</span> <span class="signup-button go_log_in">{{ __('Log In')}}</span>
                    </div>
                    <div class="exp-signup-button signup-type-btn dynamic-bottom-block" data-type="expert">
                        <span class="exp-text-title">Create an</span>
                        <span class="exp-text-desc">Expert Account</span>
                        <span class="exp-icon">EXP</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src='https://www.google.com/recaptcha/api.js' async defer></script>
<script>
    var current_year = {{date('Y')}};
    var max_date = {{date('Y')}} - 14;
    var min_date = {{date('Y')}} - 120;
    $(document).ready(function() {
        $('#dob').datepicker({
            yearRange: min_date + ":" + current_year,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            maxDate: max_date + "-12-31",
            altField: "#dob",
            altFormat: "yy-mm-dd"
        }).on('change', function() {
            $(this).valid();  // triggers the validation
        });
    });

    function changeNationlity(e){
        if(e.value != ''){
            e.style.color = "#495057";
        }else{
            e.style.color = "#b3b3b3";
        }
    }
</script>
