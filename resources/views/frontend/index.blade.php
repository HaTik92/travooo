@extends('frontend.layouts.access')

@section('access_content')
<div class="main-wrapper login-layer">
    <!-- Button trigger modal -->
    @include('frontend.layouts._header')
@php session()->forget('login_attempts');   @endphp
</div>
 
<!-- Modal -->

<div class="modal fade responsive-modal res-scroll" id="signUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content common-content">

            <div class="modal-body">
                <div class="top-layer">
                    <p class="login-title">{{ __('Welcome to Travooo') }}</p>
                    <p class="login-descripton">{{ __('Login to your account to get started') }}</p>
                    <div class="signUp_error_message">
                        @if (\Session::has('error'))
                            <p class="sub-ttl error-message">{!! \Session::get('error') !!}</p>
                        @endif
                    </div>
                </div>
                <form class="login-form" id="signUpForm">
                    <div class="form-group">
                        <input type="text" class="mobile--input form-control" id="email" name="email" required value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="{{ __('Username or Email address')}}" >
                    </div>
                    <div class="form-group">
                        <input type="password" class="mobile--input form-control" id="password" required name="password" aria-describedby="pass" placeholder="{{ __('Password') }}">
                    </div>
                    <div class="form-group">
                        <a href="{{url('password/email')}}" class="forget-password-link">{{ __('Forget your password?') }}</a>
                    </div>
                    @if(config('captcha.key'))
                        <div class="g-recaptcha form-group login-recaptcha d-none"
                             data-sitekey="{{config('captcha.key')}}">
                        </div>
                    @endif
                    <div class="form-group">
                        <button type="button" id="log_in_btn" class="btn login-access-btn">
                            <span class="spinner-border spinner-border-sm d-none signIn-spiner" role="status" aria-hidden="true"></span> {{ __('Log In') }}</button>
                    </div>
                    <div class="form-group">
                        <p class="simple-txt">OR</p>
                    </div>
                    <div class="form-group">
                        <button type="button" onclick="window.location.href='{{ url("/facebook/redirect") }}'" class="btn btn-social-button facebook_login"><img class="social-icon" src="{{asset('frontend_assets/image/facebook-icon.png')}}">{{ __('Login with Facebook')}}</button>
                    </div>
                    <div class="form-group">
                        <button type="button"  onclick="window.location.href='{{url("/twitter/redirect")}}'"  class="btn btn-social-button twitter_login"><img class="social-icon" src="{{asset('frontend_assets/image/twitter-icon.png')}}">{{ __('Login with Twitter')}}</button>
                    </div>
                    <div class="form-group">
                        <button type="button"  onclick="window.location.href='{{url("/social/google")}}'"  class="btn btn-social-button google_login"><img class="social-icon" src="{{asset('frontend_assets/image/google-icon.png')}}" width="28">{{ __('Login with Google')}}</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div>
                    <span class="foot-txt">{{ __('You are not a member yet?')}}</span> <span class="signup-button go_sign_up" data-type="regular">{{ __('Sign Up')}}</span>
                </div>
                <div class="exp-signup-button go_sign_up" data-type="expert">
                    <span class="exp-text-title">Create an</span>
                    <span class="exp-text-desc">Expert Account</span>
                    <span class="exp-icon">EXP</span>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script src='https://www.google.com/recaptcha/api.js'></script>
@section('access_script')
<script>

    $(document).ready(function() {
        @if (session()->get('flash_success'))
            @if (is_array(json_decode(session()->get('flash_success'), true)))
                toastr.success("{!! implode('', session()->get('flash_success')->all(':message<br/>')) !!}");
            @else
                toastr.success("{!! session()->get('flash_success') !!}");
            @endif
        @endif

        $('#signUp').modal('toggle');

        $('.go_sign_up').on('click', function() {
            var type = $(this).attr('data-type')
           window.location.href="{{url('signup')}}?dt=" + type;
        });
        
        $('.res-scroll').scroll( function(){
            if($(this).scrollTop() > 50){
                $(this).css('z-index', '999999')
            }else{
                $(this).css('z-index', '1050')
            }
        });

        $(document).on('keyup', function(e){
            if(e.keyCode == 13 && $('#email').val() !='' && $('#password').val() != ''){
                 $("#log_in_btn").click()
            }
        });

        $('#signUpForm').validate({
            ignore:'',
            rules: {
                email: {
                    required: true,
                    username: true,
                    custom_email: true,
                },
                password: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                email: {
                    required: "Email or username is required.",
                    username: "Username should be at least 4 characters long.",
                    custom_email: "Please enter a valid email address.",
                },
                password: {
                    required: "Password is required.",
                }
            }
        });

        $.validator.addMethod("username", function (value) {
            if (value.indexOf("@") === -1 && value.length < 4) {
                return false;
            }

            return true;
        });

        $.validator.addMethod("custom_email", function(value) {
            if (value.indexOf("@") >= 0 ) {
                return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
            }

            return true;
        });

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $("#log_in_btn").click(function(){

            if (!$('#signUpForm').validate().form()) {
                return;
            }

            $(this).prop("disabled", true);
            $('.signIn-spiner').removeClass('d-none');

            var password = $('#password').val();
            var email = $('#email').val();
            var recaptcha = $('textarea[name=g-recaptcha-response]').val();
             
            $.ajax({
                url: "{{url('login')}}",
                type: 'POST',
                data: {_token: CSRF_TOKEN,password:password,email:email, recaptcha: recaptcha},
                dataType: 'JSON',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    window.location.href = "{{url('home')}}";
                },
                error: function (data) {
                    if (data.responseJSON) {
                        $('#log_in_btn').prop("disabled", false);
                        $('.signIn-spiner').addClass('d-none');
                        $('.signUp_error_message').empty();
                        if (data.responseJSON.errors && data.responseJSON.errors.email) {
                            $('#email').parent().addClass('has-danger');
                            $('#password').parent().addClass('has-danger');
                            $('.signUp_error_message').append('<p class="sub-ttl error-message email-err-message">' + data.responseJSON.errors.email + '</p>');
                           
                           if(data.responseJSON.session === 1){
                               secondCounter(data.responseJSON.seconds);
                               $('.login-recaptcha').removeClass('d-none');
                               grecaptcha.reset();
                           }

                           if(!$('.login-recaptcha').hasClass('d-none')){
                               grecaptcha.reset();
                           }
                        }
                        if (data.responseJSON.errors && data.responseJSON.errors.password) {
                            $('#password').parent().addClass('has-danger');
                            $('.signUp_error_message').append('<p class="sub-ttl error-message password-err-message">' + data.responseJSON.errors.password[0] + '</p>');
                        }

                        if (data.responseJSON.errors && data.responseJSON.errors.recaptcha) {
                            $('#password').parent().addClass('has-danger');
                            $('.signUp_error_message').append('<p class="sub-ttl error-message recaptcha-err-message">The recaptcha field is required</p>');
                        }
                        
                        if (data.responseJSON.errors && data.responseJSON.errors.activation) {
                            $('.signUp_error_message').append('<p class="sub-ttl error-message activation-err-mesaage">' + data.responseJSON.errors.activation + '</p>');
                        }
                        
                        if (data.responseJSON.errors && data.responseJSON.errors.blocked) {
                            $('.signUp_error_message').append('<p class="sub-ttl error-message">' + data.responseJSON.errors.blocked + '</p>');
                        }

                        if (data.responseJSON.token !== undefined && data.responseJSON.step !== undefined) {
                            token = data.responseJSON.token;
                            var step = data.responseJSON.step;
                            
                            var type = (data.responseJSON.type !== null)?'&type='+data.responseJSON.type:'';
                            //todo check registration step
                           var url = '/signup?step='+step+''+type+'&t='+token;
                           
                           window.location.href = url;
                        }
                    } else {
                        window.location.reload();
                    }
                }
            });
        });

    });

    function secondCounter(seconds){
        setInterval(function() {
            seconds--;
            if (seconds >= 0) {
                $('.down-seconds').html(seconds)
            }

            if (seconds === 0) {
                clearInterval(seconds);
                $('.signUp_error_message').empty();
            }

        }, 1000);
    }
</script>
@endsection
</html>
