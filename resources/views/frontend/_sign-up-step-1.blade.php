<div class="modal fade sign-up responsive-modal res-scroll" id="createAccount1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style" role="document">
        <button type="button" class="close-signup-btn">
            <i class="trav-close-icon"></i>
        </button>
        <div class="modal-content step-content mobile--step">
            <div class="modal-body body-create">
                <div class="top-layer">
                    <p class="login-title">{{ __('Create a free account') }}</p>
                    <p class="login-descripton">{{ __('Get started now in few steps.') }}</p>
                </div>
                <form class="login-form" method="post" action="">
                    <div class="step-1">
                        <div class="form-group">
                            <button type="button"  class="btn login-access-btn createAccount1_continue continue">
                                <span class="spinner-border spinner-border-sm d-none createAccount1_spiner" role="status" aria-hidden="true"></span>{{ __('Sign up with Email') }}</button>
                        </div>
                        <div class="form-group">
                            <p class="simple-txt">OR</p>
                        </div>
                        <div class="form-group">
                            <button type="button" onclick="window.location.href ='{{ url("/facebook/redirect") }}'" class="btn btn-social-button facebook_login"><img class="social-icon" src="{{asset('frontend_assets/image/facebook-icon.png')}}"><span>{{ __('Sign up with Facebook')}}</span></button>
                        </div>
                        <div class="form-group">
                            <button type="button"  onclick="window.location.href ='{{url("/twitter/redirect")}}'"  class="btn btn-social-button twitter_login"><img class="social-icon" src="{{asset('frontend_assets/image/twitter-icon.png')}}"><span>{{ __('Sign up with Twitter')}}</span></button>
                        </div>
                        <div class="form-group">
                            <button type="button"  onclick="window.location.href='{{url("/social/google")}}'"  class="btn btn-social-button google_login"><img class="social-icon" src="{{asset('frontend_assets/image/google-icon.png')}}" width="28"><span>{{ __('Sign up with Google')}}</span></button>
                        </div>

                        <div class="form-group bottom-txt-group mobile--border">
                            <p class="bottom-txt reg-bottom-text">{{ __('Creating an account means you\'re okay with')}}</p>
                            <ul class="link-list reg-term-text">
                                <li><b>{{ __('Travooo\'s')}}</b></li>
                                <li><a href="{{route('page.terms_of_service')}}"> {{ __('Terms of Service')}}</a>,</li>
                                <li><a href="{{route('page.privacy_policy')}}"> {{ __('Privacy Policy')}}</a></li>
                            </ul>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <div>
                        <span class="foot-txt">{{ __('Already a member?')}}</span> <span class="signup-button go_log_in">{{ __('Log In')}}</span>
                    </div>
                    <div class="exp-signup-button signup-type-btn" data-type="expert">
                        <span class="exp-text-title">Create an</span>
                        <span class="exp-text-desc">Expert Account</span>
                        <span class="exp-icon">EXP</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
