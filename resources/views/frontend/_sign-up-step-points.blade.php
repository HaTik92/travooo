<div class="modal fade sign-up search step-3-mobile" id="createAccountPoints" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body body-create">
                <div class="header">
                    <img src="{{asset('assets2/image/sign_up/points.png')}}">
                </div>
                <div class="description">
                    <div class="content">You earned <span class="points">150 points</span> as a head start.</div>
                    <div class="content">You are a <span>Travooo Expert</span> now and you can use all the tools and features provided to you to make the most of your membership.</div>
                    <div class="continue">Continue the Signup Process</div>
                </div>
                <div class='footer--devider'></div>
            </div>
        </div>
    </div>
</div>




















