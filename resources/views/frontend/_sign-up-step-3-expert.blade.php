<div class="modal fade sign-up pt-70" id="createAccount3" tabindex="0" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content">
            <button type="button" class="close back-btn">
               <i class="fa fa-angle-left" aria-hidden="true"></i>
            </button>
            <div class="modal-body body-create">
                <div class="top-layer">
                    <a href="#" class="logo-wrap">
                        <img src="frontend_assets/image/main-logo.png" alt="">
                        <img src="frontend_assets/image/main-logo-white.png" class="mobile-main-logo" alt="">
                    </a>
                    <h4 class="title">{{ __('Create an Account')}}</h4>
                    <h5 class="title">{{ __('Personal details')}}</h5>
                    <div class="error-messages"></div>
                </div>
                <form class="login-form" method="post" action="" id="step3_form">
                    <div class="step-2">
                        <div class="form-group uname-group">
                            <input type="text" class="form-control" id="username" name="username" placeholder="{{ __('Username')}}" required>
                            <div class="uname-info"></div>
                        </div>

                        <div class="form-group flex-custom">
                            <input type="text" class="form-control flex-input" id="name" name="name" aria-describedby="full name" placeholder="{{ __('Full Name')}}">
                            <input type="text" class="form-control" id="dob" name="age" placeholder="{{ __('Date of birth')}}" required readonly>
                        </div>
                        <div class="form-group">
                            <select class="custom-select" name="nationality" id="nationality">
                                <option selected value="">{{ __('Nationality')}}</option>
                                @php
                                    $countries = \App\Models\Country\Countries::with('trans')->get()->pluck('trans.0.title', 'id')->toArray();
                                    asort($countries);
                                @endphp
                                @foreach($countries as $key => $val)
                                    <option value="{{$key}}">{{$val}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-check flex-custom">
                            <div class="custom-check-label">
                                <input type="radio" class="custom-check-input gender"  name="gender" id="male" value="1">
                                <label for="male">{{ __('Male')}}</label>
                            </div>
                            <div class="custom-check-label">
                                <input type="radio" class="custom-check-input gender" name="gender" id="female" value="2">
                                <label for="female">{{ __('Female')}}</label>
                            </div>
                        </div>
                        <div class="form-group flex-custom" id="expertiseContainer" style="display:none;">
                            <label for="expertiseSelect">
                                Your expertise Countries & Cities
                                <select class="custom-select form-control flex-input" name="expertise" id="expertiseSelect" multiple style="width:100%;">
                                </select>
                            </label>
                        </div>
                        <div class="form-group continue-wrapper">
                            <button type="button" class="btn btn-success createAccount2_continue continue">
                                <span class="spinner-border spinner-border-sm d-none createAccount1_spiner" role="status" aria-hidden="true"></span>  {{ __('Continue')}}</button>
                        </div>
                        <!--<div class="form-group">
                            <button type="button" class="btn btn-transp back_step1"><i class="fa fa-angle-left"></i> <span>{{ __('Back')}}</span></button>
                        </div>-->
                    </div>
                    <div class="form-group bottom-txt-group">
                        <p class="bottom-txt">{{ __('Creating an account means you\'re okay with')}}</p>
                        <ul class="link-list">
                            <li><b>{{ __('Travooo\'s')}}</b></li>
                            <li><a href="{{route('page.terms_of_service')}}"> {{ __('Terms of Service')}}</a>,</li>
                                <li><a href="{{route('page.privacy_policy')}}"> {{ __('Privacy Policy')}}</a></li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <p class="foot-txt">{{ __('Already a member?')}}</p>
                <button type="submit" class="btn btn-grey already_member">{{ __('Log In')}}</button>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function() {
        $('#dob').datepicker({
            yearRange: "1920:2020",
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            maxDate: "2007-12-31",
            altField: "#dob",
            altFormat: "yy-mm-dd"
        });
    });
</script>