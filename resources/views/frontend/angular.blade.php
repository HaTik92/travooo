<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Travooo</title>
    <base href="">
    <link rel="icon" type="image/x-icon" href="http://uat.travooo.com/favicon.ico">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="http://uat.travooo.com/dist/assets/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://unpkg.com/ng2-dnd@5.0.0/bundles/style.css">
    <link href="http://uat.travooo.com/dist/styles.b907f62bd37038558c4c.bundle.css" rel="stylesheet" />
</head>

<body>
    <app-root></app-root>
    <script type="text/javascript" src="http://uat.travooo.com/dist/inline.98b96b0d6110f272f2f7.bundle.js"></script>
    <script type="text/javascript" src="http://uat.travooo.com/dist/polyfills.4cc83066f276d1fd5cb4.bundle.js"></script>
    <script type="text/javascript" src="http://uat.travooo.com/dist/main.33b9c15dbdb9ab75a3a2.bundle.js"></script>
</body>

</html>