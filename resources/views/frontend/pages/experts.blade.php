@php
    $title = 'Travooo - Experts';
@endphp

@extends('site.layouts.site')

<link rel="stylesheet" href="{{asset('assets3/css/style-13102019.css?v=0.2')}}">

@section('base')
    @if(Auth::check())
        @include('site.layouts._header')
    @else
        @include('site.layouts._non-loggedin-user-header')
    @endif
    <div class="content-wrap trv-exp-content bg-custom-white">
        <div class="trv-exp-cover-wrap" style="background-image: url('{{asset('assets2/image/expert-page/expert-header-new.jpg')}}')">
            <div class="exp-conteiner">
                <div class="trv-exp-cover-block">
                    <div class="trv-exp-cover-inner">
                        <h1 class="trv-exp-cover-title">{{__('Your travel stories belong here')}}</h1>
                        <a href="{{url('/signup')}}" class="btn exps-signup-button" type="button">Sign up</a>
                    </div>
                    <div class="trv-exp-cover-img-block">
                            <div class="trv-exp-cover-img-inner">
                                <img class="lazy user-img" alt="Walt Disney World Resort" style="width: 418px; height: 660px;" src="{{asset('assets2/image/expert-page/user-up-photo-new.png')}}">
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="exp-min-content pt-12p">
            <div class="exp-conteiner">
                <div class="row justify-content-center">
                    <div class="exp-cont-video-block">
                        <h2>Why travel experts love Travooo?</h2>
                        <div class="exp-video-content" data-toggle="modal" data-video="https://s3.amazonaws.com/travooo-images2/trips/3132/1608720396a01282afb44fc81a9ebe4f62bb1afce7.mp4" data-target="#videoModal">
                            <video>
                                <source src="https://s3.amazonaws.com/travooo-images2/trips/3132/1608720396a01282afb44fc81a9ebe4f62bb1afce7.mp4" type="video/mp4">
                            </video>
                            <span class="exp-video-player-wrap">
                                 <img src="{{asset('assets2/image/expert-page/play.svg')}}">
                                 Watch Travooo in 250 seconds
                            </span>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="exp-min-content">
            <div class="exp-conteiner">
                <div class="row justify-content-center">
                    <div class="col-11">
                        <div class="row justify-content-between">
                            <div class="col-3 px-3">
                                <p class="exp-cnt-row-title">180k</p>
                                <p class="exp-cnt-row-desc">Experts</p>
                            </div>
                            <div class="col-3 px-3">
                                <p class="exp-cnt-row-title">3.5m+</p>
                                <p class="exp-cnt-row-desc">Places</p>
                            </div>
                            <div class="col-3 px-3">
                                <p class="exp-cnt-row-title">260k</p>
                                <p class="exp-cnt-row-desc">Hotels</p>
                            </div>
                            <div class="col-3 px-3">
                                <p class="exp-cnt-row-title">680k</p>
                                <p class="exp-cnt-row-desc">Restaurants</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="exp-min-content">
            <div class="exp-conteiner">
                <div class="row justify-content-center">
                    <div class="col-11">
                        <div class="row justify-content-between">
                            <div class="col-3 px-3">
                                <div class="exp-cnt-block">
                                    <img src="{{asset('assets2/image/expert-page/cnt-icon-1.png')}}">
                                    <p class="exp-cont-row-title">Champion your stories</p>
                                    <p class="exp-cont-row-desc">We champion travel stories and experiences once kept secret.</p>
                                </div>
                            </div>
                            <div class="col-3 px-3 mt-8p">
                                <div class="exp-cnt-block">
                                    <img src="{{asset('assets2/image/expert-page/cnt-icon-2.png')}}">
                                    <p class="exp-cont-row-title">Find your people</p>
                                    <p class="exp-cont-row-desc">When you share your story, you’ll find a community who enjoys your unique worldview.</p>
                                </div>
                            </div>
                            <div class="col-3 px-3 mt-13p">
                                <div class="exp-cnt-block">
                                    <img src="{{asset('assets2/image/expert-page/cnt-icon-3.png')}}">
                                    <p class="exp-cont-row-title">Cultivate your fanbase</p>
                                    <p class="exp-cont-row-desc">We empower diverse voices and stories all over the world, helping you build a community of followers.</p>
                                </div>
                            </div>
                            <div class="col-3 px-3">
                                <div class="exp-cnt-block">
                                    <img src="{{asset('assets2/image/expert-page/cnt-icon-4.png')}}">
                                    <p class="exp-cont-row-title">Inspire travelers</p>
                                    <p class="exp-cont-row-desc">We inspire travelers to live the experience of their lives and our experts inspire others to do the same.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="exp-min-content">
            <div class="exp-top-slider-conteiner">
                <div class="exp-top-slider-block owl-carousel owl-loaded owl-drag">

                    <div class="exp-top-slider-items">
                    <div class="row justify-content-start">
                        <div class="col-6">
                            <img class="pull-right exp-top-slider-img" src="{{asset('assets2/image/expert-page/travelogs-slide-new.png')}}" >
                        </div>
                        <div class="col-4">
                            <ul class="exp-top-slider-list">
                                <li class="exp-top-slider-info-item active pl-0">
                                    <div class="exp-top-slider-icon-wrap">
                                            <img class="owl-width-30" src="{{asset('assets2/image/expert-page/travelogs.png')}}" width="30">
                                    </div>
                                    <p>Travelog</p>
                                </li>
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-42" src="{{asset('assets2/image/expert-page/q-a-new.png')}}" width="41">
                                    </div>
                                    <p>Q&A</p>
                                </li>
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-42" src="{{asset('assets2/image/expert-page/trip-planner-icon-new.png')}}" width="41">
                                    </div>
                                    <p>Trip Planner</p>
                                </li>
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-23" src="{{asset('assets2/image/expert-page/place-icon-new.png')}}" width="21">
                                    </div>
                                    <p>Places</p>
                                </li>
                            </ul>

                            <div class="exp-top-slider-wrap">
                                <h1>Get the right<br> tools to grow your career</h1>
                                <span class="exp-top-slider-title-link">Travelog</span>

                                <p>Put your experiences into words with Travelogs, a place where you will be
                                    facilitated with all the tools you need to insert, compile, and present
                                    valuable travel information to your audience in your own way.
                                </p>

                                <a href="" class="exp-top-slider-more-link">Learn More →</a>
                            </div>
                        </div>

                    </div>
                    </div>

                    <div class="exp-top-slider-items row justify-content-start">
                        <div class="col-6">
                            <img class="pull-right exp-top-slider-img" src="{{asset('assets2/image/expert-page/qa-slider-new.png')}}">
                        </div>
                        <div class="col-4">
                            <ul class="exp-top-slider-list">
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-30" class="owl-width-30" src="{{asset('assets2/image/expert-page/travelogs.png')}}" width="30">
                                    </div>
                                    <p>Travelog</p>
                                </li>
                                <li class="exp-top-slider-info-item active">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-42" src="{{asset('assets2/image/expert-page/q-a-new.png')}}" width="41">
                                    </div>
                                    <p>Q&A</p>
                                </li>
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-42" src="{{asset('assets2/image/expert-page/trip-planner-icon-new.png')}}" width="41">
                                    </div>
                                    <p>Trip Planner</p>
                                </li>
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-23" src="{{asset('assets2/image/expert-page/place-icon-new.png')}}" width="21">
                                    </div>
                                    <p>Places</p>
                                </li>
                            </ul>

                            <div class="exp-top-slider-wrap">
                                <h1>Get the right<br> tools to grow your career</h1>
                                <span class="exp-top-slider-title-link">Q&A</span>

                                <p>Participate in discussions and answer questions posted by fellow travelers to get recognized
                                    as the go-to source for a particular place, city, or even a country!
                                </p>

                                <a href="" class="exp-top-slider-more-link">Learn More →</a>
                            </div>
                        </div>
                    </div>

                    <div class="exp-top-slider-items row justify-content-start">
                        <div class="col-6">
                            <img class="pull-right exp-top-slider-img" src="{{asset('assets2/image/expert-page/trip-planner-new.png')}}">
                        </div>
                        <div class="col-4">
                            <ul class="exp-top-slider-list">
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-30" src="{{asset('assets2/image/expert-page/travelogs.png')}}" width="31">
                                    </div>
                                    <p>Travelog</p>
                                </li>
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-42" src="{{asset('assets2/image/expert-page/q-a-new.png')}}" width="41">
                                    </div>
                                    <p>Q&A</p>
                                </li>
                                <li class="exp-top-slider-info-item active">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-42" src="{{asset('assets2/image/expert-page/trip-planner-icon-new.png')}}" width="41">
                                    </div>
                                    <p>Trip Planner</p>
                                </li>
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-23" src="{{asset('assets2/image/expert-page/place-icon-new.png')}}" width="21">
                                    </div>
                                    <p>Places</p>
                                </li>
                            </ul>

                            <div class="exp-top-slider-wrap">
                                <h1>Get the right<br> tools to grow your career</h1>
                                <span class="exp-top-slider-title-link">Trip Planner</span>

                                <p>Tailor your past, present, and future plans together to let your actions do the talking with your
                                    audience. Help others formulate their trips through your knowledge and become an advisor.
                                </p>

                                <a href="" class="exp-top-slider-more-link">Learn More →</a>
                            </div>
                        </div>
                    </div>

                    <div class="exp-top-slider-items row justify-content-start">
                        <div class="col-6">
                            <img class="pull-right exp-top-slider-img" src="{{asset('assets2/image/expert-page/place-slider-new.png')}}">
                        </div>
                        <div class="col-4">
                            <ul class="exp-top-slider-list">
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-30" src="{{asset('assets2/image/expert-page/travelogs.png')}}" width="31">
                                    </div>
                                    <p>Travelog</p>
                                </li>
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-42" src="{{asset('assets2/image/expert-page/q-a-new.png')}}" width="41">
                                    </div>
                                    <p>Q&A</p>
                                </li>
                                <li class="exp-top-slider-info-item">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-42" src="{{asset('assets2/image/expert-page/trip-planner-icon-new.png')}}" width="41">
                                    </div>
                                    <p>Trip Planner</p>
                                </li>
                                <li class="exp-top-slider-info-item active">
                                    <div class="exp-top-slider-icon-wrap">
                                        <img class="owl-width-23" src="{{asset('assets2/image/expert-page/place-icon-new.png')}}" width="21">
                                    </div>
                                    <p>Places</p>
                                </li>
                            </ul>

                            <div class="exp-top-slider-wrap">
                                <h1>Get the right tools to grow your career</h1>
                                <a href="" class="exp-top-slider-title-link">Places</a>

                                <p>Have your name labelled on the places which you're most familiar with. Attract new travelers by becoming
                                    the conqueror of every destination that you've been to.
                                </p>

                                <a href="" class="exp-top-slider-more-link">Learn More →</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="exp-min-content">
            <div class="exp-title-block">
                <h1>Traveler Programs &<br>
                    Opportunities</h1>
            </div>
            <div class="exp-mid-slider-block owl-carousel owl-loaded owl-drag">
                <!-- slide item -->
                <div class="exp-mid-slider-item row justify-content-center">
                    <div class="col-3">
                        <div class="exp-mid-slider-info">
                            <div class="mid-slide-icon-wrap">
                                <div>
                                    <img class="active" src="{{asset('assets2/image/expert-page/win-awards-icon.png')}}">
                                </div>
                            </div>
                            <p class="exp-mid-slider-title">Win Awards</p>
                            <p class="exp-mid-slider-desc">
                                We recognise the amount of effort and dedication experts like you
                                put into their contents. That's why we support and encourage each expert when they reach
                                Expert Award badges milestones.
                            </p>

                            <a href="javacript:;" class="exp-mid-slider-link">Learn More →</a>
                        </div>
                    </div>
                    <div class="col-5 pl-0 d-flex align-items-center">
                        <img class="mid-slider-img" src="{{asset('assets2/image/expert-page/win-awards-slider.png')}}">
                        <ul class="mid-slide-item-list">
                            <li><a href="javascript:;" class="slide-item win-awards-link"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/win-awards-icon.png')}}">Win Awards</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/become-slider-icon.png')}}">Become a Travooo Star</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/paid-slider-icon.png')}}">Paid Stories</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/featured-slider-icon.png')}}">Get Featured</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/contests-slider-icon.png')}}">Enter Writing Contests</a></li>
                        </ul>
                    </div>
                </div>
                <!-- slide item -->
                <div class="exp-mid-slider-item row justify-content-center">
                    <div class="col-3">
                        <div class="exp-mid-slider-info">
                            <div class="mid-slide-icon-wrap">
                                <div>
                                    <img class="active" src="{{asset('assets2/image/expert-page/become-slider-icon.png')}}">
                                </div>
                            </div>
                            <p class="exp-mid-slider-title">Become a Travooo <br>Expert Star</p>
                            <p class="exp-mid-slider-desc">
                                When you become a Travooo Expert Star, You'll unlock unique opportunities to harness your oun potential as an expert.
                            </p>

                            <p class="exp-mid-slider-desc">
                                You'll also join of community of fellow Stars - a place to collaborate and commiserate with experts who are on their own journey.
                            </p>

                            <a href="javacript:;" class="exp-mid-slider-link">Learn More →</a>
                        </div>
                    </div>
                    <div class="col-5 pl-0 d-flex align-items-center">
                        <img class="mid-slider-img" src="{{asset('assets2/image/expert-page/become-slider.png')}}">
                        <ul class="mid-slide-item-list">
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/win-awards-icon.png')}}">Win Awards</a></li>
                            <li><a href="javascript:;" class="slide-item win-awards-link"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/become-slider-icon.png')}}">Become a Travooo Star</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/paid-slider-icon.png')}}">Paid Stories</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/featured-slider-icon.png')}}">Get Featured</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/contests-slider-icon.png')}}">Enter Writing Contests</a></li>
                        </ul>
                    </div>
                </div>
                <!-- slide item -->
                <div class="exp-mid-slider-item row justify-content-center">
                    <div class="col-3">
                        <div class="exp-mid-slider-info">
                            <div class="mid-slide-icon-wrap">
                                <div>
                                    <img class="active" src="{{asset('assets2/image/expert-page/paid-slider-icon.png')}}">
                                </div>
                            </div>
                            <p class="exp-mid-slider-title">Paid Stories</p>
                            <p class="exp-mid-slider-desc">
                                Being connected to your fans is one of the most satisfying and rewarding parts of Travooo.
                            </p>
                            <p class="exp-mid-slider-desc">
                                With Paid Stories, your audience can also reward you with monetary support once they book the offers and places you recommend to them.
                            </p>

                            <a href="javacript:;" class="exp-mid-slider-link">Learn More →</a>
                        </div>
                    </div>
                    <div class="col-5 pl-0 d-flex align-items-center">
                        <img class="mid-slider-img" src="{{asset('assets2/image/expert-page/paid-slider.png')}}">
                        <ul class="mid-slide-item-list">
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/win-awards-icon.png')}}">Win Awards</a></li>
                            <li><a href="javascript:;" class="slide-item "><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/become-slider-icon.png')}}">Become a Travooo Star</a></li>
                            <li><a href="javascript:;" class="slide-item win-awards-link"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/paid-slider-icon.png')}}">Paid Stories</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/featured-slider-icon.png')}}">Get Featured</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/contests-slider-icon.png')}}">Enter Writing Contests</a></li>
                        </ul>
                    </div>
                </div>
                <!-- slide item -->
                <div class="exp-mid-slider-item row justify-content-center">
                    <div class="col-3">
                        <div class="exp-mid-slider-info">
                            <div class="mid-slide-icon-wrap">
                                <div>
                                    <img class="active" src="{{asset('assets2/image/expert-page/featured-slider-icon.png')}}">
                                </div>
                            </div>
                            <p class="exp-mid-slider-title">Get Featured</p>
                            <p class="exp-mid-slider-desc">
                                At Travooo, we love great content. They're our entire world. That's why we have a dedicated team who spends time exploring our community, finding hidden gems, and amplifying great stories.
                            </p>

                            <p class="exp-mid-slider-desc">
                                When we come across a journey that excites us, we feature it as a Travooo Pick for a limited time so our community can discover what we already love about it.
                            </p>

                            <a href="javacript:;" class="exp-mid-slider-link">Learn More →</a>
                        </div>
                    </div>
                    <div class="col-5 pl-0 d-flex align-items-center">
                        <img class="mid-slider-img" src="{{asset('assets2/image/expert-page/featured-slider.png')}}">
                        <ul class="mid-slide-item-list">
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/win-awards-icon.png')}}">Win Awards</a></li>
                            <li><a href="javascript:;" class="slide-item "><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/become-slider-icon.png')}}">Become a Travooo Star</a></li>
                            <li><a href="javascript:;" class="slide-item "><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/paid-slider-icon.png')}}">Paid Stories</a></li>
                            <li><a href="javascript:;" class="slide-item win-awards-link"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/featured-slider-icon.png')}}">Get Featured</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/contests-slider-icon.png')}}">Enter Writing Contests</a></li>
                        </ul>
                    </div>
                </div>
                <!-- slide item -->
                <div class="exp-mid-slider-item row justify-content-center">
                    <div class="col-3">
                        <div class="exp-mid-slider-info">
                            <div class="mid-slide-icon-wrap">
                                <div>
                                    <img class="active" src="{{asset('assets2/image/expert-page/contests-slider-icon.png')}}">
                                </div>
                            </div>
                            <p class="exp-mid-slider-title">Win Contests</p>
                            <p class="exp-mid-slider-desc">
                                Travooo works with the brands in the travel industry to create on a monthly basis new and exciting contests for our experts.
                            </p>

                            <p class="exp-mid-slider-desc">
                                That's why contests on Travooo are designed to spark a movement within our community that not only
                                encourages participation, but provides a platform for fledgling experts to showcase their creativity and storytelling ability.
                            </p>

                            <a href="javacript:;" class="exp-mid-slider-link">Learn More →</a>
                        </div>
                    </div>
                    <div class="col-5 pl-0 d-flex align-items-center">
                        <img class="mid-slider-img" src="{{asset('assets2/image/expert-page/contents-slider.png')}}">
                        <ul class="mid-slide-item-list">
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/win-awards-icon.png')}}">Win Awards</a></li>
                            <li><a href="javascript:;" class="slide-item "><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/win-awards-icon.png')}}">Become a Travooo Star</a></li>
                            <li><a href="javascript:;" class="slide-item "><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/paid-slider-icon.png')}}">Paid Stories</a></li>
                            <li><a href="javascript:;" class="slide-item"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/featured-slider-icon.png')}}">Get Featured</a></li>
                            <li><a href="javascript:;" class="slide-item win-awards-link"><img class="mid-slide-left-icon" src="{{asset('assets2/image/expert-page/contests-slider-icon.png')}}">Enter Writing Contests</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="exp-min-content">
            <div class="exp-title-block">
                <h1>Profile Page</h1>
                <h2>Your personal unique space</h2>
            </div>
            <div class="exp-profile-slider-content">
                <div class="exp-profile-slider-title">
                    <ul class="exp-profile-slider-icon-list">
                        <li class="exp-profile-slider-icon-item active" data-tab="mobile">
                            <i class="fa fa-mobile-alt" aria-hidden="true"></i> Mobile
                        </li>
                        <li class="exp-profile-slider-icon-item" data-tab="desc">
                            <i class="fa fa-desktop custom" aria-hidden="true"></i> Desktop
                        </li>
                    </ul>
                </div>
                <div class="exp-profile-desc-slider-block d-none">
                    <ul class="exp-profile-desc-slider-list">
                        <li class="exp-profile-desc-slider-item">
                            <img class="desc-slide-bg-image" src="{{asset('/assets2/image/expert-page/des-slide-bg.png')}}">
                            <img src="{{asset('assets2/image/expert-page/desc-slide1.png')}}">
                        </li>
                        <li class="exp-profile-desc-slider-item">
                            <img class="desc-slide-bg-image" src="{{asset('/assets2/image/expert-page/des-slide-bg.png')}}">
                            <img src="{{asset('assets2/image/expert-page/desc-slide3.png')}}">
                        </li>
                        <li class="exp-profile-desc-slider-item">
                            <img class="desc-slide-bg-image" src="{{asset('/assets2/image/expert-page/des-slide-bg.png')}}">
                            <img src="{{asset('assets2/image/expert-page/desc-slide2.png')}}">
                        </li>
                        <li class="exp-profile-desc-slider-item">
                            <img class="desc-slide-bg-image" src="{{asset('/assets2/image/expert-page/des-slide-bg.png')}}">
                            <img src="{{asset('assets2/image/expert-page/desc-slide1.png')}}">
                        </li>
                    </ul>
                </div>
                <div class="exp-profile-mob-slider-block">
                    <ul class="exp-profile-mob-slider-list">
                        <li class="exp-profile-mob-slider-item">
                            <img class="mob-slide-bg-image" src="{{asset('/assets2/image/expert-page/mob-slide-bg-new.png')}}">
                            <img class="mob-slide-main-img" src="{{asset('assets2/image/expert-page/mob-slide1.png')}}">
                        </li>
                        <li class="exp-profile-mob-slider-item">
                            <img class="mob-slide-bg-image" src="{{asset('/assets2/image/expert-page/mob-slide-bg-new.png')}}">
                            <img class="mob-slide-main-img" src="{{asset('assets2/image/expert-page/mob-slide2.png')}}">
                        </li>
                        <li class="exp-profile-mob-slider-item">
                            <img class="mob-slide-bg-image" src="{{asset('/assets2/image/expert-page/mob-slide-bg-new.png')}}">
                            <img class="mob-slide-main-img" src="{{asset('assets2/image/expert-page/mob-slide3.png')}}">
                        </li>
                        <li class="exp-profile-mob-slider-item">
                            <img class="mob-slide-bg-image" src="{{asset('/assets2/image/expert-page/mob-slide-bg-new.png')}}">
                            <img class="mob-slide-main-img" src="{{asset('assets2/image/expert-page/mob-slide4.png')}}">
                        </li>
                        <li class="exp-profile-mob-slider-item">
                            <img class="mob-slide-bg-image" src="{{asset('/assets2/image/expert-page/mob-slide-bg-new.png')}}">
                            <img class="mob-slide-main-img" src="{{asset('assets2/image/expert-page/mob-slide5.png')}}">
                        </li>
                        <li class="exp-profile-mob-slider-item">
                            <img class="mob-slide-bg-image" src="{{asset('/assets2/image/expert-page/mob-slide-bg-new.png')}}">
                            <img class="mob-slide-main-img" src="{{asset('assets2/image/expert-page/mob-slide1.png')}}">
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="exp-min-content pb-0">
            <div class="exp-title-block">
                <h1>Join 1000+ Experts</h1>
                <h2>Be part of a global community of travel experts and enthusiasts, all connected through the power<br> of journey and story.</h2>
            </div>
            <div class="exp-img-block pb-0">
                <img class="px-3" src="{{asset('assets2/image/expert-page/job-bg-new.png')}}">
            </div>
        </div>

        <div class="exp-min-content py-0">
            <div class="exp-img-block">
                <div class="exp-subimg-block row justify-content-center">
                    <div class="col-4">
                        <div class="exp-footer-img-block">
                            <h1>EXPLORE.
                                DREAM.
                                DISCOVER.</h1>
                            <p class="exp-footer-img-desc">Travel experts like you use Travooo to connect with travelers, grow their
                                audience, get access to private insights, share their stories and even endorse offers all in just one place.
                            </p>
                            <div class="exp-footer-service-block">
                                <a href="#" class="btn-outline-dark btn-icon-text service-btn app-btn">
                                    <i class="fab fa-apple"></i> App Store
                                </a>
                                <a href="#" class="btn-outline-dark btn-icon-text service-btn google-btn">
                                   <i class="fab fa-google-play"></i> Google Play
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-5">
                        <img src="{{asset('assets2/image/expert-page/footer-bg-new.png')}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="landing-footer">
            <div class="access-deined-content">

            </div>
            <div class="setting-footer-block pt-20" dir="auto">
                <ul class="setting-footer-list pb-2" dir="auto">
                    <li dir="auto"><a href="{{url('/')}}" dir="auto">About</a></li>
                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Careers</a></li>
                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Sitemap</a></li>
                    <li dir="auto"><a href="{{route('page.privacy_policy')}}" dir="auto">Privacy</a></li>
                    <li dir="auto"><a href="{{route('page.terms_of_service')}}" dir="auto">Terms</a></li>
                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Contact</a></li>
                    <li dir="auto"><a href="{{route('help.index')}}" dir="auto">Help Center</a></li>
                </ul>
                <p class="setting-copyright" dir="auto">Travooo © {{date('Y')}}</p>
            </div>
        </div>

    </div>

    <div class="modal fade disc-video-modal" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
        </button>
        <div class="modal-dialog modal-custom-style modal-740" role="document">
            <div class="modal-custom-block">
                <div class="modal-body">
                    <video controls width="100%">
                        <source src="" type="video/mp4">
                    </video>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_scripts')
    <script type="text/javascript" src="{{asset('assets2/js/slick.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            var owl = $('.exp-top-slider-block').owlCarousel({
                loop:true,
                autoplay: true,
                margin:0,
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                nav:false,
                dots: false,
                rtl:true,
                autoplayHoverPause: false,
                items: 3,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });

            $('.exp-top-slider-list').on('click', 'li', function(e) {
                owl.trigger('to.owl.carousel', [$(this).index(), 120]);
            });

            var mid_owl = $('.exp-mid-slider-block').owlCarousel({
                loop:true,
                autoplay: true,
                margin:0,
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                nav:false,
                dots: false,
                rtl:true,
                autoplayHoverPause: false,
                items: 3,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });

            $('.mid-slide-item-list').on('click', 'li', function(e) {
                mid_owl.trigger('to.owl.carousel', [$(this).index(), 120]);
            });

            $('.exp-profile-mob-slider-list').not('.slick-initialized').slick({
                 arrows: !1,
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                infinite: true,
                centerMode: true,
                centerPadding: '60px',
                focusOnSelect:false
            });

            $('.exp-profile-desc-slider-list').not('.slick-initialized').slick({
                arrows: !1,
                variableWidth: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                infinite: true,
                centerMode: true,
                centerPadding: '60px',
                focusOnSelect:false
            });

            $(document).on('click', '.exp-profile-slider-icon-item', function(){
                $('.exp-profile-slider-icon-list').find('.exp-profile-slider-icon-item').removeClass('active');
                $(this).addClass('active');

                var active_tab = $(this).attr('data-tab')

                if(active_tab == 'mobile'){
                    $('.exp-profile-mob-slider-block').removeClass('d-none')
                    $('.exp-profile-desc-slider-block').addClass('d-none')
                    $('.exp-profile-mob-slider-list').slick('refresh');
                }else{
                    $('.exp-profile-mob-slider-block').addClass('d-none')
                    $('.exp-profile-desc-slider-block').removeClass('d-none')
                    $('.exp-profile-desc-slider-list').slick('refresh');
                }
            })

            //Open video modal
            $(document).on('click', ".exp-video-content", function () {
                var theModal = $(this).data("target"),
                    videoSRC = $(this).attr("data-video"),
                    videoSRCauto = videoSRC + "";

                $(theModal + ' source').attr('src', videoSRCauto);
                $(theModal + ' video')[0].load();
                $(theModal + ' button.close').click(function () {
                    $(theModal + ' source').attr('src', videoSRC);
                });
            });
        })
    </script>
@endsection


