@php
    $title = 'Travooo - Trip Planner';
@endphp

@extends('site.layouts.site')

<link rel="stylesheet" href="{{asset('assets3/css/style-13102019.css?v=0.2')}}">

@section('base')
    @if(Auth::check())
        @include('site.layouts._header')
    @else
        @include('site.layouts._non-loggedin-user-header')
    @endif
    <div class="content-wrap trv-exp-content bg-white">
        <div class="tplanner-conteiner">
            <div class="tplanner-page-title">
                <h1>Trip planning made easier</h1>
                <p class="lng-page-subtitle">The trip planner of choice to plan your next memorable travel.</p>
            </div>
            <div class="tplanner-cover-block">
                <img src="{{asset('assets2/image/expert-page/tp-1-cover.png')}}">
            </div>

            <div class="lng-page-desc-block">
                <div class="lng-page-inner border-btm-gray">
                    <div class="lng-page-topl-wrap border-rght-gray">
                        <img src="{{asset('assets2/image/expert-page/trip-planner-icon.png')}}">
                        <p class="lng-page-desc-title">Plan your<br> trips</p>
                        <p class="lng-page-desc-content">Create your most exciting tripl plan from A to Z.</p>
                    </div>
                    <div class="lng-page-topr-wrap">
                        <img src="{{asset('assets2/image/expert-page/play-icon.png')}}">
                        <p class="lng-page-desc-title">Unlimited posts, photos and videos</p>
                        <p class="lng-page-desc-content">Add as many media as you want.</p>
                    </div>
                </div>
                <div class="lng-page-inner">
                    <div class="lng-page-btml-wrap border-rght-gray">
                        <img src="{{asset('assets2/image/expert-page/icon-3.png')}}">
                        <p class="lng-page-desc-title">Sleek, elegant and<br> easy to use</p>
                        <p class="lng-page-desc-content">It's not just incredibly useful, but also beautiful.</p>
                    </div>
                    <div class="lng-page-btmr-wrap">
                        <img src="{{asset('assets2/image/expert-page/icon-4.png')}}">
                        <p class="lng-page-desc-title">Fully synchronized website and apps</p>
                        <p class="lng-page-desc-content">Access and sync your plan from any device.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="tplanner-conteiner">
            <div class="lng-page-title">
                <h1>Be Guided. Be Inspired</h1>
                <p class="lng-page-subtitle">With the right inspiration, you will never have to face the "writer's block" effect anymore.</p>
            </div>
            <div class="tplanner-cover-block">
                <img src="{{asset('assets2/image/expert-page/tp-2-cover.png')}}">
            </div>
        </div>

        <div class="tplanner-conteiner">
            <div class="lng-page-title">
                <h1>Just the right app for your trip.</h1>
                <p class="lng-page-subtitle">The next generation trip planner app that empowers travelers with smart data for smart decision making.</p>
            </div>
            <div class="tplanner-cover-block">
                <img src="{{asset('assets2/image/expert-page/tp-3-cover.png')}}">
            </div>

            <div class="lng-page-desc-block">
                <div class="lng-page-inner border-btm-gray">
                    <div class="lng-page-topl-wrap border-rght-gray">
                        <img src="{{asset('assets2/image/expert-page/icon-6.png')}}">
                        <p class="lng-page-desc-title">Travel<br> statistics</p>
                        <p class="lng-page-desc-content">Know everything about your destinations while planning.</p>
                    </div>
                    <div class="lng-page-topr-wrap">
                        <img src="{{asset('assets2/image/expert-page/icon-5.png')}}">
                        <p class="lng-page-desc-title">Make smart  decisions</p>
                        <p class="lng-page-desc-content">Don't make hazardous decisions. Make smart ones.</p>
                    </div>
                </div>
                <div class="lng-page-inner">
                    <div class="lng-page-btml-wrap border-rght-gray">
                        <img src="{{asset('assets2/image/expert-page/icon-7.png')}}">
                        <p class="lng-page-desc-title">Sleek, elegant and<br> easy to use</p>
                        <p class="lng-page-desc-content">It's not just incredibly useful, but also beautiful.</p>
                    </div>
                    <div class="lng-page-btmr-wrap">
                        <img src="{{asset('assets2/image/expert-page/icon-8.png')}}">
                        <p class="lng-page-desc-title">Fully synchronized website and apps</p>
                        <p class="lng-page-desc-content">Access and sync your plan from any device.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="access-deined-content">

            </div>
            <div class="setting-footer-block pt-20" dir="auto">
                <ul class="setting-footer-list pb-2" dir="auto">
                    <li dir="auto"><a href="{{url('/')}}" dir="auto">About</a></li>
                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Careers</a></li>
                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Sitemap</a></li>
                    <li dir="auto"><a href="{{route('page.privacy_policy')}}" dir="auto">Privacy</a></li>
                    <li dir="auto"><a href="{{route('page.terms_of_service')}}" dir="auto">Terms</a></li>
                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Contact</a></li>
                    <li dir="auto"><a href="{{route('help.index')}}" dir="auto">Help Center</a></li>
                </ul>
                <p class="setting-copyright" dir="auto">Travooo © {{date('Y')}}</p>
            </div>
        </div>

    </div>

    <div class="modal fade disc-video-modal" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
        </button>
        <div class="modal-dialog modal-custom-style modal-740" role="document">
            <div class="modal-custom-block">
                <div class="modal-body">
                    <video controls width="100%">
                        <source src="" type="video/mp4">
                    </video>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_scripts')
    <script type="text/javascript" src="{{asset('assets2/js/slick.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            var owl = $('.exp-top-slider-block').owlCarousel({
                loop:true,
                autoplay: true,
                margin:0,
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                nav:false,
                dots: false,
                rtl:true,
                autoplayHoverPause: false,
                items: 3,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });

            $('.exp-top-slider-list').on('click', 'li', function(e) {
                owl.trigger('to.owl.carousel', [$(this).index(), 120]);
            });

            var mid_owl = $('.exp-mid-slider-block').owlCarousel({
                loop:true,
                autoplay: true,
                margin:0,
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                nav:false,
                dots: true,
                rtl:true,
                autoplayHoverPause: false,
                items: 3,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });

            $('.mid-slide-item-list').on('click', 'li', function(e) {
                mid_owl.trigger('to.owl.carousel', [$(this).index(), 120]);
            });

            $('.exp-profile-mob-slider-list').not('.slick-initialized').slick({
                 arrows: !1,
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                infinite: true,
                centerMode: true,
                centerPadding: '60px',
                focusOnSelect:false
            });

            $('.exp-profile-desc-slider-list').not('.slick-initialized').slick({
                arrows: !1,
                variableWidth: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                infinite: true,
                centerMode: true,
                centerPadding: '60px',
                focusOnSelect:false
            });

            $(document).on('click', '.exp-profile-slider-icon-item', function(){
                $('.exp-profile-slider-icon-list').find('.exp-profile-slider-icon-item').removeClass('active');
                $(this).addClass('active');

                var active_tab = $(this).attr('data-tab')

                if(active_tab == 'mobile'){
                    $('.exp-profile-mob-slider-block').removeClass('d-none')
                    $('.exp-profile-desc-slider-block').addClass('d-none')
                    $('.exp-profile-mob-slider-list').slick('refresh');
                }else{
                    $('.exp-profile-mob-slider-block').addClass('d-none')
                    $('.exp-profile-desc-slider-block').removeClass('d-none')
                    $('.exp-profile-desc-slider-list').slick('refresh');
                }
            })

            //Open video modal
            $(document).on('click', ".exp-video-content", function () {
                var theModal = $(this).data("target"),
                    videoSRC = $(this).attr("data-video"),
                    videoSRCauto = videoSRC + "";

                $(theModal + ' source').attr('src', videoSRCauto);
                $(theModal + ' video')[0].load();
                $(theModal + ' button.close').click(function () {
                    $(theModal + ' source').attr('src', videoSRC);
                });
            });
        })
    </script>
@endsection


