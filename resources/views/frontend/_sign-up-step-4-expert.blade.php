<div class="modal fade sign-up responsive-modal res-scroll" id="createAccount4_expert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content step-content">
            <div class="modal-body body-create">
                <div class="top-layer">
                    <p class="login-title">{{ __('Become a Travooo Expert') }}</p>
                    <p class="login-descripton">{{ __('Socila Media links for account validation') }}</p>
                    <div class="error-messages"></div>
                </div>
                <form class="login-form" method="post" action="" id="social_form">
                    <div class="step-2">
                        <div class="form-group description">
                            Add your website or social media links:
                            <span data-toggle="tooltip" title="It’s important to put at least one website or social media profile link where you share your current traveling activities to help us approve your Expert Account application." data-placement="bottom">Why?</span>
                        </div>
                        <div class="form-group site default">
                            <img src="{{asset('assets2/image/sign_up/website.png')}}">
                            <input type="text" class="form-control" id="website" name="website" placeholder="{{ __('Put at lease one link...')}}">
                        </div>
                        <div class="form-group site">
                            <img src="{{asset('assets2/image/sign_up/twitter.png')}}">
                            <span class="twitter-link-protocol">https://twitter.com/</span>
                            <input type="text" class="form-control" id="twitter" name="twitter" placeholder="{{ __('https://twitter.com/username')}}">
                            <div class="cross">x</div>
                        </div>
                        <div class="form-group description links">
                            Add more links:
                            <div>
                                <img data-name='tumblr' src="{{asset('assets2/image/sign_up/tumblr.png')}}">
                                <img data-name='pinterest' src="{{asset('assets2/image/sign_up/pinterest.png')}}">
                                <img data-name='instagram' src="{{asset('assets2/image/sign_up/instagram.png')}}">
                                <img data-name='facebook' src="{{asset('assets2/image/sign_up/facebook.png')}}">
                                <img data-name='youtube' src="{{asset('assets2/image/sign_up/youtube.png')}}">
                            </div>
                        </div>
                        <div class="form-group continue-wrapper">
                            <button type="button" class="btn createAccount2_continue continue disabled">
                                <span class="spinner-border spinner-border-sm d-none createAccount1_spiner" role="status" aria-hidden="true"></span>  {{ __('Continue')}}</button>
                        </div>
                        <div class="form-group">
                            <div class="signup-back-btn back-btn mb-3">
                                <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back
                            </div>
                        </div>
                    </div>
                    <div class="form-group bottom-txt-group">
                        <p class="bottom-txt">{{ __('Creating an account means you\'re okay with')}}</p>
                        <ul class="link-list">
                            <li><b>{{ __('Travooo\'s')}}</b></li>
                            <li><a href="{{route('page.terms_of_service')}}"> {{ __('Terms of Service')}}</a>,</li>
                            <li><a href="{{route('page.privacy_policy')}}"> {{ __('Privacy Policy')}}</a></li>
                        </ul>
                    </div>
                </form>
                <div class="modal-footer">
                    <div>
                        <span class="foot-txt">{{ __('Already a member?')}}</span> <span class="signup-button go_log_in">{{ __('Log In')}}</span>
                    </div>
                    <div class="exp-signup-button signup-type-btn" data-type="regular">
                        <span class="exp-text-title">Create a</span>
                        <span class="exp-text-desc">Personal Account</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        showCrosses();
        $('#social_form').validate();

        $(document).on('keyup', 'div#createAccount4_expert .form-group.site input', function () {
            if ($(this).val() != '') {
                $('div#createAccount4_expert .createAccount2_continue').removeClass('disabled')
            } else {
                $('div#createAccount4_expert .createAccount2_continue').addClass('disabled')
            }
        })

        $('div#createAccount4_expert').on('click', 'div.form-group.description.links img', function () {
            var name = $(this).data('name');
            var style = 'top: 9px; left: 9px;';
            style = '';
            var url_content = '';

            if (name === 'youtube') {
                style = '';
            }

            if(name === 'twitter' || name === 'facebook'){
                var placeholder = 'https://' + name + '/username';
            }else{
                var placeholder = 'https://' + name + '/anypage';
            }

            if(name === 'facebook'){
                url_content = '<span class="facebook-link-protocol">https://www.facebook.com/</span>'
            }

            if(name === 'twitter'){
                url_content = '<span class="twitter-link-protocol">https://twitter.com/</span>'
            }

            $('div#createAccount4_expert div.form-group.site:last').after('<div class="form-group site">\n' +
                    '                            <img style="' + style + '" src="/assets2/image/sign_up/' + name + '.png">\n' + url_content +
                    '                            <input type="text" class="form-control" id="' + name + '" name="' + name + '" placeholder="'+ placeholder +'">\n' +
                    '                        </div>');

            $(this).remove();

            if (!$('div#createAccount4_expert div.form-group.description.links img').length) {
                $('div#createAccount4_expert div.form-group.description.links').remove();
            }

            switch (name) {
                case 'website':
                    $("#website").rules("add", {
                        website: true,
                    })
                    break;
                case 'twitter':
                    $("#twitter").rules("add", {
                        // twitter: true,
                        required: true,
                    })
                    break;
                case 'tumblr':
                    $("#tumblr").rules("add", {
                        tumblr: true,
                    })
                    break;
                case 'pinterest':
                    $("#pinterest").rules("add", {
                        pinterest: true,
                    })
                    break;
                case 'instagram':
                    $("#instagram").rules("add", {
                        instagram: true,
                    })
                    break;
                case 'facebook':
                    $("#facebook").rules("add", {
                        required: true,
                    })
                    break;
                case 'youtube':
                    $("#youtube").rules("add", {
                        youtube: true,
                    })
                    break;
            }

            showCrosses();
        });

        $(document).on('blur', '#tumblr, #pinterest, #instagram, #youtube', function(){
            var media_value = $(this).val()
            var new_value = media_value.replace('https://', '');

            $(this).val(new_value)
        })

        $(document).on('blur', '#website', function(){
            var media_value = $(this).val()
            if(media_value != ''){
                $(this).css('color', '#c1c1c1 !important')
            }
        })

        $(document).on('blur', '#twitter', function(){
            var media_value = $(this).val()
            if(media_value != ''){
                var new_value = media_value.replace('https://twitter.com/', '');
                $(this).val(new_value)

                $('.twitter-link-protocol').show()
                $(this).css('padding-left', '196px')
            }
        })

        $(document).on('blur', '#facebook', function(){
            var media_value = $(this).val()
            if(media_value != ''){
                var new_value = media_value.replace('https://www.facebook.com/', '');
                $(this).val(new_value)

                $('.facebook-link-protocol').show()
                $(this).css('padding-left', '254px')
            }
        })

        function showCrosses() {
            $('div#createAccount4_expert div.form-group.site div.cross').remove();

            if ($('div#createAccount4_expert div.form-group.site').length > 1) {
                $.each($('div#createAccount4_expert div.form-group.site'), function() {

                    if(!$(this).hasClass('default')){
                        $(this).append('<div class="cross">x</div>');
                    }
                });
            }
        }

        function ucfirst(str) {
            var firstLetter = str.slice(0, 1);
            return firstLetter.toUpperCase() + str.substring(1);
        }

        $('div#createAccount4_expert').on('click', 'div.form-group.site div.cross', function () {
            var parent = $(this).parent('div.form-group.site');

            $(parent).remove();
            returnIcon($(parent).find('input').attr('id'));

            showCrosses();
        });

        function returnIcon(name) {
            if (!$('div#createAccount4_expert div.form-group.description.links').length) {
                $('div#createAccount4_expert div.continue-wrapper').before('' +
                        '<div class="form-group description links">' +
                        'Add more links:' +
                        '<div>' +
                        '</div>' +
                        '</div>')
            }

            var icon_url = '{{asset("assets2/image/sign_up/")}}/' + name + '.png';

            $('div#createAccount4_expert div.form-group.description.links div').append('<img data-name="' + name + '" src="' + icon_url + '">');
        }



        $("#website").rules("add", {
            website: true,
        })

        $("#twitter").rules("add", {
            // twitter: true,
            required: true,
        })



        // $.validator.addMethod("facebook", function (value, element) {
        //     return this.optional(element) || /(?:https?:\/\/)?(?:www\.)?facebook\.com\/.(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-\.]*)/i.test(value);
        // }, "Please enter a valid facebook URL."
        //         );

        // $.validator.addMethod("twitter", function (value, element) {
        //     return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/i.test(value);
        // }, "Please enter a valid twitter URL."
        //         );

        $.validator.addMethod("instagram", function (value, element) {
            return this.optional(element) || /(https?:\/\/(?:www\.)?instagram\.com\/([^/?#&]+)).*/i.test(value);
        }, "Please enter a valid instagram URL."
                );

        $.validator.addMethod("website", function (value, element) {
            return this.optional(element) || /^(?:(?:http|https|ftp):\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
        }, "Please enter a valid URL."
                );

        $.validator.addMethod("youtube", function (value, element) {
            return this.optional(element) || /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/i.test(value);
        }, "Please enter a valid youtube URL."
                );

        // $.validator.addMethod("tumblr", function (value, element) {
        //     return this.optional(element) || /[^"\/www\."](?<!w{3})[A-Za-z0-9]*(?=\.tumblr\.com)|(?<=\.tumblr\.com\/blog\/).*/i.test(value);
        // }, "Please enter a valid tumblr URL."
        //         );

        $.validator.addMethod("pinterest", function (value, element) {
            return this.optional(element) || /(?:(?:http|https):\/\/)?(?:www\.)?(?:pinterest\.com|instagr\.am)\/([A-Za-z0-9-_\.]+)/i.test(value);
        }, "Please enter a valid pinterest URL."
                );

    });
</script>













