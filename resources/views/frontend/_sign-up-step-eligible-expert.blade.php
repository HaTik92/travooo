<div class="modal fade sign-up search" id="createEligibleExpert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content">
            <div class="modal-body body-create">
                <div class="header">
                    <img src="{{asset('assets2/image/congrats-icon.png')}}">
                </div>
                <div class="description">
                    <h1>Congratulations <span class="exp-name"></span>! </h1>
                    <div class="content">You are eligible to become a <span>Travooo Expert</span>. You can use all the tools and features provided to you to make the most of your membership. We will ask for some additional info during the signup process. A reward awaits you ahead!</div>
                    <div class="continue">Continue the Signup Process</div>
                </div>
            </div>
        </div>
    </div>
</div>




















