<div class="modal fade sign-up search res-scroll step-3-mobile" id="createAccount5_expert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content">
            <span class="skip mobile--hide">
                {{ __('Skip')}} <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
            </span>
            <div class="modal-body body-create">
                <h4 class="title">{{ __('Select your')}} <span>{{__('areas of expertise')}}</span></h4>
                <span class="step-subtitle">{{__('Enter any countries or cities where you a solid experience.')}}</span>
                <div class="top-layer">
                    <div class="form-group search">
                        <input type="text" class="form-control" id="expert_search" name="search" placeholder="{{__('Countries or cities...')}}" required>
                        <label class="expert-search-label" for="expert_search"></label>
                        <img class="search" src="{{asset('assets2/image/sign_up/search.png')}}" alt="">
                    </div>
                    <div class="step-selected-block">
                        <h3 class="selected d-none">Selected</h3>
                        <div class="form-group exp-selected">
                        </div>
                    </div>
                    <div>
                        <div>
                            <h3 class="search-result-title d-none">Search Results</h3>
                            <div class="form-group expertise-list">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer mb-21">
                <div class="continue disabled">{{__('Continue')}}</div>
                <div class="step-back-btn back-btn mobile--hide">
                    <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back
                </div>
                <span class="step-num mobile--hide"><span class="current-step">Step 4</span> of 6</span>
            </div>
            <div class='mobile--footer'>
                <div class="step-back-btn back-btn"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</div>
                <div>Step 4 <span> of 6</span></div>
                <div class="skip">Skip <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </div>
            </div>
        </div>
    </div>
</div>
