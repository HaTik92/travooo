@extends('frontend.layouts.access')

@section('access_content')
    <div class="main-wrapper login-layer">
        @include('frontend.layouts._header')
    </div>

    <!-- create an account -->
    @include('frontend._sign-up-step-confirm')
    @include('frontend._sign-up-step-eligible-expert')
    @include('frontend._sign-up-step-1')
    @include('frontend._sign-up-step-1-expert')
    @include('frontend._sign-up-step-2')
    @include('frontend._sign-up-step-3')
    @include('frontend._sign-up-step-4')
    @include('frontend._sign-up-step-4-expert')
    @include('frontend._sign-up-step-5')
    @include('frontend._sign-up-step-5-expert')
    @include('frontend._sign-up-step-6')
    @include('frontend._sign-up-step-6-expert')
    @include('frontend._sign-up-step-7')
    @include('frontend._sign-up-step-8-expert')
    @include('frontend._sign-up-step-points')
    @include('frontend._sign-up-step-9')
    @include('frontend._sign-up-step-10')


@endsection

@section('access_script')
    <script>

        $.fn.modal.Constructor.prototype._enforceFocus = function() {};
        var changeTimer = false;
        var lng_content_list = ['1'];
        var countries_for_places = [];
        var cities_for_places = [];

        $(document).ready(function() {
            var selectedType = '1';
            var showPoints = 0;
            var showTypes = 1;
            var badge = '';
            var selected = interest_list = [];
            var expertise_list = [];
            var token = '';
            var step_slider = '';
            var currentStep = 0;
            var openStep = {
                'step-2':'createAccount2',
                1 : {
                    '2':'createAccountConfirm',
                    '3':'createAccount3',
                    '4':'createAccount4',
                    '6':'createAccount6',
                    '7':'createAccount10',
                    '8':'createAccount7',
                },
                2 : {
                    '2':'createAccountConfirm',
                    '3':'createAccount4_expert',
                    '4':'createAccount3',
                    '5':'createAccount4',
                    '6':'createAccount5_expert',
                    '7':'createAccount8_expert',
                    '8':'createAccount10',
                    '9':'createAccount9',
                },
                4 : {
                    '2':'createAccountConfirm',
                    '3':'createAccount4_expert',
                    '4':'createAccount3',
                    '5':'createAccount5_expert',
                    '6':'createAccount5',
                    '7':'createAccount8_expert',
                    '8':'createAccount9',
                },
            };

            var url_string = window.location.href;
            var url = new URL(url_string);

            var url_token = url.searchParams.get("t");
            var url_step = url.searchParams.get("step");
            var url_type = url.searchParams.get("type");
            var invited = url.searchParams.get("invited");
            var signup_type = url.searchParams.get("dt");

            if (url_step !== null && url_token !== null) {
                token = url_token;
                if(url_type !== null){
                    preloadStep('createAccount3', "{{route('frontend.auth.registration.step.3')}}");
                    getLanguageSuggestion();
                    getPlacesSuggestion();

                    selectedType = (url_type == 1)?1:2;

                    if(url_step == 2){
                        timerDecrement();
                        minutesTimerDecrement();
                    }
                    if(url_type == 1 && url_step > 2){
                        showHeaderViaUrl((url_step - 2)*20)
                    }

                    if((url_type == 2 || url_type == 4) && url_step > 3){
                        showHeaderViaUrl((url_step - 3)*20)
                    }

                    $('#' + openStep[selectedType][url_step]).modal('toggle');
                }else{
                    $('#' + openStep[url_step]).modal('toggle');
                }
            }else{
                if(signup_type !== null){
                    $('div.modal').modal('hide');
                    if(signup_type === 'regular'){

                        selectedType= '1';
                        $('#createAccount1').modal('show')
                    }else{
                        selectedType = '2';
                        $('#createAccount1_expert').modal('show')
                    }
                }else{
                    $('div.modal').modal('hide');
                    $('#createAccount1').modal('show')
                }
            }


            $(document).on('click', '.signup-type-btn', function(){
                var d_type = $(this).attr('data-type');
                $('div.modal').modal('hide');
                if(d_type == 'regular'){
                    selectedType= '1';
                    bindSteps();
                    $('#createAccount1').modal('show')
                }else{
                    selectedType = '2';
                    bindSteps();
                    $('#createAccount1_expert').modal('show')
                }
            });


            var expert_email = url.searchParams.get("expert_email");
            window.history.replaceState(null, null, window.location.pathname);
            $("#createAccount1 #email").val(expert_email);


            $('.go_log_in').on('click', function() {
                window.location.href="{{url('login')}}";
            });

            $('#step2_form').validate({
                ignore:'',
                rules: {
                    email: {
                        required: true,
                        email: true,
                        custom_email: true
                    },
                    password: {
                        required: true,
                        minlength: 6,
                        password_format:true,
                        password_strong_format: true

                    },
                    confirmation_password: {
                        required: true,
                        equalTo: "#password"
                    },
                    username: {
                        required: true,
                        minlength: 4,
                        maxlength: 15,
                        lettersonly:true
                    },
                    name: {
                        required: true,
                        maxlength: 50

                    },
                    nationality: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
                    age: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: "Email is required.",
                        email: "Please enter a valid email address."
                    },
                    password: {
                        required: "Enter the password.",
                    },
                    confirmation_password: "Enter repeat password same as password",
                    username: {
                        required: "Username is required.",
                        maxlength: "Username may not be greater then 15 characters.",
                        lettersonly:"Letters only please."
                    },
                    name:{
                        required: "Full name is required.",
                        maxlength: "Full name may not be greater than 50 characters."
                    },
                    age: "Age is required.",
                    gender: "Gender is required.",
                    nationality: "Nationality is required."
                },
                errorPlacement: function(error, element) {
                    if(element.attr("name") == "gender") {
                        error.insertAfter(element.closest('.form-check.flex-custom'));
                    } else {
                        error.insertAfter(element);
                    }
                },
                onkeyup: function (element, event) {
                    if (element.id === "username" && (element.value.length > this.settings.rules.username.maxlength + 1)) {
                        element.value = element.value.slice(0, this.settings.rules.username.maxlength + 1);
                    }
                }
            });


            if (invited !== null) {
                $('div.modal').modal('hide');
                $('#createAccount1').modal('toggle');
            }


            var steps = {
                'show': {
                    'createEligibleExpert': function() {
                        return showPoints;
                    },
                },
                'all' : [
                    'createAccount1',
                    'createAccountConfirm',
                    'createAccount2'
                ],
                '1' : [
                    'createAccount1',
                    'createAccount2',
                    'createAccountConfirm',
                    'createAccount3',
                    'createAccount4',
                    'createAccount6',
                    'createAccount10',
                    'createAccount7',
                ],
                '2' : [
                    'createAccount1_expert',
                    'createAccount2',
                    'createAccountConfirm',
                    'createEligibleExpert',
                    'createAccount4_expert',
                    'createAccount3',
                    'createAccount4',
                    'createAccount5_expert',
                    'createAccount8_expert',
                    'createAccount10',
                    'createAccount9',
                ],
                '4' : [
                    'createAccount1_expert',
                    'createAccount2',
                    'createAccountConfirm',
                    'createEligibleExpert',
                    'createAccount4_expert',
                    'createAccount3',
                    'createAccount5_expert',
                    'createAccount5',
                    'createAccount8_expert',
                    'createAccountPoints',
                    'createAccount9',
                ],
            };


            $('button.already_member, button.signup-close').on('click', function() {
                window.location.href="{{url('login')}}";
            });


            var callbacks = {
                createAccount1: function (callback) {
                    callback();
                },
                createAccount1_expert: function (callback) {
                    callback();
                },
                createAccount2: function (callback) {
                    var email = $('div#createAccount2 input#email').val();
                    var pass = $('div#createAccount2 input#password').val();
                    var confirm_pass = $('div#createAccount2 input#confirmation_password').val();
                    var username = $('div#createAccount2 input#username').val();
                    var name = $('div#createAccount2 input#name').val();
                    var age = $('div#createAccount2 input#dob').val();
                    var gender = $('div#createAccount2 input[name="gender"]:checked').val();
                    var nationality = $('div#createAccount2 select#nationality').val();
                    var recaptcha = $('textarea[name=g-recaptcha-response]').val();
                    var is_back = $('div#createAccount2 input#is_back').val();

                    if (!$('#step2_form').validate().form()) {
                        return;
                    }

                    $('.createAccount2_continue').prop("disabled", true);

                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.1')}}",
                        data: { email: email, password: pass, confirmation_password: confirm_pass, gender: gender, birth_date: age, nationality: nationality, name: name, username: username, type:selectedType, is_back:is_back, recaptcha: recaptcha}
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            timerDecrement();
                            minutesTimerDecrement();

                            token = result.data.token;
                            showPoints = result.data.is_invited === true;
                            badge = result.data.badge;

                            if (showPoints && badge != '') {
                                var username = result.data.user_name;

                                $('#createEligibleExpert .exp-name').html(username);

                                showTypes = 0;
                                selectedType = '2';
                                bindSteps();

                                $('div.modal').modal('hide');
                                $('#' + steps[selectedType][2]).modal('toggle');

                            } else {
                                callback();
                            }

                            preloadStep('createAccount3', "{{route('frontend.auth.registration.step.3')}}");
                            getLanguageSuggestion();
                            getPlacesSuggestion();
                        }
                    }).fail(function( result ) {
                        grecaptcha.reset();

                        $('div#createAccount2').animate({ scrollTop: 0 }, 100);
                        $('div.error-messages').html('');
                        if (result.responseJSON.errors && result.responseJSON.errors.email) {
                            $('div.error-messages').append('<div>' + result.responseJSON.errors.email + '</div>');
                        }

                        if (result.responseJSON.errors && result.responseJSON.errors.username) {
                            $('div.error-messages').append('<div>' + result.responseJSON.errors.username + '</div>');
                        }

                        if (result.responseJSON.errors && result.responseJSON.errors.password) {
                            $('div.error-messages').append('<div>' + result.responseJSON.errors.password + '</div>');
                        }

                        if (result.responseJSON.errors && result.responseJSON.errors.recaptcha){
                            $('div.error-messages').append('<div>' + result.responseJSON.errors.recaptcha + '</div>');
                        }

                        if (result.responseJSON.errors && result.responseJSON.errors.send_mail){
                            $('div.error-messages').append('<div>' + result.responseJSON.errors.send_mail + '</div>');
                        }
                    }).always(function () {
                        $('.createAccount2_continue').prop("disabled", false)
                    });
                },
                createAccountConfirm: function (callback) {
                    var confirmationCode = $('div#createAccountConfirm input#confirmationCode').val();
                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.confirmation')}}",
                        data: { confirmation_code: confirmationCode, token: token}
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            updateFlowNumber(selectedType);
                            callback();
                        }
                    }).fail(function( result ) {
                        showErrors(result.responseJSON.errors);
                    });
                },
                createEligibleExpert: function (callback) {
                    callback();
                },
                createAccount3: function (callback) {
                    var cities =  [];
                    var countries = [];
                    $.each(selected['location'], function( index, value ) {
                        var loc_type = value.split('-');
                        if(loc_type[0] === 'city'){
                            cities.push(loc_type[1])
                        }else{
                            countries.push(loc_type[1])
                        }
                    });

                    countries_for_places = countries;
                    cities_for_places = cities;

                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.3')}}",
                        data: {cities: cities, countries: countries, type: selectedType, token: token}
                    }).done(function( result ) {
                        if (result.status === 'success') {

                            getPlacesSuggestion($('#place_search').val(), countries, cities);
                            callback();
                        }
                    }).fail(function( result ) {
                        showErrors(JSON.parse(result.responseText));
                    });
                },
                createAccount4: function (callback) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.4')}}",
                        data: {places: selected['place'], token: token}
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            callback();
                        }
                    }).fail(function( result ) {
                        showErrors(JSON.parse(result.responseText));
                    });
                },
                createAccount4_expert: function (callback) {
                    var data = {};
                    var check_val = '';

                    $('div#createAccount4_expert div.form-group.site input').each(function(i, e) {
                        var inputId = $(e).attr('id');
                        var inputVal = $(e).val();

                        if(inputVal !=''){
                            check_val = inputVal;
                            if(inputId == 'twitter'){
                                data[inputId] = 'https://twitter.com/' + inputVal;
                            }else if(inputId == 'facebook'){
                                data[inputId] = 'https://www.facebook.com/' + inputVal;
                            }else if(inputId == 'website'){
                                data[inputId] = inputVal;
                            }else{
                                data[inputId] = 'https://' + inputVal;
                            }
                        }

                    });

                    if(check_val == ''){
                        $('div#createAccount4_expert div.error-messages').html('Any one field is required.');
                        return;
                    }

                    data['token'] = token;

                    if (!$('#social_form').validate().form()) {
                        return;
                    }

                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.4.expert')}}",
                        data: data
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            callback();
                        }
                    }).fail(function( result ) {
                        showErrors(JSON.parse(result.responseText));
                    });
                },
                createAccount5: function (callback) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.5')}}",
                        data: {interests: interest_list, type:selectedType, token: token}
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            callback();
                        }
                    }).fail(function( result ) {
                        showErrors(JSON.parse(result.responseText));
                    });
                },
                createAccount5_expert: function (callback) {
                    var cities =  [];
                    var countries = [];
                    var places = [];
                    $.each(expertise_list, function( index, value ) {
                        var loc_type = value.split('-');
                        if(loc_type[0] === 'city'){
                            cities.push(loc_type[1])
                        }else if(loc_type[0] === 'place'){
                            places.push(loc_type[1])
                        }else{
                            countries.push(loc_type[1])
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.5.expert')}}",
                        data: {cities: cities, countries: countries, places: places, token: token}
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            callback();
                        }
                    }).fail(function( result ) {
                        showErrors(JSON.parse(result.responseText));
                    });
                },
                createAccount6: function (callback) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.6')}}",
                        data: {travel_styles: selected['travel_style'], token: token}
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            callback();
                        }
                    }).fail(function( result ) {
                        showErrors(JSON.parse(result.responseText));
                    });
                },
                createAccount7: function (callback) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.final')}}",
                        data: {token: token}
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            var link = "{{ url('/home')}}?sign-up=1&type=1";

                            window.location.href = link;
                        }
                    }).fail(function( result ) {
                        showErrors(JSON.parse(result.responseText));
                    });
                },
                createAccount8_expert: function (callback) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.8.expert')}}",
                        data: {travel_styles: selected['travel_style'], token: token}
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            callback();
                        }
                    }).fail(function( result ) {
                        showErrors(JSON.parse(result.responseText));
                    });
                },
                createAccount10: function(callback) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.10')}}",
                        data: {lng_content_list: lng_content_list, token: token}
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            callback();
                        }
                    }).fail(function( result ) {
                        showErrors(JSON.parse(result.responseText));
                    });
                },
                createAccount9: function(callback) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('frontend.auth.registration.step.final')}}",
                        data: {token: token}
                    }).done(function( result ) {
                        if (result.status === 'success') {
                            var sub_link = badge != '' ? '&badge='+ badge: '';
                            var link = "{{ url('/home')}}?sign-up=1&type=2" + sub_link;

                            window.location.href = link;
                        }
                    }).fail(function( result ) {
                        showErrors(JSON.parse(result.responseText));
                    });
                }
            };

            function bindSteps() {
                steps[selectedType].forEach(function(e, i) {
                    var offset = 1;

                    if (steps.show[steps[selectedType][i + 1]] && steps.show[steps[selectedType][i + 1]]() == 0) {
                        offset = 2;
                    }

                    $('div#' + e + ' .continue').off();
                    $('div#' + e + ' .continue').on('click', function() {
                        if ($(this).hasClass('disabled')) {
                            return;
                        }

                        $('div.error-messages').html('');
                        $('input.error').removeClass('error');

                        var typeSteps = steps[selectedType];

                        callbacks[e](function () {
                            updateModalTitle(selectedType);
                            $('div.modal').modal('hide');
                            showHeader(selectedType, i + offset, 'up');

                            $('#' + typeSteps[i + offset]).modal('toggle');
                            $('#' + typeSteps[i + offset]).css('overflow-y', 'auto');
                        });
                    });

                    $('div#' + e + ' .skip').off();
                    $('div#' + e + ' .skip').on('click', function() {
                        $('div.modal').modal('hide');
                        showHeader(selectedType, i + offset, 'up');
                        (selectedType == 1)? udateSteps(i+1, selectedType, 'skip'): udateSteps(i, selectedType, 'skip');
                        $('#' + steps[selectedType][i + offset]).modal('toggle');
                        $('#' + steps[selectedType][i + offset]).css('overflow-y', 'auto');
                    });


                    $('div#' + e + ' .back-btn').off();
                    $('div#' + e + ' .back-btn').on('click', function() {
                        $('div.modal').modal('hide');
                        if(i == 2){
                            $('#is_back').val(2)
                        }

                        showHeader(selectedType, i - 1, 'down');
                        (selectedType == 1)? udateSteps(i-1, selectedType, 'back'): udateSteps(i-2, selectedType, 'back');

                        if(selectedType == 2 && i == 4){
                            $('#' + steps[selectedType][i - 2]).modal('toggle');
                            $('#' + steps[selectedType][i - 2]).css('overflow-y', 'auto');
                        }else{
                            $('#' + steps[selectedType][i - 1]).modal('toggle');
                            $('#' + steps[selectedType][i - 1]).css('overflow-y', 'auto');
                        }
                    });
                });
            }

            bindSteps();

            $('div#createAccount2 .account-types .account-type').on('click', function(e) {
                $('div#createAccount2 .account-types .account-type').removeClass('active');
                $(this).addClass('active');
                selectedType = $(this).data('value');
                bindSteps();
            });

            $('div#createAccount5 div.search input').blur(function() {
                var val = $(this).val().trim();
                value = val.replace(/,/g, '', val);

                if (value !== '') {
                    $(this).removeClass('error');
                    if($.inArray(val, interest_list) == -1){
                        if(interest_list.length < 10){
                            interest_list.push(value);
                            $('div#createAccount5 div.continue').removeClass('disabled');
                            // $('div#createAccount6 div.interests div.example').addClass('d-none');
                            $('div#createAccount5 div.interests').append('<div class="interest f-input">' + value + '  <span class="close-interest-filter" data-interest-value="'+ value +'"><i class="trav-close-icon"></i></span></div>');
                            $('div#createAccount5 h3.selected').removeClass('d-none').text(interest_list.length + ' Selected');
                            $('.interest-error-block').html('');

                            $( ".suggestion-interests .example" ).each(function( index ) {
                                if($(this).find('.int-suggestion').text() == value){
                                    $(this).addClass('d-none')
                                }
                            })

                        }else{
                            $('.interest-error-block').html('The maximum tags count is 10.')
                        }
                    }

                    $(this).val('');
                }else{
                    $(this).addClass('error')
                }
            });

            $(document).on("click", ".suggestion-interests .example",function(){
                var _this = $(this);
                var text = _this.find('.int-suggestion').text();
                text = $.trim(text);


                if($.inArray(text, interest_list) == -1){
                    if(interest_list.length < 10){
                        interest_list.push(text);
                        _this.clone().appendTo('div#createAccount5 .interests').css({'padding-right':'8px','color':'#fff'}).append('<span class="close-interest-filter" data-interest-value="'+ text +'"><i class="trav-close-icon"></i></span>');
                        $(this).addClass('d-none');
                        $('div#createAccount5 h3.selected').removeClass('d-none').text(interest_list.length + ' Selected');
                        $('div#createAccount5 div.continue').removeClass('disabled')

                        if($('div#createAccount5 .suggestion-interests .example.d-none').length == 10){
                            $('.int-suggestion-title').addClass('d-none')
                        }else{
                            $('.int-suggestion-title').removeClass('d-none')
                        }

                        $('.interest-error-block').html('')
                    }else{
                        $('.interest-error-block').html('The maximum tags count is 10.')
                    }
                }
            });

            $("div#createAccount5 div.search input").keyup(function(e) {
                if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey)
                    $(this).blur();
                $(this).focus();
            });

            $("div#createAccount5 div.search input").bind("paste", function(e){
                var pastedData = e.originalEvent.clipboardData.getData('text');
                var inputVal = pastedData.replace(/,/g, '', pastedData);
                var pasteStr = inputVal.split(' ');

                $.each( pasteStr, function( key, value ) {
                    if ($.inArray(value, interest_list) == -1) {
                        if(interest_list.length < 10){
                            interest_list.push(value);
                            $('div#createAccount5 div.continue').removeClass('disabled');

                            // $('div#createAccount5 div.interests div.example').addClass('d-none');
                            $('div#createAccount5 div.interests').append('<div class="interest f-input">' + value + ' <span class="close-interest-filter" data-interest-value="'+ value +'"><i class="trav-close-icon"></i></span></div>');

                            $('div#createAccount5 h3.selected').removeClass('d-none').text(interest_list.length + ' Selected');
                            $('.interest-error-block').html('');

                            $( ".suggestion-interests .example" ).each(function( index ) {
                                if($(this).find('.int-suggestion').text() == value){
                                    $(this).addClass('d-none')
                                }
                            });

                            if($('div#createAccount5 .suggestion-interests .example.d-none').length == 10){
                                $('.int-suggestion-title').addClass('d-none')
                            }else{
                                $('.int-suggestion-title').removeClass('d-none')
                            }
                        }else{
                            $('.interest-error-block').html('The maximum tags count is 10.')
                        }

                    }

                });
                var self = $(this);
                setTimeout(function(e) {
                    self.val('');
                }, 100);
            });

            $('.res-scroll').scroll( function(){
                if($(this).scrollTop() > 50){
                    $(this).css('z-index', '999999')
                }else{
                    $(this).css('z-index', '1050')
                }
            })

            //Remove interest
            $(document).on('click', '.close-interest-filter', function(){
                var interst = $(this).attr('data-interest-value');
                interest_list = $.grep(interest_list, function(value) {
                    return value != interst;
                });
                if(interest_list.length < 10){
                    $('.interest-error-block').html('');
                    $('div#createAccount5 h3.selected').text(interest_list.length + ' Selected');
                }

                if(interest_list.length == 0){
                    $('div#createAccount5 h3.selected').addClass('d-none');
                    $('div#createAccount5 div.continue').addClass('disabled')
                }

                $('div#createAccount5 .suggestion-interests .int-suggestion').each(function(i, obj) {
                    if($(obj).text() == interst){
                        $(obj).closest('.example').removeClass('d-none')
                    }
                });

                if($('div#createAccount5 .suggestion-interests .example.d-none').length == 10){
                    $('.int-suggestion-title').addClass('d-none')
                }else{
                    $('.int-suggestion-title').removeClass('d-none')
                }

                $('div#createAccount5 h3.selected')

                $(this).closest('.interest').remove()

            });

            function preloadStep(stepId, url, search = '') {
                var modatTitle = $('div#' + stepId).find('h4.title').find('span');
                var suggestionTitle = $('div#' + stepId).find('.suggestion-title');
                $.ajax({
                    type: "GET",
                    url: url + '?search='+search+"&token="+token,
                    data: {}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        $('div#' + stepId + ' div.favourites').html('');
                        if(result.data.length > 0) {
                            var length = 4 - result.data.length % 4;

                            result.data.forEach(function (e, i) {
                                if (!(selected[e.type] && selected[e.type].includes(e.id))) {
                                    $('div#' + stepId + ' div.favourites').append('<div class="favourite" data-id="' + e.id + '" data-type="' + e.type + '" >\n' +
                                        '<div class="fav-img-wrap" style="background:url(' + e.url + ') no-repeat center center;background-size: cover;"><div class="fav-block"><div class="name">\n' +
                                        e.name +
                                        '</div>\n' +
                                        '<div class="desc">\n' +
                                        e.desc +
                                        '</div>' +
                                        '</div></div></div>');
                                }
                            });

                            for (var i = 0; i < length; i++) {
                                $('div#' + stepId + ' div.favourites').append('<div style="width: 180px;"></div>');
                            }
                        }else{
                            if (modatTitle.length) {
                                suggestionTitle.html('No ' + modatTitle.html() + ' ...');
                            } else {
                                suggestionTitle.html('No results found.')
                            }
                        }
                    }
                }).fail(function () {
                    $('div#' + stepId + ' div.favourites').html('');
                    if (modatTitle.length) {
                        suggestionTitle.html('No ' + modatTitle.html() + ' ...');
                    } else {
                        suggestionTitle.html('No results found.')
                    }
                });
            }

            function searchCity(stepId, url, search = '') {
                if (typeof searchTimer !== 'undefined') {
                    clearTimeout(searchTimer);
                }
                searchTimer = setTimeout(function () {
                    preloadStep(stepId, url, search)
                }, 500);
            }

            function searchPlace(search = '', countries = [], cities = []) {
                if (typeof searchTimer !== 'undefined') {
                    clearTimeout(searchTimer);
                }
                searchTimer = setTimeout(function () {
                    getPlacesSuggestion(search, countries, cities)
                }, 500);
            }

            function getInterestSuggestion(){
                var url = '{{route('frontend.auth.registration.step.get.5')}}';
                $.ajax({
                    type: "GET",
                    url: url + "?token="+token,
                    data: {}
                }).done(function( result ) {
                    if (result.status === 'success') {

                        $('div#createAccount5 div.suggestion-interests').html('');
                        result.data.forEach(function (e, i) {
                            $('div#createAccount5 div.suggestion-interests').append('<div class="interest example"><span class="int-suggestion">'+ e +'</span></div>');
                        });
                    }
                });
            }


            function getPlacesSuggestion(search = '', countries = [], cities = []){
                var url = "{{route('frontend.auth.registration.step.4')}}";
                $('div#createAccount4 div.favourites').html('<img src="{{asset('assets2/image/preloader.gif')}}" style="width:80px;margin:0 auto;">');
                var modalTitle = $('div#createAccount4').find('h4.title').find('span');
                var suggestionTitle = $('div#createAccount4').find('.suggestion-title');
                $.ajax({
                    type: "GET",
                    url: url + '?search='+search+"&token="+token,
                    data: {'countries': countries, 'cities': cities}
                }).done(function( result ) {

                    if(search == ''){
                        $('div#createAccount4 .suggestion-title').html('Suggestions')
                    } else {
                        $('div#createAccount4 .suggestion-title').html('Search Results')
                    }


                    if (result.status === 'success') {
                        $('div#createAccount4 div.favourites').html('');
                        if(result.data.length > 0) {
                            var length = 4 - result.data.length % 4;

                            result.data.forEach(function (e, i) {
                                if (!(selected[e.type] && selected[e.type].includes(e.id))) {
                                    $('div#createAccount4 div.favourites').append('<div class="favourite" data-id="' + e.id + '" data-type="' + e.type + '" >\n' +
                                        '<div class="fav-img-wrap" style="background:url(' + e.url + ') no-repeat center center;background-size: cover;"><div class="fav-block"><div class="name">\n' +
                                        e.name +
                                        '</div>\n' +
                                        '<div class="desc">\n' +
                                        e.desc +
                                        '</div>' +
                                        '</div></div></div>');
                                }
                            });

                            for (var i = 0; i < length; i++) {
                                $('div#createAccount4 div.favourites').append('<div style="width: 180px;"></div>');
                            }
                        }else{
                            if (modalTitle.length) {
                                suggestionTitle.html('No ' + modalTitle.html() + ' ...');
                            } else {
                                suggestionTitle.html('No results found.')
                            }
                        }
                    }
                }).fail(function () {
                    $('div#createAccount4' + ' div.favourites').html('');
                    if (modalTitle.length) {
                        suggestionTitle.html('No ' + modalTitle.html() + ' ...');
                    } else {
                        suggestionTitle.html('No results found.')
                    }
                });
            }

            function getLanguageSuggestion(){
                $('div#createAccount10 div.language-suggestion-block').html('');
                var url = '{{route('frontend.auth.registration.step.get.7')}}';
                $.ajax({
                    type: "GET",
                    url: url + "?token="+token,
                    data: {}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        result.data.forEach(function (e, i) {
                            $('div#createAccount10 div.language-suggestion-block').append('<div class="sp-lng" lngid="'+ e.id +'" lngtext="'+ e.text +'">'+  e.text +'</div>');
                        });
                    }
                });
            }

            function showErrors(result)
            {
                grecaptcha.reset();
                $('div.error-messages').html('');

                for (var field in result) {
                    $('input#' + field).addClass('error');
                    $('div.error-messages').append('<div>' + result[field][0] + '</div>');
                }
            }

            $(document).on('keyup', 'div#createAccount3 input#traveler_location_search', function() {
                var traveler_location_search = $(this).val();
                if(traveler_location_search == ''){
                    $(this).addClass('error');
                    $('div#createAccount3 .suggestion-title').html('Suggestions')
                }else{
                    $(this).removeClass('error');
                    $('div#createAccount3 .suggestion-title').html('Search Results')
                }

                searchCity('createAccount3', "{{route('frontend.auth.registration.step.3')}}", traveler_location_search);
            });

            $(document).on('keyup', 'div#createAccount4 input#place_search', function() {
                var place_search = $(this).val();
                if(place_search == ''){
                    $(this).addClass('error');
                    $('div#createAccount4 .suggestion-title').html('Suggestions')
                }else{
                    $(this).removeClass('error');
                    $('div#createAccount4 .suggestion-title').html('Search Results')
                }

                searchPlace(place_search, countries_for_places, cities_for_places)

            });

            $(document).on('keyup', 'div#createAccount6 input#style_search', function () {
                var style_search = $(this).val();
                if(style_search == ''){
                    $(this).addClass('error');
                    $('div#createAccount6 .suggestion-title').html('Suggestions')
                }else{
                    $(this).removeClass('error');
                    $('div#createAccount6 .suggestion-title').html('Search Results')
                }

                setTimeout(function() {
                    preloadStep('createAccount6', "{{route('frontend.auth.registration.step.6')}}", style_search);
                }, 300);
            });

            $(document).on('keyup', 'div#createAccount5_expert input#expert_search', function() {
                var expert_search = $(this).val();
                if(expert_search == ''){
                    $(this).addClass('error');
                    $('div#createAccount5_expert .suggestion-title').html('Suggestions')
                }else{
                    $(this).removeClass('error');
                    $('div#createAccount5_expert .suggestion-title').html('Search Results')
                }

                setTimeout(function() {
                    preloadExpertise('createAccount5_expert', "{{route('frontend.auth.registration.step.5.expert')}}", expert_search);
                }, 300);
            });

            $(document).on('keyup', 'div#createAccount6_expert input#exp_place_search', function() {
                var exp_place_search = $(this).val();
                if(exp_place_search == ''){
                    $(this).addClass('error');
                    $('div#createAccount6_expert .suggestion-title').html('Suggestions')
                }else{
                    $(this).removeClass('error');
                    $('div#createAccount6_expert .suggestion-title').html('Search Results')
                }

                setTimeout(function() {
                    preloadStep('createAccount6_expert', "{{route('frontend.auth.registration.step.4')}}", exp_place_search);
                }, 300);

            });

            $(document).on('keyup', 'div#createAccount8_expert input#exp_style_search', function() {
                var exp_style_search = $(this).val();
                if(exp_style_search == ''){
                    $(this).addClass('error');
                    $('div#createAccount8_expert .suggestion-title').html('Suggestions')
                }else{
                    $(this).removeClass('error');
                    $('div#createAccount8_expert .suggestion-title').html('Search Results')
                }

                setTimeout(function() {
                    preloadStep('createAccount8_expert', "{{route('frontend.auth.registration.step.6')}}", exp_style_search);
                }, 300);

            });




            preloadStep('createAccount6', "{{route('frontend.auth.registration.step.6')}}");
            preloadStep('createAccount6_expert', "{{route('frontend.auth.registration.step.4')}}");
            preloadStep('createAccount8_expert', "{{route('frontend.auth.registration.step.6')}}");


            function preloadExpertise(stepId, url, search) {
                $.ajax({
                    type: "GET",
                    url: url + '?search='+search+"&token="+token,
                    data: {}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        var length = 4 - result.data.length%4;
                        $('div#' + stepId + ' div.expertise-list').html('');
                        if(result.data.length > 0){
                            $('div#createAccount5_expert h3.search-result-title').removeClass('d-none')
                            result.data.forEach(function (e, i) {
                                if(!(selected[e.type] && selected[e.type].includes(e.id))){
                                    var flag_content = e.flag != ''? '<span class="exp-c-flag"><img src="'+e.flag +'"></span>':'';

                                    $('div#' + stepId + ' div.expertise-list').append('<div class="expert-item area-suggestion" data-id="'+ e.id +'" >' + flag_content +
                                        '<span class="exp-title">'+e.name +'</span>'+
                                        '</div>');
                                }

                            });
                        }else{
                            $('div#createAccount5_expert h3.search-result-title').addClass('d-none')
                        }
                    }
                });
            }


            $(document).on("click", ".expertise-list .area-suggestion",function(){
                var _this = $(this);
                var exp_id = _this.attr('data-id');
                var title = _this.find('.exp-title').text();

                if($.inArray(exp_id, expertise_list) == -1){
                    expertise_list.push(exp_id);
                    _this.clone().appendTo('div#createAccount5_expert .exp-selected').css({'padding-right':'8px','color':'#fff'}).removeClass('area-suggestion').append('<span class="close-exp-filter" data-exp-value="'+ exp_id +'"><i class="trav-close-icon"></i></span>');
                    $(this).remove();
                    $('div#createAccount5_expert h3.selected').removeClass('d-none').text(expertise_list.length + ' Selected');
                    $('div#createAccount5_expert div.continue').removeClass('disabled')
                }

                if($('.expertise-list .area-suggestion').length == 0){
                    $('div#createAccount5_expert h3.search-result-title').addClass('d-none')
                    $('div#createAccount5_expert input#expert_search').val('')
                }
            });



            $(document).on('click', '.close-exp-filter', function(){
                var expert = $(this).attr('data-exp-value');
                expertise_list = $.grep(expertise_list, function(value) {
                    return value != expert;
                });

                if(expertise_list.length == 0){
                    $('div#createAccount5_expert h3.selected').addClass('d-none');
                    $('div#createAccount5_expert div.continue').addClass('disabled')
                }

                $(this).closest('.expert-item').remove()

            });


            $('div#createAccount3').on('click', 'div.favourite', function (e) {
                selectClick(1, $('div#createAccount3'), $(this));

                if($('div#createAccount3').find('div.favourite.selected').length == 0){
                    getPlacesSuggestion();
                }
            });

            $('div#createAccount4').on('click', 'div.favourite', function (e) {
                selectClick(1, $('div#createAccount4'), $(this));
            });

            $('div#createAccount6').on('click', 'div.favourite', function (e) {
                selectClick(1, $('div#createAccount6'), $(this));
            });

            $('div#createAccount5_expert').on('click', 'div.favourite', function (e) {
                selectClick(1, $('div#createAccount5_expert'), $(this));
            });

            $('div#createAccount6_expert').on('click', 'div.favourite', function (e) {
                selectClick(1, $('div#createAccount6_expert'), $(this));
            });

            $('div#createAccount8_expert').on('click', 'div.favourite', function (e) {
                selectClick(1, $('div#createAccount8_expert'), $(this));
            });

            //Check username
            $(document).on('keyup', 'div#createAccount2 input#username', function() {
                var username = $(this).val();

                if(username == ''){
                    $(this).addClass('error');
                    setTimeout(function() {
                        $('div#createAccount2 div.uname-info').html('')
                    }, 510);
                }else{
                    $(this).removeClass('error');
                    if (!/^[A-Za-z0-9_]+$/i.test(username)) {
                        $('div#createAccount2 div.uname-info').html('Invalid format');
                        $('div#createAccount2 div.uname-info').addClass('exit');
                        $('div#createAccount2 div.uname-info').removeClass('available');
                        return;
                    }

                    setTimeout(function() {
                        if ($('#username').val().length > 3) {
                            $.ajax({
                                type: "POST",
                                url: "{{ route('frontend.auth.registration.check.username') }}",
                                data: { username:username }
                            }).done(function( result) {
                                if (result.data.type === 'exit') {
                                    $('div#createAccount2 div.uname-info').html('Unavailable');
                                    $('div#createAccount2 div.uname-info').addClass('exit');
                                    $('div#createAccount2 div.uname-info').removeClass('available')
                                }
                                if (result.data.type === 'available') {
                                    $('div#createAccount2 div.uname-info').html('available');
                                    $('div#createAccount2 div.uname-info').addClass('available');
                                    $('div#createAccount2 div.uname-info').removeClass('exit')
                                }
                            })
                        }
                    }, 500);
                }
            });

            function selectClick(minCount, modal, clicked) {
                var type =  $(clicked).data('type');
                var id = $(clicked).data('id');
                var selectedCount = 0;
                $(clicked).removeAttr('style');
                if (!selected[type]) {
                    selected[type] = [];
                }
                var el = $(clicked).detach();

                if (!$(clicked).hasClass('selected')) {
                    $(clicked).addClass('selected');
                    modal.find('.sp-selected').append(el);

                    selected[type].push(id);
                    selectedCount = selected[type].length;
                } else {
                    $(clicked).removeClass('selected');
                    modal.find('.favourites').prepend(el);

                    selected[type] = $.grep(selected[type], function(value) {
                        return value != id;
                    });
                    selectedCount = selected[type].length;

                    if(type == 'location' && selected[type].length <= 4){
                        $('.expert-search-label').html('')
                    }
                }

                if (!modal.find('.favourites').find('.favourite').length) {
                    modal.find('.favourites').html('');

                    if (modal.find('h4.title').find('span').length) {
                        modal.find('.favourites').append('<span class="selected-all">No ' + modal.find('h4.title').find('span').html() + ' ...</span>');
                    } else {
                        modal.find('.favourites').append('<span class="selected-all">You have selected all possible versions.</span>');
                    }
                } else  {
                    modal.find('.favourites').find('.selected-all').remove();
                }

                if(selectedCount > 0){
                    modal.find('.account-selected-count').removeClass('d-none');
                    modal.find('.account-selected-count').html(selectedCount + ' Selected')
                }else{
                    modal.find('.account-selected-count').addClass('d-none');
                    modal.find('.account-selected-count').html('')
                }

                var elementWidth = 180;
                if(selectedCount > 0){
                    if(step_slider.lightSlider){
                        step_slider.destroy();
                    }
                    step_slider = $(modal).find('.sp-selected').lightSlider({
                        autoWidth:true,
                        pager: false,
                        enableDrag: true,
                        // controls: true,
                        slideMargin: 20,
                        prevHtml: '<i class="trav-angle-left"></i>',
                        nextHtml: '<i class="trav-angle-right"></i>',
                        addClass: 'post-dest-slider-wrap',
                        onSliderLoad: function (el) {
                            el.css('width', (3 * elementWidth + (elementWidth * selectedCount)) + 'px')
                        },
                        responsive : [
                            {
                                breakpoint:767,
                                settings: {
                                    slideMargin:9,
                                }
                            },
                        ],
                    })
                }else{
                    step_slider.destroy();
                }

                if (selectedCount >= minCount) {
                    $(modal).find('.continue').removeClass('disabled');
                    $(modal).find('.continue span').text('Continue');
                } else {
                    $(modal).find('.continue').addClass('disabled');
                    $(modal).find('.continue span').html('Select <span class="count"></span> More');
                    $(modal).find('.continue span.count').text(minCount - selectedCount);
                }
            }


            const time = $('div.resend_confirmation_code span.seconds');
            var minutes = $('div.timer span.minutes');

            function timerDecrement() {
                setTimeout(function() {
                    const newTime = time.text() - 1;

                    time.text(newTime);

                    if (newTime > 0) {
                        timerDecrement();
                    } else {
                        $('a.resend_confirmation_code.disabled').removeClass('disabled');
                        $('div.resend_confirmation_code span').hide();
                    }

                }, 1000);
            }

            function minutesTimerDecrement() {
                setTimeout(function() {
                    const newTime = minutes.text() - 1;

                    minutes.text(newTime);

                    if (newTime > 0) {
                        minutesTimerDecrement();
                    } else {
                        $('div.timer').text('The confirmation code was expired');
                    }

                }, 60000);
            }

            $('a.resend_confirmation_code').not('disabled').on('click', function() {
                $(this).addClass('disabled');

                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.resend_confirmation_code')}}",
                    data: {token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        $('div.resend_confirmation_code span').show();
                        time.text(59);
                        timerDecrement();
                        $('div.timer').html('The confirmation code will expire in <span class="minutes">10</span> mins');
                        minutes = $('div.timer span.minutes');
                        minutesTimerDecrement();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            });

            function showHeader(s_type, step, up_down){
                var progress = $('#signup_header .progress-bar');
                var progress_val = progress.attr('aria-valuenow');
                s_type =  parseInt(s_type);

                switch(s_type) {
                    case 1:
                        if(step > 2){
                            $('.register-progress').removeClass('d-none');
                            $('.signup-menu-list').addClass('d-none');
                            if(up_down == 'up'){
                                progress.attr('aria-valuenow', parseInt(progress_val) + 20);
                                progress.css('width', (parseInt(progress_val) + 20)+'%')
                            }else{
                                progress.attr('aria-valuenow', parseInt(progress_val) - 20);
                                progress.css('width', (parseInt(progress_val) - 20)+'%')
                            }
                        }else{
                            if(!$('.register-progress').hasClass('d-none')){
                                $('.register-progress').addClass('d-none');
                                $('.signup-menu-list').removeClass('d-none');
                            }

                            progress.attr('aria-valuenow', 0);
                            progress.css('width', '0%')
                        }
                        break;
                    case 2:
                        if(step > 4){
                            $('.register-progress').removeClass('d-none');
                            $('.signup-menu-list').addClass('d-none');
                            if(up_down == 'up'){
                                progress.attr('aria-valuenow', parseInt(progress_val) + 17);
                                progress.css('width', (parseInt(progress_val) + 17)+'%')
                            }else{
                                progress.attr('aria-valuenow', parseInt(progress_val) - 17);
                                progress.css('width', (parseInt(progress_val) - 17)+'%')
                            }
                        }else{
                            if(!$('.register-progress').hasClass('d-none')){
                                $('.register-progress').addClass('d-none');
                                $('.signup-menu-list').removeClass('d-none');
                            }

                            progress.attr('aria-valuenow', 0);
                            progress.css('width', '0%')
                        }
                        break;
                }
            }

            function updateFlowNumber(type){
                if(type == 1){
                    $('#createAccount3 .step-num').html('<span class="current-step">Step 2</span> of 5');
                    $('#createAccount4 .step-num').html('<span class="current-step">Step 3</span> of 5');
                    $('#createAccount10 .step-num').html('<span class="current-step">Step 5</span> of 5')
                }else{
                    $('#createAccount3 .step-num').html('<span class="current-step">Step 2</span> of 6');
                    $('#createAccount4 .step-num').html('<span class="current-step">Step 3</span> of 6');
                    $('#createAccount10 .step-num').html('<span class="current-step">Step 6</span> of 6')
                }
            }

            function showHeaderViaUrl(width){
                $('#signup_header').removeClass('d-none');
                $('.register-progress').removeClass('d-none');
                $('.signup-menu-list').addClass('d-none');
                var progress = $('#signup_header .progress-bar');
                var progress_val = progress.attr('aria-valuenow');

                progress.attr('aria-valuenow', width);
                progress.css('width', width +'%')
            }

            function udateSteps(step, type, step_type){
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.update.step')}}",
                    data: {step: step, type: type, step_type:step_type, token: token}
                }).done(function( result ) {
                });
            }

            function updateModalTitle(type){
                if(type == 1){
                    $('.dynamic-top-layer').html('Create a free account');
                    $('.dynamic-bottom-block').html(' <span class="exp-text-title">Create an </span><span class="exp-text-desc">Expert Account</span><span class="exp-icon">EXP</span>')
                    $('.dynamic-bottom-block').attr('data-type', 'expert')
                }else{
                    $('.dynamic-top-layer').html('Become a Travooo Expert');
                    $('.dynamic-bottom-block').html(' <span class="exp-text-title">Create a </span><span class="exp-text-desc">Personal Account</span>')
                    $('.dynamic-bottom-block').attr('data-type', 'regular')
                }
            }


            //Current language script
            $(document).on('keyup', '#setting_content_language', function (e) {
                var search_item = $(this).val();
                if (search_item != '') {
                    if(changeTimer !== false) clearTimeout(changeTimer);
                    changeTimer = setTimeout(function(){
                        getRegionalContent(search_item);
                        changeTimer = false;
                    },300);

                }
            });

            $(document).on('focus', '#setting_content_language', function (e) {
                getRegionalContent();
            });

            $(document).on('click', '.close-signup-btn', function (e) {
               window.location.href = '/login';
            });

            //select regional content
            $(document).on('click', '.content-item-wrapper', function (e) {
                var id = $(this).attr('itemId');
                var text = $(this).text();

                $('#setting_content_language').val('');
                $('.regional-content-ui').hide();
                if ($.inArray(id, lng_content_list) == -1) {
                    lng_content_list.push(id);
                    $('#createAccount10 .continue').removeClass('disabled');
                    $('#content_languages').val(lng_content_list.join(','));
                    $('.reg-content-list').prepend('<li class="" lngId="'+ id +'"><span>'+ text +'</span><a href="#" class="handle"><img src="{{asset('assets2/image/move.png')}}"></a><a href="javascript:;" class="remove-content-lng" data-id="'+ id +'"><i class="trav-close-icon"></i></a></li>');

                    reloadSortable();
                }
            });

            //select suggestion language
            $(document).on('click', '.sp-lng', function (e) {
                var id = $(this).attr('lngid');
                var text = $(this).attr('lngtext');

                $('#setting_content_language').val('');
                if ($.inArray(id, lng_content_list) == -1) {
                    lng_content_list.push(id);
                    $('#content_languages').val(lng_content_list.join(','));
                    $('#createAccount10 .continue').removeClass('disabled');
                    $(this).remove();
                    $('.reg-content-list').prepend('<li class="from-suggestion" lngId="'+ id +'"><span>'+ text +'</span><a href="#" class="handle"><img src="{{asset('assets2/image/move.png')}}"></a><a href="javascript:;" class="remove-content-lng" data-id="'+ id +'"><i class="trav-close-icon"></i></a></li>');

                    if($('.language-suggestion-block .sp-lng').length == 0){
                        $('div#createAccount10 h3.suggestion-title').hide()
                    }
                    reloadSortable();
                }
            });

            //Remove content language
            $(document).on('click', '.remove-content-lng', function () {
                var lng_id = $(this).attr('data-id');
                lng_content_list = $.grep(lng_content_list, function (value) {
                    return value != lng_id;
                });

                if(lng_content_list.length == 1){
                    $('#createAccount10 .continue').addClass('disabled')
                }

                $('#content_languages').val(lng_content_list.join(','));

                if($(this).closest('li').hasClass('from-suggestion')){
                    var text = $(this).closest('li').find('span').text();
                    $('div#createAccount10 h3.suggestion-title').show();
                    $('div#createAccount10 div.language-suggestion-block').append('<div class="sp-lng" lngid="'+ lng_id +'" lngtext="'+ text +'">'+  text +'</div>');
                }

                $(this).closest('li').remove()
            });

            //Get regional content languages
            function getRegionalContent(item) {
                $('.regional-content-ui').html('');
                $('.regional-content-ui').show();
                $.get("{{route('frontend.auth.registration.get.current.languages')}}", {q: item, token: token}, function (data) {
                    var data = JSON.parse(data);

                    data.forEach(function (s_item, index) {
                        $('.regional-content-ui').append('<li class="reg-ui-item"><div class="content-item-wrapper" itemId="' + s_item.id + '">' + s_item.text + '</div></li>')
                    });
                });
            }

            function reloadSortable(){
                $( ".reg-content-list" ).sortable({
                    handle: ".handle",
                    containment: "parent",
                    items: "> li",
                    opacity: 0.5,
                    tolerance: "pointer",
                    stop: function( event, ui ){
                        var sortedIDs = $( ".reg-content-list" ).sortable("toArray", {attribute:"lngId"});
                        lng_content_list = sortedIDs;
                        $('#content_languages').val(lng_content_list.join(','))
                    },
                });

                var sortedIDs = $( ".reg-content-list" ).sortable("toArray", {attribute:"lngId"});
                lng_content_list = sortedIDs;
                $('#content_languages').val(lng_content_list.join(','))
            }

            $(document).click(function (event) {
                $target = $(event.target);
                if (!$target.closest('.sp-language-content').length) {
                    $('.regional-content-ui').hide();
                }
            });

            $.validator.addMethod("password_format", function (value, element) {
                    return this.optional(element) ||/^(?=.*[A-Za-z])(?=.*\d)[a-zA-Z0-9!@#$%^&*]{6,}$/i.test(value);
                },
                "Password should contain 6 minimum characters consisting of at least 1 letter and number."
            );

            $.validator.addMethod("password_strong_format", function (value, element) {
                    var simple_pass_list = ['123qwe', '12qwert', '12qw12qw', '1234qwer'];
                    return this.optional(element) || ($.inArray( value, simple_pass_list ) == -1);
                },
                "That password is too easy. Please choose a stronger password."
            );

            $.validator.addMethod("custom_email",
                function(value, element) {
                    return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
                },
                "Please enter a valid email address."
            );
        });

        $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9_]+$/i.test(value)
        }, "Letters only please");
    </script>

@endsection
