<div class="modal fade sign-up res-scroll" id="createAccountConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content step-content">
            <div class="modal-body body-create">
                <div class="description">
                    <h1>We have sent you a confirmation email</h1>
                    <div class="content">To verify we have your correct email address, please enter the code from email</div>
                    <div class="error-messages"></div>
                    <input id="confirmationCode" name="confirmation_code" class="confirmation-code" placeholder="Code"/>
                    <div class="form-group resend_confirmation_code">
                        <a href="#" class="resend_confirmation_code disabled">Resend confirmation code</a>
                        <span>(will be available after <span class="seconds">59</span> sec)</span>
                    </div>
                    <div class="form-group timer">The confirmation code will expire in <span class="minutes">10</span> mins</div>
                    <div class="confirm-btn continue">Confirm</div>
                    <div class="form-group">
                        <div class="signup-back-btn back-btn mb-3">
                            <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


















