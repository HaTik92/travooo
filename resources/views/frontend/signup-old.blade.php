@extends('frontend.layouts.access')

@section('access_content')
<div class="main-wrapper login-layer">
 @include('frontend.layouts._header')
 </div>

<div class="modal fade sign-up pt-70" id="createAccount1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content">
            <div class="modal-body body-create">
                <div class="top-layer">
                    <a href="#" class="logo-wrap">
                        <img src="frontend_assets/image/main-logo.png" alt="">
                        <img src="frontend_assets/image/main-logo-white.png" class="mobile-main-logo" alt="">
                    </a>
                    <h4 class="title">{{ __('Create an Account')}}</h4>
                    <h5 class="title">{{ __('Welcome to Travooo')}}</h5>
                    <div class="error-messages"></div>
                </div>
                <form class="login-form" method="post" action="" id="step1_form">
                    <div class="step-1">
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" autocomplete="off" name="email" aria-describedby="emailHelp" placeholder="{{ __('Email address')}}" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" autocomplete="off" name="password" aria-describedby="pass" placeholder="{{ __('Password')}}" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="confirmation_password" name="confirmation_password" aria-describedby="pass" placeholder="{{ __('Repeat Password')}}" required>
                        </div>

                        @if(config('captcha.key'))
                            <div class="g-recaptcha form-group"
                                 data-sitekey="{{config('captcha.key')}}">
                            </div>
                        @endif

                        <div class="form-margin"></div>
                        <div class="form-group">
                            <button type="button" class="btn btn-success createAccount1_continue continue">
                                <span class="spinner-border spinner-border-sm d-none createAccount1_spiner" role="status" aria-hidden="true"></span>  {{ __('Continue')}}</button>
                        </div>
                        <div class="form-group social">
                            <p class="simple-txt">{{ __('Or')}}</p>
                        </div>
                        <div class="form-group social">
                            <button type="button" onclick="window.location.href='{{ url("/facebook/redirect") }}'" class="btn btn-primary facebook_login"><i class="fa fa-facebook side-icon"></i>{{ __('Continue with Facebook')}}</button>
                        </div>
                        <div class="form-group social">
                            <button type="button" onclick="window.location.href='{{url("/twitter/redirect")}}'" class="btn btn-info twitter_login"><i class="fa fa-twitter side-icon"></i>{{ __('Continue with Twitter')}}</button>
                        </div>
                    </div>
                    <div class="form-group bottom-txt-group">
                        <p class="bottom-txt">{{ __('Creating an account means you\'re okay with')}}</p>
                        <ul class="link-list">
                            <li><b>{{ __('Travooo\'s')}}</b></li>
                            <li><a href="#"> {{ __('Terms of Service')}}</a>,</li>
                            <li><a href="#"> {{ __('Privacy Policy')}}</a></li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <p class="foot-txt">{{ __('Already a member?')}}</p>
                <button type="submit" class="btn btn-grey already_member">{{ __('Log In')}}</button>
            </div>
        </div>
    </div>
</div>

<!-- create an account -->
@include('frontend._sign-up-step-confirm')
@include('frontend._sign-up-step-eligible-expert')
@include('frontend._sign-up-step-2')
@include('frontend._sign-up-step-3')
@include('frontend._sign-up-step-3-company')
@include('frontend._sign-up-step-4')
@include('frontend._sign-up-step-4-expert')
@include('frontend._sign-up-step-5')
@include('frontend._sign-up-step-5-expert')
@include('frontend._sign-up-step-6')
@include('frontend._sign-up-step-6-expert')
@include('frontend._sign-up-step-7')
@include('frontend._sign-up-step-8-expert')
@include('frontend._sign-up-step-points')
@include('frontend._sign-up-step-9')


@endsection

@section('access_script')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    
$.fn.modal.Constructor.prototype._enforceFocus = function() {};

    $(document).ready(function() {
        var selectedType = '1';
        var showPoints = 0;
        var showTypes = 1;
        var selected = interest_list = [];
        var token = '';
        var currentStep = 0;
        var openStep = {
            'step-2':'createAccount2',
            1 : {
                '3':'createAccount3',
                '4':'createAccount4',
                '5':'createAccount5',
                '6':'createAccount6',
                '7':'createAccount7',
                '8':'createAccount9',
            },
            2 : {
                '3':'createAccount3',
                '4':'createAccount4_expert',
                '5':'createAccount5_expert',
                '6':'createAccount6_expert',
                '7':'createAccount6',
                '8':'createAccount8_expert',
                '9':'createAccount9',
            }
        };
        
        var url_string = window.location.href;
        var url = new URL(url_string);
    
        var url_token = url.searchParams.get("t");
        var url_step = url.searchParams.get("step")
        var url_type = url.searchParams.get("type")
        var invited = url.searchParams.get("invited");
        var signup_type = url.searchParams.get("dt");

        if (url_step !== null && url_token !== null) {
           token = url_token;
             if(url_type !== null){
                 selectedType = url_type
                 $('#' + openStep[url_type][url_step]).modal('toggle');
                 
                 if(url_type == 1 && url_step > 3){
                     showHeaderViaUrl((url_step - 3)*20)
                 }
                 
                 if(url_type == 2 && url_step > 4){
                     showHeaderViaUrl((url_step - 4)*20)
                 }
             }else{
                  $('#' + openStep[url_step]).modal('toggle');
             }
        }else{
            $('#createAccount1').modal('toggle');
        }
        
        console.log('signup_type', signup_type)
        
        var expert_email = url.searchParams.get("expert_email");
        window.history.replaceState(null, null, window.location.pathname);
        $("#createAccount1 #email").val(expert_email);
        
        $('#step1_form').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6,
                    password_format:true
                    
                },
                confirmation_password: {
                    required: true,
                    equalTo: "#password"
                },
            },
            messages: {
                email: {
                        required: "The email is required.",
                        email: "Please enter a valid email address."
                        },
                password: {
                        required: "The password is required.",
                        minlength: "Please enter at least 6 characters."
                        },
                confirmation_password: " Enter repeat password same as password"
            }
        });
        
        $('#step3_form').validate({
            rules: {
                username: {
                    required: true,
                    maxlength: 15,
                    lettersonly:true
                },
                name: {
                    required: true,
                    maxlength: 50
                    
                },
                nationality: {
                    required: true
                },
                gender: {
                    required: true
                },
                age: {
                    required: true
                }
            },
            messages: {
               username:{
                        required: "The uername is required.",
                        maxlength: "The username may not be greater than 15 characters.",
                        lettersonly:"Letters only please."
                        },
               name:"",
               age:"",
               nationality:"The nationality is required."
            }
        });
        
        $('#company_step3_form').validate({
            rules: {
                company_name: {
                    required: true,
                    maxlength: 50
                },
                c_username: {
                    required: true,
                    maxlength: 15
                },
                website: {
                     website: true,
                },
                age: {
                    required: true
                }
            },
            messages: {
               company_name:{
                        required: "The Company/Organisation name is required.",
                        maxlength: "The Company/Organisation name may not be greater than 50 characters."
                        },
               username:{
                        required: "The uername is required.",
                        maxlength: "The username may not be greater than 15 characters."
                        },
                age:"The Founding Date is required."
            }
        });

        if (invited !== null) {
            $('div.modal').modal('hide');
            $('#createAccount1').modal('toggle');
        }

   
        var steps = {
            'show': {
                'createAccountPoints': function () {
                    return showPoints;
                },
                'createAccount2': function() {
                    return showTypes
                },
                'createEligibleExpert': function() {
                    return showPoints;
                },
                'createAccount4_expert': function() {
                    return showTypes
                }
            },
            'all' : [
                'createAccount1',
                'createAccountConfirm',
                'createAccount2'
            ],
            '1' : [
                'createAccount1',
                'createAccountConfirm',
                'createAccount2',
                'createAccount3',
                'createAccount4',
                'createAccount5',
                'createAccount6',
                'createAccount7',
                'createAccount9',
            ],
            '2' : [
                'createAccount1',
                'createAccountConfirm',
                'createEligibleExpert',
                'createAccount2',
                'createAccount3',
                'createAccount4_expert',
                'createAccount5_expert',
                'createAccount6_expert',
                'createAccount6',
                'createAccount8_expert',
                'createAccountPoints',
                'createAccount9',
            ],
            '3' : [
                'createAccount1',
                'createAccountConfirm',
                'createAccount2',
                'createAccount3_company',
                'createAccount4',
                'createAccount5',
                'createAccount6',
                'createAccount7',
                'createAccount9'
            ]
        };
        

        $('button.already_member, button.signup-close').on('click', function() {
           window.location.href="{{url('login')}}";
        });


        var callbacks = {
            createAccount1: function (callback) {
                var email = $('div#createAccount1 input#email').val()
                var pass = $('div#createAccount1 input#password').val()
                var confirm_pass = $('div#createAccount1 input#confirmation_password').val()
                var recaptcha = $('textarea[name=g-recaptcha-response]').val();
                
                if (!$('#step1_form').validate().form()) {
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.1')}}",
                    data: { email: email, password: pass, confirmation_password: confirm_pass, recaptcha: recaptcha}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        timerDecrement();
                        minutesTimerDecrement();

                        token = result.data.token;
                        showPoints = result.data.is_invited === true;

                        if (showPoints) {
                            showTypes = 0;
                            selectedType = '2';
                            bindSteps();

                            $('div.modal').modal('hide');
                            $('#' + steps[selectedType][1]).modal('toggle');

                            $('div#createAccountPoints div.content span.points').text(result.data.points + ' points');
                        } else {
                            callback();
                        }
                    }
                }).fail(function( result ) {
                    if (result.responseJSON.errors && result.responseJSON.errors.email) {
                        $('div.error-messages').append('<div>' + result.responseJSON.errors.email + '</div>');
                    }
                    
                    if(result.responseJSON.errors && result.responseJSON.errors.recaptcha){
                        $('div.error-messages').append('<div>' + result.responseJSON.errors.recaptcha + '</div>');
                    }
                });
            },
            createAccountConfirm: function (callback) {
                var confirmationCode = $('div#createAccountConfirm input#confirmationCode').val()
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.confirmation')}}",
                    data: { confirmation_code: confirmationCode, token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createEligibleExpert: function (callback) {
                callback();
            },
            createAccount2: function (callback) {
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.2')}}",
                    data: { type: selectedType, token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createAccount3: function (callback) {
                var username = $('div#createAccount3 input#username').val()
                var name = $('div#createAccount3 input#name').val()
                var age = $('div#createAccount3 input#dob').val()
                var gender = $('div#createAccount3 input[name="gender"]:checked').val();
                var nationality = $('div#createAccount3 select#nationality').val();
                
                if (!$('#step3_form').validate().form()) {
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.3')}}",
                    data: { gender: gender, birth_date: age, nationality: nationality, name: name, username: username, token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                        preloadStep('createAccount4', "{{route('frontend.auth.registration.step.4')}}", "");
                    }
                }).fail(function( result ) {
                    if (result.responseJSON.errors && result.responseJSON.errors.gender) {
                         $('div.error-messages').append('<div>' +  result.responseJSON.errors.gender + '</div>');
                    }
                });
            },
            createAccount3_company: function (callback) {
                var name = $('div#createAccount3_company input#company_name').val();
                var username = $('div#createAccount3_company input#c_username').val();
                var website = $('div#createAccount3_company input#company_website').val();
                var age = $('div#createAccount3_company input#age').val();
                var nationality = $('div#createAccount3_company select#company_nationality').val();
                
                if (!$('#company_step3_form').validate().form()) {
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.3.company')}}",
                    data: { website: website, birth_date: age, nationality: nationality, name: name, username: username, token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createAccount4: function (callback) {
                var cities =  [];
                var countries = [];
                $.each(selected['location'], function( index, value ) {
                    var loc_type = value.split('-')
                    if(loc_type[0] === 'city'){
                        cities.push(loc_type[1])
                    }else{
                        countries.push(loc_type[1])
                    }
                  });
                  
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.4')}}",
                    data: {cities: cities, countries: countries, token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createAccount4_expert: function (callback) {
                var data = {};
                var check_val = '';

                $('div#createAccount4_expert div.form-group.site input').each(function(i, e) {
                    var inputId = $(e).attr('id');
                    var inputVal = $(e).val();
                    
                    if(inputVal !=''){
                        check_val = inputVal;
                    }

                    data[inputId] = inputVal;
                });
                
                if(check_val == ''){
                    $('div#createAccount4_expert div.error-messages').html('Any one field is required.');
                    return;
                };

                data['token'] = token;
                
                if (!$('#social_form').validate().form()) {
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.4.expert')}}",
                    data: data
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createAccount5: function (callback) {
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.5')}}",
                    data: {places: selected['place'], token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createAccount5_expert: function (callback) {
                var cities =  [];
                var countries = [];
                $.each(selected['location'], function( index, value ) {
                    var loc_type = value.split('-')
                    if(loc_type[0] === 'city'){
                        cities.push(loc_type[1])
                    }else{
                        countries.push(loc_type[1])
                    }
                  });
                  
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.5.expert')}}",
                    data: {cities: cities, countries: countries, token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createAccount6_expert: function (callback) {
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.6.expert')}}",
                    data: {places: selected['place'], token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createAccount6: function (callback) {
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.6')}}",
                    data: {interests: interest_list, type:selectedType, token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createAccount7: function (callback) {
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.7')}}",
                    data: {travel_styles: selected['travel_style'], token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createAccount8_expert: function (callback) {
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.8.expert')}}",
                    data: {travel_styles: selected['travel_style'], token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        callback();
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            },
            createAccountPoints: function(callback) {
                callback();
            },
            createAccount9: function(callback) {
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.step.9')}}",
                    data: {token: token}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        var link = "{{ url('/home') }}";

                        if (selectedType == 3) {
                            link = "{{ url('/home') }}"
                        }

                        window.location.href = link;
                    }
                }).fail(function( result ) {
                    showErrors(JSON.parse(result.responseText));
                });
            }
        };

        function bindSteps() {
            steps[selectedType].forEach(function(e, i) {
                var offset = 1;

                if (steps.show[steps[selectedType][i + 1]] && steps.show[steps[selectedType][i + 1]]() == 0) {
                    offset = 2;
                }

                $('div#' + e + ' .continue').off();
                $('div#' + e + ' .continue').on('click', function() {                   
                    if ($(this).hasClass('disabled')) {
                        return;
                    }
                 
                    $('div.error-messages').html('');
                    $('input.error').removeClass('error');

                    var typeSteps = steps[selectedType];

                    callbacks[e](function () {
                        $('div.modal').modal('hide');
                        showHeader(selectedType, i + offset, 'up');
                        
                        $('#' + typeSteps[i + offset]).modal('toggle');
                        $('#' + typeSteps[i + offset]).css('overflow-y', 'auto');
                    });
                });
         
                $('div#' + e + ' .skip').off()
                $('div#' + e + ' .skip').on('click', function() {
                    $('div.modal').modal('hide');
                    showHeader(selectedType, i + offset, 'up');
                    (selectedType == 1)? udateSteps(i+1, selectedType, 'skip'): udateSteps(i, selectedType, 'skip')
                    $('#' + steps[selectedType][i + offset]).modal('toggle');
                    $('#' + steps[selectedType][i + offset]).css('overflow-y', 'auto');
                });
                
                
                $('div#' + e + ' .back-btn').off()
                $('div#' + e + ' .back-btn').on('click', function() {
                    $('div.modal').modal('hide');
                    showHeader(selectedType, i - 1, 'down');
                   (selectedType == 1)? udateSteps(i-1, selectedType, 'back'): udateSteps(i-2, selectedType, 'back')
                    $('#' + steps[selectedType][i - 1]).modal('toggle');
                    $('#' + steps[selectedType][i - 1]).css('overflow-y', 'auto');
                });
            });
        }

        bindSteps();

        $('div#createAccount2 .account-types .account-type').on('click', function(e) {
            $('div#createAccount2 .account-types .account-type').removeClass('active');
            $(this).addClass('active');
            selectedType = $(this).data('value');
            bindSteps();
        });

        $('div#createAccount6 div.search input').blur(function() {
            var val = $(this).val().trim();
            value = val.replace(/,/g, '', val);
            
            if (value !== '') {
                $(this).removeClass('error')
                if($.inArray(val, interest_list) == -1){
                    if(interest_list.length < 10){
                        interest_list.push(value); 
                        $('div#createAccount6 div.continue').removeClass('disabled')
                        // $('div#createAccount6 div.interests div.example').addClass('d-none');
                        $('div#createAccount6 div.interests').append('<div class="interest">' + value + '  <span class="close-interest-filter" data-interest-value="'+ value +'"><i class="trav-close-icon"></i></span></div>');
                        $('div#createAccount6 h3.selected').removeClass('d-none').text(interest_list.length + ' Selected');
                        $('.interest-error-block').html('')
                    }else{
                       $('.interest-error-block').html('The maximum tags count is 10.') 
                    }
                }
                
                $(this).val('');
            }else{
                $(this).addClass('error')
            }
        });

        $(document).on("click", ".suggestion-interests .example",function(){
            var _this = $(this);
            var text = _this.find('.int-suggestion').text();
            text = $.trim(text);
            _this.find('span.pl-2').attr('data-interest-value',text).addClass('close-interest-filter').removeClass('pl-2');
            
            if($.inArray(text, interest_list) == -1){
                if(interest_list.length < 10){
                    interest_list.push(text); 
                    
                    _this.clone().appendTo('div#createAccount6 .interests');
                    $(this).addClass('d-none')
                    $('div#createAccount6 h3.selected').removeClass('d-none').text(interest_list.length + ' Selected');
                     $('div#createAccount6 div.continue').removeClass('disabled')
                     
                     if($('div#createAccount6 .suggestion-interests .example.d-none').length == 6){
                         $('.int-suggestion-title').addClass('d-none')
                     }else{
                        $('.int-suggestion-title').removeClass('d-none') 
                     }

                $('.interest-error-block').html('')
                }else{
                    $('.interest-error-block').html('The maximum tags count is 10.') 
                }
            }
        });
                
        $("div#createAccount6 div.search input").keyup(function(e) {
            if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey)
                $(this).blur();
                $(this).focus();
        });
        
        $("div#createAccount6 div.search input").bind("paste", function(e){
            var pastedData = e.originalEvent.clipboardData.getData('text');
            var inputVal = pastedData.replace(/,/g, '', pastedData);
            var pasteStr = inputVal.split(' ');

            $.each( pasteStr, function( key, value ) {
            if ($.inArray(value, interest_list) == -1) {
                if(interest_list.length < 10){
                       interest_list.push(value)
                       $('div#createAccount6 div.continue').removeClass('disabled')
                       
                        // $('div#createAccount6 div.interests div.example').addClass('d-none');
                        $('div#createAccount6 div.interests').append('<div class="interest">' + value + ' <span class="close-interest-filter" data-interest-value="'+ value +'"><i class="trav-close-icon"></i></span></div>');
                        
                        $('div#createAccount6 h3.selected').removeClass('d-none').text(interest_list.length + ' Selected');
                        $('.interest-error-block').html('')
                }else{
                    $('.interest-error-block').html('The maximum tags count is 10.')
                }

                }

            });
            var self = $(this);
              setTimeout(function(e) {
                  self.val('');
              }, 100);
        });
        
    //Remove interest
    $(document).on('click', '.close-interest-filter', function(){
        var interst = $(this).attr('data-interest-value');
        interest_list = $.grep(interest_list, function(value) {
            return value != interst;
          });
          if(interest_list.length < 10){
               $('.interest-error-block').html('')
               $('div#createAccount6 h3.selected').text(interest_list.length + ' Selected');
          }
          
          if(interest_list.length == 0){
             $('div#createAccount6 h3.selected').addClass('d-none');
             $('div#createAccount6 div.continue').addClass('disabled')
          }
          
          $('div#createAccount6 .suggestion-interests .int-suggestion').each(function(i, obj) {
                if($(obj).text() == interst){
                    $(obj).closest('.example').removeClass('d-none')
                }
          });
          
           if($('div#createAccount6 .suggestion-interests .example.d-none').length == 6){
                $('.int-suggestion-title').addClass('d-none')
            }else{
               $('.int-suggestion-title').removeClass('d-none') 
            }
          
           $('div#createAccount6 h3.selected')
          
        $(this).closest('.interest').remove()

    });

        function preloadStep(stepId, url, search = '') {
            $.ajax({
                type: "GET",
                url: url + '?search='+search+"&token="+token,
                data: {}
            }).done(function( result ) {
                if (result.status === 'success') {
                    var length = 4 - result.data.length%4;
                    $('div#' + stepId + ' div.favourites').html('');
                    result.data.forEach(function (e, i) {
                        if(!(selected[e.type] && selected[e.type].includes(e.id))){
                            $('div#' + stepId + ' div.favourites').append('<div class="favourite" data-id="'+ e.id +'" data-type="'+ e.type +'" style="background:linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)), url(' + e.url + ') no-repeat center center #000; background-size: cover;">\n' +
                                '<div class="name">\n' +
                                e.name +
                                '</div>\n' +
                                '<div class="desc">\n' +
                                e.desc +
                                '</div>' +
                                '</div>');
                        }

                    });

                    for (var i = 0; i < length; i++) {
                        $('div#' + stepId + ' div.favourites').append('<div style="width: 180px;"></div>');
                    }
                }
            });
        }

        function showErrors(result)
        {
            grecaptcha.reset();
            $('div.error-messages').html('');
            
            for (var field in result) {
                $('input#' + field).addClass('error');
                $('div.error-messages').append('<div>' + result[field][0] + '</div>');
            }
        }

        $(document).on('keyup', 'div#createAccount4 input#traveler_location_search', function() {
            var traveler_location_search = $(this).val()
            if(traveler_location_search == ''){
                $(this).addClass('error')
                $('div#createAccount4 .suggestion-title').html('Suggestions')
            }else{
                $(this).removeClass('error')
                $('div#createAccount4 .suggestion-title').html('Search Results')
            }
            
            setTimeout(function() {
                 preloadStep('createAccount4', "{{route('frontend.auth.registration.step.4')}}", traveler_location_search);
            }, 300);
           
        });

        $(document).on('keyup', 'div#createAccount5 input#place_search', function() {
            var place_search = $(this).val()
            if(place_search == ''){
                $(this).addClass('error')
                $('div#createAccount5 .suggestion-title').html('Suggestions')
            }else{
                $(this).removeClass('error')
                $('div#createAccount5 .suggestion-title').html('Search Results')
            }
            
            setTimeout(function() {
                  preloadStep('createAccount5', "{{route('frontend.auth.registration.step.5')}}", place_search);
            }, 300);
           
        });
        
        $(document).on('keyup', 'div#createAccount7 input#style_search', function () {
            var style_search = $(this).val()
            if(style_search == ''){
                $(this).addClass('error')
                $('div#createAccount7 .suggestion-title').html('Suggestions')
            }else{
                $(this).removeClass('error')
                $('div#createAccount7 .suggestion-title').html('Search Results')
            }
            
            setTimeout(function() {
                 preloadStep('createAccount7', "{{route('frontend.auth.registration.step.7')}}", style_search);
            }, 300);
        })

        $(document).on('keyup', 'div#createAccount5_expert input#expert_search', function() {
            var expert_search = $(this).val()
            if(expert_search == ''){
                $(this).addClass('error')
                $('div#createAccount5_expert .suggestion-title').html('Suggestions')
            }else{
                $(this).removeClass('error')
                $('div#createAccount5_expert .suggestion-title').html('Search Results')
            }
            
            setTimeout(function() {
                 preloadStep('createAccount5_expert', "{{route('frontend.auth.registration.step.4')}}", expert_search);
            }, 300);
        });

        $(document).on('keyup', 'div#createAccount6_expert input#exp_place_search', function() {
            var exp_place_search = $(this).val()
            if(exp_place_search == ''){
                $(this).addClass('error')
                $('div#createAccount6_expert .suggestion-title').html('Suggestions')
            }else{
                $(this).removeClass('error')
                $('div#createAccount6_expert .suggestion-title').html('Search Results')
            }
            
            setTimeout(function() {
                 preloadStep('createAccount6_expert', "{{route('frontend.auth.registration.step.5')}}", exp_place_search);
            }, 300);
            
        });

        $(document).on('keyup', 'div#createAccount8_expert input#exp_style_search', function() {
            var exp_style_search = $(this).val()
             if(exp_style_search == ''){
                $(this).addClass('error')
                $('div#createAccount8_expert .suggestion-title').html('Suggestions')
            }else{
                $(this).removeClass('error')
                $('div#createAccount8_expert .suggestion-title').html('Search Results')
            }
            
            setTimeout(function() {
                preloadStep('createAccount8_expert', "{{route('frontend.auth.registration.step.7')}}", exp_style_search);
            }, 300);
           
        });


        preloadStep('createAccount4', "{{route('frontend.auth.registration.step.4')}}");
        preloadStep('createAccount5', "{{route('frontend.auth.registration.step.5')}}");
        preloadStep('createAccount7', "{{route('frontend.auth.registration.step.7')}}");
        preloadStep('createAccount5_expert', "{{route('frontend.auth.registration.step.4')}}");
        preloadStep('createAccount6_expert', "{{route('frontend.auth.registration.step.5')}}");
        preloadStep('createAccount8_expert', "{{route('frontend.auth.registration.step.7')}}");

        $('div#createAccount4').on('click', 'div.favourite', function (e) {
            selectClick(3, $('div#createAccount4'), $(this));
        });

        $('div#createAccount5').on('click', 'div.favourite', function (e) {
            selectClick(3, $('div#createAccount5'), $(this));
        });

        $('div#createAccount7').on('click', 'div.favourite', function (e) {
            selectClick(1, $('div#createAccount7'), $(this));
        });

        $('div#createAccount5_expert').on('click', 'div.favourite', function (e) {
            selectClick(3, $('div#createAccount5_expert'), $(this));
        });

        $('div#createAccount6_expert').on('click', 'div.favourite', function (e) {
            selectClick(3, $('div#createAccount6_expert'), $(this));
        });

        $('div#createAccount8_expert').on('click', 'div.favourite', function (e) {
            selectClick(1, $('div#createAccount8_expert'), $(this));
        });
        
        //Check username
        $(document).on('keyup', 'div#createAccount3 input#username', function() {
            var username = $(this).val()
            
            if(username == ''){
                $(this).addClass('error')
                setTimeout(function() {
                  $('div#createAccount3 div.uname-info').html('')
                }, 510);
            }else{
                $(this).removeClass('error')
                if (!/^[A-Za-z0-9_]+$/i.test(username)) {
                    $('div#createAccount3 div.uname-info').html('Invalid format')
                    $('div#createAccount3 div.uname-info').addClass('exit')
                    $('div#createAccount3 div.uname-info').removeClass('available')
                    return;
                }
                
                setTimeout(function() {
                $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.check.username')}}",
                    data: {username:username, token: token}
                }).done(function( result) {
                    if (result.data.type === 'exit') {
                       $('div#createAccount3 div.uname-info').html('exit')
                       $('div#createAccount3 div.uname-info').addClass('exit')
                       $('div#createAccount3 div.uname-info').removeClass('available')
                    }
                    if (result.data.type === 'available') {
                       $('div#createAccount3 div.uname-info').html('available')
                       $('div#createAccount3 div.uname-info').addClass('available')
                       $('div#createAccount3 div.uname-info').removeClass('exit')
                    }
                })
            }, 500);
            }
        });
        

        function selectClick(minCount, modal, clicked) {
            var type =  $(clicked).data('type');
            var id = $(clicked).data('id');
            var selectedCount = 0;
            if (!selected[type]) {
                selected[type] = [];
            }
            
            if (!$(clicked).hasClass('selected')) {
                if(type == 'location' && selected[type].length > 4 && selectedType == 2){
                    $('.expert-search-label').html('The maximum expertise count is 5.')
                    return;
                }
                $(clicked).addClass('selected');
                var el = $(clicked).detach();
                modal.find('.sp-selected').append(el)
               
                
                selected[type].push(id);
                selectedCount = selected[type].length;
                
                modal.find('.account-selected-count').removeClass('d-none')
                modal.find('.account-selected-count').html(selectedCount + ' Selected')
            } else {
                $(clicked).removeClass('selected');
                var el = $(clicked).detach();
                modal.find('.favourites').prepend(el)
                
                selected[type] = $.grep(selected[type], function(value) {
                    return value != id;
                });
                 selectedCount = selected[type].length;
                 
                if(type == 'location' && selected[type].length <= 4){
                    $('.expert-search-label').html('')
                }
                 
                 if(selectedCount > 0){
                    modal.find('.account-selected-count').removeClass('d-none')
                    modal.find('.account-selected-count').html(selectedCount + ' Selected')
                 }else{
                    modal.find('.account-selected-count').addClass('d-none')
                    modal.find('.account-selected-count').html('')
                 }
            }
            

            if (selectedCount >= minCount) {
                $(modal).find('.continue').removeClass('disabled');
                $(modal).find('.continue span').text('Continue');
            } else {
                $(modal).find('.continue').addClass('disabled');
                $(modal).find('.continue span').html('Select <span class="count"></span> More');
                $(modal).find('.continue span.count').text(minCount - selectedCount);
            }
        }


        const time = $('div.resend_confirmation_code span.seconds');
        var minutes = $('div.timer span.minutes');

        function timerDecrement() {
            setTimeout(function() {
                const newTime = time.text() - 1;

                time.text(newTime);

                if (newTime > 0) {
                    timerDecrement();
                } else {
                    $('a.resend_confirmation_code.disabled').removeClass('disabled');
                    $('div.resend_confirmation_code span').hide();
                }

            }, 1000);
        }

        function minutesTimerDecrement() {
            setTimeout(function() {
                const newTime = minutes.text() - 1;

                minutes.text(newTime);

                if (newTime > 0) {
                    minutesTimerDecrement();
                } else {
                    $('div.timer').text('The confirmation code was expired');
                }

            }, 60000);
        }

        $('a.resend_confirmation_code').not('disabled').on('click', function() {
            $(this).addClass('disabled');

            $.ajax({
                type: "POST",
                url: "{{route('frontend.auth.registration.resend_confirmation_code')}}",
                data: {token: token}
            }).done(function( result ) {
                if (result.status === 'success') {
                    $('div.resend_confirmation_code span').show();
                    time.text(59);
                    timerDecrement();
                    $('div.timer').html('The confirmation code will expire in <span class="minutes">10</span> mins');
                    minutes = $('div.timer span.minutes');
                    minutesTimerDecrement();
                }
            }).fail(function( result ) {
                showErrors(JSON.parse(result.responseText));
            });
        })
        
        function showHeader(s_type, step, up_down){
        var progress = $('#signup_header .progress-bar')
        var progress_val = progress.attr('aria-valuenow')
        s_type =  parseInt(s_type)
      
        switch(s_type) {
            case 1:
                if(step > 3){
                    $('#signup_header').removeClass('d-none');
                    if(up_down == 'up'){
                        progress.attr('aria-valuenow', parseInt(progress_val) + 20)
                        progress.css('width', (parseInt(progress_val) + 20)+'%')
                    }else{
                        progress.attr('aria-valuenow', parseInt(progress_val) - 20)
                        progress.css('width', (parseInt(progress_val) - 20)+'%')
                    }
                }else{
                    if(!$('#signup_header').hasClass('d-none'))
                       $('#signup_header').addClass('d-none'); 
                   
                    progress.attr('aria-valuenow', 0)
                    progress.css('width', '0%')
                }
              break;
            case 2:
                if(step > 5){
                    $('#signup_header').removeClass('d-none');
                     if(up_down == 'up'){
                        progress.attr('aria-valuenow', parseInt(progress_val) + 20)
                        progress.css('width', (parseInt(progress_val) + 20)+'%')
                    }else{
                        progress.attr('aria-valuenow', parseInt(progress_val) - 20)
                        progress.css('width', (parseInt(progress_val) - 20)+'%')
                    }
                }else{
                    if(!$('#signup_header').hasClass('d-none'))
                        $('#signup_header').addClass('d-none'); 
                    
                    progress.attr('aria-valuenow', 0)
                    progress.css('width', '0%')
                }
              break;
            case 3:
                if(step > 2){
                    $('#signup_header').removeClass('d-none');
                    if(up_down == 'up'){
                        progress.attr('aria-valuenow', parseInt(progress_val) + 20)
                        progress.css('width', (parseInt(progress_val) + 20)+'%')
                    }else{
                        progress.attr('aria-valuenow', parseInt(progress_val) - 20)
                        progress.css('width', (parseInt(progress_val) - 20)+'%')
                    }
                }else{
                     if(!$('#signup_header').hasClass('d-none'))
                        $('#signup_header').addClass('d-none'); 
                    
                    progress.attr('aria-valuenow', 0)
                    progress.css('width', '0%')
                }
              break;
          }
        }
        
        function showHeaderViaUrl(width){
            $('#signup_header').removeClass('d-none');
            var progress = $('#signup_header .progress-bar')
            var progress_val = progress.attr('aria-valuenow')
            
            progress.attr('aria-valuenow', width)
            progress.css('width', width +'%')
         }
         
         function udateSteps(step, type, step_type){
               $.ajax({
                    type: "POST",
                    url: "{{route('frontend.auth.registration.update.step')}}",
                    data: {step: step, type: type, step_type:step_type, token: token}
                }).done(function( result ) {
                });
         }
         
        
        
    $.validator.addMethod("password_format", function (value, element) {
        return this.optional(element) ||/^(?=.*[0-9])(?=.*[^\\p{L}])(?=\S+$).{6,255}$/i.test(value);
    }, "The minimum requirement is for the users to enter at least 6 minimum characters, at least one letter (any language), and 1 number."
    );
    });
    
    $.validator.addMethod("lettersonly", function(value, element) {        
      return this.optional(element) || /^[A-Za-z0-9_]+$/i.test(value)
    }, "Letters only please");  
</script>

@endsection
