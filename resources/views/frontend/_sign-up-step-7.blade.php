<div class="modal fade sign-up search res-scroll" id="createAccount7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style w-628" role="document">
        <div class="modal-content final-m-content">
            <div class="modal-body border--helper">
                <div class="header">
                    <img src="{{asset('frontend_assets/image/reg_cover.png')}}" class='succes-image'>
                </div>
                <div class="description">
                    <h1 class="desc-title">Sign up Successful!</h1>
                    <div class="content">Welcome! We're excited to have you in our community, Your signup is complete and you're all set to start enjoying Travooo.</div>
                    <div class="continue">Get Started <i class="fa fa-long-arrow-right" aria-hidden="true"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>