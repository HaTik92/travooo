<div class="modal fade sign-up search res-scroll step-3-mobile" id="createAccount10" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content">
            <span class="skip mobile--hide">
                {{ __('Skip')}} <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
            </span>
            <div class="modal-body body-create">
                <h4 class="title">{{ __('Choose your ')}} <span>{{__('preferred languages')}}</span></h4>
                <span class="step-subtitle">{{__('Enter the languages that determine the content you prefer to see.')}}</span>
                <div class="top-layer">
                    <div class="sp-language-content">
                        <div class="form-group search">
                            <input type="text" class="form-control" id="setting_content_language"  placeholder="{{__('Type any language')}}">
                            <img class="search" src="{{asset('assets2/image/sign_up/search.png')}}" alt="" style="top: 15px !important;">
                        </div>
                        <ul class="regional-content-ui setting-trev-ui" style="display: none;"></ul>
                    </div>
                    <p class="lng-subtext">Note: Drag and adjust the languages below to define their priority. This will affect your content preferences.</p>
                    <div class="step-selected-block">
                        <div class="form-group lng-selected">
                            <ul class="reg-content-list">
                                    <li class="" lngId="1"><span class="def-language">English (Default)</span>
                                        <a href="#" class="handle">
                                            <img src="{{asset('assets2/image/move.png')}}">
                                        </a>
                                    </li>
                            </ul>
                        </div>
                    </div>
                    <!-- <div>
                        <h3 class="suggestion-title pt-5">{{__('Suggestions')}}</h3>
                        <div class="form-group language-suggestion-block">
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="modal-footer mb-21">
                <div class="continue round--corners disabled">{{__('Continue')}}</div>
                <div class="step-back-btn back-btn mobile--hide">
                    <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back
                </div>
                <span class="step-num mobile--hide"><span class="current-step">Step 5</span> of 5</span>
            </div>
            
        </div>
    </div>
</div>
