<!-- left outside menu -->
<div class="left-outside-menu-wrap" id="leftOutsideMenu">
    <ul class="left-outside-menu">
        <li>
            <a href="{{route('home')}}">
                <i class="trav-home-icon"></i>
                <span>@lang('navs.general.home')</span>
                <!--<span class="counter">5</span>-->
            </a>
        </li>
        <li {{ route('country.index', $country->id)  === url()->current() ? 'class=active' : null }}>
            <a href="{{route('country.index', $country->id)}}">
                <i class="trav-about-icon"></i>
                <span>@lang('region.nav.about')</span>
            </a>
        </li>
        <li {{ route('country.packing_tips', $country->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('country.packing_tips', $country->id)}}">
                <i class="trav-trips-tips-icon"></i>
                <span>@lang('region.nav.packing_tips')</span>
            </a>
        </li>
        <li {{ route('country.weather', $country->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('country.weather', $country->id)}}">
                <i class="trav-weather-cloud-icon"></i>
                <span>@lang('region.nav.weather')</span>
            </a>
        </li>
        <li {{ route('country.etiquette', $country->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('country.etiquette', $country->id)}}">
                <i class="trav-etiquette-icon"></i>
                <span>@lang('region.nav.etiquette')</span>
            </a>
        </li>
        <li {{ route('country.health', $country->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('country.health', $country->id)}}">
                <i class="trav-health-hand-icon"></i>
                <span>@lang('region.nav.health ')</span>
            </a>
        </li>
        <li {{ route('country.visa_requirements', $country->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('country.visa_requirements', $country->id)}}">
                <i class="trav-visa-embassy-icon"></i>
                <span>@lang('region.nav.visa')</span>
            </a>
        </li>
        <li {{ route('country.when_to_go', $country->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('country.when_to_go', $country->id)}}">
                <i class="trav-flights-icon"></i>
                <span>@lang('region.nav.when_to_go')</span>
            </a>
        </li>
        <li {{ route('country.caution', $country->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('country.caution', $country->id)}}">
                <i class="trav-caution-icon"></i>
                <span>@lang('region.nav.caution')</span>
            </a>
        </li>
        <li>
            <a href="{{route('discussion.index', ['do' => 'country', 'id' => $country->id]) }}">
                <i class="trav-ask-icon"></i>
                <span>@lang('navs.frontend.forum')</span>
            </a>
        </li>
    </ul>
</div>