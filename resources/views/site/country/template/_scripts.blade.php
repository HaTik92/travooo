<script type="text/javascript">
    $('[data-tab=packing_tips], [data-tab=daily_costs]').on('click', function (e) {
        $(this).addClass('active-tab').siblings('[data-tab]').removeClass('active-tab');

        $('[data-content=daily_costs]').css('display', 'none');
        $('[data-content=packing_tips]').css('display', 'none');
        $('[data-content=' + $(this).data('tab') + ']').css('display', 'block');
        e.preventDefault()
    });

    $('[data-tab=etiquette], [data-tab=restrictions]').on('click', function (e) {
        $(this).addClass('active-tab').siblings('[data-tab]').removeClass('active-tab');

        $('[data-content=etiquette]').css('display', 'none');
        $('[data-content=restrictions]').css('display', 'none');
        $('[data-content=' + $(this).data('tab') + ']').css('display', 'block');
        e.preventDefault()
    });

    $('[data-tab=dangers], [data-tab=indexes]').on('click', function (e) {
        $(this).addClass('active-tab').siblings('[data-tab]').removeClass('active-tab');

        $('[data-content=dangers]').css('display', 'none');
        $('[data-content=indexes]').css('display', 'none');
        $('[data-content=' + $(this).data('tab') + ']').css('display', 'block');
        e.preventDefault()
    });
    
    $('[data-tab=global_experts], [data-tab=local_experts]').on('click', function (e) {
        $(this).addClass('active').siblings('[data-tab]').removeClass('active');

        $('[data-content=global_experts]').css('display', 'none');
        $('[data-content=local_experts]').css('display', 'none');
        $('[data-content=' + $(this).data('tab') + ']').css('display', 'block');
        e.preventDefault()
    });
</script>


<script>
    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{ route('country.check_follow', $country->id) }}",
            data: {name: "test"}
        }).done(function (res) {
            if (res.success == true) {
                $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                $('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow2"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
            } else if (res.success == false) {
                $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                $('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow2"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
            }
        });

        $.ajax({
            method: "POST",
            url: "{{route('country.visa_requirements', $country->id)}}",
            data: {name: "test"}
        }).done(function (res) {
            if (res.success == true) {
                $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
            } else if (res.success == false) {
                $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
            }
        });
    });

    $(document).ready(function () {
        $('body').on('click', '#button_follow', function () {
            $.ajax({
                method: "POST",
                url: "{{route('country.follow', $country->id)}}",
                data: {name: "test"}
            }).done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                        $('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });

        $('body').on('click', '#button_unfollow', function () {
            $.ajax({
                method: "POST",
                url: "{{route('country.unfollow', $country->id)}}",
                data: {name: "test"}
            }).done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                        $('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });
    });

    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{ route('city.check_follow', $country->capitals[0]->city->id) }}",
            data: {name: "test"}
        }).done(function (res) {
                if (res.success == true) {
                    $('#capital_follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_capital_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');

                } else if (res.success == false) {
                    $('#capital_follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_capital_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });
    });

    $(document).ready(function () {
        $('body').on('click', '#button_capital_follow', function () {
            $.ajax({
                method: "POST",
                url: "{{ route('city.follow', $country->capitals[0]->city->id) }}",
                data: {name: "test"}
            }).done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#capital_follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_capital_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });

        $('body').on('click', '#button_capital_unfollow', function () {
            $.ajax({
                method: "POST",
                url: "{{ route('city.unfollow', $country->capitals[0]->city->id) }}",
                data: {name: "test"}
            }).done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#capital_follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_capital_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });
    });
    
    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{url('country/'.$country->id.'/now-in-country')}}",
            data: {name: "test"}
        })
            .done(function (res) {
                data = JSON.parse(res);
                console.log(data);
                if (data.success == true) {
                    data = JSON.parse(res);

                    var output = '<ul class="city-avatar-list">';

                    for (var i = 0; i < data.live_checkins.length; i++) {
                        
                        var border_style;
                        if(data.live_checkins[i].live==1) {
                            border_style = 'border:2px solid #6ee655;';
                        }
                        else {
                            border_style = 'border:2px solid lightgrey';
                        }
                        output += '<li><img class="ava" src="' + data.live_checkins[i].profile_picture + '" style="width:60px;height:60px;'+border_style+'"></li>'
                    }
                    output += '</ul><div class="city-txt"><p>'

                    for (var ii = 0; ii < data.live_checkins.length; ii++) {
                        output += '<a href="#" class="link" data-toggle="modal" data-target="#goingPopup">' + data.live_checkins[ii].name + '</a>, ';
                    }
                    output += ' are now in {{$country->trans[0]->title}}</p></div>';
                    $('#nowInCountry').html(output);

                    //$('#nowInCountry').html(', <a href="#" class="link">Jeffrey Walters</a> and <a href="#" class="link">2 others</a>');

                }
            });
    });

</script>
<script>

    $('#countryPopupTrigger').on('click', function () {

        let $lg = $(this).lightGallery({
            dynamic: true,
            dynamicEl: [
                    @foreach($country->getMedias AS $photo)
                {


                    "src": 'https://s3.amazonaws.com/travooo-images2/th1100/{{$photo->url}}',
                    'thumb': 'https://s3.amazonaws.com/travooo-images2/th180/{{$photo->url}}',
                    'subHtml': `
            <div class='cover-block' style='display:none;'>
              <div class='cover-block-inner comment-block'>
                <ul class="modal-outside-link-list white-bg">
                  <li class="outside-link">
                    <a href="#">
                      <div class="round-icon">
                        <i class="trav-angle-left"></i>
                      </div>
                      <span>@lang("other.back")</span>
                    </a>
                  </li>
                  <li class="outside-link">
                    <a href="#">
                      <div class="round-icon">
                        <i class="trav-flag-icon"></i>
                      </div>
                      <span>Report</span>
                    </a>
                  </li>
                </ul>
                <div class='gallery-comment-wrap'>
                  <div class='gallery-comment-inner mCustomScrollbar'>

                    @if(isset($photo->users[0]) && is_object($photo->users[0]))
                        <div class="top-gallery-content gallery-comment-top">
                          <div class="top-info-layer">
                            <div class="top-avatar-wrap">
@if(isset($photo->users[0]) && is_object($photo->users[0]) && $photo->users[0]->profile_picture!='')
                        <img src="{{url($photo->users[0]->profile_picture)}}" alt="" style="width:50px;hright:50px;">
                        @endif
                        </div>
                        <div class="top-info-txt">
                          <div class="preview-txt">
                        @if(isset($photo->users[0]) && is_object($photo->users[0]) && $photo->users[0]->name!='')
                        <a class="dest-name" href="#">{{$photo->users[0]->name}}</a>
                        @endif
                        <p class="dest-place">uploaded a <b>photo</b> <span class="date">{{$photo->uploaded_at}}</span></p>
                          </div>
                        </div>
                      </div>
                      <div class="gallery-comment-txt">
                        <p>This is an amazing street to walk around and do some shopping</p>
                      </div>
                      <div class="gal-com-footer-info">
                        <div class="post-foot-block post-reaction">
                          <i class="trav-heart-fill-icon"></i>
                          <span><b>185</b></span>
                        </div>
                        <div class="post-foot-block post-comment-place">
                          <i class="trav-location"></i>
                          <span class="place-name">510 LaGuardia Pl, Paris, France</span>
                        </div>
                      </div>
                    </div>
                    @endif
                        <div class="post-comment-layer">
                          <div class="post-comment-top-info">
                            <div class="comm-count-info">
{{@count($photo->comments)}} @lang('comment.comments')
                        </div>
                        <div class="comm-count-info">
                          {{ $loop->iteration }} / {{ $loop->count }}
                        </div>
                      </div>
                      <div class="post-comment-wrapper">
                        @if(isset($photo->comments))
                            @foreach($photo->comments AS $comment)
                        <div class="post-comment-row">
                          <div class="post-com-avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                          </div>
                          <div class="post-comment-text">
                            <div class="post-com-name-layer">
                              <a href="#" class="comment-name">Katherin</a>
                              <a href="#" class="comment-nickname">@katherin</a>
                            </div>
                            <div class="comment-txt">
                              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                            </div>
                            <div class="comment-bottom-info">
                              <div class="com-reaction">
                                <img src="./assets/image/icon-smile.png" alt="">
                                <span>21</span>
                              </div>
                              <div class="com-time">6 hours ago</div>
                            </div>
                          </div>
                        </div>
                            @endforeach
                            @endif
                        </div>
                      </div>
                    </div>
@if(Auth::user())
                        <div class="post-add-comment-block">
                          <div class="avatar-wrap">
                            <img src="{{url(Auth::user()->profile_picture)}}" style="width:45px;height:45px;">
                    </div>
                    <div class="post-add-com-input">
                      <input type="text" name="comment" placeholder="@lang('comment.write_a_comment')">
                    </div>
                    <input type="hidden" name="medias_id" value="{{$photo->id}}" />
                    <input type="hidden" name="users_id" value="{{Auth::user()->id}}" />
                  </div>
                  @endif
                        </div>
                      </div>
                    </div>
`
                },

                @endforeach],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            $.each(galleryItem, function (i, val) {
                // itemArr.push(val);
            });

        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 500);
        };

        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            })
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#ContributeCaution').submit(function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route('country.contribute', $country->id) }}",
                data: $('#ContributeCaution').serialize()
            }).done(function (res) {
                    console.log(res);
                    res = JSON.parse(res);
                    if (res.success == true) {
                        $('#addCautionPopupBody').html('Thanks for your advice!');
                    } else if (res.success == false) {
                        $('#addCautionPopupBody').html('There were an error sending your advice. Please try again later!');
                    }
                });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });


    });
</script>