<!-- modals -->
<!-- gallery popup -->
@include('site/country/partials/gallery_popup')

<!-- place stories popup -->
@include('site/country/partials/place_stories_popup')

<!-- map index popup -->
@include('site/country/partials/map_index_popup')

<!-- health note popup -->
@include('site/country/partials/caution_popup')

<!-- languages popup -->
@include('site/country/partials/languages_popup')

<!-- national holidays popup -->
@include('site/country/partials/national_holidays_popup')

<!-- comments popup -->
@include('site/country/partials/comments_popup')

<!-- trip plans popup -->
@include('site/country/partials/trip_plans_popup')

<!-- your friends popup -->
@include('site/country/partials/your_friends_popup')

<!-- experts popup -->
@include('site/country/partials/experts_popup')