@php
    $title = 'Travooo - Country';
@endphp

@extends('site.country.template.country')

@section('before_content')
    <div class="top-banner-wrap">
        <img class="banner-city"
             src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$country->getMedias[0]->url}}" alt="banner"
             style="width:1070px;height:215px;">
        <div class="banner-cover-txt">
            <div class="banner-name">
                <div class="banner-ttl">{{$country->trans[0]->title}}</div>
                <div class="sub-ttl">@lang('region.country_in_name', ['name' => @$country->region->trans[0]->title])</div>
            </div>
            <div class="banner-btn" id="follow_botton2">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                    <i class="trav-comment-plus-icon"></i>
                    <span>@lang('region.buttons.follow')</span>
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="post-block post-tips-list-block">

        <div class="post-list-inner">
            <div class="post-tip-row tip-txt">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.visa')</b></div>
                <div class="row-txt"><span
                            class="red">@lang('region.required_for_moroccan_passport_holders')</span>
                </div>
            </div>
            <div class="post-tip-row tip-txt">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.id')</b></div>
                <div class="row-txt">
                    <span>@lang('region.proin_cursus_erat_at_lorem_placerat_bibendum_tincidunt')</span></div>
            </div>
            <div class="post-tip-row tip-txt">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.photos')</b></div>
                <div class="row-txt"><span>@lang('region.mauris_laoreet_nibh_nec_odio_porta_consectetur')</span>
                </div>
            </div>
            <div class="post-tip-row tip-txt">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.good_behavior_certificate')</b></div>
                <div class="row-txt"><span>@lang('region.aenean_viverra_mi_at_varius_venenatis')</span></div>
            </div>

        </div>
    </div>
@endsection

@section('sidebar')
    <div class="post-block post-embassy-block">
        <div class="post-image-wrap">
            <img src="http://placehold.it/383x170" alt="map">
            <div class="post-place-image">
                <img src="http://placehold.it/80x80" alt="place">
            </div>
        </div>
        <div class="post-place-location-info">
            <div class="post-place-txt">
                <div class="post-place-row">
                    <h3 class="place-ttl">@lang('region.embassy_location')</h3>
                </div>
                <div class="post-place-row">
                    <div class="place-txt">
                        <address>@lang('profile.address')
                        </address>
                    </div>
                </div>
                <div class="post-place-row">
                    <div class="place-label">@lang('region.opening_time_dd')</div>
                    <div class="place-txt"><span>{{ 8 . __('time.am') . ' - ' . 5 . __('time.pm')}}</span></div>
                </div>
                <div class="post-place-row">
                    <div class="place-label">@lang('region.phone_number_dd')</div>
                    <div class="place-txt"><span>05 37 63 72 00</span></div>
                </div>
                <div class="post-place-row">
                    <div class="place-label">@lang('region.website_url_dd')</div>
                    <div class="place-txt"><a href="ma.usembassy.gov">ma.usembassy.gov</a></div>
                </div>
            </div>
            <div class="post-place-info-foot">
                <div class="location-drop-wrap">
                    <a href="#" class="foot-link" id="locationDrop">@lang('region.no_in_morocco')</a>
                    <div class="location-block-inner">
                        <div class="loc-search-block">
                            <input type="text" class="" id="locSearchInput">
                        </div>
                        <div class="loc-result-block">
                            <div class="res-row">@lang('region.france')</div>
                            <div class="res-row">@lang('region.faroe_islands')</div>
                            <div class="res-row">@lang('region.fiji')</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @parent
@endsection