@php
    $title = 'Travooo - Country';
@endphp

@extends('site.country.template.country')

@section('before_content')
    <div class="top-banner-wrap">
        <img class="banner-city"
             src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$country->getMedias[0]->url}}" alt="banner"
             style="width:1070px;height:215px;">
        <div class="banner-cover-txt">
            <div class="banner-name">
                <div class="banner-ttl">{{$country->trans[0]->title}}</div>
                <div class="sub-ttl">@lang('region.country_in_name', ['name' => @$country->region->trans[0]->title])</div>
            </div>
            <div class="banner-btn" id="follow_botton2">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                    <i class="trav-comment-plus-icon"></i>
                    <span>@lang('region.buttons.follow')</span>
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="post-block post-tips-list-block">
        <div class="post-top-layer">
            <div class="top-left">
                <h3 class="post-tip-ttl_lg">@lang('region.best_time_to_go')</h3>
            </div>
            <div class="top-right">
                <a href="#" class="tip-link"><i
                            class="trav-open-video-in-window"></i>&nbsp; @lang('region.flight_search')</a>
            </div>
        </div>
        <?php
        $best_time = $country->trans[0]->best_time;
        $best_times = @explode("\n", $best_time);
        $months = array(
            __('month.january'),
            __('month.february'),
            __('month.february'),
            __('month.april'),
            __('month.may'),
            __('month.june'),
            __('month.july'),
            __('month.august'),
            __('month.september'),
            __('month.october'),
            __('month.november'),
            __('month.december')
        );
        $BT = array();

        foreach ($best_times AS $btime) {
            if (trim($btime) != '') {
                $btime = @explode(":", trim($btime));
                $bttt = @explode(",", $btime[1]);
                foreach ($bttt AS $btt) {
                    $BT[trim($btt)] = $btime[0];
                }
                //$BT[trim($btime[1])] = @explode(",", trim($btime[0]));
                //dd($btime);
            }

        }
        //var_dump($BT);
        ?>
        <div class="post-flight-content">
            <div class="progress-block">
                <ul class="color-list">
                    <li class="low">
                        @lang('region.low_season')
                    </li>
                    <li class="mid">
                        @lang('region.shoulder')
                    </li>
                    <li class="high">
                        @lang('region.high_season')
                    </li>
                </ul>
                <div class="progress-inner">
                    @foreach($months AS $month)

                        <?php
                        if (@$BT[$month] == 'Low Season') {
                            $progress = 'low';
                            $width = 20;
                        } elseif (@$BT[$month] == 'Shoulder') {
                            $progress = 'mid';
                            $width = 40;
                        } elseif (@$BT[$month] == 'High Season') {
                            $progress = 'high';
                            $width = 80;
                        }
                        ?>


                        <div class="progress-row">
                            <div class="progress-label">{{@$month}}</div>
                            <div class="progress-wrapper">
                                <div class="progress {{@$progress}}">
                                    <div class="progress-bar" style="width:{{@$width}}%"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
    <div class="post-block post-country-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.flights_from_morocco')</h3>
        </div>
        <div class="post-country-inner">
            <div class="post-txt-wrap">
                <a href="#" class="question-link">@lang('region.not_in_morocco')</a>
                <p class="post-txt">@lang('region.new_york_away_from_rabat_in_direct_flight_time')</p>
            </div>
            <div class="post-map-block">
                <div class="post-map-inner">
                    <img src="http://placehold.it/345x370" alt="map">
                    <div class="flight-icon" style="left:40%;top:40%;">
                        <i class="trav-angle-plane-icon"></i>
                        <div class="flight-tooltip">
                            {{ 7 . __('time.h') . ' ' . 48 .  __('time.min')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-country-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.main_airports') <span class="count">{{@count($airports)}}</span>
            </h3>
            <div class="side-right-control">
                <a href="#" class="see-more-link lg">@lang('region.see_all')</a>
            </div>
        </div>
        <div class="post-country-inner">
            <div class="dest-list">
                @foreach($airports AS $airport)
                    <div class="dest-item">
                        <div class="dest-location"><i
                                    class="trav-set-location-icon"></i>&nbsp; {{$airport->city->transsingle->title}}
                        </div>
                        <div class="dest-link-wrap">
                            <a href="{{route('place.index', $airport->id)}}"
                               class="dest-link">{{$airport->transsingle->title}}</a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

    @parent
@endsection