<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Country</title>
</head>
<body>
<div class="main-wrapper">
    @include('site/layouts/header')
    <div class="content-wrap">
        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>
        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url_with_locale('home')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('navs.general.home')</span>
                            <!--<span class="counter">5</span>-->
                        </a>
                    </li>
                    <li>
                        <a href="{{url_with_locale('country/'.$country->id)}}">
                            <i class="trav-about-icon"></i>
                            <span>About</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url_with_locale('country/'.$country->id.'/packing-tips')}}">
                            <i class="trav-trips-tips-icon"></i>
                            <span>Packing tips</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url_with_locale('country/'.$country->id.'/weather')}}">
                            <i class="trav-weather-cloud-icon"></i>
                            <span>@lang('place.weather')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url_with_locale('country/'.$country->id.'/etiquette')}}">
                            <i class="trav-etiquette-icon"></i>
                            <span>Etiquette</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url_with_locale('country/'.$country->id.'/health')}}">
                            <i class="trav-health-hand-icon"></i>
                            <span>Health</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url_with_locale('country/'.$country->id.'/visa-requirements')}}">
                            <i class="trav-visa-embassy-icon"></i>
                            <span>Visa</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url_with_locale('country/'.$country->id.'/when-to-go')}}">
                            <i class="trav-flights-icon"></i>
                            <span>When to go</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url_with_locale('country/'.$country->id.'/caution')}}">
                            <i class="trav-caution-icon"></i>
                            <span>Caution</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url_with_locale('discussions?do=country&id=').$country->id}}">
                            <i class="trav-ask-icon"></i>
                            <span>Forum</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="top-banner-wrap">
                <img class="banner-city"
                     src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$country->getMedias[0]->url}}" alt="banner"
                     style="width:1070px;height:215px;">
                <div class="banner-cover-txt">
                    <div class="banner-name">
                        <div class="banner-ttl">{{$country->trans[0]->title}}</div>
                        <div class="sub-ttl">Country in {{@$country->region->trans[0]->title}}</div>
                    </div>
                    <div class="banner-btn" id="follow_botton2">
                        <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                            <i class="trav-comment-plus-icon"></i>
                            <span>@lang('buttons.general.follow')</span>
                            <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <div class="post-block post-weather-block">
                        <div class="general-weather-view">
                            <div class="day-block-wrap">
                                <div class="weather-info">
                                    <div class="temperature">
                                        <span>{{$current_weather[0]->Temperature->Metric->Value}}</span><sup>o</sup><span
                                                class="temp"><b>C</b> / F</span></div>
                                    <div class="weather-day">
                                        <div class="day-name">{{weatherDate($current_weather[0]->LocalObservationDateTime)}}</div>
                                    </div>
                                </div>
                                <div class="icon-wrap">
                                    <img src="{{url('assets2/image/weather/'.$current_weather[0]->WeatherIcon.'-s.png')}}"
                                         alt="{{$current_weather[0]->WeatherText}}"
                                         title="{{$current_weather[0]->WeatherText}}" class="weather-icon">
                                </div>
                            </div>
                            <div class="weather-txt-block">
                                <div class="weather-title">
                                    {{$current_weather[0]->WeatherText}} in <span>{{$country->trans[0]->title}}</span>
                                </div>
                                <p>It's {{$current_weather[0]->WeatherText}} right now in {{$country->trans[0]->title}}.
                                    The forecast today shows a low
                                    of {{$current_weather[0]->Temperature->Metric->Value}}<sup>o</sup></p>
                            </div>
                        </div>

                        <div class="weather-block-wrapper">
                            <div class="weather-carousel-control">
                                <a href="#" class="slide-prev"><i class="trav-angle-left"></i></a>
                                <a href="#" class="slide-next"><i class="trav-angle-right"></i></a>
                            </div>
                            <div class="weather-hour-carousel" id="weatherHourCarousel">
                                @foreach($country_weather AS $cw)
                                    <div class="hour-block">
                                        <div class="time-block">{{weatherTime($cw->DateTime)}}</div>
                                        <div class="weather-image">
                                            <img src="{{url('assets2/image/weather/'.$cw->WeatherIcon.'-s.png')}}"
                                                 alt="{{$cw->IconPhrase}}" title="{{$cw->IconPhrase}}"
                                                 class="weather-icon">
                                        </div>
                                        <div class="temp-block">{{$cw->Temperature->Value}}<sup>o</sup></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="day-weather-list">
                            @foreach($daily_weather AS $dw)
                                <div class="day-weather-block">
                                    <div class="day-block-wrap">
                                        <div class="weather-icon">
                                            <img src="{{url('assets2/image/weather/'.$dw->Day->Icon.'-s.png')}}"
                                                 alt="{{$dw->Day->IconPhrase}}" title="{{$dw->Day->IconPhrase}}"
                                                 class="weather-icon">
                                            <span class="temperature">{{$dw->Temperature->Maximum->Value}}
                                                <sup>o</sup></span>
                                        </div>
                                        <div class="weather-day">
                                            <div class="day-name">{{date("l", $dw->EpochDate)}}</div>
                                            <div class="weather-desc">{{$dw->Day->IconPhrase}}</div>
                                        </div>
                                    </div>
                                    <div class="btn-wrap">
                                        <button type="button" class="btn btn-light-grey btn-bordered">Plan a Trip
                                        </button>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        <div class="post-block post-side-block">
                            <div class="post-side-top">
                                <h3 class="side-ttl">New york today</h3>
                            </div>
                            <div class="post-side-image-inner">
                                <ul class="post-image-list">
                                    <li>
                                        <img src="http://placehold.it/112x119" alt="">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/112x119" alt="">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/112x119" alt="">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/112x119" alt="">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/112x119" alt="">
                                    </li>
                                    <li class="add-photo-link">
                                        <div class="icon-wrap">
                                            <i class="fa fa-plus"></i>
                                        </div>
                                        <span>Add photo</span>
                                    </li>
                                </ul>
                            </div>
                        </div>


                        <div class="share-page-block">
                            <div class="share-page-inner">
                                <div class="share-txt">@lang('strings.frontend.share_this_page') this page</div>
                                <ul class="share-list">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-code"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                                <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                                <li><a href="{{url('/')}}">Advertising</a></li>
                                <li><a href="{{url('/')}}">Cookies</a></li>
                                <li><a href="{{url('/')}}">More</a></li>
                            </ul>
                            <p class="copyright">Travooo &copy; 2017</p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
<!-- gallery popup -->
@include('site/country/partials/gallery_popup')

<!-- place stories popup -->
@include('site/country/partials/place_stories_popup')

<!-- map index popup -->
@include('site/country/partials/map_index_popup')

<!-- health note popup -->
@include('site/country/partials/health_notes_popup')

<!-- languages popup -->
@include('site/country/partials/languages_popup')

<!-- national holidays popup -->
@include('site/country/partials/national_holidays_popup')

<!-- comments popup -->
@include('site/country/partials/comments_popup')

<!-- trip plans popup -->
@include('site/country/partials/trip_plans_popup')

<!-- your friends popup -->
@include('site/country/partials/your_friends_popup')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>
<script type="text/javascript">
    $('[data-tab=packing_tips], [data-tab=daily_costs]').on('click', function (e) {
        $(this).addClass('active-tab').siblings('[data-tab]').removeClass('active-tab');

        $('[data-content=daily_costs]').css('display', 'none');
        $('[data-content=packing_tips]').css('display', 'none');
        $('[data-content=' + $(this).data('tab') + ']').css('display', 'block');
        e.preventDefault()
    })

    $('[data-tab=etiquette], [data-tab=restrictions]').on('click', function (e) {
        $(this).addClass('active-tab').siblings('[data-tab]').removeClass('active-tab');

        $('[data-content=etiquette]').css('display', 'none');
        $('[data-content=restrictions]').css('display', 'none');
        $('[data-content=' + $(this).data('tab') + ']').css('display', 'block');
        e.preventDefault()
    })

    $('[data-tab=dangers], [data-tab=indexes]').on('click', function (e) {
        $(this).addClass('active-tab').siblings('[data-tab]').removeClass('active-tab');

        $('[data-content=dangers]').css('display', 'none');
        $('[data-content=indexes]').css('display', 'none');
        $('[data-content=' + $(this).data('tab') + ']').css('display', 'block');
        e.preventDefault()
    })
</script>

<script>
    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('country/'.$country->id.'/check-follow')}}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    $('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow2"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                } else if (res.success == false) {
                    $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    $('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow2"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });

        $.ajax({
            method: "POST",
            url: "{{url_with_locale('country/'.$country->id.'/visa-requirements')}}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                } else if (res.success == false) {
                    $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });
    });

    $(document).ready(function () {
        $('body').on('click', '#button_follow', function () {
            $.ajax({
                method: "POST",
                url: "{{url_with_locale('country/'.$country->id.'/follow')}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                        $('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });

        $('body').on('click', '#button_unfollow', function () {
            $.ajax({
                method: "POST",
                url: "{{url_with_locale('country/'.$country->id.'/unfollow')}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                        $('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });
    });

    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('city/'.$country->capitals[0]->city->id.'/check-follow')}}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#capital_follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_capital_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');

                } else if (res.success == false) {
                    $('#capital_follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_capital_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });
    });

    $(document).ready(function () {
        $('body').on('click', '#button_capital_follow', function () {
            $.ajax({
                method: "POST",
                url: "{{url_with_locale('city/'.$country->capitals[0]->city->id.'/follow')}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#capital_follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_capital_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });

        $('body').on('click', '#button_capital_unfollow', function () {
            $.ajax({
                method: "POST",
                url: "{{url_with_locale('city/'.$country->capitals[0]->city->id.'/unfollow')}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#capital_follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_capital_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });
    });

</script>
<script>

    $('#countryPopupTrigger').on('click', function () {

        let $lg = $(this).lightGallery({
            dynamic: true,
            dynamicEl: [
                    @foreach($country->getMedias AS $photo)
                {


                    "src": 'https://s3.amazonaws.com/travooo-images2/th1100/{{$photo->url}}',
                    'thumb': 'https://s3.amazonaws.com/travooo-images2/th180/{{$photo->url}}',
                    'subHtml': `
            <div class='cover-block' style='display:none;'>
              <div class='cover-block-inner comment-block'>
                <ul class="modal-outside-link-list white-bg">
                  <li class="outside-link">
                    <a href="#">
                      <div class="round-icon">
                        <i class="trav-angle-left"></i>
                      </div>
                      <span>@lang("other.back")</span>
                    </a>
                  </li>
                  <li class="outside-link">
                    <a href="#">
                      <div class="round-icon">
                        <i class="trav-flag-icon"></i>
                      </div>
                      <span>Report</span>
                    </a>
                  </li>
                </ul>
                <div class='gallery-comment-wrap'>
                  <div class='gallery-comment-inner mCustomScrollbar'>

                    @if(isset($photo->users[0]) && is_object($photo->users[0]))
                        <div class="top-gallery-content gallery-comment-top">
                          <div class="top-info-layer">
                            <div class="top-avatar-wrap">
@if(isset($photo->users[0]) && is_object($photo->users[0]) && $photo->users[0]->profile_picture!='')
                        <img src="{{url($photo->users[0]->profile_picture)}}" alt="" style="width:50px;hright:50px;">
                        @endif
                        </div>
                        <div class="top-info-txt">
                          <div class="preview-txt">
                        @if(isset($photo->users[0]) && is_object($photo->users[0]) && $photo->users[0]->name!='')
                        <a class="dest-name" href="#">{{$photo->users[0]->name}}</a>
                        @endif
                        <p class="dest-place">uploaded a <b>photo</b> <span class="date">{{$photo->uploaded_at}}</span></p>
                          </div>
                        </div>
                      </div>
                      <div class="gallery-comment-txt">
                        <p>This is an amazing street to walk around and do some shopping</p>
                      </div>
                      <div class="gal-com-footer-info">
                        <div class="post-foot-block post-reaction">
                          <i class="trav-heart-fill-icon"></i>
                          <span><b>185</b></span>
                        </div>
                        <div class="post-foot-block post-comment-place">
                          <i class="trav-location"></i>
                          <span class="place-name">510 LaGuardia Pl, Paris, France</span>
                        </div>
                      </div>
                    </div>
                    @endif
                        <div class="post-comment-layer">
                          <div class="post-comment-top-info">
                            <div class="comm-count-info">
{{@count($photo->comments)}} @lang('comment.comments')
                        </div>
                        <div class="comm-count-info">
                          {{ $loop->iteration }} / {{ $loop->count }}
                        </div>
                      </div>
                      <div class="post-comment-wrapper">
                        @if(isset($photo->comments))
                            @foreach($photo->comments AS $comment)
                        <div class="post-comment-row">
                          <div class="post-com-avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                          </div>
                          <div class="post-comment-text">
                            <div class="post-com-name-layer">
                              <a href="#" class="comment-name">Katherin</a>
                              <a href="#" class="comment-nickname">@katherin</a>
                            </div>
                            <div class="comment-txt">
                              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                            </div>
                            <div class="comment-bottom-info">
                              <div class="com-reaction">
                                <img src="./assets/image/icon-smile.png" alt="">
                                <span>21</span>
                              </div>
                              <div class="com-time">6 hours ago</div>
                            </div>
                          </div>
                        </div>
                            @endforeach
                            @endif
                        </div>
                      </div>
                    </div>
@if(Auth::user())
                        <div class="post-add-comment-block">
                          <div class="avatar-wrap">
                            <img src="{{url(Auth::user()->profile_picture)}}" style="width:45px;height:45px;">
                    </div>
                    <div class="post-add-com-input">                                      
                      <input type="text" name="comment" placeholder="@lang('comment.write_a_comment')">
                    </div>
                    <input type="hidden" name="medias_id" value="{{$photo->id}}" />
                    <input type="hidden" name="users_id" value="{{Auth::user()->id}}" />
                  </div>
                  @endif
                        </div>
                      </div>
                    </div>
`
                },

                @endforeach],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            $.each(galleryItem, function (i, val) {
                // itemArr.push(val);
            });

        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 500);
        }

        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            })
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
        });
    });
</script>
@include('site/layouts/footer')
</body>
</html>