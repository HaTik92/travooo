@php
    $title = 'Travooo - Country';
@endphp

@extends('site.country.template.country')

@section('before_content')
    <div class="top-banner-wrap">
        <div class="post-block post-flage-detail">
            <div class="post-event-block">
                <div class="post-event-image" style='height:210px;background-image:url(https://s3.amazonaws.com/travooo-images2/th1100/{{@$country->getMedias[0]->url}})'>
                    <div class="follow-block-info" style="padding-top: 190px;padding-left: 25px;">
                                <ul class="foot-avatar-list" data-toggle="modal" data-target="#expertsPopup">
                                    @foreach($country->experts AS $cexp)
                                        <li><img class="small-ava" src="{{check_profile_picture($cexp->user->profile_picture)}}" alt="ava" style="width:29px;height:29px;"></li>
                                    @endforeach
                                </ul>
                                <span>{{count($country->experts)}} Experts in {{@$country->trans[0]->title}} </span>
                            </div>
                </div>
                <div class="post-event-main">
                    <div class="flag-wrap">
                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($country->iso_code)}}.png"
                             alt="flag" style="width:155px;height:77px;">
                    </div>
                    <div class="post-placement-info">
                        <span class="hash"></span>
                        <h2 class="place-name">{{$country->trans[0]->title}}</h2>
                        <div class="event-info-layer">
                            <span class="placement-place">@lang('region.country_in_name', ['name' => @$country->region->trans[0]->title])</span>
                            <div id="talkingAbout">

                            </div>
                        </div>
                        <div class="event-main-content">
                            <p>{{strip_tags(@$country->trans[0]->description)}}</p>
                        </div>
                    </div>
                </div>
                <div class="follow-bottom-link">
                                                            <span id="follow_botton">

                                                            </span>
                    <a href="#"
                       class="follow-link">@lang('profile.count_followers', ['count' => @count($country->followers)])</a>
                </div>
            </div>
        </div>
    </div>
    <div class="top-now-in-city-block">
        <div class="city-inner" id="nowInCountry">

        </div>
    </div>
    @include('site/country/partials/top_blocks')
    <div class="top-banner-wrap">
        <img class="banner-city"
             src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$country->getMedias[0]->url}}" alt="banner"
             style="width:1070px;height:215px;">
        <div class="banner-cover-txt">
            <div class="banner-name">
                <div class="banner-ttl">{{$country->trans[0]->title}}</div>
                <div class="sub-ttl">@lang('region.country_in_name', ['name' => @$country->region->trans[0]->title])</div>
            </div>
            <div class="banner-btn" id="follow_botton2">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                    <i class="trav-comment-plus-icon"></i>
                    <span>@lang('region.buttons.follow')</span>
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#storiesModePopup">
        @lang('region.stories_popup')
    </button>


    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tripPlansPopup">
        @lang('region.trip_plans_popup')
    </button>

    <div class="post-block post-country-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.about_the_country')</h3>
        </div>
        <div class="post-country-inner">
            <div class="post-footer-info">
                <div class="post-foot-block">
                    <i class="trav-comment-icon"></i>
                    <span class="blue-txt">@lang('region.discussed')</span>&nbsp;
                    <span>@lang('chat.by')</span>&nbsp;
                    <b>john moline, pitter garmen, richerd</b>&nbsp;
                    <span>@lang('other.and_count_more', ['count' => 437])</span>
                </div>
            </div>

            <div class="post-map-block">
                <div class="post-map-inner">
                    <img src="https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{$country->lat}},{{$country->lng}}&zoom=5&size=595x360&key={{env('GOOGLE_MAPS_KEY')}}"
                         alt="Map of {{$country->trans[0]->title}}">
                    <div class="post-top-map-info">
                        <div class="info-block">
                            <div class="info-icon">
                                <i class="trav-popularity-icon"></i>
                            </div>
                            <div class="info-txt">
                                <div class="info-ttl">{{@$country->trans[0]->population}}</div>
                                <div class="info-smpl">@lang('region.pollution')</div>
                            </div>
                        </div>
                    </div>
                    <div class="post-map-flag">
                        <img class="flag-image"
                             src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($country->iso_code)}}.png"
                             alt="flag" style="width:50px;height:50px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-tips-list-block">
        <div class="post-list-inner">
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.nationality')</b>
                </div>
                <div class="row-txt">
                    <span>{{$country->trans[0]->nationality}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.languages_spoken')</b>
                </div>
                <div class="row-txt">
                    <span>{{$country->languages[0]->title}}</span>
                    <a href="#" class="more-link" data-toggle="modal"
                       data-target="#languageCurrTime">+{{@count($country->languages)-1}} @lang('buttons.general.more')</a>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.currencies')</b>
                </div>
                <div class="row-txt">
                    <span>{{$country->currencies[0]->trans[0]->title}}</span>
                    <span class="currency">(1 USD = 9.23 MAD)</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.timings')</b>
                </div>
                <div class="row-txt">
                    <span>{{@$country->timezone[0]->time_zone_name}}</span>
                    <a href="#" class="more-link" data-toggle="modal"
                       data-target="#languageCurrTime">+{{@count($country->timezone)-1}} @lang('buttons.general.more')</a>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.religions')</b>
                </div>
                <div class="row-txt">
                    <span>{{@$country->religions[0]->trans[0]->title}}</span>
                    <a href="#" class="more-link" data-toggle="modal"
                       data-target="#languageCurrTime">+{{@count($country->religions)-1}} @lang('buttons.general.more')</a>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.phone_code')</b>
                </div>
                <div class="row-txt">
                    <span>{{@$country->code}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.units_of_measurement')</b>
                </div>
                <div class="row-txt">
                    <span>{{@$country->trans[0]->metrics}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.working_days')</b>
                </div>
                <div class="row-txt">
                    <span>{{@$country->trans[0]->working_days}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.transportation_methods')</b></div>
                <div class="row-txt">
                    <?php
                    if ($country->trans[0]->transportation != '') {
                        $transportation = explode(",", $country->trans[0]->transportation);
                    }
                    ?>
                    <span>{{@$transportation[0]}}</span>
                    <a href="#" class="more-link" data-toggle="modal"
                       data-target="#languageCurrTime">+{{@count($transportation)-1}} @lang('buttons.general.more')</a>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.speed_limit')</b>
                </div>
                <div class="row-txt">
                    <?php
                    if ($country->trans[0]->speed_limit != '') {
                        $speed_limit = explode("\n", $country->trans[0]->speed_limit);
                    }
                    ?>
                    <span>{{@$speed_limit[0]}}</span>
                    <a href="#" class="more-link" data-toggle="modal"
                       data-target="#languageCurrTime">+{{@count($speed_limit)-1}} @lang('buttons.general.more')</a>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-tips-list-block">
        <div class="post-top-layer">
            <div class="top-left">
                <h3 class="post-tip-ttl_lg">@lang('region.emergency_number')
                    &nbsp;<span>{{@count($country->emergency)}}</span></h3>
            </div>
        </div>
        <div class="post-list-inner">
            @foreach($country->emergency AS $emergency)
                <div class="post-tip-row">
                    <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                        <b>{{@strip_tags($emergency->trans[0]->description)}}</b></div>
                    <div class="row-txt">
                        <span>{{$emergency->trans[0]->title}}</span>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="post-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.cities') <span class="count">{{@count($country->cities)}}</span>
            </h3>
            <div class="side-right-control">
                <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="post-slide-wrap slide-hide-right-margin">
                <ul id="citiesSlider" class="post-slider">
                    @foreach($country->cities AS $city)
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/{{@$city->getMedias[0]->url}}"
                                 alt="" style="width:230px;height:300px">
                            <div class="post-slider-caption transparent-caption">
                                <p class="post-slide-name"><a href="{{ route('city.index', $city->id) }}"
                                                              style="color:white;">{{$city->trans[0]->title}}</a>
                                </p>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="post-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.top_places') <span class="count">50</span></h3>
            <div class="side-right-control">
                <a href="#" class="see-more-link">@lang('region.see_all')</a>
                <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="post-slide-wrap slide-hide-right-margin">
                <ul id="placesSlider" class="post-slider">
                    @foreach($country->places()->take(50)->get() AS $place)
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/th230/{{@$place->medias[0]->url}}"
                                 alt="" style="width:230px;height:300px">
                            <div class="post-slider-caption transparent-caption">
                                <p class="post-slide-name"><a href="{{ route('place.index', $place->id) }}"
                                                              style="color:white;">{{$place->trans[0]->title}}</a>
                                </p>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    {{--
<div class="post-block">
<div class="post-side-top">
<h3 class="side-ttl">National holidays <span class="count">{{@count($country->holidays)}}</span></h3>
<div class="side-right-control">
<a href="#" class="see-more-link" data-toggle="modal" data-target="#nationalHolidayPopup">See all</a>
<a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
<a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
</div>
</div>
<div class="post-side-inner">
<div class="post-slide-wrap slide-hide-right-margin">
<ul id="nationalHoliday" class="post-slider">
                                    @foreach($country->holidays AS $holiday)
<li class="post-card">
    <div class="image-wrap">
        <img src="http://placehold.it/274x234" alt="">
        <div class="card-cover">
            <span class="date">4</span>
            <span class="month">Jul</span>
        </div>
    </div>
    <div class="post-slider-caption">
        <p class="post-card-name">{{$holiday->trans[0]->title}}</p>
        <div class="post-footer-info">
            <div class="post-foot-block">
                <!--<ul class="foot-avatar-list">
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                </ul>
                <span>20 Talking about this</span>
                                                                        -->
            </div>
        </div>
    </div>
</li>
                                    @endforeach
</ul>
</div>
</div>
</div>
    --}}

    <div class="post-block post-country-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.accessibility')</h3>
        </div>
        <div class="post-country-inner post-full-inner">
            <div class="post-map-block">
                <div class="post-map-inner">
                    <img src="https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{$country->lat}},{{$country->lng}}&zoom=5&size=595x360&key={{env('GOOGLE_MAPS_KEY')}}"
                         alt="Map of {{$country->trans[0]->title}}">
                    <div class="post-top-map-tabs">
                        <div class="map-tab current" data-tab="atmTxt">
                            <div class="tab-icon"><i class="trav-atms-icon"></i></div>
                            <div class="tab-txt">@lang('region.atms')</div>
                        </div>
                        <div class="map-tab" data-tab="wifiTxt">
                            <div class="tab-icon"><i class="trav-internet-icon"></i></div>
                            <div class="tab-txt">@lang('region.internet')</div>
                        </div>
                        <div class="map-tab" data-tab="socketTxt">
                            <div class="tab-icon"><i class="trav-sockets-icon"></i></div>
                            <div class="tab-txt">@lang('region.sockets')</div>
                        </div>
                        <div class="map-tab" data-tab="hotelTxt">
                            <div class="tab-icon"><i class="trav-hotels-icon"></i></div>
                            <div class="tab-txt">@lang('region.hotels')</div>
                        </div>
                    </div>
                    <div class="post-map-area">
                        <div class="area-txt" id="atmTxt">
                            <h5 class="ttl">{{number_format(@count($country->atms))}}</h5>
                            <p>@lang('region.atm_machines')</p>
                        </div>
                        <div class="area-txt" id="wifiTxt" style="display:none">
                            <h5 class="ttl">{{$country->trans[0]->internet}}</h5>
                            <p>@lang('region.internet')</p>
                        </div>
                        <div class="area-txt" id="socketTxt" style="display:none">
                            <h5 class="ttl">{{$country->trans[0]->sockets}}</h5>
                            <p>@lang('region.sockets')</p>
                        </div>
                        <div class="area-txt" id="hotelTxt" style="display:none">
                            <h5 class="ttl">{{number_format(@count($country->hotels))}}</h5>
                            <p>@lang('region.hotels')</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('sidebar')
    <div class="post-block post-card-block">
        <div class="post-place-info-card">
            <div class="image-wrap">
                <img src="https://s3.amazonaws.com/travooo-images2/{{@$country->capitals[0]->city->getMedias[0]->url}}"
                     alt="" style="width:400px;height:210px;">
                <div class="post-place-image-info">
                    <div class="place-flag-image">
                        <img class="flag-image"
                             src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($country->iso_code)}}.png"
                             alt="flag" style="width:105px;height:53px">
                    </div>
                    <div class="follow-btn-wrap" id="capital_follow_botton">

                    </div>
                </div>
            </div>
            <div class="post-slider-caption">
                <p class="post-place-name"><a
                            href="{{ route('city.index', $country->capitals[0]->city->id) }}">{{$country->capitals[0]->city->trans[0]->title}}</a>
                </p>
                <p class="post-card-placement">
                    @lang('region.capital_of') {{$country->trans[0]->title}}
                </p>
                <ul class="post-footer-info-list">
                    <li>
                        <p class="info-count">{{@count($country->capitals[0]->city->followers)}}</p>
                        <p class="info-label">@lang('profile.followers')</p>
                    </li>
                    <li>
                        <p class="info-count">{{@count($country->capitals[0]->city->plans)}}</p>
                        <p class="info-label">@lang('region.trip_plans')</p>
                    </li>
                    <li>
                        <p class="info-count">{{@count($country->capitals[0]->city->shares)}}</p>
                        <p class="info-label">@lang('region.talking_about_it')</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <div class="post-block post-country-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.main_airports') <span class="count">{{@count($airports)}}</span>
            </h3>
            <div class="side-right-control">
                <a href="#" class="see-more-link lg">@lang('region.see_all')</a>
            </div>
        </div>
        <div class="post-country-inner">
            <div class="dest-list">
                @foreach($airports AS $airport)
                    <div class="dest-item">
                        <div class="dest-location"><i
                                    class="trav-set-location-icon"></i>&nbsp; {{@$airport->city->transsingle->title}}
                        </div>
                        <div class="dest-link-wrap">
                            <a href="{{ route('place.index', $airport->id) }}"
                               class="dest-link">{{@$airport->transsingle->title}}</a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

    @parent
@endsection