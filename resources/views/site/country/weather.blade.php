@php
    $title = 'Travooo - Country';
@endphp

@extends('site.country.template.country')

@section('before_content')
    <div class="top-banner-wrap">
        <img class="banner-city"
             src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$country->getMedias[0]->url}}" alt="banner"
             style="width:1070px;height:215px;">
        <div class="banner-cover-txt">
            <div class="banner-name">
                <div class="banner-ttl">{{$country->trans[0]->title}}</div>
                <div class="sub-ttl">@lang('region.country_in_name', ['name' => @$country->region->trans[0]->title])</div>
            </div>
            <div class="banner-btn" id="follow_botton2">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                    <i class="trav-comment-plus-icon"></i>
                    <span>@lang('region.buttons.follow')</span>
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="post-block post-weather-block">
        <div class="general-weather-view">
            <div class="day-block-wrap">
                <div class="weather-info">
                    <div class="temperature">
                        <span>{{$current_weather[0]->Temperature->Metric->Value}}</span><sup>o</sup><span
                                class="temp"><b>C</b> / F</span></div>
                    <div class="weather-day">
                        <div class="day-name">{{weatherDate($current_weather[0]->LocalObservationDateTime)}}</div>
                    </div>
                </div>
                <div class="icon-wrap">
                    <img src="{{url('assets2/image/weather/'.$current_weather[0]->WeatherIcon.'-s.png')}}"
                         alt="{{$current_weather[0]->WeatherText}}"
                         title="{{$current_weather[0]->WeatherText}}" class="weather-icon">
                </div>
            </div>
            <div class="weather-txt-block">
                <div class="weather-title">
                    {{$current_weather[0]->WeatherText}} in <span>{{$country->trans[0]->title}}</span>
                </div>
                <p>It's {{$current_weather[0]->WeatherText}} right now in {{$country->trans[0]->title}}.
                    The forecast today shows a low
                    of {{$current_weather[0]->Temperature->Metric->Value}}<sup>o</sup></p>
            </div>
        </div>

        <div class="weather-block-wrapper">
            <div class="weather-carousel-control">
                <a href="#" class="slide-prev"><i class="trav-angle-left"></i></a>
                <a href="#" class="slide-next"><i class="trav-angle-right"></i></a>
            </div>
            <div class="weather-hour-carousel" id="weatherHourCarousel">
                @foreach($country_weather AS $cw)
                    <div class="hour-block">
                        <div class="time-block">{{weatherTime($cw->DateTime)}}</div>
                        <div class="weather-image">
                            <img src="{{url('assets2/image/weather/'.$cw->WeatherIcon.'-s.png')}}"
                                 alt="{{$cw->IconPhrase}}" title="{{$cw->IconPhrase}}"
                                 class="weather-icon">
                        </div>
                        <div class="temp-block">{{$cw->Temperature->Value}}<sup>o</sup></div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="day-weather-list">
            @foreach($daily_weather AS $dw)
                <div class="day-weather-block">
                    <div class="day-block-wrap">
                        <div class="weather-icon">
                            <img src="{{url('assets2/image/weather/'.$dw->Day->Icon.'-s.png')}}"
                                 alt="{{$dw->Day->IconPhrase}}" title="{{$dw->Day->IconPhrase}}"
                                 class="weather-icon">
                            <span class="temperature">{{$dw->Temperature->Maximum->Value}}
                                <sup>o</sup></span>
                        </div>
                        <div class="weather-day">
                            <div class="day-name">{{date("l", $dw->EpochDate)}}</div>
                            <div class="weather-desc">{{$dw->Day->IconPhrase}}</div>
                        </div>
                    </div>
                    <div class="btn-wrap">
                        <button type="button"
                                class="btn btn-light-grey btn-bordered">@lang('region.buttons.plan_a_trip')
                        </button>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
@endsection

@section('sidebar')
    <div class="post-block post-side-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.new_york_today')</h3>
        </div>
        <div class="post-side-image-inner">
            <ul class="post-image-list">
                <li>
                    <img src="http://placehold.it/112x119" alt="">
                </li>
                <li>
                    <img src="http://placehold.it/112x119" alt="">
                </li>
                <li>
                    <img src="http://placehold.it/112x119" alt="">
                </li>
                <li>
                    <img src="http://placehold.it/112x119" alt="">
                </li>
                <li>
                    <img src="http://placehold.it/112x119" alt="">
                </li>
                <li class="add-photo-link">
                    <div class="icon-wrap">
                        <i class="fa fa-plus"></i>
                    </div>
                    <span>@lang('region.add_photo')</span>
                </li>
            </ul>
        </div>
    </div>

    @parent
@endsection