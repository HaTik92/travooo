<div class="modal fade white-style" data-backdrop="false" id="healthNotePopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-650" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-suggest-modal">
                <h3 class="suggest-ttl">@lang('region.suggest_an_advice')</h3>
                <div class="suggest-block" id='addHealthPopupBody'>
                    <form method='post' id='ContributeHealth'>
                        <div class="form-block">
                            <div class="avatar-wrap">
                                <img src="{{url('assets2/image/placeholders/male.png')}}" style="width:40px;height:40px;">
                            </div>
                            <div class="suggest-txt-wrap">
                                <textarea required name="contents" id="contents" rows="5"
                                          placeholder="@lang('region.write_an_advice_about_health_in_place', ['place' => $country->trans[0]->title])"></textarea>
                            <!-- <input type="text" placeholder="@lang('comment.write_a_comment')"> -->
                            </div>
                        </div>
                        <div class="btn-wrapper">
                            <button type="button"
                                    class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                            <button type="submit"
                                    class="btn btn-light-primary btn-bordered">@lang('buttons.general.post')</button>
                        </div>
                        <input type='hidden' name='country_id' value='{{$country->id}}' />
                        <input type='hidden' name='type' value='health' />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>