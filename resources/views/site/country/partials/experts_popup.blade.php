<!-- Experts popup -->
<div class="modal fade white-style" data-backdrop="false" id="expertsPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-700" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-going-block post-mobile-full">
                <div class="post-top-topic topic-tabs">
                    <div class="topic-item tab-item active" data-tab='global_experts'>@lang('region.experts_global') <span
                                class="count"></span></div>
                                <div class="topic-item tab-item" data-tab='local_experts'>@lang('region.experts_local')
                                    @if(is_object($country) AND isset($country->iso_code))
                                    <span class="count"><img src='https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($country->iso_code)}}.png' style="width:25px;vertical-align:bottom;" /></span>
                                    @endif
                    </div>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar" data-content='global_experts'>
                    @foreach($country->experts AS $cexp)
                            <div class="people-row">
                                <div class="main-info-layer">
                                    <div class="img-wrap">
                                        <a href='{{url('profile/'.$cexp->user->id)}}'>
                                            <img class="ava"
                                                 src="{{check_profile_picture($cexp->user->profile_picture)}}"
                                                 alt="ava" style="width:50px;height:50px;">
                                        </a>
                                    </div>
                                    <div class="txt-block">
                                        <div class="name">
                                            <a href='{{url('profile/'.$cexp->user->id)}}'>
                                                {{$cexp->user->name}}
                                            </a>
                                        </div>
                                        <div class="info-line">
                                            <div class="info-part">
                                                <b></b></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="button-wrap">
                                    <button class="btn btn-light-grey btn-bordered"
                                            onclick='window.location.replace("https://www.travooo.com/chat/new/{{$cexp->user->id}}");'>
                                        @lang('place.message')
                                    </button>
                                </div>
                            </div>
                    @endforeach
                </div>
                
                <div class="post-people-block-wrap mCustomScrollbar" data-content='local_experts' style='display:none;'>
                    @foreach($country->experts AS $cexpl)
                        @if($cexpl->user->nationality==Auth::guard('user')->user()->nationality)
                            <div class="people-row">
                                <div class="main-info-layer">
                                    <div class="img-wrap">
                                        <a href='{{url('profile/'.$cexpl->user->id)}}'>
                                            <img class="ava"
                                                 src="{{check_profile_picture($cexpl->user->profile_picture)}}"
                                                 alt="ava" style="width:50px;height:50px;">
                                        </a>
                                    </div>
                                    <div class="txt-block">
                                        <div class="name">
                                            <a href='{{url('profile/'.$cexpl->user->id)}}'>
                                                {{$cexpl->user->name}}
                                            </a>
                                        </div>
                                        <div class="info-line">
                                            <div class="info-part">
                                                <b></b></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="button-wrap">
                                    <button class="btn btn-light-grey btn-bordered"
                                            onclick='window.location.replace("https://www.travooo.com/chat/new/{{$cexpl->user->id}}");'>
                                        @lang('place.message')
                                    </button>
                                </div>
                            </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>