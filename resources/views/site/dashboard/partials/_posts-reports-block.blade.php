<div class="table-responsive">
    <table class="table tbl-dashboard">
        <tr>
            <th class="publ">@lang('dashboard.published')</th>
            <th class="post">@lang('dashboard.post')</th>
            <th class="prog-type">Type</th>
            <th class="prog-cell">@lang('other.views')</th>
            <th class="prog-cell">@lang('dashboard.engagement')</th>
        </tr>
        @foreach($post_reports as $day=>$post_report)
        <tr class="dash-posts-table-row {{$post_report['type']}}">
            <td>
                <span class="dt">{{date('d/m/Y', $day)}}<br>
                    {{date('H:i a', $day)}}</span>
            </td>
            <td>
                <a href="#" class="post-tbl-link">
                    {!!$post_report['title']!!}
                </a>
            </td>
            <td>
                <i class="{{$post_report['icon']}}"></i>
            </td>
            <td>
                <div class="tbl-progress">
                    <div class="progress-label">{{$post_report['views_count']}}</div>
                    <div class="progress">
                        <div class="progress-bar primary" role="progressbar"
                             aria-valuenow="{{calculate_percent($post_report['views_count'])}}" aria-valuemin="0"
                             aria-valuemax="100" style="width: {{calculate_percent($post_report['views_count'])}}%;"></div>
                    </div>
                </div>
            </td>
            <td>
                <div class="tbl-progress">
                    <div class="progress-label">{{$post_report['click_count']}}</div>
                    <div class="progress">
                        <div class="progress-bar light-violet"
                             role="progressbar" aria-valuenow="{{calculate_percent($post_report['click_count'])}}"
                             aria-valuemin="0" aria-valuemax="100"
                             style="width: {{calculate_percent($post_report['click_count'])}}%;"></div>
                    </div>
                </div>
                <div class="tbl-progress">
                    <div class="progress-label">{{$post_report['engag_count']}}</div>
                    <div class="progress">
                        <div class="progress-bar orange" role="progressbar"
                             aria-valuenow="{{calculate_percent($post_report['engag_count'])}}" aria-valuemin="0"
                             aria-valuemax="100" style="width: {{calculate_percent($post_report['engag_count'])}}%;"></div>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </table>
</div>