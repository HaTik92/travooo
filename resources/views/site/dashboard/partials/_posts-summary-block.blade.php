<div class="dash-block-head-stats">
    <div class="dash-block-head-stats-item">
        <span class="ttl">All posts</span>
        <span class="count">{{$summary['texts_count'] + $summary['images_count'] + $summary['videos_count'] + $summary['trip_plans_count'] + $summary['reports_count'] + $summary['links_count']}}</span>
    </div>
    <div class="dash-block-head-stats-item">
        <span class="ttl">Text</span>
        <span class="count">{{$summary['texts_count']}}</span>
    </div>
    <div class="dash-block-head-stats-item">
        <span class="ttl">Image</span>
        <span class="count">{{$summary['images_count']}}</span>
    </div>
    <div class="dash-block-head-stats-item">
        <span class="ttl">Video</span>
        <span class="count">{{$summary['videos_count']}}</span>
    </div>
    <div class="dash-block-head-stats-item">
        <span class="ttl">Trip Plan</span>
        <span class="count">{{$summary['trip_plans_count']}}</span>
    </div>
    <div class="dash-block-head-stats-item">
        <span class="ttl">Reports</span>
        <span class="count">{{$summary['reports_count']}}</span>
    </div>
    <div class="dash-block-head-stats-item">
        <span class="ttl">Links</span>
        <span class="count">{{$summary['links_count']}}</span>
    </div>
</div>
<div class="dash-posts-chart-header">
    <h5>Views</h5>
    <p>Last update: <span>{{$summary['post_view']['last_update']}}</span></p>
</div>
<div class="dash-inner posts-chart-wrapper">
    <canvas id="postsViewsChart" data-post-label="{{json_encode($summary['post_view']['label'])}}" data-post-value="{{json_encode($summary['post_view']['values'])}}"></canvas>
</div>
