@foreach($follows as $follow)
<div class="why-block">
    <div class="img-wrap">
        <img src="{{check_profile_picture($follow->follower->profile_picture)}}" class="follower-user-img" alt="image">
    </div>
    <div class="link-wrap">
        <p><a href="{{url('profile/'.$follow->follower->id)}}" class="link"><b>{{$follow->follower->name}}</b></a></p>
    </div>
    <div class="icon-wrap">
        <i class="trav-user-icon"></i>
    </div>
    <div class="text-wrap">
        <p>{{$type == 'follow'?'Followed':'Unfollowed'}} you naturally after visiting your {{$follow->from_where}}</p>
    </div>
</div>
@endforeach