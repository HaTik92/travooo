<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl">@lang('dashboard.demographics')</h4>
    </div>
    <div class="dash-inner">
        <p>@lang('dashboard.aggregated_demographic_data_about_the_people_who_engaged_with_your_content')</p>
        <div class="dash-demographics-graph">
            <div class="dash-demographics-graph-row gender">
                <div class="dash-demographics-graph-row-title">
                    <h4>Men <span>{{$total_count > 0?round( $man_count/$total_count * 100):0}}%</span></h4>
                </div>
                <div class="dash-demographics-graph-row-data">
                    @foreach($interaction_men_ages as $interaction_men_age)
                     <div class="dash-demographics-graph-row-data-bar" style="height: {{$man_count > 0?round( $interaction_men_age/$man_count * 100)*4:0}}px" data-percent="{{$man_count > 0?round( $interaction_men_age/$man_count * 100):0}}%"></div>
                    @endforeach
                </div>
            </div>
            <div class="dash-demographics-graph-row age">
                <div class="dash-demographics-graph-row-title">
                    <h4>Age</h4>
                </div>
                <div class="dash-demographics-graph-row-data">
                    <div class="dash-demographics-graph-row-age-range">18 - 22</div>
                    <div class="dash-demographics-graph-row-age-range">23 - 30</div>
                    <div class="dash-demographics-graph-row-age-range">31 - 36</div>
                    <div class="dash-demographics-graph-row-age-range">37 - 48</div>
                    <div class="dash-demographics-graph-row-age-range">49 - 58</div>
                    <div class="dash-demographics-graph-row-age-range">59+</div>
                </div>
            </div>
            <div class="dash-demographics-graph-row gender women">
                <div class="dash-demographics-graph-row-title">
                    <h4>Women <span>{{$total_count > 0?round( $woman_count/$total_count * 100):0}}%</span></h4>
                </div>
                <div class="dash-demographics-graph-row-data">
                     @foreach($interaction_women_ages as $interaction_women_age)
                      <div class="dash-demographics-graph-row-data-bar" style="height: {{$woman_count > 0?round( $interaction_women_age/$woman_count * 100)*4:0}}px" data-percent="{{$woman_count > 0?round( $interaction_women_age/$woman_count * 100):0}}%"></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="post-block post-dashboard-inner post-map-block">
    <div class="block-head">
        <h4 class="ttl">@lang('dashboard.region')</h4>
    </div>
    <div class="dash-inner">
        <div class="post-map-inner">
        <div id="dashboardInteractionMap" data-int_mapList="{{$world_countries}}" class="dashboard-world-datamap" style="width: 650px; height: 455px;"></div>
            <div class="post-map-engagement-block">
                <div class="block-ttl">@lang('dashboard.engagement_key')</div>
                <div class="post-engagement-inner-block">
                    <span>@lang('buttons.general.more')</span>
                    <div class="eng-progress"></div>
                    <span>@lang('dashboard.less')</span>
                </div>
            </div>
        </div>
        <div class="block-head internal-block with-tab-list">
            <h4 class="ttl">@lang('dashboard.top_engaged_countries')&nbsp;(&nbsp;
                <div class="sort-by-select">
                    <div class="sort-select-wrap">
                        <select class="sort-select interaction-country-filter" placeholder="7 days">
                            <option value="30">@lang('time.count_days', ['count' => 30])</option>
                            <option value="15">@lang('time.count_days', ['count' => 15])</option>
                            <option value="7">@lang('time.count_days', ['count' => 7])</option>
                        </select>
                    </div>
                </div>
                &nbsp;)
            </h4>
            <ul class="nav nav-tabs bar-list dash-interaction-filter-list" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" data-filter="countries" href="#"
                       role="tab">@lang('dashboard.countries')</a>
                </li>
            </ul>
        </div>
        <div class="table-responsive dash-interaction-tab-table countries">
            <table class="table tbl-dashboard tbl-eng-region">
                <tr>
                    <th>@lang('dashboard.countries')</th>
                    <th class="eng-cell">@lang('dashboard.engagements')</th>
                    <th class="eng-cell">@lang('dashboard.likes')</th>
                    <th class="eng-cell">@lang('comment.comments')</th>
                    <th class="eng-cell">@lang('dashboard.shares')</th>
                </tr>
                <tbody class="interaction-countries-list">
                @foreach($interaction_countries as $country)
                    <tr>
                        <td>
                            <img class="flag" src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{$country['iso_code']}}.png" style="width: 26px !important;" alt="flag"> {{$country['country_name']}}
                        </td>
                        <td>
                            {{isset($country['engagements'])?$country['engagements']:0}}
                        </td>
                        <td>
                            {{isset($country['comments'])?$country['comments']:0}}
                        </td>
                        <td>
                            {{isset($country['likes'])?$country['likes']:0}}
                        </td>
                        <td>
                            {{isset($country['shares'])?$country['shares']:0}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>