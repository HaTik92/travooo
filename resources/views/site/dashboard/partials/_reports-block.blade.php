<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl">@lang('dashboard.summary')</h4>
    </div>
    <div class="dash-reports-chart-header">
        @if($last_view)
        <p>Last update: <span>{{diffForHumans($last_view->created_at)}}</span></p>
        @endif
    </div>
    <div class="dash-inner reports-chart-wrapper">
        <canvas id="reportsChart" data-reports-label="{{$reports_label}}" data-reports-view="{{$reports_views}}" data-reports-share="{{$reports_shares}}" data-reports-comment="{{$reports_comments}}" data-reports-like="{{$reports_likes}}"></canvas>
    </div>
</div>
<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl">@lang('dashboard.all_reports')</h4>
    </div>
    <div class="dash-info-inner">
        <ul class="info-list">
            <li>
                <span class="color-block primary"></span>
                <span class="txt">@lang('other.views')</span>
            </li>
            <li>
                <span class="color-block orange"></span>
                <span class="txt">@lang('dashboard.reactions_comments_shares')</span>
            </li>
        </ul>
        <div class="sort-by-select">
            <label>@lang('dashboard.last')</label>
            <div class="sort-select-wrap">
                <select class="sort-select reports-tab-filter" placeholder="30 days">
                   <option value="30">@lang('time.count_days', ['count' => 30])</option>
                    <option value="15">@lang('time.count_days', ['count' => 15])</option>
                    <option value="7">@lang('time.count_days', ['count' => 7])</option>
                </select>
            </div>
        </div>
    </div>
    <div class="dash-inner">
        <div class="table-responsive">
            <table class="table tbl-dashboard">
                <tr>
                    <th class="publ">@lang('dashboard.published')</th>
                    <th class="post">@lang('dashboard.post')</th>
                    <th class="prog-cell">@lang('other.views')</th>
                    <th class="prog-cell">@lang('dashboard.engagement')</th>
                </tr>
                @foreach($all_reports as $report)
                <tr>
                    <td>
                        <span class="dt">{{date('d/m/Y', strtotime($report->created_at))}}<br>
                            {{date('H:i a', strtotime($report->created_at))}}</span>
                    </td>
                    <td>
                        <a href="{{url('reports/'.$report->id)}}" target="_blank" class="post-tbl-link">
                            <img src="{{isset($report->cover[0])?$report->cover[0]->val:asset('assets2/image/placeholders/no-photo.png')}}" class="dash-posts-report-img" alt="image">
                            {{$report->title}}
                        </a>
                    </td>
                    <td>
                        <div class="tbl-progress">
                            <div class="progress-label">{{$report->views_count}}</div>
                            <div class="progress">
                                <div class="progress-bar primary" role="progressbar"
                                     aria-valuenow="0" aria-valuemin="{{calculate_percent($report->views_count)}}"
                                     aria-valuemax="100" style="width: {{calculate_percent($report->views_count)}}%;"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="tbl-progress">
                            <div class="progress-label">{{$report->comments_count + $report->likes_count + $report->shares_count}}</div>
                            <div class="progress">
                                <div class="progress-bar orange" role="progressbar"
                                     aria-valuenow="{{calculate_percent($report->comments_count + $report->likes_count + $report->shares_count)}}" aria-valuemin="0"
                                     aria-valuemax="100" style="width: {{calculate_percent($report->comments_count + $report->likes_count + $report->shares_count)}}%;"></div>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>