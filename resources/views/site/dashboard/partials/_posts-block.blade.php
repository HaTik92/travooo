<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl">Summary</h4>
        <div class="sort-by-select">
            <label>@lang('dashboard.last')</label>
            <div class="sort-select-wrap">
                <select class="sort-select posts-summary-filter" placeholder="7 days">
                    <option value="30">@lang('time.count_days', ['count' => 30])</option>
                    <option value="15">@lang('time.count_days', ['count' => 15])</option>
                    <option value="7">@lang('time.count_days', ['count' => 7])</option>
                </select>
            </div>
        </div>
    </div>
    <div class="posts-summary-block">
        @include('site.dashboard.partials._posts-summary-block')
    </div>
</div>
<div class="post-block post-dashboard-inner">
    <div class="block-head with-tab-list">
        <h4 class="ttl">@lang('dashboard.all_reports')</h4>
        <ul class="nav nav-tabs bar-list dash-posts-filter-list" role="tablist" dir="auto">
            <li class="nav-item" dir="auto">
                <a class="nav-link active" data-filter="all" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="true">All</a>
            </li>
            <li class="nav-item" dir="auto">
                <a class="nav-link" data-filter="text" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="false">Text</a>
            </li>
            <li class="nav-item" dir="auto">
                <a class="nav-link" data-filter="image" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="false">Image</a>
            </li>
            <li class="nav-item" dir="auto">
                <a class="nav-link" data-filter="video" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="false">Video</a>
            </li>
            <li class="nav-item" dir="auto">
                <a class="nav-link" data-filter="plan" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="false">Trip Plan</a>
            </li>
            <li class="nav-item" dir="auto">
                <a class="nav-link" data-filter="report" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="false">Reports</a>
            </li>
            <li class="nav-item" dir="auto">
                <a class="nav-link" data-filter="link" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="false">Links</a>
            </li>
        </ul>
    </div>
    <div class="dash-info-inner">
        <ul class="info-list">
            <li>
                <span class="color-block primary"></span>
                <span class="txt">@lang('other.views')</span>
            </li>
            <li>
                <span class="color-block light-violet"></span>
                <span class="txt">@lang('dashboard.post_clicks')</span>
            </li>
            <li>
                <span class="color-block orange"></span>
                <span class="txt">@lang('dashboard.reactions_comments_shares')</span>
            </li>
        </ul>
        <div class="sort-by-select">
            <label>@lang('dashboard.last')</label>
            <div class="sort-select-wrap">
                <select class="sort-select posts-reports-filter" placeholder="7 days">
                    <option value="30">@lang('time.count_days', ['count' => 30])</option>
                    <option value="15">@lang('time.count_days', ['count' => 15])</option>
                    <option value="7">@lang('time.count_days', ['count' => 7])</option>
                </select>
            </div>
        </div>
    </div>
    <div class="dash-inner posts-reports-block">
        @include('site.dashboard.partials._posts-reports-block')
    </div>
</div>