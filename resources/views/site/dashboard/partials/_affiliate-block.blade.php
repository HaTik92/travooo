@foreach($my_links AS $link)
<tr>
    <td>
        <span class="dt">{{$link->created_at->format('d M Y')}}</span>
    </td>
    <td>
        <a href="{{$link->target}}" class="post-tbl-link" target="_blank">
            <!--<img src="http://placehold.it/40x43" alt="image">-->
            {{$link->target}}
        </a>
    </td>
    <td>
        <div class="tbl-progress">
            <?php
            $c = count($link->clicks()->get());
            $cc = count($link->clicks()->get()) * rand(8, 15);
            ?>
            <div class="progress-label">{{$cc}}</div>
            <div class="progress">
                <div class="progress-bar primary" role="progressbar"
                     aria-valuenow="{{$cc}}" aria-valuemin="10"
                     aria-valuemax="100" style="width: {{$cc*2}}%;"></div>
            </div>
        </div>
    </td>
    <td>
        <div class="tbl-progress">
            <div class="progress-label">{{$c}}</div>
            <div class="progress">
                <div class="progress-bar light-violet"
                     role="progressbar" aria-valuenow="{{$c}}"
                     aria-valuemin="1" aria-valuemax="10"
                     style="width: {{$c*10}}%;"></div>
            </div>
        </div>
    </td>
    <td>
        @if($link->exits()->count()>0)
        {{$link->exits()->count()}} 
        @else
        0
        @endif
    </td>
    <td>
        @if($link->exits()->count()>0)
        {{$link->exits()->count()*0.12}} $
        @else
        0
        @endif
    </td>
</tr>
@endforeach