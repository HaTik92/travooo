<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl">@lang('dashboard.summary')</h4>
        <div class="sort-by-select">
            <label>@lang('dashboard.last')</label>
            <div class="sort-select-wrap">
                <select class="sort-select overview-filter" placeholder="7 days">
                    <option value="30">@lang('time.count_days', ['count' => 30])</option>
                    <option value="15">@lang('time.count_days', ['count' => 15])</option>
                    <option value="7">@lang('time.count_days', ['count' => 7])</option>
                </select>
            </div>
        </div>
    </div>
    <div class="dash-overview-inner">
        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">@lang('other.views')</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="The total number of views your posts have garnered for the last {{$daysCount}}."></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$post_view['count']}}</div>
                    <div class="view-subttl">@lang('dashboard.total_views')</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="viewsChart" data-overview-item="{{json_encode($post_view['values'])}}" data-overview-label='{{json_encode($post_view['label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">Comments</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="The total number of comments your posts have garnered for the last {{$daysCount}}."></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$post_comment['count']}}</div>
                    <div class="view-subttl">@lang('dashboard.total_views')</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="commentsChart" data-overview-item="{{json_encode($post_comment['values'])}}" data-overview-label='{{json_encode($post_comment['label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">Engagements</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="The total number of comments your posts have garnered for the last {{$daysCount}}."></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$post_engagement['count']}}</div>
                    <div class="view-subttl">@lang('dashboard.total_views')</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="engagementsChart" data-overview-item="{{json_encode($post_engagement['values'])}}" data-overview-label='{{json_encode($post_engagement['label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">@lang('profile.followers')</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" data-toggle="tooltip" data-animation="false"
                       title="Number of users who have followed you for the last {{$daysCount}}."></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$user_followers['follow_count']}}</div>
                    <div class="view-subttl">@lang('dashboard.total_followers')</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="followersChart" data-overview-item="{{json_encode($user_followers['daily_values'])}}" data-overview-label='{{json_encode($user_followers['follow_label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">Recent Followers</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" data-toggle="tooltip" data-animation="false"
                       title="Number of users who have followed you for the last {{$daysCount}}."></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{count($resent_followers)}}</div>
                    <div class="view-subttl">@lang('dashboard.total_followers')</div>
                </div>
                <ul class="recent-followers-list">
                    @if(count($resent_followers) > 0)
                        @foreach($resent_followers as $resent_follower)
                            <li>
                                <a href="{{url('profile/'.$resent_follower->id)}}"><img src="{{$resent_follower->profile_picture}}" alt="User Avatar"></a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">@lang('dashboard.unfollowers')</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="Number of users who unfollowed you for the last {{$daysCount}}."></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$user_unfollowers['follow_count']}}</div>
                    <div class="view-subttl">@lang('dashboard.total_unfollowers')</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="unfollowersChart" data-overview-item="{{json_encode($user_unfollowers['daily_values'])}}" data-overview-label='{{json_encode($user_unfollowers['follow_label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block mobile-fullwidth">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">@lang('dashboard.top_engaged_countries')</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="The top countries from where you have received the most feedback for the last {{$daysCount}}."></i>
                </div>
            </div>
            <ul class="over-list locations-list">
                @foreach($countries['list'] AS $vc)
                <li>
                    <span class="name" title="{{$vc['name']}}"><img class="flag" src="{{$vc['url']}}" style="width: 26px !important;" alt="flag"> {{$vc['name']}}</span>
                    <span class="count">{{$vc['count']}}</span>
                </li>
                @endforeach   
            </ul>
        </div>

        <div class="overview-block mobile-fullwidth">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">@lang('dashboard.popularity_map')</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="Your popularity on the globe for the last {{$daysCount}}."></i>
                </div>
            </div>
            <div class="map-inner">
                <div id="dashboardOverviewMap" data-overview-map="{{json_encode($countries['values'])}}" class="dashboard-world-datamap" style="width: 100%; height: 160px"></div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">Text Posts</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="Title"></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$post_text['count']}}</div>
                    <div class="view-subttl">Total views</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="textPostsChart" data-overview-item="{{json_encode($post_text['values'])}}" data-overview-label='{{json_encode($post_text['label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">Image Posts</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="Title"></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$post_image['count']}}</div>
                    <div class="view-subttl">Total views</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="imagePostsChart" data-overview-item="{{json_encode($post_image['values'])}}" data-overview-label='{{json_encode($post_image['label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">Video Posts</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="Title"></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$post_video['count']}}</div>
                    <div class="view-subttl">Total views</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="videoPostsChart" data-overview-item="{{json_encode($post_video['values'])}}" data-overview-label='{{json_encode($post_video['label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">Links</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="Title"></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$links['count']}}</div>
                    <div class="view-subttl">Total views</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="linksChart" data-overview-item="{{json_encode($links['values'])}}" data-overview-label='{{json_encode($links['label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">Trip Plans</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="Title"></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$trip_plan['count']}}</div>
                    <div class="view-subttl">Total views</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="tripPlansChart" data-overview-item="{{json_encode($trip_plan['values'])}}" data-overview-label='{{json_encode($trip_plan['label'])}}'></canvas>
                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">Reports</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="Title"></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$reports['count']}}</div>
                    <div class="view-subttl">Total views</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="reportsOverviewChart" data-overview-item="{{json_encode($reports['values'])}}" data-overview-label='{{json_encode($reports['label'])}}'></canvas>


                </div>
            </div>
        </div>
        
        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">@lang('dashboard.follow_rate')</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="The rate at which other users have been following you for the last {{$daysCount}}."></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count"></div>
                    <div class="view-subttl">@lang('dashboard.more_data_is_required_to_build_this_stats')
                    </div>
                </div>
                <div class="graph-wrap">
                    <canvas id="followRateChart" data-overview-item="{{json_encode($user_followers['daily_values'])}}" data-overview-label='{{json_encode($user_followers['follow_label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">@lang('dashboard.referrals_achieved')</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="Number of clicks on your referral links for the last {{$daysCount}}"></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count">{{$referral['count']}}</div>
                    <div class="view-subttl">@lang('dashboard.total_referrals')</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="referralsChart" data-overview-item="{{json_encode($referral['values'])}}" data-overview-label='{{json_encode($referral['label'])}}'></canvas>


                </div>
            </div>
        </div>

        <div class="overview-block">
            <div class="over-head">
                <div class="head-label">
                    <div class="ttl">@lang('dashboard.revenue')</div>
                    <div class="period">{{$period}}</div>
                </div>
                <div class="head-info">
                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip" data-animation="false"
                       title="The amount of revenue made by you for the last {{$daysCount}}."></i>
                </div>
            </div>
            <div class="views-block">
                <div class="view-top">
                    <div class="view-count"></div>
                    <div class="view-subttl">USD</div>
                </div>
                <div class="graph-wrap">
                    <canvas id="revenueChart" data-overview-item="[12, 19, 3, 5, 0, 1, 2, 3, 6, 15, 7, 8, 9, 45]" data-overview-label='["", "", "", "", "", "", "", "", "", "", "", "", "", ""]'></canvas>


                </div>
            </div>
        </div>

    </div>
</div>