<!-- Milestone popup -->
<div class="modal fade white-style" data-backdrop="false" id="milestonePopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog milestone-popup-dialog" role="document">
        <div class="milestone-popup-top">
            <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                <i class="trav-close-icon"></i>
            </button>
            <img src="https://travooo.com/assets2/image/placeholders/pattern.png" alt="">
            <div class="exp-badge-transition">
                @if ($current_badge)
                <div class="exp-badge-transition-wrapper">
                    <span class=""><img class="exp-icon big" src="{{$current_badge->badge->url}}" alt="" dir="auto"></span>
                </div>
                <div class="exp-badge-transition-wrapper">
                    <span class=""><img class="exp-icon big" src="{{@$next_badge->url}}" alt="" dir="auto"></span>
                </div>
                @else
                    <div class="">
                        <span class=""><img class="exp-icon big" src="{{@$next_badge->url}}" alt="" dir="auto"></span>
                    </div>
                @endif
            </div>
        </div>
        <div class="milestone-popup-text">
            <h4>Thank you!</h4>
            <p>You application for the {{@$next_badge->name}} Badge is under review, you will be notified via email very soon.</p>
            <button class="btn btn-light-primary" data-dismiss="modal" aria-label="Close">Got it</button>
        </div>
    </div>
</div>
<!-- Milestone popup END -->