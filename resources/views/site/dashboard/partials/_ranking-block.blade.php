<div class="post-block post-dashboard-inner">
    <div class="block-head" dir="auto">
        <h4 class="ttl">Ranking</h4>
    </div>
    <div class="dash-ranking-grow-info">
        <h3>Grow with Travooo</h3>
        <div class="exp-range">
            <span class="exp-icon">EXP</span>
            <span class="exp-icon bronze">EXP</span>
            <span class="exp-icon silver">EXP</span>
            <span class="exp-icon gold">EXP</span>
        </div>
        <div class="text">Here you can track your badge progress by understanding what milestones are required to hop to the next level. You can also view what milestones you have already completed.</div>
        <div class="exp-progress">
            @if ($current_badge)
                <img class="exp-icon big" src="{{$current_badge->badge->url}}" alt="" />
            @else
                <span style="width: 87px; height: 58px; display: flex;"></span>
            @endif
            @if ($next_badge)
            <div class="exp-progress-bar">
                <div class="percentage">{{$overall_progress}}%</div>
                <div class="progress">
                    <div class="progress-bar" style="width: {{$overall_progress}}%" role="progressbar" aria-valuenow="{{$overall_progress}}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <img class="exp-icon big" src="{{$next_badge->url}}" alt="" />
            @endif
        </div>
    </div>
</div>
<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl">Milestones</h4>
        <!-- Just for popups demonstration -->
        @include('site.dashboard.partials._milestonePopup')
        <!-- Just for popups demonstration -->
    </div>
    <div class="dash-ranking-milestones">
        <div class="dash-ranking-milestone-item">
            <div class="title">
                <div>
                    <i class="fas fa-check-circle @if ($has_picture) success @endif"></i>
                    Upload a profile picture
                </div>
                @if ($has_picture)
                    <div class="progress-bar" style="width: 100.0%" role="progressbar" aria-valuenow="100.0" aria-valuemin="0" aria-valuemax="100"></div>
                @endif
            </div>
            <div class="description">
                <div class="text">Upload a profile picture of yourself. Please refrain from using pictures of someone or something that does not represent you.</div>
                <div class="value picture circle">
                    @if($has_picture)
                        <img src="{{check_profile_picture($user->profile_picture)}}" alt="">
                    @else
                        <a target="_blank" href="/profile"><div class="value picture"></div></a>
                    @endif
                </div>
            </div>
        </div>
        <div class="dash-ranking-milestone-item">
            <div class="title">
                <div>
                    <i class="fas fa-check-circle @if ($has_cover) success @endif"></i>
                    Upload a cover picture
                </div>
                @if ($has_cover)
                    <div class="progress-bar" style="width: 100.0%" role="progressbar" aria-valuenow="100.0" aria-valuemin="0" aria-valuemax="100"></div>
                @endif
            </div>
            <div class="description">
                <div class="text">Show off your travel experiences to the visitors of your profile by uploading an awe-inspiring cover image.</div>
                <div class="value picture">
                    @if($has_cover)
                        <img src="{{$user->cover_photo}}" alt="">
                    @else
                        <a target="_blank" href="/profile"><div class="value picture"></div></a>
                    @endif
                </div>
            </div>
        </div>
        <div class="separator"></div>

        @if ($need_followers < $normally_need_followers || $need_interactions < $normally_need_interactions)
        <!-- Congrats -->
        <div class="dash-ranking-milestone-item congrats">
            <h4>Congratulations!</h4>
            <p>Normally every new expert will need to reach <b>{{number_format(0 - $need_followers/1000)}}K followers</b> and <b>{{number_format(0 - $need_interactions/1000)}}K interactions</b>
                to earn the Gold badge, but we have awarded it to you for free. You have unlocked all badges there were
                to unlock. However, Travooo is a lifelong journey. Maintain your spot as the king of the hill by sharing
                your travel experiences and helping others have theirs.</p>
        </div>
        @endif
        <!-- Congrats END -->
        <div class="dash-ranking-milestone-item">
            <div class="title">
                <div style="color: #cccccc;">
                    <i class="fas fa-check-circle @if ($followers_progress >= 100) success @endif"></i>
                    Reach {{number_format(0 - $need_followers/1000)}}k followers
                </div>
                <div class="progress-bar" style="width: {{$followers_progress}}%" role="progressbar" aria-valuenow="{{$followers_progress}}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="description">
                <div class="text">You need the above-mentioned number of followers to complete this milestone of your current badge. Your current overall follower count is:</div>
                <div class="value">
                    <b>{{number_format($followers)}}</b> Followers
                </div>
            </div>
        </div>
        <div class="dash-ranking-milestone-item">
            <div class="title">
                <div style="color: #cccccc;">
                    <i class="fas fa-check-circle @if ($interactions_progress >= 100) success @endif "></i>
                    Reach {{number_format(0 - $need_interactions/1000)}}k interactions
                </div>
                <div class="progress-bar" style="width: {{$interactions_progress}}%" role="progressbar" aria-valuenow="{{$interactions_progress}}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="description">
                <div class="text">You need the above-mentioned number of interactions to complete this milestone of your current badge. Your current interaction count is:</div>
                <div class="value">
                    <b>{{number_format($interactions)}}</b> Interactions
                </div>
            </div>
        </div>
    </div>
    <div class="dash-ranking-milestones-bottom">
        <div class="text">
            <h4>What’s next?</h4>
            <p>Click the button bellow when you reach all your milestones to apply for your next badge.</p>
        </div>
        @if (!$user->apply_to_badge)
        <button @if (!$has_cover || !$has_picture || $interactions_progress < 100 || $followers_progress < 100) disabled @endif class="btn btn-light-grey btn-bordered @if (!$has_cover || !$has_picture || $interactions_progress < 100 || $followers_progress < 100) disabled @endif">Apply for the next badge</button>
        @else
            <button disabled class="btn btn-light-grey btn-bordered disabled">Pending...</button>
        @endif
    </div>
</div>

<script>
    $(document).ready(function() {
        $('div.dash-ranking-milestones-bottom').on('click', 'button', function() {
            $.ajax({
                method: "POST",
                url: "{{ route('dashboard.apply_to_badge') }}",
            })
                .done(function (res) {
                    if (res.status === 'success') {
                        $('#milestonePopup').modal('show');
                    }
                });
        });
    })
</script>
