<div class="post-block post-dashboard-inner" dir="auto">
    <div class="block-head" dir="auto">
        <h4 class="ttl" dir="auto">Level Up</h4>
    </div>
    <div class="dash-global-badges-info" dir="auto">
       
        <div class="dash-global-badges-info-next" dir="auto">
            <h3 dir="auto">{{$title}}</h3>
            <div class="badge-details" dir="auto">
                <!-- Updated element -->
                <div class="icon-block">
                    <span class=""><img class="exp-icon" src="{{$badge_url}}" style="width: 40px !important;" alt="" dir="auto"></span>
                </div>
                <!-- Updated element END -->
                <div dir="auto">
                    <h5 class="badge-details-ttl" dir="auto">{{$badge_name}} Expert <span dir="auto">#{{$level['rank']}} <i>in</i> {{$level['title']}}</span></h5>
                    <div class="users-whith-badge" dir="auto">
                        <div class="users-whith-badge-list" dir="auto">
                            @foreach($users_with_same_badge['users'] as $user_list)
                                <img src="{{check_profile_picture(@$user_list->profile_picture)}}" alt="" dir="auto">
                            @endforeach
                        </div>
                        @if($users_with_same_badge['total'] > 0)
                            <div class="users-whith-badge-ttl" dir="auto">
                                {{$users_with_same_badge['total']}} users in your area of expertise have this badge
                            </div>
                        @endif
                    </div>
                </div>
               
            </div>
            @if ($has_next_badge)
            <div class="progress badge-progress" dir="auto">
                <div class="progress-bar" style="width: {{$overall_progress}}%" role="progressbar" aria-valuenow="{{$overall_progress}}" aria-valuemin="0" aria-valuemax="100" dir="auto"></div>
            </div>
            <div class="points-to-next" dir="auto">
                <b>{{100 - $overall_progress}}%</b> left to your next level
            </div>
            @endif
        </div>
    </div>
</div>
@if(count($leaderboard_lists) > 0)
    <div class="post-block post-dashboard-inner leaderboard" style="max-width: 870px;" dir="auto">
        <div class="block-head with-tab-list" dir="auto">
            <h4 class="ttl" dir="auto">Leaderboard</h4>
        </div>
        <div class="dash-info-inner" dir="auto">
            <h5 class="ttl" dir="auto">Area of expertise</h5>
            <ul class="nav nav-tabs bar-list dash-global-area-filter-list" role="tablist" dir="auto" style="overflow: auto;">
                @foreach($exp_lists as $country_id=>$country_name)
                    <li class="nav-item dash-global-area-filter-item {{$country_id}}" dir="auto">
                        <a class="nav-link dash-global-area-filter-link" data-filter="{{$country_id}}" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="true">{{$country_name}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="dash-inner" dir="auto">
            <div class="table-responsive" dir="auto">
                <table class="table tbl-dashboard" dir="auto">
                    <tbody dir="auto"><tr dir="auto">
                            <th class="rank" dir="auto">Rank</th>
                            <th class="post" dir="auto">Name</th>
                            <th class="followers" dir="auto">Followers</th>
                            <th class="interactions" dir="auto">Interactions</th>
                        </tr>
                    </tbody><tbody dir="auto">
                        @foreach($leaderboard_lists as $country_id=>$interaction_list)
                            @if ($interaction_list)
                                @foreach($interaction_list as $interaction)
                                        @if ($exp_lists && isset($interaction['user'],$exp_lists[$country_id],$interaction['interactions_progress'],$interaction['interactions_count'],$interaction['followers_progress'],$interaction['followers_count']))
                                            <tr class="dash-global-table-row all {{$country_id}}" dir="auto">
                                                <td dir="auto">
                                                    <div class="dash-global-table-rank" dir="auto">
                                                        <span class="rank" dir="auto">#{{$loop->index+1}}</span>
                                                        <span class="rank-move up" dir="auto">
                                                            <i class="trav-arrow-up-icon" dir="auto"></i>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td dir="auto">
                                                    <span class="tbl-leader-info" dir="auto">
                                                        <img src="{{check_profile_picture($interaction['user']['profile_picture'])}}" alt="User Avatar" dir="auto">
                                                        <span class="details" dir="auto">
                                                            <a href="porfile/{{$interaction['user']['id']}}" target="_blank" class="name" dir="auto">{{$interaction['user']['name']}}</a>
                                                            <span class="expertise" dir="auto">
                                                                <span dir="auto">from</span> <a href="country/{{$country_id}}" target="_blank" dir="auto">{{$exp_lists[$country_id]}}</a>
                                                            </span>
                                                        </span>
                                                    </span>
                                                </td>
                                                <td dir="auto">
                                                    <div class="tbl-progress" dir="auto">
                                                        <div class="progress-label" dir="auto">{{$interaction['followers_count']}}</div>
                                                        <div class="progress" dir="auto">
                                                            <div class="progress-bar primary" role="progressbar" aria-valuenow="{{$interaction['followers_progress']}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$interaction['followers_progress']}}%;" dir="auto"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td dir="auto">
                                                    <div class="tbl-progress" dir="auto">
                                                        <div class="progress-label" dir="auto">{{$interaction['interactions_count']}}</div>
                                                        <div class="progress" dir="auto">
                                                            <div class="progress-bar orange" role="progressbar" aria-valuenow="{{$interaction['interactions_progress']}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$interaction['interactions_progress']}}%;" dir="auto"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                @endforeach
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
