<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl light">@lang('dashboard.total_views_as_of_today'):
            <span>{{optimize_counter($engag_view['daily_count'])}}</span></h4>
        <div class="sort-by-select">
            <label>Last</label>
            <div class="sort-select-wrap">
                <select class="sort-select view-engagement" placeholder="7 days">
                    <option value="30">@lang('time.count_days', ['count' => 30])</option>
                    <option value="15">@lang('time.count_days', ['count' => 15])</option>
                    <option value="7">@lang('time.count_days', ['count' => 7])</option>
                </select>
            </div>
        </div>
    </div>
    <div class="dash-sidebar-inner">
        <div class="dash-main engagement-chart-wrapper">
            <canvas id="totalviewsasofChart" class="view-engagement-canvas" data-placeholder-title="Views" data-engagement-label="{{$engag_view['engagement_label']}}" data-engagement-daily="{{$engag_view['daily_value']}}" data-engagement-total="{{$engag_view['total_value']}}"></canvas>
        </div>
        <div class="dash-sidebar">
            <h4 class="side-ttl">@lang('dashboard.benchmark')</h4>
            <div class="side-txt">
                <p>@lang('dashboard.compare_your_average_preformance_over_time')</p>
            </div>
            <div class="side-txt border-disabled">
                <p><b>@lang('dashboard.learn_what_sources_lead_to_your_views')</b></p>
            </div>
            <a href="javascript:;"
               class="btn btn-light-grey btn-bordered dig-deeper-btn" digtype="Views">@lang('dashboard.dig_deeper')</a>
        </div>
    </div>
</div>
<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl light">@lang('dashboard.total_comments_as_of_today'): <span>{{optimize_counter($engag_comment['daily_count'])}}</span>
        </h4>
        <div class="sort-by-select">
            <label>@lang('dashboard.last')</label>
            <div class="sort-select-wrap">
                <select class="sort-select comment-engagement" placeholder="7 days">
                    <option value="30">@lang('time.count_days', ['count' => 30])</option>
                    <option value="15">@lang('time.count_days', ['count' => 15])</option>
                    <option value="7">@lang('time.count_days', ['count' => 7])</option>
                </select>
            </div>
        </div>
    </div>
    <div class="dash-sidebar-inner">
        <div class="dash-main engagement-chart-wrapper">
            <canvas id="totalcommentsasofChart" class="comment-engagement-canvas" data-placeholder-title="Comments" data-engagement-label="{{$engag_comment['engagement_label']}}" data-engagement-daily="{{$engag_comment['daily_value']}}" data-engagement-total="{{$engag_comment['total_value']}}"></canvas>
        </div>
        <div class="dash-sidebar">
            <h4 class="side-ttl">@lang('dashboard.benchmark')</h4>
            <div class="side-txt">
                <p>@lang('dashboard.compare_your_average_preformance_over_time')</p>
            </div>
            <div class="side-txt border-disabled">
                <p><b>@lang('dashboard.learn_what_sources_lead_to_your_comments')</b>
                </p>
            </div>
            <a href="#"
               class="btn btn-light-grey btn-bordered dig-deeper-btn" digtype="Comments">@lang('dashboard.dig_deeper')</a>
        </div>
    </div>
</div>
<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl light">@lang('dashboard.total_likes_as_of_today'): <span>{{optimize_counter($engag_like['daily_count'])}}</span>
        </h4>
        <div class="sort-by-select">
            <label>@lang('dashboard.last')</label>
            <div class="sort-select-wrap">
                <select class="sort-select likes-engagement" placeholder="7 days">
                    <option value="30">@lang('time.count_days', ['count' => 30])</option>
                    <option value="15">@lang('time.count_days', ['count' => 15])</option>
                    <option value="7">@lang('time.count_days', ['count' => 7])</option>
                </select>
            </div>
        </div>
    </div>
    <div class="dash-sidebar-inner">
        <div class="dash-main engagement-chart-wrapper">
            <canvas id="totallikesasofChart" class="likes-engagement-canvas" data-placeholder-title="Likes" data-engagement-label="{{$engag_like['engagement_label']}}" data-engagement-daily="{{$engag_like['daily_value']}}" data-engagement-total="{{$engag_like['total_value']}}"></canvas>
        </div>
        <div class="dash-sidebar">
            <h4 class="side-ttl">@lang('dashboard.benchmark')</h4>
            <div class="side-txt">
                <p>@lang('dashboard.compare_your_average_preformance_over_time')</p>
            </div>
            <div class="side-txt border-disabled">
                <p><b>@lang('dashboard.learn_what_sources_lead_to_your_likes')</b></p>
            </div>
            <a href="#"
               class="btn btn-light-grey btn-bordered dig-deeper-btn" digtype="Likes">@lang('dashboard.dig_deeper')</a>
        </div>
    </div>
</div>

<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl light">@lang('dashboard.total_shares_as_of_today'): <span>{{optimize_counter($engag_share['daily_count'])}}</span>
        </h4>
        <div class="sort-by-select">
            <label>@lang('dashboard.last')</label>
            <div class="sort-select-wrap">
                <select class="sort-select share-engagement" placeholder="7 days">
                    <option value="30">@lang('time.count_days', ['count' => 30])</option>
                    <option value="15">@lang('time.count_days', ['count' => 15])</option>
                    <option value="7">@lang('time.count_days', ['count' => 7])</option>
                </select>
            </div>
        </div>
    </div>
    <div class="dash-sidebar-inner">
        <div class="dash-main engagement-chart-wrapper">
            <canvas id="totalsharesasofChart" class="share-engagement-canvas" data-placeholder-title="Shares" data-engagement-label="{{$engag_share['engagement_label']}}" data-engagement-daily="{{$engag_share['daily_value']}}" data-engagement-total="{{$engag_share['total_value']}}"></canvas>
        </div>
        <div class="dash-sidebar">
            <h4 class="side-ttl">@lang('dashboard.benchmark')</h4>
            <div class="side-txt">
                <p>@lang('dashboard.compare_your_average_preformance_over_time')</p>
            </div>
            <div class="side-txt border-disabled">
                <p><b>@lang('dashboard.learn_what_sources_lead_to_your_shares')</b></p>
            </div>
            <a href="#"
               class="btn btn-light-grey btn-bordered dig-deeper-btn" digtype="Shares">@lang('dashboard.dig_deeper')</a>
        </div>
    </div>
</div>


<!-- Engagement view Dig-popup -->
<div class="modal fade white-style dashboard-dig-popup" data-backdrop="false" id="engagementDigPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-setting-block search-travel-mates">
                <div class="post-side-top">
                    <h3 class="side-ttl">Content details</h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="post-block post-dashboard-inner mt-0">
                    <div class="dash-info-inner">
                        Lorem ipsum dolor sit amet consectetur adipiscing elit
                        <div class="sort-by-select">
                            <label>Last</label>
                            <div class="sort-select-wrap">
                                <div class="sort-select-wrapper">
                                    <select class="sort-select engagement-dig-filter" placeholder="7 days">
                                        <option value="30">30 Days</option>
                                        <option value="15">15 Days</option>
                                        <option value="7">7 Days</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dash-inner posts-reports-block">
                        <div class="table-responsive engagement-dig-block">
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Dig deeper popup -->