<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl light">@lang('dashboard.followers_as_of_today'): <span>{{optimize_counter($followers['daily_count'])}}</span></h4>
        <div class="sort-by-select">
            <label>@lang('dashboard.last')</label>
            <div class="sort-select-wrap">
                <select class="sort-select followers-filter" placeholder="30 days">
                    <option value="30">@lang('time.count_days', ['count' => 30])</option>
                    <option value="15">@lang('time.count_days', ['count' => 15])</option>
                    <option value="7">@lang('time.count_days', ['count' => 7])</option>
                </select>
            </div>
        </div>
    </div>
    <div class="dash-sidebar-inner">
        <div class="dash-main engagement-chart-wrapper">
            <canvas id="totalfollowersasofChart" class="followers-canvas" data-placeholder-title="followers" data-follow-label="{{json_encode($followers['follow_label'])}}" data-follow-daily="{{json_encode($followers['daily_values'])}}" data-follow-total="{{json_encode($followers['total_values'])}}"></canvas>
        </div>
        <div class="dash-sidebar">
            <h4 class="side-ttl">@lang('dashboard.benchmark')</h4>
            <div class="side-txt">
                <p>@lang('dashboard.compare_your_average_preformance_over_time')</p>
            </div>
            <div class="side-txt">
                <ul class="txt-list">
                    <li>
                        <span>@lang('dashboard.total_followers')</span>
                        <i class="fa fa-check"></i></li>
                    <li>
                        <span>@lang('dashboard.daily_followers')</span>
                        <i class="fa fa-check"></i>
                    </li>
                </ul>
            </div>
            <div class="side-txt border-disabled">
                <p><b>@lang('dashboard.what_leaded_people_to_follow_you')</b></p>
            </div>
            <button type="button" class="btn btn-light-grey btn-bordered" data-placeholder-title="unfollowers" data-toggle="modal" data-target="#whyFollowPopup">@lang('dashboard.learn_more')</button>
        </div>
    </div>
</div>
<div class="post-block post-dashboard-inner">
    <div class="block-head">
        <h4 class="ttl light">@lang('dashboard.unFollowers_as_oftToday'): <span>{{optimize_counter($unfollowers['daily_count'])}}</span></h4>
        <div class="sort-by-select">
            <label>@lang('dashboard.last')</label>
            <div class="sort-select-wrap">
                <select class="sort-select unfollowers-filter" placeholder="30 days">
                    <option value="30">@lang('time.count_days', ['count' => 30])</option>
                    <option value="15">@lang('time.count_days', ['count' => 15])</option>
                    <option value="7">@lang('time.count_days', ['count' => 7])</option>
                </select>
            </div>
        </div>
    </div>
    <div class="dash-sidebar-inner">
        <div class="dash-main engagement-chart-wrapper">
            <canvas id="totalunfollowersasofChart" class="unfollowers-canvas" data-follow-label="{{json_encode($unfollowers['follow_label'])}}" data-follow-daily="{{json_encode($unfollowers['daily_values'])}}" data-follow-total="{{json_encode($unfollowers['total_values'])}}"></canvas>
        </div>
        <div class="dash-sidebar">
            <h4 class="side-ttl">@lang('dashboard.benchmark')</h4>
            <div class="side-txt">
                <p>@lang('dashboard.compare_your_average_preformance_over_time')</p>
            </div>
            <div class="side-txt">
                <ul class="txt-list">
                    <li>
                        <span>@lang('dashboard.total_unfollowers')</span>
                        <i class="fa fa-check"></i></li>
                    <li>
                        <span>@lang('dashboard.daily_unFollowers')</span>
                        <i class="fa fa-check"></i>
                    </li>
                </ul>
            </div>
            <div class="side-txt border-disabled">
                <p><b>@lang('dashboard.what_leaded_people_to_unfollow_you')</b></p>
            </div>
            <button type="button" class="btn btn-light-grey btn-bordered" data-toggle="modal" data-target="#whyUnfollowPopup">@lang('dashboard.learn_more')</button>
        </div>
    </div>
</div>



<!-- why follow popup -->
<div class="modal fade white-style" data-backdrop="false" id="whyFollowPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-840" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-setting-block search-travel-mates">
                <div class="post-side-top">
                    <h3 class="side-ttl">@lang('dashboard.what_leaded_people_to_follow_you')</h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="why-follow-unfollow-wrap followers-wrap">
                    <div class="why-f-u-inner why-followers-block">
                        @include('site.dashboard.partials._follow-modal-block', ['follows'=>$followers['followers_list'], 'type'=>'follow'])
                    </div>
                    <div class="notification-loader {{(count($followers['followers_list']) > 0 && count($followers['followers_list'])< 10)?'d-none':''}}" id="why_followers_loader">
                        <input type="hidden" id="followers_pagenum" value="1">
                        <span class="spinner">
                            <i class="trav-loading-icon"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- why unfollow popup -->
<div class="modal fade white-style" data-backdrop="false" id="whyUnfollowPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-840" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-setting-block search-travel-mates">
                <div class="post-side-top">
                    <h3 class="side-ttl">@lang('dashboard.what_leaded_people_to_unfollow_you')</h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="why-follow-unfollow-wrap">
                    <div class="why-f-u-inner why-unfollowers-block">
                        @include('site.dashboard.partials._follow-modal-block', ['follows'=>$unfollowers['followers_list'], 'type'=>'unfollow'])
                    </div>
                    <div class="notification-loader {{(count($unfollowers['followers_list']) > 0 && count($unfollowers['followers_list'])< 10)?'d-none':''}}" id="why_unfollowers_loader">
                        <input type="hidden" id="unfollowers_pagenum" value="1">
                        <span class="spinner">
                            <i class="trav-loading-icon"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
