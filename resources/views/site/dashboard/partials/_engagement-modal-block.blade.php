@if(count($dig_infos) > 0)
<table class="table tbl-dashboard">
    <tbody>
        <tr>
            <th class="post">Post</th>
            <th class="prog-cell">{{$dig_type}}</th>
        </tr>
        @foreach($dig_infos as $dig_info)
        @php $post_info = \App\Models\Posts\Posts::find($dig_info->posts_id);@endphp
        <tr class="dash-posts-table-row plan">
            <td width="48%">
                <a href="#" class="post-tbl-link flex">
                    @if(count($post_info->medias) > 0)
                    <img src="{{@$post_info->medias[0]->media->url}}" alt="image" class="dash-posts-report-img">
                    @else
                    <img src="{{asset('assets2/image/placeholders/pattern.png')}}" alt="image" class="dash-posts-report-img">
                    @endif
                    <div class="post-copy">
                        <div class="title">
                            @if(isset($post_info->text) && $post_info->text!='')
                                    <?php

                                $dom = new \DOMDocument;
                                @$dom->loadHTML($post_info->text);
                                $elements = @$dom->getElementsByTagName('div');
                                $text = $post_info->text;
                            ?>
                            {!! str_limit($text, 100, ' ...') !!}
                            @endif
                        </div>
                        <div class="timestamp">{{\Carbon\Carbon::parse($post_info->created_at)->format('d/m/Y - h:i a')}}</div>
                    </div>
                </a>
            </td>
            <td width="35%">
                <div class="tbl-progress">
                    <div class="progress-label">{{$dig_info->total}}</div>
                    <div class="progress">
                        <div class="progress-bar {{$progress_color}}" role="progressbar" aria-valuenow="{{$dig_info->total}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$dig_info->total}}%;"></div>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
    <div style="overflow: hidden;">No data found...</div>
@endif