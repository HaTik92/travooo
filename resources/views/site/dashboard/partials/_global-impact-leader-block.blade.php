@foreach($leader_badge_lists as $leader_badge)
<tr class="dash-global-table-row {{($leader_badge->point_author->expert == 2)?'expert':'all'}} {{$leader_badge->point_author->nationality}}">
    <td>
        <div class="dash-global-table-rank">
            <span class="rank">#{{substr(round($leader_badge->total_points), 0, 1)}}</span>
            <span class="rank-move up">
                <i class="trav-arrow-up-icon"></i>
            </span>
        </div>
    </td>
    <td>
        <span class="tbl-leader-info">
            <img src="{{check_profile_picture($leader_badge->point_author->profile_picture)}}" alt="User Avatar">
            <span class="details">
                <a href="{{url('profile/'.$leader_badge->point_author->id)}}" target="_blank" class="name">{{$leader_badge->point_author->name}}</a>
                <span class="expertise">
                    {{$leader_badge->point_author->interests?explode(',', $leader_badge->point_author->interests)[0]:''}}
                    <span>from</span> <a href="{{url('country/'.@$leader_badge->point_author->nationality)}}" target="_blank">{{@$leader_badge->point_author->country_of_nationality->transsingle->title}}</a>
                </span>
            </span>
        </span>
    </td>
    <td>
        <div class="tbl-progress">
            <div class="progress-label">{{round($leader_badge->total_points)}}</div>
            <div class="progress">
                <div class="progress-bar primary" role="progressbar"
                     aria-valuenow="{{calculate_percent($leader_badge->total_points)}}" aria-valuemin="0"
                     aria-valuemax="100" style="width: {{calculate_percent($leader_badge->total_points)}}%;"></div>
            </div>
        </div>
    </td>
    <td>
        <div class="dash-global-table-badges">
            @foreach($leader_badge->point_author->badges as $leader_badge)
                @if($leader_badge->level)
                    <img src="{{asset('assets2/image/badges/'.$leader_badge->badge->id.'_'.($leader_badge->level).'.png')}}" alt="{{$leader_badge->badge->name}}" data-toggle="tooltip" title="{{$leader_badge->badge->name}}">
                @endif
            @endforeach
        </div>
    </td>
</tr>
@endforeach