@php
$title = 'Dashboard';
@endphp

@extends('site.layouts.site')

@section('base')
@include('site/layouts/header')
<style>
    .over-list .name{
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: 200px;
    }
</style>
<div class="content-wrap">
    <div class="dashboard-wrap">
        <div class="top-bar">
            <div class="container-fluid">
                <div class="top-bar-inner">
                    <div class="ttl">Dashboard</div>
                    <ul class="nav nav-tabs bar-list" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="insightsPage" data-toggle="tab" href="" role="tab">Insights</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="affilationPage" href="" role="tab">Afillation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('profile')}}" role="tab">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('chat')}}" role="tab">Inbox @if($chat_count>0)<span class="counter">{{$chat_count}}</span>@endif</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('notification')}}" role="tab">Notifications @if($notification_count)<span class="counter">{{$notification_count}}</span>@endif</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('settings')}}" role="tab">Settings</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <!-- Tab panes -->
            <div class="tab-content dash-tab-content">
                <div class="tab-pane first-layout active" id="dashInsight" role="tabpanel">
                    <ul class="nav nav-tabs inside-list dsh-inside-list" role="tablist">
                        <li class="nav-item insights">
                            <a class="nav-link active" data-toggle="tab" href="#overview"
                               role="tab">@lang('dashboard.overview')</a>
                        </li>

                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#engagement"
                               role="tab">@lang('dashboard.engagement')</a>
                        </li>

                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#ranking" role="tab">Ranking</a>
                        </li>
                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#interaction" role="tab">Interaction Source</a>
                        </li>
                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#posts" role="tab">Posts</a>
                        </li>
                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#reports" role="tab">Reports</a>
                        </li>

                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#followers"
                               role="tab">@lang('dashboard.followers_unfollowers')</a>
                        </li>
                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#globalImpact"
                               role="tab">Global Impact</a>
                        </li>

                        <!-- Affilation nav -->
                        <li class="nav-item affilation">
                            <a class="nav-link" data-toggle="tab" href="#dashAffiliation" role="tab">Overview</a>
                        </li>
                        <li class="nav-item affilation">
                            <a class="nav-link disabled" data-toggle="tab" href=""
                               role="tab">Earnings</a>
                        </li>
                        <li class="nav-item affilation">
                            <a class="nav-link disabled" data-toggle="tab" href=""
                               role="tab">Source</a>
                        </li>
                        <li class="nav-item affilation">
                            <a class="nav-link disabled" data-toggle="tab" href=""
                               role="tab">Reports</a>
                        </li>
                        <li class="nav-item affilation">
                            <a class="nav-link disabled" data-toggle="tab" href=""
                               role="tab">Payouts</a>
                        </li>
                        <li class="nav-item affilation">
                            <a class="nav-link disabled" data-toggle="tab" href=""
                               role="tab">Settings</a>
                        </li>
                    </ul>
                    <div class="tab-content dash-inside-content">
                        <div class="tab-pane second-layout active" id="overview" role="tabpanel">
                            <div class="overview-block">
                            </div>
                            <div class="dash-content-loader overview-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/overview-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="engagement" role="tabpanel">
                            <div class="engagement-block">
                            </div>
                            <div class="dash-content-loader engagement-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/engagment-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="interaction" role="tabpanel">
                            <div class="interaction-block"></div>
                            <div class="dash-content-loader interaction-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/interaction-source-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="ranking" role="tabpanel">
                            <div class="ranking-block"></div>
                            <div class="dash-content-loader ranking-loader">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="posts" role="tabpanel">
                            <div class="posts-block"></div>
                            <div class="dash-content-loader posts-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/posts-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="reports" role="tabpanel">
                            <div class="reports-block"></div>
                            <div class="dash-content-loader reports-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/reports-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="followers" role="tabpanel">
                            <div class="followers-block"></div>
                            <div class="dash-content-loader followers-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/followers-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="globalImpact" role="tabpanel">
                            <div class="globalImpact-block"></div>
                            <div class="dash-content-loader globalImpact-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/global-impact-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>


                        <div class="tab-pane second-layout" id="dashAffiliation" role="tabpanel">
                            <div class="coming-soon">
                                <h4>Coming Soon...</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit aenean eget pharetra nisi quisque ultricies orci sed mollis varius</p>
                            </div>
                            <img src="{{asset('assets2/image/placeholders/dashboard-affilation-placeholder.png')}}" alt="Affilation placeholder">
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="dashSetting" role="tabpanel">@lang('dashboard.pane') 4</div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('before_scripts')
@include('site.dashboard._modals')
@endsection

@section('after_scripts')
@include('site.dashboard._scripts')
@endsection