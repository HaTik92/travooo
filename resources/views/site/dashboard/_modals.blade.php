<!-- modal -->
<!-- would visit place popup -->
<div class="modal fade white-style" data-backdrop="false" id="seeAllModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-780" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-all-notification">
                <div class="block-head">
                    <h4 class="ttl">@lang('dashboard.all_notifications')</h4>
                </div>
                <div class="notification-wrap">
                    <div class="conversation-inner bordered">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

