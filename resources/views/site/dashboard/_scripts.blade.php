<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/topojson/1.6.9/topojson.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datamaps/0.5.9/datamaps.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.js"></script>
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
<script>
    var order = '';
    var checkFollowersOut = true;
    var checkUnFollowersOut = true;
    var followers_filter = unfollowers_filter = 30;
    var global_impact_filter = '';
    var dig_type = '';
    var isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints > 0;

    var hash = window.location.hash;
    if (hash != "") {
        if(hash === '#ranking'){
            $('#overview').removeClass('active')
            $('.nav-tabs.dsh-inside-list a[href="' + hash + '"]').click();
            getRankingTabContent(hash);
        }
    }
    
$(document).ready(function () {
  getOverviewTabContent('#overview', 7); 
  
  $(document).on('click', '.top-bar-inner .nav-link', function (e) {
       if($(this).attr('id') == 'affilationPage' || $(this).attr('id') == 'insightsPage'){
            e.preventDefault();
            $('.top-bar-inner .nav-link').removeClass('active');
            $(this).addClass('active');

            if ($(this).attr('id') == 'affilationPage') {
                $('.nav-item.insights').hide();
                $('.nav-item.affilation').show();
                $('.nav-item.insights .nav-link').removeClass('active');
                $('.nav-item.affilation').first().find('.nav-link').trigger('click');
            } else {
                $('.nav-item.affilation').hide();
                $('.nav-item.insights').show();
                $('.nav-item.affilation .nav-link').removeClass('active');
                $('.nav-item.insights').first().find('.nav-link').trigger('click');
            }
       }
    });
                
    // dashboard open tabs
$(".nav-tabs.dsh-inside-list .nav-link").on("click", function() {
    // For mobile view
    $(this).closest('.nav.nav-tabs').toggleClass('opened');
    
    var tab_link = $(this).attr('href');
        switch(tab_link) {
            case '#overview':
                getOverviewTabContent(tab_link, 7);
              break;
            case '#engagement':
              getEngagementTabContent(tab_link, 7);
              break;
            case '#ranking':
                getRankingTabContent(tab_link);
              break;
            case '#interaction':
                getInteractionTabContent(tab_link, 7);
              break;
            case '#posts':
                getPostsTabContent(tab_link, 7);
              break;
            case '#reports':
                getReportsTabContent(tab_link, 30);
              break;
            case '#followers':
                getFollowersTabContent(tab_link, 30);
              break;
            case '#globalImpact':
                getGlobalImpactTabContent(tab_link);
              break;
          }
  
});
    
    
    //Load more global impact leader
    $(document).on('inview', '#global_loader_pagination', function (event, isInView) {
        if (isInView) {
            var nextPage = parseInt($('#global_pagenum').val()) + 1;
            var url = "{{url('dashboard/load-more-leader')}}";

            $.ajax({
                type: 'GET',
                url: url,
                data: {pagenum: nextPage, country_id: global_impact_filter},
                success: function (data) {
                    if (data.view_list != '') {
                    $('#global_loader_pagination').before(data.view_list)
                        $('#global_pagenum').val(nextPage);
                        
                    
                        $.each( data.expertise_country_lists, function( country_id, country_name ) {
                            if(!$('.dash-global-area-filter-list li').hasClass(country_id)){
                                $('.dash-global-area-filter-list').append('<li class="nav-item dash-global-area-filter-item '+ country_id +'">'+
                                                                                '<a class="nav-link dash-global-area-filter-link" data-filter="'+ country_id +'" data-toggle="tab" href="#" role="tab" aria-selected="true">'+ country_name +'</a>'+
                                                                           '</li>')
                            }
                        });

                    } else {
                        $('#global_loader_pagination').hide();
                    }
                }
            });
        }
    });
    
    //Load more affiltation
    $(document).on('inview', '#loader_pagination', function (event, isInView) {
        if (isInView) {
            var queryString = '?order='+order;
            var nextPage = parseInt($('#aff_pagenum').val()) + 1;

            var url = "{{route('dashboard.ajax_get_affiliate')}}" + queryString;

            $.ajax({
                type: 'GET',
                url: url,
                data: {pagenum: nextPage},
                success: function (data) {
                    if (data != '') {
                    $('#loader_pagination').before(data)
                        $('#aff_pagenum').val(nextPage);

                    } else {
                        $('#loader_pagination').hide();
                    }
                }
            });
        }
    });
    
  // load more followers   
     $(document).on('inview', '#why_followers_loader', function (event, isInView) {
         if(isInView){
              loadMoreFollow('followers'); 
         }
    });  
    
  // load more unfollowers   
     $(document).on('inview', '#why_unfollowers_loader', function (event, isInView) {
         if(isInView){
              loadMoreFollow('unfollowers'); 
         }
    });  

})        
 
  filterOrder('affil-filter-option', 'Newest')

$(".affil-filter-option .sort-option").on("click", function() {
$(this).parents(".sort-options").find(".sort-option").removeClass("selection");
$(this).addClass("selection");
$(this).parents(".sort-select").removeClass("opened");
$(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

order = $(this).data("value");
        
getAffiliateOrder(order)

});

//filter overview 
$(document).on("click", ".overview-filter .sort-option",  function() {
$(this).parents(".sort-options").find(".sort-option").removeClass("selection");
$(this).addClass("selection");
$(this).parents(".sort-select").removeClass("opened");
$(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

getOverviewTabContent('#overview', $(this).data("value"))

});

//filter view engagement
$(document).on("click", ".view-engagement .sort-option",  function() {
    selectEngagFilter($(this), 'view-engagement')
});

//filter comment engagement
$(document).on("click", ".comment-engagement .sort-option",  function() {
    selectEngagFilter($(this), 'comment-engagement')
});

//filter like engagement
$(document).on("click", ".likes-engagement .sort-option",  function() {
    selectEngagFilter($(this), 'likes-engagement')
});

//filter share engagement
$(document).on("click", ".share-engagement .sort-option",  function() {
    selectEngagFilter($(this), 'share-engagement')
});

//filter interaction soure 
$(document).on("click", ".interaction-country-filter .sort-option",  function() {
$(this).parents(".sort-options").find(".sort-option").removeClass("selection");
$(this).addClass("selection");
$(this).parents(".sort-select").removeClass("opened");
$(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

getInteractionFilter($(this).data("value"))

});

//filter posts summray 
$(document).on("click", ".posts-summary-filter .sort-option",  function() {
$(this).parents(".sort-options").find(".sort-option").removeClass("selection");
$(this).addClass("selection");
$(this).parents(".sort-select").removeClass("opened");
$(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

getPostsFilter('posts-summary-block', $(this).data("value"), 'summary')

});

//filter posts reports
$(document).on("click", ".posts-reports-filter .sort-option",  function() {
$(this).parents(".sort-options").find(".sort-option").removeClass("selection");
$(this).addClass("selection");
$(this).parents(".sort-select").removeClass("opened");
$(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

getPostsFilter('posts-reports-block', $(this).data("value"), 'reports')

});


//filter reports
$(document).on("click", ".reports-tab-filter .sort-option",  function() {
$(this).parents(".sort-options").find(".sort-option").removeClass("selection");
$(this).addClass("selection");
$(this).parents(".sort-select").removeClass("opened");
$(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

getReportsTabContent('#reports', $(this).data("value"))

});


//filter followers
$(document).on("click", ".followers-filter .sort-option",  function() {
$(this).parents(".sort-options").find(".sort-option").removeClass("selection");
$(this).addClass("selection");
$(this).parents(".sort-select").removeClass("opened");
$(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

followers_filter = $(this).data("value");
getFollowersFilter('followers', $(this).data("value"))

});


//filter unfollowers
$(document).on("click", ".unfollowers-filter .sort-option",  function() {
$(this).parents(".sort-options").find(".sort-option").removeClass("selection");
$(this).addClass("selection");
$(this).parents(".sort-select").removeClass("opened");
$(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

unfollowers_filter = $(this).data("value")
getFollowersFilter('unfollowers', $(this).data("value"))

});


function filterOrder(class_name, selected){
    $("." + class_name).each(function() {
var classes = $(this).attr("class"),
    id      = $(this).attr("id"),
    name    = $(this).attr("name"),
    select  = $(this).attr("data-type"),
    type    = $(this).attr("data-sorted_by");
  
var template =  '<div class="' + classes + '" data-type="'+ select +'">';
    template += '<span class="sort-select-trigger">' + selected + '</span>';
    template += '<div class="sort-options">';
    $(this).find("option").each(function() {
        template += '<span class="sort-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
    });
template += '</div></div>';

$(this).wrap('<div class="sort-select-wrapper"></div>');
$(this).hide();
$(this).after(template);
});

$(".sort-option:first-of-type").hover(function() {
$(this).parents(".sort-options").addClass("option-hover");
}, function() {
$(this).parents(".sort-options").removeClass("option-hover");
});
$(".sort-select-trigger").on("click", function() {
$('html').one('click',function() {
    $(".sel-select").removeClass("opened");
});
$(this).parents("."+class_name+".sort-select").toggleClass("opened");
event.stopPropagation();
});
}

// dashboard posts filter
$(document).on("click", ".nav-tabs.dash-posts-filter-list .nav-link", function() {
    var filter = $(this).attr('data-filter');
    $(this).closest('.post-dashboard-inner').find('.dash-posts-table-row').hide();
    $(this).closest('.post-dashboard-inner').find('.dash-posts-table-row.'+filter).show();
    if(filter == 'all') {
        $(this).closest('.post-dashboard-inner').find('.dash-posts-table-row').show();
    }
});

// dashboard interaction filter
$(document).on("click", ".nav-tabs.dash-interaction-filter-list .nav-link", function() {
    var filter = $(this).attr('data-filter');
    $(this).closest('.post-dashboard-inner').find('.dash-interaction-tab-table').hide();
    $(this).closest('.post-dashboard-inner').find('.dash-interaction-tab-table.'+filter).show();
});

// dashboard global leaderboard expert filter
$(document).on("click", ".dash-global-leaders-filter-list .nav-link", function() {
    var filter = $(this).attr('data-filter');
    $(this).closest('.post-dashboard-inner').find('.dash-global-table-row').hide();
    $(this).closest('.post-dashboard-inner').find('.dash-global-table-row.'+filter).show();
    if(filter == 'all') {
        $(this).closest('.post-dashboard-inner').find('.dash-global-table-row').show();
    }
});

// dashboard global leaderboard area filter
$(document).on("click", ".dash-global-area-filter-list .nav-link", function() {
    var filter = $(this).attr('data-filter');
    global_impact_filter = filter;
    
    $(this).closest('.post-dashboard-inner').find('.dash-global-table-row').hide();
    $(this).closest('.post-dashboard-inner').find('.dash-global-table-row.'+filter).show();
});

// Default chart font
Chart.defaults.global.defaultFontFamily = "circularairpro light, sans-serif";

// Overview Charts Ids
var overviewChartsIds = [
    'viewsChart',
    'commentsChart',
    'engagementsChart',
    'followersChart',
    'unfollowersChart',
    'textPostsChart',
    'imagePostsChart',
    'videoPostsChart',
    'linksChart',
    'tripPlansChart',
    'reportsOverviewChart',
    'followRateChart',
    'referralsChart',
    'revenueChart'
]

// Engagement Charts Ids
var engagementChartsIds = [
    'totalviewsasofChart',
    'totalcommentsasofChart',
    'totallikesasofChart',
    'totalsharesasofChart'
]

// Reports Charts Ids
var postsChartsIds = [
    'postsViewsChart'
]

// Reports Charts Ids
var reportsChartsIds = [
    'reportsChart'
]

// Followers Charts Ids
var followersChartsIds = [
    'totalfollowersasofChart',
    'totalunfollowersasofChart'
]

function overviewChartsOptions(id) {
    var label = JSON.parse($('#' + id).attr('data-overview-label'))
    var data_item = JSON.parse($('#' + id).attr('data-overview-item'))
    return  {
        type: 'line',
    data: {
        labels: label,
        datasets: [{
            data: data_item,
            backgroundColor: 'rgba(64,129,255,0.1)',
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                },
                display: false,
            }],
            xAxes: [{
                ticks: {
                    beginAtZero: true
                },
                display: false,
            }],
        },
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        elements: {
            line: {
                tension: 0,
                borderWidth: 2,
                borderColor: 'rgba(64,129,255,1)',
            },
            point: {
                backgroundColor: 'rgba(64,129,255,0)',
                borderColor: 'rgba(64,129,255,0)',
            }
        }
    }
}
}

 function engagementChartsOptions(id, type){
     if(type == 'followers'){
        var label = JSON.parse($('#' + id).attr('data-follow-label'));
        var data_daily = JSON.parse($('#' + id).attr('data-follow-daily'));
        var data_total = JSON.parse($('#' + id).attr('data-follow-total'));
        var placeholder_label = $('#' + id).attr('data-placeholder-title');
     }else{
        var label = JSON.parse($('#' + id).attr('data-engagement-label'));
        var data_daily = JSON.parse($('#' + id).attr('data-engagement-daily'));
        var data_total = JSON.parse($('#' + id).attr('data-engagement-total'));
        var placeholder_label = $('#' + id).attr('data-placeholder-title');
     }
   
    return {type: 'line',
    data: {
        labels: label,
        datasets: [{
                label: 'Daily ' + placeholder_label,
                fill: false,
                backgroundColor: 'rgba(64,129,255,1)',
                borderColor: 'rgba(64,129,255,1)',
                data: data_daily
            }, {
                label: 'Total ' + placeholder_label,
                backgroundColor: 'rgba(64,129,255,.1)',
                borderColor: 'rgba(643,19,235,0)',
                data: data_total,
                fill: true,
            }]
    },
    options: {
        legend: {
            display: true,
            position: 'top',
            align: 'end',
            labels: {
                boxWidth: 30,
                fontSize: 14,
                fontColor: '#000',
                padding: 30 
            }
        },
        elements: {
            line: {
                borderWidth: 2,
            },
            point: {
                radius: 0
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    padding: 10,
                    fontColor: '#999999',
                    fontSize: 14,
                    stepSize: 1500,
                },
                gridLines: {
                    color: 'rgba(64,129,255,.1)',
                    zeroLineColor: 'rgba(64,129,255,1)',
                    drawBorder: false,
                    tickMarkLength: 0
                }
            }],
            xAxes: [{
                    ticks: {
                    fontColor: '#999999',
                },
                gridLines: {
                    color: 'rgba(64,129,255,0)',
                    zeroLineColor: 'rgba(64,129,255,0)',
                    drawBorder: false
                }
            }],
        }
    }
}
 }

function postsChartsOptions(id){
    var label = JSON.parse($('#' + id).attr('data-post-label'))
    var data_item = JSON.parse($('#' + id).attr('data-post-value'))
    return {type: 'line',
        data: {
            labels: label,
            datasets: [{
                    label: 'Views',
                    fill: true,
                    backgroundColor: 'rgba(64,129,255,0.1)',
                    borderColor: 'rgba(64,129,255,1)',
                    data: data_item
                }]
        },
        options: {
            legend: {
                display: false
            },
            elements: {
                line: {
                    borderWidth: 2,
                },
                point: {
                    radius: 0
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        padding: 10,
                        fontColor: '#999999',
                        fontSize: 14,
                        stepSize: 300,
                    },
                    gridLines: {
                        color: 'rgba(64,129,255,.1)',
                        zeroLineColor: 'rgba(64,129,255,.1)',
                        drawBorder: false,
                        tickMarkLength: 0
                    }
                }],
                xAxes: [{
                        ticks: {
                        fontColor: '#999999',
                    },
                    gridLines: {
                        color: 'rgba(64,129,255,0)',
                        zeroLineColor: 'rgba(64,129,255,0)',
                        drawBorder: false
                    }
                }],
            }
        }
    }
}

function reportsChartsOptions (id) {
    var label = JSON.parse($('#' + id).attr('data-reports-label'));
    var report_view = JSON.parse($('#' + id).attr('data-reports-view'));
    var report_comment = JSON.parse($('#' + id).attr('data-reports-comment'));
    var report_share = JSON.parse($('#' + id).attr('data-reports-share'));
    var report_like = JSON.parse($('#' + id).attr('data-reports-like'));
    
    return {type: 'line',
    data: {
        labels: label,
        datasets: [{
                label: 'Comments',
                fill: false,
                backgroundColor: 'rgba(252,34,90,1)',
                borderColor: 'rgba(252,34,90,1)',
                data: report_comment
            }, {
                label: 'Shares',
                fill: false,
                backgroundColor: 'rgba(251,183,102,1)',
                borderColor: 'rgba(251,183,102,1)',
                data: report_share
            },  {
                label: 'Views',
                fill: false,
                backgroundColor: 'rgba(64,129,255,1)',
                borderColor: 'rgba(64,129,255,1)',
                data: report_view
            },  {
                label: 'Likes',
                fill: false,
                backgroundColor: 'rgba(50, 201, 132, 1)',
                borderColor: 'rgba(50, 201, 132, 1)',
                data: report_like
            }]
    },
    options: {
        legend: {
            display: true,
            position: 'top',
            align: 'end',
            labels: {
                boxWidth: 30,
                fontSize: 14,
                fontColor: '#000',
                padding: 30 
            }
        },
        elements: {
            line: {
                tension: 0,
                borderWidth: 2,
            },
            point: {
                radius: 0
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    padding: 10,
                    fontColor: '#999999',
                    fontSize: 14,
                    stepSize: 60,
                },
                gridLines: {
                    color: 'rgba(64,129,255,.1)',
                    zeroLineColor: 'rgba(64,129,255,.1)',
                    drawBorder: false,
                    tickMarkLength: 0
                }
            }],
            xAxes: [{
                    ticks: {
                    fontColor: '#999999',
                },
                gridLines: {
                    color: 'rgba(64,129,255,0)',
                    zeroLineColor: 'rgba(64,129,255,0)',
                    drawBorder: false
                }
            }],
        }
    }
    }
}

function chartContext(id) {
    return document.getElementById(id).getContext('2d')
}


function createOverviewCharts(id) {
    return  new Chart(chartContext(id), {
        ...overviewChartsOptions(id)
    })
}

function createEngagementCharts(id) {
    if ( engagementChartsIds[id] != undefined)
    {
        engagementChartsIds[id].destroy();
    }

    engagementChartsIds[id] =  new Chart(chartContext(id), {
        ...engagementChartsOptions(id, 'engag')
    })
    
    return engagementChartsIds[id];
}

// Create posts charts
function createPostsCharts(id) {
    return  new Chart(chartContext(id), {
        ...postsChartsOptions(id)
    })
}

// Create reports charts
function createReportsCharts(id) {
    return  new Chart(chartContext(id), {
        ...reportsChartsOptions(id)
    })
}

// Create followers charts
function createFollowersCharts(id) {
    if ( followersChartsIds[id] != undefined)
    {
        followersChartsIds[id].destroy();
    }

    followersChartsIds[id] =  new Chart(chartContext(id), {
        ...engagementChartsOptions(id, 'followers')
    })
    
    return followersChartsIds[id];
    
}

// World Data Map

function getWorldMapOptions(type){
    
    var worldDataMapSeries = type;

// Datamaps expect data in format:
var worldDataMapDataset = {};

var mapColorsOnlyValues = worldDataMapSeries.map(function(obj){ return obj[1]; });
var mapColorsMinValue = Math.min.apply(null, mapColorsOnlyValues),
    mapColorsMaxValue = Math.max.apply(null, mapColorsOnlyValues);

var paletteScale = d3.scale.linear()
        .domain([mapColorsMinValue,mapColorsMaxValue])
        .range(["#a9caff","#246de2"]); 

// fill dataset in appropriate format
worldDataMapSeries.forEach(function(item){ 
   
    var iso = item[0],
    value = item[1];
    worldDataMapDataset[iso] = { numberOfThings: value, fillColor: paletteScale(value) };
});

return {
    scope: 'world',
    responsive: true,
    aspectRatio: 0.7,
    projection: 'mercator', // big world map
    // countries don't listed in dataset will be painted with this color
    fills: { defaultFill: '#d7d7d7' },
    data: worldDataMapDataset,
    geographyConfig: {
        borderColor: '#ffffff',
        highlightBorderWidth: 1,
        // don't change color on mouse hover
        highlightFillColor: function(geo) {
            return geo['fillColor'] || '#d7d7d7';
        },
        // only change border
        highlightBorderColor: '#246de2',
        // show desired information in tooltip
        popupTemplate: function(geo, data) {
            // don't show tooltip if country don't present in dataset
            if (!data) { return ; }
            // tooltip content
            return ['<div class="world-datamap-hover-info">',
                '<strong>', geo.properties.name, '</strong>',
                '<br>People: <strong>', data.numberOfThings, '</strong>',
                '</div>'].join('');
        }
    }
}
}

// render maps
function interactionMap(){
var worldMap = JSON.parse($('#dashboardInteractionMap').attr('data-int_mapList'))
    var dashboardInteractionMap = new Datamap({
        element: document.getElementById('dashboardInteractionMap'),
        height: 455,
        width: 650,
        ...getWorldMapOptions(worldMap)
    });

    dashboardInteractionMap.resize();
    
    window.addEventListener('resize', function() {
        dashboardInteractionMap.resize();
    });
}


function overviewMap(){
    var overviewdMap = JSON.parse($('#dashboardOverviewMap').attr('data-overview-map'))
    
    var dashboardOverviewMap = new Datamap({
        element: document.getElementById('dashboardOverviewMap'),
        height: 160,
        width: 225,
        ...getWorldMapOptions(overviewdMap)
    });

    dashboardOverviewMap.resize();

    window.addEventListener('resize', function() {
        dashboardOverviewMap.resize();
    });
}

// tooltips
$('.dash-global-table-badges [data-toggle="tooltip"]').tooltip({
    placement: 'top',
    trigger: 'hover'
})

// badges info modal tabs
$(document).on('click', '.dash-badge-info-tab', function () {
    var current_tub = $(this).attr('data-tab');
    
    $('.dash-badge-info-wrapper .info-tab-content').each(function(){
        if($(this).hasClass(current_tub)){
            $(this).removeClass('d-none');
        }else{
                $(this).addClass('d-none');
        }
    })
    
    $(this).parent().find('.current').removeClass('current')
    $(this).addClass('current')
})


//Get overview tab
function getOverviewTabContent(id, filter){
    var url_content = id.replace("#", "");
    var url = "{{url('dashboard')}}/get-" + url_content;
     $(id).find('.' + url_content +'-block').html('')
     $('.' + url_content +'-loader').show()
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: filter},
        success: function (data) {
            if (data != '') {
                $(id).find('.' + url_content +'-block').html(data)
                $('.' + url_content +'-loader').hide()
                
                 for(i = 0; i < overviewChartsIds.length; i++) {
                    createOverviewCharts(overviewChartsIds[i])
                }
                
                $('.head-info [data-toggle="tooltip"]').tooltip({
                    trigger: 'hover',
                    placement: isTouchDevice ? 'left' : 'top',
                })
                
                filterOrder('overview-filter', filter + ' days')
                overviewMap();
            } else {
            }
        }
    });
    
}

//Engagement dig popup
$(document).on('click', '.dig-deeper-btn', function(){
    dig_type = $(this).attr('digtype')
   
    getEngagementDigInfo(dig_type, 7)
    $('#engagementDigPopup').modal('show')
   
})

//filter dig popup 
$(document).on("click", ".engagement-dig-filter .sort-option",  function() {
    $(this).parents(".sort-options").find(".sort-option").removeClass("selection");
    $(this).addClass("selection");
    $(this).parents(".sort-select").removeClass("opened");
    $(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

getEngagementDigInfo(dig_type, $(this).data("value"))

});

//Get engagtment dig info
function getEngagementDigInfo(dig_type, filter){
     $('.engagement-dig-block').html('')
    
    $.ajax({
        type: 'POST',
        url: "{{url('dashboard/get-engagement-diginfo')}}",
        data: {filter: filter, dig_type:dig_type},
        success: function (data) {
            if (data != '') {
               $('.engagement-dig-block').html(data)
               
                $('div.engagement-dig-filter').remove()
                 filterOrder('engagement-dig-filter', filter + ' days')
            } 
        }
    });
   
}


//Select engagement filter
function selectEngagFilter(_this, type){
    _this.parents(".sort-options").find(".sort-option").removeClass("selection");
    _this.addClass("selection");
    _this.parents(".sort-select").removeClass("opened");
    _this.parents(".sort-select").find(".sort-select-trigger").html(_this.html());

    getEngagementFilter(type, _this.data("value")) 
}


//Get engagtment tab
function getEngagementTabContent(id, filter){
    var url_content = id.replace("#", "");
    var url = "{{url('dashboard')}}/get-" + url_content;
     $(id).find('.' + url_content +'-block').html('')
     $('.' + url_content +'-loader').show()
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: filter},
        success: function (data) {
            if (data != '') {
                $(id).find('.' + url_content +'-block').html(data)
                $('.' + url_content +'-loader').hide()
                
                for(i = 0; i < engagementChartsIds.length; i++) {
                    createEngagementCharts(engagementChartsIds[i])
                }
                
                filterOrder('view-engagement', filter + ' days')
                filterOrder('comment-engagement', filter + ' days')
                filterOrder('likes-engagement', filter + ' days')
                filterOrder('share-engagement', filter + ' days')
            } 
        }
    });
   
}


//Get engagtment tab
function getRankingTabContent(id){
    var url_content = id.replace("#", "");
    var url = "{{url('dashboard')}}/get-" + url_content;
     $(id).find('.' + url_content +'-block').html('')
     $('.' + url_content +'-loader').show()
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: 7},
        success: function (data) {
            if (data != '') {
                $(id).find('.' + url_content +'-block').html(data)
                $('.' + url_content +'-loader').hide()
            } 
        }
    });
   
}


//Get engagtment filter by days
function getEngagementFilter(id, filter){
    var url = "{{url('dashboard/get-engagement-filter')}}";
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: filter, filter_type:id},
        success: function (data) {
            if (data != '') {
                var canvas =  $('.'+ id +'-canvas');
                canvas.attr('data-engagement-label', data.engagement_label);
                canvas.attr('data-engagement-daily', data.daily_value);
                canvas.attr('data-engagement-total', data.total_value);
                
                 createEngagementCharts(canvas.attr('id'))
            } 
        }
    });
   
}

//Get Interaction source tab
function getInteractionTabContent(id, filter){
    var url_content = id.replace("#", "");
    var url = "{{url('dashboard')}}/get-" + url_content;
     $(id).find('.' + url_content +'-block').html('')
     $('.' + url_content +'-loader').show()
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: filter},
        success: function (data) {
            if (data != '') {
                $(id).find('.' + url_content +'-block').html(data)
                $('.' + url_content +'-loader').hide()
                
                filterOrder('interaction-country-filter', filter + ' days')
                interactionMap();
            }
        }
    });
   
}

//Get engagtment filter by days
function getInteractionFilter(filter){
    var url = "{{url('dashboard/get-interaction-filter')}}";
    $('.interaction-countries-list').html('');
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: filter},
        success: function (data) {
            if (data != '') {
                $('.interaction-countries-list').html(data.countries_view);
                $('.interaction-cities-list').html(data.cities_view);
            } 
        }
    });
   
}

//Get Posts tab
function getPostsTabContent(id, filter){
    var url_content = id.replace("#", "");
    var url = "{{url('dashboard')}}/get-" + url_content;
     $(id).find('.' + url_content +'-block').html('')
     $('.' + url_content +'-loader').show()
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: filter, type:'all'},
        success: function (data) {
            if (data != '') {
                $(id).find('.' + url_content +'-block').html(data)
                $('.' + url_content +'-loader').hide()
                
                for(i = 0; i < postsChartsIds.length; i++) {
                    createPostsCharts(postsChartsIds[i])
                }
                
                filterOrder('posts-summary-filter', filter + ' days')
                filterOrder('posts-reports-filter', filter + ' days')
            }
        }
    });
   
}

//Get posts filter by days
function getPostsFilter(id, filter, type){
    var url = "{{url('dashboard/get-posts')}}";
     $('.' + id).html('')
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: filter, type:type},
        success: function (data) {
            if (data != '') {
               $('.' + id).html(data)
               if(type == 'summary'){
                   createPostsCharts('postsViewsChart')
               }
            } 
        }
    });
   
}

//Get Reports tab
function getReportsTabContent(id, filter){
    var url_content = id.replace("#", "");
    var url = "{{url('dashboard')}}/get-" + url_content;
     $(id).find('.' + url_content +'-block').html('')
     $('.' + url_content +'-loader').show()
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: filter},
        success: function (data) {
            if (data != '') {
                $(id).find('.' + url_content +'-block').html(data)
                $('.' + url_content +'-loader').hide()
                
               for(i = 0; i < reportsChartsIds.length; i++) {
                    createReportsCharts(reportsChartsIds[i])
                }
                
                filterOrder('reports-tab-filter', filter + ' days')
            }
        }
    });
   
}

//Get Followers/Unfollowers tab
function getFollowersTabContent(id, filter){
    var url_content = id.replace("#", "");
    var url = "{{url('dashboard')}}/get-" + url_content;
     $(id).find('.' + url_content +'-block').html('')
     $('.' + url_content +'-loader').show()
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: filter},
        success: function (data) {
            if (data != '') {
                $(id).find('.' + url_content +'-block').html(data)
                $('.' + url_content +'-loader').hide()
                
               for(i = 0; i < followersChartsIds.length; i++) {
                    createFollowersCharts(followersChartsIds[i])
                }
                
                filterOrder('followers-filter', filter + ' days')
                filterOrder('unfollowers-filter', filter + ' days')
            }
        }
    });
   
}


//Get followers filter by days
function getFollowersFilter(id, filter){
    var url = "{{url('dashboard/get-follower-filter')}}";
    $.ajax({
        type: 'GET',
        url: url,
        data: {filter: filter, filter_type:id},
        success: function (data) {
            if (data != '') {
                var canvas =  $('.'+ id +'-canvas');
                canvas.attr('data-follow-label', data.follow.follow_label);
                canvas.attr('data-follow-daily', data.follow.daily_values);
                canvas.attr('data-follow-total', data.follow.total_values);
                
                if(id == 'followers'){
                    $('.why-followers-block').html(data.view_list);
                    $('#followers_pagenum').val(1)
                    $('#why_followers_loader').removeClass('d-none')
                }else{
                     $('.why-unfollowers-block').html(data.view_list);
                     $('#unfollowers_pagenum').val(1)
                     $('#why_unfollowers_loader').removeClass('d-none')
                }
                
                 createFollowersCharts(canvas.attr('id'))
            } 
        }
    });
   
}

//Load more followers/unfollowers
function loadMoreFollow(type){
      if(type == 'followers'){
          var nextPage = parseInt($('#followers_pagenum').val())+1;
          var follow_filter = followers_filter;
      }else{
          var nextPage = parseInt($('#unfollowers_pagenum').val())+1;
          var follow_filter = unfollowers_filter
      }
    var url = '{{url("dashboard/load-more-followers")}}';

    $.ajax({
        type: 'GET',
        url: url,
        data: { pagenum: nextPage, type:type, filter:follow_filter},
        success: function(data){
            if(type == 'followers'){
                if(data != ''){
                   $('.why-followers-block').append(data);
                   $('#followers_pagenum').val(nextPage);
                } else {								 
                    $("#why_followers_loader").addClass('d-none');
                }
            }else{
                if(data != ''){	
                   $('.why-unfollowers-block').append(data);
                   $('#unfollowers_pagenum').val(nextPage);
                } else {								 
                    $("#why_unfollowers_loader").addClass('d-none');
                }
            }
        }
    });
}

function getGlobalImpactTabContent(id){
    var url_content = id.replace("#", "");
    var url = "{{url('dashboard')}}/get-" + url_content;
    $(id).find('.' + url_content +'-block').html('')
    $('.' + url_content +'-loader').show()
    $.ajax({
        type: 'GET',
        url: url,
        data: {type: id},
        success: function (data) {
            if (data != '') {
                $(id).find('.' + url_content +'-block').html(data)
                $('.' + url_content +'-loader').hide()
                
                $('.dash-global-table-badges [data-toggle="tooltip"]').tooltip({
                    placement: 'top',
                    trigger: 'hover'
                })

            }
        }
    });
}


</script>
