<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="./assets2/css/style.css">
    <title>Travooo - messages</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">
        <div class="dashboard-wrap">
            <div class="top-bar">
                <div class="container-fluid">
                    <div class="top-bar-inner">
                        <div class="ttl">Dashboard</div>
                        <ul class="nav nav-tabs bar-list" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('chat')}}" role="tab">Inbox</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#dashInsight" role="tab">Insights</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('settings')}}" role="tab">Settings</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="container-fluid">

                <!-- Tab panes -->
                <div class="tab-content dash-tab-content">
                    <div class="tab-pane " id="dashPage" role="tabpanel">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#whyFollowUnfollowPopup">
                            Why follow/unfollow popup
                        </button>
                    </div>
                    <div class="tab-pane" id="dashInbox" role="tabpanel">

                        <div class="message-wrap">
                            <div class="message-block hide-side">
                                <div class="message-conversation">
                                    <div class="m-top-block">
                                        <h4 class="block-ttl">Messages <span class="count">5</span></h4>
                                        <div class="icon-wrap" id="mobileSideToggler">
                                            <i class="trav-message-edit-icon"></i>
                                        </div>
                                    </div>
                                    <div class="m-content-block">
                                        <div class="conversation-inner">
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/50x50" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Stephen Bugno</a>
                                                    </div>
                                                    <div class="msg-txt">
                                                        <p>Hello! are you free next week?</p>
                                                    </div>
                                                </div>
                                                <div class="conv-time">
                                                    <span class="time">4:00pm</span>
                                                    <a href="#" class="setting-link">
                                                        <i class="trav-cog"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="conv-block current">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/50x50" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Edwin</a>
                                                    </div>
                                                    <div class="msg-txt">
                                                        <p>Let's go to this place <img class="smile-icon"
                                                                                       src="./assets2/image/smile-smiley.svg"
                                                                                       alt=""></p>
                                                    </div>
                                                </div>
                                                <div class="conv-time">
                                                    <span class="time">4:00pm</span>
                                                    <a href="#" class="setting-link">
                                                        <i class="trav-cog"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/50x50" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Miral</a>
                                                    </div>
                                                    <div class="msg-txt">
                                                        <p><b>Hi Anime, are you going to New York?</b></p>
                                                    </div>
                                                </div>
                                                <div class="conv-time">
                                                    <span class="time">8:00am</span>
                                                    <a href="#" class="setting-link">
                                                        <i class="trav-cog"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/50x50" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Steve Wong</a>
                                                    </div>
                                                    <div class="msg-txt">
                                                        <p><a href="#" class="reply-link"><i
                                                                        class="trav-reply-icon"></i></a> Ok thank you!
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="conv-time">
                                                    <span class="time">1 day ago</span>
                                                    <a href="#" class="setting-link">
                                                        <i class="trav-cog"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/50x50" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Teresa</a>
                                                    </div>
                                                    <div class="msg-txt">
                                                        <p><b>We need to go ASAP to the airport so we Lorem ipsum dolor
                                                                sit amet.</b></p>
                                                    </div>
                                                </div>
                                                <div class="conv-time">
                                                    <span class="time">1 day ago</span>
                                                    <a href="#" class="setting-link">
                                                        <i class="trav-cog"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="message-chat">
                                    <div class="m-top-block">
                                        <h4 class="block-ttl">Bugno</h4>
                                        <!-- <h4 class="block-ttl">To: <span class="friend-name">Friend name...</span></h4> -->
                                    </div>
                                    <div class="m-content-block">
                                        <div class="chat-group-wrap">
                                            <div class="chat-group">
                                                <div class="avatar-block">
                                                    <img src="http://placehold.it/20x20" alt="avatar">
                                                </div>
                                                <div class="text-block">
                                                    Hi, how are you going?
                                                </div>
                                            </div>
                                            <div class="chat-group right">
                                                <div class="avatar-block">
                                                    <img src="http://placehold.it/20x20" alt="avatar">
                                                </div>
                                                <div class="text-block">
                                                    I'm good and you?
                                                </div>
                                            </div>
                                            <div class="chat-group">
                                                <div class="avatar-block">
                                                    <img src="http://placehold.it/20x20" alt="avatar">
                                                </div>
                                                <div class="text-block">
                                                    Great! what should we talk about here?
                                                </div>
                                            </div>
                                            <div class="chat-group right">
                                                <div class="avatar-block">
                                                    <img src="http://placehold.it/20x20" alt="avatar">
                                                </div>
                                                <div class="text-block">
                                                    IDK, what do you think? <img class="smile"
                                                                                 src="./assets2/image/smile-eek.svg"
                                                                                 alt="smile">
                                                </div>
                                            </div>
                                            <div class="chat-group right">
                                                <div class="avatar-block">
                                                    <img src="http://placehold.it/20x20" alt="avatar">
                                                </div>
                                                <div class="text-block">
                                                    I'll be happy to know what is in your mind
                                                </div>
                                            </div>
                                            <div class="chat-group">
                                                <div class="avatar-block">
                                                    <img src="http://placehold.it/20x20" alt="avatar">
                                                </div>
                                                <div class="text-block">
                                                    I want to go in a trip next week
                                                </div>
                                            </div>
                                            <div class="add-input-block">
                                                <div class="input-wrap">
                                                    <input type="text">
                                                </div>
                                                <div class="add-input-action">
                                                    <div class="smile-link-wrap">
                                                        <a href="#" class="attachment">
                                                            <i class="trav-attachment"></i>
                                                            <span>Attachment</span>
                                                        </a>
                                                        <div class="mobile-icon-wrap">
                                                            <a href="#" class="mobile-icon-target" id="mobileIconShow">
                                                                <i class="trav-smile-toggler"></i>
                                                            </a>
                                                            <ul class="smile-list">
                                                                <li>
                                                                    <a href="#">
                                                                        <img class="smile"
                                                                             src="./assets2/image/smile-smiley.svg"
                                                                             alt="smile">
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <img class="smile"
                                                                             src="./assets2/image/smile-eek.svg"
                                                                             alt="smile">
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <img class="smile"
                                                                             src="./assets2/image/smile-eye-heart.svg"
                                                                             alt="smile">
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <img class="smile"
                                                                             src="./assets2/image/smile-happy-cry.svg"
                                                                             alt="smile">
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <img class="smile"
                                                                             src="./assets2/image/smile-angry.svg"
                                                                             alt="smile">
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <img class="smile"
                                                                             src="./assets2/image/smile-finger-up.svg"
                                                                             alt="smile">
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <img class="smile"
                                                                             src="./assets2/image/smile-finger-down.svg"
                                                                             alt="smile">
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-light-primary btn-bordered">
                                                        Send
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane first-layout" id="dashNotification" role="tabpanel">
                        <div class="side-toggler">
                            <i class="trav-cog"></i>
                        </div>
                        <ul class="nav nav-tabs inside-list" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#dashAll"
                                   role="tab">@lang('other.all')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#dashComments"
                                   role="tab">@lang('comment.comments')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#dashReactions"
                                   role="tab">@choice('other.reaction', 6, ['count' => 6])</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#dashShares" role="tab">Shares</a>
                            </li>
                        </ul>
                        <div class="tab-content dash-inside-content">
                            <div class="tab-pane second-layout active" id="dashAll" role="tabpanel">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">All Notifications</h4>
                                        <a href="#" class="head-link">Mark All as Read</a>
                                    </div>
                                    <div class="notification-wrap">
                                        <div class="conversation-inner bordered">
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Stephen Bugno</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Steve</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>4 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Sarah New</a>
                                                        @lang('dashboard.reacted_on_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                        @lang('dashboard.of_you')
                                                    </div>
                                                    <div class="time">
                                                        <p>12 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">James M. Province</a>
                                                        @lang('dashboard.is_now_following')
                                                        <a href="#" class="inner-link">Tokyo Tv Tower</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>1 day ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Veronica</a>,
                                                        <a href="#" class="inner-link">Bagwell</a>...
                                                        <a href="#"
                                                           class="inner-link">@lang('other.and_count_more', ['count' => 5])</a>
                                                        @lang('dashboard.commented_on_your')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>2 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Stephen Bugno</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>2 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Warner</a>
                                                        @lang('dashboard.reacted_on_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                        @lang('dashboard.of_you')
                                                    </div>
                                                    <div class="time">
                                                        <p>4 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Zelma Brady</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Dona Ramsey</a>
                                                        @lang('dashboard.is_now_following')
                                                        <a href="#" class="inner-link">New York City</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="notification-loader">
                        <span class="spinner">
                          <i class="trav-loading-icon"></i>
                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane second-layout" id="dashComments" role="tabpanel">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">@lang('comment.comments')</h4>
                                        <a href="#" class="head-link">Mark All as Read</a>
                                    </div>
                                    <div class="notification-wrap">
                                        <div class="conversation-inner bordered">
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Stephen Bugno</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Steve</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>4 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Sarah New</a>
                                                        @lang('dashboard.reacted_on_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                        @lang('dashboard.of_you')
                                                    </div>
                                                    <div class="time">
                                                        <p>12 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">James M. Province</a>
                                                        @lang('dashboard.is_now_following')
                                                        <a href="#" class="inner-link">Tokyo Tv Tower</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>1 day ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Veronica</a>,
                                                        <a href="#" class="inner-link">Bagwell</a>...
                                                        <a href="#"
                                                           class="inner-link">@lang('other.and_count_more', ['count' => 5])</a>
                                                        @lang('dashboard.commented_on_your')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>2 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Stephen Bugno</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>2 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Warner</a>
                                                        @lang('dashboard.reacted_on_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                        @lang('dashboard.of_you')
                                                    </div>
                                                    <div class="time">
                                                        <p>4 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Zelma Brady</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Dona Ramsey</a>
                                                        @lang('dashboard.is_now_following')
                                                        <a href="#" class="inner-link">New York City</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="notification-loader">
                        <span class="spinner">
                          <i class="trav-loading-icon"></i>
                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane second-layout" id="dashReactions" role="tabpanel">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">@choice('other.reaction', 6, ['count' => 6])</h4>
                                        <a href="#" class="head-link">Mark All as Read</a>
                                    </div>
                                    <div class="notification-wrap">
                                        <div class="conversation-inner bordered">
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Stephen Bugno</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Steve</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>4 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Sarah New</a>
                                                        @lang('dashboard.reacted_on_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                        @lang('dashboard.of_you')
                                                    </div>
                                                    <div class="time">
                                                        <p>12 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">James M. Province</a>
                                                        @lang('dashboard.is_now_following')
                                                        <a href="#" class="inner-link">Tokyo Tv Tower</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>1 day ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Veronica</a>,
                                                        <a href="#" class="inner-link">Bagwell</a>...
                                                        <a href="#"
                                                           class="inner-link">@lang('other.and_count_more', ['count' => 5])</a>
                                                        @lang('dashboard.commented_on_your')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>2 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Stephen Bugno</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>2 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Warner</a>
                                                        @lang('dashboard.reacted_on_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                        @lang('dashboard.of_you')
                                                    </div>
                                                    <div class="time">
                                                        <p>4 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Zelma Brady</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Dona Ramsey</a>
                                                        @lang('dashboard.is_now_following')
                                                        <a href="#" class="inner-link">New York City</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="notification-loader">
                        <span class="spinner">
                          <i class="trav-loading-icon"></i>
                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane second-layout" id="dashShares" role="tabpanel">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Shares</h4>
                                        <a href="#" class="head-link">Mark All as Read</a>
                                    </div>
                                    <div class="notification-wrap">
                                        <div class="conversation-inner bordered">
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Stephen Bugno</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Steve</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>4 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Sarah New</a>
                                                        @lang('dashboard.reacted_on_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                        @lang('dashboard.of_you')
                                                    </div>
                                                    <div class="time">
                                                        <p>12 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">James M. Province</a>
                                                        @lang('dashboard.is_now_following')
                                                        <a href="#" class="inner-link">Tokyo Tv Tower</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>1 day ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Veronica</a>,
                                                        <a href="#" class="inner-link">Bagwell</a>...
                                                        <a href="#"
                                                           class="inner-link">@lang('other.and_count_more', ['count' => 5])</a>
                                                        @lang('dashboard.commented_on_your')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>2 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Stephen Bugno</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>2 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Warner</a>
                                                        @lang('dashboard.reacted_on_a')
                                                        <a href="#" class="inner-link">@lang('place.photo')</a>
                                                        @lang('dashboard.of_you')
                                                    </div>
                                                    <div class="time">
                                                        <p>4 days ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Zelma Brady</a>
                                                        @lang('profile.shared_a')
                                                        <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="conv-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/30x30" alt="image">
                                                </div>
                                                <div class="conv-txt">
                                                    <div class="name">
                                                        <a href="#" class="inner-link">Dona Ramsey</a>
                                                        @lang('dashboard.is_now_following')
                                                        <a href="#" class="inner-link">New York City</a>
                                                    </div>
                                                    <div class="time">
                                                        <p>3 hours ago</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="notification-loader">
                        <span class="spinner">
                          <i class="trav-loading-icon"></i>
                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane first-layout active" id="dashInsight" role="tabpanel">
                        <div class="side-toggler">
                            <i class="trav-cog"></i>
                        </div>
                        <ul class="nav nav-tabs inside-list" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#dashOverview"
                                   role="tab">@lang('trip.overview')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#dashEngagement" role="tab">Engagement</a>
                            </li>
                            <!--
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#dashInteractionSource" role="tab">Interaction Source</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#dashPosts" role="tab">Posts</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#dashReports" role="tab">Reports</a>
                            </li>
                            -->
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#dashFollowersUnfollowers" role="tab">Followers
                                    / UnFollowers</a>
                            </li>
                        </ul>
                        <div class="tab-content dash-inside-content">
                            <div class="tab-pane second-layout active" id="dashOverview" role="tabpanel">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Summary</h4>
                                        <div class="sort-by-select">
                                            <label>Last</label>
                                            <div class="sort-select-wrap">
                                                <select class="form-control" id="sortBy">
                                                    <option>@lang('time.count_days', ['count' => 30])</option>
                                                    <option>@lang('time.count_days', ['count' => 15])</option>
                                                    <option selected="selected">@lang('time.count_days', ['count' => 7])</option>
                                                </select>
                                                <i class="trav-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-overview-inner">
                                        <div class="overview-block">
                                            <div class="over-head">
                                                <div class="head-label">
                                                    <div class="ttl">Views</div>
                                                    <div class="period">{{date("M d", strtotime("-7 day", time()))}}
                                                        - {{date("M d", time())}}</div>
                                                </div>
                                                <div class="head-info">
                                                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip"
                                                       title="A short description to explain the section if needed"></i>
                                                </div>
                                            </div>
                                            <div class="views-block">
                                                <div class="view-top">
                                                    <div class="view-count">{{$my_posts_views_total}}</div>
                                                    <div class="view-subttl">Total views</div>
                                                </div>
                                                <div class="graph-wrap">
                                                    <canvas id="viewsChart" width="230" height="150"></canvas>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="overview-block">
                                            <div class="over-head">
                                                <div class="head-label">
                                                    <div class="ttl">Top engaged countries</div>
                                                    <div class="period">{{date("M d", strtotime("-7 day", time()))}}
                                                        - {{date("M d", time())}}</div>
                                                </div>
                                                <div class="head-info">
                                                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip"
                                                       title="A short description to explain the section if needed"></i>
                                                </div>
                                            </div>
                                            <ul class="over-list">
                                                @foreach($my_posts_views_countries AS $vc)
                                                    <li>
                                                        <span class="name">{{$vc->nationality}}</span>
                                                        <span class="count">{{$vc->count}}</span>
                                                    </li>
                                                @endforeach

                                            </ul>
                                        </div>
                                        <div class="overview-block">
                                            <div class="over-head">
                                                <div class="head-label">
                                                    <div class="ttl">Popularity map</div>
                                                    <div class="period">{{date("M d", strtotime("-7 day", time()))}}
                                                        - {{date("M d", time())}}</div>
                                                </div>
                                                <div class="head-info">
                                                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip"
                                                       title="A short description to explain the section if needed"></i>
                                                </div>
                                            </div>
                                            <div class="map-inner">
                                                <img src="{{asset('assets2/image/map00pop.jpg')}}" alt="map" class="map"
                                                     style="width:260px;">
                                            </div>
                                        </div>
                                        <div class="overview-block">
                                            <div class="over-head">
                                                <div class="head-label">
                                                    <div class="ttl">Followers</div>
                                                    <div class="period">{{date("M d", strtotime("-7 day", time()))}}
                                                        - {{date("M d", time())}}</div>
                                                </div>
                                                <div class="head-info">
                                                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip"
                                                       title="A short description to explain the section if needed"></i>
                                                </div>
                                            </div>
                                            <div class="views-block">
                                                <div class="view-top">
                                                    <div class="view-count">{{$my_posts_views_total*1.5/3}}</div>
                                                    <div class="view-subttl">Total followers</div>
                                                </div>
                                                <div class="graph-wrap">
                                                    <canvas id="followersChart" width="230" height="150"></canvas>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="overview-block">
                                            <div class="over-head">
                                                <div class="head-label">
                                                    <div class="ttl">@lang('buttons.general.follow') rate</div>
                                                    <div class="period">{{date("M d", strtotime("-7 day", time()))}}
                                                        - {{date("M d", time())}}</div>
                                                </div>
                                                <div class="head-info">
                                                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip"
                                                       title="A short description to explain the section if needed"></i>
                                                </div>
                                            </div>
                                            <div class="views-block">
                                                <div class="view-top">
                                                    <div class="view-count"></div>
                                                    <div class="view-subttl">More data is required to build this stats
                                                    </div>
                                                </div>
                                                <div class="graph-wrap">
                                                    <canvas id="viewsChart" width="230" height="150"></canvas>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="overview-block">
                                            <div class="over-head">
                                                <div class="head-label">
                                                    <div class="ttl">Unfollowers</div>
                                                    <div class="period">{{date("M d", strtotime("-7 day", time()))}}
                                                        - {{date("M d", time())}}</div>
                                                </div>
                                                <div class="head-info">
                                                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip"
                                                       title="A short description to explain the section if needed"></i>
                                                </div>
                                            </div>
                                            <div class="views-block">
                                                <div class="view-top">
                                                    <div class="view-count">{{round($my_posts_views_total*1.5/10)}}</div>
                                                    <div class="view-subttl">Total unfollowers</div>
                                                </div>
                                                <div class="graph-wrap">
                                                    <canvas id="unfollowersChart" width="230" height="150"></canvas>


                                                </div>
                                            </div>
                                        </div>

                                        <div class="overview-block">
                                            <div class="over-head">
                                                <div class="head-label">
                                                    <div class="ttl">Referrals achieved</div>
                                                    <div class="period">{{date("M d", strtotime("-7 day", time()))}}
                                                        - {{date("M d", time())}}</div>
                                                </div>
                                                <div class="head-info">
                                                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip"
                                                       title="A short description to explain the section if needed"></i>
                                                </div>
                                            </div>
                                            <div class="views-block">
                                                <div class="view-top">
                                                    <div class="view-count">{{round($my_posts_views_total*2)}}</div>
                                                    <div class="view-subttl">Total referrals</div>
                                                </div>
                                                <div class="graph-wrap">
                                                    <canvas id="unfollowersChart" width="230" height="150"></canvas>


                                                </div>
                                            </div>
                                        </div>

                                        <div class="overview-block">
                                            <div class="over-head">
                                                <div class="head-label">
                                                    <div class="ttl">Revenue</div>
                                                    <div class="period">{{date("M d", strtotime("-7 day", time()))}}
                                                        - {{date("M d", time())}}</div>
                                                </div>
                                                <div class="head-info">
                                                    <i class="fa fa-info-circle" rel="tooltip" data-toggle="tooltip"
                                                       title="A short description to explain the section if needed"></i>
                                                </div>
                                            </div>
                                            <div class="views-block">
                                                <div class="view-top">
                                                    <div class="view-count">{{money_format('%i', ($my_posts_views_total*2*15)/100)}}</div>
                                                    <div class="view-subttl">USD</div>
                                                </div>
                                                <div class="graph-wrap">
                                                    <canvas id="unfollowersChart" width="230" height="150"></canvas>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane second-layout" id="dashEngagement" role="tabpanel">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Total Views as of Today:
                                            <span>{{$my_posts_views_total*6}}</span></h4>
                                        <div class="sort-by-select">
                                            <label>Last</label>
                                            <div class="sort-select-wrap">
                                                <select class="form-control" id="sortBy">
                                                    <option>@lang('time.count_days', ['count' => 30])</option>
                                                    <option>@lang('time.count_days', ['count' => 15])</option>
                                                    <option>@lang('time.count_days', ['count' => 7])</option>
                                                </select>
                                                <i class="trav-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-sidebar-inner">
                                        <div class="dash-main">
                                            <canvas id="totalviewsasofChart" width="610" height="300"></canvas>
                                        </div>
                                        <div class="dash-sidebar">
                                            <h4 class="side-ttl">Benchmark</h4>
                                            <div class="side-txt">
                                                <p>Compare your average preformance over time</p>
                                            </div>
                                            <div class="side-txt border-disabled">
                                                <p><b>Learn what sources lead to your views?</b></p>
                                            </div>
                                            <a href="#" class="btn btn-light-grey btn-bordered">Dig deeper</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Total @lang('comment.comments') as of Today: <span>42</span>
                                        </h4>
                                        <div class="sort-by-select">
                                            <label>Last</label>
                                            <div class="sort-select-wrap">
                                                <select class="form-control" id="sortBy">
                                                    <option>@lang('time.count_days', ['count' => 30])</option>
                                                    <option>@lang('time.count_days', ['count' => 15])</option>
                                                    <option>@lang('time.count_days', ['count' => 7])</option>
                                                </select>
                                                <i class="trav-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-sidebar-inner">
                                        <div class="dash-main">
                                            <canvas id="totalcommentsasofChart" width="610" height="300"></canvas>
                                        </div>
                                        <div class="dash-sidebar">
                                            <h4 class="side-ttl">Benchmark</h4>
                                            <div class="side-txt">
                                                <p>Compare your average preformance over time</p>
                                            </div>
                                            <div class="side-txt border-disabled">
                                                <p><b>Learn what sources lead to your comments?</b></p>
                                            </div>
                                            <a href="#" class="btn btn-light-grey btn-bordered">Dig deeper</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Total Likes as of Today: <span>176</span></h4>
                                        <div class="sort-by-select">
                                            <label>Last</label>
                                            <div class="sort-select-wrap">
                                                <select class="form-control" id="sortBy">
                                                    <option>@lang('time.count_days', ['count' => 30])</option>
                                                    <option>@lang('time.count_days', ['count' => 15])</option>
                                                    <option>@lang('time.count_days', ['count' => 7])</option>
                                                </select>
                                                <i class="trav-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-sidebar-inner">
                                        <div class="dash-main">
                                            <canvas id="totallikesasofChart" width="610" height="300"></canvas>
                                        </div>
                                        <div class="dash-sidebar">
                                            <h4 class="side-ttl">Benchmark</h4>
                                            <div class="side-txt">
                                                <p>Compare your average preformance over time</p>
                                            </div>
                                            <div class="side-txt border-disabled">
                                                <p><b>Learn what sources lead to your likes?</b></p>
                                            </div>
                                            <a href="#" class="btn btn-light-grey btn-bordered">Dig deeper</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Total Shares as of Today: <span>66</span></h4>
                                        <div class="sort-by-select">
                                            <label>Last</label>
                                            <div class="sort-select-wrap">
                                                <select class="form-control" id="sortBy">
                                                    <option>@lang('time.count_days', ['count' => 30])</option>
                                                    <option>@lang('time.count_days', ['count' => 15])</option>
                                                    <option>@lang('time.count_days', ['count' => 7])</option>
                                                </select>
                                                <i class="trav-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-sidebar-inner">
                                        <div class="dash-main">
                                            <canvas id="totalsharesasofChart" width="610" height="300"></canvas>
                                        </div>
                                        <div class="dash-sidebar">
                                            <h4 class="side-ttl">Benchmark</h4>
                                            <div class="side-txt">
                                                <p>Compare your average preformance over time</p>
                                            </div>
                                            <div class="side-txt border-disabled">
                                                <p><b>Learn what sources lead to your shares?</b></p>
                                            </div>
                                            <a href="#" class="btn btn-light-grey btn-bordered">Dig deeper</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane second-layout" id="dashInteractionSource" role="tabpanel">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Demographics</h4>
                                    </div>
                                    <div class="dash-inner">
                                        <p>Aggregated demographic data about the people who engaged with your content
                                            based on the age and gender information they provide in their user
                                            profiles.</p>
                                        <img src="http://placehold.it/800x250?text=Demographics graph" alt="graph">
                                    </div>
                                </div>
                                <div class="post-block post-dashboard-inner post-map-block">
                                    <div class="block-head">
                                        <h4 class="ttl">Region</h4>
                                    </div>
                                    <div class="dash-inner">
                                        <div class="post-map-inner">
                                            <img src="http://placehold.it/800x500/e6e6e6?text=Region map" alt="map">
                                            <div class="post-map-engagement-block">
                                                <div class="block-ttl">Engagement Key</div>
                                                <div class="post-engagement-inner-block">
                                                    <span>More</span>
                                                    <div class="eng-progress"></div>
                                                    <span>Less</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-head internal-block with-tab-list">
                                            <h4 class="ttl">Top engaged countries&nbsp;(&nbsp;
                                                <div class="sort-by-select">
                                                    <div class="sort-select-wrap">
                                                        <select class="form-control" id="sortBy">
                                                            <option>@lang('time.count_days', ['count' => 30])</option>
                                                            <option>@lang('time.count_days', ['count' => 15])</option>
                                                            <option>Item2</option>
                                                        </select>
                                                        <i class="trav-caret-down"></i>
                                                    </div>
                                                </div>
                                                &nbsp;)
                                            </h4>
                                            <ul class="nav nav-tabs bar-list" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#" role="tab">Countries</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#" role="tab">Cities</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table tbl-dashboard tbl-eng-region">
                                                <tr>
                                                    <th>Countries</th>
                                                    <th class="eng-cell">Engagements</th>
                                                    <th class="eng-cell">Likes</th>
                                                    <th class="eng-cell">@lang('comment.comments')</th>
                                                    <th class="eng-cell">Shares</th>
                                                    <th class="eng-cell">@choice('other.reaction', 6, ['count' => 6])</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img class="flag" src="http://placehold.it/24x18" alt="flag">
                                                        &nbsp;
                                                        Morocco
                                                    </td>
                                                    <td>
                                                        7,440
                                                    </td>
                                                    <td>
                                                        7,440
                                                    </td>
                                                    <td>
                                                        7,440
                                                    </td>
                                                    <td>
                                                        7,440
                                                    </td>
                                                    <td>
                                                        7,440
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img class="flag" src="http://placehold.it/24x18" alt="flag">
                                                        &nbsp;
                                                        Spain
                                                    </td>
                                                    <td>
                                                        447
                                                    </td>
                                                    <td>
                                                        447
                                                    </td>
                                                    <td>
                                                        447
                                                    </td>
                                                    <td>
                                                        447
                                                    </td>
                                                    <td>
                                                        447
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img class="flag" src="http://placehold.it/24x18" alt="flag">
                                                        &nbsp;
                                                        Algeria
                                                    </td>
                                                    <td>
                                                        396
                                                    </td>
                                                    <td>
                                                        396
                                                    </td>
                                                    <td>
                                                        396
                                                    </td>
                                                    <td>
                                                        396
                                                    </td>
                                                    <td>
                                                        396
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img class="flag" src="http://placehold.it/24x18" alt="flag">
                                                        &nbsp;
                                                        France
                                                    </td>
                                                    <td>
                                                        360
                                                    </td>
                                                    <td>
                                                        360
                                                    </td>
                                                    <td>
                                                        360
                                                    </td>
                                                    <td>
                                                        360
                                                    </td>
                                                    <td>
                                                        360
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane second-layout" id="dashPosts" role="tabpanel">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Summary</h4>
                                        <div class="sort-by-select">
                                            <label>Last</label>
                                            <div class="sort-select-wrap">
                                                <select class="form-control" id="sortBy">
                                                    <option>@lang('time.count_days', ['count' => 30])</option>
                                                    <option>@lang('time.count_days', ['count' => 15])</option>
                                                    <option selected="selected">@lang('time.count_days', ['count' => 7])</option>
                                                </select>
                                                <i class="trav-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="summary-post-info">
                                        <div class="info-block">
                                            <div class="info-label">All posts</div>
                                            <div class="info-count">1360</div>
                                        </div>
                                        <div class="info-block">
                                            <div class="info-label">Text</div>
                                            <div class="info-count">125</div>
                                        </div>
                                        <div class="info-block">
                                            <div class="info-label">Image</div>
                                            <div class="info-count">254</div>
                                        </div>
                                        <div class="info-block">
                                            <div class="info-label">Video</div>
                                            <div class="info-count">52</div>
                                        </div>
                                        <div class="info-block">
                                            <div class="info-label">@lang('trip.trip_plan')</div>
                                            <div class="info-count">509</div>
                                        </div>
                                        <div class="info-block">
                                            <div class="info-label">Reports</div>
                                            <div class="info-count">109</div>
                                        </div>
                                        <div class="info-block">
                                            <div class="info-label">Links</div>
                                            <div class="info-count">366</div>
                                        </div>
                                    </div>
                                    <div class="dash-inner">
                                        <img src="http://placehold.it/800x240?text=Posts summary graph" alt="graph">
                                    </div>
                                </div>
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head with-tab-list">
                                        <h4 class="ttl">All Posts Published</h4>
                                        <ul class="nav nav-tabs bar-list" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#"
                                                   role="tab">@lang('other.all')</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#" role="tab">Text</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#" role="tab">Image</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#" role="tab">Video</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#"
                                                   role="tab">@lang('trip.trip_plan')</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#" role="tab">Reports</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#" role="tab">Links</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="dash-info-inner">
                                        <ul class="info-list">
                                            <li>
                                                <span class="color-block primary"></span>
                                                <span class="txt">Views</span>
                                            </li>
                                            <li>
                                                <span class="color-block light-violet"></span>
                                                <span class="txt">Post clicks</span>
                                            </li>
                                            <li>
                                                <span class="color-block orange"></span>
                                                <span class="txt">Reactions, comments & shares</span>
                                            </li>
                                        </ul>
                                        <div class="sort-by-select">
                                            <label>Last</label>
                                            <div class="sort-select-wrap">
                                                <select class="form-control" id="sortBy">
                                                    <option>@lang('time.count_days', ['count' => 30])</option>
                                                    <option>@lang('time.count_days', ['count' => 15])</option>
                                                    <option>Item2</option>
                                                </select>
                                                <i class="trav-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-inner">
                                        <div class="table-responsive">
                                            <table class="table tbl-dashboard">
                                                <tr>
                                                    <th class="publ">Published</th>
                                                    <th class="post">Post</th>
                                                    <th class="prog-cell">Views</th>
                                                    <th class="prog-cell">Engagement</th>
                                                </tr>
                                                <tr>
                                                    <td>
                              <span class="dt">01/10/2018<br>
                              5:41pm</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="post-tbl-link">
                                                            <img src="http://placehold.it/40x43" alt="image">
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Quis, id!
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">632</div>
                                                            <div class="progress">
                                                                <div class="progress-bar primary" role="progressbar"
                                                                     aria-valuenow="63.2" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 63.2%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">32</div>
                                                            <div class="progress">
                                                                <div class="progress-bar light-violet"
                                                                     role="progressbar" aria-valuenow="32"
                                                                     aria-valuemin="0" aria-valuemax="100"
                                                                     style="width: 32%;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">8</div>
                                                            <div class="progress">
                                                                <div class="progress-bar orange" role="progressbar"
                                                                     aria-valuenow="40" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 40%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                              <span class="dt">01/10/2018<br>
                              5:41pm</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="post-tbl-link">
                                                            <img src="http://placehold.it/40x43" alt="image">
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Quis, id!
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">126</div>
                                                            <div class="progress">
                                                                <div class="progress-bar primary" role="progressbar"
                                                                     aria-valuenow="12.6" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 12.6%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">42</div>
                                                            <div class="progress">
                                                                <div class="progress-bar light-violet"
                                                                     role="progressbar" aria-valuenow="42"
                                                                     aria-valuemin="0" aria-valuemax="100"
                                                                     style="width: 42%;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">9</div>
                                                            <div class="progress">
                                                                <div class="progress-bar orange" role="progressbar"
                                                                     aria-valuenow="45" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 45%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                              <span class="dt">01/10/2018<br>
                              5:41pm</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="post-tbl-link">
                                                            <img src="http://placehold.it/40x43" alt="image">
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Quis, id!
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">489</div>
                                                            <div class="progress">
                                                                <div class="progress-bar primary" role="progressbar"
                                                                     aria-valuenow="48.9" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 48.9%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">87</div>
                                                            <div class="progress">
                                                                <div class="progress-bar light-violet"
                                                                     role="progressbar" aria-valuenow="87"
                                                                     aria-valuemin="0" aria-valuemax="100"
                                                                     style="width: 87%;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">16</div>
                                                            <div class="progress">
                                                                <div class="progress-bar orange" role="progressbar"
                                                                     aria-valuenow="80" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 80%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                              <span class="dt">01/10/2018<br>
                              5:41pm</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="post-tbl-link">
                                                            <img src="http://placehold.it/40x43" alt="image">
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Quis, id!
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">87</div>
                                                            <div class="progress">
                                                                <div class="progress-bar primary" role="progressbar"
                                                                     aria-valuenow="8.7" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 8.7%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">46</div>
                                                            <div class="progress">
                                                                <div class="progress-bar light-violet"
                                                                     role="progressbar" aria-valuenow="46"
                                                                     aria-valuemin="0" aria-valuemax="100"
                                                                     style="width: 46%;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">12</div>
                                                            <div class="progress">
                                                                <div class="progress-bar orange" role="progressbar"
                                                                     aria-valuenow="62" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 62%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane second-layout" id="dashReports" role="tabpanel">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Summary</h4>
                                    </div>
                                    <div class="dash-inner">
                                        <img src="http://placehold.it/800x400?text=Reports summary graph" alt="graph">
                                    </div>
                                </div>
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">All Reports</h4>
                                    </div>
                                    <div class="dash-info-inner">
                                        <ul class="info-list">
                                            <li>
                                                <span class="color-block primary"></span>
                                                <span class="txt">Views</span>
                                            </li>
                                            <li>
                                                <span class="color-block light-violet"></span>
                                                <span class="txt">Post clicks</span>
                                            </li>
                                            <li>
                                                <span class="color-block orange"></span>
                                                <span class="txt">Reactions, comments & shares</span>
                                            </li>
                                        </ul>
                                        <div class="sort-by-select">
                                            <label>Last</label>
                                            <div class="sort-select-wrap">
                                                <select class="form-control" id="sortBy">
                                                    <option>@lang('time.count_days', ['count' => 30])</option>
                                                    <option>@lang('time.count_days', ['count' => 15])</option>
                                                    <option>Item2</option>
                                                </select>
                                                <i class="trav-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-inner">
                                        <div class="table-responsive">
                                            <table class="table tbl-dashboard">
                                                <tr>
                                                    <th class="publ">Published</th>
                                                    <th class="post">Post</th>
                                                    <th class="prog-cell">Views</th>
                                                    <th class="prog-cell">Engagement</th>
                                                </tr>
                                                <tr>
                                                    <td>
                              <span class="dt">01/10/2018<br>
                              5:41pm</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="post-tbl-link">
                                                            <img src="http://placehold.it/40x43" alt="image">
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Quis, id!
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">632</div>
                                                            <div class="progress">
                                                                <div class="progress-bar primary" role="progressbar"
                                                                     aria-valuenow="63.2" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 63.2%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">32</div>
                                                            <div class="progress">
                                                                <div class="progress-bar light-violet"
                                                                     role="progressbar" aria-valuenow="32"
                                                                     aria-valuemin="0" aria-valuemax="100"
                                                                     style="width: 32%;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">8</div>
                                                            <div class="progress">
                                                                <div class="progress-bar orange" role="progressbar"
                                                                     aria-valuenow="40" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 40%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                              <span class="dt">01/10/2018<br>
                              5:41pm</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="post-tbl-link">
                                                            <img src="http://placehold.it/40x43" alt="image">
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Quis, id!
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">126</div>
                                                            <div class="progress">
                                                                <div class="progress-bar primary" role="progressbar"
                                                                     aria-valuenow="12.6" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 12.6%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">42</div>
                                                            <div class="progress">
                                                                <div class="progress-bar light-violet"
                                                                     role="progressbar" aria-valuenow="42"
                                                                     aria-valuemin="0" aria-valuemax="100"
                                                                     style="width: 42%;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">9</div>
                                                            <div class="progress">
                                                                <div class="progress-bar orange" role="progressbar"
                                                                     aria-valuenow="45" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 45%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                              <span class="dt">01/10/2018<br>
                              5:41pm</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="post-tbl-link">
                                                            <img src="http://placehold.it/40x43" alt="image">
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Quis, id!
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">489</div>
                                                            <div class="progress">
                                                                <div class="progress-bar primary" role="progressbar"
                                                                     aria-valuenow="48.9" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 48.9%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">87</div>
                                                            <div class="progress">
                                                                <div class="progress-bar light-violet"
                                                                     role="progressbar" aria-valuenow="87"
                                                                     aria-valuemin="0" aria-valuemax="100"
                                                                     style="width: 87%;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">16</div>
                                                            <div class="progress">
                                                                <div class="progress-bar orange" role="progressbar"
                                                                     aria-valuenow="80" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 80%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                              <span class="dt">01/10/2018<br>
                              5:41pm</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="post-tbl-link">
                                                            <img src="http://placehold.it/40x43" alt="image">
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Quis, id!
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">87</div>
                                                            <div class="progress">
                                                                <div class="progress-bar primary" role="progressbar"
                                                                     aria-valuenow="8.7" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 8.7%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">46</div>
                                                            <div class="progress">
                                                                <div class="progress-bar light-violet"
                                                                     role="progressbar" aria-valuenow="46"
                                                                     aria-valuemin="0" aria-valuemax="100"
                                                                     style="width: 46%;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tbl-progress">
                                                            <div class="progress-label">12</div>
                                                            <div class="progress">
                                                                <div class="progress-bar orange" role="progressbar"
                                                                     aria-valuenow="62" aria-valuemin="0"
                                                                     aria-valuemax="100" style="width: 62%;"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane second-layout" id="dashFollowersUnfollowers" role="tabpanel">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Followers as of Today: <span>176</span></h4>
                                        <div class="sort-by-select">
                                            <label>Last</label>
                                            <div class="sort-select-wrap">
                                                <select class="form-control" id="sortBy">
                                                    <option>@lang('time.count_days', ['count' => 30])</option>
                                                    <option>@lang('time.count_days', ['count' => 15])</option>
                                                    <option>Item2</option>
                                                </select>
                                                <i class="trav-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-sidebar-inner">
                                        <div class="dash-main">
                                            <canvas id="totalfollowersasofChart" width="230" height="150"></canvas>
                                        </div>
                                        <div class="dash-sidebar">
                                            <h4 class="side-ttl">Benchmark</h4>
                                            <div class="side-txt">
                                                <p>Compare your average preformance over time</p>
                                            </div>
                                            <div class="side-txt">
                                                <ul class="txt-list">
                                                    <li>
                                                        <span>Total Followers</span>
                                                        <i class="fa fa-check"></i></li>
                                                    <li>
                                                        <span>Daily Followers</span>
                                                        <i class="fa fa-check"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="side-txt border-disabled">
                                                <p><b>What leaded people to follow you?</b></p>
                                            </div>
                                            <a href="#" class="btn btn-light-grey btn-bordered">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">UnFollowers as of Today: <span>23</span></h4>
                                        <div class="sort-by-select">
                                            <label>Last</label>
                                            <div class="sort-select-wrap">
                                                <select class="form-control" id="sortBy">
                                                    <option>@lang('time.count_days', ['count' => 30])</option>
                                                    <option>@lang('time.count_days', ['count' => 15])</option>
                                                    <option>Item2</option>
                                                </select>
                                                <i class="trav-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-sidebar-inner">
                                        <div class="dash-main">
                                            <canvas id="totalunfollowersasofChart" width="230" height="150"></canvas>
                                        </div>
                                        <div class="dash-sidebar">
                                            <h4 class="side-ttl">Benchmark</h4>
                                            <div class="side-txt">
                                                <p>Compare your average preformance over time</p>
                                            </div>
                                            <div class="side-txt">
                                                <ul class="txt-list">
                                                    <li>
                                                        <span>Total UnFollowers</span>
                                                        <i class="fa fa-check"></i></li>
                                                    <li>
                                                        <span>Daily UnFollowers</span>
                                                        <i class="fa fa-check"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="side-txt border-disabled">
                                                <p><b>What leaded people to unfollow you?</b></p>
                                            </div>
                                            <a href="#" class="btn btn-light-grey btn-bordered">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="dashSetting" role="tabpanel">pane 4</div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- modal -->
<!-- would visit place popup -->
<div class="modal fade white-style" data-backdrop="false" id="seeAllModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-780" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-all-notification">
                <div class="block-head">
                    <h4 class="ttl">All Notifications</h4>
                </div>
                <div class="notification-wrap">
                    <div class="conversation-inner bordered">
                        <div class="conv-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/30x30" alt="image">
                            </div>
                            <div class="conv-txt">
                                <div class="name">
                                    <a href="#" class="inner-link">Stephen Bugno</a>
                                    @lang('profile.shared_a')
                                    <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                </div>
                                <div class="time">
                                    <p>3 hours ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="conv-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/30x30" alt="image">
                            </div>
                            <div class="conv-txt">
                                <div class="name">
                                    <a href="#" class="inner-link">Steve</a>
                                    @lang('profile.shared_a')
                                    <a href="#" class="inner-link">@lang('place.photo')</a>
                                </div>
                                <div class="time">
                                    <p>4 hours ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="conv-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/30x30" alt="image">
                            </div>
                            <div class="conv-txt">
                                <div class="name">
                                    <a href="#" class="inner-link">Sarah New</a>
                                    @lang('dashboard.reacted_on_a')
                                    <a href="#" class="inner-link">@lang('place.photo')</a>
                                    @lang('dashboard.of_you')
                                </div>
                                <div class="time">
                                    <p>12 hours ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="conv-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/30x30" alt="image">
                            </div>
                            <div class="conv-txt">
                                <div class="name">
                                    <a href="#" class="inner-link">James M. Province</a>
                                    @lang('dashboard.is_now_following')
                                    <a href="#" class="inner-link">Tokyo Tv Tower</a>
                                </div>
                                <div class="time">
                                    <p>1 day ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="conv-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/30x30" alt="image">
                            </div>
                            <div class="conv-txt">
                                <div class="name">
                                    <a href="#" class="inner-link">Veronica</a>,
                                    <a href="#" class="inner-link">Bagwell</a>...
                                    <a href="#" class="inner-link">@lang('other.and_count_more', ['count' => 5])</a>
                                    @lang('dashboard.commented_on_your')
                                    <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                </div>
                                <div class="time">
                                    <p>2 days ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="conv-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/30x30" alt="image">
                            </div>
                            <div class="conv-txt">
                                <div class="name">
                                    <a href="#" class="inner-link">Stephen Bugno</a>
                                    @lang('profile.shared_a')
                                    <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                </div>
                                <div class="time">
                                    <p>2 days ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="conv-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/30x30" alt="image">
                            </div>
                            <div class="conv-txt">
                                <div class="name">
                                    <a href="#" class="inner-link">Warner</a>
                                    @lang('dashboard.reacted_on_a')
                                    <a href="#" class="inner-link">@lang('place.photo')</a>
                                    @lang('dashboard.of_you')
                                </div>
                                <div class="time">
                                    <p>4 days ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="conv-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/30x30" alt="image">
                            </div>
                            <div class="conv-txt">
                                <div class="name">
                                    <a href="#" class="inner-link">Zelma Brady</a>
                                    @lang('profile.shared_a')
                                    <a href="#" class="inner-link">@lang('trip.trip_plan')</a>
                                </div>
                                <div class="time">
                                    <p>3 hours ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="conv-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/30x30" alt="image">
                            </div>
                            <div class="conv-txt">
                                <div class="name">
                                    <a href="#" class="inner-link">Dona Ramsey</a>
                                    @lang('dashboard.is_now_following')
                                    <a href="#" class="inner-link">New York City</a>
                                </div>
                                <div class="time">
                                    <p>3 hours ago</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="notification-loader">
              <span class="spinner">
                <i class="trav-loading-icon"></i>
              </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- why follow/unfollow popup -->
<div class="modal fade white-style" data-backdrop="false" id="whyFollowUnfollowPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-840" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-setting-block search-travel-mates">
                <div class="post-side-top">
                    <h3 class="side-ttl">What leaded people to follow you?</h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="why-follow-unfollow-wrap">
                    <div class="why-f-u-inner">
                        <div class="why-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/60x60" alt="image">
                            </div>
                            <div class="link-wrap">
                                <p><a href="#" class="link"><b>Bryan Gomez</b></a></p>
                            </div>
                            <div class="icon-wrap">
                                <i class="trav-user-icon"></i>
                            </div>
                            <div class="text-wrap">
                                <p>Followed you naturally after visiting your profile</p>
                            </div>
                        </div>
                        <div class="why-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/60x60" alt="image">
                            </div>
                            <div class="link-wrap">
                                <p><a href="#" class="link">Aliquam vehicula nibh et condimentum condimentum.</a></p>
                            </div>
                            <div class="icon-wrap">
                                <i class="trav-post-icon"></i>
                            </div>
                            <div class="text-wrap">
                                <p><b>12</b> Followed you after seeing your post</p>
                            </div>
                        </div>
                        <div class="why-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/60x60" alt="image">
                            </div>
                            <div class="link-wrap">
                                <p>Timeline <a href="#" class="link">@lang('place.photo')</a></p>
                            </div>
                            <div class="icon-wrap">
                                <i class="trav-camera2"></i>
                            </div>
                            <div class="text-wrap">
                                <p><b>3</b> Followed you after seeing your photo</p>
                            </div>
                        </div>
                        <div class="why-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/60x60" alt="image">
                            </div>
                            <div class="link-wrap">
                                <p><a href="#" class="link">Vivamus tincidunt vel nisi laoreet mollis. Praesent
                                        ma...</a></p>
                            </div>
                            <div class="icon-wrap">
                                <i class="trav-video-icon"></i>
                            </div>
                            <div class="text-wrap">
                                <p><b>17</b> Followed you after watching your video</p>
                            </div>
                        </div>
                        <div class="why-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/60x60" alt="image">
                            </div>
                            <div class="link-wrap">
                                <p><a href="#" class="link">3 Days in Tokyo</a></p>
                            </div>
                            <div class="icon-wrap">
                                <i class="trav-trip-plan-loc-icon"></i>
                            </div>
                            <div class="text-wrap">
                                <p><b>8</b> Followed you after seeing your trip plan</p>
                            </div>
                        </div>
                        <div class="why-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/60x60" alt="image">
                            </div>
                            <div class="link-wrap">
                                <p><a href="#" class="link">5 Things to Know Before Visiting France</a></p>
                            </div>
                            <div class="icon-wrap">
                                <i class="trav-report-icon"></i>
                            </div>
                            <div class="text-wrap">
                                <p><b>8</b> Followed you after seeing your report</p>
                            </div>
                        </div>
                    </div>
                    <div class="notification-loader">
              <span class="spinner">
                <i class="trav-loading-icon"></i>
              </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="./assets2/js/script.js"></script>

<script src="https://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
<script>
    var ctx = document.getElementById("viewsChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        gridLines: {
            display: false
        },
        data: {
            labels: ["", "", "", "", "", "", ""],

            datasets: [{
                label: 'Daily views',
                data: [12, 19, 3, 5, 2, 3, 6],
                backgroundColor: [
                    'rgba(255, 255, 255, 0.2)',

                ],
                borderColor: [
                    'rgba(64,129,255,1)',

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    });


    var ctx2 = document.getElementById("followersChart").getContext('2d');
    var myChart = new Chart(ctx2, {
        type: 'line',
        gridLines: {
            display: false
        },
        data: {
            labels: ["", "", "", "", "", "", ""],

            datasets: [{
                label: 'Daily followers',
                data: [3, 2, 1, 1, 1, 1, 1],
                backgroundColor: [
                    'rgba(255, 255, 255, 0.2)',

                ],
                borderColor: [
                    'rgba(64,129,255,1)',

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    });

    var ctx3 = document.getElementById("unfollowersChart").getContext('2d');
    var myChart = new Chart(ctx3, {
        type: 'line',
        gridLines: {
            display: false
        },
        data: {
            labels: ["", "", "", "", "", "", ""],

            datasets: [{
                label: 'Daily unfollowers',
                data: [0, 0, 1, 1, 0, 0, 1],
                backgroundColor: [
                    'rgba(255, 255, 255, 0.2)',

                ],
                borderColor: [
                    'rgba(64,129,255,1)',

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    });

    var ctx4 = document.getElementById("totalviewsasofChart").getContext('2d');
    var myChart = new Chart(ctx4, {
        type: 'line',
        gridLines: {
            display: false
        },
        data: {
            labels: ["", "", "", "", "", "", ""],

            datasets: [{
                label: 'Total daily views of your posts',
                data: [45, 32, 21, 34, 12, 10, 11],
                backgroundColor: [
                    'rgba(255, 255, 255, 0.2)',

                ],
                borderColor: [
                    'rgba(64,129,255,1)',

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    });

    var ctx5 = document.getElementById("totalcommentsasofChart").getContext('2d');
    var myChart = new Chart(ctx5, {
        type: 'line',
        gridLines: {
            display: false
        },
        data: {
            labels: ["", "", "", "", "", "", ""],

            datasets: [{
                label: 'Total daily comments of your posts',
                data: [12, 10, 8, 12, 9, 7, 6],
                backgroundColor: [
                    'rgba(255, 255, 255, 0.2)',

                ],
                borderColor: [
                    'rgba(64,129,255,1)',

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    });

    var ctx6 = document.getElementById("totallikesasofChart").getContext('2d');
    var myChart = new Chart(ctx6, {
        type: 'line',
        gridLines: {
            display: false
        },
        data: {
            labels: ["", "", "", "", "", "", ""],

            datasets: [{
                label: 'Total daily likes of your posts',
                data: [65, 51, 55, 61, 23, 35, 36],
                backgroundColor: [
                    'rgba(255, 255, 255, 0.2)',

                ],
                borderColor: [
                    'rgba(64,129,255,1)',

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    });

    var ctx7 = document.getElementById("totalsharesasofChart").getContext('2d');
    var myChart = new Chart(ctx7, {
        type: 'line',
        gridLines: {
            display: false
        },
        data: {
            labels: ["", "", "", "", "", "", ""],

            datasets: [{
                label: 'Total daily shares of your posts',
                data: [12, 10, 11, 9, 8, 12, 13],
                backgroundColor: [
                    'rgba(255, 255, 255, 0.2)',

                ],
                borderColor: [
                    'rgba(64,129,255,1)',

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    });


    var ctx8 = document.getElementById("totalfollowersasofChart").getContext('2d');
    var myChart = new Chart(ctx8, {
        type: 'line',
        gridLines: {
            display: false
        },
        data: {
            labels: ["", "", "", "", "", "", ""],

            datasets: [{
                label: 'Total daily followers for you',
                data: [24, 20, 18, 20, 12, 18, 11],
                backgroundColor: [
                    'rgba(255, 255, 255, 0.2)',

                ],
                borderColor: [
                    'rgba(64,129,255,1)',

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    });


    var ctx9 = document.getElementById("totalunfollowersasofChart").getContext('2d');
    var myChart = new Chart(ctx9, {
        type: 'line',
        gridLines: {
            display: false
        },
        data: {
            labels: ["", "", "", "", "", "", ""],

            datasets: [{
                label: 'Total daily unfollowers for you',
                data: [2, 0, 2, 3, 3, 2, 3],
                backgroundColor: [
                    'rgba(255, 255, 255, 0.2)',

                ],
                borderColor: [
                    'rgba(64,129,255,1)',

                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    });
</script>

</body>

</html>