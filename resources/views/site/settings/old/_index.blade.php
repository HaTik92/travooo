<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="./assets2/css/style.css">
    <title>Travooo - settings page</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">

        <button class="btn btn-mobile-side setting-sidebar-toggler" id="leftSidebarToggler">
            <i class="trav-cog"></i>
        </button>

        <div class="container-fluid">
            <div class="custom-row setting-layout">
                <!-- MAIN-CONTENT -->
                <div class="left-side-tab-layer" id="sidebarSetting">
                    <h3 class="left-side-ttl">Settings</h3>
                    <ul class="left-side-list">
                        <li class="active" data-tab="account"><a href="#">Account info</a></li>
                        <li data-tab="privacy"><a href="#">Privacy</a></li>
                        <li data-tab="security"><a href="#">Security</a></li>
                        <li data-tab="social"><a href="#">Social profiles</a></li>
                    </ul>
                </div>
                <div class="main-content-layer">
                    <div class="post-block post-setting-block" data-content="account">
                        <form method="post" action="{{url('settings/account')}}" autocomplete="off">
                            <div class="post-side-top">
                                <h3 class="side-ttl">Account info</h3>
                            </div>
                            <div class="post-content-inner">

                                <div class="post-form-wrapper">
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="name" placeholder="Name"
                                                       value="{{$user->name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">I am</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <select name="gender" id="gender" class="custom-select">
                                                    <option value="1"
                                                            @if($user->gender==1) selected @endif>@lang('profile.male')
                                                    </option>
                                                    <option value="2"
                                                            @if($user->gender==2) selected @endif>@lang('profile.female')
                                                    </option>
                                                    <option value="0" @if($user->gender==0) selected @endif>
                                                        Unspecified
                                                    </option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">@lang('profile.age')</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <select name="age" id="age" class="custom-select">
                                                    @for($i=18;$i<100;$i++)
                                                        <option value="{{$i}}"
                                                                @if($user->age==$i) selected @endif>{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <!--
                                    <div class="row">
                                      <label for="" class="col-sm-3 col-form-label">Preferred language</label>
                                      <div class="col-sm-9">
                                        <div class="flex-custom">
                                          <select name="" id="" class="custom-select">
                                            <option value="male">English</option>
                                          </select>
                                        </div>
                                        <div class="form-txt">
                                          <p>We'll send you messages in this language.</p>
                                        </div>
                                      </div>
                                    </div>
                                    -->
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Where you from</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="where_you_from"
                                                       id="where_you_from" placeholder="Where you from"
                                                       value="{{$user->nationality}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Profession</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="profession"
                                                       placeholder="Your profession" value="{{$user->display_name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Describe Yourself</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <textarea class="flex-text" name="about" id="about"
                                                          rows="6">{{$user->about}}</textarea>
                                            </div>
                                            <div class="form-txt">
                                                <p>Travooo is built on relationships. Help other people get to know
                                                    you.</p>
                                                <p>Tell them about the things you like: What are 5 things you can’t live
                                                    without? Share your favorite travel destinations, books, movies,
                                                    shows, music, @lang('place.food').</p>
                                                <p>Tell them what it’s like to have you as a guest or host: What’s your
                                                    style of traveling? Of Airbnb hosting?</p>
                                                <p>Tell them about you: Do you have a life motto?</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-foot-btn">
                                <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')</button>
                            </div>
                        </form>
                    </div>

                    <div class="post-block post-setting-block" data-content="privacy" style="display:none;">
                        <form method="post" action="{{url('settings/privacy')}}">
                            <div class="post-side-top">
                                <h3 class="side-ttl">Privacy</h3>
                            </div>
                            <div class="post-content-inner">
                                <div class="post-form-wrapper">
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Who can see my</label>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <label for="" class="col-md-6 col-lg-3 col-form-label label-inside">Posts</label>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="flex-custom">
                                                        <select name="" id="" class="custom-select">
                                                            <option value="everyone">Everyone</option>
                                                            <option value="friends">@lang('profile.friends')</option>
                                                            <option value="followers">Followers</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <label for="" class="col-md-6 col-lg-3 col-form-label label-inside">Favorites</label>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="flex-custom">
                                                        <select name="" id="" class="custom-select">
                                                            <option value="everyone">Everyone</option>
                                                            <option value="friends">@lang('profile.friends')</option>
                                                            <option value="followers">Followers</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label for=""
                                                       class="col-md-6 col-lg-3 col-form-label label-inside">@lang('place.photos')</label>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="flex-custom">
                                                        <select name="" id="" class="custom-select">
                                                            <option value="everyone">Everyone</option>
                                                            <option value="friends">@lang('profile.friends')</option>
                                                            <option value="followers">Followers</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <label for="" class="col-md-6 col-lg-3 col-form-label label-inside">Followings</label>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="flex-custom">
                                                        <select name="" id="" class="custom-select">
                                                            <option value="everyone">Everyone</option>
                                                            <option value="friends">@lang('profile.friends')</option>
                                                            <option value="followers">Followers</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label for=""
                                                       class="col-md-6 col-lg-3 col-form-label label-inside">@lang('place.videos')</label>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="flex-custom">
                                                        <select name="" id="" class="custom-select">
                                                            <option value="everyone">Everyone</option>
                                                            <option value="friends">@lang('profile.friends')</option>
                                                            <option value="followers">Followers</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label for="" class="col-md-6 col-lg-3 col-form-label label-inside">Trip
                                                    Plans</label>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="flex-custom">
                                                        <select name="" id="" class="custom-select">
                                                            <option value="everyone">Everyone</option>
                                                            <option value="friends">@lang('profile.friends')</option>
                                                            <option value="followers">Followers</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-txt">
                                                <p>If you use this tool, content on your timeline you've shared with
                                                    friends of friends or Public will change to Friends. Remember:
                                                    people who are tagged and their friends may see those posts as
                                                    well.</p>
                                                <p>You also have the option to individually change the audience of your
                                                    posts. Just go to the post you want to change and choose a different
                                                    audience.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-foot-btn">
                                <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')</button>
                            </div>
                        </form>
                    </div>

                    <div class="post-block post-setting-block" data-content="security" style="display:none;">
                        <form method="post" action="{{url('settings/security')}}">
                            <div class="post-side-top">
                                <h3 class="side-ttl">Security settings</h3>
                            </div>
                            <div class="post-content-inner">
                                <div class="post-form-wrapper">
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Change password</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" id=""
                                                       placeholder="Current password" value="">
                                            </div>
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" id="" placeholder="New password"
                                                       value="">
                                            </div>
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" id=""
                                                       placeholder="Re-type new password" value="">
                                            </div>
                                            <div class="form-txt">
                                                <p>It's a good idea to use a strong password that you're not using
                                                    elsewhere</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="post-foot-btn">
                                <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')</button>
                            </div>
                        </form>
                    </div>

                    <div class="post-block post-setting-block" data-content="social" style="display:none;">
                        <form method="post" action="{{url('settings/social')}}">
                            <div class="post-side-top">
                                <h3 class="side-ttl">Social profiles</h3>
                            </div>
                            <div class="post-content-inner">
                                <div class="post-form-wrapper">
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Custom URL</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="website" id="website"
                                                       placeholder="Your website link" value="{{$user->website}}">
                                            </div>
                                            <div class="form-txt">
                                                <p>Add a link to your website</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Facebook</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="facebook" id="facebook"
                                                       placeholder="Facebook profile link: facebook.com/username"
                                                       value="{{$user->facebook}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Twitter</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="twitter" id="twitter"
                                                       placeholder="Twitter ID" value="{{$user->twitter}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Instagram</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="instagram" id="instagram"
                                                       placeholder="Instagram profile link: instagram.com/username"
                                                       value="{{$user->instagram}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Medium</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="medium" id="medium"
                                                       placeholder="Medium ID" value="{{$user->medium}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Youtube</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="youtube" id="youtube"
                                                       placeholder="Channel ID" value="{{$user->youtube}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">Vimeo</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="vimeo" id="vimeo"
                                                       placeholder="Vimeo ID" value="{{$user->vimeo}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="" class="col-sm-3 col-form-label">TripAdvisor</label>
                                        <div class="col-sm-9">
                                            <div class="flex-custom">
                                                <input type="text" class="flex-input" name="tripadvisor"
                                                       id="tripadvisor" placeholder="TripAdvisor user name"
                                                       value="{{$user->tripadvisor}}">
                                            </div>
                                            <div class="form-txt">
                                                <p>Add elsewhere links to your profile so people can be in touch with
                                                    you</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-foot-btn">
                                <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="./assets2/js/script.js"></script>

<script type="text/javascript">
    $('[data-tab=account], [data-tab=privacy], [data-tab=security], [data-tab=payments], [data-tab=social]').on('click', function (e) {
        $(this).addClass('active').siblings('[data-tab]').removeClass('active');

        $('[data-content=account]').css('display', 'none');
        $('[data-content=privacy]').css('display', 'none');
        $('[data-content=security]').css('display', 'none');
        $('[data-content=payments]').css('display', 'none');
        $('[data-content=social]').css('display', 'none');
        $('[data-content=' + $(this).data('tab') + ']').css('display', 'block');
        e.preventDefault()
    });
</script>
</body>

</html>