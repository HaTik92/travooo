@section('before_site_script')
<link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
<link href="{{asset('/plugins/intl-tel-input/css/intlTelInput.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="{{asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('/plugins/intl-tel-input/js/intlTelInput-jquery.js')}}" type="text/javascript"></script> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
@endsection
@section('after_scripts')
<style>
    .select2-input {
        min-height:49px !important;
        padding:8px !important;
        border: 1px solid #e6e6e6 !important;
        margin-bottom: 8px;
    }  
    .select2-selection__choice {
        background: #f7f7f7 !important;
        padding: 5px 10px !important;
        position: relative !important;
        border-radius: 0 !important;
        color: #4080ff !important;
        margin-top:0px !important;
    }
    .select2-selection__arrow{
        top:11px !important;
    }
    .ui-state-highlight {
        height:25px;
        background-color:#c4d9ff;
    }
    .padding-10{
        padding: 10px 0 !important;
    }
</style>
<script type="text/javascript">
@if (session()->get('flash_success'))
        @if (is_array(json_decode(session()->get('flash_success'), true)))
toastr.success("{!! implode('', session()->get('flash_success')->all(':message<br/>')) !!}");
@else
toastr.success("{!! session()->get('flash_success') !!}");
@endif
@endif


// mobile view
$("#sidebarSetting a").on("click", function() {
    $(this).closest('.left-side-list').toggleClass('opened');

});

//info tab script
var expertise_value = $(".setting-exp-sel-block").attr('expId');
var expertise_list = expertise_value != '' ? expertise_value.split('-:') : [];

var travelstyle_value = $(".setting-styles-sel-block").attr('styleId')
var travelstyle_list = travelstyle_value != '' ? travelstyle_value.split(',') : [];

var regional_content_list = ['1'];
var changeTimer = false;

var interest_value = $(".setting-interest-sel-block").attr('interestId')
var interest_list = interest_value != '' ? interest_value.split(',') : [];


$(document).on('click', '.del-account-link', function () {
    $('.delete-account-block').removeClass('d-none')
    $('.account-info-block').addClass('d-none')
})

$(document).on('click', '[data-tab=account]', function () {
    $('.delete-account-block').addClass('d-none')
    $('.account-info-block').removeClass('d-none')
})


//delete account select option script
$(".reason-sel-select").each(function () {
    var classes = $(this).attr("class"),
            id = $(this).attr("id"),
            name = $(this).attr("name");

    var template = '<div class="' + classes + '">';
    template += '<span class="r-sel-select-trigger sel-placeholder">' + $(this).attr("placeholder") + '</span>';
    template += '<div class="r-sel-select-options">';
    $(this).find("option").each(function () {
        template += '<span class="r-sel-select-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
    });
    template += '</div></div><label id="delete_reason-error" class="error" for="delete_reason"></label>';

    $(this).wrap('<div class="reason-sel-block"></div>');
    $(this).hide();
    $(this).after(template);
});

$(".r-sel-select-option:first-of-type").hover(function () {
    $(this).parents(".r-sel-select-options").addClass("option-hover");
}, function () {
    $(this).parents(".r-sel-select-options").removeClass("option-hover");
});
$(".r-sel-select-trigger").on("click", function () {
    $('html').one('click', function () {
        $(".reason-sel-select").removeClass("opened");
    });
    $(this).parents(".reason-sel-select").toggleClass("opened");
    event.stopPropagation();
});


//Select delete account reason
$(document).on("click", ".r-sel-select-option", function () {
    $(this).parents(".r-sel-select-options").find(".r-sel-select-option").removeClass("selection");
    $(this).addClass("selection");
    $(this).parents(".reason-sel-select").removeClass("opened");
    $(this).parents(".reason-sel-select").find(".r-sel-select-trigger").removeClass('sel-placeholder');
    $(this).parents(".reason-sel-select").find(".r-sel-select-trigger").html($(this).html());
    $('#delete_reason-error').html('')

    $(this).parents(".reason-sel-block").find("select").val($(this).data('value'));

});

$(document).on('keyup', '#setting_social_form input', function () {
    $('.socila-link-error-block').addClass('d-none');
});

$(document).ready(function () {
   reloadSortable(); 

    
    $('.setting-inputs-phone').intlTelInput({
        autoHideDialCode: false,
        formatOnDisplay: true,
        separateDialCode: true,
        initialCountry: '',
        utilsScript: $('.setting-inputs-phone').attr('data-utils')
    })


    $('#delete_account_form').validate({
        ignore: [],
        rules: {
            delete_reason: {
                required: true,
            },
        },
        messages: {
            delete_reason: {
                required: "Select a reason field is required."
            }
        }
    });

    $('#setting_social_form').validate({
        rules: {
            website: {
                website: true,
            },
            facebook: {
                facebook: true,
            },
            twitter: {
                twitter: true,
            },
            instagram: {
                instagram: true,
            },
            youtube: {
                youtube: true,
            },
            tumblr: {
                tumblr: true,
            },
            pinterest: {
                pinterest: true,
            },
        },
        submitHandler: function (form) {
                var check_val = '';
            $('#setting_social_form input').each(function (i, e) {
                var inputVal = $(e).val();

                if (inputVal != '') {
                    check_val = inputVal;
                }
            });

            if (check_val == '') {
               $('.socila-link-error-block').removeClass('d-none');
                return false;
            };
            
             form.submit();
        }
    });

    $('#setting_abount_info_form').validate({
        rules: {
            name: {
                required: true,
                maxlength: 50,
            },
            username: {
                required: true,
                minlength: 4,
                maxlength: 15,
            },
        },
        messages: {
            name: {
                required: "The Full Name field is required.",
                maxlength: "Full Name can be max 50 characters long."
            },
            username: {
                required: "The Username field is required.",
                maxlength: "Username can be max 15 characters long.",
                minlength: "Username should be at least 4 characters long."
            }
        },
        submitHandler: function (form) {
            var countryData = $('.setting-inputs-phone').intlTelInput('getSelectedCountryData');
            var tel = '';

            if ($('.setting-inputs-phone').val() != '')
                tel = '+' + countryData['dialCode'] + ' ' + $('.setting-inputs-phone').val();

            if (tel != '' && !$('.setting-inputs-phone').intlTelInput('isValidNumber')) {
                $('.phone_error').html('Please enter valid number');
                return false;
            }

            $('.setting-mobile-number').val(tel)

            form.submit();
        }
    });
    
    //Security validation
    $('#setting_security_form').validate({
        rules: {
            current_password: {
                required: true,
            },
            new_password: {
                required: true,
                minlength: 6,
                password_format:true
            },
            new_confirm_password: {
                required: true,
                equalTo: "#new_password"
            },
        },
        messages: {
            new_confirm_password: " Enter repeat password same as new password",
        },
        submitHandler: function (form) {
            var countryData = $('.setting-inputs-phone').intlTelInput('getSelectedCountryData');
            var tel = '';

            if ($('.setting-inputs-phone').val() != '')
                tel = '+' + countryData['dialCode'] + ' ' + $('.setting-inputs-phone').val();

            if (tel != '' && !$('.setting-inputs-phone').intlTelInput('isValidNumber')) {
                $('.phone_error').html('Please enter valid number');
                return false;
            }

            $('.setting-mobile-number').val(tel)

            form.submit();
        }
    });
    
    //Nationality script
    $('#user_nationality').select2({
        debug: !0,
        placeholder: "Select nationality",
        containerCssClass: "select2-about-input about-sel-select",
    })

//Select expertise
    $('#setting_exp_autocomplete').autocomplete({
        source: function (request, response) {
            jQuery.get('{{url("home/searchCountriesCities")}}', {
                q: request.term
            }, function (data) {
                var items = [];
                var data = JSON.parse(data);

                data.forEach(function (s_item, index) {
                    items.push({
                        id: s_item.id, value: s_item.text, image: s_item.image, type: s_item.type, query: s_item.query
                    });
                });
                response(items);
            });
        },
        classes: {"ui-autocomplete": "travel-mates-autocomplate"},
        focus: function (event, ui) {
            return false;
        },
        select: function (event, ui) {
            $('#setting_exp_autocomplete').val('')
            if ($.inArray(ui.item.id, expertise_list) == -1) {
                if (expertise_list.length < 5) {
                    expertise_list.push(ui.item.id)
                    $('.setting-exp-list').val(expertise_list.join('-:'))
                    $('.setting-exp-sel-block').addClass('mb-15')
                    $('.setting-exp-sel-block').append(getSelectedHtml(ui.item.value, 'close-expertise-filter', 'expertise-id', ui.item.id, 'expertise_list[]'));
                    $('.expertise-error-block').html('')
                } else {
                    $('.expertise-error-block').html('The maximum expertise count is 5.')
                }
            }
            return false;
        },
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
                .append(getMarkupHtml(item))
                .appendTo(ul);
    }

})

//Delete expertise
$(document).on('click', '.close-expertise-filter', function () {
    var exp = $(this).attr('data-expertise-id');
    expertise_list = $.grep(expertise_list, function (value) {
        return value != exp;
    });

    $('.setting-exp-list').val(expertise_list.length > 0 ? expertise_list.join('-:') : '')

    if (expertise_list.length <= 5) {
        $('.expertise-error-block').html('')
    }

    $(this).closest('li').remove();

    if ($('.setting-exp-sel-block').find('li').length == 0) {
        $('.setting-exp-sel-block').removeClass('mb-15')
    }

});

//Travel styles script
$(document).on('keyup', '#setting_trevstyle_autocomplete', function (e) {
    var search_item = $(this).val()
    if (search_item != '') {
        searchTravelStyles(search_item);
    }
});

$(document).on('focus', '#setting_trevstyle_autocomplete', function (e) {
    getTravelStyles();
})

$(document).click(function (event) {
    $target = $(event.target);
    if (!$target.closest('.trev-style-block').length) {
        $('.travel-styles-ui').hide();
    }
});

//select travel styles
$(document).on('click', '.item-wrapper', function (e) {
    var id = $(this).attr('itemId');
    var text = $(this).text();

    $('#setting_trevstyle_autocomplete').val('');
    $('.travel-styles-ui').hide();
    if ($.inArray(id, travelstyle_list) == -1) {
        travelstyle_list.push(id)
        $('.setting-travstyle-list').val(travelstyle_list.join(','))
        $('.setting-styles-sel-block').append(getSelectedHtml(text, 'close-styles-filter', 'styles-id', id, 'travstyle_list[]'));
    }
});

//Remove travel styles
$(document).on('click', '.close-styles-filter', function () {
    var t_style = $(this).attr('data-styles-id');
    travelstyle_list = $.grep(travelstyle_list, function (value) {
        return value != t_style;
    });

    $('.setting-travstyle-list').val(travelstyle_list > 0 ? travelstyle_list.join(',') : '')

    $(this).closest('li').remove()

});


//interests script
$(document).on("paste", ".setting-interests-input", function (e) {
    var pastedData = e.originalEvent.clipboardData.getData('text');
    var inputVal = pastedData.replace(/,/g, '', pastedData);
    var pasteStr = inputVal.split(' ');

    $.each(pasteStr, function (key, value) {
        if ($.inArray(value, interest_list) == -1) {
            if (interest_list.length < 10) {
                interest_list.push(value)
                $('.setting-interests-list').val(interest_list.join(','))
                $('.setting-interest-sel-block').append(getSelectedHtml(value, 'close-interest-filter', 'interest-value', value));
                $('.interest-error-block').html('')
            } else {
                $('.interest-error-block').html('The maximum tags count is 10')
            }
        }

    });
    var self = $(this);
    setTimeout(function (e) {
        self.val('');
    }, 100);
});
$(document).on('keydown', '.setting-interests-input', function (e) {
    if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey) {
        e.preventDefault();
    }
});

$(document).on('keyup', '.setting-interests-input', function (e) {
    if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey) {
        var input_value = $(this).val().trim();
        if (input_value != '') {
            if ($.inArray(input_value, interest_list) == -1) {
                if (interest_list.length < 10) {
                    interest_list.push(input_value)
                    $('.setting-interests-list').val(interest_list.join(','))
                    $('.setting-interest-sel-block').append(getSelectedHtml(input_value, 'close-interest-filter', 'interest-value', input_value));
                    $('.interest-error-block').html('')
                } else {
                    $('.interest-error-block').html('The maximum tags count is 10')
                }
            }
            $(this).val('')
        }
    }
});

//Remove interest
$(document).on('click', '.close-interest-filter', function () {
    var interst = $(this).attr('data-interest-value');
    interest_list = $.grep(interest_list, function (value) {
        return value != interst;
    });
    $('.setting-interests-list').val(travelstyle_list > 0 ? travelstyle_list.join(',') : '')
    if (interest_list.length < 10) {
        $('.interest-error-block').html('')
    }

    $(this).closest('li').remove()

});

$(document).on('blur', '.setting-interests-input', function (e) {
    $(this).val('')
});


//Regional Content styles script
$(document).on('keyup', '#setting_content_language', function (e) {
    var search_item = $(this).val()
    if (search_item != '') {
            if(changeTimer !== false) clearTimeout(changeTimer);
                changeTimer = setTimeout(function(){
                    getRegionalContent(search_item);
                    changeTimer = false;
                },300);
       
    }
});

$(document).on('focus', '#setting_content_language', function (e) {
    getRegionalContent();
})

$(document).click(function (event) {
    $target = $(event.target);
    if (!$target.closest('.regional-content-wrap').length) {
        $('.regional-content-ui').hide();
    }
});

//select regional content
$(document).on('click', '.content-item-wrapper', function (e) {
    var id = $(this).attr('itemId');
    var text = $(this).text();

    $('#setting_content_language').val('');
    $('.regional-content-ui').hide();
    if ($.inArray(id, regional_content_list) == -1) {
        regional_content_list.push(id)
        $('#content_languages').val(regional_content_list.join(','))
        $('.reg-content-list').prepend('<li class="" lngId="'+ id +'"><span>'+ text +'</span><a href="#" class="handle"><img src="{{asset('assets2/image/move.png')}}"></a><a href="javascript:;" class="remove-content-lng" data-id="'+ id +'"><i class="trav-close-icon"></i></a></li>');
        
        reloadSortable();
    }
});

//Remove content language
$(document).on('click', '.remove-content-lng', function () {
    var lng_id = $(this).attr('data-id');
    regional_content_list = $.grep(regional_content_list, function (value) {
        return value != lng_id;
    });
   
    $('#content_languages').val(regional_content_list.join(','))

    $(this).closest('li').remove()

});


//Search blocking users
$('#search_blocking_user').select2({
    minimumInputLength: 3,
    placeholder: "@lang('setting.type_a_name')",
    ajax: {
        url: "{{url('settings/searchUsers')}}",
        dataType: 'json',
        processResults: function (data) {
            return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.text, image:obj.image, nationality:obj.nationality, query:obj.query};
                    })
                };
            }
    },
    templateResult: function (response) {
        if(response){
                var markup = '<div class="search-country-block">';
                if(response.image){
                    markup +='<div class="search-country-imag"><img src="' + response.image + '" /></div>';
                }
                 markup +='<div class="span10">' +
                    '<div class="search-country-info">' +
                    '<div class="search-country-text">' + markLocetionMatch(response.text, response.query) + '</div>';

                    if(response.nationality && response.nationality !== ''){
                           markup += '<div class="search-country-type">From '+ response.nationality +'</div>';
                    }
                    markup +='</div></div>';

              return markup;
        }
    },
    dropdownParent: $('.search-blocking-block'),
    containerCssClass: "setting-block-select",
    dropdownCssClass: "sel-bigdrop",
    escapeMarkup: function (m) { return m; }
});
            
$('#search_blocking_user').on('select2:select', function (e) {
    var data = e.params.data;
    $('#search_blocking_user-error').addClass('d-none')
      
    $('#blocked_user').val(data.id)
});

$(document).on('click', '.add-block-user-link', function(){
    var blocked_user_id = $('#blocked_user').val();
    if(blocked_user_id == ''){
        $('#search_blocking_user-error').html("@lang('setting.select_user_field_is_required')")
        $('#search_blocking_user-error').removeClass('d-none')
        return;
    }else{
        $('#search_blocking_user-error').html("")
        $('#search_blocking_user-error').addClass('d-none')
    }
 
    $('#setting_blocking_form').submit();
})

$(document).on('click', '.unblocked-user-btn', function(e){
    e.preventDefault();
    var blocked_user_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{url('settings/unBlocking')}}",
            data: {'id':blocked_user_id},
            async: false
            }).done(function(data){
                window.location.reload();
            })
})


// load more blocking users
$('.blocked-loader').on('inview', function(event, isInView) {
     if (isInView) {
         var nextPage = parseInt($('#pagenum').val())+1;
         var url = '{{url("settings/get-more-blocking-user")}}';

         $.ajax({
             type: 'GET',
             url: url,
             data: { pagenum: nextPage },
             success: function(data){
                 if(data != ''){
                    $(".blocked-loader").before(data);
                    $('#pagenum').val(nextPage);

                 } else {								 
                     $(".blocked-loader").hide();
                 }
             }
         });
     }
 });


function searchTravelStyles(item) {
    $('.travel-styles-ui').show()
    $.get('home/searchTravelstyles', {q: item}, function (data) {
        var data = JSON.parse(data);
        $('.travel-styles-ui').html('')
        data.results.forEach(function (s_item, index) {
            $('.travel-styles-ui').append('<li class="style-item"><div class="item-wrapper" itemId="' + s_item.id + '">' + s_item.text + '</div></li>')
        });
    });
}


function getTravelStyles() {
    $('.travel-styles-ui').html('')
    $('.travel-styles-ui').show()
    $.get('home/listTravelstylesForSelect', function (data) {
        var data = JSON.parse(data);

        data.results.forEach(function (s_item, index) {
            $('.travel-styles-ui').append('<li class="style-item"><div class="item-wrapper" itemId="' + s_item.id + '">' + s_item.text + '</div></li>')
        });
    });
}

//Get regional content languages
function getRegionalContent(item) {
    $('.regional-content-ui').html('')
    $('.regional-content-ui').show()
    $.get("{{url('settings/search-regional-language')}}", {q: item}, function (data) {
        var data = JSON.parse(data);

        data.forEach(function (s_item, index) {
            $('.regional-content-ui').append('<li class="reg-ui-item"><div class="content-item-wrapper" itemId="' + s_item.id + '">' + s_item.text + '</div></li>')
        });
    });
}

function reloadSortable(){
    $( ".reg-content-list" ).sortable({
        handle: ".handle",
        containment: "parent",
        items: "> li",
        opacity: 0.5,
        tolerance: "pointer",
        stop: function( event, ui ){
            var sortedIDs = $( ".reg-content-list" ).sortable("toArray", {attribute:"lngId"});
            regional_content_list = sortedIDs
            $('#content_languages').val(regional_content_list.join(','))
        },
    });
    
      var sortedIDs = $( ".reg-content-list" ).sortable("toArray", {attribute:"lngId"});
        regional_content_list = sortedIDs
        $('#content_languages').val(regional_content_list.join(','))
}

function getSelectedHtml(title, class_name, data_attr, id, input_name = '') {
    var html = '<li><span class="search-selitem-title">' + title + '</span>' +
            '<span class="close-search-item ' + class_name + '" data-' + data_attr + '="' + id + '"> x</span>';

    if (input_name != '') {
        html += '<input type="hidden"  name="' + input_name + '" value="' + id + '">';
    }

    html += '</li>'

    return html;
}

function getMarkupHtml(response) {
    if (response) {
        var markup = '<div class="search-country-block">';
        if (response.image) {
            markup += '<div class="search-country-imag"><img src="' + response.image + '" /></div>';
        }
        markup += '<div class="span10">' +
                '<div class="search-country-info">' +
                '<div class="search-country-text">' + markLocetionMatch(response.value, response.query) + '</div>';
        if (response.type != '') {
            markup += '<div class="search-country-type">City in ' + response.type + '</div>';
        } else {
            markup += '<div class="search-country-type">Country</div>';
        }
        markup += '</div></div>';

        return markup;
    }
}


function markLocetionMatch(text, term) {
    var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
    var output = text.replace(regEx, "<span class='select2-locetion-rendered'>$1</span>");
    return output;
}

$('[data-tab=account], [data-tab=privacy], [data-tab=security], [data-tab=payments], [data-tab=social], [data-tab=blocking]').on('click', function (e) {
    $(this).addClass('active').siblings('[data-tab]').removeClass('active');

    $('.post-setting-block').css('display', 'none');
    $('[data-content=' + $(this).data('tab') + ']').css('display', 'block');
    e.preventDefault()
});


//Social links validation
$.validator.addMethod("facebook", function (value, element) {
    return this.optional(element) || /(?:https?:\/\/)?(?:www\.)?facebook\.com\/.(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-\.]*)/i.test(value);
}, "Please enter a valid facebook URL."
        );

$.validator.addMethod("twitter", function (value, element) {
    return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/i.test(value);
}, "Please enter a valid twitter URL."
        );

$.validator.addMethod("instagram", function (value, element) {
    return this.optional(element) || /(https?:\/\/(?:www\.)?instagram\.com\/([^/?#&]+)).*/i.test(value);
}, "Please enter a valid instagram URL."
        );

$.validator.addMethod("website", function (value, element) {
    return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
}, "Please enter a valid URL."
        );

$.validator.addMethod("youtube", function (value, element) {
    return this.optional(element) || /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/i.test(value);
}, "Please enter a valid youtube URL."
        );

// $.validator.addMethod("tumblr", function (value, element) {
//     return this.optional(element) || /[^"\/www\."](?<!w{3})[A-Za-z0-9]*(?=\.tumblr\.com)|(?<=\.tumblr\.com\/blog\/).*/i.test(value);
// }, "Please enter a valid tumblr URL."
//         );

$.validator.addMethod("tumblr", function (value, element) {
    return this.optional(element) || /(.*?)/i.test(value);
}, "Please enter a valid tumblr URL."
        );

$.validator.addMethod("pinterest", function (value, element) {
    return this.optional(element) || /(?:(?:http|https):\/\/)?(?:www\.)?(?:pinterest\.com|instagr\.am)\/([A-Za-z0-9-_\.]+)/i.test(value);
}, "Please enter a valid pinterest URL."
        );

$.validator.addMethod("password_format", function (value, element) {
        return this.optional(element) ||/^(?=.*[A-Za-z])(?=.*\d)[a-zA-Z0-9!@#$%^&*]{6,}$/i.test(value);
    },
    "Password should contain 6 minimum characters consisting of at least 1 letter and number."
);

</script>
@endsection