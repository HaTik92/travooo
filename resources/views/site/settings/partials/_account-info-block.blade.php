<div class="account-info-block">
    <form method="post" action="{{url_with_locale('settings/account')}}" id="setting_abount_info_form" autocomplete="off">
        <div class="post-side-top setting-side-block">
            <h3 class="side-ttl">@lang('setting.account_info')</h3>
        </div>
        <div class="post-content-inner">
            @if($errors->any())
            <div class="alert alert-danger profile-alert">
                {!! implode('', $errors->all('<div>:message</div>')) !!}
            </div>
            @endif
            <div class="post-form-wrapper">
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('setting.name')</label>
                    <div class="col-sm-9">
                        <div class="flex-custom">
                            <input type="text" class="flex-input setting-input" name="name" placeholder="@lang('setting.name')" value="{{$user->name}}" maxlength="50" required="required">
                        </div>
                        <label id="name-error" class="error" for="name" style="margin-bottom: 5px;"></label>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('setting.username')</label>
                    <div class="col-sm-9">
                        <div class="flex-custom">
                            <input type="text" class="flex-input setting-input" name="username" placeholder="@lang('setting.username')" value="{{$user->username}}" maxlength="15" required="required">
                        </div>
                        <label id="username-error" class="error" for="username"></label>
                        <div class="form-txt">
                            <p>@lang('setting.your_public_profile_only_shows_your_full_name')</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('setting.iam')</label>
                    <div class="col-sm-3">
                        <div class="flex-custom">
                            <div class="account-inf-text mob-bold">
                                @if($user->gender==1)
                                @lang('setting.male')
                                @elseif($user->gender==2)
                                @lang('setting.female')
                                @else
                                @lang('setting.unspecified')
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('setting.birth_date')</label>
                    <div class="col-sm-9">
                        <div class="flex-custom">
                            <div class="account-inf-text mob-bold">
                                @if($user->birth_date)
                                {{ \Carbon\Carbon::parse($user->birth_date)->format('d M Y') }}
                                @endif
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <label for=""
                           class="col-sm-3 col-form-label">@lang('setting.where_you_from')</label>
                    <div class="col-sm-9">
                        <div class="flex-custom mb-10">
                            <select name="nationality" id="user_nationality" class="about-sel-select" style="width: 100%">
                                @foreach($countries AS $country)
                                @if(is_object($country) AND is_object($country->transsingle))
                                <option value="{{$country->id}}"
                                        @if($user->nationality==$country->id) selected @endif>{{$country->transsingle->title}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('setting.email_address')</label>
                    <div class="col-sm-9">
                        <div class="flex-custom">
                            <input type="email" class="flex-input setting-input" name="contact_email"
                                   placeholder="@lang('setting.insert_your_contact_email')"
                                   value="{{$user->contact_email}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('setting.phone_number')</label>
                    <div class="col-sm-9">
                        <div class="flex-custom">
                            <input type="tel" onkeyup="this.value = this.value.replace(/[^\d]/g, '')" class="setting-inputs-phone" data-utils ="{{asset('/plugins/intl-tel-input/js/utils.js')}}" id="setting_phone_number"  value="{{$user->mobile}}" style="margin-bottom: 0;padding-left: 72px !important;" maxlength="50">
                        </div>
                        <label class="phone_error profile-error-text"></label>
                        <input type="hidden" name="full_mobile_number" class="setting-mobile-number" value="{{$user->mobile}}">
                    </div>
                </div>

                <div class="row">
                    <label for=""  class="col-sm-3 col-form-label">@lang('setting.your_expertise')</label>
                    <div class="col-sm-9">
                        <div class="flex-custom">
                            <input type="text" class="flex-input setting-input setting-exp-autocomplete " id="setting_exp_autocomplete" placeholder="@lang('setting.your_expertise_countries')" style="margin-bottom: 0;" >
                        </div>
                        <label for="exp_autocomplete" class="error expertise-error-block"></label>
                        <ul class="search-selected-block p-8 setting-exp-sel-block {{count($expertise)>0?'mb-15':''}}" expId="{{implode('-:', $user_expertise)}}">
                            @if(count($expertise)>0)
                            @foreach($expertise as $val)
                            <li>
                                <span class="search-selitem-title">{{$val['text']}}</span>  
                                <span class="close-search-item close-expertise-filter" data-expertise-id="{{$val['id']}}"> <i class="trav-close-icon" dir="auto"></i></span>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                        <input type="hidden" name="expertise" class="setting-exp-list" value="{{implode('-:', $user_expertise)}}">
                    </div>
                </div>


                <div class="row">
                    <label for=""  class="col-sm-3 col-form-label">@lang('setting.your_travelstayle')</label>
                    <div class="col-sm-9 trev-style-block">
                        <div class="flex-custom">
                            <input type="text" class="flex-input setting-input trev-style-autocomplete" id="setting_trevstyle_autocomplete" placeholder="@lang('setting.your_travelstayle_placeholder')" style="margin-bottom: 0;" >
                        </div>
                        <ul class="travel-styles-ui setting-trev-ui" style="display: none;"></ul> 
                        <ul class="search-selected-block p-8 setting-styles-sel-block {{count(@$user->travelstyles)>0?'mb-15':''}}" styleId="{{implode(',', $styles_list)}}">
                            @if(count(@$user->travelstyles)>0)
                            @foreach(@$user->travelstyles as $val)
                            <li>
                                <span class="search-selitem-title">{{$val->travelstyle->trans[0]->title}}</span>  
                                <span class="close-search-item close-styles-filter" data-styles-id="{{$val->travelstyle->id}}"> <i class="trav-close-icon" dir="auto"></i></span>

                            </li>
                            @endforeach
                            @endif
                        </ul>
                        <input type="hidden" name="travelstyles" class="setting-travstyle-list" value="{{implode(',', $styles_list)}}">
                    </div>
                </div>

                <div class="row">
                    <label for=""
                           class="col-sm-3 col-form-label">@lang('profile.interest')</label>
                    <div class="col-sm-9">
                        <div class="flex-custom">
                            <input type="text" class="flex-input setting-input mb-0 setting-interests-input" maxlength="15"  placeholder="@lang('profile.interest')">
                        </div>
                        <label for="interests" class="error interest-error-block"></label>
                        <ul class="search-selected-block p-8 setting-interest-sel-block {{@$user->interests?'mb-15':''}}" interestId="{{@$user->interests}}">
                            @if(@$user->interests)
                            @php $intersts = explode(',', @$user->interests); @endphp
                            @foreach($intersts as $val)
                            <li>
                                <span class="search-selitem-title">{{$val}}</span>  
                                <span class="close-search-item close-styles-filter" data-styles-id="{{$val}}"> <i class="trav-close-icon" dir="auto"></i></span>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                        <input type="hidden" class="setting-interests-list" name="interest_list" value="{{@$user->interests}}">
                    </div>
                </div>

                <div class="row">
                    <label for=""
                           class="col-sm-3 col-form-label">@lang('setting.content_language')</label>
                    <div class="col-sm-9 regional-content-wrap">
                        <div class="flex-custom">
                            <input type="text" class="flex-input setting-input mb-0 setting-regional-input" id="setting_content_language" maxlength="15"  placeholder="@lang('setting.search_for_preferred_languages')">
                        </div>
                        <input type="hidden" name="content_languages" id="content_languages" value="1">
                        <ul class="regional-content-ui setting-trev-ui" style="display: none;"></ul> 
                        <div class="form-txt">
                            <p>@lang('setting.enter_the_languages_that_determine_the_content')</p>
                        </div>
                        <div class="reg-content-block">
                            <ul class="reg-content-list">
                                @if(count(@$user->content_languages) > 0)
                                    @foreach(@$user->content_languages as $lang)
                                        <li class="" lngId="{{$lang->language_id}}"><span class="{{$lang->language_id == 1?'def-language':''}}">{{@$lang->languages->transsingle->title}}</span>
                                        <a href="#" class="handle">
                                            <img src="{{asset('assets2/image/move.png')}}">
                                        </a>
                                            @if($lang->language_id != 1)
                                                <a href="javascript:;" class="remove-content-lng" data-id="{{$lang->language_id}}"><i class="trav-close-icon"></i></a>
                                            @endif
                                        </li>
                                    @endforeach
                                @else
                                <li class="" lngId="1"><span class="def-language">English</span>
                                    <a href="#" class="handle">
                                        <img src="{{asset('assets2/image/move.png')}}">
                                    </a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label for=""
                           class="col-sm-3 col-form-label">@lang('setting.describe_yourself')</label>
                    <div class="col-sm-9">
                        <div class="flex-custom">
                            <textarea class="flex-text setting-text" name="about" id="about"
                                      rows="6">{{$user->about}}</textarea>
                        </div>
                        <div class="form-txt">
                            <p>@lang('setting.travooo_is_built_on_relationships')</p>
                            <p>@lang('setting.tell_them_about_the_things_you_like')</p>
                            <p>@lang('setting.tell_them_what_it_like_to_have_you')</p>
                            <p>@lang('setting.tell_them_about_you')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-foot-btn">
            <a href="javascript:;" class="del-account-link">@lang('setting.delete_account')</a>
            <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')</button>
        </div>
    </form>
</div>



<div class="delete-account-block d-none">
    <form method="post" action="{{url_with_locale('settings/delete-account')}}" id="delete_account_form" autocomplete="off">
        <div class="post-side-top setting-side-block">
            <h3 class="side-ttl">@lang('setting.delete_your_account')</h3>
        </div>
        <div class="post-content-inner setting-content-inner">
            <div class="setting-inner-block">
                <p>Are you sure you want to delete your account?</p>
            </div>
            <div class="setting-inner-block">
                <p>Deleting your account means that everything that you have created will also be removed. This includes your Trip Plans,
                    Discussions, Reviews, Travelogs, along with other data. You will be removed from any Trip Plans or other collaborations
                    that you were a part of, and you'll be delisted from any of the suggestion based showcases. Your username will also become
                    available for other users to acquire.</p>
            </div>
            <div class="setting-inner-block">
                <p><span class="rel-bold">Please note:</span> if you delete your account you won't be able to reactivate it.</p>
            </div>
            <div class="setting-inner-block">
                <div><span class="rel-bold">Why are you deleting your account?</span></div> 
                <div class="reason-sel-block">
                    <select name="delete_reason" class="reason-sel-select" placeholder="Select a reason" required="required">
                        <option value=""></option>
                        @foreach($del_reason_list as $del_reason)
                        <option value="{{$del_reason->id}}">{{$del_reason->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="post-foot-btn">
            <button class="btn btn-light-danger btn-bordered" type="submit">@lang('setting.button_delete_account')</button>
        </div>
    </form>
</div>


