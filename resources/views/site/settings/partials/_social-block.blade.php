<form method="post" action="{{url_with_locale('settings/social')}}" id="setting_social_form" autocomplete="off">
    <div class="post-side-top setting-side-block">
        <h3 class="side-ttl">@lang('setting.social_profiles')</h3>
    </div>
    <div class="post-content-inner">
         <div class="alert alert-danger profile-alert socila-link-error-block d-none">
                        Any one field is required.
                </div>
        <div class="post-form-wrapper">
            <div class="row mb-10">
                <label for="website" class="col-sm-3 col-form-label">@lang('setting.custom_url')</label>
                <div class="col-sm-9">
                    <div class="flex-custom">
                        <input type="text" class="flex-input setting-input" name="website" id="website"
                               placeholder="@lang('setting.your_website_link')"
                               value="{{$user->website}}">
                    </div>
                    <label id="website-error" class="error" for="website"></label>
                    <div class="form-txt">
                        <p>@lang('setting.add_a_link_to_your_website')</p>
                    </div>
                </div>
            </div>
            <div class="row mb-10">
                <label for="facebook" class="col-sm-3 col-form-label">@lang('setting.facebook')</label>
                <div class="col-sm-9">
                    <div class="flex-custom">
                        <input type="text" class="flex-input setting-input" name="facebook" id="facebook"
                               placeholder="@lang('setting.facebook_profile_link')"
                               value="{{$user->facebook}}">
                    </div>
                    <label id="facebook-error" class="error" for="facebook"></label>
                </div>
            </div>
            <div class="row mb-10">
                <label for="twitter" class="col-sm-3 col-form-label">@lang('setting.twitter')</label>
                <div class="col-sm-9">
                    <div class="flex-custom">
                        <input type="text" class="flex-input setting-input" name="twitter" id="twitter"
                               placeholder="@lang('setting.twitter_link')"
                               value="{{$user->twitter}}">
                    </div>
                    <label id="twitter-error" class="error" for="twitter"></label>
                </div>
            </div>
            <div class="row mb-10">
                <label for="instagram" class="col-sm-3 col-form-label">@lang('setting.instagram')</label>
                <div class="col-sm-9">
                    <div class="flex-custom">
                        <input type="text" class="flex-input setting-input" name="instagram" id="instagram"
                               placeholder="@lang('setting.instagram_profile_link')"
                               value="{{$user->instagram}}">
                    </div>
                    <label id="instagram-error" class="error" for="instagram"></label>
                </div>
            </div>
            <div class="row mb-10">
                <label for="pinterest" class="col-sm-3 col-form-label">@lang('setting.pinterest')</label>
                <div class="col-sm-9">
                    <div class="flex-custom">
                        <input type="text" class="flex-input setting-input" name="pinterest" id="pinterest"
                               placeholder="@lang('setting.pinterest_link')"
                               value="{{$user->pinterest}}">
                    </div>
                    <label id="pinterest-error" class="error" for="pinterest"></label>
                </div>
            </div>
            <div class="row mb-10">
                <label for="tumblr" class="col-sm-3 col-form-label">@lang('setting.tumblr')</label>
                <div class="col-sm-9">
                    <div class="flex-custom">
                        <input type="text" class="flex-input setting-input" name="tumblr" id="tumblr"
                               placeholder="@lang('setting.tumblr_link')" value="{{$user->tumblr}}">
                    </div>
                    <label id="tumblr-error" class="error" for="tumblr"></label>
                </div>
            </div>
            <div class="row mb-10">
                <label for="youtube" class="col-sm-3 col-form-label">@lang('setting.youtube')</label>
                <div class="col-sm-9">
                    <div class="flex-custom">
                        <input type="text" class="flex-input setting-input" name="youtube" id="youtube"
                               placeholder="@lang('setting.youtuble_link')"
                               value="{{$user->youtube}}">
                    </div>
                    <label id="youtube-error" class="error" for="youtube"></label>
                    <div class="form-txt">
                        <p>@lang('setting.add_elsewhere_links_to_your_profile_so_people')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="post-foot-btn">
        <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')</button>
    </div>
</form>