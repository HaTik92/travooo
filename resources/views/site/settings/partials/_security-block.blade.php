<form method="post" action="{{url_with_locale('settings/security')}}" id="setting_security_form" autocomplete="off">
    <div class="post-side-top setting-side-block">
        <h3 class="side-ttl">@lang('setting.security_settings')</h3>
    </div>
    <div class="post-content-inner">
        @if($errors->any())
        <div class="alert alert-danger profile-alert">
            {!! implode('', $errors->all('<div>:message</div>')) !!}
        </div>
        @endif
        <div class="post-form-wrapper">
            <div class="row">
                <label for=""
                       class="col-sm-3 col-form-label">@lang('setting.change_password')</label>
                <div class="col-sm-9">
                    <div class="flex-custom">
                        <input name="current_password" type="password" class="flex-input setting-input" id="current_password"
                               placeholder="@lang('setting.current_password')" value="">
                    </div>
                    <label id="current_password-error" class="error" for="current_password"></label>
                    <div class="flex-custom">
                        <input name="new_password" type="password" class="flex-input setting-input" id="new_password"
                               placeholder="@lang('setting.new_password')"
                               value="">
                    </div>
                    <label id="new_password-error" class="error" for="new_password"></label>
                    <div class="flex-custom">
                        <input name="new_confirm_password" type="password" class="flex-input setting-input" id="new_confirm_password"
                               placeholder="@lang('setting.re_type_new_password')" value="">
                    </div>
                    <label id="new_confirm_password-error" class="error" for="new_confirm_password"></label>
                    <div class="form-txt">
                        <p>@lang('setting.it_s_a_good_idea_to_use_a_strong_password_that_you')</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <label for=""
                       class="col-sm-3 col-form-label">@lang('setting.your_session')</label>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-4 pr-0 sec-info-title">Current Session </div>
                        <div class="col-sm-8 pl-0 pb-2">
                            <div class="sec-info-block">
                                <div class="sec-info-wrap">
                                    <p><span class="sec-info-bold">{{$aget_info['user_ip']}}</span></p>
                                    <p><span class="sec-info-bold">{{$aget_info['browser']}}</span> on {{$aget_info['platform']}}</p>
                                    <p><span class="sec-info-bold">Last accessed:</span> {{($user->last_login)?\Carbon\Carbon::parse($user->last_login)->format('d M, Y'):\Carbon\Carbon::parse($user->updated_at)->format('d M, Y')}}</p>
                                    <p><span class="sec-info-bold">Signed in:</span>  {{\Carbon\Carbon::parse($user->created_at)->format('d M, Y')}} </p>
                                </div>
                                <div class="sec-info-report-wrap">
                                    <a href="javascript:;" class="sec-report-link">Report</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-txt">
                        <p>@lang('setting.this_is_a_list_of_devices')</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="post-foot-btn">
        <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')</button>
    </div>
</form>