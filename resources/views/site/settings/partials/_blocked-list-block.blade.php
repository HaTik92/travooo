@foreach($setting_block_users as $blocked_users)
<li class="blocked-user-item">
    <div class="blocked-user-info-wrap">
        <img src="{{check_profile_picture(@$blocked_users->blocked_user->profile_picture)}}">
        <div class="blocked-user-info-title">
            <p>{{@$blocked_users->blocked_user->name}}</p>
            <span>{{@$blocked_users->blocked_user->nationality ? 'From '.$blocked_users->blocked_user->country_of_nationality->transsingle->title : ''}}</span>
        </div>
    </div>
    <div>
        <button class="btn unblocked-user-btn" data-id="{{$blocked_users->blocked_user->id}}">@lang('setting.unblock')</button>
    </div>
</li>
@endforeach