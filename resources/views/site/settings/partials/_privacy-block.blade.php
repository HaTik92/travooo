<form method="post" action="{{url_with_locale('settings/privacy')}}" id="setting_privacy_form" autocomplete="off">
    <div class="post-side-top setting-side-block">
        <h3 class="side-ttl">@lang('setting.privacy_settings')</h3>
    </div>
    <div class="post-content-inner setting-privacy-form">
        <div class="post-form-wrapper">
            <div class="row mb-4">
                <label for=""
                       class="col-sm-3 col-form-label">@lang('setting.who_can_see_my')</label>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="row col-sm-6 pr-0">
                            <label for=""
                                   class="col-md-5 col-lg-6 col-form-label label-inside">@lang('setting.followings')</label>
                            <div class="col-md-7 col-lg-6 pl-0">
                                <div class="flex-custom">
                                    <select name="see_my_following" class="custom-select">
                                        <option value="0" @if(@$user->privacy->see_my_following==0) selected @endif>@lang('setting.everyone')</option>
                                        <option value="1" @if(@$user->privacy->see_my_following==1) selected @endif>@lang('setting.friends')</option>
                                        <option value="2" @if(@$user->privacy->see_my_following==2) selected @endif>@lang('setting.followers')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <label for=""
                       class="col-sm-3 col-form-label">@lang('setting.who_can')</label>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="row col-sm-6 pr-0">
                            <label for=""
                                   class="col-md-5 col-lg-6 col-form-label label-inside">@lang('setting.message_me')</label>
                            <div class="col-md-7 col-lg-6 pl-0">
                                <div class="flex-custom">
                                    <select name="message_me" class="custom-select">
                                        <option value="0" @if(@$user->privacy->message_me==0) selected @endif>@lang('setting.everyone')</option>
                                        <option value="1" @if(@$user->privacy->message_me==1) selected @endif>@lang('setting.friends')</option>
                                        <option value="2" @if(@$user->privacy->message_me==2) selected @endif>@lang('setting.followers')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row col-sm-6 pr-0">
                            <label for=""
                                   class="col-md-5 col-lg-6 col-form-label label-inside text-center">@lang('setting.follow_me')</label>
                            <div class="col-md-7 col-lg-6 pl-0">
                                <div class="flex-custom">
                                    <select name="follow_me" class="custom-select">
                                        <option value="0" @if(@$user->privacy->follow_me==0) selected @endif>@lang('setting.everyone')</option>
                                        <option value="1" @if(@$user->privacy->follow_me==1) selected @endif>@lang('setting.only_people_that_I_am_following')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="row col-sm-6 pr-0">
                            <label for=""
                                   class="col-md-6 col-lg-6 col-form-label label-inside">@lang('setting.find_me')</label>
                            <div class="col-md-6 col-lg-6 pl-0">
                                <div class="flex-custom">
                                    <select name="find_me" id="" class="custom-select">
                                        <option value="0" @if(@$user->privacy->find_me==0) selected @endif>@lang('setting.everyone')</option>
                                        <option value="1" @if(@$user->privacy->find_me==1) selected @endif>@lang('setting.friends')</option>
                                        <option value="2" @if(@$user->privacy->find_me==2) selected @endif>@lang('setting.followers')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-txt">
                        <p>@lang('setting.if_you_use_this_tool_content_on_your_timeline_you')</p>
                        <p>@lang('setting.you_also_have_the_option_to_individually_change')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="post-foot-btn">
        <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')</button>
    </div>
</form>