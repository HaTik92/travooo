<div class="payment-container">
    <div class="post-side-top setting-side-block">
        <h3 class="side-ttl" dir="auto">Security settings</h3>
    </div>
    <div class="payment-container__content">
        <div class='flext-container margin--helper'>         
            <div class="flex--left">Payment Mehtods</div>
            <div class='flex--right'>
                <div class="payment-card">
                    <div class="payment-card__header">
                        <i class="fas fa-check-circle"></i>
                        Primary payment method
                    </div>
                    <div class="payment-card__content">
                        <div>
                            <div>Card type & number:</div>
                            <div>**** **** **** 9709</div>
                        </div>
                        <div>
                            <div>Expiration date:</div>
                            <div>07 / 2019</div>
                        </div>
                        <div>
                            <div>Cardholder name:</div>
                            <div>Willie Rosenberg</div>
                        </div>
                    </div>
                    <div class="payment-card__footer">
                    <a class='btn--edit' href="#">Edit</a>
                    <a class='btn--remove' href="#">Remove</a>
                    </div>
                </div>
                <div class="payment-add">
                    <a class='btn--edit' href="#">+ Add Payment Method</a>
                </div>
                <div class="payment-txt">
                    Learn more about <a href="#">Travoo Payments</a>
                </div>
            </div>
        </div>
        <div class='flext-container'>
            <div class="flex--left">Currency</div>
            <div class='flex--right'>
                <div class="dropdown">
                    <button id="dLabel" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Us Dollars            
                    </button>
                    <i class="fas fa-caret-down"></i>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                    <li>Option 1</li>
                    <li>Option 2</li>
                    </ul>
                </div>
                <div class="payment-txt">
                    You will be charged for payments on Travooo in US Dollars.
                </div>
            </div>
        </div>  
    </div>
    <div class='payment-content-footer'>
        <a class="btn" href="#">Save</a>
    </div>
</div>

<script>
    $('.dropdown-menu li').on('click', function() {
  var getValue = $(this).text();
  $('.dropdown-select').text(getValue);
});
</script>
