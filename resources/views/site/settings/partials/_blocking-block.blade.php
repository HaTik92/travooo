<form method="post" action="{{url_with_locale('settings/blocking')}}" id="setting_blocking_form" autocomplete="off">
    <div class="post-side-top setting-side-block">
        <h3 class="side-ttl">@lang('setting.manage_blocking')</h3>
    </div>
    <div class="post-content-inner">
        @if($errors->any())
        <div class="alert alert-danger profile-alert">
            {!! implode('', $errors->all('<div>:message</div>')) !!}
        </div>
        @endif
        <div class="post-form-wrapper">
            <div class="row">
                <label for=""
                       class="col-sm-3 col-form-label">@lang('setting.block_users')</label>
                <div class="col-sm-9">
                    <div class="form-txt pt-13">
                        <p>@lang('setting.once_you_block_someone_that_person_can_no_longer')</p>
                    </div>
                    <div class="flex-custom">
                        <div class="search-blocking-block">
                            <select class="flex-input setting-input" id="search_blocking_user"  placeholder="@lang('setting.type_a_name')" style="width:100%">
                            </select>
                            <a href="javascript:;" type="submit" class="add-block-user-link">@lang('setting.block')</a>
                            <input type="hidden" name="blocked_user" id="blocked_user">
                        </div>
                    </div>
                    <label id="search_blocking_user-error" class="error d-none" for="search_blocking_user"></label>
                    <div class="form-txt">
                        <p>@lang('setting.search_and_block_users')</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <label for=""
                       class="col-sm-3 col-form-label">@lang('setting.blocked_users')</label>
                <div class="col-sm-9">
                    <div class="form-txt pt-13">
                        <p>@lang('setting.a_list_of_all_the_blocked_users_in_your_account_you')</p>
                    </div>
                    <div class="blocked-users-block">
                        @if(count(@$user->setting_users_block) > 0)
                        @php $setting_block_users = $user->setting_users_block()->orderBy('created_at', 'DESC')->take(10)->get(); @endphp
                        <ul class="blocked-user-list">
                            @include('site.settings.partials._blocked-list-block', ['setting_block_users'=>$setting_block_users])
                             <li class="blocked-user-item blocked-loader {{count(@$user->setting_users_block)<10?'d-none':''}}">
                                <input type="hidden" id="pagenum" value="1">
                                <div class="blocked-user-info-wrap">
                                    <div class="b-loader-image-wrap animation"></div>
                                    <div class="blocked-user-info-title">
                                        <p class="b-loader-text animation"></p>
                                        <p class="b-loader-subtext animation"></p>
                                    </div>
                                </div>
                                <div>
                                    <div class="b-loader-button animation"></div>
                                </div>
                            </li>
                            </ul>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</form>