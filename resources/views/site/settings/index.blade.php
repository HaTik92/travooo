@php
$title = 'Travooo - settings page';
@endphp

@extends('site.layouts.site')

@section('base')
@include('site/layouts/_header')
<div class="content-wrap settings--mobile">
    <div class="dashboard-wrap mobile-hide">
        <div class="top-bar">
            <div class="container-fluid">
                <div class="top-bar-inner">
                    <div class="ttl">@lang('setting.settings')</div>
                    <ul class="nav nav-tabs bar-list" role="tablist">
                        @if(Auth::user()->is_approved == 1)
                        <li class="nav-item">
                            <a class="nav-link " id="insightsPage"  href="{{url('dashboard')}}" role="tab">Insights</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="affilationPage" href="{{url('dashboard')}}" role="tab">Afillation</a>
                        </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('profile')}}" role="tab">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('chat')}}" role="tab">Inbox @if($chat_count>0)<span class="counter">{{$chat_count}}</span>@endif</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('notification')}}" role="tab">Notifications @if($notification_count)<span class="counter">{{$notification_count}}</span>@endif</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{url('settings')}}" role="tab">Settings</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="custom-row setting-layout">
            <!-- MAIN-CONTENT -->
            <div class="left-side-tab-layer" id="sidebarSetting">
                <h3 class="left-side-ttl setting-left-menu-title">@lang('setting.settings')</h3>
                <ul class="left-side-list">
                    <li class="active" data-tab="account"><a href="#">@lang('setting.account_info')</a></li>
                    <li class="" data-tab="privacy"><a href="#">@lang('setting.privacy')</a></li>
                    <li class="" data-tab="security"><a href="#">@lang('setting.security')</a></li>
                    <li class="" data-tab="payments"><a href="#">@lang('setting.payments')</a></li>
                    <li class="" data-tab="social"><a href="#">@lang('setting.social_profiles')</a></li>
                    <li class="" data-tab="blocking"><a href="#">@lang('setting.blocking')</a></li>
                </ul>
            </div>
            <div class="main-content-layer">
                <div class="post-block post-setting-block" data-content="account">
                    @include('site.settings.partials._account-info-block')
                </div>

                <div class="post-block post-setting-block" data-content="privacy" style="display:none;">
                    @include('site.settings.partials._privacy-block')
                </div>

                <div class="post-block post-setting-block" data-content="security" style="display:none;">
                    @include('site.settings.partials._security-block')
                </div>

                <div class="post-block post-setting-block" data-content="payments" style="display:none;">
                    @include('site.settings.partials._payments-block')
                </div>

                <div class="post-block post-setting-block" data-content="social" style="display:none;">
                    @include('site.settings.partials._social-block')
                </div>
                <div class="post-block post-setting-block" data-content="blocking" style="display:none;">
                    @include('site.settings.partials._blocking-block')
                </div>
            </div>
        </div>
        <div class="setting-footer-block">
            <ul class="setting-footer-list">
                <li><a href="{{url('/')}}">@lang('setting.about')</a></li>
                <li><a href="{{url('/')}}">@lang('setting.careers')</a></li>
                <li><a href="{{url('/')}}">@lang('setting.sitemap')</a></li>
                <li><a href="{{route('page.privacy_policy')}}">@lang('setting.privacy')</a></li>
                <li><a href="{{route('page.terms_of_service')}}">@lang('setting.terms')</a></li>
                <li><a href="{{url('/')}}">@lang('setting.contact')</a></li>
                <li><a href="{{route('help.index')}}">@lang('setting.help_center')</a></li>
            </ul>
            <p class="setting-copyright">Travooo © {{date('Y')}}</p>
        </div>
    </div>

</div>
@endsection

@include('site.settings._scripts')

