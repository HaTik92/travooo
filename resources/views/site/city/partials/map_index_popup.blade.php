<div class="modal fade white-style" data-backdrop="false" id="mapIndexPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-1140" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-map-index">
                <div class="post-map-block">
                    <div class="post-map-inner">
                        <img src="./assets/image/map-index-image.jpg" alt="map">
                        <div class="post-top-map-tabs horizontal-view">
                            <div class="map-tab" data-tab="pollution2">
                                <div class="tab-icon"><i class="trav-about-icon"></i></div>
                                <div class="tab-txt">@lang('region.pollution')</div>
                            </div>
                            <div class="map-tab" data-tab="costOfLiving2">
                                <div class="tab-icon"><i class="trav-about-icon"></i></div>
                                <div class="tab-txt">@lang('region.cost_of_living')</div>
                            </div>
                            <div class="map-tab" data-tab="crimeRate2">
                                <div class="tab-icon"><i class="trav-about-icon"></i></div>
                                <div class="tab-txt">@lang('region.crime_rate')</div>
                            </div>
                            <div class="map-tab current" data-tab="qualityOfLife2">
                                <div class="tab-icon"><i class="trav-about-icon"></i></div>
                                <div class="tab-txt">@lang('region.quality_of_life')</div>
                            </div>
                        </div>
                        <div class="post-map-safe-block">
                            <span>@lang('region.less_safe')</span>
                            <div class="safe-progress"></div>
                            <span>@lang('region.safeer')</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>