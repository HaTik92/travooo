<div class="modal fade white-style" data-backdrop="false" id="storiesModePopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
			<i class="trav-close-icon"></i>
		</button>
		<div class="story-bottom-slider-wrapper">
			<div class="bottom-slider" id="storiesModeSlider">
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Suzanne</a>
							<ul class="slider-info-list">
                                <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                                <li>@lang('trip.spent') <b>$ 310</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Lonnie Nelson</a>
							<ul class="slider-info-list">
                                <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                                <li>@lang('trip.spent') <b>$ 310</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Barbara Hafer</a>
							<ul class="slider-info-list">
                                <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                                <li>@lang('trip.spent') <b>$ 310</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">David</a>
							<ul class="slider-info-list">
                                <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                                <li>@lang('trip.spent') <b>$ 310</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Suzanne</a>
							<ul class="slider-info-list">
                                <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                                <li>@lang('trip.spent') <b>$ 310</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Barbara Hafer</a>
							<ul class="slider-info-list">
                                <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                                <li>@lang('trip.spent') <b>$ 310</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">David</a>
							<ul class="slider-info-list">
                                <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                                <li>@lang('trip.spent') <b>$ 310</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Suzanne</a>
							<ul class="slider-info-list">
                                <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                                <li>@lang('trip.spent') <b>$ 310</b></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-dialog modal-custom-style modal-bottom-slider modal-1070" role="document">
			<div class="modal-custom-block">
				<div class="post-block post-story-block">
					<div class="modal-story-top">
						<div class="top-txt">
							<a class="post-link" href="#">11 days in United States.</a>
                            <span>@lang('region.check_the_full_trip_plan_dd')</span>
							<a class="post-link" href="#">My First Trip 3 Countries</a>
							<i class="trav-open-video-in-window"></i>
						</div>
					</div>
					<div class="story-header-map">
						<img src="http://placehold.it/1070x365?text=map+layer" alt="">
						<div class="caption-inner">
							<div class="post-map-info-caption map-black">
								<div class="map-avatar">
									<img src="http://placehold.it/25x25" alt="ava">
								</div>
								<div class="map-label-txt">Stayed <b>10 days</b> in <b>United States</b>, spent arround <b>$810</b> and have visited <b>41 places</b> in <b>New York</b>, <b>Dallas</b> and <b>+4 more city</b></div>
							</div>
						</div>
					</div>
					<div class="story-place-layer">
						<div class="story-place">
							<div class="place-name">
								<ul class="place-name-list">
									<li class="current">New Your City</li>
									<li>Washington</li>
									<li>Indianapolis</li>
									<li>Nashville</li>
									<li>Dallas</li>
								</ul>
							</div>
							<div class="story-arrow-block">
								<span class="arrow"></span>
                                <div class="block-tag">@lang('time.day_value', ['value' => '2'])</div>
								<h2 class="info-ttl">Tuesday 19 Jun 2017 at 10:00 am</h2>
							</div>
						</div>
					</div>
					<div class="story-content-block">
						<div class="place-over">
							<div class="place-over_top">
								<div class="place-over-txt">
									<b>Stutue of Liberty</b>
									<i class="trav-set-location-icon"></i>
									<span class="place-txt">Sculpture in New York, United States</span>
								</div>
								<div class="follow-btn-wrap">
                                    <button type="button"
                                            class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
								</div>
							</div>
							<div class="trip-photos">
								<div class="photo-wrap">
									<img src="http://placehold.it/356x365" alt="photo">
								</div>
								<div class="photo-wrap">
									<img src="http://placehold.it/356x365/f5f5f5" alt="photo">
								</div>
								<div class="photo-wrap">
									<img src="http://placehold.it/356x365" alt="photo">
								</div>
                                <a href="#" class="see-more-link">@lang('place.see_more_photos')</a>
							</div>
						</div>
						<div class="story-content-inner">
							<div class="story-date-layer">
								<div class="follow-avatar">
									<img src="http://placehold.it/68x68" alt="avatar">
								</div>
								<div class="follow-by">
                                    <span>@lang('other.by')</span>
									<b>Suzanne</b>
								</div>
								<div class="s-date-line">
                                    <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
								</div>
								<div class="s-date-line">
									<div class="s-date-badge">Mon, 18 Jun 2017</div>
								</div>
								<div class="s-date-line">
									<div class="s-date-badge">@ 10:00am</div>
								</div>
							</div>
							<div class="info-block">
								<div class="info-top-layer">
									<ul class="info-list">
										<li>
                                            <i class="trav-clock-icon"></i><span class="info-txt"> @lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></span>
										</li>
										<li>
                                            <i class="trav-budget-icon"></i><span class="info-txt"> @lang('trip.spent') <b>$310</b></span>
										</li>
										<li>
                                            <i class="trav-weather-cloud-icon"></i><span class="info-txt"> @lang('place.weather') <b>12° C</b></span>
										</li>
									</ul>
								</div>
								<div class="info-content">
                                    <span class="good-label">@lang('place.good_for')</span>
									<ul class="block-tag-list">
                                        <li><span class="block-tag">@lang('place.food')</span></li>
                                        <li><span class="block-tag">@lang('place.relaxation')</span></li>
                                        <li><span class="block-tag">@lang('place.shopping')</span></li>
                                        <li><span class="block-tag">@lang('place.photography')</span></li>
									</ul>
								</div>
								<div class="info-bottom-layer">
									<p>“It was an amazing experience, I’m happy I selected this place to be in my trip”</p>
								</div>
							</div>
							<div class="info-main-layer">
								<p>Cras ac tortor orci a liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est, tempor volutpat urna sempernec.</p>
								<h3>Headline Here</h3>
								<p>Consectetur adipiscing elit nam consequat ligula non mi rutrum suscipit cras ac tortor orci.</p>
								<p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est, tempor volutpat urna sempernec.</p>
								<ul class="img-list">
									<li><img src="http://placehold.it/700x460" alt="image"></li>
								</ul>
								<p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est, tempor volutpat urna sempernec.</p>
								<div class="flex-image-wrap fh-460">
									<div class="image-column col-66">
										<div class="image-inner" style="background-image:url(http://placehold.it/475x460)"></div>
									</div>
									<div class="image-column col-33">
										<div class="image-inner img-h-66" style="background-image:url(http://placehold.it/225x310)"></div>
										<div class="image-inner img-h-33" style="background-image:url(http://placehold.it/225x150)"></div>
									</div>
								</div>
							</div>
							<div class="post-tips">
								<div class="post-tips-top">
                                    <h4 class="post-tips_ttl">@lang('trip.tips_about_place', ['place' => 'Dubai Airport'])</h4>
                                    <a href="#" class="see-all-tip">@lang('trip.see_all_tips_count', ['count' => 7])</a>
								</div>
								<div class="post-tips_block-wrap">
									<div class="post-tips_block">
										<div class="tips-content">
											<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet, architecto vel magnam accusamus nemo odio unde autem, provident eos itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
										</div>
										<div class="tips-footer">
											<div class="tip-by-info">
												<div class="tip-by-img">
													<img src="http://placehold.it/25x25" alt="">
												</div>
												<div class="tip-by-txt">
                                                    @lang('other.by') <span class="tag blue">Suzanne</span> <span
                                                            class="date-info">5 days ago</span>
												</div>
											</div>
											<ul class="tip-right-info-list">
												<li class="round">
													<i class="trav-bookmark-icon"></i>
												</li>
												<li class="round">
													<i class="trav-flag-icon"></i>
												</li>
												<li class="round blue">
													<i class="trav-chevron-up"></i>
												</li>
												<li class="count">
													<span>6</span>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="foot-reaction">
								<ul class="post-round-info-list">
									<li>
										<div class="map-reaction">
											<span class="dropdown">
												<i class="trav-heart-fill-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
												<div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
													<ul class="dropdown-emoji-list">
														<li>
															<a href="#" class="emoji-link">
																<div class="emoji-wrap like-icon"></div>
																<span class="emoji-name">@lang('chat.emoticons.like')</span>
															</a>
														</li>
														<li>
															<a href="#" class="emoji-link">
																<div class="emoji-wrap haha-icon"></div>
																<span class="emoji-name">@lang('chat.emoticons.haha')</span>
															</a>
														</li>
														<li>
															<a href="#" class="emoji-link">
																<div class="emoji-wrap wow-icon"></div>
																<span class="emoji-name">@lang('chat.emoticons.wow')</span>
															</a>
														</li>
														<li>
															<a href="#" class="emoji-link">
																<div class="emoji-wrap sad-icon"></div>
																<span class="emoji-name">@lang('chat.emoticons.sad')</span>
															</a>
														</li>
														<li>
															<a href="#" class="emoji-link">
																<div class="emoji-wrap angry-icon"></div>
																<span class="emoji-name">@lang('chat.emoticons.angry')</span>
															</a>
														</li>
													</ul>
												</div>
											</span>
											<span class="count">6</span>
										</div>
									</li>
									<li>
										<div class="round-icon">
											<i class="trav-comment-icon"></i>
										</div>
										<span class="count">3</span>
									</li>
									<li>
										<div class="round-icon">
											<i class="trav-light-icon"></i>
										</div>
										<span class="count">37</span>
									</li>
									<li>
										<a href="#" class="info-link">
											<div class="round-icon">
												<i class="trav-share-fill-icon"></i>
											</div>
                                            <span>@lang('profile.share')</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="post-comment-layer-wrap">
							<div class="post-comment-layer">
								<div class="post-comment-top-info">
									<ul class="comment-filter">
                                        <li class="active">@lang('other.top')</li>
                                        <li>@lang('other.new')</li>
									</ul>
									<div class="comm-count-info">
										3 / 20
									</div>
								</div>
								<div class="post-comment-wrapper">
									<div class="post-comment-row">
										<div class="post-com-avatar-wrap">
											<img src="http://placehold.it/45x45" alt="">
										</div>
										<div class="post-comment-text">
											<div class="post-com-name-layer">
												<a href="#" class="comment-name">Katherin</a>
												<a href="#" class="comment-nickname">@katherin</a>
											</div>
											<div class="comment-txt">
												<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
											</div>
											<div class="comment-bottom-info">
												<div class="com-reaction">
													<img src="./assets/image/icon-smile.png" alt="">
													<span>21</span>
												</div>
												<div class="com-time">6 hours ago</div>
											</div>
										</div>
									</div>
									<div class="post-comment-row">
										<div class="post-com-avatar-wrap">
											<img src="http://placehold.it/45x45" alt="">
										</div>
										<div class="post-comment-text">
											<div class="post-com-name-layer">
												<a href="#" class="comment-name">Amine</a>
												<a href="#" class="comment-nickname">@ak0117</a>
											</div>
											<div class="comment-txt">
												<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
											</div>
											<div class="comment-bottom-info">
												<div class="com-reaction">
													<img src="./assets/image/icon-like.png" alt="">
													<span>19</span>
												</div>
												<div class="com-time">6 hours ago</div>
											</div>
										</div>
									</div>
									<div class="post-comment-row">
										<div class="post-com-avatar-wrap">
											<img src="http://placehold.it/45x45" alt="">
										</div>
										<div class="post-comment-text">
											<div class="post-com-name-layer">
												<a href="#" class="comment-name">Katherin</a>
												<a href="#" class="comment-nickname">@katherin</a>
											</div>
											<div class="comment-txt">
												<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
											</div>
											<div class="comment-bottom-info">
												<div class="com-reaction">
													<img src="./assets/image/icon-smile.png" alt="">
													<span>15</span>
												</div>
												<div class="com-time">6 hours ago</div>
											</div>
										</div>
									</div>
                                    <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
								</div>
								<div class="post-add-comment-block">
									<div class="avatar-wrap">
										<img src="http://placehold.it/45x45" alt="">
									</div>
									<div class="post-add-com-input">
                                        <input type="text" placeholder="@lang('comment.write_a_comment')">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>