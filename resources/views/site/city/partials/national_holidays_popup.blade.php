<div class="modal fade white-style" data-backdrop="false" id="nationalHolidayPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
      <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-660" role="document">
      <div class="modal-custom-block">
        <div class="post-block post-modal-language-style">
          <div class="post-lang-top">
            <div class="flag-wrap">
              <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($city->iso_code)}}.png" alt="flag" class="flag-img" style="width:99px;height:53px;">
            </div>
            <div class="top-txt">
              <h3 class="ttl">{{$city->trans[0]->title}}</h3>
              <p>@lang('region.country_in_name', ['name' => @$city->region->trans[0]->title])</p>
            </div>
          </div>
          <div class="post-lang-inner">
            <div class="lang-row">
              <div class="lang-label" data-toggle="collapse" data-target="#national_holiday" aria-expanded="false" aria-controls="national_holiday">
                  @lang('region.national_holidays') <span>({{count($city->holidays)}})</span>
              </div>
              <div class="collapse lang-collapse" id="national_holiday">
                <div class="lang-list">
                    @foreach($city->holidays AS $holiday)
                  <div class="lang-string">
                    <div class="name">{{$holiday->trans[0]->title}}</div>
                    <div class="info">4 @lang('month.short.july')</div>
                  </div>
                    @endforeach
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>