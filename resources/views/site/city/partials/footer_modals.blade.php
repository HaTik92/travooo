<!-- gallery popup -->
	@include('site/city/partials/gallery_popup')
        
	<!-- place stories popup -->
	@include('site/city/partials/place_stories_popup')

	<!-- map index popup -->
	@include('site/city/partials/map_index_popup')

	<!-- languages popup -->
	@include('site/city/partials/languages_popup')

	<!-- national holidays popup -->
@include('site/city/partials/national_holidays_popup')

<!-- comments popup -->
@include('site/city/partials/comments_popup')

	<!-- trip plans popup -->
	@include('site/city/partials/trip_plans_popup')

	<!-- your friends popup -->
	@include('site/city/partials/your_friends_popup')