<div class="modal fade white-style" data-backdrop="false" id="commentsPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
      <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-650" role="document">
      <div class="modal-custom-block">
        <div class="post-block post-modal-comment">
          <div class="post-comment-main">
            <div class="discussion-block">
                <span class="disc-ttl">@lang('comment.comments')</span>
              <ul class="disc-ava-list">
                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
              </ul>
            </div>
          </div>
          <div class="post-comment-layer">
            <div class="post-comment-top-info">
              <ul class="comment-filter">
                  <li class="active">@lang('other.top')</li>
                <li>New</li>
              </ul>
              <div class="comm-count-info">
                3 / 20
              </div>
            </div>
            <div class="post-comment-wrapper">
              <div class="post-comment-row">
                <div class="post-com-avatar-wrap">
                  <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-comment-text">
                  <div class="post-com-name-layer">
                    <a href="#" class="comment-name">Katherin</a>
                    <a href="#" class="comment-nickname">@katherin</a>
                  </div>
                  <div class="comment-txt">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                  </div>
                  <div class="comment-bottom-info">
                    <div class="com-reaction">
                      <img src="./assets/image/icon-smile.png" alt="">
                      <span>21</span>
                    </div>
                    <div class="com-time">6 hours ago</div>
                  </div>
                </div>
              </div>
              <div class="post-comment-row">
                <div class="post-com-avatar-wrap">
                  <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-comment-text">
                  <div class="post-com-name-layer">
                    <a href="#" class="comment-name">Amine</a>
                    <a href="#" class="comment-nickname">@ak0117</a>
                  </div>
                  <div class="comment-txt">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                  </div>
                  <div class="comment-bottom-info">
                    <div class="com-reaction">
                      <img src="./assets/image/icon-like.png" alt="">
                      <span>19</span>
                    </div>
                    <div class="com-time">6 hours ago</div>
                  </div>
                </div>
              </div>
              <div class="post-comment-row">
                <div class="post-com-avatar-wrap">
                  <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-comment-text">
                  <div class="post-com-name-layer">
                    <a href="#" class="comment-name">Katherin</a>
                    <a href="#" class="comment-nickname">@katherin</a>
                  </div>
                  <div class="comment-txt">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                  </div>
                  <div class="comment-bottom-info">
                    <div class="com-reaction">
                      <img src="./assets/image/icon-smile.png" alt="">
                      <span>15</span>
                    </div>
                    <div class="com-time">6 hours ago</div>
                  </div>
                </div>
              </div>
                <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
            </div>
            <div class="post-add-comment-block post-add-foot grey-style">
              <div class="avatar-wrap">
                <img src="http://placehold.it/45x45" alt="">
              </div>
              <div class="post-add-com-input">
                  <input type="text" placeholder="@lang('comment.write_a_comment')">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>