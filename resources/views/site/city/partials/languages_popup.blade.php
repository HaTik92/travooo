<div class="modal fade white-style" data-backdrop="false" id="languageCurrTime" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-660" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-language-style">
                <div class="post-lang-top">
                    <div class="top-txt">
                        <h3 class="ttl">{{$city->trans[0]->title}}</h3>
                        <p>@lang('region.country_in_name', ['name' => @$city->region->trans[0]->title])</p>
                    </div>
                </div>
                <div class="post-lang-inner">
                    <div class="lang-row">
                        <div class="lang-label" data-toggle="collapse" data-target="#languagesSpoken"
                             aria-expanded="false" aria-controls="languagesSpoken">
                            @lang('region.languages_spoken') <span>({{@count($city->languages)}})</span>
                        </div>
                        <div class="collapse lang-collapse" id="languagesSpoken">
                            <div class="lang-list">
                                @foreach($city->languages AS $lang)
                                    <div class="lang-string">
                                        <div class="name">{{$lang->title}}</div>
                                        <div class="info"></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="lang-row">
                        <div class="lang-label" data-toggle="collapse" data-target="#curencies" aria-expanded="false"
                             aria-controls="curencies">
                            @lang('region.currencies') <span>({{@count($city->currencies)}})</span>
                        </div>
                        <div class="collapse lang-collapse" id="curencies">
                            <div class="lang-list">
                                @foreach($city->currencies AS $curr)
                                    <div class="lang-string">
                                        <div class="name">{{$curr->trans[0]->title}}</div>
                                        <div class="info"></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="lang-row">
                        <div class="lang-label" data-toggle="collapse" data-target="#timings" aria-expanded="false"
                             aria-controls="timings">
                            @lang('region.timings') <span>({{@count($city->timezone)}})</span>
                        </div>
                        <div class="collapse lang-collapse" id="timings">
                            <div class="lang-list">
                                @if($city->timezone)
                                    @foreach($city->timezone AS $tz)
                                        <div class="lang-string">
                                            <div class="name">{{$tz}}</div>
                                            <div class="info"></div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="lang-row">
                        <div class="lang-label" data-toggle="collapse" data-target="#religion" aria-expanded="false"
                             aria-controls="religion">
                            @lang('region.religions') <span>({{@count($city->religions)}})</span>
                        </div>
                        <div class="collapse lang-collapse" id="religion">
                            <div class="lang-list">
                                @foreach($city->religions AS $rel)
                                    <div class="lang-string">
                                        <div class="name">{{$rel->trans[0]->title}}</div>
                                        <div class="info"></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="lang-row">
                        <?php
                        if ($city->trans[0]->transportation != '') {
                            $transportation = @explode(",", $city->trans[0]->transportation);
                        }
                        ?>
                        <div class="lang-label" data-toggle="collapse" data-target="#transportaionMethod"
                             aria-expanded="false" aria-controls="transportaionMethod">
                            @lang('region.transportation_methods') <span>({{@count($transportation)}})</span>
                        </div>
                        <div class="collapse lang-collapse" id="transportaionMethod">
                            <div class="lang-list">
                                @if(isset($transportation))
                                    @foreach($transportation AS $transp)
                                        <div class="lang-string">
                                            <div class="name">{{$transp}}</div>
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="lang-row">
                        <?php
                        if ($city->trans[0]->speed_limit != '') {
                            $speed_limits = explode("\n", $city->trans[0]->speed_limit);
                        }
                        ?>
                        <div class="lang-label" data-toggle="collapse" data-target="#speedLimit" aria-expanded="false"
                             aria-controls="speedLimit">
                            @lang('region.speed_limit') <span>({{@count($speed_limits)}})</span>
                        </div>
                        <div class="collapse lang-collapse" id="speedLimit">
                            <div class="lang-list">
                                @if(isset($speed_limits))
                                    @foreach($speed_limits AS $slimit)
                                        @if(trim($slimit) != '')
                                            <div class="lang-string">
                                                <div class="name">{{$slimit}}</div>
                                                <div class="info"></div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>