<div class="modal fade white-style" data-backdrop="false" id="goingPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-700" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-going-block post-mobile-full">
                <div class="post-top-topic topic-tabs">
                    <div class="topic-item active">@lang('place.your_friends') <span
                                class="count">{{count($checkins)}}</span></div>
                    <div class="topic-item">@lang('place.experts_users') <span class="count">{{count($checkins)}}</span>
                    </div>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar">
                    @foreach($checkins AS $checkin)
                        <div class="people-row">
                            <div class="main-info-layer">
                                <div class="img-wrap">
                                    <a href='{{url('profile/'.$checkin->post_checkin->post->author->id)}}'>
                                        <img class="ava"
                                             src="{{check_profile_picture($checkin->post_checkin->post->author->profile_picture)}}"
                                             alt="ava" style="width:50px;height:50px;">
                                    </a>
                                </div>
                                <div class="txt-block">
                                    <div class="name">
                                        <a href='{{url('profile/'.$checkin->post_checkin->post->author->id)}}'>
                                            {{$checkin->post_checkin->post->author->name}}
                                        </a>
                                    </div>
                                    <div class="info-line">
                                        <div class="info-part">@lang('place.checked_in') -
                                            <b>{{$checkin->post_checkin->post->created_at}}</b></div>
                                    </div>
                                </div>
                            </div>
                            <div class="button-wrap">
                                <button class="btn btn-light-grey btn-bordered"
                                        onclick='window.location.replace("https://www.travooo.com/chat/new/{{$checkin->post_checkin->post->author->id}}");'>
                                    @lang('place.message')
                                </button>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>