<div class="top-board-wrap">
    <div class="post-block post-board">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.photos')</h3>
            <div class="side-right-control">
                <div class="side-count">{{count($city->getMedias)}}</div>
            </div>
        </div>
        <div class="post-side-inner" id="countryPopupTrigger" style="cursor:pointer;">
            <div class="board-photo-list">
                @for ($i = 0; $i < 3; $i++)
                @if ($i==0)
                <div class="board-image full-image">
                    <img src="https://s3.amazonaws.com/travooo-images2/th230/{{@$city->getMedias[$i]->url}}" alt="photo" style="width:185px;height:115px;">
                </div>
                @else
                <div class="board-image half-image">
                    <img src="https://s3.amazonaws.com/travooo-images2/th230/{{@$city->getMedias[$i]->url}}" alt="photo" style="width:84px;height:89px;">
                </div>
                @endif
                @endfor
            </div>
        </div>
    </div>
    <div class="post-block post-board">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.trip_plans')</h3>
            <div class="side-right-control">
                <div class="side-count">{{$plans_count}}</div>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="board-photo-list">
                @for ($i = 0; $i < 3; $i++)
                @if(isset($plans[$i]) && is_object($plans[$i]))
                @if ($i==0)
                <div class="board-image full-image">
                    <img src="{{@get_plan_map($plans[$i], 185, 115)}}" alt="photo" style="width:185px;height:115px;">
                </div>
                @else
                <div class="board-image half-image">
                    <img src="{{@get_plan_map($plans[$i], 84, 89)}}" alt="photo" style="width:84px;height:89px;">
                </div>
                @endif
                @endif
                @endfor
            </div>
        </div>
    </div>
    <div class="post-block post-board">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.best_places')</h3>
            <div class="side-right-control">
                <div class="side-count">{{@count($city->places)}}</div>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="board-photo-list">
                @for ($i = 0; $i < 3; $i++)
                @if ($i==0)
                <div class="board-image full-image">
                    <img src="https://s3.amazonaws.com/travooo-images2/th230/{{@$city->places[$i]->medias[0]->url}}" alt="photo" style="width:185px;height:115px;">
                </div>
                @else
                <div class="board-image half-image">
                    <img src="https://s3.amazonaws.com/travooo-images2/th230/{{@$city->places[$i]->medias[0]->url}}" alt="photo" style="width:84px;height:89px;">
                </div>
                @endif
                @endfor
            </div>
        </div>
    </div>
    @if(count($events))
    <div class="post-block post-board">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.nearby_events')</h3>
            <div class="side-right-control">
              
            </div>
        </div>
        <div class="post-side-inner" id="nearByEventsTrigger" style="cursor:pointer;">
            <div class="board-photo-list">
                <?php
                $ev = array_slice($events, 0, 3);
                ?>
                @foreach($ev AS $event)
               
                    <?php
                    $rand = rand(1,10);
                    ?>
                
                    @if ($loop->first)
                    <div class="board-image full-image">
                        <img src="{{asset('assets2/image/events/'.$event->category.'/'.$rand.'.jpg')}}" alt="photo" style="width:185px;height:115px;">
                    </div>
                    @else
                    <div class="board-image half-image">
                        <img src="{{asset('assets2/image/events/'.$event->category.'/'.$rand.'.jpg')}}" alt="photo" style="width:84px;height:89px;">
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    @endif
    
</div>