<div class="top-banner-wrap">
    <div class="post-block post-flage-detail">
        <div class="post-event-block">
            <div class="post-event-image" style="height:239px;background-image:url(https://s3.amazonaws.com/travooo-images2/th1100/{{@$city->getMedias[0]->url}})">
                <div class="follow-block-info" style="padding-top: 190px;padding-left: 25px;">
                                <ul class="foot-avatar-list" data-toggle="modal" data-target="#expertsPopup">
                                    @foreach($city->experts AS $cexp)
                                        <li><img class="small-ava" src="{{check_profile_picture($cexp->user->profile_picture)}}" alt="ava" style="width:29px;height:29px;"></li>
                                    @endforeach
                                </ul>
                                <span>{{count($city->experts)}} Experts in {{@$city->trans[0]->title}} </span>
                            </div>
            </div>
            <div class="post-event-main">
                <div class="post-placement-info">
                    <span class="hash"></span>
                    <h2 class="place-name">{{@$city->trans[0]->title}}</h2>
                    <div class="event-info-layer">
                        <span class="placement-place">@lang('region.city_in') <a href="{{url('country/'.@$city->country->id)}}">{{@$city->country->trans[0]->title}}</a></span>
                        <div id="talkingAbout">

                        </div>
                    </div>
                    <div class="event-main-content">
                        <p>{{strip_tags(@$city->trans[0]->description)}}</p>
                    </div>
                </div>
            </div>
            <div class="follow-bottom-link">
                <span id="follow_botton">

                </span>
                <a href="#"
                   class="follow-link">@lang('profile.count_followers', ['count' => @count($city->followers)])</a>
            </div>
        </div>
    </div>
</div>
<div class="top-now-in-city-block">
    <div class="city-inner" id="nowInCountry">

    </div>
</div>