@php
    $title = 'Travooo - Country';
@endphp

@extends('site.city.template.city')


@section('before_content')
    <div class="top-banner-wrap">
        <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$city->getMedias[0]->url}}"
             alt="banner" style="width:1070px;height:215px;">
        <div class="banner-cover-txt">
            <div class="banner-name">
                <div class="banner-ttl">{{$city->trans[0]->title}}</div>
                <div class="sub-ttl">@lang('region.country_in_name', ['name' => @$city->region->trans[0]->title])</div>
            </div>
            <div class="banner-btn" id="follow_botton2">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                    <i class="trav-comment-plus-icon"></i>
                    <span>@lang('region.buttons.follow')</span>
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="post-block post-tab-block post-tip-tab">
        <div class="post-tab-inner post-tip-tab" id="postTabBlock">
            <div class="tab-item active-tab" data-tab='etiquette'>
                @lang('region.etiquette')
            </div>
            <div class="tab-item" data-tab='restrictions'>
                @lang('region.restrictions')
            </div>
        </div>
    </div>


    <div class="post-block post-tips-list-block" data-content='etiquette'>

        <div class="post-list-inner">
            <?php
            $etiquettes = $city->trans[0]->etiquette != '' ? $etiquettes = $city->trans[0]->etiquette : $etiquettes = $city->country->trans[0]->etiquette;
            $etiquettes = @explode("\n", $etiquettes);

            ?>
            @foreach($etiquettes AS $et)
                <?php
                if(trim($et) != '') {
                $et = @explode(":", $et);
                ?>
                <div class="post-tip-row tip-txt">
                    <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>{{@$et[0]}}</b>
                    </div>
                    <div class="row-txt"><span>{{@$et[1]}}</span></div>
                </div>
                <?php
                }
                ?>
            @endforeach

        </div>
    </div>

    <div class="post-block post-tips-list-block" data-content='restrictions' style='display:none;'>
        <div class="post-list-inner">
            <?php
            $city->trans[0]->economy != '' ? $restrictions = $city->trans[0]->economy : $restrictions = $city->country->trans[0]->economy;
            $restrictions = @explode("\n", $restrictions);
            //dd($restrictions);

            ?>
            @foreach($restrictions AS $rest)
                <?php
                if(trim($rest) != '') {
                $rest = @explode(":", $rest);
                ?>
                <div class="post-tip-row tip-txt">
                    <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>{{@$rest[0]}}</b>
                    </div>
                    <div class="row-txt"><span>{{@$rest[1]}}</span></div>
                </div>
                <?php
                }
                ?>
            @endforeach

        </div>
    </div>

@endsection


@section('sidebar')
    <div class="post-block post-side-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.suggest')</h3>
        </div>
        <div class="post-suggest-inner">
            <div class="suggest-icon-wrap">
                <i class="trav-etiquette-icon"></i>
            </div>
            <div class="suggest-txt">
                <p>@lang('region.have_something_to_say_about_etiquette_in_new_york')</p>
            </div>
            <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                    data-toggle="modal" data-target="#etiquettePopup">
                @lang('region.buttons.suggest_one')
            </button>
        </div>
    </div>
    @parent
@endsection