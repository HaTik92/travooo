@php
    $title = 'Travooo - Country';
@endphp

@extends('site.city.template.city')

@section('before_content')
    <div class="top-banner-wrap">
        <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$city->getMedias[0]->url}}"
             alt="banner" style="width:1070px;height:215px;">
        <div class="banner-cover-txt">
            <div class="banner-name">
                <div class="banner-ttl">{{$city->trans[0]->title}}</div>
                <div class="sub-ttl">@lang('region.country_in_name', ['name' => @$city->region->trans[0]->title])</div>
            </div>
            <div class="banner-btn" id="follow_botton2">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                    <i class="trav-comment-plus-icon"></i>
                    <span>@lang('region.follow') </span>
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="post-block post-tips-list-block">
        <div class="post-top-layer">
            <div class="top-left">
                <h3 class="post-tip-ttl">@lang('region.risk_of_disease')</h3>
            </div>
        </div>
        <div class="post-list-inner">
            <?php
            $city->trans[0]->health_notes != '' ? $risks = $city->trans[0]->health_notes : $city->country->trans[0]->health_notes;
            $risks = @explode("\n", $risks);
            ?>

            @foreach($risks AS $risk)
                @if(trim($risk)!='')
                    <?php
                    $risk = @explode(":", $risk);
                    ?>
                    @if(@$risk[0]!="Number")
                        <div class="post-tip-row tip-txt">
                            <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                <b>{{@$risk[0]}}</b></div>
                            <div class="row-txt"><span>{{@$risk[1]}}</span></div>
                        </div>
                    @endif
                @endif
            @endforeach
        </div>
    </div>

@endsection

@section('sidebar')
    <div class="post-block post-side-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.suggest')</h3>
        </div>
        <div class="post-suggest-inner">
            <div class="suggest-icon-wrap">
                <i class="trav-health-hand-icon"></i>
            </div>
            <div class="suggest-txt">
                <p>@lang('region.have_something_to_say_about_health_in_new_york')</p>
            </div>
            <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                    data-toggle="modal" data-target="#healthNotePopup">
                @lang('region.buttons.add_your_advice')
            </button>
        </div>
    </div>
    @parent
@endsection