<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Country</title>
</head>
<body>
<div class="main-wrapper">
    @include('site.layouts.header')
    <div class="content-wrap">
        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>
        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url('home')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('city.nav.home')</span>
                            <!--<span class="counter">5</span>-->
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id)}}">
                            <i class="trav-about-icon"></i>
                            <span>@lang('city.nav.about')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/packing-tips')}}">
                            <i class="trav-trips-tips-icon"></i>
                            <span>@lang('city.nav.about')</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('city/'.$city->id.'/weather')}}">
                            <i class="trav-weather-cloud-icon"></i>
                            <span>@lang('city.nav.weather')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/etiquette')}}">
                            <i class="trav-etiquette-icon"></i>
                            <span>@lang('city.nav.etiquette')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/health')}}">
                            <i class="trav-health-hand-icon"></i>
                            <span>@lang('city.nav.health')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/visa-requirements')}}">
                            <i class="trav-visa-embassy-icon"></i>
                            <span>@lang('city.nav.visa')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/when-to-go')}}">
                            <i class="trav-flights-icon"></i>
                            <span>@lang('city.nav.when_to_go')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/caution')}}">
                            <i class="trav-caution-icon"></i>
                            <span>@lang('city.nav.caution')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('discussions?do=city&id=').$city->id}}">
                            <i class="trav-ask-icon"></i>
                            <span>@lang('city.nav.forum')</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="top-banner-wrap">
                <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/{{@$city->getMedias[0]->url}}"
                     alt="banner" style="width:1070px;height:215px;">
                <div class="banner-cover-txt">
                    <div class="banner-name">
                        <div class="banner-ttl">{{$city->trans[0]->title}}</div>
                        <div class="sub-ttl">@lang('city.strings.country_in') {{@$city->region->trans[0]->title}}</div>
                    </div>
                    <div class="banner-btn" id="follow_botton2">
                        <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                            <i class="trav-comment-plus-icon"></i>
                            <span>@lang('city.buttons.follow')</span>
                            <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <div class="post-block post-weather-block">
                        <div class="general-weather-view">
                            <div class="day-block-wrap">
                                <div class="weather-info">
                                    <div class="temperature">
                                        <span>{{$current_weather[0]->Temperature->Metric->Value}}</span><sup>o</sup><span
                                                class="temp"><b>C</b> / F</span></div>
                                    <div class="weather-day">
                                        <div class="day-name">{{weatherDate($current_weather[0]->LocalObservationDateTime)}}</div>
                                    </div>
                                </div>
                                <div class="icon-wrap">
                                    <img src="{{url('assets2/image/weather/'.$current_weather[0]->WeatherIcon.'-s.png')}}"
                                         alt="{{$current_weather[0]->WeatherText}}"
                                         title="{{$current_weather[0]->WeatherText}}" class="weather-icon">
                                </div>
                            </div>
                            <div class="weather-txt-block">
                                <div class="weather-title">
                                    {{$current_weather[0]->WeatherText}} in <span>{{$city->trans[0]->title}}</span>
                                </div>
                                <p>It's {{$current_weather[0]->WeatherText}} right now in {{$city->trans[0]->title}}.
                                    The forecast today shows a low
                                    of {{$current_weather[0]->Temperature->Metric->Value}}<sup>o</sup></p>
                            </div>
                        </div>

                        <div class="weather-block-wrapper">
                            <div class="weather-carousel-control">
                                <a href="#" class="slide-prev"><i class="trav-angle-left"></i></a>
                                <a href="#" class="slide-next"><i class="trav-angle-right"></i></a>
                            </div>
                            <div class="weather-hour-carousel" id="weatherHourCarousel">
                                @foreach($country_weather AS $cw)
                                    <div class="hour-block">
                                        <div class="time-block">{{weatherTime($cw->DateTime)}}</div>
                                        <div class="weather-image">
                                            <img src="{{url('assets2/image/weather/'.$cw->WeatherIcon.'-s.png')}}"
                                                 alt="{{$cw->IconPhrase}}" title="{{$cw->IconPhrase}}"
                                                 class="weather-icon">
                                        </div>
                                        <div class="temp-block">{{$cw->Temperature->Value}}<sup>o</sup></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="day-weather-list">
                            @foreach($daily_weather AS $dw)
                                <div class="day-weather-block">
                                    <div class="day-block-wrap">
                                        <div class="weather-icon">
                                            <img src="{{url('assets2/image/weather/'.$dw->Day->Icon.'-s.png')}}"
                                                 alt="{{$dw->Day->IconPhrase}}" title="{{$dw->Day->IconPhrase}}"
                                                 class="weather-icon">
                                            <span class="temperature">{{$dw->Temperature->Maximum->Value}}<sup>o</sup></span>
                                        </div>
                                        <div class="weather-day">
                                            <div class="day-name">{{date("l", $dw->EpochDate)}}</div>
                                            <div class="weather-desc">{{$dw->Day->IconPhrase}}</div>
                                        </div>
                                    </div>
                                    <div class="btn-wrap">
                                        <button type="button"
                                                class="btn btn-light-grey btn-bordered">@lang('city.buttons.plan_a_trip')
                                        </button>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        <div class="post-block post-side-block">
                            <div class="post-side-top">
                                <h3 class="side-ttl">@lang('city.strings.new_york_today')</h3>
                            </div>
                            <div class="post-side-image-inner">
                                <ul class="post-image-list">
                                    <li>
                                        <img src="http://placehold.it/112x119" alt="">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/112x119" alt="">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/112x119" alt="">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/112x119" alt="">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/112x119" alt="">
                                    </li>
                                    <li class="add-photo-link">
                                        <div class="icon-wrap">
                                            <i class="fa fa-plus"></i>
                                        </div>
                                        <span>@lang('city.strings.add_photo')</span>
                                    </li>
                                </ul>
                            </div>
                        </div>


                        <div class="share-page-block">
                            <div class="share-page-inner">
                                <div class="share-txt">@lang('city.strings.share_this_page')</div>
                                <ul class="share-list">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-code"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="#">@lang('navs.frontend.privacy')</a></li>
                                <li><a href="#">@lang('navs.frontend.terms')</a></li>
                                <li><a href="#">@lang('navs.frontend.advertising')</a></li>
                                <li><a href="#">@lang('navs.frontend.cookies')</a></li>
                                <li><a href="#">@lang('navs.frontend.more')</a></li>
                            </ul>
                            <p class="copyright">@lang('strings.frontend.copy')</p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
@include('site/city/partials/footer_modals')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>

@include('site/city/partials/footer_scripts')
@include('site/layouts/footer')
</body>
</html>