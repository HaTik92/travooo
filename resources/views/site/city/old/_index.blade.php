<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Country</title>
</head>
<body>
<div class="main-wrapper">
    @include('site.layouts.header')
    <div class="content-wrap">
        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>
        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url('home')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('city.nav.home')</span>
                            <!--<span class="counter">5</span>-->
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('city/'.$city->id)}}">
                            <i class="trav-about-icon"></i>
                            <span>@lang('city.nav.about')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/packing-tips')}}">
                            <i class="trav-trips-tips-icon"></i>
                            <span>@lang('city.nav.packing_tips')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/weather')}}">
                            <i class="trav-weather-cloud-icon"></i>
                            <span>@lang('city.nav.weather')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/etiquette')}}">
                            <i class="trav-etiquette-icon"></i>
                            <span>@lang('city.nav.etiquette')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/health')}}">
                            <i class="trav-health-hand-icon"></i>
                            <span>@lang('city.nav.health')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/visa-requirements')}}">
                            <i class="trav-visa-embassy-icon"></i>
                            <span>@lang('city.nav.visa')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/when-to-go')}}">
                            <i class="trav-flights-icon"></i>
                            <span>@lang('city.nav.when_to_go')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/caution')}}">
                            <i class="trav-caution-icon"></i>
                            <span>@lang('city.nav.caution')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('discussions?do=city&id=').$city->id}}">
                            <i class="trav-ask-icon"></i>
                            <span>@lang('city.nav.forum')</span>
                        </a>
                    </li>
                </ul>
            </div>

            @include('site/city/partials/top_banner')
            @include('site/city/partials/top_blocks')
            <div class="top-banner-wrap">
                <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/{{@$city->getMedias[0]->url}}"
                     alt="banner" style="width:1070px;height:215px;">
                <div class="banner-cover-txt">
                    <div class="banner-name">
                        <div class="banner-ttl">{{@$city->trans[0]->title}}</div>
                        <div class="sub-ttl">@lang('city.strings.city_in') {{@$city->country->trans[0]->title}}</div>
                    </div>
                    <div class="banner-btn" id="follow_botton2">
                        <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                            <i class="trav-comment-plus-icon"></i>
                            <span>@lang('city.buttons.follow')</span>
                            <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <div class="post-block post-country-block">
                        <div class="post-side-top">
                            <h3 class="side-ttl">@lang('city.strings.about_the_city')</h3>
                        </div>

                    </div>

                    <div class="post-block post-tips-list-block">
                        <div class="post-list-inner">
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.nationality')</b></div>
                                <div class="row-txt">
                                    <span>{{@($city->trans[0]->nationality!="") ? $city->trans[0]->nationality : $city->country->trans[0]->nationality}}</span>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.languages_spoken')</b>
                                </div>
                                <div class="row-txt">
                                    <span>{{@($city->languages[0]->title!="") ? $city->languages[0]->title : $city->country->languages[0]->title}}</span>
                                    <a href="#" class="more-link" data-toggle="modal"
                                       data-target="#languageCurrTime">+{{@count($city->languages)-1}} @lang('buttons.general.more')</a>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.currencies')</b></div>
                                <div class="row-txt">
                                    <span>{{@$city->currencies[0]->trans[0]->title!="" ? $city->currencies[0]->trans[0]->title : $city->country->currencies[0]->trans[0]->title}}</span>
                                    <span class="currency"></span>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.timings')</b></div>
                                <div class="row-txt">
                                    <span>{{@$city->country->timezone[0]->time_zone_name}}</span>
                                    <a href="#" class="more-link" data-toggle="modal"
                                       data-target="#languageCurrTime">+{{@count($city->country->timezone)-1}} @lang('buttons.general.more')</a>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.religions')</b></div>
                                <div class="row-txt">
                                    <span>{{@$city->religions[0]->trans[0]->title!="" ? $city->religions[0]->trans[0]->title : $city->country->religions[0]->trans[0]->title}}</span>
                                    <a href="#" class="more-link" data-toggle="modal"
                                       data-target="#languageCurrTime">+{{@count($city->religions)-1}} @lang('buttons.general.more')</a>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.phone_code')</b></div>
                                <div class="row-txt">
                                    <span>{{@$city->code!="" ? $city->code : $city->country->code}}</span>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.units_of_measurement')</b>
                                </div>
                                <div class="row-txt">
                                    <span>{{@$city->trans[0]->metrics!="" ? $city->trans[0]->metrics : $city->country->trans[0]->metrics}}</span>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.working_days')</b></div>
                                <div class="row-txt">
                                    <span>{{@$city->trans[0]->working_days!="" ? $city->trans[0]->working_days : $city->country->trans[0]->working_days}}</span>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.transportation_methods')</b></div>
                                <div class="row-txt">
                                    <?php
                                    if (@$city->trans[0]->transportation != '') {
                                        $transportation = explode(",", $city->trans[0]->transportation);
                                    } else {
                                        $transportation = explode(",", $city->country->trans[0]->transportation);
                                    }
                                    ?>
                                    <span>{{@$transportation[0]}}</span>
                                    <a href="#" class="more-link" data-toggle="modal"
                                       data-target="#languageCurrTime">+{{@count($transportation)-1}} @lang('buttons.general.more')</a>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.speed_limit')</b></div>
                                <div class="row-txt">
                                    <?php
                                    if ($city->trans[0]->speed_limit != '') {
                                        $speed_limit = explode("\n", $city->trans[0]->speed_limit);
                                    } else {
                                        $speed_limit = explode("\n", $city->country->trans[0]->speed_limit);
                                    }
                                    ?>
                                    <span>{{@$speed_limit[0]}}</span>
                                    <a href="#" class="more-link" data-toggle="modal"
                                       data-target="#languageCurrTime">+{{@count($speed_limit)-1}} @lang('buttons.general.more')</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="post-block post-tips-list-block">
                        <div class="post-top-layer">
                            <div class="top-left">
                                <h3 class="post-tip-ttl_lg">@lang('city.strings.emergency_number')
                                    &nbsp;<span>{{@count($city->emergency) ? count($city->emergency) : count($city->country->emergency)}}</span>
                                </h3>
                            </div>
                        </div>
                        <div class="post-list-inner">
                            @if(count($city->emergency))
                                @foreach($city->emergency AS $emergency)
                                    <div class="post-tip-row">
                                        <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                            <b>{{@strip_tags($emergency->trans[0]->description)}}</b></div>
                                        <div class="row-txt">
                                            <span>{{@$emergency->trans[0]->title}}</span>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                @foreach($city->country->emergency AS $emergency)
                                    <div class="post-tip-row">
                                        <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                            <b>{{@strip_tags($emergency->trans[0]->description)}}</b></div>
                                        <div class="row-txt">
                                            <span>{{@$emergency->trans[0]->title}}</span>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>


                    <div class="post-block">
                        <div class="post-side-top">
                            <h3 class="side-ttl">@lang('city.strings.top_places') <span class="count">50</span></h3>
                            <div class="side-right-control">
                                <a href="#" class="see-more-link">@lang('city.strings.see_all')</a>
                                <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                                <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
                            </div>
                        </div>
                        <div class="post-side-inner">
                            <div class="post-slide-wrap slide-hide-right-margin">
                                <ul id="placesSlider" class="post-slider">
                                    @foreach($city->places()->take(50)->get() AS $place)
                                        <li>
                                            <img src="https://s3.amazonaws.com/travooo-images2/{{@$place->medias[0]->url}}"
                                                 alt="" style="width:230px;height:300px">
                                            <div class="post-slider-caption transparent-caption">
                                                <p class="post-slide-name"><a href="{{url('place/'.$place->id)}}"
                                                                              style="color:white;">{{@$place->trans[0]->title}}</a>
                                                </p>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>

                    {{--
<div class="post-block">
    <div class="post-side-top">
        <h3 class="side-ttl">National holidays <span class="count">{{@count($city->country->holidays)}}</span></h3>
        <div class="side-right-control">
            <a href="#" class="see-more-link" data-toggle="modal" data-target="#nationalHolidayPopup">See all</a>
            <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
            <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
        </div>
    </div>
    <div class="post-side-inner">
        <div class="post-slide-wrap slide-hide-right-margin">
            <ul id="nationalHoliday" class="post-slider">
                                                    @foreach($city->country->holidays AS $holiday)
                <li class="post-card">
                    <div class="image-wrap">
                        <img src="http://placehold.it/274x234" alt="">
                        <div class="card-cover">
                            <span class="date">4</span>
                            <span class="month">Jul</span>
                        </div>
                    </div>
                    <div class="post-slider-caption">
                        <p class="post-card-name">{{@$holiday->trans[0]->title}}</p>
                        <div class="post-footer-info">
                            <div class="post-foot-block">
                                <!--<ul class="foot-avatar-list">
                                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                </ul>
                                <span>20 Talking about this</span>
                                                                                        -->
                            </div>
                        </div>
                    </div>
                </li>
                                                    @endforeach
            </ul>
        </div>
    </div>
</div>
                    --}}

                    <div class="post-block post-country-block">
                        <div class="post-side-top">
                            <h3 class="side-ttl">@lang('city.strings.accessibility')</h3>
                        </div>
                        <div class="post-country-inner post-full-inner">
                            <div class="post-map-block">
                                <div class="post-map-inner">
                                    <img src="https://maps.googleapis.com/maps/api/staticmap?center={{$city->lat}},{{$city->lng}}&zoom=5&size=595x360&key=AIzaSyBqTXNqPdQlFIV5QNvYQeDrJ5vH0y9_D-M"
                                         alt="Map of {{@$city->trans[0]->title}}">
                                    <div class="post-top-map-tabs">
                                        <div class="map-tab current" data-tab="atmTxt">
                                            <div class="tab-icon"><i class="trav-atms-icon"></i></div>
                                            <div class="tab-txt">@lang('city.strings.atms')</div>
                                        </div>
                                        <div class="map-tab" data-tab="wifiTxt">
                                            <div class="tab-icon"><i class="trav-internet-icon"></i></div>
                                            <div class="tab-txt">@lang('city.strings.internet')</div>
                                        </div>
                                        <div class="map-tab" data-tab="socketTxt">
                                            <div class="tab-icon"><i class="trav-sockets-icon"></i></div>
                                            <div class="tab-txt">@lang('city.strings.sockets')</div>
                                        </div>
                                        <div class="map-tab" data-tab="hotelTxt">
                                            <div class="tab-icon"><i class="trav-hotels-icon"></i></div>
                                            <div class="tab-txt">@lang('city.strings.hotels')</div>
                                        </div>
                                    </div>
                                    <div class="post-map-area">
                                        <div class="area-txt" id="atmTxt">
                                            <h5 class="ttl">{{number_format(@count($city->atms))}}</h5>
                                            <p>@lang('city.strings.atm_machines')</p>
                                        </div>
                                        <div class="area-txt" id="wifiTxt" style="display:none">
                                            <h5 class="ttl">{{@$city->trans[0]->internet!='' ? $city->trans[0]->internet : $city->country->trans[0]->internet}}</h5>
                                            <p>@lang('city.strings.internet')</p>
                                        </div>
                                        <div class="area-txt" id="socketTxt" style="display:none">
                                            <h5 class="ttl">{{@$city->trans[0]->sockets!="" ? $city->trans[0]->sockets : $city->country->trans[0]->sockets}}</h5>
                                            <p>@lang('city.strings.sockets')</p>
                                        </div>
                                        <div class="area-txt" id="hotelTxt" style="display:none">
                                            <h5 class="ttl">{{number_format(@count($city->hotels))}}</h5>
                                            <p>@lang('city.strings.hotels')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        @if($city->id==185 OR $city->id==184 OR $city->id==322 OR $city->id==490)
                            <?php
                            $tomorrow = date("m/d/Y", time() + (24 * 60 * 60));
                            $after_tomorrow = date("m/d/Y", time() + (2 * 24 * 60 * 60));
                            ?>
                            <div class="post-block post-side-block">
                                <div class="post-side-top">
                                    <h3 class="side-ttl">@lang('city.strings.stay_at') {{$city->transsingle->title}}</h3>
                                </div>
                                <div class="post-suggest-inner">
                                    <div class="suggest-txt">
                                        <p>@lang('city.strings.interested_in_staying_at') {{$city->transsingle->title}}
                                            ?</p>
                                    </div>
                                    @if($city->id==185)
                                        <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                                                onclick='window.location.href = "{{url('book/search-hotel?city_select=MAD&daterange='.$tomorrow.'%20-%20'.$after_tomorrow)}}";'>
                                            @lang('city.strings.book_a_hotel_now')
                                        </button>
                                    @elseif($city->id==184)
                                        <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                                                onclick='window.location.href = "{{url('book/search-hotel?city_select=BCN&daterange='.$tomorrow.'%20-%20'.$after_tomorrow)}}";'>
                                            @lang('city.strings.book_a_hotel_now')
                                        </button>
                                    @elseif($city->id==322)
                                        <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                                                onclick='window.location.href = "{{url('book/search-hotel?city_select=UKY&daterange='.$tomorrow.'%20-%20'.$after_tomorrow)}}";'>
                                            Book a hotel NOW!
                                        </button>
                                    @elseif($city->id==490)
                                        <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                                                onclick='window.location.href = "{{url('book/search-hotel?city_select=LAX&daterange='.$tomorrow.'%20-%20'.$after_tomorrow)}}";'>
                                            @lang('city.strings.book_a_hotel_now')
                                        </button>
                                    @endif
                                </div>
                            </div>
                        @endif


                        <div class="post-block post-country-block">
                            <div class="post-side-top">
                                <h3 class="side-ttl">@lang('city.strings.main_airports') <span
                                            class="count">{{@count($airports)}}</span></h3>
                                <div class="side-right-control">
                                    <a href="#" class="see-more-link lg">@lang('city.strings.see_all')</a>
                                </div>
                            </div>
                            <div class="post-country-inner">
                                <div class="dest-list">
                                    @foreach($airports AS $airport)
                                        <div class="dest-item">
                                            <div class="dest-location"><i
                                                        class="trav-set-location-icon"></i>&nbsp; {{$airport->city->transsingle->title}}
                                            </div>
                                            <div class="dest-link-wrap">
                                                <a href="{{url('place/'.$airport->id)}}"
                                                   class="dest-link">{{$airport->transsingle->title}}</a>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                        <div class="share-page-block">
                            <div class="share-page-inner">
                                <div class="share-txt">@lang('city.strings.share_this_page')</div>
                                <ul class="share-list">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-code"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="#">@lang('navs.frontend.privacy')</a></li>
                                <li><a href="#">@lang('navs.frontend.terms')</a></li>
                                <li><a href="#">@lang('navs.frontend.advertising')</a></li>
                                <li><a href="#">@lang('navs.frontend.cookies')</a></li>
                                <li><a href="#">@lang('navs.frontend.more')</a></li>
                            </ul>
                            <p class="copyright">@lang('strings.frontend.copy')</p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
@include('site/city/partials/footer_modals')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>

@include('site/city/partials/footer_scripts')
@include('site/layouts/footer')

<script>
    // nearby events
    $('#nearByEventsTrigger').on('click', function () {

        let $lg = $(this).lightGallery({
            dynamic: true,
            dynamicEl: [
                    @foreach($events AS $ev)
                    <?php
                    $rand = rand(1, 10);
                    ?>
                {
                    "src": "{{asset('assets2/image/events/'.$ev->category.'/'.$rand.'.jpg')}}",
                    'thumb': "{{asset('assets2/image/events/'.$ev->category.'/'.$rand.'.jpg')}}",
                    'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>@lang("other.back")</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="sub-post-info">

                <div class="follow-btn-wrap">

                </div>
              </div>
            </div>
            <div class="map-preview">
              <img src="https://maps.googleapis.com/maps/api/staticmap?center={{$ev->location[1]}},{{$ev->location[0]}}&markers=color:red%7Clabel:C%7C{{$ev->location[1]}},{{$ev->location[0]}}&zoom=13&size=150x150&key=AIzaSyBqTXNqPdQlFIV5QNvYQeDrJ5vH0y9_D-M" alt="map">
            </div>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-avatar-wrap">

                    </div>
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <p class="dest-name">{{$ev->title}}</p>
                        <p class="dest-place">
                                {{ucfirst($ev->category)}}
                        <br />@lang("time.from_to", ['from' => $ev->start, 'to' => $ev->end])</p>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="post-comment-layer">

                  <div class="post-comment-wrapper" style="color: black;padding: 20px;line-height: 1.3;">
                    {!!nl2br($ev->description)!!}
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>`
                },

                @endforeach
            ],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            $.each(galleryItem, function (i, val) {
                // itemArr.push(val);
            });
            $.each(galleryThumb, function (i, val) {
                // thumbArr.push(val);
                // let startCnt = `<div class="thumb-txt"><i class="trav-flag-icon"></i> start</div>`;
                // let startCntEmpty = `<div class="thumb-txt">&nbsp;</div>`;
                // let placetxt = 'rabar-sale airport'
                // let placeName = `<div class="thumb-txt">${placetxt}</div>`;
                // if(i == 0){
                //   $(val).addClass('place-thumb');
                //   $(val).append(placeName).prepend(startCnt);
                // }
                // if(i == 2){
                //   $(val).addClass('place-thumb');
                //   $(val).append(placeName).prepend(startCntEmpty);
                // }
            });
        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 500);
        }

        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            })
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
        });
    });
</script>

</body>
</html>