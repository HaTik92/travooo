<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Country</title>
</head>
<body>
<div class="main-wrapper">
    @include('site.layouts.header')
    <div class="content-wrap">
        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>
        <div class="container-fluid">

            <div class="top-banner-wrap">
                <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/{{@$city->getMedias[0]->url}}"
                     alt="banner" style="width:1070px;height:215px;">
                <div class="banner-cover-txt">
                    <div class="banner-name">
                        <div class="banner-ttl">{{$city->trans[0]->title}}</div>
                        <div class="sub-ttl">@lang('city.strings.country_in') {{@$city->region->trans[0]->title}}</div>
                    </div>
                    <div class="banner-btn" id="follow_botton2">
                        <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                            <i class="trav-comment-plus-icon"></i>
                            <span>@lang('city.buttons.follow')</span>
                            <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <div class="post-block post-tab-block post-tip-tab">
                        <div class="post-tab-inner post-tip-tab" id="postTabBlock">
                            <div class="tab-item active-tab" data-tab="dangers">
                                @lang('city.strings.potential_dangers')
                            </div>
                            <div class="tab-item" data-tab="indexes">
                                @lang('city.strings.indexes') (<a href="#" class="tip-link" data-toggle="modal"
                                                                  data-target="#mapIndexPopup"
                                                                  style='color:#4080ff'>@lang('city.strings.map_index')</a>)
                            </div>
                        </div>
                    </div>

                    <div class="post-block post-tips-list-block" data-content="dangers">
                        <div class="post-list-inner">
                            <?php
                            $city->trans[0]->potential_dangers != '' ? $dangers = $city->trans[0]->potential_dangers : $dangers = $city->country->trans[0]->potential_dangers;
                            $dangers = @explode("\n", $dangers);
                            ?>

                            @foreach($dangers AS $danger)
                                @if(trim($danger)!='')
                                    <?php
                                    $danger = @explode(":", $danger);
                                    ?>
                                    <div class="post-tip-row tip-txt">
                                        <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                            <b>{{@$danger[0]}}</b></div>
                                        <div class="row-txt"><span>{{@$danger[1]}}</span></div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="post-block post-tips-list-block" data-content="indexes" style="display:none;">
                        <div class="post-list-inner">
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.pollution')</b></div>
                                <div class="row-txt">
                                    <div class="index-slider-wrap">
                                        <div class="counter">
                                            <span id="current">{{round($city->trans[0]->pollution_index)}}</span>&nbsp;/&nbsp;<span
                                                    id="total">200</span>
                                        </div>
                                        <div id="sliderPollution" class="slider"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.cost_of_living')</b></div>
                                <div class="row-txt">
                                    <div class="index-slider-wrap">
                                        <div class="counter">
                                            <span id="currentCost">{{round($city->trans[0]->cost_of_living)}}</span>&nbsp;/&nbsp;<span
                                                    id="totalCost">200</span>
                                        </div>
                                        <div id="costOfLiving" class="slider"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.crime_rate')</b></div>
                                <div class="row-txt">
                                    <div class="index-slider-wrap">
                                        <div class="counter">
                                            <span id="currentRate">{{round($city->trans[0]->geo_stats)}}</span>&nbsp;/&nbsp;<span
                                                    id="totalRate">200</span>
                                        </div>
                                        <div id="crimeRate" class="slider"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-tip-row">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.quality_of_life')</b>
                                </div>
                                <div class="row-txt">
                                    <div class="index-slider-wrap">
                                        <div class="counter">
                                            <span id="currentQuolity">{{round($city->trans[0]->demographics)}}</span>&nbsp;/&nbsp;<span
                                                    id="totalQuolity">200</span>
                                        </div>
                                        <div id="qualityOfLife" class="slider"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">


                        <div class="post-block post-side-block">
                            <div class="post-side-top">
                                <h3 class="side-ttl">@lang('city.strings.suggest')</h3>
                            </div>
                            <div class="post-suggest-inner">
                                <div class="suggest-icon-wrap">
                                    <i class="trav-caution-icon"></i>
                                </div>
                                <div class="suggest-txt">
                                    <p>@lang('city.strings.have_something_to_say_about_caution_in_new_york')</p>
                                </div>
                                <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                                        data-toggle="modal" data-target="#cautionPopup">
                                    @lang('city.buttons.let_us_know')
                                </button>
                            </div>
                        </div>


                        <div class="share-page-block">
                            <div class="share-page-inner">
                                <div class="share-txt">@lang('city.strings.share_this_page')</div>
                                <ul class="share-list">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-code"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="#">@lang('navs.frontend.privacy')</a></li>
                                <li><a href="#">@lang('navs.frontend.terms')</a></li>
                                <li><a href="#">@lang('navs.frontend.advertising')</a></li>
                                <li><a href="#">@lang('navs.frontend.cookies')</a></li>
                                <li><a href="#">@lang('navs.frontend.more')</a></li>
                            </ul>
                            <p class="copyright">@lang('strings.frontend.copy')</p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
@include('site/city/partials/footer_modals')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>

@include('site/city/partials/footer_scripts')
@include('site/layouts/footer')
</body>
</html>