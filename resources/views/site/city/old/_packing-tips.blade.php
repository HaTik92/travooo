<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Country</title>
</head>
<body>
<div class="main-wrapper">
    @include('site.layouts.header')
    <div class="content-wrap">
        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>
        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url('home')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('city.nav.home')</span>
                            <!--<span class="counter">5</span>-->
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id)}}">
                            <i class="trav-about-icon"></i>
                            <span>@lang('city.nav.about')</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('city/'.$city->id.'/packing-tips')}}">
                            <i class="trav-trips-tips-icon"></i>
                            <span>@lang('city.nav.packing_tips')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/weather')}}">
                            <i class="trav-weather-cloud-icon"></i>
                            <span>@lang('city.nav.weather')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/etiquette')}}">
                            <i class="trav-etiquette-icon"></i>
                            <span>@lang('city.nav.etiquette')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/health')}}">
                            <i class="trav-health-hand-icon"></i>
                            <span>@lang('city.nav.health')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/visa-requirements')}}">
                            <i class="trav-visa-embassy-icon"></i>
                            <span>@lang('city.nav.visa')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/when-to-go')}}">
                            <i class="trav-flights-icon"></i>
                            <span>@lang('city.nav.when_to_go')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/caution')}}">
                            <i class="trav-caution-icon"></i>
                            <span>@lang('city.nav.caution')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('discussions?do=city&id=').$city->id}}">
                            <i class="trav-ask-icon"></i>
                            <span>@lang('city.nav.forum')</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="top-banner-wrap">
                <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/{{@$city->getMedias[0]->url}}"
                     alt="banner" style="width:1070px;height:215px;">
                <div class="banner-cover-txt">
                    <div class="banner-name">
                        <div class="banner-ttl">{{$city->trans[0]->title}}</div>
                        <div class="sub-ttl">@lang('city.strings.country_in') {{@$city->region->trans[0]->title}}</div>
                    </div>
                    <div class="banner-btn" id="follow_botton2">
                        <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                            <i class="trav-comment-plus-icon"></i>
                            <span>@lang('city.buttons.follow')</span>
                            <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <div class="post-block post-tab-block post-tip-tab">
                        <div class="post-tab-inner post-tip-tab">
                            <div class="tab-item active-tab" data-tab='packing_tips'>
                                @lang('city.strings.packing_tips')
                            </div>
                            <div class="tab-item" data-tab='daily_costs'>
                                @lang('city.strings.daily_costs')
                            </div>
                        </div>
                    </div>

                    <div class="post-block post-tips-list-block" data-content="daily_costs" style="display:none;">
                        <?php
                        $dcosts = $city->trans[0]->other;
                        $dcosts = @explode("\n", $dcosts);
                        ?>
                        <div class="post-list-inner">
                            @foreach($dcosts AS $dcost)
                                @if(trim($dcost))
                                    <div class="post-tip-row">
                                        <div class="row-label">{{$dcost}}</div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>

                    <div class="post-block post-tips-list-block" data-content="packing_tips">
                        <?php
                        $city->trans[0]->planning_tips != '' ? $ptips = $city->trans[0]->planning_tips : $ptips = $city->country->trans[0]->planning_tips;
                        $ptips = @explode(":", $ptips);
                        $ptips = @explode("\n", $ptips[1]);
                        ?>
                        <div class="post-list-inner">
                            @foreach($ptips AS $ptip)
                                @if(trim($ptip)!='')
                                    <div class="post-tip-row tip-txt">
                                        <div class="row-txt"><i class="trav-about-icon"></i> {{$ptip}}</div>
                                        <div class="row-txt"><span></span></div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>

                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        <div class="post-block post-side-block">
                            <div class="post-side-inner">
                                <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                                        data-toggle="modal" data-target="#addATipPopup">
                                    <i class="trav-add-trip-icon"></i>
                                    @lang('city.buttons.add_a_tip')
                                </button>
                            </div>
                        </div>

                        <div class="post-block post-side-block">
                            <div class="post-side-inner">
                                <div class="post-map-block">
                                    <div class="post-map-inner">
                                        <img src="https://maps.googleapis.com/maps/api/staticmap?center={{$city->lat}},{{$city->lng}}&zoom=5&size=345x345&key=AIzaSyBqTXNqPdQlFIV5QNvYQeDrJ5vH0y9_D-M"
                                             alt="Map of {{$city->trans[0]->title}}">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="share-page-block">
                            <div class="share-page-inner">
                                <div class="share-txt">@lang('city.strings.share_this_page')</div>
                                <ul class="share-list">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-code"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="#">@lang('navs.frontend.privacy')</a></li>
                                <li><a href="#">@lang('navs.frontend.terms')</a></li>
                                <li><a href="#">@lang('navs.frontend.advertising')</a></li>
                                <li><a href="#">@lang('navs.frontend.cookies')</a></li>
                                <li><a href="#">@lang('navs.frontend.more')</a></li>
                            </ul>
                            <p class="copyright">@lang('strings.frontend.copy')</p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
@include('site/city/partials/footer_modals')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>

@include('site/city/partials/footer_scripts')
@include('site/layouts/footer')
</body>
</html>