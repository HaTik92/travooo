<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Country</title>
</head>
<body>
<div class="main-wrapper">
    @include('site.layouts.header')
    <div class="content-wrap">
        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>
        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url('home')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('city.nav.home')</span>
                            <!--<span class="counter">5</span>-->
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id)}}">
                            <i class="trav-about-icon"></i>
                            <span>@lang('city.nav.about')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/packing-tips')}}">
                            <i class="trav-trips-tips-icon"></i>
                            <span>@lang('city.nav.about')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/weather')}}">
                            <i class="trav-weather-cloud-icon"></i>
                            <span>@lang('city.nav.weather')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/etiquette')}}">
                            <i class="trav-etiquette-icon"></i>
                            <span>@lang('city.nav.etiquette')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/health')}}">
                            <i class="trav-health-hand-icon"></i>
                            <span>@lang('city.nav.health')</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('city/'.$city->id.'/visa-requirements')}}">
                            <i class="trav-visa-embassy-icon"></i>
                            <span>@lang('city.nav.visa')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/when-to-go')}}">
                            <i class="trav-flights-icon"></i>
                            <span>@lang('city.nav.when_to_go')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/caution')}}">
                            <i class="trav-caution-icon"></i>
                            <span>@lang('city.nav.caution')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('discussions?do=city&id=').$city->id}}">
                            <i class="trav-ask-icon"></i>
                            <span>@lang('city.nav.forum')</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="top-banner-wrap">
                <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/{{@$city->getMedias[0]->url}}"
                     alt="banner" style="width:1070px;height:215px;">
                <div class="banner-cover-txt">
                    <div class="banner-name">
                        <div class="banner-ttl">{{$city->trans[0]->title}}</div>
                        <div class="sub-ttl">@lang('region.country_in_name', ['name' => @$city->region->trans[0]->title])</div>
                    </div>
                    <div class="banner-btn" id="follow_botton2">
                        <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                            <i class="trav-comment-plus-icon"></i>
                            <span>@lang('city.strings.follow')</span>
                            <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <div class="post-block post-tips-list-block">

                        <div class="post-list-inner">
                            <div class="post-tip-row tip-txt">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.visa')</b></div>
                                <div class="row-txt"><span
                                            class="red">@lang('city.strings.required_for_moroccan_passport_holders')</span>
                                </div>
                            </div>
                            <div class="post-tip-row tip-txt">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.id')</b></div>
                                <div class="row-txt">
                                    <span>@lang('city.strings.proin_cursus_erat_at_lorem_placerat_bibendum_tincidunt')</span>
                                </div>
                            </div>
                            <div class="post-tip-row tip-txt">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.photos')</b></div>
                                <div class="row-txt">
                                    <span>@lang('city.strings.mauris_laoreet_nibh_nec_odio_porta_consectetur')</span>
                                </div>
                            </div>
                            <div class="post-tip-row tip-txt">
                                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                    <b>@lang('city.strings.good_behavior_certificate')</b></div>
                                <div class="row-txt">
                                    <span>@lang('city.strings.aenean_viverra_mi_at_varius_venenatis')</span></div>
                            </div>

                        </div>
                    </div>


                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        <div class="post-block post-embassy-block">
                            <div class="post-image-wrap">
                                <img src="http://placehold.it/383x170" alt="map">
                                <div class="post-place-image">
                                    <img src="http://placehold.it/80x80" alt="place">
                                </div>
                            </div>
                            <div class="post-place-location-info">
                                <div class="post-place-txt">
                                    <div class="post-place-row">
                                        <h3 class="place-ttl">@lang('city.strings.embassy_location')</h3>
                                    </div>
                                    <div class="post-place-row">
                                        <div class="place-txt">
                                            <address>@lang('profile.address')
                                            </address>
                                        </div>
                                    </div>
                                    <div class="post-place-row">
                                        <div class="place-label">@lang('city.strings.opening_time'):</div>
                                        <div class="place-txt"><span>8AM – 5PM</span></div>
                                    </div>
                                    <div class="post-place-row">
                                        <div class="place-label">@lang('city.strings.phone_number'):</div>
                                        <div class="place-txt"><span>05 37 63 72 00</span></div>
                                    </div>
                                    <div class="post-place-row">
                                        <div class="place-label">@lang('city.strings.website_url'):</div>
                                        <div class="place-txt"><a href="ma.usembassy.gov">ma.usembassy.gov</a></div>
                                    </div>
                                </div>
                                <div class="post-place-info-foot">
                                    <div class="location-drop-wrap">
                                        <a href="#" class="foot-link"
                                           id="locationDrop">@lang('city.strings.no_in_morocco')</a>
                                        <div class="location-block-inner">
                                            <div class="loc-search-block">
                                                <input type="text" class="" id="locSearchInput">
                                            </div>
                                            <div class="loc-result-block">
                                                <div class="res-row">@lang('city.strings.france')</div>
                                                <div class="res-row">@lang('city.strings.faroe_islands')</div>
                                                <div class="res-row">@lang('city.strings.fiji')</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="share-page-block">
                            <div class="share-page-inner">
                                <div class="share-txt">@lang('city.strings.share_this_page')</div>
                                <ul class="share-list">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-code"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="#">@lang('navs.frontend.privacy')</a></li>
                                <li><a href="#">@lang('navs.frontend.terms')</a></li>
                                <li><a href="#">@lang('navs.frontend.advertising')</a></li>
                                <li><a href="#">@lang('navs.frontend.cookies')</a></li>
                                <li><a href="#">@lang('navs.frontend.more')</a></li>
                            </ul>
                            <p class="copyright">@lang('strings.frontend.copy')</p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
@include('site/city/partials/footer_modals')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>

@include('site/city/partials/footer_scripts')
@include('site/layouts/footer')
</body>
</html>