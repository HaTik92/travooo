<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Country</title>
</head>
<body>
<div class="main-wrapper">
    @include('site.layouts.header')
    <div class="content-wrap">
        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>
        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url('home')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('city.nav.home')</span>
                            <!--<span class="counter">5</span>-->
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id)}}">
                            <i class="trav-about-icon"></i>
                            <span>@lang('city.nav.about')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/packing-tips')}}">
                            <i class="trav-trips-tips-icon"></i>
                            <span>@lang('city.nav.packing_tips')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/weather')}}">
                            <i class="trav-weather-cloud-icon"></i>
                            <span>@lang('city.nav.weather')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/etiquette')}}">
                            <i class="trav-etiquette-icon"></i>
                            <span>@lang('city.nav.etiquette')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/health')}}">
                            <i class="trav-health-hand-icon"></i>
                            <span>@lang('city.nav.health')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/visa-requirements')}}">
                            <i class="trav-visa-embassy-icon"></i>
                            <span>@lang('city.nav.visa')</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('city/'.$city->id.'/when-to-go')}}">
                            <i class="trav-flights-icon"></i>
                            <span>@lang('city.nav.when_to_go')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('city/'.$city->id.'/caution')}}">
                            <i class="trav-caution-icon"></i>
                            <span>@lang('city.nav.caution')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('discussions?do=city&id=').$city->id}}">
                            <i class="trav-ask-icon"></i>
                            <span>@lang('city.nav.forum')</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="top-banner-wrap">
                <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/{{@$city->getMedias[0]->url}}"
                     alt="banner" style="width:1070px;height:215px;">
                <div class="banner-cover-txt">
                    <div class="banner-name">
                        <div class="banner-ttl">{{$city->trans[0]->title}}</div>
                        <div class="sub-ttl">@lang('city.strings.country_in') {{@$city->region->trans[0]->title}}</div>
                    </div>
                    <div class="banner-btn" id="follow_botton2">
                        <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                            <i class="trav-comment-plus-icon"></i>
                            <span>@lang('city.buttons.follow')</span>
                            <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <div class="post-block post-tips-list-block">
                        <div class="post-top-layer">
                            <div class="top-left">
                                <h3 class="post-tip-ttl_lg">@lang('city.strings.best_time_to_go')</h3>
                            </div>
                            <div class="top-right">
                                <a href="#" class="tip-link"><i class="trav-open-video-in-window"></i>&nbsp; Flight
                                    @lang('buttons.general.search')</a>
                            </div>
                        </div>
                        <?php
                        $city->trans[0]->best_time != '' ? $best_time = $city->trans[0]->best_time : $city->country->trans[0]->best_time;
                        $best_times = @explode("\n", $best_time);
                        $months = array(
                            __('month.january'),
                            __('month.february'),
                            __('month.february'),
                            __('month.april'),
                            __('month.may'),
                            __('month.june'),
                            __('month.july'),
                            __('month.august'),
                            __('month.september'),
                            __('month.october'),
                            __('month.november'),
                            __('month.december')
                        );
                        $BT = array();

                        foreach ($best_times AS $btime) {
                            if (trim($btime) != '') {
                                $btime = @explode(":", trim($btime));
                                $bttt = @explode(",", $btime[1]);
                                foreach ($bttt AS $btt) {
                                    $BT[trim($btt)] = $btime[0];
                                }
                                //$BT[trim($btime[1])] = @explode(",", trim($btime[0]));
                                //dd($btime);
                            }

                        }
                        //var_dump($BT);
                        ?>
                        <div class="post-flight-content">
                            <div class="progress-block">
                                <ul class="color-list">
                                    <li class="low">
                                        @lang('city.strings.low_season')
                                    </li>
                                    <li class="mid">
                                        @lang('city.strings.shoulder')
                                    </li>
                                    <li class="high">
                                        @lang('city.strings.high_season')
                                    </li>
                                </ul>
                                <div class="progress-inner">
                                    @foreach($months AS $month)

                                        <?php
                                        if (@$BT[$month] == 'Low Season') {
                                            $progress = 'low';
                                            $width = 20;
                                        } elseif (@$BT[$month] == 'Shoulder') {
                                            $progress = 'mid';
                                            $width = 40;
                                        } elseif (@$BT[$month] == 'High Season') {
                                            $progress = 'high';
                                            $width = 80;
                                        }
                                        ?>


                                        <div class="progress-row">
                                            <div class="progress-label">{{@$month}}</div>
                                            <div class="progress-wrapper">
                                                <div class="progress {{@$progress}}">
                                                    <div class="progress-bar" style="width:{{@$width}}%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        <div class="post-block post-country-block">
                            <div class="post-side-top">
                                <h3 class="side-ttl">@lang('city.strings.flights_from_morocco')</h3>
                            </div>
                            <div class="post-country-inner">
                                <div class="post-txt-wrap">
                                    <a href="#" class="question-link">@lang('city.strings.not_in_morocco')</a>
                                    <p class="post-txt">@lang('city.strings.new_york_away_from_rabat_in_direct_flight_time')</p>
                                </div>
                                <div class="post-map-block">
                                    <div class="post-map-inner">
                                        <img src="http://placehold.it/345x370" alt="map">
                                        <div class="flight-icon" style="left:40%;top:40%;">
                                            <i class="trav-angle-plane-icon"></i>
                                            <div class="flight-tooltip">
                                                {{ 7 . __('time.h') . ' ' . 48 .  __('time.min')}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-block post-country-block">
                            <div class="post-side-top">
                                <h3 class="side-ttl">@lang('city.strings.main_airports') <span
                                            class="count">{{@count($airports)}}</span></h3>
                                <div class="side-right-control">
                                    <a href="#" class="see-more-link lg">@lang('city.strings.see_all')</a>
                                </div>
                            </div>
                            <div class="post-country-inner">
                                <div class="dest-list">
                                    @foreach($airports AS $airport)
                                        <div class="dest-item">
                                            <div class="dest-location"><i
                                                        class="trav-set-location-icon"></i>&nbsp; {{$airport->city->transsingle->title}}
                                            </div>
                                            <div class="dest-link-wrap">
                                                <a href="{{url('places/'.$airport->id)}}"
                                                   class="dest-link">{{$airport->transsingle->title}}</a>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                        <div class="share-page-block">
                            <div class="share-page-inner">
                                <div class="share-txt">@lang('city.strings.share_this_page')</div>
                                <ul class="share-list">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-code"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="#">@lang('navs.frontend.privacy')</a></li>
                                <li><a href="#">@lang('navs.frontend.terms')</a></li>
                                <li><a href="#">@lang('navs.frontend.advertising')</a></li>
                                <li><a href="#">@lang('navs.frontend.cookies')</a></li>
                                <li><a href="#">@lang('navs.frontend.more')</a></li>
                            </ul>
                            <p class="copyright">@lang('strings.frontend.copy')</p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
@include('site/city/partials/footer_modals')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>

@include('site/city/partials/footer_scripts')
@include('site/layouts/footer')
</body>
</html>