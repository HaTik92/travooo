@php
    $title = 'Travooo - Country';
@endphp

@extends('site.city.template.city')

@section('before_content')
    <div class="top-banner-wrap">
        <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$city->getMedias[0]->url}}"
             alt="banner" style="width:1070px;height:215px;">
        <div class="banner-cover-txt">
            <div class="banner-name">
                <div class="banner-ttl">{{$city->trans[0]->title}}</div>
                <div class="sub-ttl">@lang('region.country_in_name', ['name' => @$city->region->trans[0]->title])</div>
            </div>
            <div class="banner-btn" id="follow_botton2">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                    <i class="trav-comment-plus-icon"></i>
                    <span>@lang('region.buttons.follow')</span>
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="post-block post-tab-block post-tip-tab">
        <div class="post-tab-inner post-tip-tab" id="postTabBlock">
            <div class="tab-item active-tab" data-tab="dangers">
                @lang('region.potential_dangers')
            </div>
            <div class="tab-item" data-tab="indexes">
                @lang('region.indexes') (<a href="#" class="tip-link" data-toggle="modal"
                                            data-target="#mapIndexPopup"
                                            style='color:#4080ff'>@lang('region.map_index')</a>)
            </div>
        </div>
    </div>

    <div class="post-block post-tips-list-block" data-content="dangers">
        <div class="post-list-inner">
            <?php
            $city->trans[0]->potential_dangers != '' ? $dangers = $city->trans[0]->potential_dangers : $dangers = $city->country->trans[0]->potential_dangers;
            $dangers = @explode("\n", $dangers);
            ?>

            @foreach($dangers AS $danger)
                @if(trim($danger)!='')
                    <?php
                    $danger = @explode(":", $danger);
                    ?>
                    <div class="post-tip-row tip-txt">
                        <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                            <b>{{@$danger[0]}}</b></div>
                        <div class="row-txt"><span>{{@$danger[1]}}</span></div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>

    <div class="post-block post-tips-list-block" data-content="indexes" style="display:none;">
        <div class="post-list-inner">
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.pollution')</b>
                </div>
                <div class="row-txt">
                    <div class="index-slider-wrap">
                        <div class="counter">
                            <span id="current">{{round($city->trans[0]->pollution_index)}}</span>&nbsp;/&nbsp;<span
                                    id="total">200</span>
                        </div>
                        <div id="sliderPollution" class="slider"></div>
                    </div>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.cost_of_living')</b>
                </div>
                <div class="row-txt">
                    <div class="index-slider-wrap">
                        <div class="counter">
                            <span id="currentCost">{{round($city->trans[0]->cost_of_living)}}</span>&nbsp;/&nbsp;<span
                                    id="totalCost">200</span>
                        </div>
                        <div id="costOfLiving" class="slider"></div>
                    </div>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.crime_rate')</b>
                </div>
                <div class="row-txt">
                    <div class="index-slider-wrap">
                        <div class="counter">
                            <span id="currentRate">{{round($city->trans[0]->geo_stats)}}</span>&nbsp;/&nbsp;<span
                                    id="totalRate">200</span>
                        </div>
                        <div id="crimeRate" class="slider"></div>
                    </div>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.quality_of_life')</b>
                </div>
                <div class="row-txt">
                    <div class="index-slider-wrap">
                        <div class="counter">
                            <span id="currentQuolity">{{round($city->trans[0]->demographics)}}</span>&nbsp;/&nbsp;<span
                                    id="totalQuolity">200</span>
                        </div>
                        <div id="qualityOfLife" class="slider"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')

    <div class="post-block post-side-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.suggest')</h3>
        </div>
        <div class="post-suggest-inner">
            <div class="suggest-icon-wrap">
                <i class="trav-caution-icon"></i>
            </div>
            <div class="suggest-txt">
                <p>@lang('region.have_something_to_say_about_caution_in_new_york')</p>
            </div>
            <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                    data-toggle="modal" data-target="#cautionPopup">
                @lang('region.buttons.let_us_know')
            </button>
        </div>
    </div>

    @parent
@endsection