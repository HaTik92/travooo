@php
    $title = 'Travooo - Country';
@endphp

@extends('site.city.template.city')

@section('before_content')
    <div class="top-banner-wrap">
        <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$city->getMedias[0]->url}}"
             alt="banner" style="width:1070px;height:215px;">
        <div class="banner-cover-txt">
            <div class="banner-name">
                <div class="banner-ttl">{{$city->trans[0]->title}}</div>
                <div class="sub-ttl">@lang('region.country_in_name', ['name' => @$city->region->trans[0]->title])</div>
            </div>
            <div class="banner-btn" id="follow_botton2">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                    <i class="trav-comment-plus-icon"></i>
                    <span>@lang('region.buttons.follow')</span>
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="post-block post-tab-block post-tip-tab">
        <div class="post-tab-inner post-tip-tab">
            <div class="tab-item active-tab" data-tab='packing_tips'>
                @lang('region.packing_tips')
            </div>
            <div class="tab-item" data-tab='daily_costs'>
                @lang('region.daily_costs')
            </div>
        </div>
    </div>

    <div class="post-block post-tips-list-block" data-content="daily_costs" style="display:none;">
        <?php
        $dcosts = $city->trans[0]->other;
        $dcosts = @explode("\n", $dcosts);
        ?>
        <div class="post-list-inner">
            @foreach($dcosts AS $dcost)
                @if(trim($dcost))
                    <div class="post-tip-row">
                        <div class="row-label">{{$dcost}}</div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>

    <div class="post-block post-tips-list-block" data-content="packing_tips">
        <?php
        $city->trans[0]->planning_tips != '' ? $ptips = $city->trans[0]->planning_tips : $ptips = $city->country->trans[0]->planning_tips;
        $ptips = @explode(":", $ptips);
        $ptips = @explode("\n", $ptips[1]);
        ?>
        <div class="post-list-inner">
            @foreach($ptips AS $ptip)
                @if(trim($ptip)!='')
                    <div class="post-tip-row tip-txt">
                        <div class="row-txt"><i class="trav-about-icon"></i> {{$ptip}}</div>
                        <div class="row-txt"><span></span></div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>

@endsection

@section('sidebar')
    <div class="post-block post-side-block">
        <div class="post-side-inner">
            <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                    data-toggle="modal" data-target="#addATipPopup">
                <i class="trav-add-trip-icon"></i>
                @lang('region.buttons.add_a_tip')
            </button>
        </div>
    </div>

    <div class="post-block post-side-block">
        <div class="post-side-inner">
            <div class="post-map-block">
                <div class="post-map-inner">
                    <img src="https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{$city->lat}},{{$city->lng}}&zoom=5&size=345x345&key={{env('GOOGLE_MAPS_KEY')}}"
                         alt="Map of {{$city->trans[0]->title}}">
                </div>
            </div>
        </div>
    </div>
    @parent
@endsection