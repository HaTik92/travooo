@php
    $title = 'Travooo - Country';
@endphp

@extends('site.city.template.city')

@section('before_content')
    @include('site/city/partials/top_banner')
    @include('site/city/partials/top_blocks')
    <div class="top-banner-wrap">
        <img class="banner-city" src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$city->getMedias[0]->url}}"
             alt="banner" style="width:1070px;height:215px;">
        <div class="banner-cover-txt">
            <div class="banner-name">
                <div class="banner-ttl">{{@$city->trans[0]->title}}</div>
                <div class="sub-ttl">@lang('region.city_in') {{@$city->country->trans[0]->title}}</div>
            </div>
            <div class="banner-btn" id="follow_botton2">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                    <i class="trav-comment-plus-icon"></i>
                    <span>@lang('region.buttons.follow')</span>
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="post-block post-country-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.about_the_city')</h3>
        </div>

    </div>

    <div class="post-block post-tips-list-block">
        <div class="post-list-inner">
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.nationality')</b></div>
                <div class="row-txt">
                    <span>{{@($city->trans[0]->nationality!="") ? $city->trans[0]->nationality : $city->country->trans[0]->nationality}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.languages_spoken')</b>
                </div>
                <div class="row-txt">
                    <span>{{@($city->languages[0]->title!="") ? $city->languages[0]->title : $city->country->languages[0]->title}}</span>
                    <a href="#" class="more-link" data-toggle="modal"
                       data-target="#languageCurrTime">+{{@count($city->languages)-1}} @lang('buttons.general.more')</a>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.currencies')</b></div>
                <div class="row-txt">
                    <span>{{@$city->currencies[0]->trans[0]->title!="" ? $city->currencies[0]->trans[0]->title : $city->country->currencies[0]->trans[0]->title}}</span>
                    <span class="currency"></span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.timings')</b></div>
                <div class="row-txt">
                    <span>{{@$city->country->timezone[0]->time_zone_name}}</span>
                    <a href="#" class="more-link" data-toggle="modal"
                       data-target="#languageCurrTime">+{{@count($city->country->timezone)-1}} @lang('buttons.general.more')</a>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.religions')</b></div>
                <div class="row-txt">
                    <span>{{@$city->religions[0]->trans[0]->title!="" ? $city->religions[0]->trans[0]->title : $city->country->religions[0]->trans[0]->title}}</span>
                    <a href="#" class="more-link" data-toggle="modal"
                       data-target="#languageCurrTime">+{{@count($city->religions)-1}} @lang('buttons.general.more')</a>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.phone_code')</b></div>
                <div class="row-txt">
                    <span>{{@$city->code!="" ? $city->code : $city->country->code}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.units_of_measurement')</b>
                </div>
                <div class="row-txt">
                    <span>{{@$city->trans[0]->metrics!="" ? $city->trans[0]->metrics : $city->country->trans[0]->metrics}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.working_days')</b></div>
                <div class="row-txt">
                    <span>{{@$city->trans[0]->working_days!="" ? $city->trans[0]->working_days : $city->country->trans[0]->working_days}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.transportation_methods')</b></div>
                <div class="row-txt">
                    <?php
                    if (@$city->trans[0]->transportation != '') {
                        $transportation = explode(",", $city->trans[0]->transportation);
                    } else {
                        $transportation = explode(",", $city->country->trans[0]->transportation);
                    }
                    ?>
                    <span>{{@$transportation[0]}}</span>
                    <a href="#" class="more-link" data-toggle="modal"
                       data-target="#languageCurrTime">+{{@count($transportation)-1}} @lang('buttons.general.more')</a>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.speed_limit')</b></div>
                <div class="row-txt">
                    <?php
                    if ($city->trans[0]->speed_limit != '') {
                        $speed_limit = explode("\n", $city->trans[0]->speed_limit);
                    } else {
                        $speed_limit = explode("\n", $city->country->trans[0]->speed_limit);
                    }
                    ?>
                    <span>{{@$speed_limit[0]}}</span>
                    <a href="#" class="more-link" data-toggle="modal"
                       data-target="#languageCurrTime">+{{@count($speed_limit)-1}} @lang('buttons.general.more')</a>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-tips-list-block">
        <div class="post-top-layer">
            <div class="top-left">
                <h3 class="post-tip-ttl_lg">@lang('region.emergency_number')
                    &nbsp;<span>{{@count($city->emergency) ? count($city->emergency) : count($city->country->emergency)}}</span>
                </h3>
            </div>
        </div>
        <div class="post-list-inner">
            @if(count($city->emergency))
                @foreach($city->emergency AS $emergency)
                    <div class="post-tip-row">
                        <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                            <b>{{@strip_tags($emergency->trans[0]->description)}}</b></div>
                        <div class="row-txt">
                            <span>{{@$emergency->trans[0]->title}}</span>
                        </div>
                    </div>
                @endforeach
            @else
                @foreach($city->country->emergency AS $emergency)
                    <div class="post-tip-row">
                        <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                            <b>{{@strip_tags($emergency->trans[0]->description)}}</b></div>
                        <div class="row-txt">
                            <span>{{@$emergency->trans[0]->title}}</span>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    <div class="post-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.top_places') <span class="count">50</span></h3>
            <div class="side-right-control">
                <a href="#" class="see-more-link">@lang('region.see_all')</a>
                <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="post-slide-wrap slide-hide-right-margin">
                <ul id="placesSlider" class="post-slider">
                    @foreach($city->places()->take(50)->get() AS $place)
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/{{@$place->medias[0]->url}}"
                                 alt="" style="width:230px;height:300px">
                            <div class="post-slider-caption transparent-caption">
                                <p class="post-slide-name"><a href="{{url_with_locale('place/'.$place->id)}}"
                                                              style="color:white;">{{@$place->trans[0]->title}}</a>
                                </p>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    {{--
<div class="post-block">
<div class="post-side-top">
<h3 class="side-ttl">National holidays <span class="count">{{@count($city->country->holidays)}}</span></h3>
<div class="side-right-control">
<a href="#" class="see-more-link" data-toggle="modal" data-target="#nationalHolidayPopup">See all</a>
<a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
<a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
</div>
</div>
<div class="post-side-inner">
<div class="post-slide-wrap slide-hide-right-margin">
<ul id="nationalHoliday" class="post-slider">
                                    @foreach($city->country->holidays AS $holiday)
<li class="post-card">
    <div class="image-wrap">
        <img src="http://placehold.it/274x234" alt="">
        <div class="card-cover">
            <span class="date">4</span>
            <span class="month">Jul</span>
        </div>
    </div>
    <div class="post-slider-caption">
        <p class="post-card-name">{{@$holiday->trans[0]->title}}</p>
        <div class="post-footer-info">
            <div class="post-foot-block">
                <!--<ul class="foot-avatar-list">
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                </ul>
                <span>20 Talking about this</span>
                                                                        -->
            </div>
        </div>
    </div>
</li>
                                    @endforeach
</ul>
</div>
</div>
</div>
    --}}

    <div class="post-block post-country-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.accessibility')</h3>
        </div>
        <div class="post-country-inner post-full-inner">
            <div class="post-map-block">
                <div class="post-map-inner">
                    <img src="https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{$city->lat}},{{$city->lng}}&zoom=5&size=595x360&key={{env('GOOGLE_MAPS_KEY')}}"
                         alt="Map of {{@$city->trans[0]->title}}">
                    <div class="post-top-map-tabs">
                        <div class="map-tab current" data-tab="atmTxt">
                            <div class="tab-icon"><i class="trav-atms-icon"></i></div>
                            <div class="tab-txt">@lang('region.atms')</div>
                        </div>
                        <div class="map-tab" data-tab="wifiTxt">
                            <div class="tab-icon"><i class="trav-internet-icon"></i></div>
                            <div class="tab-txt">@lang('region.internet')</div>
                        </div>
                        <div class="map-tab" data-tab="socketTxt">
                            <div class="tab-icon"><i class="trav-sockets-icon"></i></div>
                            <div class="tab-txt">@lang('region.sockets')</div>
                        </div>
                        <div class="map-tab" data-tab="hotelTxt">
                            <div class="tab-icon"><i class="trav-hotels-icon"></i></div>
                            <div class="tab-txt">@lang('region.hotels')</div>
                        </div>
                    </div>
                    <div class="post-map-area">
                        <div class="area-txt" id="atmTxt">
                            <h5 class="ttl">{{number_format(@count($city->atms))}}</h5>
                            <p>@lang('region.atm_machines')</p>
                        </div>
                        <div class="area-txt" id="wifiTxt" style="display:none">
                            <h5 class="ttl">{{@$city->trans[0]->internet!='' ? $city->trans[0]->internet : $city->country->trans[0]->internet}}</h5>
                            <p>@lang('region.internet')</p>
                        </div>
                        <div class="area-txt" id="socketTxt" style="display:none">
                            <h5 class="ttl">{{@$city->trans[0]->sockets!="" ? $city->trans[0]->sockets : $city->country->trans[0]->sockets}}</h5>
                            <p>@lang('region.sockets')</p>
                        </div>
                        <div class="area-txt" id="hotelTxt" style="display:none">
                            <h5 class="ttl">{{number_format(@count($city->hotels))}}</h5>
                            <p>@lang('region.hotels')</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('sidebar')
    @if($city->id==185 OR $city->id==184 OR $city->id==322 OR $city->id==490)
        <?php
        $tomorrow = date("m/d/Y", time() + (24 * 60 * 60));
        $after_tomorrow = date("m/d/Y", time() + (2 * 24 * 60 * 60));
        ?>
        <div class="post-block post-side-block">
            <div class="post-side-top">
                <h3 class="side-ttl">@lang('region.stay_at') {{$city->transsingle->title}}</h3>
            </div>
            <div class="post-suggest-inner">
                <div class="suggest-txt">
                    <p>@lang('region.interested_in_staying_at') {{$city->transsingle->title}}
                        ?</p>
                </div>
                @if($city->id==185)
                    <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                            onclick='window.location.href = "{{url_with_locale('book/search-hotel?city_select=MAD&daterange='.$tomorrow.'%20-%20'.$after_tomorrow)}}";'>
                        @lang('region.book_a_hotel_now')
                    </button>
                @elseif($city->id==184)
                    <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                            onclick='window.location.href = "{{url_with_locale('book/search-hotel?city_select=BCN&daterange='.$tomorrow.'%20-%20'.$after_tomorrow)}}";'>
                        @lang('region.book_a_hotel_now')
                    </button>
                @elseif($city->id==322)
                    <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                            onclick='window.location.href = "{{url_with_locale('book/search-hotel?city_select=UKY&daterange='.$tomorrow.'%20-%20'.$after_tomorrow)}}";'>
                        @lang('region.book_a_hotel_now')
                    </button>
                @elseif($city->id==490)
                    <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                            onclick='window.location.href = "{{url_with_locale('book/search-hotel?city_select=LAX&daterange='.$tomorrow.'%20-%20'.$after_tomorrow)}}";'>
                        @lang('region.book_a_hotel_now')
                    </button>
                @endif
            </div>
        </div>
    @endif


    <div class="post-block post-country-block">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('region.main_airports') <span
                        class="count">{{@count($airports)}}</span></h3>
            <div class="side-right-control">
                <a href="#" class="see-more-link lg">@lang('region.see_all')</a>
            </div>
        </div>
        <div class="post-country-inner">
            <div class="dest-list">
                @foreach($airports AS $airport)
                    <div class="dest-item">
                        <div class="dest-location"><i
                                    class="trav-set-location-icon"></i>&nbsp; {{$airport->city->transsingle->title}}
                        </div>
                        <div class="dest-link-wrap">
                            <a href="{{route('place.index', $airport->id) }}"
                               class="dest-link">{{$airport->transsingle->title}}</a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
    @parent
@endsection


@section('after_scripts')
    @parent

    <script>
        // nearby events
        $('#nearByEventsTrigger').on('click', function () {

            let $lg = $(this).lightGallery({
                dynamic: true,
                dynamicEl: [
                        @foreach($events AS $ev)
                        <?php
                        $rand = rand(1, 10);
                        ?>
                    {
                        "src": "{{asset('assets2/image/events/'.$ev->category.'/'.$rand.'.jpg')}}",
                        'thumb': "{{asset('assets2/image/events/'.$ev->category.'/'.$rand.'.jpg')}}",
                        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>@lang("other.back")</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="sub-post-info">

                <div class="follow-btn-wrap">

                </div>
              </div>
            </div>
            <div class="map-preview">
              <img src="https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{$ev->location[1]}},{{$ev->location[0]}}&markers=color:red%7Clabel:C%7C{{$ev->location[1]}},{{$ev->location[0]}}&zoom=13&size=150x150&key={{env('GOOGLE_MAPS_KEY')}}" alt="map">
            </div>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-avatar-wrap">

                    </div>
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <p class="dest-name">{{$ev->title}}</p>
                        <p class="dest-place">
                                {{ucfirst($ev->category)}}
                            <br />@lang("time.from_to", ['from' => $ev->start, 'to' => $ev->end])</p>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="post-comment-layer">

                  <div class="post-comment-wrapper" style="color: black;padding: 20px;line-height: 1.3;">
                    {!!nl2br($ev->description)!!}
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>`
                    },

                    @endforeach
                ],
                addClass: 'main-gallery-block',
                pager: false,
                hideControlOnEnd: true,
                loop: false,
                slideEndAnimatoin: false,
                thumbnail: true,
                toogleThumb: false,
                thumbHeight: 100,
                thumbMargin: 20,
                thumbContHeight: 180,
                actualSize: false,
                zoom: false,
                autoplayControls: false,
                fullScreen: false,
                download: false,
                counter: false,
                mousewheel: false,
                appendSubHtmlTo: 'lg-item',
                prevHtml: '<i class="trav-angle-left"></i>',
                nextHtml: '<i class="trav-angle-right"></i>',
                hideBarsDelay: 100000000
            });

            $lg.on('onAfterOpen.lg', function () {
                $('body').css('overflow', 'hidden');
                let itemArr = [], thumbArr = [];
                let galleryBlock = $('.main-gallery-block');
                let galleryItem = $(galleryBlock).find('.lg-item');
                let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
                $.each(galleryItem, function (i, val) {
                    // itemArr.push(val);
                });
                $.each(galleryThumb, function (i, val) {
                    // thumbArr.push(val);
                    // let startCnt = `<div class="thumb-txt"><i class="trav-flag-icon"></i> start</div>`;
                    // let startCntEmpty = `<div class="thumb-txt">&nbsp;</div>`;
                    // let placetxt = 'rabar-sale airport'
                    // let placeName = `<div class="thumb-txt">${placetxt}</div>`;
                    // if(i == 0){
                    //   $(val).addClass('place-thumb');
                    //   $(val).append(placeName).prepend(startCnt);
                    // }
                    // if(i == 2){
                    //   $(val).addClass('place-thumb');
                    //   $(val).append(placeName).prepend(startCntEmpty);
                    // }
                });
            });
            $lg.on('onBeforeClose.lg', function () {
                $('body').removeAttr('style');
            });
            let setWidth = function () {
                let mainBlock = $('.main-gallery-block');
                let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
                let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

                let slide = $('.main-gallery-block .lg-item');
                let currentItem = $('.main-gallery-block .lg-current');
                let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
                let currentImg = $('.main-gallery-block .lg-current .lg-image');
                let currentCommentIs = $(subTtl).hasClass('comment-block');
                let currentImgPos = $(currentImg).position().top;
                setTimeout(function () {
                    let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                    let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                    if (currentCommentIs) {
                        // console.log('yes');
                        $(currentImgWrap).css('padding-right', commentWidth);
                        $(subTtl).css('width', currentWidth + commentWidth);
                    } else {
                        $(currentImgWrap).removeAttr('style');
                        $(subTtl).css('width', currentWidth);
                    }
                    $(subTtlWrp).show();
                    $('.mCustomScrollbar').mCustomScrollbar();
                }, 500);
            }

            $lg.on('onSlideItemLoad.lg', function (e) {

                setWidth();
                $(window).on('resize', function () {
                    setWidth();
                })
            });
            $lg.on('onAfterSlide.lg', function () {
                setWidth();
            });
        });
    </script>

@endsection