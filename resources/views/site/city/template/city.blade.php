@extends('site.layouts.site')

@section('before_styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
@endsection

@section('content-area')

    @include('site.city.template._menu')

    @yield('before_content')

    <div class="custom-row">
        <!-- MAIN-CONTENT -->
        <div class="main-content-layer">

            @yield('content')

        </div>
        <!-- SIDEBAR -->
        <div class="sidebar-layer" id="sidebarLayer">
            <aside class="sidebar">
                @section('sidebar')
                    @include('site.layouts._share')
                    @include('site.layouts._sidebar')
                @show
            </aside>
        </div>
    </div>


@endsection


@section('before_scripts')
    @include('site.city.template._modals')
@endsection

@section('before_site_script')
    <link rel="stylesheet" href="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.css')}}">
    <script src="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>
@endsection

@section('after_scripts')
    @include('site.city.template._scripts')

    @include('site.layouts._footer-scripts')
@endsection

