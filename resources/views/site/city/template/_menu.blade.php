<!-- left outside menu -->
<div class="left-outside-menu-wrap" id="leftOutsideMenu">
    <ul class="left-outside-menu">
        <li>
            <a href="{{route('home')}}">
                <i class="trav-home-icon"></i>
                <span>@lang('navs.general.home')</span>
                <!--<span class="counter">5</span>-->
            </a>
        </li>
        <li {{ route('city.index', $city->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('city.index', $city->id)}}">
                <i class="trav-about-icon"></i>
                <span>@lang('region.nav.about')</span>
            </a>
        </li>
        <li {{ route('city.packing_tips', $city->id)  === url()->current() ? 'class=active' : null }}>
            <a href="{{ route('city.packing_tips', $city->id) }}">
                <i class="trav-trips-tips-icon"></i>
                <span>@lang('region.nav.packing_tips')</span>
            </a>
        </li>
        <li {{ route('city.weather', $city->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('city.weather', $city->id)}}">
                <i class="trav-weather-cloud-icon"></i>
                <span>@lang('region.nav.weather')</span>
            </a>
        </li>
        <li {{ route('city.etiquette', $city->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('city.etiquette', $city->id)}}">
                <i class="trav-etiquette-icon"></i>
                <span>@lang('region.nav.etiquette')</span>
            </a>
        </li>
        <li {{ route('city.health', $city->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{ route('city.health', $city->id) }}">
                <i class="trav-health-hand-icon"></i>
                <span>@lang('region.nav.health ')</span>
            </a>
        </li>
        <li {{ route('city.visa_requirements', $city->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{ route('city.visa_requirements', $city->id) }}">
                <i class="trav-visa-embassy-icon"></i>
                <span>@lang('region.nav.visa')</span>
            </a>
        </li>
        <li {{ route('city.when_to_go', $city->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{route('city.when_to_go', $city->id)}}">
                <i class="trav-flights-icon"></i>
                <span>@lang('region.nav.when_to_go')</span>
            </a>
        </li>
        <li {{ route('city.caution', $city->id) === url()->current() ? 'class=active' : null }}>
            <a href="{{ route('city.caution', $city->id) }}">
                <i class="trav-caution-icon"></i>
                <span>@lang('region.nav.caution')</span>
            </a>
        </li>
        <li>
            <a href="{{route('discussion.index', ['do' => 'city', 'id' => $city->id]) }}">
                <i class="trav-ask-icon"></i>
                <span>@lang('navs.frontend.forum')</span>
            </a>
        </li>
    </ul>
</div>