<!DOCTYPE html>
<html class="page" lang="en">
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="date=no">
        <meta name="format-detection" content="address=no">
        <meta name="format-detection" content="email=no">
        <meta content="notranslate" name="google">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
         <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css">
        <!-- <link rel="stylesheet" href="{{asset('assets2/css/place.css?v='.time()) }}"> -->
        <link rel="stylesheet" href="{{asset('assets2/css/style-15102019-9.css')}}">
        <link rel="stylesheet" href="{{asset('assets2/css/travooo.css?v='.time())}}">
        <link rel="stylesheet" href="{{asset('assets3/css/datepicker-extended.css')}}">
        <!-- <link rel="stylesheet" type="text/css" href="{{asset('assets3/css/slider-pro.min.css')}}" media="screen"/> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
        <link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>

        <link rel="stylesheet" href="{{asset('assets3/css/star.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/lightslider.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> -->
        <script src="{{ asset('assets2/js/jquery-3.1.1.min.js') }}"></script>
        <!-- include summernote css/js -->
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
        <script>
            var map_var = "";
            var map_func = "";

            // Design page scripts
            $(document).ready(function() {
                var isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints > 0;

                // Sticky left menu for desktop
                if(!isTouchDevice) {
                    var leftMenu = $('#leftOutsideMenu');
                    var mainHeader = $('.main-header');
                    $(window).scroll(function() {
                        var windowTop = $(window).scrollTop();
                        if (windowTop > mainHeader.height()) {
                            leftMenu.addClass('sticky');
                        } else {
                            leftMenu.removeClass('sticky');
                        }
                    });
                }

                $('.add-post__emoji').emojiInit({
                    fontSize:20,
                    success : function(data){

                    },
                    error : function(data,msg){
                    }
                });
                $('.add-msg__emoji').emojiInit({
                    fontSize:20,
                    success : function(data){

                    },
                    error : function(data,msg){
                    }
                });
                $('.checkin_date').datepicker({
                    format: 'M dd yy',
                });
                $('.checkin_time').timepicker({
                    timeFormat: 'h:i A'
                });
                // Create new post medias slider
                $('.medias.medias-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 10,
                    pager: false,
                    controls: false,
                    addClass: 'new-post-medias',
                });
                // Post type - shared place, slider
                $('.shared-place-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 22,
                    pager: false,
                    controls: false,
                    responsive: [
                        {
                            breakpoint:767,
                            settings: {
                                slideMargin: 10,
                            }
                        },
                    ],
                });
                // More/less content
                $(document).on('click', ".read-more-link", function () {
                    $(this).closest('.post-txt-wrap').find('.less-content').hide()
                    $(this).closest('.post-txt-wrap').find('.more-content').show()
                    $(this).hide()
                });
                $(document).on('click', ".read-less-link", function () {
                    $(this).closest('.more-content').hide()
                    $(this).closest('.post-txt-wrap').find('.less-content').show()
                    $(this).closest('.post-txt-wrap').find('.read-more-link').show()
                });
                // Like button
                $(document).on('click', ".post_like_button a", function (e) {
                    e.preventDefault();
                    $(this).closest('.post_like_button').toggleClass('liked');
                });
                // Tooltips
                $('[data-toggle="tooltip"]').tooltip({
                    placement: 'top',
                    trigger: 'hover'
                });
                // Post text tag popover
                $(".post-text-tag").popover({
                    html: true, 
                    content: $('#popover-content').html(),
                    template: '<div class="popover bottom tagging-popover" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
                // Post more-people popover
                $(".post-more-people").popover({
                    html: true, 
                    content: $('#popover-content-people').html(),
                    template: '<div class="popover bottom tagging-popover white" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
                // Post holiday popover
                $(".post-holiday-popover").popover({
                    html: true, 
                    content: $('#popover-content-holiday').html(),
                    template: '<div class="popover bottom tagging-popover white" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'click',
                });
                // Map popover
                $(".map-popover").popover({
                    html: true, 
                    content: $('#popover-content-map').html(),
                    template: '<div class="popover top forecast-map-popover white" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
                // Trending videos slider
                let videoSlider = $('.trending-videos-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 20,
                    pager: false,
                    controls: false,
                    responsive: [
                        {
                            breakpoint:767,
                            settings: {
                                slideMargin: 10,
                            }
                        },
                    ],
                });
                $('.video-slide-prev').click(function(e){
                    e.preventDefault();
                    videoSlider.goToPrevSlide(); 
                });
                $('.video-slide-next').click(function(e){
                    e.preventDefault();
                    videoSlider.goToNextSlide(); 
                });
                // Custom scrollbar
                $('.friend-search-results').mCustomScrollbar();
                // Extend button
                $('.extednd-btn').click(function(e){
                    e.preventDefault();
                    $(this).toggleClass('shrink');
                    $(this).closest('.share-to-friend').toggleClass('relative');
                });
                // Aside Photo Slider
                $('.aside-photo-slider').lightSlider({
                    item: 1,
                    loop: true,
                    slideMargin: 20,
                    pager: false,
                    enableDrag: false
                });
                // Aside Photo Slider Inner
                $('.openInnerSlider').click(function(){
                    if($('.aside-slider-modal .visible').length < 1) {
                        setTimeout(() => {
                            var sideInnerslider = $('.aside-photo-slider-inner').lightSlider({
                                gallery: true,
                                item: 1,
                                loop: true,
                                slideMargin: 20,
                                currentPagerPosition: 'middle',
                                thumbMargin: 10,
                                adaptiveHeight: true,
                                controls: false,
                                enableDrag: false,
                            });
                            $('.aside-slider-modal .wrapper').addClass('visible');
                            $('.side-inner-slider-prev').click(function(e){
                                e.preventDefault();
                                sideInnerslider.goToPrevSlide(); 
                            });
                            $('.side-inner-slider-next').click(function(e){
                                e.preventDefault();
                                sideInnerslider.goToNextSlide(); 
                            });
                    }, 50);
                    }
                });
                // Trip plan home slider
                var homeTripSlider = $('.trip-plan-home-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 10,
                    pager: false,
                    controls: false
                });
                $('.trip-plan-home-slider-prev').click(function(e){
                    e.preventDefault();
                    homeTripSlider.goToPrevSlide(); 
                });
                $('.trip-plan-home-slider-next').click(function(e){
                    e.preventDefault();
                    homeTripSlider.goToNextSlide(); 
                });
                // Recommended places slider
                var homeRecommendedePlacesSlider = $('.recommended-places-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 20,
                    pager: false,
                    controls: false,
                    responsive: [
                        {
                            breakpoint:767,
                            settings: {
                                slideMargin: 10,
                            }
                        },
                    ],
                });
                $('.recommended-places-slider-prev').click(function(e){
                    e.preventDefault();
                    homeRecommendedePlacesSlider.goToPrevSlide(); 
                });
                $('.recommended-places-slider-next').click(function(e){
                    e.preventDefault();
                    homeRecommendedePlacesSlider.goToNextSlide(); 
                });
                // Place you might like slider
                var placeYouMightLikeSlider = $('.places-you-might-like-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 20,
                    pager: false,
                    controls: false,
                    responsive: [
                        {
                            breakpoint:767,
                            settings: {
                                slideMargin: 10,
                            }
                        },
                    ],
                });
                $('.places-you-might-like-slider-prev').click(function(e){
                    e.preventDefault();
                    placeYouMightLikeSlider.goToPrevSlide(); 
                });
                $('.places-you-might-like-slider-next').click(function(e){
                    e.preventDefault();
                    placeYouMightLikeSlider.goToNextSlide(); 
                });
                // Video button
                $('.v-play-btn, .video-play-btn').click(function(){
                    $(this).siblings('video').get(0).play();
                    $(this).toggleClass('hide');
                });
                // Post prifile blocks slider
                $('.post-profile-blocks-slider').lightSlider({
                    item: 1,
                    loop: true,
                    slideMargin: 10,
                    pager: false,
                    controls: true
                });
                // Place you might like slider
                var newTravelerDiscover = $('#newTravelerDiscover').lightSlider({
                    autoWidth: true,
                    slideMargin: 20,
                    pager: false,
                    controls: false,
                    responsive: [
                        {
                            breakpoint:767,
                            settings: {
                                slideMargin: 10,
                            }
                        },
                    ],
                });
                $('.newTravelerDiscover-prev').click(function(e){
                    e.preventDefault();
                    newTravelerDiscover.goToPrevSlide(); 
                });
                $('.newTravelerDiscover-next').click(function(e){
                    e.preventDefault();
                    newTravelerDiscover.goToNextSlide(); 
                });
                // Trending destinations slider
                var discoverNewDestination = $('#discoverNewDestination').lightSlider({
                    autoWidth: true,
                    slideMargin: 20,
                    pager: false,
                    controls: false,
                    responsive: [
                        {
                            breakpoint:767,
                            settings: {
                                slideMargin: 10,
                            }
                        },
                    ],
                });
                $('.discoverNewDestination-prev').click(function(e){
                    e.preventDefault();
                    discoverNewDestination.goToPrevSlide(); 
                });
                $('.discoverNewDestination-next').click(function(e){
                    e.preventDefault();
                    discoverNewDestination.goToNextSlide(); 
                });
            });
        </script>
        <style>
            /* Left menu */
            .left-outside-menu-wrap {
                width: auto;
                margin-top: 30px;
            }
            .left-outside-menu-wrap .left-outside-menu li a {
                opacity: 1;
                font-size: 18px;
                font-family: inherit;
                color: #999999;
                flex-direction: row;
                align-items: center;
                justify-content: flex-start;
            }
            .left-outside-menu-wrap .left-outside-menu li.active a {
                color: #1a1a1a;
                font-family: 'Circular Std Bold';
            }
            .left-outside-menu-wrap .left-outside-menu li a .icon-wrap {
                width: 70px;
                text-align: center;
            }
            @media (min-width: 992px) {
                #leftOutsideMenu.sticky {
                    transform: translate(-217px, -68px);
                }
            }
            /* Left menu END */

            .create-new-post-popup .note-placeholder {
                display: none!important;
            }
        </style>
    </head>

    <body class="page__body">

    <div class="main-wrapper newsfeed-page">

    <svg style="position: absolute; width: 0; height: 0; overflow: hidden" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="angle-circle-up-solid" viewBox="0 0 21 21">
                <path d="M10.5.5c5.523 0 10 4.476 10 9.999 0 5.523-4.477 10-10 10s-10-4.477-10-10 4.477-10 10-10zm1.325 7.937L10.539 7.06l-.04.041-.038-.04-1.287 1.377.032.034L6.1 11.758l1.286 1.378L10.5 9.841l3.112 3.295 1.287-1.378-3.105-3.288z"/>
            </symbol>
        </defs>
    </svg>
        @include('site.layouts._header')
        <div class="content-wrap">
            <div class="container-fluid mobile-fullwidth">
                <!-- Left Menu -->
                <div class="left-outside-menu-wrap" id="leftOutsideMenu" dir="auto">
                <div class="leftOutsideMenuClose" dir="auto"></div>
                <ul class="left-outside-menu" dir="auto">
                <li class="active" dir="auto">
                <a href="https://travooo.com/home" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-home.png')}}" width="49" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Home</span>

                </a>
                </li>
                <li dir="auto">
                <a href="https://travooo.com/travelmates-newest" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-mates.png')}}" width="47" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Travel mates</span>
                </a>
                </li>
                <li class="" dir="auto">
                <a href="https://travooo.com/trip-plans" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-plans.png')}}" width="43" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Trip Plans</span>
                </a>
                </li>
                <li class="" dir="auto">
                <a href="https://travooo.com/discussions" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-ask.png')}}" width="46" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Ask</span>
                </a>
                </li>
                <li dir="auto"><a href="https://travooo.com/profile-map" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-map.png')}}"width="44" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Map</span>
                </a>
                </li>
                <li class="" dir="auto">
                <a href="https://travooo.com/reports/list?order=newest&amp;" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-travelogs.png')}}"width="46" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Reports</span>
                </a>
                </li>
                </ul>
                </div>
                <!-- Left Menu END -->
                <div class="custom-row">
                    <!-- Main content -->
                    <div class="main-content-layer" style="padding-top:10px">
                        <div id="feed">
                            <!-- Summernote test -->
                            <div class="post-block">
                                <div id="sanitizeSummernote"></div>
                            </div>
                            <!-- Summernote test -->
                            <!-- New post box -->
                            <div class="post-block create-new-post-block">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="https://travooo.com/profile/555">Amine Kaddari</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="create-new-post-form-trigger" data-toggle="modal" data-target="#createNewPostPopup">
                                    <span class="text">Write something...</span>
                                    <span>
                                        <i class="trav-camera"></i>
                                        <i class="trav-set-location-icon location"></i>
                                    </span>
                                </div>
                                <!-- Create new post popup -->
                                <div class="modal white-style create-new-post-popup" data-backdrop="false" id="createNewPostPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <!-- Mobile element -->
                                    <button class="mobile-close-btn" type="button" data-dismiss="modal" aria-label="Close"><i class="fas fa-plus"></i></button>
                                    <!-- Mobile element -->
                                    <div class="modal-custom-style modal-615" role="document">
                                        <div class="modal-custom-block">
                                            <!-- Home page new post block(updated, see the comments) -->
                                            <div class="post-block post-create-block" id="createPostBlock" >
                                                <!-- New element -->
                                                <div class="post-top-info-layer">
                                                    <div class="post-top-info-wrap">
                                                        <div class="post-top-avatar-wrap">
                                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                        </div>
                                                        <div class="post-top-info-txt">
                                                            <div class="post-top-name">
                                                                <a class="post-name-link" href="https://travooo.com/profile/555">Amine Kaddari</a>
                                                            </div>
                                                        </div>
                                                        <!-- privacy settings -->
                                                        <div class="dropdown">
                                                            <button id="add_post_permission_button" class="btn btn--sm btn--outline dropdown-toggle" data-toggle="dropdown">
                                                                <i class="trav-globe-icon"></i>
                                                                PUBLIC
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right permissoin_show hided">
                                                                <a class="dropdown-item" href="#">
                                                                    <span class="icon-wrap">
                                                                        <i class="trav-globe-icon"></i>
                                                                    </span>
                                                                    <div class="drop-txt">
                                                                        <p><b>Public</b></p>
                                                                        <p>Anyone can see this post</p>
                                                                    </div>
                                                                </a>
                                                                <a class="dropdown-item" href="#">
                                                                    <span class="icon-wrap">
                                                                        <i class="trav-users-icon"></i>
                                                                    </span>
                                                                    <div class="drop-txt">
                                                                        <p><b>Friends Only</b></p>
                                                                        <p>Only you and your friends can see this post</p>
                                                                    </div>
                                                                </a>
                                                                <a class="dropdown-item" href="#">
                                                                    <span class="icon-wrap">
                                                                        <i class="trav-lock-icon"></i>
                                                                    </span>
                                                                    <div class="drop-txt">
                                                                        <p><b>Only Me</b></p>
                                                                        <p>Only you can see this post</p>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- privacy settings -->
                                                    </div>
                                                </div>
                                                <!-- New element END -->
                                                <div class="post-create-input" >
                                                <!-- New element -->
                                                <div class="tagging-block">
                                                    <button class="add-post__tags" data-dismiss="modal" data-toggle="modal" data-target="#addPostTagsModal" type="button">
                                                        <i class="trav-tag-icon"></i>
                                                    </button>
                                                </div>
                                                <div class="emoji-block">
                                                    <button class="add-post__emoji" emoji-target="add_post_text" type="button">🙂</button> 
                                                </div>
                                                <div class="note-codable">
                                                    <textarea name="" id="sanitizeTextarea" style="width: 100%; height: 100px; resize: none;" placeholder="Sanitize textarea..."></textarea>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec nulla feugiat, dictum augue 🥐 dictum augue. 
                                                    Proin dignissim dapibus felis, vitae ornare lorem tincidunt sit amet ✈ Duis non purus facilisis, pulvinar leo sit 
                                                    <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@mike</span>
                                                    <div class="dropdown-menu tagging-dropdown">
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" href="#people" role="tab" data-toggle="tab">People <span>10</span></a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" href="#places" role="tab" data-toggle="tab">Places <span>+10</span></a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" href="#countries" role="tab" data-toggle="tab">Countries & Cities <span>3</span></a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane fade in active show" id="people" aria-expanded="true" >
                                                                <div class="drop-wrap">
                                                                    <div class="drop-row">
                                                                        <div class="img-wrap rounded place-img">
                                                                            <img src="https://www.travooo.com/assets2/image/placeholders/place.png" alt="logo">
                                                                        </div>
                                                                        <div class="drop-content place-content">
                                                                            <h3 class="content-ttl"><span>Mike</span> Workman</h3>
                                                                            <p class="place-name">United States</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="drop-row">
                                                                        <div class="img-wrap rounded place-img">
                                                                            <img src="https://www.travooo.com/assets2/image/placeholders/place.png" alt="logo">
                                                                        </div>
                                                                        <div class="drop-content place-content">
                                                                            <h3 class="content-ttl"><span>Mike</span> Wiley</h3>
                                                                            <p class="place-name">United Kingdom</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane fade in" id="places">
                                                            <div class="drop-wrap">
                                                                    <div class="drop-row">
                                                                        <div class="img-wrap rounded place-img">
                                                                            <img src="https://www.travooo.com/assets2/image/placeholders/place.png" alt="logo">
                                                                        </div>
                                                                        <div class="drop-content place-content">
                                                                            <h3 class="content-ttl"><span>Mike</span> Workman</h3>
                                                                            <p class="place-name">United States</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="drop-row">
                                                                        <div class="img-wrap rounded place-img">
                                                                            <img src="https://www.travooo.com/assets2/image/placeholders/place.png" alt="logo">
                                                                        </div>
                                                                        <div class="drop-content place-content">
                                                                            <h3 class="content-ttl"><span>Mike</span> Wiley</h3>
                                                                            <p class="place-name">United Kingdom</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane fade in" id="countries">
                                                            <div class="drop-wrap">
                                                                    <div class="drop-row">
                                                                        <div class="img-wrap rounded place-img">
                                                                            <img src="https://www.travooo.com/assets2/image/placeholders/place.png" alt="logo">
                                                                        </div>
                                                                        <div class="drop-content place-content">
                                                                            <h3 class="content-ttl"><span>Mike</span> Workman</h3>
                                                                            <p class="place-name">United States</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="drop-row">
                                                                        <div class="img-wrap rounded place-img">
                                                                            <img src="https://www.travooo.com/assets2/image/placeholders/place.png" alt="logo">
                                                                        </div>
                                                                        <div class="drop-content place-content">
                                                                            <h3 class="content-ttl"><span>Mike</span> Wiley</h3>
                                                                            <p class="place-name">United Kingdom</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="check-in-point">
                                                    <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                                                </div>
                                                <!-- New element END -->
                                                <!-- Tags block -->
                                                <div class="post-tags-list">
                                                    <div>Tags:</div>
                                                    <ul>
                                                        <li class="tag">Food <i class="trav-close-icon delete"></i></li>
                                                        <li class="tag">Travel <i class="trav-close-icon delete"></i></li>
                                                        <li class="tag">Sport <i class="trav-close-icon delete"></i></li>
                                                    </ul>
                                                </div>
                                                <script>
                                                    $(document).on('click', '.create-new-post-popup .post-tags-list .delete', function () {
                                                        $(this).parent('.tag').remove();
                                                        if($.trim($('.create-new-post-popup .post-tags-list ul').html()) == '') {
                                                            $('.create-new-post-popup .post-tags-list').remove();
                                                        }
                                                    });
                                                    $('#sanitizeTextarea').keyup(function() {
                                                        var input = $(this).val();
                                                        input.replace(/<(|\/|[^>\/bi]|\/[^>bi]|[^\/>][^>]+|\/[^>][^>]+)>/g, '');
                                                        $(this).val(input);
                                                    });
                                                </script>
                                                <!-- Tags block END -->
                                                <!-- Updated elemant -->
                                                <div class="medias medias-slider" >
                                                <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                                                <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                                                <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                                                <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                                                <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                                                <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                                                <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                                                <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                                                <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                                                <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                                                <div class="img-wrap-newsfeed loading">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                </div><!-- Updated elemant END -->
                                                </div>
                                                <div class="post-create-controls" >
                                                <div class="post-alloptions" >
                                                <ul class="create-link-list" >
                                                    <li class="post-options" >
                                                        <input type="file" name="file[]" id="file" style="display:none" multiple="" >
                                                        <i class="trav-camera click-target" data-target="file" ></i>
                                                    </li>
                                                    <li class="post-options" >
                                                        <!-- Updated item -->
                                                        <i class="trav-set-location-icon location" data-dismiss="modal" data-toggle="modal" data-target="#createNewPostCheckinPopup"></i>
                                                        <!-- Updated item -->
                                                    </li>
                                                    <!-- Removed calendar-related list item -->
                                                </ul>
                                                <!-- Removed blocks - .locationLoad, .locationSelect -->
                                                </div>
                                                <!-- New element -->
                                                <button type="button" class="btn btn-link mr-3" data-dismiss="modal" aria-label="Close">Cancel</button>
                                                <!-- New element -->
                                                <div class="spinner-border" role="status" style="display:none" ><span class="sr-only" >Loading...</span></div>
                                                <button type="submit" class="btn btn-link primary btn-disabled" id="sendPostForm"  disabled="disabled">POST </button>
                                                </div>
                                                <div id="output" ></div>
                                                <div id="loader" ></div>
                                                <div class="search-box tag-tooltip" style="display:none;position: absolute;top:80px;z-index: 1000;background-color: white;width: 400px;max-height: 300px;overflow: auto;-webkit-box-shadow: 0px 0px 26px 0px rgba(0,0,0,0.27);-moz-box-shadow: 0px 0px 26px 0px rgba(0,0,0,0.27);box-shadow: 0px 0px 26px 0px rgba(0,0,0,0.27);border-radius: 3px;"><div style="font-style: italic; padding: 5px 0 0 5px" class="results-for-container" ><i >Results for</i>&nbsp;<b class="results-for-query" >query</b></div>  <ul class="nav nav-tabs search-tabs" role="tablist" >    <li class="active nav-item" role="presentation" >           <a class="nav-link active" href="#lc-places-999" aria-controls="lc-places-999" role="tab" data-toggle="tab" >Places <span class="num-results-places" ></span></a>    </li>    <li class="nav-item" role="presentation" >           <a class="nav-link " href="#lc-people-999" aria-controls="lc-people-999" role="tab" data-toggle="tab" >People <span class="num-results-people" ></span></a>    </li>  </ul>  <div class="tab-content" >
                                                        <div role="tabpanel" class="tab-pane drop-wrap active" id="lc-places-999" ></div>
                                                        <div role="tabpanel" class="tab-pane drop-wrap" id="lc-people-999" ></div>
                                                </div></div></div>
                                            <!-- Home page new post block END -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Create new post popup CHECKIN -->
                                <div class="modal white-style create-new-post-checkin-popup" data-backdrop="false" id="createNewPostCheckinPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <!-- Mobile element -->
                                    <button class="mobile-close-btn" type="button" data-dismiss="modal" aria-label="Close"><i class="fas fa-plus"></i></button>
                                    <!-- Mobile element -->
                                    <div class="modal-custom-style modal-615" role="document">
                                        <div class="modal-custom-block" >
                                            <div class="post-block">
                                                <div class="post-top-info-layer">
                                                    <div class="post-top-info-wrap">
                                                        <button class="back" type="button" data-dismiss="modal" data-toggle="modal" data-target="#createNewPostPopup">
                                                            <i class="trav-angle-left"></i>
                                                        </button>
                                                        <h5 class="side-ttl" >Check-in</h5>
                                                    </div>
                                                </div>
                                                <div class="checkin-details-block">
                                                    <i class="trav-search-icon" ></i>
                                                    <input type="text" placeholder="Check-in location...">
                                                    <div class="check-date-wrap">
                                                        <i class="trav-calendar-icon" ></i>
                                                        <input autocomplete="off" placeholder="JUN 13 20" value="JUN 13 20" type="text" name="checkin_date" class="checkin_date"
                                                    class="form-control" required>
                                                    </div>
                                                    <span class="divider">|</span>
                                                    <div class="check-time-wrap">
                                                        <i class="trav-clock-icon" ></i>
                                                        <input autocomplete="off" placeholder="09:50 AM" value="09:50 AM" type="text" name="checkin_time" class="checkin_time"
                                                    class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="checkin-results-block">
                                                    <div class="checkin-results-item">
                                                        <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                        <div class="text">
                                                            <div class="location-title">Paris</div>
                                                            <div class="location-description">City in France</div>
                                                        </div>
                                                    </div>
                                                    <div class="checkin-results-item">
                                                        <i class="trav-plane-icon"></i>
                                                        <div class="text">
                                                            <div class="location-title">Tokyo Airport</div>
                                                            <div class="location-description">Hanedakuko, Ota City, Tokyo 144-0041, Japan</div>
                                                        </div>
                                                    </div>
                                                    <div class="checkin-results-item">
                                                        <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                        <div class="text">
                                                            <div class="location-title">Marrakech</div>
                                                            <div class="location-description">City in Morocco</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Create new post popup CHECKIN END -->
                                <!-- Create new post modal TAGS -->
                                <div class="modal white-style create-new-post-checkin-popup" data-backdrop="false" id="addPostTagsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <!-- Mobile element -->
                                    <button class="mobile-close-btn" type="button" data-dismiss="modal" aria-label="Close"><i class="fas fa-plus"></i></button>
                                    <!-- Mobile element -->
                                    <div class="modal-custom-style modal-615" role="document">
                                        <div class="modal-custom-block" >
                                            <div class="post-block">
                                                <div class="post-top-info-layer">
                                                    <div class="post-top-info-wrap">
                                                        <button class="back" type="button" data-dismiss="modal" data-toggle="modal" data-target="#createNewPostPopup">
                                                            <i class="trav-angle-left"></i>
                                                        </button>
                                                        <h5 class="side-ttl" >Tagging</h5>
                                                    </div>
                                                </div>
                                                <div class="checkin-details-block">
                                                    <i class="trav-search-icon" ></i>
                                                    <input type="text" placeholder="People, places, cities...">
                                                </div>
                                                <ul class="search-selected-block tagging-search-block">
                                                    <li>
                                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                        <span class="search-selitem-title">Marrakesh</span>
                                                        <span class="close-search-item close-search-interest-filter" data-interest-val="marrakesh">
                                                            <i class="trav-close-icon"></i>
                                                        </span>
                                                    </li>
                                                </ul>
                                                <ul class="nav tagging-results-filter" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="tags-all-results-tab" data-toggle="pill" href="#tags-all-results" role="tab" aria-controls="pills-all" aria-selected="true">All Results</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="tags-people-results-tab" data-toggle="pill" href="#tags-people-results" role="tab" aria-controls="pills-people" aria-selected="false">People <i>12</i></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="tags-places-results-tab" data-toggle="pill" href="#tags-places-results" role="tab" aria-controls="pills-places" aria-selected="false">Places <i>0</i></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="tags-cities-results-tab" data-toggle="pill" href="#tags-cities-results" role="tab" aria-controls="pills-cities" aria-selected="false">Cities <i>4</i></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="tags-countries-results-tab" data-toggle="pill" href="#tags-countries-results" role="tab" aria-controls="pills-countries" aria-selected="false">Countries <i>7</i></a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane show active" id="tags-all-results" role="tabpanel" aria-labelledby="tags-all-results-tab">
                                                        <!-- Results -->
                                                        <div class="checkin-results-block">
                                                            <div class="checkin-results-item">
                                                                <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                                <div class="text">
                                                                    <div class="location-title">Paris <span>City</span></div>
                                                                    <div class="location-description">City in France</div>
                                                                </div>
                                                            </div>
                                                            <div class="checkin-results-item">
                                                                <i class="trav-plane-icon"></i>
                                                                <div class="text">
                                                                    <div class="location-title">Tokyo Airport <span>Place</span></div>
                                                                    <div class="location-description">Hanedakuko, Ota City, Tokyo 144-0041, Japan</div>
                                                                </div>
                                                            </div>
                                                            <div class="checkin-results-item">
                                                                <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                                <div class="text">
                                                                    <div class="location-title">Marrakech <span>City</span></div>
                                                                    <div class="location-description">City in Morocco</div>
                                                                </div>
                                                            </div>
                                                            <div class="checkin-results-item">
                                                                <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                                <div class="text">
                                                                    <div class="location-title">James Robinson <span>Friend</span></div>
                                                                    <div class="location-description">New York, United States</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Results END -->
                                                    </div>
                                                    <div class="tab-pane" id="tags-people-results" role="tabpanel" aria-labelledby="tags-people-results-tab">
                                                        <!-- Results -->
                                                        <div class="checkin-results-block">
                                                            <div class="checkin-results-item">
                                                                <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                                <div class="text">
                                                                    <div class="location-title">James Robinson</div>
                                                                    <div class="location-description">New York, United States</div>
                                                                </div>
                                                            </div>
                                                            <div class="checkin-results-item">
                                                                <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                                <div class="text">
                                                                    <div class="location-title">William Keo</div>
                                                                    <div class="location-description">New York, United States</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Results END -->
                                                    </div>
                                                    <div class="tab-pane" id="tags-places-results" role="tabpanel" aria-labelledby="tags-places-results-tab">
                                                        <!-- Results -->
                                                        <div class="checkin-results-block">
                                                            <div class="checkin-results-item">
                                                                <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                                <div class="text">
                                                                    <div class="location-title">Marrakech</div>
                                                                    <div class="location-description">City in Morocco</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Results END -->
                                                    </div>
                                                    <div class="tab-pane" id="tags-cities-results" role="tabpanel" aria-labelledby="tags-cities-results-tab">
                                                        <!-- Results -->
                                                        <div class="checkin-results-block">
                                                            <div class="checkin-results-item">
                                                                <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                                <div class="text">
                                                                    <div class="location-title">Paris</div>
                                                                    <div class="location-description">City in France</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Results END -->
                                                    </div>
                                                    <div class="tab-pane" id="tags-countries-results" role="tabpanel" aria-labelledby="tags-countries-results-tab">
                                                        <!-- Results -->
                                                        <div class="checkin-results-block">
                                                            <div class="checkin-results-item">
                                                                <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                                                                <div class="text">
                                                                    <div class="location-title">Marrakech</div>
                                                                    <div class="location-description">City in Morocco</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Results END -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Create new post modal TAGS END -->
                            </div>
                            <!-- New post box END -->
                            <!-- This content isn't available right now -->
                            <div class="post-block">
                            <div class="post-top-info-layer">
                            <div class="post-top-info-wrap">
                            <div class="post-top-avatar-wrap">
                            <a class="post-name-link" href="https://travooo.com/profile/831">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                            </a>
                            </div>
                            <div class="post-top-info-txt">
                            <div class="post-top-name">
                            <a class="post-name-link" href="https://travooo.com/profile/831">Ivan Turlakov</a>
                            </div>
                            <div class="post-info">
                            Checked-in
                            <a href="https://travooo.com/place/3485165" class="link-place">The House of Dancing Water</a>
                            on Sep 1, 2020
                            <i class="trav-globe-icon"></i>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="content-placeholder bordered">
                                <div class="img-placeholder">
                                    <i class="fas fa-eye-slash"></i>
                                </div>
                                <div class="text-placeholder">
                                    <h3>This content isn't available right now</h3>
                                    <p>When this happens it's usually because the owner only shared it with a small group of people, change who can see it or it's been deleted.</p>
                                </div>
                            </div>
                            </div>
                            <!-- This content isn't available right now END -->
                            <!-- Discover new travelers slider - copied from home page -->
                            <div class="post-block discover-travellers-block" dir="auto">
                            <div class="post-side-top" dir="auto">
                            <h3 class="side-ttl" dir="auto">Discover new travelers</h3>
                            <div class="side-right-control" dir="auto">
                            <a href="#" class="slide-link newTravelerDiscover-prev" dir="auto"><i class="trav-angle-left" dir="auto"></i></a>
                            <a href="#" class="slide-link newTravelerDiscover-next" dir="auto"><i class="trav-angle-right" dir="auto"></i></a>
                            </div>
                            </div>
                            <div class="post-side-inner" dir="auto">
                            <div class="post-slide-wrap" dir="auto">
                            <!-- Deleted animation placeholder -->
                            <ul id="newTravelerDiscover" class="post-slider slide-same-height lightSlider lsGrab lSSlide" dir="auto" style="width: 3618px; transform: translate3d(-804px, 0px, 0px); height: 240px; padding-bottom: 0%;">
                                <li class="post-follow-card post-travel-card clone left" dir="auto" style="margin-right: 9px;">
                                <div class="follow-card-inner" dir="auto">
                                <div class="image-wrap" dir="auto">
                                <img class="lazy" alt="" style="width: 62px; height: 62px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/users/profile/1404/1596448250_image.png">
                                </div>
                                <div class="post-slider-caption" dir="auto">
                                <p class="post-card-name" title="Turi Marchesi" dir="auto">Turi Marchesi</p>
                                <p class="post-card-spec" dir="auto"></p>
                                <button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_traveller_following('unfollow',1404, this)" dir="auto">Unfollow</button>
                                <p class="post-card-follow-count" dir="auto">127 Followers</p>
                                </div>
                                </div>
                                </li><li class="post-follow-card post-travel-card clone left" dir="auto" style="margin-right: 9px;">
                                <div class="follow-card-inner" dir="auto">
                                <div class="image-wrap" dir="auto">
                                <img class="lazy" alt="" style="width: 62px; height: 62px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/users/profile/326/1594005435_image.png">
                                </div>
                                <div class="post-slider-caption" dir="auto">
                                <p class="post-card-name" title="Matthew Root" dir="auto">Matthew Root</p>
                                <p class="post-card-spec" dir="auto"></p>
                                <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_traveller_following('follow',326, this)" dir="auto">Follow</button>
                                <p class="post-card-follow-count" dir="auto">127 Followers</p>
                                </div>
                                </div>
                                </li><li class="post-follow-card post-travel-card clone left" dir="auto" style="margin-right: 9px;">
                                <div class="follow-card-inner" dir="auto">
                                <div class="image-wrap" dir="auto">
                                <img class="lazy" alt="" style="width: 62px; height: 62px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/users/profile/321/1594005130_image.png">
                                </div>
                                <div class="post-slider-caption" dir="auto">
                                <p class="post-card-name" title="Bernadette Hannah" dir="auto">Bernadette Hannah</p>
                                <p class="post-card-spec" dir="auto"></p>
                                <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_traveller_following('follow',321, this)" dir="auto">Follow</button>
                                <p class="post-card-follow-count" dir="auto">127 Followers</p>
                                </div>
                                </div>
                                </li><li class="post-follow-card post-travel-card clone left" dir="auto" style="margin-right: 9px;">
                                <div class="follow-card-inner" dir="auto">
                                <div class="image-wrap" dir="auto">
                                <img class="lazy" alt="" style="width: 62px; height: 62px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/users/profile/319/1594005029_image.png">
                                </div>
                                <div class="post-slider-caption" dir="auto">
                                <p class="post-card-name" title="Joseph Maurice" dir="auto">Joseph Maurice</p>
                                <p class="post-card-spec" dir="auto"></p>
                                <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_traveller_following('follow',319, this)" dir="auto">Follow</button>
                                <p class="post-card-follow-count" dir="auto">127 Followers</p>
                                </div>
                                </div>
                                </li>
                            </ul>
                            </div>
                            </div>
                            </div>
                            <!-- Discover new travelers slider END -->
                            <!-- Trending destinations - copied from home page -->
                            <div class="post-block discover-destinations-block" dir="auto">
                            <div class="post-side-top" dir="auto">
                            <h3 class="side-ttl">
                                <i class="trav-trending-destination-icon" dir="auto"></i> Trending destinations <span class="count">20</span>
                                <!-- New element -->
                                <div class="dropdown">
                                    <a class="hashtag-dropdown dropdown-toggle" type="button" id="hashTagDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        #Morocco
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="hashTagDropdown">
                                        <a class="dropdown-item" href="#">France</a>
                                        <a class="dropdown-item" href="#">Saudi Arabia</a>
                                        <a class="dropdown-item" href="#">United States</a>
                                    </div>
                                </div>
                                <!-- New element END -->
                            </h3>
                            <div class="side-right-control" dir="auto">
                            <a href="#" class="slide-link discoverNewDestination-prev" dir="auto"><i class="trav-angle-left" dir="auto"></i></a>
                            <a href="#" class="slide-link discoverNewDestination-next" dir="auto"><i class="trav-angle-right" dir="auto"></i></a>
                            </div>
                            </div>
                            <div class="post-side-inner" dir="auto">
                            <div class="post-slide-wrap slide-hide-right-margin" dir="auto">
                            <!-- Deleted animation placeholder -->
                            <ul id="discoverNewDestination" class="post-slider lightSlider lsGrab lSSlide" dir="auto" style="width: 2250px; transform: translate3d(-750px, 0px, 0px); height: 300px; padding-bottom: 0%;"><li dir="auto" class="clone left lslide" style="margin-right: 20px;">
                            <img class="lazy" alt="King Salman Safari Park" style="width: 230px; height: 300px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th700/places/ChIJt163w6LsLj4RUMfZMWXp8Vw/a2fa11c678327036b0317616ee0ceea3b575d1e4.jpg">
                            <div class="post-slider-caption transparent-caption" dir="auto">
                            <p class="post-slide-name" dir="auto">
                            <a href="https://travooo.com/place/3540616" style="color:white;" dir="auto">
                            King Salman Safari Park
                            </a>
                            </p>
                            <div class="post-slide-description" dir="auto">
                            <span class="tag" dir="auto">Park</span>
                            in Riyadh
                            </div>
                            </div>
                            </li><li dir="auto" class="clone left lslide" style="margin-right: 20px;">
                            <img class="lazy" alt="Thomas Jefferson Memorial" style="width: 230px; height: 300px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th700/places/ChIJu2icngu3t4kRcpjATtc3QQQ/81c7ade4ee26ca3ff969bdd906015aff5482a132.jpg">
                            <div class="post-slider-caption transparent-caption" dir="auto">
                            <p class="post-slide-name" dir="auto">
                            <a href="https://travooo.com/place/3084498" style="color:white;" dir="auto">
                            Thomas Jefferson Memorial
                            </a>
                            </p>
                            <div class="post-slide-description" dir="auto">
                            <span class="tag" dir="auto">Point of interest</span>
                            in Washington
                            </div>
                            </div>
                            </li>
                            <li dir="auto" class="lslide" style="margin-right: 20px;">
                            <img class="lazy" alt="Smithsonian Institution" style="width: 230px; height: 300px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th700/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/6ce5fc0167f799c3769ded9cded0ad1b992a80d1.jpg">
                            <div class="post-slider-caption transparent-caption" dir="auto">
                            <p class="post-slide-name" dir="auto">
                            <a href="https://travooo.com/place/3089590" style="color:white;" dir="auto">
                            Smithsonian Institution
                            </a>
                            </p>
                            <div class="post-slide-description" dir="auto">
                            <span class="tag" dir="auto">Point of interest</span>
                            in Washington
                            </div>
                            </div>
                            </li>
                            <li dir="auto" class="lslide active" style="margin-right: 20px;">
                            <img class="lazy" alt="Buckingham Palace" style="width: 230px; height: 300px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th700/places/ChIJtV5bzSAFdkgRpwLZFPWrJgo/2f5a88eba1c65cad7ae558b3e897b7145c05939b.jpg">
                            <div class="post-slider-caption transparent-caption" dir="auto">
                            <p class="post-slide-name" dir="auto">
                            <a href="https://travooo.com/place/3862876" style="color:white;" dir="auto">
                            Buckingham Palace
                            </a>
                            </p>
                            <div class="post-slide-description" dir="auto">
                            <span class="tag" dir="auto">Premise</span>
                            in London
                            </div>
                            </div>
                            </li>
                            <li dir="auto" class="lslide" style="margin-right: 20px;">
                            <img class="lazy" alt="King Salman Safari Park" style="width: 230px; height: 300px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th700/places/ChIJt163w6LsLj4RUMfZMWXp8Vw/a2fa11c678327036b0317616ee0ceea3b575d1e4.jpg">
                            <div class="post-slider-caption transparent-caption" dir="auto">
                            <p class="post-slide-name" dir="auto">
                            <a href="https://travooo.com/place/3540616" style="color:white;" dir="auto">
                            King Salman Safari Park
                            </a>
                            </p>
                            <div class="post-slide-description" dir="auto">
                            <span class="tag" dir="auto">Park</span>
                            in Riyadh
                            </div>
                            </div>
                            </li>
                            </ul>
                            </div>
                            </div>
                            </div>
                            <!-- Trending destinations END -->
                            <!-- Friends follow activity -->
                            <div class="post-block post-block-notification">
                            <div class="post-top-info-layer">
                                <div class="post-info-line">
                                    <ul class="avatar-list">
                                        <li><img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava"></li>
                                        <li><img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava"></li>
                                        <li><img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava"></li>
                                    </ul>
                                    <a class="post-name-link" href="https://travooo.com/profile/831">Katerine</a>, <a class="post-name-link" href="https://travooo.com/profile/831">Amine</a> <a class="post-name-link post-more-people" type="button" data-placement="bottom" data-toggle="popover" data-html="true">+5 others</a> followed this place
                                </div>
                                <div class="post-top-info-action">
                                <div class="dropdown">
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="trav-angle-down"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" x-placement="bottom-end" style="position: absolute; transform: translate3d(-251px, 31px, 0px); top: 0px; left: 0px; will-change: transform;">
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap">
                                            <i class="trav-user-plus-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Unfollow User</b></p>
                                            <p>Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap">
                                            <i class="trav-share-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Share</b></p>
                                            <p>Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap">
                                            <i class="trav-link"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Copy Link</b></p>
                                            <p>Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#sendToFriendModal">
                                        <span class="icon-wrap">
                                            <i class="trav-share-on-travo-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Send to a Friend</b></p>
                                            <p>Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap">
                                            <i class="trav-heart-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Add to Favorites</b></p>
                                            <p>Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap">
                                            <i class="trav-flag-icon-o"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Report</b></p>
                                            <p>Help us understand</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                            </div>
                            <!-- New element -->
                            <div class="shared-place-slider-wrap">
                                <div class="shared-place-slider">
                                    <div class="shared-place-slider-card">
                                        <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                        <div class="shared-place-slider-card-description">
                                            <div class="details">
                                                <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                                <div class="text">
                                                    <h3>New York City</h3>
                                                    <p>City in New York State, United States</p>
                                                    <div class="users">
                                                        <button class="post__comment-btn">
                                                            <div class="user-list">
                                                                <div class="user-list__item">
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                </div>
                                                            </div>
                                                            <i class="trav-comment-icon icon"></i>
                                                            <span><strong>64K</strong> Talking about this</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <button class="btn btn-light-primary orange">Book</button>
                                                <button class="btn btn-light-primary">Follow</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="shared-place-slider-card">
                                        <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                        <div class="shared-place-slider-card-description">
                                            <div class="details">
                                                <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                                <div class="text">
                                                    <h3>New York City</h3>
                                                    <p>City in New York State, United States</p>
                                                    <div class="users">
                                                        <button class="post__comment-btn">
                                                            <div class="user-list">
                                                                <div class="user-list__item">
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                </div>
                                                            </div>
                                                            <i class="trav-comment-icon icon"></i>
                                                            <span><strong>64K</strong> Talking about this</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <button class="btn btn-light-primary orange">Book</button>
                                                <button class="btn btn-light-primary">Follow</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="shared-place-slider-card">
                                        <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                        <div class="shared-place-slider-card-description">
                                            <div class="details">
                                                <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                                <div class="text">
                                                    <h3>New York City</h3>
                                                    <p>City in New York State, United States</p>
                                                    <div class="users">
                                                        <button class="post__comment-btn">
                                                            <div class="user-list">
                                                                <div class="user-list__item">
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                </div>
                                                            </div>
                                                            <i class="trav-comment-icon icon"></i>
                                                            <span><strong>64K</strong> Talking about this</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <button class="btn btn-light-primary orange">Book</button>
                                                <button class="btn btn-light-primary">Follow</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- New element END -->
                            </div>
                            <!-- Friends follow activity END -->
                            <!-- Places you might like -->
                            <div class="post-block">
                                <div class="post-side-top" >
                                    <h3 class="side-ttl" > Places you might like <span class="count" >20</span></h3>
                                    <div class="side-right-control" >
                                        <a href="#" class="slide-link places-you-might-like-slider-prev" ><i class="trav-angle-left" ></i></a>
                                        <a href="#" class="slide-link places-you-might-like-slider-next" ><i class="trav-angle-right" ></i></a>
                                    </div>
                                </div>
                                <div class="post-side-inner">
                                    <div class="post-slide-wrap slide-hide-right-margin">
                                        <ul class="post-slider places-you-might-like-slider">
                                            <li>
                                                <div class="post-popular-inner" >
                                                    <div class="img-wrap" >
                                                        <a style="text-decoration: none;" href="https://travooo.com/profile/591" >
                                                        <img style="background-image: url(https://travooo.com/assets2/image/placeholders/male.png)" src="https://s3.amazonaws.com/travooo-images2/users/profile/th180/591/1593919828_image.png?v=1600942403" alt="" >
                                                    </a>
                                                    </div>
                                                    <div class="pop-txt" >
                                                        <span class="location-badge">
                                                            <i class="trav-set-location-icon"></i>
                                                        </span>
                                                        <h5><a style="text-decoration: none;" href="https://travooo.com/profile/591">Walt Disnay World</a></h5>
                                                        <div class="desc"><span>Park</span> in Florida</div>
                                                        <div class="reactions">
                                                            <button class="post__comment-btn">
                                                                <div class="user-list">
                                                                    <div class="user-list__item">
                                                                        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                    </div>
                                                                </div>
                                                                <span><strong>64K</strong> Talking about this</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="post-popular-inner" >
                                                    <div class="img-wrap" >
                                                        <a style="text-decoration: none;" href="https://travooo.com/profile/591" >
                                                        <img style="background-image: url(https://travooo.com/assets2/image/placeholders/male.png)" src="https://s3.amazonaws.com/travooo-images2/users/profile/th180/591/1593919828_image.png?v=1600942403" alt="" >
                                                    </a>
                                                    </div>
                                                    <div class="pop-txt" >
                                                        <span class="location-badge">
                                                            <i class="trav-set-location-icon"></i>
                                                        </span>
                                                        <h5><a style="text-decoration: none;" href="https://travooo.com/profile/591">Walt Disnay World</a></h5>
                                                        <div class="desc"><span>Park</span> in Florida</div>
                                                        <div class="reactions">
                                                            <button class="post__comment-btn">
                                                                <div class="user-list">
                                                                    <div class="user-list__item">
                                                                        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                    </div>
                                                                </div>
                                                                <span><strong>64K</strong> Talking about this</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="post-popular-inner" >
                                                    <div class="img-wrap" >
                                                        <a style="text-decoration: none;" href="https://travooo.com/profile/591" >
                                                        <img style="background-image: url(https://travooo.com/assets2/image/placeholders/male.png)" src="https://s3.amazonaws.com/travooo-images2/users/profile/th180/591/1593919828_image.png?v=1600942403" alt="" >
                                                    </a>
                                                    </div>
                                                    <div class="pop-txt" >
                                                        <span class="location-badge">
                                                            <i class="trav-set-location-icon"></i>
                                                        </span>
                                                        <h5><a style="text-decoration: none;" href="https://travooo.com/profile/591">Walt Disnay World</a></h5>
                                                        <div class="desc"><span>Park</span> in Florida</div>
                                                        <div class="reactions">
                                                            <button class="post__comment-btn">
                                                                <div class="user-list">
                                                                    <div class="user-list__item">
                                                                        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                    </div>
                                                                </div>
                                                                <span><strong>64K</strong> Talking about this</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="post-popular-inner" >
                                                    <div class="img-wrap" >
                                                        <a style="text-decoration: none;" href="https://travooo.com/profile/591" >
                                                        <img style="background-image: url(https://travooo.com/assets2/image/placeholders/male.png)" src="https://s3.amazonaws.com/travooo-images2/users/profile/th180/591/1593919828_image.png?v=1600942403" alt="" >
                                                    </a>
                                                    </div>
                                                    <div class="pop-txt" >
                                                        <h5><a style="text-decoration: none;" href="https://travooo.com/profile/591">Walt Disnay World</a></h5>
                                                        <div class="desc"><span>Park</span> in Florida</div>
                                                        <div class="reactions">
                                                            <button class="post__comment-btn">
                                                                <div class="user-list">
                                                                    <div class="user-list__item">
                                                                        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                    </div>
                                                                </div>
                                                                <span><strong>64K</strong> Talking about this</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="post-popular-inner" >
                                                    <div class="img-wrap" >
                                                        <a style="text-decoration: none;" href="https://travooo.com/profile/591" >
                                                        <img style="background-image: url(https://travooo.com/assets2/image/placeholders/male.png)" src="https://s3.amazonaws.com/travooo-images2/users/profile/th180/591/1593919828_image.png?v=1600942403" alt="" >
                                                    </a>
                                                    </div>
                                                    <div class="pop-txt" >
                                                        <h5><a style="text-decoration: none;" href="https://travooo.com/profile/591">Walt Disnay World</a></h5>
                                                        <div class="desc"><span>Park</span> in Florida</div>
                                                        <div class="reactions">
                                                            <button class="post__comment-btn">
                                                                <div class="user-list">
                                                                    <div class="user-list__item">
                                                                        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                    </div>
                                                                </div>
                                                                <span><strong>64K</strong> Talking about this</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Places you might like END -->
                            <!-- Recommended places to follow -->
                            <div class="post-block">
                                <div class="post-side-top" >
                                    <h3 class="side-ttl" ><i class="fas fa-thumbs-up"></i> Recommended Places to Follow <span class="count" >20</span></h3>
                                    <div class="side-right-control" >
                                        <a href="#" class="slide-link recommended-places-slider-prev" ><i class="trav-angle-left" ></i></a>
                                        <a href="#" class="slide-link recommended-places-slider-next" ><i class="trav-angle-right" ></i></a>
                                    </div>
                                </div>
                                <div class="post-side-inner">
                                    <div class="post-slide-wrap slide-hide-right-margin">
                                        <ul class="post-slider recommended-places-slider">
                                            <li>
                                                <a href="https://travooo.com/place/1957669" target="_blank" >
                                                    <img class="lazy" alt="Walt Disney World Resort" style="width: 230px; height: 300px;"  src="https://s3.amazonaws.com/travooo-images2/th1100/places/ChIJ96XKNOZ-3YgRoPEc0B85Hqc/8eeb7505b3bdcba81e861444cc042e7d2a39a0c7.jpg">
                                                    <div class="post-slider-caption transparent-caption" >
                                                        <p class="post-slide-name" >Walt Disney World Resort</p>
                                                        <div class="post-slide-description" >
                                                            <i class="trav-set-location-icon"></i> New York City
                                                        </div>
                                                    </div>
                                                    <div class="place-badge">City</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://travooo.com/place/1957669" target="_blank" >
                                                    <img class="lazy" alt="Walt Disney World Resort" style="width: 230px; height: 300px;"  src="https://s3.amazonaws.com/travooo-images2/th1100/places/ChIJLU7jZClu5kcR4PcOOO6p3I0/486135422c81a4ae8c79a7fb69d2aa6f5a8fc64a.jpg">
                                                    <div class="post-slider-caption transparent-caption" >
                                                        <p class="post-slide-name" >Walt Disney World Resort</p>
                                                        <div class="post-slide-description" >
                                                            <i class="trav-set-location-icon"></i> Japan
                                                        </div>
                                                    </div>
                                                    <div class="place-badge">Country</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://travooo.com/place/1957669" target="_blank" >
                                                    <img class="lazy" alt="Walt Disney World Resort" style="width: 230px; height: 300px;"  src="https://s3.amazonaws.com/travooo-images2/th1100/places/ChIJ96XKNOZ-3YgRoPEc0B85Hqc/8eeb7505b3bdcba81e861444cc042e7d2a39a0c7.jpg">
                                                    <div class="post-slider-caption transparent-caption" >
                                                        <p class="post-slide-name" >Walt Disney World Resort</p>
                                                        <div class="post-slide-description" >
                                                            <i class="trav-set-location-icon"></i> France
                                                        </div>
                                                    </div>
                                                    <div class="place-badge">Country</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://travooo.com/place/1957669" target="_blank" >
                                                    <img class="lazy" alt="Walt Disney World Resort" style="width: 230px; height: 300px;"  src="https://s3.amazonaws.com/travooo-images2/th1100/places/ChIJLU7jZClu5kcR4PcOOO6p3I0/486135422c81a4ae8c79a7fb69d2aa6f5a8fc64a.jpg">
                                                    <div class="post-slider-caption transparent-caption" >
                                                        <p class="post-slide-name" >Walt Disney World Resort</p>
                                                        <div class="post-slide-description" >
                                                            <i class="trav-set-location-icon"></i> Asia
                                                        </div>
                                                    </div>
                                                    <div class="place-badge">Asia</div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Recommended places to follow END -->
                            <!-- Primary post block - text -->
                            <div class="post-block" >
                            <div class="post-top-info-layer" >
                            <div class="post-top-info-wrap" >
                            <div class="post-top-avatar-wrap" >
                            <a class="post-name-link" href="https://travooo.com/profile/831" >
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                            </a>
                            </div>
                            <div class="post-top-info-txt" >
                            <div class="post-top-name" >
                            <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                            <span class="exp-icon" >EXP</span>
                            </div>
                            <div class="post-info" >
                            15 July at 4:35am
                            <!-- New element -->
                            <!-- privacy settings -->
                            <div class="dropdown privacy-settings" style="position: relative">
                                <button class="btn btn--sm btn--outline dropdown-toggle" data-toggle="dropdown">
                                    <i class="trav-globe-icon" ></i>
                                    PUBLIC
                                </button>
                                <div class="dropdown-menu dropdown-menu-left permissoin_show hided">
                                    <a class="dropdown-item" href="#">
                                        <span class="icon-wrap">
                                            <i class="trav-globe-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Public</b></p>
                                            <p>Anyone can see this post</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <span class="icon-wrap">
                                            <i class="trav-users-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Friends Only</b></p>
                                            <p>Only you and your friends can see this post</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <span class="icon-wrap">
                                            <i class="trav-lock-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Only Me</b></p>
                                            <p>Only you can see this post</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- privacy settings -->
                            <!-- New element END -->
                            </div>
                            </div>
                            </div>
                            <!-- New elements -->
                            <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap" >
                                            <i class="trav-user-plus-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Unfollow User</b></p>
                                            <p >Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap" >
                                            <i class="trav-share-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Share</b></p>
                                            <p >Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap" >
                                            <i class="trav-link" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Copy Link</b></p>
                                            <p >Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#sendToFriendModal">
                                        <span class="icon-wrap" >
                                            <i class="trav-share-on-travo-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Send to a Friend</b></p>
                                            <p >Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap" >
                                            <i class="trav-heart-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Add to Favorites</b></p>
                                            <p >Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap" >
                                            <i class="trav-flag-icon-o" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Report</b></p>
                                            <p >Help us understand</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                                <!-- New elements END -->
                            </div>
                            <!-- New element -->
                                <style>
                                    .btn-toggle-translate {
                                        color: #1a1a1a;
                                        font-size: calc(14 / 16 * 1em);
                                        cursor: pointer;
                                        margin-bottom: 0;
                                    }
                                    .btn-toggle-translate span {
                                        font-size: inherit!important;
                                    }
                                    .btn-toggle-translate .is-shown {
                                        display: none;
                                    }
                                    .translate-content {
                                        display: none;
                                        font-family: sans-serif;
                                        border-left: 3px solid #dddddd;
                                        padding-left: 1em;
                                    }
                                    #translate-toggle:checked ~ .post-txt-wrap .translate-content,
                                    #translate-toggle:checked ~ .post-txt-wrap ~ .post-footer-info .btn-toggle-translate .is-shown{
                                        display: block;
                                    }
                                    #translate-toggle:checked ~ .post-txt-wrap ~ .post-footer-info .btn-toggle-translate .is-hidden {
                                        display: none;
                                    }
                                </style>
                                <input type="checkbox" id="translate-toggle" hidden="hidden">
                            <div class="post-txt-wrap">
                                <p class="translate-content">
                                    Перевод текста Перевод текста Перевод текста Перевод текста Перевод текста Перевод текста Перевод текста Перевод текста
                                </p>
                                <div>
                                    <span class="less-content disc-ml-content">Dolor sit amet <span class="post-text-tag" type="button" data-placement="bottom" data-toggle="popover" data-html="true">Amine</span> consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                    <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet <span class="post-text-tag" type="button" data-placement="bottom" data-toggle="popover" data-html="true">Amine</span> consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                </div>
                            </div>
                            <div class="check-in-point">
                                <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                            </div>
                            <!-- Post text tag tooltip content -->
                            <div id="popover-content" style="display: none;">
                                <div class="user-hero">
                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                                    <div class="user-hero-text">
                                        <div class="name">Sue Perez</div>
                                        <div class="location">
                                            <i class="trav-set-location-icon"></i>
                                            United States
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-light-primary btn-bordered">Follow </button>
                                </div>
                                <div class="user-badges">
                                    <div class="badges-ttl">Badges</div>
                                    <div class="badges-list">
                                        <img src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
                                        <img src="https://travooo.com/assets2/image/badges/5_1.png" alt="Personality">
                                        <img src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
                                        <img class="fade" src="https://travooo.com/assets2/image/badges/5_1.png" alt="Personality">
                                        <img class="fade" src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
                                    </div>
                                    <button type="button" class="btn btn-light-primary btn-bordered">Follow </button>
                                </div>
                                <div class="actions">
                                    <a class="btn btn-light-grey">Follow</a>
                                    <a class="btn btn-light-grey popover-msg-btn">Message</a>
                                </div>
                            </div>
                            <!-- Post text tag tooltip content END -->
                            <!-- New element END -->
                            <div class="post-image-container" >
                            </div>
                            <div class="post-footer-info" >
                            <!-- Updated elements -->
                                <div class="post-foot-block">
                                    <label for="translate-toggle" class="btn-toggle-translate">
                                        <span class="is-shown">
                                            Hide translate
                                        </span>
                                        <span class="is-hidden">
                                            Show translate
                                        </span>
                                    </label>
                                </div>
                            <div class="post-foot-block post-reaction">
                                <span class="post_like_button" id="601451">
                                    <a href="#">
                                        <i class="trav-heart-fill-icon"></i>
                                    </a>
                                </span>
                                <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                    <b >0</b> Likes
                                </span>
                            </div>
                            <!-- Updated element END -->
                            <div class="post-foot-block" >
                            <a href="#" data-tab="comments601451" >
                            <i class="trav-comment-icon" ></i>
                            </a>
                            <ul class="foot-avatar-list" >   
                                <li>
                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                </li>
                                <li>
                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                </li>
                                <li>
                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                </li>          
                            </ul>
                            <span ><a href="#" data-tab="comments601451" ><span class="601451-comments-count" >0</span> Comments</a></span>
                            </div>
                            <div class="post-foot-block ml-auto" >
                            <span class="post_share_button" id="601451" >
                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                            <i class="trav-share-icon" ></i>
                            </a>
                            </span>
                            <span id="post_share_count_601451" ><a href="#" data-tab="shares601451" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                            </div>
                            </div>
                            <div class="post-comment-wrapper" id="following601451" >
                            </div>
                            <!-- Copied from home page -->
                            <div class="post-comment-layer" data-content="comments622665" style="display: block;" dir="auto">
                            <div class="post-comment-top-info" dir="auto">
                            <ul class="comment-filter" dir="auto">
                            <li onclick="commentSort('Top', this, $(this).closest('.post-block'))" dir="auto">Top</li>
                            <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))" dir="auto">New</li>
                            </ul>
                            <div class="comm-count-info" dir="auto">
                            <strong dir="auto">2</strong> / <span class="622665-comments-count" dir="auto">2</span>
                            </div>
                            </div>
                            <div class="post-comment-wrapper sortBody" id="comments622665" dir="auto" travooo-comment-rows="2" travooo-current-page="1">
                            <div topsort="0" newsort="1601563217" dir="auto" class="displayed" style="">
                            <div class="post-comment-row news-feed-comment" id="commentRow13235" dir="auto">
                            <div class="post-com-avatar-wrap" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" dir="auto">
                            </div>
                            <div class="post-comment-text" dir="auto">
                            <div class="post-com-name-layer" dir="auto">
                            <a href="#" class="comment-name" dir="auto">Ivan Turlakov</a>
                            <div class="post-com-top-action" dir="auto">
                            <div class="dropdown " dir="auto" style="display: none;">
                            <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                            <i class="trav-angle-down" dir="auto"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment" dir="auto">
                            <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="13235" data-post="622665" dir="auto">
                            <span class="icon-wrap" dir="auto">
                            <i class="trav-pencil" aria-hidden="true" dir="auto"></i>
                            </span>
                            <div class="drop-txt comment-edit__drop-text" dir="auto">
                            <p dir="auto"><b dir="auto">Edit</b></p>
                            </div>
                            </a>
                            <a href="javascript:;" class="dropdown-item postCommentDelete" id="13235" data-post="622665" data-type="1" dir="auto">
                            <span class="icon-wrap" dir="auto">
                            <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;" dir="auto">
                            </span>
                            <div class="drop-txt comment-delete__drop-text" dir="auto">
                            <p dir="auto"><b dir="auto">Delete</b></p>
                            </div>
                            </a>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="comment-txt comment-text-13235" dir="auto">
                            <p dir="auto"></p>
                            <form class="commentEditForm13235 comment-edit-form d-none" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto"><input type="hidden" data-id="pair13235" name="pair" value="5f770bda7bc30" dir="auto">
                            <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0" dir="auto">
                            <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                            <textarea name="text" data-id="text13235" class="textarea-customize post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="Write a comment" dir="auto"></textarea>
                            </div>
                            <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                            <div class="post-alloptions" dir="auto">
                            <ul class="create-link-list" dir="auto">
                            <li class="post-options" dir="auto">
                            <input type="file" name="file[]" class="commenteditfile" id="commenteditfile13235" style="display:none" multiple="" dir="auto">
                            <i class="fa fa-camera click-target" data-target="commenteditfile13235" dir="auto"></i>
                            </li>
                            </ul>
                            </div>
                            <div class="comment-edit-action" dir="auto">
                            <a href="javascript:;" class="edit-cancel-link" data-comment_id="13235" dir="auto">Cancel</a>
                            <a href="javascript:;" class="edit-post-link" data-comment_id="13235" dir="auto">Post</a>
                            </div>
                            </div>
                            <div class="medias p-media b-whitesmoke" dir="auto">
                            <div class="img-wrap-newsfeed" dir="auto">
                            <div dir="auto">
                            <img class="thumb" src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" dir="auto">
                            </div>
                            <span class="close remove-media-file" data-media_id="14592485" dir="auto">
                            <span dir="auto">×</span>
                            </span></div>
                            </div>
                            </div>
                            <input type="hidden" name="post_id" value="622665" dir="auto">
                            <input type="hidden" name="comment_id" value="13235" dir="auto">
                            <input type="hidden" name="comment_type" value="1" dir="auto">
                            <button type="submit" class="d-none" dir="auto"></button>
                            </form>
                            </div>
                            <div class="post-image-container" dir="auto">
                            <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;" dir="auto">
                            <li style="overflow: hidden;margin:1px;" dir="auto">
                            <a href="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" data-lightbox="comment__media13235" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" style="width:192px;height:210px;object-fit: cover;" dir="auto">
                            </a>
                            </li>
                            </ul></div>
                            <div class="comment-bottom-info" dir="auto">
                            <div class="comment-info-content" dir="auto">
                            <div class="dropdown" dir="auto">
                            <a href="#" class="postCommentLikes like-link dropbtn" id="13235" dir="auto"><i class="trav-heart-fill-icon " aria-hidden="true" dir="auto"></i> <span class="comment-like-count" dir="auto">0</span></a>
                            </div>
                            <a href="#" class="postCommentReply reply-link" id="13235" dir="auto">Reply</a>
                            <span class="com-time" dir="auto"><span class="comment-dot" dir="auto"> · </span>20 hours ago</span>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="post-add-comment-block" id="replyForm13235" style="padding-top:0px;padding-left: 59px;display:none;" dir="auto">
                            <div class="avatar-wrap" style="padding-right:10px" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" style="width:30px !important;height:30px !important;" dir="auto">
                            </div>
                            <div class="post-add-com-inputs" dir="auto">
                            <form class="commentReplyForm13235" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto">
                            <input type="hidden" data-id="pair13235" name="pair" value="5f770bda7cebc" dir="auto">
                            <div class="post-create-block post-comment-create-block post-reply-block" tabindex="0" dir="auto">
                            <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                            <textarea name="text" data-id="text13235" class="textarea-customize post-comment-emoji" style="display: none; vertical-align: top; min-height: 50px;" placeholder="Write a comment" dir="auto"></textarea><div class="note-editor note-frame panel panel-default"><div class="note-dropzone">  <div class="note-dropzone-message" dir="auto"></div></div><div class="note-toolbar panel-heading" role="toolbar" dir="auto"><div class="note-btn-group btn-group note-insert"><button type="button" class="note-btn btn btn-default btn-sm" role="button" tabindex="-1" dir="auto"><div class="emoji-picker-container emoji-picker emojionearea-button d-none" dir="auto"></div></button><div class="emoji-menu b8bffcba-32a1-48ce-98ea-dcf4a8fbf53b" style="display: none;">
                                <div class="emoji-items-wrap1" dir="auto">
                                    <table class="emoji-menu-tabs" dir="auto">
                                        <tbody dir="auto">
                                        <tr dir="auto">
                                            <td dir="auto"><a class="emoji-menu-tab icon-recent-selected" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-smile" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-flower" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-bell" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-car" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-grid" dir="auto"></a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="emoji-items-wrap mobile_scrollable_wrap" dir="auto">
                                        <div class="emoji-items" dir="auto"><a href="javascript:void(0)" title=":flushed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -350px 0px no-repeat;background-size:675px 175px;" alt=":flushed:" dir="auto"><span class="label" dir="auto">:flushed:</span></a><a href="javascript:void(0)" title=":grin:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -375px 0px no-repeat;background-size:675px 175px;" alt=":grin:" dir="auto"><span class="label" dir="auto">:grin:</span></a><a href="javascript:void(0)" title=":rage:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px -25px no-repeat;background-size:675px 175px;" alt=":rage:" dir="auto"><span class="label" dir="auto">:rage:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -300px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_closed_eyes:" dir="auto"><span class="label" dir="auto">:stuck_out_tongue_closed_eyes:</span></a><a href="javascript:void(0)" title=":kissing_heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px 0px no-repeat;background-size:675px 175px;" alt=":kissing_heart:" dir="auto"><span class="label" dir="auto">:kissing_heart:</span></a><a href="javascript:void(0)" title=":ok_hand:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -650px -75px no-repeat;background-size:675px 175px;" alt=":ok_hand:" dir="auto"><span class="label" dir="auto">:ok_hand:</span></a><a href="javascript:void(0)" title=":heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -250px -150px no-repeat;background-size:675px 175px;" alt=":heart:" dir="auto"><span class="label" dir="auto">:heart:</span></a><a href="javascript:void(0)" title=":frog:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_1.png') -150px 0px no-repeat;background-size:725px 100px;" alt=":frog:" dir="auto"><span class="label" dir="auto">:frog:</span></a><a href="javascript:void(0)" title=":+1:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -75px no-repeat;background-size:675px 175px;" alt=":+1:" dir="auto"><span class="label" dir="auto">:+1:</span></a><a href="javascript:void(0)" title=":heart_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -150px 0px no-repeat;background-size:675px 175px;" alt=":heart_eyes:" dir="auto"><span class="label" dir="auto">:heart_eyes:</span></a><a href="javascript:void(0)" title=":blush:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px 0px no-repeat;background-size:675px 175px;" alt=":blush:" dir="auto"><span class="label" dir="auto">:blush:</span></a><a href="javascript:void(0)" title=":joy:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -550px 0px no-repeat;background-size:675px 175px;" alt=":joy:" dir="auto"><span class="label" dir="auto">:joy:</span></a><a href="javascript:void(0)" title=":relaxed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -100px 0px no-repeat;background-size:675px 175px;" alt=":relaxed:" dir="auto"><span class="label" dir="auto">:relaxed:</span></a><a href="javascript:void(0)" title=":pensive:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -400px 0px no-repeat;background-size:675px 175px;" alt=":pensive:" dir="auto"><span class="label" dir="auto">:pensive:</span></a><a href="javascript:void(0)" title=":smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px 0px no-repeat;background-size:675px 175px;" alt=":smile:" dir="auto"><span class="label" dir="auto">:smile:</span></a><a href="javascript:void(0)" title=":sob:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -575px 0px no-repeat;background-size:675px 175px;" alt=":sob:" dir="auto"><span class="label" dir="auto">:sob:</span></a><a href="javascript:void(0)" title=":kiss:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px -150px no-repeat;background-size:675px 175px;" alt=":kiss:" dir="auto"><span class="label" dir="auto">:kiss:</span></a><a href="javascript:void(0)" title=":unamused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -450px 0px no-repeat;background-size:675px 175px;" alt=":unamused:" dir="auto"><span class="label" dir="auto">:unamused:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_winking_eye:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_winking_eye:" dir="auto"><span class="label" dir="auto">:stuck_out_tongue_winking_eye:</span></a><a href="javascript:void(0)" title=":see_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px -75px no-repeat;background-size:675px 175px;" alt=":see_no_evil:" dir="auto"><span class="label" dir="auto">:see_no_evil:</span></a><a href="javascript:void(0)" title=":wink:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px 0px no-repeat;background-size:675px 175px;" alt=":wink:" dir="auto"><span class="label" dir="auto">:wink:</span></a><a href="javascript:void(0)" title=":smiley:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -25px 0px no-repeat;background-size:675px 175px;" alt=":smiley:" dir="auto"><span class="label" dir="auto">:smiley:</span></a><a href="javascript:void(0)" title=":cry:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -525px 0px no-repeat;background-size:675px 175px;" alt=":cry:" dir="auto"><span class="label" dir="auto">:cry:</span></a><a href="javascript:void(0)" title=":scream:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -25px no-repeat;background-size:675px 175px;" alt=":scream:" dir="auto"><span class="label" dir="auto">:scream:</span></a><a href="javascript:void(0)" title=":smirk:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px -50px no-repeat;background-size:675px 175px;" alt=":smirk:" dir="auto"><span class="label" dir="auto">:smirk:</span></a><a href="javascript:void(0)" title=":disappointed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px 0px no-repeat;background-size:675px 175px;" alt=":disappointed:" dir="auto"><span class="label" dir="auto">:disappointed:</span></a><a href="javascript:void(0)" title=":sweat_smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px -25px no-repeat;background-size:675px 175px;" alt=":sweat_smile:" dir="auto"><span class="label" dir="auto">:sweat_smile:</span></a><a href="javascript:void(0)" title=":kissing_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -200px 0px no-repeat;background-size:675px 175px;" alt=":kissing_closed_eyes:" dir="auto"><span class="label" dir="auto">:kissing_closed_eyes:</span></a><a href="javascript:void(0)" title=":speak_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -75px no-repeat;background-size:675px 175px;" alt=":speak_no_evil:" dir="auto"><span class="label" dir="auto">:speak_no_evil:</span></a><a href="javascript:void(0)" title=":relieved:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -425px 0px no-repeat;background-size:675px 175px;" alt=":relieved:" dir="auto"><span class="label" dir="auto">:relieved:</span></a><a href="javascript:void(0)" title=":grinning:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px 0px no-repeat;background-size:675px 175px;" alt=":grinning:" dir="auto"><span class="label" dir="auto">:grinning:</span></a><a href="javascript:void(0)" title=":yum:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px -25px no-repeat;background-size:675px 175px;" alt=":yum:" dir="auto"><span class="label" dir="auto">:yum:</span></a><a href="javascript:void(0)" title=":neutral_face:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -25px no-repeat;background-size:675px 175px;" alt=":neutral_face:" dir="auto"><span class="label" dir="auto">:neutral_face:</span></a><a href="javascript:void(0)" title=":confused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -625px -25px no-repeat;background-size:675px 175px;" alt=":confused:" dir="auto"><span class="label" dir="auto">:confused:</span></a></div>
                                    </div>
                                </div>
                            </div></div><div class="note-btn-group btn-group note-custom"></div></div><div class="note-editing-area" dir="auto"><div class="note-placeholder custom-placeholder" style="display: none;">Write a comment</div><div class="note-handle"><div class="note-control-selection" dir="auto"><div class="note-control-selection-bg" dir="auto"></div><div class="note-control-holder note-control-nw" dir="auto"></div><div class="note-control-holder note-control-ne" dir="auto"></div><div class="note-control-holder note-control-sw" dir="auto"></div><div class="note-control-sizing note-control-se" dir="auto"></div><div class="note-control-selection-info" dir="auto"></div></div></div><textarea class="note-codable" role="textbox" aria-multiline="true" dir="auto"></textarea><div class="note-editable" contenteditable="true" role="textbox" aria-multiline="true" dir="auto" spellcheck="true"><div><br dir="auto"></div></div></div><output class="note-status-output" aria-live="polite" dir="auto"></output><div class="note-statusbar" role="status" dir="auto" style="">  <div class="note-resizebar" role="seperator" aria-orientation="horizontal" aria-label="Resize" dir="auto">    <div class="note-icon-bar" dir="auto"></div>    <div class="note-icon-bar" dir="auto"></div>    <div class="note-icon-bar" dir="auto"></div>  </div></div><div class="modal link-dialog" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Link"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Link</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group" dir="auto"><label class="note-form-label" dir="auto">Text to display</label><input class="note-link-text form-control note-form-control note-input" type="text" dir="auto"></div><div class="form-group note-form-group" dir="auto"><label class="note-form-label" dir="auto">To what URL should this link go?</label><input class="note-link-url form-control note-form-control note-input" type="text" value="http://" dir="auto"></div><div class="checkbox sn-checkbox-open-in-new-window" dir="auto"><label dir="auto"> <input role="checkbox" type="checkbox" checked="" aria-checked="true" dir="auto">Open in new window</label></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-link-btn" value="Insert Link" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Image"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Image</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group note-group-select-from-files" dir="auto"><label class="note-form-label" dir="auto">Select from files</label><input class="note-image-input form-control-file note-form-control note-input" type="file" name="files" accept="image/*" multiple="multiple" dir="auto"></div><div class="form-group note-group-image-url" style="overflow:auto;" dir="auto"><label class="note-form-label" dir="auto">Image URL</label><input class="note-image-url form-control note-form-control note-input  col-md-12" type="text" dir="auto"></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-image-btn" value="Insert Image" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Video"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Video</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group row-fluid" dir="auto"><label class="note-form-label" dir="auto">Video URL <small class="text-muted" dir="auto">(YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)</small></label><input class="note-video-url form-control note-form-control note-input" type="text" dir="auto"></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-video-btn" value="Insert Video" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Help"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Help</h4>    </div>    <div class="modal-body" dir="auto" style="max-height: 300px; overflow: scroll;"><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+Z</kbd></label><span dir="auto">Undoes the last command</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+Y</kbd></label><span dir="auto">Redoes the last command</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">TAB</kbd></label><span dir="auto">Tab</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">SHIFT+TAB</kbd></label><span dir="auto">Untab</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+B</kbd></label><span dir="auto">Set a bold style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+I</kbd></label><span dir="auto">Set a italic style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+U</kbd></label><span dir="auto">Set a underline style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+S</kbd></label><span dir="auto">Set a strikethrough style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+BACKSLASH</kbd></label><span dir="auto">Clean a style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+L</kbd></label><span dir="auto">Set left align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+E</kbd></label><span dir="auto">Set center align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+R</kbd></label><span dir="auto">Set right align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+J</kbd></label><span dir="auto">Set full align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+NUM7</kbd></label><span dir="auto">Toggle unordered list</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+NUM8</kbd></label><span dir="auto">Toggle ordered list</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+LEFTBRACKET</kbd></label><span dir="auto">Outdent on current paragraph</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+RIGHTBRACKET</kbd></label><span dir="auto">Indent on current paragraph</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM0</kbd></label><span dir="auto">Change current block's format as a paragraph(P tag)</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM1</kbd></label><span dir="auto">Change current block's format as H1</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM2</kbd></label><span dir="auto">Change current block's format as H2</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM3</kbd></label><span dir="auto">Change current block's format as H3</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM4</kbd></label><span dir="auto">Change current block's format as H4</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM5</kbd></label><span dir="auto">Change current block's format as H5</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM6</kbd></label><span dir="auto">Change current block's format as H6</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+ENTER</kbd></label><span dir="auto">Insert horizontal rule</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+K</kbd></label><span dir="auto">Show Link Dialog</span></div>    <div class="modal-footer" dir="auto"><p class="text-center" dir="auto"><a href="http://summernote.org/" target="_blank" dir="auto">Summernote 0.8.12</a> · <a href="https://github.com/summernote/summernote" target="_blank" dir="auto">Project</a> · <a href="https://github.com/summernote/summernote/issues" target="_blank" dir="auto">Issues</a></p></div>  </div></div></div></div>
                            <div class="textcomplete-wrapper"></div></div>
                            <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                            <div class="post-alloptions" dir="auto">
                            <ul class="create-link-list" dir="auto">
                            <li class="post-options" dir="auto">
                            <input type="file" name="file[]" id="commentreplyfile13235" style="display:none" multiple="" dir="auto">
                            <i class="fa fa-camera click-target" data-target="commentreplyfile13235" dir="auto"></i>
                            </li>
                            </ul>
                            </div>
                            <button type="submit" class="btn btn-primary d-none" dir="auto"></button>
                            <div class="comment-edit-action" dir="auto">
                            <a href="javascript:;" class="p-comment-cancel-link post-r-cancel-link" dir="auto">Cancel</a>
                            <a href="javascript:;" class="p-comment-link post-r-reply-comment-link" data-comment_id="13235" dir="auto">Post</a>
                            </div>
                            </div>
                            <div class="medias p-media b-whitesmoke" dir="auto"></div>
                            </div>
                            <input type="hidden" name="post_id" value="622665" dir="auto">
                            <input type="hidden" name="comment_id" value="13235" dir="auto">
                            </form>
                            </div>
                            </div>
                            </div>
                            <div topsort="1" newsort="1601563197" dir="auto" class="displayed" style="">
                            <div class="post-comment-row news-feed-comment" id="commentRow13234" dir="auto">
                            <div class="post-com-avatar-wrap" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" dir="auto">
                            </div>
                            <div class="post-comment-text" dir="auto">
                            <div class="post-com-name-layer" dir="auto">
                            <a href="#" class="comment-name" dir="auto">Ivan Turlakov</a>
                            <div class="post-com-top-action" dir="auto">
                            <div class="dropdown " dir="auto">
                            <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                            <i class="trav-angle-down" dir="auto"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment" dir="auto">
                            <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="13234" data-post="622665" dir="auto">
                            <span class="icon-wrap" dir="auto">
                            <i class="trav-pencil" aria-hidden="true" dir="auto"></i>
                            </span>
                            <div class="drop-txt comment-edit__drop-text" dir="auto">
                            <p dir="auto"><b dir="auto">Edit</b></p>
                            </div>
                            </a>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="comment-txt comment-text-13234" dir="auto">
                            <p dir="auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id erat
a velit mollis consectetur nec quis nunc. Morbi a neque vel lacus tincidunt
vehicula non non justo.</p>
                            <form class="commentEditForm13234 comment-edit-form d-none" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto"><input type="hidden" data-id="pair13234" name="pair" value="5f770bda7ee63" dir="auto">
                            <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0" dir="auto">
                            <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                            <textarea name="text" data-id="text13234" class="textarea-customize post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="Write a comment" dir="auto">Text comment</textarea>
                            </div>
                            <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                            <div class="post-alloptions" dir="auto">
                            <ul class="create-link-list" dir="auto">
                            <li class="post-options" dir="auto">
                            <input type="file" name="file[]" class="commenteditfile" id="commenteditfile13234" style="display:none" multiple="" dir="auto">
                            <i class="fa fa-camera click-target" data-target="commenteditfile13234" dir="auto"></i>
                            </li>
                            </ul>
                            </div>
                            <div class="comment-edit-action" dir="auto">
                            <a href="javascript:;" class="edit-cancel-link" data-comment_id="13234" dir="auto">Cancel</a>
                            <a href="javascript:;" class="edit-post-link" data-comment_id="13234" dir="auto">Post</a>
                            </div>
                            </div>
                            <div class="medias p-media b-whitesmoke" dir="auto">
                            </div>
                            </div>
                            <input type="hidden" name="post_id" value="622665" dir="auto">
                            <input type="hidden" name="comment_id" value="13234" dir="auto">
                            <input type="hidden" name="comment_type" value="1" dir="auto">
                            <button type="submit" class="d-none" dir="auto"></button>
                            </form>
                            </div>
                            <div class="post-image-container" dir="auto">
                            </div>
                            <div class="comment-bottom-info" dir="auto">
                            <div class="comment-info-content" dir="auto">
                            <div class="dropdown" dir="auto">
                            <a href="#" class="postCommentLikes like-link dropbtn" id="13234" dir="auto"><i class="trav-heart-fill-icon red" aria-hidden="true" dir="auto"></i> <span class="comment-like-count" dir="auto">0</span></a>
                            </div>
                            <a href="#" class="postCommentReply reply-link" id="13234" dir="auto">Reply</a>
                            <span class="com-time" dir="auto"><span class="comment-dot" dir="auto"> · </span>20 hours ago</span>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="post-comment-row doublecomment post-comment-reply" id="commentRow13236" data-parent_id="13234" dir="auto">
                            <div class="post-com-avatar-wrap" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" dir="auto">
                            </div>
                            <div class="post-comment-text" dir="auto">
                            <div class="post-com-name-layer" dir="auto">
                            <a href="#" class="comment-name" dir="auto">Ivan Turlakov</a>
                            <div class="post-com-top-action" dir="auto">
                            <div class="dropdown" dir="auto" style="display: none;">
                            <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                            <i class="trav-angle-down" dir="auto"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment" dir="auto">
                            <a href="#" class="dropdown-item post-edit-comment" data-id="13236" data-post="622665" dir="auto">
                            <span class="icon-wrap" dir="auto">
                            <i class="trav-pencil" aria-hidden="true" dir="auto"></i>
                            </span>
                            <div class="drop-txt comment-edit__drop-text" dir="auto">
                            <p dir="auto"><b dir="auto">Edit</b></p>
                            </div>
                            </a>
                            <a href="javascript:;" class="dropdown-item postCommentDelete" id="13236" data-parent="13234" data-type="2" dir="auto">
                            <span class="icon-wrap" dir="auto">
                            <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;" dir="auto">
                            </span>
                            <div class="drop-txt comment-delete__drop-text" dir="auto">
                            <p dir="auto"><b dir="auto">Delete</b></p>
                            </div>
                            </a>
                            <a href="#" class="dropdown-item" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(13236,this)" dir="auto">
                            <span class="icon-wrap" dir="auto">
                            <i class="trav-flag-icon-o" dir="auto"></i>
                            </span>
                            <div class="drop-txt comment-report__drop-text" dir="auto">
                            <p dir="auto"><b dir="auto">Report</b></p>
                            </div>
                            </a>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="comment-txt comment-text-13236" dir="auto">
                            <p dir="auto"></p>
                            <form class="commentEditForm13236 comment-edit-form d-none" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto"><input type="hidden" data-id="pair13236" name="pair" value="5f770bda80259" dir="auto">
                            <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0" dir="auto">
                            <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                            <textarea name="text" id="text13236" class="textarea-customize  post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="Write a comment" dir="auto"></textarea>
                            </div>
                            <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                            <div class="post-alloptions" dir="auto">
                            <ul class="create-link-list" dir="auto">
                            <li class="post-options" dir="auto">
                            <input type="file" name="file[]" class="commenteditfile" id="commenteditfile13236" style="display:none" multiple="" dir="auto">
                            <i class="fa fa-camera click-target" data-target="commenteditfile13236" dir="auto"></i>
                            </li>
                            </ul>
                            </div>
                            <div class="comment-edit-action" dir="auto">
                            <a href="javascript:;" class="edit-cancel-link" data-comment_id="13236" dir="auto">Cancel</a>
                            <a href="javascript:;" class="edit-post-link" data-comment_id="13236" dir="auto">Post</a>
                            </div>
                            </div>
                            <div class="medias p-media b-whitesmoke" dir="auto">
                            <div class="img-wrap-newsfeed" dir="auto">
                            <div dir="auto">
                            <img class="thumb" src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563234_images.jpg" alt="" dir="auto">
                            </div>
                            <span class="close remove-media-file" data-media_id="14592486" dir="auto">
                            <span dir="auto">×</span>
                            </span></div>
                            </div>
                            </div>
                            <input type="hidden" name="post_id" value="622665" dir="auto">
                            <input type="hidden" name="comment_id" value="13236" dir="auto">
                            <input type="hidden" name="comment_type" value="2" dir="auto">
                            <button type="submit" class="d-none" dir="auto"></button>
                            </form>
                            </div>
                            <div class="post-image-container" dir="auto">
                            <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;" dir="auto">
                            <li style="overflow: hidden;margin:1px;" dir="auto">
                            <a href="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563234_images.jpg" data-lightbox="comment__replymedia13236" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563234_images.jpg" alt="" style="width:192px;height:210px;object-fit: cover;" dir="auto">
                            </a>
                            </li>
                            </ul></div>
                            <div class="comment-bottom-info" dir="auto">
                            <div class="comment-info-content" posttype="Postcomment" dir="auto">
                            <div class="dropdown" dir="auto">
                            <a href="#" class="postCommentLikes like-link" id="13236" dir="auto"><i class="trav-heart-fill-icon " aria-hidden="true" dir="auto"></i> <span class="comment-like-count" dir="auto">0</span></a>
                            </div>
                            <span class="com-time" dir="auto"><span class="comment-dot" dir="auto"> · </span>20 hours ago</span>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="post-add-comment-block" id="replyForm13234" style="padding-top:0px;padding-left: 59px;display:none;" dir="auto">
                            <div class="avatar-wrap" style="padding-right:10px" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" style="width:30px !important;height:30px !important;" dir="auto">
                            </div>
                            <div class="post-add-com-inputs" dir="auto">
                            <form class="commentReplyForm13234" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto">
                            <input type="hidden" data-id="pair13234" name="pair" value="5f770bda8135d" dir="auto">
                            <div class="post-create-block post-comment-create-block post-reply-block" tabindex="0" dir="auto">
                            <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                            <textarea name="text" data-id="text13234" class="textarea-customize post-comment-emoji" style="display: none; vertical-align: top; min-height: 50px;" placeholder="Write a comment" dir="auto"></textarea><div class="note-editor note-frame panel panel-default"><div class="note-dropzone">  <div class="note-dropzone-message" dir="auto"></div></div><div class="note-toolbar panel-heading" role="toolbar" dir="auto"><div class="note-btn-group btn-group note-insert"><button type="button" class="note-btn btn btn-default btn-sm" role="button" tabindex="-1" dir="auto"><div class="emoji-picker-container emoji-picker emojionearea-button d-none" dir="auto"></div></button><div class="emoji-menu 00b5dc61-5c5c-40de-9479-07713f85c370" style="display: none;">
                                <div class="emoji-items-wrap1" dir="auto">
                                    <table class="emoji-menu-tabs" dir="auto">
                                        <tbody dir="auto">
                                        <tr dir="auto">
                                            <td dir="auto"><a class="emoji-menu-tab icon-recent-selected" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-smile" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-flower" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-bell" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-car" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-grid" dir="auto"></a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="emoji-items-wrap mobile_scrollable_wrap" dir="auto">
                                        <div class="emoji-items" dir="auto"><a href="javascript:void(0)" title=":flushed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -350px 0px no-repeat;background-size:675px 175px;" alt=":flushed:" dir="auto"><span class="label" dir="auto">:flushed:</span></a><a href="javascript:void(0)" title=":grin:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -375px 0px no-repeat;background-size:675px 175px;" alt=":grin:" dir="auto"><span class="label" dir="auto">:grin:</span></a><a href="javascript:void(0)" title=":rage:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px -25px no-repeat;background-size:675px 175px;" alt=":rage:" dir="auto"><span class="label" dir="auto">:rage:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -300px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_closed_eyes:" dir="auto"><span class="label" dir="auto">:stuck_out_tongue_closed_eyes:</span></a><a href="javascript:void(0)" title=":kissing_heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px 0px no-repeat;background-size:675px 175px;" alt=":kissing_heart:" dir="auto"><span class="label" dir="auto">:kissing_heart:</span></a><a href="javascript:void(0)" title=":ok_hand:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -650px -75px no-repeat;background-size:675px 175px;" alt=":ok_hand:" dir="auto"><span class="label" dir="auto">:ok_hand:</span></a><a href="javascript:void(0)" title=":heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -250px -150px no-repeat;background-size:675px 175px;" alt=":heart:" dir="auto"><span class="label" dir="auto">:heart:</span></a><a href="javascript:void(0)" title=":frog:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_1.png') -150px 0px no-repeat;background-size:725px 100px;" alt=":frog:" dir="auto"><span class="label" dir="auto">:frog:</span></a><a href="javascript:void(0)" title=":+1:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -75px no-repeat;background-size:675px 175px;" alt=":+1:" dir="auto"><span class="label" dir="auto">:+1:</span></a><a href="javascript:void(0)" title=":heart_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -150px 0px no-repeat;background-size:675px 175px;" alt=":heart_eyes:" dir="auto"><span class="label" dir="auto">:heart_eyes:</span></a><a href="javascript:void(0)" title=":blush:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px 0px no-repeat;background-size:675px 175px;" alt=":blush:" dir="auto"><span class="label" dir="auto">:blush:</span></a><a href="javascript:void(0)" title=":joy:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -550px 0px no-repeat;background-size:675px 175px;" alt=":joy:" dir="auto"><span class="label" dir="auto">:joy:</span></a><a href="javascript:void(0)" title=":relaxed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -100px 0px no-repeat;background-size:675px 175px;" alt=":relaxed:" dir="auto"><span class="label" dir="auto">:relaxed:</span></a><a href="javascript:void(0)" title=":pensive:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -400px 0px no-repeat;background-size:675px 175px;" alt=":pensive:" dir="auto"><span class="label" dir="auto">:pensive:</span></a><a href="javascript:void(0)" title=":smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px 0px no-repeat;background-size:675px 175px;" alt=":smile:" dir="auto"><span class="label" dir="auto">:smile:</span></a><a href="javascript:void(0)" title=":sob:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -575px 0px no-repeat;background-size:675px 175px;" alt=":sob:" dir="auto"><span class="label" dir="auto">:sob:</span></a><a href="javascript:void(0)" title=":kiss:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px -150px no-repeat;background-size:675px 175px;" alt=":kiss:" dir="auto"><span class="label" dir="auto">:kiss:</span></a><a href="javascript:void(0)" title=":unamused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -450px 0px no-repeat;background-size:675px 175px;" alt=":unamused:" dir="auto"><span class="label" dir="auto">:unamused:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_winking_eye:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_winking_eye:" dir="auto"><span class="label" dir="auto">:stuck_out_tongue_winking_eye:</span></a><a href="javascript:void(0)" title=":see_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px -75px no-repeat;background-size:675px 175px;" alt=":see_no_evil:" dir="auto"><span class="label" dir="auto">:see_no_evil:</span></a><a href="javascript:void(0)" title=":wink:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px 0px no-repeat;background-size:675px 175px;" alt=":wink:" dir="auto"><span class="label" dir="auto">:wink:</span></a><a href="javascript:void(0)" title=":smiley:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -25px 0px no-repeat;background-size:675px 175px;" alt=":smiley:" dir="auto"><span class="label" dir="auto">:smiley:</span></a><a href="javascript:void(0)" title=":cry:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -525px 0px no-repeat;background-size:675px 175px;" alt=":cry:" dir="auto"><span class="label" dir="auto">:cry:</span></a><a href="javascript:void(0)" title=":scream:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -25px no-repeat;background-size:675px 175px;" alt=":scream:" dir="auto"><span class="label" dir="auto">:scream:</span></a><a href="javascript:void(0)" title=":smirk:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px -50px no-repeat;background-size:675px 175px;" alt=":smirk:" dir="auto"><span class="label" dir="auto">:smirk:</span></a><a href="javascript:void(0)" title=":disappointed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px 0px no-repeat;background-size:675px 175px;" alt=":disappointed:" dir="auto"><span class="label" dir="auto">:disappointed:</span></a><a href="javascript:void(0)" title=":sweat_smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px -25px no-repeat;background-size:675px 175px;" alt=":sweat_smile:" dir="auto"><span class="label" dir="auto">:sweat_smile:</span></a><a href="javascript:void(0)" title=":kissing_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -200px 0px no-repeat;background-size:675px 175px;" alt=":kissing_closed_eyes:" dir="auto"><span class="label" dir="auto">:kissing_closed_eyes:</span></a><a href="javascript:void(0)" title=":speak_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -75px no-repeat;background-size:675px 175px;" alt=":speak_no_evil:" dir="auto"><span class="label" dir="auto">:speak_no_evil:</span></a><a href="javascript:void(0)" title=":relieved:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -425px 0px no-repeat;background-size:675px 175px;" alt=":relieved:" dir="auto"><span class="label" dir="auto">:relieved:</span></a><a href="javascript:void(0)" title=":grinning:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px 0px no-repeat;background-size:675px 175px;" alt=":grinning:" dir="auto"><span class="label" dir="auto">:grinning:</span></a><a href="javascript:void(0)" title=":yum:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px -25px no-repeat;background-size:675px 175px;" alt=":yum:" dir="auto"><span class="label" dir="auto">:yum:</span></a><a href="javascript:void(0)" title=":neutral_face:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -25px no-repeat;background-size:675px 175px;" alt=":neutral_face:" dir="auto"><span class="label" dir="auto">:neutral_face:</span></a><a href="javascript:void(0)" title=":confused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -625px -25px no-repeat;background-size:675px 175px;" alt=":confused:" dir="auto"><span class="label" dir="auto">:confused:</span></a></div>
                                    </div>
                                </div>
                            </div></div><div class="note-btn-group btn-group note-custom"></div></div><div class="note-editing-area" dir="auto"><div class="note-placeholder custom-placeholder" style="display: none;">Write a comment</div><div class="note-handle"><div class="note-control-selection" dir="auto"><div class="note-control-selection-bg" dir="auto"></div><div class="note-control-holder note-control-nw" dir="auto"></div><div class="note-control-holder note-control-ne" dir="auto"></div><div class="note-control-holder note-control-sw" dir="auto"></div><div class="note-control-sizing note-control-se" dir="auto"></div><div class="note-control-selection-info" dir="auto"></div></div></div><textarea class="note-codable" role="textbox" aria-multiline="true" dir="auto"></textarea><div class="note-editable" contenteditable="true" role="textbox" aria-multiline="true" dir="auto" spellcheck="true"><div><br dir="auto"></div></div></div><output class="note-status-output" aria-live="polite" dir="auto"></output><div class="note-statusbar" role="status" dir="auto" style="">  <div class="note-resizebar" role="seperator" aria-orientation="horizontal" aria-label="Resize" dir="auto">    <div class="note-icon-bar" dir="auto"></div>    <div class="note-icon-bar" dir="auto"></div>    <div class="note-icon-bar" dir="auto"></div>  </div></div><div class="modal link-dialog" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Link"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Link</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group" dir="auto"><label class="note-form-label" dir="auto">Text to display</label><input class="note-link-text form-control note-form-control note-input" type="text" dir="auto"></div><div class="form-group note-form-group" dir="auto"><label class="note-form-label" dir="auto">To what URL should this link go?</label><input class="note-link-url form-control note-form-control note-input" type="text" value="http://" dir="auto"></div><div class="checkbox sn-checkbox-open-in-new-window" dir="auto"><label dir="auto"> <input role="checkbox" type="checkbox" checked="" aria-checked="true" dir="auto">Open in new window</label></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-link-btn" value="Insert Link" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Image"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Image</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group note-group-select-from-files" dir="auto"><label class="note-form-label" dir="auto">Select from files</label><input class="note-image-input form-control-file note-form-control note-input" type="file" name="files" accept="image/*" multiple="multiple" dir="auto"></div><div class="form-group note-group-image-url" style="overflow:auto;" dir="auto"><label class="note-form-label" dir="auto">Image URL</label><input class="note-image-url form-control note-form-control note-input  col-md-12" type="text" dir="auto"></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-image-btn" value="Insert Image" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Video"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Video</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group row-fluid" dir="auto"><label class="note-form-label" dir="auto">Video URL <small class="text-muted" dir="auto">(YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)</small></label><input class="note-video-url form-control note-form-control note-input" type="text" dir="auto"></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-video-btn" value="Insert Video" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Help"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Help</h4>    </div>    <div class="modal-body" dir="auto" style="max-height: 300px; overflow: scroll;"><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+Z</kbd></label><span dir="auto">Undoes the last command</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+Y</kbd></label><span dir="auto">Redoes the last command</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">TAB</kbd></label><span dir="auto">Tab</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">SHIFT+TAB</kbd></label><span dir="auto">Untab</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+B</kbd></label><span dir="auto">Set a bold style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+I</kbd></label><span dir="auto">Set a italic style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+U</kbd></label><span dir="auto">Set a underline style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+S</kbd></label><span dir="auto">Set a strikethrough style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+BACKSLASH</kbd></label><span dir="auto">Clean a style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+L</kbd></label><span dir="auto">Set left align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+E</kbd></label><span dir="auto">Set center align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+R</kbd></label><span dir="auto">Set right align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+J</kbd></label><span dir="auto">Set full align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+NUM7</kbd></label><span dir="auto">Toggle unordered list</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+NUM8</kbd></label><span dir="auto">Toggle ordered list</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+LEFTBRACKET</kbd></label><span dir="auto">Outdent on current paragraph</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+RIGHTBRACKET</kbd></label><span dir="auto">Indent on current paragraph</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM0</kbd></label><span dir="auto">Change current block's format as a paragraph(P tag)</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM1</kbd></label><span dir="auto">Change current block's format as H1</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM2</kbd></label><span dir="auto">Change current block's format as H2</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM3</kbd></label><span dir="auto">Change current block's format as H3</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM4</kbd></label><span dir="auto">Change current block's format as H4</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM5</kbd></label><span dir="auto">Change current block's format as H5</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM6</kbd></label><span dir="auto">Change current block's format as H6</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+ENTER</kbd></label><span dir="auto">Insert horizontal rule</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+K</kbd></label><span dir="auto">Show Link Dialog</span></div>    <div class="modal-footer" dir="auto"><p class="text-center" dir="auto"><a href="http://summernote.org/" target="_blank" dir="auto">Summernote 0.8.12</a> · <a href="https://github.com/summernote/summernote" target="_blank" dir="auto">Project</a> · <a href="https://github.com/summernote/summernote/issues" target="_blank" dir="auto">Issues</a></p></div>  </div></div></div></div>
                            <div class="textcomplete-wrapper"></div></div>
                            <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                            <div class="post-alloptions" dir="auto">
                            <ul class="create-link-list" dir="auto">
                            <li class="post-options" dir="auto">
                            <input type="file" name="file[]" id="commentreplyfile13234" style="display:none" multiple="" dir="auto">
                            <i class="fa fa-camera click-target" data-target="commentreplyfile13234" dir="auto"></i>
                            </li>
                            </ul>
                            </div>
                            <button type="submit" class="btn btn-primary d-none" dir="auto"></button>
                            <div class="comment-edit-action" dir="auto">
                            <a href="javascript:;" class="p-comment-cancel-link post-r-cancel-link" dir="auto">Cancel</a>
                            <a href="javascript:;" class="p-comment-link post-r-reply-comment-link" data-comment_id="13234" dir="auto">Post</a>
                            </div>
                            </div>
                            <div class="medias p-media b-whitesmoke" dir="auto"></div>
                            </div>
                            <input type="hidden" name="post_id" value="622665" dir="auto">
                            <input type="hidden" name="comment_id" value="13234" dir="auto">
                            </form>
                            </div>
                            </div>
                            </div>
                            </div>
                            <a href="#" class="load-more-comments">Load more...</a>
                            <div class="post-add-comment-block" dir="auto">
                            <div class="avatar-wrap" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" style="width:45px;height:45px;" dir="auto">
                            </div>
                            <!-- New elements -->
                            <div class="add-comment-input-group">
                                <input type="text" placeholder="Write a comment...">
                                <div>
                                    <button class="add-post__emoji" emoji-target="add_post_text" type="button" onclick="showFaceBlock()" id="faceEnter">🙂</button>
                                    <button><i class="trav-camera click-target" data-target="file"></i></button>
                                </div>
                            </div>
                            <!-- New elements END  -->
                            </div>
                            </div>
                            <!-- Copied from home page END -->
                            <div class="post-comment-layer" data-content="shares601451" style="display:none;" >
                            <div class="post-comment-wrapper" id="shares601451" >
                            </div>
                            </div>
                            </div>
                            <!-- Primary post block - text END -->
                            <!-- Primary post block - trip plan -->
                            <div class="post-block" >
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                </a>
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                    <a class="post-name-link" href="https://travooo.com/profile/831" >Michael Powell</a>
                                </div>
                                <div class="post-info" >
                                    Planned a <strong>trip</strong> to <a href="" class="link-place">Disneyland Park</a> today at 5:29 pm
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#deletePostNew">
                                <span class="icon-wrap" >

                                </span>
                                <div class="drop-txt" >
                                <p ><b >Delete</b></p>
                                <p style="color:red" >Remove this post</p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="post-image-container" >
                                    <!-- New element -->
                                    <div class="trip-plan-container">
                                        <div class="trip-plan-map">
                                            <img src="https://travooo.com/assets2/image/popup-map.jpg" alt="">
                                            <button class="btn btn-light-primary trip-plan-btn">View Plan</button>
                                            <div class="forecast-map-marker map-popover" type="button" data-placement="top" data-toggle="popover" data-html="true" data-original-title="" title="">
                                                <i class="fas fa-map-marker-alt"></i>
                                                <span class="count-label">3</span>
                                            </div>
                                            <div class="forecast-map-marker transfer" style="top: 20%; left: 80%;">
                                                <i class="fas fa-plane"></i>
                                            </div>
                                            <div class="forecast-map-marker transfer" style="top: 40%; left: 60%;">
                                                <i class="fas fa-bus"></i>
                                            </div>
                                            <div class="map-marker-img multiple" style="top: 20%; left: 30%;">
                                                <span class="count-label">3</span>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                                            </div>
                                            <div class="map-marker-img" style="top: 60%; left: 20%;">
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                                            </div>
                                        </div>
                                        <div class="trip-plan-slider-container">
                                            <a href="" class="trip-plan-home-slider-prev"></a>
                                            <a href="" class="trip-plan-home-slider-next"></a>
                                            <div class="trip-plan-home-slider">
                                                <div class="trip-plan-home-slider-item active">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/places/ChIJISQme4O3t4kRG-iM3TfJ5_g/b5ff2630c3a24c7ec42951bf877e70a2e79ee0c1.jpg" alt="">
                                                    <div class="title">
                                                        Thailand
                                                        <span class="dest">3 destinations</span>
                                                    </div>
                                                </div>
                                                <div class="trip-plan-home-slider-item">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/places/ChIJISQme4O3t4kRG-iM3TfJ5_g/b5ff2630c3a24c7ec42951bf877e70a2e79ee0c1.jpg" alt="">
                                                    <div class="title">
                                                        Bulgari Tokyo Restaurant
                                                    </div>
                                                </div>
                                                <div class="trip-plan-home-slider-item">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/places/ChIJISQme4O3t4kRG-iM3TfJ5_g/b5ff2630c3a24c7ec42951bf877e70a2e79ee0c1.jpg" alt="">
                                                    <div class="title">
                                                        Bulgari Tokyo Restaurant
                                                    </div>
                                                </div>
                                                <div class="trip-plan-home-slider-item">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/places/ChIJISQme4O3t4kRG-iM3TfJ5_g/b5ff2630c3a24c7ec42951bf877e70a2e79ee0c1.jpg" alt="">
                                                    <div class="title">
                                                        Bulgari Tokyo Restaurant
                                                    </div>
                                                </div>
                                                <div class="trip-plan-home-slider-item">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/places/ChIJISQme4O3t4kRG-iM3TfJ5_g/b5ff2630c3a24c7ec42951bf877e70a2e79ee0c1.jpg" alt="">
                                                    <div class="title">
                                                        Bulgari Tokyo Restaurant
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- New element END -->
                                </div>
                                <div class="post-footer-info" >
                                <!-- Updated elements -->
                                <div class="post-foot-block post-reaction">
                                    <span class="post_like_button" id="601451">
                                        <a href="#">
                                            <i class="trav-heart-fill-icon"></i>
                                        </a>
                                    </span>
                                    <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                        <b >0</b> Likes
                                    </span>
                                </div>
                                <!-- Updated element END -->
                                <div class="post-foot-block" >
                                <a href="#" data-tab="comments616211" >
                                <i class="trav-comment-icon" ></i>
                                </a>
                                <ul class="foot-avatar-list" >
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                </ul>
                                <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                                </div>
                                <div class="post-foot-block ml-auto" >
                                <span class="post_share_button" id="616211" >
                                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                <i class="trav-share-icon" ></i>
                                </a>
                                </span>
                                <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                                </div>
                                </div>
                                <div class="post-comment-wrapper" id="following616211" >
                                </div>
                                <!-- Removed comments block -->
                            </div>
                            <!-- Primary post block - trip plan END -->
                            <!-- Primary post block - photo only -->
                            <div class="post-block" >
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                </a>
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                <span class="exp-icon" >
                                    <i class="fas fa-city"></i>
                                </span>
                                </div>
                                <div class="post-info" >
                                Addad a <strong>photo</strong> yesterday at 10:33am <i class="trav-set-location-icon primary"></i> <strong>21km</strong> from <a href="#" class="link-place">Arizona</a>
                                <i class="trav-lock-icon" ></i>
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#deletePostNew">
                                <span class="icon-wrap" >

                                </span>
                                <div class="drop-txt" >
                                <p ><b >Delete</b></p>
                                <p style="color:red" >Remove this post</p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="post-image-container" >
                                <!-- New element -->
                                <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;" >
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;" >
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" data-lightbox="media__post210172" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                </ul>
                                <!-- New element END -->
                                </div>
                                <div class="post-footer-info" >
                                <!-- Updated elements -->
                                <div class="post-foot-block post-reaction">
                                    <span class="post_like_button" id="601451">
                                        <a href="#">
                                            <i class="trav-heart-fill-icon"></i>
                                        </a>
                                    </span>
                                    <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                        <b >0</b> Likes
                                    </span>
                                </div>
                                <!-- Updated element END -->
                                <div class="post-foot-block" >
                                <a href="#" data-tab="comments616211" >
                                <i class="trav-comment-icon" ></i>
                                </a>
                                <ul class="foot-avatar-list" >
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                </ul>
                                <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                                </div>
                                <div class="post-foot-block ml-auto" >
                                <span class="post_share_button" id="616211" >
                                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                <i class="trav-share-icon" ></i>
                                </a>
                                </span>
                                <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                                </div>
                                </div>
                                <div class="post-comment-wrapper" id="following616211" >
                                </div>
                                <!-- Removed comments block -->
                            </div>
                            <!-- Primary post block - photo only END -->
                            <!-- Primary post block - video only -->
                            <div class="post-block" >
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                </a>
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                </div>
                                <div class="post-info" >
                                Addad a <strong>photo</strong> yesterday at 10:33am <i class="trav-set-location-icon primary"></i> <strong>21km</strong> from <a href="#" class="link-place">Arizona</a>
                                <i class="trav-users-icon" ></i>
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                                <a class="dropdown-item" href="#" onclick="post_delete('616211','Post', this, event)" >
                                <span class="icon-wrap" >

                                </span>
                                <div class="drop-txt" >
                                <p ><b >Delete</b></p>
                                <p style="color:red" >Remove this post</p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="post-image-container" >
                                <!-- New element -->
                                <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;" >
                                    <li style="padding: 0;position: relative;margin:0 0 0 0;overflow: hidden;">
                                        <video width="100%" height="auto" controls>
                                            <source src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1599583946_160_1591890126_1024897622-preview.mp4#t=5" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                                <!-- New element END -->
                                </div>
                                <div class="post-footer-info" >
                                <!-- Updated elements -->
                                <div class="post-foot-block post-reaction">
                                    <span class="post_like_button" id="601451">
                                        <a href="#">
                                            <i class="trav-heart-fill-icon"></i>
                                        </a>
                                    </span>
                                    <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                        <b >0</b> Likes
                                    </span>
                                </div>
                                <!-- Updated element END -->
                                <div class="post-foot-block" >
                                <a href="#" data-tab="comments616211" >
                                <i class="trav-comment-icon" ></i>
                                </a>
                                <ul class="foot-avatar-list" >
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                </ul>
                                <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                                </div>
                                <div class="post-foot-block ml-auto" >
                                <span class="post_share_button" id="616211" >
                                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                <i class="trav-share-icon" ></i>
                                </a>
                                </span>
                                <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                                </div>
                                </div>
                                <div class="post-comment-wrapper" id="following616211" >
                                </div>
                                <!-- Removed comments block -->
                            </div>
                            <!-- Primary post block - video only END -->
                            <!-- Primary post block - photo*2 only -->
                            <div class="post-block" >
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                </a>
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                </div>
                                <div class="post-info" >
                                Addad a <strong>photo</strong> yesterday at 10:33am <i class="trav-set-location-icon primary"></i> <strong>21km</strong> from <a href="#" class="link-place">Arizona</a>
                                <i class="trav-users-icon"></i>
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                                <a class="dropdown-item" href="#" onclick="post_delete('616211','Post', this, event)" >
                                <span class="icon-wrap" >

                                </span>
                                <div class="drop-txt" >
                                <p ><b >Delete</b></p>
                                <p style="color:red" >Remove this post</p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="post-image-container" >
                                <!-- New element -->
                                <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;" >
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                        <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                </ul>
                                <!-- New element END -->
                                </div>
                                <div class="post-footer-info" >
                                <!-- Updated elements -->
                                <div class="post-foot-block post-reaction">
                                    <span class="post_like_button" id="601451">
                                        <a href="#">
                                            <i class="trav-heart-fill-icon"></i>
                                        </a>
                                    </span>
                                    <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                        <b >0</b> Likes
                                    </span>
                                </div>
                                <!-- Updated element END -->
                                <div class="post-foot-block" >
                                <a href="#" data-tab="comments616211" >
                                <i class="trav-comment-icon" ></i>
                                </a>
                                <ul class="foot-avatar-list" >
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                </ul>
                                <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                                </div>
                                <div class="post-foot-block ml-auto" >
                                <span class="post_share_button" id="616211" >
                                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                <i class="trav-share-icon" ></i>
                                </a>
                                </span>
                                <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                                </div>
                                </div>
                                <div class="post-comment-wrapper" id="following616211" >
                                </div>
                                <!-- Removed comments block -->
                            </div>
                            <!-- Primary post block - photo*2 only END -->
                            <!-- Primary post block - photo*3 only -->
                            <div class="post-block" >
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                </a>
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                </div>
                                <div class="post-info" >
                                Addad a <strong>photo</strong> yesterday at 10:33am <i class="trav-set-location-icon primary"></i> <strong>21km</strong> from <a href="#" class="link-place">Arizona</a>
                                <i class="trav-lock-icon" ></i>
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                                <a class="dropdown-item" href="#" onclick="post_delete('616211','Post', this, event)" >
                                <span class="icon-wrap" >

                                </span>
                                <div class="drop-txt" >
                                <p ><b >Delete</b></p>
                                <p style="color:red" >Remove this post</p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="post-image-container" >
                                <!-- New element -->
                                <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;" >
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                        <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                </ul>
                                <!-- New element END -->
                                </div>
                                <div class="post-footer-info" >
                                <!-- Updated elements -->
                                <div class="post-foot-block post-reaction">
                                    <span class="post_like_button" id="601451">
                                        <a href="#">
                                            <i class="trav-heart-fill-icon"></i>
                                        </a>
                                    </span>
                                    <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                        <b >0</b> Likes
                                    </span>
                                </div>
                                <!-- Updated element END -->
                                <div class="post-foot-block" >
                                <a href="#" data-tab="comments616211" >
                                <i class="trav-comment-icon" ></i>
                                </a>
                                <ul class="foot-avatar-list" >
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                </ul>
                                <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                                </div>
                                <div class="post-foot-block ml-auto" >
                                <span class="post_share_button" id="616211" >
                                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                <i class="trav-share-icon" ></i>
                                </a>
                                </span>
                                <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                                </div>
                                </div>
                                <div class="post-comment-wrapper" id="following616211" >
                                </div>
                                <!-- Removed comments block -->
                            </div>
                            <!-- Primary post block - photo*3 only END -->
                            <!-- Primary post block - photo*3 and text -->
                            <div class="post-block" >
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                </a>
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                </div>
                                <!-- Updated element -->
                                <div class="post-info">
                                    Checked-in
                                    <a href="https://travooo.com/place/3485165" class="link-place">The House of Dancing Water</a>
                                    on Sep 1, 2020
                                    <i class="trav-globe-icon" ></i>
                                </div>
                                <!-- Updated element END -->
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                                <a class="dropdown-item" href="#" onclick="post_delete('616211','Post', this, event)" >
                                <span class="icon-wrap" >

                                </span>
                                <div class="drop-txt" >
                                <p ><b >Delete</b></p>
                                <p style="color:red" >Remove this post</p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <!-- New elements -->
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                </div>
                                <div class="check-in-point">
                                    <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                                </div>
                                <!-- New elements END -->
                                <div class="post-image-container" >
                                <!-- New elements -->
                                <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;" >
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                        <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                </ul>
                                <!-- New element END -->
                                </div>
                                <div class="post-footer-info" >
                                <!-- Updated elements -->
                                <div class="post-foot-block post-reaction">
                                    <span class="post_like_button" id="601451">
                                        <a href="#">
                                            <i class="trav-heart-fill-icon"></i>
                                        </a>
                                    </span>
                                    <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                        <b >0</b> Likes
                                    </span>
                                </div>
                                <!-- Updated element END -->
                                <div class="post-foot-block" >
                                <a href="#" data-tab="comments616211" >
                                <i class="trav-comment-icon" ></i>
                                </a>
                                <ul class="foot-avatar-list" >
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                </ul>
                                <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                                </div>
                                <div class="post-foot-block ml-auto" >
                                <span class="post_share_button" id="616211" >
                                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                <i class="trav-share-icon" ></i>
                                </a>
                                </span>
                                <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                                </div>
                                </div>
                                <div class="post-comment-wrapper" id="following616211" >
                                </div>
                                <!-- Removed comments block -->
                            </div>
                            <!-- Primary post block - photo*3 and text END -->
                            <!-- Primary post block - photo*3 and text -->
                            <div class="post-block post-block-notification" >
                                <!-- New element -->
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a> is check-in Central Park today at 09:50am
                                    </div>
                                </div>
                                <!-- New element END -->
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                </a>
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                </div>
                                <!-- Updated element -->
                                <div class="post-info">
                                    Checked-in
                                    <a href="https://travooo.com/place/3485165" class="link-place">The House of Dancing Water</a>
                                    on Sep 1, 2020
                                    <i class="trav-globe-icon" ></i>
                                </div>
                                <!-- Updated element END -->
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                                <a class="dropdown-item" href="#" onclick="post_delete('616211','Post', this, event)" >
                                <span class="icon-wrap" >

                                </span>
                                <div class="drop-txt" >
                                <p ><b >Delete</b></p>
                                <p style="color:red" >Remove this post</p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <!-- New elements -->
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                </div>
                                <div class="check-in-point">
                                    <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                                </div>
                                <!-- New elements END -->
                                <div class="post-image-container" >
                                <!-- New elements -->
                                <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;" >
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                        <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                        <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                </ul>
                                <!-- New element END -->
                                </div>
                                <div class="post-footer-info" >
                                <!-- Updated elements -->
                                <div class="post-foot-block post-reaction">
                                    <span class="post_like_button" id="601451">
                                        <a href="#">
                                            <i class="trav-heart-fill-icon"></i>
                                        </a>
                                    </span>
                                    <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                        <b >0</b> Likes
                                    </span>
                                </div>
                                <!-- Updated element END -->
                                <div class="post-foot-block" >
                                <a href="#" data-tab="comments616211" >
                                <i class="trav-comment-icon" ></i>
                                </a>
                                <ul class="foot-avatar-list" >
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                </ul>
                                <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                                </div>
                                <div class="post-foot-block ml-auto" >
                                <span class="post_share_button" id="616211" >
                                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                <i class="trav-share-icon" ></i>
                                </a>
                                </span>
                                <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                                </div>
                                </div>
                                <div class="post-comment-wrapper" id="following616211" >
                                </div>
                                <!-- Removed comments block -->
                            </div>
                            <!-- Primary post block - photo*3 and text END -->
                            <!-- SHARED Primary post block - photo*3 and text -->
                            <div class="post-block" >
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                </a>
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                    <a class="post-name-link" href="https://travooo.com/profile/831" >Katherin</a> shared <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan's</a> post
                                </div>
                                <!-- Updated element -->
                                <div class="post-info">
                                    3 hours ago
                                </div>
                                <!-- Updated element END -->
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                                <a class="dropdown-item" href="#" onclick="post_delete('616211','Post', this, event)" >
                                <span class="icon-wrap" >

                                </span>
                                <div class="drop-txt" >
                                <p ><b >Delete</b></p>
                                <p style="color:red" >Remove this post</p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <!-- New elements -->
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                </div>
                                <!-- New elements END -->
                                <!-- SHARED POST -->
                                <div class="post-block post-block-shared" >
                                    <div class="post-top-info-layer" >
                                    <div class="post-top-info-wrap" >
                                    <div class="post-top-avatar-wrap" >
                                    <a class="post-name-link" href="https://travooo.com/profile/831" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                    </a>
                                    </div>
                                    <div class="post-top-info-txt" >
                                    <div class="post-top-name" >
                                    <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                    </div>
                                    <!-- Updated element -->
                                    <div class="post-info">
                                        Checked-in
                                        <a href="https://travooo.com/place/3485165" class="link-place">The House of Dancing Water</a>
                                        on Sep 1, 2020
                                        <i class="trav-globe-icon" ></i>
                                    </div>
                                    <!-- Updated element END -->
                                    </div>
                                    </div>
                                    <div class="post-top-info-action" >
                                    <div class="dropdown" >
                                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                    <i class="trav-angle-down" ></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                                    <a class="dropdown-item" href="#" onclick="post_delete('616211','Post', this, event)" >
                                    <span class="icon-wrap" >

                                    </span>
                                    <div class="drop-txt" >
                                    <p ><b >Delete</b></p>
                                    <p style="color:red" >Remove this post</p>
                                    </div>
                                    </a>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    <!-- New elements -->
                                    <div class="post-txt-wrap">
                                        <div>
                                            <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                            <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                        </div>
                                    </div>
                                    <div class="check-in-point">
                                        <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                                    </div>
                                    <!-- New elements END -->
                                    <div class="post-image-container" >
                                    <!-- New elements -->
                                    <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;" >
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- New element END -->
                                    </div>
                                </div>
                                <!-- SHARED POST END -->
                                <div class="post-footer-info" >
                                <!-- Updated elements -->
                                <div class="post-foot-block post-reaction">
                                    <span class="post_like_button" id="601451">
                                        <a href="#">
                                            <i class="trav-heart-fill-icon"></i>
                                        </a>
                                    </span>
                                    <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                        <b >0</b> Likes
                                    </span>
                                </div>
                                <!-- Updated element END -->
                                <div class="post-foot-block" >
                                <a href="#" data-tab="comments616211" >
                                <i class="trav-comment-icon" ></i>
                                </a>
                                <ul class="foot-avatar-list" >
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                </ul>
                                <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                                </div>
                                <div class="post-foot-block ml-auto" >
                                <span class="post_share_button" id="616211" >
                                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                <i class="trav-share-icon" ></i>
                                </a>
                                </span>
                                <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                                </div>
                                </div>
                                <div class="post-comment-wrapper" id="following616211" >
                                </div>
                                <!-- Removed comments block -->
                            </div>
                            <!-- SHARED Primary post block - photo*3 and text END -->
                            <!-- SHARED Primary post block - photo only -->
                            <div class="post-block post-block-notification" >
                                <!-- New element -->
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        <a class="post-name-link" href="https://travooo.com/profile/831" >Katherin</a> shared a photo from <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan's</a> trip plan
                                    </div>
                                </div>
                                <!-- New element END -->
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                </a>
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                </div>
                                <div class="post-info" >
                                Uploaded a <strong>photo</strong> in <a href="#" class="link-place">Siberia</a> yesterday at 8:30am <i class="trav-globe-icon"></i>
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                                <a class="dropdown-item" href="#" onclick="post_delete('616211','Post', this, event)" >
                                <span class="icon-wrap" >

                                </span>
                                <div class="drop-txt" >
                                <p ><b >Delete</b></p>
                                <p style="color:red" >Remove this post</p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="post-image-container" >
                                <!-- New element -->
                                <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;" >
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;" >
                                        <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" data-lightbox="media__post210172" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                        </a>
                                    </li>
                                </ul>
                                <div class="original-trip-plan">
                                    <img src="https://travooo.com/assets2/image/placeholders/place.png" alt="">
                                </div>
                                <!-- New element END -->
                                </div>
                                <div class="post-footer-info" >
                                <!-- Updated elements -->
                                <div class="post-foot-block post-reaction">
                                    <span class="post_like_button" id="601451">
                                        <a href="#">
                                            <i class="trav-heart-fill-icon"></i>
                                        </a>
                                    </span>
                                    <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                        <b >0</b> Likes
                                    </span>
                                </div>
                                <!-- Updated element END -->
                                <div class="post-foot-block" >
                                <a href="#" data-tab="comments616211" >
                                <i class="trav-comment-icon" ></i>
                                </a>
                                <ul class="foot-avatar-list" >
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                    <li>
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                    </li>
                                </ul>
                                <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                                </div>
                                <div class="post-foot-block ml-auto" >
                                <span class="post_share_button" id="616211" >
                                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                <i class="trav-share-icon" ></i>
                                </a>
                                </span>
                                <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                                </div>
                                </div>
                                <div class="post-comment-wrapper" id="following616211" >
                                </div>
                                <!-- Removed comments block -->
                            </div>
                            <!-- SHARED Primary post block - photo only END -->
                            <!-- SHARED PLACE Primary post block - text -->
                            <div class="post-block" >
                            <div class="post-top-info-layer" >
                            <div class="post-top-info-wrap" >
                            <div class="post-top-avatar-wrap" >
                            <a class="post-name-link" href="https://travooo.com/profile/831" >
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                            </a>
                            </div>
                            <div class="post-top-info-txt" >
                            <div class="post-top-name" >
                            <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                            </div>
                            <div class="post-info" >
                            15 July at 12:30pm <i class="trav-lock-icon"></i>
                            </div>
                            </div>
                            </div>
                            <div class="post-top-info-action" >
                            <div class="dropdown" >
                            <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="trav-angle-down" ></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                            <a class="dropdown-item" href="#" onclick="post_delete('601451','Post', this, event)" >
                            <span class="icon-wrap" >

                            </span>
                            <div class="drop-txt" >
                            <p ><b >Delete</b></p>
                            <p style="color:red" >Remove this post</p>
                            </div>
                            </a>
                            </div>
                            </div>
                            </div>
                            </div>
                            <!-- New element -->
                            <div class="post-txt-wrap">
                                <div>
                                    <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing <span class="post-text-tag" type="button" data-placement="bottom" data-toggle="popover" data-html="true"><i class="trav-set-location-icon"></i> New York City</span> elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                    <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing <span class="post-text-tag" type="button" data-placement="bottom" data-toggle="popover" data-html="true"><i class="trav-set-location-icon"></i> New York City</span> elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                </div>
                            </div>
                            <div class="shared-place-media">
                                <div class="post-image-container" >
                                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;" >
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="shared-place-slider-wrap">
                                <div class="shared-place-slider">
                                    <div class="shared-place-slider-card">
                                        <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                        <div class="shared-place-slider-card-description">
                                            <div class="details">
                                                <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                                <div class="text">
                                                    <h3>New York City</h3>
                                                    <p>City in New York State, United States</p>
                                                    <div class="users">
                                                        <button class="post__comment-btn">
                                                            <div class="user-list">
                                                                <div class="user-list__item">
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                </div>
                                                            </div>
                                                            <i class="trav-comment-icon icon"></i>
                                                            <span><strong>64K</strong> Talking about this</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <button class="btn btn-light-primary orange">Book</button>
                                                <button class="btn btn-light-primary">Follow</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="shared-place-slider-card">
                                        <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                        <div class="shared-place-slider-card-description">
                                            <div class="details">
                                                <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                                <div class="text">
                                                    <h3>New York City</h3>
                                                    <p>City in New York State, United States</p>
                                                    <div class="users">
                                                        <button class="post__comment-btn">
                                                            <div class="user-list">
                                                                <div class="user-list__item">
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                </div>
                                                            </div>
                                                            <i class="trav-comment-icon icon"></i>
                                                            <span><strong>64K</strong> Talking about this</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <button class="btn btn-light-primary orange">Book</button>
                                                <button class="btn btn-light-primary">Follow</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="shared-place-slider-card">
                                        <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                        <div class="shared-place-slider-card-description">
                                            <div class="details">
                                                <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                                <div class="text">
                                                    <h3>New York City</h3>
                                                    <p>City in New York State, United States</p>
                                                    <div class="users">
                                                        <button class="post__comment-btn">
                                                            <div class="user-list">
                                                                <div class="user-list__item">
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>
                                                                    <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>
                                                                </div>
                                                            </div>
                                                            <i class="trav-comment-icon icon"></i>
                                                            <span><strong>64K</strong> Talking about this</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <button class="btn btn-light-primary orange">Book</button>
                                                <button class="btn btn-light-primary">Follow</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- New element END -->
                            <div class="post-footer-info" >
                            <!-- Updated elements -->
                            <div class="post-foot-block post-reaction">
                                <span class="post_like_button" id="601451">
                                    <a href="#">
                                        <i class="trav-heart-fill-icon"></i>
                                    </a>
                                </span>
                                <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                    <b >0</b> Likes
                                </span>
                            </div>
                            <!-- Updated element END -->
                            <div class="post-foot-block" >
                            <a href="#" data-tab="comments601451" >
                            <i class="trav-comment-icon" ></i>
                            </a>
                            <ul class="foot-avatar-list" >
                                <li>
                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                </li>
                                <li>
                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                </li>
                                <li>
                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                </li>
                            </ul>
                            <span ><a href="#" data-tab="comments601451" ><span class="601451-comments-count" >0</span> Comments</a></span>
                            </div>
                            <div class="post-foot-block ml-auto" >
                            <span class="post_share_button" id="601451" >
                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                            <i class="trav-share-icon" ></i>
                            </a>
                            </span>
                            <span id="post_share_count_601451" ><a href="#" data-tab="shares601451" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                            </div>
                            </div>
                            <div class="post-comment-wrapper" id="following601451" >
                            </div>
                            <div class="post-comment-layer" data-content="comments601451" style="display:none;" >
                            <div class="post-comment-top-info" >
                            <ul class="comment-filter" >
                            <li onclick="commentSort('Top', this, $(this).closest('.post-block'))" >Top</li>
                            <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))" >New</li>
                            </ul>
                            <div class="comm-count-info" >
                            <strong >0</strong> / <span class="601451-comments-count" >0</span>
                            </div>
                            </div>
                            <div class="post-comment-wrapper sortBody" id="comments601451" >
                            </div>
                            <div class="post-add-comment-block" >
                            <div class="avatar-wrap" >
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" style="width:45px;height:45px;" >
                            </div>
                            <div class="post-add-com-inputs" >
                            <form class="commentForm601451" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" >
                            <input type="hidden" name="_token" value="4rN8nRC849NcMh7EzP6Ty2BViFSl2RhMV6k1QFNc" >
                            <input type="hidden" id="pair601451" name="pair" value="5f58beea349c8" >
                            <div class="post-create-block post-comment-create-block post-regular-block pb-2" tabindex="0" >
                            <div class="post-create-input p-create-input b-whitesmoke" >
                            <textarea name="text" id="text601451" class="textarea-customize post-comment-emoji" style="display: none; vertical-align: top; min-height: 50px;" placeholder="Write a comment" ></textarea><div class="note-editor note-frame panel panel-default"><div class="note-dropzone">  <div class="note-dropzone-message" ></div></div><div class="note-toolbar panel-heading" role="toolbar" ><div class="note-btn-group btn-group note-insert"><button type="button" class="note-btn btn btn-default btn-sm" role="button" tabindex="-1" ><div class="emoji-picker-container emoji-picker emojionearea-button d-none" ></div></button><div class="emoji-menu e93bca8a-7b76-4f32-922c-6fc47430ecf8" style="display: none;">
                                <div class="emoji-items-wrap1" >
                                    <table class="emoji-menu-tabs" >
                                        <tbody >
                                        <tr >
                                            <td ><a class="emoji-menu-tab icon-recent-selected" ></a></td>
                                            <td ><a class="emoji-menu-tab icon-smile" ></a></td>
                                            <td ><a class="emoji-menu-tab icon-flower" ></a></td>
                                            <td ><a class="emoji-menu-tab icon-bell" ></a></td>
                                            <td ><a class="emoji-menu-tab icon-car" ></a></td>
                                            <td ><a class="emoji-menu-tab icon-grid" ></a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="emoji-items-wrap mobile_scrollable_wrap" >
                                        <div class="emoji-items" ><a href="javascript:void(0)" title=":blush:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px 0px no-repeat;background-size:675px 175px;" alt=":blush:" ><span class="label" >:blush:</span></a><a href="javascript:void(0)" title=":heart_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -150px 0px no-repeat;background-size:675px 175px;" alt=":heart_eyes:" ><span class="label" >:heart_eyes:</span></a><a href="javascript:void(0)" title=":joy:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -550px 0px no-repeat;background-size:675px 175px;" alt=":joy:" ><span class="label" >:joy:</span></a><a href="javascript:void(0)" title=":kissing_heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px 0px no-repeat;background-size:675px 175px;" alt=":kissing_heart:" ><span class="label" >:kissing_heart:</span></a><a href="javascript:void(0)" title=":heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -250px -150px no-repeat;background-size:675px 175px;" alt=":heart:" ><span class="label" >:heart:</span></a><a href="javascript:void(0)" title=":grin:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -375px 0px no-repeat;background-size:675px 175px;" alt=":grin:" ><span class="label" >:grin:</span></a><a href="javascript:void(0)" title=":+1:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -75px no-repeat;background-size:675px 175px;" alt=":+1:" ><span class="label" >:+1:</span></a><a href="javascript:void(0)" title=":relaxed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -100px 0px no-repeat;background-size:675px 175px;" alt=":relaxed:" ><span class="label" >:relaxed:</span></a><a href="javascript:void(0)" title=":pensive:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -400px 0px no-repeat;background-size:675px 175px;" alt=":pensive:" ><span class="label" >:pensive:</span></a><a href="javascript:void(0)" title=":smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px 0px no-repeat;background-size:675px 175px;" alt=":smile:" ><span class="label" >:smile:</span></a><a href="javascript:void(0)" title=":sob:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -575px 0px no-repeat;background-size:675px 175px;" alt=":sob:" ><span class="label" >:sob:</span></a><a href="javascript:void(0)" title=":kiss:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px -150px no-repeat;background-size:675px 175px;" alt=":kiss:" ><span class="label" >:kiss:</span></a><a href="javascript:void(0)" title=":unamused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -450px 0px no-repeat;background-size:675px 175px;" alt=":unamused:" ><span class="label" >:unamused:</span></a><a href="javascript:void(0)" title=":flushed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -350px 0px no-repeat;background-size:675px 175px;" alt=":flushed:" ><span class="label" >:flushed:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_winking_eye:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_winking_eye:" ><span class="label" >:stuck_out_tongue_winking_eye:</span></a><a href="javascript:void(0)" title=":see_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px -75px no-repeat;background-size:675px 175px;" alt=":see_no_evil:" ><span class="label" >:see_no_evil:</span></a><a href="javascript:void(0)" title=":wink:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px 0px no-repeat;background-size:675px 175px;" alt=":wink:" ><span class="label" >:wink:</span></a><a href="javascript:void(0)" title=":smiley:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -25px 0px no-repeat;background-size:675px 175px;" alt=":smiley:" ><span class="label" >:smiley:</span></a><a href="javascript:void(0)" title=":cry:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -525px 0px no-repeat;background-size:675px 175px;" alt=":cry:" ><span class="label" >:cry:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -300px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_closed_eyes:" ><span class="label" >:stuck_out_tongue_closed_eyes:</span></a><a href="javascript:void(0)" title=":scream:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -25px no-repeat;background-size:675px 175px;" alt=":scream:" ><span class="label" >:scream:</span></a><a href="javascript:void(0)" title=":rage:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px -25px no-repeat;background-size:675px 175px;" alt=":rage:" ><span class="label" >:rage:</span></a><a href="javascript:void(0)" title=":smirk:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px -50px no-repeat;background-size:675px 175px;" alt=":smirk:" ><span class="label" >:smirk:</span></a><a href="javascript:void(0)" title=":disappointed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px 0px no-repeat;background-size:675px 175px;" alt=":disappointed:" ><span class="label" >:disappointed:</span></a><a href="javascript:void(0)" title=":sweat_smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px -25px no-repeat;background-size:675px 175px;" alt=":sweat_smile:" ><span class="label" >:sweat_smile:</span></a><a href="javascript:void(0)" title=":kissing_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -200px 0px no-repeat;background-size:675px 175px;" alt=":kissing_closed_eyes:" ><span class="label" >:kissing_closed_eyes:</span></a><a href="javascript:void(0)" title=":speak_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -75px no-repeat;background-size:675px 175px;" alt=":speak_no_evil:" ><span class="label" >:speak_no_evil:</span></a><a href="javascript:void(0)" title=":relieved:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -425px 0px no-repeat;background-size:675px 175px;" alt=":relieved:" ><span class="label" >:relieved:</span></a><a href="javascript:void(0)" title=":grinning:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px 0px no-repeat;background-size:675px 175px;" alt=":grinning:" ><span class="label" >:grinning:</span></a><a href="javascript:void(0)" title=":yum:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px -25px no-repeat;background-size:675px 175px;" alt=":yum:" ><span class="label" >:yum:</span></a><a href="javascript:void(0)" title=":ok_hand:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -650px -75px no-repeat;background-size:675px 175px;" alt=":ok_hand:" ><span class="label" >:ok_hand:</span></a><a href="javascript:void(0)" title=":neutral_face:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -25px no-repeat;background-size:675px 175px;" alt=":neutral_face:" ><span class="label" >:neutral_face:</span></a><a href="javascript:void(0)" title=":confused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -625px -25px no-repeat;background-size:675px 175px;" alt=":confused:" ><span class="label" >:confused:</span></a></div>
                                    </div>
                                </div>
                            </div></div><div class="note-btn-group btn-group note-custom"></div></div><div class="note-editing-area" ><div class="note-placeholder custom-placeholder" style="display: block;">Write a comment</div><div class="note-handle"><div class="note-control-selection" ><div class="note-control-selection-bg" ></div><div class="note-control-holder note-control-nw" ></div><div class="note-control-holder note-control-ne" ></div><div class="note-control-holder note-control-sw" ></div><div class="note-control-sizing note-control-se" ></div><div class="note-control-selection-info" ></div></div></div><textarea class="note-codable" role="textbox" aria-multiline="true" ></textarea><div class="note-editable" contenteditable="true" role="textbox" aria-multiline="true"  spellcheck="true"><div><br ></div></div></div><output class="note-status-output" aria-live="polite" ></output><div class="note-statusbar" role="status"  style="">  <div class="note-resizebar" role="seperator" aria-orientation="horizontal" aria-label="Resize" >    <div class="note-icon-bar" ></div>    <div class="note-icon-bar" ></div>    <div class="note-icon-bar" ></div>  </div></div><div class="modal link-dialog" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Link"><div class="modal-dialog" >  <div class="modal-content" >    <div class="modal-header" >      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" >×</button>      <h4 class="modal-title" >Insert Link</h4>    </div>    <div class="modal-body" ><div class="form-group note-form-group" ><label class="note-form-label" >Text to display</label><input class="note-link-text form-control note-form-control note-input" type="text" ></div><div class="form-group note-form-group" ><label class="note-form-label" >To what URL should this link go?</label><input class="note-link-url form-control note-form-control note-input" type="text" value="http://" ></div><div class="checkbox sn-checkbox-open-in-new-window" ><label > <input role="checkbox" type="checkbox" checked="" aria-checked="true" >Open in new window</label></div></div>    <div class="modal-footer" ><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-link-btn" value="Insert Link" disabled="" ></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Image"><div class="modal-dialog" >  <div class="modal-content" >    <div class="modal-header" >      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" >×</button>      <h4 class="modal-title" >Insert Image</h4>    </div>    <div class="modal-body" ><div class="form-group note-form-group note-group-select-from-files" ><label class="note-form-label" >Select from files</label><input class="note-image-input form-control-file note-form-control note-input" type="file" name="files" accept="image/*" multiple="multiple" ></div><div class="form-group note-group-image-url" style="overflow:auto;" ><label class="note-form-label" >Image URL</label><input class="note-image-url form-control note-form-control note-input  col-md-12" type="text" ></div></div>    <div class="modal-footer" ><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-image-btn" value="Insert Image" disabled="" ></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Video"><div class="modal-dialog" >  <div class="modal-content" >    <div class="modal-header" >      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" >×</button>      <h4 class="modal-title" >Insert Video</h4>    </div>    <div class="modal-body" ><div class="form-group note-form-group row-fluid" ><label class="note-form-label" >Video URL <small class="text-muted" >(YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)</small></label><input class="note-video-url form-control note-form-control note-input" type="text" ></div></div>    <div class="modal-footer" ><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-video-btn" value="Insert Video" disabled="" ></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Help"><div class="modal-dialog" >  <div class="modal-content" >    <div class="modal-header" >      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" >×</button>      <h4 class="modal-title" >Help</h4>    </div>    <div class="modal-body"  style="max-height: 300px; overflow: scroll;"><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+Z</kbd></label><span >Undoes the last command</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+Y</kbd></label><span >Redoes the last command</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >TAB</kbd></label><span >Tab</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >SHIFT+TAB</kbd></label><span >Untab</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+B</kbd></label><span >Set a bold style</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+I</kbd></label><span >Set a italic style</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+U</kbd></label><span >Set a underline style</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+SHIFT+S</kbd></label><span >Set a strikethrough style</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+BACKSLASH</kbd></label><span >Clean a style</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+SHIFT+L</kbd></label><span >Set left align</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+SHIFT+E</kbd></label><span >Set center align</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+SHIFT+R</kbd></label><span >Set right align</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+SHIFT+J</kbd></label><span >Set full align</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+SHIFT+NUM7</kbd></label><span >Toggle unordered list</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+SHIFT+NUM8</kbd></label><span >Toggle ordered list</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+LEFTBRACKET</kbd></label><span >Outdent on current paragraph</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+RIGHTBRACKET</kbd></label><span >Indent on current paragraph</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+NUM0</kbd></label><span >Change current block's format as a paragraph(P tag)</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+NUM1</kbd></label><span >Change current block's format as H1</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+NUM2</kbd></label><span >Change current block's format as H2</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+NUM3</kbd></label><span >Change current block's format as H3</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+NUM4</kbd></label><span >Change current block's format as H4</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+NUM5</kbd></label><span >Change current block's format as H5</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+NUM6</kbd></label><span >Change current block's format as H6</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+ENTER</kbd></label><span >Insert horizontal rule</span><div class="help-list-item" ></div><label style="width: 180px; margin-right: 10px;" ><kbd >CTRL+K</kbd></label><span >Show Link Dialog</span></div>    <div class="modal-footer" ><p class="text-center" ><a href="http://summernote.org/" target="_blank" >Summernote 0.8.12</a> · <a href="https://github.com/summernote/summernote" target="_blank" >Project</a> · <a href="https://github.com/summernote/summernote/issues" target="_blank" >Issues</a></p></div>  </div></div></div></div>
                            <div class="textcomplete-wrapper"></div></div>
                            <div class="post-create-controls  b-whitesmoke d-none" >
                            <div class="post-alloptions" >
                            <ul class="create-link-list" >
                            <li class="post-options" >
                            <input type="file" name="file[]" id="commentfile601451" style="display:none" multiple="" >
                            <i class="fa fa-camera click-target" aria-hidden="true" data-target="commentfile601451" ></i>
                            </li>
                            </ul>
                            </div>
                            <div class="comment-edit-action" >
                            <a href="javascript:;" class="p-comment-cancel-link post-r-cancel-link" >Cancel</a>
                            <a href="javascript:;" class="p-comment-link post-r-comment-link-601451" data-p_id="601451" >Post</a>
                            </div>
                            </div>
                            <div class="medias p-media b-whitesmoke" ></div>
                            </div>
                            <input type="hidden" name="post_id" value="601451" >
                            <button type="submit" class="btn btn-primary d-none" ></button>
                            </form>
                            </div>
                            </div>
                            </div>
                            <div class="post-comment-layer" data-content="shares601451" style="display:none;" >
                            <div class="post-comment-wrapper" id="shares601451" >
                            </div>
                            </div>
                            </div>
                            <!-- SHARED PLACE Primary post block - text END -->
                            <!-- Primary post type - Review -->
                            <div class="post-block post-block-review" >
                                <div class="post-top-info-layer" >
                                    <div class="post-top-info-wrap" >
                                        <div class="post-top-avatar-wrap" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                        </div>
                                        <div class="post-top-info-txt" >
                                            <div class="post-top-name" >
                                                <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                            </div>
                                            <div class="post-info" >
                                                Reviewed <a href="https://travooo.com/place/4263691" class="link-place" >Dongguan Science &amp; Technology Museum</a> <span class="review-rating">5 <i class="trav-star-icon" ></i></span> 2 minutes ago
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Dropdown -->
                                    <div class="post-top-info-action" dir="auto">
                                    <div class="dropdown" dir="auto">
                                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                                    <i class="trav-angle-down" dir="auto"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" dir="auto" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-user-plus-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Unfollow User</b></p>
                                    <p dir="auto">Stop seeing posts from User</p>
                                    </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Share</b></p>
                                    <p dir="auto">Spread the word</p>
                                    </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-link" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Copy Link</b></p>
                                    <p dir="auto">Paste the link anyywhere you want</p>
                                    </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#sendToFriendModal" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-on-travo-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                    <p dir="auto">Share with your friends</p>
                                    </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-heart-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Add to Favorites</b></p>
                                    <p dir="auto">Save it for later</p>
                                    </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-flag-icon-o" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Report</b></p>
                                    <p dir="auto">Help us understand</p>
                                    </div>
                                    </a>
                                    </div>
                                    </div>
                                    </div>
                                    <!-- Dropdown END -->
                                </div>
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                </div>
                                <div class="post-image-container post-follow-container wide" >
                                    <!-- New elements -->
                                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;" >
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" data-lightbox="media__post210172" >
                                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- New element END -->
                                    <!-- New Elements - Review-bottom -->
                                    <div class="review-inner-layer review-inner-content" >
                                        <div class="review-block" datesort="1600247687" scoresort="5.0" >
                                            <div class="review-top" >
                                                <div class="top-main" >
                                                    <div class="location-icon" >
                                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1795/1598930510_image.png" alt="">
                                                        <i class="trav-set-location-icon" ></i>
                                                    </div>
                                                    <div class="review-txt" >
                                                        <h3 class="review-ttl" >Dongguan Science &amp; Technology Museum</h3>
                                                        <div class="sub-txt" >
                                                            <div class="rate-label" >
                                                                <b >5.0</b>
                                                                <i class="trav-star-icon" ></i>
                                                            </div>&nbsp;
                                                            <span >from <b>9 reviews</b></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-wrap" >
                                                    <button class="btn btn-light-primary btn-bordered place-follow-btn" data-id="4263691" data-type="follow" >
                                                    Follow </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- New Elements - Review bottom END -->
                                </div>
                                <div class="post-footer-info" >
                                    <!-- Updated elements -->
                                    <div class="post-foot-block post-reaction">
                                        <span class="post_like_button" id="601451">
                                            <a href="#">
                                                <i class="trav-heart-fill-icon"></i>
                                            </a>
                                        </span>
                                        <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                            <b >0</b> Likes
                                        </span>
                                    </div>
                                    <!-- Updated element END -->
                                    <div class="post-foot-block" >
                                        <a href="#" data-tab="comments616211" >
                                            <i class="trav-comment-icon" ></i>
                                        </a>
                                        <ul class="foot-avatar-list" >
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                        </ul>
                                        <span >
                                            <a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a>
                                        </span>
                                    </div>
                                    <div class="post-foot-block ml-auto" >
                                        <span class="post_share_button" id="616211" >
                                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                                <i class="trav-share-icon" ></i>
                                            </a>
                                        </span>
                                        <span id="post_share_count_616211" >
                                            <a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="post-comment-wrapper"></div>
                            </div>
                            <!-- Primary post type - Review END -->
                            <!-- Primary post type - Review with multiple media -->
                            <div class="post-block post-block-review" >
                                <div class="post-top-info-layer" >
                                    <div class="post-top-info-wrap" >
                                        <div class="post-top-avatar-wrap" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                        </div>
                                        <div class="post-top-info-txt" >
                                            <div class="post-top-name" >
                                                <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                            </div>
                                            <div class="post-info" >
                                                Reviewed <a href="https://travooo.com/place/4263691" class="link-place" >Dongguan Science &amp; Technology Museum</a> <span class="review-rating">5 <i class="trav-star-icon" ></i></span> 2 minutes ago
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Dropdown -->
                                    <div class="post-top-info-action" dir="auto">
                                    <div class="dropdown" dir="auto">
                                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                                    <i class="trav-angle-down" dir="auto"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" dir="auto" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-user-plus-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Unfollow User</b></p>
                                    <p dir="auto">Stop seeing posts from User</p>
                                    </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Share</b></p>
                                    <p dir="auto">Spread the word</p>
                                    </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-link" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Copy Link</b></p>
                                    <p dir="auto">Paste the link anyywhere you want</p>
                                    </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#sendToFriendModal" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-on-travo-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                    <p dir="auto">Share with your friends</p>
                                    </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-heart-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Add to Favorites</b></p>
                                    <p dir="auto">Save it for later</p>
                                    </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                    <i class="trav-flag-icon-o" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Report</b></p>
                                    <p dir="auto">Help us understand</p>
                                    </div>
                                    </a>
                                    </div>
                                    </div>
                                    </div>
                                    <!-- Dropdown END -->
                                </div>
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                </div>
                                <div class="post-image-container post-follow-container wide" >
                                    <!-- New elements -->
                                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;" >
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- New element END -->
                                    <!-- New Elements - Review-bottom -->
                                    <div class="review-inner-layer review-inner-content" >
                                        <div class="review-block" datesort="1600247687" scoresort="5.0" >
                                            <div class="review-top" >
                                                <div class="top-main" >
                                                    <div class="location-icon" >
                                                        <i class="trav-set-location-icon" ></i>
                                                    </div>
                                                    <div class="review-txt" >
                                                        <h3 class="review-ttl" >Dongguan Science &amp; Technology Museum</h3>
                                                        <div class="sub-txt" >
                                                            <div class="rate-label" >
                                                                <b >5.0</b>
                                                                <i class="trav-star-icon" ></i>
                                                            </div>&nbsp;
                                                            <span >from <b>9 reviews</b></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-wrap" >
                                                    <button class="btn btn-light-primary btn-bordered place-follow-btn" data-id="4263691" data-type="follow" >
                                                    Follow </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- New Elements - Review bottom END -->
                                </div>
                                <div class="post-footer-info" >
                                    <!-- Updated elements -->
                                    <div class="post-foot-block post-reaction">
                                        <span class="post_like_button" id="601451">
                                            <a href="#">
                                                <i class="trav-heart-fill-icon"></i>
                                            </a>
                                        </span>
                                        <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                            <b >0</b> Likes
                                        </span>
                                    </div>
                                    <!-- Updated element END -->
                                    <div class="post-foot-block" >
                                        <a href="#" data-tab="comments616211" >
                                            <i class="trav-comment-icon" ></i>
                                        </a>
                                        <ul class="foot-avatar-list" >
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                        </ul>
                                        <span >
                                            <a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a>
                                        </span>
                                    </div>
                                    <div class="post-foot-block ml-auto" >
                                        <span class="post_share_button" id="616211" >
                                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                                <i class="trav-share-icon" ></i>
                                            </a>
                                        </span>
                                        <span id="post_share_count_616211" >
                                            <a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="post-comment-wrapper"></div>
                            </div>
                            <!-- Primary post type - Review with multiple media END -->
                            <!-- Primary post type for trip plans -->
                            <div class="post-block post-block-notification" >
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        <a class="post-name-link" href="https://travooo.com/profile/831" >Katherin</a> shared a story from <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan's</a> trip plan
                                    </div>
                                </div>
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap ava-50" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1795/1598930510_image.png" alt="" >
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/1795" >Lei Wen</a>
                                </div>
                                <div class="post-info" >
                                shared a <strong>story</strong> story about
                                <a href="https://travooo.com/place/274" class="link-place" >Indian Club</a>
                                on 2 days ago
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-user-plus-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Unfollow User</b></p>
                                            <p >Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Share</b></p>
                                            <p >Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-link" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Copy Link</b></p>
                                            <p >Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-on-travo-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Send to a Friend</b></p>
                                            <p >Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-heart-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Add to Favorites</b></p>
                                            <p >Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-flag-icon-o" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Report</b></p>
                                            <p >Help us understand</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-edit"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Edit Post</b></p>
                                            <p >Edit the text or add the media</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-trash-alt"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b  style="color: red;">Delete</b></p>
                                            <p >Delete this post forever</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <!-- New elements -->
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                </div>
                                <!-- New elements END -->
                                <div class="topic-inside-panel-wrap" >
                                <div class="topic-inside-panel" >
                                <div class="panel-txt w-75" >
                                <h3 class="panel-ttl" >
                                    18 Natural Wonders of The US That Will Inspire Your Next Road Trip
                                </h3>
                                <!-- <p style="overflow-wrap: break-word;" >
                                I want to know about the special things in South Korea. It will help me in my upcoming trip.</p> -->
                                <button class="btn btn-outline-secondary">Read Story</button>
                                </div>
                                <div class="panel-img disc-panel-img" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg" alt="" style="width: 230px;
    height: 195px;" >
                                </div>
                                </div>
                                </div>
                                <div class="post-footer-info" >
                                    <!-- Updated elements -->
                                    <div class="post-foot-block post-reaction">
                                        <span class="post_like_button" id="601451">
                                            <a href="#">
                                                <i class="trav-heart-fill-icon"></i>
                                            </a>
                                        </span>
                                        <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                            <b >0</b> Likes
                                        </span>
                                    </div>
                                    <!-- Updated element END -->
                                    <div class="post-foot-block" >
                                        <a href="#" data-tab="comments616211" >
                                            <i class="trav-comment-icon" ></i>
                                        </a>
                                        <ul class="foot-avatar-list" >
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                        </ul>
                                        <span >
                                            <a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a>
                                        </span>
                                    </div>
                                    <div class="post-foot-block ml-auto" >
                                        <span class="post_share_button" id="616211" >
                                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                                <i class="trav-share-icon" ></i>
                                            </a>
                                        </span>
                                        <span id="post_share_count_616211" >
                                            <a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="post-comment-wrapper"></div>
                                <!-- Copied from home page -->
                                <div class="post-comment-layer" data-content="comments622665" style="display: block;" dir="auto">
                                <div class="post-comment-top-info" dir="auto">
                                <ul class="comment-filter" dir="auto">
                                <li class="active" onclick="commentSort('Top', this, $(this).closest('.post-block'))" dir="auto">Top</li>
                                <li onclick="commentSort('New', this, $(this).closest('.post-block'))" dir="auto">New</li>
                                </ul>
                                <div class="comm-count-info" dir="auto">
                                <strong dir="auto">2</strong> / <span class="622665-comments-count" dir="auto">2</span>
                                </div>
                                </div>
                                <div class="post-comment-wrapper sortBody" id="comments622665" dir="auto" travooo-comment-rows="2" travooo-current-page="1">
                                
                                <div topsort="1" newsort="1601563197" dir="auto" class="displayed" style="">
                                <div class="post-comment-row news-feed-comment" id="commentRow13234" dir="auto">
                                <div class="post-com-avatar-wrap" dir="auto">
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" dir="auto">
                                </div>
                                <div class="post-comment-text" dir="auto">
                                <div class="post-com-name-layer" dir="auto">
                                <a href="#" class="comment-name" dir="auto">Ivan Turlakov</a>
                                <div class="post-com-top-action" dir="auto">
                                <div class="dropdown " dir="auto">
                                <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                                <i class="trav-angle-down" dir="auto"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment" dir="auto">
                                <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="13234" data-post="622665" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                <i class="trav-pencil" aria-hidden="true" dir="auto"></i>
                                </span>
                                <div class="drop-txt comment-edit__drop-text" dir="auto">
                                <p dir="auto"><b dir="auto">Edit</b></p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="comment-txt comment-text-13234" dir="auto">
                                <p dir="auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id erat
    a velit mollis consectetur nec quis nunc. Morbi a neque vel lacus tincidunt
    vehicula non non justo.</p>
                                <form class="commentEditForm13234 comment-edit-form d-none" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                                <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto"><input type="hidden" data-id="pair13234" name="pair" value="5f770bda7ee63" dir="auto">
                                <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0" dir="auto">
                                <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                                <textarea name="text" data-id="text13234" class="textarea-customize post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="Write a comment" dir="auto">Text comment</textarea>
                                </div>
                                <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                                <div class="post-alloptions" dir="auto">
                                <ul class="create-link-list" dir="auto">
                                <li class="post-options" dir="auto">
                                <input type="file" name="file[]" class="commenteditfile" id="commenteditfile13234" style="display:none" multiple="" dir="auto">
                                <i class="fa fa-camera click-target" data-target="commenteditfile13234" dir="auto"></i>
                                </li>
                                </ul>
                                </div>
                                <div class="comment-edit-action" dir="auto">
                                <a href="javascript:;" class="edit-cancel-link" data-comment_id="13234" dir="auto">Cancel</a>
                                <a href="javascript:;" class="edit-post-link" data-comment_id="13234" dir="auto">Post</a>
                                </div>
                                </div>
                                <div class="medias p-media b-whitesmoke" dir="auto">
                                </div>
                                </div>
                                <input type="hidden" name="post_id" value="622665" dir="auto">
                                <input type="hidden" name="comment_id" value="13234" dir="auto">
                                <input type="hidden" name="comment_type" value="1" dir="auto">
                                <button type="submit" class="d-none" dir="auto"></button>
                                </form>
                                </div>
                                <div class="post-image-container" dir="auto">
                                </div>
                                <div class="comment-bottom-info" dir="auto">
                                <div class="comment-info-content" dir="auto">
                                <div class="dropdown" dir="auto">
                                <a href="#" class="postCommentLikes like-link dropbtn" id="13234" dir="auto"><i class="trav-heart-fill-icon red" aria-hidden="true" dir="auto"></i> <span class="comment-like-count" dir="auto">0</span></a>
                                </div>
                                <a href="#" class="postCommentReply reply-link" id="13234" dir="auto">Reply</a>
                                <span class="com-time" dir="auto"><span class="comment-dot" dir="auto"> · </span>20 hours ago</span>
                                </div>
                                </div>
                                </div>
                                </div>
                                
                                <div class="post-add-comment-block" id="replyForm13234" style="padding-top:0px;padding-left: 59px;display:none;" dir="auto">
                                <div class="avatar-wrap" style="padding-right:10px" dir="auto">
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" style="width:30px !important;height:30px !important;" dir="auto">
                                </div>
                                <div class="post-add-com-inputs" dir="auto">
                                <form class="commentReplyForm13234" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                                <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto">
                                <input type="hidden" data-id="pair13234" name="pair" value="5f770bda8135d" dir="auto">
                                <div class="post-create-block post-comment-create-block post-reply-block" tabindex="0" dir="auto">
                                <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                                <textarea name="text" data-id="text13234" class="textarea-customize post-comment-emoji" style="display: none; vertical-align: top; min-height: 50px;" placeholder="Write a comment" dir="auto"></textarea><div class="note-editor note-frame panel panel-default"><div class="note-dropzone">  <div class="note-dropzone-message" dir="auto"></div></div><div class="note-toolbar panel-heading" role="toolbar" dir="auto"><div class="note-btn-group btn-group note-insert"><button type="button" class="note-btn btn btn-default btn-sm" role="button" tabindex="-1" dir="auto"><div class="emoji-picker-container emoji-picker emojionearea-button d-none" dir="auto"></div></button><div class="emoji-menu 00b5dc61-5c5c-40de-9479-07713f85c370" style="display: none;">
                                    <div class="emoji-items-wrap1" dir="auto">
                                        <table class="emoji-menu-tabs" dir="auto">
                                            <tbody dir="auto">
                                            <tr dir="auto">
                                                <td dir="auto"><a class="emoji-menu-tab icon-recent-selected" dir="auto"></a></td>
                                                <td dir="auto"><a class="emoji-menu-tab icon-smile" dir="auto"></a></td>
                                                <td dir="auto"><a class="emoji-menu-tab icon-flower" dir="auto"></a></td>
                                                <td dir="auto"><a class="emoji-menu-tab icon-bell" dir="auto"></a></td>
                                                <td dir="auto"><a class="emoji-menu-tab icon-car" dir="auto"></a></td>
                                                <td dir="auto"><a class="emoji-menu-tab icon-grid" dir="auto"></a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="emoji-items-wrap mobile_scrollable_wrap" dir="auto">
                                            <div class="emoji-items" dir="auto"><a href="javascript:void(0)" title=":flushed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -350px 0px no-repeat;background-size:675px 175px;" alt=":flushed:" dir="auto"><span class="label" dir="auto">:flushed:</span></a><a href="javascript:void(0)" title=":grin:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -375px 0px no-repeat;background-size:675px 175px;" alt=":grin:" dir="auto"><span class="label" dir="auto">:grin:</span></a><a href="javascript:void(0)" title=":rage:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px -25px no-repeat;background-size:675px 175px;" alt=":rage:" dir="auto"><span class="label" dir="auto">:rage:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -300px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_closed_eyes:" dir="auto"><span class="label" dir="auto">:stuck_out_tongue_closed_eyes:</span></a><a href="javascript:void(0)" title=":kissing_heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px 0px no-repeat;background-size:675px 175px;" alt=":kissing_heart:" dir="auto"><span class="label" dir="auto">:kissing_heart:</span></a><a href="javascript:void(0)" title=":ok_hand:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -650px -75px no-repeat;background-size:675px 175px;" alt=":ok_hand:" dir="auto"><span class="label" dir="auto">:ok_hand:</span></a><a href="javascript:void(0)" title=":heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -250px -150px no-repeat;background-size:675px 175px;" alt=":heart:" dir="auto"><span class="label" dir="auto">:heart:</span></a><a href="javascript:void(0)" title=":frog:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_1.png') -150px 0px no-repeat;background-size:725px 100px;" alt=":frog:" dir="auto"><span class="label" dir="auto">:frog:</span></a><a href="javascript:void(0)" title=":+1:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -75px no-repeat;background-size:675px 175px;" alt=":+1:" dir="auto"><span class="label" dir="auto">:+1:</span></a><a href="javascript:void(0)" title=":heart_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -150px 0px no-repeat;background-size:675px 175px;" alt=":heart_eyes:" dir="auto"><span class="label" dir="auto">:heart_eyes:</span></a><a href="javascript:void(0)" title=":blush:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px 0px no-repeat;background-size:675px 175px;" alt=":blush:" dir="auto"><span class="label" dir="auto">:blush:</span></a><a href="javascript:void(0)" title=":joy:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -550px 0px no-repeat;background-size:675px 175px;" alt=":joy:" dir="auto"><span class="label" dir="auto">:joy:</span></a><a href="javascript:void(0)" title=":relaxed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -100px 0px no-repeat;background-size:675px 175px;" alt=":relaxed:" dir="auto"><span class="label" dir="auto">:relaxed:</span></a><a href="javascript:void(0)" title=":pensive:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -400px 0px no-repeat;background-size:675px 175px;" alt=":pensive:" dir="auto"><span class="label" dir="auto">:pensive:</span></a><a href="javascript:void(0)" title=":smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px 0px no-repeat;background-size:675px 175px;" alt=":smile:" dir="auto"><span class="label" dir="auto">:smile:</span></a><a href="javascript:void(0)" title=":sob:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -575px 0px no-repeat;background-size:675px 175px;" alt=":sob:" dir="auto"><span class="label" dir="auto">:sob:</span></a><a href="javascript:void(0)" title=":kiss:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px -150px no-repeat;background-size:675px 175px;" alt=":kiss:" dir="auto"><span class="label" dir="auto">:kiss:</span></a><a href="javascript:void(0)" title=":unamused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -450px 0px no-repeat;background-size:675px 175px;" alt=":unamused:" dir="auto"><span class="label" dir="auto">:unamused:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_winking_eye:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_winking_eye:" dir="auto"><span class="label" dir="auto">:stuck_out_tongue_winking_eye:</span></a><a href="javascript:void(0)" title=":see_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px -75px no-repeat;background-size:675px 175px;" alt=":see_no_evil:" dir="auto"><span class="label" dir="auto">:see_no_evil:</span></a><a href="javascript:void(0)" title=":wink:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px 0px no-repeat;background-size:675px 175px;" alt=":wink:" dir="auto"><span class="label" dir="auto">:wink:</span></a><a href="javascript:void(0)" title=":smiley:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -25px 0px no-repeat;background-size:675px 175px;" alt=":smiley:" dir="auto"><span class="label" dir="auto">:smiley:</span></a><a href="javascript:void(0)" title=":cry:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -525px 0px no-repeat;background-size:675px 175px;" alt=":cry:" dir="auto"><span class="label" dir="auto">:cry:</span></a><a href="javascript:void(0)" title=":scream:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -25px no-repeat;background-size:675px 175px;" alt=":scream:" dir="auto"><span class="label" dir="auto">:scream:</span></a><a href="javascript:void(0)" title=":smirk:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px -50px no-repeat;background-size:675px 175px;" alt=":smirk:" dir="auto"><span class="label" dir="auto">:smirk:</span></a><a href="javascript:void(0)" title=":disappointed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px 0px no-repeat;background-size:675px 175px;" alt=":disappointed:" dir="auto"><span class="label" dir="auto">:disappointed:</span></a><a href="javascript:void(0)" title=":sweat_smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px -25px no-repeat;background-size:675px 175px;" alt=":sweat_smile:" dir="auto"><span class="label" dir="auto">:sweat_smile:</span></a><a href="javascript:void(0)" title=":kissing_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -200px 0px no-repeat;background-size:675px 175px;" alt=":kissing_closed_eyes:" dir="auto"><span class="label" dir="auto">:kissing_closed_eyes:</span></a><a href="javascript:void(0)" title=":speak_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -75px no-repeat;background-size:675px 175px;" alt=":speak_no_evil:" dir="auto"><span class="label" dir="auto">:speak_no_evil:</span></a><a href="javascript:void(0)" title=":relieved:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -425px 0px no-repeat;background-size:675px 175px;" alt=":relieved:" dir="auto"><span class="label" dir="auto">:relieved:</span></a><a href="javascript:void(0)" title=":grinning:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px 0px no-repeat;background-size:675px 175px;" alt=":grinning:" dir="auto"><span class="label" dir="auto">:grinning:</span></a><a href="javascript:void(0)" title=":yum:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px -25px no-repeat;background-size:675px 175px;" alt=":yum:" dir="auto"><span class="label" dir="auto">:yum:</span></a><a href="javascript:void(0)" title=":neutral_face:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -25px no-repeat;background-size:675px 175px;" alt=":neutral_face:" dir="auto"><span class="label" dir="auto">:neutral_face:</span></a><a href="javascript:void(0)" title=":confused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -625px -25px no-repeat;background-size:675px 175px;" alt=":confused:" dir="auto"><span class="label" dir="auto">:confused:</span></a></div>
                                        </div>
                                    </div>
                                </div></div><div class="note-btn-group btn-group note-custom"></div></div><div class="note-editing-area" dir="auto"><div class="note-placeholder custom-placeholder" style="display: block;">Write a comment</div><div class="note-handle"><div class="note-control-selection" dir="auto"><div class="note-control-selection-bg" dir="auto"></div><div class="note-control-holder note-control-nw" dir="auto"></div><div class="note-control-holder note-control-ne" dir="auto"></div><div class="note-control-holder note-control-sw" dir="auto"></div><div class="note-control-sizing note-control-se" dir="auto"></div><div class="note-control-selection-info" dir="auto"></div></div></div><textarea class="note-codable" role="textbox" aria-multiline="true" dir="auto"></textarea><div class="note-editable" contenteditable="true" role="textbox" aria-multiline="true" dir="auto" spellcheck="true"><div><br dir="auto"></div></div></div><output class="note-status-output" aria-live="polite" dir="auto"></output><div class="note-statusbar" role="status" dir="auto" style="">  <div class="note-resizebar" role="seperator" aria-orientation="horizontal" aria-label="Resize" dir="auto">    <div class="note-icon-bar" dir="auto"></div>    <div class="note-icon-bar" dir="auto"></div>    <div class="note-icon-bar" dir="auto"></div>  </div></div><div class="modal link-dialog" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Link"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Link</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group" dir="auto"><label class="note-form-label" dir="auto">Text to display</label><input class="note-link-text form-control note-form-control note-input" type="text" dir="auto"></div><div class="form-group note-form-group" dir="auto"><label class="note-form-label" dir="auto">To what URL should this link go?</label><input class="note-link-url form-control note-form-control note-input" type="text" value="http://" dir="auto"></div><div class="checkbox sn-checkbox-open-in-new-window" dir="auto"><label dir="auto"> <input role="checkbox" type="checkbox" checked="" aria-checked="true" dir="auto">Open in new window</label></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-link-btn" value="Insert Link" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Image"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Image</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group note-group-select-from-files" dir="auto"><label class="note-form-label" dir="auto">Select from files</label><input class="note-image-input form-control-file note-form-control note-input" type="file" name="files" accept="image/*" multiple="multiple" dir="auto"></div><div class="form-group note-group-image-url" style="overflow:auto;" dir="auto"><label class="note-form-label" dir="auto">Image URL</label><input class="note-image-url form-control note-form-control note-input  col-md-12" type="text" dir="auto"></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-image-btn" value="Insert Image" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Video"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Video</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group row-fluid" dir="auto"><label class="note-form-label" dir="auto">Video URL <small class="text-muted" dir="auto">(YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)</small></label><input class="note-video-url form-control note-form-control note-input" type="text" dir="auto"></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-video-btn" value="Insert Video" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Help"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Help</h4>    </div>    <div class="modal-body" dir="auto" style="max-height: 300px; overflow: scroll;"><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+Z</kbd></label><span dir="auto">Undoes the last command</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+Y</kbd></label><span dir="auto">Redoes the last command</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">TAB</kbd></label><span dir="auto">Tab</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">SHIFT+TAB</kbd></label><span dir="auto">Untab</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+B</kbd></label><span dir="auto">Set a bold style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+I</kbd></label><span dir="auto">Set a italic style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+U</kbd></label><span dir="auto">Set a underline style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+S</kbd></label><span dir="auto">Set a strikethrough style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+BACKSLASH</kbd></label><span dir="auto">Clean a style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+L</kbd></label><span dir="auto">Set left align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+E</kbd></label><span dir="auto">Set center align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+R</kbd></label><span dir="auto">Set right align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+J</kbd></label><span dir="auto">Set full align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+NUM7</kbd></label><span dir="auto">Toggle unordered list</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+NUM8</kbd></label><span dir="auto">Toggle ordered list</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+LEFTBRACKET</kbd></label><span dir="auto">Outdent on current paragraph</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+RIGHTBRACKET</kbd></label><span dir="auto">Indent on current paragraph</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM0</kbd></label><span dir="auto">Change current block's format as a paragraph(P tag)</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM1</kbd></label><span dir="auto">Change current block's format as H1</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM2</kbd></label><span dir="auto">Change current block's format as H2</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM3</kbd></label><span dir="auto">Change current block's format as H3</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM4</kbd></label><span dir="auto">Change current block's format as H4</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM5</kbd></label><span dir="auto">Change current block's format as H5</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM6</kbd></label><span dir="auto">Change current block's format as H6</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+ENTER</kbd></label><span dir="auto">Insert horizontal rule</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+K</kbd></label><span dir="auto">Show Link Dialog</span></div>    <div class="modal-footer" dir="auto"><p class="text-center" dir="auto"><a href="http://summernote.org/" target="_blank" dir="auto">Summernote 0.8.12</a> · <a href="https://github.com/summernote/summernote" target="_blank" dir="auto">Project</a> · <a href="https://github.com/summernote/summernote/issues" target="_blank" dir="auto">Issues</a></p></div>  </div></div></div></div>
                                <div class="textcomplete-wrapper"></div></div>
                                <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                                <div class="post-alloptions" dir="auto">
                                <ul class="create-link-list" dir="auto">
                                <li class="post-options" dir="auto">
                                <input type="file" name="file[]" id="commentreplyfile13234" style="display:none" multiple="" dir="auto">
                                <i class="fa fa-camera click-target" data-target="commentreplyfile13234" dir="auto"></i>
                                </li>
                                </ul>
                                </div>
                                <button type="submit" class="btn btn-primary d-none" dir="auto"></button>
                                <div class="comment-edit-action" dir="auto">
                                <a href="javascript:;" class="p-comment-cancel-link post-r-cancel-link" dir="auto">Cancel</a>
                                <a href="javascript:;" class="p-comment-link post-r-reply-comment-link" data-comment_id="13234" dir="auto">Post</a>
                                </div>
                                </div>
                                <div class="medias p-media b-whitesmoke" dir="auto"></div>
                                </div>
                                <input type="hidden" name="post_id" value="622665" dir="auto">
                                <input type="hidden" name="comment_id" value="13234" dir="auto">
                                </form>
                                </div>
                                </div>
                                </div>
                                </div>
                                <a href="#" class="load-more-comments">Load more...</a>
                                <div class="post-add-comment-block" dir="auto">
                                <div class="avatar-wrap" dir="auto">
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" style="width:45px;height:45px;" dir="auto">
                                </div>
                                <!-- New elements -->
                                <div class="add-comment-input-group">
                                    <input type="text" placeholder="Write a comment...">
                                    <div style="position: relative;">
                                        <button class="add-post__emoji" emoji-target="add_post_text" type="button" onclick="showFaceBlock()" id="faceEnter">🙂</button><div id="faceBlock" style="background:rgb(216, 216, 216);border-radius: 12px;display: none;position: absolute;border: 1px solid #e2e2e2;padding: 5px;top: 25px;width: 300px;z-index:100;"><i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😀&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😀</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😁&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😁</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😂&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😂</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤣&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤣</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😃&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😃</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😄&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😄</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😅&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😅</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😆&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😆</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😉&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😉</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😊&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😊</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😋&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😋</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😎&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😎</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😍&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😍</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😘&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😘</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😗&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😗</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😙&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😙</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😚&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😚</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🙂&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🙂</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤗&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤗</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤩&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤩</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤔&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤔</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤨&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤨</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😐&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😐</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😑&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😑</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😶&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😶</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🙄&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🙄</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😏&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😏</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😣&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😣</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😥&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😥</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😮&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😮</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤐&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤐</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😯&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😯</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😪&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😪</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😫&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😫</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😴&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😴</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😌&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😌</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😛&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😛</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😜&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😜</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😝&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😝</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤤&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤤</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😒&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😒</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😓&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😓</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😔&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😔</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😕&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😕</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🙃&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🙃</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤑&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤑</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😲&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😲</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;☹&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">☹</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🙁&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🙁</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😖&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😖</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😞&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😞</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😟&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😟</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😤&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😤</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😢&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😢</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😭&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😭</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😦&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😦</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😧&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😧</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😨&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😨</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😩&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😩</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤯&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤯</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😬&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😬</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😰&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😰</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😱&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😱</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😳&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😳</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤪&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤪</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😵&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😵</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😠&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😠</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😡&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😡</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤬&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤬</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😷&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😷</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤒&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤒</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤕&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤕</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤢&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤢</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤮&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤮</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤧&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤧</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😇&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😇</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤠&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤠</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤡&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤡</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤥&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤥</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤫&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤫</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤭&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤭</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🧐&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🧐</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤓&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤓</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😈&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😈</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👿&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👿</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👹&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👹</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👺&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👺</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;💀&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">💀</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;☠&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">☠</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👻&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👻</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👽&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👽</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👾&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👾</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤖&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤖</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;💩&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">💩</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😺&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😺</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😸&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😸</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😹&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😹</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😻&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😻</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😼&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😼</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😽&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😽</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🙀&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🙀</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😿&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😿</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;😾&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">😾</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐱&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐱</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👤&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👤</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐱&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐱</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;‍🏍&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">‍🏍</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐱&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐱</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;💻&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">💻</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐱&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐱</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;‍🐉&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">‍🐉</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐱&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐱</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👓&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👓</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐱&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐱</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🚀&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🚀</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🙈&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🙈</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🙉&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🙉</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🙊&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🙊</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐵&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐵</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐶&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐶</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐺&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐺</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐱&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐱</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦁&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦁</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐯&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐯</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦒&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦒</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦊&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦊</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐮&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐮</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐷&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐷</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐗&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐗</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐭&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐭</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐹&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐹</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐰&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐰</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐻&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐻</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐨&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐨</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐼&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐼</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐸&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐸</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦓&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦓</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐴&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐴</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦄&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦄</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐔&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐔</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐲&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐲</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐾&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐾</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐽&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐽</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐒&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐒</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦍&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦍</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐕&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐕</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐩&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐩</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐕&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐕</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐈&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐈</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐅&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐅</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐆&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐆</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐎&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐎</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦌&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦌</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦏&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦏</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐂&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐂</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐃&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐃</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐄&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐄</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐖&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐖</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐏&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐏</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐑&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐑</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐐&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐐</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐪&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐪</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐫&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐫</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐘&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐘</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐁&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐁</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐀&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐀</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦔&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦔</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐇&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐇</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐿&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐿</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦎&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦎</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐊&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐊</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐢&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐢</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐍&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐍</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐉&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐉</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦕&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦕</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦖&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦖</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦈&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦈</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐬&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐬</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐳&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐳</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐋&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐋</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐟&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐟</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐠&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐠</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐡&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐡</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦐&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦐</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦑&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦑</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐙&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐙</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦀&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦀</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐚&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐚</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦆&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦆</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐓&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐓</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦃&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦃</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦅&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦅</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🕊&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🕊</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦉&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦉</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐦&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐦</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐧&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐧</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐥&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐥</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐤&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐤</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐣&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐣</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦇&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦇</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦋&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦋</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐌&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐌</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐛&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐛</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦗&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦗</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐜&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐜</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐝&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐝</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🐞&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🐞</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🦂&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🦂</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🕷&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🕷</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🕸&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🕸</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🧞&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🧞</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🗣&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🗣</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👤&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👤</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👥&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👥</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👁&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👁</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👀&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👀</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👅&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👅</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👄&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👄</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🧠&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🧠</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;👣&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">👣</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;🤺&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">🤺</i>&nbsp;<i onclick="insertAtCaret(&quot;add_post_text&quot;,&quot;⛷&quot;,this)" style="font: normal normal normal 14px/1 FontAwesome;cursor: pointer;padding:3px;font-size:20px;width: 20px;display: inline-block;text-align:center;">⛷</i>&nbsp;</div>
                                        <button><i class="trav-camera click-target" data-target="file"></i></button>
                                    </div>
                                </div>
                                <!-- New elements END  -->
                                </div>
                                </div>
                                <!-- Copied from home page END -->
                            </div>
                            <!-- Primary post type for trip plans END -->
                            <!-- Primary post type for discussions -->
                            <div class="post-block post-block-notification" >
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        <i class="fa fa-question-circle" rel="tooltip" data-toggle="tooltip" data-animation="false" title=""  data-original-title="The total number of views your posts have garnered"></i> <strong>Question</strong> for you
                                    </div>
                                </div>
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap ava-50" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1795/1598930510_image.png" alt="" >
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/1795" >Lei Wen</a>
                                </div>
                                <div class="post-info" >
                                Asked for <strong>tips</strong> about
                                <a href="https://travooo.com/place/274" class="link-place" >Indian Club</a>
                                on 2 days ago
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-user-plus-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Unfollow User</b></p>
                                            <p >Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Share</b></p>
                                            <p >Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-link" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Copy Link</b></p>
                                            <p >Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-on-travo-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Send to a Friend</b></p>
                                            <p >Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-heart-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Add to Favorites</b></p>
                                            <p >Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-flag-icon-o" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Report</b></p>
                                            <p >Help us understand</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-edit"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Edit Post</b></p>
                                            <p >Edit the text or add the media</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-trash-alt"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b  style="color: red;">Delete</b></p>
                                            <p >Delete this post forever</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="topic-inside-panel-wrap" >
                                <div class="topic-inside-panel" >
                                <div class="panel-txt" >
                                <h3 class="panel-ttl" >
                                    Any tips before I go to <a href="">New York City</a>?
                                </h3>
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                </div>
                                </div>
                                <div class="panel-img disc-panel-img" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg" alt="" style="width: 130px;
    height: 140px;" >
                                </div>
                                </div>
                                </div>
                                <div class="post-footer-info" >
                                    <!-- Updated elements -->
                                    <div class="post-foot-block answer-cta">
                                        <button class="btn btn-light">
                                            <i class="trav-pencil"></i> Answer
                                        </button>
                                        Be the first to answer this question
                                    </div>
                                    <!-- Updated element END -->
                                    <div class="post-foot-block ml-auto" >
                                        <span class="post_share_button" id="616211" >
                                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                                <i class="trav-share-icon" ></i>
                                            </a>
                                        </span>
                                        <span id="post_share_count_616211" >
                                            <a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="post-comment-wrapper"></div>
                            </div>
                            <!-- Primary post type for discussions END -->
                            <!-- Primary post type for discussions -->
                            <div class="post-block post-block-notification" >
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        <i class="fa fa-question-circle" rel="tooltip" data-toggle="tooltip" data-animation="false" title=""  data-original-title="The total number of views your posts have garnered"></i> <strong>Question</strong> for you
                                    </div>
                                </div>
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap ava-50" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1795/1598930510_image.png" alt="" >
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/1795" >Lei Wen</a>
                                </div>
                                <div class="post-info" >
                                Asked for <strong>tips</strong> about
                                <a href="https://travooo.com/place/274" class="link-place" >Indian Club</a>
                                on 2 days ago
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-user-plus-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Unfollow User</b></p>
                                            <p >Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Share</b></p>
                                            <p >Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-link" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Copy Link</b></p>
                                            <p >Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-on-travo-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Send to a Friend</b></p>
                                            <p >Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-heart-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Add to Favorites</b></p>
                                            <p >Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-flag-icon-o" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Report</b></p>
                                            <p >Help us understand</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-edit"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Edit Post</b></p>
                                            <p >Edit the text or add the media</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-trash-alt"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b  style="color: red;">Delete</b></p>
                                            <p >Delete this post forever</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="topic-inside-panel-wrap" >
                                <div class="topic-inside-panel" >
                                <div class="panel-txt" >
                                <h3 class="panel-ttl" >
                                    Any tips before I go to <a href="">New York City</a>?
                                </h3>
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                </div>
                                </div>
                                <div class="panel-img disc-panel-img" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg" alt="" style="width: 130px;
    height: 140px;" >
                                </div>
                                </div>
                                </div>
                                <!-- Top tips section -->
                                <div class="post-tips-top-layer post-tips-wrapper" dir="auto">
                                <div class="post-tips-top" dir="auto">
                                <h4 class="post-tips-ttl" dir="auto">Top Tips</h4>
                                </div>
                                <div class="post-tips-main-block" dir="auto">
                                <div class="post-tips-row" dir="auto">
                                <div class="tips-top" dir="auto">
                                <div class="tip-avatar" dir="auto">
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1993/1601255651_image.png" alt="" style="width:25px;height:25px;" dir="auto">
                                </div>
                                <div class="tip-content ov-wrap" dir="auto">
                                <div class="top-content-top" dir="auto">
                                <a href="https://travooo.com/profile/1993" class="name-link" dir="auto">Svetlana Vysoká</a>
                                <span dir="auto">said</span><span class="dot" dir="auto"> · </span>
                                <span dir="auto">3 hours ago</span>
                                <div class="d-inline-block pull-right" dir="auto">
                                <div class="post-top-info-action" dir="auto">
                                <div class="dropdown" dir="auto">
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                                <i class="trav-angle-down" dir="auto"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="DiscussionReplies" dir="auto">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(9628,this)" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                <i class="trav-flag-icon-o" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Report</b></p>
                                <p dir="auto">Help us understand</p>
                                </div>
                                </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="tip-txt ov-wrap" dir="auto">
                                <p class="dis_reply_9628 ov-wrap disc-text-block" dir="auto">
                                Duis iaculis ex ac imperdiet viverra praesent sollicitudin nunc vel pulvinar sodales sed a leo feugiat, condimentum odio vitae, auctor lorem mauris cursus libero sed pretium faucibus.</p>
                                </div>
                                </div>
                                </div>
                                <div class="tips-footer updownvote" id="9629" dir="auto">
                                <a href="#" class="upvote-link up" id="9629" dir="auto">
                                <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-up" dir="auto"></i></span>
                                </a>
                                <span><b class="upvote-count" dir="auto">12</b> Upvotes</span>
                                <div class="more-tips">
                                    <ul class="avatar-list">
                                        <li><img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava"></li>
                                        <li><img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava"></li>
                                    </ul>
                                    <a href="#"><b>2</b> more tips</a>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                <!-- Top tips section END -->
                            </div>
                            <!-- Primary post type for discussions END -->
                            <!-- Primary post type for discussions -->
                            <div class="post-block post-block-notification" >
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        <i class="fa fa-question-circle" rel="tooltip" data-toggle="tooltip" data-animation="false" title=""  data-original-title="The total number of views your posts have garnered"></i> <strong>Question</strong> for you
                                    </div>
                                </div>
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap ava-50" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1795/1598930510_image.png" alt="" >
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/1795" >Lei Wen</a>
                                </div>
                                <div class="post-info" >
                                Asked for <strong>tips</strong> about
                                <a href="https://travooo.com/place/274" class="link-place" >Indian Club</a>
                                on 2 days ago
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-user-plus-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Unfollow User</b></p>
                                            <p >Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Share</b></p>
                                            <p >Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-link" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Copy Link</b></p>
                                            <p >Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-on-travo-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Send to a Friend</b></p>
                                            <p >Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-heart-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Add to Favorites</b></p>
                                            <p >Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-flag-icon-o" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Report</b></p>
                                            <p >Help us understand</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-edit"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Edit Post</b></p>
                                            <p >Edit the text or add the media</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-trash-alt"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b  style="color: red;">Delete</b></p>
                                            <p >Delete this post forever</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="topic-inside-panel-wrap" >
                                <div class="topic-inside-panel" >
                                <div class="panel-txt" >
                                <h3 class="panel-ttl" >
                                    Any tips before I go to <a href="">New York City</a>?
                                </h3>
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                </div>
                                </div>
                                
                                </div>
                                </div>
                                <div class="post-footer-info" >
                                    <!-- Updated elements -->
                                    <div class="post-foot-block answer-cta">
                                        <button class="btn btn-light">
                                            <i class="trav-pencil"></i> Answer
                                        </button>
                                        Be the first to answer this question
                                    </div>
                                    <!-- Updated element END -->
                                    <div class="post-foot-block ml-auto" >
                                        <span class="post_share_button" id="616211" >
                                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                                <i class="trav-share-icon" ></i>
                                            </a>
                                        </span>
                                        <span id="post_share_count_616211" >
                                            <a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="post-comment-wrapper"></div>
                            </div>
                            <!-- Primary post type for discussions END -->
                            <!-- Primary post type for travlogs -->
                            <div class="post-block post-block-notification travlog" >
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        <a class="post-name-link" href="https://travooo.com/profile/831" >Cheryl</a> updated her travelog 2 hours ago
                                    </div>
                                </div>
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap ava-50" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1795/1598930510_image.png" alt="" >
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/1795" >Cheryl Cornett</a>
                                </div>
                                <div class="post-info" >
                                wrote a <strong>travelog</strong> about
                                <a href="https://travooo.com/place/274" class="link-place" >Indian Club</a>
                                on 2 days ago
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-user-plus-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Unfollow User</b></p>
                                            <p >Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Share</b></p>
                                            <p >Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-link" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Copy Link</b></p>
                                            <p >Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-on-travo-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Send to a Friend</b></p>
                                            <p >Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-heart-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Add to Favorites</b></p>
                                            <p >Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-flag-icon-o" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Report</b></p>
                                            <p >Help us understand</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-edit"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Edit Post</b></p>
                                            <p >Edit the text or add the media</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-trash-alt"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b  style="color: red;">Delete</b></p>
                                            <p >Delete this post forever</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="topic-inside-panel-wrap" >
                                <div class="topic-inside-panel" >
                                <div class="panel-txt" >
                                <h3 class="panel-ttl" >A day in Disneyland Park</h3>
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                    <div class="read-time-badge">15 min read</div>
                                </div>
                                </div>
                                <div class="panel-img disc-panel-img" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg" alt="" style="width: 170px;
    height: 120px;" >
                                </div>
                                </div>
                                </div>
                                <div class="post-footer-info" >
                                    <!-- Updated elements -->
                                    <div class="post-foot-block post-reaction">
                                        <span class="post_like_button" id="601451">
                                            <a href="#">
                                                <i class="trav-heart-fill-icon"></i>
                                            </a>
                                        </span>
                                        <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                            <b >0</b> Likes
                                        </span>
                                    </div>
                                    <!-- Updated element END -->
                                    <div class="post-foot-block" >
                                        <a href="#" data-tab="comments616211" >
                                            <i class="trav-comment-icon" ></i>
                                        </a>
                                        <ul class="foot-avatar-list" >
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                        </ul>
                                        <span >
                                            <a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a>
                                        </span>
                                    </div>
                                    <div class="post-foot-block ml-auto" >
                                        <span class="post_share_button" id="616211" >
                                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                                <i class="trav-share-icon" ></i>
                                            </a>
                                        </span>
                                        <span id="post_share_count_616211" >
                                            <a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="post-comment-wrapper"></div>
                            </div>
                            <!-- Primary post type for travlogs END -->
                            <!-- Primary post type for links -->
                            <div class="post-block" >
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap ava-50" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1795/1598930510_image.png" alt="" >
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/1795" >Cheryl Cornett</a>
                                </div>
                                <div class="post-info" >
                                Posted a <strong>link</strong> about
                                <a href="https://travooo.com/place/274" class="link-place" >Indian Club</a>
                                on 2 days ago <i class="trav-globe-icon"></i>
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-user-plus-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Unfollow User</b></p>
                                            <p >Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Share</b></p>
                                            <p >Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-link" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Copy Link</b></p>
                                            <p >Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-on-travo-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Send to a Friend</b></p>
                                            <p >Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-heart-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Add to Favorites</b></p>
                                            <p >Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-flag-icon-o" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Report</b></p>
                                            <p >Help us understand</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-edit"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Edit Post</b></p>
                                            <p >Edit the text or add the media</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-trash-alt"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b  style="color: red;">Delete</b></p>
                                            <p >Delete this post forever</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="topic-inside-panel-wrap p-0" >
                                <div class="topic-inside-panel" >
                                <div class="panel-txt" >
                                <h3 class="panel-ttl" >
                                    Top 10 things you must do when visiting  <a href="">New York City <i class="trav-link-out-icon"></i></a>
                                </h3>
                                <div class="post-txt-wrap">
                                    <div>
                                        <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                        <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                    </div>
                                </div>
                                </div>
                                <div class="panel-img disc-panel-img" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg" alt="" style="width: 130px;
    height: 140px;" >
                                </div>
                                </div>
                                </div>
                                <div class="post-footer-info" >
                                    <!-- Updated elements -->
                                    <div class="post-foot-block post-reaction">
                                        <span class="post_like_button" id="601451">
                                            <a href="#">
                                                <i class="trav-heart-fill-icon"></i>
                                            </a>
                                        </span>
                                        <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                            <b >0</b> Likes
                                        </span>
                                    </div>
                                    <!-- Updated element END -->
                                    <div class="post-foot-block" >
                                        <a href="#" data-tab="comments616211" >
                                            <i class="trav-comment-icon" ></i>
                                        </a>
                                        <ul class="foot-avatar-list" >
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                        </ul>
                                        <span >
                                            <a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a>
                                        </span>
                                    </div>
                                    <div class="post-foot-block ml-auto" >
                                        <span class="post_share_button" id="616211" >
                                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                                <i class="trav-share-icon" ></i>
                                            </a>
                                        </span>
                                        <span id="post_share_count_616211" >
                                            <a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="post-comment-wrapper"></div>
                            </div>
                            <!-- Primary post type for links END -->
                            <!-- Secondary post type for events -->
                            <div class="post-block post-block-notification" >
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        Event in <a class="post-name-link" href="https://travooo.com/profile/831" >New York City</a>
                                    </div>
                                    <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-user-plus-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Unfollow User</b></p>
                                            <p >Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Share</b></p>
                                            <p >Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-link" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Copy Link</b></p>
                                            <p >Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-on-travo-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Send to a Friend</b></p>
                                            <p >Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-heart-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Add to Favorites</b></p>
                                            <p >Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-flag-icon-o" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Report</b></p>
                                            <p >Help us understand</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-edit"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Edit Post</b></p>
                                            <p >Edit the text or add the media</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-trash-alt"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b  style="color: red;">Delete</b></p>
                                            <p >Delete this post forever</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <!-- New elements -->
                                <div class="post-event-hero">
                                    <img class="post-event-hero-img" src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                    <div class="post-event-hero-caption">
                                        <div class="event-theme-badge">Art</div>
                                        <h3 class="event-title">
                                            <span>3 days event,  <strong>JUL 15 - JUL 18</strong></span>
                                            Quick Chek New Jersey Festival of Ballooning
                                        </h3>
                                    </div>
                                </div>
                                <div class="post-event-details">
                                    <img src="https://travooo.com/assets2/image/placeholders/place.png" alt="" class="post-event-details-map">
                                    <div class="post-event-details-text">
                                        <p><i class="trav-bookmark-icon"></i> By <strong>Avenevv Pte. Ltd.</strong></p>
                                        <p><i class="trav-set-location-icon"></i> <strong>The American Language Center, Oujda</strong></p>
                                        <p><i class="trav-star-icon"></i> <strong>20</strong> Reviews</p>
                                    </div>
                                </div>
                                <!-- New elements END -->
                                <div class="post-footer-info" >
                                    <!-- Updated elements -->
                                    <div class="post-foot-block post-reaction">
                                        <span class="post_like_button" id="601451">
                                            <a href="#">
                                                <i class="trav-heart-fill-icon"></i>
                                            </a>
                                        </span>
                                        <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                            <b >0</b> Likes
                                        </span>
                                    </div>
                                    <!-- Updated element END -->
                                    <div class="post-foot-block" >
                                        <a href="#" data-tab="comments616211" >
                                            <i class="trav-comment-icon" ></i>
                                        </a>
                                        <ul class="foot-avatar-list" >
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                        </ul>
                                        <span >
                                            <a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a>
                                        </span>
                                    </div>
                                    <div class="post-foot-block ml-auto" >
                                        <span class="post_share_button" id="616211" >
                                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                                <i class="trav-share-icon" ></i>
                                            </a>
                                        </span>
                                        <span id="post_share_count_616211" >
                                            <a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="post-comment-wrapper"></div>
                            </div>
                            <!-- Secondary post type for events END -->
                            <!-- Secondary post type for updates on trip plans -->
                            <div class="post-block post-block-notification travlog" >
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        The trip <a class="post-name-link" href="https://travooo.com/profile/831" >From Morocco to Japan</a> has started
                                    </div>
                                </div>
                                <div class="post-top-info-layer" >
                                <div class="post-top-info-wrap" >
                                <div class="post-top-avatar-wrap ava-50" >
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1795/1598930510_image.png" alt="" >
                                </div>
                                <div class="post-top-info-txt" >
                                <div class="post-top-name" >
                                <a class="post-name-link" href="https://travooo.com/profile/1795" >Michael Powell</a>
                                </div>
                                <div class="post-info" >
                                    <strong>Started</strong> his <strong>trip</strong> to
                                    <a href="https://travooo.com/place/274" class="link-place" >3 countries</a>
                                    2 days ago
                                </div>
                                </div>
                                </div>
                                <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-user-plus-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Unfollow User</b></p>
                                            <p >Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Share</b></p>
                                            <p >Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-link" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Copy Link</b></p>
                                            <p >Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-share-on-travo-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Send to a Friend</b></p>
                                            <p >Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-heart-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Add to Favorites</b></p>
                                            <p >Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="trav-flag-icon-o" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Report</b></p>
                                            <p >Help us understand</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-edit"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Edit Post</b></p>
                                            <p >Edit the text or add the media</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(3458,this)" >
                                        <span class="icon-wrap" >
                                            <i class="far fa-trash-alt"></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b  style="color: red;">Delete</b></p>
                                            <p >Delete this post forever</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                                </div>
                                <!-- New elements -->
                                <div class="trip-plan-update">
                                    <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg" alt="" class="trip-plan-update-cover">
                                    <div class="trip-plan-update-inner">
                                        <div class="trip-plan-update-badge">
                                            <span><i class="trav-dot-circle-icon"></i></span> Traveling
                                        </div>
                                        <!-- <div class="trip-plan-update-badge ended">
                                            <span><i class="trav-flag-icon"></i></span> Trip ended
                                        </div> -->
                                        <div class="trip-plan-update-details">
                                            <div class="flags">
                                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/es.png" alt="">
                                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/fr.png" alt="">
                                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/us.png" alt="">
                                            </div>
                                            <h3 class="ttl">From Morocco to Japan</h3>
                                            <div class="stats">
                                                <b>7</b> Days <b>&middot;</b> <b>$ 2,150</b> Budget <b>&middot;</b> <b>12</b> Destinations
                                            </div>
                                            <div class="desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque viverra, eros ut porta rutrum.</p>
                                                <button class="btn btn-light-primary">View Plan</button>
                                            </div>
                                            <div class="post-image-container" >
                                            <!-- New element -->
                                            <ul class="post-image-list" style="margin:0px 0px 1px 0px;" >
                                                <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                                    <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                                        <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                                    </a>
                                                </li>
                                                <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                                    <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                                    </a>
                                                </li>
                                                <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                                    <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                                    </a>
                                                </li>
                                                <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                                    <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                                    </a>
                                                </li>
                                                <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                                    <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">+8</a>
                                                    <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                                        <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                                    </a>
                                                </li>
                                            </ul>
                                            <!-- New element END -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- New elements END -->
                                <div class="post-footer-info" >
                                    <!-- Updated elements -->
                                    <div class="post-foot-block post-reaction">
                                        <span class="post_like_button" id="601451">
                                            <a href="#">
                                                <i class="trav-heart-fill-icon"></i>
                                            </a>
                                        </span>
                                        <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                            <b >0</b> Likes
                                        </span>
                                    </div>
                                    <!-- Updated element END -->
                                    <div class="post-foot-block" >
                                        <a href="#" data-tab="comments616211" >
                                            <i class="trav-comment-icon" ></i>
                                        </a>
                                        <ul class="foot-avatar-list" >
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                            <li>
                                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                                            </li>
                                        </ul>
                                        <span >
                                            <a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a>
                                        </span>
                                    </div>
                                    <div class="post-foot-block ml-auto" >
                                        <span class="post_share_button" id="616211" >
                                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                                <i class="trav-share-icon" ></i>
                                            </a>
                                        </span>
                                        <span id="post_share_count_616211" >
                                            <a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="post-comment-wrapper"></div>
                            </div>
                            <!-- Secondary post type for updates on trip plans END -->
                            <!-- Secondary post type for more-people popover -->
                            <div class="post-block post-top-bordered" >
                            <div class="post-top-info-layer" >
                            <div class="post-top-info-wrap" >
                            <div class="post-top-avatar-wrap" >
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1360/1598296239_image.png" alt="" >
                            </div>
                            <div class="post-top-info-txt" >
                            <div class="post-top-name profile-name" >
                            <a class="post-name-link" href="https://travooo.com/profile/1360" >Hussein A. Hussein</a>
                            Started following <a href="https://travooo.com/profile/219" class="post-name-link" >Richard Alvarado</a>
                            <!-- New elements -->
                            and <a class="post-name-link post-more-people" type="button" data-placement="bottom" data-toggle="popover" data-html="true">5 more people</a>
                            <!-- Post text tag tooltip content -->
                            <div id="popover-content-people" style="display: none;">
                                <div class="popover-people-list">
                                    <div class="popover-people-list-item">
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/219/1594031404_image.png" alt="">
                                        <div class="txt">
                                            <a href="">Gerald Stuber</a>
                                            <span>United States</span>
                                        </div>
                                        <a class="btn btn-light-grey btn-bordered">Follow</a>
                                    </div>
                                    <div class="popover-people-list-item">
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/219/1594031404_image.png" alt="">
                                        <div class="txt">
                                            <a href="">Gerald Stuber</a>
                                            <span>United States</span>
                                        </div>
                                        <a class="btn btn-light-grey btn-bordered">Follow</a>
                                    </div>
                                    <div class="popover-people-list-item">
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/219/1594031404_image.png" alt="">
                                        <div class="txt">
                                            <a href="">Gerald Stuber</a>
                                            <span>United States</span>
                                        </div>
                                        <a class="btn btn-light-grey btn-bordered">Follow</a>
                                    </div>
                                    <div class="popover-people-list-item">
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/219/1594031404_image.png" alt="">
                                        <div class="txt">
                                            <a href="">Gerald Stuber</a>
                                            <span>United States</span>
                                        </div>
                                        <a class="btn btn-light-grey btn-bordered">Follow</a>
                                    </div>
                                    <div class="popover-people-list-item">
                                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/219/1594031404_image.png" alt="">
                                        <div class="txt">
                                            <a href="">Gerald Stuber</a>
                                            <span>United States</span>
                                        </div>
                                        <a class="btn btn-light-grey btn-bordered">Follow</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Post text tag tooltip content END -->
                            <!-- New elements END -->
                            </div>
                            <div class="post-info-date" >
                            4 Hours ago
                            </div>
                            </div>
                            </div>
                            <div class="post-top-info-action" >
                            <div class="dropdown" >
                            <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="trav-angle-down" ></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Userfollow" >
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(350453,this)" >
                            <span class="icon-wrap" >
                            <i class="trav-flag-icon-o" ></i>
                            </span>
                            <div class="drop-txt" >
                            <p ><b >Report</b></p>
                            <p >Help us understand</p>
                            </div>
                            </a>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="post-content-inner" >
                            <div class="post-profile-wrap" >
                            <div class="post-profile-block">
                            <div class="post-prof-image">
                            <img class="prof-cover" src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt="photo">
                            <a href="#" class="btn btn-light-primary btn-follow">
                            <i class="fas fa-plus-square"></i>
                            </a>
                            </div>
                            <div class="post-prof-main">
                            <div class="avatar-wrap">
                            <a href="https://travooo.com/profile/219">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/219/1594031404_image.png" alt="ava">
                            </a>
                            </div>
                            <div class="post-person-info">
                            <div class="prof-name">Richard Alvarado</div>
                            <div class="prof-location">Morocco</div>
                            <ul class="user-photos">
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                            </ul>
                            </div>
                            </div>
                            <div class="drop-bottom-link">
                            <a href="https://travooo.com/profile/219" class="profile-link">View profile</a>
                            </div>
                            </div>
                            <div class="post-profile-block-slider-wrapper">
                            <div class="post-profile-blocks-slider">
                            <div class="post-profile-block" >
                            <div class="post-prof-image" >
                            <img class="prof-cover" src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt="photo" >
                            <a href="#" class="btn btn-light-grey btn-follow" >
                            <i class="trav-user-plus-icon" ></i>
                            </a>
                            </div>
                            <div class="post-prof-main" >
                            <div class="avatar-wrap" >
                            <a href="https://travooo.com/profile/219" >
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/219/1594031404_image.png" alt="ava" >
                            </a>
                            </div>
                            <div class="post-person-info" >
                            <div class="prof-name" >Richard Alvarado</div>
                            <div class="prof-location" >Morocco</div>
                            <ul class="user-photos">
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                            </ul>
                            </div>
                            </div>
                            <div class="drop-bottom-link" >
                            <a href="https://travooo.com/profile/219" class="profile-link" >View profile</a>
                            </div>
                            </div>
                            <div class="post-profile-block" >
                            <div class="post-prof-image" >
                            <img class="prof-cover" src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt="photo" >
                            <a href="#" class="btn btn-light-primary btn-follow" >
                            <i class="trav-user-plus-icon" ></i>
                            </a>
                            </div>
                            <div class="post-prof-main" >
                            <div class="avatar-wrap" >
                            <a href="https://travooo.com/profile/219" >
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/219/1594031404_image.png" alt="ava" >
                            </a>
                            </div>
                            <div class="post-person-info" >
                            <div class="prof-name" >Richard Alvarado</div>
                            <div class="prof-location" >Morocco</div>
                            <ul class="user-photos">
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                            </ul>
                            </div>
                            </div>
                            <div class="drop-bottom-link" >
                            <a href="https://travooo.com/profile/219" class="profile-link" >View profile</a>
                            </div>
                            </div>
                            <div class="post-profile-block" >
                            <div class="post-prof-image" >
                            <img class="prof-cover" src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt="photo" >
                            <a href="#" class="btn btn-light-primary btn-follow" >
                            <i class="trav-user-plus-icon" ></i>
                            </a>
                            </div>
                            <div class="post-prof-main" >
                            <div class="avatar-wrap" >
                            <a href="https://travooo.com/profile/219" >
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/219/1594031404_image.png" alt="ava" >
                            </a>
                            </div>
                            <div class="post-person-info" >
                            <div class="prof-name" >Richard Alvarado</div>
                            <div class="prof-location" >Morocco</div>
                            <ul class="user-photos">
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                                <li><img src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S" alt=""></li>
                            </ul>
                            </div>
                            </div>
                            <div class="drop-bottom-link" >
                            <a href="https://travooo.com/profile/219" class="profile-link" >View profile</a>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <!-- Secondary post type for more-people popover -->
                            <!-- Secondary post type for trending videos -->
                            <div class="post-block post-block-trending-videos" >
                            <div class="post-side-top" >
                            <h3 class="side-ttl" > Videos you might like <span class="count" >20</span></h3>
                            <div class="side-right-control" >
                                <a href="#" class="slide-link video-slide-prev" ><i class="trav-angle-left" ></i></a>
                                <a href="#" class="slide-link video-slide-next" ><i class="trav-angle-right" ></i></a>
                            </div>
                            </div>
                            <div class="post-side-inner" >
                            <ul id="loadReportDestAnimation" class="discover-dest-animation-content d-none" >
                                <li >
                                <div class="dest-animation-card-inner" >
                                <div class="dest-animation-wrap" >
                                <p class="animation dest-animation-title" ></p>
                                <p class="animation dest-animation-text" ></p>
                                </div>
                                </div>
                                </li>
                                <li >
                                <div class="dest-animation-card-inner" >
                                <div class="dest-animation-wrap" >
                                <p class="animation dest-animation-title" ></p>
                                <p class="animation dest-animation-text" ></p>
                                </div>
                                </div>
                                </li>
                                <li >
                                <div class="dest-animation-card-inner" >
                                <div class="dest-animation-wrap" >
                                <p class="animation dest-animation-title" ></p>
                                <p class="animation dest-animation-text" ></p>
                                </div>
                                </div>
                                </li>
                            </ul>
                            <div class="post-slide-wrap" >
                            <ul class="post-slider trending-videos-slider">
                                <li class="trending-videos-slider-item">
                                    <div class="video-box">
                                        <video width="100%" height="auto">
                                            <source src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1599583946_160_1591890126_1024897622-preview.mp4#t=5" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="javascript:;" class="video-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="text">
                                        <div class="ttl">Tokyo Vacation Travel Guide | Expedia</div>
                                        <div class="author">Posted by <a href="">Mike</a></div>
                                        <div class="post-footer-info">
                                            <div class="post-foot-block post-reaction">
                                                <span class="post_like_button" id="601451">
                                                    <a href="#">
                                                        <i class="trav-heart-fill-icon"></i>
                                                    </a>
                                                </span>
                                                <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                                    <b >1</b> Likes
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="trending-videos-slider-item">
                                    <div class="video-box">
                                        <video width="100%" height="auto" controls>
                                            <source src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1599583946_160_1591890126_1024897622-preview.mp4#t=5" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="javascript:;" class="video-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="text">
                                        <div class="ttl">Tokyo Vacation Travel Guide | Expedia</div>
                                        <div class="author">Posted by <a href="">Mike</a></div>
                                        <div class="post-footer-info">
                                            <div class="post-foot-block post-reaction">
                                                <span class="post_like_button" id="601451">
                                                    <a href="#">
                                                        <i class="trav-heart-fill-icon"></i>
                                                    </a>
                                                </span>
                                                <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                                    <b >3</b> Likes
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="trending-videos-slider-item">
                                    <div class="video-box">
                                        <video width="100%" height="auto" controls>
                                            <source src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1599583946_160_1591890126_1024897622-preview.mp4#t=5" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="javascript:;" class="video-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="text">
                                        <div class="ttl">Tokyo Vacation Travel Guide | Expedia</div>
                                        <div class="author">Posted by <a href="">Mike</a></div>
                                        <div class="post-footer-info">
                                            <div class="post-foot-block post-reaction">
                                                <span class="post_like_button" id="601451">
                                                    <a href="#">
                                                        <i class="trav-heart-fill-icon"></i>
                                                    </a>
                                                </span>
                                                <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                                    <b >3</b> Likes
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="trending-videos-slider-item">
                                    <div class="video-box">
                                        <video width="100%" height="auto" controls>
                                            <source src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1599583946_160_1591890126_1024897622-preview.mp4#t=5" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="javascript:;" class="video-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="text">
                                        <div class="ttl">Tokyo Vacation Travel Guide | Expedia</div>
                                        <div class="author">Posted by <a href="">Mike</a></div>
                                        <div class="post-footer-info">
                                            <div class="post-foot-block post-reaction">
                                                <span class="post_like_button" id="601451">
                                                    <a href="#">
                                                        <i class="trav-heart-fill-icon"></i>
                                                    </a>
                                                </span>
                                                <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                                    <b >4</b> Likes
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="trending-videos-slider-item">
                                    <div class="video-box">
                                        <video width="100%" height="auto" controls>
                                            <source src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1599583946_160_1591890126_1024897622-preview.mp4#t=5" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="javascript:;" class="video-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="text">
                                        <div class="ttl">Tokyo Vacation Travel Guide | Expedia</div>
                                        <div class="author">Posted by <a href="">Mike</a></div>
                                        <div class="post-footer-info">
                                            <div class="post-foot-block post-reaction">
                                                <span class="post_like_button" id="601451">
                                                    <a href="#">
                                                        <i class="trav-heart-fill-icon"></i>
                                                    </a>
                                                </span>
                                                <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                                    <b >4</b> Likes
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="trending-videos-slider-item">
                                    <div class="video-box">
                                        <video width="100%" height="auto" controls>
                                            <source src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1599583946_160_1591890126_1024897622-preview.mp4#t=5" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="javascript:;" class="video-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="text">
                                        <div class="ttl">Tokyo Vacation Travel Guide | Expedia</div>
                                        <div class="author">Posted by <a href="">Mike</a></div>
                                        <div class="post-footer-info">
                                            <div class="post-foot-block post-reaction">
                                                <span class="post_like_button" id="601451">
                                                    <a href="#">
                                                        <i class="trav-heart-fill-icon"></i>
                                                    </a>
                                                </span>
                                                <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                                    <b >4</b> Likes
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </div>
                            </div>
                            </div>
                            <!-- Secondary post type for trending videos -->
                            <!-- Secondary post type - for holidays notification -->
                            <!-- Updated element -->
                            <div class="post-block post-block-holiday" >
                            <!-- Updated element END -->
                            <div class="post-top-info-layer" >
                            <div class="post-top-info-wrap" >
                            <div class="post-top-avatar-wrap" >
                            <!-- Updated element -->
                                <span class="avatar-icon primary">
                                    <i class="trav-calendar-icon" ></i>
                                </span>
                            <!-- Updated element END-->
                            </div>
                            <div class="post-top-info-txt" >
                            <div class="post-top-name" >
                            <a class="post-name-link" href="#" >Independence day</a>
                            </div>
                            <div class="post-info" >
                                <!-- Updated element -->
                                <span class="badge badge-light">National Holiday</span> in <a class="link-place post-holiday-popover" type="button" data-placement="bottom" data-toggle="popover" data-html="true">United States of America</a>
                                <!-- Updated element END -->
                                <!-- Post holiday popover content -->
                                <div id="popover-content-holiday" style="display: none;">
                                    <div class="holiday-hero">
                                        <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="" class="cover">
                                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/us.png" alt="" class="flag">
                                    </div>
                                    <div class="holiday-desc">
                                        <div class="holiday-desc-country">United States of America</div>
                                        <div class="holiday-desc-location">Country in North America</div>
                                        <a href="" class="btn btn-light-grey btn-bordered">Follow</a>
                                        <div class="holiday-desc-followers">28K Followers</div>
                                        <a href="" class="btn btn-link holiday-desc-page-link">View Page</a>
                                    </div>
                                </div>
                                <!-- Post holiday popover content END -->
                            </div>
                            </div>
                            </div>
                            <!-- Removed post-top-info-action -->
                            </div>
                            <div class="post-image-container post-follow-container" >
                            <ul class="post-image-list" >
                            <li >
                            <img class="lazy" alt=""  src="https://s3.amazonaws.com/travooo-images2/th700/places/ChIJVYmnYM9aDDUR4jnTKUd1pwk/13faa83ee5565ad8017c41d68677ea1e829d4f3b.jpg" style="">
                            </li>
                            </ul>
                            <div class="post-follow-block" >
                            <div class="follow-txt-wrap" >
                                <!-- New elements -->
                                <div class="holiday-follow-text">
                                    <div class="date">
                                        <div class="month">Jul</div>
                                        <div class="day">4</div>
                                    </div>
                                    <div class="desc">
                                        Donec odio ipsum, aliquet id ex eget, euismod consectetur felis. In pharetra turpis a nulla...
                                    </div>
                                </div>
                                <!-- New elements END -->
                            </div>
                            <div class="follow-btn-wrap check-follow-place" data-id="4606823" >
                                <button class="btn btn-light-grey btn-bordered btn-grey-txt place-follow-btn" data-id="4263691" data-type="follow" >Follow </button>
                            </div>
                            </div>
                            </div>
                            </div>
                            <!-- Secondary post type - for holidays notification END -->
                            <!-- Secondary post type for weather forecast -->
                            <div class="post-block post-block-notification" >
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        <i class="fas fa-cloud-sun" rel="tooltip" data-toggle="tooltip" data-animation="false" title=""  data-original-title="The total number of views your posts have garnered"></i>
                                        Weather forcast about <a href="" class="link-place primary"><i class="trav-link-out-icon"></i> your visit</a> to <a href="" class="link-place primary">Disnayland</a> on Monday 28 September 2020
                                    </div>
                                </div>
                                <div class="post-weather-inner">
                                    <div class="top-line">
                                        <i class="far fa-lightbulb"></i> It will be partly cloudy in <a href="">Disneyland</a> and there is a chance of rain
                                    </div>
                                    <div class="forecast-box">
                                        <div class="forecast-map">
                                            <img src="https://travooo.com/assets2/image/popup-map.jpg" alt="">
                                            <div class="forecast-map-marker map-popover" type="button" data-placement="top" data-toggle="popover" data-html="true">
                                                <i class="fas fa-map-marker-alt"></i>
                                            </div>
                                            <div id="popover-content-map" style="display: none">
                                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                                <div class="forecast-map-popover-text">
                                                    <h3>Disnayland</h3>
                                                    <p>Park in Anaheim, California, United States</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="forecast-table">
                                            <div class="forecast-table-day visit-day">
                                                <div>
                                                    <div class="temp">26 <sup>&deg;</sup></div>
                                                    <div class="day">Monday 28 <span>Oct</span></div>
                                                </div>
                                                <i class="fas fa-cloud-sun"></i>
                                            </div>
                                            <div class="forecast-table-day">
                                                <div class="day">Tue</div>
                                                <i class="fas fa-sun"></i>
                                                <div class="temp">23 <sup>&deg;</sup></div>
                                            </div>
                                            <div class="forecast-table-day">
                                                <div class="day">Wed</div>
                                                <i class="fas fa-cloud"></i>
                                                <div class="temp">20 <sup>&deg;</sup></div>
                                            </div>
                                            <div class="forecast-table-day">
                                                <div class="day">Thu</div>
                                                <i class="fas fa-cloud-showers-heavy"></i>
                                                <div class="temp">17 <sup>&deg;</sup></div>
                                            </div>
                                            <div class="forecast-table-day">
                                                <div class="day">Fri</div>
                                                <i class="fas fa-cloud-rain"></i>
                                                <div class="temp">19 <sup>&deg;</sup></div>
                                            </div>
                                            <div class="forecast-table-day">
                                                <div class="day">Sat</div>
                                                <i class="fas fa-cloud"></i>
                                                <div class="temp">20 <sup>&deg;</sup></div>
                                            </div>
                                            <div class="forecast-table-day">
                                                <div class="day">sun</div>
                                                <i class="fas fa-cloud-rain"></i>
                                                <div class="temp">19 <sup>&deg;</sup></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Secondary post type for weather forecast END -->
                            <!-- Secondary post type for weather forecast -->
                            <div class="post-block post-block-notification" >
                                <div class="post-top-info-layer">
                                    <div class="post-info-line">
                                        <i class="fas fa-cloud-sun" rel="tooltip" data-toggle="tooltip" data-animation="false" title=""  data-original-title="The total number of views your posts have garnered"></i>
                                        Weather forcast about your upcoming trip <a href="" class="link-place primary"><i class="trav-link-out-icon"></i> New York City, where to go?</a>
                                    </div>
                                </div>
                                <div class="post-weather-inner">
                                    <div class="forecast-box">
                                        <div class="forecast-map">
                                            <img src="https://travooo.com/assets2/image/popup-map.jpg" alt="" style="max-height: 400px;">
                                            <div class="map-days-list">
                                                <span class="map-days-list-item">Day</span>
                                                <a href="" class="map-days-list-item active">1 <i class="fas fa-exclamation-circle"></i></a>
                                                <a href="" class="map-days-list-item">2</a>
                                                <a href="" class="map-days-list-item">... <i class="fas fa-exclamation-circle"></i></a>
                                            </div>
                                            <div class="map-day-forecast">
                                                <strong>8 October</strong> <i class="fas fa-cloud-showers-heavy"></i> Max <strong>5° C</strong> <span>&nbsp;&middot;&nbsp;</span> Min <strong>2° C</strong>
                                            </div>
                                            <div class="forecast-map-marker map-popover" type="button" data-placement="top" data-toggle="popover" data-html="true">
                                                <i class="fas fa-map-marker-alt"></i>
                                            </div>
                                            <div class="forecast-map-marker transfer" style="top: 20%; left: 80%;">
                                                <i class="fas fa-plane"></i>
                                            </div>
                                        </div>
                                        <div class="forecast-place">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="" class="forecast-place-img">
                                            <div class="forecast-place-text">
                                                <h3 class="ttl">New York City</h3>
                                                <div class="weather">
                                                    <strong>8 October</strong> <i class="fas fa-angle-right"></i> <strong>10 October</strong>
                                                    <i class="fas fa-thermometer-half"></i> Max <strong>11° C</strong> <span>&nbsp;&middot;&nbsp;</span> Min <strong>2° C</strong>
                                                </div>
                                                <div class="note">
                                                    <i class="fas fa-exclamation-circle"></i>
                                                    Stormy and cold weather, Not suitable for the trip.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Secondary post type for weather forecast END -->
                        </div>
                    </div>
                    <!-- Sidebar -->
                    <div class="sidebar-layer" style="padding-top:10px">
                        <aside class="sidebar">
                            <!-- Photo Slider -->
                            <div class="aside-photo-slider-wrapper">
                                <div class="aside-photo-slider">
                                    <div class="aside-photo-slider-item">
                                        <a class="openInnerSlider" data-toggle="modal" data-target="#asideSliderPopup">
                                            <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg" alt="">
                                        </a>
                                        <div class="aside-photo-slider-item-top">
                                            <div class="info">
                                                <div class="ttl">Disneyland</div>
                                                <div class="reactions"><i class="trav-heart-fill-icon"></i> <span>185 Likes</span></div>
                                            </div>
                                            <div class="count">6</div>
                                        </div>
                                        <div class="aside-photo-slider-item-bottom">
                                            <div class="author">Photo by <strong>Patrick Smith</strong></div>
                                            <div class="comments-box">
                                                <div class="users-list">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="">
                                                </div>
                                                <div class="text"><strong>64K</strong> talking about this</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="aside-photo-slider-item">
                                        <a class="openInnerSlider" data-toggle="modal" data-target="#asideSliderPopup">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                        </a>
                                        <div class="aside-photo-slider-item-top">
                                            <div class="info">
                                                <div class="ttl">Disneyland</div>
                                                <div class="reactions"><i class="trav-heart-fill-icon"></i> <span>185 Likes</span></div>
                                            </div>
                                            <div class="count">6</div>
                                        </div>
                                        <div class="aside-photo-slider-item-bottom">
                                            <div class="author">Photo by <strong>Patrick Smith</strong></div>
                                            <div class="comments-box">
                                                <div class="users-list">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="">
                                                </div>
                                                <div class="text"><strong>64K</strong> talking about this</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="aside-photo-slider-item">
                                        <a class="openInnerSlider" data-toggle="modal" data-target="#asideSliderPopup">
                                            <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                                        </a>
                                        <div class="aside-photo-slider-item-top">
                                            <div class="info">
                                                <div class="ttl">Disneyland</div>
                                                <div class="reactions"><i class="trav-heart-fill-icon"></i> <span>185 Likes</span></div>
                                            </div>
                                            <div class="count">6</div>
                                        </div>
                                        <div class="aside-photo-slider-item-bottom">
                                            <div class="author">Photo by <strong>Patrick Smith</strong></div>
                                            <div class="comments-box">
                                                <div class="users-list">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="">
                                                </div>
                                                <div class="text"><strong>64K</strong> talking about this</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Photo Slider END -->
                            <!-- Suggested Travelers -->
                            <div class="post-block post-side-block post-side-suggested-travelers">
                                <div class="post-side-top">
                                    <h3 class="side-ttl">Suggested Travelers</h3>
                                </div>
                                <div class="post-side-inner">
                                    <div class="side-tab-inner mCustomScrollbar">
                                        <div class="side-pane-row">
                                            <div class="side-pane-avatar-wrap">
                                                <img class="lazy mCS_img_loaded" alt="" src="https://s3.amazonaws.com/travooo-images2/users/profile/1/1600767352_image.png" style="">
                                            </div>
                                            <div class="side-pane-txt">
                                                <div class="side-pane-post-ttl">
                                                    <a class="in-side-link" href="https://travooo.com/profile/1">Cheryl Cornett</a>
                                                </div>
                                            </div>
                                            <button class="btn btn-light-grey">Follow</button>
                                        </div>
                                        
                                        <div class="side-pane-row">
                                            <div class="side-pane-avatar-wrap">
                                                <img class="lazy mCS_img_loaded" alt="" src="https://s3.amazonaws.com/travooo-images2/users/profile/1/1600767352_image.png" style="">
                                            </div>
                                            <div class="side-pane-txt">
                                                <div class="side-pane-post-ttl">
                                                    <a class="in-side-link" href="https://travooo.com/profile/1">Cheryl Cornett</a>
                                                </div>
                                            </div>
                                            <button class="btn btn-light-grey">Follow</button>
                                        </div>

                                        <div class="side-pane-row">
                                            <div class="side-pane-avatar-wrap">
                                                <img class="lazy mCS_img_loaded" alt="" src="https://s3.amazonaws.com/travooo-images2/users/profile/1/1600767352_image.png" style="">
                                            </div>
                                            <div class="side-pane-txt">
                                                <div class="side-pane-post-ttl">
                                                    <a class="in-side-link" href="https://travooo.com/profile/1">Cheryl Cornett</a>
                                                </div>
                                            </div>
                                            <button class="btn btn-light-grey">Follow</button>
                                        </div>

                                        <div class="side-pane-row">
                                            <div class="side-pane-avatar-wrap">
                                                <img class="lazy mCS_img_loaded" alt="" src="https://s3.amazonaws.com/travooo-images2/users/profile/1/1600767352_image.png" style="">
                                            </div>
                                            <div class="side-pane-txt">
                                                <div class="side-pane-post-ttl">
                                                    <a class="in-side-link" href="https://travooo.com/profile/1">Cheryl Cornett</a>
                                                </div>
                                            </div>
                                            <button class="btn btn-light-grey">Follow</button>
                                        </div>

                                        <div class="side-pane-row">
                                            <div class="side-pane-avatar-wrap">
                                                <img class="lazy mCS_img_loaded" alt="" src="https://s3.amazonaws.com/travooo-images2/users/profile/1/1600767352_image.png" style="">
                                            </div>
                                            <div class="side-pane-txt">
                                                <div class="side-pane-post-ttl">
                                                    <a class="in-side-link" href="https://travooo.com/profile/1">Cheryl Cornett</a>
                                                </div>
                                            </div>
                                            <button class="btn btn-light-grey">Follow</button>
                                        </div>

                                        <div class="side-pane-row">
                                            <div class="side-pane-avatar-wrap">
                                                <img class="lazy mCS_img_loaded" alt="" src="https://s3.amazonaws.com/travooo-images2/users/profile/1/1600767352_image.png" style="">
                                            </div>
                                            <div class="side-pane-txt">
                                                <div class="side-pane-post-ttl">
                                                    <a class="in-side-link" href="https://travooo.com/profile/1">Cheryl Cornett</a>
                                                </div>
                                            </div>
                                            <button class="btn btn-light-grey">Follow</button>
                                        </div>

                                        <div class="side-pane-row">
                                            <div class="side-pane-avatar-wrap">
                                                <img class="lazy mCS_img_loaded" alt="" src="https://s3.amazonaws.com/travooo-images2/users/profile/1/1600767352_image.png" style="">
                                            </div>
                                            <div class="side-pane-txt">
                                                <div class="side-pane-post-ttl">
                                                    <a class="in-side-link" href="https://travooo.com/profile/1">Cheryl Cornett</a>
                                                </div>
                                            </div>
                                            <button class="btn btn-light-grey">Follow</button>
                                        </div>

                                        <div class="side-pane-row">
                                            <div class="side-pane-avatar-wrap">
                                                <img class="lazy mCS_img_loaded" alt="" src="https://s3.amazonaws.com/travooo-images2/users/profile/1/1600767352_image.png" style="">
                                            </div>
                                            <div class="side-pane-txt">
                                                <div class="side-pane-post-ttl">
                                                    <a class="in-side-link" href="https://travooo.com/profile/1">Cheryl Cornett</a>
                                                </div>
                                            </div>
                                            <button class="btn btn-light-grey">Follow</button>
                                        </div>

                                        <div class="side-pane-row">
                                            <div class="side-pane-avatar-wrap">
                                                <img class="lazy mCS_img_loaded" alt="" src="https://s3.amazonaws.com/travooo-images2/users/profile/1/1600767352_image.png" style="">
                                            </div>
                                            <div class="side-pane-txt">
                                                <div class="side-pane-post-ttl">
                                                    <a class="in-side-link" href="https://travooo.com/profile/1">Cheryl Cornett</a>
                                                </div>
                                            </div>
                                            <button class="btn btn-light-grey">Follow</button>
                                        </div>

                                        <div class="side-pane-row">
                                            <div class="side-pane-avatar-wrap">
                                                <img class="lazy mCS_img_loaded" alt="" src="https://s3.amazonaws.com/travooo-images2/users/profile/1/1600767352_image.png" style="">
                                            </div>
                                            <div class="side-pane-txt">
                                                <div class="side-pane-post-ttl">
                                                    <a class="in-side-link" href="https://travooo.com/profile/1">Cheryl Cornett</a>
                                                </div>
                                            </div>
                                            <button class="btn btn-light-grey">Follow</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- Suggested Travelers END -->
                            <!-- Discover -->
                            <div class="post-block post-side-block post-side-discover" style="display: none;">
                                <div class="post-side-top">
                                    <h3 class="side-ttl">Discover</h3>
                                </div>
                                <div class="post-side-inner">
                                    <div class="side-tab-inner mCustomScrollbar">      
                                        <a href="#" class="discover-place-row">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                            <span class="text">
                                                <strong>Disnayland Park</strong>
                                                <i>Theme park, United States</i>
                                            </span>
                                            <i class="fas fa-angle-right"></i>
                                        </a>

                                        <a href="#" class="discover-place-row">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                            <span class="text">
                                                <strong>Disnayland Park</strong>
                                                <i>Theme park, United States</i>
                                            </span>
                                            <i class="fas fa-angle-right"></i>
                                        </a>

                                        <a href="#" class="discover-place-row">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                            <span class="text">
                                                <strong>Disnayland Park</strong>
                                                <i>Theme park, United States</i>
                                            </span>
                                            <i class="fas fa-angle-right"></i>
                                        </a>

                                        <a href="#" class="discover-place-row">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                            <span class="text">
                                                <strong>Disnayland Park</strong>
                                                <i>Theme park, United States</i>
                                            </span>
                                            <i class="fas fa-angle-right"></i>
                                        </a>

                                        <a href="#" class="discover-place-row">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                            <span class="text">
                                                <strong>Disnayland Park</strong>
                                                <i>Theme park, United States</i>
                                            </span>
                                            <i class="fas fa-angle-right"></i>
                                        </a>

                                        <a href="#" class="discover-place-row">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                            <span class="text">
                                                <strong>Disnayland Park</strong>
                                                <i>Theme park, United States</i>
                                            </span>
                                            <i class="fas fa-angle-right"></i>
                                        </a>

                                        <a href="#" class="discover-place-row">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                            <span class="text">
                                                <strong>Disnayland Park</strong>
                                                <i>Theme park, United States</i>
                                            </span>
                                            <i class="fas fa-angle-right"></i>
                                        </a>

                                        <a href="#" class="discover-place-row">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                            <span class="text">
                                                <strong>Disnayland Park</strong>
                                                <i>Theme park, United States</i>
                                            </span>
                                            <i class="fas fa-angle-right"></i>
                                        </a>

                                        <a href="#" class="discover-place-row">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                            <span class="text">
                                                <strong>Disnayland Park</strong>
                                                <i>Theme park, United States</i>
                                            </span>
                                            <i class="fas fa-angle-right"></i>
                                        </a>

                                        <a href="#" class="discover-place-row">
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                                            <span class="text">
                                                <strong>Disnayland Park</strong>
                                                <i>Theme park, United States</i>
                                            </span>
                                            <i class="fas fa-angle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Discover END -->
                            <!-- Top places UPDATED -->
                            <!-- Update -->
                            <div class="post-block post-side-block post-side-top-places" dir="auto">
                            <!-- Update END -->
                            <div class="post-side-top" dir="auto">
                            <h3 class="side-ttl" dir="auto">Top places</h3>
                            <div class="side-right-control" dir="auto">
                            <a href="#" class="see-more-link" dir="auto">See More</a>
                            </div>
                            </div>

                            <!-- Update -->
                            <div class="post-side-inner" id="placeAsideBlock" dir="auto">
                            <div class="side-tab-inner mCustomScrollbar">
                            <!-- Update END -->
                            <div class="side-place-block" dir="auto">
                            <div class="side-place-top" dir="auto">
                            <div class="side-place-avatar-wrap" dir="auto">
                            <img class="lazy" alt="" style="width: 46px; height: 46px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJSS5pkozMHkcRwi0fMeV66cI/23acede389aaa5e99552c4e427794c8da382563c.jpg">
                            </div>
                            <div class="side-place-txt" dir="auto">
                            <a class="side-place-name" href="https://travooo.com/place/691945" dir="auto">Palace of Culture and Science</a>
                            <div class="side-place-description" dir="auto">
                            <b dir="auto">Point of interest</b> in <a href="https://travooo.com/city/149" dir="auto">Warsaw</a>,
                            <a href="https://travooo.com/country/170" dir="auto">Poland</a>
                            </div>
                            </div>
                            </div>
                            <ul class="side-place-image-list" dir="auto">
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJSS5pkozMHkcRwi0fMeV66cI/4b6ae385d8b721849188e23575ffb63f3652b826.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJSS5pkozMHkcRwi0fMeV66cI/9d4e17a5498fbb551aaec8bfab3da42752c9d64f.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJSS5pkozMHkcRwi0fMeV66cI/3ea53801e93d12f04cfe4675c8a01fee6af65b7f.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJSS5pkozMHkcRwi0fMeV66cI/0eef371494ea949610c396e60e395c619aa2656a.jpg"></li>
                            </ul>
                            <div class="side-place-bottom" dir="auto">
                            <div class="side-follow-info" dir="auto">
                            <b dir="auto">1</b> Following this place </div>
                            <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following('follow', 691945, this)" dir="auto">Follow </button>
                            </div>
                            </div>
                            <div class="side-place-block" dir="auto">
                            <div class="side-place-top" dir="auto">
                            <div class="side-place-avatar-wrap" dir="auto">
                            <img class="lazy" alt="" style="width: 46px; height: 46px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJX1rTlu8JxkcRGsV8-a4oKMI/f00099f8949b0f465299ee28a079f784bc27f7c4.jpg">
                            </div>
                            <div class="side-place-txt" dir="auto">
                            <a class="side-place-name" href="https://travooo.com/place/14671" dir="auto">Van Gogh Museum</a>
                            <div class="side-place-description" dir="auto">
                            <b dir="auto">Museum</b> in <a href="https://travooo.com/city/201" dir="auto">Amsterdam</a>,
                            <a href="https://travooo.com/country/148" dir="auto">Netherlands</a>
                            </div>
                            </div>
                            </div>
                            <ul class="side-place-image-list" dir="auto">
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJX1rTlu8JxkcRGsV8-a4oKMI/e13e37be1b339c1e9000c6938dcb1cab797d3d8f.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJX1rTlu8JxkcRGsV8-a4oKMI/9efe7a896aabd66dd54f92c413187572f472dcfc.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJX1rTlu8JxkcRGsV8-a4oKMI/d4a437ef0f9fadb4d8997f03d1133eaf31c3d142.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJX1rTlu8JxkcRGsV8-a4oKMI/e1a13787df9d2773a7800ff43fe79eac46c46851.jpg"></li>
                            </ul>
                            <div class="side-place-bottom" dir="auto">
                            <div class="side-follow-info" dir="auto">
                            <b dir="auto">5</b> Following this place </div>
                            <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following('follow', 14671, this)" dir="auto">Follow </button>
                            </div>
                            </div>
                            <div class="side-place-block" dir="auto">
                            <div class="side-place-top" dir="auto">
                            <div class="side-place-avatar-wrap" dir="auto">
                            <img class="lazy" alt="" style="width: 46px; height: 46px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJxf6409OxfkcR7UmjymSPxuM/40f2e7d9c2d799dadd83876725d1b258df2de415.jpg">
                            </div>
                            <div class="side-place-txt" dir="auto">
                            <a class="side-place-name" href="https://travooo.com/place/87353" dir="auto">Peggy Guggenheim Collection</a>
                            <div class="side-place-description" dir="auto">
                            <b dir="auto">Museum</b> in <a href="https://travooo.com/city/181" dir="auto">Venice</a>,
                            <a href="https://travooo.com/country/104" dir="auto">Italy</a>
                            </div>
                            </div>
                            </div>
                            <ul class="side-place-image-list" dir="auto">
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJxf6409OxfkcR7UmjymSPxuM/6d7c6978f901351739a32d448b6d0c76fdaa3bb2.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJxf6409OxfkcR7UmjymSPxuM/a8aaaa1b4cc4954589feb84018b765db5f29b3d1.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJxf6409OxfkcR7UmjymSPxuM/b5c5814ca01d8cde8b54305ca0bde98782b0bc0f.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJxf6409OxfkcR7UmjymSPxuM/d9bcc10ef350c094a8198c3baa4c1efb4e9f7590.jpg"></li>
                            </ul>
                            <div class="side-place-bottom" dir="auto">
                            <div class="side-follow-info" dir="auto">
                            <b dir="auto">5</b> Following this place </div>
                            <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following('follow', 87353, this)" dir="auto">Follow </button>
                            </div>
                            </div>
                            <div class="side-place-block" dir="auto">
                            <div class="side-place-top" dir="auto">
                            <div class="side-place-avatar-wrap" dir="auto">
                            <img class="lazy" alt="" style="width: 46px; height: 46px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJLxyMQZZw44kRxTZwZ5PGulc/647d00459e3c2d500a572e31f14a4854903d30e6.jpg">
                            </div>
                            <div class="side-place-txt" dir="auto">
                            <a class="side-place-name" href="https://travooo.com/place/2758757" dir="auto">Museum of Science</a>
                            <div class="side-place-description" dir="auto">
                            <b dir="auto">Museum</b> in <a href="https://travooo.com/city/572" dir="auto">Boston</a>,
                            <a href="https://travooo.com/country/227" dir="auto">United States</a>
                            </div>
                            </div>
                            </div>
                            <ul class="side-place-image-list" dir="auto">
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJLxyMQZZw44kRxTZwZ5PGulc/3b7d32f2a15f81e5dd5c1a7cf7909dcfc7d7d894.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJLxyMQZZw44kRxTZwZ5PGulc/5718b05996a60e95d605c123e13e522dde7035ad.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJLxyMQZZw44kRxTZwZ5PGulc/1b3346378c1e2e2d0171c0a70e6d395caef3f62d.jpg"></li>
                            <li dir="auto">
                            <img class="lazy" alt="photo" style="width: 79px; height: 75px;" dir="auto" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJLxyMQZZw44kRxTZwZ5PGulc/5e1d47ad3bab7cd8ab43c565b517f03104750154.jpg"></li>
                            </ul>
                            <div class="side-place-bottom" dir="auto">
                            <div class="side-follow-info" dir="auto">
                            <b dir="auto">2</b> Following this place </div>
                            <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following('follow', 2758757, this)" dir="auto">Follow </button>
                            </div>
                            </div>
                            </div>
                            <!-- Update -->
                            </div>
                            <!-- Update END -->
                            </div>
                            <!-- Top places UPDATED END -->
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('site.place2.partials.modal_like')

<footer class="page-footer page__footer"></footer>

<script src="{{ asset('assets/js/lightslider.min.js') }}"></script>
<script src="{{ asset('assets3/js/tether.min.js') }}"></script>
<script src="{{ asset('assets2/js/popper.min.js') }}"></script>
<script src="{{ asset('assets2/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets3/js/bootstrap-datepicker-extended.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets2/js/slick.min.js') }}"></script>
<script src="{{ asset('assets3/js/main.js?0.3.2') }}"></script>
<script src="{{ asset('assets2/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/js/lightgallery-all.min.js"></script>
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
<script src="{{ asset('assets3/js/jquery.barrating.min.js') }}"></script>
<script src="{{ asset('assets3/js/star.js') }}"></script>

<script src="{{ asset('assets2/js/emoji/jquery.emojiFace.js?v=0.1') }}"></script>
<script src="{{ asset('assets2/js/skyloader.js') }}"></script>
<!-- <script src="{{ asset('assets3/js/jquery.sliderPro.min.js') }}"></script> -->
<script src="{{ asset('assets2/js/jquery-confirm/jquery-confirm.min.js') }}"></script>
<script src="{{ asset('assets2/js/lightbox2/src/js/lightbox.js') }}"></script>
<script data-cfasync="false" src="{{asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('assets2/js/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('assets2/js/posts-script.js?v=0.3') }}"></script>
@include('site.home.partials._spam_dialog')

<!-- Message to user popup -->
<div class="modal msg-to-user-popup" id="messageToUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-650" role="document" >
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <div class="top-title-layer" >
                    <h3 class="title" >
                        Message
                        <img class="msg-to" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                        <span class="txt">Bora Young</span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-create-input">
                    <textarea name="" id="" cols="30" rows="10" placeholder="Type a message..."></textarea>
                    <div class="medias medias-slider" >
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed loading">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-create-controls">
                    <div class="emoji-block">
                        <button class="add-msg__emoji" emoji-target="add_msg_text" type="button">🙂</button> 
                    </div>
                    <div class="post-alloptions" >
                        <ul class="create-link-list" >
                            <li class="post-options" >
                                <input type="file" name="file[]" id="file" style="display:none" multiple="" >
                                <i class="trav-camera click-target" data-target="file" ></i>
                            </li>
                        </ul>
                    </div>
                    <div class="ml-auto post-buttons">
                        <button type="button" class="btn btn-light-grey btn-bordered mr-3" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-light-primary btn-bordered" >Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Message to user popup END -->
<!-- Users who like modal -->
<div class="modal white-style users-who-like-modal" data-backdrop="false" id="usersWhoLike" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                            Poeple who liked this post <span>16</span>
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar" >
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Friend</span>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Follows you</span>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-grey btn-bordered">Unfollow</button>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Users who like modal END -->
<!-- Share post modal -->
<div class="modal white-style share-post-modal" data-backdrop="false" id="sharePostModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-670" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                        Share this post
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="shared-post-comment">
                    <div class="shared-post-comment-details">
                        <div class="shared-post-comment-author">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            Ivan Turlakov
                        </div>
                        <!-- privacy settings -->
                        <div class="dropdown">
                            <button class="btn btn--sm btn--outline dropdown-toggle" data-toggle="dropdown">
                                <i class="trav-globe-icon"></i>
                                PUBLIC
                            </button>
                            <div class="dropdown-menu dropdown-menu-right permissoin_show hided">
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-globe-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Public</b></p>
                                        <p>Anyone can see this post</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-users-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Friends Only</b></p>
                                        <p>Only you and your friends can see this post</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-lock-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Only Me</b></p>
                                        <p>Only you can see this post</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- privacy settings -->
                    </div>
                    <div class="shered-post-comment-text">
                        <textarea name="" id="" cols="30" rows="10" placeholder="Write something..."></textarea>
                    </div>
                </div>
                <div class="shared-post-wrap">
                    <!-- Primary post block - photo*3 and text -->
                    <div class="post-block" >
                        <div class="post-top-info-layer" >
                        <div class="post-top-info-wrap" >
                        <div class="post-top-avatar-wrap" >
                        <a class="post-name-link" href="https://travooo.com/profile/831" >
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                        </a>
                        </div>
                        <div class="post-top-info-txt" >
                        <div class="post-top-name" >
                        <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                        </div>
                        <!-- Updated element -->
                        <div class="post-info">
                            Checked-in
                            <a href="https://travooo.com/place/3485165" class="link-place">The House of Dancing Water</a>
                            on Sep 1, 2020
                            <i class="trav-globe-icon" ></i>
                        </div>
                        <!-- Updated element END -->
                        </div>
                        </div>
                        </div>
                        <!-- New elements -->
                        <div class="post-txt-wrap">
                            <div>
                                <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                            </div>
                        </div>
                        <div class="check-in-point">
                            <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                        </div>
                        <!-- New elements END -->
                        <div class="post-image-container" >
                        <!-- New elements -->
                        <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;" >
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                        </ul>
                        <!-- New element END -->
                        </div>
                        <div class="post-footer-info" >
                        <!-- Updated elements -->
                        <div class="post-foot-block post-reaction">
                            <span class="post_like_button" id="601451">
                                <a href="#">
                                    <i class="trav-heart-fill-icon"></i>
                                </a>
                            </span>
                            <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                <b >0</b> Likes
                            </span>
                        </div>
                        <!-- Updated element END -->
                        <div class="post-foot-block" >
                        <a href="#" data-tab="comments616211" >
                        <i class="trav-comment-icon" ></i>
                        </a>
                        <ul class="foot-avatar-list" >
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                        </ul>
                        <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                        </div>
                        <div class="post-foot-block ml-auto" >
                        <span class="post_share_button" id="616211" >
                        <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                        <i class="trav-share-icon" ></i>
                        </a>
                        </span>
                        <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                        </div>
                        </div>
                        <div class="post-comment-wrapper" id="following616211" >
                        </div>
                        <!-- Removed comments block -->
                    </div>
                    <!-- Primary post block - photo*3 and text END -->
                </div>
                <div class="shared-post-actions">
                    <button type="submit" class="btn btn-link circle"  rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share on <b>Facebook</b>">
                       <i class="fab fa-facebook-f"></i>
                    </button>
                    <span class="divider"></span>
                    <button type="submit" class="btn btn-link circle"  rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share on <b>Twitter</b>">
                        <i class="trav-twitter-icon"></i>
                    </button>
                    <span class="divider"></span>
                    <button type="submit" class="btn btn-link circle"  rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share on <b>Pintrest</b>">
                        <i class="trav-pintrest-icon"></i>
                    </button>
                    <span class="divider"></span>
                    <button type="submit" class="btn btn-link circle"  rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share on <b>Tumblr</b>">
                        <i class="trav-tumblr-icon"></i>
                    </button>
                    <span class="divider"></span>
                    <button type="submit" class="btn btn-link circle"  rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share the <b>link</b>">
                        <i class="trav-link"></i>
                    </button>
                    <span class="divider"></span>
                    <button type="submit" class="btn btn-link" rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share on <b>Travooo</b>">SHARE</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Share post modal END -->
<!-- Users who shared modal -->
<div class="modal white-style users-who-like-modal" data-backdrop="false" id="usersWhoShare" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                            Poeple who shared this post <span>16</span>
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar" >
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Friend</span>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Follows you</span>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Users who shared modal END -->
<!-- Send to a friend modal -->
<div class="modal white-style share-post-modal" data-backdrop="false" id="sendToFriendModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-670" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                            Send to a Friend
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="shared-post-comment">
                    <div class="shared-post-comment-details">
                        <div class="shared-post-comment-author">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            Ivan Turlakov
                        </div>
                        <!-- privacy settings -->
                        <div class="dropdown">
                            <button class="btn btn--sm btn--outline dropdown-toggle" data-toggle="dropdown">
                                <i class="trav-globe-icon"></i>
                                PUBLIC
                            </button>
                            <div class="dropdown-menu dropdown-menu-right permissoin_show hided">
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-globe-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Public</b></p>
                                        <p>Anyone can see this post</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-users-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Friends Only</b></p>
                                        <p>Only you and your friends can see this post</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-lock-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Only Me</b></p>
                                        <p>Only you can see this post</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- privacy settings -->
                    </div>
                    <div class="shered-post-comment-text">
                        <textarea name="" id="" cols="30" rows="10" placeholder="Write something..."></textarea>
                    </div>
                </div>
                <div class="shared-post-wrap">
                    <!-- Primary post block - photo*3 and text -->
                    <div class="post-block" >
                        <div class="post-top-info-layer" >
                        <div class="post-top-info-wrap" >
                        <div class="post-top-avatar-wrap" >
                        <a class="post-name-link" href="https://travooo.com/profile/831" >
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                        </a>
                        </div>
                        <div class="post-top-info-txt" >
                        <div class="post-top-name" >
                        <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                        </div>
                        <!-- Updated element -->
                        <div class="post-info">
                            Checked-in
                            <a href="https://travooo.com/place/3485165" class="link-place">The House of Dancing Water</a>
                            on Sep 1, 2020
                            <i class="trav-globe-icon" ></i>
                        </div>
                        <!-- Updated element END -->
                        </div>
                        </div>
                        <div class="post-top-info-action" >
                        <div class="dropdown" >
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <i class="trav-angle-down" ></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                        <a class="dropdown-item" href="#" onclick="post_delete('616211','Post', this, event)" >
                        <span class="icon-wrap" >

                        </span>
                        <div class="drop-txt" >
                        <p ><b >Delete</b></p>
                        <p style="color:red" >Remove this post</p>
                        </div>
                        </a>
                        </div>
                        </div>
                        </div>
                        </div>
                        <!-- New elements -->
                        <div class="post-txt-wrap">
                            <div>
                                <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                            </div>
                        </div>
                        <div class="check-in-point">
                            <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                        </div>
                        <!-- New elements END -->
                        <div class="post-image-container" >
                        <!-- New elements -->
                        <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;" >
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                        </ul>
                        <!-- New element END -->
                        </div>
                        <div class="post-footer-info" >
                        <!-- Updated elements -->
                        <div class="post-foot-block post-reaction">
                            <span class="post_like_button" id="601451">
                                <a href="#">
                                    <i class="trav-heart-fill-icon"></i>
                                </a>
                            </span>
                            <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                <b >0</b> Likes
                            </span>
                        </div>
                        <!-- Updated element END -->
                        <div class="post-foot-block" >
                        <a href="#" data-tab="comments616211" >
                        <i class="trav-comment-icon" ></i>
                        </a>
                        <ul class="foot-avatar-list" >
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                        </ul>
                        <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                        </div>
                        <div class="post-foot-block ml-auto" >
                        <span class="post_share_button" id="616211" >
                        <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                        <i class="trav-share-icon" ></i>
                        </a>
                        </span>
                        <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                        </div>
                        </div>
                        <div class="post-comment-wrapper" id="following616211" >
                        </div>
                        <!-- Removed comments block -->
                    </div>
                    <!-- Primary post block - photo*3 and text END -->
                </div>
                <div class="shared-post-actions share-to-friend">
                    <button class="extednd-btn">
                        <span class="extend-ttl">Extend</span> 
                        <span class="shrink-ttl">Shrink</span>
                    </button>
                    <div class="friend-search">
                        <i class="trav-search-icon" ></i>
                        <input type="text" placeholder="Search in your friends list...">
                    </div>
                    <div class="friend-search-results">
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light">Send</button>
                        </div>
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light" disabled>Sent</button>
                        </div>
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light">Send</button>
                        </div>
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light">Send</button>
                        </div>
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light" disabled>Sent</button>
                        </div>
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Send to a friend modal END -->
<!-- Spam Report modal -->
<div class="modal white-style spam-report-modal" data-backdrop="false"                                               id="spamReportDlgNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style" role="document">
        <div class="post-block">
            <div class="modal-header">
                <h4 class="side-ttl">Report Post</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-check mb-2">
                        <i class="fa fa-exclamation-circle report-exclamation" aria-hidden="true"></i>
                        <h5 class="d-inline-block">What is the problem with this post?</h5>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container">
                                    <div class="radio-title-group">
                                        <div class="input-container">
                                            <input id="spam" class="radio-button" type="radio" name="spam_post_type" value="0" checked="checked">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Spam
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="fake-news" class="radio-button" type="radio" name="spam_post_type" value="2">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Fake News
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="harassment" class="radio-button" type="radio" name="spam_post_type" value="3">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Harassment                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="hate-speech" class="radio-button" type="radio" name="spam_post_type" value="4">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Hate Speech                                               </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="nudity" class="radio-button" type="radio" name="spam_post_type" value="5">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Nudity
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="terrorism" class="radio-button" type="radio" name="spam_post_type" value="6">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Terrorism
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="violence" class="radio-button" type="radio" name="spam_post_type" value="7">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Violence
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="other" class="radio-button" type="radio" name="spam_post_type" value="1">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Other
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control report-span-input" id="spamText" placeholder="Something Else" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="send-report-btn" id="spamSend">Send</button>
            </div>
            <input type="hidden" name="dataid" id="dataid">
            <input type="hidden" name="posttype" id="posttype">
        </div>
    </div>
</div>
<!-- Spam Report modal END -->
<!-- Delete Post modal -->
<div class="modal white-style spam-report-modal" data-backdrop="false" id="deletePostNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style" role="document">
        <div class="post-block">
            <div class="modal-header">
                <h4 class="side-ttl">Delete Post</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group mb-3">
                    <div class="form-check mb-2">
                        <h5 class="d-inline-block">Are you sure you want to delete this post?</h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="send-report-btn link">Cancel</button>
                <button type="submit" class="send-report-btn red">Delete</button>
            </div>
            <input type="hidden" name="dataid" id="dataid">
            <input type="hidden" name="posttype" id="posttype">
        </div>
    </div>
</div>
<!-- Delete Post modal END -->
<!-- Aside Slider modal -->
<div class="modal aside-slider-modal" id="asideSliderPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="wrapper hidden">
        <a href="#" class="side-inner-slider-prev"></a>
        <a href="#" class="side-inner-slider-next"></a>
        <!-- Photo Slider -->
        <div class="aside-photo-slider-inner">
            <div class="aside-photo-slider-item" data-thumb="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg">
                <a href="#" class="go-to-link-box">
                    <div class="go-to-link"><i class="trav-link-out-icon"></i> Go to <span> Disneyland</span></div>
                    <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg" alt="">
                    <div class="aside-photo-slider-item-top">
                        <div>
                            <div>Photo by <strong>Patrick Smith</strong> <i>&middot;</i> <span>5 hours ago</span></div>
                            <div class="reactions">
                                <span><i class="trav-heart-fill-icon"></i> 6</span>
                                <span><i class="trav-comment-icon"></i> 20</span>
                            </div>
                        </div>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="aside-photo-slider-item-bottom">
                        <h3>Disneyland</h3>
                        <p>Theme park in Anaheim, California, United States</p>
                    </div>
                </a>
            </div>
            <div class="aside-photo-slider-item" data-thumb="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg">
                <a href="#" class="go-to-link-box">
                    <div class="go-to-link"><i class="trav-link-out-icon"></i> Go to <span> Disneyland</span></div>
                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                    <div class="aside-photo-slider-item-top">
                        <div>
                            <div>Photo by <strong>Patrick Smith</strong> <i>&middot;</i> <span>5 hours ago</span></div>
                            <div class="reactions">
                                <span><i class="trav-heart-fill-icon"></i> 6</span>
                                <span><i class="trav-comment-icon"></i> 20</span>
                            </div>
                        </div>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="aside-photo-slider-item-bottom">
                        <h3>Disneyland</h3>
                        <p>Theme park in Anaheim, California, United States</p>
                    </div>
                </a>
            </div>
            <div class="aside-photo-slider-item" data-thumb="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg">
                <a href="#" class="go-to-link-box">
                    <div class="go-to-link"><i class="trav-link-out-icon"></i> Go to <span> Disneyland</span></div>
                    <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                    <div class="aside-photo-slider-item-top">
                        <div>
                            <div>Photo by <strong>Patrick Smith</strong> <i>&middot;</i> <span>5 hours ago</span></div>
                            <div class="reactions">
                                <span><i class="trav-heart-fill-icon"></i> 6</span>
                                <span><i class="trav-comment-icon"></i> 20</span>
                            </div>
                        </div>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="aside-photo-slider-item-bottom">
                        <h3>Disneyland</h3>
                        <p>Theme park in Anaheim, California, United States</p>
                    </div>
                </a>
            </div>
            <div class="aside-photo-slider-item" data-thumb="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg">
                <a href="#" class="go-to-link-box">
                    <div class="go-to-link"><i class="trav-link-out-icon"></i> Go to <span> Disneyland</span></div>
                    <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg" alt="">
                    <div class="aside-photo-slider-item-top">
                        <div>
                            <div>Photo by <strong>Patrick Smith</strong> <i>&middot;</i> <span>5 hours ago</span></div>
                            <div class="reactions">
                                <span><i class="trav-heart-fill-icon"></i> 6</span>
                                <span><i class="trav-comment-icon"></i> 20</span>
                            </div>
                        </div>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="aside-photo-slider-item-bottom">
                        <h3>Disneyland</h3>
                        <p>Theme park in Anaheim, California, United States</p>
                    </div>
                </a>
            </div>
            <div class="aside-photo-slider-item" data-thumb="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg">
                <a href="#" class="go-to-link-box">
                    <div class="go-to-link"><i class="trav-link-out-icon"></i> Go to <span> Disneyland</span></div>
                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                    <div class="aside-photo-slider-item-top">
                        <div>
                            <div>Photo by <strong>Patrick Smith</strong> <i>&middot;</i> <span>5 hours ago</span></div>
                            <div class="reactions">
                                <span><i class="trav-heart-fill-icon"></i> 6</span>
                                <span><i class="trav-comment-icon"></i> 20</span>
                            </div>
                        </div>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="aside-photo-slider-item-bottom">
                        <h3>Disneyland</h3>
                        <p>Theme park in Anaheim, California, United States</p>
                    </div>
                </a>
            </div>
            <div class="aside-photo-slider-item" data-thumb="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg">
                <a href="#" class="go-to-link-box">
                    <div class="go-to-link"><i class="trav-link-out-icon"></i> Go to <span> Disneyland</span></div>
                    <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                    <div class="aside-photo-slider-item-top">
                        <div>
                            <div>Photo by <strong>Patrick Smith</strong> <i>&middot;</i> <span>5 hours ago</span></div>
                            <div class="reactions">
                                <span><i class="trav-heart-fill-icon"></i> 6</span>
                                <span><i class="trav-comment-icon"></i> 20</span>
                            </div>
                        </div>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="aside-photo-slider-item-bottom">
                        <h3>Disneyland</h3>
                        <p>Theme park in Anaheim, California, United States</p>
                    </div>
                </a>
            </div>
            <div class="aside-photo-slider-item" data-thumb="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg">
                <a href="#" class="go-to-link-box">
                    <div class="go-to-link"><i class="trav-link-out-icon"></i> Go to <span> Disneyland</span></div>
                    <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg" alt="">
                    <div class="aside-photo-slider-item-top">
                        <div>
                            <div>Photo by <strong>Patrick Smith</strong> <i>&middot;</i> <span>5 hours ago</span></div>
                            <div class="reactions">
                                <span><i class="trav-heart-fill-icon"></i> 6</span>
                                <span><i class="trav-comment-icon"></i> 20</span>
                            </div>
                        </div>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="aside-photo-slider-item-bottom">
                        <h3>Disneyland</h3>
                        <p>Theme park in Anaheim, California, United States</p>
                    </div>
                </a>
            </div>
            <div class="aside-photo-slider-item" data-thumb="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg">
                <a href="#" class="go-to-link-box">
                    <div class="go-to-link"><i class="trav-link-out-icon"></i> Go to <span> Disneyland</span></div>
                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
                    <div class="aside-photo-slider-item-top">
                        <div>
                            <div>Photo by <strong>Patrick Smith</strong> <i>&middot;</i> <span>5 hours ago</span></div>
                            <div class="reactions">
                                <span><i class="trav-heart-fill-icon"></i> 6</span>
                                <span><i class="trav-comment-icon"></i> 20</span>
                            </div>
                        </div>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="aside-photo-slider-item-bottom">
                        <h3>Disneyland</h3>
                        <p>Theme park in Anaheim, California, United States</p>
                    </div>
                </a>
            </div>
            <div class="aside-photo-slider-item" data-thumb="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg">
                <a href="#" class="go-to-link-box">
                    <div class="go-to-link"><i class="trav-link-out-icon"></i> Go to <span> Disneyland</span></div>
                    <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="">
                    <div class="aside-photo-slider-item-top">
                        <div>
                            <div>Photo by <strong>Patrick Smith</strong> <i>&middot;</i> <span>5 hours ago</span></div>
                            <div class="reactions">
                                <span><i class="trav-heart-fill-icon"></i> 6</span>
                                <span><i class="trav-comment-icon"></i> 20</span>
                            </div>
                        </div>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="aside-photo-slider-item-bottom">
                        <h3>Disneyland</h3>
                        <p>Theme park in Anaheim, California, United States</p>
                    </div>
                </a>
            </div>
        </div>
        <!-- Photo Slider END -->
    </div>
</div>
<!-- Aside Slider modal END -->
</div>
</body>
<script>
var isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints > 0;
if(isTouchDevice) {
    $('.create-new-post-form-trigger').click(function () {
        $('.mobile-user-actions-popup').addClass('show');
        $('.mobile-user-actions-popup').addClass('bottom');
    })
    $('.create-new-post-popup .mobile-close-btn, .create-new-post-checkin-popup .mobile-close-btn, .post-create-controls button').click(function () {
        $('.mobile-user-actions-popup.slide-up').removeClass('slide-up');
        $('.mobile-user-actions-popup.bottom').removeClass('bottom');
        $('.mobile-user-actions-popup').removeClass('show');
    });
}
</script>
<!-- To sanitize summernote input -->
<script>
    /**
    * Summernote StripTags
    *
    * This is a plugin for Summernote (www.summernote.org) WYSIWYG editor.
    * To strip unwanted HTML tags and attributes while pasting content in editor.
    *
    * @author Hitesh Aggarwal, Extenzine
    *
    */

    (function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(window.jQuery);
    }
    }(function ($) {
    $.extend($.summernote.options, {
        stripTags: ['font', 'style', 'embed', 'param', 'script', 'html', 'body', 'head', 'meta', 'title', 'link', 'iframe', 'applet', 'noframes', 'noscript', 'form', 'input', 'select', 'option', 'colgroup', 'col', 'std', 'xml:', 'st1:', 'o:', 'w:', 'v:'],
        stripAttributes: ['font', 'style', 'embed', 'param', 'script', 'html', 'body', 'head', 'meta', 'title', 'link', 'iframe', 'applet', 'noframes', 'noscript', 'form', 'input', 'select', 'option', 'colgroup', 'col', 'std', 'xml:', 'st1:', 'o:', 'w:', 'v:'],
        onAfterStripTags: function ($html) {
            return $html;
        }
    });

    $.extend($.summernote.plugins, {
        'striptags': function (context) {
            var $note = context.layoutInfo.note;
            var $options = context.options;
            $note.on('summernote.paste', function (e, evt) {
                evt.preventDefault();
                var text = evt.originalEvent.clipboardData.getData('text/plain'), html = evt.originalEvent.clipboardData.getData('text/html');
                if (html) {
                var tagStripper = new RegExp('<[ /]*(' + $options.stripTags.join('|') + ')[^>]*>', 'gi'), attributeStripper = new RegExp(' (' + $options.stripAttributes.join('|') + ')(="[^"]*"|=\'[^\']*\'|=[^ ]+)?', 'gi'), commentStripper = new RegExp('<!--(.*?)-->', 'g');
                html = html.toString().replace(commentStripper, '').replace(tagStripper, '').replace(attributeStripper, ' ').replace(/( class=(")?Mso[a-zA-Z]+(")?)/g, ' ').replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">").replace(/[\u2018\u2019\u201A]/g, "'").replace(/[\u201C\u201D\u201E]/g, '"').replace(/\u2026/g, '...').replace(/[\u2013\u2014]/g, '-');
                }
                var $html = $('<div/>').html(html || text);
                $html = $options.onAfterStripTags($html);
                $note.summernote('insertNode', $html[0]);
                return false;
            });
        }
    });
    }));

    $('#sanitizeSummernote').summernote({
        placeholder: 'Type to test sanitizer...',
        tabsize: 2,
        height: 100,
        toolbar: [
            ['custom', ['striptags']],
        ],
        stripTags: ['style', 'a', 'br', 'p', 'div', 'ul', 'ol', 'li', 'span', 'h1', 'h2', 'h3', 'h4', 'h5', 'font', 'b', 'strong', 'i', 'img', 'embed', 'param', 'script'],
        onAfterStripTags: function ($html) {
            $html.find('table').addClass('table');
            return $html;
        }
    });
</script>
@include('site.home.partials.modal_comments_like')
@include('site.layouts._footer-scripts')
</html>
