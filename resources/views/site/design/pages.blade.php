<!DOCTYPE html>
<html class="page" lang="en">
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="date=no">
        <meta name="format-detection" content="address=no">
        <meta name="format-detection" content="email=no">
        <meta content="notranslate" name="google">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
         <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css">
        <!-- <link rel="stylesheet" href="{{asset('assets2/css/place.css?v='.time()) }}"> -->
        <link rel="stylesheet" href="{{asset('assets2/css/style-15102019-9.css')}}">
        <link rel="stylesheet" href="{{asset('assets2/css/travooo.css?v='.time())}}">
        <link rel="stylesheet" href="{{asset('assets3/css/datepicker-extended.css')}}">
        <!-- <link rel="stylesheet" type="text/css" href="{{asset('assets3/css/slider-pro.min.css')}}" media="screen"/> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
        <link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>

        <link rel="stylesheet" href="{{asset('assets3/css/star.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/lightslider.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous">
        <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> -->
        <script src="{{ asset('assets2/js/jquery-3.1.1.min.js') }}"></script>
        <script>
            var map_var = "";
            var map_func = "";

            // Design page scripts
            $(document).ready(function() {
                // Page nav
                $(document).on('click', ".pageNav", function (e) {
                    e.preventDefault();
                    var pageId = '#' + $(this).data('page');
                    $('#feed > div').addClass('hide');
                    $('#' + $(this).data('page')).removeClass('hide');
                });
                var isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints > 0;

                // Sticky left menu for desktop
                if(!isTouchDevice) {
                    var leftMenu = $('#leftOutsideMenu');
                    var mainHeader = $('.main-header');
                    $(window).scroll(function() {
                        var windowTop = $(window).scrollTop();
                        if (windowTop > mainHeader.height()) {
                            leftMenu.addClass('sticky');
                        } else {
                            leftMenu.removeClass('sticky');
                        }
                    });
                }

                $('.add-post__emoji').emojiInit({
                    fontSize:20,
                    success : function(data){

                    },
                    error : function(data,msg){
                    }
                });
                $('.add-msg__emoji').emojiInit({
                    fontSize:20,
                    success : function(data){

                    },
                    error : function(data,msg){
                    }
                });
                $('.checkin_date').datepicker({
                    format: 'M dd yy',
                });
                $('.checkin_time').timepicker({
                    timeFormat: 'h:i A'
                });
                // Like button
                $(document).on('click', ".post_like_button a", function (e) {
                    e.preventDefault();
                    $(this).closest('.post_like_button').toggleClass('liked');
                });
                // Tooltips
                $('[data-toggle="tooltip"]').tooltip({
                    placement: 'top',
                    trigger: 'hover'
                });
                // Post text tag popover
                $(".post-text-tag").popover({
                    html: true, 
                    content: $('#popover-content').html(),
                    template: '<div class="popover bottom tagging-popover" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
                // Post more-people popover
                $(".post-more-people").popover({
                    html: true, 
                    content: $('#popover-content-people').html(),
                    template: '<div class="popover bottom tagging-popover white" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
                // Post holiday popover
                $(".post-holiday-popover").popover({
                    html: true, 
                    content: $('#popover-content-holiday').html(),
                    template: '<div class="popover bottom tagging-popover white" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'click',
                });
                // Map popover
                $(".map-popover").popover({
                    html: true, 
                    content: $('#popover-content-map').html(),
                    template: '<div class="popover top forecast-map-popover white" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
                // Custom scrollbar
                $('.friend-search-results').mCustomScrollbar();
            });
        </script>
        <style>
            /* Left menu */
            .left-outside-menu-wrap {
                width: auto;
                margin-top: 30px;
            }
            .left-outside-menu-wrap .left-outside-menu li a {
                opacity: 1;
                font-size: 18px;
                font-family: inherit;
                color: #999999;
                flex-direction: row;
                align-items: center;
                justify-content: flex-start;
            }
            .left-outside-menu-wrap .left-outside-menu li.active a {
                color: #1a1a1a;
                font-family: 'Circular Std Bold';
            }
            .left-outside-menu-wrap .left-outside-menu li a .icon-wrap {
                width: 70px;
                text-align: center;
            }
            @media (min-width: 992px) {
                #leftOutsideMenu.sticky {
                    transform: translate(-217px, -68px);
                }
            }
            /* Left menu END */

            .create-new-post-popup .note-placeholder {
                display: none!important;
            }
        </style>
        <style>
            /* Landing page styles */
            .post-block .check-in-point {
                margin-bottom: 22px;
            }
            #feed .hide {
                display: none;
            }
        </style>
    </head>

    <body class="page__body">

    <svg style="position: absolute; width: 0; height: 0; overflow: hidden" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="angle-circle-up-solid" viewBox="0 0 21 21">
                <path d="M10.5.5c5.523 0 10 4.476 10 9.999 0 5.523-4.477 10-10 10s-10-4.477-10-10 4.477-10 10-10zm1.325 7.937L10.539 7.06l-.04.041-.038-.04-1.287 1.377.032.034L6.1 11.758l1.286 1.378L10.5 9.841l3.112 3.295 1.287-1.378-3.105-3.288z"/>
            </symbol>
        </defs>
    </svg>
        @include('site.layouts._header')
        <div class="content-wrap page-body__mobile">
            <div class="container-fluid">
                <!-- Left Menu -->
                <div class="left-outside-menu-wrap" id="leftOutsideMenu" dir="auto">
                <div class="leftOutsideMenuClose" dir="auto"></div>
                <ul class="left-outside-menu" dir="auto">
                <li class="active" dir="auto">
                <a href="https://travooo.com/home" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-home.png')}}" width="49" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Home</span>

                </a>
                </li>
                <li dir="auto">
                <a href="https://travooo.com/travelmates-newest" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-mates.png')}}" width="47" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Travel mates</span>
                </a>
                </li>
                <li class="" dir="auto">
                <a href="https://travooo.com/trip-plans" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-plans.png')}}" width="43" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Trip Plans</span>
                </a>
                </li>
                <li class="" dir="auto">
                <a href="https://travooo.com/discussions" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-ask.png')}}" width="46" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Ask</span>
                </a>
                </li>
                <li dir="auto"><a href="https://travooo.com/profile-map" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-map.png')}}"width="44" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Map</span>
                </a>
                </li>
                <li class="" dir="auto">
                <a href="https://travooo.com/reports/list?order=newest&amp;" dir="auto">
                <!-- New element -->
                <span class="icon-wrap">
                    <img src="{{asset('assets2/image/left-menu-icon-travelogs.png')}}"width="46" alt="">
                </span>
                <!-- New element END -->
                <span dir="auto">Reports</span>
                </a>
                </li>
                </ul>
                </div>
                <!-- Left Menu END -->
                <!-- Page nav - just for comfortable pages demonstration -->
                <div style="position: fixed; right: 100px; top:100px; background: #ffffff; border: 1px solid #e6e6e6; border-radius: 5px; width: 200px; display: none; padding: 10px;">
                    <h3>Navigation</h3>
                    <a href="" data-page="textPage" class="pageNav" style="display: block; margin-bottom: 10px;">1. Text post landing page</a>
                    <a href="" data-page="imagePage" class="pageNav" style="display: block; margin-bottom: 10px;">2. Image post type with or without caption</a>
                    <a href="" data-page="videoPage" class="pageNav" style="display: block; margin-bottom: 10px;">3. Video post type with or without caption</a>
                    <a href="" data-page="mediaPage" class="pageNav" style="display: block; margin-bottom: 10px;">4. Multiple media (photos & videos) post type with or without caption</a>
                    <a href="" data-page="mediaPage" class="pageNav" style="display: block; margin-bottom: 10px;">5. Check-in post type with text/media and without text/media</a>
                    <a href="" data-page="reviewPage" class="pageNav" style="display: block; margin-bottom: 10px;">6. Review post type with and without media.</a>
                    <a href="" data-page="discussionPage" class="pageNav" style="display: block; margin-bottom: 10px;">7. Discussion post type with and without media.</a>
                    <a href="" data-page="tripPage" class="pageNav" style="display: block; margin-bottom: 10px;">8. Post type for Trip Plan</a>
                    <a href="" data-page="tripMediaPage" class="pageNav" style="display: block; margin-bottom: 10px;">9. Post type for media shared from within a trip plan</a>
                    <a href="" data-page="sharedPage" class="pageNav" style="display: block; margin-bottom: 10px;">10. Post type which are shared internally</a>
                    <a href="" data-page="linkPage" class="pageNav" style="display: block; margin-bottom: 10px;">11. Post type for sharing an external link</a>
                    <a href="" data-page="eventPage" class="pageNav" style="display: block; margin-bottom: 10px;">12. Post type for event & activity post type.</a>
                </div>
                <!-- Page nav END -->
                <div class="custom-row">
                    <!-- Main content -->
                    <div class="main-content-layer">
                        <div id="feed">
                        <div id="ImagesInnerPage">@include('site.design.partials._ImagesInnerPage')</div>
                        <div id="textPage">@include('site.design.partials._textPostPage')</div>
                        <div id="imagePage">@include('site.design.partials._imagePostPage')</div>
                        <div id="videoPage">@include('site.design.partials._videoPostPage')</div>
                        <div id="mediaPage">@include('site.design.partials._multipleMediaPostPage')</div>
                        <div id="reviewPage">@include('site.design.partials._reviewPostPage')</div>
                        <div id="discussionPage">@include('site.design.partials._discussionPostPage')</div>
                        <div id="tripPage">@include('site.design.partials._tripPostPage')</div>
                        <div id="tripMediaPage">@include('site.design.partials._tripMediaPostPage')</div>
                        <div id="linkPage">@include('site.design.partials._linkPostPage')</div>
                        <div id="sharedPage">@include('site.design.partials._sharedPostPage')</div>
                        <div id="eventPage">@include('site.design.partials._eventPostPage')</div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('site.place2.partials.modal_like')

<footer class="page-footer page__footer"></footer>

<script src="{{ asset('assets3/js/tether.min.js') }}"></script>
<script src="{{ asset('assets2/js/popper.min.js') }}"></script>
<script src="{{ asset('assets2/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets3/js/bootstrap-datepicker-extended.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets2/js/slick.min.js') }}"></script>
<script src="{{ asset('assets3/js/main.js?0.3.2') }}"></script>
<script src="{{ asset('assets2/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/js/lightgallery-all.min.js"></script>
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
<script src="{{ asset('assets3/js/jquery.barrating.min.js') }}"></script>
<script src="{{ asset('assets3/js/star.js') }}"></script>

<script src="{{ asset('assets2/js/emoji/jquery.emojiFace.js?v=0.1') }}"></script>
<script src="{{ asset('assets2/js/skyloader.js') }}"></script>
<!-- <script src="{{ asset('assets3/js/jquery.sliderPro.min.js') }}"></script> -->
<script src="{{ asset('assets2/js/jquery-confirm/jquery-confirm.min.js') }}"></script>
<script src="{{ asset('assets2/js/lightbox2/src/js/lightbox.js') }}"></script>
<script data-cfasync="false" src="{{asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('assets2/js/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('assets2/js/posts-script.js?v=0.3') }}"></script>
@include('site.home.partials._spam_dialog')

<!-- Message to user popup -->
<div class="modal msg-to-user-popup" id="messageToUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-650" role="document" >
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <div class="top-title-layer" >
                    <h3 class="title" >
                        Message
                        <img class="msg-to" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                        <span class="txt">Bora Young</span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-create-input">
                    <textarea name="" id="" cols="30" rows="10" placeholder="Type a message..."></textarea>
                    <div class="medias medias-slider" >
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed loading">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-create-controls">
                    <div class="emoji-block">
                        <button class="add-msg__emoji" emoji-target="add_msg_text" type="button">🙂</button> 
                    </div>
                    <div class="post-alloptions" >
                        <ul class="create-link-list" >
                            <li class="post-options" >
                                <input type="file" name="file[]" id="file" style="display:none" multiple="" >
                                <i class="trav-camera click-target" data-target="file" ></i>
                            </li>
                        </ul>
                    </div>
                    <div class="ml-auto post-buttons">
                        <button type="button" class="btn btn-light-grey btn-bordered mr-3" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-light-primary btn-bordered" >Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Message to user popup END -->
<!-- Users who like modal -->
<div class="modal white-style users-who-like-modal" data-backdrop="false" id="usersWhoLike" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                            Poeple who liked this post <span>16</span>
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar" >
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer">
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Friend</span>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Follows you</span>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-grey btn-bordered">Unfollow</button>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Users who like modal END -->
<!-- Share post modal -->
<div class="modal opacity--wrap share-post-modal" data-backdrop="false" id="sharePostModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog share-post-new modal-670" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" style="padding-bottom: 40px;" >
                <div class="share-modal-header">
                    <div>
                      <h4>Share this post</h4>
                    </div>
                    <button type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon" dir="auto"></i>
                    </button>
                </div>
                <div class="share-modal-action">
                    <div class='user--info'>
                      <img class="ava" src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                      <p>Name</p>
                    </div>
                    <div class='share-dropdown-modal'>
                      <div class="dropdown privacy-settings">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="far fa-globe-americas"></i>
                            <span>PUBLIC</span>
                            <i class="fas fa-sort-down"></i>
                        </button>
                            <div class="dropdown-menu dropdown-toggle permissoin_show hided" aria-labelledby="dropdownMenuButton">
                            <div class="share-drop-flex">
                                <i class="fal fa-globe-asia"></i>
                                <div>
                                    <a class="dropdown-item" href="#">
                                        <span>Public</span>
                                        <span>Anyone can see this post</span>
                                    </a>
                                </div>
                            </div>
                            <div class="share-drop-flex">
                                <i class="fal fa-user-friends"></i>
                                <div>
                                    <a class="dropdown-item" href="#">
                                        <span>Friends Only</span>
                                        <span>Only you and your friends</span>
                                    </a>
                                </div>
                            </div>
                            <div class="share-drop-flex">
                                <i class="fal fa-lock"></i>
                                <div>
                                    <a class="dropdown-item" href="#">
                                        <span>Only Me</span>
                                        <span>Only you can see this post</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <textarea name="" style="resize: none;" id="message_popup_text_for_share" cols="30" rows="2" placeholder="Write something..."></textarea>
                  <div class="shared-post-wrap">
                    <!-- Primary post block - photo*3 and text -->
                    <div class="post-block" >
                        <div class="post-top-info-layer" >
                        <div class="post-top-info-wrap" >
                        <div class="post-top-avatar-wrap" >
                        <a class="post-name-link" href="https://travooo.com/profile/831" >
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                        </a>
                        </div>
                        <div class="post-top-info-txt" >
                        <div class="post-top-name" >
                        <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                        </div>
                        <!-- Updated element -->
                        <div class="post-info">
                            Checked-in
                            <a href="https://travooo.com/place/3485165" class="link-place">The House of Dancing Water</a>
                            on Sep 1, 2020
                            <i class="trav-globe-icon" ></i>
                        </div>
                        <!-- Updated element END -->
                        </div>
                        </div>
                        </div>
                        <!-- New elements -->
                        <div class="post-txt-wrap">
                            <div>
                                <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                            </div>
                        </div>
                        <div class="check-in-point">
                            <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                        </div>
                        <!-- New elements END -->
                        <div class="post-image-container" >
                        <!-- New elements -->
                        <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;" >
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                        </ul>
                        </div>       
                    </div>
                    <!-- Primary post block - photo*3 and text END -->
                </div>
                <div class="share-footer-modal">
                    <div  class="share-footer-modal__icons">
                        <a class="ico--ws"><i class="fab fa-whatsapp"></i></a>
                        <a class="ico--face"><i class="fab fa-facebook-f"></i></a>
                        <a class="ico--twitter"><i class="fab fa-twitter"></i></a>
                        <a class="ico--pin"><i class="fab fa-pinterest-p"></i></a>
                        <a class="ico--tumb"><i class="fab fa-tumblr"></i></a>
                        <a class="ico--link"><i class="far fa-link"></i></a>
                    </div>
                    <button class="btn place_cancel_btn share-modal__close" type="button">Cancel</button>
                    <button class="btn place_share_btn share_post_btn">Share</button> 
                    <input type="hidden" name="share-post-id" id="share-post-id">
                    <input type="hidden" name="share-post-type" id="share-post-type">
                </div>

            </div>

        </div>
    </div>
</div>
<!-- Share post modal END -->
<!-- Users who shared modal -->
<div class="modal opacity--wrap users-who-like-modal" data-backdrop="false" id="usersWhoShare" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="share-modal-header">
                    <div>
                      <h4>Poeple who shared this post</h4>
                    </div>
                    <button type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon" dir="auto"></i>
                    </button>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar" >
                    <div class="people-row" id="mate648">
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name mobile--followers" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Friend</span>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                    <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648">
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name mobile--followers" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Friend</span>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                    <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Users who shared modal END -->
<!-- Send to a friend modal -->
<div class="modal opacity--wrap share-post-modal" data-backdrop="false" id="sendToFriendModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-670" role="document" >
        <div class='friend-share-modal scroll--helper'>
            <div class='modal-content'>
                <div class='friend-share-modal__header'>
                    <div>
                        <h2>Send to a Friend</h2>
                    </div>
                    <button class="modal--close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="shared-post-comment">
                    <div class="shared-post-comment-details">
                        <div class="shared-post-comment-author">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                            Name
                        </div>
                    </div>
                    <div class="shered-post-comment-text">
                        <textarea name="" id="message_popup_text" cols="30" rows="1" placeholder="Write something..."></textarea>
                    </div>     
                    <div class="module-colapse">
                        <div aria-expanded="false" class="shared-post-wrap collapse demo" id="message_popup_content">
                            <div class="shared-post-wrap">
                                <!-- Primary post block - photo*3 and text -->
                                <div class="post-block" >
                                    <div class="post-top-info-layer" >
                                    <div class="post-top-info-wrap" >
                                    <div class="post-top-avatar-wrap" >
                                    <a class="post-name-link" href="https://travooo.com/profile/831" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" >
                                    </a>
                                    </div>
                                    <div class="post-top-info-txt" >
                                    <div class="post-top-name" >
                                    <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                                    </div>
                                    <!-- Updated element -->
                                    <div class="post-info">
                                        Checked-in
                                        <a href="https://travooo.com/place/3485165" class="link-place">The House of Dancing Water</a>
                                        on Sep 1, 2020
                                        <i class="trav-globe-icon" ></i>
                                    </div>
                                    <!-- Updated element END -->
                                    </div>
                                    </div>
                                    </div>
                                    <!-- New elements -->
                                    <div class="post-txt-wrap">
                                        <div>
                                            <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                            <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                                        </div>
                                    </div>
                                    <div class="check-in-point">
                                        <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                                    </div>
                                    <!-- New elements END -->
                                    <div class="post-image-container" >
                                    <!-- New elements -->
                                    <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;" >
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                            <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                            <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                            <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                            </a>
                                        </li>
                                    </ul>
                                    </div>       
                                </div>
                                <!-- Primary post block - photo*3 and text END -->
                            </div>
                        </div>
                        <a role="button" class='button--colapse collapsed' class="collapsed" data-toggle="collapse" data-target=".demo"
                        aria-expanded="false" aria-controls="collapseExample"></a>
                    </div>
                    <div class="friend-search">
                        <i class="trav-search-icon" ></i>
                        <input type="text" placeholder="Search in your friends list...">
                    </div>
                </div>
                <div class="friend-search-results">
                    @php
                        $friends_users = App\Models\User\User::whereIn('id',get_friendlist())->distinct()->get();

                    @endphp
                    @if(count($friends_users)>0)
                        @foreach ($friends_users as $f_user)
                            <div class="friend-search-results-item">
                                <img class="user-img" src="{{check_profile_picture($f_user->profile_picture)}}" alt="">
                                <div class="text">
                                    <div class="location-title">{{$f_user->name}}</div>
                                </div>
                                <button class="btn btn-light send_message" id="send_message" data-user="{{$f_user->id}}">Send</button>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

    </div>
</div>
<!-- Send to a friend modal END -->
<!-- Spam Report modal -->
<div class="modal opacity--wrap spam-report-modal" data-backdrop="false"                                               id="spamReportDlgNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style report-modal-helper" role="document">
        <div class="post-block">
            <div class="share-modal-header">
                <div>
                  <h4>Report Post</h4>
                </div>
                <button class="share-modal__close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-check mb-2">
                        <i class="fa fa-exclamation-circle report-exclamation" aria-hidden="true"></i>
                        <h5 class="d-inline-block">What is the problem with this post?</h5>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container">
                                    <div class="radio-title-group">
                                        <div class="input-container">
                                            <input id="spam" class="radio-button" type="radio" name="spam_post_type" value="0" checked="checked">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Spam
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="fake-news" class="radio-button" type="radio" name="spam_post_type" value="2">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Fake News
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="harassment" class="radio-button" type="radio" name="spam_post_type" value="3">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Harassment                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="hate-speech" class="radio-button" type="radio" name="spam_post_type" value="4">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Hate Speech                                               </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="nudity" class="radio-button" type="radio" name="spam_post_type" value="5">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Nudity
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="terrorism" class="radio-button" type="radio" name="spam_post_type" value="6">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Terrorism
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="violence" class="radio-button" type="radio" name="spam_post_type" value="7">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Violence
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="other" class="radio-button" type="radio" name="spam_post_type" value="1">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Other
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group spam--input" style='margin-bottom: 0!important;'>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control report-span-input" id="spamText" placeholder="Something Else" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="share-footer-modal modal-footer">
                <div  class="share-footer-modal__icons">
                </div>
                <button style="display: block;" class="btn place_cancel_btn share-modal__close" type="button">Cancel</button>
                <button class="btn" type="submit" id="spamSend">Send</button> 
            </div>
            <input type="hidden" name="dataid" id="dataid">
            <input type="hidden" name="posttype" id="posttype">
        </div>
    </div>
</div>
<!-- Spam Report modal END -->
<!-- Delete Post modal -->
<div class="modal white-style spam-report-modal" data-backdrop="false" id="deletePostNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style" role="document">
        <div class="post-block">
            <div class="modal-header">
                <h4 class="side-ttl">Delete Post</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group mb-3">
                    <div class="form-check mb-2">
                        <h5 class="d-inline-block">Are you sure you want to delete this post?</h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="send-report-btn link">Cancel</button>
                <button type="submit" class="send-report-btn red">Delete</button>
            </div>
            <input type="hidden" name="dataid" id="dataid">
            <input type="hidden" name="posttype" id="posttype">
        </div>
    </div>
</div>
<!-- Delete Post modal END -->
</body>
@include('site.home.partials.modal_comments_like')
@include('site.layouts._footer-scripts')

</html>
