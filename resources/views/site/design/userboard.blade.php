@php
$title = 'Dashboard';
@endphp

@extends('site.layouts.site')

@section('base')
<header class="main-header" dir="auto">
<div class="container-fluid fullwidth" dir="auto">
<nav class="navbar navbar-toggleable-md navbar-light bg-faded" dir="auto">
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" dir="auto">
<i class="trav-bars" dir="auto"></i>
</button>
<a class="navbar-brand" href="https://travooo.com/home" dir="auto">
<img src="https://travooo.com/assets2/image/main-circle-logo.png" alt="" dir="auto">
</a>
<div class="collapse navbar-collapse" id="navbarSupportedContent" dir="auto">
<div class="header-search-block" dir="auto">
<div class="head-search-inner" dir="auto">
<div class="search-block" dir="auto">
<form autocomplete="off" dir="auto">
<a class="search-btn" href="#" dir="auto"><i class="trav-search-icon" dir="auto"></i></a>
<input type="text" class="" id="mainSearchInput" autocomplete="off" placeholder="Search..." dir="auto">
<a href="#" class="delete-search" dir="auto">
<i class="trav-close-icon" dir="auto"></i>
</a>
 </form>
</div>
<div class="search-box" dir="auto">
<ul class="nav nav-tabs search-tabs" role="tablist" dir="auto">
<li class="nav-item" dir="auto">
<a class="nav-link active" href="#places" role="tab" data-toggle="tab" dir="auto">places <span id="numPlacesResult" dir="auto"></span></a>
</li>
<li class="nav-item" dir="auto">
<a class="nav-link" href="#restaurants" role="tab" data-toggle="tab" dir="auto">restaurants <span id="numRestaurantsResult" dir="auto"></span></a>
</li>
<li class="nav-item" dir="auto">
<a class="nav-link" href="#hotels" role="tab" data-toggle="tab" dir="auto">hotels <span id="numHotelsResult" dir="auto"></span></a>
</li>
<li class="nav-item" dir="auto">
<a class="nav-link" href="#countries" role="tab" data-toggle="tab" dir="auto">countries <span id="numCountriesResult" dir="auto"></span></a>
</li>
<li class="nav-item" dir="auto">
<a class="nav-link" href="#cities" role="tab" data-toggle="tab" dir="auto">cities <span id="numCitiesResult" dir="auto"></span></a>
</li>
<li class="nav-item" dir="auto">
<a class="nav-link" href="#people" role="tab" data-toggle="tab" dir="auto">people <span id="numUsersResult" dir="auto"></span></a>
</li>
</ul>
<div class="tab-content" dir="auto">
<div role="tabpanel" class="tab-pane fade in active show" id="places" aria-expanded="true" dir="auto">
<div class="drop-wrap" id="placesSearchResults" dir="auto">
</div>
</div>
<div role="tabpanel" class="tab-pane fade in" id="restaurants" dir="auto">
<div class="drop-wrap" id="restaurantsSearchResults" dir="auto">
</div>
</div>
<div role="tabpanel" class="tab-pane fade in" id="hotels" dir="auto">
<div class="drop-wrap" id="hotelsSearchResults" dir="auto">
</div>
</div>
<div role="tabpanel" class="tab-pane fade in" id="countries" dir="auto">
<div class="drop-wrap" id="countriesSearchResults" dir="auto">
</div>
</div>
<div role="tabpanel" class="tab-pane fade in" id="cities" dir="auto">
<div class="drop-wrap" id="citiesSearchResults" dir="auto">
</div>
</div>
<div role="tabpanel" class="tab-pane fade in" id="people" dir="auto">
<div class="drop-wrap" id="usersSearchResults" dir="auto">
</div>
</div>
</div>
</div>
</div>
</div>
<!-- New element -->
<div class="exp-progress" dir="auto">
    <span class="exp-icon" dir="auto">EXP</span>
    <div class="exp-progress-bar" dir="auto">
        <div class="progress" dir="auto">
            <div class="progress-bar" style="width: 70.0%" role="progressbar" aria-valuenow="70.0" aria-valuemin="0" aria-valuemax="100" dir="auto"></div>
        </div>
    </div>
    <span class="exp-icon bronze" dir="auto">EXP</span>
</div>
<!-- New element END -->
                <ul class="navbar-nav" dir="auto">
                    <li class="nav-item dropdown" dir="auto">
                        <a class="nav-link" href="#" onclick="toggleLastMessages()" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                            <i class="trav-comment-dots-icon" dir="auto"></i>
                            <div id="chat-unread-counter" class="counter" style="display: none;" dir="auto">0</div>
                        </a>
                        <div id="header-last-messages" class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-all-head" dir="auto">
                            <div class="drop-ttl" dir="auto">Messages</div>
                            <div class="drop-inner" style="height: 400px;overflow: auto;" dir="auto">
                                <div id="container-last-messages" class="conversation-inner bordered" dir="auto"><p id="loading-last-messages" style="margin: 20px; display: none;">Loading...</p><div class="conv-block" style="cursor: pointer;" onclick="window.location.href = 'https://travooo.com/chat/3669';"><div class="img-wrap" style="max-width: 70px;" dir="auto">

                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" onclick="window.open('/profile/1011')" alt="image" style="width:50px;height:50px;" dir="auto">

                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/chat/3669" class="inner-link" dir="auto">Andrey Emilov Alexov </a>
                                            </div>
                                            <div class="msg-txt" dir="auto">
                                                <p style="font-weight: normal;" dir="auto"></p>
                                            </div>
                                        </div>
                                        <div class="conv-time" dir="auto">
                                            <span class="time" dir="auto">02/012/2020 5:52 PM</span>
                                        </div>
                                    </div><div class="conv-block" style="cursor: pointer;" onclick="window.location.href = 'https://travooo.com/chat/3664';"><div class="img-wrap" style="max-width: 70px;" dir="auto">

                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" onclick="window.open('/profile/1011')" alt="image" style="width:50px;height:50px;" dir="auto">

                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/chat/3664" class="inner-link" dir="auto">Andrey Emilov Alexov </a>
                                            </div>
                                            <div class="msg-txt" dir="auto">
                                                <p style="font-weight: normal;" dir="auto">Ivan Turlakov was added to the chat</p>
                                            </div>
                                        </div>
                                        <div class="conv-time" dir="auto">
                                            <span class="time" dir="auto">02/012/2020 11:01 AM</span>
                                        </div>
                                    </div><div class="conv-block" style="cursor: pointer;" onclick="window.location.href = 'https://travooo.com/chat/3575';"><div class="img-wrap" style="max-width: 70px;" dir="auto">

                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" onclick="window.open('/profile/1011')" alt="image" style="width:50px;height:50px;" dir="auto">

                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/chat/3575" class="inner-link" dir="auto">Andrey Emilov Alexov </a>
                                            </div>
                                            <div class="msg-txt" dir="auto">
                                                <p style="font-weight: normal;" dir="auto">Hi</p>
                                            </div>
                                        </div>
                                        <div class="conv-time" dir="auto">
                                            <span class="time" dir="auto">28/011/2020 11:19 AM</span>
                                        </div>
                                    </div><div class="conv-block" style="cursor: pointer;" onclick="window.location.href = 'https://travooo.com/chat/3573';"><div class="img-wrap" style="max-width: 70px;" dir="auto">

                                            <img src="https://travooo.com/assets2/image/placeholders/male.png" onclick="window.open('/profile/')" alt="image" style="width:50px;height:50px;" dir="auto">

                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/chat/3573" class="inner-link" dir="auto"> </a>
                                            </div>
                                            <div class="msg-txt" dir="auto">
                                                <p style="font-weight: normal;" dir="auto">text</p>
                                            </div>
                                        </div>
                                        <div class="conv-time" dir="auto">
                                            <span class="time" dir="auto">27/011/2020 5:14 PM</span>
                                        </div>
                                    </div><div class="conv-block" style="cursor: pointer;" onclick="window.location.href = 'https://travooo.com/chat/3527';"><div class="img-wrap" style="max-width: 70px;" dir="auto">

                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/573/1593875668_image.png" onclick="window.open('/profile/573')" alt="image" style="width:50px;height:50px;" dir="auto">

                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/chat/3527" class="inner-link" dir="auto">Conall Armstrong </a>
                                            </div>
                                            <div class="msg-txt" dir="auto">
                                                <p style="font-weight: normal;" dir="auto"></p>
                                            </div>
                                        </div>
                                        <div class="conv-time" dir="auto">
                                            <span class="time" dir="auto">26/011/2020 7:26 PM</span>
                                        </div>
                                    </div><div class="conv-block" style="cursor: pointer;" onclick="window.location.href = 'https://travooo.com/chat/3528';"><div class="img-wrap" style="max-width: 70px;" dir="auto">

                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/866/1589913225_image.png" onclick="window.open('/profile/866')" alt="image" style="width:50px;height:50px;" dir="auto">

                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/chat/3528" class="inner-link" dir="auto">Ellis </a>
                                            </div>
                                            <div class="msg-txt" dir="auto">
                                                <p style="font-weight: normal;" dir="auto"></p>
                                            </div>
                                        </div>
                                        <div class="conv-time" dir="auto">
                                            <span class="time" dir="auto">26/011/2020 7:26 PM</span>
                                        </div>
                                    </div><div class="conv-block" style="cursor: pointer;" onclick="window.location.href = 'https://travooo.com/chat/3513';"><div class="img-wrap" style="max-width: 70px;" dir="auto">

                                            <img src="https://travooo.com/assets2/image/placeholders/male.png" onclick="window.open('/profile/')" alt="image" style="width:50px;height:50px;" dir="auto">

                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/chat/3513" class="inner-link" dir="auto"> </a>
                                            </div>
                                            <div class="msg-txt" dir="auto">
                                                <p style="font-weight: normal;" dir="auto"></p>
                                            </div>
                                        </div>
                                        <div class="conv-time" dir="auto">
                                            <span class="time" dir="auto">26/011/2020 1:33 PM</span>
                                        </div>
                                    </div><div class="conv-block" style="cursor: pointer;" onclick="window.location.href = 'https://travooo.com/chat/100';"><div class="img-wrap" style="max-width: 70px;" dir="auto">

                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" onclick="window.open('/profile/1011')" alt="image" style="width:50px;height:50px;" dir="auto">

                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/chat/100" class="inner-link" dir="auto">Andrey Emilov Alexov </a>
                                            </div>
                                            <div class="msg-txt" dir="auto">
                                                <p style="font-weight: normal;" dir="auto"><img class="smile" src="https://travooo.com/assets2/image/smile-finger-up.svg" alt="smile" style="width:16px" dir="auto"></p>
                                            </div>
                                        </div>
                                        <div class="conv-time" dir="auto">
                                            <span class="time" dir="auto">24/010/2020 6:32 PM</span>
                                        </div>
                                    </div><div class="conv-block" style="cursor: pointer;" onclick="window.location.href = 'https://travooo.com/chat/2055';"><div class="img-wrap" style="max-width: 70px;" dir="auto">

                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/534/1594169545_image.png" onclick="window.open('/profile/534')" alt="image" style="width:50px;height:50px;" dir="auto">

                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/chat/2055" class="inner-link" dir="auto">Aloisa Romani </a>
                                            </div>
                                            <div class="msg-txt" dir="auto">
                                                <p style="font-weight: normal;" dir="auto"></p>
                                            </div>
                                        </div>
                                        <div class="conv-time" dir="auto">
                                            <span class="time" dir="auto">14/010/2020 12:22 PM</span>
                                        </div>
                                    </div><div class="conv-block" style="cursor: pointer;" onclick="window.location.href = 'https://travooo.com/chat/2056';"><div class="img-wrap" style="max-width: 70px;" dir="auto">

                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/538/1594169987_image.png" onclick="window.open('/profile/538')" alt="image" style="width:50px;height:50px;" dir="auto">

                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/chat/2056" class="inner-link" dir="auto">Peter Nagel </a>
                                            </div>
                                            <div class="msg-txt" dir="auto">
                                                <p style="font-weight: normal;" dir="auto"></p>
                                            </div>
                                        </div>
                                        <div class="conv-time" dir="auto">
                                            <span class="time" dir="auto">14/010/2020 12:22 PM</span>
                                        </div>
                                    </div></div>
                            </div>
                            <div class="drop-foot" dir="auto">
                                <a href="https://travooo.com/chat" class="see-all" dir="auto">See All Messages</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown" id="notificationsDropDown" dir="auto">
                        <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="header-notifications" dir="auto">
                            <i class="trav-bell-icon" dir="auto"></i>
                            <div id="notifications-count-unseen" class="counter" style="display: block;" dir="auto">1</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-all-head" aria-labelledby="header-notifications" dir="auto">
                            <div class="drop-ttl" dir="auto">Notifications</div>
                            <div class="drop-inner" style="height: 400px;overflow: auto;" dir="auto">
                                <div class="conversation-inner bordered" id="notificationsRows" dir="auto"><div class="conv-block not-block">
                                        <div class="img-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" alt="image" style="width:50px;height:50px;" dir="auto">
                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/profile/1011" class="inner-link" dir="auto">Andrey Emilov Alexov</a> invited you to participate in their <a href="https://travooo.com/trip/plan/2973" class="inner-link" dir="auto">Trip Plan</a> <span dir="auto">(<a href="" class="inner-link acceptPlanInvitation" data-id="1017" dir="auto">Accept</a> - <a href="" class="inner-link rejectPlanInvitation" data-id="1017" dir="auto">Reject</a>)</span></div>
                                            <div class="time" dir="auto">
                                                <p dir="auto">19 hours ago</p>
                                            </div>
                                        </div>
                                    </div><div class="conv-block not-block">
                                        <div class="img-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" alt="image" style="width:50px;height:50px;" dir="auto">
                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/profile/1011" class="inner-link" dir="auto">Andrey Emilov Alexov</a> Your suggestion was disapproved by plan admin</div>
                                            <div class="time" dir="auto">
                                                <p dir="auto">1 day ago</p>
                                            </div>
                                        </div>
                                    </div><div class="conv-block not-block">
                                        <div class="img-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" alt="image" style="width:50px;height:50px;" dir="auto">
                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/profile/1011" class="inner-link" dir="auto">Andrey Emilov Alexov</a> Your suggestion was approved by plan admin</div>
                                            <div class="time" dir="auto">
                                                <p dir="auto">1 day ago</p>
                                            </div>
                                        </div>
                                    </div><div class="conv-block not-block">
                                        <div class="img-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" alt="image" style="width:50px;height:50px;" dir="auto">
                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/profile/1011" class="inner-link" dir="auto">Andrey Emilov Alexov</a> Your suggestion was disapproved by plan admin</div>
                                            <div class="time" dir="auto">
                                                <p dir="auto">1 day ago</p>
                                            </div>
                                        </div>
                                    </div><div class="conv-block not-block">
                                        <div class="img-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" alt="image" style="width:50px;height:50px;" dir="auto">
                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/profile/1011" class="inner-link" dir="auto">Andrey Emilov Alexov</a> Your suggestion was disapproved by plan admin</div>
                                            <div class="time" dir="auto">
                                                <p dir="auto">1 day ago</p>
                                            </div>
                                        </div>
                                    </div><div class="conv-block not-block">
                                        <div class="img-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" alt="image" style="width:50px;height:50px;" dir="auto">
                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/profile/1011" class="inner-link" dir="auto">Andrey Emilov Alexov</a> Your suggestion was approved by plan admin</div>
                                            <div class="time" dir="auto">
                                                <p dir="auto">1 day ago</p>
                                            </div>
                                        </div>
                                    </div><div class="conv-block not-block">
                                        <div class="img-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" alt="image" style="width:50px;height:50px;" dir="auto">
                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/profile/1011" class="inner-link" dir="auto">Andrey Emilov Alexov</a> accepted your inivtation</div>
                                            <div class="time" dir="auto">
                                                <p dir="auto">1 day ago</p>
                                            </div>
                                        </div>
                                    </div><div class="conv-block not-block">
                                        <div class="img-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" alt="image" style="width:50px;height:50px;" dir="auto">
                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/profile/1011" class="inner-link" dir="auto">Andrey Emilov Alexov</a> made a suggestion in <a href="https://travooo.com/trip/plan/2374" class="inner-link" dir="auto">Test Memory trip</a> plan</div>
                                            <div class="time" dir="auto">
                                                <p dir="auto">3 days ago</p>
                                            </div>
                                        </div>
                                    </div><div class="conv-block not-block">
                                        <div class="img-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" alt="image" style="width:50px;height:50px;" dir="auto">
                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/profile/1011" class="inner-link" dir="auto">Andrey Emilov Alexov</a> made a suggestion in <a href="https://travooo.com/trip/plan/2374" class="inner-link" dir="auto">Test Memory trip</a> plan</div>
                                            <div class="time" dir="auto">
                                                <p dir="auto">3 days ago</p>
                                            </div>
                                        </div>
                                    </div><div class="conv-block not-block">
                                        <div class="img-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1011/1594734805_image.png" alt="image" style="width:50px;height:50px;" dir="auto">
                                        </div>
                                        <div class="conv-txt" dir="auto">
                                            <div class="name" dir="auto">
                                                <a href="https://travooo.com/profile/1011" class="inner-link" dir="auto">Andrey Emilov Alexov</a> Your suggestion was approved by plan admin</div>
                                            <div class="time" dir="auto">
                                                <p dir="auto">3 days ago</p>
                                            </div>
                                        </div>
                                    </div></div>
                            </div>
                            <div class="drop-foot" dir="auto">
                                <a href="#" class="see-all" data-toggle="modal" data-target="#seeAllModal" dir="auto">See All</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item" dir="auto">
                        <a class="nav-link" href="#" dir="auto">
                            <i class="trav-comment-question-icon" dir="auto"></i>
                        </a>
                    </li>
                    <li class="nav-item" dir="auto">
                        <a class="nav-link plan-trip-icon" href="https://travooo.com/trip/plan/0" dir="auto">
                            <i class="trav-trip-plan-loc-icon" dir="auto"></i>
                            <span dir="auto">Plan trip</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown" dir="auto">
                        <a class="profile-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" dir="auto">
                            <span dir="auto">Ivan Turlakov</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-use-menu" dir="auto">
                            <!-- New element -->
                            <div class="img-block">
                                <img src="https://s3.amazonaws.com/travooo-images2/users/cover/831/1597146015WP_20150803_004.jpg" width="100%" alt="" dir="auto">
                                <div class="profile-details">
                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" width="60" alt="" dir="auto">
                                    <div>
                                        <h4>Ivan Turlakov</h4>
                                        <a href="#">View your profile</a>
                                    </div>
                                </div>
                            </div>
                            <!-- New element END -->
                            <a class="dropdown-item" href="https://travooo.com/profile" dir="auto">
                                <div class="drop-txt" dir="auto">
                                    <h5>Expert Dashboard</h5>
                                    <p dir="auto">Insights & Affiliation</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="https://travooo.com/profile" dir="auto">
                                <div class="drop-txt" dir="auto">
                                    <h5>Help Center</h5>
                                    <p dir="auto">Learn how to use Travooo</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="https://travooo.com/dashboard" dir="auto">
                                <div class="drop-txt pb-3" dir="auto">
                                    <h5>Settings</h5>
                                    <p dir="auto">Account & Personal Settings</p>
                                </div>
                            </a>
                            <a class="dropdown-item log-out" href="https://travooo.com/settings" dir="auto">
                                <div class="drop-txt" dir="auto">
                                    <h5>Log Out</h5>
                                </div>
                            </a>
                            <a class="dropdown-item switch" href="#" dir="auto" data-toggle="modal" data-target="#becomeExpertPopup">
                                <div class="drop-txt" dir="auto">
                                    <h4 dir="auto">Switch to <b>Expert Account</b> <span class="exp-icon" dir="auto">EXP</span></h4>
                                </div>
                            </a>
                            <!-- New element -->
                            <div class="aside-footer" dir="auto">
                                <ul class="aside-foot-menu" dir="auto">
                                    <li dir="auto"><a href="{{route('page.privacy_policy')}}" dir="auto">Privacy</a></li>
                                    <li dir="auto"><a href="{{route('page.terms_of_service')}}" dir="auto">Terms</a></li>
                                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Advertising</a></li>
                                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Cookies</a></li>
                                </ul>
                                <p class="copyright" dir="auto">Travooo © 2017 - 2020</p>
                            </div>
                            <!-- New element END -->
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
@include('site.design.partials._becomeExpertPopup')
<style>
    .over-list .name{
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: 200px;
    }
</style>
<div class="content-wrap">
    <div class="dashboard-wrap">
        <div class="top-bar">
            <div class="container-fluid">
                <div class="top-bar-inner">
                    <div class="ttl">Dashboard</div>
                    <ul class="nav nav-tabs bar-list" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="insightsPage" data-toggle="tab" href="" role="tab">Insights</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="affilationPage" href="" role="tab">Afillation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="" role="tab">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="" role="tab">Inbox <span class="counter">2</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="" role="tab">Notifications <span class="counter">2</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="" role="tab">Settings</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $(document).on('click', '.top-bar-inner .nav-link', function (e) {
                    e.preventDefault();
                    $('.top-bar-inner .nav-link').removeClass('active');
                    $(this).addClass('active');

                    if ($(this).attr('id') == 'affilationPage') {
                        $('.nav-item.insights').hide();
                        $('.nav-item.affilation').show();
                        $('.nav-item.insights .nav-link').removeClass('active');
                        $('.nav-item.affilation').first().find('.nav-link').trigger('click');
                    } else {
                        $('.nav-item.affilation').hide();
                        $('.nav-item.insights').show();
                        $('.nav-item.affilation .nav-link').removeClass('active');
                        $('.nav-item.insights').first().find('.nav-link').trigger('click');
                    }
                });
            })
        </script>
        <div class="container-fluid">
            <!-- Tab panes -->
            <div class="tab-content dash-tab-content">
                <div class="tab-pane first-layout active" id="dashInsight" role="tabpanel">
                    <ul class="nav nav-tabs inside-list dsh-inside-list" role="tablist">
                        <!-- Insights nav -->
                        <li class="nav-item insights">
                            <a class="nav-link active" data-toggle="tab" href="#overview"
                               role="tab">@lang('dashboard.overview')</a>
                        </li>
                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#engagement-demo"
                               role="tab">@lang('dashboard.engagement')</a>
                        </li>

                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#ranking"
                               role="tab">Ranking</a>
                        </li>

                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#interaction" role="tab">Interaction Source</a>
                        </li>
                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#posts" role="tab">Posts</a>
                        </li>
                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#reports" role="tab">Reports</a>
                        </li>

                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#followers"
                               role="tab">@lang('dashboard.followers_unfollowers')</a>
                        </li>
                        <li class="nav-item insights">
                            <a class="nav-link" data-toggle="tab" href="#globalImpact"
                               role="tab">Global Impact</a>
                        </li>
                        <!-- Affilation nav -->
                        <li class="nav-item affilation">
                            <a class="nav-link" data-toggle="tab" href="#dashAffiliation" role="tab">Overview</a>
                        </li>
                        <li class="nav-item affilation">
                            <a class="nav-link disabled" data-toggle="tab" href=""
                               role="tab">Earnings</a>
                        </li>
                        <li class="nav-item affilation">
                            <a class="nav-link disabled" data-toggle="tab" href=""
                               role="tab">Source</a>
                        </li>
                        <li class="nav-item affilation">
                            <a class="nav-link disabled" data-toggle="tab" href=""
                               role="tab">Reports</a>
                        </li>
                        <li class="nav-item affilation">
                            <a class="nav-link disabled" data-toggle="tab" href=""
                               role="tab">Payouts</a>
                        </li>
                        <li class="nav-item affilation">
                            <a class="nav-link disabled" data-toggle="tab" href=""
                               role="tab">Settings</a>
                        </li>
                    </ul>
                    <div class="tab-content dash-inside-content">
                        <div class="tab-pane second-layout" id="dashAffiliation" role="tabpanel">
                            <div class="coming-soon">
                                <h4>Coming Soon...</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit aenean eget pharetra nisi quisque ultricies orci sed mollis varius</p>
                            </div>
                            <img src="{{asset('assets2/image/placeholders/dashboard-affilation-placeholder.png')}}" alt="Affilation placeholder">
                        </div>
                        <div class="tab-pane second-layout active" id="overview" role="tabpanel">
                            <div class="overview-block">
                            </div>
                            <div class="dash-content-loader overview-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/overview-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <!-- Just for Dig-popup demonstration -->
                        <div class="tab-pane second-layout" id="engagement-demo" role="tabpanel">
                            <div class="engagement-block">
                                <div class="post-block post-dashboard-inner" dir="auto">
                                    <div class="block-head" dir="auto">
                                        <h4 class="ttl light" dir="auto">Total Views as of Today:<span>0</span></h4>
                                        <div class="sort-by-select" dir="auto">
                                            <label dir="auto">Last</label>
                                            <div class="sort-select-wrap" dir="auto">
                                                <div class="sort-select-wrapper"><select class="sort-select view-engagement" placeholder="7 days" dir="auto" style="display: none;">
                                                        <option value="30" dir="auto">30 Days</option>
                                                        <option value="15" dir="auto">15 Days</option>
                                                        <option value="7" dir="auto">7 Days</option>
                                                    </select><div class="sort-select view-engagement" data-type="undefined"><span class="sort-select-trigger" dir="auto">7 days</span><div class="sort-options" dir="auto"><span class="sort-option undefined" data-value="30" dir="auto">30 Days</span><span class="sort-option undefined" data-value="15" dir="auto">15 Days</span><span class="sort-option undefined" data-value="7" dir="auto">7 Days</span></div></div></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-sidebar-inner" dir="auto">
                                        <div class="dash-main engagement-chart-wrapper" dir="auto"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand" dir="auto"><div class="" dir="auto"></div></div><div class="chartjs-size-monitor-shrink" dir="auto"><div class="" dir="auto"></div></div></div>
                                            <canvas id="totalviewsasofChart" class="view-engagement-canvas chartjs-render-monitor" data-placeholder-title="Views" data-engagement-label="[&quot;28&quot;,&quot;29&quot;,&quot;30&quot;,&quot;01&quot;,&quot;02&quot;,&quot;03&quot;,&quot;04&quot;]" data-engagement-daily="[1,0,0,1,0,16,0]" data-engagement-total="[1042,1041,1041,1042,1041,1057,1041]" dir="auto" style="display: block; width: 608px; height: 300px;" width="608" height="300"></canvas>
                                        </div>
                                        <div class="dash-sidebar" dir="auto">
                                            <h4 class="side-ttl" dir="auto">Benchmark</h4>
                                            <div class="side-txt" dir="auto">
                                                <p dir="auto">Compare your average preformance over time</p>
                                            </div>
                                            <div class="side-txt border-disabled" dir="auto">
                                                <p dir="auto"><b dir="auto">Learn what sources lead to your views?</b></p>
                                            </div>
                                            <a href="#" class="btn btn-light-grey btn-bordered" data-toggle="modal" data-target="#digPopup">Dig deeper</a>
                                            @include('site.design.partials._digPopup')
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        </div>
                        <div class="tab-pane second-layout" id="ranking" role="tabpanel">
                            <div class="ranking-block">
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head" dir="auto">
                                        <h4 class="ttl">Ranking</h4>
                                    </div>
                                    <div class="dash-ranking-grow-info">
                                        <h3>Grow with Travooo</h3>
                                        <div class="exp-range">
                                            <span class="exp-icon">EXP</span>
                                            <span class="exp-icon bronze">EXP</span>
                                            <span class="exp-icon silver">EXP</span>
                                            <span class="exp-icon gold">EXP</span>
                                        </div>
                                        <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent euismod rutrum egestas. Suspendisse potenti. Etiam vel tristique purus, nec porttitor nulla. Curabitur venenatis urna eu <a href="">posuere dignissim</a>.</div>
                                        <div class="exp-progress">
                                            <span class="exp-icon big">EXP</span>
                                            <div class="exp-progress-bar">
                                                <div class="percentage">70%</div>
                                                <div class="progress">
                                                    <div class="progress-bar" style="width: 70.0%" role="progressbar" aria-valuenow="70.0" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <span class="exp-icon big bronze">EXP</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-block post-dashboard-inner">
                                    <div class="block-head">
                                        <h4 class="ttl">Milestones</h4>
                                        <!-- Just for popups demonstration -->
                                        <a href="#" data-toggle="modal" data-target="#milestonePopup">Milestone popup</a>
                                        @include('site.design.partials._milestonePopup')
                                        <!-- Just for popups demonstration -->
                                    </div>
                                    <div class="dash-ranking-milestones">
                                        <div class="dash-ranking-milestone-item">
                                            <div class="title">
                                                <div>
                                                    <i class="fas fa-check-circle"></i>
                                                    Upload a profile picture
                                                </div>
                                            </div>
                                            <div class="description">
                                                <div class="text">Aliquam erat volutpat fusce in est varius  consectetur mauris rhoncus vestibulum quis tincidunt odio.</div>
                                                <div class="value picture circle"></div>
                                            </div>
                                        </div>
                                        <div class="dash-ranking-milestone-item">
                                            <div class="title">
                                                <div>
                                                    <i class="fas fa-check-circle"></i>
                                                    Upload a cover picture
                                                </div>
                                            </div>
                                            <div class="description">
                                                <div class="text">Aliquam erat volutpat fusce in est varius  consectetur mauris rhoncus vestibulum quis tincidunt odio.</div>
                                                <div class="value picture"></div>
                                            </div>
                                        </div>
                                        <div class="dash-ranking-milestone-item">
                                            <div class="title">
                                                <div>
                                                    <i class="fas fa-check-circle success"></i>
                                                    Upload a profile picture
                                                </div>
                                                <div class="progress-bar" style="width: 100.0%" role="progressbar" aria-valuenow="100.0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="description">
                                                <div class="text">Aliquam erat volutpat fusce in est varius  consectetur mauris rhoncus vestibulum quis tincidunt odio.</div>
                                                <div class="value picture circle">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dash-ranking-milestone-item">
                                            <div class="title">
                                                <div>
                                                    <i class="fas fa-check-circle success"></i>
                                                    Upload a cover picture
                                                </div>
                                                <div class="progress-bar" style="width: 100.0%" role="progressbar" aria-valuenow="100.0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="description">
                                                <div class="text">Aliquam erat volutpat fusce in est varius  consectetur mauris rhoncus vestibulum quis tincidunt odio.</div>
                                                <div class="value picture">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/users/cover/831/1597146015WP_20150803_004.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="separator"></div>
                                        <!-- Congrats -->
                                        <div class="dash-ranking-milestone-item congrats">
                                            <h4>Congratulations!</h4>
                                            <p>Normally every new expert will need to reach <b>1,000 followers</b> and <b>10,000 interactions</b> to move to the next level, but we have a special discount for you today, you only need to reach <b>50%</b> of the requered points.</p>
                                        </div>
                                        <!-- Congrats END -->
                                        <div class="dash-ranking-milestone-item">
                                            <div class="title">
                                                <div>
                                                    <i class="fas fa-check-circle"></i>
                                                    Reach 1k followers
                                                </div>
                                                <span class="tip">Normally 1,000</span>
                                                <div class="progress-bar" style="width: 70.0%" role="progressbar" aria-valuenow="70.0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="description">
                                                <div class="text">Aliquam erat volutpat fusce in est varius  consectetur mauris rhoncus vestibulum quis tincidunt odio.</div>
                                                <div class="value">
                                                    <b>700</b> Followers
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dash-ranking-milestone-item">
                                            <div class="title">
                                                <div>
                                                    <i class="fas fa-check-circle success"></i>
                                                    Reach 5k interactions
                                                </div>
                                                <span class="tip">Normally 5,000</span>
                                                <div class="progress-bar" style="width: 100.0%" role="progressbar" aria-valuenow="100.0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="description">
                                                <div class="text">Aliquam erat volutpat fusce in est varius  consectetur mauris rhoncus vestibulum quis tincidunt odio.</div>
                                                <div class="value">
                                                    <b>5,000</b> Interactions
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dash-ranking-milestones-bottom">
                                        <div class="text">
                                            <h4>What’s next?</h4>
                                            <p>Click the button bellow when you reach all your milestones to apply for your next badge.</p>
                                        </div>
                                        <button class="btn btn-light-grey btn-bordered disabled">Apply for the next badge</button>
                                    </div>
                                </div>
                            </div>   
                        </div>
                        <div class="tab-pane second-layout" id="interaction" role="tabpanel">
                            <div class="interaction-block"></div>
                            <div class="dash-content-loader interaction-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/interaction-source-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="posts" role="tabpanel">
                            <div class="posts-block"></div>
                            <div class="dash-content-loader posts-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/posts-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="reports" role="tabpanel">
                            <div class="reports-block"></div>
                            <div class="dash-content-loader reports-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/reports-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="followers" role="tabpanel">
                            <div class="followers-block"></div>
                            <div class="dash-content-loader followers-loader">
                                <img class="dash-loader-img" src="{{asset('assets2/image/dashboard/followers-bg.png')}}">
                                <div class="dash-loader">
                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                </div>
                            </div>    
                        </div>
                        <div class="tab-pane second-layout" id="globalImpact" role="tabpanel">
                            <!-- Updated elements -->
                            <!-- <div class="globalImpact-block"> --><div>
                                <div class="post-block post-dashboard-inner" dir="auto">
                                    <div class="block-head" dir="auto">
                                        <h4 class="ttl" dir="auto">Level Up</h4>
                                    </div>
                                    <div class="dash-global-badges-info" dir="auto">
                                        <div class="dash-global-badges-info-next" dir="auto">
                                            <h3 dir="auto">Your next Badge</h3>
                                            <div class="badge-details" dir="auto">
                                                <!-- Updated element -->
                                                <div class="icon-block">
                                                    <span class="exp-icon gold">EXP</span>
                                                </div>
                                                <!-- Updated element END -->
                                                <div dir="auto">
                                                    <h5 class="badge-details-ttl" dir="auto">Golden Expert <span dir="auto">#8 <i>in</i> France</span></h5>
                                                    <div class="users-whith-badge" dir="auto">
                                                        <div class="users-whith-badge-list" dir="auto">
                                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/2245/1606984448_image.png" alt="User Avatar" dir="auto">
                                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/2242/1606740004_image.png" alt="User Avatar" dir="auto">
                                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/2239/1606641673_image.png" alt="User Avatar" dir="auto">
                                                        </div>
                                                        <div class="users-whith-badge-ttl" dir="auto">
                                                            87 users like you have this badge
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress badge-progress" dir="auto">
                                                <div class="progress-bar" style="width: 70%" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" dir="auto"></div>
                                            </div>
                                            <div class="points-to-next" dir="auto">
                                                <b>30%</b> left to your next level
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Table -->
                            <div class="post-block post-dashboard-inner leaderboard" style="max-width: 870px;" dir="auto">
                                <div class="block-head with-tab-list" dir="auto">
                                    <h4 class="ttl" dir="auto">Leaderboard</h4>
                                </div>
                                <div class="dash-info-inner" dir="auto">
                                    <h5 class="ttl" dir="auto">Area of expertise</h5>
                                    <ul class="nav nav-tabs bar-list dash-global-area-filter-list" role="tablist" dir="auto" style="overflow: auto;">
                                        <li class="nav-item dash-global-area-filter-item 276" dir="auto">
                                            <a class="nav-link dash-global-area-filter-link" data-filter="276" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="true">United Kingdom</a>
                                        </li>
                                        <li class="nav-item dash-global-area-filter-item 98" dir="auto">
                                            <a class="nav-link dash-global-area-filter-link" data-filter="98" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="true">India</a>
                                        </li>
                                        <li class="nav-item dash-global-area-filter-item 80" dir="auto">
                                            <a class="nav-link dash-global-area-filter-link" data-filter="80" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="true">Germany</a>
                                        </li>
                                        <li class="nav-item dash-global-area-filter-item 148" dir="auto">
                                            <a class="nav-link dash-global-area-filter-link" data-filter="148" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="true">Netherlands</a>
                                        </li>
                                        <li class="nav-item dash-global-area-filter-item 14" dir="auto">
                                            <a class="nav-link dash-global-area-filter-link" data-filter="14" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="true">Austria</a>
                                        </li>
                                        <li class="nav-item dash-global-area-filter-item 21" dir="auto">
                                            <a class="nav-link dash-global-area-filter-link" data-filter="21" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="true">Belgium</a>
                                        </li>
                                        <li class="nav-item dash-global-area-filter-item 227" dir="auto">
                                            <a class="nav-link dash-global-area-filter-link" data-filter="227" data-toggle="tab" href="#" role="tab" dir="auto" aria-selected="true">United States</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="dash-inner" dir="auto">
                                    <div class="table-responsive" dir="auto">
                                        <table class="table tbl-dashboard" dir="auto">
                                            <tbody dir="auto"><tr dir="auto">
                                                    <th class="rank" dir="auto">Rank</th>
                                                    <th class="post" dir="auto">Name</th>
                                                    <th class="followers" dir="auto">Followers</th>
                                                    <th class="interactions" dir="auto">Interactions</th>
                                                </tr>
                                            </tbody><tbody dir="auto">
                                                <tr class="dash-global-table-row all 276" dir="auto">
                                                    <td dir="auto">
                                                        <div class="dash-global-table-rank" dir="auto">
                                                            <span class="rank" dir="auto">#1</span>
                                                            <span class="rank-move up" dir="auto">
                                                                <i class="trav-arrow-up-icon" dir="auto"></i>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td dir="auto">
                                                        <span class="tbl-leader-info" dir="auto">
                                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/989/1594181330_image.png" alt="User Avatar" dir="auto">
                                                            <span class="details" dir="auto">
                                                                <a href="https://travooo.com/profile/989" target="_blank" class="name" dir="auto">Harry Doyle</a>
                                                                <span class="expertise" dir="auto">
                                                                    <span dir="auto">from</span> <a href="https://travooo.com/country/276" target="_blank" dir="auto">United Kingdom</a>
                                                                </span>
                                                            </span>
                                                        </span>
                                                    </td>
                                                    <td dir="auto">
                                                        <div class="tbl-progress" dir="auto">
                                                            <div class="progress-label" dir="auto">100050</div>
                                                            <div class="progress" dir="auto">
                                                                <div class="progress-bar primary" role="progressbar" aria-valuenow="10.00499" aria-valuemin="0" aria-valuemax="100" style="width: 10.00499%;" dir="auto"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td dir="auto">
                                                        <div class="tbl-progress" dir="auto">
                                                            <div class="progress-label" dir="auto">100050</div>
                                                            <div class="progress" dir="auto">
                                                                <div class="progress-bar orange" role="progressbar" aria-valuenow="20.00499" aria-valuemin="0" aria-valuemax="100" style="width: 20.00499%;" dir="auto"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="dash-global-table-row all 98" dir="auto">
                                                    <td dir="auto">
                                                        <div class="dash-global-table-rank" dir="auto">
                                                            <span class="rank" dir="auto">#1</span>
                                                            <span class="rank-move up" dir="auto">
                                                                <i class="trav-arrow-up-icon" dir="auto"></i>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td dir="auto">
                                                        <span class="tbl-leader-info" dir="auto">
                                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/1818/1598938851_image.png" alt="User Avatar" dir="auto">
                                                            <span class="details" dir="auto">
                                                                <a href="https://travooo.com/profile/1818" target="_blank" class="name" dir="auto">Bumati Kamath</a>
                                                                <span class="expertise" dir="auto">
                                                                    <span dir="auto">from</span> <a href="https://travooo.com/country/98" target="_blank" dir="auto">India</a>
                                                                </span>
                                                            </span>
                                                        </span>
                                                    </td>
                                                    <td dir="auto">
                                                        <div class="tbl-progress" dir="auto">
                                                            <div class="progress-label" dir="auto">10350</div>
                                                            <div class="progress" dir="auto">
                                                                <div class="progress-bar primary" role="progressbar" aria-valuenow="30.35" aria-valuemin="0" aria-valuemax="100" style="width: 30.35%;" dir="auto"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td dir="auto">
                                                        <div class="tbl-progress" dir="auto">
                                                            <div class="progress-label" dir="auto">100050</div>
                                                            <div class="progress" dir="auto">
                                                                <div class="progress-bar orange" role="progressbar" aria-valuenow="40.00499" aria-valuemin="0" aria-valuemax="100" style="width: 40.00499%;" dir="auto"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="dash-global-table-row all 80" dir="auto">
                                                    <td dir="auto">
                                                        <div class="dash-global-table-rank" dir="auto">
                                                            <span class="rank" dir="auto">#1</span>
                                                            <span class="rank-move up" dir="auto">
                                                                <i class="trav-arrow-up-icon" dir="auto"></i>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td dir="auto">
                                                        <span class="tbl-leader-info" dir="auto">
                                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/997/1594176594_image.png" alt="User Avatar" dir="auto">
                                                            <span class="details" dir="auto">
                                                                <a href="https://travooo.com/profile/997" target="_blank" class="name" dir="auto">Caroline Franck</a>
                                                                <span class="expertise" dir="auto">
                                                                    love
                                                                    <span dir="auto">from</span> <a href="https://travooo.com/country/80" target="_blank" dir="auto">Germany</a>
                                                                </span>
                                                            </span>
                                                        </span>
                                                    </td>
                                                    <td dir="auto">
                                                        <div class="tbl-progress" dir="auto">
                                                            <div class="progress-label" dir="auto">10174</div>
                                                            <div class="progress" dir="auto">
                                                                <div class="progress-bar primary" role="progressbar" aria-valuenow="50.17375" aria-valuemin="0" aria-valuemax="100" style="width: 50.17375%;" dir="auto"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td dir="auto">
                                                        <div class="tbl-progress" dir="auto">
                                                            <div class="progress-label" dir="auto">100050</div>
                                                            <div class="progress" dir="auto">
                                                                <div class="progress-bar orange" role="progressbar" aria-valuenow="60.00499" aria-valuemin="0" aria-valuemax="100" style="width: 60.00499%;" dir="auto"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="dash-global-table-row all 148" dir="auto">
                                                    <td dir="auto">
                                                        <div class="dash-global-table-rank" dir="auto">
                                                            <span class="rank" dir="auto">#1</span>
                                                            <span class="rank-move up" dir="auto">
                                                                <i class="trav-arrow-up-icon" dir="auto"></i>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td dir="auto">
                                                        <span class="tbl-leader-info" dir="auto">
                                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/912/1594179247_image.png" alt="User Avatar" dir="auto">
                                                            <span class="details" dir="auto">
                                                                <a href="https://travooo.com/profile/912" target="_blank" class="name" dir="auto">Detlev Hersch</a>
                                                                <span class="expertise" dir="auto">
                                                                    <span dir="auto">from</span> <a href="https://travooo.com/country/148" target="_blank" dir="auto">Netherlands</a>
                                                                </span>
                                                            </span>
                                                        </span>
                                                    </td>
                                                    <td dir="auto">
                                                        <div class="tbl-progress" dir="auto">
                                                            <div class="progress-label" dir="auto">10130</div>
                                                            <div class="progress" dir="auto">
                                                                <div class="progress-bar primary" role="progressbar" aria-valuenow="70.1302" aria-valuemin="0" aria-valuemax="100" style="width: 70.1302%;" dir="auto"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td dir="auto">
                                                        <div class="tbl-progress" dir="auto">
                                                            <div class="progress-label" dir="auto">100050</div>
                                                            <div class="progress" dir="auto">
                                                                <div class="progress-bar orange" role="progressbar" aria-valuenow="80.00499" aria-valuemin="0" aria-valuemax="100" style="width: 80.00499%;" dir="auto"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- Updated elements END -->
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="dashSetting" role="tabpanel">@lang('dashboard.pane') 4</div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('before_scripts')
@include('site.dashboard._modals')
@endsection

@section('after_scripts')
@include('site.dashboard._scripts')
@endsection