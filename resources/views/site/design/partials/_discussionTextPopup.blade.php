<!-- Discussion text popup -->
<div class="modal white-style" data-backdrop="false" id="discussionTextPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="post-type-modal-inner text-post">
        <!-- Primary post block - text -->
        <div class="post-block discussion" >
                            <div class="post-top-info-layer" >
                            <div class="post-top-info-wrap" >
                            <div class="post-top-avatar-wrap" >
                            <a class="post-name-link" href="https://travooo.com/profile/831" >
                            <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" >
                            </a>
                            </div>
                            <div class="post-top-info-txt" >
                            <div class="post-top-name" >
                            <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                            <span class="exp-icon" >EXP</span>
                            </div>
                            <div class="post-info" >
                                Asked for tips about <a href="https://travooo.com/place/547483" class="link-place" dir="auto">New York</a>
                                15 July at 4:35am <i class="trav-globe-icon" dir="auto"></i>
                            </div>
                            </div>
                            </div>
                            <!-- New elements -->
                            <div class="post-top-info-action" >
                                <div class="dropdown" >
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-angle-down" ></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap" >
                                            <i class="trav-user-plus-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Unfollow User</b></p>
                                            <p >Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap" >
                                            <i class="trav-share-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Share</b></p>
                                            <p >Spread the word</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap" >
                                            <i class="trav-link" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Copy Link</b></p>
                                            <p >Paste the link anyywhere you want</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#sendToFriendModal">
                                        <span class="icon-wrap" >
                                            <i class="trav-share-on-travo-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Send to a Friend</b></p>
                                            <p >Share with your friends</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap" >
                                            <i class="trav-heart-icon" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Add to Favorites</b></p>
                                            <p >Save it for later</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                                        <span class="icon-wrap" >
                                            <i class="trav-flag-icon-o" ></i>
                                        </span>
                                        <div class="drop-txt" >
                                            <p ><b >Report</b></p>
                                            <p >Help us understand</p>
                                        </div>
                                    </a>
                                </div>
                                </div>
                                </div>
                                <!-- New elements END -->
                            </div>
                            <!-- Discussion wrapper -->
                            <div class="discussion-wrapper">
                            <!-- New element -->
                            <div class="post-txt-wrap">
                                <h3>Any tips before I go to <a href="">New York City</a> ?</h3>
                                <p>In gravida ullamcorper metus, quis blandit odio posuere sed. Donec dignissim mollis suscipit. Nullam magna tellus, volutpat sodales rhoncus malesuada, scelerisque ut risus. Praesent pharetra sem id felis lobortis vehicula. Vivamus egestas est dolor. Nullam a lorem ac lorem dignissim fermentum ut eget lacus sed dictum ornare magna, vestibulum placerat tellus rutrum eu.</p>
                                <p>Vivamus egestas est dolor. Nullam a lorem ac lorem dignissim fermentum ut eget lacus sed dictum ornare magna, vestibulum placerat tellus rutrum eu.</p>
                            </div>
                            <div class="disc-media-block" dir="auto">
                            <a href="https://s3.amazonaws.com/travooo-images2/discussion-photo/2116_0_1604906705_St._Nicholas'_Collegiate_Church_Interior.jpg" data-lightbox="ask__media" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/2116_0_1604906705_St._Nicholas'_Collegiate_Church_Interior.jpg" alt="image" class="disc-media-wrap" dir="auto">
                            </a>
                            </div>
                            <div class="post-modal-content-foot" dir="auto">
                            <div class="post-modal-foot-list" dir="auto">
                            <span dir="auto">Yesterday at <b dir="auto"><span dir="auto">10:25 am</span></b></span>
                            <span class="dot" dir="auto"> · </span>
                            <span dir="auto">Views <b dir="auto"><span class="3712-view-count" dir="auto">3</span></b></span>
                            <span class="dot" dir="auto"> · </span>
                            <span dir="auto">Tagged <b dir="auto">2</b></span>
                            <div class="disc-experts-block" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/2115/1604865056_image.png" data-toggle="bs-tooltip" title="" dir="auto" data-original-title="User Name">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/2114/1604864555_image.png" data-toggle="bs-tooltip" title="" dir="auto" data-original-title="User Name">
                            </div>
                            </div>
                            <div class="disc-topic-text" dir="auto">
                            Church, Modernfacts
                            </div>
                            </div>
                            <!-- New element END -->
                            </div>
                            <!-- Discussion text wrapper END -->
                            <div class="post-footer-info" >
                            <!-- Updated elements -->
                            <div class="post-foot-block ml-auto" >
                            <span class="post_share_button" id="601451" >
                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                            <i class="trav-share-icon" ></i>
                            </a>
                            </span>
                            <span id="post_share_count_601451" ><a href="#" data-tab="shares601451" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                            </div>
                            </div>
                            <div class="post-comment-wrapper" id="following601451" >
                            </div>
                            <!-- Copied from home page -->
                            <div class="post-comment-layer" data-content="comments622665" style="display: block;" dir="auto">
                            <div class="post-comment-top-info" dir="auto">
                            <ul class="comment-filter" dir="auto">
                            <li onclick="commentSort('Top', this, $(this).closest('.post-block'))" dir="auto">Top</li>
                            <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))" dir="auto">New</li>
                            </ul>
                            <div class="comm-count-info" dir="auto">
                            <strong dir="auto">2</strong> / <span class="622665-comments-count" dir="auto">2</span>
                            </div>
                            </div>
                            <div class="post-comment-wrapper sortBody" id="comments622665" dir="auto" travooo-comment-rows="2" travooo-current-page="1">
                            <div topsort="0" newsort="1601563217" dir="auto" class="displayed" style="">
                            <div class="post-comment-row news-feed-comment" id="commentRow13235" dir="auto">
                            <div class="post-com-avatar-wrap" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" dir="auto">
                            </div>
                            <div class="post-comment-text" dir="auto">
                            <div class="post-com-name-layer" dir="auto">
                            <a href="#" class="comment-name" dir="auto">Eula Cooper</a>
                            <span class="timestamp">said &nbsp;&middot;&nbsp; <span>5 hours ago</span></span>
                            <div class="post-com-top-action" dir="auto">
                            <div class="dropdown " dir="auto" style="display: block;">
                            <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                            <i class="trav-angle-down" dir="auto"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment" dir="auto">
                            <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="13235" data-post="622665" dir="auto">
                            <span class="icon-wrap" dir="auto">
                            <i class="trav-pencil" aria-hidden="true" dir="auto"></i>
                            </span>
                            <div class="drop-txt comment-edit__drop-text" dir="auto">
                            <p dir="auto"><b dir="auto">Edit</b></p>
                            </div>
                            </a>
                            <a href="javascript:;" class="dropdown-item postCommentDelete" id="13235" data-post="622665" data-type="1" dir="auto">
                            <span class="icon-wrap" dir="auto">
                            <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;" dir="auto">
                            </span>
                            <div class="drop-txt comment-delete__drop-text" dir="auto">
                            <p dir="auto"><b dir="auto">Delete</b></p>
                            </div>
                            </a>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="comment-txt comment-text-13235" dir="auto">
                            <p dir="auto">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos accusamus deserunt error harum voluptatum dicta minima aspernatur fugiat temporibus ut natus aliquam non repudiandae iusto impedit accusantium, odio molestias sunt.
                            </p>
                            <form class="commentEditForm13235 comment-edit-form d-none" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto"><input type="hidden" data-id="pair13235" name="pair" value="5f770bda7bc30" dir="auto">
                            <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0" dir="auto">
                            <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                            <textarea name="text" data-id="text13235" class="textarea-customize post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="Write a comment" dir="auto"></textarea>
                            </div>
                            <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                            <div class="post-alloptions" dir="auto">
                            <ul class="create-link-list" dir="auto">
                            <li class="post-options" dir="auto">
                            <input type="file" name="file[]" class="commenteditfile" id="commenteditfile13235" style="display:none" multiple="" dir="auto">
                            <i class="fa fa-camera click-target" data-target="commenteditfile13235" dir="auto"></i>
                            </li>
                            </ul>
                            </div>
                            <div class="comment-edit-action" dir="auto">
                            <a href="javascript:;" class="edit-cancel-link" data-comment_id="13235" dir="auto">Cancel</a>
                            <a href="javascript:;" class="edit-post-link" data-comment_id="13235" dir="auto">Post</a>
                            </div>
                            </div>
                            <div class="medias p-media b-whitesmoke" dir="auto">
                            <div class="img-wrap-newsfeed" dir="auto">
                            <div dir="auto">
                            <img class="thumb" src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" dir="auto">
                            </div>
                            <span class="close remove-media-file" data-media_id="14592485" dir="auto">
                            <span dir="auto">×</span>
                            </span></div>
                            </div>
                            </div>
                            <input type="hidden" name="post_id" value="622665" dir="auto">
                            <input type="hidden" name="comment_id" value="13235" dir="auto">
                            <input type="hidden" name="comment_type" value="1" dir="auto">
                            <button type="submit" class="d-none" dir="auto"></button>
                            </form>
                            </div>
                            <div class="post-image-container" dir="auto"></div>
                            <div class="comment-bottom-info" dir="auto">
                            <div class="comment-info-content" dir="auto">
                                <!-- New element -->
                                <div class="tips-footer updownvote" id="9953" dir="auto">
                                    <a href="#" class="upvote-link up" id="9953" dir="auto">
                                        <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-up" dir="auto"></i></span>
                                    </a>
                                    <span dir="auto"><b class="upvote-count" dir="auto">13</b> Upvotes</span>
                                </div>
                                <!-- New element -->
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="post-add-comment-block" id="replyForm13235" style="padding-top:0px;padding-left: 59px;display:none;" dir="auto">
                            <div class="avatar-wrap" style="padding-right:10px" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" style="width:30px !important;height:30px !important;" dir="auto">
                            </div>
                            <div class="post-add-com-inputs" dir="auto">
                            <form class="commentReplyForm13235" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto">
                            <input type="hidden" data-id="pair13235" name="pair" value="5f770bda7cebc" dir="auto">
                            <div class="post-create-block post-comment-create-block post-reply-block" tabindex="0" dir="auto">
                            <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                            <textarea name="text" data-id="text13235" class="textarea-customize post-comment-emoji" style="display: none; vertical-align: top; min-height: 50px;" placeholder="Write a comment" dir="auto"></textarea><div class="note-editor note-frame panel panel-default"><div class="note-dropzone">  <div class="note-dropzone-message" dir="auto"></div></div><div class="note-toolbar panel-heading" role="toolbar" dir="auto"><div class="note-btn-group btn-group note-insert"><button type="button" class="note-btn btn btn-default btn-sm" role="button" tabindex="-1" dir="auto"><div class="emoji-picker-container emoji-picker emojionearea-button d-none" dir="auto"></div></button><div class="emoji-menu b8bffcba-32a1-48ce-98ea-dcf4a8fbf53b" style="display: none;">
                                <div class="emoji-items-wrap1" dir="auto">
                                    <table class="emoji-menu-tabs" dir="auto">
                                        <tbody dir="auto">
                                        <tr dir="auto">
                                            <td dir="auto"><a class="emoji-menu-tab icon-recent-selected" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-smile" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-flower" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-bell" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-car" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-grid" dir="auto"></a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="emoji-items-wrap mobile_scrollable_wrap" dir="auto">
                                        <div class="emoji-items" dir="auto"><a href="javascript:void(0)" title=":flushed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -350px 0px no-repeat;background-size:675px 175px;" alt=":flushed:" dir="auto"><span class="label" dir="auto">:flushed:</span></a><a href="javascript:void(0)" title=":grin:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -375px 0px no-repeat;background-size:675px 175px;" alt=":grin:" dir="auto"><span class="label" dir="auto">:grin:</span></a><a href="javascript:void(0)" title=":rage:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px -25px no-repeat;background-size:675px 175px;" alt=":rage:" dir="auto"><span class="label" dir="auto">:rage:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -300px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_closed_eyes:" dir="auto"><span class="label" dir="auto">:stuck_out_tongue_closed_eyes:</span></a><a href="javascript:void(0)" title=":kissing_heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px 0px no-repeat;background-size:675px 175px;" alt=":kissing_heart:" dir="auto"><span class="label" dir="auto">:kissing_heart:</span></a><a href="javascript:void(0)" title=":ok_hand:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -650px -75px no-repeat;background-size:675px 175px;" alt=":ok_hand:" dir="auto"><span class="label" dir="auto">:ok_hand:</span></a><a href="javascript:void(0)" title=":heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -250px -150px no-repeat;background-size:675px 175px;" alt=":heart:" dir="auto"><span class="label" dir="auto">:heart:</span></a><a href="javascript:void(0)" title=":frog:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_1.png') -150px 0px no-repeat;background-size:725px 100px;" alt=":frog:" dir="auto"><span class="label" dir="auto">:frog:</span></a><a href="javascript:void(0)" title=":+1:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -75px no-repeat;background-size:675px 175px;" alt=":+1:" dir="auto"><span class="label" dir="auto">:+1:</span></a><a href="javascript:void(0)" title=":heart_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -150px 0px no-repeat;background-size:675px 175px;" alt=":heart_eyes:" dir="auto"><span class="label" dir="auto">:heart_eyes:</span></a><a href="javascript:void(0)" title=":blush:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px 0px no-repeat;background-size:675px 175px;" alt=":blush:" dir="auto"><span class="label" dir="auto">:blush:</span></a><a href="javascript:void(0)" title=":joy:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -550px 0px no-repeat;background-size:675px 175px;" alt=":joy:" dir="auto"><span class="label" dir="auto">:joy:</span></a><a href="javascript:void(0)" title=":relaxed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -100px 0px no-repeat;background-size:675px 175px;" alt=":relaxed:" dir="auto"><span class="label" dir="auto">:relaxed:</span></a><a href="javascript:void(0)" title=":pensive:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -400px 0px no-repeat;background-size:675px 175px;" alt=":pensive:" dir="auto"><span class="label" dir="auto">:pensive:</span></a><a href="javascript:void(0)" title=":smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px 0px no-repeat;background-size:675px 175px;" alt=":smile:" dir="auto"><span class="label" dir="auto">:smile:</span></a><a href="javascript:void(0)" title=":sob:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -575px 0px no-repeat;background-size:675px 175px;" alt=":sob:" dir="auto"><span class="label" dir="auto">:sob:</span></a><a href="javascript:void(0)" title=":kiss:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px -150px no-repeat;background-size:675px 175px;" alt=":kiss:" dir="auto"><span class="label" dir="auto">:kiss:</span></a><a href="javascript:void(0)" title=":unamused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -450px 0px no-repeat;background-size:675px 175px;" alt=":unamused:" dir="auto"><span class="label" dir="auto">:unamused:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_winking_eye:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_winking_eye:" dir="auto"><span class="label" dir="auto">:stuck_out_tongue_winking_eye:</span></a><a href="javascript:void(0)" title=":see_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px -75px no-repeat;background-size:675px 175px;" alt=":see_no_evil:" dir="auto"><span class="label" dir="auto">:see_no_evil:</span></a><a href="javascript:void(0)" title=":wink:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px 0px no-repeat;background-size:675px 175px;" alt=":wink:" dir="auto"><span class="label" dir="auto">:wink:</span></a><a href="javascript:void(0)" title=":smiley:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -25px 0px no-repeat;background-size:675px 175px;" alt=":smiley:" dir="auto"><span class="label" dir="auto">:smiley:</span></a><a href="javascript:void(0)" title=":cry:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -525px 0px no-repeat;background-size:675px 175px;" alt=":cry:" dir="auto"><span class="label" dir="auto">:cry:</span></a><a href="javascript:void(0)" title=":scream:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -25px no-repeat;background-size:675px 175px;" alt=":scream:" dir="auto"><span class="label" dir="auto">:scream:</span></a><a href="javascript:void(0)" title=":smirk:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px -50px no-repeat;background-size:675px 175px;" alt=":smirk:" dir="auto"><span class="label" dir="auto">:smirk:</span></a><a href="javascript:void(0)" title=":disappointed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px 0px no-repeat;background-size:675px 175px;" alt=":disappointed:" dir="auto"><span class="label" dir="auto">:disappointed:</span></a><a href="javascript:void(0)" title=":sweat_smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px -25px no-repeat;background-size:675px 175px;" alt=":sweat_smile:" dir="auto"><span class="label" dir="auto">:sweat_smile:</span></a><a href="javascript:void(0)" title=":kissing_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -200px 0px no-repeat;background-size:675px 175px;" alt=":kissing_closed_eyes:" dir="auto"><span class="label" dir="auto">:kissing_closed_eyes:</span></a><a href="javascript:void(0)" title=":speak_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -75px no-repeat;background-size:675px 175px;" alt=":speak_no_evil:" dir="auto"><span class="label" dir="auto">:speak_no_evil:</span></a><a href="javascript:void(0)" title=":relieved:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -425px 0px no-repeat;background-size:675px 175px;" alt=":relieved:" dir="auto"><span class="label" dir="auto">:relieved:</span></a><a href="javascript:void(0)" title=":grinning:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px 0px no-repeat;background-size:675px 175px;" alt=":grinning:" dir="auto"><span class="label" dir="auto">:grinning:</span></a><a href="javascript:void(0)" title=":yum:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px -25px no-repeat;background-size:675px 175px;" alt=":yum:" dir="auto"><span class="label" dir="auto">:yum:</span></a><a href="javascript:void(0)" title=":neutral_face:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -25px no-repeat;background-size:675px 175px;" alt=":neutral_face:" dir="auto"><span class="label" dir="auto">:neutral_face:</span></a><a href="javascript:void(0)" title=":confused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -625px -25px no-repeat;background-size:675px 175px;" alt=":confused:" dir="auto"><span class="label" dir="auto">:confused:</span></a></div>
                                    </div>
                                </div>
                            </div></div><div class="note-btn-group btn-group note-custom"></div></div><div class="note-editing-area" dir="auto"><div class="note-placeholder custom-placeholder" style="display: none;">Write a comment</div><div class="note-handle"><div class="note-control-selection" dir="auto"><div class="note-control-selection-bg" dir="auto"></div><div class="note-control-holder note-control-nw" dir="auto"></div><div class="note-control-holder note-control-ne" dir="auto"></div><div class="note-control-holder note-control-sw" dir="auto"></div><div class="note-control-sizing note-control-se" dir="auto"></div><div class="note-control-selection-info" dir="auto"></div></div></div><textarea class="note-codable" role="textbox" aria-multiline="true" dir="auto"></textarea><div class="note-editable" contenteditable="true" role="textbox" aria-multiline="true" dir="auto" spellcheck="true"><div><br dir="auto"></div></div></div><output class="note-status-output" aria-live="polite" dir="auto"></output><div class="note-statusbar" role="status" dir="auto" style="">  <div class="note-resizebar" role="seperator" aria-orientation="horizontal" aria-label="Resize" dir="auto">    <div class="note-icon-bar" dir="auto"></div>    <div class="note-icon-bar" dir="auto"></div>    <div class="note-icon-bar" dir="auto"></div>  </div></div><div class="modal link-dialog" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Link"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Link</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group" dir="auto"><label class="note-form-label" dir="auto">Text to display</label><input class="note-link-text form-control note-form-control note-input" type="text" dir="auto"></div><div class="form-group note-form-group" dir="auto"><label class="note-form-label" dir="auto">To what URL should this link go?</label><input class="note-link-url form-control note-form-control note-input" type="text" value="http://" dir="auto"></div><div class="checkbox sn-checkbox-open-in-new-window" dir="auto"><label dir="auto"> <input role="checkbox" type="checkbox" checked="" aria-checked="true" dir="auto">Open in new window</label></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-link-btn" value="Insert Link" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Image"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Image</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group note-group-select-from-files" dir="auto"><label class="note-form-label" dir="auto">Select from files</label><input class="note-image-input form-control-file note-form-control note-input" type="file" name="files" accept="image/*" multiple="multiple" dir="auto"></div><div class="form-group note-group-image-url" style="overflow:auto;" dir="auto"><label class="note-form-label" dir="auto">Image URL</label><input class="note-image-url form-control note-form-control note-input  col-md-12" type="text" dir="auto"></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-image-btn" value="Insert Image" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Video"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Video</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group row-fluid" dir="auto"><label class="note-form-label" dir="auto">Video URL <small class="text-muted" dir="auto">(YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)</small></label><input class="note-video-url form-control note-form-control note-input" type="text" dir="auto"></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-video-btn" value="Insert Video" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Help"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Help</h4>    </div>    <div class="modal-body" dir="auto" style="max-height: 300px; overflow: scroll;"><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+Z</kbd></label><span dir="auto">Undoes the last command</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+Y</kbd></label><span dir="auto">Redoes the last command</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">TAB</kbd></label><span dir="auto">Tab</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">SHIFT+TAB</kbd></label><span dir="auto">Untab</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+B</kbd></label><span dir="auto">Set a bold style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+I</kbd></label><span dir="auto">Set a italic style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+U</kbd></label><span dir="auto">Set a underline style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+S</kbd></label><span dir="auto">Set a strikethrough style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+BACKSLASH</kbd></label><span dir="auto">Clean a style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+L</kbd></label><span dir="auto">Set left align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+E</kbd></label><span dir="auto">Set center align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+R</kbd></label><span dir="auto">Set right align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+J</kbd></label><span dir="auto">Set full align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+NUM7</kbd></label><span dir="auto">Toggle unordered list</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+NUM8</kbd></label><span dir="auto">Toggle ordered list</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+LEFTBRACKET</kbd></label><span dir="auto">Outdent on current paragraph</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+RIGHTBRACKET</kbd></label><span dir="auto">Indent on current paragraph</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM0</kbd></label><span dir="auto">Change current block's format as a paragraph(P tag)</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM1</kbd></label><span dir="auto">Change current block's format as H1</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM2</kbd></label><span dir="auto">Change current block's format as H2</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM3</kbd></label><span dir="auto">Change current block's format as H3</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM4</kbd></label><span dir="auto">Change current block's format as H4</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM5</kbd></label><span dir="auto">Change current block's format as H5</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM6</kbd></label><span dir="auto">Change current block's format as H6</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+ENTER</kbd></label><span dir="auto">Insert horizontal rule</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+K</kbd></label><span dir="auto">Show Link Dialog</span></div>    <div class="modal-footer" dir="auto"><p class="text-center" dir="auto"><a href="http://summernote.org/" target="_blank" dir="auto">Summernote 0.8.12</a> · <a href="https://github.com/summernote/summernote" target="_blank" dir="auto">Project</a> · <a href="https://github.com/summernote/summernote/issues" target="_blank" dir="auto">Issues</a></p></div>  </div></div></div></div>
                            <div class="textcomplete-wrapper"></div></div>
                            <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                            <div class="post-alloptions" dir="auto">
                            <ul class="create-link-list" dir="auto">
                            <li class="post-options" dir="auto">
                            <input type="file" name="file[]" id="commentreplyfile13235" style="display:none" multiple="" dir="auto">
                            <i class="fa fa-camera click-target" data-target="commentreplyfile13235" dir="auto"></i>
                            </li>
                            </ul>
                            </div>
                            <button type="submit" class="btn btn-primary d-none" dir="auto"></button>
                            <div class="comment-edit-action" dir="auto">
                            <a href="javascript:;" class="p-comment-cancel-link post-r-cancel-link" dir="auto">Cancel</a>
                            <a href="javascript:;" class="p-comment-link post-r-reply-comment-link" data-comment_id="13235" dir="auto">Post</a>
                            </div>
                            </div>
                            <div class="medias p-media b-whitesmoke" dir="auto"></div>
                            </div>
                            <input type="hidden" name="post_id" value="622665" dir="auto">
                            <input type="hidden" name="comment_id" value="13235" dir="auto">
                            </form>
                            </div>
                            </div>
                            </div>
                            <div topsort="1" newsort="1601563197" dir="auto" class="displayed" style="">
                            <div class="post-comment-row news-feed-comment" id="commentRow13234" dir="auto">
                            <div class="post-com-avatar-wrap" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" dir="auto">
                            </div>
                            <div class="post-comment-text" dir="auto">
                            <div class="post-com-name-layer" dir="auto">
                            <a href="#" class="comment-name" dir="auto">Rocio Smither</a>
                            <span class="timestamp">said &nbsp;&middot;&nbsp; <span>9 hours ago</span></span>
                            <div class="post-com-top-action" dir="auto">
                            <div class="dropdown " dir="auto">
                            <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                            <i class="trav-angle-down" dir="auto"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment" dir="auto">
                            <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="13234" data-post="622665" dir="auto">
                            <span class="icon-wrap" dir="auto">
                            <i class="trav-pencil" aria-hidden="true" dir="auto"></i>
                            </span>
                            <div class="drop-txt comment-edit__drop-text" dir="auto">
                            <p dir="auto"><b dir="auto">Edit</b></p>
                            </div>
                            </a>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="comment-txt comment-text-13234" dir="auto">
                            <p dir="auto">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos accusamus deserunt error harum voluptatum dicta minima aspernatur fugiat temporibus ut natus aliquam non repudiandae iusto impedit accusantium, odio molestias sunt.
                            </p>
                            <form class="commentEditForm13234 comment-edit-form d-none" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto"><input type="hidden" data-id="pair13234" name="pair" value="5f770bda7ee63" dir="auto">
                            <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0" dir="auto">
                            <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                            <textarea name="text" data-id="text13234" class="textarea-customize post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="Write a comment" dir="auto">Text comment</textarea>
                            </div>
                            <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                            <div class="post-alloptions" dir="auto">
                            <ul class="create-link-list" dir="auto">
                            <li class="post-options" dir="auto">
                            <input type="file" name="file[]" class="commenteditfile" id="commenteditfile13234" style="display:none" multiple="" dir="auto">
                            <i class="fa fa-camera click-target" data-target="commenteditfile13234" dir="auto"></i>
                            </li>
                            </ul>
                            </div>
                            <div class="comment-edit-action" dir="auto">
                            <a href="javascript:;" class="edit-cancel-link" data-comment_id="13234" dir="auto">Cancel</a>
                            <a href="javascript:;" class="edit-post-link" data-comment_id="13234" dir="auto">Post</a>
                            </div>
                            </div>
                            <div class="medias p-media b-whitesmoke" dir="auto">
                            </div>
                            </div>
                            <input type="hidden" name="post_id" value="622665" dir="auto">
                            <input type="hidden" name="comment_id" value="13234" dir="auto">
                            <input type="hidden" name="comment_type" value="1" dir="auto">
                            <button type="submit" class="d-none" dir="auto"></button>
                            </form>
                            </div>
                            <div class="post-image-container" dir="auto">
                            </div>
                            <div class="comment-bottom-info" dir="auto">
                            <div class="comment-info-content" dir="auto">
                                <!-- New element -->
                                <div class="tips-footer updownvote" id="9953" dir="auto">
                                    <a href="#" class="upvote-link up disabled" id="9953" dir="auto">
                                        <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-up" dir="auto"></i></span>
                                    </a>
                                    <span dir="auto"><b class="upvote-count" dir="auto">13</b> Upvotes</span>
                                    &nbsp;&nbsp;
                                    <a href="#" class="upvote-link down disabled" id="9953" dir="auto">
                                        <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-down" dir="auto"></i></span>
                                    </a>
                                    <span dir="auto"><b class="downvote-count" dir="auto">3</b> Downvotes</span>
                                </div>
                                <!-- New element -->
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="post-add-comment-block" id="replyForm13234" style="padding-top:0px;padding-left: 59px;display:none;" dir="auto">
                            <div class="avatar-wrap" style="padding-right:10px" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" style="width:30px !important;height:30px !important;" dir="auto">
                            </div>
                            <div class="post-add-com-inputs" dir="auto">
                            <form class="commentReplyForm13234" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data" dir="auto">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR" dir="auto">
                            <input type="hidden" data-id="pair13234" name="pair" value="5f770bda8135d" dir="auto">
                            <div class="post-create-block post-comment-create-block post-reply-block" tabindex="0" dir="auto">
                            <div class="post-create-input p-create-input b-whitesmoke" dir="auto">
                            <textarea name="text" data-id="text13234" class="textarea-customize post-comment-emoji" style="display: none; vertical-align: top; min-height: 50px;" placeholder="Write a comment" dir="auto"></textarea><div class="note-editor note-frame panel panel-default"><div class="note-dropzone">  <div class="note-dropzone-message" dir="auto"></div></div><div class="note-toolbar panel-heading" role="toolbar" dir="auto"><div class="note-btn-group btn-group note-insert"><button type="button" class="note-btn btn btn-default btn-sm" role="button" tabindex="-1" dir="auto"><div class="emoji-picker-container emoji-picker emojionearea-button d-none" dir="auto"></div></button><div class="emoji-menu 00b5dc61-5c5c-40de-9479-07713f85c370" style="display: none;">
                                <div class="emoji-items-wrap1" dir="auto">
                                    <table class="emoji-menu-tabs" dir="auto">
                                        <tbody dir="auto">
                                        <tr dir="auto">
                                            <td dir="auto"><a class="emoji-menu-tab icon-recent-selected" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-smile" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-flower" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-bell" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-car" dir="auto"></a></td>
                                            <td dir="auto"><a class="emoji-menu-tab icon-grid" dir="auto"></a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="emoji-items-wrap mobile_scrollable_wrap" dir="auto">
                                        <div class="emoji-items" dir="auto"><a href="javascript:void(0)" title=":flushed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -350px 0px no-repeat;background-size:675px 175px;" alt=":flushed:" dir="auto"><span class="label" dir="auto">:flushed:</span></a><a href="javascript:void(0)" title=":grin:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -375px 0px no-repeat;background-size:675px 175px;" alt=":grin:" dir="auto"><span class="label" dir="auto">:grin:</span></a><a href="javascript:void(0)" title=":rage:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px -25px no-repeat;background-size:675px 175px;" alt=":rage:" dir="auto"><span class="label" dir="auto">:rage:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -300px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_closed_eyes:" dir="auto"><span class="label" dir="auto">:stuck_out_tongue_closed_eyes:</span></a><a href="javascript:void(0)" title=":kissing_heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -175px 0px no-repeat;background-size:675px 175px;" alt=":kissing_heart:" dir="auto"><span class="label" dir="auto">:kissing_heart:</span></a><a href="javascript:void(0)" title=":ok_hand:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -650px -75px no-repeat;background-size:675px 175px;" alt=":ok_hand:" dir="auto"><span class="label" dir="auto">:ok_hand:</span></a><a href="javascript:void(0)" title=":heart:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -250px -150px no-repeat;background-size:675px 175px;" alt=":heart:" dir="auto"><span class="label" dir="auto">:heart:</span></a><a href="javascript:void(0)" title=":frog:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_1.png') -150px 0px no-repeat;background-size:725px 100px;" alt=":frog:" dir="auto"><span class="label" dir="auto">:frog:</span></a><a href="javascript:void(0)" title=":+1:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -75px no-repeat;background-size:675px 175px;" alt=":+1:" dir="auto"><span class="label" dir="auto">:+1:</span></a><a href="javascript:void(0)" title=":heart_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -150px 0px no-repeat;background-size:675px 175px;" alt=":heart_eyes:" dir="auto"><span class="label" dir="auto">:heart_eyes:</span></a><a href="javascript:void(0)" title=":blush:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px 0px no-repeat;background-size:675px 175px;" alt=":blush:" dir="auto"><span class="label" dir="auto">:blush:</span></a><a href="javascript:void(0)" title=":joy:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -550px 0px no-repeat;background-size:675px 175px;" alt=":joy:" dir="auto"><span class="label" dir="auto">:joy:</span></a><a href="javascript:void(0)" title=":relaxed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -100px 0px no-repeat;background-size:675px 175px;" alt=":relaxed:" dir="auto"><span class="label" dir="auto">:relaxed:</span></a><a href="javascript:void(0)" title=":pensive:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -400px 0px no-repeat;background-size:675px 175px;" alt=":pensive:" dir="auto"><span class="label" dir="auto">:pensive:</span></a><a href="javascript:void(0)" title=":smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px 0px no-repeat;background-size:675px 175px;" alt=":smile:" dir="auto"><span class="label" dir="auto">:smile:</span></a><a href="javascript:void(0)" title=":sob:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -575px 0px no-repeat;background-size:675px 175px;" alt=":sob:" dir="auto"><span class="label" dir="auto">:sob:</span></a><a href="javascript:void(0)" title=":kiss:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px -150px no-repeat;background-size:675px 175px;" alt=":kiss:" dir="auto"><span class="label" dir="auto">:kiss:</span></a><a href="javascript:void(0)" title=":unamused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -450px 0px no-repeat;background-size:675px 175px;" alt=":unamused:" dir="auto"><span class="label" dir="auto">:unamused:</span></a><a href="javascript:void(0)" title=":stuck_out_tongue_winking_eye:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_winking_eye:" dir="auto"><span class="label" dir="auto">:stuck_out_tongue_winking_eye:</span></a><a href="javascript:void(0)" title=":see_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -75px -75px no-repeat;background-size:675px 175px;" alt=":see_no_evil:" dir="auto"><span class="label" dir="auto">:see_no_evil:</span></a><a href="javascript:void(0)" title=":wink:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px 0px no-repeat;background-size:675px 175px;" alt=":wink:" dir="auto"><span class="label" dir="auto">:wink:</span></a><a href="javascript:void(0)" title=":smiley:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -25px 0px no-repeat;background-size:675px 175px;" alt=":smiley:" dir="auto"><span class="label" dir="auto">:smiley:</span></a><a href="javascript:void(0)" title=":cry:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -525px 0px no-repeat;background-size:675px 175px;" alt=":cry:" dir="auto"><span class="label" dir="auto">:cry:</span></a><a href="javascript:void(0)" title=":scream:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -25px no-repeat;background-size:675px 175px;" alt=":scream:" dir="auto"><span class="label" dir="auto">:scream:</span></a><a href="javascript:void(0)" title=":smirk:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px -50px no-repeat;background-size:675px 175px;" alt=":smirk:" dir="auto"><span class="label" dir="auto">:smirk:</span></a><a href="javascript:void(0)" title=":disappointed:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -475px 0px no-repeat;background-size:675px 175px;" alt=":disappointed:" dir="auto"><span class="label" dir="auto">:disappointed:</span></a><a href="javascript:void(0)" title=":sweat_smile:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') 0px -25px no-repeat;background-size:675px 175px;" alt=":sweat_smile:" dir="auto"><span class="label" dir="auto">:sweat_smile:</span></a><a href="javascript:void(0)" title=":kissing_closed_eyes:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -200px 0px no-repeat;background-size:675px 175px;" alt=":kissing_closed_eyes:" dir="auto"><span class="label" dir="auto">:kissing_closed_eyes:</span></a><a href="javascript:void(0)" title=":speak_no_evil:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -125px -75px no-repeat;background-size:675px 175px;" alt=":speak_no_evil:" dir="auto"><span class="label" dir="auto">:speak_no_evil:</span></a><a href="javascript:void(0)" title=":relieved:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -425px 0px no-repeat;background-size:675px 175px;" alt=":relieved:" dir="auto"><span class="label" dir="auto">:relieved:</span></a><a href="javascript:void(0)" title=":grinning:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -50px 0px no-repeat;background-size:675px 175px;" alt=":grinning:" dir="auto"><span class="label" dir="auto">:grinning:</span></a><a href="javascript:void(0)" title=":yum:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -275px -25px no-repeat;background-size:675px 175px;" alt=":yum:" dir="auto"><span class="label" dir="auto">:yum:</span></a><a href="javascript:void(0)" title=":neutral_face:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -600px -25px no-repeat;background-size:675px 175px;" alt=":neutral_face:" dir="auto"><span class="label" dir="auto">:neutral_face:</span></a><a href="javascript:void(0)" title=":confused:"><img src="https://travooo.com/plugins/summernote-master/tam-emoji/img/blank.gif" class="img" style="display:inline-block;width:25px;height:25px;background:url('https://travooo.com/plugins/summernote-master/tam-emoji/img/emoji_spritesheet_0.png') -625px -25px no-repeat;background-size:675px 175px;" alt=":confused:" dir="auto"><span class="label" dir="auto">:confused:</span></a></div>
                                    </div>
                                </div>
                            </div></div><div class="note-btn-group btn-group note-custom"></div></div><div class="note-editing-area" dir="auto"><div class="note-placeholder custom-placeholder" style="display: none;">Write a comment</div><div class="note-handle"><div class="note-control-selection" dir="auto"><div class="note-control-selection-bg" dir="auto"></div><div class="note-control-holder note-control-nw" dir="auto"></div><div class="note-control-holder note-control-ne" dir="auto"></div><div class="note-control-holder note-control-sw" dir="auto"></div><div class="note-control-sizing note-control-se" dir="auto"></div><div class="note-control-selection-info" dir="auto"></div></div></div><textarea class="note-codable" role="textbox" aria-multiline="true" dir="auto"></textarea><div class="note-editable" contenteditable="true" role="textbox" aria-multiline="true" dir="auto" spellcheck="true"><div><br dir="auto"></div></div></div><output class="note-status-output" aria-live="polite" dir="auto"></output><div class="note-statusbar" role="status" dir="auto" style="">  <div class="note-resizebar" role="seperator" aria-orientation="horizontal" aria-label="Resize" dir="auto">    <div class="note-icon-bar" dir="auto"></div>    <div class="note-icon-bar" dir="auto"></div>    <div class="note-icon-bar" dir="auto"></div>  </div></div><div class="modal link-dialog" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Link"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Link</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group" dir="auto"><label class="note-form-label" dir="auto">Text to display</label><input class="note-link-text form-control note-form-control note-input" type="text" dir="auto"></div><div class="form-group note-form-group" dir="auto"><label class="note-form-label" dir="auto">To what URL should this link go?</label><input class="note-link-url form-control note-form-control note-input" type="text" value="http://" dir="auto"></div><div class="checkbox sn-checkbox-open-in-new-window" dir="auto"><label dir="auto"> <input role="checkbox" type="checkbox" checked="" aria-checked="true" dir="auto">Open in new window</label></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-link-btn" value="Insert Link" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Image"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Image</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group note-group-select-from-files" dir="auto"><label class="note-form-label" dir="auto">Select from files</label><input class="note-image-input form-control-file note-form-control note-input" type="file" name="files" accept="image/*" multiple="multiple" dir="auto"></div><div class="form-group note-group-image-url" style="overflow:auto;" dir="auto"><label class="note-form-label" dir="auto">Image URL</label><input class="note-image-url form-control note-form-control note-input  col-md-12" type="text" dir="auto"></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-image-btn" value="Insert Image" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Insert Video"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Insert Video</h4>    </div>    <div class="modal-body" dir="auto"><div class="form-group note-form-group row-fluid" dir="auto"><label class="note-form-label" dir="auto">Video URL <small class="text-muted" dir="auto">(YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)</small></label><input class="note-video-url form-control note-form-control note-input" type="text" dir="auto"></div></div>    <div class="modal-footer" dir="auto"><input type="button" href="#" class="btn btn-primary note-btn note-btn-primary note-video-btn" value="Insert Video" disabled="" dir="auto"></div>  </div></div></div><div class="modal" aria-hidden="false" tabindex="-1" role="dialog" aria-label="Help"><div class="modal-dialog" dir="auto">  <div class="modal-content" dir="auto">    <div class="modal-header" dir="auto">      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true" dir="auto">×</button>      <h4 class="modal-title" dir="auto">Help</h4>    </div>    <div class="modal-body" dir="auto" style="max-height: 300px; overflow: scroll;"><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+Z</kbd></label><span dir="auto">Undoes the last command</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+Y</kbd></label><span dir="auto">Redoes the last command</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">TAB</kbd></label><span dir="auto">Tab</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">SHIFT+TAB</kbd></label><span dir="auto">Untab</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+B</kbd></label><span dir="auto">Set a bold style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+I</kbd></label><span dir="auto">Set a italic style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+U</kbd></label><span dir="auto">Set a underline style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+S</kbd></label><span dir="auto">Set a strikethrough style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+BACKSLASH</kbd></label><span dir="auto">Clean a style</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+L</kbd></label><span dir="auto">Set left align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+E</kbd></label><span dir="auto">Set center align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+R</kbd></label><span dir="auto">Set right align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+J</kbd></label><span dir="auto">Set full align</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+NUM7</kbd></label><span dir="auto">Toggle unordered list</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+SHIFT+NUM8</kbd></label><span dir="auto">Toggle ordered list</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+LEFTBRACKET</kbd></label><span dir="auto">Outdent on current paragraph</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+RIGHTBRACKET</kbd></label><span dir="auto">Indent on current paragraph</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM0</kbd></label><span dir="auto">Change current block's format as a paragraph(P tag)</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM1</kbd></label><span dir="auto">Change current block's format as H1</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM2</kbd></label><span dir="auto">Change current block's format as H2</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM3</kbd></label><span dir="auto">Change current block's format as H3</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM4</kbd></label><span dir="auto">Change current block's format as H4</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM5</kbd></label><span dir="auto">Change current block's format as H5</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+NUM6</kbd></label><span dir="auto">Change current block's format as H6</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+ENTER</kbd></label><span dir="auto">Insert horizontal rule</span><div class="help-list-item" dir="auto"></div><label style="width: 180px; margin-right: 10px;" dir="auto"><kbd dir="auto">CTRL+K</kbd></label><span dir="auto">Show Link Dialog</span></div>    <div class="modal-footer" dir="auto"><p class="text-center" dir="auto"><a href="http://summernote.org/" target="_blank" dir="auto">Summernote 0.8.12</a> · <a href="https://github.com/summernote/summernote" target="_blank" dir="auto">Project</a> · <a href="https://github.com/summernote/summernote/issues" target="_blank" dir="auto">Issues</a></p></div>  </div></div></div></div>
                            <div class="textcomplete-wrapper"></div></div>
                            <div class="post-create-controls b-whitesmoke d-none" dir="auto">
                            <div class="post-alloptions" dir="auto">
                            <ul class="create-link-list" dir="auto">
                            <li class="post-options" dir="auto">
                            <input type="file" name="file[]" id="commentreplyfile13234" style="display:none" multiple="" dir="auto">
                            <i class="fa fa-camera click-target" data-target="commentreplyfile13234" dir="auto"></i>
                            </li>
                            </ul>
                            </div>
                            <button type="submit" class="btn btn-primary d-none" dir="auto"></button>
                            <div class="comment-edit-action" dir="auto">
                            <a href="javascript:;" class="p-comment-cancel-link post-r-cancel-link" dir="auto">Cancel</a>
                            <a href="javascript:;" class="p-comment-link post-r-reply-comment-link" data-comment_id="13234" dir="auto">Post</a>
                            </div>
                            </div>
                            <div class="medias p-media b-whitesmoke" dir="auto"></div>
                            </div>
                            <input type="hidden" name="post_id" value="622665" dir="auto">
                            <input type="hidden" name="comment_id" value="13234" dir="auto">
                            </form>
                            </div>
                            </div>
                            </div>
                            </div>
                            <a href="#" class="load-more-comments">Load more...</a>
                            <div class="post-add-comment-block" dir="auto">
                            <div class="avatar-wrap" dir="auto">
                            <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" style="width:45px;height:45px;" dir="auto">
                            </div>
                            <!-- New elements -->
                            <div class="add-comment-input-group">
                                <input type="text" placeholder="Write a comment...">
                                <div>
                                    <button class="add-post__emoji" emoji-target="add_post_text" type="button" onclick="showFaceBlock()" id="faceEnter">🙂</button>
                                    <button><i class="trav-camera click-target" data-target="file"></i></button>
                                </div>
                            </div>
                            <!-- New elements END  -->
                            </div>
                            </div>
                            <!-- Copied from home page END -->
                            <div class="post-comment-layer" data-content="shares601451" style="display:none;" >
                            <div class="post-comment-wrapper" id="shares601451" >
                            </div>
                            </div>
                            </div>
                            <!-- Primary post block - text END -->
    </div>                        
</div>
<script>
$(function () {
  $('[data-toggle="bs-tooltip"]').tooltip()
})
</script>
<!-- Discussion text popup END -->