<!-- Dig deeper popup -->
<div class="modal fade white-style dashboard-dig-popup" data-backdrop="false" id="digPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-setting-block search-travel-mates">
                <div class="post-side-top">
                    <h3 class="side-ttl">Content details</h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="post-block post-dashboard-inner mt-0">
                    <div class="dash-info-inner">
                        Lorem ipsum dolor sit amet consectetur adipiscing elit
                        <div class="sort-by-select">
                            <label>Last</label>
                            <div class="sort-select-wrap">
                                <div class="sort-select-wrapper">
                                    <select class="sort-select posts-reports-filter" placeholder="7 days" style="display: none;">
                                        <option value="30">30 Days</option>
                                        <option value="15">15 Days</option>
                                        <option value="7">7 Days</option>
                                    </select>
                                    <div class="sort-select posts-reports-filter" data-type="undefined">
                                        <span class="sort-select-trigger">7 days</span>
                                        <div class="sort-options">
                                            <span class="sort-option undefined" data-value="30">30 Days</span>
                                            <span class="sort-option undefined" data-value="15">15 Days</span>
                                            <span class="sort-option undefined" data-value="7">7 Days</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dash-inner posts-reports-block">
                        <div class="table-responsive">
                            <table class="table tbl-dashboard">
                                <tbody>
                                    <tr>
                                        <th class="post">Post</th>
                                        <th class="prog-cell">Views</th>
                                        <th class="prog-cell">Likes</th>
                                        <th class="prog-cell">Comments</th>
                                        <th class="prog-cell">Shares</th>
                                    </tr>
                                    <tr class="dash-posts-table-row plan">
                                        <td width="48%">
                                            <a href="#" class="post-tbl-link flex">
                                                <img src="https://api.mapbox.com/styles/v1/mapbox/satellite-v9/static/151,-34,0/140x150?access_token=pk.eyJ1IjoidHJhdm9vbyIsImEiOiJja2ZqbXE4dzMwZjd5MnBtcDYzZTBvdGF2In0.cP7Qz8lNHawWv_mUF4c7gw" alt="image" class="dash-posts-report-img">
                                                <div class="post-copy">
                                                    <div class="title">Vestibulum euismod nunc quis nisl conval</div>
                                                    <div class="timestamp">01/10/2018 - 5:41pm</div>
                                                </div>
                                            </a>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">50</div>
                                                <div class="progress">
                                                    <div class="progress-bar orange" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="width: 50%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">32</div>
                                                <div class="progress">
                                                    <div class="progress-bar red" role="progressbar" aria-valuenow="30" aria-valuemin="30" aria-valuemax="100" style="width: 30%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">5</div>
                                                <div class="progress">
                                                    <div class="progress-bar primary" role="progressbar" aria-valuenow="10" aria-valuemin="10" aria-valuemax="100" style="width: 10%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">40</div>
                                                <div class="progress">
                                                    <div class="progress-bar green" role="progressbar" aria-valuenow="40" aria-valuemin="40" aria-valuemax="100" style="width: 40%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="dash-posts-table-row plan">
                                        <td width="48%">
                                            <a href="#" class="post-tbl-link flex">
                                                <img src="https://api.mapbox.com/styles/v1/mapbox/satellite-v9/static/151,-34,0/140x150?access_token=pk.eyJ1IjoidHJhdm9vbyIsImEiOiJja2ZqbXE4dzMwZjd5MnBtcDYzZTBvdGF2In0.cP7Qz8lNHawWv_mUF4c7gw" alt="image" class="dash-posts-report-img">
                                                <div class="post-copy">
                                                    <div class="title">Vestibulum euismod nunc quis nisl conval</div>
                                                    <div class="timestamp">01/10/2018 - 5:41pm</div>
                                                </div>
                                            </a>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">50</div>
                                                <div class="progress">
                                                    <div class="progress-bar orange" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="width: 50%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">32</div>
                                                <div class="progress">
                                                    <div class="progress-bar red" role="progressbar" aria-valuenow="30" aria-valuemin="30" aria-valuemax="100" style="width: 30%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">5</div>
                                                <div class="progress">
                                                    <div class="progress-bar primary" role="progressbar" aria-valuenow="10" aria-valuemin="10" aria-valuemax="100" style="width: 10%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">40</div>
                                                <div class="progress">
                                                    <div class="progress-bar green" role="progressbar" aria-valuenow="40" aria-valuemin="40" aria-valuemax="100" style="width: 40%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="dash-posts-table-row plan">
                                        <td width="48%">
                                            <a href="#" class="post-tbl-link flex">
                                                <img src="https://api.mapbox.com/styles/v1/mapbox/satellite-v9/static/151,-34,0/140x150?access_token=pk.eyJ1IjoidHJhdm9vbyIsImEiOiJja2ZqbXE4dzMwZjd5MnBtcDYzZTBvdGF2In0.cP7Qz8lNHawWv_mUF4c7gw" alt="image" class="dash-posts-report-img">
                                                <div class="post-copy">
                                                    <div class="title">Vestibulum euismod nunc quis nisl conval</div>
                                                    <div class="timestamp">01/10/2018 - 5:41pm</div>
                                                </div>
                                            </a>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">50</div>
                                                <div class="progress">
                                                    <div class="progress-bar orange" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="width: 50%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">32</div>
                                                <div class="progress">
                                                    <div class="progress-bar red" role="progressbar" aria-valuenow="30" aria-valuemin="30" aria-valuemax="100" style="width: 30%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">5</div>
                                                <div class="progress">
                                                    <div class="progress-bar primary" role="progressbar" aria-valuenow="10" aria-valuemin="10" aria-valuemax="100" style="width: 10%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">40</div>
                                                <div class="progress">
                                                    <div class="progress-bar green" role="progressbar" aria-valuenow="40" aria-valuemin="40" aria-valuemax="100" style="width: 40%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="dash-posts-table-row plan">
                                        <td width="48%">
                                            <a href="#" class="post-tbl-link flex">
                                                <img src="https://api.mapbox.com/styles/v1/mapbox/satellite-v9/static/151,-34,0/140x150?access_token=pk.eyJ1IjoidHJhdm9vbyIsImEiOiJja2ZqbXE4dzMwZjd5MnBtcDYzZTBvdGF2In0.cP7Qz8lNHawWv_mUF4c7gw" alt="image" class="dash-posts-report-img">
                                                <div class="post-copy">
                                                    <div class="title">Vestibulum euismod nunc quis nisl conval</div>
                                                    <div class="timestamp">01/10/2018 - 5:41pm</div>
                                                </div>
                                            </a>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">50</div>
                                                <div class="progress">
                                                    <div class="progress-bar orange" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="width: 50%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">32</div>
                                                <div class="progress">
                                                    <div class="progress-bar red" role="progressbar" aria-valuenow="30" aria-valuemin="30" aria-valuemax="100" style="width: 30%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">5</div>
                                                <div class="progress">
                                                    <div class="progress-bar primary" role="progressbar" aria-valuenow="10" aria-valuemin="10" aria-valuemax="100" style="width: 10%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="13%">
                                            <div class="tbl-progress">
                                                <div class="progress-label">40</div>
                                                <div class="progress">
                                                    <div class="progress-bar green" role="progressbar" aria-valuenow="40" aria-valuemin="40" aria-valuemax="100" style="width: 40%;"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Dig deeper popup -->