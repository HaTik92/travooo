<div class='multiple-images'>
    <div class='multiple-images__header'>
        <div class="image--avatar">
            <img src="{{asset('assets2/image/temp2.jpg')}}" alt="">
        </div>
        <div class="user--info">
            <h4>Freckled Girl</h4>
            <div>15 July at 4:35am <img src="{{asset('assets2/image/globe-mobile.png')}}"></div>
        </div>
    </div>
    <div class='multiple-images__text'>
        <p>Lorem ipsum dolor sit amet,<a>link text</a> consectetur adipiscing elit. Duis ut rutrum dolor, vitae vehicula risus. Nam magna nulla, efficitur nec aliquam at, dictum at massa. Sed mattis lacinia justo vitae condimentum. Phasellus at urna id arcu fringilla sagittis quis et tortor. Praesent eget erat posuere,laoreet enim in, tristique mi. Fusce ac ultricies mi, vel porttitor felis. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
    </div>
    <div class="post-footer-info">
        <!-- Updated elements -->
        <div class="post-foot-block post-reaction">
            <span class="post_like_button">
                <a href="#">
                    <i class="trav-heart-fill-icon"></i>
                </a>
            </span>
            <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                <span class="mobile--comment">13</span><span class="hidden--comment">likes</span>
            </span>
        </div>
        <!-- Updated element END -->
        <div class="post-foot-block comments--wrap">
            <a role='button' class="toggle--coments">
                <img src="{{asset('assets2/image/comment-mobile.png')}}" alt="">
                <div class="mobile--reverse">
                    <ul class="foot-avatar-list">
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                        </li>
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                        </li>
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                        </li>
                    </ul>

                    <span><a href="#" data-tab="comments601451" ><span class="601451-comments-count" ><span class="mobile--comment">20</span> <span class="hidden--comment">Comments</span></a>
            </span>
            </div>
        </div>
        <div class="post-foot-block ml-auto">
            <span class="post_share_button">
                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                    <img src="{{asset('assets2/image/share-mobile.png')}}">
                </a>
            </span>
            <span>
                    <a href="#" data-tab="shares601451" data-toggle="modal" data-target="#usersWhoShare">
                    <span class="mobile--comment">3</span>
            <span class="hidden--comment">Shares</span></a>
            </span>
        </div>
    </div>
    <div class='toggle--comments-large'>
        <div class='multiple-images__comment'>
            <div class="image--posts">
                <img src="{{asset('assets2/image/temp2.jpg')}}" alt="">
            </div>
            <div class="posted--coment">
                <div class="bg--color">
                    <h4>Freckled Girl</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ut rutrum dolor, vitae vehicula risus. Nam magna nulla, efficitur nec aliquam at, dictum at massa. Sed mattis lacinia justo vitae condimentum.</p>
                </div>
                <div class="post--reactions">
                    <div class="text--grey">2 hr</div>
                    <div>
                        <i class="fal fa-heart"></i>
                        <p class="text--bold">2</p>
                    </div>
                    <p class="text--bold">Reply</p>
                </div>
            </div>
        </div>
        <div class='multiple-images__comment inner--comment'>
            <div class="image--posts">
                <img src="{{asset('assets2/image/temp3.jpg')}}" alt="">
            </div>
            <div class="posted--coment">
                <div class="bg--color">
                    <h4>Anni Girl</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ut rutrum dolor, vitae vehicula risus. Nam magna nulla, efficitur nec aliquam at, dictum at massa. Sed mattis lacinia justo vitae condimentum.</p>
                </div>
                <div class="post--reactions">
                    <div class="text--grey">2 hr</div>
                    <div>
                        <i class="fal fa-heart"></i>
                        <p class="text--bold">2</p>
                    </div>
                    <p class="text--bold">Reply</p>
                </div>
            </div>
        </div>
        <div class='multiple-images__input'>
            <div class="input--wrap">
                <Textarea placeholder="Write a comment..."></Textarea>
                <div class="close--wrap">
                    <div><i class="fal fa-times"></i></div>
                    <img src="{{asset('assets2/image/temp3.jpg')}}" alt="">
                </div>
            </div>
            <div class="icon--wrap">
                <i class="fal fa-camera"></i>
                <i class="fal fa-smile"></i>
            </div>  
        </div>
    </div>
    <div class="image-large">
        <img src="{{asset('assets2/image/temp2.jpg')}}" alt="">
        <div class="post-footer-info">
            <!-- Updated elements -->
            <div class="post-foot-block post-reaction">
                <span class="post_like_button">
                    <a href="#">
                        <i class="trav-heart-fill-icon"></i>
                    </a>
                </span>
                <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                    <span class="mobile--comment">13</span><span class="hidden--comment">likes</span>
                </span>
            </div>
            <!-- Updated element END -->
            <div class="post-foot-block comments--wrap">
                <a role='button' class="toggle--coments">
                    <img src="{{asset('assets2/image/comment-mobile.png')}}" alt="">
                    <div class="mobile--reverse">
                        <ul class="foot-avatar-list">
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                            </li>
                        </ul>
    
                        <span><a href="#" data-tab="comments601451" ><span class="601451-comments-count" ><span class="mobile--comment">20</span> <span class="hidden--comment">Comments</span></a>
                </span>
                </div>
            </div>
            <div class="post-foot-block ml-auto">
                <span class="post_share_button">
                    <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                        <img src="{{asset('assets2/image/share-mobile.png')}}">
                    </a>
                </span>
                <span>
                        <a href="#" data-tab="shares601451" data-toggle="modal" data-target="#usersWhoShare">
                        <span class="mobile--comment">3</span>
                <span class="hidden--comment">Shares</span></a>
                </span>
            </div>
        </div>
    </div>
    <div class="image-large">
        <img src="{{asset('assets2/image/temp1.jpg')}}" alt="">
        <div class="post-footer-info">
            <!-- Updated elements -->
            <div class="post-foot-block post-reaction">
                <span class="post_like_button">
                    <a href="#">
                        <i class="trav-heart-fill-icon"></i>
                    </a>
                </span>
                <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                    <span class="mobile--comment">13</span><span class="hidden--comment">likes</span>
                </span>
            </div>
            <!-- Updated element END -->
            <div class="post-foot-block comments--wrap">
                <a role='button' class="toggle--coments">
                    <img src="{{asset('assets2/image/comment-mobile.png')}}" alt="">
                    <div class="mobile--reverse">
                        <ul class="foot-avatar-list">
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                            </li>
                        </ul>
    
                        <span><a href="#" data-tab="comments601451" ><span class="601451-comments-count" ><span class="mobile--comment">20</span> <span class="hidden--comment">Comments</span></a>
                </span>
                </div>
            </div>
            <div class="post-foot-block ml-auto">
                <span class="post_share_button">
                    <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                        <img src="{{asset('assets2/image/share-mobile.png')}}">
                    </a>
                </span>
                <span>
                        <a href="#" data-tab="shares601451" data-toggle="modal" data-target="#usersWhoShare">
                        <span class="mobile--comment">3</span>
                <span class="hidden--comment">Shares</span></a>
                </span>
            </div>
        </div>
    </div>
</div>

<script>
    $(".toggle--coments").click(function() {
         $(".toggle--comments-large").toggle(100);
     });
</script>