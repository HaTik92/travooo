<!-- Discussion post block - text -->
<div class="post-block discussion-page">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <a class="post-name-link" href="https://travooo.com/profile/831">
                    <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="">
                </a>
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="https://travooo.com/profile/831">Ivan Turlakov</a>
                    <span class="exp-icon">EXP</span>
                </div>
                <div class="post-info">
                    Asked for tips about <a href="https://travooo.com/place/547483" class="link-place">New York</a> 15 July at 4:35am <i class="trav-globe-icon"></i>
                </div>
            </div>
        </div>
        <!-- New elements -->
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-user-plus-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Unfollow User</b></p>
                            <p>Stop seeing posts from User</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-share-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Share</b></p>
                            <p>Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-link"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Copy Link</b></p>
                            <p>Paste the link anyywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#sendToFriendModal">
                        <span class="icon-wrap">
                            <i class="trav-share-on-travo-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Send to a Friend</b></p>
                            <p>Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-heart-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Add to Favorites</b></p>
                            <p>Save it for later</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-flag-icon-o"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Report</b></p>
                            <p>Help us understand</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- New elements END -->
    </div>
    <!-- Discussion wrapper -->
    <div class="discussion-wrapper">
        <!-- New element -->
        <div class="post-txt-wrap">
            <h3>Any tips before I go to <a href="">New York City</a> ?</h3>
            <p>In gravida ullamcorper metus, quis blandit odio posuere sed. Donec dignissim mollis suscipit. Nullam magna tellus, volutpat sodales rhoncus malesuada, scelerisque ut risus. Praesent pharetra sem id felis lobortis vehicula. Vivamus egestas
                est dolor. Nullam a lorem ac lorem dignissim fermentum ut eget lacus sed dictum ornare magna, vestibulum placerat tellus rutrum eu.</p>
            <p>Vivamus egestas est dolor. Nullam a lorem ac lorem dignissim fermentum ut eget lacus sed dictum ornare magna, vestibulum placerat tellus rutrum eu.</p>
        </div>
        <div class="disc-media-block">
            <a href="https://s3.amazonaws.com/travooo-images2/discussion-photo/2116_0_1604906705_St._Nicholas'_Collegiate_Church_Interior.jpg" data-lightbox="ask__media">
                <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/2116_0_1604906705_St._Nicholas'_Collegiate_Church_Interior.jpg" alt="image" class="disc-media-wrap">
            </a>
        </div>
        <div class="post-modal-content-foot">
            <div class="post-modal-foot-list">
                <span>Yesterday at <b><span>10:25 am</span></b>
                </span>
                <span class="dot"> · </span>
                <span>Views <b><span class="3712-view-count">3</span></b>
                </span>
                <span class="dot"> · </span>
                <span>Tagged <b>2</b></span>
                <div class="disc-experts-block">
                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/2115/1604865056_image.png" data-toggle="bs-tooltip" title=""  data-original-title="User Name">
                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/2114/1604864555_image.png" data-toggle="bs-tooltip" title=""  data-original-title="User Name">
                </div>
            </div>
            <div class="disc-topic-text">
                Church, Modernfacts
            </div>
        </div>
        <!-- New element END -->
    </div>
    <!-- Discussion text wrapper END -->
    <div class="post-footer-info">
        <!-- Updated elements -->
        <div class="post-foot-block ml-auto">
            <span class="post_share_button" id="601451">
                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                    <i class="trav-share-icon"></i>
                </a>
            </span>
            <span id="post_share_count_601451"><a href="#" data-tab="shares601451" data-toggle="modal" data-target="#usersWhoShare"><strong>0</strong> Shares</a></span>
        </div>
    </div>
    <div class="post-comment-wrapper" id="following601451">
    </div>
    <!-- Copied from home page -->
    <div class="post-comment-layer" data-content="comments622665" style="display: block;">
        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">Top</li>
                <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">New</li>
            </ul>
            <div class="comm-count-info">
                <strong>2</strong> / <span class="622665-comments-count">2</span>
            </div>
        </div>
        <div class="post-comment-wrapper sortBody" id="comments622665"  travooo-comment-rows="2" travooo-current-page="1">
            <div topsort="0" newsort="1601563217"  class="displayed" style="">
                <div class="post-comment-row news-feed-comment" id="commentRow13235">
                    <div class="post-com-avatar-wrap">
                        <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Eula Cooper</a>
                            <span class="timestamp">said &nbsp;&middot;&nbsp; <span>5 hours ago</span></span>
                            <div class="post-com-top-action">
                                <div class="dropdown "  style="display: block;">
                                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="trav-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                                        <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="13235" data-post="622665">
                                            <span class="icon-wrap">
                                                <i class="trav-pencil" aria-hidden="true"></i>
                                            </span>
                                            <div class="drop-txt comment-edit__drop-text">
                                                <p><b>Edit</b></p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item postCommentDelete" id="13235" data-post="622665" data-type="1">
                                            <span class="icon-wrap">
                                                <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;">
                                            </span>
                                            <div class="drop-txt comment-delete__drop-text">
                                                <p><b>Delete</b></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-txt comment-text-13235">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos accusamus deserunt error harum voluptatum dicta minima aspernatur fugiat temporibus ut natus aliquam non repudiandae iusto impedit accusantium, odio molestias sunt.
                            </p>
                            <form class="commentEditForm13235 comment-edit-form d-none" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR">
                                <input type="hidden" data-id="pair13235" name="pair" value="5f770bda7bc30">
                                <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0">
                                    <div class="post-create-input p-create-input b-whitesmoke">
                                        <textarea name="text" data-id="text13235" class="textarea-customize post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="Write a comment"></textarea>
                                    </div>
                                    <div class="post-create-controls b-whitesmoke d-none">
                                        <div class="post-alloptions">
                                            <ul class="create-link-list">
                                                <li class="post-options">
                                                    <input type="file" name="file[]" class="commenteditfile" id="commenteditfile13235" style="display:none" multiple="">
                                                    <i class="fa fa-camera click-target" data-target="commenteditfile13235"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="comment-edit-action">
                                            <a href="javascript:;" class="edit-cancel-link" data-comment_id="13235">Cancel</a>
                                            <a href="javascript:;" class="edit-post-link" data-comment_id="13235">Post</a>
                                        </div>
                                    </div>
                                    <div class="medias p-media b-whitesmoke">
                                        <div class="img-wrap-newsfeed">
                                            <div>
                                                <img class="thumb" src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="">
                                            </div>
                                            <span class="close remove-media-file" data-media_id="14592485">
                                                <span>×</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="post_id" value="622665">
                                <input type="hidden" name="comment_id" value="13235">
                                <input type="hidden" name="comment_type" value="1">
                                <button type="submit" class="d-none"></button>
                            </form>
                        </div>
                        <div class="post-image-container"></div>
                        <div class="comment-bottom-info">
                            <div class="comment-info-content">
                                <!-- New element -->
                                <div class="tips-footer updownvote" id="9953">
                                    <a href="#" class="upvote-link up" id="9953">
                                        <span class="arrow-icon-wrap"><i class="trav-angle-up"></i></span>
                                    </a>
                                    <span><b class="upvote-count">13</b> Upvotes</span>
                                </div>
                                <!-- New element -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-add-comment-block" id="replyForm13235" style="padding-top:0px;padding-left: 59px;display:none;">
                    <div class="avatar-wrap" style="padding-right:10px">
                        <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" style="width:30px !important;height:30px !important;">
                    </div>
                    <div class="post-add-com-inputs">
                        <form class="commentReplyForm13235" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR">
                            <input type="hidden" data-id="pair13235" name="pair" value="5f770bda7cebc">
                            <div class="post-create-block post-comment-create-block post-reply-block" tabindex="0">
                                <div class="post-create-input p-create-input b-whitesmoke">
                                    <textarea name="text" data-id="text13235" class="textarea-customize post-comment-emoji" style="display: none; vertical-align: top; min-height: 50px;" placeholder="Write a comment"></textarea>
                                    <div class="note-editor note-frame panel panel-default">
                                        <div class="note-dropzone">
                                            <div class="note-dropzone-message"></div>
                                        </div>
                                        <div class="note-toolbar panel-heading" role="toolbar">
                                            <div class="note-btn-group btn-group note-insert">
                                                <button type="button" class="note-btn btn btn-default btn-sm" role="button" tabindex="-1">
                                                    <div class="emoji-picker-container emoji-picker emojionearea-button d-none"></div>
                                                </button>
                                                <div class="emoji-menu b8bffcba-32a1-48ce-98ea-dcf4a8fbf53b" style="display: none;">

                                                </div>
                                            </div>
                                            <div class="note-btn-group btn-group note-custom"></div>
                                        </div>
                                        <div class="note-editing-area">
                                            <div class="note-placeholder custom-placeholder" style="display: none;">Write a comment</div>
                                            <div class="note-handle">
                                                <div class="note-control-selection">
                                                    <div class="note-control-selection-bg"></div>
                                                    <div class="note-control-holder note-control-nw"></div>
                                                    <div class="note-control-holder note-control-ne"></div>
                                                    <div class="note-control-holder note-control-sw"></div>
                                                    <div class="note-control-sizing note-control-se"></div>
                                                    <div class="note-control-selection-info"></div>
                                                </div>
                                            </div>
                                            <textarea class="note-codable" role="textbox" aria-multiline="true"></textarea>
                                            <div class="note-editable" contenteditable="true" role="textbox" aria-multiline="true"  spellcheck="true">
                                                <div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                        <output class="note-status-output" aria-live="polite"></output>
                                        <div class="note-statusbar" role="status"  style="">
                                            <div class="note-resizebar" role="seperator" aria-orientation="horizontal" aria-label="Resize">
                                                <div class="note-icon-bar"></div>
                                                <div class="note-icon-bar"></div>
                                                <div class="note-icon-bar"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="textcomplete-wrapper"></div>
                                </div>
                                <div class="post-create-controls b-whitesmoke d-none">
                                    <div class="post-alloptions">
                                        <ul class="create-link-list">
                                            <li class="post-options">
                                                <input type="file" name="file[]" id="commentreplyfile13235" style="display:none" multiple="">
                                                <i class="fa fa-camera click-target" data-target="commentreplyfile13235"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <button type="submit" class="btn btn-primary d-none"></button>
                                    <div class="comment-edit-action">
                                        <a href="javascript:;" class="p-comment-cancel-link post-r-cancel-link">Cancel</a>
                                        <a href="javascript:;" class="p-comment-link post-r-reply-comment-link" data-comment_id="13235">Post</a>
                                    </div>
                                </div>
                                <div class="medias p-media b-whitesmoke"></div>
                            </div>
                            <input type="hidden" name="post_id" value="622665">
                            <input type="hidden" name="comment_id" value="13235">
                        </form>
                    </div>
                </div>
            </div>
            <div topsort="1" newsort="1601563197"  class="displayed" style="">
                <div class="post-comment-row news-feed-comment" id="commentRow13234">
                    <div class="post-com-avatar-wrap">
                        <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Rocio Smither</a>
                            <span class="timestamp">said &nbsp;&middot;&nbsp; <span>9 hours ago</span></span>
                            <div class="post-com-top-action">
                                <div class="dropdown ">
                                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="trav-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                                        <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="13234" data-post="622665">
                                            <span class="icon-wrap">
                                                <i class="trav-pencil" aria-hidden="true"></i>
                                            </span>
                                            <div class="drop-txt comment-edit__drop-text">
                                                <p><b>Edit</b></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-txt comment-text-13234">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos accusamus deserunt error harum voluptatum dicta minima aspernatur fugiat temporibus ut natus aliquam non repudiandae iusto impedit accusantium, odio molestias sunt.
                            </p>
                        </div>
                        <div class="post-image-container">
                        </div>
                        <div class="comment-bottom-info">
                            <div class="comment-info-content">
                                <!-- New element -->
                                <div class="tips-footer updownvote" id="9953">
                                    <a href="#" class="upvote-link up disabled" id="9953">
                                        <span class="arrow-icon-wrap"><i class="trav-angle-up"></i></span>
                                    </a>
                                    <span><b class="upvote-count">13</b> Upvotes</span> &nbsp;&nbsp;
                                    <a href="#" class="upvote-link down disabled" id="9953">
                                        <span class="arrow-icon-wrap"><i class="trav-angle-down"></i></span>
                                    </a>
                                    <span><b class="downvote-count">3</b> Downvotes</span>
                                </div>
                                <!-- New element -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <a href="#" class="load-more-comments">Load more...</a>
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" style="width:45px;height:45px;">
                </div>
                <!-- New elements -->
                <div class="add-comment-input-group">
                    <input type="text" placeholder="Write a comment...">
                    <div>
                        <button class="add-post__emoji" emoji-target="add_post_text" type="button" onclick="showFaceBlock()" id="faceEnter">🙂</button>
                        <button><i class="trav-camera click-target" data-target="file"></i></button>
                    </div>
                </div>
                <!-- New elements END  -->
            </div>
        </div>
        <!-- Copied from home page END -->
        <div class="post-comment-layer" data-content="shares601451" style="display:none;">
            <div class="post-comment-wrapper" id="shares601451">
            </div>
        </div>
    </div>
    <!-- Discussion post block - text END -->
    <script>
        $(function () {
          $('[data-toggle="bs-tooltip"]').tooltip()
        })
    </script>