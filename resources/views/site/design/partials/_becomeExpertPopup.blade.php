<!-- Become an Expert popup -->
<div class="modal fade become-expert-popup" id="becomeExpertPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-740" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="top-layer">
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" dir="auto">
                        <i class="trav-close-icon" dir="auto"></i>
                    </button>
                    <h3>Become an Expert</h3>
                    <p>Enter more details about you, we will review your application as soon as possible.</p>
                </div>
                <form method="post" action="">
                    <div class="hr"></div>
                    <div class="form-group description">
                        <h4>Social network links</h4>
                        <p>Add your website or social media links</p>
                        <span data-toggle="tooltip" title="" data-placement="top" data-original-title="It’s important to put at least one website or social media profile link where you share your current traveling activities to help us approve your Expert Account application.">Why?</span>
                    </div>
                    <div class="form-group site">
                        <img src="https://travooo.com/assets2/image/sign_up/website.png">
                        <input type="text" class="form-control" placeholder="Put at lease one link...">
                    </div>
                    <div class="form-group site">
                        <img src="https://travooo.com/assets2/image/sign_up/youtube.png">
                        <input type="text" class="form-control" value="https://www.youtube.com/user/mig..." placeholder="Insert your YouTube account URL here...">
                        <div class="cross"><i class="trav-close-icon" dir="auto"></i></div>
                    </div>
                    <div class="form-group description links">
                        Add more links:
                        <div>
                            <img data-name="tumblr" src="https://travooo.com/assets2/image/sign_up/tumblr.png">
                            <img data-name="pinterest" src="https://travooo.com/assets2/image/sign_up/pinterest.png">
                            <img data-name="instagram" src="https://travooo.com/assets2/image/sign_up/instagram.png">
                            <img data-name="facebook" src="https://travooo.com/assets2/image/sign_up/facebook.png">
                            <img data-name="youtube" src="https://travooo.com/assets2/image/sign_up/youtube.png">
                        </div>
                    </div>
                    <div class="hr"></div>
                    <div class="form-group description">
                        <h4>Areas of expertise</h4>
                        <p>Enter any countries, cities or places where you a solid experience.</p>
                    </div>
                    <div class="form-group areas">
                        <i class="trav-search-icon"></i>
                        <input type="text" class="form-control" placeholder="Countries, cities or places...">
                    </div>
                    <div class="form-group description">
                        <p>Selected</p>
                    </div>
                    <div class="form-group selected-areas">
                        <div class="selected-areas-item">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/ma.png" alt="">
                            <span>Morocco</span>
                            <i class="trav-close-icon" dir="auto"></i>
                        </div>
                        <div class="selected-areas-item">
                            <span>Paris</span>
                            <i class="trav-close-icon" dir="auto"></i>
                        </div>
                        <div class="selected-areas-item">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/ma.png" alt="">
                            <span>Morocco</span>
                            <i class="trav-close-icon" dir="auto"></i>
                        </div>
                        <div class="selected-areas-item">
                            <span>Paris</span>
                            <i class="trav-close-icon" dir="auto"></i>
                        </div>
                        <div class="selected-areas-item">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/ma.png" alt="">
                            <span>Morocco</span>
                            <i class="trav-close-icon" dir="auto"></i>
                        </div>
                        <div class="selected-areas-item">
                            <span>Paris</span>
                            <i class="trav-close-icon" dir="auto"></i>
                        </div>
                        <div class="selected-areas-item">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/ma.png" alt="">
                            <span>Morocco</span>
                            <i class="trav-close-icon" dir="auto"></i>
                        </div>
                        <div class="selected-areas-item">
                            <span>Paris</span>
                            <i class="trav-close-icon" dir="auto"></i>
                        </div>
                    </div>
                    <div class="form-group description">
                        <p>Suggestions</p>
                    </div>
                    <div class="form-group suggested-areas">
                        <div class="suggested-areas-item">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/ma.png" alt="">
                            <span>Morocco</span>
                        </div>
                        <div class="suggested-areas-item">
                            <span>Paris</span>
                        </div>
                        <div class="suggested-areas-item">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/ma.png" alt="">
                            <span>Morocco</span>
                        </div>
                        <div class="suggested-areas-item">
                            <span>Paris</span>
                        </div>
                        <div class="suggested-areas-item">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/ma.png" alt="">
                            <span>Morocco</span>
                        </div>
                        <div class="suggested-areas-item">
                            <span>Paris</span>
                        </div>
                        <div class="suggested-areas-item">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/ma.png" alt="">
                            <span>Morocco</span>
                        </div>
                        <div class="suggested-areas-item">
                            <span>Paris</span>
                        </div>
                        <div class="suggested-areas-item">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/ma.png" alt="">
                            <span>Morocco</span>
                        </div>
                        <div class="suggested-areas-item">
                            <span>Paris</span>
                        </div>
                    </div>
                    <div class="form-group bottom">
                        <button type="button" class="btn btn-light-primary">Become an Expert</button>
                    </div>
                </form>
            </div>  
        </div>
    </div>        
</div>
<!-- Become an Expert popup -->