<!-- Primary post block - text -->
<div class="post-block">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <a class="post-name-link" href="https://travooo.com/profile/831">
                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                </a>
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="https://travooo.com/profile/831">Ivan Turlakov</a>
                    <span class="exp-icon">EXP</span>
                </div>
                <div class="post-info">
                    Uploaded <strong>a video</strong> 15 July at 4:35am <img src="{{asset('assets2/image/globe-mobile.png')}}" width="49" alt="">
                    <!-- New element -->
                    <!-- New element END -->
                </div>
            </div>
        </div>
        <!-- New elements -->
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-user-plus-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Unfollow User</b></p>
                            <p>Stop seeing posts from User</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-share-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Share</b></p>
                            <p>Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-link"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Copy Link</b></p>
                            <p>Paste the link anyywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#sendToFriendModal">
                        <span class="icon-wrap">
                            <i class="trav-share-on-travo-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Send to a Friend</b></p>
                            <p>Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-heart-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Add to Favorites</b></p>
                            <p>Save it for later</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-flag-icon-o"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Report</b></p>
                            <p>Help us understand</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- New elements END -->
    </div>
    <!-- New element -->
    <div class="post-txt-wrap">
        <p>Dolor sit amet <span class="post-text-tag" type="button" data-placement="bottom" data-toggle="popover" data-html="true">Amine</span> consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est.
            Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. </p>
    </div>
    <div class="check-in-point">
        <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon"></i> 13 Jun 2020 at 09:50AM
    </div>
    <!-- Post text tag tooltip content -->
    <div id="popover-content" style="display: none;">
        <div class="user-hero">
            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
            <div class="user-hero-text">
                <div class="name">Sue Perez</div>
                <div class="location">
                    <i class="trav-set-location-icon"></i> United States
                </div>
            </div>
            <button type="button" class="btn btn-light-primary btn-bordered">Follow </button>
        </div>
        <div class="user-badges">
            <div class="badges-ttl">Badges</div>
            <div class="badges-list">
                <img src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
                <img src="https://travooo.com/assets2/image/badges/5_1.png" alt="Personality">
                <img src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
                <img class="fade" src="https://travooo.com/assets2/image/badges/5_1.png" alt="Personality">
                <img class="fade" src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
            </div>
            <button type="button" class="btn btn-light-primary btn-bordered">Follow </button>
        </div>
        <div class="actions">
            <a class="btn btn-light-grey">Follow</a>
            <a class="btn btn-light-grey popover-msg-btn">Message</a>
        </div>
    </div>
    <!-- Post text tag tooltip content END -->
    <!-- New element END -->
    <div class="post-image-container">
        <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;">
            <li style="padding: 0;position: relative;margin:0 0 0 0;overflow: hidden;">
                <video width="100%" height="auto" controls="" class="fullwidth">
                    <source src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1599583946_160_1591890126_1024897622-preview.mp4#t=5" type="video/mp4"> Your browser does not support the video tag.
                </video>
                <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
            </li>
        </ul>
    </div>


    <div class="post-footer-info image--post">
        <!-- Updated elements -->
        <div class="post-foot-block post-reaction">
            <span class="post_like_button" id="601451">
                <a href="#">
                    <i class="trav-heart-fill-icon"></i>
                </a>
            </span>
            <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                <span class="mobile--comment">13</span><span class="hidden--comment">likes</span>
            </span>
        </div>
        <!-- Updated element END -->
        <div class="post-foot-block comments--wrap">
            <a href="#" data-tab="comments601451">
                <img src="{{asset('assets2/image/comment-mobile.png')}}" width="49" alt="">
                <div class="mobile--reverse">
                    <ul class="foot-avatar-list">
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                        </li>
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                        </li>
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                        </li>
                    </ul>
                    <span>
                        <a href="#" data-tab="comments601451"><span class="601451-comments-count"><span class="mobile--comment">20</span> <span class="hidden--comment">Comments</span></a>
                    </span>
            </div>
        </div>
        <div class="post-foot-block ml-auto">
            <span class="post_share_button" id="601451">
                            <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                                <img src="{{asset('assets2/image/share-mobile.png')}}" width="49" alt="">
                            </a>
                        </span>
            <span id="post_share_count_601451"><a href="#" data-tab="shares601451" data-toggle="modal" data-target="#usersWhoShare"><span class="mobile--comment">3</span> <span class="hidden--comment">Shares</span></a>
            </span>
        </div>
    </div>
    <!-- Copied from home page -->
    <div class="post-comment-layer" data-content="comments622665" style="display: block;">
        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">Top</li>
                <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">New</li>
            </ul>
            <div class="comm-count-info">
                <strong>2</strong> / <span class="622665-comments-count">2</span>
            </div>
        </div>
        <div class="post-comment-wrapper sortBody" id="comments622665" travooo-comment-rows="2" travooo-current-page="1">
            <div topsort="0" newsort="1601563217" class="displayed" style="">
                <div class="post-comment-row news-feed-comment" id="commentRow13235">
                    <div class="post-com-avatar-wrap">
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Ivan Turlakov</a>
                            <div class="post-com-top-action">
                                <div class="dropdown " style="display: none;">
                                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="trav-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                                        <a href="javascript:;" class="dropdown-item post-edit-comment" data- data-post="622665">
                                            <span class="icon-wrap">
                                                <i class="trav-pencil" aria-hidden="true"></i>
                                            </span>
                                            <div class="drop-txt comment-edit__drop-text">
                                                <p><b>Edit</b></p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item postCommentDelete" data-post="622665" data-type="1">
                                            <span class="icon-wrap">
                                                <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;">
                                            </span>
                                            <div class="drop-txt comment-delete__drop-text">
                                                <p><b>Delete</b></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-txt comment-text-13235">
                            <p></p>
                            <form class="commentEditForm13235 comment-edit-form d-none" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR">
                                <input type="hidden" data-id="pair13235" name="pair" value="5f770bda7bc30">
                                <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0">
                                    <div class="post-create-input p-create-input b-whitesmoke">
                                        <textarea name="text" data-id="text13235" class="textarea-customize post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="Write a comment"></textarea>
                                    </div>
                                    <div class="post-create-controls b-whitesmoke d-none">
                                        <div class="post-alloptions">
                                            <ul class="create-link-list">
                                                <li class="post-options">
                                                    <input type="file" name="file[]" class="commenteditfile" id="commenteditfile13235" style="display:none" multiple="">
                                                    <i class="fa fa-camera click-target" data-target="commenteditfile13235"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="comment-edit-action">
                                            <a href="javascript:;" class="edit-cancel-link" data-comment_>Cancel</a>
                                            <a href="javascript:;" class="edit-post-link" data-comment_>Post</a>
                                        </div>
                                    </div>
                                    <div class="medias p-media b-whitesmoke">
                                        <div class="img-wrap-newsfeed">
                                            <div>
                                                <img class="thumb" src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="">
                                            </div>
                                            <span class="close remove-media-file" data-media_id="14592485">
                                                <span>×</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="post_id" value="622665">
                                <input type="hidden" name="comment_id" value="13235">
                                <input type="hidden" name="comment_type" value="1">
                                <button type="submit" class="d-none"></button>
                            </form>
                        </div>
                        <div class="post-image-container">
                            <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;">
                                <li style="overflow: hidden;margin:1px;">
                                    <a href="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" data-lightbox="comment__media13235">
                                        <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" style="width:192px;height:210px;object-fit: cover;">
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="comment-info-content">
                                <div class="dropdown">
                                    <a href="#" class="postCommentLikes like-link dropbtn"><i class="trav-heart-fill-icon " aria-hidden="true"></i> <span class="comment-like-count">0</span></a>
                                </div>
                                <a href="#" class="postCommentReply reply-link">Reply</a>
                                <span class="com-time"><span class="comment-dot"> · </span>20 hours ago</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-add-comment-block" id="replyForm13235" style="padding-top:0px;padding-left: 59px;display:none;">
                    <div class="avatar-wrap" style="padding-right:10px">
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" style="width:30px !important;height:30px !important;">
                    </div>

                </div>
            </div>
            <div topsort="1" newsort="1601563197" class="displayed" style="">
                <div class="post-comment-row news-feed-comment" id="commentRow13234">
                    <div class="post-com-avatar-wrap">
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Ivan Turlakov</a>
                            <div class="post-com-top-action">
                                <div class="dropdown ">
                                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="trav-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                                        <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="13234" data-post="622665">
                                            <span class="icon-wrap">
                                                <i class="trav-pencil" aria-hidden="true"></i>
                                            </span>
                                            <div class="drop-txt comment-edit__drop-text">
                                                <p><b>Edit</b></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-txt comment-text-13234">
                            <p>123Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id erat a velit mollis consectetur nec quis nunc. Morbi a neque vel lacus tincidunt vehicula non non justo.</p>
                            <form class="commentEditForm13234 comment-edit-form d-none" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR">
                                <input type="hidden" data-id="pair13234" name="pair" value="5f770bda7ee63">
                                <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0">
                                    <div class="post-create-input p-create-input b-whitesmoke">
                                        <textarea name="text" data-id="text13234" class="textarea-customize post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="Write a comment">Text comment</textarea>
                                    </div>
                                    <div class="post-create-controls b-whitesmoke d-none">
                                        <div class="post-alloptions">
                                            <ul class="create-link-list">
                                                <li class="post-options">
                                                    <input type="file" name="file[]" class="commenteditfile" id="commenteditfile13234" style="display:none" multiple="">
                                                    <i class="fa fa-camera click-target" data-target="commenteditfile13234"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="comment-edit-action">
                                            <a href="javascript:;" class="edit-cancel-link" data-comment_id="13234">Cancel</a>
                                            <a href="javascript:;" class="edit-post-link" data-comment_id="13234">Post</a>
                                        </div>
                                    </div>
                                    <div class="medias p-media b-whitesmoke">
                                    </div>
                                </div>
                                <input type="hidden" name="post_id" value="622665">
                                <input type="hidden" name="comment_id" value="13234">
                                <input type="hidden" name="comment_type" value="1">
                                <button type="submit" class="d-none"></button>
                            </form>
                        </div>
                        <div class="post-image-container">
                        </div>
                        <div class="comment-bottom-info">
                            <div class="comment-info-content">
                                <div class="dropdown">
                                    <a href="#" class="postCommentLikes like-link dropbtn" id="13234"><i class="trav-heart-fill-icon red" aria-hidden="true"></i> <span class="comment-like-count">0</span></a>
                                </div>
                                <a href="#" class="postCommentReply reply-link" id="13234">Reply</a>
                                <span class="com-time"><span class="comment-dot"> · </span>20 hours ago</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-comment-row doublecomment post-comment-reply" id="commentRow13236" data-parent_id="13234">
                    <div class="post-com-avatar-wrap">
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Ivan Turlakov</a>
                            <div class="post-com-top-action">
                                <div class="dropdown" style="display: none;">
                                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="trav-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                                        <a href="#" class="dropdown-item post-edit-comment" data-id="13236" data-post="622665">
                                            <span class="icon-wrap">
                                                <i class="trav-pencil" aria-hidden="true"></i>
                                            </span>
                                            <div class="drop-txt comment-edit__drop-text">
                                                <p><b>Edit</b></p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item postCommentDelete" id="13236" data-parent="13234" data-type="2">
                                            <span class="icon-wrap">
                                                <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;">
                                            </span>
                                            <div class="drop-txt comment-delete__drop-text">
                                                <p><b>Delete</b></p>
                                            </div>
                                        </a>
                                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(13236,this)">
                                            <span class="icon-wrap">
                                                <i class="trav-flag-icon-o"></i>
                                            </span>
                                            <div class="drop-txt comment-report__drop-text">
                                                <p><b>Report</b></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-txt comment-text-13236">
                            <form class="commentEditForm13236 comment-edit-form d-none" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR">
                                <input type="hidden" data-id="pair13236" name="pair" value="5f770bda80259">
                                <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0">
                                    <div class="post-create-input p-create-input b-whitesmoke">
                                        <textarea name="text" id="text13236" class="textarea-customize  post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="Write a comment"></textarea>
                                    </div>
                                    <div class="post-create-controls b-whitesmoke d-none">
                                        <div class="post-alloptions">
                                            <ul class="create-link-list">
                                                <li class="post-options">
                                                    <input type="file" name="file[]" class="commenteditfile" id="commenteditfile13236" style="display:none" multiple="">
                                                    <i class="fa fa-camera click-target" data-target="commenteditfile13236"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="comment-edit-action">
                                            <a href="javascript:;" class="edit-cancel-link" data-comment_id="13236">Cancel</a>
                                            <a href="javascript:;" class="edit-post-link" data-comment_id="13236">Post</a>
                                        </div>
                                    </div>
                                    <div class="medias p-media b-whitesmoke">
                                        <div class="img-wrap-newsfeed">
                                            <div>
                                                <img class="thumb" src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563234_images.jpg" alt="">
                                            </div>
                                            <span class="close remove-media-file" data-media_id="14592486">
                                                <span>×</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="post_id" value="622665">
                                <input type="hidden" name="comment_id" value="13236">
                                <input type="hidden" name="comment_type" value="2">
                                <button type="submit" class="d-none"></button>
                            </form>
                        </div>
                        <div class="post-image-container">
                            <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;">
                                <li style="overflow: hidden;margin:1px;">
                                    <a href="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563234_images.jpg" data-lightbox="comment__replymedia13236">
                                        <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563234_images.jpg" alt="" style="width:192px;height:210px;object-fit: cover;">
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="comment-info-content" posttype="Postcomment">
                                <div class="dropdown">
                                    <a href="#" class="postCommentLikes like-link" id="13236"><i class="trav-heart-fill-icon " aria-hidden="true"></i> <span class="comment-like-count">0</span></a>
                                </div>
                                <span class="com-time"><span class="comment-dot"> · </span>20 hours ago</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-add-comment-block" id="replyForm13234" style="padding-top:0px;padding-left: 59px;display:none;">
                    <div class="avatar-wrap" style="padding-right:10px">
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" style="width:30px !important;height:30px !important;">
                    </div>
                    <div class="post-add-com-inputs">
                        <form class="commentReplyForm13234" method="POST" action="https://travooo.com/new-comment" autocomplete="off" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="G3Qx6CBzRnuKzGiiXT7y5LE8vKRd5z7dbBKgJcRR">
                            <input type="hidden" data-id="pair13234" name="pair" value="5f770bda8135d">
                            <div class="post-create-block post-comment-create-block post-reply-block" tabindex="0">
                                <div class="post-create-input p-create-input b-whitesmoke">
                                    <textarea name="text" data-id="text13234" class="textarea-customize post-comment-emoji" style="display: none; vertical-align: top; min-height: 50px;" placeholder="Write a comment"></textarea>
                                    <div class="note-editor note-frame panel panel-default">
                                        <div class="note-dropzone">
                                            <div class="note-dropzone-message"></div>
                                        </div>
                                        <div class="note-toolbar panel-heading" role="toolbar">
                                            <div class="note-btn-group btn-group note-insert">
                                                <button type="button" class="note-btn btn btn-default btn-sm" role="button" tabindex="-1">
                                                    <div class="emoji-picker-container emoji-picker emojionearea-button d-none"></div>
                                                </button>
                                                <div class="emoji-menu 00b5dc61-5c5c-40de-9479-07713f85c370" style="display: none;">

                                                </div>
                                            </div>
                                            <div class="note-btn-group btn-group note-custom"></div>
                                        </div>
                                        <div class="note-editing-area">
                                            <div class="note-placeholder custom-placeholder" style="display: none;">Write a comment</div>
                                            <div class="note-handle">
                                                <div class="note-control-selection">
                                                    <div class="note-control-selection-bg"></div>
                                                    <div class="note-control-holder note-control-nw"></div>
                                                    <div class="note-control-holder note-control-ne"></div>
                                                    <div class="note-control-holder note-control-sw"></div>
                                                    <div class="note-control-sizing note-control-se"></div>
                                                    <div class="note-control-selection-info"></div>
                                                </div>
                                            </div>
                                            <textarea class="note-codable" role="textbox" aria-multiline="true"></textarea>
                                            <div class="note-editable" contenteditable="true" role="textbox" aria-multiline="true" spellcheck="true">
                                                <div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                        <output class="note-status-output" aria-live="polite"></output>
                                    </div>
                                    <div class="textcomplete-wrapper"></div>
                                </div>
                                <div class="post-create-controls b-whitesmoke d-none">
                                    <div class="post-alloptions">
                                        <ul class="create-link-list">
                                            <li class="post-options">
                                                <input type="file" name="file[]" id="commentreplyfile13234" style="display:none" multiple="">
                                                <i class="fa fa-camera click-target" data-target="commentreplyfile13234"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <button type="submit" class="btn btn-primary d-none"></button>
                                    <div class="comment-edit-action">
                                        <a href="javascript:;" class="p-comment-cancel-link post-r-cancel-link">Cancel</a>
                                        <a href="javascript:;" class="p-comment-link post-r-reply-comment-link" data-comment_id="13234">Post</a>
                                    </div>
                                </div>
                                <div class="medias p-media b-whitesmoke"></div>
                            </div>
                            <input type="hidden" name="post_id" value="622665">
                            <input type="hidden" name="comment_id" value="13234">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="load-more-comments">Load more...</a>
        <div class="post-add-comment-block">
            <div class="avatar-wrap">
                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" style="width:45px;height:45px;">
            </div>
            <!-- New elements -->
            <div class="add-comment-input-group">
                <input type="text" placeholder="Write a comment...">
                <div>
                    <button class="add-post__emoji" emoji-target="add_post_text" type="button" onclick="showFaceBlock()" id="faceEnter">🙂</button>
                    <button><i class="trav-camera click-target" data-target="file"></i></button>
                </div>
            </div>
            <!-- New elements END  -->
        </div>
    </div>
    <!-- Copied from home page END -->
    <div class="post-comment-layer" data-content="shares601451" style="display:none;">
        <div class="post-comment-wrapper" id="shares601451">
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        // Video
        $('.v-play-btn, .video-play-btn').click(function(){
            $(this).siblings('video').attr('controls', true);
            $(this).siblings('video').get(0).play();
            $(this).toggleClass('hide');
        });
        $('.post-image-container video, .post-block-trending-videos video').on('playing', function () {
            $(this).siblings('.v-play-btn, .video-play-btn').addClass('hide')
        });
        $('.post-image-container video, .post-block-trending-videos video').on('pause', function () {
            $(this).siblings('.v-play-btn, .video-play-btn').removeClass('hide')
        });
    })
</script>
<!-- Primary post block - text END -->