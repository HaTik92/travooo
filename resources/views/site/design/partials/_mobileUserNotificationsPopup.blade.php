<!-- Mobile User-notifications popup -->
<div class="mobile-user-notifications-popup">
    <button type="button" class="close"><i class="trav-angle-left"></i></button>
    <a href="/" class="header-ttl">Travooo</a>
    <ul class="nav mobile-user-notifications-filter" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="mobile-user-notifications-all-tab" data-toggle="pill" href="#mobile-user-notifications-all" role="tab" aria-controls="pills-all" aria-selected="false">All Notifications</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="mobile-user-notifications-content-tab" data-toggle="pill" href="#mobile-user-notifications-content" role="tab" aria-controls="pills-all" aria-selected="false">Your Content</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="mobile-user-notifications-friends-tab" data-toggle="pill" href="#mobile-user-notifications-friends" role="tab" aria-controls="pills-all" aria-selected="false">Your Friends</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane show active" id="mobile-user-notifications-all" role="tabpanel" aria-labelledby="mobile-user-notifications-all-tab">
            <div class="mobile-user-notifications-block">
                <div class="mobile-user-notifications-item">
                    <a href="#" class="user">
                        <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/326/1594005435_image.png" alt="">
                        <span class="icon">
                            <img src="{{asset('assets2/image/trip-plan.svg')}}" alt="">
                        </span>
                    </a>
                    <div class="text">
                        <span class="info"><a href="">Stephen Bugno</a> shared a <a href="">Trip Plan</a></span>
                        <span class="time">3 hours ago</span>
                    </div>
                </div>
                <div class="mobile-user-notifications-item">
                    <a href="#" class="user">
                        <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/326/1594005435_image.png" alt="">
                        <span class="icon">
                            <img src="{{asset('assets2/image/travelog.svg')}}" alt="">
                        </span>
                    </a>
                    <div class="text">
                        <span class="info"><a href="">John Doe</a> commented your <a href="">Post</a></span>
                        <span class="time">7 hours ago</span>
                    </div>
                </div>
                <div class="mobile-user-notifications-item">
                    <a href="#" class="user">
                        <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/326/1594005435_image.png" alt="">
                        <span class="icon">
                            <img src="{{asset('assets2/image/ask.svg')}}" alt="">
                        </span>
                    </a>
                    <div class="text">
                        <span class="info"><a href="">Sarah White</a> and +5 more reacted on your <a href="">Photo</a></span>
                        <span class="time">3 hours ago</span>
                    </div>
                </div>
                <div class="mobile-user-notifications-item">
                    <a href="#" class="user">
                        <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/326/1594005435_image.png" alt="">
                        <span class="icon">
                            <img src="{{asset('assets2/image/travel-mates.svg')}}" alt="">
                        </span>
                    </a>
                    <div class="text">
                        <span class="info"><a href="">Jerome Wilson</a> asked a question</span>
                        <span class="time">3 hours ago</span>
                    </div>
                </div>
                <div class="mobile-user-notifications-item">
                    <div class="loading">Loading...</div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="mobile-user-notifications-content" role="tabpanel" aria-labelledby="mobile-user-notifications-content-tab">
            <div class="mobile-user-notifications-block">
                <div class="mobile-user-notifications-item">
                    <a href="#" class="user">
                        <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/326/1594005435_image.png" alt="">
                        <span class="icon">
                            <img src="{{asset('assets2/image/travelog.svg')}}" alt="">
                        </span>
                    </a>
                    <div class="text">
                        <span class="info"><a href="">John Doe</a> commented your <a href="">Post</a></span>
                        <span class="time">7 hours ago</span>
                    </div>
                </div>
                <div class="mobile-user-notifications-item">
                    <a href="#" class="user">
                        <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/326/1594005435_image.png" alt="">
                        <span class="icon">
                            <img src="{{asset('assets2/image/ask.svg')}}" alt="">
                        </span>
                    </a>
                    <div class="text">
                        <span class="info"><a href="">Sarah White</a> and +5 more reacted on your <a href="">Photo</a></span>
                        <span class="time">3 hours ago</span>
                    </div>
                </div>
                <div class="mobile-user-notifications-item">
                    <div class="loading">Loading...</div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="mobile-user-notifications-friends" role="tabpanel" aria-labelledby="mobile-user-notifications-friends-tab">
            <div class="mobile-user-notifications-block">
                <div class="mobile-user-notifications-item">
                    <a href="#" class="user">
                        <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/326/1594005435_image.png" alt="">
                        <span class="icon">
                            <img src="{{asset('assets2/image/trip-plan.svg')}}" alt="">
                        </span>
                    </a>
                    <div class="text">
                        <span class="info"><a href="">Stephen Bugno</a> shared a <a href="">Trip Plan</a></span>
                        <span class="time">3 hours ago</span>
                    </div>
                </div>
                <div class="mobile-user-notifications-item">
                    <a href="#" class="user">
                        <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/326/1594005435_image.png" alt="">
                        <span class="icon">
                            <img src="{{asset('assets2/image/travel-mates.svg')}}" alt="">
                        </span>
                    </a>
                    <div class="text">
                        <span class="info"><a href="">Jerome Wilson</a> asked a question</span>
                        <span class="time">3 hours ago</span>
                    </div>
                </div>
                <div class="mobile-user-notifications-item">
                    <div class="loading">Loading...</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.mobile-user-notifications-button').click(function () {
        $('.mobile-user-notifications-popup').addClass('show');
    });
    $('.mobile-user-notifications-popup .close').click(function () {
        $('.mobile-user-notifications-popup').removeClass('show');
    });
</script>
<!-- Mobile User-notifications popup -->