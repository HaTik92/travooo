<!-- Milestone popup -->
<div class="modal fade white-style" data-backdrop="false" id="milestonePopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog milestone-popup-dialog" role="document">
        <div class="milestone-popup-top">
            <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                <i class="trav-close-icon"></i>
            </button>
            <img src="https://travooo.com/assets2/image/placeholders/pattern.png" alt="">
            <div class="exp-badge-transition">
                <div class="exp-badge-transition-wrapper">
                    <span class="exp-icon big">EXP</span>
                </div>
                <div class="exp-badge-transition-wrapper">
                    <span class="exp-icon big bronze">EXP</span>
                </div>
            </div>
        </div>
        <div class="milestone-popup-text">
            <h4>Thank you!</h4>
            <p>You application for the Bronze Badge is under review, you will be notified via email very soon.</p>
            <button class="btn btn-light-primary">Got it</button>
        </div>
    </div>
</div>
<!-- Milestone popup END -->