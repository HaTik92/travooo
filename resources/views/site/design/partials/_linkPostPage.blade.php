<!-- Primary post block - text -->
<div class="post-block">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <a class="post-name-link" href="https://travooo.com/profile/831">
                    <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                </a>
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="https://travooo.com/profile/831">Stephen Bugno</a>
                    <span class="exp-icon">EXP</span>
                </div>
                <div class="post-info">
                    15 July at 4:35am
                    <i class="trav-globe-icon"></i>
                    <!-- New element -->
                    <!-- New element END -->
                </div>
            </div>
        </div>
        <!-- New elements -->
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-user-plus-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Unfollow User</b></p>
                            <p>Stop seeing posts from User</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-share-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Share</b></p>
                            <p>Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-link"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Copy Link</b></p>
                            <p>Paste the link anyywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#sendToFriendModal">
                        <span class="icon-wrap">
                            <i class="trav-share-on-travo-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Send to a Friend</b></p>
                            <p>Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-heart-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Add to Favorites</b></p>
                            <p>Save it for later</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap">
                            <i class="trav-flag-icon-o"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Report</b></p>
                            <p>Help us understand</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- New elements END -->
    </div>
    <!-- New element -->
    <div class="post-txt-wrap">
        <p>Sed ultricies quam id mattis venenati vivamus sapien purus, tincidunt quis aliquet vitae, eleifend nec sem.</p>
    </div>
    <div class="shared-link-details">
        <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1592067226_whistler2-675x390-c0d.jpg" alt="">
        <div class="bottom">
            <div class="text">
                <h4>200 Days - A Trip Around the World Travel Film</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit praesent vitae...</p>
            </div>
            <a href="#">
                <i class="fas fa-external-link-alt"></i>
            </a>
        </div>
    </div>
    <!-- New element END -->
    <div class="post-footer-info image--post">
        <!-- Updated elements -->
        <div class="post-foot-block post-reaction">
            <span class="post_like_button" id="601451">
        <a href="#">
            <i class="trav-heart-fill-icon"></i>
        </a>
    </span>
            <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
        <span class="mobile--comment">13</span><span class="hidden--comment">likes</span>
            </span>
        </div>
        <!-- Updated element END -->
        <div class="post-foot-block comments--wrap">
            <a href="#" data-tab="comments601451">
                <img src="{{asset('assets2/image/comment-mobile.png')}}" width="49" alt="">
                <div class="mobile--reverse">
                    <ul class="foot-avatar-list">
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                        </li>
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                        </li>
                        <li>
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">
                        </li>
                    </ul>

                    <span><a href="#" data-tab="comments601451"><span class="601451-comments-count"><span class="mobile--comment">20</span> <span class="hidden--comment">Comments</span></a>
            </span>
            </div>
        </div>
        <div class="post-foot-block ml-auto">
            <span class="post_share_button" id="601451">
                <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                    <img src="{{asset('assets2/image/share-mobile.png')}}" width="49" alt="">
                </a>
            </span>
            <span id="post_share_count_601451"><a href="#" data-tab="shares601451" data-toggle="modal" data-target="#usersWhoShare"><span class="mobile--comment">3</span> <span class="hidden--comment">Shares</span></a>
            </span>
        </div>
    </div>
    <div class="post-comment-wrapper" id="following601451">
    </div>
    <!-- Copied from home page -->
    <div class="post-comment-layer" data-content="comments622665" style="display: block;">
        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">Top</li>
                <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">New</li>
            </ul>
            <div class="comm-count-info">
                <strong>2</strong> / <span class="622665-comments-count">2</span>
            </div>
        </div>
        <div class="post-comment-wrapper sortBody" id="comments622665"  travooo-comment-rows="2" travooo-current-page="1">
            <div topsort="0" newsort="1601563217"  class="displayed" style="">
                <div class="post-comment-row news-feed-comment" id="commentRow13235">
                    <div class="post-com-avatar-wrap">
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Ivan Turlakov</a>
                            <div class="post-com-top-action">
                                <div class="dropdown "  style="display: none;">
                                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="trav-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                                        <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="13235" data-post="622665">
                                            <span class="icon-wrap">
                                                <i class="trav-pencil" aria-hidden="true"></i>
                                            </span>
                                            <div class="drop-txt comment-edit__drop-text">
                                                <p><b>Edit</b></p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item postCommentDelete" id="13235" data-post="622665" data-type="1">
                                            <span class="icon-wrap">
                                                <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;">
                                            </span>
                                            <div class="drop-txt comment-delete__drop-text">
                                                <p><b>Delete</b></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="post-image-container">
                            <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;">
                                <li style="overflow: hidden;margin:1px;">
                                    <a href="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" data-lightbox="comment__media13235">
                                        <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="" style="width:192px;height:210px;object-fit: cover;">
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="comment-info-content">
                                <div class="dropdown">
                                    <a href="#" class="postCommentLikes like-link dropbtn" id="13235"><i class="trav-heart-fill-icon " aria-hidden="true"></i> <span class="comment-like-count">0</span></a>
                                </div>
                                <a href="#" class="postCommentReply reply-link" id="13235">Reply</a>
                                <span class="com-time"><span class="comment-dot"> · </span>20 hours ago</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div topsort="1" newsort="1601563197"  class="displayed" style="">
                <div class="post-comment-row news-feed-comment" id="commentRow13234">
                    <div class="post-com-avatar-wrap">
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Katherine</a>
                            <div class="post-com-top-action">
                                <div class="dropdown ">
                                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="trav-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                                        <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="13234" data-post="622665">
                                            <span class="icon-wrap">
                                                <i class="trav-pencil" aria-hidden="true"></i>
                                            </span>
                                            <div class="drop-txt comment-edit__drop-text">
                                                <p><b>Edit</b></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-image-container">
                        </div>
                        <div class="comment-bottom-info">
                            <div class="comment-info-content">
                                <div class="dropdown">
                                    <a href="#" class="postCommentLikes like-link dropbtn" id="13234"><i class="trav-heart-fill-icon red" aria-hidden="true"></i> <span class="comment-like-count">0</span></a>
                                </div>
                                <a href="#" class="postCommentReply reply-link" id="13234">Reply</a>
                                <span class="com-time"><span class="comment-dot"> · </span>20 hours ago</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-comment-row doublecomment post-comment-reply" id="commentRow13236" data-parent_id="13234">
                    <div class="post-com-avatar-wrap">
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Ivan Turlakov</a>
                            <div class="post-com-top-action">
                                <div class="dropdown"  style="display: none;">
                                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="trav-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                                        <a href="#" class="dropdown-item post-edit-comment" data-id="13236" data-post="622665">
                                            <span class="icon-wrap">
                                                <i class="trav-pencil" aria-hidden="true"></i>
                                            </span>
                                            <div class="drop-txt comment-edit__drop-text">
                                                <p><b>Edit</b></p>
                                            </div>
                                        </a>
                                        <a href="javascript:;" class="dropdown-item postCommentDelete" id="13236" data-parent="13234" data-type="2">
                                            <span class="icon-wrap">
                                                <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;">
                                            </span>
                                            <div class="drop-txt comment-delete__drop-text">
                                                <p><b>Delete</b></p>
                                            </div>
                                        </a>
                                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData(13236,this)">
                                            <span class="icon-wrap">
                                                <i class="trav-flag-icon-o"></i>
                                            </span>
                                            <div class="drop-txt comment-report__drop-text">
                                                <p><b>Report</b></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-image-container">
                            <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;">
                                <li style="overflow: hidden;margin:1px;">
                                    <a href="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563234_images.jpg" data-lightbox="comment__replymedia13236">
                                        <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563234_images.jpg" alt="" style="width:192px;height:210px;object-fit: cover;">
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="comment-info-content" posttype="Postcomment">
                                <div class="dropdown">
                                    <a href="#" class="postCommentLikes like-link" id="13236"><i class="trav-heart-fill-icon " aria-hidden="true"></i> <span class="comment-like-count">0</span></a>
                                </div>
                                <span class="com-time"><span class="comment-dot"> · </span>20 hours ago</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="load-more-comments">Load more...</a>
        <div class="post-add-comment-block">
            <div class="avatar-wrap">
                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png" alt="" style="width:45px;height:45px;">
            </div>
            <!-- New elements -->
            <div class="add-comment-input-group">
                <input type="text" placeholder="Write a comment...">
                <div>
                    <button class="add-post__emoji" emoji-target="add_post_text" type="button" onclick="showFaceBlock()" id="faceEnter">🙂</button>
                    <button><i class="trav-camera click-target" data-target="file"></i></button>
                </div>
            </div>
            <!-- New elements END  -->
        </div>
    </div>
    <!-- Copied from home page END -->
    <div class="post-comment-layer" data-content="shares601451" style="display:none;">
        <div class="post-comment-wrapper" id="shares601451">
        </div>
    </div>
</div>
<!-- Primary post block - text END -->