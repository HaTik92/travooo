<!-- Mobile User-actions popup -->
<div class="mobile-user-actions-popup">
    <button type="button" class="close"><i class="fas fa-chevron-down"></i></button>
    <button type="button" class="slide"><i class="fas fa-chevron-down"></i></button>
    <ul class="mobile-user-actions-list">
        <li>
            <a href="javascript:;" class="mobile-user-actions-list-item cp-add-post" data-type="home" data-url="{{url('/home#create-post')}}">
                <img src="{{asset('assets2/image/write-post.svg')}}" alt="">
                <span>
                    <span class="action-title">Write a Post</span>
                    <span>Speak your mind User</span>
                </span>
            </a>
        </li>
        <li>
            <a href="https://travooo.com/trip/plan/0" class="mobile-user-actions-list-item">
                <img src="{{asset('assets2/image/trip-plan.svg')}}" alt="">
                <span>
                    <span class="action-title">Create a Trip Plan</span>
                    <span>Plan your next trip or create a new one</span>
                </span>
            </a>
        </li>
        <li>
            <a href="https://travooo.com/reports/list?order=newest&amp;" class="mobile-user-actions-list-item">
                <img src="{{asset('assets2/image/travelog.svg')}}" alt="">
                <span>
                    <span class="action-title">Post a Travelog</span>
                    <span>Tell your travel stories and show your creativity</span>
                </span>
            </a>
        </li>
        <li>
            <a href="https://travooo.com/travelmates-newest" class="mobile-user-actions-list-item">
                <img src="{{asset('assets2/image/travel-mates.svg')}}" alt="">
                <span>
                    <span class="action-title">Find a Travel Mate</span>
                    <span>Want to travel in group? find your mates here</span>
                </span>
            </a>
        </li>
        <li>
            <a href="https://travooo.com/discussions" class="mobile-user-actions-list-item">
                <img src="{{asset('assets2/image/ask.svg')}}" alt="">
                <span>
                    <span class="action-title">Ask a Question</span>
                    <span>Get answers from people with unique insights</span>
                </span>
            </a>
        </li>
        <li>
            <a href="" class="mobile-user-actions-list-item">
                <img src="{{asset('assets2/image/booking.svg')}}" alt="">
                <span>
                    <span class="action-title">Online Booking</span>
                    <span>Find a flight or a place to stay with ease</span>
                </span>
            </a>
        </li>
    </ul>      
</div>
<script>
    $('.mobile-user-actions-button').click(function () {
        $('.mobile-user-actions-popup').addClass('show');
        $('body').css('overflow', 'hidden');
    });
    $('.mobile-user-actions-popup .close').click(function () {
        $('.mobile-user-actions-popup').removeClass('show');
        $('body').css('overflow', 'auto');
    });
    $('.mobile-user-actions-popup .slide').click(function () {
        $('.mobile-user-actions-popup').toggleClass('slide-up');
    });
    $('.mobile-user-actions-popup .close').click(function () {
        $('.mobile-user-actions-popup').removeClass('show');
    });
</script>
<!-- Mobile User-actions popup -->