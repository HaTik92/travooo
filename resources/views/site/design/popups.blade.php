<!DOCTYPE html>
<html class="page" lang="en">
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="date=no">
        <meta name="format-detection" content="address=no">
        <meta name="format-detection" content="email=no">
        <meta content="notranslate" name="google">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
         <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css">
        <!-- <link rel="stylesheet" href="{{asset('assets2/css/place.css?v='.time()) }}"> -->
        <link rel="stylesheet" href="{{asset('assets2/css/style-15102019-9.css')}}">
        <link rel="stylesheet" href="{{asset('assets2/css/travooo.css?v='.time())}}">
        <link rel="stylesheet" href="{{asset('assets3/css/datepicker-extended.css')}}">
        <!-- <link rel="stylesheet" type="text/css" href="{{asset('assets3/css/slider-pro.min.css')}}" media="screen"/> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
        <link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>

        <link href="https://dev.travooo.com/assets2/js/summernote/summernote.css" rel="stylesheet">

        <link rel="stylesheet" href="{{asset('assets3/css/star.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/lightslider.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> -->
        <script src="{{ asset('assets2/js/jquery-3.1.1.min.js') }}"></script>
        <script>
            var map_var = "";
            var map_func = "";

            // Design page scripts
            $(document).ready(function() {
                var isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints > 0;

                // Sticky left menu for desktop
                if(!isTouchDevice) {
                    var leftMenu = $('#leftOutsideMenu');
                    var mainHeader = $('.main-header');
                    $(window).scroll(function() {
                        var windowTop = $(window).scrollTop();
                        if (windowTop > mainHeader.height()) {
                            leftMenu.addClass('sticky');
                        } else {
                            leftMenu.removeClass('sticky');
                        }
                    });
                }

                $('.add-post__emoji').emojiInit({
                    fontSize:20,
                    success : function(data){

                    },
                    error : function(data,msg){
                    }
                });
                $('.add-msg__emoji').emojiInit({
                    fontSize:20,
                    success : function(data){

                    },
                    error : function(data,msg){
                    }
                });
                $('.checkin_date').datepicker({
                    format: 'M dd yy',
                });
                $('.checkin_time').timepicker({
                    timeFormat: 'h:i A'
                });
                // Create new post medias slider
                $('.medias.medias-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 10,
                    pager: false,
                    controls: false,
                    addClass: 'new-post-medias',
                });
                // Post type - shared place, slider
                $('.shared-place-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 22,
                    pager: false,
                    controls: false,
                });
                // More/less content
                $(document).on('click', ".read-more-link", function () {
                    $(this).closest('.post-txt-wrap').find('.less-content').hide()
                    $(this).closest('.post-txt-wrap').find('.more-content').show()
                    $(this).hide()
                });
                $(document).on('click', ".read-less-link", function () {
                    $(this).closest('.more-content').hide()
                    $(this).closest('.post-txt-wrap').find('.less-content').show()
                    $(this).closest('.post-txt-wrap').find('.read-more-link').show()
                });
                // Like button
                $(document).on('click', ".post_like_button a", function (e) {
                    e.preventDefault();
                    $(this).closest('.post_like_button').toggleClass('liked');
                });
                // Tooltips
                $('[data-toggle="tooltip"]').tooltip({
                    placement: 'top',
                    trigger: 'hover'
                });
                // Post text tag popover
                $(".post-text-tag").popover({
                    html: true, 
                    content: $('#popover-content').html(),
                    template: '<div class="popover bottom tagging-popover" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
                // Post more-people popover
                $(".post-more-people").popover({
                    html: true, 
                    content: $('#popover-content-people').html(),
                    template: '<div class="popover bottom tagging-popover white" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
                // Post holiday popover
                $(".post-holiday-popover").popover({
                    html: true, 
                    content: $('#popover-content-holiday').html(),
                    template: '<div class="popover bottom tagging-popover white" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'click',
                });
                // Map popover
                $(".map-popover").popover({
                    html: true, 
                    content: $('#popover-content-map').html(),
                    template: '<div class="popover top forecast-map-popover white" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
                // Trending videos slider
                let videoSlider = $('.trending-videos-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 20,
                    pager: false,
                    controls: false,
                });
                $('.video-slide-prev').click(function(e){
                    e.preventDefault();
                    videoSlider.goToPrevSlide(); 
                });
                $('.video-slide-next').click(function(e){
                    e.preventDefault();
                    videoSlider.goToNextSlide(); 
                });
                // Custom scrollbar
                $('.friend-search-results').mCustomScrollbar();
                // Extend button
                $('.extednd-btn').click(function(e){
                    e.preventDefault();
                    $(this).toggleClass('shrink');
                    $(this).closest('.share-to-friend').toggleClass('relative');
                });
                // Aside Photo Slider
                $('.aside-photo-slider').lightSlider({
                    item: 1,
                    loop: true,
                    slideMargin: 20,
                    pager: false,
                    enableDrag: false
                });
                // Aside Photo Slider Inner
                $('.openInnerSlider').click(function(){
                    if($('.aside-slider-modal .visible').length < 1) {
                        setTimeout(() => {
                            var sideInnerslider = $('.aside-photo-slider-inner').lightSlider({
                                gallery: true,
                                item: 1,
                                loop: true,
                                slideMargin: 20,
                                currentPagerPosition: 'middle',
                                thumbMargin: 10,
                                adaptiveHeight: true,
                                controls: false,
                                enableDrag: false,
                            });
                            $('.aside-slider-modal .wrapper').addClass('visible');
                            $('.side-inner-slider-prev').click(function(e){
                                e.preventDefault();
                                sideInnerslider.goToPrevSlide(); 
                            });
                            $('.side-inner-slider-next').click(function(e){
                                e.preventDefault();
                                sideInnerslider.goToNextSlide(); 
                            });
                    }, 50);
                    }
                });
                // Trip plan home slider
                var homeTripSlider = $('.trip-plan-home-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 10,
                    pager: false,
                    controls: false
                });
                $('.trip-plan-home-slider-prev').click(function(e){
                    e.preventDefault();
                    homeTripSlider.goToPrevSlide(); 
                });
                $('.trip-plan-home-slider-next').click(function(e){
                    e.preventDefault();
                    homeTripSlider.goToNextSlide(); 
                });
                // Recommended places slider
                var homeRecommendedePlacesSlider = $('.recommended-places-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 20,
                    pager: false,
                    controls: false
                });
                $('.recommended-places-slider-prev').click(function(e){
                    e.preventDefault();
                    homeRecommendedePlacesSlider.goToPrevSlide(); 
                });
                $('.recommended-places-slider-next').click(function(e){
                    e.preventDefault();
                    homeRecommendedePlacesSlider.goToNextSlide(); 
                });
                // Place you might like slider
                var placeYouMightLikeSlider = $('.places-you-might-like-slider').lightSlider({
                    autoWidth: true,
                    slideMargin: 20,
                    pager: false,
                    controls: false
                });
                $('.places-you-might-like-slider-prev').click(function(e){
                    e.preventDefault();
                    placeYouMightLikeSlider.goToPrevSlide(); 
                });
                $('.places-you-might-like-slider-next').click(function(e){
                    e.preventDefault();
                    placeYouMightLikeSlider.goToNextSlide(); 
                });
                // Video button
                $('.v-play-btn, .video-play-btn').click(function(){
                    $(this).siblings('video').get(0).play();
                    $(this).toggleClass('hide');
                });
                // Post prifile blocks slider
                $('.post-profile-blocks-slider').lightSlider({
                    item: 1,
                    loop: true,
                    slideMargin: 10,
                    pager: false,
                    controls: true
                });
                // Place you might like slider
                var newTravelerDiscover = $('#newTravelerDiscover').lightSlider({
                    autoWidth: true,
                    slideMargin: 20,
                    pager: false,
                    controls: false
                });
                $('.newTravelerDiscover-prev').click(function(e){
                    e.preventDefault();
                    newTravelerDiscover.goToPrevSlide(); 
                });
                $('.newTravelerDiscover-next').click(function(e){
                    e.preventDefault();
                    newTravelerDiscover.goToNextSlide(); 
                });
                // Trending destinations slider
                var discoverNewDestination = $('#discoverNewDestination').lightSlider({
                    autoWidth: true,
                    slideMargin: 20,
                    pager: false,
                    controls: false
                });
                $('.discoverNewDestination-prev').click(function(e){
                    e.preventDefault();
                    discoverNewDestination.goToPrevSlide(); 
                });
                $('.discoverNewDestination-next').click(function(e){
                    e.preventDefault();
                    discoverNewDestination.goToNextSlide(); 
                });
            });
        </script>
        <style>
            /* Left menu */
            .left-outside-menu-wrap {
                width: auto;
                margin-top: 30px;
            }
            .left-outside-menu-wrap .left-outside-menu li a {
                opacity: 1;
                font-size: 18px;
                font-family: inherit;
                color: #999999;
                flex-direction: row;
                align-items: center;
                justify-content: flex-start;
            }
            .left-outside-menu-wrap .left-outside-menu li.active a {
                color: #1a1a1a;
                font-family: 'Circular Std Bold';
            }
            .left-outside-menu-wrap .left-outside-menu li a .icon-wrap {
                width: 70px;
                text-align: center;
            }
            @media (min-width: 992px) {
                #leftOutsideMenu.sticky {
                    transform: translate(-217px, -68px);
                }
            }
            /* Left menu END */

            .create-new-post-popup .note-placeholder {
                display: none!important;
            }
        </style>
    </head>

    <body class="page__body">

    <svg style="position: absolute; width: 0; height: 0; overflow: hidden" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="angle-circle-up-solid" viewBox="0 0 21 21">
                <path d="M10.5.5c5.523 0 10 4.476 10 9.999 0 5.523-4.477 10-10 10s-10-4.477-10-10 4.477-10 10-10zm1.325 7.937L10.539 7.06l-.04.041-.038-.04-1.287 1.377.032.034L6.1 11.758l1.286 1.378L10.5 9.841l3.112 3.295 1.287-1.378-3.105-3.288z"/>
            </symbol>
        </defs>
    </svg>
        @include('site.layouts._header')
        <div class="content-wrap">
            <div class="container-fluid">
                <div class="custom-row">
                    <!-- Main content -->
                    <div class="main-content-layer" style="padding-top:10px">
                        <div id="feed">

                            <div style="padding: 30px; background: #fff; border-radius: 10px; margin-top: 20px; border: 1px solid #e6e6e6; display: none;">
                                <input type="text" id="postText">
                            </div>
                            
                            <!-- Post types popups buttons -->
                            <div style="padding: 30px; background: #fff; border-radius: 10px; margin-top: 20px; border: 1px solid #e6e6e6;">
                                <ol style="margin-left:60px;">
                                    <li style="list-style-type: decimal; margin-bottom: 20px;">
                                        <a href="" data-toggle="modal" data-target="#standAloneTextPopup">Standalone text popup</a>
                                    </li>
                                    <li style="list-style-type: decimal; margin-bottom: 20px;">
                                        <a href="" data-toggle="modal" data-target="#mediaTextPopup">Image popup with or without caption</a>
                                    </li>
                                    <li style="list-style-type: decimal; margin-bottom: 20px;">
                                        <a href="" data-toggle="modal" data-target="#videoTextPopup">Video popup with or without caption</a>
                                    </li>
                                    <li style="list-style-type: decimal; margin-bottom: 20px;">
                                        <a href="" data-toggle="modal" data-target="#checkInTextPopup">Check-in popup with text/media and without text/media</a>
                                    </li>
                                    <li style="list-style-type: decimal; margin-bottom: 20px;">
                                        <a href="" data-toggle="modal" data-target="#reviewTextPopup">Review popup with and without media</a>
                                    </li>
                                    <li style="list-style-type: decimal; margin-bottom: 20px;">
                                        <a href="" data-toggle="modal" data-target="#discussionTextPopup">Discussion popup with and without media</a>
                                    </li>
                                    <li style="list-style-type: decimal; margin-bottom: 20px;">
                                        <a href="" data-toggle="modal" data-target="#tripTextPopup">Popup for Trip Plan</a>
                                    </li>
                                    <li style="list-style-type: decimal; margin-bottom: 20px;">
                                        <a href="" data-toggle="modal" data-target="#tripMediaTextPopup">Popup for media shared from within a trip plan</a>
                                    </li>
                                    <li style="list-style-type: decimal; margin-bottom: 20px;">
                                        <a href="" data-toggle="modal" data-target="#sharedPostTextPopup">Popup for shared post</a>
                                    </li>
                                    <li style="list-style-type: decimal; margin-bottom: 20px;">
                                        <a href="" data-toggle="modal" data-target="#eventTextPopup">Popup for event & activity post type</a>
                                    </li>
                                </ol>
                            </div>
                            <!-- Post types popups buttons END -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('site.place2.partials.modal_like')

<footer class="page-footer page__footer"></footer>

<script src="{{ asset('assets/js/lightslider.min.js') }}"></script>
<script src="{{ asset('assets3/js/tether.min.js') }}"></script>
<script src="{{ asset('assets2/js/popper.min.js') }}"></script>
<script src="{{ asset('assets2/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets3/js/bootstrap-datepicker-extended.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets2/js/slick.min.js') }}"></script>
<script src="{{ asset('assets3/js/main.js?0.3.2') }}"></script>
<script src="{{ asset('assets2/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/js/lightgallery-all.min.js"></script>
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
<script src="{{ asset('assets3/js/jquery.barrating.min.js') }}"></script>
<script src="{{ asset('assets3/js/star.js') }}"></script>

<script src="{{ asset('assets2/js/emoji/jquery.emojiFace.js?v=0.1') }}"></script>
<script src="{{ asset('assets2/js/skyloader.js') }}"></script>
<!-- <script src="{{ asset('assets3/js/jquery.sliderPro.min.js') }}"></script> -->
<script src="{{ asset('assets2/js/jquery-confirm/jquery-confirm.min.js') }}"></script>
<script src="{{ asset('assets2/js/lightbox2/src/js/lightbox.js') }}"></script>
<script data-cfasync="false" src="{{asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('assets2/js/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('assets2/js/posts-script.js?v=0.3') }}"></script>

<script src="{{ asset('assets2/js/summernote/summernote.js') }}"></script>

<script>
/**
    * Summernote StripTags
    *
    * This is a plugin for Summernote (www.summernote.org) WYSIWYG editor.
    * To strip unwanted HTML tags and attributes while pasting content in editor.
    *
    * @author Hitesh Aggarwal, Extenzine
    *
    */

    (function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(window.jQuery);
    }
    }(function ($) {
    $.extend($.summernote.options, {
        stripTags: ['a', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'div', 'p', 'span', 'strong', 'code', 'img', 'video', 'font', 'style', 'embed', 'param', 'script', 'html', 'body', 'head', 'meta', 'title', 'link', 'iframe', 'applet', 'noframes', 'noscript', 'form', 'input', 'select', 'option', 'colgroup', 'col', 'std', 'xml:', 'st1:', 'o:', 'w:', 'v:'],
        stripAttributes: ['font', 'style', 'embed', 'param', 'script', 'html', 'body', 'head', 'meta', 'title', 'link', 'iframe', 'applet', 'noframes', 'noscript', 'form', 'input', 'select', 'option', 'colgroup', 'col', 'std', 'xml:', 'st1:', 'o:', 'w:', 'v:'],
        onAfterStripTags: function ($html) {
            return $html;
        }
    });

    $.extend($.summernote.plugins, {
        'striptags': function (context) {
            var $note = context.layoutInfo.note;
            var $options = context.options;
            $note.on('summernote.paste', function (e, evt) {
                evt.preventDefault();
                var text = evt.originalEvent.clipboardData.getData('text/plain'), html = evt.originalEvent.clipboardData.getData('text/html');
                if (html) {
                var tagStripper = new RegExp('<[ /]*(' + $options.stripTags.join('|') + ')[^>]*>', 'gi'), attributeStripper = new RegExp(' (' + $options.stripAttributes.join('|') + ')(="[^"]*"|=\'[^\']*\'|=[^ ]+)?', 'gi'), commentStripper = new RegExp('<!--(.*?)-->', 'g');
                html = html.toString().replace(commentStripper, '').replace(tagStripper, '').replace(attributeStripper, ' ').replace(/( class=(")?Mso[a-zA-Z]+(")?)/g, ' ').replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">").replace(/[\u2018\u2019\u201A]/g, "'").replace(/[\u201C\u201D\u201E]/g, '"').replace(/\u2026/g, '...').replace(/[\u2013\u2014]/g, '-');
                }
                var $html = $('<div/>').html(html || text);
                $html = $options.onAfterStripTags($html);
                $note.summernote('insertNode', $html[0]);
                return false;
            });
        }
    });

    }));
</script>

<script>
    $("#postText").summernote({
        airMode: false,
        focus: false,
        disableDragAndDrop: true,
        placeholder: 'Write something...',
        toolbar: [
            ['insert', ['emoji']],
            ['custom', ['textComplate'], ['striptags']],
        ],
        callbacks: {
            onFocus: function(e) {
                $(e.target).parent('.note-editing-area').find(".note-placeholder").removeClass('custom-placeholder');
                $(e.target).closest('#createPostTxt').find('.post-create-controls').removeClass('d-none');
            },
            onInit: function(e) {
                e.editingArea.find(".note-placeholder").addClass('custom-placeholder')
            }
        },
        onAfterStripTags: function ($html) {
            setTimeout(() => {
                $html.siblings('div').remove();
            }, 50);
            return $html;
        }
    });
</script>

@include('site.home.partials._spam_dialog')
<!-- Users who like modal -->
<div class="modal white-style users-who-like-modal" data-backdrop="false" id="usersWhoLike" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                            Poeple who liked this post <span>16</span>
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar" >
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Friend</span>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Follows you</span>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-grey btn-bordered">Unfollow</button>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Users who like modal END -->
<!-- Message to user popup -->
<div class="modal msg-to-user-popup" id="messageToUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-650" role="document" >
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <div class="top-title-layer" >
                    <h3 class="title" >
                        Message
                        <img class="msg-to" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                        <span class="txt">Bora Young</span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-create-input">
                    <textarea name="" id="" cols="30" rows="10" placeholder="Type a message..."></textarea>
                    <div class="medias medias-slider" >
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed loading">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-create-controls">
                    <div class="emoji-block">
                        <button class="add-msg__emoji" emoji-target="add_msg_text" type="button">🙂</button> 
                    </div>
                    <div class="post-alloptions" >
                        <ul class="create-link-list" >
                            <li class="post-options" >
                                <input type="file" name="file[]" id="file" style="display:none" multiple="" >
                                <i class="trav-camera click-target" data-target="file" ></i>
                            </li>
                        </ul>
                    </div>
                    <div class="ml-auto post-buttons">
                        <button type="button" class="btn btn-light-grey btn-bordered mr-3" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-light-primary btn-bordered" >Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Message to user popup END -->

<!-- Share post modal -->
<div class="modal white-style share-post-modal" data-backdrop="false" id="sharePostModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-670" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                        Share this post
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="shared-post-comment">
                    <div class="shared-post-comment-details">
                        <div class="shared-post-comment-author">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            Ivan Turlakov
                        </div>
                        <!-- privacy settings -->
                        <div class="dropdown">
                            <button class="btn btn--sm btn--outline dropdown-toggle" data-toggle="dropdown">
                                <i class="trav-globe-icon"></i>
                                PUBLIC
                            </button>
                            <div class="dropdown-menu dropdown-menu-right permissoin_show hided">
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-globe-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Public</b></p>
                                        <p>Anyone can see this post</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-users-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Friends Only</b></p>
                                        <p>Only you and your friends can see this post</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-lock-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Only Me</b></p>
                                        <p>Only you can see this post</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- privacy settings -->
                    </div>
                    <div class="shered-post-comment-text">
                        <textarea name="" id="" cols="30" rows="10" placeholder="Write something..."></textarea>
                    </div>
                </div>
                <div class="shared-post-wrap">
                    <!-- Primary post block - photo*3 and text -->
                    <div class="post-block" >
                        <div class="post-top-info-layer" >
                        <div class="post-top-info-wrap" >
                        <div class="post-top-avatar-wrap" >
                        <a class="post-name-link" href="https://travooo.com/profile/831" >
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1597941078_image.png" alt="" >
                        </a>
                        </div>
                        <div class="post-top-info-txt" >
                        <div class="post-top-name" >
                        <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                        </div>
                        <!-- Updated element -->
                        <div class="post-info">
                            Checked-in
                            <a href="https://travooo.com/place/3485165" class="link-place">The House of Dancing Water</a>
                            on Sep 1, 2020
                            <i class="trav-globe-icon" ></i>
                        </div>
                        <!-- Updated element END -->
                        </div>
                        </div>
                        </div>
                        <!-- New elements -->
                        <div class="post-txt-wrap">
                            <div>
                                <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                            </div>
                        </div>
                        <div class="check-in-point">
                            <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                        </div>
                        <!-- New elements END -->
                        <div class="post-image-container" >
                        <!-- New elements -->
                        <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;" >
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                        </ul>
                        <!-- New element END -->
                        </div>
                        <div class="post-footer-info" >
                        <!-- Updated elements -->
                        <div class="post-foot-block post-reaction">
                            <span class="post_like_button" id="601451">
                                <a href="#">
                                    <i class="trav-heart-fill-icon"></i>
                                </a>
                            </span>
                            <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                <b >0</b> Likes
                            </span>
                        </div>
                        <!-- Updated element END -->
                        <div class="post-foot-block" >
                        <a href="#" data-tab="comments616211" >
                        <i class="trav-comment-icon" ></i>
                        </a>
                        <ul class="foot-avatar-list" >
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                        </ul>
                        <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                        </div>
                        <div class="post-foot-block ml-auto" >
                        <span class="post_share_button" id="616211" >
                        <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                        <i class="trav-share-icon" ></i>
                        </a>
                        </span>
                        <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                        </div>
                        </div>
                        <div class="post-comment-wrapper" id="following616211" >
                        </div>
                        <!-- Removed comments block -->
                    </div>
                    <!-- Primary post block - photo*3 and text END -->
                </div>
                <div class="shared-post-actions">
                    <button type="submit" class="btn btn-link circle"  rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share on <b>Facebook</b>">
                       <i class="fab fa-facebook-f"></i>
                    </button>
                    <span class="divider"></span>
                    <button type="submit" class="btn btn-link circle"  rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share on <b>Twitter</b>">
                        <i class="trav-twitter-icon"></i>
                    </button>
                    <span class="divider"></span>
                    <button type="submit" class="btn btn-link circle"  rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share on <b>Pintrest</b>">
                        <i class="trav-pintrest-icon"></i>
                    </button>
                    <span class="divider"></span>
                    <button type="submit" class="btn btn-link circle"  rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share on <b>Tumblr</b>">
                        <i class="trav-tumblr-icon"></i>
                    </button>
                    <span class="divider"></span>
                    <button type="submit" class="btn btn-link circle"  rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share the <b>link</b>">
                        <i class="trav-link"></i>
                    </button>
                    <span class="divider"></span>
                    <button type="submit" class="btn btn-link" rel="tooltip" data-toggle="tooltip" data-html="true" data-animation="false"
                       title="Share on <b>Travooo</b>">SHARE</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Share post modal END -->
<!-- Users who shared modal -->
<div class="modal white-style users-who-like-modal" data-backdrop="false" id="usersWhoShare" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                            Poeple who shared this post <span>16</span>
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar" >
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Friend</span>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                    <span class="user-status">Follows you</span>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                    <div class="people-row" id="mate648" >
                        <div class="main-info-layer" >
                            <div class="img-wrap" >
                                <img class="ava mCS_img_loaded" src="https://s3.amazonaws.com/travooo-images2/users/profile/648/1594022310_image.png" style="width:50px;height:50px" >
                            </div>
                            <div class="txt-block" >
                                <div class="name" >
                                    <a href="https://travooo.com/profile/648" class="tm-user-link" target="_blank" >Bora Young </a>
                                </div>
                            </div>
                            <div class="going-action-block going-648" >
                                <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-toggle="modal" data-target="#messageToUser">Message</button>
                                <button type="button" class="btn btn-light-primary btn-bordered">Follow</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Users who shared modal END -->
<!-- Send to a friend modal -->
<div class="modal white-style share-post-modal" data-backdrop="false" id="sendToFriendModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-670" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                            Send to a Friend
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="shared-post-comment">
                    <div class="shared-post-comment-details">
                        <div class="shared-post-comment-author">
                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            Ivan Turlakov
                        </div>
                        <!-- privacy settings -->
                        <div class="dropdown">
                            <button class="btn btn--sm btn--outline dropdown-toggle" data-toggle="dropdown">
                                <i class="trav-globe-icon"></i>
                                PUBLIC
                            </button>
                            <div class="dropdown-menu dropdown-menu-right permissoin_show hided">
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-globe-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Public</b></p>
                                        <p>Anyone can see this post</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-users-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Friends Only</b></p>
                                        <p>Only you and your friends can see this post</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-lock-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Only Me</b></p>
                                        <p>Only you can see this post</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- privacy settings -->
                    </div>
                    <div class="shered-post-comment-text">
                        <textarea name="" id="" cols="30" rows="10" placeholder="Write something..."></textarea>
                    </div>
                </div>
                <div class="shared-post-wrap">
                    <!-- Primary post block - photo*3 and text -->
                    <div class="post-block" >
                        <div class="post-top-info-layer" >
                        <div class="post-top-info-wrap" >
                        <div class="post-top-avatar-wrap" >
                        <a class="post-name-link" href="https://travooo.com/profile/831" >
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1597941078_image.png" alt="" >
                        </a>
                        </div>
                        <div class="post-top-info-txt" >
                        <div class="post-top-name" >
                        <a class="post-name-link" href="https://travooo.com/profile/831" >Ivan Turlakov</a>
                        </div>
                        <!-- Updated element -->
                        <div class="post-info">
                            Checked-in
                            <a href="https://travooo.com/place/3485165" class="link-place">The House of Dancing Water</a>
                            on Sep 1, 2020
                            <i class="trav-globe-icon" ></i>
                        </div>
                        <!-- Updated element END -->
                        </div>
                        </div>
                        <div class="post-top-info-action" >
                        <div class="dropdown" >
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <i class="trav-angle-down" ></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                        <a class="dropdown-item" href="#" onclick="post_delete('616211','Post', this, event)" >
                        <span class="icon-wrap" >

                        </span>
                        <div class="drop-txt" >
                        <p ><b >Delete</b></p>
                        <p style="color:red" >Remove this post</p>
                        </div>
                        </a>
                        </div>
                        </div>
                        </div>
                        </div>
                        <!-- New elements -->
                        <div class="post-txt-wrap">
                            <div>
                                <span class="less-content disc-ml-content">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est<span class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a></span>
                                <span class="more-content disc-ml-content" style="display: none;">Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. Dolor sit amet consectetur adipiscing elit integer leo neque pulvinar ut neque eu, laoreet tellus etiam aliquam lacinia est. <a href="javascript:;" class="read-less-link">Less</a></span>
                            </div>
                        </div>
                        <div class="check-in-point">
                            <i class="trav-set-location-icon"></i> <strong>Tokyo Airport</strong>, Tokyo, Japan <i class="trav-clock-icon" ></i> 13 Jun 2020 at 09:50AM
                        </div>
                        <!-- New elements END -->
                        <div class="post-image-container" >
                        <!-- New elements -->
                        <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;" >
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" data-lightbox="media__post199366" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637009_images (1).jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" data-lightbox="media__post199366" >
                                <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_whistler2-675x390-c0d.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;" >
                                <a class="more-photos" href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366">5 More Photos</a>
                                <a href="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" data-lightbox="media__post199366" >
                                    <img src="https://s3.amazonaws.com/travooo-images2/post-photo/831_1591637010_images.jpg" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                                </a>
                            </li>
                        </ul>
                        <!-- New element END -->
                        </div>
                        <div class="post-footer-info" >
                        <!-- Updated elements -->
                        <div class="post-foot-block post-reaction">
                            <span class="post_like_button" id="601451">
                                <a href="#">
                                    <i class="trav-heart-fill-icon"></i>
                                </a>
                            </span>
                            <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                <b >0</b> Likes
                            </span>
                        </div>
                        <!-- Updated element END -->
                        <div class="post-foot-block" >
                        <a href="#" data-tab="comments616211" >
                        <i class="trav-comment-icon" ></i>
                        </a>
                        <ul class="foot-avatar-list" >
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                            <li>
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" class="small-ava">    
                            </li>
                        </ul>
                        <span ><a href="#" data-tab="comments616211" ><span class="616211-comments-count" >0</span> Comments</a></span>
                        </div>
                        <div class="post-foot-block ml-auto" >
                        <span class="post_share_button" id="616211" >
                        <a href="#"  data-toggle="modal" data-target="#sharePostModal">
                        <i class="trav-share-icon" ></i>
                        </a>
                        </span>
                        <span id="post_share_count_616211" ><a href="#" data-tab="shares616211" data-toggle="modal" data-target="#usersWhoShare"><strong >0</strong> Shares</a></span>
                        </div>
                        </div>
                        <div class="post-comment-wrapper" id="following616211" >
                        </div>
                        <!-- Removed comments block -->
                    </div>
                    <!-- Primary post block - photo*3 and text END -->
                </div>
                <div class="shared-post-actions share-to-friend">
                    <button class="extednd-btn">
                        <span class="extend-ttl">Extend</span> 
                        <span class="shrink-ttl">Shrink</span>
                    </button>
                    <div class="friend-search">
                        <i class="trav-search-icon" ></i>
                        <input type="text" placeholder="Search in your friends list...">
                    </div>
                    <div class="friend-search-results">
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light">Send</button>
                        </div>
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light" disabled>Sent</button>
                        </div>
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light">Send</button>
                        </div>
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light">Send</button>
                        </div>
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light" disabled>Sent</button>
                        </div>
                        <div class="friend-search-results-item">
                            <img class="user-img" src="https://s3.amazonaws.com/travooo-images2/users/profile/555/1593871763_image.png" alt="">
                            <div class="text">
                                <div class="location-title">James Robinson</div>
                            </div>
                            <button class="btn btn-light">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Send to a friend modal END -->
<!-- Spam Report modal -->
<div class="modal white-style spam-report-modal" data-backdrop="false"                                               id="spamReportDlgNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style" role="document">
        <div class="post-block">
            <div class="modal-header">
                <h4 class="side-ttl">Report Post</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-check mb-2">
                        <i class="fa fa-exclamation-circle report-exclamation" aria-hidden="true"></i>
                        <h5 class="d-inline-block">What is the problem with this post?</h5>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container">
                                    <div class="radio-title-group">
                                        <div class="input-container">
                                            <input id="spam" class="radio-button" type="radio" name="spam_post_type" value="0" checked="checked">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Spam
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="fake-news" class="radio-button" type="radio" name="spam_post_type" value="2">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Fake News
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="harassment" class="radio-button" type="radio" name="spam_post_type" value="3">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Harassment                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="hate-speech" class="radio-button" type="radio" name="spam_post_type" value="4">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Hate Speech                                               </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="nudity" class="radio-button" type="radio" name="spam_post_type" value="5">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Nudity
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="terrorism" class="radio-button" type="radio" name="spam_post_type" value="6">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Terrorism
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="violence" class="radio-button" type="radio" name="spam_post_type" value="7">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Violence
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="other" class="radio-button" type="radio" name="spam_post_type" value="1">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Other
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control report-span-input" id="spamText" placeholder="Something Else" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="send-report-btn" id="spamSend">Send</button>
            </div>
            <input type="hidden" name="dataid" id="dataid">
            <input type="hidden" name="posttype" id="posttype">
        </div>
    </div>
</div>
<!-- Spam Report modal END -->
<!-- Delete Post modal -->
<div class="modal white-style spam-report-modal" data-backdrop="false" id="deletePostNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style" role="document">
        <div class="post-block">
            <div class="modal-header">
                <h4 class="side-ttl">Delete Post</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group mb-3">
                    <div class="form-check mb-2">
                        <h5 class="d-inline-block">Are you sure you want to delete this post?</h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="send-report-btn link">Cancel</button>
                <button type="submit" class="send-report-btn red">Delete</button>
            </div>
            <input type="hidden" name="dataid" id="dataid">
            <input type="hidden" name="posttype" id="posttype">
        </div>
    </div>
</div>
<!-- Delete Post modal END -->
<!-- Post types popups -->
@include('site.design.partials._standAloneTextPopup')
@include('site.design.partials._mediaTextPopup')
@include('site.design.partials._videoTextPopup')
@include('site.design.partials._checkInTextPopup')
@include('site.design.partials._reviewTextPopup')
@include('site.design.partials._discussionTextPopup')
@include('site.design.partials._tripTextPopup')
@include('site.design.partials._tripMediaTextPopup')
@include('site.design.partials._sharedPostTextPopup')
@include('site.design.partials._eventTextPopup')
<!-- Post types popups END -->
</body>
@include('site.home.partials.modal_comments_like')
@include('site.layouts._footer-scripts')

</html>
