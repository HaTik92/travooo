@php
    $title = 'Travooo - travel mates';
@endphp

@extends('site.travelmates.template.travelmate')

@section('content')
    @if(session()->has('alert-success'))
        <div class="alert alert-success" id="joinRequestSuccess">
            {{ session()->get('alert-success') }}
        </div>
    @endif

    @if(session()->has('alert-danger'))
        <div class="alert alert-danger" id="joinRequestError">
            {{ session()->get('alert-danger') }}
        </div>
    @endif

    <div class="post-block post-flight-style">
        <div class="post-side-top">
            <div class="post-side-top-inner">
                <button type="button" class="btn btn-light-bg-grey btn-bordered btn-back">
                    <i class="trav-angle-left"></i>
                </button>
                <h3 class="side-ttl">@lang('travelmate.starting_soon')</h3>

            </div>
        </div>
        <div class="post-side-inner">
            <div class="travel-mates-trip-plan-wrapper">
                @foreach($requests AS $request)

                    @include('site/travelmates/partials/trip-block')
                    @include('site/travelmates/partials/going-popup')

                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('before_scripts')
    @include('site/travelmates/partials/request-popup')
    @include('site/travelmates/partials/search-popup')
    @include('site/travelmates/partials/trip-popup')
@endsection