<div class="post-block post-flight-style travel-mates right">
    <div class="post-side-top">
        <div class="post-side-top-nav">
            <li class="active">
                <h3 class="side-ttl">@lang('travelmate.become_travel_mate')</h3>
            </li>
        </div>
    </div>
    <div class="search-mates">
        <div class="become-img">
            <img src="{{asset('assets2/image/become_bg.png')}}">
        </div>
        <div class="row become-mate">
            <span>Are you planning a trip to this place?</span> you can list yourself as a Travel Mate so other travelers can find you and send travel requests
        </div>
        <div class="row">
            <div class="search-button" data-toggle="modal" data-target="#requestPopup">
                @lang('travelmate.become_travel_mate')
            </div>
        </div>
    </div>
</div>
