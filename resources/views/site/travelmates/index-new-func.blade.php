@php
    $title = 'Travooo - travel mates';
@endphp

@extends('site.travelmates.template.travelmate')

@section('content')
            @if(session()->has('alert-success'))
                <div class="alert alert-success" id="postSavedSuccess">
                    {{ session()->get('alert-success') }}
                </div>
            @endif
            
            @if(session()->has('alert-error'))
                <div class="alert alert-error" id="postSavedError">
                    {{ session()->get('alert-error') }}
                </div>
            @endif
            
            
    @include('site.travelmates._popular-section')
    @include('site.home.partials._spam_dialog')
    @include('site.travelmates._travel-mates-section')
    <div class="travel-mates right travel-mates-mobile-search">
        @include('site.travelmates._search-section')
        
        @if($count_my_plans > 1)
            @include('site.travelmates._become-section')
        @endif
    </div>
@endsection
@section('before_scripts')
    @include('site/travelmates/partials/request-popup')
@endsection
@section('after_scripts')
    @include('site.travelmates._travel-mates-js')
@endsection
