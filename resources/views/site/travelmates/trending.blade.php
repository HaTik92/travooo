@php
    $title = 'Travooo - travel mates';
@endphp

@extends('site.travelmates.template.travelmate')

@section('content')
    <div class="top-banner-block top-travel-main-banner"
         style="background-image: url(./assets2/image/popup-map.jpg)">
        <div class="top-banner-inner">
            <div class="banner-row">
                <div class="travel-left-info">
                    <div class="travel-banner-label"><i class="trav-trending-destination-icon"></i>
                        @lang('trip.trending_trip_plan')
                    </div>
                    <h3 class="travel-title">@lang('trip.across_the_country_road_trip', ['country' => 'United States'])</h3>
                    <div class="left-info-block">
                        <i class="trav-talk-icon"></i>
                        <span><b>64k</b> @lang('trip.talking_about_it')</span>
                        <ul class="foot-avatar-list">
                            <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                            -->
                            <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                            -->
                            <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                        </ul>
                    </div>
                    <div class="left-info-block">
                        <i class="trav-map"></i>
                        <span>@lang('travelmate.view_in_the_map')</span>
                    </div>
                    <div class="btn-wrap">
                        <a class="btn btn-light-primary" href="#">@lang('travelmate.ask_to_join')</a>
                    </div>
                </div>
                <div class="travel-right-info">
                    <ul class="sub-list">
                        <li>
                            <div class="icon-wrap">
                                <i class="trav-comment-plus-icon"></i>
                            </div>
                            <div class="ctxt">
                                <div class="top-txt">28K</div>
                                <div class="sub-txt">@choice('travelmate.travel_mate', 1)</div>
                            </div>
                        </li>
                        <li>
                            <div class="icon-wrap">
                                <i class="trav-day-duration-icon"></i>
                            </div>
                            <div class="ctxt">
                                <div class="top-txt">@choice('time.count_day', 3, ['count' => 3])</div>
                                <div class="sub-txt">@lang('trip.duration')</div>
                            </div>
                        </li>
                        <li>
                            <div class="icon-wrap">
                                <i class="trav-user-rating-icon"></i>
                            </div>
                            <div class="ctxt">
                                <div class="top-txt">17</div>
                                <div class="sub-txt">@choice('trip.destination', 2)</div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="banner-row" style="overflow:hidden;">
                <div class="head-trip-plan_trip-line">
                    <div class="trip-line" id="tripLineSlider">
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon">
                                    <img src="./assets2/image/flag-img-1.png" alt="flag-icon">
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Morocco</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('region.count_city', 1, ['count' => 1])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon blue-icon">
                                    <i class="trav-set-location-icon"></i>
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name blue">Rabat-Sale</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('place.count_place', 1, ['count' => 1])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon">
                                    <img src="http://placehold.it/32x32" alt="flag-icon">
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">United arab emirates</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('region.count_city', 1, ['count' => 1])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon disabled-icon">
                                    <i class="trav-radio-checked2"></i>
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Dubai</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('place.count_place', 1, ['count' => 1])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon">
                                    <img src="http://placehold.it/32x32" alt="flag-icon">
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Japan</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('region.count_city', 3, ['count' => 3])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon disabled-icon">
                                    <i class="trav-radio-checked2"></i>
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Tokyo</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('trip.count_place', 2, ['count' => 8])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon disabled-icon">
                                    <i class="trav-radio-checked2"></i>
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Nagoya</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('trip.count_place', 2, ['count' => 3])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon disabled-icon">
                                    <i class="trav-radio-checked2"></i>
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Osaka</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('trip.count_place', 2, ['count' => 5])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(session()->has('alert-success'))
        <div class="alert alert-success" id="joinRequestSuccess">
            {{ session()->get('alert-success') }}
        </div>
    @endif

    @if(session()->has('alert-danger'))
        <div class="alert alert-danger" id="joinRequestError">
            {{ session()->get('alert-danger') }}
        </div>
    @endif

    <div class="post-block post-flight-style">
        <div class="post-side-top">
            <h3 class="side-ttl"><i
                        class="trav-trending-destination-icon"></i> @lang('travelmate.trending_destinations')
            </h3>
        </div>
        <div class="post-side-inner">
            <div class="post-slide-wrap post-destination-block">
                <ul id="trendingTravelDestinations" class="post-slider">
                    <li>
                        <div class="post-dest-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/165x95" alt="">
                                <div class="dest-name">France</div>
                            </div>
                            <div class="dest-txt">
                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                <p><span>12K</span> @choice('travelmate.travel_mate', 2)</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-dest-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/165x95" alt="">
                                <div class="dest-name">Washington</div>
                            </div>
                            <div class="dest-txt">
                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                <p><span>12K</span> @choice('travelmate.travel_mate', 2)</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-dest-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/165x95" alt="">
                                <div class="dest-name">@lang('other.central_part')</div>
                            </div>
                            <div class="dest-txt">
                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                <p><span>12K</span> @choice('travelmate.travel_mate', 2)</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-dest-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/165x95" alt="">
                                <div class="dest-name">Japan</div>
                            </div>
                            <div class="dest-txt">
                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                <p><span>12K</span> @choice('travelmate.travel_mate', 2)</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-dest-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/165x95" alt="">
                                <div class="dest-name">Grand Canyon National park</div>
                            </div>
                            <div class="dest-txt">
                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                <p><span>12K</span> @choice('travelmate.travel_mate', 2)</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-dest-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/165x95" alt="">
                                <div class="dest-name">Big ben</div>
                            </div>
                            <div class="dest-txt">
                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                <p><span>12K</span> @choice('travelmate.travel_mate', 2)</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-dest-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/165x95" alt="">
                                <div class="dest-name">@lang('other.central_part')</div>
                            </div>
                            <div class="dest-txt">
                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                <p><span>12K</span> @choice('travelmate.travel_mate', 2)</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-dest-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/165x95" alt="">
                                <div class="dest-name">Japan</div>
                            </div>
                            <div class="dest-txt">
                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                <p><span>12K</span> @choice('travelmate.travel_mate', 2)</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <div class="post-block post-flight-style">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('travelmate.travel_mates_and_trip_plans')</h3>
        </div>
        <div class="post-side-inner">
            <div class="travel-mates-trip-plan-wrapper">
                @foreach($requests AS $request)
                    @include('site/travelmates/partials/trip-block')
                    @include('site/travelmates/partials/going-popup')
                @endforeach


            </div>
        </div>
    </div>
@endsection

@section('before_scripts')
    @include('site/travelmates/partials/request-popup')
    @include('site/travelmates/partials/search-popup')
    @include('site/travelmates/partials/trip-popup')
@endsection