<div class="post-block post-flight-style travel-mates right">
    <div class="post-side-top">
        <h3 class="side-ttl">@lang('travelmate.find_a_travel_mate')</h3>
    </div>
    <div class="search-mates">
        <div class="row">
            <div class="plan-type active" filter-type="1">@lang('travelmate.with_a_trip_plan')</div>
            <div class="plan-type" filter-type="2">@lang('travelmate.without_a_trip_plan')</div>
        </div>
        <div class="row">
            <h5>@lang('time.mutual_dates')</h5>
            <div class="dates">
                <input type="text" class="date start" id="search_start_date" readonly placeholder="@lang('time.trip_start_date')"/>
                <input type="text" class="date finish" id="search_end_date" readonly placeholder="@lang('time.trip_finish_date')" />
            </div>
            <label for="search_end_date" class="error finish-date-error-label" style="color:red"></label>
        </div>
        <div class="row destinations">
            <h5>@choice('trip.destination', 2)</h5>
            <div>
                <input type="text" class="search-destination" autocomplete="off" id="search_destination" placeholder="@lang('travelmate.search_destinations')" value=""/>
            </div>
            <label for="search_destination" class="error filter-destination-error-label" style="color:red"></label>
            <ul class="search-selected-block destination-search-block"></ul>
        </div>
        <div class="row countries">
            <h5>Starting Location</h5>
            <div>
                <input type="text" class="search-location" autocomplete="off" placeholder="City name..." value=""/>
            </div>
             <ul class="search-selected-block location-filter-block"></ul>
        </div>
        <div class="row countries">
            <h5>Nationality</h5>
            <div>
                <input type="text" class="search-nationality" autocomplete="off" placeholder="@lang('region.country_name_dots')" value=""/>
            </div>
            <ul class="search-selected-block nationality-search-block"></ul>
        </div>
        <div class="row interests">
            <h5>@lang('profile.interests')</h5>
            <div>
                <input type="text" class="search-interst" id="filter_interests" maxlength="15" placeholder="@lang('profile.search_for_an_interest')" value=""/>
            </div>
            <label for="filter_interests" class="error filter-interest-error-block" style="color:red"></label>
            <ul class="search-selected-block interest-search-block"></ul>
        </div>
        <div class="row">
            <div class="dates">
                <div>
                    <h5>@lang('profile.age')</h5>
                    <div>
                        <select class="age">
                            <option value="">Any</option>
                            <option value="18-25">18 - 25</option>
                            <option value="25-35">25 - 35</option>
                            <option value="35-50">35 - 50</option>
                            <option value="50-100">50 +</option>
                        </select>
                        <img src="/assets2/image/select-arrow.png" alt="" class="select-arrow" />
                    </div>
                </div>
                <div>
                    <h5>@lang('profile.gender')</h5>
                    <div>
                        <select class="gender">
                            <option value="">Any</option>
                            <option value="1">@lang('profile.male')</option>
                            <option value="2">@lang('profile.female')</option>
                        </select>
                        <img src="/assets2/image/select-arrow.png" alt="" class="select-arrow" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="search-button search-filter-button">
                @lang('travelmate.button_start_search')
            </div>
        </div>
    </div>
</div>
