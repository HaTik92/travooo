<!-- For mobile layout only -->
<button class="mobile-search-travel-mates-toggler">Find a travel mate</button>
<!-- For mobile layout only -->

<div class="post-block post-flight-style travel-mates left mates-main-content">
    <div class="post-side-top">
        <div class="post-side-top-nav">
            <ul class="trev-mates-top-nav">
                <li class="active" filter-type="1" data-type="without_plan_tab">
                    <h3 class="side-ttl">Join Travelers</h3>
                </li>
                <li filter-type="2" data-type="plan_tab">
                    <h3 class="side-ttl">Invite Travelers</h3>
                </li>
            </ul>
        </div>
        <div class="post-side-top-sort">
            <h3 class="side-ttl">@lang('travelmate.filter_by'):</h3>
                <select class="maters-filter-option sort-select" placeholder="Select Type">
                    <option value=""></option>
                    <option value="relevant">Relevant</option>
                    <option value="top">Top</option>
                    <option value="new">New</option>
                    <option value="upcoming">Upcoming</option>
                </select>
        </div>
    </div>
    <div class="mt-tab-content">
         @include('site.travelmates._travel-mates-section-block')
    </div>
</div>

<div class="post-block post-flight-style travel-mates left mates-search-content d-none">
    <div class="post-side-top">
        <div class="post-side-top-nav">
            <ul class="trev-mates-top-nav">
                <li>
                    <h3 class="side-ttl">Travooo found <strong class="search-count"></strong> results for your search...</h3>
                </li>
            </ul>
        </div>
        <div class="post-side-top-sort">
            <h3 class="side-ttl">@lang('travelmate.filter_by'):</h3>
            <select class="maters-filter-option sort-select" placeholder="Select Type">
                <option value=""></option>
                <option value="relevant">Relevant</option>
                <option value="top">Top</option>
                <option value="new">New</option>
                <option value="upcoming">Upcoming</option>
            </select>
        </div>
    </div>
    <div class="search-with-plan-block d-none">
        <div class="travmates-content"></div>
        <div class="mates_pagination" id="mates_pagination">
            <input type="hidden" id="plan_pagenum" value="1">
            <div id="mates_loader" class="mates-animation-content post-side-inner">
                <div class="travel-mates-trip-plan-wrapper">
                    <div class="post-top-info-wrap mates-top-info">
                        <div class="post-top-avatar-wrap mate-avatar-animation animation"></div>
                        <div class="top-info-animation animation"></div>
                        <div class="top-info-buttion-animation animation"></div>
                    </div>
                    <div class="post-content-wrap">
                        <div class="mate-map-animation animation"></div>
                        <div class="info">
                            <div class="params">
                                <ul class="tm-tp-info-list">
                                    <li><div class="mates-info-animation w-135 animation"></div></li>
                                    <li><div class="mates-info-animation w-110 animation"></div></li>
                                    <li><div class="mates-info-animation w-105 animation"></div></li>
                                    <li><div class="mates-info-animation w-125 animation"></div></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
    
    <div class="search-without-plan-content d-none">
        
        <div class="travmates-content">
        </div>
        <div class="w_mates_pagination" id="w_mates_pagination">
            <input type="hidden" id="w_plan_pagenum" value="1">
            <div id="w_mates_loader" class="mates-animation-content post-side-inner">
                <div class="travel-mates-trip-plan-wrapper">
                    <div class="post-top-info-wrap mates-top-info">
                        <div class="post-top-avatar-wrap mate-avatar-animation animation"></div>
                        <div class="top-info-animation animation"></div>
                        <div class="top-info-buttion-animation animation"></div>
                    </div>
                </div>
            </div>
        </div>
   </div>
    <div class="pt-3 pl-4 search-no-result-content"></div>
</div>

    @include('site/travelmates/partials/invite-popup')

