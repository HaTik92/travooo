<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
var loadMoreCount = 0;
var my_interests = [];
@if (Auth::user()->interests)
    my_interests = <?= json_encode(explode(',', Auth::user()->interests)); ?>;
@endif

var destinationsFilters = [];
var countriesFilters = [];
var interestsFilters = [];
var locationFilter;
var orderFilter = 'relevant';
var mates_type = '1';
var globalFilters = {};
var plan_count =  $('.inv-count').html();

    var url_string = window.location.href;
    var url = new URL(url_string);
    
    var url_dest = url.searchParams.get("dest");
console.log('url_dest', url_dest)

if(url_dest !== null){
    var start_date = "{{\Carbon\Carbon::now()->subDays(200)->format('d M yy')}}"
    var end_date = "{{\Carbon\Carbon::now()->addDays(100)->format('d M yy')}}"
    $('.search-mates .date.start').val(start_date);
    $('.search-mates .date.finish').val(end_date);
    
    destinationsFilters.push(url_dest + '-city')

    updateFilters();
   loadFilterContent();
   
    window.history.replaceState(null, null, window.location.pathname);
}

$(document).on('click', 'button.invite-button', function (e) {
    var user_id = $(this).attr('user-id');
    var w_plan_id = $(this).attr('request-id');
    $('div#invitationPopup').attr('user-id', user_id);
    $('div#invitationPopup').attr('w-plan-id', w_plan_id);
     var new_plan_count =  parseInt(plan_count);
    $('.plan-country-list').each(function(){
       
        var plan_country = ($(this).attr('plan_country') !='')?$(this).attr('plan_country').split(','):[];
        var plan_destinations = ($('.plan-destination-' + w_plan_id).attr('plan_destinations'))?$('.plan-destination-' + w_plan_id).attr('plan_destinations').split(','):[];
      
        var common = $.grep(plan_country, function(element) {
            return $.inArray(element, plan_destinations ) !== -1;
        });
        
        if(common.length == 0){
            new_plan_count = new_plan_count - 1;
            $(this).closest('.inv-plan-list').addClass('d-none') 
        }else{
             $(this).closest('.inv-plan-list').removeClass('d-none') 
        }
    })
    
     $('.inv-count').html(new_plan_count)
});

$(document).on('click', 'button#invite-travel-mate', function (e) {
    e.preventDefault();
    var modal = $(this).closest('div#invitationPopup');
    var plan_id = $(modal).find('input[name=plan_id]:checked').val();
    var user_id = $(modal).attr('user-id');
    var w_plan_id = $(modal).attr('w-plan-id');

    $.ajax({
        method: "POST",
        url: "{{route('trip.ajax_invite_friends')}}",
        data: {trip_id: plan_id, users_id: user_id, role: 'travel-mate', w_plans_id:w_plan_id}
    })
    .done(function (res) {
        $('div#without_plan_tab button[request-id='+w_plan_id+'].invite-button').replaceWith('<div plan-id="' + plan_id + '" user-id="' + user_id + '" class="cancel-button" id="cancel-travel-mate-invitation">Cancel</div>');
        $(modal).modal('toggle');
    });
});

$(document).on('click', 'div#cancel-travel-mate-invitation', function (e) {
    var plan_id = $(this).attr('plan-id');
    var user_id = $(this).attr('user-id');

    var button = $(this);

    $.ajax({
        method: "POST",
        url: "{{route('trip.ajax_cancel_invitation')}}",
        data: {trip_id: plan_id, users_id: user_id}
    })
        .done(function (res) {
            $(button).replaceWith('<button class="invite-button" user-id="' + user_id + '" data-toggle="modal" data-target="#invitationPopup">Invite</button>');
        });
});

$(document).on('click', 'button.cancel-invite-button', function (e) {
    e.preventDefault();
    var plan_id = $(this).data('plan');
    var user_id = $(this).data('user');

    var button = $(this);

    $.ajax({
        method: "POST",
        url: "{{route('trip.ajax_cancel_invitation')}}",
        data: {trip_id: plan_id, users_id: user_id}
    })
        .done(function (res) {
            $(button).parents('div.notification-block').remove();
        });
});

$('#invited_pagination').on('inview', function (event, isInView) {
    if (isInView) {
        var nextPage = parseInt($('#invite_pagenum').val()) + 1;

        var url = "{{route('travelmate.ajax.notifications_invitations')}}";

        $.ajax({
            type: 'GET',
            url: url,
            data: {page: nextPage},
            success: function (data) {
                if (data.render != '') {
                    $('div.travel-mates-notification-wrapper').append(data.render);
                    $('#invite_pagenum').val(nextPage);
                } else {
                    $('#invited_pagination').hide();
                }
            }
        });
    }
});

$(document).on('click', '.join-button', function () {
    var _this = $(this);
    var status = _this.attr('status');

    if (status === '0') {
        var request_id = _this.attr('request-id');
       
        $.ajax({
            method: "POST",
            url: "{{route('travelmate.ajaxjoin')}}",
            data: {request_id: request_id}
        })
        .done(function (response) {
            if(response == 'success'){
                _this.closest('.mates-label-block').append(' <div class="cancel-button cancel_request_button" data-type="1" data-requestid="' + request_id + '">Cancel</div>')
                _this.closest('.mates-label-block').find('.join-button').remove()
            }else if(response == 'canseled'){
                $.confirm({
                    title: 'Info!',
                    content: '<span class="disapprove-confirm-text">The request is closed!</span> <div class="mb-3"></div>',
                    columnClass: 'col-md-5 col-md-offset-5',
                    closeIcon: true,
                    offsetTop: 0,
                    offsetBottom: 500,
                    scrollToPreviousElement: false,
                    scrollToPreviousElementAnimate: false,
                    buttons: {
                        cancel: function () {},
                        confirm: {
                            text: 'ok',
                            btnClass: 'btn-red',
                            keys: ['enter', 'shift'],
                            action: function () {
                                
                            }
                        }
                    }
                });
            }
        });
    }
});

//Close requests
$(document).on('click', '.close-requests-btn', function () {
         var request_id = $(this).attr('data-requestid');
         $.post("{{url('travelmates/ajax-close-request')}}",
             {request_id: request_id},
             function (data) {
                 if (data == "success") {
                    window.location.reload()
                 }
             });
     });
     
     
//Delete invitation
$(document).on('click', '.remove_invitation_button', function () {
    var trip_id = $(this).attr('data-invitetionid');
    var invite_user_id = $(this).attr('data-mateid');
    
    $.confirm({
        title: 'Confirm!',
        content: '<span class="disapprove-confirm-text">Are you sure you want to remove this invitation?</span> <div class="mb-3"></div>',
        columnClass: 'col-md-5 col-md-offset-5',
        closeIcon: true,
        offsetTop: 0,
        offsetBottom: 500,
        scrollToPreviousElement: false,
        scrollToPreviousElementAnimate: false,
        buttons: {
            cancel: function () {},
            confirm: {
                text: 'Confirm',
                btnClass: 'btn-red',
                keys: ['enter', 'shift'],
                action: function () {
                    $.ajax({
                        method: "POST",
                        url: "{{route('trip.ajax_cancel_invitation')}}",
                        data: {trip_id: trip_id, users_id: invite_user_id}
                    })
                    .done(function (res) {
                        var reault = JSON.parse(res)
                
                        if(reault.status == 'success'){
                            $("#mate" + invite_user_id).fadeOut("slow");
                        }
                    });
                }
            }
        }
    });
});
     
     
//Unpublish request 
$(document).on('click', '.unpublish-btn', function () {
         var request_id = $(this).attr('data-requestid');
         
        $.confirm({
            title: 'Confirm!',
            content: '<span class="disapprove-confirm-text">Are you sure you want to unpublish?</span> <div class="mb-3"></div>',
            columnClass: 'col-md-5 col-md-offset-5',
            closeIcon: true,
            offsetTop: 0,
            offsetBottom: 500,
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: {
                cancel: function () {},
                confirm: {
                    text: 'Unpublish',
                    btnClass: 'btn-red',
                    keys: ['enter', 'shift'],
                    action: function () {
                        $.post("{{url('travelmates/ajax-unpublish-request')}}",
                         {request_id: request_id},
                         function (data) {
                             if (data == "success") {
                                window.location.reload()
                             }
                         });
                    }
                }
            }
        });
    
     });
     
//Cancel request 
$(document).on('click', '.cancel_request_button', function () {
         var request_id = $(this).attr('data-requestid');
         var type = $(this).attr('data-type');
         var _this = $(this);
         $.post("{{url('travelmates/ajax-cancel-request')}}",
             {request_id: request_id},
             function (data) {
                 if (data == "success") {
                     if(type == 1){
                        _this.closest('.mates-label-block').append('<div class="join-button" request-id="' + request_id + '" status="0">Join</div>')
                        _this.closest('.mates-label-block').find('.cancel-button').remove()
                     }else{
                        window.location.reload()
                     }
                 }
             });
     });


//Leave trip plan
$(document).on('click', '.leave-request-btn', function () {
    var request_id = $(this).attr('data-requestid');

    $.confirm({
        title: 'Confirm!',
        content: '<span class="disapprove-confirm-text">Are you sure you want to leave this trip plan?</span> <div class="mb-3"></div>',
        columnClass: 'col-md-5 col-md-offset-5',
        closeIcon: true,
        offsetTop: 0,
        offsetBottom: 500,
        scrollToPreviousElement: false,
        scrollToPreviousElementAnimate: false,
        buttons: {
            cancel: function () {},
            confirm: {
                text: 'Confirm',
                btnClass: 'btn-red',
                keys: ['enter', 'shift'],
                action: function () {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('travelmate.leave_request') }}",
                        data: {request_id: request_id}
                    })
                            .done(function (res) {
                                if (res == 'success') {
                                    $("#approved" + request_id).fadeOut("slow");
                                }
                            });
                }
            }
        }
    });
});

function loadFilterContent() {
    var has_plan = $('.plan-type.active').attr('filter-type');
    mates_type = has_plan
    $.ajax({
        type: 'GET',
        url: "{{route('travelmate.new.func.ajax')}}",
        data: {type: has_plan, filters: globalFilters, order_filter:orderFilter},
        success: function (data) {
            $('.search-no-result-content').html('')
            $('.search-with-plan-block .travmates-content').html('')
            $('.search-without-plan-content .travmates-content').html('')
            $('.mates-main-content').addClass('d-none')
            $('.mates-search-content').removeClass('d-none')
            if (data.view != '') {
                if (has_plan == 1) {
                    $('.search-with-plan-block').removeClass('d-none')
                    $('.search-with-plan-block .travmates-content').append(data.view)
                    $('.search-with-plan-block').find('#mates_loader').addClass('mates_width_plan_loader')
                } else {
                    $('.search-without-plan-content').removeClass('d-none')
                    $('.search-without-plan-content .travmates-content').append(data.view)
                    $('.search-without-plan-content').find('#w_mates_loader').addClass('mates_loader')
                }
                
                if(data.count < 10){
                    if (has_plan == 1) {
                        $('.mates_pagination').hide();
                    } else {
                        $('.w_mates_pagination').hide();
                    }

                }
                $(".travelMateCountries").each(function (i, e) {
                    if (!$(e).hasClass('lightSlider')) {
                        $(e).lightSlider({
                            autoWidth: true,
                            slideMargin: 0,
                            pager: false,
                            addClass: 'country',
                            controls: false
                        });
                    }
                });

            } else {
                if (has_plan == 1) {
                    $('.mates_pagination').find(".mates_loader").hide();
                } else{
                    $('.w_mates_pagination').find(".mates_loader").hide();
                }
                $('.search-no-result-content').html('<p>There are no Travel Mates... <a class="add-plan-modal" href="javascript:;" data-toggle="modal" data-target="#requestPopup">be the first one</a></p>')
            }
            $('.search-count').html(data.count);
        }
    });
}

function getOrderFilter() {
    if (destinationsFilters.length != 0 || $('.search-mates .date.finish').val() != '') {
        updateFilters();
        loadFilterContent();
    }else{
        $.ajax({
            type: 'GET',
            url: "{{route('travelmate.new.order.filter')}}",
            data: {order_filter: orderFilter},
            success: function (data) {
                $('.mt-tab-content').html(data)
                $('.post-side-top-nav li[filter-type='+ mates_type +']').trigger('click')

                $(".travelMateCountries").each(function (i, e) {
                        if (!$(e).hasClass('lightSlider')) {
                            $(e).lightSlider({
                                autoWidth: true,
                                slideMargin: 0,
                                pager: false,
                                addClass: 'country',
                                controls: false
                            });
                        }
                    });
            }
        });
    }
}

function clearPlans() {
    loadMoreCount = 0;
    $('div.travel-mates div.post-side-inner').fadeOut();
    $('div.travel-mates div.post-side-inner').remove();
}

function join(request_id) {
    $.ajax({
        method: "POST",
        url: "{{route('travelmate.ajaxjoin')}}",
        data: {request_id: request_id}
    })
            .done(function (response) {
                console.log(response)
            });
}

//    $(".search-mates input.date").datepicker();


function updateFilters() {
    var filters = {};

    var startDate = $('.search-mates .date.start').val();
    var finishDate = $('.search-mates .date.finish').val();

    filters.dates = {
        'start_date': startDate,
        'finish_date': finishDate
    };
    filters.destinations = destinationsFilters;
    filters.countries = countriesFilters;
    filters.interests = interestsFilters;
    filters.location = locationFilter;
    filters.age = $('.search-mates .age').val();
    filters.gender = $('.search-mates .gender').val();

    globalFilters = filters;
}


$(document).ready(function () {
    //--START-- Manage Publications script
    $('.manage-tabs-nav li').on('click', function () {
       
        $('.manage-tabs-nav li').removeClass('active');
        $(this).addClass('active');
        var type = $(this).attr('data-type')
        $('.manage-tab-block').removeClass('d-none')
        $('#' + type).addClass('d-none')
        
//          $('.wp-countries .wpRequestCountries').css('width', '100%') 
    });
    
//Approve joined request 
$(document).on('click', '.approve-request-button', function () {
    var _this = $(this);
    var request_id = _this.attr('data-requestid');
    var uaer_id = _this.attr('data-mateid')
    var mates_count = _this.attr('mates')
    $.ajax({
        method: "POST",
        url: "{{route('travelmate.approve_request')}}",
        data: {request_id: request_id, mates_id: uaer_id}
    })
            .done(function (data) {
                if (data == 'success') {
                    $('.going-'+ uaer_id).html('<button type="button" class="btn btn-light-red-bordered request-action-btn remove_mate_button"'+
                                                    'data-requestid="'+ request_id +'" data-mateid="'+ uaer_id +'">'+
                                                '<i class="fa fa-trash-o" aria-hidden="true"></i>'+
                                            '</button>')
                    $('.mates-name-' + uaer_id).html('<span style="color:green;font-size:80%">(going)</span>')
                    if(mates_count == 1){
                        $('.joined-' + request_id).html('')
                    }else if(mates_count == 2){
                        $('.joined-' + request_id).find('.more-going-link').remove()
                    }else{
                         $('.joined-' + request_id).find('.going-cnt').html(mates_count-2)
                    }
                    
                    $('.going-action-block').find('button').attr('mates', mates_count-1)
                }
            });
});

//Dispprove joined request 
$(document).on('click', '.disapprove_request_button', function () {
    var request_id = $(this).attr('data-requestid');
    var uaer_id = $(this).attr('data-mateid');
     var mates_count = $(this).attr('mates');

    $.confirm({
        title: 'Confirm!',
        content: '<span class="disapprove-confirm-text">Are you sure you want to disapprove this request?</span> <div class="mb-3"></div>',
        columnClass: 'col-md-5 col-md-offset-5',
        closeIcon: true,
        offsetTop: 0,
        offsetBottom: 500,
        scrollToPreviousElement: false,
        scrollToPreviousElementAnimate: false,
        buttons: {
            cancel: function () {},
            confirm: {
                text: 'Confirm',
                btnClass: 'btn-red',
                keys: ['enter', 'shift'],
                action: function () {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('travelmate.disapprove_request') }}",
                        data: {request_id: request_id, mates_id: uaer_id}
                    })
                            .done(function (res) {
                                if (res == 'success') {
                                     $("#mate" + uaer_id).fadeOut("slow");
                             
                                    if(mates_count == 1){
                                        $('.joined-' + request_id).html('')
                                    }else if(mates_count == 2) {
                                        $('.joined-' + request_id).find('.more-going-link').remove()
                                    } else {
                                        $('.joined-' + request_id).find('.going-cnt').html(mates_count - 2)
                                    }

                                    $('.going-action-block').find('button').attr('mates', mates_count - 1)
                                }
                            });
                }
            }
        }
    });
});

//with plan load more
$('#request_with_plan_pagination').on('inview', function (event, isInView) {
    if (isInView) {
        var nextPage = parseInt($('#request_with_plan_pagenum').val()) + 1;

        var url = "{{route('travelmate.ajax.notifications_requests')}}";

        $.ajax({
            type: 'GET',
            url: url,
            data: {pagenum: nextPage},
            success: function (data) {
                if (data != '') {
                    $('div.n-with-plan-requests-block').append(data);
                    $('#request_with_plan_pagenum').val(nextPage);
                } else {
                    $('.request_with_plan_pagination').hide();
                }
            }
        });
    }
});

//Without plans script

//Unpublish request 
$(document).on('click', '.unpublish-w-plan-btn', function () {
         var request_id = $(this).attr('data-requestid');
         
        $.confirm({
            title: 'Confirm!',
            content: '<span class="disapprove-confirm-text">Are you sure you want to unpublish?</span> <div class="mb-3"></div>',
            columnClass: 'col-md-5 col-md-offset-5',
            closeIcon: true,
            offsetTop: 0,
            offsetBottom: 500,
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: {
                cancel: function () {},
                confirm: {
                    text: 'Unpublish',
                    btnClass: 'btn-red',
                    keys: ['enter', 'shift'],
                    action: function () {
                        $.post("{{url('travelmates/ajax-unpublish-request')}}",
                            {request_id: request_id, type:'without_plan'},
                            function (data) {
                                if (data == "success") {
                                   window.location.reload()
                                }
                        });
                    }
                }
            }
        });
});

//without plan load more
$('#request_without_plan_pagination').on('inview', function (event, isInView) {
    if (isInView) {
        var nextPage = parseInt($('#request_without_plan_pagenum').val()) + 1;

        var url = "{{route('travelmate.ajax.notifications_my_invitations')}}";

        $.ajax({
            type: 'GET',
            url: url,
            data: {pagenum: nextPage},
            success: function (data) {
                if (data != '') {
                    $('div.n-without-plan-requests-block').append(data);
                    $('#request_without_plan_pagenum').val(nextPage);
                    $('.wpRequestCountries').lightSlider({
                                                autoWidth: true,
                                                slideMargin: 0,
                                                pager: false,
                                                addClass: 'country',
                                                controls: false,
                                                onSliderLoad: function (el) {
                                                    $(el).width(480);
                                                }
                                            });
                } else {
                    $('.request_without_plan_pagination').hide();
                }
            }
        });
    }
});

//Accept invitation
$(document).on('click', '.accept-invitation-button', function () {
    var invtitation_id = $(this).attr('data-invitationId');
    var request_id = $(this).attr('data-requestid');

    $.confirm({
        title: 'Confirm!',
        content: '<span class="disapprove-confirm-text">Are you sure you want to accept this invitation?</span> <div class="mb-3"></div>',
        columnClass: 'col-md-5 col-md-offset-5',
        closeIcon: true,
        offsetTop: 0,
        offsetBottom: 500,
        scrollToPreviousElement: false,
        scrollToPreviousElementAnimate: false,
        buttons: {
            cancel: function () {},
            confirm: {
                text: 'Accept',
                btnClass: 'btn-green',
                keys: ['enter', 'shift'],
                action: function () {
                    $.ajax({
                        method: "POST",
                        url: "{{url_with_locale('trip/ajaxAcceptInvitation')}}",
                        data: {invitation_id: invtitation_id}
                    })
                        .done(function (res) {
                            var result = JSON.parse(res);
                            if(result.status=='success') {
                                $.post("{{url('travelmates/ajax-close-request')}}",
                                       {request_id: request_id},
                                       function (data) {
                                           if (data == "success") {
                                              window.location.reload()
                                           }
                                       });
                            }

                        });
                }
            }
        }
    });
});


//Reject invitation
$(document).on('click', '.reject-invitation-button', function () {
    var invitation_id = $(this).attr('data-invitationId');
    var user_id = $(this).attr('data-mateid');

    $.confirm({
        title: 'Confirm!',
        content: '<span class="disapprove-confirm-text">Are you sure you want to reject this invitation?</span> <div class="mb-3"></div>',
        columnClass: 'col-md-5 col-md-offset-5',
        closeIcon: true,
        offsetTop: 0,
        offsetBottom: 500,
        scrollToPreviousElement: false,
        scrollToPreviousElementAnimate: false,
        buttons: {
            cancel: function () {},
            confirm: {
                text: 'Reject',
                btnClass: 'btn-red',
                keys: ['enter', 'shift'],
                action: function () {
                    $.ajax({
                        method: "POST",
                        url: "{{url_with_locale('trip/ajaxRejectInvitation')}}",
                        data: {invitation_id: invitation_id}
                    })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if(result.status=='success') {
                            $("#invitationMate" + user_id).fadeOut("slow");
                        }
                    });
                }
            }
        }
    });
});


$(".wpRequestCountries").each(function (i, e) {
         if (!$(e).hasClass('lightSlider')) {
             $(e).lightSlider({
                 autoWidth: true,
                 slideMargin: 0,
                 pager: false,
                 addClass: 'country',
                 controls: false,
                  onSliderLoad: function (el) {
                    $(el).width(900);
                },
             });
         }
     });

//--END-- Manage Publications script

//--START-- My Requests & Invitations script
    $('.my-request-tabs-nav li').on('click', function () {
       
        $('.my-request-tabs-nav li').removeClass('active');
        $(this).addClass('active');
        var type = $(this).attr('data-type')
        $('.my-request-tab-block').removeClass('d-none')
        $('#' + type).addClass('d-none')
    });
    
//My requests load more
$('#my_requests_loader').on('inview', function (event, isInView) {
    if (isInView) {
        var nextPage = parseInt($('#my_requests_pagenum').val()) + 1;

        var url = "{{route('travelmate.ajax.my_requests')}}";

        $.ajax({
            type: 'GET',
            url: url,
            data: {pagenum: nextPage},
            success: function (data) {
                if (data != '') {
                    $('div.my-requests-block').append(data);
                    $('#my_requests_pagenum').val(nextPage);
                } else {
                    $('.my-requests-pagination').hide();
                }
            }
        });
    }
});

//My invitations load more
$('#my_invitations_loader').on('inview', function (event, isInView) {
    if (isInView) {
        var nextPage = parseInt($('#my_invitations_pagenum').val()) + 1;

        var url = "{{route('travelmate.ajax.my_invitations')}}";

        $.ajax({
            type: 'GET',
            url: url,
            data: {pagenum: nextPage},
            success: function (data) {
                if (data != '') {
                    $('div.my-invitations-block').append(data);
                    $('#my_invitations_pagenum').val(nextPage);
                } else {
                    $('.my-invitations-pagination').hide();
                }
            }
        });
    }
});

//Cancel All Invitations
    $('.cancel-all-invite-button').on('click', function () {
        var _this = $(this);
        var plan_id = _this.attr('data-plan')
        
        $.ajax({
        method: "POST",
        url: "{{route('travelmate.ajax_cancel_all_invitation')}}",
        data: {trip_id: plan_id}
    })
        .done(function (res) {
            _this.parents('div.notification-block').remove();
        });
    });

//--END-- My Requests & Invitations script


    //Filter type select
    $(".sort-select").each(function() {
  var classes = $(this).attr("class"),
      id      = $(this).attr("id"),
      name    = $(this).attr("name"),
      select  = $(this).attr("data-type"),
      type    = $(this).attr("data-sorted_by");
      
  var template =  '<div class="' + classes + '" data-type="'+ select +'">';
      template += '<span class="sort-select-trigger">' + $(this).attr("placeholder") + '</span>';
      template += '<div class="sort-options">';
      $(this).find("option").each(function() {
        template += '<span class="sort-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
      });
  template += '</div></div>';
  
  $(this).wrap('<div class="sort-select-wrapper"></div>');
  $(this).hide();
  $(this).after(template);
});

$(".sort-option:first-of-type").hover(function() {
  $(this).parents(".sort-options").addClass("option-hover");
}, function() {
  $(this).parents(".sort-options").removeClass("option-hover");
});
$(".sort-select-trigger").on("click", function() {
  $('html').one('click',function() {
    $(".sel-select").removeClass("opened");
  });
  $(this).parents(".sort-select").toggleClass("opened");
  event.stopPropagation();
});


$(".sort-option").on("click", function() {
  $(this).parents(".sort-options").find(".sort-option").removeClass("selection");
  $(this).addClass("selection");
  $(this).parents(".sort-select").removeClass("opened");
  $(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

  orderFilter = $(this).data("value")

    getOrderFilter();
});
    
    
    $('.requestCountries').lightSlider({
        autoWidth: true,
        slideMargin: 0,
        pager: false,
        addClass: 'country',
        controls: false
    });

    $("#popularTravelMates").lightSlider({
        autoWidth: true,
        pager: false,
        enableDrag: true,
        slideMargin: 20,
        prevHtml: '<i class="trav-angle-left"></i>',
        nextHtml: '<i class="trav-angle-right"></i>',
        addClass: 'post-dest-slider-wrap',
        onBeforeStart: function (el) {
            el.removeAttr("style")
        },
        responsive : [
            {
                breakpoint:767,
                settings: {
                    slideMargin:9,
                }
            },
        ],
    });



    $('.post-side-top-nav li').on('click', function () {
        $('.post-side-top-nav li').removeClass('active');
        $(this).addClass('active');
        var type = $(this).attr('data-type')
        $('.mates-tab').removeClass('d-none')
        $('#' + type).addClass('d-none')

        mates_type = $('.post-side-top-nav li.active').attr('filter-type');
        
        $('.wplan-countries .travelMateCountries').css('width', '100%')

    });

    // load more with plan mates  
    $(document).on('inview', '.mates_width_plan_loader', function (event, isInView) {
        if (isInView) {
            var type = mates_type;
            var queryString = '?type=' + mates_type+'&order_filter='+orderFilter;
            var nextPage = parseInt($('#plan_pagenum').val()) + 1;

            var url = "{{route('travelmate.new.func.ajax')}}" + queryString;

            $.ajax({
                type: 'GET',
                url: url,
                data: {pagenum: nextPage},
                success: function (data) {
                    if (data.view != '') {
                        $('#plan_tab .travmates-content').append(data.view);
                        $('#plan_pagenum').val(nextPage);

                        $(".travelMateCountries").each(function (i, e) {
                            if (!$(e).hasClass('lightSlider')) {
                                $(e).lightSlider({
                                    autoWidth: true,
                                    slideMargin: 0,
                                    pager: false,
                                    addClass: 'country',
                                    controls: false
                                });
                            }
                        });

                    } else {
                        $('.mates_pagination').find(".mates_width_plan_loader").hide();
                    }
                }
            });
        }
    });
    
    // load more mates  
    $(document).on('inview', '.mates_loader', function (event, isInView) {
        if (isInView) {
             console.log('eee')
            var type = mates_type;
            var queryString = '?type=' + mates_type+'&order_filter='+orderFilter;
            var nextPage = parseInt($('#w_plan_pagenum').val()) + 1;

            var url = "{{route('travelmate.new.func.ajax')}}" + queryString;

            $.ajax({
                type: 'GET',
                url: url,
                data: {pagenum: nextPage},
                success: function (data) {
                    if (data.view != '') {
                        $('#without_plan_tab .travmates-content').append(data.view);
                        $('#w_plan_pagenum').val(nextPage);

                        $(".travelMateCountries").each(function (i, e) {
                            if (!$(e).hasClass('lightSlider')) {
                                $(e).lightSlider({
                                    autoWidth: true,
                                    slideMargin: 0,
                                    pager: false,
                                    addClass: 'country',
                                    controls: false
                                });
                            }
                        });

                    } else {
                        $('.w_mates_pagination').find(".mates_loader").hide();
                    }
                }
            });
        }
    });


    
//--START-- Search mates
    $('.search-mates .plan-type').on('click', function () {
        $('.search-mates .plan-type').removeClass('active');
        $(this).addClass('active');
    });

//Search by date
    var dateFormat = "d MM yy",
            filter_from = $("#search_start_date").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        dateFormat: "d MM yy",
        setDate: new Date(),
        numberOfMonths: 1
    })
            .on("change", function () {
                filter_to.datepicker("option", "minDate", getDate(this));
            }),
            filter_to = $("#search_end_date").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        dateFormat: "d MM yy",
        numberOfMonths: 1
    })
            .on("change", function () {
                if (this.value != '') {
                    $('.finish-date-error-label').hide()
                }
                filter_from.datepicker("option", "maxDate", getDate(this));
            });

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }

    filter_from.datepicker("setDate", new Date());

//Search destination
    $('.search-destination').autocomplete({
        source: function (request, response) {
            jQuery.get("{{route('travelmate.filters.ajax')}}", {
                q: request.term
            }, function (data) {
                var items = [];

                data.forEach(function (s_item, index) {
                    items.push({
                        value: s_item.text, image: s_item.image, query: s_item.query, id: s_item.id, selected_id: s_item.selected_id, country_id: s_item.country_id, country_name: s_item.country_name
                    });
                });
                response(items);
            });
        },
        classes: {"ui-autocomplete": "travel-mates-autocomplate"},
        focus: function (event, ui) {
            return false;
        },
        select: function (event, ui) {
            $('.search-destination').val('');
            if ($.inArray(ui.item.selected_id, destinationsFilters) == -1) {
                destinationsFilters.push(ui.item.selected_id)
                $('.filter-destination-error-label').hide();
                $('.destination-search-block').append(getSelectedHtml(ui.item.value, 'close-search-destination', 'destination-id', ui.item.selected_id));
            }
            return false;
        },
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
                .append(getMarkupHtml(item))
                .appendTo(ul);
    };

//Remove search destination
    $(document).on('click', '.close-search-destination', function () {
        var destination_id = $(this).attr('data-destination-id');
        destinationsFilters = $.grep(destinationsFilters, function (value) {
            return value != destination_id;
        });
        $(this).closest('li').remove()

    });

//Search location
    $('.search-location').autocomplete({
        source: function (request, response) {
            jQuery.get("{{url('travelmates/ajaxGetSearchCity')}}", {
                q: request.term
            }, function (data) {
                var items = [];

                data.forEach(function (s_item, index) {
                    items.push({
                        value: s_item.text, image: s_item.image, query: s_item.query, id: s_item.id, country_name: s_item.country_name
                    });
                });
                response(items);
            });
        },
        classes: {"ui-autocomplete": "travel-mates-autocomplate"},
        focus: function (event, ui) {
            return false;
        },
        select: function (event, ui) {
            $('.search-location').val('');
            locationFilter = ui.item.id;
            $('.location-filter-block').html(getSelectedHtml(ui.item.value, 'close-search-location-filter', 'location-id', ui.item.id))
            return false;
        },
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
                .append(getMarkupHtml(item))
                .appendTo(ul);
    };

//Remove search location
    $(document).on('click', '.close-search-location-filter', function () {
        var location_id = $(this).attr('data-location-id');
        locationFilter = '';
        $(this).closest('li').remove()

    });

//Search nationality
    $('.search-nationality').autocomplete({
        source: function (request, response) {
            jQuery.get("{{url('travelmates/ajaxGetSearchCountry')}}", {
                q: request.term
            }, function (data) {
                var items = [];

                data.forEach(function (s_item, index) {
                    items.push({
                        value: s_item.text, image: s_item.image, query: s_item.query, id: s_item.id, country_name: null
                    });
                });
                response(items);
            });
        },
        classes: {"ui-autocomplete": "travel-mates-autocomplate"},
        focus: function (event, ui) {
            return false;
        },
        select: function (event, ui) {
            $('.search-nationality').val('');
            if ($.inArray(ui.item.selected_id, countriesFilters) == -1) {
                countriesFilters.push(ui.item.id)
                $('.nationality-search-block').append(getSelectedHtml(ui.item.value, 'close-search-nationality', 'country-id', ui.item.id));
            }
            return false;
        },
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
                .append(getMarkupHtml(item))
                .appendTo(ul);
    };

//Remove search nationality
    $(document).on('click', '.close-search-nationality', function () {
        var country_id = $(this).attr('data-country-id');
        countriesFilters = $.grep(countriesFilters, function (value) {
            return value != country_id;
        });
        $(this).closest('li').remove()

    });

//Search interest
  $(document).on('keydown', '#filter_interests', function (e) {
        if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey) {
            e.preventDefault();
        }
    });
    
    $("#filter_interests").bind("paste", function(e){
        var pastedData = e.originalEvent.clipboardData.getData('text');
        var inputVal = pastedData.replace(/,/g, '', pastedData);
        var pasteStr = inputVal.split(' ');
 
        $.each( pasteStr, function( key, value ) {
        if ($.inArray(value, interestsFilters) == -1) {
            if(interestsFilters.length < 10){
                   interestsFilters.push(value)
                    $('.interest-search-block').append(getSelectedHtml(value, 'close-search-interest-filter', 'interest-val', value));
                    $('.filter-interest-error-block').html('')
            }else{
                $('.filter-interest-error-block').html('The maximum tags count is 10')
            }
            
            }
              
        });
        var self = $(this);
          setTimeout(function(e) {
              self.val('');
          }, 100);
    } );

    $(document).on('keyup', '#filter_interests', function (e) {
        if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey) {
            var input_value = $(this).val().trim();
            
            if (input_value != '') {
                if($.inArray(input_value, interestsFilters) == -1){
                    if(interestsFilters.length < 10){
                        interestsFilters.push(input_value)
                        $('#filter_interests').val('');
                        $('.interest-search-block').append(getSelectedHtml(input_value, 'close-search-interest-filter', 'interest-val', input_value));
                        $('.filter-interest-error-block').html('')
                    }else{
                        $('.filter-interest-error-block').html('The maximum tags count is 10')
                    }
                }
                $(this).val('')
                $('ul.travel-mates-autocomplate').hide()
            }
        }
    });
    
    
    $('.search-interst').autocomplete({
        source: my_interests,
        classes: {"ui-autocomplete": "travel-mates-autocomplate", },
        select: function (event, ui) {
            event.preventDefault();
            $('.search-interst').val('');
            if ($.inArray(ui.item.value, interestsFilters) == -1) {
                if(interestsFilters.length < 10){
                    interestsFilters.push(ui.item.value)
                    $('.interest-search-block').append(getSelectedHtml(ui.item.value, 'close-search-interest-filter', 'interest-val', ui.item.value));
                }else{
                    $('.filter-interest-error-block').html('The maximum tags count is 10')
                }
            }
        },
    });
    
    

    //Remove interest
    $(document).on('click', '.close-search-interest-filter', function () {
        var interst = $(this).attr('data-interest-val');
        interestsFilters = $.grep(interestsFilters, function (value) {
            return value != interst;
        });
         if(interestsFilters.length < 10){
               $('.filter-interest-error-block').html('')
          }
        $(this).closest('li').remove()
    });

    // Show/hide mates search block on mobile
    $('.mobile-search-travel-mates-toggler').on('click', function () {
        $('.travel-mates-mobile-search').toggleClass('mobile-show');
    })

    $('.search-mates .search-filter-button').on('click', function () {
        if (destinationsFilters.length == 0 || $('.search-mates .date.finish').val() == '') {
            $('.filter-destination-error-label').html('Destination field is required.')
            $('.finish-date-error-label').html('Finish date field is required.')
            return false;
        }

        // Show/hide mates search block on mobile
        $('.travel-mates-mobile-search').toggleClass('mobile-show');
        updateFilters();
        loadFilterContent();
    });



//--END-- Search mates
});

function getSelectedHtml(title, class_name, data_attr, id) {
    var html = '<li><span class="search-selitem-title">' + title + '</span>' +
            '<span class="close-search-item ' + class_name + '" data-' + data_attr + '="' + id + '"><i class="trav-close-icon" dir="auto"></i></span></li>'

    return html;
}


//Add destination item
function getMarkupHtml(response) {
    if (response) {
        var markup = '<div class="search-country-block">';
        if (response.image) {
            markup += '<div class="search-country-imag"><img src="' + response.image + '" /></div>';
        }
        markup += '<div class="span10">' +
                '<div class="search-country-info">' +
                '<div class="search-country-text">' + markDestinationMatch(response.value, response.query) + '</div>';
        if (response.country_name != null) {
            markup += '<div class="search-country-type">City in ' + response.country_name + '</div>';
        } else {
            markup += '<div class="search-country-type">Country</div>';
        }
        markup += '</div></div>';

        return markup;
    }
}

function markDestinationMatch(text, term) {
    var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
    var output = text.replace(regEx, "<span class='destination-rendered'>$1</span>");
    return output;
}
</script>
