@php
$title = 'Travooo - travel mates';
@endphp

@extends('site.travelmates.template.travelmate')

@section('content')
<div class="post-block post-flight-style post-block-notifications">
    <div class="post-side-top">
        <div class="post-side-top-inner">
            <a href="{{route('travelmate.newest')}}" type="button" class="btn btn-light-bg-grey btn-bordered btn-back">
                <i class="trav-angle-left"></i>
            </a>
            <h3 class="side-ttl">@choice('dashboard.notification', 2)</h3>
        </div>
        <div class="post-top-tabs">
            <div class="detail-list">
                <div class="detail-block @if($active_tab=='requests')  current @endif">
                    <a href="{{url('travelmates-notifications-requests')}}"><b>Manage Publications</b></a>
                </div>
                <div class="detail-block @if($active_tab=='invitations')  current @endif">
                    <a href="{{url('travelmates-notifications-invitations')}}"><b>My Requests & Invitations</b></a>
                </div>
            </div>
        </div>
    </div>
    <div class="manage-side-top">
        <ul class="my-request-tabs-nav">
            <li class="active" my-request-type="1" data-type="my_invitations">
                <h3 class="side-ttl">My Requests</h3>
            </li>
            <li class="" my-request-type="2" data-type="my_requests">
                <h3 class="side-ttl">My Invitations</h3>
            </li>
        </ul>
    </div>
    <div class="post-side-inner">
        <div class="my-request-tab-block" id="my_requests">
            <div class="travel-mates-notification-wrapper">
                <div class="my-requests-block">
                    @include('site/travelmates/partials/_notifications-my-requests')
                </div>
                <div class="my-requests-pagination">
                    <input type="hidden" id="my_requests_pagenum" value="1">
                    <div id="my_requests_loader" class="mates-animation-content post-side-inner">
                        <div class="notification-block">
                            <div class="n-img-wrap animation">
                            </div>
                            <ul class="n-tp-info-list" >
                                <li><div class="mates-info-animation w-135 animation"></div></li>
                                <li><div class="mates-info-animation w-310 animation" ></div></li>
                                <li><div class="mates-info-animation w-265 animation"></div></li>
                                <li>
                                    <div class="mates-info-animation w-285 animation"></div>
                                    <div class="n-info-animation animation"></div>
                                </li>
                            </ul>
                            <div class="btn-wrap request-btn-wrap pull-right">
                                <div class="nb-info-animation animation"></div>
                                <div class="nb-s-info-animation  animation"></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="my-request-tab-block d-none" id="my_invitations">
            <div class="travel-mates-notification-wrapper">
                <div class="my-invitations-block">
                    @include('site/travelmates/partials/_notifications-my-invitations')
                </div>
                <div class="my-invitations-pagination">
                    <input type="hidden" id="my_invitations_pagenum" value="1">
                    <div id="my_invitations_loader" class="mates-animation-content post-side-inner">
                        <div class="notification-block">
                            <div class="n-img-wrap animation">
                            </div>
                            <ul class="n-tp-info-list" >
                                <li><div class="mates-info-animation w-135 animation"></div></li>
                                <li><div class="mates-info-animation w-310 animation" ></div></li>
                                <li><div class="mates-info-animation w-265 animation"></div></li>
                                <li>
                                    <div class="mates-info-animation w-285 animation"></div>
                                    <div class="n-info-animation animation"></div>
                                </li>
                            </ul>
                            <div class="btn-wrap request-btn-wrap pull-right">
                                <div class="nb-info-animation animation"></div>
                                <div class="nb-s-info-animation  animation"></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('before_scripts')
@include('site/travelmates/partials/request-popup')
@include('site.travelmates._travel-mates-js')
@endsection