@foreach($sent_requests AS $sent_request)
<div class="notification-block" id="invitation{{$sent_request->id}}">
    <div class="img-wrap">
        <div class="mt-map-wrap">
            <img src="{{get_plan_map($sent_request->plan, 330, 200)}}" alt="" class="travel-map"
                 style="width:330px;height:200px;">

            <a href="{{route('trip.plan', $sent_request->plan->id)}}" class="view-plan-button">
                <img src="{{asset('assets2/image/collapse-geo.png')}}" class="geo"><span>View Plan</span>
            </a>
        </div>
        <span class="rep-creation-time">{{$sent_request->created_at->format('M d, Y')}}</span>
    </div>
    <div class="notification-txt">
        <h3 class="not-ttl">
            <a href="{{route('trip.plan', $sent_request->plan->id)}}">{{$sent_request->plan->title}}</a>
        </h3>
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{check_profile_picture(@$sent_request->plan->author->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{route('profile.show', ['id' => $sent_request->plan->author->id])}}">{{$sent_request->plan->author->name}}</a>
                    {!! get_exp_icon($sent_request->plan->author) !!}
                </div>
                <div class="post-info" dir="auto">{{$sent_request->plan->author->display_name}}</div>
            </div>
            @if(count($sent_request->going()->where('status', 1)->get())>0)
            <div class="post-map-info-caption map-blue" data-toggle="modal"
                 data-target="#notificationGoingPopup{{$sent_request->id}}"
                 style='cursor:pointer;'>
                <ul class="foot-avatar-list">
                    @foreach($sent_request->going()->where('status', 1)->orderBy('id', 'DESC')->limit(3)->get() AS $gogo)
                    <li><img class="small-ava"
                             src="{{$gogo->author->profile_picture}}" alt="ava"
                             style="height:25px;"></li>
                    @endforeach
                </ul>
                <div class="map-label-txt">
                    @lang('travelmate.count_more_going', [
                    'count' => '+' . count($sent_request->going()->where('status', 1)->get())
                    ])
                </div>
            </div>
            @else
            <div class="post-map-info-caption map-blue">
                <div class="post-map-info-caption map-blue">
                    <ul class="foot-avatar-list">
                    </ul>
                    <div class="map-label-txt">
                        @lang('trip.count_are_going', ['count' => 0])
                    </div>
                </div>
            </div>
            @endif
        </div>
        <ul class="not-info-list">
            <li>
                <i class="trav-point-flag-icon"></i>
                <span>@lang('trip.starting_dd')</span>
                <b class="date text-uppercase">{{plan_starting_date($sent_request->plan)}}</b>
            </li>
            <li>
                <i class="trav-budget-icon"></i>
                <span>@lang('trip.budget_dd')</span>
                <b>{{$sent_request->plan->budget ? '$'.$sent_request->plan->budget : 'n/a'}}</b>
            </li>
            <li>
                <i class="trav-clock-icon"></i>
                <span>@lang('trip.duration_dd')</span>
                <b>{{calculate_duration($sent_request->plan->id, 'all') ? calculate_duration($sent_request->plan->id, 'all') : 'n/a'}}</b>
            </li><br>
            <li class="pl-0">
                <i class="trav-map-marker-icon"></i>
                <span>@choice('trip.destination_dd', 2)</span>
                <b>{{$sent_request->plan->trips_places()->count()}}</b>
            </li>
            <li>
                <div class="request-countries">
                    <div @if($sent_request->plan->trips_countries()->count() > 1)class="requestCountries"@else class="country" @endif>
                          @foreach($sent_request->plan->trips_countries()->groupBy('countries_id')->get() as $country)
                          @if ($loop->iteration > 1)
                          <img src="{{asset('assets2/image/arrow.png')}}" class="arrow">
                        @endif
                        <div style="display: inline-flex;height: 42px;justify-content: center;align-items: center;">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/small/{{strtolower($country->country->iso_code)}}.png" class="flag" alt="flag"><span>{{$country->country->transsingle->title}}</span>
                        </div>
                        @endforeach
                    </div>
                </div>
            </li>
        </ul>
        <div class="post-top-info-wrap pt-20">
            <div class="post-top-info-txt pl-0">
                <div class="post-top-name">
                    <span class="join-desc">You requested to join 
                        <a class="post-name-link" href="{{route('profile.show', ['id' => $sent_request->plan->author->id])}}" style="font-weight: 500 !important">{{$sent_request->plan->author->name}}'s</a>
                    {!! get_exp_icon($sent_request->plan->author) !!} plan.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="btn-wrap request-btn-wrap">
        @if($sent_request->going()->where('users_id', Auth::user()->id)->first()->status==0)
         <button type="button" class="btn btn-light-bg-grey btn-bordered mt-15 mb-3"
                data-toggle="modal"
                data-target="#notificationGoingPopup{{$sent_request->id}}">More
        </button>
        <div class="btn-label">@lang('travelmate.pending_approval')</div>
        <button type="button" class="btn btn-light-red-bordered cancel_request_button" data-type="2" data-requestid="{{$sent_request->id}}">
            @lang('buttons.general.cancel')
        </button>
        @elseif($sent_request->going()->where('users_id', Auth::user()->id)->first()->status==1)
         <button type="button" class="btn btn-light-bg-grey btn-bordered mt-15 mb-3"
                data-toggle="modal"
                data-target="#notificationGoingPopup{{$sent_request->id}}">More
        </button>
        <div class="btn-label green">@lang('other.approved')</div>
        <button type="button" class="btn btn-light-red-bordered cancel_request_button" data-type="2" data-requestid="{{$sent_request->id}}">
            Leave
        </button>
        @elseif($sent_request->going()->where('users_id', Auth::user()->id)->first()->status==2)
        <div class="btn-label red">Disapproved</div>
        @endif
    </div>
</div>
<?php $request = $sent_request; ?>
@include('site/travelmates/partials/notification-going-popup')
@endforeach