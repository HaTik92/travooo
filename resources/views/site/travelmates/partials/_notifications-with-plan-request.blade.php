@foreach($join_with_plans_requests AS $join_request)
<div class="notification-block" id="invitation{{$join_request->id}}">
    <div class="img-wrap">
        <div class="mt-map-wrap">
            <img src="{{get_plan_map($join_request->plan, 330, 200)}}" alt="" class="travel-map"
                 style="width:330px;height:200px;">
            <a href="{{route('trip.plan', $join_request->plan->id)}}" class="view-plan-button">
                <img src="{{asset('assets2/image/collapse-geo.png')}}" class="geo"><span>View Plan</span>
            </a>
        </div>
        <span class="rep-creation-time">{{$join_request->created_at->format('M d, Y')}}</span>
    </div>
    <div class="notification-txt">
        <h3 class="not-ttl">
            <a href="{{route('trip.plan', $join_request->plan->id)}}">{{$join_request->plan->title}}</a>
        </h3>
        <ul class="not-info-list request-not-info-list">
            <li>
                <i class="trav-point-flag-icon"></i>
                <span>@lang('trip.starting_dd')</span>
                <b class="date text-uppercase">{{plan_starting_date($join_request->plan)}}</b>
            </li>
            <li>
                <i class="trav-budget-icon"></i>
                <span>@lang('trip.budget_dd')</span>
                <b>{{$join_request->plan->budget ? '$'.$join_request->plan->budget : 'n/a'}}</b>
            </li>
            <li>
                <i class="trav-clock-icon"></i>
                <span>@lang('trip.duration_dd')</span>
                <b>{{calculate_duration($join_request->plan->id, 'all') ? calculate_duration($join_request->plan->id, 'all') : 'n/a'}}</b>
            </li><br>
            <li class="pl-0">
                <i class="trav-map-marker-icon"></i>
                <span>@choice('trip.destination_dd', 2)</span>
                <b>{{$join_request->plan->trips_places()->count()}}</b>
            </li>
            <li>
                <div class="request-countries">
                    <div @if($join_request->plan->trips_countries()->count() > 1)class="requestCountries"@else class="country" @endif>
                          @foreach($join_request->plan->trips_countries()->groupBy('countries_id')->get() as $country)
                          @if ($loop->iteration > 1)
                          <img src="{{asset('assets2/image/arrow.png')}}" class="arrow">
                        @endif
                        <div style="display: inline-flex;height: 42px;justify-content: center;align-items: center;">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/small/{{strtolower($country->country->iso_code)}}.png" class="flag" alt="flag"><span>{{$country->country->transsingle->title}}</span>
                        </div>
                        @endforeach
                    </div>
                </div>
            </li>
        </ul>
        @if(@$join_request->going()->where('status', 0)->count() > 0)
        <div class="post-top-info-wrap pt-20 joined-{{@$join_request->id}}">
            <div class="post-top-avatar-wrap">
                <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{check_profile_picture(@$join_request->going()->where('status', 0)->first()->author->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{route('profile.show', ['id' => $join_request->going()->where('status', 0)->first()->author->id])}}">{{$join_request->going()->where('status', 0)->first()->author->name}}</a>
                    {!! get_exp_icon($join_request->going()->where('status', 0)->first()->author) !!}
                    @if(@$join_request->going()->where('status', 0)->count() > 1)
                    <a href="javascript:;"  data-toggle="modal"  data-target="#goingPopup{{$join_request->id}}" class="more-going-link"> and +<span class="going-cnt">{{@$join_request->going()->where('status', 0)->count() -1}}</span> More</a>
                    @endif
                    <span class="join-desc">wants to join you.</span>
                </div>
                <div class="post-info" dir="auto">{{$join_request->going()->first()->author->display_name}}</div>
            </div>
        </div>
        @endif
    </div>
    <div class="btn-wrap request-btn-wrap">
        <button type="button" class="btn btn-light-bg-grey btn-bordered mt-15 mb-3"
                data-toggle="modal"
                data-target="#goingPopup{{$join_request->id}}">More
        </button>
        <button type="button" class="btn btn-light-red-bordered unpublish-btn"
                data-requestid="{{$join_request->id}}">Unpublish
        </button>
    </div>
</div>
<?php $request = $join_request; ?>
@include('site/travelmates/partials/going-popup')
@endforeach