@foreach($invitations AS $invitation)
@include('site/travelmates/partials/invited-going-popup')
<div class="notification-block" invitation-id="{{$invitation->id}}">
    <div class="img-wrap">
        <div class="mt-map-wrap">
            <img src="{{get_plan_map(@$invitation->plan, 330, 200)}}" alt="" class="travel-map"
                 style="width:330px;height:200px;">
            <a href="{{route('trip.plan', @$invitation->plan->id)}}" class="view-plan-button">
                <img src="{{asset('assets2/image/collapse-geo.png')}}" class="geo"><span>View Plan</span>
            </a>
        </div>
        <span class="rep-creation-time">{{@$invitation->created_at->format('M d, Y')}}</span>
    </div>
    <div class="notification-txt">
        <h3 class="not-ttl">
            <a href="{{route('trip.plan', @$invitation->plan->id)}}">{{$invitation->plan->title}}</a>
        </h3>
        <ul class="not-info-list request-not-info-list">
            <li>
                <i class="trav-point-flag-icon"></i>
                <span>@lang('trip.starting_dd')</span>
                <b class="date text-uppercase">{{plan_starting_date(@$invitation->plan)}}</b>
            </li>
            <li>
                <i class="trav-budget-icon"></i>
                <span>@lang('trip.budget_dd')</span>
                <b>{{@$invitation->plan->budget ? '$'.@$invitation->plan->budget : 'n/a'}}</b>
            </li>
            <li>
                <i class="trav-clock-icon"></i>
                <span>@lang('trip.duration_dd')</span>
                <b>{{calculate_duration($invitation->plan->id, 'all') ? calculate_duration($invitation->plan->id, 'all') : 'n/a'}}</b>
            </li><br>
            <li class="pl-0">
                <i class="trav-map-marker-icon"></i>
                <span>@choice('trip.destination_dd', 2)</span>
                <b>{{$invitation->plan->trips_places()->count()}}</b>
            </li>
            <li>
                <div class="request-countries">
                    <div @if($invitation->plan->trips_countries()->count() > 1)class="requestCountries"@else class="country" @endif>
                          @foreach($invitation->plan->trips_countries()->groupBy('countries_id')->get() as $country)
                          @if ($loop->iteration > 1)
                          <img src="{{asset('assets2/image/arrow.png')}}" class="arrow">
                        @endif
                        <div style="display: inline-flex;height: 42px;justify-content: center;align-items: center;">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/small/{{strtolower($country->country->iso_code)}}.png" class="flag" alt="flag"><span>{{$country->country->transsingle->title}}</span>
                        </div>
                        @endforeach
                    </div>
                </div>
            </li>
        </ul>
       
        <div class="post-top-info-wrap pt-20">
            <div class="post-top-avatar-wrap">
                <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{check_profile_picture(@$invitation->user->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <span class="join-desc">You invited</span>
                    <a class="post-name-link" href="{{route('profile.show', ['id' => @$invitation->user->id])}}">{{@$invitation->user->name}}</a> {!! get_exp_icon(@$invitation->user) !!}
                   @php $invite_count = \App\Models\TripPlans\TripsContributionRequests::where('plans_id', $invitation->plans_id)->where('users_id', '!=', auth()->id())->where('status', 0)->count()  @endphp
                    @if($invite_count > 1)
                    <a href="javascript:;" data-toggle="modal"  data-target="#invitedGoingPopup{{@$invitation->plans_id}}" class="more-going-link">and +{{$invite_count - 1}} others</a>
                    @endif
                    <span class="join-desc">to your plan.</span>
                </div>
                <div class="post-info" dir="auto">{{@$invitation->user->display_name}}</div>
            </div>
        </div>
       
    </div>
    <div class="btn-wrap request-btn-wrap">
        <button type="button" class="btn btn-light-bg-grey btn-bordered mt-15 mb-3"
                data-toggle="modal"
                data-target="#invitedGoingPopup{{@$invitation->plans_id}}">More
        </button>
        @if(\App\Models\TripPlans\TripsContributionRequests::where('plans_id', $invitation->plans_id)->where('users_id', '!=', auth()->id())->where('status', 1)->count() > 0)
        <button type="button" class="btn btn-light-bg-grey btn-bordered mb-3"
                disabled>Cancel All
        </button>
        @else
            <button type="button" class="btn btn-light-red-bordered mb-3 {{($invite_count > 1)?'cancel-all-invite-button':'cancel-invite-button'}}"
                data-plan="{{@$invitation->plans_id}}" data-user="{{@$invitation->user->id}}">{{($invite_count > 1)?'Cancel All':'Cancel'}}
            </button>
        @endif
    </div>
</div>
@endforeach