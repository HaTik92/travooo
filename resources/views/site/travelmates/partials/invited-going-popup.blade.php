<!-- going popup -->
<div class="modal fade white-style" data-backdrop="false" id="invitedGoingPopup{{$invitation->plans_id}}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-700" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-going-block post-mobile-full">
                <div class="post-top-layer">
                    <div class="post-top-info">
                        <h3 class="info-title">{{@$invitation->plan->title}}</h3>
                        <ul class="info-list">
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">{{calculate_duration(@$invitation->plan->id, 'all')}}</p>
                                    <p class="sub-title">@lang('trip.duration')</p>
                                </div>
                            </li>
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-budget-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">$ {{@$invitation->plan->budget}}</p>
                                    <p class="sub-title">@lang('trip.budget')</p>
                                </div>
                            </li>
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-distance-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">{{calculate_distance(@$invitation->plan->id)}}</p>
                                    <p class="sub-title">@lang('trip.distance')</p>
                                </div>
                            </li>
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-map-marker-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">{{@$invitation->plan->trips_places()->count()}}</p>
                                    <p class="sub-title">@choice('trip.place', 2)</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="post-photo-wrap">
                        <ul class="photo-list">
                            @if($invitation->plan->trips_places()->count())
                            @foreach($invitation->plan->trips_places()->take(3)->get() AS $places)
                            <li>
                                <img src="{{@check_place_photo2($places->place)}}" alt="image" class="image"  style='width:60px;height:60px;'>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="post-sub-info-layer">
                    <div class="post-sub-layer">
                            People going with You.
                    </div>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar">
                    <?php
                     $array_statuses = array(0, 1);
                    ?>
                    @foreach(\App\Models\TripPlans\TripsContributionRequests::where('plans_id', $invitation->plans_id)->where('users_id', '!=', auth()->id())->whereIn('status', $array_statuses)->get() AS $going)
                        <div class="people-row" id="mate{{$going->user->id}}">
                            <div class="main-info-layer">
                                <div class="img-wrap">
                                    <img class="ava" src="{{check_profile_picture($going->user->profile_picture)}}"
                                         style='width:50px;height:50px'>
                                </div>
                                <div class="txt-block">
                                    <div class="name"><a href="{{url('profile/'.$going->user->id)}}" class="tm-user-link" target="_blank">{{$going->user->name}} {!! get_exp_icon($going->user) !!}</a>
                                        @if($going->status==0)
                                            <span style="color:red;font-size:80%">(pending approval)</span>
                                        @elseif($going->status==1)
                                            <span style="color:green;font-size:80%">(going)</span>
                                        @endif
                                    </div>
                                    <div class="info-line">
                                        @if(get_users_age($going->user->id) !='')
                                            <div class="info-part">{{get_users_age($going->user->id).' years'}}</div>
                                        @endif
                                        <div class="info-part">{{@$going->user->country_of_nationality->transsingle->title}}</div>
                                        <div class="info-part">{{process_gender($going->user->gender)}}</div>
                                    </div>
                                </div>
                                  <div class="going-action-block">
                                    @if($invitation->author->id==Auth::guard('user')->user()->id)
                                        <button type="button"
                                                class="btn btn-light-red-bordered request-action-btn remove_invitation_button"
                                                data-invitetionid="{{$invitation->plans_id}}" data-mateid="{{$going->user->id}}">
                                           <i class="fa fa-times" aria-hidden="true"></i>
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
