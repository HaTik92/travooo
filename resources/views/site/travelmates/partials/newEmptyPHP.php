{
                key: 'Saudia Airlines',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Saudia Airlines',
                key: 'Manufacturing',
                color: blues[1],
                everExpanded: false
            }, {
                parent: 'Saudia Airlines',
                key: 'Logistics & Transportation',
                color: blues[1],
                everExpanded: false
            }, {
                parent: 'Saudia Airlines',
                key: 'Accommodation & Food Services',
                color: blues[1],
                everExpanded: false
            }, {
                parent: 'Manufacturing',
                key: 'Rubber tyres and tubes (0)',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Manufacturing',
                key: 'Control equipment (0)',
                color: '#ff0000',
                everExpanded: false
            }, {
                parent: 'Manufacturing',
                key: 'Engines and turbines (5)',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Manufacturing',
                key: 'Air and spacecraft (2)',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Logistics & Transportation',
                key: 'Warehousing and storage (65)',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Logistics & Transportation',
                key: 'Cargo handling (8)',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Logistics & Transportation',
                key: 'Service activities incidental to air transportation (10)',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Logistics & Transportation',
                key: 'Other transportation support activities (10)',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Accommodation & Food Services',
                key: 'Other food service activities (5)',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Accommodation & Food Services',
                key: 'Short term accommodation activities (80)',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Seats',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Piston engines',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Turbo-jets',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Reaction engines',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Parts of spark-ignition',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Parts of turbo-jets',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Balloons and dirigibles',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Helicopters',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Aeroplanes < 2000kg',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Aeroplanes > 2000kg',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Spacecraft',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Parts of aircraft and spacecraft',
                color: blues[0],
                everExpanded: false
            }, {
                parent: 'Air and spacecraft (2)',
                key: 'Air and spacecraft manufacturing services',
                color: blues[0],
                everExpanded: false
            }