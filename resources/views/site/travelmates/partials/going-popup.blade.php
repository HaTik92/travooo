<!-- going popup -->
<div class="modal fade white-style" data-backdrop="false" id="goingPopup{{$request->id}}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-700" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-going-block post-mobile-full">
                <div class="post-top-layer">
                    <div class="post-top-info">
                        <h3 class="info-title">{{@$request->plan->title}}</h3>
                        <ul class="info-list">
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">{{calculate_duration(@$request->plan->id, 'all')}}</p>
                                    <p class="sub-title">@lang('trip.duration')</p>
                                </div>
                            </li>
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-budget-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">$ {{@$request->plan->budget}}</p>
                                    <p class="sub-title">@lang('trip.budget')</p>
                                </div>
                            </li>
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-distance-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">{{calculate_distance(@$request->plan->id)}}</p>
                                    <p class="sub-title">@lang('trip.distance')</p>
                                </div>
                            </li>
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-map-marker-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">{{@$request->plan->trips_places()->count()}}</p>
                                    <p class="sub-title">@choice('trip.place', 2)</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="post-photo-wrap">
                        <ul class="photo-list">
                            @if($request->plan->trips_places()->count())
                            @foreach($request->plan->trips_places()->take(3)->get() AS $places)
                            <li>
                                <img src="{{@check_place_photo2($places->place)}}" alt="image" class="image"  style='width:60px;height:60px;'>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="post-sub-info-layer">
                    <div class="post-sub-layer">
                        @if($request->plan->author->id == Illuminate\Support\Facades\Auth::guard('user')->user()->id)
                            People going with You.
                        @else
                           @lang('travelmate.people_going_with') <span>{{$request->author->name}}</span>
                        @endif
                    </div>
                    @if($request->plan->author->id == Illuminate\Support\Facades\Auth::guard('user')->user()->id)
                        <div class="post-sub-buttons">
                            @if($request->status == -1)
                            <div class="pr-3">Closed</div>
                            @else
                            <button type="button"
                                class="btn btn-small btn-light-red-bordered sub-btn mr-2 close-requests-btn"
                                data-requestid="{{$request->id}}">
                                Close Requests
                            </button>
                            @endif
                            <button type="button"
                                class="btn btn-small btn-danger btn-custom sub-btn unpublish-btn"
                                data-requestid="{{$request->id}}" data-mateid="">
                                Unpublish
                            </button>
                        </div>
                    @endif
                </div>
                <div class="post-people-block-wrap mCustomScrollbar">
                    <?php
                        $array_statuses = array(0, 1);
                    ?>
                    @foreach($request->going()->whereIn('status', $array_statuses)->orderBy('id', 'desc')->get() AS $going)
                        <div class="people-row" id="mate{{$going->author->id}}">
                            <div class="main-info-layer">
                                <div class="img-wrap">
                                    <img class="ava" src="{{check_profile_picture($going->author->profile_picture)}}"
                                         style='width:50px;height:50px'>
                                </div>
                                <div class="txt-block">
                                    <div class="name"><a href="{{url('profile/'.$going->author->id)}}" class="tm-user-link" target="_blank">{{$going->author->name}} {!! get_exp_icon($going->author) !!}</a>
                                        @if($request->author->id== Illuminate\Support\Facades\Auth::guard('user')->user()->id)
                                        <span class="mates-name-{{$going->author->id}}">
                                            @if($going->status==0)
                                                <span style="color:red;font-size:80%">(pending approval)</span>
                                            @elseif($going->status==1)
                                                <span style="color:green;font-size:80%">(going)</span>
                                            @endif
                                        </span>
                                        @endif
                                    </div>
                                    <div class="info-line">
                                        @if(get_users_age($going->author->id) !='')
                                            <div class="info-part">{{get_users_age($going->author->id).' years'}}</div>
                                        @endif
                                        <div class="info-part">{{@$going->author->country_of_nationality->transsingle->title}}</div>
                                        <div class="info-part">{{process_gender($going->author->gender)}}</div>
                                    </div>
                                </div>
                                <div class="going-action-block going-{{$going->author->id}}">
                                    @if($request->author->id==Auth::guard('user')->user()->id)
                                        @if($going->status==0)
                                        <button type="button"
                                                class="btn btn-light-green-bordered request-action-btn approve-request-button"
                                                data-requestid="{{$request->id}}" data-mateid="{{$going->author->id}}" mates="{{@$request->going()->where('status', 0)->count()}}">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </button>
                                        <button type="button"
                                                class="btn btn-light-red-bordered request-action-btn disapprove_request_button"
                                                data-requestid="{{$request->id}}" data-mateid="{{$going->author->id}}" mates="{{@$request->going()->where('status', 0)->count()}}">
                                           <i class="fa fa-times" aria-hidden="true"></i>
                                        </button>
                                        @elseif($going->status==1)
                                            <button type="button"
                                                    class="btn btn-light-red-bordered request-action-btn remove_mate_button"
                                                    data-requestid="{{$request->id}}" data-mateid="{{$going->author->id}}">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
