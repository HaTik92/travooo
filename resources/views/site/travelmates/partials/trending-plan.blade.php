<div class="top-banner-block top-travel-main-banner"
     style="background-image: url('{{asset("assets2/image/staticmap.png")}}')">
    <div class="top-banner-inner">
        <div class="banner-row">
            <div class="travel-left-info">
                <div class="travel-banner-label"><i
                            class="trav-trending-destination-icon"></i> @lang('trip.trending_trip_plan')</div>
                <h3 class="travel-title">Around the world in 21 days</h3>
                <div class="left-info-block">

                </div>
                <div class="left-info-block">
                    <i class="trav-map"></i>
                    <span>@lang('travelmate.view_in_the_map')</span>
                </div>
                <div class="btn-wrap">
                    <a class="btn btn-light-primary" href="#">@lang('travelmate.ask_to_join')</a>
                </div>
            </div>
            <div class="travel-right-info">
                <ul class="sub-list">
                    <li>
                        <div class="icon-wrap">
                            <i class="trav-comment-plus-icon"></i>
                        </div>
                        <div class="ctxt">
                            <div class="top-txt">28K</div>
                            <div class="sub-txt">@choice('travelmate.travel_mate', 1)</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon-wrap">
                            <i class="trav-day-duration-icon"></i>
                        </div>
                        <div class="ctxt">
                            <div class="top-txt">@choice('time.count_day', 3, ['count' => 3])</div>
                            <div class="sub-txt">@lang('trip.duration')</div>
                        </div>
                    </li>
                    <li>
                        <div class="icon-wrap">
                            <i class="trav-user-rating-icon"></i>
                        </div>
                        <div class="ctxt">
                            <div class="top-txt">{{count($trending_plan->plan->cities)}}</div>
                            <div class="sub-txt">@choice('trip.destination_dd', 2)</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="banner-row" style="overflow:hidden;">
            <div class="head-trip-plan_trip-line">
                <div class="trip-line" id="tripLineSlider">
                    <div class="trip-line_slide">
                        <div class="trip-line_slide__inner">
                            <div class="trip-icon">
                                <img src="./assets2/image/flag-img-1.png" alt="flag-icon">
                            </div>
                            <div class="trip-content">
                                <div class="trip-line-name">Morocco</div>
                                <div class="trip-line-tag">
                                    <span class="place-tag">@choice('region.count_city', 1, ['count' => 1])</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="trip-line_slide">
                        <div class="trip-line_slide__inner">
                            <div class="trip-icon blue-icon">
                                <i class="trav-set-location-icon"></i>
                            </div>
                            <div class="trip-content">
                                <div class="trip-line-name blue">Rabat-Sale</div>
                                <div class="trip-line-tag">
                                    <span class="place-tag">@choice('place.count_place', 1, ['count' => 1])</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="trip-line_slide">
                        <div class="trip-line_slide__inner">
                            <div class="trip-icon">
                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/ae.png"
                                     alt="flag-icon" style='width:32px;height:32px;'>
                            </div>
                            <div class="trip-content">
                                <div class="trip-line-name">United arab emirates</div>
                                <div class="trip-line-tag">
                                    <span class="place-tag">@choice('region.count_city', 1, ['count' => 1])</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="trip-line_slide">
                        <div class="trip-line_slide__inner">
                            <div class="trip-icon disabled-icon">
                                <i class="trav-radio-checked2"></i>
                            </div>
                            <div class="trip-content">
                                <div class="trip-line-name">Dubai</div>
                                <div class="trip-line-tag">
                                    <span class="place-tag">@choice('place.count_place', 1, ['count' => 1])</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="trip-line_slide">
                        <div class="trip-line_slide__inner">
                            <div class="trip-icon">
                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/jp.png"
                                     alt="flag-icon" style='width:32px;height:32px;'>
                            </div>
                            <div class="trip-content">
                                <div class="trip-line-name">Japan</div>
                                <div class="trip-line-tag">
                                    <span class="place-tag">@choice('region.count_city', 3, ['count' => 3])</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="trip-line_slide">
                        <div class="trip-line_slide__inner">
                            <div class="trip-icon disabled-icon">
                                <i class="trav-radio-checked2"></i>
                            </div>
                            <div class="trip-content">
                                <div class="trip-line-name">Tokyo</div>
                                <div class="trip-line-tag">
                                    <span class="place-tag">@choice('trip.count_place', 2, ['count' => 5])</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="trip-line_slide">
                        <div class="trip-line_slide__inner">
                            <div class="trip-icon disabled-icon">
                                <i class="trav-radio-checked2"></i>
                            </div>
                            <div class="trip-content">
                                <div class="trip-line-name">Nagoya</div>
                                <div class="trip-line-tag">
                                    <span class="place-tag">@choice('trip.count_place', 2, ['count' => 3])</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="trip-line_slide">
                        <div class="trip-line_slide__inner">
                            <div class="trip-icon disabled-icon">
                                <i class="trav-radio-checked2"></i>
                            </div>
                            <div class="trip-content">
                                <div class="trip-line-name">Osaka</div>
                                <div class="trip-line-tag">
                                    <span class="place-tag">@choice('trip.count_place', 2, ['count' => 5])</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>