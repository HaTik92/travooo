<!-- invitation popup -->
<div class="modal fade white-style" data-backdrop="false" id="invitationPopup" user-id="" w-plan-id="" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-780" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-top-bordered post-request-modal-block post-mobile-full">
                <div class="post-side-top">
                    <div class="post-top-txt horizontal">
                        <h3 class="side-ttl">@lang('trip.select_a_trip_plan') <span
                                class="count inv-count">{{@count($my_plans)}}</span></h3>
                    </div>
                </div>
                <form method='post' action=''>
                    <div class="trip-plan-inner mCustomScrollbar">
                        @if($my_plans)
                        @foreach($my_plans AS $plan)
                            @if(isset($plan->myversion(Auth::user()->id)[0]))
                                @include('site.travelmates.partials._add_invitation_plan_block')
                            @endif
                        @endforeach
                        @endif
                    </div>

                    <div class="trip-modal-foot">
                        <a href="{{ route('trip.plan', 0) }}" class="btn  btn-clear without-trip-plan-btn">Create a new plan</a>

                        <button class="btn btn-transp btn-clear"
                                data-dismiss="modal">@lang('buttons.general.cancel')</button>
                        <button class="btn btn-light-primary btn-bordered" id="invite-travel-mate">Invite</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
