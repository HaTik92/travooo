<div class="{{(!$plan->join_requests && isset($plan->myversion(Auth::user()->id)[0]))?'':'plan-added'}}">
    @if($plan->join_requests && isset($plan->myversion(Auth::user()->id)[0]))
        <div class="py-2 px-4">This plan has already been published.</div>
    @endif
<div class="trip-plan-row pl-45 " title="{{$plan->title}}">
    <div style='width:6%'>
        <label class="container">
            <input type="radio" name='plan_id' value='{{$plan->id}}' required {{(!$plan->join_requests && isset($plan->myversion(Auth::user()->id)[0]))?'':'disabled'}}>
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="trip-plan-inside-block">
        <div class="trip-plan-map-img">
            <img src="{{get_plan_map($plan, 140, 150)}}"
                 alt="map-image" style='width:140px;height:150px;'>
        </div>
    </div>
    <div class="trip-plan-inside-block trip-plan-txt">
        <div class="trip-plan-txt-inner">
            <div class="trip-txt-ttl-layer">
                <h2 class="trip-ttl">{{$plan->title}}</h2>
                <p class="trip-date">{{weatherDate($plan->myversion(Auth::user()->id)[0]->start_date)}}
                    @lang('chat.to') {{weatherDate($plan->myversion(Auth::user()->id)[0]->end_date)}}</p>
                @if(Auth::user()->id!=$plan->users_id)
                <p class="trip-date" style='margin:0px'>@lang('profile.invited')
                    @lang('chat.by'): {{$plan->author->name}}</p>
                @endif
            </div>
            <div class="trip-txt-info">
                <div class="trip-info">
                    <div class="icon-wrap">
                        <i class="trav-clock-icon"></i>
                    </div>
                    <div class="trip-info-inner">
                        <p>
                            <b>{{calculate_duration($plan->id, 'all')}}</b>
                        </p>
                        <p>@lang('trip.duration')</p>

                    </div>
                </div>
                <div class="trip-info">
                    <div class="icon-wrap">
                        <i class="trav-distance-icon"></i>
                    </div>
                    <div class="trip-info-inner">
                        <p><b>{{calculate_distance($plan->id)}}</b></p>
                        <p>@lang('trip.distance')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="trip-plan-inside-block" style='width:25%'>
        <div class="dest-trip-plan">
            <h3 class="dest-ttl">@choice('trip.destination', 2)
                <span>{{$plan->trips_places()->count()}}</span></h3>
            <ul class="dest-image-list">
                @if($plan->trips_places()->count())
                @foreach($plan->trips_places AS $places)
                <li>
                    <img src="{{@check_place_photo2($places->place)}}"
                         alt="photo" style='width:52px;height:52px;'>
                </li>
                @endforeach
                @else
                <li style='font-size:14px;'>
                    @lang('trip.no_destinations_added_yet')
                </li>
                @endif

            </ul>
        </div>
    </div>
</div>
</div>