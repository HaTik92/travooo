<!-- request popup -->
<div class="modal fade white-style" data-backdrop="false" id="requestPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-780" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-top-bordered post-request-modal-block post-mobile-full">
                <div class="post-side-top">
                    <div class="post-top-txt horizontal">
                        <h3 class="side-ttl">Looking for Travel Mates to be a part of your plan?<span
                                class="count">{{@count($my_plans)}}</span></h3>
                    </div>
                </div>
                <form method='post' class="do-request-form" action='{{url('travelmates/dorequest')}}'>
                    <div class="trip-plan-inner mCustomScrollbar">
                        @if($my_plans)
                        @foreach($my_plans AS $plan)
                            @include('site.travelmates.partials._add_trip_plan_block')
                        @endforeach
                        @endif
                    </div>

                    <div class="trip-modal-foot">
                        <button class="btn  btn-clear without-trip-plan-btn" data-toggle="modal" data-target="#requestWithoutPlanPopup" >I don't have a trip plan</button>

                        <button class="btn btn-transp btn-clear"
                                data-dismiss="modal">@lang('buttons.general.cancel')</button>
                        <button class="btn btn-light-primary btn-bordered do-request-btn" type='submit'
                                name='submit'>Become a Travel Mate</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@php
    if(Auth::user()->interests)
        $user_interests = explode(',', Auth::user()->interests); 
    else
        $user_interests = [];
@endphp

<!-- request without a plan popup -->
<div class="modal fade white-style request-without-plan" data-backdrop="false" id="requestWithoutPlanPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <div class="top-title-layer">
                    <h3 class="title">
                        <span class="txt">List yourself as a Travel Mate and let others invite you to their Trip Plan!</span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <form method="post" action="{{url('travelmates/create-without-plan')}}" id="withoutPlanForm">
                    <div class="travlog-details-block d-flex">
                        <div class="left-side-detail-block">
                            <div class="detail-row">
                                <div class="title-block tmates-title-block">
                                    <div class="title">Title</div>
                                </div>
                                <div class="input-layer pr-0">
                                    <div class="input-block">
                                        <input type="text" class="detail-input" maxlength="50" name="title" id="title" required placeholder="Plan title..." >
                                    </div>
                                    <label for="title" class="error title-error-label" style="color:red"></label>
                                </div>
                            </div>
                            <div class="detail-row">
                                <div class="title-block tmates-title-block">
                                    <div class="title">
                                        Mutual Dates
                                    </div>
                                </div>
                                <div class="input-layer pr-0 ">
                                    <div class="col-6 pull-left pr-0">
                                        <div class="form-group">
                                            <div class="input-group input-group-time">
                                                <input type="text" name='mutual_start_date' class="form-control date-input" id="datepicker_mates_start"  readonly="readonly" placeholder="Trip start date"  required >
                                            </div>
                                             <label id="datepicker_mates_start-error" class="error" for="datepicker_mates_start" style="color:red"></label>
                                        </div>
                                    </div>
                                    <div class="col-6 pull-left pr-0 pl-3">
                                        <div class="form-group">
                                            <div class="input-group input-group-time">
                                                <input type="text" name='mutual_end_date' id='datepicker_mates_end' class="form-control date-input" readonly="readonly" placeholder="Trip finish date" required>
                                            </div>
                                            <label for="datepicker_mates_end" class="error end-date-error-label" style="color:red"></label>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="detail-row">
                                <div class="title-block tmates-title-block">
                                    <div class="title">Your Destinations</div>
                                </div>
                                <div class="input-layer pr-0">
                                    <div class="input-block">
                                        <input type="text" class="detail-input destination-autocomplete" id="destination_autocomplete" placeholder="Search for any country or city..." >
                                    </div>
                                    <input type="hidden" name="destinations_list" id="destinations_list" required>
                                    <label for="destinations_list" class="error destination-error-label" style="color:red"></label>
                                    <ul class="search-selected-block destination-sel-block">
                                    </ul>
                                </div>
                            </div>

                            <div class="detail-row">
                                <div class="title-block tmates-title-block">
                                    <div class="title">What's your current location?</div>
                                </div>
                                <div class="input-layer pr-0">
                                    <div class="input-block">
                                        <input type="text" id="current_location" class="detail-input "  placeholder="City Name..." value="">
                                    </div>
                                    <div class="search-filtered-block search-origin-country"></div>
                                    <input type="hidden" id="mates_current_location" name="current_location" required>
                                    <label for="mates_current_location" class="error c-location-error-label" style="color:red"></label>
                                    <ul class="search-selected-block location-sel-block">
                                    </ul>
                                </div>
                            </div>
                            <div class="detail-row">
                                <div class="title-block tmates-title-block">
                                    <div class="title">Your interests</div>
                                </div>
                                <div class="input-layer pr-0">
                                    <div class="input-block">
                                        <input class="detail-input" id="interests" maxlength="15" type="text" placeholder="Search for an interest Or type and press enter...">
                                    </div>
                                    <input type="hidden" name="interests" id="interests_list">
                                    <label for="interests" class="error interest-error-block" style="color:red"></label>
                                    <ul class="search-selected-block interest-sel-block">
                                        @foreach($user_interests as $user_interest)
                                            <li>
                                                <span class="search-selitem-title">{{$user_interest}}</span>
                                                <span class="close-search-item close-interest-filter" data-interest-value="{{$user_interest}}"><i class="trav-close-icon" dir="auto"></i></span>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="right-side-detail-block">
                            <div class="r-detail-row">
                                <div class="post-title-label">
                                    <div class="label-txt">Title:</div>
                                    <div class="selected-location-block mt-1 mates-plan-title">
                                    </div>
                                </div>
                            </div>
                            <div class="r-detail-row">
                                <div class="post-title-label">
                                    <div class="label-txt">Mutual Dates:</div>
                                    <div class="selected-mutual-date-block mt-1">
                                        <span class="selected-mutual-start-date"></span>
                                        <span class="selected-mutual-end-date"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="r-detail-row">
                                <div class="post-title-label">
                                    <div class="label-txt">Destinations:</div>
                                    <div class="selected-destination-block mt-1">
                                        <div class="selected-locetion-block"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="r-detail-row">
                                <div class="post-title-label">
                                    <div class="label-txt">Current location:</div>
                                    <div class="selected-location-block mt-1">
                                        <ul class="selected-location-list selected-list-item">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="r-detail-row">
                                <div class="post-title-label">
                                    <div class="label-txt">Interests:</div>
                                    <div class="selected-interest-block mt-1">
                                        <ul class="selected-interest-list selected-list-item">
                                            @foreach($user_interests as $user_interest)
                                               <li class="interest-{{$user_interest}}">{{$user_interest}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="post-foot-btn without-plan-foot-btn">
                        <button class="btn btn-transp btn-clear" data-dismiss="modal">@lang('buttons.general.cancel')</button>
                        <button class="btn btn-light-primary btn-bordered" type="submit">Become a Travel Mate</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function generateTag(value, id, type = '') {
      
    }
    var existing_interests = [];
    var destinations = [];
    var interests_list = [];
    @if(Auth::user()->interests)
        interests_list = existing_interests = <?= json_encode(explode(',', Auth::user()->interests));?>;
    @endif
     
    $(document).ready(function () {
    
    //Insert title
    $(document).on("keyup", "#title", function(){
        $('.mates-plan-title').html($(this).val())
    });
    
    //Select interests
    $(document).on('keydown', '#interests', function (e) {
        if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey) {
            e.preventDefault();
        }
    });
    
    $("#interests").bind("paste", function(e){
        var pastedData = e.originalEvent.clipboardData.getData('text');
        var inputVal = pastedData.replace(/,/g, '', pastedData);
        var pasteStr = inputVal.split(' ');
 
        $.each( pasteStr, function( key, value ) {
        if ($.inArray(value, interests_list) == -1) {
            if(interests_list.length < 10){
                   interests_list.push(value)
                    $('#interests_list').val(interests_list)
                    $('.interests-error-label').hide()
                    $('.selected-interest-list').append('<li class="interest-'+value +'">'+ value +'</li>')
                    $('.interest-sel-block').append(getSelectedHtml(value, 'close-interest-filter', 'interest-value', value));
                    $('.interest-error-block').html('')
            }else{
                $('.interest-error-block').html('The maximum tags count is 10')
            }
            
            }
              
        });
        var self = $(this);
          setTimeout(function(e) {
              self.val('');
          }, 100);
    } );

    $(document).on('keyup', '#interests', function (e) {
        if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey) {
            var input_value = $(this).val().trim();
            
            if (input_value != '') {
                if($.inArray(input_value, interests_list) == -1){
                    if(interests_list.length < 10){
                     interests_list.push(input_value)
                     $('#interests_list').val(interests_list)
                     $('.interests-error-label').hide()
                     $('.selected-interest-list').append('<li class="interest-'+input_value +'">'+ input_value +'</li>')
                     $('.interest-sel-block').append(getSelectedHtml(input_value, 'close-interest-filter', 'interest-value', input_value));
                    $('.interest-error-block').html('')
                    }else{
                        $('.interest-error-block').html('The maximum tags count is 10')
                    }
                }
                $(this).val('')
                $('ul.travel-mates-autocomplate').hide()
            }
        }
    });
    
     $('#interests').autocomplete({
        source: existing_interests,
        classes: {"ui-autocomplete": "travel-mates-autocomplate",},
        select: function( event, ui ) {
            event.preventDefault();
            $('#interests').val('');
             if($.inArray(ui.item.value, interests_list) == -1){
                if(interests_list.length < 10){
                 interests_list.push(ui.item.value)
                  $('#interests_list').val(interests_list)
                  $('.interests-error-label').hide()
                  $('.selected-interest-list').append('<li class="interest-'+ ui.item.value +'">'+ ui.item.value +'</li>')
                  $('.interest-sel-block').append(getSelectedHtml(ui.item.value, 'close-interest-filter', 'interest-value', ui.item.value));
                  $('.interest-error-block').html('')
                }else{
                    $('.interest-error-block').html('The maximum tags count is 10')
                }
             }
        },
    });
    
    //Select Destination
     $('#destination_autocomplete').autocomplete({
        source: function (request, response) {
            jQuery.get("{{route('travelmate.filters.ajax')}}", {
                q: request.term
            }, function (data) {
                 var items = [];

                data.forEach(function(s_item, index) {
                    items.push({
                        value: s_item.text, image: s_item.image, query: s_item.query, id: s_item.id, selected_id: s_item.selected_id, country_id: s_item.country_id, country_name: s_item.country_name
                    });
                });
                response(items);
            });
        },
        classes: {"ui-autocomplete": "travel-mates-autocomplate"},
         focus: function( event, ui ) {
            return false;
          },
        select: function( event, ui ) {
            $('#destination_autocomplete').val('');
            if($.inArray(ui.item.selected_id, destinations) == -1){
                destinations.push(ui.item.selected_id)
                $('#destinations_list').val(destinations)
                $('.destination-error-label').hide();
                addSelectedDestinationRow(ui.item)  
                $('.destination-sel-block').append(getSelectedHtml(ui.item.value, 'close-destination-filter', 'destination-id', ui.item.selected_id));
            }
           return false; 
        },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append(getMarkupHtml(item))
        .appendTo( ul );
    };
    
    
    //Select current location
     $('#current_location').autocomplete({
        source: function (request, response) {
            jQuery.get("{{url('travelmates/ajaxGetSearchCity')}}", {
                q: request.term
            }, function (data) {
                 var items = [];

                data.forEach(function(s_item, index) {
                    items.push({
                        value: s_item.text, image: s_item.image, query: s_item.query, id: s_item.id, country_name:s_item.country_name
                    });
                });
                response(items);
            });
        },
        classes: {"ui-autocomplete": "travel-mates-autocomplate"},
         focus: function( event, ui ) {
            return false;
          },
        select: function( event, ui ) {
            $('#current_location').val('');
            $('#mates_current_location').val(ui.item.id);
            $('.c-location-error-label').hide();
            $('.selected-location-list').html('<li class="location-'+ ui.item.id +'">'+ ui.item.value +'</li>')
            $('.location-sel-block').html(getSelectedHtml(ui.item.value, 'close-location-filter', 'location-id', ui.item.id))
           return false; 
        },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append(getMarkupHtml(item))
        .appendTo( ul );
    };

 
     //plan mutual date   
    var dateFormat = "d MM yy",
            from = $("#datepicker_mates_start").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        dateFormat: "d MM yy",
        setDate: new Date(),
        numberOfMonths: 1,
        beforeShowDay: DisableDates
    })
            .on("change", function () {
                if (this.value != '') {
                    $('.selected-mutual-start-date').html(this.value)
                }
                to.datepicker("option", "minDate", getDate(this));
            }),
            to = $("#datepicker_mates_end").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        dateFormat: "d MM yy",
        numberOfMonths: 1,
        beforeShowDay: DisableDates
    })
            .on("change", function () {
                if(fromDateCheck($("#datepicker_mates_start").val(), this.value)){
                    if (this.value != '') {
                        $('.selected-mutual-end-date').html('- ' + this.value)
                        $('.end-date-error-label').hide()
                    }
                }else{
                   $("#datepicker_mates_end").val('') 
                   $('.end-date-error-label').html('Please select valid date')
                }
                from.datepicker("option", "maxDate", getDate(this));
            });

    
    function DisableDates(date){
         var string = jQuery.datepicker.formatDate('yy-m-d', date);
         return dateCheck(string)

    }

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }
    
    //Validation without plan form
            $('#withoutPlanForm').validate({
                ignore:[],
                rules: {
                    title: {
                    required: true
                    },
                    mutual_end_date: {
                    required: true
                    },
                    current_location: {
                    required: true
                    },
                    destinations_list: {
                    required: true
                    },
                },
                messages: {
                    title: 'Title field is required.',   
                    destinations_list: 'Destinations field is required.',   
                    current_location: 'Current location field is required.',   
                }
        });
        
    })
    
    //Remove current location
    $(document).on('click', '.close-location-filter', function(){
        var location_id = $(this).attr('data-location-id');
         $('#mates_current_location').val('')
          
        $(this).closest('li').remove()
        $('.selected-location-list').find('.location-'+location_id).remove();

    });
    
    //Remove interest
    $(document).on('click', '.close-interest-filter', function(){
        var interst = $(this).attr('data-interest-value');
        interests_list = $.grep(interests_list, function(value) {
            return value != interst;
          });
          if(interests_list.length < 10){
               $('.interest-error-block').html('')
          }
        $('#interests_list').val(interests_list)
        $(this).closest('li').remove()
        $('.selected-interest-list').find('.interest-'+interst).remove();

    });
    
    //Remove destination
    $(document).on('click', '.close-destination-filter', function(){
        var destination_id = $(this).attr('data-destination-id');
        destinations = $.grep(destinations, function(value) {
            return value != destination_id;
          });
        $('#destinations_list').val(destinations)
        $(this).closest('li').remove()
        removeSelectedDestinationRow(destination_id)

    });
 
    
    
    function addSelectedDestinationRow(item){
        if(item.country_id){
            if($('.selected-locetion-block').find('.country-'+item.country_id).length>0){
                $('.selected-locetion-block').find('.country-'+item.country_id).append('<div class="city-item" id="'+ item.selected_id +'">'+
                                                                                            '<span class="loc-city-name selected">'+ item.value +'</span><span class="loc-type">City</span>'+
                                                                                        '</div>')
            }else{
                $('.selected-locetion-block').append('</div>'+
                            '<div class="selected-locetion-item country-'+ item.country_id +'">'+
                                '<div class="country-item" id="'+ item.country_id +'-country">'+
                                    '<span class="loc-country-name">'+ item.country_name +'</span><span class="loc-type">Country</span>'+
                                '</div>'+
                                '<div class="city-item" id="'+ item.selected_id +'">'+
                                    '<span class="loc-city-name selected">'+ item.value +'</span><span class="loc-type">City</span>'+
                                '</div>'+
                            '</div>')
            }
        }else{
            if($('.selected-locetion-block').find('.country-'+item.id).length>0){
                $('.selected-locetion-block').find('.country-'+item.id).find('.loc-country-name').addClass('selected')
            }else{
                $('.selected-locetion-block').append('<div class="selected-locetion-item country-'+ item.id +'">'+
                                   '<div class="country-item" id="'+ item.selected_id +'">'+
                                       '<span class="loc-country-name selected">'+ item.value +'</span><span class="loc-type">Country</span>'+
                                   '</div>'+
                               '</div>')
           }
        }
    }
    
    function removeSelectedDestinationRow(destination_id){
        if(destination_id){
            var des_info = destination_id.split("-");
            if(des_info[1] == 'country'){
                 if($('#'+destination_id).closest('.selected-locetion-item').find('.city-item').length == 0)
                      $('#'+destination_id).closest('.selected-locetion-item').remove();
                  else
                     $('#'+destination_id).find('.loc-country-name').removeClass('selected');
            }else{
                 if($('#'+destination_id).closest('.selected-locetion-item').find('.city-item').length == 1 && !$('#'+destination_id).closest('.selected-locetion-item').find('.loc-country-name').hasClass('selected'))
                        $('#'+destination_id).closest('.selected-locetion-item').remove();
                     else
                        $('.selected-locetion-item').find('#'+destination_id).remove();
            }
        }
    }

   
   function dateCheck(check) {
        
   @foreach(get_without_plan_date_list() as $date)
   var fDate,lDate,cDate;
    fDate = Date.parse('{{$date["start_date"]}}');
    lDate = Date.parse('{{$date["end_date"]}}');
    cDate = Date.parse(check);
     if((cDate <= lDate && cDate >= fDate)) {
       return [false, "dateNA", ''];
    }
    @endforeach
    return [true, "dateValid", ''];
}

function fromDateCheck(start_date, check_date) {
    
   @foreach(get_without_plan_date_list() as $date)
   var fDate,lDate,cDate;
    fDate = Date.parse('{{$date["start_date"]}}');
    lDate = Date.parse('{{$date["end_date"]}}');
    cDate = Date.parse(check_date);
    sDate = Date.parse(start_date);
     if((sDate <= fDate && cDate >= lDate)) {
       return false;
    }
    @endforeach
    return true;
}
</script>