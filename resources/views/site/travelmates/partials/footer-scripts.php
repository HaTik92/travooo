        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <link href="{{url_with_locale('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
        <script src="{{url_with_locale('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>

        <script src="{{url_with_locale('assets2/js/script.js')}}"></script>
        <script>
        $(document).ready(function () {
            $(".datepicker_start").datepicker({
                minDate: 0,
                dateFormat: "d MM yy",
                altField: "#actual_trip_start_date",
                altFormat: "yy-mm-dd",
                numberOfMonths: 1
            });
            $(".datepicker_end").datepicker({
                minDate: 0,
                dateFormat: "d MM yy",
                altField: "#actual_trip_end_date",
                altFormat: "yy-mm-dd",
                numberOfMonths: 1
            });


            $('#country_select').select2({
                ajax: {
                    url: '{{url_with_locale("api/countries/select2")}}',
                    dataType: 'json'
                            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });
            $('#country_select2').select2({
                ajax: {
                    url: '{{url_with_locale("api/countries/select2")}}',
                    dataType: 'json'
                            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });
        });
        </script>