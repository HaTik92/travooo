@foreach($join_without_plans_requests as $plan)
<div class="notification-block w-plan-block">
    <div class="travel-mates-trip-wrapper">
        <div class="w-plan-top-info">
            <div class="post-top-info-wrap" style="width: 100%;">
                <h3 class="trev-plan-title">{{$plan->without_plan->title}}</h3>
                <div class="post-top-avatar-wrap">
                    <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{check_profile_picture(@$plan->without_plan->author->profile_picture)}}" class="avatar" alt="">

                    <div class="post-top-info-txt">
                        <div class="post-top-name">
                            <a class="post-name-link" href="{{route('profile.show', ['id' => $plan->author->id])}}">{{$plan->author->name}}</a>
                            {!! get_exp_icon($plan->author) !!}
                        </div>
                        <div class="post-info">
                            @if($plan->without_plan->author_location)
                            Lives in {{$plan->without_plan->author_location->transsingle->title}}, {{$plan->without_plan->author_location->country->transsingle->title}}
                            @endif
                            @if(isset($plan->without_plan->author_location->country->iso_code))
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($plan->without_plan->author_location->country->iso_code)}}.png" alt="flag" style="width:18px;height:14px;margin-left: 2px;">
                            @endif
                        </div>
                    </div>
                    <div class="countries request-countries wp-countries" style="width: 480px;position: relative;">
                        <div @if(count($plan->without_plan->countries()) > 1)class="wpRequestCountries"@else class="country" @endif>
                              @foreach($plan->without_plan->countries() as $country)
                              @if ($loop->iteration > 1)
                              <img src="{{asset('assets2/image/arrow.png')}}" class="arrow">
                            @endif
                            <div style="display: inline-flex;height: 42px;justify-content: center;align-items: center;">
                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/small/{{strtolower($country['country_info']->iso_code)}}.png" class="flag" alt="flag"><span>{{$country['name']}}</span>
                            </div>
                            @endforeach
                        </div>

                    </div>
                </div>
                <div class="post-content-wrap">
                    <div class="info wplan-info">
                        <div class="wplan-params">
                            <ul>
                                <li>
                                    <div class="icon-wrap">
                                        <i class="trav-point-flag-icon"></i>
                                    </div>
                                    <span>{{date('D j M', strtotime($plan->without_plan->start_date))}}</span>
                                </li>
                                <li><span class="med-border"></span></li>
                                <li>
                                    <div class="icon-wrap">
                                        <i class="trav-clock-icon"></i>
                                    </div>
                                    <span>{{dateDiffDays($plan->without_plan->start_date, $plan->without_plan->end_date)}}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="wp-invitation-block">
                            @if(@$plan->without_plan->plan_invitations()->where('status', 0)->count() > 0)
                            @php $invited_user = @$plan->without_plan->plan_invitations()->where('status', 0)->orderBy('id', 'desc')->first(); @endphp
                            <div class="post-top-info-wrap">
                                <div class="post-top-avatar-wrap">
                                    <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{check_profile_picture(@$invited_user->author->profile_picture)}}" alt="">
                                </div>
                                <div class="post-top-info-txt">
                                    <div class="post-top-name">
                                        <a class="post-name-link" href="{{route('profile.show', ['id' => @$invited_user->author->id])}}">{{@$invited_user->author->name}}</a> {!! get_exp_icon(@$invited_user->author) !!}
                                        @if(@$plan->without_plan->plan_invitations()->where('status', 0)->count() > 1)
                                        <a href="javascript:;"  data-toggle="modal"  data-target="#myInvitedGoingPopup{{@$plan->without_plan->id}}" class="more-going-link"> and +<span class="going-cnt">{{@$plan->without_plan->plan_invitations()->where('status', 0)->count() -1}}</span> other</a>
                                        @endif
                                        <span class="join-desc">invited you to their plan</span>
                                    </div>
                                    <div class="post-info" dir="auto">{{@$invited_user->author->display_name}}</div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="post-top-info-participants">
                <div class="mates-label-block">
                    <div class="btn-wrap request-btn-wrap">
                        <button type="button" class="btn btn-light-bg-grey btn-bordered mt-15 mb-3"
                                 data-toggle="modal"  data-target="#myInvitedGoingPopup{{@$plan->without_plan->id}}">More
                        </button>
                        <button type="button" class="btn btn-light-red-bordered unpublish-w-plan-btn"
                                data-requestid="{{@$plan->id}}">Unpublish
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('site/travelmates/partials/my-invited-going-popup', ['invitetion'=>@$plan->without_plan])
@endforeach