<!-- travel mates search popup -->
<div class="modal fade white-style" data-backdrop="false" id="searchTravelMates" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-840" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-setting-block search-travel-mates">
                <form method="post" action="{{url_with_locale('travelmates-search')}}">
                    <div class="post-side-top">
                        <h3 class="side-ttl">@lang('travelmate.find_a_travel_mate')</h3>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="post-content-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="post-form-wrapper">
                                    <div class="row">
                                        <label class="col-sm-4 col-form-label">@lang('time.mutual_dates')</label>
                                        <div class="col-sm-3">
                                            <div class="flex-custom">
                                                <input type="text" name='trip_start_date' id='trip_start_date'
                                                       class="flex-input datepicker_start"
                                                       placeholder="@lang('time.trip_start_date')">
                                                <input type="hidden" name='actual_trip_start_date'
                                                       id='actual_trip_start_date'>
                                            </div>
                                        </div>
                                        <label class="col-sm-1 col-form-label">@lang('other.until')</label>
                                        <div class="col-sm-3">
                                            <div class="flex-custom">
                                                <input type="text" name='trip_end_date' id='trip_end_date'
                                                       class="flex-input datepicker_end"
                                                       placeholder="@lang('trip.trip_end_date')">
                                                <input type="hidden" name='actual_trip_end_date'
                                                       id='actual_trip_end_date'>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-4 col-form-label">@lang('trip.destination')</label>
                                        <div class="col-sm-7">
                                            <div class="flex-custom">
                                                <select class="flex-input" name="destinations[]" id="country_select"
                                                        style="width: 100%; height:40px !important;"
                                                        multiple="multiple">
                                                    <option value="0">@lang('other.please_select')</option>
                                                    @foreach($cities AS $city)
                                                        <option value="{{$city->id}}">{{@$city->transsingle->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-4 col-form-label">@lang('profile.country_of_origin')</label>
                                        <div class="col-sm-7">
                                            <div class="flex-custom">
                                                <select class="flex-input" name="origins[]" id="country_select2"
                                                        style="width: 100%; height:40px !important;"
                                                        multiple="multiple">
                                                    <option value="0">@lang('other.please_select')</option>
                                                    @foreach($countries AS $country)
                                                        <option value="{{@$country->transsingle->title}}">{{@$country->transsingle->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-4 col-form-label">@lang('profile.gender')</label>
                                        <div class="col-sm-7">
                                            <div class="flex-custom">
                                                <select name="gender" id="gender" class="custom-select">
                                                    <option value="">@lang('other.please_select')</option>
                                                    <option value="0">@lang('other.no_preference')</option>
                                                    <option value="1">@lang('profile.male')</option>
                                                    <option value="2">@lang('profile.female')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-4 col-form-label">@lang('profile.age')</label>
                                        <div class="col-sm-7">
                                            <div class="flex-custom">
                                                <select name="age_range" id="age_range" class="custom-select">
                                                    <option value="">@lang('other.please_select')</option>
                                                    <option value="less20">< 20</option>
                                                    <option value="20">20 - 25</option>
                                                    <option value="25">25 - 30</option>
                                                    <option value="30">30 - 35</option>
                                                    <option value="35">35 - 40</option>
                                                    <option value="40">40 - 50</option>
                                                    <option value="50">50 - 60</option>
                                                    <option value="greater60">> 60</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-btn">
                        <button class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                        <button type="submit" name="submit"
                                class="btn btn-light-primary btn-bordered">@lang('buttons.general.search')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
