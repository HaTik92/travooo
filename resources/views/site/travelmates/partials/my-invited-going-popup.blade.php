<!-- going popup -->
<div class="modal fade white-style" data-backdrop="false" id="myInvitedGoingPopup{{$invitetion->id}}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-700" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-going-block post-mobile-full">
                <div class="post-top-layer">
                    <div class="post-top-info">
                        <h3 class="info-title">{{@$invitetion->title}}</h3>
                    </div>
                </div>
                <div class="post-sub-info-layer">
                    <div class="post-sub-layer">
                         People want to go with You.
                    </div>
                        <div class="post-sub-buttons">
                            @if(@$invitetion->plan_requests->status == -1)
                            <div class="pr-3">Closed</div>
                            @else
                            <button type="button"
                                class="btn btn-small btn-light-red-bordered sub-btn mr-2 close-requests-btn"
                                data-requestid="{{@$invitetion->plan_requests->id}}">
                                Close Request
                            </button>
                            @endif
                            <button type="button"
                                class="btn btn-small btn-danger btn-custom sub-btn unpublish-btn"
                                data-requestid="{{@$invitetion->plan_requests->id}}" unpublish-w-plan-btn>
                                Unpublish
                            </button>
                        </div>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar">
                    <?php
                     $array_statuses = array(0, 1);
                     $check_invitation = @$invitetion->plan_invitations()->where('status', 1)->first();
                    ?>
                    @foreach(@$invitetion->plan_invitations()->whereIn('status', $array_statuses)->get() AS $going)
                        <div class="people-row {{(is_object($check_invitation) && $check_invitation->authors_id != $going->author->id)?'bg-gray':''}}" id="invitationMate{{$going->author->id}}">
                            <div class="main-info-layer">
                                <div class="img-wrap">
                                    <img class="ava" src="{{check_profile_picture($going->author->profile_picture)}}"
                                         style='width:50px;height:50px'>
                                </div>
                                <div class="txt-block">
                                    <div class="name"><a href="{{url('profile/'.$going->author->id)}}" class="tm-user-link" target="_blank">{{$going->author->name}} {!! get_exp_icon($going->author) !!}</a>
                                        <span class="mates-name-{{$going->author->id}}">
                                            @if(!is_object($check_invitation) && $going->status==0)
                                                <span style="color:red;font-size:80%">(pending approval)</span>
                                            @elseif($going->status==1)
                                                <span style="color:green;font-size:80%">(going)</span>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="info-line">
                                        @if(get_users_age($going->author->id) !='')
                                            <div class="info-part">{{get_users_age($going->author->id).' years'}}</div>
                                        @endif
                                        <div class="info-part">{{@$going->author->country_of_nationality->transsingle->title}}</div>
                                        <div class="info-part">{{process_gender($going->author->gender)}}</div>
                                    </div>
                                </div>
                                <div class="going-action-block going-{{$going->author->id}}">
                                    @if($check_invitation)
                                        @if($check_invitation->author->id == $going->author->id)
                                            <a type="button" href="{{route('trip.plan', $going->plans_id)}}" target="_blank"
                                                    class="btn btn-primary request-action-btn view-invited-plan-link">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                        @endif
                                    @else
                                        <a type="button" href="{{route('trip.plan', $going->plans_id)}}" target="_blank"
                                                class="btn btn-primary request-action-btn view-invited-plan-link">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                        <button type="button"
                                                class="btn btn-light-green-bordered request-action-btn accept-invitation-button"
                                                data-invitationId="{{$going->id}}" data-mateid="{{$going->author->id}}" data-requestid="{{@$invitetion->plan_requests->id}}">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </button>
                                        <button type="button"
                                                class="btn btn-light-red-bordered request-action-btn reject-invitation-button"
                                                data-invitationId="{{$going->id}}" data-mateid="{{$going->author->id}}">
                                           <i class="fa fa-times" aria-hidden="true"></i>
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
