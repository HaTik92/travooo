<div class="travel-mates-trip-plan-block">

    <form method='post' action='{{url('travelmates/join')}}'>
        <div class="travel-mates-trip-plan-inner">
            <div class="post-top-info-layer">
                @if ($request->author->id !== auth()->id())
                <div class="post-top-info-action">
                    <div class="dropdown">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="TravelMateRequest">
                            @include('site.home.partials._info-actions', ['post'=>$request])
                        </div>
                    </div>
                </div>
                @endif
                <div class="post-top-info-wrap">
                    <div class="post-top-avatar-wrap">
                        <img src="{{check_profile_picture($request->author->profile_picture)}}"
                             style='width:45px;height:45px;'>
                    </div>
                    <div class="post-top-info-txt">
                        <div class="post-top-name">
                            <a class="post-name-link"
                               href="{{url('profile/'.$request->author->id)}}">{{$request->author->name}}</a>
                            {!! get_exp_icon($request->author) !!}
                        </div>
                        <div class="post-info">{{$request->author->display_name}}</div>
                    </div>
                </div>
            </div>
            @if(is_object($request->plan))
                <div class="tm-tp-info-block">
                    <ul class="tm-tp-info-list">
                        <li class="country">
                            <a href='#' data-toggle="modal"
                               data-target="#placeOneDayPopup">{{$request->plan->title}}</a>
                        </li>
                        <li>
                            <div class="info-left">
                                <div class="icon-wrap">
                                    <i class="trav-map-marker-icon"></i>
                                </div>
                                <span>@choice('trip.destination_dd', 2)</span>
                            </div>
                            <div class="info-right">

                                <span>{{count($request->plan->activeversion->cities)}}</span>
                            </div>
                        </li>
                        <li>
                            <div class="info-left">
                                <div class="icon-wrap">
                                    <i class="trav-point-flag-icon"></i>
                                </div>
                                <span>@lang('trip.starting_dd')</span>
                            </div>
                            <div class="info-right">
                                <span class="date">{{$request->plan->activeversion->start_date}}</span>
                            </div>
                        </li>
                        <li>
                            <div class="info-left">
                                <div class="icon-wrap">
                                    <i class="trav-budget-icon"></i>
                                </div>
                                <span>@lang('trip.budget_dd')</span>
                            </div>
                            <div class="info-right">
                                <span>@if($request->plan->budget){{$request->plan->budget}}@else n/a @endif</span>
                            </div>
                        </li>
                        <li>
                            <div class="info-left">
                                <div class="icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <span>@lang('trip.duration_dd')</span>
                            </div>
                            <div class="info-right">
                                <span>{{dateDiffDays($request->plan->activeversion->start_date, $request->plan->activeversion->end_date)}}</span>
                            </div>
                        </li>
                    </ul>
                </div>
            @endif
            @if(count($request->going()->where('status', 1)->get())>0)
                <div class="post-map-info-caption map-blue" data-toggle="modal"
                     data-target="#goingPopup{{$request->id}}" style='cursor:pointer;'>
                    <ul class="foot-avatar-list">
                        @foreach($request->going()->where('status', 1)->get() AS $gogo)
                            <li><img class="small-ava" src="{{$gogo->user->profile_picture}}" alt="ava"
                                     style="width:20px;height:20px;"></li>
                        @endforeach
                    </ul>
                    <div class="map-label-txt">
                        @lang('travelmate.count_more_going', [
                            'count' => '+' . count($request->going()->where('status', 1)->get())
                        ])
                    </div>
                </div>
            @else
                <div class="post-map-info-caption map-blue">
                    <ul class="foot-avatar-list">
                    </ul>
                    <div class="map-label-txt">
                        @lang('trip.count_are_going', ['count' => 0])
                    </div>
                </div>
            @endif

            <div class="btn-wrap">
                <input type='hidden' name='request_id' value='{{$request->id}}'/>
                @if(count($request->going()->where('users_id', Illuminate\Support\Facades\Auth::guard('user')->user()->id)->get()) <= 0)
                    <button type="submit" name="submit"
                            class="btn btn-light-bg-grey btn-bordered">@lang('travelmate.ask_to_join')</button>
                @elseif($request->going()->where('users_id', Illuminate\Support\Facades\Auth::guard('user')->user()->id)->get()->first()->status==0)
                    <button type="button" name="submit"
                            class="btn btn-light-bg-grey btn-bordered">@lang('travelmate.your_join_request_is_pending')</button>
                @elseif($request->going()->where('users_id', Illuminate\Support\Facades\Auth::guard('user')->user()->id)->get()->first()->status==1)
                    <button type="button" name="submit"
                            class="btn btn-light-bg-grey btn-light-primary">@lang('travelmate.joined')</button>
                @elseif($request->going()->where('users_id', Illuminate\Support\Facades\Auth::guard('user')->user()->id)->get()->first()->status==-1)
                    <button type="button" name="submit"
                            class="btn btn-light-bg-grey btn-light-red-bordered">@lang('travelmate.your_join_request_is_declined')</button>
                @endif
            </div>
        </div>
    </form>
</div>
