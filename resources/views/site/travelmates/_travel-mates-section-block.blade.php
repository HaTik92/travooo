<div class="mates-tab active" id="plan_tab">
    @if(count($plan) > 0)
    <div class="travmates-content">
        @foreach($plan as $item)
        @include('site.travelmates._travel-mates-block', ['plan'=>$item])
        @endforeach
    </div>
    <div class="mates_pagination" id="mates_pagination">
        <input type="hidden" id="plan_pagenum" value="1">
        <div id="mates_loader" class="mates_width_plan_loader mates-animation-content post-side-inner">
            <div class="travel-mates-trip-plan-wrapper">
                <div class="post-top-info-wrap mates-top-info">
                    <div class="post-top-avatar-wrap mate-avatar-animation animation"></div>
                    <div class="top-info-animation animation"></div>
                    <div class="top-info-buttion-animation animation"></div>
                </div>
                <div class="post-content-wrap">
                    <div class="mate-map-animation animation"></div>
                    <div class="info">
                        <div class="params">
                            <ul class="tm-tp-info-list">
                                <li><div class="mates-info-animation w-135 animation"></div></li>
                                <li><div class="mates-info-animation w-110 animation"></div></li>
                                <li><div class="mates-info-animation w-105 animation"></div></li>
                                <li><div class="mates-info-animation w-125 animation"></div></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="pt-3 pl-1"><p>No items yet..</p></div>
    @endif
</div>

<div class="mates-tab d-none" id="without_plan_tab">
    @if(count($without_plan) > 0)
    <div class="travmates-content">
        @foreach($without_plan as $item)
        @include('site.travelmates._travel-mates-without-plan-block', ['plan'=>$item])
        @endforeach
    </div>
    <div class="w_mates_pagination" id="w_mates_pagination">
        <input type="hidden" id="w_plan_pagenum" value="1">
        <div id="w_mates_loader" class="mates_loader mates-animation-content post-side-inner">
            <div class="travel-mates-trip-plan-wrapper">
                <div class="post-top-info-wrap mates-top-info">
                    <div class="post-top-avatar-wrap mate-avatar-animation animation"></div>
                    <div class="top-info-animation animation"></div>
                    <div class="top-info-buttion-animation animation"></div>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="pt-3 pl-1"><p>No items yet..</p></div>
    @endif
</div>
