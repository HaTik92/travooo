@php
$title = 'Travooo - travel mates';
@endphp

@extends('site.travelmates.template.travelmate')

@section('content')
<div class="post-block post-flight-style post-block-notifications">
    <div class="post-side-top">
        <div class="post-side-top-inner">
            <a href="{{route('travelmate.newest')}}" class="btn btn-light-bg-grey btn-bordered btn-back">
                <i class="trav-angle-left"></i>
            </a>
            <h3 class="side-ttl">@choice('dashboard.notification', 2)</h3>
        </div>
        <div class="post-top-tabs">
            <div class="detail-list">
                <div class="detail-block @if($active_tab=='requests')  current @endif">
                    <a href="{{url('travelmates-notifications-requests')}}"><b>Manage Publications</b></a>
                </div>
                <div class="detail-block @if($active_tab=='invitations')  current @endif">
                    <a href="{{url('travelmates-notifications-invitations')}}"><b>My Requests & Invitations</b></a>
                </div>
            </div>
        </div>
    </div>
    <div class="manage-side-top">
        <ul class="manage-tabs-nav">
            <li class="active" manage-type="1" data-type="without_plan">
                <h3 class="side-ttl">With A Plan</h3>
            </li>
            <li class="" manage-type="2" data-type="with_plan">
                <h3 class="side-ttl">Without A Plan</h3>
            </li>
        </ul>
    </div>
    <div class="post-side-inner">
        <div class="manage-tab-block" id="with_plan">
            <div class="travel-mates-notification-wrapper">
                <div class="n-with-plan-requests-block">
                    @include('site/travelmates/partials/_notifications-with-plan-request')
                </div>
                <div class="request_pagination request_with_plan_pagination">
                    <input type="hidden" id="request_with_plan_pagenum" value="1">
                    <div id="request_with_plan_pagination" class="mates-animation-content post-side-inner">
                        <div class="notification-block">
                            <div class="n-img-wrap animation">
                            </div>
                            <ul class="n-tp-info-list" >
                                <li><div class="mates-info-animation w-135 animation"></div></li>
                                <li><div class="mates-info-animation w-310 animation" ></div></li>
                                <li><div class="mates-info-animation w-265 animation"></div></li>
                                <li>
                                    <div class="mates-info-animation w-285 animation"></div>
                                    <div class="n-info-animation animation"></div>
                                </li>
                            </ul>
                            <div class="btn-wrap request-btn-wrap pull-right">
                                <div class="nb-info-animation animation"></div>
                                <div class="nb-s-info-animation  animation"></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="manage-tab-block d-none" id="without_plan">
            <div class="travel-mates-notification-wrapper">
                <div class="n-without-plan-requests-block">
                    @include('site/travelmates/partials/_notifications-without-plan-request')
                </div>
                <div class="request_without_plan_pagination">
                    <input type="hidden" id="request_without_plan_pagenum" value="1">
                    <div id="request_without_plan_pagination" class="mates-animation-content post-side-inner">
                        <div class="travel-mates-trip-plan-wrapper">
                            <div class="post-top-info-wrap">
                                <div class="mates-info-animation w-135 animation"></div>
                                <div class="wp-left-animation">
                                    <div class="post-top-avatar-wrap mate-avatar-animation animation"></div>
                                    <div class="top-info-animation animation"></div>
                                </div>
                                <div class="mates-info-animation w-310 animation" ></div>
                            </div>
                            <div class="right-btn-animation">
                                <div class="top-info-buttion-animation animation"></div>
                                <div class="top-info-buttion-animation animation"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('before_scripts')
@include('site/travelmates/partials/request-popup')
@include('site/travelmates/partials/trip-popup')
@include('site.travelmates._travel-mates-js')
@endsection