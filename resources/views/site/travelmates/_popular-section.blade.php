<div class="post-block post-flight-style travel-mates-top-post-block">
    <div class="post-side-top">
        <h3 class="side-ttl"><i class="trav-trending-destination-icon"></i> @lang('travelmate.popular_travel_mates')
        </h3>
    </div>
    <div class="post-side-inner">
        <div class="post-slide-wrap post-destination-block mates-top-slider-block">
            <ul id="popularTravelMates" class="post-slider lightSlider lsGrab lSSlide" style="width: 3700px; padding-bottom: 0%;">
                @foreach($popularTravelMates as $popularTravelMate)
                <li class="pull-left">
                    <div class="post-popular-inner">
                        <div class="img-wrap">
                            <a style="text-decoration: none;" href="{{route('profile.show', ['id' => $popularTravelMate->id])}}">    
                                <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{get_profile_cropped_picture($popularTravelMate->profile_picture, $popularTravelMate->id)}}" alt="">
                            </a>
                        </div>
                        <div class="pop-txt">
                            <h5><a style="text-decoration: none;" href="{{route('profile.show', ['id' => $popularTravelMate->id])}}">{{$popularTravelMate->name}}</a></h5>
                            @if(isset($popularTravelMate->country_of_nationality) && is_object($popularTravelMate->country_of_nationality))
                            <div class="travel-mate-from">
                                <a style="text-decoration: none;" href="">
                                    <img
                                        @if(isset($popularTravelMate->country_of_nationality) && isset($popularTravelMate->country_of_nationality->iso_code))
                                            src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($popularTravelMate->country_of_nationality->iso_code)}}.png"
                                        @endif
                                        class="flag-image"
                                        alt=""/><spost-top-info-wrappan>
                                        {{$popularTravelMate->country_of_nationality->trans[0]->title}}
                                    </spost-top-info-wrappan>
                                </a>
                            </div>
                            @endif
                            <p>@choice('profile.count_follower', $popularTravelMate->cnt, ['count' => $popularTravelMate->cnt])</p>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
