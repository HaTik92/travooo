@php
    $title = 'Travooo - travel mates';
@endphp

@extends('site.travelmates.template.travelmate')

@section('content')
    <div class="post-block post-flight-style">
        <div class="post-side-top">
            <h3 class="side-ttl"><i class="trav-trending-destination-icon"></i> @lang('travelmate.popular_travel_mates')
            </h3>
        </div>
        <div class="post-side-inner">
            <div class="post-slide-wrap post-destination-block">
                <ul id="popularTravelMates" class="post-slider">
                    <li>
                        <div class="post-popular-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/155x144" alt="">
                            </div>
                            <div class="pop-txt">
                                <h5>Dorothy Young test teeestov</h5>
                                <div class="travel-mate-from">
                                    <img src="http://placehold.it/16x12" class="flag-image" alt="flag-image"/><span>United States</span>
                                </div>
                                <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-popular-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/155x144" alt="">
                            </div>
                            <div class="pop-txt">
                                <h5>Dorothy Young</h5>
                                <div class="travel-mate-from">
                                    <img src="http://placehold.it/16x12" class="flag-image" alt="flag-image"/><span>United States</span>
                                </div>
                                <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-popular-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/155x144" alt="">
                            </div>
                            <div class="pop-txt">
                                <h5>Dorothy Young</h5>
                                <div class="travel-mate-from">
                                    <img src="http://placehold.it/16x12" class="flag-image" alt="flag-image"/><span>United States</span>
                                </div>
                                <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-popular-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/155x144" alt="">
                            </div>
                            <div class="pop-txt">
                                <h5>Dorothy Young</h5>
                                <div class="travel-mate-from">
                                    <img src="http://placehold.it/16x12" class="flag-image" alt="flag-image"/><span>United States</span>
                                </div>
                                <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-popular-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/155x144" alt="">
                            </div>
                            <div class="pop-txt">
                                <h5>Dorothy Young</h5>
                                <div class="travel-mate-from">
                                    <img src="http://placehold.it/16x12" class="flag-image" alt="flag-image"/><span>United States</span>
                                </div>
                                <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="post-popular-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/155x144" alt="">
                            </div>
                            <div class="pop-txt">
                                <h5>Dorothy Young</h5>
                                <div class="travel-mate-from">
                                    <img src="http://placehold.it/16x12" class="flag-image" alt="flag-image"/><span>United States</span>
                                </div>
                                <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="post-block post-flight-style travel-mates left">
        <div class="post-side-top">
            <div class="post-side-top-nav">
                <li class="active">
                    <h3 class="side-ttl">@lang('travelmate.travel_mates')</h3>
                </li>
                <li>
                    <h3 class="side-ttl">@lang('travelmate.people_around_you')</h3>
                </li>
            </div>
            <div class="post-side-top-sort">
                <h3 class="side-ttl">@lang('travelmate.filter_by')</h3>
                <span class="select2 select2-container select2-container--default">
                    <span class="selection">
                        <span class="select2-selection select2-selection--single select2-input">
                            <span class="select2-selection__rendered">
                                <span class="select2-selection__placeholder">Date</span>
                            </span>
                            <span class="select2-selection__arrow">
                                <b></b>
                            </span>
                        </span>
                    </span>
                    <span class="dropdown-wrapper"></span>
                </span>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="travel-mates-trip-plan-wrapper">
                <div class="post-top-info-wrap">
                    <div class="post-top-avatar-wrap">
                        <img src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="post-top-info-txt">
                        <div class="post-top-name">
                            <a class="post-name-link" href="#">Larry Baber</a>
                        </div>
                        <div class="post-info" dir="auto">Photographer</div>
                    </div>
                    <div class="post-top-info-participants">
                        <div class="participants-imgs">
                            <img src="http://placehold.it/25x25" alt="">
                            <img src="http://placehold.it/25x25" alt="">
                            <img src="http://placehold.it/25x25" alt="">
                        </div>
                        <div class="participants-count">
                            <span class="count">+15</span>@lang('travelmate.more_going')
                        </div>
                        <div class="join-button">
                            @lang('travelmate.ask_to_join')
                        </div>
                    </div>
                </div>
                <div class="post-content-wrap">
                    <div class="map">
                        <img src="http://placehold.it/269x180" alt="map">
                    </div>
                    <div class="info">
                        <div class="params">
                            <ul class="tm-tp-info-list">
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-map-marker-icon"></i>
                                        </div>
                                        <span>@choice('trip.destination_dd', 2)</span>
                                    </div>
                                    <div class="info-right">
                                        <span>12</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-point-flag-icon"></i>
                                        </div>
                                        <span>@lang('trip.starting_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span class="date">sun 30 apr</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-budget-icon"></i>
                                        </div>
                                        <span>@lang('trip.budget_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span>$1520</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <span>@lang('trip.duration_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span>@choice('time.count_day', 15, ['count' => 15])</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="countries">
                            <div class="country">
                                <img src="http://placehold.it/16x12" class="flag" alt="flag"><span>United States</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="travel-mates-trip-plan-wrapper">
                <div class="post-top-info-wrap">
                    <div class="post-top-avatar-wrap">
                        <img src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="post-top-info-txt">
                        <div class="post-top-name">
                            <a class="post-name-link" href="#">Larry Baber</a>
                        </div>
                        <div class="post-info" dir="auto">Photographer</div>
                    </div>
                    <div class="post-top-info-participants">
                        <div class="participants-imgs">
                            <img src="http://placehold.it/25x25" alt="">
                            <img src="http://placehold.it/25x25" alt="">
                            <img src="http://placehold.it/25x25" alt="">
                        </div>
                        <div class="participants-count">
                            <span class="count">+15</span>@lang('travelmate.more_going')
                        </div>
                        <div class="join-button">
                            @lang('travelmate.ask_to_join')
                        </div>
                    </div>
                </div>
                <div class="post-content-wrap">
                    <div class="map">
                        <img src="http://placehold.it/269x180" alt="map">
                    </div>
                    <div class="info">
                        <div class="params">
                            <ul class="tm-tp-info-list">
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-map-marker-icon"></i>
                                        </div>
                                        <span>@choice('trip.destination_dd', 2)</span>
                                    </div>
                                    <div class="info-right">
                                        <span>12</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-point-flag-icon"></i>
                                        </div>
                                        <span>@lang('trip.starting_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span class="date">sun 30 apr</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-budget-icon"></i>
                                        </div>
                                        <span>@lang('trip.budget_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span>$1520</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <span>@lang('trip.duration_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span>@choice('time.count_day', 15, ['count' => 15])</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="countries">
                            <div class="country">
                                <img src="http://placehold.it/16x12" class="flag" alt="flag"><span>United States</span>
                                <img src="{{asset('assets2/image/arrow.png')}}" class="arrow">
                                <img src="http://placehold.it/16x12" class="flag" alt="flag"><span>Morocco</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="travel-mates-trip-plan-wrapper">
                <div class="post-top-info-wrap">
                    <div class="post-top-avatar-wrap">
                        <img src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="post-top-info-txt">
                        <div class="post-top-name">
                            <a class="post-name-link" href="#">Larry Baber</a>
                        </div>
                        <div class="post-info" dir="auto">Photographer</div>
                    </div>
                    <div class="post-top-info-participants">
                        <div class="participants-imgs">
                            <img src="http://placehold.it/25x25" alt="">
                            <img src="http://placehold.it/25x25" alt="">
                            <img src="http://placehold.it/25x25" alt="">
                        </div>
                        <div class="participants-count">
                            <span class="count">+15</span>@lang('travelmate.more_going')
                        </div>
                        <div class="join-button">
                            @lang('travelmate.ask_to_join')
                        </div>
                    </div>
                </div>
                <div class="post-content-wrap">
                    <div class="map">
                        <img src="http://placehold.it/269x180" alt="map">
                    </div>
                    <div class="info">
                        <div class="params">
                            <ul class="tm-tp-info-list">
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-map-marker-icon"></i>
                                        </div>
                                        <span>@choice('trip.destination_dd', 2)</span>
                                    </div>
                                    <div class="info-right">
                                        <span>12</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-point-flag-icon"></i>
                                        </div>
                                        <span>@lang('trip.starting_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span class="date">sun 30 apr</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-budget-icon"></i>
                                        </div>
                                        <span>@lang('trip.budget_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span>$1520</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <span>@lang('trip.duration_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span>@choice('time.count_day', 15, ['count' => 15])</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="countries">
                            <div class="country">
                                <img src="http://placehold.it/16x12" class="flag" alt="flag"><span>United States</span>
                            </div>
                            <div class="collapse">
                                <img src="/assets2/image/collapse-geo.png" class="geo"><span>@lang('travelmate.collapse')</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="travel-mates-trip-plan-wrapper">
                <div class="post-top-info-wrap">
                    <div class="post-top-avatar-wrap">
                        <img src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="post-top-info-txt">
                        <div class="post-top-name">
                            <a class="post-name-link" href="#">Larry Baber</a>
                        </div>
                        <div class="post-info" dir="auto">Photographer</div>
                    </div>
                    <div class="post-top-info-participants">
                        <div class="participants-imgs">
                            <img src="http://placehold.it/25x25" alt="">
                            <img src="http://placehold.it/25x25" alt="">
                            <img src="http://placehold.it/25x25" alt="">
                        </div>
                        <div class="participants-count">
                            <span class="count">+15</span>@lang('travelmate.more_going')
                        </div>
                        <div class="join-button">
                            @lang('travelmate.ask_to_join')
                        </div>
                    </div>
                </div>
                <div class="post-content-wrap">
                    <div class="map">
                        <img src="http://placehold.it/269x180" alt="map">
                    </div>
                    <div class="info">
                        <div class="params">
                            <ul class="tm-tp-info-list">
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-map-marker-icon"></i>
                                        </div>
                                        <span>@choice('trip.destination_dd', 2)</span>
                                    </div>
                                    <div class="info-right">
                                        <span>12</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-point-flag-icon"></i>
                                        </div>
                                        <span>@lang('trip.starting_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span class="date">sun 30 apr</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-budget-icon"></i>
                                        </div>
                                        <span>@lang('trip.budget_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span>$1520</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="info-left">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <span>@lang('trip.duration_dd')</span>
                                    </div>
                                    <div class="info-right">
                                        <span>@choice('time.count_day', 15, ['count' => 15])</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="countries">
                            <div class="country">
                                <img src="http://placehold.it/16x12" class="flag" alt="flag"><span>United States</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="load-more">
            <span>@lang('travelmate.load_more')</span>
        </div>
    </div>
    <div class="post-block post-flight-style travel-mates right">
        <div class="post-side-top">
            <div class="post-side-top-nav">
                <li class="active">
                    <h3 class="side-ttl">@lang('travelmate.find_a_travel_mate')</h3>
                </li>
            </div>
        </div>
        <div class="search-mates">
            <div class="row">
                <div class="plan-type active">@lang('travelmate.with_a_trip_plan')</div>
                <div class="plan-type">@lang('travelmate.without_a_trip_plan')</div>
            </div>
            <div class="row">
                <h5>@lang('time.mutual_dates')</h5>
                <div class="dates">
                    <input type="text" placeholder="@lang('time.trip_start_date')" value="3 April 2018"/>
                    <input type="text" placeholder="@lang('time.trip_finish_date')" />
                </div>
            </div>
            <div class="row">
                <h5>@choice('trip.destination', 2)</h5>
                <div>
                    <input type="text" placeholder="@lang('travelmate.search_destinations')" value=""/>
                </div>
                <div class="tags">
                    <div class="label-tag">
                        <span dir="auto">United States</span>
                        <a href="#" class="tag-close">
                            <i class="trav-close-icon"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <h5>@lang('profile.country_of_origin')</h5>
                <div>
                    <input type="text" placeholder="@lang('region.country_name_dots')" value=""/>
                </div>
                <div class="tags">
                    <div class="label-tag">
                        <span dir="auto">Morocco</span>
                        <a href="#" class="tag-close">
                            <i class="trav-close-icon"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <h5>@lang('profile.interests')</h5>
                <div>
                    <input type="text" placeholder="@lang('profile.search_for_an_interest')" value=""/>
                </div>
                <div class="tags">
                    <div class="label-tag">
                        <span>@lang('trip.nature')</span>
                        <a href="#" class="tag-close">
                            <i class="trav-close-icon"></i>
                        </a>
                    </div>
                    <div class="label-tag">
                        <span>@lang('place.food')</span>
                        <a href="#" class="tag-close">
                            <i class="trav-close-icon"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="dates">
                    <div>
                        <h5>@lang('profile.age')</h5>
                        <div>
                            <select>
                                <option value="25">25 - 30</option>
                                <option value="30">30 - 35</option>
                            </select>
                            <img src="/assets2/image/select-arrow.png" alt="" class="select-arrow" />
                        </div>
                    </div>
                    <div>
                        <h5>@lang('profile.gender')</h5>
                        <div>
                            <select>
                                <option value="male">@lang('profile.male')</option>
                                <option value="female">@lang('profile.female')</option>
                            </select>
                            <img src="/assets2/image/select-arrow.png" alt="" class="select-arrow" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="search-button">
                    @lang('travelmate.button_start_search')
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-flight-style travel-mates right">
        <div class="post-side-top">
            <div class="post-side-top-nav">
                <li class="active">
                    <h3 class="side-ttl">@lang('travelmate.become_travel_mate')</h3>
                </li>
            </div>
        </div>
        <div class="search-mates">
            <div class="become-img">
                <img src="/assets2/image/become_bg.png">
            </div>
            <div class="row become-mate">
                <span>Are you planning a trip to this place?</span> you can list yourself as a Travel Mate so other travelers can find you and send travel requests
            </div>
            <div class="row">
                <div class="search-button">
                    @lang('travelmate.become_travel_mate')
                </div>
            </div>
        </div>
    </div>
@endsection
