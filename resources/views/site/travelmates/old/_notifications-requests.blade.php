<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">

    <title>Travooo - travel mates</title>
</head>

<body>

<div class="main-wrapper">
    @include('site.layouts.header')

    <div class="content-wrap">
        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">

            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url('home')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('navs.general.home')</span>
                            <!--<span class="counter">5</span>-->
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-newest')}}">
                            <i class="trav-newest-icon"></i>
                            <span>@lang('navs.frontend.newest')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-trending')}}">
                            <i class="trav-trending-icon"></i>
                            <span>@lang('navs.frontend.trending')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-soon')}}">
                            <i class="trav-starting-soon-icon"></i>
                            <span>@lang('travelmate.starting_soon')</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('travelmates-notifications-invitations')}}">
                            <i class="trav-notifications-icon"></i>
                            <span>@choice('dashboard.notification', 2)</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#requestPopup">
                            <i class="trav-circle-request-icon"></i>
                            <span>@lang('buttons.general.request')</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#searchTravelMates">
                            <i class="trav-search-lg-icon"></i>
                            <span>@lang('buttons.general.search')</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <div class="post-side-top-inner">
                        <button type="button" class="btn btn-light-bg-grey btn-bordered btn-back">
                            <i class="trav-angle-left"></i>
                        </button>
                        <h3 class="side-ttl">@choice('dashboard.notification', 2)</h3>
                    </div>
                    <div class="post-top-tabs">
                        <div class="detail-list">
                            <div class="detail-block @if($active_tab=='invitations')  current @endif">
                                <a href="{{url('travelmates-notifications-invitations')}}"><b>@lang('travelmate.your_invitations')</b></a>
                            </div>
                            <div class="detail-block @if($active_tab=='requests') current @endif">
                                <a href="{{url('travelmates-notifications-requests')}}"><b>@lang('travelmate.your_requests')</b></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-side-inner">
                    <div class="travel-mates-notification-wrapper">
                        @foreach($invitations AS $invitation)
                            <?php $invitation->plan = \App\Models\TripPlans\TripPlans::find(\App\Models\TravelMates\TravelMatesRequests::find($invitation->id)->plans_id); ?>
                            <div class="notification-block">
                                <div class="img-wrap">
                                    <img src="{{get_plan_map($invitation->plan, 330, 200)}}" alt="" class="travel-map"
                                         style="width:330px;height:200px;">
                                </div>
                                <div class="notification-txt">
                                    <h3 class="not-ttl">{{$invitation->plan->title}}</h3>
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="{{check_profile_picture($invitation->plan->author->profile_picture)}}"
                                                 alt="" style="width:45px;height:45px;">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link"
                                                   href="{{url('profile/'.$invitation->plan->author->id)}}">{{$invitation->plan->author->name}}</a>
                                                {!! get_exp_icon($invitation->plan->author) !!}
                                            </div>
                                            <div class="post-info">{{$invitation->plan->author->display_name}}</div>
                                        </div>
                                        <div class="post-map-info-caption map-blue">
                                            @if(count($invitation->request->going()->where('status', 1)->get())>0)
                                                <div class="post-map-info-caption map-blue" data-toggle="modal"
                                                     data-target="#goingPopup{{$invitation->id}}"
                                                     style='cursor:pointer;'>
                                                    <ul class="foot-avatar-list">
                                                        @foreach($invitation->request->going()->where('status', 1)->get() AS $gogo)
                                                            <li><img class="small-ava"
                                                                     src="{{$gogo->user->profile_picture}}" alt="ava"
                                                                     style="width:20px;height:20px;"></li>
                                                        @endforeach
                                                    </ul>
                                                    <div class="map-label-txt">
                                                        +{{count($invitation->request->going()->where('status', 1)->get())}}
                                                        more going
                                                    </div>
                                                </div>
                                            @else
                                                <div class="post-map-info-caption map-blue">
                                                    <ul class="foot-avatar-list">
                                                    </ul>
                                                    <div class="map-label-txt">
                                                        @lang('trip.count_are_going', ['count' => 0])
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <ul class="not-info-list">
                                        <li>
                                            <i class="trav-point-flag-icon"></i>
                                            <span>Starting:</span>
                                            <b class="date">{{$invitation->plan->start_date}}</b>
                                        </li>
                                        <li>
                                            <i class="trav-budget-icon"></i>
                                            <span>@lang('trip.budget_dd')</span>
                                            <b>{{$invitation->plan->budget ? $invitation->plan->budget : 'n/a'}}</b>
                                        </li>
                                        <li>
                                            <i class="trav-clock-icon"></i>
                                            <span>Duration:</span>
                                            <b>{{$invitation->plan->duration ? $invitation->plan->duration.' Days' : 'n/a'}}</b>
                                        </li>
                                        <li>
                                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/small/{{@strtolower(App\Models\Country\Countries::find(get_plan_first_country_id($invitation->plan->id))->iso_code)}}.png"
                                                 alt="" class="flag" style="width:18px;height:14px;">
                                            <b>{{@App\Models\Country\Countries::find(get_plan_first_country_id($invitation->plan->id))->transsingle->title}}</b>
                                        </li>
                                        <li>
                                            <i class="trav-map-marker-icon"></i>
                                            <span>@choice('trip.destination_dd', 2)</span>
                                            <b>{{$invitation->plan->cities ? count($invitation->plan->cities) : 'n/a'}}</b>
                                        </li>
                                    </ul>
                                </div>
                                <div class="btn-wrap" style="width:144px;">
                                    @if($invitation->status==0)
                                        <div class="btn-label">@lang('travelmate.pending_approval')</div>
                                        <button type="button"
                                                class="btn btn-light-red-bordered">@lang('buttons.general.cancel')</button>
                                    @elseif($invitation->status==1)
                                        <div class="btn-label green">@lang('other.approved')</div>
                                        <button type="button"
                                                class="btn btn-light-red-bordered">@lang('trip.leave_trip_plan')
                                        </button>
                                    @elseif($invitation->status==2)
                                        <div class="btn-label">@lang('other.declined')</div>
                                    @endif


                                </div>
                            </div>
                            <?php $request = $invitation->request;?>
                            @include('site/travelmates/partials/going-popup')
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


@include('site/travelmates/partials/request-popup')
@include('site/travelmates/partials/search-popup')
@include('site/travelmates/partials/trip-popup')
</body>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="{{url('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
<script src="{{url('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
<script src="{{url('assets2/js/script.js')}}"></script>
<script>
    $(document).ready(function () {
        $(".datepicker_start").datepicker({
            minDate: 0,
            dateFormat: "d MM yy",
            altField: "#actual_trip_start_date",
            altFormat: "yy-mm-dd"
        });
        $(".datepicker_end").datepicker({
            minDate: 0,
            dateFormat: "d MM yy",
            altField: "#actual_trip_end_date",
            altFormat: "yy-mm-dd"
        });


        $('#country_select').select2({
            dropdownParent: $('#searchTravelMates')
        });
        $('#country_select2').select2({
            dropdownParent: $('#searchTravelMates')
        });
    });
</script>

</html>
