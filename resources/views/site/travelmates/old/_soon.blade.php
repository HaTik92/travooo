<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">

    <title>Travooo - travel mates</title>
</head>

<body>

<div class="main-wrapper">
    @include('site.layouts.header')

    <div class="content-wrap">
        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">

            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url('home')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('navs.general.home')</span>
                            <!--<span class="counter">5</span>-->
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-newest')}}">
                            <i class="trav-newest-icon"></i>
                            <span>@lang('navs.frontend.newest')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-trending')}}">
                            <i class="trav-trending-icon"></i>
                            <span>@lang('navs.frontend.trending')</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('travelmates-soon')}}">
                            <i class="trav-starting-soon-icon"></i>
                            <span>@lang('travelmate.starting_soon')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-notifications-invitations')}}">
                            <i class="trav-notifications-icon"></i>
                            <span>@choice('dashboard.notification', 2)</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#requestPopup">
                            <i class="trav-circle-request-icon"></i>
                            <span>@lang('buttons.general.request')</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#searchTravelMates">
                            <i class="trav-search-lg-icon"></i>
                            <span>@lang('buttons.general.search')</span>
                        </a>
                    </li>
                </ul>
            </div>

            @if(session()->has('alert-success'))
                <div class="alert alert-success" id="joinRequestSuccess">
                    {{ session()->get('alert-success') }}
                </div>
            @endif

            @if(session()->has('alert-danger'))
                <div class="alert alert-danger" id="joinRequestError">
                    {{ session()->get('alert-danger') }}
                </div>
            @endif

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <div class="post-side-top-inner">
                        <button type="button" class="btn btn-light-bg-grey btn-bordered btn-back">
                            <i class="trav-angle-left"></i>
                        </button>
                        <h3 class="side-ttl">@lang('travelmate.starting_soon')</h3>

                    </div>
                </div>
                <div class="post-side-inner">
                    <div class="travel-mates-trip-plan-wrapper">
                        @foreach($requests AS $request)

                            @include('site/travelmates/partials/trip-block')
                            @include('site/travelmates/partials/going-popup')

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@include('site/travelmates/partials/request-popup')
@include('site/travelmates/partials/search-popup')
@include('site/travelmates/partials/trip-popup')


</body>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="{{url('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
<script src="{{url('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
<script src="{{url('assets2/js/script.js')}}"></script>
<script>
    $(document).ready(function () {
        $(".datepicker_start").datepicker({
            minDate: 0,
            dateFormat: "d MM yy",
            altField: "#actual_trip_start_date",
            altFormat: "yy-mm-dd"
        });
        $(".datepicker_end").datepicker({
            minDate: 0,
            dateFormat: "d MM yy",
            altField: "#actual_trip_end_date",
            altFormat: "yy-mm-dd"
        });


        $('#country_select').select2({
            dropdownParent: $('#searchTravelMates')
        });
        $('#country_select2').select2({
            dropdownParent: $('#searchTravelMates')
        });
    });
</script>

</html>