<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - travel mates</title>
</head>

<body>

<div class="main-wrapper">
    @include('site.layouts.header')

    <div class="content-wrap">
        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">

            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url('home')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('navs.general.home')</span>
                            <!--<span class="counter">5</span>-->
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-newest')}}">
                            <i class="trav-newest-icon"></i>
                            <span>@lang('navs.frontend.newest')</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('travelmates-trending')}}">
                            <i class="trav-trending-icon"></i>
                            <span>@lang('navs.frontend.trending')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-soon')}}">
                            <i class="trav-starting-soon-icon"></i>
                            <span>@lang('travelmate.starting_soon')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-notifications')}}">
                            <i class="trav-notifications-icon"></i>
                            <span>@choice('dashboard.notification', 2)</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-request')}}">
                            <i class="trav-circle-request-icon"></i>
                            <span>@lang('buttons.general.request')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('travelmates-search')}}">
                            <i class="trav-search-lg-icon"></i>
                            <span>@lang('buttons.general.search')</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="top-banner-block top-travel-main-banner"
                 style="background-image: url(./assets2/image/popup-map.jpg)">
                <div class="top-banner-inner">
                    <div class="banner-row">
                        <div class="travel-left-info">
                            <div class="travel-banner-label"><i class="trav-trending-destination-icon"></i> Trending
                                Trip Plan
                            </div>
                            <h3 class="travel-title">@lang('trip.across_the_country_road_trip', ['country' => 'United States'])</h3>
                            <div class="left-info-block">
                                <i class="trav-talk-icon"></i>
                                <span><b>64k</b> @lang('trip.talking_about_it')</span>
                                <ul class="foot-avatar-list">
                                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                            -->
                                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                            -->
                                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                </ul>
                            </div>
                            <div class="left-info-block">
                                <i class="trav-map"></i>
                                <span>View in the map</span>
                            </div>
                            <div class="btn-wrap">
                                <a class="btn btn-light-primary" href="#">Ask to join</a>
                            </div>
                        </div>
                        <div class="travel-right-info">
                            <ul class="sub-list">
                                <li>
                                    <div class="icon-wrap">
                                        <i class="trav-comment-plus-icon"></i>
                                    </div>
                                    <div class="ctxt">
                                        <div class="top-txt">28K</div>
                                        <div class="sub-txt">Travel mate</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-wrap">
                                        <i class="trav-day-duration-icon"></i>
                                    </div>
                                    <div class="ctxt">
                                        <div class="top-txt">3 Days</div>
                                        <div class="sub-txt">Duration</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-wrap">
                                        <i class="trav-user-rating-icon"></i>
                                    </div>
                                    <div class="ctxt">
                                        <div class="top-txt">17</div>
                                        <div class="sub-txt">Destinations</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="banner-row">
                        <div class="head-trip-plan_trip-line">
                            <div class="trip-line" id="tripLineSlider">
                                <div class="trip-line_slide">
                                    <div class="trip-line_slide__inner">
                                        <div class="trip-icon">
                                            <img src="./assets2/image/flag-img-1.png" alt="flag-icon">
                                        </div>
                                        <div class="trip-content">
                                            <div class="trip-line-name">Morocco</div>
                                            <div class="trip-line-tag">
                                                <span class="place-tag">@choice('region.count_city', 1, ['count' => 1])</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trip-line_slide">
                                    <div class="trip-line_slide__inner">
                                        <div class="trip-icon blue-icon">
                                            <i class="trav-set-location-icon"></i>
                                        </div>
                                        <div class="trip-content">
                                            <div class="trip-line-name blue">Rabat-Sale</div>
                                            <div class="trip-line-tag">
                                                <span class="place-tag">@choice('place.count_place', 1, ['count' => 1])</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trip-line_slide">
                                    <div class="trip-line_slide__inner">
                                        <div class="trip-icon">
                                            <img src="http://placehold.it/32x32" alt="flag-icon">
                                        </div>
                                        <div class="trip-content">
                                            <div class="trip-line-name">United arab emirates</div>
                                            <div class="trip-line-tag">
                                                <span class="place-tag">@choice('region.count_city', 1, ['count' => 1])</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trip-line_slide">
                                    <div class="trip-line_slide__inner">
                                        <div class="trip-icon disabled-icon">
                                            <i class="trav-radio-checked2"></i>
                                        </div>
                                        <div class="trip-content">
                                            <div class="trip-line-name">Dubai</div>
                                            <div class="trip-line-tag">
                                                <span class="place-tag">@choice('place.count_place', 1, ['count' => 1])</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trip-line_slide">
                                    <div class="trip-line_slide__inner">
                                        <div class="trip-icon">
                                            <img src="http://placehold.it/32x32" alt="flag-icon">
                                        </div>
                                        <div class="trip-content">
                                            <div class="trip-line-name">Japan</div>
                                            <div class="trip-line-tag">
                                                <span class="place-tag">@choice('region.count_city', 3, ['count' => 3])</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trip-line_slide">
                                    <div class="trip-line_slide__inner">
                                        <div class="trip-icon disabled-icon">
                                            <i class="trav-radio-checked2"></i>
                                        </div>
                                        <div class="trip-content">
                                            <div class="trip-line-name">Tokyo</div>
                                            <div class="trip-line-tag">
                                                <span class="place-tag">@choice('trip.count_place', 2, ['count' => 8])</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trip-line_slide">
                                    <div class="trip-line_slide__inner">
                                        <div class="trip-icon disabled-icon">
                                            <i class="trav-radio-checked2"></i>
                                        </div>
                                        <div class="trip-content">
                                            <div class="trip-line-name">Nagoya</div>
                                            <div class="trip-line-tag">
                                                <span class="place-tag">@choice('trip.count_place', 2, ['count' => 3])</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trip-line_slide">
                                    <div class="trip-line_slide__inner">
                                        <div class="trip-icon disabled-icon">
                                            <i class="trav-radio-checked2"></i>
                                        </div>
                                        <div class="trip-content">
                                            <div class="trip-line-name">Osaka</div>
                                            <div class="trip-line-tag">
                                                <span class="place-tag">@choice('trip.count_place', 2, ['count' => 5])</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#goingPopup">
                Launch going popup
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#placeOneDayPopup">
                Launch trip plan popup
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#searchTravelMates">
                Search travel mates popup
            </button>

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <h3 class="side-ttl"><i class="trav-trending-destination-icon"></i> Trending destinations
                    </h3>
                </div>
                <div class="post-side-inner">
                    <div class="post-slide-wrap post-destination-block">
                        <ul id="trendingTravelDestinations" class="post-slider">
                            <li>
                                <div class="post-dest-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/165x95" alt="">
                                        <div class="dest-name">France</div>
                                    </div>
                                    <div class="dest-txt">
                                        <p><span>1.3K</span> Trip plans</p>
                                        <p><span>12K</span> Travel mates</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-dest-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/165x95" alt="">
                                        <div class="dest-name">Washington</div>
                                    </div>
                                    <div class="dest-txt">
                                        <p><span>1.3K</span> Trip plans</p>
                                        <p><span>12K</span> Travel mates</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-dest-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/165x95" alt="">
                                        <div class="dest-name">@lang('other.central_part')</div>
                                    </div>
                                    <div class="dest-txt">
                                        <p><span>1.3K</span> Trip plans</p>
                                        <p><span>12K</span> Travel mates</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-dest-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/165x95" alt="">
                                        <div class="dest-name">Japan</div>
                                    </div>
                                    <div class="dest-txt">
                                        <p><span>1.3K</span> Trip plans</p>
                                        <p><span>12K</span> Travel mates</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-dest-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/165x95" alt="">
                                        <div class="dest-name">Grand Canyon National park</div>
                                    </div>
                                    <div class="dest-txt">
                                        <p><span>1.3K</span> Trip plans</p>
                                        <p><span>12K</span> Travel mates</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-dest-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/165x95" alt="">
                                        <div class="dest-name">Big ben</div>
                                    </div>
                                    <div class="dest-txt">
                                        <p><span>1.3K</span> Trip plans</p>
                                        <p><span>12K</span> Travel mates</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-dest-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/165x95" alt="">
                                        <div class="dest-name">@lang('other.central_part')</div>
                                    </div>
                                    <div class="dest-txt">
                                        <p><span>1.3K</span> Trip plans</p>
                                        <p><span>12K</span> Travel mates</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-dest-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/165x95" alt="">
                                        <div class="dest-name">Japan</div>
                                    </div>
                                    <div class="dest-txt">
                                        <p><span>1.3K</span> Trip plans</p>
                                        <p><span>12K</span> Travel mates</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <h3 class="side-ttl">Travel mates & trip plans</h3>
                </div>
                <div class="post-side-inner">
                    <div class="travel-mates-trip-plan-wrapper">
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>Morocco</span>
                                            <i class="fa fa-long-arrow-right"></i>
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">@choice('travelmate.count_more_going', ['count' => '+15'])</div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>Morocco</span>
                                            <i class="fa fa-long-arrow-right"></i>
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>Morocco</span>
                                            <i class="fa fa-long-arrow-right"></i>
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>Morocco</span>
                                            <i class="fa fa-long-arrow-right"></i>
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <div class="post-side-top-inner">
                        <button type="button" class="btn btn-light-bg-grey btn-bordered btn-back">
                            <i class="trav-angle-left"></i>
                        </button>
                        <h3 class="side-ttl">@lang('travelmate.newest_travel_mates')</h3>
                    </div>
                </div>
                <div class="post-side-inner">
                    <div class="travel-mates-trip-plan-wrapper">
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>Morocco</span>
                                            <i class="fa fa-long-arrow-right"></i>
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>Morocco</span>
                                            <i class="fa fa-long-arrow-right"></i>
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>@lang('trip.duration')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <div class="post-side-top-inner">
                        <button type="button" class="btn btn-light-bg-grey btn-bordered btn-back">
                            <i class="trav-angle-left"></i>
                        </button>
                        <h3 class="side-ttl">@lang('travelmate.search_results')</h3>
                        <div class="filter-tag">
                            <span>Age 25 - 30</span>
                            <a href="#" class="tag-close">
                                <i class="trav-close-icon"></i>
                            </a>
                        </div>
                        <div class="filter-tag">
                            <span>Male</span>
                            <a href="#" class="tag-close">
                                <i class="trav-close-icon"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="post-side-inner">
                    <div class="travel-mates-trip-plan-wrapper">
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>Morocco</span>
                                            <i class="fa fa-long-arrow-right"></i>
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>Morocco</span>
                                            <i class="fa fa-long-arrow-right"></i>
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="top-banner-wrap">
                <img class="banner-city" src="http://placehold.it/1070x215" alt="banner">
                <div class="banner-cover-txt">
                    <div class="banner-name">
                        <div class="img-wrap">
                            <img src="http://placehold.it/150x95/fff" alt="" class="flag-lg">
                        </div>
                        <div class="banner-name-inner">
                            <div class="banner-ttl">Japan</div>
                            <div class="sub-ttl">@lang('region.country_in_name', ['name' => 'Asia'])</div>
                            <div class="trav-mate-info">
                                <ul class="foot-avatar-list">
                                    <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li><!--
                                            -->
                                    <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li><!--
                                            -->
                                    <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li>
                                </ul>
                                <span>@lang('travelmate.count_travel_mate', ['count' => '21.3K'])</span>
                            </div>
                        </div>
                    </div>
                    <div class="banner-btn">
                        <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side">
                            <i class="trav-comment-plus-icon"></i>
                            <span>@lang('buttons.general.follow')</span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="top-banner-wrap">
                <img class="banner-city" src="http://placehold.it/1070x215" alt="banner">
                <div class="banner-cover-txt">
                    <div class="banner-name">
                        <div class="img-wrap">
                            <img src="http://placehold.it/150x95/fff" alt="" class="flag-lg">
                        </div>
                        <div class="banner-name-inner">
                            <div class="banner-ttl">Paris</div>
                            <div class="sub-ttl">@lang('region.city_in_name', ['name' => 'France'])</div>
                            <div class="trav-mate-info">
                                <ul class="foot-avatar-list">
                                    <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li><!--
                                            -->
                                    <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li><!--
                                            -->
                                    <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li>
                                </ul>
                                <span>@lang('travelmate.count_travel_mate', ['count' => '21.3K'])</span>
                            </div>
                        </div>
                    </div>
                    <div class="banner-btn">
                        <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side">
                            <i class="trav-comment-plus-icon"></i>
                            <span>@lang('buttons.general.follow')</span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <h3 class="side-ttl"><i
                                class="trav-trending-destination-icon"></i> @lang('travelmate.popular_travel_mates')
                    </h3>
                </div>
                <div class="post-side-inner">
                    <div class="post-slide-wrap post-destination-block">
                        <ul id="popularTravelMates" class="post-slider">
                            <li>
                                <div class="post-popular-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/155x145" alt="">
                                    </div>
                                    <div class="pop-txt">
                                        <h5>Dorothy Young</h5>
                                        <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                                        <button type="button"
                                                class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-popular-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/155x145" alt="">
                                    </div>
                                    <div class="pop-txt">
                                        <h5>Dorothy Young</h5>
                                        <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                                        <button type="button"
                                                class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-popular-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/155x145" alt="">
                                    </div>
                                    <div class="pop-txt">
                                        <h5>Dorothy Young</h5>
                                        <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                                        <button type="button"
                                                class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-popular-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/155x145" alt="">
                                    </div>
                                    <div class="pop-txt">
                                        <h5>Dorothy Young</h5>
                                        <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                                        <button type="button"
                                                class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-popular-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/155x145" alt="">
                                    </div>
                                    <div class="pop-txt">
                                        <h5>Dorothy Young</h5>
                                        <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                                        <button type="button"
                                                class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-popular-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/155x145" alt="">
                                    </div>
                                    <div class="pop-txt">
                                        <h5>Dorothy Young</h5>
                                        <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                                        <button type="button"
                                                class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-popular-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/155x145" alt="">
                                    </div>
                                    <div class="pop-txt">
                                        <h5>Dorothy Young</h5>
                                        <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                                        <button type="button"
                                                class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-popular-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/155x145" alt="">
                                    </div>
                                    <div class="pop-txt">
                                        <h5>Dorothy Young</h5>
                                        <p>@choice('profile.count_follower', 261, ['count' => 261])</p>
                                        <button type="button"
                                                class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <div class="post-side-top-inner">
                        <h3 class="side-ttl">@lang('travelmate.travel_mates_in_region', ['region' => 'Japan'])</h3>
                    </div>
                </div>
                <div class="post-side-inner">
                    <div class="travel-mates-trip-plan-wrapper">
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>Morocco</span>
                                            <i class="fa fa-long-arrow-right"></i>
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-mates-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x180" alt="">
                            </div>
                            <div class="travel-mates-trip-plan-inner">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-top-name">
                                                <a class="post-name-link" href="#">Larry Baber</a>
                                            </div>
                                            <div class="post-info">Photographer</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tm-tp-info-block">
                                    <ul class="tm-tp-info-list">
                                        <li class="country">
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>Morocco</span>
                                            <i class="fa fa-long-arrow-right"></i>
                                            <img src="http://placehold.it/18x14" alt="" class="flag">
                                            <span>United States</span>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-map-marker-icon"></i>
                                                </div>
                                                <span>@choice('trip.destination_dd', 2)</span>
                                            </div>
                                            <div class="info-right">
                                                <span>12</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-point-flag-icon"></i>
                                                </div>
                                                <span>Starting:</span>
                                            </div>
                                            <div class="info-right">
                                                <span class="date">sun 30 apr</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-budget-icon"></i>
                                                </div>
                                                <span>@lang('trip.budget_dd')</span>
                                            </div>
                                            <div class="info-right">
                                                <span>$1520</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info-left">
                                                <div class="icon-wrap">
                                                    <i class="trav-clock-icon"></i>
                                                </div>
                                                <span>Duration:</span>
                                            </div>
                                            <div class="info-right">
                                                <span>15 Days</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                                -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <div class="map-label-txt">
                                        @choice('travelmate.count_more_going', ['count' => '+15'])
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-bg-grey btn-bordered">Ask to join
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <div class="post-side-top-inner">
                        <button type="button" class="btn btn-light-bg-grey btn-bordered btn-back">
                            <i class="trav-angle-left"></i>
                        </button>
                        <h3 class="side-ttl">@choice('dashboard.notification', 2)</h3>
                    </div>
                    <div class="post-top-tabs">
                        <div class="detail-list">
                            <div class="detail-block">
                                <b>@lang('travelmate.your_requests')</b>
                            </div>
                            <div class="detail-block current">
                                <b>Travel Mates</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-side-inner">
                    <div class="travel-mates-notification-wrapper">
                        <div class="notification-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x200" alt="" class="travel-map">
                            </div>
                            <div class="notification-txt">
                                <h3 class="not-ttl">From Morocco to Japan in 16 Days</h3>
                                <div class="post-top-info-wrap">
                                    <div class="post-top-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-top-info-txt">
                                        <div class="post-top-name">
                                            <a class="post-name-link" href="#">Larry Baber</a>
                                        </div>
                                        <div class="post-info">Photographer</div>
                                    </div>
                                    <div class="post-map-info-caption map-blue">
                                        <ul class="foot-avatar-list">
                                            <li><img class="small-ava" src="http://placehold.it/25x25" alt="ava"></li><!--
                                                    -->
                                            <li><img class="small-ava" src="http://placehold.it/25x25" alt="ava"></li><!--
                                                    -->
                                            <li><img class="small-ava" src="http://placehold.it/25x25" alt="ava"></li>
                                        </ul>
                                        <div class="map-label-txt">
                                            @choice('travelmate.count_more_going', ['count' => '+15'])
                                        </div>
                                    </div>
                                </div>
                                <ul class="not-info-list">
                                    <li>
                                        <i class="trav-point-flag-icon"></i>
                                        <span>Starting:</span>
                                        <b class="date">sun 30 apr</b>
                                    </li>
                                    <li>
                                        <i class="trav-budget-icon"></i>
                                        <span>@lang('trip.budget_dd')</span>
                                        <b>$1520</b>
                                    </li>
                                    <li>
                                        <i class="trav-clock-icon"></i>
                                        <span>Duration:</span>
                                        <b>15 Days</b>
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/18x14" alt="" class="flag">
                                        <b>United States</b>
                                    </li>
                                    <li>
                                        <i class="trav-map-marker-icon"></i>
                                        <span>@choice('trip.destination_dd', 2)</span>
                                        <b>12</b>
                                    </li>
                                </ul>
                            </div>
                            <div class="btn-wrap">
                                <button type="button"
                                        class="btn btn-light-bg-grey btn-bordered">@choice('travelmate.view_mate', 2)</button>
                            </div>
                        </div>
                        <div class="notification-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x200" alt="" class="travel-map">
                            </div>
                            <div class="notification-txt">
                                <h3 class="not-ttl">From Morocco to Japan in 16 Days</h3>
                                <div class="post-top-info-wrap">
                                    <div class="post-top-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-top-info-txt">
                                        <div class="post-top-name">
                                            <a class="post-name-link" href="#">Larry Baber</a>
                                        </div>
                                        <div class="post-info">Photographer</div>
                                    </div>
                                    <div class="post-map-info-caption map-blue">
                                        <ul class="foot-avatar-list">
                                            <li><img class="small-ava" src="http://placehold.it/25x25" alt="ava"></li><!--
                                                    -->
                                            <li><img class="small-ava" src="http://placehold.it/25x25" alt="ava"></li><!--
                                                    -->
                                            <li><img class="small-ava" src="http://placehold.it/25x25" alt="ava"></li>
                                        </ul>
                                        <div class="map-label-txt">
                                            @choice('travelmate.count_more_going', ['count' => '+15'])
                                        </div>
                                    </div>
                                </div>
                                <ul class="not-info-list">
                                    <li>
                                        <i class="trav-point-flag-icon"></i>
                                        <span>Starting:</span>
                                        <b class="date">sun 30 apr</b>
                                    </li>
                                    <li>
                                        <i class="trav-budget-icon"></i>
                                        <span>@lang('trip.budget_dd')</span>
                                        <b>$1520</b>
                                    </li>
                                    <li>
                                        <i class="trav-clock-icon"></i>
                                        <span>Duration:</span>
                                        <b>15 Days</b>
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/18x14" alt="" class="flag">
                                        <b>United States</b>
                                    </li>
                                    <li>
                                        <i class="trav-map-marker-icon"></i>
                                        <span>@choice('trip.destination_dd', 2)</span>
                                        <b>12</b>
                                    </li>
                                </ul>
                            </div>
                            <div class="btn-wrap">
                                <div class="btn-label">@lang('travelmate.pending_approval')</div>
                                <button type="button"
                                        class="btn btn-light-red-bordered">@lang('buttons.general.cancel')</button>
                            </div>
                        </div>
                        <div class="notification-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/330x200" alt="" class="travel-map">
                            </div>
                            <div class="notification-txt">
                                <h3 class="not-ttl">From Morocco to Japan in 16 Days</h3>
                                <div class="post-top-info-wrap">
                                    <div class="post-top-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-top-info-txt">
                                        <div class="post-top-name">
                                            <a class="post-name-link" href="#">Larry Baber</a>
                                        </div>
                                        <div class="post-info">Photographer</div>
                                    </div>
                                    <div class="post-map-info-caption map-blue">
                                        <ul class="foot-avatar-list">
                                            <li><img class="small-ava" src="http://placehold.it/25x25" alt="ava"></li><!--
                                                    -->
                                            <li><img class="small-ava" src="http://placehold.it/25x25" alt="ava"></li><!--
                                                    -->
                                            <li><img class="small-ava" src="http://placehold.it/25x25" alt="ava"></li>
                                        </ul>
                                        <div class="map-label-txt">
                                            @choice('travelmate.count_more_going', ['count' => '+15'])
                                        </div>
                                    </div>
                                </div>
                                <ul class="not-info-list">
                                    <li>
                                        <i class="trav-point-flag-icon"></i>
                                        <span>Starting:</span>
                                        <b class="date">sun 30 apr</b>
                                    </li>
                                    <li>
                                        <i class="trav-budget-icon"></i>
                                        <span>@lang('trip.budget_dd')</span>
                                        <b>$1520</b>
                                    </li>
                                    <li>
                                        <i class="trav-clock-icon"></i>
                                        <span>Duration:</span>
                                        <b>15 Days</b>
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/18x14" alt="" class="flag">
                                        <b>United States</b>
                                    </li>
                                    <li>
                                        <i class="trav-map-marker-icon"></i>
                                        <span>@choice('trip.destination_dd', 2)</span>
                                        <b>12</b>
                                    </li>
                                </ul>
                            </div>
                            <div class="btn-wrap">
                                <div class="btn-label green">@lang('other.approved')</div>
                                <button type="button"
                                        class="btn btn-light-red-bordered">@lang('buttons.general.cancel')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- modal -->
<!-- going popup -->
<div class="modal fade white-style" data-backdrop="false" id="goingPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-700" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-going-block post-mobile-full">
                <div class="post-top-layer">
                    <div class="post-top-info">
                        <h3 class="info-title">From Morocco to Japan in 7 Days</h3>
                        <ul class="info-list">
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">7 Days</p>
                                    <p class="sub-title">Duration</p>
                                </div>
                            </li>
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-budget-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">$ 5000</p>
                                    <p class="sub-title">@lang('trip.budget')</p>
                                </div>
                            </li>
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-distance-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">2.3 km</p>
                                    <p class="sub-title">@lang('trip.distance')</p>
                                </div>
                            </li>
                            <li class="info-inner">
                                <div class="icon-wrap">
                                    <i class="trav-map-marker-icon"></i>
                                </div>
                                <div class="info-txt">
                                    <p class="title">12</p>
                                    <p class="sub-title">@choice('trip.place', 2)</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="post-photo-wrap">
                        <ul class="photo-list">
                            <li><img src="http://placehold.it/60x60" alt="image" class="image"></li>
                            <li><img src="http://placehold.it/60x60" alt="image" class="image"></li>
                            <li><img src="http://placehold.it/60x60" alt="image" class="image"></li>
                        </ul>
                    </div>
                </div>
                <div class="post-sub-layer">
                    @lang('travelmate.people_going_with') <span>Elijah</span>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar">
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Amy Green</div>
                                <div class="info-line">
                                    <div class="info-part">Photographer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Mark Poe</div>
                                <div class="info-line">
                                    <div class="info-part">Photographer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Randall Burgess</div>
                                <div class="info-line">
                                    <div class="info-part">Photographer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Andria Hinkle</div>
                                <div class="info-line">
                                    <div class="info-part">Surfer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Amy Green</div>
                                <div class="info-line">
                                    <div class="info-part">Photographer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Mark Poe</div>
                                <div class="info-line">
                                    <div class="info-part">Photographer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Randall Burgess</div>
                                <div class="info-line">
                                    <div class="info-part">Photographer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Andria Hinkle</div>
                                <div class="info-line">
                                    <div class="info-part">Surfer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Amy Green</div>
                                <div class="info-line">
                                    <div class="info-part">Photographer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Mark Poe</div>
                                <div class="info-line">
                                    <div class="info-part">Photographer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Randall Burgess</div>
                                <div class="info-line">
                                    <div class="info-part">Photographer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="people-row">
                        <div class="main-info-layer">
                            <div class="img-wrap">
                                <img class="ava" src="http://placehold.it/50x50" alt="ava">
                            </div>
                            <div class="txt-block">
                                <div class="name">Andria Hinkle</div>
                                <div class="info-line">
                                    <div class="info-part">Surfer</div>
                                    <div class="info-part">@choice('profile.count_years', 25, ['count' => 25])</div>
                                    <div class="info-part">Japan</div>
                                    <div class="info-part">@lang('profile.male')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- request popup -->
<div class="modal fade white-style" data-backdrop="false" id="requestPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-780" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-top-bordered post-request-modal-block post-mobile-full">
                <div class="post-side-top">
                    <div class="post-top-txt horizontal">
                        <h3 class="side-ttl">@lang('trip.my_trip_plan') <span class="count">16</span></h3>
                    </div>
                </div>
                <div class="trip-plan-inner mCustomScrollbar">
                    <div class="trip-plan-row">
                        <div class="trip-check-layer"></div>
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img">
                                <img src="http://placehold.it/140x150" alt="map-image">
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl">New york city the right way</h2>
                                    <p class="trip-date">18 to 21 Sep 2017</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>4 Days</b></p>
                                            <p>Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>237 km</b></p>
                                            <p>@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('trip.destination') <span>6</span></h3>
                                <ul class="dest-image-list">
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="trip-plan-row checked-plan">
                        <div class="trip-check-layer"></div>
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img">
                                <img src="http://placehold.it/140x150" alt="map-image">
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl">Over the mediterranean sea</h2>
                                    <p class="trip-date">18 to 21 Sep 2017</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>16 Days</b></p>
                                            <p>Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>16K km</b></p>
                                            <p>@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('trip.destination') <span>13</span></h3>
                                <ul class="dest-image-list">
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li class="more-photo-link">
                                        <img src="http://placehold.it/52x52" alt="photo">
                                        <span class="cover">+8</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="trip-plan-row">
                        <div class="trip-check-layer"></div>
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img">
                                <img src="http://placehold.it/140x150" alt="map-image">
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl">New york city the right way</h2>
                                    <p class="trip-date">18 to 21 Sep 2017</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>4 Days</b></p>
                                            <p>Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>237 km</b></p>
                                            <p>@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('trip.destination') <span>6</span></h3>
                                <ul class="dest-image-list">
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="trip-plan-row">
                        <div class="trip-check-layer"></div>
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img">
                                <img src="http://placehold.it/140x150" alt="map-image">
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl">New york city the right way</h2>
                                    <p class="trip-date">18 to 21 Sep 2017</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>4 Days</b></p>
                                            <p>Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>237 km</b></p>
                                            <p>@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('trip.destination') <span>6</span></h3>
                                <ul class="dest-image-list">
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li class="more-photo-link">
                                        <img src="http://placehold.it/52x52" alt="photo">
                                        <span class="cover">+2</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="trip-plan-row">
                        <div class="trip-check-layer"></div>
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img">
                                <img src="http://placehold.it/140x150" alt="map-image">
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl">New york city the right way</h2>
                                    <p class="trip-date">18 to 21 Sep 2017</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>4 Days</b></p>
                                            <p>Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>237 km</b></p>
                                            <p>@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('trip.destination') <span>6</span></h3>
                                <ul class="dest-image-list">
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li class="more-photo-link">
                                        <img src="http://placehold.it/52x52" alt="photo">
                                        <span class="cover">+2</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-modal-foot">
                    <button class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                    <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.request')</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- place one day popup -->
<div class="modal fade white-style" data-backdrop="false" id="placeOneDayPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-1140 full-height" role="document">
        <ul class="modal-outside-link-list white-bg">
            <li class="outside-link">
                <a href="#">
                    <div class="round-icon">
                        <i class="trav-flag-icon"></i>
                    </div>
                    <span>@lang('other.report')</span>
                </a>
            </li>
        </ul>
        <div class="modal-custom-block">
            <button class="btn btn-mobile-side comment-toggler" id="commentToggler">
                <i class="trav-comment-icon"></i>
            </button>
            <div class="post-block post-place-plan-block">

                <div class="post-image-container post-follow-container mCustomScrollbar">
                    <div class="post-image-inner">
                        <div class="post-map-wrap">
                            <div class="post-map-breadcrumb">
                                <ul class="breadcrumb-list">
                                    <li><a href="#">@lang('trip.trip_overview')</a></li>
                                    <li><a href="#">Japan</a></li>
                                    <li>@choice('count_day_in', 1, ['count' => 1]) <b>Tokyo</b></li>
                                </ul>
                            </div>
                            <ul class="post-time-trip">
                                <li class="day">@lang('time.day')</li>
                                <li class="count">6</li>
                                <li class="all">@lang('other.all')</li>
                            </ul>
                            <div class="post-map-inner">
                                <button class="btn btn-success btn-check btn-bordered">@lang('travelmate.ask_to_join')</button>

                                <img src="./assets2/image/trip-plan-image.jpg" alt="map">
                                <div class="destination-point" style="top:80px;left: 20%;">
                                    <img class="map-marker" src="./assets2/image/marker.png" alt="marker">
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <div class="map-avatar">
                                        <img src="http://placehold.it/25x25" alt="ava">
                                    </div>
                                    <div class="map-label-txt">
                                        Checking on <b>2 Sep</b> at <b>8:30 am</b> and will stay <b>30 min</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="post-destination-block slide-dest-hide-right-margin">
                            <div class="post-dest-slider" id="postDestSliderInner4">
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">Grififth</div>
                                            <div class="dest-count">Observatory</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">Hearst Castle</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">SeaWorld San</div>
                                            <div class="dest-count">Diego</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">United Arab Emirates</div>
                                            <div class="dest-count">2 Destinations</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">SeaWorld San</div>
                                            <div class="dest-count">Diego</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">United Arab Emirates</div>
                                            <div class="dest-count">2 Destinations</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="gallery-comment-wrap" id="galleryCommentWrap">
                    <div class="gallery-comment-inner mCustomScrollbar">
                        <div class="gallery-comment-top">
                            <div class="top-info-layer">
                                <div class="top-avatar-wrap">
                                    <img src="http://placehold.it/50x50" alt="">
                                </div>
                                <div class="top-info-txt">
                                    <div class="preview-txt">
                                        <b>@lang('other.by')</b>
                                        <a class="dest-name" href="#">Elijah Hughes</a>
                                        <p class="dest-date">30 Aug 2017 at 10:00 pm</p>
                                    </div>
                                </div>
                            </div>
                            <div class="gal-com-footer-info">
                                <div class="post-foot-block post-reaction">
                                    <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile">
                                    <span><b>2</b> @choice('other.reaction', 6, ['count' => 6])</span>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-layer">
                            <div class="post-comment-top-info">
                                <div class="comm-count-info">
                                    @choice('comment.count_comment', 5, ['count' => 5])
                                </div>
                                <div class="comm-count-info">
                                    3 / 20
                                </div>
                            </div>
                            <div class="post-comment-wrapper">
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets2/image/icon-smile.png" alt="">
                                                <span>21</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Amine</a>
                                            <a href="#" class="comment-nickname">@ak0117</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                doloribus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets2/image/icon-like.png" alt="">
                                                <span>19</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets2/image/icon-smile.png" alt="">
                                                <span>15</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                            </div>
                        </div>
                    </div>
                    <div class="post-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-add-com-input">
                            <input type="text" placeholder="@lang('comment.write_a_comment')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- travel mates search popup -->
<div class="modal fade white-style" data-backdrop="false" id="searchTravelMates" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-840" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-setting-block search-travel-mates">
                <div class="post-side-top">
                    <h3 class="side-ttl">@lang('travelmate.find_a_travel_mate')</h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="post-content-inner">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="post-form-wrapper">
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">@lang('time.mutual_dates')</label>
                                    <div class="col-sm-4">
                                        <div class="flex-custom">
                                            <input class="flex-input" id="" placeholder="@lang('time.trip_start_date')"
                                                   value="3 April 2018" type="text">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="flex-custom dropdown">
                                            <input data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                                   class="flex-input" id="" placeholder="@lang('time.trip_finish_date')"
                                                   value=""
                                                   type="text" id="dateInput">
                                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-calendar">
                                                <div class="calendar-drop">
                                                    <div class="date-line">
                                                        <a href="#" class="slide-control"><i
                                                                    class="trav-angle-left"></i></a>
                                                        <div class="date-name">
                                                            <b>April</b>
                                                            <span>2018</span>
                                                        </div>
                                                        <a href="#" class="slide-control"><i
                                                                    class="trav-angle-right"></i></a>
                                                    </div>
                                                    <div class="post-calendar-wrap">
                                                        <ul class="cal-day-name-list">
                                                            <li>@lang('time.week.short.sunday')</li>
                                                            <li>@lang('time.week.short.monday')</li>
                                                            <li>@lang('time.week.short.tuesday')</li>
                                                            <li>@lang('time.week.short.wednesday')</li>
                                                            <li>@lang('time.week.short.thursday')</li>
                                                            <li>@lang('time.week.short.friday')</li>
                                                            <li>@lang('time.week.short.saturday')</li>
                                                        </ul>
                                                    </div>
                                                    <div class="post-calendar-wrap">
                                                        <ul class="cal-day-list">
                                                            <li>1</li>
                                                            <li>2</li>
                                                            <li>3</li>
                                                            <li>4</li>
                                                            <li>5</li>
                                                            <li>6</li>
                                                            <li>7</li>
                                                            <li>8</li>
                                                            <li>9</li>
                                                            <li>10</li>
                                                            <li>11</li>
                                                            <li class="selected">12</li>
                                                            <li>13</li>
                                                            <li>14</li>
                                                            <li>15</li>
                                                            <li>16</li>
                                                            <li>17</li>
                                                            <li>18</li>
                                                            <li>19</li>
                                                            <li>20</li>
                                                            <li>21</li>
                                                            <li>22</li>
                                                            <li>23</li>
                                                            <li>24</li>
                                                            <li>25</li>
                                                            <li>26</li>
                                                            <li>27</li>
                                                            <li>28</li>
                                                            <li>29</li>
                                                            <li>30</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">Destinations</label>
                                    <div class="col-sm-8">
                                        <div class="flex-custom">
                                            <input class="flex-input" id=""
                                                   placeholder="@lang('travelmate.search_destinations')" value=""
                                                   type="text">
                                        </div>
                                        <div class="img-wrap">
                                            <img src="http://placehold.it/360x290" alt="" class="map-image">
                                            <ul class="controls">
                                                <li><i class="fa fa-plus"></i></li>
                                                <li><i class="fa fa-minus"></i></li>
                                            </ul>
                                        </div>
                                        <div class="label-tag-wrap">
                                            <div class="label-tag">
                                                <span>United States</span>
                                                <a href="#" class="tag-close">
                                                    <i class="trav-close-icon"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">@lang('profile.country_of_origin')</label>
                                    <div class="col-sm-8">
                                        <div class="flex-custom">
                                            <input class="flex-input" id=""
                                                   placeholder="@lang('region.country_name_dots')" value=""
                                                   type="text">
                                        </div>
                                        <div class="label-tag-wrap">
                                            <div class="label-tag">
                                                <span>Morocco</span>
                                                <a href="#" class="tag-close">
                                                    <i class="trav-close-icon"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">@lang('profile.interests')</label>
                                    <div class="col-sm-8">
                                        <div class="flex-custom">
                                            <input class="flex-input" id=""
                                                   placeholder="@lang('profile.search_for_an_interest')"
                                                   value="" type="text">
                                        </div>
                                        <div class="label-tag-wrap">
                                            <div class="label-tag">
                                                <span>@lang('trip.nature')</span>
                                                <a href="#" class="tag-close">
                                                    <i class="trav-close-icon"></i>
                                                </a>
                                            </div>
                                            <div class="label-tag">
                                                <span>@lang('place.food')</span>
                                                <a href="#" class="tag-close">
                                                    <i class="trav-close-icon"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">@lang('trip.starting_date')</label>
                                    <div class="col-sm-4">
                                        <div class="flex-custom">
                                            <input class="flex-input" id="" placeholder="@lang('trip.starting_date')"
                                                   value="18 April 2018" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">@lang('profile.gender')</label>
                                    <div class="col-sm-8">
                                        <div class="flex-custom">
                                            <select name="" id="" class="custom-select">
                                                <option value="male">@lang('profile.male')</option>
                                                <option value="female">@lang('profile.female')</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">@lang('profile.age')</label>
                                    <div class="col-sm-8">
                                        <div class="flex-custom">
                                            <select name="" id="" class="custom-select">
                                                <option value="25">25 - 30</option>
                                                <option value="30">30 - 35</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="search-travel-side">
                                <div class="search-side-block">
                                    <div class="search-label">@lang('time.mutual_dates_dd')</div>
                                    <div class="search-sub-ttl">3 April 2018</div>
                                </div>
                                <div class="search-side-block">
                                    <div class="search-label">@choice('trip.destination_dd', 2)</div>
                                    <ul class="search-side-list">
                                        <li>United states</li>
                                    </ul>
                                </div>
                                <div class="search-side-block">
                                    <div class="search-label">@lang('profile.country_of_origin'):</div>
                                    <ul class="search-side-list">
                                        <li>Morocco</li>
                                    </ul>
                                </div>
                                <div class="search-side-block">
                                    <div class="search-label">@lang('profile.interests'):</div>
                                    <ul class="search-side-list">
                                        <li>@lang('trip.nature')</li>
                                        <li>@lang('place.food')</li>
                                    </ul>
                                </div>
                                <div class="search-side-block">
                                    <div class="search-label">@lang('trip.starting_date'):</div>
                                    <div class="search-sub-ttl">3 April 2018</div>
                                </div>
                                <div class="search-side-block">
                                    <div class="search-label">@lang('profile.gender'):</div>
                                    <div class="search-sub-ttl">@lang('profile.male')</div>
                                </div>
                                <div class="search-side-block">
                                    <div class="search-label">@lang('profile.age'):</div>
                                    <div class="search-sub-ttl">25 - 30</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-foot-btn">
                    <button class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                    <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.search')</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="{{url('assets2/js/script.js')}}"></script>

</body>

</html>