@extends('site.layouts.site')

@section('before_site_style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@endsection


@section('content-area')
    @include('site.travelmates.template._menu')

    @yield('content')

@endsection



@section('before_site_script')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="{{ asset('assets2/js/select2-4.0.5/dist/css/select2.min.css') }}" rel="stylesheet"/>
    <script src="{{ asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('after_scripts')
    <script>
        $(document).ready(function () {
            $(".datepicker_start").datepicker({
                minDate: 0,
                dateFormat: "d MM yy",
                altField: "#actual_trip_start_date",
                altFormat: "yy-mm-dd"
            });
            $(".datepicker_end").datepicker({
                minDate: 0,
                dateFormat: "d MM yy",
                altField: "#actual_trip_end_date",
                altFormat: "yy-mm-dd"
            });


            $('#country_select').select2({
                dropdownParent: $('#searchTravelMates')
            });
            $('#country_select2').select2({
                dropdownParent: $('#searchTravelMates')
            });

            $(".remove_mate_button").click(function () {
                var request_id = $(this).attr('data-requestid');
                var mate_id = $(this).attr('data-mateid');
                $.post("{{url('travelmates/ajax_remove_mate')}}",
                    {request_id: request_id, mate_id: mate_id},
                    function (data) {
                        if (data == "success") {
                            $("#mate" + mate_id).fadeOut("slow");
                        }
                    });
            });

            $(".cancel_invitation_button").click(function () {
                var request_id = $(this).attr('data-requestid');
                $.post("{{url('travelmates/ajax_cancel_invitation')}}",
                    {request_id: request_id},
                    function (data) {
                        if (data == "success") {
                            $("#invitation" + request_id).fadeOut("slow");
                        }
                    });
            });

        });
    </script>
@endsection
