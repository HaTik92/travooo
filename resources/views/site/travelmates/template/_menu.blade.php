<!-- left outside menu -->
<div class="left-outside-menu-wrap" id="leftOutsideMenu">
    <div class="leftOutsideMenuClose"></div>
    <ul class="left-outside-menu">
        <li>
            <a href="{{url('home')}}">
                <i class="trav-home-icon"></i>
                <span>@lang('navs.general.home')</span>
                <!--<span class="counter">5</span>-->
            </a>
        </li>
        <li  class="{{(Route::currentRouteName() == 'travelmate.newest')?'active':''}}">
            <a href="{{url('travelmates-newest')}}">
                <i class="trav-newest-icon"></i>
                <span>@lang('navs.frontend.trending')</span>
            </a>
        </li>
        {{--
        <li>
            <a href="{{url('travelmates-trending')}}">
                <i class="trav-trending-icon"></i>
                <span>@lang('navs.frontend.trending')</span>
            </a>
        </li>
        <li>
            <a href="{{url('travelmates-soon')}}">
                <i class="trav-starting-soon-icon"></i>
                <span>@lang('travelmate.starting_soon')</span>
            </a>
        </li>
        --}}
        <li class="{{(Route::currentRouteName() == 'travelmate.notifications_requests' || Route::currentRouteName() == 'travelmate.notifications_invitations')?'active':''}}">
            <a href="{{url('travelmates-notifications-requests')}}">
                <i class="trav-notifications-icon"></i>
                <span>@choice('dashboard.notification', 2)</span>
            </a>
        </li>
        <li>
            <a href="#" data-toggle="modal" data-target="#requestPopup">
                <i class="trav-circle-request-icon"></i>
                <span>@lang('buttons.general.request')</span>
            </a>
        </li>
        {{--
        <li>
            <a href="#" data-toggle="modal" data-target="#searchTravelMates">
                <i class="trav-search-lg-icon"></i>
                <span>@lang('buttons.general.search')</span>
            </a>
        </li>
        --}}
    </ul>
</div>