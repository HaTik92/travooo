<div class="post-side-inner">
    <div class="travel-mates-trip-plan-wrapper">
        <div class="w-100">
            <div class="d-inline-block ">
                <h3 class="trev-plan-title">
                    <a href="{{route('trip.plan', $plan->id)}}">{{$plan->title}}</a>
                </h3>
            </div>
            @if ($plan->author->id !== Auth::user()->id)
                <div class="post-top-info-action pull-right">
                    <div class="dropdown">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Trip">
                            @include('site.home.partials._info-actions', ['post'=>$plan])
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="post-top-info-wrap" style="width: 100%;">
            <div class="post-top-avatar-wrap">
                <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{check_profile_picture(@$plan->author->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{route('profile.show', ['id' => $plan->author->id])}}">{{$plan->author->name}}</a>
                    {!! get_exp_icon($plan->author) !!}
                </div>
                <div class="post-info">
                      @if($plan->author->nationality !='')
                        Lives in {{$plan->author->country_of_nationality->transsingle->title}}
                    @endif
                    @if(isset($plan->author->country_of_nationality->iso_code))
                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($plan->author->country_of_nationality->iso_code)}}.png" alt="flag" style="width:18px;height:14px;margin-left: 2px;">
                    @endif
                </div>
            </div>
            <div class="post-top-info-participants">
                @if($plan->join_requests->going()->where('status', 1)->count() > 0)
                <div class="tm-participans-block"  data-toggle="modal" data-target="#goingPopup{{$mates[$plan->id]}}">
                        <div class="participants-imgs">
                            @foreach($plan->join_requests->going()->where('status', 1)->orderBy('id', 'DESC')->limit(3)->get() as $request)
                            <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{check_profile_picture($request->author->profile_picture)}}" alt="">
                            @endforeach
                        </div>
                        <div class="participants-count ">
                            <span class="count">+{{$plan->join_requests->going()->where('status', 1)->count()}}</span>@lang('travelmate.more_going')
                        </div>
                    </div>
                @endif
                <div class="mates-label-block">
                    @if(@$request_status[$plan->id] == 0)
                        <div class="cancel-button cancel_request_button" data-type="1" data-requestid="{{@$mates[$plan->id]}}">Cancel</div>
                    @elseif(@$request_status[$plan->id] == 1)
                        <div class="btn-label travel-mates-label green">@lang('other.approved')</div>
                    @elseif(@$request_status[$plan->id] == 2)
                        <div class="btn-label travel-mates-label red">Disapproved</div>
                    @elseif(@$request_status[$plan->id] == 3)
                        <div class="join-button" request-id="{{@$mates[$plan->id]}}" status="0">
                               Join
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="post-content-wrap">
            <div class="plan-map">
                <div class="map">
                     <img src="{{get_plan_map($plan, 269, 180)}}" alt="map">
                </div>
                <a href="{{route('trip.plan', $plan->id)}}" class="view-plan-button">
                    <img src="{{asset('assets2/image/collapse-geo.png')}}" class="geo"><span>View Plan</span>
                </a>
                 <span class="rep-creation-time">{{@$plan->join_requests->created_at->format('M d, Y')}}</span>
            </div>
            <div class="info">
                <div class="params">
                    <ul class="tm-tp-info-list tmp-list">
                        <li>
                            <div class="info-left">
                                <div class="icon-wrap">
                                    <i class="trav-map-marker-icon"></i>
                                </div>
                                <span>@choice('trip.destination_dd', $plan->trips_places()->count())</span>
                            </div>
                            <div class="info-right">
                                <span>{{$plan->trips_places()->count()}}</span>
                            </div>
                        </li>
                        <li>
                            <div class="info-left">
                                <div class="icon-wrap">
                                    <i class="trav-point-flag-icon"></i>
                                </div>
                                <span>@lang('trip.starting_dd')</span>
                            </div>
                            <div class="info-right">
                                <span class="date text-uppercase">{{plan_starting_date($plan)}}</span>
                            </div>
                        </li>
                        @if($plan->budget)
                        <li>
                            <div class="info-left">
                                <div class="icon-wrap">
                                    <i class="trav-budget-icon"></i>
                                </div>
                                <span>@lang('trip.budget_dd')</span>
                            </div>
                            <div class="info-right">
                                <span>${{$plan->budget}}</span>
                            </div>
                        </li>
                        @endif
                        <li>
                            <div class="info-left">
                                <div class="icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <span>@lang('trip.duration_dd')</span>
                            </div>
                            <div class="info-right">
                                <span>{{calculate_duration($plan->id, 'all')}}</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="countries">
                    <div @if($plan->trips_countries()->count() > 1)class="travelMateCountries"@else class="country" @endif>
                          @foreach($plan->trips_countries()->groupBy('countries_id')->get() as $country)
                          @if ($loop->iteration > 1)
                          <img src="{{asset('assets2/image/arrow.png')}}" class="arrow">
                        @endif
                        <div style="display: inline-flex;height: 42px;justify-content: center;align-items: center;">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/small/{{strtolower($country->country->iso_code)}}.png" class="flag" alt="flag"><span>{{$country->country->transsingle->title}}</span>
                        </div>
                        @endforeach
                
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $request = App\Models\TravelMates\TravelMatesRequests::find($mates[$plan->id]);
?>
@if($request->plan)
@include('site/travelmates/partials/going-popup', ['request'=>$request, 'from_main_view'=>1])
@endif