<div class="post-side-inner">
    <div class="travel-mates-trip-plan-wrapper">
        <div class="w-100">
            <div class="d-inline-block ">
                <h3 class="trev-plan-title">{{$plan->title}}</h3>
            </div>
            @if($plan->author->id !== Auth::user()->id)
                <div class="post-top-info-action pull-right">
                    <div class="dropdown">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Trip">
                            @include('site.home.partials._info-actions', ['post'=>$plan])
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="post-top-info-wrap" style="width: 100%;">
            <div class="post-top-avatar-wrap">
                <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{check_profile_picture(@$plan->author->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt m-top-text">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{route('profile.show', ['id' => $plan->author->id])}}">{{$plan->author->name}}</a>
                    {!! get_exp_icon($plan->author) !!}
                </div>
                <div class="post-info m-post-info">
                    @if($plan->author_location)
                        Lives in {{$plan->author_location->transsingle->title}}, {{$plan->author_location->country->transsingle->title}}
                    @endif
                    @if(isset($plan->author_location->country->iso_code))
                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($plan->author_location->country->iso_code)}}.png" alt="flag" style="width:18px;height:14px;margin-left: 2px;">
                    @endif
                </div>
            </div>
            <div class="post-top-info-participants">
                @if($plan->plan_requests->going()->where('status', 1)->count() > 0)
                    <div class="participants-imgs">
                        @foreach($plan->plan_requests->going()->where('status', 1)->orderBy('id', 'DESC')->limit(3)->get() as $request)
                        <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{check_profile_picture($request->author->profile_picture)}}" alt="">
                        @endforeach
                    </div>
                    <div class="participants-count">
                        <span class="count">+{{$plan->plan_requests->going()->where('status', 1)->count()}}</span>@lang('travelmate.more_going')
                    </div>
                @endif
                <div class="mates-label-block">
                    @if(@$status[$plan->id] == \App\Services\Trips\TripInvitationsService::STATUS_PENDING)
                        <div plan-id="{{@$invited_plan_id[$plan->id]}}" user-id="{{$plan->users_id}}" class="cancel-button" id="cancel-travel-mate-invitation">Cancel</div>
                    @elseif(@$status[$plan->id] == \App\Services\Trips\TripInvitationsService::STATUS_ACCEPTED)
                        <div class="btn-label travel-mates-label green">Approved</div>
                    @elseif(@$status[$plan->id] == \App\Services\Trips\TripInvitationsService::STATUS_REJECTED)
                        <div class="btn-label travel-mates-label red">Disapproved</div>
                    @elseif(@$status[$plan->id] == 3)
                        <button class="invite-button" request-id="{{@$plan->id}}" user-id="{{$plan->users_id}}"   data-toggle="modal" data-target="#invitationPopup">Invite</button>
                    @endif
                </div>
            </div>
        </div>
        <div class="post-content-wrap">
            <div class="info wplan-info">
                <div class="wplan-params">
                    <ul>
                        <li>
                            <div class="icon-wrap">
                                <i class="trav-point-flag-icon"></i>
                            </div>
                            <span>{{date('D j M', strtotime($plan->start_date))}}</span>
                        </li>
                        <li><span class="med-border"></span></li>
                        <li>
                            <div class="icon-wrap">
                                <i class="trav-clock-icon"></i>
                            </div>
                            <span>{{dateDiffDays($plan->start_date, $plan->end_date)}}</span>
                        </li>
                    </ul>
                </div>
                <div class="countries wplan-countries">
                    @php $country_list = []; @endphp
                    <div @if(count($plan->countries()) > 1)class="travelMateCountries"@else class="country" @endif >
                          @foreach($plan->countries() as $country)
                          @php $country_list[] = $country['country_info']->id; @endphp
                          @if ($loop->iteration > 1)
                          <img src="{{asset('assets2/image/arrow.png')}}" class="arrow">
                        @endif
                        <div style="display: inline-flex;height: 42px;justify-content: center;align-items: center;">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/small/{{strtolower($country['country_info']->iso_code)}}.png" class="flag" alt="flag"><span>{{$country['name']}}</span>
                        </div>
                        @endforeach
                         </div>
                </div>
                <span class="plan-destination-{{@$plan->id}}" plan_destinations="{{count(@$plan->countries()) > 1?implode(',', $country_list):''}}"></span>
            </div>
        </div>
        <span class="rep-creation-time">{{@$plan->created_at->format('M d, Y')}}</span>
    </div>
</div>