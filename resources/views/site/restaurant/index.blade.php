@php
    $title = 'Travooo - pages of places';
@endphp

@extends('site.layouts.site')

@section('before_site_style')
    <link rel="stylesheet" href="{{asset('assets2/js/jquery-bar-rating/dist/themes/fontawesome-stars.css')}}">
@endsection

@section('base')
    @include('site.layouts._header')

    <div class="follow-header" id="followHeader">
        <div class="container-fluid">
            <div class="follow-inner">
                <div class="place-name">
                    <span>{{$restaurant->trans[0]->title}}</span>
                    <span class="place-tag">{{do_placetype($restaurant->place_type)}}</span>
                </div>
                <div class="follow-btn" id="followContainer2">

                </div>
            </div>
        </div>
    </div>

    <div class="content-wrap">

        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">
            @include('site.restaurant._menu')

            <div class="top-banner-wrap">
                <div class="top-banner-block"
                     style="background-image: url(@if(isset($restaurant->getMedias[0]->url)) https://s3.amazonaws.com/travooo-images2/{{$restaurant->getMedias[0]->url}} @else asset('assets2/image/placeholders/pattern.png') @endif)">
                    <div class="top-banner-text">
                        <div class="place-info-block">
                            <div class="place-title">{{$restaurant->trans[0]->title}} <span>#{{$restaurant->id}}</span>
                            </div>
                            <div class="place-placement"><span
                                        class="event-tag">{{do_placetype($restaurant->place_type)}}</span> {{$restaurant->trans[0]->address}}
                            </div>
                            <div class="follow-block-info">
                                <ul class="foot-avatar-list">
                                    @foreach($restaurant->followers AS $follower)
                                        <li><img class="small-ava"
                                                 src="{{check_profile_picture($follower->user->profile_picture)}}"
                                                 alt="ava" style="width:29px;height:29px;"></li>
                                    @endforeach

                                </ul>
                                <i class="trav-talk-icon"></i>
                                <span>{{count($restaurant->followers)}} @lang('place.following_this_place')</span>
                            </div>
                        </div>
                        <div class="banner-comment">
                            <div class="comment-inner">
                                @if(isset($restaurant->medias[0]) && is_object($restaurant->medias[0]))
                                    @foreach($restaurant->medias[0]->comments AS $mc)
                                        <div class="comment-alert">
                                            <div class="comment-txt-wrap">
                                                <div class="comment-name">{{$mc->user->name}}</div>
                                                <div class="comment-txt">{{$mc->comment}}</div>
                                            </div>
                                            <div class="comment-avatar-wrap">
                                                <img src="{{check_profile_picture($mc->user->profile_picture)}}" alt=""
                                                     style="width:27px;height:27px;">
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                            <span id="followContainer1">

                            </span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="top-now-in-city-block">
                <div class="city-inner" id="nowInPlace">

                </div>
            </div>

            @include('site/restaurant/partials/top_blocks')

            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <div class="post-block post-place-block">
                        <div class="post-place-top">
                            <h3 class="side-ttl">@lang('place.about_this_place')</h3>
                        </div>
                        <div class="post-content-inner">

                            <div class="post-map-block">
                                <div class="post-map-inner">
                                    <img src="https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{$restaurant->lat}},{{$restaurant->lng}}&markers=color:red%7Clabel:C%7C{{$restaurant->lat}},{{$restaurant->lng}}&zoom=14&size=595x360&key={{env('GOOGLE_MAPS_KEY')}}"
                                         alt="Map of {{@$restaurant->trans[0]->title}}">
                                    <div class="post-top-map-info">
                                        <div class="info-block">
                                            <div class="info-icon">
                                                <i class="trav-popularity-icon"></i>
                                            </div>
                                            <div class="info-txt">
                                                <div class="info-ttl">#{{$restaurant->id}}</div>
                                                <div class="info-smpl">@lang('place.popularity')</div>
                                            </div>
                                        </div>
                                        <div class="info-block">
                                            <div class="info-icon">
                                                <i class="trav-safety-big-icon"></i>
                                            </div>
                                            <div class="info-txt">
                                                <div class="info-ttl">9/10</div>
                                                <div class="info-smpl">@lang('place.safety')</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-map-flag">
                                        <img class="flag-image"
                                             src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($restaurant->country->iso_code)}}.png"
                                             alt="flag" style='width:50px;'>
                                    </div>

                                </div>
                            </div>
                            <div class="post-place-footer">
                                <div class="place-address">
                                    <span class="address-label">@lang('profile.address'):</span>
                                    <span>{{$restaurant->trans[0]->address}}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="post-block post-review-block">
                        <div class="review-inner">
                            @if($restaurant->trans[0]->working_days!='')
                                <div class="review-info-block half-block">
                                    <div class="info-ttl">@lang('place.working_times')</div>
                                    <div class="info-txt">
                                        <p>
                                            {!!$restaurant->trans[0]->working_days!!}
                                        </p>
                                    </div>
                                </div>
                            @endif

                            @if($restaurant->trans[0]->description!='')
                                <div class="review-info-block half-block">
                                    <div class="info-ttl">@lang('profile.website')</div>
                                    <div class="info-txt">
                                        <a href="{{$restaurant->trans[0]->description}}" target="blank"
                                           class="web-site">{{$restaurant->trans[0]->description}}</a>
                                    </div>
                                </div>
                            @endif

                            @if($restaurant->trans[0]->phone!='')
                                <div class="review-info-block half-block">
                                    <div class="info-ttl">@lang('profile.phone')</div>
                                    <div class="info-txt">
                                        <a href="{{$restaurant->trans[0]->phone}}" target="blank"
                                           class="web-site">{{$restaurant->trans[0]->phone}}</a>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>

                    <div class="post-block post-user-rating">
                        <div class="post-side-top">
                            <div class="post-top-txt">
                                <h3 class="side-ttl">@lang('place.user_rating')</h3>
                                <div class="com-star-block">
                                    <select class="rating_stars">
                                        <option value="1" @if(round($reviews_avg)==1) selected @endif>1</option>
                                        <option value="2" @if(round($reviews_avg)==2) selected @endif>2</option>
                                        <option value="3" @if(round($reviews_avg)==3) selected @endif>3</option>
                                        <option value="4" @if(round($reviews_avg)==4) selected @endif>4</option>
                                        <option value="5" @if(round($reviews_avg)==5) selected @endif>5</option>
                                    </select>
                                    <span class="count">
                                      <b>{{round($reviews_avg, 1)}}</b>
                                    </span>
                                    <span>@lang('chat.from')</span>
                                    <a href="#" class="review-link">{{count($reviews)}} @lang('place.reviews')</a>
                                </div>
                            </div>
                            <div class="top-btn-wrap">
                                <button type="button" class="btn btn-transp btn-bordered" data-toggle="modal"
                                        data-target="#writeReviewPopup">@lang('place.write_a_review')
                                </button>
                            </div>
                        </div>
                        <div class="post-comment-layer">
                            <div class="post-comment-wrapper">
                                @foreach($reviews AS $review)

                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="{{$review->google_profile_photo_url ? $review->google_profile_photo_url : check_profile_picture($review->author->profile_picture)}}"
                                                 alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#"
                                                   class="comment-name">{{$review->google_author_name ? $review->google_author_name : $review->author->name}}</a>
                                                {!! get_exp_icon($review->author) !!}
                                                <a href="#" class="comment-nickname"></a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>{{$review->text}}</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <select class="rating_stars">
                                                        <option value="1" @if($review->score==1) selected @endif>1
                                                        </option>
                                                        <option value="2" @if($review->score==2) selected @endif>2
                                                        </option>
                                                        <option value="3" @if($review->score==3) selected @endif>3
                                                        </option>
                                                        <option value="4" @if($review->score==4) selected @endif>4
                                                        </option>
                                                        <option value="5" @if($review->score==5) selected @endif>5
                                                        </option>
                                                    </select>

                                                    <span class="count">
                                                        <b>{{$review->score}}</b> / 5
                                                    </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">{{$review->google_time ? diffForHumans(date("Y-m-d", $review->google_time)) : diffForHumans(date("Y-m-d", $review->created_at->timestamp))}}</div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="post-block">
                        <div class="post-side-top">
                            <h3 class="side-ttl">@lang('place.photos_of') {{$restaurant->trans[0]->title}} <span
                                        class="count">{{count($restaurant->getMedias)}}</span></h3>
                            <div class="side-right-control">
                                <a href="#" class="see-more-link lg">@lang('place.more_photos')</a>
                            </div>
                        </div>
                        <div class="post-image-container">
                            <div class="flex-image-wrap fh-395 bw-5" id="placePopupTrigger2" style="cursor:pointer;">
                                <div class="image-column col-33">
                                    @foreach($restaurant->getMedias AS $photo)
                                        <div class="image-inner img-h-66"
                                             style="background-image:url(https://s3.amazonaws.com/travooo-images2/{{$photo->url}})"></div>
                                        @if($loop->iteration%2==0) </div>
                                <div class="image-column col-33">@endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="post-block">
                        <div class="post-side-top">
                            <h3 class="side-ttl">@lang('place.happening_today_near') {{$restaurant->transsingle->title}}</h3>
                        </div>
                        <div class="post-side-inner">
                            <div class="post-map-block">
                                <div class="post-map-inner">
                                    <div id="happeningTodayMap" style='width:595px;height:360px;'></div>
                                </div>
                            </div>
                            <div class="post-happen-wrap mCustomScrollbar" id='happeningToday'>


                            </div>
                        </div>
                    </div>

                    @if(count($restaurant->followers))
                        <div class="post-block">
                            <div class="post-side-top">
                                <h3 class="side-ttl">@lang('place.following_this_place') <span
                                            class="count">{{count($restaurant->followers)}}</span></h3>
                                <div class="side-right-control">
                                    <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                                    <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
                                </div>
                            </div>
                            <div class="post-side-inner">
                                <div class="post-slide-wrap slide-hide-right-margin">
                                    <ul id="newPeopleDiscover" class="post-slider">
                                        @foreach($restaurant->followers AS $follower)
                                            <li class="post-follow-card">
                                                <div class="follow-card-inner">
                                                    <div class="image-wrap">
                                                        <img src="{{check_profile_picture($follower->user->profile_picture)}}"
                                                             alt="" style='width:42px;height:42px;'>
                                                    </div>
                                                    <div class="post-slider-caption">
                                                        <p class="post-card-name">{{$follower->user->name}}</p>
                                                        <p class="post-card-spec">{{$follower->user->display_name}}</p>
                                                        <button type="button" class="btn btn-light-grey btn-bordered">
                                                            @lang('place.follow')
                                                        </button>
                                                        <button type="button"
                                                                class="btn btn-light-primary btn-bordered">@lang('place.following')
                                                        </button>
                                                        <p class="post-card-follow-count">
                                                            @lang('profile.count_followers', [
                                                                'count' => count($follower->user->followers)
                                                            ])
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        @if(count($restaurant->getMedias))
                            <div class="post-block post-photo-block">
                                <?php
                                $rand = rand(0, count($restaurant->getMedias) - 1);
                                ?>
                                <div class="photo-inner">
                                    <img src="https://s3.amazonaws.com/travooo-images2/{{$restaurant->getMedias[$rand]->url}}"
                                         alt="photo" style="width:350px;height:405px;">
                                    <div class="photo-text-cover">
                                        <div class="photo-top-layer">
                                            <div class="place-info">
                                                <div class="place-name">{{$restaurant->trans[0]->title}}</div>
                                                <div class="photo-like">
                                                    <i class="trav-heart-fill-icon"></i>
                                                    <span class="txt">{{count($restaurant->medias[$rand]->likes)}}
                                                        @lang('place.likes')</span>
                                                </div>
                                            </div>
                                            <div class="photo-popup-link">
                                                <a href="#">
                                                    <i class="trav-gallery-o-icon"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="author-info">@lang('place.photo_by')
                                            <span>{{@$restaurant->getMedias[$rand]->author_name}}</span></div>
                                    </div>
                                </div>
                                <div class="follow-block-info">
                                    <ul class="foot-avatar-list">
                                        @foreach($restaurant->followers AS $pfollower)
                                            <li><img class="small-ava"
                                                     src="{{check_profile_picture($pfollower->user->profile_picture)}}"
                                                     alt="ava" style="width:29px;height:29px;"></li>
                                        @endforeach

                                    </ul>
                                    <i class="trav-comment-plus-icon"></i>
                                    <span>{{count($restaurant->followers)}} @lang('place.following_this_place')</span>
                                </div>
                                <div class="photo-btn-wrap">
                                    <a class="btn btn-light-red" href="#">@lang('place.book_a_hotel-flight')</a>
                                </div>
                            </div>
                        @endif

                        @include('site.layouts._share')

                        @include('site.layouts._sidebar')

                    </aside>
                </div>
            </div>

            <div class="full-width-block">

                <div class="post-block post-flage-detail">
                    <!-- <div class="big-place-image">
                      <img src="http://placehold.it/1070x225" alt="place">
                    </div> -->

                    <div class="post-event-block">
                        <div class="post-event-image">
                            <img class="event-cover"
                                 src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$restaurant->country->getMedias[0]->url}}"
                                 alt="photo">
                        </div>
                        <div class="post-event-main">
                            <div class="flag-wrap">
                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($restaurant->country->iso_code)}}.png"
                                     alt="flag" style='width:155px;'>
                            </div>
                            <div class="post-placement-info">
                                <span class="hash"></span>
                                <h2 class="place-name">{{@$restaurant->country->trans[0]->title}}</h2>
                                <div class="event-info-layer">
                                    <span class="placement-place">@lang('place.country_in') {{@$restaurant->country->region->trans[0]->title}}</span>
                                    <div id="talkingAbout">

                                    </div>
                                </div>
                                <div class="event-main-content">
                                    <p>{{strip_tags(@$restaurant->country->trans[0]->description)}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="follow-bottom-link" id="country_follow_button">

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('before_scripts')
    @include('site.place._modals')
@endsection

@section('before_site_script')
    <script src="{{asset('assets2/js/jquery-bar-rating/dist/jquery.barrating.min.js')}}"></script>
@endsection

@section('after_scripts')
    @include('site.place._scripts')
@endsection
