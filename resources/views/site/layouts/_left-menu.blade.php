<!-- left outside menu -->
<div class="left-outside-menu-wrap" id="leftOutsideMenu">
    <div class="leftOutsideMenuClose"></div>
    <ul class="left-outside-menu">
        <li class="{{(Route::currentRouteName() == 'home')?'active':''}}">
            <a href="{{route('home')}}">
                <i class="trav-home-icon"></i>
                <span>@lang('navs.general.home')</span>
                <!--<span class="counter">5</span>-->
            </a>
        </li>
<!--        <li>
            <a href="{{route('travelmate.newest')}}">
                <i class="trav-travel-mates-icon"></i>
                <span>@choice('travelmate.travel_mate', 2)</span>
            </a>
        </li>-->
        <li class="{{(Route::currentRouteName() == 'globaltrip.index')?'active':''}}">
            <a href="{{ route('globaltrip.index') }}">
                <i class="trav-trip-plans-p-icon"></i>
                <span>@lang('trip.trip_plans')</span>
            </a>
        </li>
        <li class="{{(Route::currentRouteName() == 'discussion.index')?'active':''}}">
            <a href="{{route('discussion.index')}}">
                <i class="trav-ask-icon"></i>
                <span>@lang('navs.frontend.ask')</span>
            </a>
        </li>
        <!-- <li><a href="{{ route('profile.map') }}">
                <i class="trav-map-o"></i>
                <span>@lang('navs.frontend.map')</span>
            </a>
        </li> -->
<!--        <li class="{{(Route::currentRouteName() == 'book.flight' || Route::currentRouteName() == 'book.search_flight')?'active':''}}">
            <a href="{{route('book.flight')}}">
                <i class="trav-angle-plane-icon"></i>
                <span>@lang('navs.frontend.flights')</span>
            </a>
        </li>-->
<!--        <li class="{{(Route::currentRouteName() == 'book.hotel' || Route::currentRouteName() == 'book.search_hotel')?'active':''}}">
            <a href="{{route('book.hotel')}}">
                <i class="trav-building-icon"></i>
                <span>@lang('navs.frontend.hotels')</span>
            </a>
        </li>-->
        <li class="{{(Route::currentRouteName() == 'report.list')?'active':''}}">
            <a href="{{ url_with_locale('reports/list') }}">
            <i class="fa-2x fa fa-newspaper-o" aria-hidden="true"></i>
                <span>@lang('navs.frontend.reports')</span>
            </a>
        </li>

    @if(Auth::check() && Route::currentRouteName() != 'newsfeed')
        <li class="">
            <button class="page-content__add-post-btn open-create-actions" type="button"  dir="auto" data-toggle="modal" data-target="#createNewActions">
                <svg class="icon icon--add" dir="auto">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#add')}}" dir="auto"></use>
                </svg>Create
            </button>
        </li>
    @endif
    </ul>
</div>
