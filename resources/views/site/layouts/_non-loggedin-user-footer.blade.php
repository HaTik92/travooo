<div class="non-logged-footer">
    <div class="container-fluid d-flex align-items-center justify-content-center">
        <div>
            <p class="non-logged-footer-title mobile-hide">Don’t miss what’s happening</p>
            <p class="non-logged-footer-desc mobile-hide">People on Travooo are the first to know.</p>
        </div>
        <div class="non-logged-footer-action-block">
            <a href="{{url('/login')}}" class="btn non-logged-btn footer-login-btn">Log In</a>
            <a href="{{url('/signup?dt=regular')}}" class="btn non-logged-btn footer-signup-btn">Sign Up</a>
      </div>
    </div>
</div>

<div class="modal fade responsive-modal res-scroll non-loggedin-login-modal" id="signUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog sign-up-style" role="document">
        <div class="modal-content common-content">

            <div class="modal-body">
                <div class="top-layer text-center">
                    <img class="nl-logo" src="{{asset('frontend_assets/image/main-logo.png')}}">
                    <p class="login-descripton">{{ __('Login to your account to get started') }}</p>
                    <div class="signUp_error_message">
                        @if (\Session::has('error'))
                            <p class="sub-ttl error-message">{!! \Session::get('error') !!}</p>
                        @endif
                    </div>
                </div>
                <form class="login-form" id="signUpForm">
                    <div class="form-group input-group">
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="{{ __('Username or Email address')}}" >
                    </div>
                    <div class="form-group input-group">
                        <input type="password" class="form-control" id="password" name="password" aria-describedby="pass" placeholder="{{ __('Password') }}">
                    </div>
                    <div class="form-group">
                        <a href="{{url('password/email')}}" class="forget-password-link">{{ __('Forget your password?') }}</a>
                    </div>
                    @if(config('captcha.key'))
                        <div class="g-recaptcha form-group login-recaptcha d-none"
                             data-sitekey="{{config('captcha.key')}}">
                        </div>
                    @endif
                    <div class="form-group">
                        <button type="button" id="log_in_btn" class="btn login-access-btn">
                            <span class="spinner-border spinner-border-sm d-none signIn-spiner" role="status" aria-hidden="true"></span> {{ __('Log In') }}</button>
                    </div>
                    <div class="form-group">
                        <p class="simple-txt text-center">OR</p>
                    </div>
                    <div class="form-group">
                        <button type="button" onclick="window.location.href='{{ url("/facebook/redirect") }}'" class="btn btn-social-button facebook_login"><img class="social-icon" src="{{asset('frontend_assets/image/facebook-icon.png')}}">{{ __('Login with Facebook')}}</button>
                    </div>
                    <div class="form-group">
                        <button type="button"  onclick="window.location.href='{{url("/twitter/redirect")}}'"  class="btn btn-social-button twitter_login"><img class="social-icon" src="{{asset('frontend_assets/image/twitter-icon.png')}}">{{ __('Login with Twitter')}}</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div>
                    <span class="foot-txt">{{ __('You are not a member yet?')}}</span> <span class="signup-button go_sign_up" data-type="regular">{{ __('Sign Up')}}</span>
                </div>
                <div class="exp-signup-button go_sign_up" data-type="expert">
                    <span class="exp-text-title">Create an</span>
                    <span class="exp-text-desc">Expert Account</span>
                    <span class="exp-icon">EXP</span>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '.open-login', function(){
        $('.modal').modal('hide');
        $('.non-logged-footer').hide();
        $('#signUp').modal('show')
    })
    
    $('#signUp').on('hidden.bs.modal', function () {
         $('.non-logged-footer').show();
    });
    
     $(document).on('click', '.go_sign_up', function() {
        var type = $(this).attr('data-type')
       window.location.href="{{url('signup')}}?dt=" + type;
    });
    
     $(document).on('keyup', function(e){
            if(e.keyCode == 13 && $('#email').val() !='' && $('#password').val() != ''){
                 $("#log_in_btn").click()
            }
        })
        
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("#log_in_btn").click(function(){
        $(this).prop("disabled", true);
        $('.signIn-spiner').removeClass('d-none')

        var password = $('#password').val();
        var email = $('#email').val();
        var recaptcha = $('textarea[name=g-recaptcha-response]').val();

        $.ajax({
            url: "{{url('login')}}",
            type: 'POST',
            data: {_token: CSRF_TOKEN,password:password,email:email, recaptcha: recaptcha},
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                window.location.reload();
            },
            error: function (data) {
                if (data.responseJSON) {
                    $('#log_in_btn').prop("disabled", false);
                    $('.signIn-spiner').addClass('d-none')
                    $('.signUp_error_message').empty();
                    if (data.responseJSON.errors && data.responseJSON.errors.email) {
                        $('#email').parent().addClass('has-danger');
                        $('.signUp_error_message').append('<p class="sub-ttl error-message">' + data.responseJSON.errors.email + '</p>');

                       if(data.responseJSON.session == 1){
                           $('.login-recaptcha').removeClass('d-none')
                           grecaptcha.reset();
                       }
                    }
                    if (data.responseJSON.errors && data.responseJSON.errors.password) {
                        $('#password').parent().addClass('has-danger');
                        $('.signUp_error_message').append('<p class="sub-ttl error-message">' + data.responseJSON.errors.password[0] + '</p>');
                    }

                    if (data.responseJSON.errors && data.responseJSON.errors.activation) {
                        $('.signUp_error_message').append('<p class="sub-ttl error-message">' + data.responseJSON.errors.activation + '</p>');
                    }

                    if (data.responseJSON.errors && data.responseJSON.errors.blocked) {
                        $('.signUp_error_message').append('<p class="sub-ttl error-message">' + data.responseJSON.errors.blocked + '</p>');
                    }

                    if (data.responseJSON.token !== undefined && data.responseJSON.step !== undefined) {
                        token = data.responseJSON.token;
                        var step = data.responseJSON.step;

                        var type = (data.responseJSON.type !== null)?'&type='+data.responseJSON.type:'';
                        //todo check registration step
                       var url = '/signup?step='+step+''+type+'&t='+token;

                       window.location.href = url;
                    }

                }
            }
        });
    });
</script>
