<script data-cfasync="false" src="{{ asset('assets2/js/main.js?ver=4') }}"></script>
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 
@if(Auth::user())

<script>
let user_id = '{{\Illuminate\Support\Facades\Auth::id()}}';
let user_name = '{{\Illuminate\Support\Facades\Auth::user()->name}}';
</script>

<script data-cfasync="false"  src="{{ asset('assets2/js/chat.js') }}"></script>
<script data-cfasync="false" src="{{ asset('assets2/js/header.js') }}"></script>
<script data-cfasync="false" src="{{ asset('chat-functions.js') }}"></script>
@endif
<header class="main-header">
    <div class="container-fluid">
        <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <i class="trav-bars"></i>
            </button>
            <a class="navbar-brand" href="{{url('home')}}">
                <img src="{{url('assets2/image/main-circle-logo.png')}}" alt="">
            </a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="header-search-block">
                    <div class="head-search-inner">
                        <div class="search-block">
                            <form  autocomplete="off">
                                <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                                <input type="text" class="" id="mainSearchInput" autocomplete="off"
                                       @if(isset($search_in))
                                       placeholder="Search {{$search_in->transsingle->title}}"
                                       @else
                                       placeholder="@lang('buttons.general.search_dots')"
                                       @endif
                                       >
                                       <a href="#" class="delete-search">
                                    <i class="trav-close-icon"></i>
                                </a>
                            </form>
                        </div>
                        <div class="search-box">
                            <ul class="nav nav-tabs search-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#places" role="tab" data-toggle="tab">places <span id="numPlacesResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#restaurants" role="tab" data-toggle="tab">restaurants <span id="numRestaurantsResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#hotels" role="tab" data-toggle="tab">hotels <span id="numHotelsResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#countries" role="tab" data-toggle="tab">countries <span id="numCountriesResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#cities" role="tab" data-toggle="tab">cities <span id="numCitiesResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#people" role="tab" data-toggle="tab">people <span id="numUsersResult"></span></a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active show" id="places" aria-expanded="true">
                                    <div class="drop-wrap" id="placesSearchResults">
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="restaurants">
                                    <div class="drop-wrap" id="restaurantsSearchResults">
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="hotels">
                                    <div class="drop-wrap" id="hotelsSearchResults">
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="countries">
                                    <div class="drop-wrap" id="countriesSearchResults">
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="cities">
                                    <div class="drop-wrap" id="citiesSearchResults">
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="people">
                                    <div class="drop-wrap" id="usersSearchResults">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <ul class="navbar-nav">
                    @if (auth()->user()->expert)
                        @php
                            /** @var \App\Services\Ranking\RankingService $rankingService */
                            $rankingService = app(\App\Services\Ranking\RankingService::class);
                            $progress = $rankingService->getProgressData(auth()->id());

                            $current_badge = $progress['current_badge'];
                            $next_badge = $progress['next_badge'];
                            $overall_progress = $progress['overall_progress'];
                        @endphp

                    <div class="exp-progress" style="margin-left: 30px;">
                        @if ($current_badge)
                            <img class="exp-icon" style="width: 25px" src="{{$current_badge->badge->url}}" alt="" />
                        @else
                            <span style="width: 25px;"></span>
                        @endif
                        <div class="exp-progress-bar">
                            <div class="progress">
                                <div class="progress-bar" style="width: {{$overall_progress}}%" role="progressbar" aria-valuenow="{{$overall_progress}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        @if ($next_badge)
                            <img class="exp-icon" style="width: 25px" src="{{$next_badge->url}}" alt="" />
                        @endif
                    </div>
                    @endif
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" onclick="toggleLastMessages()" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="trav-comment-dots-icon"></i>
                            @if(Auth::user())
                            <?php
                            $chat_unread_count = \App\Models\User\User::find(\Illuminate\Support\Facades\Auth::id())->unread_chat_conversations_count;
                            ?>

                            <div id="chat-unread-counter" class="counter"
                                 style="display: {{$chat_unread_count>0? 'block':'none'}};">{{$chat_unread_count}}</div>
                            @endif
                        </a>
                        <div id="header-last-messages"
                             class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-all-head">
                            <div class="drop-ttl">Messages</div>
                            <div class="drop-inner" style="height: 400px;overflow: auto;">
                                <div id="container-last-messages" class="conversation-inner bordered">

                                </div>
                            </div>
                            <div class="drop-foot">
                                <a href="{{url('chat')}}" class="see-all">See All Messages</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown" id='notificationsDropDown'>
                        <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="header-notifications">
                            <i class="trav-bell-icon"></i>
                            <div id="notifications-count-unseen" class="counter" style="display: none;"></div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-all-head"  aria-labelledby="header-notifications">
                            <div class="drop-ttl">@choice('dashboard.notification', 2)</div>
                            <div class="drop-inner" style="height: 400px;overflow: auto;">
                                <div class="conversation-inner bordered" id='notificationsRows'>


                                </div>
                            </div>
                            <div class="drop-foot">
                                <a href="#" class="see-all" data-toggle="modal" data-target="#seeAllModal">See All</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="trav-comment-question-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        @if(isset($my_plans) && count($my_plans))
                        <a class="nav-link plan-trip-icon" href="{{route('profile.plans')}}">
                            <i class="trav-trip-plan-loc-icon"></i>
                            <span>Plan trip</span>
                        </a>
                        @else
                        <a class="nav-link plan-trip-icon" href="{{url('trip/plan/0')}}">
                            <i class="trav-trip-plan-loc-icon"></i>
                            <span>Plan trip</span>
                        </a>
                        @endif
                    </li>
                    <li class="nav-item dropdown">
                        @if(Auth::user())
                        <a class="profile-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="{{Auth::user()->name}}">
                            <span>{{Auth::user()->name}}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-use-menu">
                            <div class="img-block">
                                @if(isset(Auth::user()->cover_photo))
                                <img class="ava-cover" src="{{Auth::user()->cover_photo}}" width="100%" alt="">
                                @else
                                <img class="ava-cover" src="{{asset('assets2/image/placeholders/pattern.png')}}" width="100%" alt="">
                                @endif
                                <div class="profile-details">
                                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" width="60" alt="">
                                    <div>
                                        <h4>{{Auth::user()->name}}</h4>
                                        <a href="{{url('profile')}}">View your profile</a>
                                    </div>
                                </div>
                            </div>
                            @if(Auth::user()->expert == 1 && Auth::user()->is_approved == 1)
                            <a class="dropdown-item" href="{{url('dashboard')}}">
                                <div class="drop-txt">
                                    <h5>Expert Dashboard</h5>
                                    <p>Insights & Affiliation</p>
                                </div>
                            </a>
                            @endif
                            <a class="dropdown-item" href="javascript:;">
                                <div class="drop-txt">
                                    <h5>Help Center</h5>
                                    <p>Learn how to use Travooo</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{url('settings')}}">
                                <div class="drop-txt pb-3">
                                    <h5>Settings</h5>
                                    <p>Account & Personal Settings</p>
                                </div>
                            </a>
                            <a class="dropdown-item log-out" href="{{url('user/logout')}}">
                                <div class="drop-txt">
                                    <h5>Log Out</h5>
                                </div>
                            </a>
                           @if(Auth::user()->is_approved !=1)
                            <a class="dropdown-item switch"  href="#" dir="auto" data-toggle="modal" data-target="{{((Auth::user()->type == 1 && Auth::user()->expert == 0) || (Auth::user()->type == 2 && Auth::user()->expert == 0 && Auth::user()->is_approved === 0))?'#becomeExpertPopup':'#underReviewPopup'}}">
                                <div class="drop-txt" dir="auto">
                                    <h4 dir="auto">Switch to <b>Expert Account</b> <span class="exp-icon" dir="auto">EXP</span></h4>
                                </div>
                            </a>
                            @endif
                            <!-- New element -->
                            <div class="aside-footer" dir="auto">
                                <ul class="aside-foot-menu" dir="auto">
                                    <li dir="auto"><a href="{{route('page.privacy_policy')}}" dir="auto">Privacy</a></li>
                                    <li dir="auto"><a href="{{route('page.terms_of_service')}}" dir="auto">Terms</a></li>
                                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Advertising</a></li>
                                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Cookies</a></li>
                                </ul>
                                <p class="copyright" dir="auto">Travooo © 2017 - {{date('Y')}}</p>
                            </div>
                        </div>
                        @endif
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
@include('site.profile.partials._becomeExpertPopup')
