<script data-cfasync="false" defer src="{{ asset('assets2/js/main.js?ver='.time()) }}"></script>
@if(Auth::user())
    <script data-cfasync="false"  type="text/javascript">
        let user_id = '{{\Illuminate\Support\Facades\Auth::id()}}';
        let user_name = '{{\Illuminate\Support\Facades\Auth::user()->name}}';
        let profileFollowText = '{{ __('profile.follow') }}';
        let profileUnfollowText = '{{ __('profile.unfollow') }}';
        let baseUrl = window.location.protocol + "//" + window.location.host;
    </script>

    <script data-cfasync="false" src="{{ asset('assets2/js/chat.js') }}"></script>
    <script data-cfasync="false" src="{{ asset('assets2/js/header.js') }}"></script>
    <script defer src="{{ asset('js/chat-functions.js') }}"></script>

    <style>
        #container-last-messages .conv-block .main-blk .avatar {
            display: inline-block;
            border-radius: 50%;
            overflow: hidden;
            width: 50px;
        }

        #container-last-messages .conv-block .main-blk .avatar:not(:first-child) {
            margin-left: -35px;
            -webkit-mask:radial-gradient(circle 25px at 5px 50%,transparent 99%,#fff 100%);
                mask:radial-gradient(circle 55px at 5px 50%,transparent 99%,#fff 100%);
        }

        #container-last-messages .conv-block .main-blk .avatar img {
            width: 100%;
            display: block;
            height: 100%;
        }
    </style>
@endif
<header class="main-header ">
    <!-- Mobile element END -->
    <div class="container-fluid">
        <nav class="navbar navbar-toggleable-md mobile--display navbar-light bg-faded">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{url('assets2/image/main-circle-logo.png')}}" alt="">
            </a>
            <div class="collapse navbar-collapse always--show" id="navbarSupportedContent">
                <div class="header-search-block">
                    <div class="head-search-inner">
                        <div class="search-block">
                            <a class="search-btn" href="#" data-search-btn><i class="trav-search-icon"></i></a>
                            <input type="text" class="" id="mainSearchInput" autocomplete="off"
                                   @if(isset($search_in))
                                   placeholder="Search {{$search_in->transsingle->title}}"
                                   @else
                                   placeholder="@lang('buttons.general.search_dots')"
                                    @endif
                            >
                            <a href="#" class="delete-search">
                                <i class="trav-close-icon"></i>
                            </a>
                        </div>
                        <div class="search-box">
                            <ul class="nav nav-tabs search-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#all" role="tab"
                                       data-toggle="tab">Places <span
                                                id="numAllResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#countries" role="tab"
                                       data-toggle="tab">@lang('header.countries') <span
                                                id="numCountriesResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#cities" role="tab"
                                       data-toggle="tab">@lang('header.cities') <span
                                                id="numCitiesResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#people" role="tab"
                                       data-toggle="tab">@lang('header.people') <span
                                                id="numUsersResult"></span></a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active show" id="all"
                                     aria-expanded="true">
                                    <div class="drop-wrap" id="allSearchResults">
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="countries">
                                    <div class="drop-wrap" id="countriesSearchResults">
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="cities">
                                    <div class="drop-wrap" id="citiesSearchResults">
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="people">
                                    <div class="drop-wrap" id="usersSearchResults">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tabpanel new-place-box">
                            <div class="drop-wrap">
                                <div class="drop-row">
                                    <div class="new-location-icon">
                                        <img src="{{asset('assets2/image/location-icon.svg')}}" alt="">
                                    </div>
                                    <div class="drop-content place-content new-place-content">
                                        <span>You can find what you are looking for?</span>
                                        <span>Add a missing place to Travooo</span>
                                    </div>
                                    <div>
                                        <img src="{{asset('assets2/image/arrow-right.svg')}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="navbar-nav custom-header">
                    @if(Auth::user())
                        @if (auth()->user()->expert)
                            @php
                                /** @var \App\Services\Ranking\RankingService $rankingService */
                                $rankingService = app(\App\Services\Ranking\RankingService::class);
                                $progress = $rankingService->getProgressData(auth()->id());

                                $current_badge = $progress['current_badge'];
                                $next_badge = $progress['next_badge'];
                                $overall_progress = $progress['overall_progress'];
                            @endphp

                            <div class="exp-progress">
                                @if ($current_badge)
                                    <img class="exp-icon" style="width: 25px" src="{{$current_badge->badge->url}}" alt="" />
                                @else
                                    <span style="width: 25px;"></span>
                                @endif
                                <div class="exp-progress-bar">
                                    <div class="progress">
                                        <div class="progress-bar" style="width: {{$overall_progress}}%" role="progressbar" aria-valuenow="{{$overall_progress}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                @if ($next_badge)
                                    <img class="exp-icon" style="width: 25px" src="{{$next_badge->url}}" alt="" />
                                @endif
                            </div>
                        @endif
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" onclick="toggleLastMessages()" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                <i class="trav-comment-dots-icon"></i>
                                @if(Auth::user())
                                <?php
                                $chat_unread_count = \App\Models\User\User::find(\Illuminate\Support\Facades\Auth::id())->unread_chat_conversations_count;
    
                                ?>
    
                                <div id="chat-unread-counter" class="counter"
                                     style="display: {{$chat_unread_count>0? 'block':'none'}};">{{$chat_unread_count}}</div>
                                @endif
                            </a>
                            <div id="header-last-messages"
                                 class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-all-head">
                                <div class="drop-ttl">Messages</div>
                                <div class="drop-inner" style="height: 400px;overflow: auto;">
                                    <div id="container-last-messages" class="conversation-inner bordered">
    
                                    </div>
                                </div>
                                <div class="drop-foot">
                                    <a href="{{url('chat')}}" class="see-all">See All Messages</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown" id='notificationsDropDown'>
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                               id="header-notifications">
                                <i class="trav-bell-icon"></i>
                                <div id="notifications-count-unseen" class="counter" style="display: none;"></div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-all-head"
                                 aria-labelledby="header-notifications">
                                <div class="drop-ttl">@choice('dashboard.notification', 2)</div>
                                <div class="drop-inner" style="height: 400px;overflow: auto;">
                                    <div class="conversation-inner bordered" id='notificationsRows'>


                                    </div>
                                </div>
                                <div class="drop-foot">
                                    <a href="#" class="see-all" data-toggle="modal"
                                       data-target="#seeAllModal">@lang('buttons.general.see_all')</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/copyright-infringement">
                                <i class="trav-comment-question-icon"></i>
                            </a>
                        </li>
                    @endif
                    <li class="nav-item">
                        @if(isset($my_plans) && count($my_plans))
                            <a class="nav-link plan-trip-icon" href="{{route('profile.plans')}}">
                                <i class="trav-trip-plan-loc-icon"></i>
                                <span>@lang('trip.plan_trip')</span>
                            </a>
                        @else
                            <a class="nav-link plan-trip-icon" href="{{route('trip.plan', 0)}}">
                                <i class="trav-trip-plan-loc-icon"></i>
                                <span>@lang('trip.plan_trip')</span>
                            </a>
                        @endif
                    </li>
                    <li class="nav-item dropdown">
                        @if(Auth::user())
                        <a class="profile-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="{{Auth::user()->name}}">
                            <span>{{Auth::user()->name}}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-use-menu">
                            <div class="img-block">
                                @if(isset(Auth::user()->cover_photo))
                                <img class="ava-cover" src="{{Auth::user()->cover_photo}}" width="100%" alt="">
                                @else
                                <img class="ava-cover" src="{{asset('assets2/image/placeholders/pattern.png')}}" width="100%" alt="">
                                @endif
                                <div class="profile-details">
                                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" width="60" alt="">
                                    <div>
                                        <h4>{{Auth::user()->name}}</h4>
                                        <a href="{{url('profile')}}">View your profile</a>
                                    </div>
                                </div>
                            </div>
                            @if(Auth::user()->expert == 1 && Auth::user()->is_approved == 1)
                            <a class="dropdown-item" href="{{url('dashboard')}}">
                                <div class="drop-txt">
                                    <h5>Expert Dashboard</h5>
                                    <p>Insights & Affiliation</p>
                                </div>
                            </a>
                            @endif
                            <a class="dropdown-item" href="{{route('help.index')}}">
                                <div class="drop-txt">
                                    <h5>Help Center</h5>
                                    <p>Learn how to use Travooo</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{url('settings')}}">
                                <div class="drop-txt pb-3">
                                    <h5>Settings</h5>
                                    <p>Account & Personal Settings</p>
                                </div>
                            </a>
                            <a class="dropdown-item log-out" href="{{url('user/logout')}}">
                                <div class="drop-txt">
                                    <h5>Log Out</h5>
                                </div>
                            </a>
                            @if(Auth::user()->is_approved !=1)
                            <a class="dropdown-item switch" href="#" dir="auto" data-toggle="modal" data-target="{{((Auth::user()->type == 1 && Auth::user()->expert == 0) || (Auth::user()->type == 2 && Auth::user()->expert == 0 && Auth::user()->is_approved === 0))?'#becomeExpertPopup':'#underReviewPopup'}}">
                                <div class="drop-txt" dir="auto">
                                    <h4 dir="auto">Switch to <b>Expert Account</b> <span class="exp-icon" dir="auto">EXP</span></h4>
                                </div>
                            </a>
                            @endif
                            <!-- New element -->
                            <div class="aside-footer" dir="auto">
                                <ul class="aside-foot-menu" dir="auto">
                                    <li dir="auto"><a href="{{route('page.privacy_policy')}}" dir="auto">Privacy</a></li>
                                    <li dir="auto"><a href="{{route('page.terms_of_service')}}" dir="auto">Terms</a></li>
                                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Advertising</a></li>
                                    <li dir="auto"><a href="{{url('/')}}" dir="auto">Cookies</a></li>
                                </ul>
                                <p class="copyright" dir="auto">Travooo © 2017 - {{date('Y')}}</p>
                            </div>
                        </div>
                        @endif
                    </li>
                </ul>
            </div>
        </nav>
        <!-- Mobile element -->
        <div class="home-mobile-top-actions ease-in-trans">
            @if (strpos(url()->current(), 'profile'))
                <a href="{{url()->previous()}}" class="mobile-back-button"><i class="fas fa-angle-left"></i></a>
            @else
                <button type="button" class="mobile-user-actions-button"><i class="fas fa-plus"></i></button>
            @endif

            <a href="/" class="travooo">Travooo</a>
            <button type="button" class="search-btn"><i class="trav-search-icon"></i></button>
            </div>
            <div class="home-mobile-header">
                <li class="dropdown">
                    @if(Auth::user())
                    <a href="#" class="user-photo profile-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mobile--profile dropdown-arrow dropdown-use-menu">
                        <div class="img-block">
                            @if(isset(Auth::user()->cover_photo))
                            <img class="ava-cover" src="{{Auth::user()->cover_photo}}" width="100%" alt="">
                            @else
                            <img class="ava-cover" src="{{asset('assets2/image/placeholders/pattern.png')}}" width="100%" alt="">
                            @endif
                            <div class="profile-details">
                                <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" width="60" alt="">
                                <div>
                                    <h4>{{Auth::user()->name}}</h4>
                                    <a href="{{url('profile')}}">View your profile</a>
                                </div>
                            </div>
                        </div>
                        @if(Auth::user()->expert == 1 && Auth::user()->is_approved == 1)
                        <a class="dropdown-item" href="{{url('dashboard')}}">
                            <div class="drop-txt">
                                <h5>Expert Dashboard</h5>
                                <p>Insights & Affiliation</p>
                            </div>
                        </a>
                        @endif
                        <a class="dropdown-item" href="javascript:;">
                            <div class="drop-txt">
                                <h5>Help Center</h5>
                                <p>Learn how to use Travooo</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="{{url('settings')}}">
                            <div class="drop-txt">
                                <h5>Settings</h5>
                                <p>Account & Personal Settings</p>
                            </div>
                        </a>
                        <a class="dropdown-item log-out" href="{{url('user/logout')}}">
                            <div class="drop-txt border--text">
                                <h5>Log Out</h5>
                            </div>
                        </a>
                     
                        @if(Auth::user()->is_approved !=1)
                        <a class="dropdown-item switch" href="#" dir="auto" data-toggle="modal" data-target="{{((Auth::user()->type == 1 && Auth::user()->expert == 0) || (Auth::user()->type == 2 && Auth::user()->expert == 0 && Auth::user()->is_approved === 0))?'#becomeExpertPopup':'#underReviewPopup'}}">
                            <div class="drop-txt" dir="auto">
                                <h4 dir="auto">Switch to <b>Expert Account</b> <span class="exp-icon" dir="auto">EXP</span></h4>
                            </div>
                        </a>
                        @endif
                       
                        <!-- New element -->
                        <div class="aside-footer" dir="auto">
                            <ul class="aside-foot-menu" dir="auto">
                                <li dir="auto"><a href="{{route('page.privacy_policy')}}" dir="auto">Privacy</a></li>
                                <li dir="auto"><a href="{{route('page.terms_of_service')}}" dir="auto">Terms</a></li>
                                <li dir="auto"><a href="{{url('/')}}" dir="auto">Advertising</a></li>
                                <li dir="auto"><a href="{{url('/')}}" dir="auto">Cookies</a></li>
                            </ul>
                            <p class="copyright" dir="auto">Travooo © 2017 - {{date('Y')}}</p>
                        </div>
                    </div>
                    @endif
                </li>
           
            <ul class="home-mobile-navbar">
                <li>
                    <a class='mobile-user-notifications-button'><img src="{{asset('assets2/image/notification-placeholder.png')}}"width="44" alt=""></a>
                    <span class="counter">0</span>
                </li>
                <li>
                    <a href="{{url('home')}}"><img src="{{asset('assets2/image/left-menu-icon-home.png')}}" width="49" alt=""></a>
                </li>
                <li>
                    <a href="{{ route('globaltrip.index') }}"><img src="{{asset('assets2/image/left-menu-icon-plans.png')}}" width="43" alt=""></a>
                </li>
                <li>
                    <a href="{{route('discussion.index')}}"><img src="{{asset('assets2/image/left-menu-icon-ask.png')}}" width="46" alt=""></a>
                </li>
                <li>
                    <a href="{{url('reports/list')}}"><img src="{{asset('assets2/image/left-menu-icon-travelogs.png')}}"width="46" alt=""></a>
                </li>  
            </ul>
            <!-- <button type="button" class="more-items dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-h"></i>
            </button> -->
            <!-- <div class="dropdown-menu">
                <ul class="home-mobile-navbar">
                    <li>
                        <a href="https://travooo.com/home"><img src="{{asset('assets2/image/left-menu-icon-home.png')}}" width="49" alt=""></a>
                    </li>
                    <li>
                        <a href="https://travooo.com/profile-map"><img src="{{asset('assets2/image/left-menu-icon-map.png')}}"width="44" alt=""></a>
                    </li>
                    <li>
                        <a href="https://travooo.com/reports/list?order=newest&amp;"><img src="{{asset('assets2/image/left-menu-icon-travelogs.png')}}"width="46" alt=""></a>
                    </li>
                    <li>
                        <a href="https://travooo.com/travelmates-newest"><img src="{{asset('assets2/image/left-menu-icon-mates.png')}}" width="47" alt=""></a>
                    </li>
                    <li>
                        <a href="https://travooo.com/trip-plans"><img src="{{asset('assets2/image/left-menu-icon-plans.png')}}" width="43" alt=""></a>
                    </li>
                    <li>
                        <a href="https://travooo.com/discussions"><img src="{{asset('assets2/image/left-menu-icon-ask.png')}}" width="46" alt=""></a>
                    </li>
                </ul>
            </div> -->
            </div>
        </div>
</header>
<script>
    $('.home-mobile-top-actions .search-btn, [data-search-btn]').on('click', function(){
        $('.modal').modal('hide');
        $('.mobile-user-actions-popup').removeClass('show bottom slide-up');
        setTimeout(() => {
            showMobileHeaderSearch();
        }, 100);
    })
    function showMobileHeaderSearch() {
        $('.navbar .header-search-block').addClass('show');
        $('#mainSearchInput').focus();
    }
</script>

<script>
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
});

function hasScrolled() {
    var st = $(this).scrollTop();
    
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('.home-mobile-header').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('.home-mobile-header').removeClass('nav-up').addClass('nav-down');
        }
    }   
    lastScrollTop = st;
}
</script>
@include('site.profile.partials._becomeExpertPopup')
@include('site.design.partials._mobileUserActionsPopup')
@include('site.design.partials._mobileUserNotificationsPopup')
