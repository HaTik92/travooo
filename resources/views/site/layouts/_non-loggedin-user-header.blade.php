<script data-cfasync="false" defer src="{{ asset('assets2/js/main.js?ver='.time()) }}"></script>
<link rel="stylesheet" href="{{asset('assets2/css/sign-up.css?v='.time())}}">
<header class="main-header position-fixed">
    <div class="non-logged-header-block">
      <a href="{{url_with_locale('home')}}" class="navbar-brand"><img src="{{asset('frontend_assets/image/main-logo.png')}}" alt=""></a>
      <div class="non-logged-action-block">
            <a href="{{url('/login')}}" class="btn non-logged-btn login-btn">Log In</a>
            <a href="{{url('/signup?dt=regular')}}" class="btn non-logged-btn signup-btn">Sign Up</a>
      </div>
    </div>

</header>

