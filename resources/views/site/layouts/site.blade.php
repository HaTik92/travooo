<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>{{ $title ?? config('app.name') }}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 
    @yield('before_styles')

    @section('styles')
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    @show

    <style>
        .pagination > li > a,
            .pagination > li > span {
                color: #4080ff;
            }

            .pagination > .active > a,
            .pagination > .active > a:focus,
            .pagination > .active > a:hover,
            .pagination > .active > span,
            .pagination > .active > span:focus,
            .pagination > .active > span:hover {
                background-color: #4080ff;
                border-color: #4080ff;
            }
            .conversation-inner .conv-block .conv-txt .name {
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .emoji .fa .fa-heart {
                color: #a3a3a3;
            }
            .emoji .fa .fa-heart red {
                color: red;
            }
    </style>
    @yield('before_site_style')

    @section('site_style')
        <link rel="stylesheet" href="{{asset('assets2/css/style-15102019-9.css?v='.time())}}">
        <link rel="stylesheet" href="{{asset('assets2/css/travooo.css?0.6.8'.time())}}">

        <link href="{{asset('/dist-parcel/assets/Header/main.css')}}" rel="stylesheet" type="text/css" />
    @show

    @yield('after_styles')
    <script data-cfasync="false" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>
<body @if(session('lang-rtl')) class="rtl-direction @if(!Auth::check()) non-loggedIn @endif" @endif class="@if(!Auth::check())non-loggedIn @endif" style="overflow-y:scroll">
<div class="main-wrapper @if(Request::is('home')) newsfeed-page @endif" id="app">
    @section('base')
    @if(Auth::check())
        @include('site/layouts/_header')
    @else
        @include('site/layouts/_non-loggedin-user-header')
    @endif
        <div class="content-wrap {{!Auth::check()?'pb-90':''}}">
            <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
                <i class="trav-cog"></i>
            </button>
            <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
                <i class="trav-bars">
                    <span>Menu</span>
                </i>
            </button>

            
            @if(!isset($withContainer))
                @php $withContainer = true; @endphp
            @endif

            <div class="@if($withContainer) container-fluid @endif @if(Request::is('home')) mobile-fullwidth @endif">

                @yield('content-area')

            </div>
        </div>
    @if(!Auth::check())
       @include('site/layouts/_non-loggedin-user-footer')        
    @endif
    @show
</div>

@yield('before_scripts')

@section('scripts')
    <script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script data-cfasync="false" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
    <script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
    <script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
    <script data-cfasync="false" src="{{asset('/assets2/js/jquery.lazy.min.js')}}"></script>
    <script data-cfasync="false" src="{{asset('/assets2/js/jquery.lazy.ajax.min.js')}}"></script>
@show

@yield('before_site_script')

@section('site_script')
    <script data-cfasync="false" src="{{asset('/assets2/js/script.js?v='.time())}}"></script>
@show

@yield('after_scripts')
@include('site.layouts._footer-scripts')
</body>
<script data-cfasync="false">
    lazyloader();
    
    function lazyloader(){
        $('.lazy').lazy();
    }
    $(document).on('DOMNodeInserted', 'img.lazy', function() {
        $(this).lazy();
    });

    $("*").not("script").not("style").not("link").attr("dir", "auto");
    $("*").not("script").not("style").not("link").each(function(){
        if(/[\u0600-\u06FF]/.test($(this).text()) && !/</.test($(this).html()))
            $(this).attr("dir", "");
    });
    $(".ql-direction-rtl").attr("dir", "rtl");
    $('body').on('DOMNodeInserted', function(e) {
        $(e.target).find("*").attr('dir', "auto");
        $(e.target).find("*").not("script").not("style").not("link").each(function(){
            if(/[\u0600-\u06FF]/.test($(this).text()) && !/</.test($(this).html()))
                $(this).attr("dir", "");
        });
    });
</script>
</html>
