<!-- left outside menu -->
<div class="left-outside-menu-wrap" id="leftOutsideMenu">
    <div class='leftOutsideMenuClose'></div>
    <ul class="left-outside-menu">
        <li class="{{(Route::currentRouteName() == 'home')?'active':''}}">
            <a href="{{url('home')}}">
                <i class="trav-home-icon"></i>
                <span>@lang('navs.general.home')</span>
                <!--<span class="counter">5</span>-->
            </a>
        </li>
        <li>
            <a href="{{url('travelmates-newest')}}">
                <i class="trav-travel-mates-icon"></i>
                <span>@choice('travelmate.travel_mate', 2)</span>
            </a>
        </li>
        <li>
            <a href="{{url('plans')}}">
                <i class="trav-trip-plans-p-icon"></i>
                <span>Trip Plans</span>
            </a>
        </li>
        <li>
            <a href="{{url('trending')}}">
                <i class="trav-trending-icon"></i>
                <span>@lang('navs.frontend.trending')</span>
            </a>
        </li>
        <li class="{{(Route::currentRouteName() == 'discussion.index')?'active':''}}">
            <a href="{{url('discussions')}}">
                <i class="trav-ask-icon"></i>
                <span>Ask</span>
            </a>
        </li>
        <!-- <li><a href="{{url('profile-map')}}">
                <i class="trav-map-o"></i>
                <span>Map</span>
            </a>
        </li> -->
        <li class="{{(Route::currentRouteName() == 'book.flight' || Route::currentRouteName() == 'book.search_flight')?'active':''}}">
            <a href="{{url('book/flight')}}">
                <i class="trav-angle-plane-icon"></i>
                <span>Flights</span>
            </a>
        </li>
        <li class="{{(Route::currentRouteName() == 'book.hotel' || Route::currentRouteName() == 'book.search_hotel')?'active':''}}">
            <a href="{{url('book/hotel')}}">
                <i class="trav-building-icon"></i>
                <span>Hotels</span>
            </a>
        </li>
        <li><a href="{{url('reports/list')}}">
               <i class="fa-2x fa fa-newspaper-o" aria-hidden="true"></i>
                <span>Reports</span>
            </a>
        </li>
    </ul>
</div>