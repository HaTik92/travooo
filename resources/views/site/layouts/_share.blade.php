<div class="share-page-block">
    <div class="share-page-inner">
        <div class="share-txt">@lang('strings.frontend.share_this_page')</div>
        <ul class="share-list">
            <li>
                <a href="#"><i class="fa fa-facebook"></i></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-twitter"></i></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-pinterest-p"></i></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-code"></i></a>
            </li>
        </ul>
    </div>
</div>