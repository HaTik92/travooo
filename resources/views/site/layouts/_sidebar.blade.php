<div class="aside-footer">
    <ul class="aside-foot-menu">
        <li><a href="{{route('page.privacy_policy')}}">@lang('navs.frontend.privacy')</a></li>
        <li><a href="{{route('page.terms_of_service')}}">@lang('navs.frontend.terms')</a></li>
        <li><a href="#">@lang('navs.frontend.advertising')</a></li>
        <li><a href="#">@lang('navs.frontend.cookies')</a></li>
        <li><a href="#">@lang('navs.frontend.more')</a></li>
        <li><a href="/copyright-infringement">@lang('navs.frontend.copyright')</a></li>
        <li><a href="{{route('help.index')}}">Help Center</a></li>
    </ul>
    <p class="copyright">Travooo © 2017 - {{date('Y')}}</p>
</div>
