<script data-cfasync="false" type="text/javascript">
$(document).ready(function () {
    if($("#place-map-check").length != 0) {
        mapInit();
    }
});
    $('body').on('click', '.logout-link', function(){
       localStorage.setItem('logout-event', 'logout' + Math.random());
    });
    
window.addEventListener('storage', function(event){
    if (event.key == 'logout-event') { 
       window.location.href="{{url('user/logout')}}";
    }
});
    function delay(callback, ms) {
        var timer = 0;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };

    }

    window.locationLat = false;
    window.locationLng = false;

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            document.getElementById('locationMsg').innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    getLocation();

    function showPosition(position) {
        console.log('lat');
        console.log(position.coords.latitude);
        window.locationLat = position.coords.latitude;
        window.locationLng = position.coords.longitude;
        document.getElementById('post_actual_location').value = position.coords.latitude + ',' + position.coords.longitude;
    }

    function showError(error) {
        if(document.getElementById('locationMsg') == null) return;
        switch (error.code) {
            case error.PERMISSION_DENIED:
                document.getElementById('locationMsg').innerHTML = "We are using your location information to give you a better experience showing most accurate & nearby results. Please note that some interesting functionality - like showing Places of interests and updates according to their location from you - will not be working as expected until you allow Travooo to access your location. \n\
(<a href='https://www.lifewire.com/denying-access-to-your-location-4027789' target='_blank'>Know how here</a>) " + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                document.getElementById('locationMsg').style.display = 'block';
                break;
            case error.POSITION_UNAVAILABLE:
                document.getElementById('locationMsg').innerHTML = "Location information is unavailable."
                document.getElementById('locationMsg').style.display = 'block';
                break;
            case error.TIMEOUT:
                document.getElementById('locationMsg').innerHTML = "The request to get user location timed out."
                document.getElementById('locationMsg').style.display = 'block';
                break;
            case error.UNKNOWN_ERROR:
                document.getElementById('locationMsg').innerHTML = "An unknown error occurred."
                document.getElementById('locationMsg').style.display = 'block';
                break;
        }
    }


    $('.close-icon').click(function(e){
        $("#createNewPlace").modal('hide');
    })

    // show popup for add new place
    $(".new-place-box").click(function(e) {
        $(e.target).parents('.header-search-block').removeClass('show').find('input').val(null);
        $("#createNewPlace").modal('show');
        window.myMap.resize();
    });

    function mapInit() {
        var bounds = new mapboxgl.LngLatBounds();

        mapboxgl.accessToken = '{{config('mapbox.token')}}';

        myMap = new mapboxgl.Map({
            container: 'place-map-check',
            style: 'mapbox://styles/mapbox/satellite-streets-v11',
            center: [0, 0],
            zoom: 2
        });

        geocoder = new MapboxGeocoder({
                                    accessToken: mapboxgl.accessToken,
                                    reverseGeocode: true,
                                    mapboxgl
                                });
        myMap.addControl(geocoder);

        var marker = new mapboxgl.Marker({
                    draggable: true,
                    color: "red"
                    })
                    .setLngLat([0, 0])
                    .addTo(myMap);
        
        function onDragEnd() {
            var lngLat = marker.getLngLat();
            console.log('Longitude: ' + lngLat.lng + 'Latitude: ' + lngLat.lat)
            newPosition = [lngLat.lng, lngLat.lat];
            geocoder.geocoderService.reverseGeocode({
                query: [lngLat.lng, lngLat.lat],
                mode: "mapbox.places" 
            })
            .send()
            .then(response => {
                // GeoJSON document with geocoding matches
                const match = response.body;
                $("#place_address").val(match.features[0].place_name)
                console.log(match);
                // $("#place_category").val(place.info.category)
                placeCountry = match.features[0].context.filter(ctx => ctx.id.includes("country"))[0].text;
                placeState = match.features[0].context.filter(ctx => ctx.id.includes("region"))[0];
                placeState = placeState ? placeState.text : undefined;
                let placeCity = match.features[0].context.filter(ctx => ctx.id.includes("place."))[0];
                placeCity = placeCity ? placeCity.text : match.features[0].place_name.split(", ")[0];
                isoCode = match.features[0].context.filter(ctx => ctx.id.includes("country"))[0].short_code.toUpperCase();
                $.ajax({
                    type: 'get',
                    url: `http://api.geonames.org/search?name=${placeCity}&country=${isoCode}&type=json&username=routi`,
                    success: function (response) {
                        $("#place_city").val(response.geonames[0].name);     
                    }
                });
            });
        }
        
        marker.on('dragend', onDragEnd);

        myMap.on('move', function (e) {
            console.log(`Current Map Center: ${myMap.getCenter()}`);
            marker.setLngLat(myMap.getCenter());
        });

        myMap.on('style.load', function() {
            myMap.on('click', function(e) {
                var lngLat = e.lngLat;
                console.log('Longitude: ' + lngLat.lng + 'Latitude: ' + lngLat.lat)
                // geocoder.geocoderService.reverseGeocode({
                //     query: [lngLat.lng, lngLat.lat],
                //     mode: "mapbox.places" 
                // })
                // .send()
                // .then(response => {
                //     // GeoJSON document with geocoding matches
                //     const match = response.body;
                //     console.log(match);
                // });
            });
        });

        let placeAddress = document.getElementById("place_address");
        let placeCity = document.getElementById("place_city");
        let placeName = document.getElementById("place_name");
        function getSearchedPlace() {
            let placeAddressVal = placeAddress.value;
            let placeCityVal = placeCity.value;
            let placeNameVal = placeName.value;
            if(placeAddressVal && placeCityVal && placeNameVal) {
                let value = `${placeNameVal} ${placeAddressVal} ${placeCityVal}`;
                setTimeout(() => {
                    $.ajax({
                        type: "get",
                        url: `https://api.mapbox.com/geocoding/v5/mapbox.places/${value}.json?access_token=${mapboxgl.accessToken}`,
                        success: function (response) {
                            console.log(response)
                            if(response.features.length >= 1){
                                newPosition = response.features[0].center
                                myMap.flyTo({
                                    center: response.features[0].center,
                                    bearing: 0,
                                    speed: 0.7,
                                    zoom: 16
                                })
                            }
                        }
                    });
                }, 2000);
            }
        }
        placeAddress.addEventListener('keyup', getSearchedPlace)
        // placeCity.addEventListener('keyup', getSearchedPlace)
        // placeName.addEventListener('keyup', getSearchedPlace)

        let markers = [];
        myMap.on('sourcedata', function(e) {
            if (e.isSourceLoaded) {
                if(myMap.getZoom() >= 16) {
                    if(markers.length) {
                        markers.forEach(elem => {
                            elem.marker.remove();
                        });
                        markers.length = 0
                    }
                    let radius = 1000;
                    let limit = 50;
                    $.ajax({
                        type: "get",
                        url: `https://api.mapbox.com/v4/mapbox.mapbox-streets-v8/tilequery/${myMap.getCenter().lng},${myMap.getCenter().lat}.json?radius=${radius}&limit=${limit}&geometry=point&access_token=${mapboxgl.accessToken}&language=en&matching_text=en&matching_place_name=en`,
                        success: function (response) {
                            console.log(response);
                            response.features.forEach((feauture, id) => {
                                let poiName = feauture.properties.name;
                                let category = feauture.properties.category_en;
                                // console.log(feauture);
                                if(poiName) {
                                    let info = (poiName ? `Name: ${poiName.replace(/\"/g, '')}` : ' Name: Empty') + '<br>' + (category ? `Category: ${category}` : ' Category: Empty')
                                    var popup = new mapboxgl.Popup({ offset: 25 }).setHTML(info);
                                    var el = document.createElement('div');
                                    el.className = 'marker';
                                    el.setAttribute('data-value', id)
                                    let marker = new mapboxgl.Marker(el)
                                        .setLngLat(feauture.geometry.coordinates)
                                        .setPopup(popup)
                                        .addTo(myMap)
                                    marker.getElement().addEventListener('click', function(e){
                                        let id = Number(e.target.getAttribute('data-value'));
                                        console.log(id)  
                                        let place = markers.filter(elem => elem.id === id)[0]
                                        console.log(place)
                                        newPosition = place.info.coordinates;
                                        geocoder.geocoderService.reverseGeocode({
                                            query: place.info.coordinates,
                                            mode: "mapbox.places" 
                                        })
                                        .send()
                                        .then(response => {
                                            // GeoJSON document with geocoding matches
                                            const match = response.body;
                                            $("#place_name").val(place.info.poiName)
                                            $("#place_address").val(match.features[0].place_name)
                                            console.log(match);
                                            // $("#place_category").val(place.info.category)
                                            placeCountry = match.features[0].context.filter(ctx => ctx.id.includes("country"))[0].text;
                                            placeState = match.features[0].context.filter(ctx => ctx.id.includes("region"))[0];
                                            placeState = placeState ? placeState.text : undefined;
                                            let placeCity = match.features[0].context.filter(ctx => ctx.id.includes("place."))[0];
                                            placeCity = placeCity ? placeCity.text : match.features[0].place_name.split(", ")[0];
                                            isoCode = match.features[0].context.filter(ctx => ctx.id.includes("country"))[0].short_code.toUpperCase();
                                            $.ajax({
                                                type: 'get',
                                                url: `http://api.geonames.org/search?name=${placeCity}&country=${isoCode}&type=json&username=routi`,
                                                success: function (response) {
                                                    $("#place_city").val(response.geonames[0].name);     
                                                }
                                            });
                                        });
                                    })
                                    markers.push({
                                        id,
                                        marker,
                                        info: {
                                            poiName,
                                            category,
                                            coordinates: feauture.geometry.coordinates
                                        }
                                    });
                                }
                            });                       
                        }
                    });
                }
            }
        });

        function createSubmission() {
            let address = document.getElementById('place_address').value;
            let city = document.getElementById('place_city').value;
            let name = document.getElementById('place_name').value;
            let category = document.getElementById('place_category').value;
            if(address && city && name && category) {
                $.ajax({
                    type: "POST",
                    url: '{{route('submissions.create')}}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        placeAddress: document.getElementById('place_address').value,
                        placeCity: document.getElementById('place_city').value,
                        placeName: document.getElementById('place_name').value,
                        placeCategory: document.getElementById('place_category').value,
                        placeCountry,
                        isoCode,
                        newPosition,
                        placeState
                    },
                    success: function (response) {
                        console.log(response)
                        if(response == 1) $("#createNewPlace").modal('hide');
                        $("#place_address").val(' ')
                        $("#place_city").val(' ')
                        $("#place_name").val(' ')
                        markers.forEach(elem => {
                            elem.marker.remove();
                        });
                    }
                });
            }
        }
        document.getElementById('create-submission').addEventListener('click', createSubmission)
    }

    // message input dropdown
    $('#mainSearchInput').keyup(delay(function (e) {
        let value = $(e.target).val();
        var search_in = false;
        var search_id = false;
        var active_key = false;
        @if(isset($country))
            search_in = 'countries';
            search_id = {{@$country->id}};
        @elseif(isset($city))
            search_in = 'cities';
            search_id = {{@$city->id}};
        @elseif(isset($place))
            search_in = 'place';
            search_id = {{ @$place->id }};
            active_key = $(".location-identity").hasClass("active");
        @endif
        if (value.length >= 3) {
            if ( active_key === false ) {
                $(this).parents('.header-search-block').addClass('show');
                $(".location-identity").hide();
                $(".header-search-block .search-box").html(`<ul class="nav nav-tabs search-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#all" role="tab" data-toggle="tab">Everything <span id="numAllResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#countries" role="tab" data-toggle="tab">@lang('header.countries') <span id="numCountriesResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#cities" role="tab" data-toggle="tab">@lang('header.cities') <span id="numCitiesResult"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#people" role="tab" data-toggle="tab">@lang('header.people') <span id="numUsersResult"></span></a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active show" id="all" aria-expanded="true">
                                    <div class="drop-wrap" id="allSearchResults"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="countries">
                                    <div class="drop-wrap" id="countriesSearchResults"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="cities">
                                    <div class="drop-wrap" id="citiesSearchResults"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="people">
                                    <div class="drop-wrap" id="usersSearchResults"></div>
                                </div>
                            </div>`);
                // start searching all POI types
                $.ajax({
                    method: "GET",
                    url: "{{url('home/searchAllPOIs')}}",
                    data: {
                        q: $('#mainSearchInput').val(), lat: window.locationLat, lng: window.locationLng,
                        search_in: search_in, search_id: search_id
                    }
                })
                    .done(function (res) {
                        res = JSON.parse(res);
                        console.log(res);
                        var num_places_result = res.length;
                        if(num_places_result==20) {
                            $('#numAllResult').text(num_places_result+'+');
                        }
                        else {
                            $('#numAllResult').text(num_places_result);
                        }
                        $('#allSearchResults').empty();
                        res.forEach(function (element) {
                            //console.log(element);
                            if (!element.image) {
                                element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                            } else {
                                element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.image;
                            }
                            if(!element.is_new){

                                if(element.place_type=="country") {
                                    var element_id  = element.id;
                                    element_id  = element_id.split("_").pop() ;
                                    $('#allSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'{{url("country")}}/' + element_id + '\';"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">' + element.title + '</h3><p class="place-name"><span class="follow-tag">' + element.place_type + '</span> <span>in ' + element.address + '</span></p></div></div>');
                                }
                                else if(element.place_type=="city") {
                                    var element_id  = element.id;
                                    element_id  = element_id.split("_").pop() ;
                                    $('#allSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'{{url("city")}}/' + element_id + '\';"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">' + element.title + '</h3><p class="place-name"><span class="follow-tag">' + element.place_type + '</span> <span>in ' + element.address + '</span></p></div></div>');
                                }
                                else {
                                    $('#allSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'{{url("place")}}/' + element.id + '\';"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">' + element.title + '</h3><p class="place-name"><span class="follow-tag">' + element.place_type + '</span> <span>in ' + element.address + '</span></p></div></div>');

                                }

                                /// get POI image
                                // $.ajax({
                                //     method: "GET",
                                //     url: "{{url('places/ajax_get_poi_media')}}",
                                //     data: {
                                //         place_id: element.id
                                //     }
                                // })
                                //     .done(function (res) {
                                //         if(res.length>0) {
                                //             $('#POIImage'+element.id).attr('src', "https://s3.amazonaws.com/travooo-images2/"+res);
                                //         }
                                //     });
                            }
                            else {
                                console.log(element.place_type);
                                if(element.place_type=="country") {
                                    var element_id  = element.id;
                                    element_id  = element_id.split("_").pop() ;
                                    $('#allSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'{{url("country")}}/' + element_id + '?is_new=1\';"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">' + element.title + '</h3><p class="place-name"><span class="follow-tag">' + element.place_type + '</span> <span>in ' + element.address + '</span></p></div></div>');
                                }
                                else if(element.place_type=="city") {
                                    var element_id  = element.id;
                                    element_id  = element_id.split("_").pop() ;
                                    $('#allSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'{{url("city")}}/' + element_id + '?is_new=1\';"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">' + element.title + '</h3><p class="place-name"><span class="follow-tag">' + element.place_type + '</span> <span>in ' + element.address + '</span></p></div></div>');
                                }
                                else {
                                    $('#allSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'{{url("place")}}/' + element.id + '?is_new=1\';"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">' + element.title + '</h3><p class="place-name"><span class="follow-tag">' + element.place_type + '</span> <span>in ' + element.address + '</span></p></div></div>');
                                }
                            }
                            
                        });
                    });

                // start searching countries
                $.ajax({
                    method: "GET",
                    url: "{{url('home/searchCountries')}}",
                    data: {q: $('#mainSearchInput').val(), lat: window.locationLat, lng: window.locationLng}
                })
                    .done(function (res) {
                        res = JSON.parse(res);
                        var num_places_result = res.length;
                        $('#numCountriesResult').text(num_places_result);
                        $('#countriesSearchResults').empty();
                        res.forEach(function (element) {
                            //console.log(element);
                            if (typeof element.get_medias[0] === 'undefined' || typeof element.get_medias[0].url === 'undefined' || ! element.get_medias[0].url) {
                                element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                            } else {
                                element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.get_medias[0].url;
                            }
                            $('#countriesSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'{{url("country")}}/' + element.id + '\';"><div class="img-wrap rounded place-img"><img src="' + element.image + '" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">' + element.transsingle.title + '</h3><p class="place-name"><span class="follow-tag">Country</span> <span>in ' + element.region.transsingle.title + '</span></p></div></div>');
                        });
                    });
                // start searching cities
                $.ajax({
                    method: "GET",
                    url: "{{url('home/searchCities')}}",
                    data: {q: $('#mainSearchInput').val(), lat: window.locationLat, lng: window.locationLng}
                })
                    .done(function (res) {
                        res = JSON.parse(res);
                        var num_places_result = res.length;
                        $('#numCitiesResult').text(num_places_result);
                        $('#citiesSearchResults').empty();
                        res.forEach(function (element) {
                            //console.log(element);
                            if (typeof element.get_medias[0] === 'undefined' || typeof element.get_medias[0].url === 'undefined' || ! element.get_medias[0].url) {
                                element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                            } else {
                                element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.get_medias[0].url;
                            }
                            $('#citiesSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'{{url("city")}}/' + element.id + '\';"><div class="img-wrap rounded place-img"><img src="' + element.image + '" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">' + element.transsingle.title + '</h3><p class="place-name"><span class="follow-tag">City</span> <span>in ' + element.country.transsingle.title + '</span></p></div></div>');
                        });
                    });
                // start searching users
                $.ajax({
                    method: "GET",
                    url: "{{url('home/searchUsers')}}",
                    data: {q: $('#mainSearchInput').val(), lat: window.locationLat, lng: window.locationLng}
                })
                    .done(function (res) {
                        res = JSON.parse(res);
                        var num_places_result = res.length;
                        $('#numUsersResult').text(num_places_result);
                        $('#usersSearchResults').empty();
                        res.forEach(function (element) {
                            //console.log(element);
                            if (!element.profile_picture) {
                                if (element.gender == 0 || element.gender == 1) {
                                    element.profile_picture = "https://www.travooo.com/assets2/image/placeholders/male.png";
                                } else {
                                    element.profile_picture = "https://www.travooo.com/assets2/image/placeholders/female.png";
                                }
                            } else {
                                element.profile_picture =  element.profile_picture;
                            }
                            $('#usersSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'{{url("profile")}}/' + element.id + '\';"><div class="img-wrap rounded place-img"><img src="' + element.profile_picture + '" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">' + element.name + '</h3><p class="place-name"><span class="follow-tag">' + element.display_name + '</span></p></div></div>');
                        });
                    });
            } else {
                if( search_in == 'place' ) {
                    // $(".search-box").html(`<ul class="nav nav-tabs search-tabs" role="tablist">
                    //             <li class="nav-item">
                    //                 <a class="nav-link active" href="#search_discussion" role="tab" data-toggle="tab">Discussion <span id="numsearch_discussion"></span></a>
                    //             </li>
                    //             <li class="nav-item">
                    //                 <a class="nav-link" href="#search_post" role="tab" data-toggle="tab">Posts <span id="numsearch_post"></span></a>
                    //             </li>
                    //             <li class="nav-item">
                    //                 <a class="nav-link" href="#search_media" role="tab" data-toggle="tab">Medias <span id="numsearch_media"></span></a>
                    //             </li>
                    //             <li class="nav-item">
                    //                 <a class="nav-link" href="#search_plan" role="tab" data-toggle="tab">Plans <span id="numsearch_plan"></span></a>
                    //             </li>
                    //             <li class="nav-item">
                    //                 <a class="nav-link" href="#search_mate" role="tab" data-toggle="tab">Mates <span id="numsearch_mate"></span></a>
                    //             </li>

                    //             <li class="nav-item">
                    //                 <a class="nav-link" href="#search_report" role="tab" data-toggle="tab">Reports <span id="numsearch_report"></span></a>
                    //             </li>
                    //             <li class="nav-item">
                    //                 <a class="nav-link" href="#search_review" role="tab" data-toggle="tab">Reviews <span id="numsearch_review"></span></a>
                    //             </li>
                    //         </ul>

                    //         <div class="tab-content">
                    //             <div role="tabpanel" class="tab-pane fade in active show" id="search_discussion" aria-expanded="true">
                    //                 <div class="drop-wrap" id="search_discussionResults"></div>
                    //             </div>
                    //             <div role="tabpanel" class="tab-pane fade in" id="search_post">
                    //                 <div class="drop-wrap" id="search_postResults"></div>
                    //             </div>
                    //             <div role="tabpanel" class="tab-pane fade in" id="search_media">
                    //                 <div class="drop-wrap" id="search_mediaResults"></div>
                    //             </div>
                    //             <div role="tabpanel" class="tab-pane fade in" id="search_plan">
                    //                 <div class="drop-wrap" id="search_planResults"></div>
                    //             </div>
                    //             <div role="tabpanel" class="tab-pane fade in" id="search_mate">
                    //                 <div class="drop-wrap" id="search_mateResults"></div>
                    //             </div>

                    //             <div role="tabpanel" class="tab-pane fade in" id="search_report">
                    //                 <div class="drop-wrap" id="search_reportResults"></div>
                    //             </div>
                    //             <div role="tabpanel" class="tab-pane fade in" id="search_review">
                    //                 <div class="drop-wrap" id="search_reviewResults"></div>
                    //             </div>
                    //         </div>`);

                    // $.ajax({
                    //     method: "POST",
                    //     url: "{{route('place.searchall')}}",
                    //     data: {
                    //         q: $('#mainSearchInput').val(),
                    //         search_in: search_in, search_id: search_id
                    //     }
                    // })
                    // .done(function (res) {
                    //     res = JSON.parse(res);
                    //     for( id in res ) {
                    //         $("#num"+id).html(res[id].count);
                    //         $("#"+id+"Results").html(res[id].data);
                    //     }

                    // });
                    filter_array['q'] =$('#mainSearchInput').val();
                    $('#pageno').val(1);
                    pos_ar=0;
                    $('.filter-'+$(this).data('type')).not(this).prop('checked', false);
                    var tabattr = $(".posts-filter__link.posts-filter__link--active").attr('href');
                    update_newsfeed(tabattr, 0,filter_array);
                    // $.ajax({
                    //     method: "POST",
                    //     url: "{{route('place.searchall')}}",
                    //     data: {
                    //         q: $('#mainSearchInput').val(),
                    //         search_in: search_in, search_id: search_id
                    //     }
                    // })
                    // .done(function (res) {
                    //     $('.posts-filter__link--active').removeClass('posts-filter__link--active');
                    //     $('.posts').find('[filter-tab]').fadeOut();
                    //     $('.posts').find('[search-tab]').remove();
                    //     if(res != "") {
                    //         $('.posts').append(res);
                    //         initMap();
                    //     } else {
                    //         $('.posts').append('<div class="user-card" search-tab="#searched"><div class="user-card__main"><div class="user-card__content"><div class="user-card__header"><a class="user-card__name" href="#" style="margin-left: calc(50% - 35px)">No result!</a></div></div></div></div>');
                    //     }
                    // });
                }
            }
        } else {
                    filter_array['q'] ='';
                    $('#pageno').val(1);
                    pos_ar=0;
                    $('.filter-'+$(this).data('type')).not(this).prop('checked', false);
                    var tabattr = $(".posts-filter__link.posts-filter__link--active").attr('href');
                    update_newsfeed(tabattr, 0,filter_array);
            $(this).parents('.header-search-block').removeClass('show');
            $(".location-identity").show();
        }
    }, 500));

    // $('body').on('submit', '.mediaCommentForm', function (e) {
    //     var values = $(this).serialize();
    //     var hidden_field = $(this).find('input:hidden');
    //     var media_id = hidden_field.val();
    //     console.log(media_id);
    //     $.ajax({
    //         method: "POST",
    //         url: "{{url_with_locale('medias')}}" + "/" + media_id + "/comment",
    //         data: values
    //     })
    //         .done(function (res) {

    //             result = JSON.parse(res);
    //             console.log(result);
    //             if (result.success == 1) {
    //                 $('#commentsContainer' + media_id).append(result.comment_html);
    //             }

    //         });
    //     e.preventDefault();
    // });

    $('body').click(function(evt){
        if($(evt.target).hasClass('header-search-block'))
        return;
        //For descendants of header-search-block being clicked, remove this check if you do not want to put constraint on descendants.
        if($(evt.target).closest('.header-search-block').length)
        return;

        //Do processing of click event here for every element except with class header-search-block
        if($('.header-search-block').hasClass('show') && !("ontouchstart" in window || navigator.msMaxTouchPoints > 0))
        {
            $('.search-block .delete-search').click();
        }

    });
    
    $('.search-block .delete-search').on('click', function(e){
        $(e.target).parents('.header-search-block').removeClass('show').find('input').val(null);
        $(".location-identity").show();
        $(e.target).parents('.search-block').find('input').val(null);
    })

    @if(\Illuminate\Support\Facades\Auth::check())
    $(document).ready(function () {

        $('body').on('click', '.acceptPlanInvitation', function (e) {
            var invitation_id = $(this).attr('data-id');
            $.ajax({
                method: "POST",
                url: "{{ route('trip.ajax_accept_invitation') }}",
                data: {invitation_id: invitation_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    var trip_id = result.trip_id;

                    if (result.status == 'success') {
                        window.location.replace("{{  url('trip/plan') }}"+"/"+trip_id + '?do=edit');
                    }

                });
            e.preventDefault()
        });

        $('body').on('click', '.rejectPlanInvitation', function (e) {
            var invitation_id = $(this).attr('data-id');
            var self = $(this);
            $.ajax({
                method: "POST",
                url: "{{ route('trip.ajax_reject_invitation') }}",
                data: {invitation_id: invitation_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == 'success') {
                        alert('Invitation rejected successfully!');
                        $(self).parents('div.notification-block').remove();
                    }

                });
            e.preventDefault()
        });


        function load_unseen_notification(view = '') {
            $.ajax({
                url: "{{ route('home.ajax_get_notifications') }}",
                method: "POST",
                data: {view: view},
                dataType: "json",
                success: function (data) {
                    $('#notificationsRows').html(data.notifications);
                    $('#seeAllModal').find('.conversation-inner').html(data.notifications);
                    if (data.count_unseen > 0) {
                        $('#notifications-count-unseen').css('display', 'block');
                        $('#notifications-count-unseen').html(data.count_unseen);
                    }
                }
            });
        }

        load_unseen_notification();


        function load_unread_message(view = '') {
//            $.ajax({
//                url: "{{ route('home.ajax_get_messages') }}",
//                method: "POST",
//                dataType: "json",
//                success: function (data) {
//                    if (data.count == 0) {
//                        //$('#chat-unread-counter').css('display', 'none');
//                        //$('#chat-unread-counter').html('');
//                    }else{
//                        //$('#chat-unread-counter').css('display', 'block');
//                        //$('#chat-unread-counter').html(data.count);
//                    }
//                }
//            });
        }
//        load_unread_message();


// load new notifications

        $('#notificationsDropDown').on('show.bs.dropdown', function () {
            $('#notifications-count-unseen').html('');
            $('#notifications-count-unseen').css('display', 'none');

            load_unseen_notification('yes');

        });

        setInterval(function () {

            load_unseen_notification();
            load_unread_message();

        }, 50000);

    });
    @endif

    $(document).ready(function () {
        toggleLastMessages();
    });
</script>
