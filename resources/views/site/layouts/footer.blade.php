<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
<link rel="stylesheet" href="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.css')}}">
<script src="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.js')}}"></script>
<script src="{{url('assets2/js/script.js')}}"></script>
<script type="text/javascript">
    
    
    function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
  
}

window.locationLat = false;
window.locationLng = false;

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition, showError);
  } else {
    document.getElementById('locationMsg').innerHTML = "Geolocation is not supported by this browser.";
  }
}

getLocation();

function showPosition(position) {
    window.locationLat = position.coords.latitude;
    window.locationLng = position.coords.longitude;
}

function showError(error) {
    if(document.getElementById('locationMsg') == null) return;
  switch(error.code) {
    case error.PERMISSION_DENIED:
      document.getElementById('locationMsg').innerHTML = "We are using your location information to give you a better experience showing most accurate & nearby results. Please note that some interesting functionality - like showing Places of interests and updates according to their location from you - will not be working as expected until you allow Travooo to access your location. \n\
(<a href='https://www.lifewire.com/denying-access-to-your-location-4027789' target='_blank'>Know how here</a>) " + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
      document.getElementById('locationMsg').style.display = 'block';
                break;
    case error.POSITION_UNAVAILABLE:
      document.getElementById('locationMsg').innerHTML = "Location information is unavailable."
      document.getElementById('locationMsg').style.display = 'block';
                break;
    case error.TIMEOUT:
      document.getElementById('locationMsg').innerHTML = "The request to get user location timed out."
      document.getElementById('locationMsg').style.display = 'block';
      break;
    case error.UNKNOWN_ERROR:
      document.getElementById('locationMsg').innerHTML = "An unknown error occurred."
      document.getElementById('locationMsg').style.display = 'block';
      break;
  }
  }

    // message input dropdown
  $('#mainSearchInput').keyup(delay(function(e){
    let value = $(e.target).val();
    var search_in = false;
    var search_id = false;
    @if(isset($country))
        search_in = 'countries';
        search_id = {{$country->id}};
    @elseif(isset($city))
        search_in = 'cities';
        search_id = {{$city->id}};
    @endif
    if(value.length >= 3){
      $(this).parents('.header-search-block').addClass('show');
      
            // start searching places
            $.ajax({
                method: "GET",
                url: "{{url('home/searchPlaces')}}",
                data: { q: $('#mainSearchInput').val(), lat:window.locationLat , lng:window.locationLng,
                search_in: search_in, search_id: search_id}
            })
            .done(function(res){
                res = JSON.parse(res);
                 console.log(res);
                var num_places_result = res.length;
                $('#numPlacesResult').text(num_places_result);
                $('#placesSearchResults').empty();
                res.forEach(function(element) {
                    //console.log(element);
                    if(!element.image) {
                        element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                    } else {
                        element.image = 'https://s3.amazonaws.com/travooo-images2/'+element.image;
                    }
                    $('#placesSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'https://www.travooo.com/place/'+element.id+'\';"><div class="img-wrap rounded place-img"><img src="'+element.image+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">'+element.title+'</h3><p class="place-name"><span class="follow-tag">'+element.type+'</span> <span>in '+element.address+'</span></p></div></div>');
                  });
            });
            // start searching restaurants
            $.ajax({
                method: "GET",
                url: "{{url('home/searchRestaurants')}}",
                data: { q: $('#mainSearchInput').val(), lat:window.locationLat , lng:window.locationLng,
                search_in: search_in, search_id: search_id}
            })
            .done(function(res){
                res = JSON.parse(res);
                var num_places_result = res.length;
                $('#numRestaurantsResult').text(num_places_result);
                $('#restaurantsSearchResults').empty();
                res.forEach(function(element) {
                    //console.log(element);
                    if(!element.image) {
                        element.image = "https://www.travooo.com/assets2/image/placeholders/restaurant.png"
                    } else {
                        element.image = 'https://s3.amazonaws.com/travooo-images2/'+element.image;
                    }
                    $('#restaurantsSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'https://www.travooo.com/restaurant/'+element.id+'\';"><div class="img-wrap rounded place-img"><img src="'+element.image+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">'+element.title+'</h3><p class="place-name"><span class="follow-tag">'+element.type+'</span> <span>in '+element.address+'</span></p></div></div>');
                  });
            });
            // start searching hotels
            $.ajax({
                method: "GET",
                url: "{{url('home/searchHotels')}}",
                data: { q: $('#mainSearchInput').val(), lat:window.locationLat , lng:window.locationLng,
                search_in: search_in, search_id: search_id}
            })
            .done(function(res){
                res = JSON.parse(res);
                var num_places_result = res.length;
                $('#numHotelsResult').text(num_places_result);
                $('#hotelsSearchResults').empty();
                res.forEach(function(element) {
                    //console.log(element);
                    if(!element.image) {
                        element.image = "https://www.travooo.com/assets2/image/placeholders/hotel.png"
                    } else {
                        element.image = 'https://s3.amazonaws.com/travooo-images2/'+element.image;
                    }
                    $('#hotelsSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'https://www.travooo.com/hotel/'+element.id+'\';"><div class="img-wrap rounded place-img"><img src="'+element.image+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">'+element.title+'</h3><p class="place-name"><span class="follow-tag">'+element.type+'</span> <span>in '+element.address+'</span></p></div></div>');
                  });
            });
            // start searching countries
            $.ajax({
                method: "GET",
                url: "{{url('home/searchCountries')}}",
                data: { q: $('#mainSearchInput').val(), lat:window.locationLat , lng:window.locationLng}
            })
            .done(function(res){
                res = JSON.parse(res);
                var num_places_result = res.length;
                $('#numCountriesResult').text(num_places_result);
                $('#countriesSearchResults').empty();
                res.forEach(function(element) {
                    //console.log(element);
                    if(!element.get_medias[0].url) {
                        element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                    } else {
                        element.image = 'https://s3.amazonaws.com/travooo-images2/'+element.get_medias[0].url;
                    }
                    $('#countriesSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'https://www.travooo.com/country/'+element.id+'\';"><div class="img-wrap rounded place-img"><img src="'+element.image+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">'+element.transsingle.title+'</h3><p class="place-name"><span class="follow-tag">Country</span> <span>in '+element.region.transsingle.title+'</span></p></div></div>');
                  });
            });
            // start searching cities
            $.ajax({
                method: "GET",
                url: "{{url('home/searchCities')}}",
                data: { q: $('#mainSearchInput').val(), lat:window.locationLat , lng:window.locationLng}
            })
            .done(function(res){
                res = JSON.parse(res);
                var num_places_result = res.length;
                $('#numCitiesResult').text(num_places_result);
                $('#citiesSearchResults').empty();
                res.forEach(function(element) {
                    //console.log(element);
                    if(!element.get_medias[0].url) {
                        element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                    } else {
                        element.image = 'https://s3.amazonaws.com/travooo-images2/'+element.get_medias[0].url;
                    }
                    $('#citiesSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'https://www.travooo.com/city/'+element.id+'\';"><div class="img-wrap rounded place-img"><img src="'+element.image+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">'+element.transsingle.title+'</h3><p class="place-name"><span class="follow-tag">City</span> <span>in '+element.country.transsingle.title+'</span></p></div></div>');
                  });
            });
            // start searching users
            $.ajax({
                method: "GET",
                url: "{{url('home/searchUsers')}}",
                data: { q: $('#mainSearchInput').val(), lat:window.locationLat , lng:window.locationLng}
            })
            .done(function(res){
                res = JSON.parse(res);
                var num_places_result = res.length;
                $('#numUsersResult').text(num_places_result);
                $('#usersSearchResults').empty();
                res.forEach(function(element) {
                    //console.log(element);
                    if(!element.profile_picture) {
                        if(element.gender==0 || element.gender==1) {
                            element.profile_picture = "https://www.travooo.com/assets2/image/placeholders/male.png";
                        } else {
                            element.profile_picture = "https://www.travooo.com/assets2/image/placeholders/female.png";
                        }
                    } else {
                        element.profile_picture = "assets2/" + element.profile_picture;
                    }
                    $('#usersSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="window.location.href = \'https://www.travooo.com/profile/'+element.id+'\';"><div class="img-wrap rounded place-img"><img src="'+element.profile_picture+'" alt="logo" style="width:60px;height:60px;min-width:60px !important;"></div><div class="drop-content place-content"><h3 class="content-ttl">'+element.name+'</h3><p class="place-name"><span class="follow-tag">'+element.display_name+'</span></p></div></div>');
                  });
            });

    } else{
      $(this).parents('.header-search-block').removeClass('show');
    }
  }, 500));
  
    $('.home-mobile-top-actions .search-btn').on('click', function(e){
        $('.modal').modal('hide');
        $('.navbar .header-search-block').addClass('show');
        $('#mainSearchInput').focus();
    });
  
//   $('body').on('submit', '.mediaCommentForm', function (e) {
//         var values = $(this).serialize();
//         var hidden_field = $( this ).find( 'input:hidden' );
//         var media_id = hidden_field.val();
//         console.log(media_id);
//         $.ajax({
//             method: "POST",
//             url: "{{url_with_locale('medias')}}" + "/" + media_id + "/comment",
//             data: values
//         })
//                 .done(function (res) {
                    
//                     result = JSON.parse(res);
//             console.log(result);
//             if(result.success==1) {
//                 $('#commentsContainer'+media_id).append(result.comment_html);
//             }

//                 });
//         e.preventDefault();
//     });
  $('body').on('click', '#messagesDropDown', function () {
        toggleLastMessages();
    });
    
    
    $('body').on('click', '.media_like_button', function (e) {
        mediaId = $(this).attr('id');
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('medias/likeunlike')}}",
            data: {media_id: mediaId}
        })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    console.log('<a href="#" data-tab="comments'+mediaId+'"><b>'+result.count+'</b> Likes</a>');
                    $('#media_like_count_' + mediaId).html('<a href="#" class="media_like_button" id="'+mediaId+'"><b>'+result.count+'</b> Likes</a>');
                });
        e.preventDefault();
    });
    
    
    @if(\Illuminate\Support\Facades\Auth::check())
    $(document).ready(function () {
                    
        $('body').on('click', '.acceptPlanInvitation', function (e) {
        var invitation_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('trip/ajaxAcceptInvitation')}}",
            data: {invitation_id: invitation_id}
        })
                .done(function (res) {
                    var result = JSON.parse(res);
            console.log(result);
                    if(result.status=='success') {
                        window.location.replace("{{route('profile.plans')}}");
                    }
                    
                });
        e.preventDefault()
    });
    
    $('body').on('click', '.rejectPlanInvitation', function (e) {
        var invitation_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('trip/ajaxRejectInvitation')}}",
            data: {invitation_id: invitation_id}
        })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if(result.status=='success') {
                        alert('Invitation rejected successfully!');
                    }
                    
                });
        e.preventDefault()
    });
    
    
        function load_unseen_notification(view = '')
        {
            $.ajax({
                url: "{{url_with_locale('ajaxGetNotifications')}}",
                method: "POST",
                data: {view: view},
                dataType: "json",
                success: function (data)
                {
                    $('#notificationsRows').html(data.notifications);
                    if (data.count_unseen > 0)
                    {
                        $('#notifications-count-unseen').css('display', 'block');
                        $('#notifications-count-unseen').html(data.count_unseen);
                    }
                }
            });
        }
        load_unseen_notification();

// load new notifications

        $('#notificationsDropDown').on('show.bs.dropdown', function () {
            $('#notifications-count-unseen').html('');
            $('#notifications-count-unseen').css('display', 'none');

            load_unseen_notification('yes');

        });

        setInterval(function () {

            load_unseen_notification();
            ;

        }, 5000);

    });
    @endif
</script>
