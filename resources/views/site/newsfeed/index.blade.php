@php
$title = 'Post on Travooo';
@endphp

@isset($ogMeta)
    @foreach ($ogMeta as $ogMetaProperty => $ogMetaContent)
        @if (in_array($ogMetaProperty, ['title','description']))
            <meta property="og:{{$ogMetaProperty}}" content="{!! strip_tags($ogMetaContent) !!}" />
        @else
            <meta property="og:{{$ogMetaProperty}}" content="{{ $ogMetaContent }}" />
        @endif
    @endforeach
@endisset

@section('before_styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"
      rel="stylesheet"/>
<link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets2/css/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets2/js/jquery.mentionsInput/jquery.mentionsInput.css?date=20/08/2019') }}" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<link href="{{ asset('assets2/js/summernote/summernote.css') }}" rel="stylesheet">
<link href="{{ asset('/plugins/summernote-master/tam-emoji/css/emoji.css') }}" rel="stylesheet"/>
<link rel="stylesheet" href="{{asset('assets3/css/style-13102019.css')}}">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous">
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="{{asset('assets3/css/datepicker-extended.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/lightslider.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
<link
rel="stylesheet"
href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css"
type="text/css"
/>
<link rel="stylesheet" href="{{asset('dist-parcel/assets/Post/main-AEA-2029.css')}}">

<style type="text/css">
    #progress-wrp {
        border: 1px solid #0099CC;
        padding: 1px;
        position: relative;
        height: 30px;
        border-radius: 3px;
        margin: 10px;
        text-align: left;
        background: #fff;
        box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
    }

    #progress-wrp .progress-bar {
        height: 100%;
        border-radius: 3px;
        background-color: #f39ac7;
        width: 0;
        box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
    }

    #progress-wrp .status {
        top: 3px;
        left: 50%;
        position: absolute;
        display: inline-block;
        color: #000000;
    }

    #lightbox {
        display: flex;
        flex-direction: column-reverse;
    }
    .frm-url-container {
        border: #ced1d4 1px solid;
        border-radius: 4px;
        max-width: 550px;
        padding: 5px;
        position: relative;
    }

    .url-input {
        width: 100%;
        border: none;
        height: 25px;
        padding-left: 10px;
        font-family: Arial, Helvetica, sans-serif;
        padding-right: 30px;
        min-height: 50px;
    }

    .url-input:focus {
        outline: none;
    }

    .image-container {
        position: relative;
        z-index: 1;
    }

    .image-preview {
        width: 100%;
        margin-right: 100px;
    }

    .image-preview img {
        width: 100px;
        height: 100px;
    }

    #loader {
        position: absolute;
        right: 10px;
        margin-top: -12px;
        width: 16px;
        height: 24px;
        top: 50%;
        display: none;
    }

    #output {
        display: none;
    }

    .page-title {
        color: #000;
        text-decoration: none;
        font-size: 1.2em;
        line-height: 30px;
    }

    .text-data {
        margin-top: 20px;
    }

    .prev-next-navigation .prev-img {
        background: url(prev-img.png) no-repeat center center;
        display: inline-block;
        width: 16px;
        height: 16px;
        cursor: hand;
        cursor: pointer;
        border: #9a9b9a 1px solid;
        padding: 3px;
        opacity: 0.5;
    }

    .prev-next-navigation .next-img {
        background: url(next-img.png) no-repeat center center;
        display: inline-block;
        width: 16px;
        height: 16px;
        cursor: hand;
        cursor: pointer;
        border: #9a9b9a 1px solid;
        padding: 3px;
        opacity: 0.5;
    }

    a {
        color: #0254EB
    }
    a:visited {
        color: #0254EB
    }
    a.morelink {
        text-decoration:none;
        outline: none;
        font-size: 18px;
    }
    .morecontent span {
        display: none;
    }
    .comment {
        width: 400px;
        background-color: #f0f0f0;
        margin: 10px;
    }
    .thumb::before{
        content:"X";
        color: black;
        width: 1em;
        height: 1em;
        font-size: 10px;
    }
    .textarea-customize
    {
        border: 1px solid #dcdcdc;
        border-radius: 3px;
        resize: none;
    }

    .comment_showmore_btn
    {
        text-align: right;
        cursor: pointer;
        color: blue;
    }
    .spinner-border {
        display: inline-block;
        width: 1rem;
        height: 1rem;
        vertical-align: text-bottom;
        border: .2em solid currentColor;
        border-right-color: transparent;
        border-radius: 50%;
        -webkit-animation: spinner-border .75s linear infinite;
        animation: spinner-border .75s linear infinite;
    }
    @keyframes spinner-border {
        to { transform: rotate(360deg); }
    }

    .vid-quality-selector button.active {
        background-color: white;
        color: #222;
    }

    .vid-quality-selector button {
        background-color: rgba(34, 34, 34, 0.5);
        padding: 7px 10px;
        font-weight: 700;
        font-size: 11px;
        color: white;
        cursor: pointer;
    }

    .vid-quality-selector {
        position: absolute;
        top: 55%;
        right: 30px;
        z-index: 3;
        flex-direction: column;
        border-radius: 5px;
        overflow: hidden;
        transition: all 550ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
    }

    .flex {
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flex;
        display: -o-flex;
        display: flex;
    }
    .player{
        width: 595px;
        position: relative;
    }

    .note-popover {
        display: none;
    }

    .right-inner-addon {
        position: relative;

        width: 200px;
    }
    .right-inner-addon input {
        padding-right: 30px;
        width: 170px; /* 200px - 30px */
    }
    .right-inner-addon svg {
        position: absolute;
        right: -10px;
        padding: 10px 12px;
        pointer-events: none;

        top: -8px;
    }
    input.post-input {
        border: none;
        background-color: #f8f8f8;
    }
    a.post-author-name {
        color: black;
        text-decoration: none;
    }
    .post-media-options {
        right: 0;
        top: 6px;
        position: absolute;
    }
    .check-in-search {
        left: 0;
        top: 6px;
        position: absolute;
    }
    body .modal-backdrop.show {
        opacity: 0.8;
    }
    body .modal-dialog {
        width: 600px;
    }
    #add_post_permission_button {
        color: #b4b4b5;
        font-family: inherit;
    }
    body .modal-buttons {
        text-decoration: none!important;
        cursor: pointer;
        color: #b4b4b5;
        font-weight: 500;
    }
    body .modal-buttons:hover {
        color: #b4b4b5;
    }
    i.icon {
        display: inline-block;
        border-radius: 60px;
        background-color: #e6e6e6;
        padding: 0.25em 0.3em;

    }

    .footer-actions {
        color: #000;
        background-color: #fff;
    }
    .footer-actions svg * {
        fill: #000;
    }
    .footer-actions:hover {
        background-color: #e6e6e6;
    }
    .add-post__textarea {
        height: 100%;
        overflow: hidden;
    }
    button.disabled, button:disabled {
        cursor: not-allowed;
        opacity: .65;
    }
    .label-tag {
        color:red !important;
    }

    .report-span-input:not(#post-author-name) {
        padding-left: 27px!important;
    }

    .fa.fa-clock:after {
        content: "\f017";
    }


    #checkInModal .modal-dialog .modal-content, #taggingModal .modal-dialog .modal-content {
        height: 358px;
    }

    #taggingButton {
        outline: none;
        border-radius: 50%;
    }
    #modalTagging .post-create-input {
        display: flex;
    }
    #modalTagging .post-create-input .note-editor {
        width: 80%;
    }
    #taggingButton:hover, .blue-tagging-button {
        border-radius: 50%;
        border: 1px solid #c9dcff;
    }
    #taggingButton i:hover,.blue-tagging-button i {
        border-radius: 50%;
        color: #3e81ff;
    }
    #createNewPostPopup .note-editor {
        border: none;
    }
    /*#createNewPostPopup .note-toolbar.panel-heading*/
    #createNewPostPopup .note-statusbar {
        display: none;
    }
    #createNewPostPopup .note-editor.note-frame {
        position: relative;
    }
    #createNewPostPopup .note-toolbar {
        position: static;
    }
    #createNewPostPopup .note-toolbar .note-insert {
        position: absolute;
        bottom: -60px;
        z-index: 4;
        right: 6px;
    }
    #createNewPostPopup .note-toolbar .note-insert button div {
        display: block!important;
    }
    #createNewPostPopup .note-toolbar .note-insert button {
        background: transparent;
        border: none;
        height: 24px;
        /* width: 24px; */
        padding: 0;
    }
    #createNewPostPopup .note-editable {
        width: 100%;
        height: auto;
        min-height: 160px;
        padding: 0;
        font-family: inherit;
        font-size: 22px;
        line-height: 32px;
        color: #1a1a1a;
        resize: none;
    }
    .post-create-input .img-wrap-newsfeed video::-webkit-media-controls-enclosure {
        display: none;
    }
    #checkinSearchResults {
        min-height: 300px;
    }
    .check-in-point {
        display: none;
    }

    button.create-new-post-form-trigger, button.trav-set-location-button {
        outline: none;
    }
    .btn-link, .btn-link:active, .btn-link:focus {
        background-color: transparent !important;
        border: none;
    }
    .note-editor.note-frame .custom-placeholder {
        display: block !important;
    }
    .note-toolbar.panel-heading {
        height: 0;
    }
    .taggedPlaces {
        display: inline-block;
    }
    .summernote-selected-items i {
        color: #b3b3b3;
        font-size: 16px;
    }
    .check-follow-place button {
        margin-bottom: 0 !important;
    }
    .shared-place-slider-card img {
        max-width: unset;
    }
    .country-flag {
        height: 13px !important;
        width: auto !important;
        border-radius: unset !important;
        border: none !important;
        margin-right: 5px !important;
    }
    
    .mapboxgl-ctrl-bottom-left, .mapboxgl-ctrl-top-right, .mapboxgl-ctrl-bottom-right{
        display: none;
    }
    
</style>
@endsection

@extends('site.layouts.site')

@section('content-area')
@include('site.layouts._left-menu')

<div class="custom-row">
    <!-- MAIN-CONTENT -->
    <div class="main-content-layer">
        <a type="button" onclick="goBack()" class="back-link hidden-more-sm"><i class="trav-angle-left"></i> <span class="back-link-title">@lang('other.back')</span></a>
        <div id="feed" class='post-page-unique'>
            {!! $view !!}
        </div>
    </div>

    <!-- SIDEBAR -->
    <div class="sidebar-layer" id="sidebarLayer">
        <aside class="sidebar">

            <div class="post-block post-side-block">
                <div class="post-side-top">
                    <h3 class="side-ttl">@lang('home.top_places')</h3>
                </div>
                <div class="post-side-inner place-animation-side-block" id="placeAsideAnimation">
                    <div class="side-place-block place-animation-side-block">
                        <div class="place-animation-title">
                            <div class="place-top-info-layer">
                                <div class="place-top-info-wrap">
                                    <div class="place-top-avatar-wrap animation post-animation-avatar"></div>
                                    <div class="place-animation-info-txt">
                                        <div class="place-top-name animation"></div>
                                        <div class="place-info animation"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="place-animation-content">
                            <div class="animation"></div><div class="animation"></div><div class="animation"></div><div class="animation"></div>
                        </div>
                        <div class="place-animation-footer">
                            <div class="place-animation-count animation"></div>
                        </div>
                    </div>
                    <div class="side-place-block place-animation-side-block">
                        <div class="place-animation-title">
                            <div class="place-top-info-layer">
                                <div class="place-top-info-wrap">
                                    <div class="place-top-avatar-wrap animation post-animation-avatar"></div>
                                    <div class="place-animation-info-txt">
                                        <div class="place-top-name animation"></div>
                                        <div class="place-info animation"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="place-animation-content">
                            <div class="animation"></div><div class="animation"></div><div class="animation"></div><div class="animation"></div>
                        </div>
                        <div class="place-animation-footer">
                            <div class="place-animation-count animation"></div>
                        </div>
                    </div>
                </div>
                <div class="post-side-inner d-none" id="placeAsideBlock">
                    @foreach($top_places as $tplace)
                    <div class="side-place-block">
                        <div class="side-place-top">
                            <div class="side-place-avatar-wrap">

                                <img class="lazy" data-src="https://s3.amazonaws.com/travooo-images2/th180/{{@$tplace->place->medias[0]->url}}"
                                     alt="" style="width:46px;height:46px;">
                            </div>
                            <div class="side-place-txt">
                                <a class="side-place-name"
                                   href="{{ route('place.index', @$tplace->place->id) }}">{{@$tplace->place->trans[0]->title}}</a>
                                <div class="side-place-description">
                                    <b>{{@do_placetype($tplace->place->place_type)}}</b> in <a
                                        href="{{ route('city.index', @$tplace->place->city->id) }}">{{@$tplace->place->city->trans[0]->title}}</a>,
                                    <a href="{{ route('country.index', @$tplace->place->country->id)}}">{{@$tplace->place->country->trans[0]->title}}</a>
                                </div>
                            </div>
                        </div>
                        <ul class="side-place-image-list">
                            @if(isset($tplace->place))
                            @foreach($tplace->place->medias AS $pm)
                            @if(!$loop->first AND $loop->iteration<6)
                            <li>
                                <img class="lazy" data-src="https://s3.amazonaws.com/travooo-images2/th180/{{@$pm->url}}"
                                     alt="photo" style="width:79px;height:75px;"></li>
                            @endif
                            @endforeach
                            @endif
                        </ul>
                        <div class="side-place-bottom">
                            <div class="side-follow-info">
                                <b>{{ @count($tplace->place->followers) }}</b> @lang('home.following_this_place')
                            </div>
                            @if(Auth::check())
                                <?php
                                    $placefollow = \App\Models\Place\PlaceFollowers::where('users_id', Auth::user()->id)->where('places_id', $tplace->places_id)->get();
                                ?>
                                @if(count($placefollow) > 0)
                                <button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_place_following('unfollow', {{$tplace->places_id}}, this)">Unfollow
                                </button>
                                @else
                                <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following('follow', {{$tplace->places_id}}, this)">@lang('home.follow')
                                </button>
                                @endif
                            @else
                                <button type="button" class="btn btn-light-primary btn-bordered open-login">@lang('home.follow')</button>
                            @endif
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>

        </aside>
    </div>

@if(Auth::check())
<div class="modal white-style share-post-modal" data-backdrop="false" id="shareablePost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog share-post-new modal-670" role="document" >
            <div class="modal-custom-block" >
                <div class="post-block post-travlog post-create-travlog" style="padding-bottom: 40px;" >
                    <div class="share-modal-header">
                        <div>
                        <h4>Share this post</h4>
                        </div>
                        <button type="button" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times"></i>
                        </button>
                    </div>
                    <!-- <div class="shared-post-comment">
                        <div class="shared-post-comment-details">
                            <div class="shared-post-comment-author">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                                {{Auth::user()->name}}
                            </div>
                            <div class="dropdown privacy-settings">
                                <button id="add_post_permission_buttonz" class="btn btn--sm btn--outline dropdown-toggle " data-toggle="dropdown">
                                    <i class="trav-globe-icon"></i> PUBLIC
                                </button>
                                <div class="dropdown-menu dropdown-menu-left permissoin_show hided">
                                    <a class="dropdown-item " href="#">
                                        <span class="icon-wrap">
                                            <i class="trav-globe-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Public</b></p>
                                            <p>Anyone can see this post</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item permission_drop_down"   href="#">
                                        <span class="icon-wrap">
                                            <i class="trav-users-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Friends Only</b></p>
                                            <p>Only you and your friends can see this post</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item permission_drop_down" href="#">
                                        <span class="icon-wrap">
                                            <i class="trav-lock-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Only Me</b></p>
                                            <p>Only you can see this post</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="share-modal-action">
                        <div class='user--info'>
                        <img class="ava" src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                        <p>{{Auth::user()->name}}</p>
                        </div>
                        <div class='share-dropdown-modal'>
                            <div class="dropdown privacy-settings">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="far fa-globe-americas"></i>
                                    <span>PUBLIC</span>
                                    <i class="fas fa-sort-down"></i>
                                </button>
                                    <div class="dropdown-menu dropdown-toggle permissoin_show hided" aria-labelledby="dropdownMenuButton">
                                    <div class="share-drop-flex">
                                        <i class="fal fa-globe-asia"></i>
                                        <div>
                                            <a class="dropdown-item" href="#">
                                                <span>Public</span>
                                                <span>Anyone can see this post</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="share-drop-flex">
                                        <i class="fal fa-user-friends"></i>
                                        <div>
                                            <a class="dropdown-item" href="#">
                                                <span>Friends Only</span>
                                                <span>Only you and your friends</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="share-drop-flex">
                                        <i class="fal fa-lock"></i>
                                        <div>
                                            <a class="dropdown-item" href="#">
                                                <span>Only Me</span>
                                                <span>Only you can see this post</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <textarea name="" style="resize: none;" id="message_popup_text_for_share" cols="30" rows="2" placeholder="Write something..."></textarea>
                    <div class="shared-post-wrap" id="shared-post-wrap-content">
                    </div>
                    <div class="share-footer-modal">
                        <div  class="share-footer-modal__icons">
                            <a href="javascript:;" class="ico--ws" onclick="__postShare('whatsapp')"><i class="fab fa-whatsapp"></i></a>
                            <a href="javascript:;" class="ico--face" onclick="__postShare('facebook')"><i class="fab fa-facebook-f"></i></a>
                            <a href="javascript:;" class="ico--twitter" onclick="__postShare('twitter')"><i class="fab fa-twitter"></i></a>
                            <a href="javascript:;" class="ico--pin" onclick="__postShare('pintrest')"><i class="fab fa-pinterest-p"></i></a>
                            <a href="javascript:;" class="ico--tumb" onclick="__postShare('tumblr')"><i class="fab fa-tumblr"></i></a>
                            <a href="javascript:;" class="ico--link" onclick="__postShare('link')"><i class="fa fa-link"></i></a>
                        </div>
                        <button class="btn place_cancel_btn share-modal__close" data-dismiss="modal" type="button">Cancel</button>
                        <button class="btn place_share_btn share_post_btn">Share</button> 
                        <input type="hidden" name="share-post-id" id="share-post-id">
                        <input type="hidden" name="share-post-type" id="share-post-type">
                    </div>

                </div>

            </div>
        </div>
    </div>

@endif

@if(Auth::check())
    <div class="modal opacity--wrap share-post-modal" data-backdrop="false" id="sendToFriendModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-custom-style modal-670" role="document" >
            <div class='friend-share-modal'>
                <div class='modal-content'>
                    <div class='friend-share-modal__header'>
                        <div>
                            <h2>Send to a Friend</h2>
                        </div>
                        <button class="modal--close" type="button" data-dismiss="modal" aria-label="Close" >
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="shared-post-comment">
                        <div class="shared-post-comment-details">
                            <div class="shared-post-comment-author">
                                <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                                {{Auth::user()->name}}
                            </div>
                        </div>
                        <div class="shered-post-comment-text">
                            <textarea name="" id="message_popup_text" cols="30" rows="1" placeholder="Write something..."></textarea>
                        </div>     
                        <div class="module-colapse">
                            <div aria-expanded="false" class="shared-post-wrap collapse demo" id="message_popup_content">
                            
                            </div>
                            <a role="button" class='button--colapse collapsed' class="collapsed" data-toggle="collapse" data-target=".demo"
                            aria-expanded="false" aria-controls="collapseExample"></a>
                        </div>
                        <div class="friend-search">
                            <i class="trav-search-icon" ></i>
                            <input type="text" placeholder="Search in your friends list...">
                        </div>
                    </div>
                    <div class="friend-search-results">
                        @php
                            $friends_users = App\Models\User\User::whereIn('id',get_friendlist())->distinct()->get();

                        @endphp
                        @if(count($friends_users)>0)
                            @foreach ($friends_users as $f_user)
                                <div class="friend-search-results-item">
                                    <img class="user-img" src="{{check_profile_picture($f_user->profile_picture)}}" alt="">
                                    <div class="text">
                                        <div class="location-title">{{$f_user->name}}</div>
                                    </div>
                                    <button class="btn btn-light send_message" id="send_message" data-user="{{$f_user->id}}">Send</button>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('site.home.new.partials._delete-modal')

@endif

@if(Auth::check())
    @include('site.home.partials._spam_dialog')
    @include('site.dashboard._modals')
@endif

</div>

@endsection

@section('before_site_script')
<script>
    function goBack() {
        window.history.back();
    }
</script>

</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script defer src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<script src="{{ asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js') }}"></script>
<script src="{{asset('assets2/js/lightbox2/src/js/lightbox.js')}}"></script>
<script src="https://underscorejs.org/underscore-min.js"></script>
<script src="{{ asset('assets2/js/jquery.mentionsInput/jquery.mentionsInput.js?date=31/08/2019') }}"></script>
<script src="https://podio.github.io/jquery-mentions-input/lib/jquery.elastic.js"></script>
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
<script src="{{ asset('assets2/js/posts-script.js?v=0.3') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="{{ asset('assets2/js/summernote/summernote.js') }}"></script>
<script src="{{ asset('assets2/js/emoji/jquery.emojiFace.js?v=0.1') }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/config.js') }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/tam-emoji.js?v='.time()) }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/textcomplete.js?v='.time()) }}"></script>
<script src="{{ asset('assets3/js/bootstrap-datepicker-extended.js') }}"></script>
<script src="{{asset('assets2/js/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="{{ asset('assets/js/lightslider.min.js') }}"></script>
<script src="{{ asset('assets3/js/tether.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />
<script src="{{ asset('assets2/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>

@include('site.home.partials._comments_scripts')
@include('site.newsfeed._scripts')
@endsection

