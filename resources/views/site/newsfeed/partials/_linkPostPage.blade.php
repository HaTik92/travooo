<?php
$ch = curl_init();

curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

$data = curl_exec($ch);
curl_close($ch);

// Load HTML to DOM Object
$dom = new \DOMDocument();
@$dom->loadHTML($data);

// Parse DOM to get Title
$nodes = $dom->getElementsByTagName('title');
if (count($nodes)) {
    $title = $nodes->item(0)->nodeValue;
} else {
    $title = '';
}

// Parse DOM to get Meta Description
$metas = $dom->getElementsByTagName('meta');
$body = "";
for ($i = 0; $i < $metas->length; $i ++) {
    $meta = $metas->item($i);
    if ($meta->getAttribute('name') == 'description') {
        $body = $meta->getAttribute('content');
    }
}

// Parse DOM to get Images
$image_urls = array();
$images = $dom->getElementsByTagName('canvas');

for ($i = 0; $i < $images->length; $i ++) {
    $image = $images->item($i);
    $src = $image->getAttribute('src');

    if (filter_var($src, FILTER_VALIDATE_URL)) {
        $image_urls[] = $src;
    }
}

$output = [
    'title' => $title,
    'image_src' => count($image_urls) ? $image_urls[0] : 'https://travooo.com/assets2/image/placeholders/pattern.png',
    'body' => $body
];
?>

<!-- Primary post block - text -->
<div class="post-block" >
    <div class="post-top-info-layer" >
        <div class="post-top-info-wrap" >
            <div class="post-top-avatar-wrap" >
                <a class="post-name-link" href="{{url('profile/'.@$post->author->id)}}" >
                    <img src="{{check_profile_picture(@$post->author->profile_picture)}}" alt="" >
                </a>
            </div>
            <div class="post-top-info-txt" >
                <div class="post-top-name" >
                    <a class="post-name-link" href="{{url('profile/'.@$post->author->id)}}" >{{@$post->author->name}}</a>
                    {!! get_exp_icon($post->author) !!}
                </div>
                <div class="post-info">{{diffForHumans($post->created_at)}} <i class="trav-globe-icon"></i>
                </div>
            </div>
        </div>
        <!-- New elements -->
        <div class="post-top-info-action" >
            <div class="dropdown" >
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <i class="trav-angle-down" ></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                    @if(Auth::check() && Auth::user()->id != $post->author->id)
                        @if(in_array($post->author->id, $followers))
                        <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $post->author->id}}" dir="auto">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-user-plus-icon" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Unfollow {{ $post->author->name ?? 'User' }}</b></p>
                                <p dir="auto">Stop seeing posts from {{ $post->author->name ?? 'User' }}</p>
                            </div>
                        </a>
                        @endif
                    @endif
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" dir="auto" href="#shareablePost" data-uniqueurl="{{ url()->full() }}" data-tab="shares{{$post->id}}" data-toggle="modal" data-id="{{$post->id}}" data-type="text">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Share</b></p>
                            <p dir="auto">Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item copy-newsfeed-link" data-text="" data-link="{{url('external/'. @$post->author->username, $post->id)}}">
                        <span class="icon-wrap">
                            <i class="trav-link" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Copy Link</b></p>
                            <p dir="auto">Paste the link anywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="link_{{$post->id}}" data-toggle="modal" data-target="" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-on-travo-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                            <p dir="auto">Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$post->id}},this)" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-flag-icon-o" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Report</b></p>
                            <p dir="auto">Help us understand</p>
                        </div>
                    </a>
                    @if(Auth::check() && Auth::user()->id == $post->author->id)
                    <a class="dropdown-item" data-toggle="modal" data-type="Other" data-element = "link_{{$post->id}}" href="#deletePostNew" data-id="{{$post->id}}">
                        <span class="icon-wrap">
                            <i class="far fa-trash-alt"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b style="color: red;">Delete</b></p>
                            <p>Delete this post forever</p>
                        </div>
                    </a>
                    @endif
                </div>
            </div>
        </div>
        <!-- New elements END -->
    </div>
    <!-- New element -->
    <div class="post-txt-wrap">
        {!! $post->text !!}
    </div>
    <div class="shared-link-details">
        <img src="{{$output['image_src']}}" alt="">
        <div class="bottom">
            <div class="text">
                <h4>{{$output['title']}}</h4>
                <p>{{$output['body']}}</p>
            </div>
            <a href="{{ $url }}" target="_blank">
                <i class="fas fa-external-link-alt"></i>
            </a>
        </div>
    </div>
     @php
    $variable = $post->id;
    $flag_liked  =false;
        if(count($post->likes)>0){
            foreach($post->likes as $like){
                if(Auth::check() && $like->users_id == Auth::user()->id)
                    $flag_liked = true;
            }
        }
    @endphp
    <!-- New element END -->
    <div class="post-footer-info" >
        @include('site.home.new.partials._comments-posts', ['post'=>$post, 'post_type'=>'text'])
    </div>
   
<div class="post-comment-layer" data-content="comments{{$post->id}}">

    <div class="post-comment-top-info">
        <ul class="comment-filter">
            <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
            <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
        </ul>
        <div class="comm-count-info">
            <strong class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 3? 3 : count($post->comments)}}</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
        </div>
    </div>
    <div class="ss post-comment-wrapper sortBody comments{{$post->id}}" id="comments{{$post->id}}">
        @if(count($post->comments)>0)
            @foreach($post->comments()->take(3)->get() AS $comment)
                @if(!user_access_denied($comment->author->id))
                    @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                @endif
            @endforeach
        @endif
    </div>
     @if(count($post->comments)>3)
        <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="post" data-id="{{$variable}}">Load more...</a>
    @endif

        @include('site.home.partials.new-comment_form_block', ['post_type'=>'post', 'post_id'=>$post->id])
</div>
</div>
<!-- Primary post block - text END -->
