<!-- Primary post block - text -->
@php
    $uniqueurl = url('trip-media/'. @$trip->author->username, $newsfeed_id);
@endphp
<div class="post-block" id="trip_media_{{$trip_plan->id}}">
    <div class="post-top-info-layer" >
        <div class="post-top-info-wrap" >
            <div class="post-top-avatar-wrap" >
                <a class="post-name-link" href="{{ url_with_locale('profile/'.$trip->author->id)}}" >
                    <img src="{{check_profile_picture($trip->author->profile_picture)}}" alt="" >
                </a>
            </div>
            <div class="post-top-info-txt" >
                <div class="post-top-name" >
                    <a class="post-name-link" href="{{ url_with_locale('profile/'.$trip->author->id)}}" >{{$trip->author->name}}</a>
                    {!! get_exp_icon($trip->author) !!}
                </div>
                <div class="post-info" >
                    Uploaded a <strong> @if(in_array($ext, $allowed_video)) video @else photo @endif</strong> in <a href="{{ route('place.index', @$place->id) }}" class="link-place">{{ @$place->trans[0]->title}}</a> {{diffForHumans(@$media->created_at)}}
                    <i class="trav-globe-icon"></i> 
                </div>
            </div>
        </div>
        <!-- New elements -->
        <div class="post-top-info-action" >
            <div class="dropdown" >
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <i class="trav-angle-down" ></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" >
                    @if(Auth::check() && Auth::user()->id != $trip->author->id)
                    @if(in_array($trip->author->id,$followers))
                    <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $trip->author->id}}" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-user-plus-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Unfollow {{ $trip->author->name ?? 'User' }}</b></p>
                            <p dir="auto">Stop seeing posts from {{ $trip->author->name ?? 'User' }}</p>
                        </div>
                    </a>
                    @endif
                    @endif
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$trip_plan->id}}" data-toggle="modal" data-id="{{$trip_plan->id}}" data-type="trip-media">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Share</b></p>
                            <p dir="auto">Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-link" dir="auto"></i>
                        </span>
                        <div class="drop-txt ">
                            <p dir="auto"><b dir="auto">Copy Link</b></p>
                            <p dir="auto">Paste the link anywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="trip_media_{{$trip_plan->id}}" data-toggle="modal" data-target="" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-on-travo-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                            <p dir="auto">Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlgNew" data-toggle="modal" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-flag-icon-o" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Report</b></p>
                            <p dir="auto">Help us understand</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- New elements END -->
    </div>
    <!-- New element -->
    <div class="post-image-container">

        <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;">
            @if(in_array($ext, $allowed_video)) 
                <li style="padding: 0;position: relative;margin:0 0 0 0;overflow: hidden;">
                    <video width="100%" height="auto" controls="">
                    <source src="{{@$media->url}}" type="video/mp4">
                    Your browser does not support the video tag.
                    </video>
                    <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                </li> 
            @else 
                <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;">
                    <a href="{{@$media->url}}" data-lightbox="media__post210172">
                            <img src="{{@$media->url}}"  alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                    </a>
                </li>
            @endif
        </ul>
        <div class="original-trip-plan" style="right: 0;">
            @if(isset($place->lat) && isset($place->lng))
                <img alt='Place' src='https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/static/{{$place->lng}},{{@$place->lat}},15,0.0,0/1000x600@2x?access_token=pk.eyJ1IjoiZHh0cmlhbiIsImEiOiJja2Zta3pyZzYwODkzMnZtZTMwZjlqbmhqIn0.BWDQFwk0-Mk6qZw59BgTtQ'>
            @else
                <img src="https://travooo.com/assets2/image/placeholders/place.png" alt="">
            @endif
            <a href="/trip/plan/{{$trip->id}}" target="_blank" class="view-trip">View Trip Plan</a>
        </div>

    </div>
        @php 
        $flag_liked  =false;
           if(count($trip->likes->where('places_id',$place->id))>0){
               foreach($trip->likes as $like){
                   if(Auth::check() && $like->users_id == Auth::user()->id)
                        $flag_liked = true;
               }
           }
    @endphp
    <!-- New element END -->
    <div class="post-footer-info" >
        <div class="post-foot-block post-reaction">
            <span class="post_like_button @if($flag_liked) liked @endif">
                <a href="#" class="btn-trip-like" data-id="{{$media->id}}" data-type="trip"  id="trips_{{$media->id}}">
                    <i class="trav-heart-fill-icon"></i>
                </a>
            </span>
            <span id="trip_like_count_{{$media->id}}" >
                <b>{{count($trip->likes->where('places_id',$place->id))}}</b> <a href="#usersWhoLike" data-toggle="modal" data-type="trip" data-id="{{$media->id}}">Likes</a>
            </span>
        </div>
         <div class="post-foot-block">
            <a href="#" data-id="{{$media->id}}" class="comment-btn" data-tab="comments{{$media->id}}">
                <i class="trav-comment-icon"></i>
            </a>
        @if(count($trip_media_details->comments) > 0)
            <ul class="foot-avatar-list">
               @php $us = array(); @endphp
               @foreach($trip_media_details->comments()->orderBy('created_at', 'desc')->get() AS $pc)
                   @if(!in_array($pc->users_id, $us))
                   @php $us[] = $pc->users_id; @endphp
                       @if(count($us)<4)
                           <li><img class="small-ava" src="{{check_profile_picture($pc->author->profile_picture)}}" alt="{{$pc->author->name}}" title="{{$pc->author->name}}"></li>
                        @else
                            breack;
                        @endif
                   @endif
               @endforeach
            </ul>
        @endif
            <span><a href="#" data-id="{{$media->id}}" class="comment-btn" data-tab="comments{{$trip_media_details->id}}"><span class="{{$trip_media_details->id}}-comments-count">{{count($trip_media_details->comments )}}</span> Comments</a></span>
        </div>
    </div>
    
        <div class="post-comment-layer" data-content="comments{{$trip_media_details->id}}">

        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
            </ul>
            <div class="comm-count-info">
                <strong class="{{$trip_media_details->id}}-opened-comments-count">{{count($trip_media_details->comments) > 3? 3 : count($trip_media_details->comments)}}</strong> / <span class="{{$trip_media_details->id}}-comments-count">{{count($trip_media_details->comments)}}</span>
            </div>
        </div>
        <div class="post-comment-wrapper" id="comments{{$trip_media_details->id}}">
            @if(count($trip_media_details->comments )>0)
                @foreach($trip_media_details->comments()->take(3)->get()  AS $comment)
                    @if(!user_access_denied($comment->author->id))
                        @include('site.home.partials.new-post_comment_block', ['post_id'=>$trip_media_details->id, 'type'=>'trip-media', 'comment'=>$comment])
                    @endif
                @endforeach
            @endif
        </div>
        @if(count($trip_media_details->comments)>3)
            <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="trip-media" data-id="{{$trip_media_details->id}}">Load more...</a>
        @endif

        @include('site.home.partials.new-comment_form_block', ['post_type'=>'trip-media', 'post_id'=>$trip_media_details->id])
    </div>

</div>
<!-- Primary post block - text END -->
