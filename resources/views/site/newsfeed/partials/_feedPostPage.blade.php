<!-- Primary post block - text -->
@php
    $uniqueurl = url('post/'. @$post->author->username, $post->id);
@endphp
@if ($post->author)
<div class="post-block">
    <div class="post-top-info-layer" >
        <div class="post-top-info-wrap" >
            <div class="post-top-avatar-wrap" >
                <a class="post-name-link" href="{{url('profile/'.$post->author->id)}}" >
                    <img src="{{check_profile_picture($post->author->profile_picture)}}" alt="" >
                </a>
            </div>
            <div class="post-top-info-txt" >
                <div class="post-top-name" >
                    <a class="post-name-link" href="{{url('profile/'.$post->author->id)}}" >{{$post->author->name}}</a>
                    @if($post->author)
                        {!! get_exp_icon($post->author) !!}
                    @endif
                </div>
                <div class="post-info" >
                    @if(isset($post->checkin[0]))
                        {!! $info_title !!}
                    @else
                        @if(count($media_array) == 1)
                            @php
                                $post_object = $media_array[0];
                                $file_url = $post_object->url;
                                $file_url_array = explode(".", $file_url);
                                $ext = end($file_url_array);
                                $allowed_video = array('mp4');
                            @endphp
                            @if(in_array($ext, $allowed_video))
                                Uploaded a <strong>video</strong> {{diffForHumans($post->created_at)}}
                            @else 
                                Uploaded a <strong>photo</strong> {{diffForHumans($post->created_at)}}
                            @endif
                        @elseif(count($media_array) >1)
                            Uploaded <strong> media </strong> {{diffForHumans($post->created_at)}}
                        @else
                            {{diffForHumans($post->created_at)}}
                        @endif
                    @endif
                    <i class="trav-globe-icon"></i> 
                    <!-- New element -->
                    <!-- New element END -->
                </div>
            </div>
        </div>
        <!-- New elements -->
        <div class="post-top-info-action" >
            <div class="dropdown" >
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <i class="trav-angle-down" ></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" >
                    @if(Auth::check())
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew">
                        <span class="icon-wrap" >
                            <i class="trav-user-plus-icon" ></i>
                        </span>
                        <div class="drop-txt" >
                            <p ><b >Unfollow User</b></p>
                            <p >Stop seeing posts from User</p>
                        </div>
                    </a>
                    @endif
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" dir="auto" href="#shareablePost" data-uniqueurl="{{ $uniqueurl }}" data-tab="shares{{$post->id}}" data-toggle="modal" data-id="{{$post->id}}" data-type="post">
                        <span class="icon-wrap" >
                            <i class="trav-share-icon" ></i>
                        </span>
                        <div class="drop-txt" >
                            <p ><b >Share</b></p>
                            <p >Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item copy-newsfeed-link" href="#" data-link="{{$uniqueurl}}">
                        <span class="icon-wrap" >
                            <i class="trav-link" ></i>
                        </span>
                        <div class="drop-txt" >
                            <p ><b >Copy Link</b></p>
                            <p >Paste the link anywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{!Auth::check()? 'open-login':''}}" href="#sendToFriendModal" data-id="post_{{$post->id}}" data-toggle="modal" data-target="" dir="auto">
                        <span class="icon-wrap" >
                            <i class="trav-share-on-travo-icon" ></i>
                        </span>
                        <div class="drop-txt" >
                            <p ><b >Send to a Friend</b></p>
                            <p >Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$post->id}},this)" dir="auto">
                        <span class="icon-wrap" >
                            <i class="trav-flag-icon-o" ></i>
                        </span>
                        <div class="drop-txt" >
                            <p ><b >Report</b></p>
                            <p >Help us understand</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- New elements END -->
    </div>
    <!-- New element -->
    <div class="post-txt-wrap">
       {!! $post->text !!}
    </div>
    @if(isset($post->checkin[0]))
        <div class="check-in-point d-block">
            <i class="trav-set-location-icon"></i> <strong>{{@$post->checkin[0]->place->transsingle->title}}</strong>
            @if(isset($post->checkin[0]->city->transsingle->title))
                , {{@$post->checkin[0]->city->transsingle->title}}, {{@$post->checkin[0]->country->transsingle->title}}
            @endif
            <i class="trav-clock-icon"></i>{{diffForHumans(@$post->checkin[0]->checkin_time)}}
        </div>
    @endif
    @php 
    $post_counter= 0;
    @endphp
    <div class="post-image-container">
        @if(count($media_array) == 1)
            @php 
                $post_object = $media_array[0];
                $file_url = $post_object->url;
                $file_url_array = explode(".", $file_url);
                $ext = end($file_url_array);
                $allowed_video = array('mp4');
            @endphp
            <!-- New element -->
            <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;">
                @if(in_array($ext, $allowed_video))
                    <li style="padding: 0;position: relative;margin:0 0 0 0;overflow: hidden;">
                        <video width="100%" height="auto" class="fullwidth">
                            <source src="{{$post_object->url}}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <a href="javascript:;" class="v-play-btn play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                    </li>
                @else
                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;">
                        <a href="{{$post_object->url}}" data-lightbox="media__post210172">
                            <img src="{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                        </a>
                    </li>
                @endif
            </ul>

        @elseif(count($media_array) == 2)
            <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;">
                @foreach ($media_array as $post_object)
                    @php
                        $file_url = $post_object->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp
                    @if(in_array($ext, $allowed_video))
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                            <video width="100%">
                                <source src="{{$post_object->url}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <a href="javascript:;" class="v-play-btn "><i class="fa fa-play" aria-hidden="true"></i></a>
                        </li>
                    @else
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                            <a href="{{$post_object->url}}" data-lightbox="media__post199366">
                                <img src="{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                            </a>
                        </li>
                    @endif
                @endforeach
                
                
            </ul>
        @elseif(count($media_array)  >2)
        <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;">
            @foreach ($media_array as $post_object)
                @php
                    $file_url = $post_object->url;
                    $file_url_array = explode(".", $file_url);
                    $ext = end($file_url_array);
                    $allowed_video = array('mp4');
                    $post_counter++;
                @endphp
                <!-- New element -->
                    @if(in_array($ext, $allowed_video))
                        @if( $post_counter  <=3)
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                <video width="100%">
                                    <source src="{{$post_object->url}}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                                <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                            </li>
                        @endif
                    @else
                        @if( $post_counter  <=3)
                                <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                    @if(count($media_array)>3 &&  $post_counter ==3)
                                        <a class="more-photos" href="" data-lightbox="media__post199366">{{count($media_array) -3 . ' More Photos'}}</a>
                                    @endif
                                        <a  href="{{$post_object->url}}" data-lightbox="media__post199366">
                                            <img src="{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                                        </a>
                                </li>
                        @endif
                    @endif
                <!-- New element END -->
            @endforeach
        </ul>
        @endif
    </div>
        @php
        $flag_liked  =false;
            if(count($post->likes)>0){
                foreach($post->likes as $like){
                    if(Auth::check() && $like->users_id == Auth::user()->id)
                        $flag_liked = true;
                }
            }
        @endphp
    <!-- New element END -->
    <div class="post-footer-info" >
         @include('site.home.new.partials._comments-posts', ['post'=>$post, 'post_type'=>'text'])
    </div>
  <div class="post-comment-layer" data-content="comments{{$post->id}}">

        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
            </ul>
            <div class="comm-count-info">
                <strong class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 3? 3 : count($post->comments)}}</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
            </div>
        </div>
        <div class="post-comment-wrapper sortBody comments{{$post->id}}" id="comments{{$post->id}}">
            @if(count($post->comments)>0)
                @foreach($post->comments()->take(3)->get() AS $comment)
                    @if(Auth::check() && !user_access_denied($comment->author->id))
                        @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                    @else
                        @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                    @endif
                   
                @endforeach
            @endif
        </div>
        @if(count($post->comments)>3)
            <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3"  data-type="post" data-id="{{$post->id}}">Load more...</a>
        @endif

        @include('site.home.partials.new-comment_form_block', ['post_type'=>'post', 'post_id'=>$post->id])
    </div>
</div>
@endif
<!-- Primary post block - text END -->
