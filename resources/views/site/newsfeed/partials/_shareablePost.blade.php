<?php
$type_text = '';
$sharedStr = '';
$postContent = '';
$uniqueurl = url()->full();
?>
@if(is_object($original_post ))
    @if($post_action == 'review')
        <div class="post-block " id="review_share_{{$original_post->posts_type_id}}">
            <div class="post-top-info-layer" >
                <div class="post-top-info-wrap" >
                    <div class="post-top-avatar-wrap" >
                        <img src="{{ check_profile_picture(@$sharing_user->profile_picture)}}" alt="" >
                    </div>
                    <div class="post-top-info-txt" >
                        <div class="post-top-name" >
                            <a class="post-name-link" href="{{url('profile/'.@$sharing_user->id)}}" > {{@$sharing_user->name}}</a>
                        </div>
                        <div class="post-info" >
                            shared  a <strong>Post</strong> {{diffForHumans($original_post->created_at)}}
                        </div>
                    </div>
                </div>
                <div class="post-top-info-action" dir="auto">
                    <div class="dropdown" dir="auto">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false" dir="auto">
                            <i class="trav-angle-down" dir="auto"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Place" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                            {{-- @if(Auth::check() && Auth::user()->id != @$review->author->id)
                                @if(in_array(@$review->author->id,$followers))
                                    <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $review->author->id}}" dir="auto">
                                        <span class="icon-wrap" dir="auto">
                                            <i class="trav-user-plus-icon" dir="auto"></i>
                                        </span>
                                        <div class="drop-txt" dir="auto">
                                            <p dir="auto"><b dir="auto">Unfollow User</b></p>
                                            <p dir="auto">Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                @endif
                            @endif --}}
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$original_post->posts_type_id}}" data-toggle="modal" data-id="{{$original_post->posts_type_id}}" data-type="{{$post_action}}">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Share</b></p>
                                    <p dir="auto">Spread the word</p>
                                </div>
                            </a>
                            <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-link" dir="auto"></i>
                                </span>
                                <div class="drop-txt">
                                    <p dir="auto"><b dir="auto">Copy Link</b></p>
                                    <p dir="auto">Paste the link anywhere you want</p>
                                </div>
                            </a>
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="review_share_{{$original_post->posts_type_id}}" data-toggle="modal" data-target="" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-on-travo-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                    <p dir="auto">Share with your friends</p>
                                </div>
                            </a>
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$original_post->posts_type_id}},this)" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-flag-icon-o" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Report</b></p>
                                    <p dir="auto">Help us understand</p>
                                </div>
                            </a>
                            {{-- @if(Auth::check() && Auth::user()->id == @$review->author->id)
                                <a class="dropdown-item" data-toggle="modal" data-type="review" data-element = "review_{{$review->id}}" href="#deletePostNew" data-id="{{$review->id}}">
                                    <span class="icon-wrap">
                                    <i class="far fa-trash-alt"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b style="color: red;">Delete</b></p>
                                        <p>Delete this post forever</p>
                                    </div>
                                </a>
                            @endif --}}
                        </div>
                    </div>
                </div>
            </div>
                {{$original_post->text}}
                @include('site.home.new.partials.shareable-rating', array('post'=>['type' =>'Review','variable'=> $original_post->posts_type_id])) 
            @php 
                $post = App\Models\Posts\Posts::find($original_post->posts_id);
                $flag_liked  =false;
                if(count($post->likes)>0){
                    foreach($post->likes as $like){
                        if(Auth::check() && $like->users_id == Auth::user()->id)
                            $flag_liked = true;
                    }
                }
            @endphp
            <div class="post-footer-info">
                @include('site.home.new.partials._comments-posts', ['post'=>$post, 'post_type'=>'text'])
            </div>
            <div class="post-comment-wrapper" id="following{{$post->id}}">
                @if(count($post->comments)>0)
                    @foreach($post->comments AS $comment)
                        @if(!user_access_denied($comment->author->id))
                            @php
                            if(!in_array($comment->users_id, $followers))
                                continue;
                            @endphp
                            @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="post-comment-layer" data-content="comments{{$post->id}}">

                <div class="post-comment-top-info">
                    <ul class="comment-filter">
                        <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                        <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                    </ul>
                    <div class="comm-count-info">
                        <strong class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 3? 3 : count($post->comments)}}</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
                    </div>
                </div>
                <div class="post-comment-wrapper comments{{$post->id}}" id="comments{{$post->id}}">
                    @if(count($post->comments)>0)
                        @foreach($post->comments()->take(3)->get() AS $comment)
                            @if(!user_access_denied($comment->author->id))
                                @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                            @endif
                        @endforeach
                    @endif
                </div>
                @if(count($post->comments)>3)
                    <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="post" data-id="{{$variable}}">Load more...</a>
                @endif
        
                @include('site.home.partials.new-comment_form_block', ['post_type'=>'post', 'post_id'=>$post->id])
            </div>
        </div>  

    @elseif($post_action == 'post')
        <div class="post-block " id="post_share_{{$original_post->posts_type_id}}">
            <div class="post-top-info-layer" style="border: none; margin-bottom: 0;">
                <div class="post-top-info-wrap" >
                    <div class="post-top-avatar-wrap" >
                        <img src="{{ check_profile_picture(@$sharing_user->profile_picture)}}" alt="" >
                    </div>
                    <div class="post-top-info-txt" >
                        <div class="post-top-name" >
                            <a class="post-name-link" href="{{url('profile/'.@$sharing_user->id)}}" > {{@$sharing_user->name}}</a>
                        </div>
                        <div class="post-info" >
                            shared  a <strong>Post</strong> {{diffForHumans($original_post->created_at)}}
                        </div>
                    </div>
                </div>
                <div class="post-top-info-action" dir="auto">
                    <div class="dropdown" dir="auto">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false" dir="auto">
                            <i class="trav-angle-down" dir="auto"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                            {{-- @if(Auth::check() && Auth::user()->id != @$review->author->id)
                                @if(in_array(@$review->author->id,$followers))
                                    <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $review->author->id}}" dir="auto">
                                        <span class="icon-wrap" dir="auto">
                                            <i class="trav-user-plus-icon" dir="auto"></i>
                                        </span>
                                        <div class="drop-txt" dir="auto">
                                            <p dir="auto"><b dir="auto">Unfollow User</b></p>
                                            <p dir="auto">Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                @endif
                            @endif --}}
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$original_post->posts_type_id}}" data-toggle="modal" data-id="{{$original_post->posts_type_id}}" data-type="{{$post_action}}">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Share</b></p>
                                    <p dir="auto">Spread the word</p>
                                </div>
                            </a>
                            <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-link" dir="auto"></i>
                                </span>
                                <div class="drop-txt">
                                    <p dir="auto"><b dir="auto">Copy Link</b></p>
                                    <p dir="auto">Paste the link anywhere you want</p>
                                </div>
                            </a>
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="post_share_{{$original_post->posts_type_id}}" data-toggle="modal" data-target="" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-on-travo-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                    <p dir="auto">Share with your friends</p>
                                </div>
                            </a>
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$original_post->posts_type_id}},this)" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-flag-icon-o" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Report</b></p>
                                    <p dir="auto">Help us understand</p>
                                </div>
                            </a>
                            {{-- @if(Auth::check() && Auth::user()->id == @$review->author->id)
                                <a class="dropdown-item" data-toggle="modal" data-type="review" data-element = "review_{{$review->id}}" href="#deletePostNew" data-id="{{$review->id}}">
                                    <span class="icon-wrap">
                                    <i class="far fa-trash-alt"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b style="color: red;">Delete</b></p>
                                        <p>Delete this post forever</p>
                                    </div>
                                </a>
                            @endif --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="post-txt-wrap">
                {{$original_post->text}}
                @include('site.home.new.partials.shareable-text', array('post'=>['type' =>'Post','variable'=> $original_post->posts_type_id])) 
            </div>
            @php 
                $post = App\Models\Posts\Posts::find($original_post->posts_id);
                $flag_liked  =false;
                if(count($post->likes)>0){
                    foreach($post->likes as $like){
                        if(Auth::check() && $like->users_id == Auth::user()->id)
                            $flag_liked = true;
                    }
                }
            @endphp
            <div class="post-footer-info">
                @include('site.home.new.partials._comments-posts', ['post'=>$post, 'post_type'=>'text'])
            </div>
            <div class="post-comment-wrapper" id="following{{$post->id}}">
                @if(count($post->comments)>0)
                    @foreach($post->comments AS $comment)
                        @if(!user_access_denied($comment->author->id))
                            @php
                            if(!in_array($comment->users_id, $followers))
                                continue;
                            @endphp
                            @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="post-comment-layer" data-content="comments{{$post->id}}">

                <div class="post-comment-top-info">
                    <ul class="comment-filter">
                        <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                        <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                    </ul>
                    <div class="comm-count-info">
                        <strong class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 3? 3 : count($post->comments)}}</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
                    </div>
                </div>
                <div class="post-comment-wrapper comments{{$post->id}}" id="comments{{$post->id}}">
                    @if(count($post->comments)>0)
                        @foreach($post->comments()->take(3)->get() AS $comment)
                            @if(!user_access_denied($comment->author->id))
                                @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                            @endif
                        @endforeach
                    @endif
                </div>
                @if(count($post->comments)>3)
                    <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="post" data-id="{{$variable}}">Load more...</a>
                @endif
        
                @include('site.home.partials.new-comment_form_block', ['post_type'=>'post', 'post_id'=>$post->id])
            </div>
        </div>  
    @elseif($post_action == 'discussion')
        <div class="post-block" id="discussion_share_{{$original_post->posts_type_id}}">
            <div class="post-top-info-layer" style="border: none; margin-bottom: 0;">
                <div class="post-top-info-wrap" >
                    <div class="post-top-avatar-wrap" >
                        <img src="{{ check_profile_picture(@$sharing_user->profile_picture)}}" alt="" >
                    </div>
                    <div class="post-top-info-txt" >
                        <div class="post-top-name" >
                            <a class="post-name-link" href="{{url('profile/'.@$sharing_user->id)}}" > {{@$sharing_user->name}}</a>
                        </div>
                        <div class="post-info" >
                            <a target="_black" href="{{ $uniqueurl }}">
                                shared  a <strong>Discussion</strong> {{diffForHumans($original_post->created_at)}}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="post-top-info-action" dir="auto">
                    <div class="dropdown" dir="auto">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false" dir="auto">
                            <i class="trav-angle-down" dir="auto"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                            {{-- @if(Auth::check() && Auth::user()->id != @$review->author->id)
                                @if(in_array(@$review->author->id,$followers))
                                    <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $review->author->id}}" dir="auto">
                                        <span class="icon-wrap" dir="auto">
                                            <i class="trav-user-plus-icon" dir="auto"></i>
                                        </span>
                                        <div class="drop-txt" dir="auto">
                                            <p dir="auto"><b dir="auto">Unfollow User</b></p>
                                            <p dir="auto">Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                @endif
                            @endif --}}
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$original_post->posts_type_id}}" data-toggle="modal" data-id="{{$original_post->posts_type_id}}" data-type="{{$post_action}}">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Share</b></p>
                                    <p dir="auto">Spread the word</p>
                                </div>
                            </a>
                            <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-link" dir="auto"></i>
                                </span>
                                <div class="drop-txt">
                                    <p dir="auto"><b dir="auto">Copy Link</b></p>
                                    <p dir="auto">Paste the link anywhere you want</p>
                                </div>
                            </a>
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="discussion_share_{{$original_post->posts_type_id}}" data-toggle="modal" data-target="" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-on-travo-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                    <p dir="auto">Share with your friends</p>
                                </div>
                            </a>
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$original_post->posts_type_id}},this)" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-flag-icon-o" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Report</b></p>
                                    <p dir="auto">Help us understand</p>
                                </div>
                            </a>
                            {{-- @if(Auth::check() && Auth::user()->id == @$review->author->id)
                                <a class="dropdown-item" data-toggle="modal" data-type="review" data-element = "review_{{$review->id}}" href="#deletePostNew" data-id="{{$review->id}}">
                                    <span class="icon-wrap">
                                    <i class="far fa-trash-alt"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b style="color: red;">Delete</b></p>
                                        <p>Delete this post forever</p>
                                    </div>
                                </a>
                            @endif --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="post-txt-wrap">
                {{$original_post->text}}
                @include('site.home.new.partials.shareable-discussion', array('post'=>[ 'type' =>'Discussion','variable'=> $original_post->posts_type_id])) 
            </div>
            @php 
                $post = App\Models\Posts\Posts::find($original_post->posts_id);
                $flag_liked  =false;
                if(count($post->likes)>0){
                    foreach($post->likes as $like){
                        if(Auth::check() && $like->users_id == Auth::user()->id)
                            $flag_liked = true;
                    }
                }
            @endphp
            <div class="post-footer-info">
                @include('site.home.new.partials._comments-posts', ['post'=>$post, 'post_type'=>'text'])
            </div>
            <div class="post-comment-wrapper" id="following{{$post->id}}">
                @if(count($post->comments)>0)
                    @foreach($post->comments AS $comment)
                        @if(!user_access_denied($comment->author->id))
                            @php
                            if(!in_array($comment->users_id, $followers))
                                continue;
                            @endphp
                            @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="post-comment-layer" data-content="comments{{$post->id}}">

                <div class="post-comment-top-info">
                    <ul class="comment-filter">
                        <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                        <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                    </ul>
                    <div class="comm-count-info">
                        <strong class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 3? 3 : count($post->comments)}}</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
                    </div>
                </div>
                <div class="post-comment-wrapper comments{{$post->id}}" id="comments{{$post->id}}">
                    @if(count($post->comments)>0)
                        @foreach($post->comments()->take(3)->get() AS $comment)
                            @if(!user_access_denied($comment->author->id))
                                @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                            @endif
                        @endforeach
                    @endif
                </div>
                @if(count($post->comments)>3)
                    <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="post" data-id="{{$variable}}">Load more...</a>
                @endif
                @include('site.home.partials.new-comment_form_block', ['post_type'=>'post', 'post_id'=>$post->id])
            </div>
        </div>
    @elseif($post_action == 'trip')
        <div class="post-block" id="trip_share_{{$original_post->posts_type_id}}">
            <div class="post-top-info-layer" >
                <div class="post-top-info-wrap" >
                    <div class="post-top-avatar-wrap" >
                        <img src="{{ check_profile_picture(@$sharing_user->profile_picture)}}" alt="" >
                    </div>
                    <div class="post-top-info-txt" >
                        <div class="post-top-name" >
                            <a class="post-name-link" href="{{url('profile/'.@$sharing_user->id)}}" > {{@$sharing_user->name}}</a>
                        </div>
                        <div class="post-info" >
                            shared  a <strong>Plan</strong> {{diffForHumans($original_post->created_at)}}
                        </div>
                    </div>
                </div>
                <div class="post-top-info-action" dir="auto">
                    <div class="dropdown" dir="auto">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false" dir="auto">
                            <i class="trav-angle-down" dir="auto"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Trip" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                            {{-- @if(Auth::check() && Auth::user()->id != @$review->author->id)
                                @if(in_array(@$review->author->id,$followers))
                                    <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $review->author->id}}" dir="auto">
                                        <span class="icon-wrap" dir="auto">
                                            <i class="trav-user-plus-icon" dir="auto"></i>
                                        </span>
                                        <div class="drop-txt" dir="auto">
                                            <p dir="auto"><b dir="auto">Unfollow User</b></p>
                                            <p dir="auto">Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                @endif
                            @endif --}}
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$original_post->posts_type_id}}" data-toggle="modal" data-id="{{$original_post->posts_type_id}}" data-type="{{$post_action}}">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Share</b></p>
                                    <p dir="auto">Spread the word</p>
                                </div>
                            </a>
                            <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-link" dir="auto"></i>
                                </span>
                                <div class="drop-txt">
                                    <p dir="auto"><b dir="auto">Copy Link</b></p>
                                    <p dir="auto">Paste the link anywhere you want</p>
                                </div>
                            </a>
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="trip_share_{{$original_post->posts_type_id}}" data-toggle="modal" data-target="" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-on-travo-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                    <p dir="auto">Share with your friends</p>
                                </div>
                            </a>
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$original_post->posts_type_id}},this)" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-flag-icon-o" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Report</b></p>
                                    <p dir="auto">Help us understand</p>
                                </div>
                            </a>
                            {{-- @if(Auth::check() && Auth::user()->id == @$review->author->id)
                                <a class="dropdown-item" data-toggle="modal" data-type="review" data-element = "review_{{$review->id}}" href="#deletePostNew" data-id="{{$review->id}}">
                                    <span class="icon-wrap">
                                    <i class="far fa-trash-alt"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b style="color: red;">Delete</b></p>
                                        <p>Delete this post forever</p>
                                    </div>
                                </a>
                            @endif --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="post-txt-wrap">
                {{$original_post->text}}
                @include('site.home.new.partials.shareable-tripplan', array('post'=>[ 'type' =>'Trip','variable'=> $original_post->posts_type_id])) 
            </div>
            @php 
                $post = App\Models\Posts\Posts::find($original_post->posts_id);
                $flag_liked  =false;
                if(count($post->likes)>0){
                    foreach($post->likes as $like){
                        if(Auth::check() && $like->users_id == Auth::user()->id)
                            $flag_liked = true;
                    }
                }
            @endphp
            <div class="post-footer-info">
                @include('site.home.new.partials._comments-posts', ['post'=>$post, 'post_type'=>'text'])
            </div>
            <div class="post-comment-wrapper" id="following{{$post->id}}">
                @if(count($post->comments)>0)
                    @foreach($post->comments AS $comment)
                        @if(!user_access_denied($comment->author->id))
                            @php
                            if(!in_array($comment->users_id, $followers))
                                continue;
                            @endphp
                            @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="post-comment-layer" data-content="comments{{$post->id}}">

                <div class="post-comment-top-info">
                    <ul class="comment-filter">
                        <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                        <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                    </ul>
                    <div class="comm-count-info">
                        <strong class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 3? 3 : count($post->comments)}}</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
                    </div>
                </div>
                <div class="post-comment-wrapper comments{{$post->id}}" id="comments{{$post->id}}">
                    @if(count($post->comments)>0)
                        @foreach($post->comments()->take(3)->get() AS $comment)
                            @if(!user_access_denied($comment->author->id))
                                @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                            @endif
                        @endforeach
                    @endif
                </div>
                @if(count($post->comments)>3)
                    <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="post" data-id="{{$variable}}">Load more...</a>
                @endif
        
                @include('site.home.partials.new-comment_form_block', ['post_type'=>'post', 'post_id'=>$post->id])
            </div>
        </div>   
    @elseif($post_action == 'report')
        <div class="post-block" id="trip_share_{{$original_post->posts_type_id}}">
            <div class="post-top-info-layer" >
                <div class="post-top-info-wrap" >
                    <div class="post-top-avatar-wrap" >
                        <img src="{{ check_profile_picture(@$sharing_user->profile_picture)}}" alt="" >
                    </div>
                    <div class="post-top-info-txt" >
                        <div class="post-top-name" >
                            <a class="post-name-link" href="{{url('profile/'.@$sharing_user->id)}}" > {{@$sharing_user->name}}</a>
                        </div>
                        <div class="post-info" >
                            shared  a <strong>Report</strong> {{diffForHumans($original_post->created_at)}}
                        </div>
                    </div>
                </div>
                <div class="post-top-info-action" dir="auto">
                    <div class="dropdown" dir="auto">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false" dir="auto">
                            <i class="trav-angle-down" dir="auto"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Report" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                            {{-- @if(Auth::check() && Auth::user()->id != @$review->author->id)
                                @if(in_array(@$review->author->id,$followers))
                                    <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $review->author->id}}" dir="auto">
                                        <span class="icon-wrap" dir="auto">
                                            <i class="trav-user-plus-icon" dir="auto"></i>
                                        </span>
                                        <div class="drop-txt" dir="auto">
                                            <p dir="auto"><b dir="auto">Unfollow User</b></p>
                                            <p dir="auto">Stop seeing posts from User</p>
                                        </div>
                                    </a>
                                @endif
                            @endif --}}
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$original_post->posts_type_id}}" data-toggle="modal" data-id="{{$original_post->posts_type_id}}" data-type="{{$post_action}}">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Share</b></p>
                                    <p dir="auto">Spread the word</p>
                                </div>
                            </a>
                            <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-link" dir="auto"></i>
                                </span>
                                <div class="drop-txt">
                                    <p dir="auto"><b dir="auto">Copy Link</b></p>
                                    <p dir="auto">Paste the link anywhere you want</p>
                                </div>
                            </a>
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="trip_share_{{$original_post->posts_type_id}}" data-toggle="modal" data-target="" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-share-on-travo-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                    <p dir="auto">Share with your friends</p>
                                </div>
                            </a>
                            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$original_post->posts_type_id}},this)" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-flag-icon-o" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Report</b></p>
                                    <p dir="auto">Help us understand</p>
                                </div>
                            </a>
                            {{-- @if(Auth::check() && Auth::user()->id == @$review->author->id)
                                <a class="dropdown-item" data-toggle="modal" data-type="review" data-element = "review_{{$review->id}}" href="#deletePostNew" data-id="{{$review->id}}">
                                    <span class="icon-wrap">
                                    <i class="far fa-trash-alt"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b style="color: red;">Delete</b></p>
                                        <p>Delete this post forever</p>
                                    </div>
                                </a>
                            @endif --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="post-txt-wrap">
                {{$original_post->text}}
                @include('site.home.new.partials.shareable-report', array('post'=>[ 'type' =>'Report','variable'=> $original_post->posts_type_id])) 
            </div>
            @php 
                $post = App\Models\Posts\Posts::find($original_post->posts_id);
                $flag_liked  =false;
                if(count($post->likes)>0){
                    foreach($post->likes as $like){
                        if(Auth::check() && $like->users_id == Auth::user()->id)
                            $flag_liked = true;
                    }
                }
            @endphp
            <div class="post-footer-info">
                @include('site.home.new.partials._comments-posts', ['post'=>$post, 'post_type'=>'text'])
            </div>
            <div class="post-comment-wrapper" id="following{{$post->id}}">
                @if(count($post->comments)>0)
                    @foreach($post->comments AS $comment)
                        @if(!user_access_denied($comment->author->id))
                            @php
                            if(!in_array($comment->users_id, $followers))
                                continue;
                            @endphp
                            @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="post-comment-layer" data-content="comments{{$post->id}}">

                <div class="post-comment-top-info">
                    <ul class="comment-filter">
                        <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                        <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                    </ul>
                    <div class="comm-count-info">
                        <strong class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 3? 3 : count($post->comments)}}</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
                    </div>
                </div>
                <div class="post-comment-wrapper comments{{$post->id}}" id="comments{{$post->id}}">
                    @if(count($post->comments)>0)
                        @foreach($post->comments()->take(3)->get() AS $comment)
                            @if(!user_access_denied($comment->author->id))
                                @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                            @endif
                        @endforeach
                    @endif
                </div>
                @if(count($post->comments)>3)
                    <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="post" data-id="{{$variable}}">Load more...</a>
                @endif
        
                @include('site.home.partials.new-comment_form_block', ['post_type'=>'post', 'post_id'=>$post->id])
            </div>
        </div>   
    @endif

@endif
