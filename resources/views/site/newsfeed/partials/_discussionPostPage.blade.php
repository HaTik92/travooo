@section('after_styles')
    <link rel="stylesheet" href="{{asset('/dist-parcel/assets/Discussion/re-polishing/aea-2029.css')}}">
@endsection
<!-- Discussion post block - text -->
@php
    $uniqueurl = url('discussion/'. $discussion->author->username, $discussion->id);
@endphp
<div class="post-block discussion-page" >
    <div class="post-top-info-layer" >
        <div class="post-top-info-wrap" >
            <div class="post-top-avatar-wrap" >
                <a class="post-name-link" href="{{ url_with_locale('profile/'.$discussion->author->id)}}" >
                    <img src="{{check_profile_picture($discussion->author->profile_picture)}}" alt="" >
                </a>
            </div>
            <div class="post-top-info-txt" >
                <div class="post-top-name" >
                    <a class="post-name-link" href="{{ url_with_locale('profile/'.$discussion->author->id)}}" >{{$discussion->author->name}}</a>
                    {!! get_exp_icon($discussion->author) !!}
                </div>
                <div class="post-info">
                    Asked for tips about <a href="{{url_with_locale(strtolower($discussion->destination_type)."/".$discussion->destination_id)}}" class="link-place">{{getDiscussionDestination($discussion->destination_type, $discussion->destination_id)}}</a>
                    on {{diffForHumans($discussion->created_at)}} <i class="trav-globe-icon"></i>
                </div>
            </div>
        </div>
        <div class="post-top-info-action" dir="auto">
           <div class="dropdown" dir="auto">
               <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"      aria-expanded="false" dir="auto">
                   <i class="trav-angle-down" dir="auto"></i>
               </button>
               <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                   @if(Auth::check() && Auth::user()->id != $discussion->author->id)
                       @if(in_array($discussion->author->id,$followers))
                           <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $discussion->author->id}}" dir="auto">
                               <span class="icon-wrap" dir="auto">
                                   <i class="trav-user-plus-icon" dir="auto"></i>
                               </span>
                               <div class="drop-txt" dir="auto">
                                   <p dir="auto"><b dir="auto">Unfollow {{ $discussion->author->name ?? 'User' }}</b></p>
                                   <p dir="auto">Stop seeing posts from {{ $discussion->author->name ?? 'User' }}</p>
                               </div>
                           </a>
                       @endif
                   @endif
                   <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$discussion->id}}" data-toggle="modal" data-id="{{$discussion->id}}" data-type="discussion">
                       <span class="icon-wrap" dir="auto">
                           <i class="trav-share-icon" dir="auto"></i>
                       </span>
                       <div class="drop-txt" dir="auto">
                           <p dir="auto"><b dir="auto">Share</b></p>
                           <p dir="auto">Spread the word</p>
                       </div>
                   </a>
                   <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                       <span class="icon-wrap" dir="auto">
                           <i class="trav-link" dir="auto"></i>
                       </span>
                       <div class="drop-txt" dir="auto">
                           <p dir="auto"><b dir="auto">Copy Link</b></p>
                           <p dir="auto">Paste the link anywhere you want</p>
                       </div>
                   </a>
                   <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="discussion_{{$discussion->id}}" data-toggle="modal" data-target="" dir="auto">
                       <span class="icon-wrap" dir="auto">
                           <i class="trav-share-on-travo-icon" dir="auto"></i>
                       </span>
                       <div class="drop-txt" dir="auto">
                           <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                           <p dir="auto">Share with your friends</p>
                       </div>
                   </a>
                   <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$discussion->id}},this)" dir="auto">
                       <span class="icon-wrap" dir="auto">
                           <i class="trav-flag-icon-o" dir="auto"></i>
                       </span>
                       <div class="drop-txt" dir="auto">
                           <p dir="auto"><b dir="auto">Report</b></p>
                           <p dir="auto">Help us understand</p>
                       </div>
                   </a>

                   @if(Auth::check() && Auth::user()->id==$discussion->author->id)
                       <a class="dropdown-item" href="#" onclick="discussion_delete('{{$discussion->id}}', event, true)">
                            <span class="icon-wrap">
                                <img src="{{asset('assets2/image/delete.svg')}}" style="width:20px;">
                            </span>
                           <div class="drop-txt">
                               <p><b>@lang('buttons.general.crud.delete')</b></p>
                               <p style="color:red">@lang('home.remove_this_post')</p>
                           </div>
                       </a>
                   @endif

               </div>
           </div>
       </div>
    </div>
    <!-- Discussion wrapper -->
    <div class="discussion-wrapper">
        <!-- New element -->
        <div class="post-txt-wrap">
            <h3>{!! $discussion->question !!}</h3>
            <p>{!! $discussion->description !!}</p>
        </div>
        <div class="disc-media-block">
            @if($discussion->medias)
                    @foreach($discussion->medias as $media)
                        @php
                            $file_url = @$media->media_url;
                            $file_url_array = explode(".", $file_url);
                            $ext = end($file_url_array);
                            $allowed_video = array('mp4');
                        @endphp
                        @if(in_array($ext, $allowed_video))
                            <a href="javascript:;" class="disc-media-wrap disc-video-wrap video-link" data-video="{{$file_url}}"
                               data-toggle="modal" data-target="#videoModal">
                                <video width="122" height="65">
                                    <source src="{{$file_url}}" type="video/mp4">
                                </video>
                                <i class="fa fa-play" aria-hidden="true" dir="auto"></i>
                            </a>
                        @else
                            <a href="{{$file_url}}" data-lightbox="ask__media">
                                <img src="{{$file_url}}" alt="image" class="disc-media-wrap">
                            </a>
                        @endif
                    @endforeach
            @endif
        </div>
        <div class="post-modal-content-foot">
            <div class="post-modal-foot-list">
                <span>{!!discussionDateFormat($discussion->created_at)!!}</span>
                <span class="dot"> · </span>
                <span dir="auto">Views <b><span class="{{$discussion->id}}-view-count">{{count($discussion->views)}}</span></b></span>
                <span class="dot"> · </span>
                <span dir="auto">Tagged <b>{{count($discussion->experts)}}</b></span>
                <div class="disc-experts-block" dir="auto">
                    @foreach($discussion->experts()->take(12)->get() as $expert)
                    <a href="{{url('profile', $expert->user->id)}}" target="_blank" class="exp-user-link">
                        <img src="{{check_profile_picture($expert->user->profile_picture)}}" data-toggle="bs-tooltip" title="" data-original-title="{{$expert->user->name}}">
                    </a>
                    @endforeach
                </div>
            </div>
            @if($discussion->topic)
            <div class="disc-topic-text">
                {{str_replace(",",", ",$discussion->topic)}}
            </div>
            @endif
        </div>
        <!-- New element END -->
    </div>
    <!-- Discussion text wrapper END -->
    <div class="post-footer-info disscusion--footer" >
        <!-- Updated elements -->
        <div class="post-foot-block post-reaction">

            <div class="tips-footer updownvote " id="updown_{{$discussion->id}}"  dir="auto">
                <a href="#" class="discussion-vote-link {{Auth::check() ? 'up' : 'open-login'}} {{(Auth::check() && $discussion->discussion_upvotes()->where('users_id', Auth::user()->id)->first())?'':'disabled'}} upvote_{{$discussion->id}}" data-id="{{$discussion->id}}" id="upvote_{{$discussion->id}}" data-type="up" dir="auto">
                    <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-up" style="padding-left: 2px;" dir="auto"></i></span>
                </a>
                <span><b class="upvote-count  upvote-count_{{$discussion->id}}" dir="auto">{{ optimize_counter(count($discussion->discussion_upvotes))}}</b></span>

                &nbsp;&nbsp;
                <a  href="#" class="discussion-vote-link {{Auth::check() ? 'down' : 'open-login'}}  {{(Auth::check() &&  $discussion->discussion_downvotes()->where('users_id', Auth::user()->id)->first())?'':'disabled'}} downvote_{{$discussion->id}}" data-id="{{$discussion->id}}" id="downvote_{{$discussion->id}}" data-type="down" dir="auto">
                    <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-down" style="padding-left: 2px;" dir="auto"></i></span>
                </a>
                <span><b class="upvote-count downvote-count_{{$discussion->id}}" dir="auto">{{ optimize_counter(count($discussion->discussion_downvotes))}}</b></span>
            </div>
        </div>
        @if(count($discussion->replies) > 0)
            <div class="post-foot-block">
                <div class="tips-footer">
                    <div class="more-tips">
                        <ul class="avatar-list">
                            @php $us_list = array(); @endphp
                            @foreach ($discussion->replies as $reply)
                                @if(!in_array($reply->author->id, $us_list))
                                    @php $us_list[] = $reply->author->id; @endphp
                                    @if(count($us_list)<4)
                                        <li><img src="{{check_profile_picture($reply->author->profile_picture)}}" class="small-ava"></li>
                                    @else
                                        @php break; @endphp
                                    @endif
                                @endif
                            @endforeach
                        </ul>
                        <a href="javascript:;"><b>{{count($discussion->replies)}}</b> tips</a>
                    </div>
                </div>
            </div>
        @endif
        <div class="post-foot-block ml-auto">
                <span class="post_share_buttons" id="{{$discussion->id}}">
                    <a  href="#shareablePost" data-id="{{$discussion->id}}" data-type="discussion" data-toggle="modal"  data-toggle="modal" data-target="#sharePostModal">
                        <img class='share--image' src="{{asset('assets2/image/share-mobile.png')}}">
                    </a>
                </span>
            <span id="post_share_count_{{$discussion->id}}">
                    <a href="#shareablePost" class="{{Auth::check() ? '' : 'open-login'}}" data-id="{{$discussion->id}}" data-type="discussion" data-toggle="modal" ><strong>0</strong>
                        Shares</a>
                </span>
        </div>

    </div>

    <div class="post-comment-layer post-tips-top-layer post-discussion-reply-block" dir="auto">
        <div class="post-tips-main-block reply-block-{{$discussion->id}}" id="discussioReply{{$discussion->id}}">
            @if(count($discussion->replies)>0)
                @foreach($discussion->replies()->where('parents_id', 0)->orderBy('created_at', 'desc')->take(3)->get() AS
                 $reply)
                    @include('site.discussions.partials._reply-block', ['is_single_page'=>true])
                @endforeach
            @endif
        </div>
        @if(count($discussion->replies)>3)
        <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="discussion" data-id="{{$discussion->id}}">Load more...</a>
        @endif
        
         @include('site.home.partials.new-comment_form_block', ['post_type'=>'discussion', 'post' => $discussion, 'post_id'=>$discussion->id, 'placeholder'=>'Write a tip ...'])
    </div>
</div>

<script>
    $(function () {
        $('[data-toggle="bs-tooltip"]').tooltip()
    })
</script>
@include('site.discussions.partials._view-scripts')
