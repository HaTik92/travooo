<!-- Primary post block - trip plan -->
@php
    $uniqueurl = url()->full();
@endphp
<div class="post-block" id="trip_{{$trip_plan->id}}">
    <div class="post-top-info-layer" >
        <div class="post-top-info-wrap" >
            <div class="post-top-avatar-wrap" >
                <a class="post-name-link" href="{{url('profile/'.$author->id)}}" >
                    <img src="{{check_profile_picture($author->profile_picture)}}" alt="" >
                </a>
            </div>
            <div class="post-top-info-txt" >
                <div class="post-top-name" >
                    <a class="post-name-link" href="{{url('profile/'.$author->id)}}" >{{$author->name}}</a>
                    {!! get_exp_icon($author) !!}
                </div>
                <div class="post-info" >
                    {{-- 11111  || id : {{$trip_plan->id}} --}}
                    @if($trip_plan->memory !=0)
                        @if($post_action == 'plan_updated') Edited @else Posted a @endif <strong>Memory Trip</strong> 
                    @else 
                        @if($post_action == 'plan_updated') Edited @else Planning a @endif <strong>Upcoming Trip</strong>
                    @endif
                    {{ count($places) }} Destinations @if($country_title) in <strong>{{$country_title}} </strong> @else in <strong>{{ count($countries) }} countries</strong> @endif on {{diffForHumans($log_time)}}
                </div>
            </div>
        </div>
        <div class="post-top-info-action" dir="auto">
            <div class="dropdown" dir="auto">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"      aria-expanded="false" dir="auto">
                    <i class="trav-angle-down" dir="auto"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                    @if(Auth::check() && Auth::user()->id != $author->id)
                        @if(in_array($author->id,$followers))
                            <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $author->id}}" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-user-plus-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Unfollow {{ $author->name ?? 'User' }}</b></p>
                                    <p dir="auto">Stop seeing posts from {{ $author->name ?? 'User' }}</p>
                                </div>
                            </a>
                        @endif
                    @endif
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$trip_plan->id}}" data-toggle="modal" data-id="{{$trip_plan->id}}" data-type="trip">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Share</b></p>
                            <p dir="auto">Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item copy-newsfeed-link" data-text="" data-link="{{ $uniqueurl }}">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-link" dir="auto"></i>
                        </span>
                        <div class="drop-txt">
                            <p dir="auto"><b dir="auto">Copy Link</b></p>
                            <p dir="auto">Paste the link anyywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="trip_{{$trip_plan->id}}" data-toggle="modal" data-target="" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-on-travo-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                            <p dir="auto">Share with your friends</p>
                        </div>
                    </a> 
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlgNew" data-toggle="modal" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-flag-icon-o" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Report</b></p>
                            <p dir="auto">Help us understand</p>
                        </div>
                    </a>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="post-image-container" >
        <!-- New element -->
        <div class="trip-plan-container arrow--opacity">
                <div class="trip-plan-map">
                    <img style='width:100%;height:400px;' src="{{tripPlanThumb($trip_plan, 615, 400)}}">
                    <a href="/trip/plan/{{$trip_plan->id}}?do=edit"  class="btn btn-light-primary trip-plan-btn" style="color:white">View Plan</a>
                </div>
                <div class="trip-plan-slider-container">
                    <a  class="trip-plan-home-slider{{$mapindex}}prev trip-plan-home-slider-prev"></a>
                    <a  class="trip-plan-home-slider{{$mapindex}}next trip-plan-home-slider-next "></a>
                    <div class="trip-plan-home-slider{{$mapindex}}" style="margin-left: 5px;">
                        @foreach ($locationData as $key=>$item)
                            @if($key==0)
                                <div class="trip-plan-home-slider-item active">
                                    <img src="{{ $item['image'] }}" alt="">
                                    <div class="title"> 
                                        {{$item['title']}} 
                                        @if ($item['type'] != 'place')
                                           <br> {{ $item['total_destination'] }}  Destination
                                        @endif
                                    </div>
                                </div>
                            @elseif($key == array_key_last($places))
                                <div class="trip-plan-home-slider-item ss" style="margin-right: 0px !important">
                                    <img src="{{ $item['image'] }}" alt="">
                                    <div class="title"> 
                                        {{$item['title']}} 
                                        @if ($item['type'] != 'place')
                                           <br> {{ $item['total_destination'] }}  Destination
                                        @endif
                                    </div>
                                </div>
                            @else 
                                <div class="trip-plan-home-slider-item">
                                    <img src="{{ $item['image'] }}" alt="">
                                    <div class="title"> 
                                        {{$item['title']}} 
                                        @if ($item['type'] != 'place')
                                           <br> {{ $item['total_destination'] }}  Destination
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
        </div>
        <!-- New element END -->
    </div>
    @php
        $flag_liked  =false;
        if(count($trip_plan->likes->where('places_id',null))>0){
            foreach($trip_plan->likes as $like){
                if(Auth::check() && $like->users_id == Auth::user()->id)
                     $flag_liked = true;
            }
        }
    @endphp
    <div class="post-footer-info">
         @include('site.home.new.partials._comments-posts', ['post'=>$trip_plan, 'post_type'=>'trip'])
    </div>
        <div class="post-comment-layer" data-content="comments{{$trip_plan->id}}">

            <div class="post-comment-top-info">
                <ul class="comment-filter">
                    <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                    <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                </ul>
                <div class="comm-count-info">
                    <strong class="{{$trip_plan->id}}-opened-comments-count">{{count($trip_plan->comments) > 3? 3 : count($trip_plan->comments)}}</strong> / <span class="{{$trip_plan->id}}-comments-count">{{count($trip_plan->comments)}}</span>
                </div>
            </div>
            <div class="ss post-comment-wrapper sortBody" id="comments{{$trip_plan->id}}">
                @if(count($trip_plan->comments)>0)
                    @foreach($trip_plan->comments()->take(3)->get() AS $comment)
                        @if(!user_access_denied($comment->author->id))
                            @include('site.home.partials.new-post_comment_block', ['post_id'=>$trip_plan->id, 'type'=>'trip', 'comment'=>$comment])
                        @endif
                    @endforeach
                @endif
            </div>
            @if(count($trip_plan->comments)>3)
                <a href="javascript:;" class="load-more-comment-link" data-type="trip" pagenum="0" comskip="3" data-id="{{$trip_plan->id}}">Load more...</a>
            @endif
        
          @include('site.home.partials.new-comment_form_block', ['post_type'=>'trip', 'post_id'=>$trip_plan->id, 'post' => $trip_plan])
        </div>
<!-- Removed comments block -->
</div>
<!-- Primary post block - trip plan END -->
@include('site.home.common_trip_script')
