<!-- Secondary post type for events -->
@php
    $uniqueurl = url()->full();
@endphp
<div class="post-block post-block-notification" id="event_{{$evt->id}}">
    <div class="post-top-info-layer">
        <div class="post-info-line">
            Event in <a class="post-name-link" href="{{url($dest_type.'/'.@$loc->id)}}">{{ @$loc->trans[0]->title }}</a>
        </div>
        <div class="post-top-info-action" >
            <div class="dropdown" >
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <i class="trav-angle-down" ></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Event" >
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$evt->id}}" data-toggle="modal" data-id="{{$evt->id}}" data-type="event">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Share</b></p>
                            <p dir="auto">Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item" data-text="" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-link" dir="auto"></i>
                        </span>
                        <div class="drop-txt copy-newsfeed-link" data-link="{{$uniqueurl}}">
                            <p dir="auto"><b dir="auto">Copy Link</b></p>
                            <p dir="auto">Paste the link anywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="event_{{$evt->id}}" data-toggle="modal" data-target="" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-on-travo-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                            <p dir="auto">Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$evt->id}},this)" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-flag-icon-o" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Report</b></p>
                            <p dir="auto">Help us understand</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- New elements -->
    <div class="post-event-hero">
        <img class="post-event-hero-img" src="{{asset('assets2/image/events/festivals/4.jpg')}}" alt="">
        <div class="post-event-hero-caption">
            <div class="event-theme-badge">{{$evt->category}}</div>
            <h3 class="event-title" style="color: white;">
                <span>{{date('j', strtotime($evt->end)) - date('j', strtotime($evt->start))}} days event, <strong>{{date('j M', strtotime($evt->start))}} - {{date('j M', strtotime($evt->end))}}</strong></span>
                {{$evt->title}}
            </h3>
        </div>
    </div>
    <div class="post-event-details">
        <img src="https://travooo.com/assets2/image/placeholders/place.png" alt="" class="post-event-details-map">
        <div class="post-event-details-text">
            <p><i class="trav-bookmark-icon"></i> By <strong>Avenevv Pte. Ltd.</strong></p>
            <p><i class="trav-set-location-icon"></i> <strong>{{ @$evt->address }}</strong></p>
            <p><i class="trav-star-icon"></i> <strong>@if(isset($loc->reviews)) {{$loc->reviews->count() }} @endif</strong> Reviews</p>
        </div>
        @if($url !='')
        <div class="actions" style="margin-left: 180px;">            
            <a href="{{$url}}" target="_blank" style="    background: rgb(255, 133, 33); border-color: rgb(255, 133, 33);" class="btn btn-light-primary orange">BOOK</a>
        </div>
        @endif
    </div>
    @php
    $flag_liked  =false;
    if(count($evt->likes)>0){
    foreach($evt->likes as $like){
    if(Auth::check() && $like->users_id == Auth::user()->id)
    $flag_liked = true;
    }
    }
    @endphp
    <!-- New elements END -->
    <div class="post-footer-info" >
        @include('site.home.new.partials._comments-posts', ['post'=>$evt, 'post_type'=>'event'])
    </div>
    <div class="post-comment-layer" data-content="comments{{$evt->id}}">

        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
            </ul>
            <div class="comm-count-info">
                <strong  class="{{$evt->id}}-opened-comments-count">{{count($evt->comments) > 3?3:count($evt->comments)}}</strong> / <span class="{{$evt->id}}-comments-count">{{count($evt->comments)}}</span>
            </div>
        </div>
        <div class="ss post-comment-wrapper sortBody comments{{$evt->id}}" id="comments{{$evt->id}}">
            @if(count($evt->comments)>0)
                @foreach($evt->comments()->take(3)->get() AS $comment)
                    @if(!user_access_denied($comment->author->id))
                        @include('site.home.partials.new-post_comment_block', ['post' => $evt, 'post_id'=>$evt->id, 'type'=>'event', 'comment'=>$comment])
                    @endif
                @endforeach
            @endif
        </div>@if(count($evt->comments)>3)
        <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="event" data-id="{{$evt->id}}">Load more...</a>
        @endif

        @include('site.home.partials.new-comment_form_block', ['post_type'=>'event', 'post_id'=>$evt->id])
    </div>
</div>
<!-- Secondary post type for events END -->
