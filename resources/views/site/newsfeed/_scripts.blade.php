<link href="{{ asset('/plugins/emoji/emojionearea_custom.css') }}" rel="stylesheet"/>
<script data-cfasync="false" src="{{ asset('/plugins/emoji/emojionearea.js?v='.time()) }}" type="text/javascript"></script>
<script>
    // For spam reporting
    function injectData(id, obj)
    {
        // var button = $(event.relatedTarget);
        var posttype = $(obj).parent().attr("posttype");
        $("#spamReportDlg").find("#dataid").val(id);
        $("#spamReportDlg").find("#posttype").val(posttype);
    }


    $(document).ready(function () {
        Comment_Show.init($('.post-block'));
         
        CommentEmoji($('.postCommentForm'));
        $('.emojionearea-button').removeClass('d-none');
        $('.note-placeholder').addClass('custom-placeholder');
    });
    
    setTimeout(function () {
        $('#placeAsideAnimation').remove()
        $('#placeAsideBlock').removeClass('d-none')
    }, 2100);
        
       
    $(document).on('click', ".post_like_button a", function (e) {
        e.preventDefault();
        $(this).closest('.post_like_button').toggleClass('liked');
    });
    
    $('body').on('click', '.post_like_button a', function (e) {
        postId = $(this).attr('id');
        type = $(this).data('type');
        if(type == 'trip'){
            postId = $(this).data('id');
            $.ajax({
                url: "{{route('trip.likeunlike')}}",
                type: "POST",
                data: { type: type, id: postId},
                dataType: "json",
                success: function(resp, status){
                    if(resp.status ='yes'){
                        $('#trip_like_count_' + postId).html('<a href="#usersWhoLike" data-toggle="modal" class="trip_like_button" id="' + postId + '" data-id="'+postId+'" data-type="trip"><b>' + resp.count + '</b> Likes</a>');
                    }
                },
                error: function(){}
            });
            
        }else if(type == 'report'){
            
            $.ajax({
                method: "POST",
                url: "{{ route('report.like') }}",
                data: {post_id: postId}
            })
            .done(function (res) {
                var resp = JSON.parse(res);
                $('#report_like_count_' + postId).html('<a href="#usersWhoLike" data-toggle="modal" class="report_like_button" id="' + postId + '" data-id="'+postId+'" data-type="report"><b>' + resp.count + '</b> Likes</a>');
            });
        }else if(type == 'review'){
            value_vote = $(this).data('value');
            $.ajax({
                method: "POST",
                url: "{{route('place.revirews_updownvote')}}",
                data: {review_id: postId, vote_type:value_vote}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if(result.status ='yes')
                        $('#review_like_count_' + postId).html('<a  data-toggle="modal" class="trip_like_button" id="' + postId + '" data-id="'+postId+'" data-value="" data-type="review"><b>' + result.count_upvotes + '</b> Was this helpful?</a>');
                });
            e.preventDefault();
        }else{
            $.ajax({
                method: "POST",
                url: "{{ route('post.likeunlike') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    $('#post_like_count_' + postId).html('<a href="#usersWhoLike" data-toggle="modal" class="trip_like_button" id="' + postId + '" data-id="'+postId+'" data-type="post"><b>' + result.count + '</b> Likes</a>');
                });
            e.preventDefault();
        }
    });
        
    
    // Sharing Popup
    $('#shareablePost').on('show.bs.modal',function (event){
        $('#shared-post-wrap-content').empty();
        id = event.relatedTarget.dataset.id;
        type = event.relatedTarget.dataset.type;
        $('#share-post-id').val(id); 
        $('#share-post-type').val(type); 
        $.ajax({
            method: "GET",
            url: '{{  url("get-shareable-post")}}',
            data: {id: id,'type':type}
        })
        .done(function (res) {
            if(res !=0)
                $('#shared-post-wrap-content').html(res);
        });
    });
    $('.share_post_btn').on('click',function(){
        $.ajax({
            method: "POST",
            url: '{{url("share-post")}}',
            data: {id: $('#share-post-id').val(),'type':$('#share-post-type').val(),'text':$('#message_popup_text_for_share').val()}
        })
        .done(function (res) {  
            if(res !=0){
                var result = JSON.parse(res);
                console.log('res.status', result.status)
                if(result.status == 0){
                    toastr.success(result.messages);
                }else {
                    toastr.success('Post has been shared');
                }

                $('#shareablePost').modal('hide');
                $('#share-post-id').val('');
                $('#share-post-type').val('');
                $('#message_popup_text_for_share').val('');
            }
        });
    });
    
    // copy post link  
    $(document).on( 'click', '.copy-newsfeed-link', function(){
        var copy_link = $(this).attr('data-link');
        var _temp = $("<input>");
        $("body").append(_temp);
        _temp.val(copy_link).select();
         document.execCommand("copy");
         _temp.remove()

        toastr.success('The link copied!');
    });
    //delete posts
    $('.del_post_btn').on('click',function (){
        $.ajax({
            method: "POST",
            url: "{{url("delete-post") }}",
            data: {id:$('#delete_id').val(),type: $('#deletePostType').val()}
        }).done(function (res) {
            if(res>0){
                $('#'+$('#element_id').val()).remove();
                $('#delete_id').val(''); 
                $('#deletePostType').val(''); 
                $('#element_id').val(''); 
                $('#deletePostNew').modal('hide');
                $('#post_view').data("bs.modal")?._isShown ? $('#post_view').modal('hide') : '';
                toastr.success('Post has been deleted');
            }
        });
    });
    $('#deletePostNew').on('show.bs.modal',function (event){
        id = event.relatedTarget.dataset.id;
        type = event.relatedTarget.dataset.type;
        element = event.relatedTarget.dataset.element;
        $('#delete_id').val(id); 
        $('#deletePostType').val(type); 
        $('#element_id').val(element); 
    });
    //
    //Discussion upvote
    // $(document).on('click', '.discussion-vote-link.up', function (e) {
    //     console.log("tow")
    //     var _this = $(this);
    //     var discussion_id = $(this).attr('id');
    //     var parent_content = _this.closest('#updown_' + discussion_id)
    //     if(_this.hasClass('disabled')){
    //         is_add = false;
    //     }else{
    //         is_add = true;
    //     }

    //     $.ajax({
    //         method: "POST",
    //         url: "{{route('discussion.updownvote')}}",
    //         data: {discussion_id: discussion_id, type:1, is_add:is_add}
    //     })
    //         .done(function (res) {
    //             $('.upvote-count_' + discussion_id).html(res.count_upvotes)
    //             $('.downvote-count_' + discussion_id).html(res.count_downvotes)

    //             if(_this.hasClass('disabled')){
    //                 _this.removeClass('disabled')
    //             }else{
    //                 _this.addClass('disabled')
    //             }

    //             if(!parent_content.find('.discussion-vote-link.down').hasClass('disabled')){
    //                 parent_content.find('.discussion-vote-link.down').addClass('disabled')
    //             }
    //         });

    //     e.preventDefault()
    // });

    //Discussion downvote
    // $(document).on('click', '.discussion-vote-link.down', function (e) {
    //     var discussion_id = $(this).attr('id');
    //     var parent_content = $('#updown_' + discussion_id)
    //     var _this = parent_content.find('.discussion-vote-link.down');
    //     if(_this.hasClass('disabled')){
    //         is_add = false;
    //     }else{
    //         is_add = true;
    //     }

    //     $.ajax({
    //         method: "POST",
    //         url: "{{route('discussion.updownvote')}}",
    //         data: {discussion_id: discussion_id, type:0, is_add:is_add}
    //     })
    //         .done(function (res) {
    //             $('.upvote-count_' + discussion_id).html(res.count_upvotes)
    //             $('.downvote-count_' + discussion_id).html(res.count_downvotes)

    //             if(_this.hasClass('disabled')){
    //                 _this.removeClass('disabled')
    //             }else{
    //                 _this.addClass('disabled')
    //             }

    //             if(!parent_content.find('.discussion-vote-link.up').hasClass('disabled')){
    //                 parent_content.find('.discussion-vote-link.up').addClass('disabled')
    //             }

    //         });
    //     e.preventDefault()
    // });

    

    
    //for following of place
function newsfeed_place_following(type, id, obj)
{
    var followurl = "{{  route('place.follow', ':user_id')}}";
    var unfollowurl = "{{  route('place.unfollow', ':user_id')}}";
    if(type == "follow")
    {
        url = followurl.replace(':user_id', id);
    }
    else if(type == "unfollow")
    {
        url = unfollowurl.replace(':user_id', id);
    }
    $.ajax({
        method: "POST",
        url: url,
        data: {name: "test"}
    })
    .done(function (res) {
        if (res.success == true) {
            $(obj).parent().find("b").html(res.count);
            if(type == "follow")
            {
                $(obj).replaceWith('<button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_place_following(\'unfollow\','+ id +', this)">Unfollow</button>');
            }
            else if(type == "unfollow")
            {
                $(obj).replaceWith('<button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following(\'follow\','+ id +', this)">@lang('home.follow')</button>');
            }
        } else if (res.success == false) {

        }
    });
}

// Social Sharing
var uniquePostUrl = window.location.href;

function __postShare(type){
    var socialUrl = null,
    shareText = $("#message_popup_text_for_share").val() || '';
    if(uniquePostUrl){
        switch(type){
            case 'whatsapp':
                /*  https://web.whatsapp.com/send?text=
                    https://wa.me/(phone)?text=hello
                    https://api.whatsapp.com/send/?phone&text=hello */
                socialUrl = `https://web.whatsapp.com/send?text=${shareText} ${uniquePostUrl}`;
                break;

            case 'facebook':
                //t= // t for text
                socialUrl = `https://www.facebook.com/sharer.php?u=${uniquePostUrl}&t=${shareText}`;
                break;

            case 'twitter':
                socialUrl = `https://twitter.com/intent/tweet?text=${shareText} ${uniquePostUrl}`;
                break;

            case 'pintrest':
                //&media=&description=
                socialUrl = `https://pinterest.com/pin/create/button/?url=${uniquePostUrl}&description=${shareText}`;
                break;

            case 'tumblr':
                //t= // t for text
                socialUrl = `https://www.tumblr.com/share?t=hello&v=3&u=${uniquePostUrl}&t=${shareText}`;
                break;
        }

        if(socialUrl){
            window.open(socialUrl, '_blank', 'location=yes,scrollbars=yes,status=yes');
        }
    }
}  

$('.extednd-btn').click(function(e){
        e.preventDefault();
        $(this).toggleClass('shrink');
        $(this).closest('.share-to-friend').toggleClass('relative');
    });
    $('#sendToFriendModal').on('hidden.bs.modal', function (e) {
        $('#sendToFriendModal .extednd-btn').removeClass('shrink');
        $('#sendToFriendModal .share-to-friend').removeClass('relative');
    })
    $('.friend-search-results').mCustomScrollbar();
    $('.send_message').on('click',function(){
            var message = $('#message_popup_text').val();
            
            message = message+'<br><br><br>' +$('#message_popup_content').html();
            var participants = $(this).data('user');
            $.ajax({
                method: "POST",
                url: './../../chat/send',
                data: {chat_conversation_id: 0,participants:participants,message:message},
                success:function(e){
                    toastr.success('Your message has been sent');
                    $('#sendToFriendModal').modal('hide');
                    $('#message_popup_content').empty();
                    $('#message_popup_text').val()
                }
            });
            $('#sendToFriendModal .extednd-btn').removeClass('shrink');
            $('#sendToFriendModal .share-to-friend').removeClass('relative');
    });
    $('#sendToFriendModal').on('shown.bs.modal', function (event) {
            $('#message_popup_content').empty();
            var html = $('#'+event.relatedTarget.dataset.id).html();
            var jHtmlObject = jQuery(html);
            var editor = jQuery("<p>").append(jHtmlObject);
            editor.find('.post-top-info-action').remove();
            editor.find('.privacy-settings').remove();
            editor.find('.post-footer-info').remove();
            editor.find('.post-comment-wrapper').remove();
            editor.find('.post-comment-layer').remove();
            var newHtml = editor.html();
            
            $('#message_popup_content').html('<div class="post-block ">'+newHtml+'</div>');

    });
 
</script>
