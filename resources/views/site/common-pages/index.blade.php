@php
    use Carbon\Carbon;
    use App\Models\CommonPage\CommonPage;
    $title = $page->trans[0]->title;
@endphp
@section('after_styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style type="text/css">
        *{
            font-size: 16px ;
        }
        .container {
            padding-left: 15px;
            padding-right: 15px;
        }
        .sub-header{
            background-color: #eaf0ff;
            height: 110px;
            /* position: relative; */
            display: flex;
            align-items: flex-end;
        }
        .sub-header .title {
            font-family: 'Montserrat';
            font-weight: 600;
            color: #1a1a1a;
            border-bottom: 3px solid #4080ff;
            display: inline;
            padding-bottom: 10px;
        }
        .peta-header{
            border-bottom: 1px solid #e6e6e6;
        }
        .peta-header span{
            font-style: italic;
            color: #4d4d4d;
            font-size: 16px;
        }
        
        .basic-contain-area p{
            line-height: 1.2rem !important;
            color :#1a1a1a;
        }
        ol li{
            list-style-type: inherit;
            line-height: 1.2rem !important;
        }
        .light-info{
            color: #4d4d4d;
        }

        .footer{
            padding: 15px 0px;
            padding-bottom: 70px;
        }
        .footer .links-area{
            text-align: center;
        }
        .footer .links-area ul{
            display: inline-flex;
            flex-wrap: wrap;
            margin-left: -10px;
            margin-right: -10px;
        }
        .footer .links-area ul li a{
            display: block;
            padding: 10px;
            text-transform: capitalize;
            color: #333333;
            font-family: 'CircularAirPro';
            font-size: 16px;
            font-weight: 600;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            text-align: center;
        }
        .footer .copyright-line{
            font-family: 'SegoeUI';
            font-size: 14px;
            padding-top: 15px;
            font-weight: normal;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            text-align: center;
            color: #999999;
        }
    </style>
@endsection

@extends('site.layouts.site')

@section('content-area')
    <div class="sub-header">
        <div class="container">
            <h2 class="title">{{$title}}</h2>
        </div>
    </div>
    <div class="container">
        @if ($page->slug == CommonPage::SLUG_PRIVACY_POLICY || $page->slug == CommonPage::SLUG_TERMS_OF_SERVICE)
            <div class="peta-header py-4">
                <span>Last updated {{ Carbon::parse($page->updated_at)->format("F d, Y")}}</span>
            </div>
        @endif
        {!!$page->trans[0]->description!!}
        <div class="footer">
            <div class="links-area">
                <ul>
                    <li><a href="{{url('/')}}">About</a></li>
                    <li><a href="{{url('/')}}">Careers</a></li>
                    <li><a href="{{url('/')}}">Sitemap</a></li>
                    <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                    <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                    <li><a href="{{url('/')}}">Contact</a></li>
                    <li><a href="{{route('help.index')}}">Help Center</a></li>
                </ul>
            </div>
            <div class="copyright-line">Travooo © 2020</div>
        </div>
    </div>
@endsection

@section('before_site_script')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection

