<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="{{url('assets2/js/jquery-bar-rating/dist/themes/fontawesome-stars.css')}}">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - pages of places</title>
</head>
<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="follow-header" id="followHeader">
        <div class="container-fluid">
            <div class="follow-inner">
                <div class="place-name">
                    <span>{{$hotel->trans[0]->title}}</span>
                    <span class="place-tag">{{do_placetype($hotel->place_type)}}</span>
                </div>
                <div class="follow-btn" id="followContainer2">

                </div>
            </div>
        </div>
    </div>

    <div class="content-wrap">

        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url('home')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('navs.general.home')</span>
                            <!--<span class="counter">5</span>-->
                        </a>
                    </li>
                    <li class="active">
                        <a href="#">
                            <i class="trav-about-icon"></i>
                            <span>About</span>
                        </a></li>
                    <li class="active">
                        <a href="#">
                            <i class="trav-review-icon"></i>
                            <span>@lang('place.reviews')</span>
                        </a></li>
                    <li class="active">
                        <a href="#">
                            <i class="trav-photos-icon"></i>
                            <span>@lang('place.photos')</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#">
                            <i class="trav-today-icon"></i>
                            <span>Today</span>
                        </a></li>
                </ul>
            </div>

            <div class="top-banner-wrap">
                <div class="top-banner-block"
                     style="background-image: url(@if(isset($hotel->getMedias[0]->url)) https://s3.amazonaws.com/travooo-images2/{{$hotel->getMedias[0]->url}} @else asset('assets2/image/placeholders/pattern.png') @endif)">
                    <div class="top-banner-text">
                        <div class="place-info-block">
                            <div class="place-title">{{$hotel->trans[0]->title}} <span>#{{$hotel->id}}</span></div>
                            <div class="place-placement"><span
                                        class="event-tag">{{do_placetype($hotel->place_type)}}</span> {{$hotel->trans[0]->address}}
                            </div>
                            <div class="follow-block-info">
                                <ul class="foot-avatar-list">
                                    @foreach($hotel->followers AS $follower)
                                        <li><img class="small-ava"
                                                 src="{{check_profile_picture($follower->user->profile_picture)}}"
                                                 alt="ava" style="width:29px;height:29px;"></li>
                                    @endforeach

                                </ul>
                                <i class="trav-talk-icon"></i>
                                <span>{{count($hotel->followers)}} Following this place</span>
                            </div>
                        </div>
                        <div class="banner-comment">
                            <div class="comment-inner">
                                @if(isset($hotel->medias[0]) && is_object($hotel->medias[0]))
                                    @foreach($hotel->medias[0]->comments AS $mc)
                                        <div class="comment-alert">
                                            <div class="comment-txt-wrap">
                                                <div class="comment-name">{{@$mc->user->name}}</div>
                                                <div class="comment-txt">{{@$mc->comment}}</div>
                                            </div>
                                            <div class="comment-avatar-wrap">
                                                <img src="{{check_profile_picture(@$mc->user->profile_picture)}}" alt=""
                                                     style="width:27px;height:27px;">
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                            <span id="followContainer1">

                </span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="top-now-in-city-block">
                <div class="city-inner" id="nowInPlace">

                </div>
            </div>

            @include('site/hotel/partials/top_blocks')

            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <div class="post-block post-place-block">
                        <div class="post-place-top">
                            <h3 class="side-ttl">About this Place</h3>
                        </div>
                        <div class="post-content-inner">

                            <div class="post-map-block">
                                <div class="post-map-inner">
                                    <img src="https://maps.googleapis.com/maps/api/staticmap?center={{$hotel->lat}},{{$hotel->lng}}&markers=color:red%7Clabel:C%7C{{$hotel->lat}},{{$hotel->lng}}&zoom=14&size=595x360&key=AIzaSyBqTXNqPdQlFIV5QNvYQeDrJ5vH0y9_D-M"
                                         alt="Map of {{@$hotel->trans[0]->title}}">
                                    <div class="post-top-map-info">
                                        <div class="info-block">
                                            <div class="info-icon">
                                                <i class="trav-popularity-icon"></i>
                                            </div>
                                            <div class="info-txt">
                                                <div class="info-ttl">#{{$hotel->id}}</div>
                                                <div class="info-smpl">Popularity</div>
                                            </div>
                                        </div>
                                        <div class="info-block">
                                            <div class="info-icon">
                                                <i class="trav-safety-big-icon"></i>
                                            </div>
                                            <div class="info-txt">
                                                <div class="info-ttl">9/10</div>
                                                <div class="info-smpl">Safety</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-map-flag">
                                        <img class="flag-image"
                                             src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($hotel->country->iso_code)}}.png"
                                             alt="flag" style='width:50px;'>
                                    </div>

                                </div>
                            </div>
                            <div class="post-place-footer">
                                <div class="place-address">
                                    <span class="address-label">Address:</span>
                                    <span>{{$hotel->trans[0]->address}}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="post-block post-review-block">
                        <div class="review-inner">
                            @if($hotel->trans[0]->working_days!='')
                                <div class="review-info-block half-block">
                                    <div class="info-ttl">@lang('place.working_times')</div>
                                    <div class="info-txt">
                                        <p>
                                            {!!$hotel->trans[0]->working_days!!}
                                        </p>
                                    </div>
                                </div>
                            @endif

                            @if($hotel->trans[0]->description!='')
                                <div class="review-info-block half-block">
                                    <div class="info-ttl">@lang('place.website')</div>
                                    <div class="info-txt">
                                        <a href="{{$hotel->trans[0]->description}}" target="blank"
                                           class="web-site">{{$hotel->trans[0]->description}}</a>
                                    </div>
                                </div>
                            @endif

                            @if($hotel->trans[0]->phone!='')
                                <div class="review-info-block half-block">
                                    <div class="info-ttl">Phone</div>
                                    <div class="info-txt">
                                        <a href="{{$hotel->trans[0]->phone}}" target="blank"
                                           class="web-site">{{$hotel->trans[0]->phone}}</a>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>

                    <div class="post-block post-user-rating">
                        <div class="post-side-top">
                            <div class="post-top-txt">
                                <h3 class="side-ttl">User rating</h3>
                                <div class="com-star-block">
                                    <select class="rating_stars">
                                        <option value="1" @if(round($reviews_avg)==1) selected @endif>1</option>
                                        <option value="2" @if(round($reviews_avg)==2) selected @endif>2</option>
                                        <option value="3" @if(round($reviews_avg)==3) selected @endif>3</option>
                                        <option value="4" @if(round($reviews_avg)==4) selected @endif>4</option>
                                        <option value="5" @if(round($reviews_avg)==5) selected @endif>5</option>
                                    </select>
                                    <span class="count">
                      <b>{{round($reviews_avg, 1)}}</b>
                    </span>
                                    <span>from</span>
                                    <a href="#" class="review-link">{{count($reviews)}} @lang('place.reviews')</a>
                                </div>
                            </div>
                            <div class="top-btn-wrap">
                                <button type="button" class="btn btn-transp btn-bordered" data-toggle="modal"
                                        data-target="#writeReviewPopup">Write a Review
                                </button>
                            </div>
                        </div>
                        <div class="post-comment-layer">
                            <div class="post-comment-wrapper">
                                @foreach($reviews AS $review)

                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="{{$review->google_profile_photo_url ? $review->google_profile_photo_url : check_profile_picture($review->author->profile_picture)}}"
                                                 alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#"
                                                   class="comment-name">{{$review->google_author_name ? $review->google_author_name : $review->author->name}}</a>
                                                <a href="#" class="comment-nickname"></a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>{{$review->text}}</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <select class="rating_stars">
                                                        <option value="1" @if($review->score==1) selected @endif>1
                                                        </option>
                                                        <option value="2" @if($review->score==2) selected @endif>2
                                                        </option>
                                                        <option value="3" @if($review->score==3) selected @endif>3
                                                        </option>
                                                        <option value="4" @if($review->score==4) selected @endif>4
                                                        </option>
                                                        <option value="5" @if($review->score==5) selected @endif>5
                                                        </option>
                                                    </select>

                                                    <span class="count">
                            <b>{{$review->score}}</b> / 5
                          </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">{{$review->google_time ? diffForHumans(date("Y-m-d", $review->google_time)) : diffForHumans(date("Y-m-d", $review->created_at->timestamp))}}</div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="post-block">
                        <div class="post-side-top">
                            <h3 class="side-ttl">Photos of {{$hotel->trans[0]->title}} <span
                                        class="count">{{count($hotel->getMedias)}}</span></h3>
                            <div class="side-right-control">
                                <a href="#" class="see-more-link lg">More photos</a>
                            </div>
                        </div>
                        <div class="post-image-container">
                            <div class="flex-image-wrap fh-395 bw-5" id="placePopupTrigger2" style="cursor:pointer;">
                                <div class="image-column col-33">
                                    @foreach($hotel->getMedias AS $photo)
                                        <div class="image-inner img-h-66"
                                             style="background-image:url(https://s3.amazonaws.com/travooo-images2/{{$photo->url}})"></div>
                                        @if($loop->iteration%2==0) </div>
                                <div class="image-column col-33">@endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="post-block">
                        <div class="post-side-top">
                            <h3 class="side-ttl">Happening Today near {{$hotel->transsingle->title}}</h3>
                        </div>
                        <div class="post-side-inner">
                            <div class="post-map-block">
                                <div class="post-map-inner">
                                    <div id="happeningTodayMap" style='width:595px;height:360px;'></div>
                                </div>
                            </div>
                            <div class="post-happen-wrap mCustomScrollbar" id='happeningToday'>


                            </div>
                        </div>
                    </div>

                    @if(count($hotel->followers))
                        <div class="post-block">
                            <div class="post-side-top">
                                <h3 class="side-ttl">People following this place <span
                                            class="count">{{count($hotel->followers)}}</span></h3>
                                <div class="side-right-control">
                                    <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                                    <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
                                </div>
                            </div>
                            <div class="post-side-inner">
                                <div class="post-slide-wrap slide-hide-right-margin">
                                    <ul id="newPeopleDiscover" class="post-slider">
                                        @foreach($hotel->followers AS $follower)
                                            <li class="post-follow-card">
                                                <div class="follow-card-inner">
                                                    <div class="image-wrap">
                                                        <img src="{{check_profile_picture($follower->user->profile_picture)}}"
                                                             alt="" style='width:42px;height:42px;'>
                                                    </div>
                                                    <div class="post-slider-caption">
                                                        <p class="post-card-name">{{$follower->user->name}}</p>
                                                        <p class="post-card-spec">{{$follower->user->display_name}}</p>
                                                        <button type="button" class="btn btn-light-grey btn-bordered">
                                                            @lang('buttons.general.follow')
                                                        </button>
                                                        <button type="button"
                                                                class="btn btn-light-primary btn-bordered">Following
                                                        </button>
                                                        <p class="post-card-follow-count">{{@count($follower->user->followers)}}
                                                            Followers</p>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        @if(count($hotel->getMedias))
                            <div class="post-block post-photo-block">
                                <?php
                                $rand = rand(0, count($hotel->getMedias) - 1);
                                ?>
                                <div class="photo-inner">
                                    <img src="https://s3.amazonaws.com/travooo-images2/{{$hotel->getMedias[$rand]->url}}"
                                         alt="photo" style="width:350px;height:405px;">
                                    <div class="photo-text-cover">
                                        <div class="photo-top-layer">
                                            <div class="place-info">
                                                <div class="place-name">{{$hotel->trans[0]->title}}</div>
                                                <div class="photo-like">
                                                    <i class="trav-heart-fill-icon"></i>
                                                    <span class="txt">{{count($hotel->medias[$rand]->likes)}}
                                                        Likes</span>
                                                </div>
                                            </div>
                                            <div class="photo-popup-link">
                                                <a href="#">
                                                    <i class="trav-gallery-o-icon"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="author-info">Photo by
                                            <span>{{@$hotel->getMedias[$rand]->author_name}}</span></div>
                                    </div>
                                </div>
                                <div class="follow-block-info">
                                    <ul class="foot-avatar-list">
                                        @foreach($hotel->followers AS $pfollower)
                                            <li><img class="small-ava"
                                                     src="{{check_profile_picture($pfollower->user->profile_picture)}}"
                                                     alt="ava" style="width:29px;height:29px;"></li>
                                        @endforeach

                                    </ul>
                                    <i class="trav-comment-plus-icon"></i>
                                    <span>{{count($hotel->followers)}} Following this place</span>
                                </div>
                                <div class="photo-btn-wrap">
                                    <a class="btn btn-light-red" href="#">Book a hotel/flight</a>
                                </div>
                            </div>
                        @endif

                        <div class="share-page-block">
                            <div class="share-page-inner">
                                <div class="share-txt">@lang('strings.frontend.share_this_page') this page</div>
                                <ul class="share-list">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-code"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                                <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                                <li><a href="{{url('/')}}">Advertising</a></li>
                                <li><a href="{{url('/')}}">Cookies</a></li>
                                <li><a href="{{url('/')}}">More</a></li>
                            </ul>
                            <p class="copyright">Travooo &copy; 2017</p>
                        </div>
                    </aside>
                </div>
            </div>

            <div class="full-width-block">


                <div class="post-block post-flage-detail">
                    <!-- <div class="big-place-image">
                      <img src="http://placehold.it/1070x225" alt="place">
                    </div> -->

                    <div class="post-event-block">
                        <div class="post-event-image">
                            <img class="event-cover"
                                 src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$hotel->country->getMedias[0]->url}}"
                                 alt="photo">
                        </div>
                        <div class="post-event-main">
                            <div class="flag-wrap">
                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($hotel->country->iso_code)}}.png"
                                     alt="flag" style='width:155px;'>
                            </div>
                            <div class="post-placement-info">
                                <span class="hash"></span>
                                <h2 class="place-name">{{@$hotel->country->trans[0]->title}}</h2>
                                <div class="event-info-layer">
                                    <span class="placement-place">Country in {{@$hotel->country->region->trans[0]->title}}</span>
                                    <div id="talkingAbout">

                                    </div>
                                </div>
                                <div class="event-main-content">
                                    <p>{{strip_tags(@$hotel->country->trans[0]->description)}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="follow-bottom-link" id="country_follow_button">

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modals -->

@include('site/hotel/partials/write-review-popup')

<!-- about comment popup -->
<div class="modal fade white-style" data-backdrop="false" id="aboutCommentPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-650" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-comment">
                <div class="post-comment-head">
                    <div class="c-head-txt-wrap">
                        <div class="c-head-txt">
                            <div class="dest-name">
                                <span>Disneyland</span>
                                <span class="block-tag">Park</span>
                            </div>
                        </div>
                    </div>
                    <div class="c-head-btn-wrap">
                        <button type="button" class="btn btn-light-primary">
                            @lang('buttons.general.follow')
                        </button>
                    </div>
                </div>
                <div class="post-comment-main">
                    <div class="dest-pic">
                        <img src="http://placehold.it/590x320" alt="">
                    </div>
                    <div class="discussion-block">
                        <span class="disc-ttl">@lang('place.discussion')</span>
                        <span class="disc-count">129</span>
                        <ul class="disc-ava-list">
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="post-comment-layer">
                    <div class="post-comment-top-info">
                        <ul class="comment-filter">
                            <li class="active">@lang('other.top')</li>
                            <li>@lang('other.new')</li>
                        </ul>
                        <div class="comm-count-info">
                            3 / 20
                        </div>
                    </div>
                    <div class="post-comment-wrapper">
                        <div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="http://placehold.it/45x45" alt="">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">Katherin</a>
                                    <a href="#" class="comment-nickname">@katherin</a>
                                </div>
                                <div class="comment-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore
                                        tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-reaction">
                                        <img src="./assets/image/icon-smile.png" alt="">
                                        <span>21</span>
                                    </div>
                                    <div class="com-time">6 hours ago</div>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="http://placehold.it/45x45" alt="">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">Amine</a>
                                    <a href="#" class="comment-nickname">@ak0117</a>
                                </div>
                                <div class="comment-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-reaction">
                                        <img src="./assets/image/icon-like.png" alt="">
                                        <span>19</span>
                                    </div>
                                    <div class="com-time">6 hours ago</div>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="http://placehold.it/45x45" alt="">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">Katherin</a>
                                    <a href="#" class="comment-nickname">@katherin</a>
                                </div>
                                <div class="comment-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore
                                        tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-reaction">
                                        <img src="./assets/image/icon-smile.png" alt="">
                                        <span>15</span>
                                    </div>
                                    <div class="com-time">6 hours ago</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                    </div>
                    <div class="post-add-comment-block post-add-foot">
                        <div class="avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-add-com-input">
                            <input type="text" placeholder="@lang('comment.write_a_comment')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- place one day popup -->
<div class="modal fade white-style" data-backdrop="false" id="placeOneDayPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="story-bottom-slider-wrapper">
        <div class="bottom-slider" id="storiesModeSlider">
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('trip.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Lonnie Nelson</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('trip.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Barbara Hafer</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('trip.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">David</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('trip.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('trip.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Barbara Hafer</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('trip.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">David</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('trip.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('trip.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-dialog modal-custom-style modal-1140 bottom-slider-height" role="document">
        <ul class="modal-outside-link-list white-bg">
            <li class="outside-link">
                <a href="#">
                    <div class="round-icon">
                        <i class="trav-flag-icon"></i>
                    </div>
                    <span>@lang('other.report')</span>
                </a>
            </li>
        </ul>
        <div class="modal-custom-block">
            <div class="post-block post-place-plan-block">

                <div class="post-image-container post-follow-container mCustomScrollbar">
                    <div class="post-image-inner">
                        <div class="post-map-wrap">
                            <div class="post-place-top-info">
                                <a href="#" class="time-link">@lang('time.day_value', ['value' => '2']) </a>
                                <span>of the trip plan</span>
                                <a href="#" class="place-link">Visiting California for a Day</a>
                            </div>
                            <img src="./assets/image/trip-plan-image.jpg" alt="map">
                            <div class="destination-point" style="top:80px;left: 145px;">
                                <img class="map-marker" src="./assets/image/marker.png" alt="marker">
                            </div>
                            <div class="post-map-info-caption map-blue">
                                <div class="map-avatar">
                                    <img src="http://placehold.it/25x25" alt="ava">
                                </div>
                                <div class="map-label-txt">
                                    Checking on <b>2 Sep</b> at <b>8:30 am</b> and will stay <b>30 min</b>
                                </div>
                            </div>
                        </div>
                        <div class="post-destination-block slide-dest-hide-right-margin">
                            <div class="post-dest-slider" id="postDestSliderInner4">
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">Grififth</div>
                                            <div class="dest-count">Observatory</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">Hearst Castle</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">SeaWorld San</div>
                                            <div class="dest-count">Diego</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">United Arab Emirates</div>
                                            <div class="dest-count">2 Destinations</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">SeaWorld San</div>
                                            <div class="dest-count">Diego</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">United Arab Emirates</div>
                                            <div class="dest-count">2 Destinations</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="modal-place-block">

                </div> -->
                <div class="gallery-comment-wrap" id="galleryCommentWrap">
                    <div class="gallery-comment-inner mCustomScrollbar">
                        <div class="gallery-comment-top">
                            <div class="top-info-layer">
                                <div class="top-avatar-wrap">
                                    <img src="http://placehold.it/50x50" alt="">
                                </div>
                                <div class="top-info-txt">
                                    <div class="preview-txt">
                                        <b>@lang('other.by')</b>
                                        <a class="dest-name" href="#">Elijah Hughes</a>
                                        <p class="dest-date">30 Aug 2017 at 10:00 pm</p>
                                    </div>
                                </div>
                            </div>
                            <div class="gal-com-footer-info">
                                <div class="post-foot-block post-reaction">
                                    <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                                    <span><b>2</b> @choice('other.reaction', 6, ['count' => 6])</span>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-layer">
                            <div class="post-comment-top-info">
                                <div class="comm-count-info">
                                    @choice('comment.count_comment', 5, ['count' => 5])
                                </div>
                                <div class="comm-count-info">
                                    3 / 20
                                </div>
                            </div>
                            <div class="post-comment-wrapper">
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>21</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Amine</a>
                                            <a href="#" class="comment-nickname">@ak0117</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                doloribus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-like.png" alt="">
                                                <span>19</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>15</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                            </div>
                        </div>
                    </div>
                    <div class="post-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-add-com-input">
                            <input type="text" placeholder="@lang('comment.write_a_comment')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- place stories popup -->
<div class="modal fade white-style" data-backdrop="false" id="storiesModePopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="story-bottom-slider-wrapper">
        <div class="bottom-slider" id="storiesModeSlider">
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('trip.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Lonnie Nelson</a>
                        <ul class="slider-info-list">
                            <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('trip.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Barbara Hafer</a>
                        <ul class="slider-info-list">
                            <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('trip.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">David</a>
                        <ul class="slider-info-list">
                            <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('trip.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('trip.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Barbara Hafer</a>
                        <ul class="slider-info-list">
                            <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('trip.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">David</a>
                        <ul class="slider-info-list">
                            <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('trip.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('trip.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-dialog modal-custom-style modal-bottom-slider modal-1070" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-story-block">
                <div class="modal-story-top">
                    <div class="top-txt">
                        <span>This is <b>day 2</b> of the trip plan</span>
                        <a class="post-link" href="#">What to See in California</a>
                        <i class="trav-open-video-in-window"></i>
                    </div>
                </div>
                <div class="story-content-block">
                    <div class="place-over">
                        <div class="place-over_top">
                            <div class="place-over-txt">
                                <b>@lang('place.name_airport', ['name' => 'Dubai international'])</b>
                                <i class="trav-set-location-icon"></i>
                                <span class="place-txt">@lang('place.airport_in_place', ['place' => 'Dubai, United Arab Emirates'])</span>
                            </div>
                            <div class="follow-btn-wrap">
                                <button type="button"
                                        class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                            </div>
                        </div>
                        <div class="trip-photos">
                            <div class="photo-wrap">
                                <img src="http://placehold.it/356x365" alt="photo">
                            </div>
                            <div class="photo-wrap">
                                <img src="http://placehold.it/356x365/f5f5f5" alt="photo">
                            </div>
                            <div class="photo-wrap">
                                <img src="http://placehold.it/356x365" alt="photo">
                            </div>
                            <a href="#" class="see-more-link">@lang('place.see_more_photos')</a>
                        </div>
                    </div>
                    <div class="story-content-inner">
                        <div class="story-date-layer">
                            <div class="follow-avatar">
                                <img src="http://placehold.it/68x68" alt="avatar">
                            </div>
                            <div class="follow-by">
                                <span>@lang('other.by')</span>
                                <b>Suzanne</b>
                            </div>
                            <div class="s-date-line">
                                <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                            </div>
                            <div class="s-date-line">
                                <div class="s-date-badge">Mon, 18 Jun 2017</div>
                            </div>
                            <div class="s-date-line">
                                <div class="s-date-badge">@ 10:00am</div>
                            </div>
                        </div>
                        <div class="info-block">
                            <div class="info-top-layer">
                                <ul class="info-list">
                                    <li>
                                        <i class="trav-clock-icon"></i><span
                                                class="info-txt"> @lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></span>
                                    </li>
                                    <li>
                                        <i class="trav-budget-icon"></i><span class="info-txt"> @lang('trip.spent') <b>$310</b></span>
                                    </li>
                                    <li>
                                        <i class="trav-weather-cloud-icon"></i><span
                                                class="info-txt"> @lang('place.weather') <b>12° C</b></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="info-content">
                                <span class="good-label">@lang('place.good_for')</span>
                                <ul class="block-tag-list">
                                    <li><span class="block-tag">@lang('place.food')</span></li>
                                    <li><span class="block-tag">@lang('place.relaxation')</span></li>
                                    <li><span class="block-tag">@lang('place.shopping')</span></li>
                                    <li><span class="block-tag">@lang('place.photography')</span></li>
                                </ul>
                            </div>
                            <div class="info-bottom-layer">
                                <p>“It was an amazing experience, I’m happy I selected this place to be in my trip”</p>
                            </div>
                        </div>
                        <div class="info-main-layer">
                            <p>Cras ac tortor orci a liquam fermentum leo faucibus tellus blandit dapibus ras convallis
                                placerat est, tempor volutpat urna sempernec.</p>
                            <h3>Headline Here</h3>
                            <p>Consectetur adipiscing elit nam consequat ligula non mi rutrum suscipit cras ac tortor
                                orci.</p>
                            <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est, tempor
                                volutpat urna sempernec.</p>
                            <ul class="img-list">
                                <li><img src="http://placehold.it/700x460" alt="image"></li>
                            </ul>
                            <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est, tempor
                                volutpat urna sempernec.</p>
                            <div class="flex-image-wrap fh-460">
                                <div class="image-column col-66">
                                    <div class="image-inner"
                                         style="background-image:url(http://placehold.it/475x460)"></div>
                                </div>
                                <div class="image-column col-33">
                                    <div class="image-inner img-h-66"
                                         style="background-image:url(http://placehold.it/225x310)"></div>
                                    <div class="image-inner img-h-33"
                                         style="background-image:url(http://placehold.it/225x150)"></div>
                                </div>
                            </div>
                        </div>
                        <div class="post-tips">
                            <div class="post-tips-top">
                                <h4 class="post-tips_ttl">@lang('trip.tips_about_place', ['place' => 'Dubai Airport'])</h4>
                                <a href="#" class="see-all-tip">@lang('trip.see_all_tips_count', ['count' => 7])</a>
                            </div>
                            <div class="post-tips_block-wrap">
                                <div class="post-tips_block">
                                    <div class="tips-content">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                            architecto vel magnam accusamus nemo odio unde autem, provident eos itaque
                                            quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                    </div>
                                    <div class="tips-footer">
                                        <div class="tip-by-info">
                                            <div class="tip-by-img">
                                                <img src="http://placehold.it/25x25" alt="">
                                            </div>
                                            <div class="tip-by-txt">
                                                @lang('other.by') <span class="tag blue">Suzanne</span> <span
                                                        class="date-info">5 days ago</span>
                                            </div>
                                        </div>
                                        <ul class="tip-right-info-list">
                                            <li class="round">
                                                <i class="trav-bookmark-icon"></i>
                                            </li>
                                            <li class="round">
                                                <i class="trav-flag-icon"></i>
                                            </li>
                                            <li class="round blue">
                                                <i class="trav-chevron-up"></i>
                                            </li>
                                            <li class="count">
                                                <span>6</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="foot-reaction">
                            <ul class="post-round-info-list">
                                <li>
                                    <div class="map-reaction">
                      <span class="dropdown">
                        <i class="trav-heart-fill-icon" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"></i>
                        <div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
                          <ul class="dropdown-emoji-list">
                            <li>
                              <a href="#" class="emoji-link">
                                <div class="emoji-wrap like-icon"></div>
                                <span class="emoji-name">@lang('chat.emoticons.like')</span>
                              </a>
                            </li>
                            <li>
                              <a href="#" class="emoji-link">
                                <div class="emoji-wrap haha-icon"></div>
                                <span class="emoji-name">@lang('chat.emoticons.haha')</span>
                              </a>
                            </li>
                            <li>
                              <a href="#" class="emoji-link">
                                <div class="emoji-wrap wow-icon"></div>
                                <span class="emoji-name">@lang('chat.emoticons.wow')</span>
                              </a>
                            </li>
                            <li>
                              <a href="#" class="emoji-link">
                                <div class="emoji-wrap sad-icon"></div>
                                <span class="emoji-name">@lang('chat.emoticons.sad')</span>
                              </a>
                            </li>
                            <li>
                              <a href="#" class="emoji-link">
                                <div class="emoji-wrap angry-icon"></div>
                                <span class="emoji-name">@lang('chat.emoticons.angry')</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </span>
                                        <span class="count">6</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="round-icon">
                                        <i class="trav-comment-icon"></i>
                                    </div>
                                    <span class="count">3</span>
                                </li>
                                <li>
                                    <div class="round-icon">
                                        <i class="trav-light-icon"></i>
                                    </div>
                                    <span class="count">37</span>
                                </li>
                                <li>
                                    <a href="#" class="info-link">
                                        <div class="round-icon">
                                            <i class="trav-share-fill-icon"></i>
                                        </div>
                                        <span>@lang('profile.share')</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="post-comment-layer-wrap">
                        <div class="post-comment-layer">
                            <div class="post-comment-top-info">
                                <ul class="comment-filter">
                                    <li class="active">@lang('other.top')</li>
                                    <li>@lang('other.new')</li>
                                </ul>
                                <div class="comm-count-info">
                                    3 / 20
                                </div>
                            </div>
                            <div class="post-comment-wrapper">
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>21</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Amine</a>
                                            <a href="#" class="comment-nickname">@ak0117</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                doloribus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-like.png" alt="">
                                                <span>19</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>15</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                            </div>
                            <div class="post-add-comment-block">
                                <div class="avatar-wrap">
                                    <img src="http://placehold.it/45x45" alt="">
                                </div>
                                <div class="post-add-com-input">
                                    <input type="text" placeholder="@lang('comment.write_a_comment')">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- happen question popup -->
<div class="modal fade white-style" data-backdrop="false" id="happenQuestionPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-850" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-happen-question">
                <div class="post-top-info-layer">
                    <div class="post-top-info-wrap">
                        <div class="post-top-avatar-wrap">
                            <img src="http://placehold.it/50x50" alt="">
                        </div>
                        <div class="post-top-info-txt">
                            <div class="post-top-name">
                                <a class="post-name-link" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">Michael Powell</a>
                            </div>
                            <div class="post-info">
                                Asked a question
                            </div>
                        </div>
                    </div>
                    <div class="post-top-info-action">
                        <a class="post-collapse" href="#">
                            <i class="trav-angle-down"></i>
                        </a>
                    </div>
                </div>
                <div class="post-content-inner">
                    <div class="post-main-content">
                        <h3>What is the best part in Disneyland?</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi aliquid rem dolore
                            explicabo exercitationem. Porro recusandae reiciendis labore aliquam laborum?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam doloribus maxime soluta,
                            deserunt ipsam quia dicta, ex dolor rem quibusdam laborum, quidem eos dolorem eum dolore
                            saepe facere aperiam iure.</p>

                        <div class="post-modal-content-foot">
                            <div class="post-modal-foot-list">
                                <span>Today at <b>5:26 pm</b></span>
                                <span class="dot"> · </span>
                                <span>Views <b>931</b></span>
                                <span class="dot"> · </span>
                                <span>@lang('buttons.general.follow') <b>27</b></span>
                                <ul class="disc-ava-list">
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="post-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-add-com-input">
                            <input type="text" placeholder="@lang('comment.write_a_comment')">
                        </div>
                    </div>
                    <div class="post-tips-top-layer">
                        <div class="post-tips-top">
                            <h4 class="post-tips-ttl">Top Tips</h4>
                            <a href="#" class="top-first-link">Top first <i class="trav-caret-down"></i></a>
                        </div>
                        <div class="post-tips-main-block">
                            <div class="post-tips-row">
                                <div class="tips-top">
                                    <div class="tip-avatar">
                                        <img src="http://placehold.it/25x25" alt="">
                                    </div>
                                    <div class="tip-content">
                                        <div class="top-content-top">
                                            <a href="#" class="name-link">Suzanne</a> <span>said</span><span
                                                    class="dot"> · </span> <span>6 hours ago</span>
                                        </div>
                                        <div class="tip-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tips-footer">
                                    <a href="#" class="upvote-link">
                                        <span class="arrow-icon-wrap"><i class="trav-chevron-up"></i></span>
                                        <span><b>13</b> Upvotes</span>
                                    </a>
                                </div>
                            </div>
                            <div class="post-tips-row">
                                <div class="tips-top">
                                    <div class="tip-avatar">
                                        <img src="http://placehold.it/25x25" alt="">
                                    </div>
                                    <div class="tip-content">
                                        <div class="top-content-top">
                                            <a href="#" class="name-link">Suzanne</a> <span>said</span><span
                                                    class="dot"> · </span> <span>6 hours ago</span>
                                        </div>
                                        <div class="tip-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tips-footer">
                                    <a href="#" class="upvote-link disabled">
                                        <span class="arrow-icon-wrap"><i class="trav-chevron-up"></i></span>
                                        <span><b>13</b> Upvotes</span>
                                    </a>
                                </div>
                            </div>
                            <div class="post-tips-row">
                                <div class="tips-top">
                                    <div class="tip-avatar">
                                        <img src="http://placehold.it/25x25" alt="">
                                    </div>
                                    <div class="tip-content">
                                        <div class="top-content-top">
                                            <a href="#" class="name-link">Suzanne</a> <span>said</span><span
                                                    class="dot"> · </span> <span>6 hours ago</span>
                                        </div>
                                        <div class="tip-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tips-footer">
                                    <a href="#" class="upvote-link">
                                        <span class="arrow-icon-wrap"><i class="trav-chevron-up"></i></span>
                                        <span><b>13</b> Upvotes</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- your friends popup -->
<div class="modal fade white-style" data-backdrop="false" id="goingPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-700" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-going-block post-mobile-full">
                <div class="post-top-topic topic-tabs">
                    <div class="topic-item active">Your friends <span class="count">{{count($checkins)}}</span></div>
                    <div class="topic-item">Experts & Users <span class="count">{{count($checkins)}}</span></div>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar">
                    @foreach($checkins AS $checkin)
                        <div class="people-row">
                            <div class="main-info-layer">
                                <div class="img-wrap">
                                    <a href='{{url('profile/'.$checkin->post_checkin->post->author->id)}}'>
                                        <img class="ava"
                                             src="{{check_profile_picture($checkin->post_checkin->post->author->profile_picture)}}"
                                             alt="ava" style="width:50px;height:50px;">
                                    </a>
                                </div>
                                <div class="txt-block">
                                    <div class="name">
                                        <a href='{{url('profile/'.$checkin->post_checkin->post->author->id)}}'>
                                            {{$checkin->post_checkin->post->author->name}}
                                        </a>
                                        {!! get_exp_icon($checkin->post_checkin->post->author) !!}
                                    </div>
                                    <div class="info-line">
                                        <div class="info-part">Checked in -
                                            <b>{{$checkin->post_checkin->post->created_at}}</b></div>
                                    </div>
                                </div>
                            </div>
                            <div class="button-wrap">
                                <button class="btn btn-light-grey btn-bordered"
                                        onclick='window.location.replace("https://www.travooo.com/chat/new/{{$checkin->post_checkin->post->author->id}}");'>
                                    Message
                                </button>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="{{url('assets2/js/script.js')}}"></script>

<script src="{{url('assets2/js/jquery-bar-rating/dist/jquery.barrating.min.js')}}"></script>
<script type="text/javascript">
    $(function () {
        $('.rating_stars').barrating({
            theme: 'fontawesome-stars',
            readonly: true
        });
    });
    $(function () {
        $('.rating_stars_active').barrating({
            theme: 'fontawesome-stars',
            readonly: false
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#writeReviewForm').submit(function (e) {

            $.ajax({
                method: "POST",
                url: "{{url('hotel/'.$hotel->id.'/ajaxPostReview')}}",
                data: $('#writeReviewForm').serialize()
            })
                .done(function (res) {
                    res = JSON.parse(res);
                    if (res.success == true) {
                        $('#reviewsContainer').prepend(res.prepend);
                        $('#reviewAddCommentBlock').hide();
                    }

                });
            e.preventDefault(); // avoid to execute the actual submit of the form.

        });


    });

    $(document).ready(function (e) {
        $.ajax({
            method: "POST",
            url: "{{url('hotel/'.$hotel->id.'/check-follow')}}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#followContainer1').html('<a class="btn btn-light-primary button_unfollow" href="#">@lang('buttons.general.unfollow')<</a>');
                    $('#followContainer2').html('<a class="btn btn-light-primary button_unfollow" href="#">@lang('buttons.general.unfollow')<</a>');
                } else if (res.success == false) {
                    $('#followContainer1').html('<a class="btn btn-light-primary button_follow" href="#">@lang('buttons.general.follow')</a>');
                    $('#followContainer2').html('<a class="btn btn-light-primary button_follow" href="#">@lang('buttons.general.follow')</a>');
                }
            });

    });

    $(document).ready(function () {
        $('body').on('click', '.button_follow', function (e) {
            $.ajax({
                method: "POST",
                url: "{{url('hotel/'.$hotel->id.'/follow')}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#followContainer1').html('<a class="btn btn-light-primary button_unfollow" href="#">@lang('buttons.general.unfollow')<</a>');
                        $('#followContainer2').html('<a class="btn btn-light-primary button_unfollow" href="#">@lang('buttons.general.unfollow')<</a>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
            e.preventDefault();
        });

        $('body').on('click', '.button_unfollow', function (e) {
            $.ajax({
                method: "POST",
                url: "{{url('hotel/'.$hotel->id.'/unfollow')}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#followContainer1').html('<a class="btn btn-light-primary button_follow" href="#">@lang('buttons.general.follow')</a>');
                        $('#followContainer2').html('<a class="btn btn-light-primary button_follow" href="#">@lang('buttons.general.follow')</a>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
            e.preventDefault();
        });
    });


    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{url('hotel/'.$hotel->id.'/now-in-place')}}",
            data: {name: "test"}
        })
            .done(function (res) {
                data = JSON.parse(res);
                if (data.success == true) {
                    console.log(data['live_checkins'].length);
                    var someone_is_there = false;

                    var output = '<ul class="city-avatar-list">';

                    for (var i = 0; i < data.live_checkins.length; i++) {
                        output += '<li><a href="#" data-toggle="modal" data-target="#goingPopup"><img class="ava" src="' + data.live_checkins[i]['profile_picture'] + '" style="width:60px;height:60px;" alt="' + data.live_checkins[i]['name'] + '" title="' + data.live_checkins[i]['name'] + '"></a></li>';
                        someone_is_there = true;
                    }
                    output += '</ul><div class="city-txt"><p>'

                    for (var ii = 0; ii < data.live_checkins.length; ii++) {
                        output += '<a href="#" class="link" data-toggle="modal" data-target="#goingPopup">' + data.live_checkins[ii]['name'] + '</a>, ';
                    }
                    console.log(someone_is_there);
                    if (someone_is_there) {
                        output += ' are now in {{$hotel->trans[0]->title}}</p></div>';
                        $('#nowInPlace').html(output);
                    }

                    //$('#nowInCountry').html(', <a href="#" class="link">Jeffrey Walters</a> and <a href="#" class="link">2 others</a>');

                }
            });
    });

    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{url('hotel/'.$hotel->id.'/ajaxHappeningToday')}}",
            data: {name: "test"}
        })
            .done(function (res) {
                data = JSON.parse(res);
                console.log(data);
                if (data.success == true) {
                    data = JSON.parse(res);
                    var happening_today = false;

                    var output = '';

                    for (var i = 0; i < data.happenings.length; i++) {
                        output += '<div class="post-happen-block"><div class="happen-info"><div class="img-wrap"><img src="' + data.happenings[i].profile_picture + '" alt="ava" style="width:30px;height:30px;"></div><div class="happen-txt"><a href="/profile/' + data.happenings[i].id + '" class=\"post-name-link\">' + data.happenings[i].name + '</a> <span>posted</span> <a href=\"/post/' + data.happenings[i].post_id + '\" class=\"photo-link\">an update</a></div></div><div class=\"happen-time\"><span>' + data.happenings[i].date + '</span></div></div>';
                    }
                    output += ''


                    $('#happeningToday').html(output);

                    //$('#nowInCountry').html(', <a href="#" class="link">Jeffrey Walters</a> and <a href="#" class="link">2 others</a>');

                }
            });
    });

    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{url('country/'.$hotel->countries_id.'/check-follow')}}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#country_follow_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_country_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    //$('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow2"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                } else if (res.success == false) {
                    $('#country_follow_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_country_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    //$('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow2"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });

    });

    $(document).ready(function () {

        $('body').on('click', '#button_country_follow', function () {
            $.ajax({
                method: "POST",
                url: "{{route('country.follow', $hotel->country->id)}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#country_follow_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_country_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });

        $('body').on('click', '#button_country_unfollow', function () {
            $.ajax({
                method: "POST",
                url: "{{route('country.unfollow', $hotel->country->id)}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#country_follow_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_country_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });
    });

</script>
<script>

    $('#placePopupTrigger,#placePopupTrigger2').on('click', function () {

        let $lg = $(this).lightGallery({
            dynamic: true,
            dynamicEl: [
                    @foreach($hotel->getMedias AS $photo)
                {


                    "src": 'https://s3.amazonaws.com/travooo-images2/th1100/{{$photo->url}}',
                    'thumb': 'https://s3.amazonaws.com/travooo-images2/th180/{{$photo->url}}',
                    'subHtml': `
            <div class='cover-block' style='display:none;'>
              <div class='cover-block-inner comment-block'>
                <ul class="modal-outside-link-list white-bg">
                  <li class="outside-link">
                    <a href="#">
                      <div class="round-icon">
                        <i class="trav-angle-left"></i>
                      </div>
                      <span>@lang("other.back")</span>
                    </a>
                  </li>
                  <li class="outside-link">
                    <a href="#">
                      <div class="round-icon">
                        <i class="trav-flag-icon"></i>
                      </div>
                      <span>Report</span>
                    </a>
                  </li>
                </ul>
                <div class='gallery-comment-wrap'>
                  <div class='gallery-comment-inner mCustomScrollbar'>

                    @if(isset($photo->users[0]) && is_object($photo->users[0]))
                        <div class="top-gallery-content gallery-comment-top">
                          <div class="top-info-layer">
                            <div class="top-avatar-wrap">
@if(isset($photo->users[0]) && is_object($photo->users[0]) && $photo->users[0]->profile_picture!='')
                        <img src="{{url($photo->users[0]->profile_picture)}}" alt="" style="width:50px;hright:50px;">
                        @endif
                        </div>
                        <div class="top-info-txt">
                          <div class="preview-txt">
                        @if(isset($photo->users[0]) && is_object($photo->users[0]) && $photo->users[0]->name!='')
                        <a class="dest-name" href="#">{{$photo->users[0]->name}}</a>
                        @endif
                        <p class="dest-place">uploaded a <b>photo</b> <span class="date">{{$photo->uploaded_at}}</span></p>
                          </div>
                        </div>
                      </div>
                      <div class="gallery-comment-txt">
                        <p>This is an amazing street to walk around and do some shopping</p>
                      </div>
                      <div class="gal-com-footer-info">
                        <div class="post-foot-block post-reaction">
                          <i class="trav-heart-fill-icon"></i>
                          <span><b>185</b></span>
                        </div>
                        <div class="post-foot-block post-comment-place">
                          <i class="trav-location"></i>
                          <span class="place-name">510 LaGuardia Pl, Paris, France</span>
                        </div>
                      </div>
                    </div>
                    @endif
                        <div class="post-comment-layer">
                          <div class="post-comment-top-info">
                            <div class="comm-count-info">
{{@count($photo->comments)}} @lang('comment.comments')
                        </div>
                        <div class="comm-count-info">
                          {{ $loop->iteration }} / {{ $loop->count }}
                        </div>
                      </div>
                      <div class="post-comment-wrapper">
                        @if(isset($photo->comments))
                            @foreach($photo->comments AS $comment)
                        <div class="post-comment-row">
                          <div class="post-com-avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                          </div>
                          <div class="post-comment-text">
                            <div class="post-com-name-layer">
                              <a href="#" class="comment-name">Katherin</a>
                              <a href="#" class="comment-nickname">@katherin</a>
                            </div>
                            <div class="comment-txt">
                              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                            </div>
                            <div class="comment-bottom-info">
                              <div class="com-reaction">
                                <img src="./assets/image/icon-smile.png" alt="">
                                <span>21</span>
                              </div>
                              <div class="com-time">6 hours ago</div>
                            </div>
                          </div>
                        </div>
                            @endforeach
                            @endif
                        </div>
                      </div>
                    </div>
@if(Auth::user())
                        <div class="post-add-comment-block">
                          <div class="avatar-wrap">
                            <img src="{{url(Auth::user()->profile_picture)}}" style="width:45px;height:45px;">
                    </div>
                    <div class="post-add-com-input">
                      <input type="text" name="comment" placeholder="@lang('comment.write_a_comment')">
                    </div>
                    <input type="hidden" name="medias_id" value="{{$photo->id}}" />
                    <input type="hidden" name="users_id" value="{{Auth::user()->id}}" />
                  </div>
                  @endif
                        </div>
                      </div>
                    </div>
`
                },

                @endforeach],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            $.each(galleryItem, function (i, val) {
                // itemArr.push(val);
            });

        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 500);
        }

        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            })
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
        });
    });


    // pages of places popup
    $('#nearByPlacesTrigger').on('click', function () {

        let $lg = $(this).lightGallery({
            dynamic: true,
            dynamicEl: [
                    @foreach($hotels_nearby AS $np)

                {
                    "src": '{{check_place_photo($np)}}',
                    'thumb': '{{check_place_photo($np)}}',
                    'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>@lang("other.back")</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="sub-post-info">
                <ul class="sub-list">
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-popularity-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">n/a</div>
                      <div class="sub-txt">Popularity</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-safety-big-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">n/a</div>
                      <div class="sub-txt">Safety</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-user-rating-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">{{$np->rating}}/5</div>
                      <div class="sub-txt">User rating</div>
                    </div>
                  </li>
                </ul>
                <div class="follow-btn-wrap">

                </div>
              </div>
            </div>
            <div class="map-preview">
              <img src="https://maps.googleapis.com/maps/api/staticmap?center={{$np->lat}},{{$np->lng}}&markers=color:red%7Clabel:C%7C{{$np->lat}},{{$np->lng}}&zoom=13&size=150x150&key=AIzaSyBqTXNqPdQlFIV5QNvYQeDrJ5vH0y9_D-M&language=en" alt="map">
            </div>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-avatar-wrap">

                    </div>
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <p class="dest-name"><a href="{{url('place/'.$np->id)}}">{{$np->trans[0]->title}}</a></p>
                        <p class="dest-place">{{do_placetype($np->place_type)}} - {{$np->trans[0]->address}}</p>
                      </div>
                    </div>
                  </div>
                  <div class="gal-com-footer-info">
                    <div class="post-foot-block post-reaction">
                      <span><b>{{count($np->reviews)}}</b> @lang('place.reviews')</span>
                    </div>
                  </div>
                </div>
                <div class="post-comment-layer">
                  <div class="post-comment-top-info">
                    <div class="comm-count-info">
                      0 Reviews
                    </div>
                    <div class="comm-count-info">

                    </div>
                  </div>
                  <div class="post-comment-wrapper">


                  </div>
                </div>
              </div>
              <div class="post-add-comment-block">

              </div>
            </div>
          </div>
        </div>`
                },

                @endforeach
            ],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            $.each(galleryItem, function (i, val) {
                // itemArr.push(val);
            });
            $.each(galleryThumb, function (i, val) {
                // thumbArr.push(val);
                // let startCnt = `<div class="thumb-txt"><i class="trav-flag-icon"></i> start</div>`;
                // let startCntEmpty = `<div class="thumb-txt">&nbsp;</div>`;
                // let placetxt = 'rabar-sale airport'
                // let placeName = `<div class="thumb-txt">${placetxt}</div>`;
                // if(i == 0){
                //   $(val).addClass('place-thumb');
                //   $(val).append(placeName).prepend(startCnt);
                // }
                // if(i == 2){
                //   $(val).addClass('place-thumb');
                //   $(val).append(placeName).prepend(startCntEmpty);
                // }
            });
        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 500);
        }

        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            })
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
        });
    });


    // nearby events
    $('#nearByEventsTrigger').on('click', function () {

        let $lg = $(this).lightGallery({
            dynamic: true,
            dynamicEl: [
                    @foreach($events AS $ev)
                    <?php
                    $rand = rand(1, 10);
                    ?>
                {
                    "src": "{{asset('assets2/image/events/'.$ev->category.'/'.$rand.'.jpg')}}",
                    'thumb': "{{asset('assets2/image/events/'.$ev->category.'/'.$rand.'.jpg')}}",
                    'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>@lang("other.back")</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="sub-post-info">

                <div class="follow-btn-wrap">

                </div>
              </div>
            </div>
            <div class="map-preview">
              <img src="https://maps.googleapis.com/maps/api/staticmap?center={{$ev->location[1]}},{{$ev->location[0]}}&markers=color:red%7Clabel:C%7C{{$ev->location[1]}},{{$ev->location[0]}}&zoom=13&size=150x150&key=AIzaSyBqTXNqPdQlFIV5QNvYQeDrJ5vH0y9_D-M" alt="map">
            </div>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-avatar-wrap">

                    </div>
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <p class="dest-name">{{$ev->title}}</p>
                        <p class="dest-place">
                                {{ucfirst($ev->category)}}
                        <br />@lang("time.from_to", ['from' => $ev->start, 'to' => $ev->end])</p>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="post-comment-layer">

                  <div class="post-comment-wrapper" style="color: black;padding: 20px;line-height: 1.3;">
                    {!!nl2br($ev->description)!!}
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>`
                },

                @endforeach
            ],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            $.each(galleryItem, function (i, val) {
                // itemArr.push(val);
            });
            $.each(galleryThumb, function (i, val) {
                // thumbArr.push(val);
                // let startCnt = `<div class="thumb-txt"><i class="trav-flag-icon"></i> start</div>`;
                // let startCntEmpty = `<div class="thumb-txt">&nbsp;</div>`;
                // let placetxt = 'rabar-sale airport'
                // let placeName = `<div class="thumb-txt">${placetxt}</div>`;
                // if(i == 0){
                //   $(val).addClass('place-thumb');
                //   $(val).append(placeName).prepend(startCnt);
                // }
                // if(i == 2){
                //   $(val).addClass('place-thumb');
                //   $(val).append(placeName).prepend(startCntEmpty);
                // }
            });
        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 500);
        }

        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            })
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
        });
    });
</script>

<script>

    //595x360
    var map;

    function initMap() {
        map = new google.maps.Map(document.getElementById('happeningTodayMap'), {
            zoom: 16,
            center: new google.maps.LatLng({{$hotel->lat}}, {{$hotel->lng}}),
            mapTypeId: 'roadmap'
        });


        function HTMLMarker(lat, lng, img) {
            this.lat = lat;
            this.lng = lng;
            this.img = img;
            this.pos = new google.maps.LatLng(lat, lng);
        }

        HTMLMarker.prototype = new google.maps.OverlayView();
        HTMLMarker.prototype.onRemove = function () {
        }

        //init your html element here


        HTMLMarker.prototype.onAdd = function () {
            div = document.createElement('DIV');
            div.className = "dest-img-block";
            div.innerHTML = "<img class='dest-img' src='" + this.img + "' alt='' style='width:30px;height:30px;'><div class='icon-wrap'><ul class='icon-list'><li><i class='trav-comment-icon'></i></li></ul></div>";
            var panes = this.getPanes();
            panes.overlayImage.appendChild(div);
        }

        HTMLMarker.prototype.draw = function () {
            var overlayProjection = this.getProjection();
            var position = overlayProjection.fromLatLngToDivPixel(this.pos);
            var panes = this.getPanes();
            panes.overlayImage.style.left = position.x + 'px';
            panes.overlayImage.style.top = position.y - 30 + 'px';
        }

        //to use it
                @foreach($checkins AS $checkin)
        var htmlMarker = new HTMLMarker({{$checkin->place->lat}}, {{$checkin->place->lng}}, '{{check_profile_picture($checkin->post_checkin->post->author->profile_picture)}}');
        htmlMarker.setMap(map);
        @endforeach


    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqTXNqPdQlFIV5QNvYQeDrJ5vH0y9_D-M&callback=initMap&language=en">
</script>
</body>
</html>
