<!-- left outside menu -->
<div class="left-outside-menu-wrap" id="leftOutsideMenu">
    <ul class="left-outside-menu">
        <li>
            <a href="{{ route('home')}}">
                <i class="trav-home-icon"></i>
                <span>@lang('navs.general.home')</span>
                <!--<span class="counter">5</span>-->
            </a>
        </li>
        <li class="active">
            <a href="#">
                <i class="trav-about-icon"></i>
                <span>@lang('navs.frontend.about')</span>
            </a>
        </li>
        <li class="active">
            <a href="#">
                <i class="trav-review-icon"></i>
                <span>@lang('place.navs.reviews')</span>
            </a>
        </li>
        <li class="active">
            <a href="#">
                <i class="trav-photos-icon"></i>
                <span>@lang('place.navs.photos')</span>
            </a>
        </li>
        <li class="active">
            <a href="#">
                <i class="trav-today-icon"></i>
                <span>@lang('place.navs.today')</span>
            </a>
        </li>
    </ul>
</div>