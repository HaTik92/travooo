<?php

use App\Models\TripPlans\TripPlans;
?>
<div class="top-board-wrap">
    @if(count($hotel->getMedias))
    <div class="post-block post-board">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('place.photos')</h3>
            <div class="side-right-control">
                <div class="side-count">{{count($hotel->getMedias)}}</div>
            </div>
        </div>
        <div class="post-side-inner" id="placePopupTrigger" style="cursor:pointer;">
            <div class="board-photo-list">
                @for ($i = 0; $i < 3; $i++)
                @if ($i==0)
                <div class="board-image full-image">
                    @if(@$hotel->getMedias[$i]->thumbs_done)
                    <img src="https://s3.amazonaws.com/travooo-images2/th230/{{@$hotel->getMedias[$i]->url}}"
                         alt="photo" style="width:185px;height:115px;">
                    @else
                    <img src="https://s3.amazonaws.com/travooo-images2/{{@$hotel->getMedias[$i]->url}}"
                         alt="photo" style="width:185px;height:115px;">
                    @endif
                </div>
                @else
                <div class="board-image half-image">
                    @if(@$hotel->getMedias[$i]->thumbs_done)
                    <img src="https://s3.amazonaws.com/travooo-images2/th180/{{@$hotel->getMedias[$i]->url}}"
                         alt="photo" style="width:84px;height:89px;">
                    @else
                    <img src="https://s3.amazonaws.com/travooo-images2/{{@$hotel->getMedias[$i]->url}}"
                         alt="photo" style="width:84px;height:89px;">
                    @endif
                </div>
                @endif
                @endfor
            </div>
        </div>
    </div>
    @endif
    <div class="post-block post-board">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('place.plans')</h3>
            <div class="side-right-control">
                <div class="side-count">{{@count($hotel->versions)}}</div>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="board-photo-list">

                <div class="board-image full-image">
                    <img src="{{get_plan_map(TripPlans::find(139), 185, 115, 1)}}" alt="photo"
                         style="width:185px;height:115px;">
                </div>
                <div class="board-image half-image">
                    <img src="{{get_plan_map(TripPlans::find(87), 84, 89, 1)}}" alt="photo"
                         style="width:84px;height:89px;">
                </div>
                <div class="board-image half-image">
                    <img src="{{get_plan_map(TripPlans::find(120), 84, 89, 1)}}" alt="photo"
                         style="width:84px;height:89px;">
                </div>


            </div>
        </div>
    </div>
    <div class="post-block post-board">
        <div class="post-side-top">
            <h3 class="side-ttl">Travel Mates</h3>
            <div class="side-right-control">

            </div>
        </div>
        <div class="post-side-inner" id="nearByEventsTrigger" style="cursor:pointer;">
            <div class="board-photo-list" dir="auto">
                <div class="board-image full-image" dir="auto">
                    <img src="https://i1.wp.com/www.solopinaytravels.com/wp-content/uploads/2017/12/IMG20170704105047.jpg?zoom=2&resize=738%2C355" alt="photo" style="width:185px;height:115px;" dir="auto">
                </div>
                <div class="board-image half-image" dir="auto">
                    <img src="https://www.australianexplorer.com/forum/photographs/travel-mates-needed-for-grampians-trek-1.jpg" alt="photo" style="width:84px;height:89px;" dir="auto">
                </div>
                <div class="board-image half-image" dir="auto">
                    <img src="https://www.australianexplorer.com/forum/photographs/great-ocean-walk-and-then-up-to-darwin-via-adelaid-1_s.jpg" alt="photo" style="width:84px;height:89px;" dir="auto">
                </div>
            </div>
        </div>
    </div>
    @if(count($hotels_nearby))
    <div class="post-block post-board">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('place.nearby_places')</h3>
            <div class="side-right-control">
                <div class="side-count">{{count($hotels_nearby)}}</div>
            </div>
        </div>
        <div class="post-side-inner" id="nearByPlacesTrigger" style="cursor:pointer;">
            <div class="board-photo-list">
                @foreach($hotels_nearby->slice(0, 3) AS $np)
                @if ($loop->first)
                <div class="board-image full-image">
                    <img src="{{check_place_photo($np)}}" alt="photo" style="width:185px;height:115px;">
                </div>
                @else
                <div class="board-image half-image">
                    <img src="{{check_place_photo($np)}}" alt="photo" style="width:84px;height:89px;">
                </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    @endif
    @if(count($events))
    <div class="post-block post-board">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('place.nearby_events')</h3>
            <div class="side-right-control">

            </div>
        </div>
        <div class="post-side-inner" id="nearByEventsTrigger" style="cursor:pointer;">
            <div class="board-photo-list">
                <?php
                $ev = array_slice($events, 0, 3);
                ?>
                @foreach($ev AS $event)

                <?php
                $rand = rand(1, 10);
                ?>

                @if ($loop->first)
                <div class="board-image full-image">
                    <img src="{{asset('assets2/image/events/'.$event->category.'/'.$rand.'.jpg')}}"
                         alt="photo" style="width:185px;height:115px;">
                </div>
                @else
                <div class="board-image half-image">
                    <img src="{{asset('assets2/image/events/'.$event->category.'/'.$rand.'.jpg')}}"
                         alt="photo" style="width:84px;height:89px;">
                </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    @endif


    <div class="post-block post-board">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('place.nearby_events')</h3>
            <div class="side-right-control">

            </div>
        </div>
        <div class="post-side-inner" id="nearByEventsTrigger" style="cursor:pointer;">
            <div class="board-photo-list" dir="auto">
                <div class="board-image full-image" dir="auto">
                    <img src="{{asset('assets2/image/events/sports/10.jpg')}}" alt="photo" style="width:185px;height:115px;" dir="auto">
                </div>
                <div class="board-image half-image" dir="auto">
                    <img src="{{asset('assets2/image/events/concerts/10.jpg')}}" alt="photo" style="width:84px;height:89px;" dir="auto">
                </div>
                <div class="board-image half-image" dir="auto">
                    <img src="{{asset('assets2/image/events/festivals/10.jpg')}}" alt="photo" style="width:84px;height:89px;" dir="auto">
                </div>
            </div>
        </div>
    </div>




</div>