<script src="https://rawgit.com/jackmoore/autosize/master/dist/autosize.min.js"></script>
<script src="{{asset('assets2/js/hilitor.js')}}"></script>

<script type="text/javascript">

    $(".hero__user-list:not(:has(*))").hide()

    autosize(document.getElementById("AddReview"));

    $(document).on('click', '.place-comment-btn', function(){
        var report_id = $(this).data('id')
         Comment_Show.init($('.pp-report-'+report_id));
    })

    $(document).on('click', '.comment-filter-type', function(){
       var filter_type = $(this).data('type');
        var report_id = $(this).data('report_id');
        commentSort(filter_type, $(this), $('.pp-report-' + report_id));
    })

    $(function () {
        $('.rating_stars').barrating({
            theme: 'fontawesome-stars',
            readonly: true
        });
    });

    $(function () {
        $('.rating_stars_active').barrating({
            theme: 'fontawesome-stars',
            readonly: false
        });
    });

    $(".add_post_permission").click(function(e){
        $("#add_post_permission_button").html($(this).find('label').html());
    });

    $('.datepicker').datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: '+5d',
        changeMonth: true,
        changeYear: true,
        altField: "#idTourDateDetailsHidden",
        altFormat: "yy-mm-dd"
    });

    $(document).ready(function () {
        // add tag script 
     $('#add_post_text').on('keyup',
             delay(function(e) {
                
                var text = $( "#add_post_text" ).text();
                var pattern = /\B@[\s\p{L}\d_-]+/ugi;
                var match = text.match(pattern);
                if (!match)
                    return null;
                var last = match.pop();
                var Word = last.replace("@", "");
                
                if(Word){
                   

                    //search name 
                    $.ajax({
                        method: "POST",
                        url: "{{ url('reports/get_search_info') }}",
                        data: { q: Word }
                    })
                    .done(function (res) {
                        var res = JSON.parse(res);
                        $('.textcomplete-wrapper').removeClass('d-none');
                        $('.textcomplete-wrapper').html(buildTaggedItems(res,Word));
                    });
                }



        },500));
        //Add trip plan script
        $('body').on('click', '#addTripPlan', function (e) {
            var checked_val = $(this).closest('#AddToPlanModal').find("input[name='plans_id']:checked").val();
            var url = "{{url('/trip/plan/')}}";
            var place_id = $(this).attr('data-place_id')
            if(typeof checked_val != "undefined"){
              if(checked_val == 0){
                 window.location.href = url + '/' + checked_val;
              }else{
                  window.location.href = url + '/' + checked_val + '?do=edit&action=' + place_id;
              }

            }
        });

        $('.follow-user-list').not('.slick-initialized').slick({
             variableWidth: !0,
             infinite: false,
            prevArrow: '<button class="about-modal__carousel-control about-modal__carousel-control--prev"><svg class="icon icon--angle-left"> <use xlink:href="img/sprite.svg#angle-left"></use> </svg></button>',
            nextArrow: '<button class="about-modal__carousel-control about-modal__carousel-control--next"><svg class="icon icon--angle-right"> <use xlink:href="img/sprite.svg#angle-right"></use> </svg></button>'
        })

        // Revires votes
        $('body').on('click', '.review-vote.up', function (e) {
            var review_id = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{route(session('place_country') . '.revirews_updownvote')}}",
                data: {review_id: review_id, vote_type:1}
            })
                .done(function (res) {
                     var up_result = JSON.parse(res);
                    $('.upvote-'+review_id+' b').html(up_result.count_upvotes);
                    $('.downvote-'+review_id+' b').html(up_result.count_downvotes);
                });
            e.preventDefault()
        });

        $('body').on('click', '.review-vote.down', function (e) {
            var review_id = $(this).attr('id');
            var obj = $(this);
            $.ajax({
                method: "POST",
                url: "{{route(session('place_country') . '.revirews_updownvote')}}",
                data: {review_id: review_id, vote_type:2}
            })
                .done(function (res) {
                    var down_result = JSON.parse(res);
                    obj.parent().find('.upvote-'+review_id+' b').html(down_result.count_upvotes);
                    obj.parent().find('.downvote-'+review_id+' b').html(down_result.count_downvotes);
                });
            e.preventDefault()
        });
        // $.ajax({
        //     method: "POST",
        //     url: "{{ route('place.ajax_happening_today', $place->id) }}",
        //     data: {name: "test"}
        // })
        //     .done(function (res) {
        //         var result = JSON.parse(res);
        //         if(result.success)
        //         {
        //             $("#modal-about").find(".map__users").html('');
        //             var marker = new Array();
        //             for(var i in result.happenings)
        //             {
        //                 var name = result.happenings[i].name != null ? result.happenings[i].name : "";
        //                 var map__user    = '<div class="map__user" style="cursor: pointer" onclick="setCenter_usermap(today_map, ' + result.happenings[i].lat + ',' + result.happenings[i].lng + ')">';
        //                 map__user       += '<div class="map__user-content"><img class="map__user-avatar" src="' + result.happenings[i].profile_picture + '" alt="#" title="" />';
        //                 map__user       += '<div class="map__user-text"><a class="map__user-name" href="#">' + name + '</a> checked-in to';
        //                 map__user       += '<a class="map__user-added" href="#">&nbsp;' + result.happenings[i].title + '</a></div>';
        //                 map__user       += '</div><div class="map__posted">' + result.happenings[i].date + '</div></div>';

        //                 var title = name + " checked-in to " + result.happenings[i].title;
        //                 $("#modal-about").find(".map__users").append(map__user);

        //                 var canvas = document.createElement("canvas");
        //                 var scale = 1;
        //                 var ctx = canvas.getContext("2d");
        //                 ctx.canvas.width = 34 * scale;
        //                 ctx.canvas.height = 34 * scale;

        //                 var img= document.createElement("img");
        //                 img.src = result.happenings[i].profile_picture;
        //                 ctx.save();
        //                 ctx.beginPath();
        //                 ctx.arc(17, 17, 15, 0, 2 * Math.PI);
        //                 ctx.stroke();
        //                 ctx.clip();
        //                 ctx.drawImage(img, 2, 2, 30, 30);
        //                 ctx.restore();

        //                 ctx.strokeStyle = "#FFFFFF";
        //                 ctx.beginPath();
        //                 ctx.lineWidth = 2;
        //                 ctx.arc(17, 17, 15, 0, 2 * Math.PI);
        //                 ctx.stroke();

        //                 var img = {
        //                     url: canvas.toDataURL(),
        //                 }
        //                 var latLng = new google.maps.LatLng(result.happenings[i].lat, result.happenings[i].lng);
        //                 var marker = new google.maps.Marker({
        //                     position: latLng,
        //                     icon: img,
        //                     map: today_map,
        //                     title: title
        //                 });
        //             }
        //         }

        //     });

        var photo_position_calc = function(content) {
            var col_height = new Array(0, 0, 0), i = 0;

            $("#box-photos").find(".gallery>.gallery__col").each(function(){
                $(this).find('img').each(function(){
                    col_height[i] += this.height + 100;
                });
                i++;
            });

            var col_index = col_height.indexOf(Math.min.apply(null, col_height));
            $("#box-photos").find(".gallery>.gallery__col").eq(col_index).append(content);
        }
        // about_photo_init();

        $(".about_more_photo").click(function(e){
            var num = parseInt($(this).attr("data-page"));
            $.post(
                '{{route(session("place_country") . ".ajax_more_photo", $place->id)}}',
                {pagenum: num},
                function(res)
                {
                    if(res.success)
                    {
                        $(".about_more_photo").attr("data-page", (num+1));
                        for(var i in res.data)
                            photo_position_calc(res.data[i]);
                        // $("#box-photos").find(".gallery").append(res.data);
                    }
                },"json"
            );
        });
        $(".about_more_photo").click();

        $('body').on('click', '.report_like_button', function (e) {
            var obj = $(this);
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('report.like') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    //console.log('#post_like_count_' + postId);
                    $(obj).find('strong').html(result.count);
                    // $('#report_like_count_' + postId).html('<strong>' + result.count + '</strong> Likes');
                });
            e.preventDefault();
        });
        $('body').on('click', '.report_share_button', function (e) {
            var obj = $(this);
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('report.share') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    //console.log('#post_like_count_' + postId);
                    $(obj).find('strong').html(result.count);
                    // $('#report_share_count_' + postId).html('<strong>' + result.count + '</strong> Shares');
                });
            e.preventDefault();
        });

        $('body').on('click', '.followbtn4user', function (e) {
            var obj = $(this);
            var id = obj.parent().attr('data-id');
            var followurl = "{{  route('profile.follow', ':user_id')}}";
            var unfollowurl = "{{  route('profile.unfolllow', ':user_id')}}";
            var type = obj.hasClass('btn-light-grey') ? 'unfollow' : 'follow' ;
            if(type == "follow")
            {
                url = followurl.replace(':user_id', id);
            }
            else if(type == "unfollow")
            {
                url = unfollowurl.replace(':user_id', id);
            }
            $.ajax({
                method: "POST",
                url: url,
                data: {name: "test"}
            })
            .done(function (res) {
                if (res.success == true) {
                    if(type == "follow")
                    {
                        obj.removeClass('btn-light-primary');
                        obj.addClass('btn-light-grey');
                        obj.html('UnFollow');
                    }
                    else if(type == "unfollow")
                    {
                        obj.removeClass('btn-light-grey');
                        obj.addClass('btn-light-primary');
                        obj.html('Follow');
                    }
                } else if (res.success == false) {

                }
            });
        });

        // $('body').on('click', '.discussion_like_button', function (e) {
        //     postId = $(this).attr('id');
        //     $.ajax({
        //         method: "POST",
        //         url: "{{ route('discussion.likeunlike') }}",
        //         data: {post_id: postId}
        //     })
        //         .done(function (res) {
        //             var result = JSON.parse(res);

        //             if (res.status == 'yes') {


        //             } else if (res == 'no') {

        //             }

        //             $('#discussion_like_count_' + postId).html('<strong>' + result.count + '</strong> Likes');
        //         });
        //     e.preventDefault();
        // });
        // $('body').on('click', '.discussion_share_button', function (e) {
        //     postId = $(this).attr('id');
        //     $.ajax({
        //         method: "POST",
        //         url: "{{ route('discussion.shareunshare') }}",
        //         data: {post_id: postId}
        //     })
        //         .done(function (res) {
        //             var result = JSON.parse(res);

        //             if (res.status == 'yes') {


        //             } else if (res == 'no') {

        //             }

        //             $('#discussion_share_count_' + postId).html('<strong>' + result.count + '</strong> Shares');
        //         });
        //     e.preventDefault();
        // });

        // $('body').on('click', '.post_like_button', function (e) {
        //     var obj = $(this);
        //     postId = $(this).attr('id');
        //     $.ajax({
        //         method: "POST",
        //         url: "{{ route('post.likeunlike') }}",
        //         data: {post_id: postId}
        //     })
        //         .done(function (res) {
        //             var result = JSON.parse(res);
        //             if (res.status == 'yes') {


        //             } else if (res == 'no') {

        //             }
        //            $('#post_like_count_' + postId).html(result.count + ' Likes')
        //         });
        //     e.preventDefault();
        // });

        // $('body').on('click', '.plan_like_button', function (e) {
        //     var obj = $(this);
        //     postId = $(this).attr('id');
        //     $.ajax({
        //         method: "POST",
        //         url: "{{ route('trip.likeunlike') }}",
        //         data: {id: postId}
        //     })
        //         .done(function (res) {
        //             var result = JSON.parse(res);
        //             if (res.status == 'yes') {


        //             } else if (res == 'no') {

        //             }
        //            $('#plan_like_count_' + postId).html(result.count + ' Likes')
        //         });
        //     e.preventDefault();
        // });

        // $('body').on('click', '.post_share_button', function (e) {
        //     var obj = $(this);
        //     postId = $(this).attr('id');
        //     $.ajax({
        //         method: "POST",
        //         url: "{{ route('post.shareunshare') }}",
        //         data: {post_id: postId}
        //     })
        //         .done(function (res) {
        //             var result = JSON.parse(res);
        //             if (res.status == 'yes') {


        //             } else if (res == 'no') {

        //             }
        //             //console.log('#post_like_count_' + postId);
        //             $(obj).find('strong').html(result.count);
        //         });
        //     e.preventDefault();
        // });
        // $('body').on('click', '.plan_share_button', function (e) {
        //     postId = $(this).attr('id');
        //     $.ajax({
        //         method: "POST",
        //         url: "{{ route('trip.shareunshare') }}",
        //         data: {trip_id: postId}
        //     })
        //         .done(function (res) {
        //             var result = JSON.parse(res);
        //             if (res.status == 'yes') {


        //             } else if (res == 'no') {

        //             }
        //             //console.log('#post_like_count_' + postId);
        //             $('#plan_share_count_' + postId).html('<strong>' + result.count + '</strong> Shares');
        //         });
        //     e.preventDefault();
        // });

        $('body').on('click', '.post__tip-loadmore', function(e) {

            var btn         = $(this);
            var replies_id  = btn.attr('reply-id');
            var page_num    = btn.attr('page_num');

            $.ajax({
                method: "POST",
                url: "{{ route(session('place_country') . '.ajax_discuss_loadmore') }}",
                data: { dis_id: replies_id, page_num: page_num }
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.data != "") {

                        btn.parent().prev().append( result.data );

                    }
                    if ( result.has_next )
                    {
                        btn.parent().show();
                        btn.attr('page_num', 1 * page_num + 1);
                    }
                    else
                    {
                        btn.parent().hide();
                    }

                });
            e.preventDefault();
        });

        $('body').on('submit', '.dicussioncomment', function(e) {

            var form            = $(this);
            var discussion_id    = form.attr('data-id');
            var text            = form.find("textarea").val().trim();
            if (text.trim() == ""){
                return false;
            }
            $.ajax({
                method: "POST",
                url: "{{ route('discussion.ajaxreply') }}",
                data: { discussion_id: discussion_id, text: text }
            })
                .done(function (res) {

                    if (res != "error") {

                        form.find("textarea").val('');
                        form.closest('.post').find(".post__tips").html(res);
                        var c = form.closest('.post').find('.post__footer').find('.post__comment-btn').find('strong').html() * 1;
                        form.closest('.post').find('.post__footer').find('.post__comment-btn').find('strong').html( c + 1 );
                        form.closest('.post').find('.post__tip-loadmore').attr('page_num', 1);

                    }
                    else
                    {
                        alert("error");
                    }

                });
            return false;
        })

       var scrollTimeout = null;

        // $(window).mousemove(function(){
        //     $('.hero__user-list').fadeIn('slow');
        //     if (scrollTimeout) clearTimeout(scrollTimeout);
        //     scrollTimeout = setTimeout(function(){
        //         $('.hero__user-list').fadeOut('slow');
        //     },2000);
        // });



        window._addPostBox = new AddPostBox (
            new AddPostMain(document.getElementById('add-post')),
            new AddPostDiscussionBlock(document.getElementById('add-post-discussion-block')),
            new AddPostTripplanBlock(document.getElementById('add-post-tripplan-block')),
            new AddPostTravelMatesBlock(document.getElementById('add-post-travelmates-block')),
            new AddPostReviewBlock(document.getElementById('add-post-review-block')),
            new AddPostReportBlock(document.getElementById('add-post-report-block'))
        );


    }); // end $(document).ready

    function discuss_loadmore(obj)
    {
        $(obj).closest('.post').find('.post__tip-loadmore').click();
        $(obj).hide();
    }

    function review_share(id, e) {
        $.ajax({
                method: "POST",
                url: "{{ route(session('place_country') . '.share-review') }}",
                data: {id: id},
                dataType: 'json'
            })
                .done(function (res) {

                    alert(res.msg);

                });
            e.preventDefault();
    }

</script>

<script>
    $(document).ready(function () {
        $('#writeReviewForm').submit(function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route(session('place_country') . '.ajax_post_review', $place->id) }}",
                data: $('#writeReviewForm').serialize(),
            })
                .done(function (res) {
                    res = JSON.parse(res);
                    if (res.success == true) {

                        $("#modal-review").find('.comments__items').prepend(res.prepend);

                        var cnt = $("#modal-review").find('.add-review__footer').find("strong").html() * 1;
                        $("#modal-review").find('.add-review__footer').find("strong").html(cnt + 1);

                        var disp_cnt = $("#modal-review").find(".comments__number").find("strong").html() * 1;
                        $("#modal-review").find(".comments__number").find("strong").html(disp_cnt + 1);
                        $("#modal-review").find(".comments__number").find("span").html(cnt + 1);

                        $("#modal-review").find(".comments__number").find("span")
                        $('#writeReviewForm').trigger("reset");
                        $("#writeReviewForm").find("#1-star").click();
                    }

                });
            e.preventDefault(); // avoid to execute the actual submit of the form.

        });

        $('body').on('click', '.button_checkin', function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) . '.checkin', $place->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#checkinContainer1').html('<a class="btn btn--secondary button_checkin" style="font-family: inherit;" href="#"><svg class="icon icon--location"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use></svg>Check-in</a>');
                    } else if (res.success == false) {
                        $('#checkinContainer1').html('<a class="btn btn--secondary button_checkin" style="font-family: inherit;" href="#"><svg class="icon icon--location"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use></svg>Check-in</a>');
                    }
                });
            e.preventDefault();
        });


        $('body').on('click', '.button_follow', function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route(session('place_country') . '.follow', $place->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#followContainer1').html('<a class="btn btn--main button_unfollow" style="font-family: inherit;opacity:0.7" href="#"><svg class="icon icon--follow"><use xlink:href="{{asset('assets3/img/sprite.svg#follow')}}"></use></svg>@lang("place.unfollow")</a>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
            e.preventDefault();
        });

        $('body').on('click', '.button_unfollow', function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route(session('place_country') . '.unfollow', $place->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#followContainer1').html('<a class="btn btn--main button_follow" style="font-family: inherit;" href="#"><svg class="icon icon--follow"><use xlink:href="{{asset('assets3/img/sprite.svg#follow')}}"></use></svg>@lang("place.follow")</a>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
            e.preventDefault();
        });
    });

    function reviewsSort(type, obj)
    {
        if($(obj).hasClass('comments__filter-btn--active'))
            return;

        $(obj).parent().find('button').removeClass('comments__filter-btn--active');
        $(obj).addClass('comments__filter-btn--active');
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $(obj).closest('.comments').find('.comments__items');
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if(type == "Top")
                {
                    if ($(list[i]).attr('topsort') < $(list[i + 1]).attr('topsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "New")
                {
                    if ($(list[i]).attr('newsort') < $(list[i + 1]).attr('newsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "Worse")
                {
                    if ($(list[i]).attr('topsort') > $(list[i + 1]).attr('topsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }
        Reviews_Show.reload($("#modal-review").find(".comments"));
    }

    var Reviews_Show = (()=>{
        var total_rows = 0;
        var display_unit = 5;

        function init(obj){
            var target = $(obj).find('.sortBody');
            if(target.attr('travoo-comment-rows'))
            {
                return;
            }
            else
            {
                target.find(">div").hide();
                var btn = $('<a href="javascript:;"/>').addClass('review__load-more').text('Load more...').click(function(){
                    Reviews_Show.inc(target)
                });
                target.after(btn);
                get_totalrows(target)

            }
        }

        function init_sort(obj, sortkey){

            var list, i, switching, shouldSwitch, switchcount = 0;
            switching = true;
            parent_obj = $(obj).find('.sortBody');
            while (switching) {
                switching = false;
                list = parent_obj.find('>div');
                shouldSwitch = false;
                for (i = 0; i < list.length - 1; i++) {
                    shouldSwitch = false;
                    if(sortkey == "desc")
                    {
                        if ($(list[i]).attr('sortval')*1 < $(list[i + 1]).attr('sortval')*1) {
                            shouldSwitch = true;
                            break;
                        }
                    }
                    else if(sortkey == "asc")
                    {
                        if ($(list[i]).attr('sortval')*1 > $(list[i + 1]).attr('sortval')*1) {
                            shouldSwitch = true;
                            break;
                        }
                    }
                }
                if (shouldSwitch) {
                    $(list[i]).before(list[i + 1]);
                    switching = true;
                    switchcount ++;
                } else {
                    if (switchcount == 0 && list.length > 1 && i == 0) {
                        switching = true;
                    }
                }
            }

            init(obj);
        }

        function reload(obj)
        {
            var target = $(obj).find('.sortBody');
            target.find(">div").hide();
            target.find(">div").removeClass('displayed');
            get_totalrows(target);
        }

        function get_totalrows(obj)
        {
            this.total_rows = obj.find(">div").length;
            obj.attr('travoo-comment-rows', this.total_rows);
            if(!obj.attr('travoo-current-page'))
                obj.attr('travoo-current-page', 1);
            show_rows(obj, obj.attr('travoo-current-page'));
        }

        function show_rows(obj, page)
        {
            this.total_rows = parseInt(obj.attr('travoo-comment-rows'));
            var showing_rows = parseInt(page) * display_unit;
            showing_rows = this.total_rows > showing_rows ? showing_rows : this.total_rows;
            for(var i = 0; i < showing_rows; i++)
            {
                obj.find(">div").eq(i).show().addClass("displayed");
            }

            if( showing_rows == this.total_rows )
            {
                obj.next().hide();
            }
            var disnum = obj.find(".displayed").length;
            obj.parent().find('.comments__number').find('strong').html( disnum );
        }

        function increase_show(obj)
        {
            var current_page = parseInt(obj.attr('travoo-current-page'));
            obj.attr('travoo-current-page', (current_page + 1));
            get_totalrows(obj);
        }


        return  {
            init: init,
            init_sort: init_sort,
            inc: increase_show,
            reload: reload
        }
    })();
    new Reviews_Show.init($("#modal-review").find(".comments"));
    new Reviews_Show.init_sort($("#box-ratings").find(".comments"), 'desc');
    new Reviews_Show.init_sort($("#box-qa"), 'desc');

  function tripslike(id, obj)
  {
      $.ajax({
          url: "{{route('trip.likeunlike')}}",
          type: "POST",
          data: {id: id},
          dataType: "json",
          success: function(data, status){
              $(obj).find('strong').html(data.count);
          },
          error: function(){}
      });
  }

  // for spam reporting
  function injectData(id, obj)
  {
      // var button = $(event.relatedTarget);
      var posttype = $(obj).parent().attr("posttype");
      $("#spamReportDlg").find("#dataid").val(id);
      $("#spamReportDlg").find("#posttype").val(posttype);
  }
    // photo comment start
    $(document).on('keydown', '#mediacommenttext', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });
    
    

    $(document).on('keyup', '#mediacommenttext', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').submit();
        }
    });

//    $(document).on('keyup', '#mediacomment4modal', function(e){
//        if(e.keyCode == 13 && $(this).val().trim() != ""){
//            var text = $(this).val().trim();
//            var post_id = $(this).parent().attr('data-id');
//            var form = $(this).parent();
//
//            var url = '{{ route("media.comment4place", ":postId") }}';
//            url = url.replace(':postId', post_id);
//
//            $(form).find('input').val('');
//
//            $.ajax({
//                method: "POST",
//                url: url,
//                data: {text: text}
//            })
//                .done(function (res) {
//                    if(res != '') {
//                        var comment_body = $("#modal-medias").find(".comments");
//                        comment_body.find(".comments__items").find(".simplebar-content").prepend(res);
//
//                        var count = comment_body.find(".comment").length;
//                        comment_body.find(".comments__total").html(parseInt(comment_body.find(".comments__total").html()) + 1 + " Comments");
//                        comment_body.find(".comments__number").html( count + "/" + parseInt(comment_body.find(".comments__total").html()) );
//
//                    }
//                });
//        }
//    });

    $(document).on('submit', '.media_comment_form', function(e){
        var form = $(this);
        var text = form.find('#mediacommenttext').val().trim();

        if(text == "")
        {
            return false;
        }
        var values = $(this).serialize();

        form.trigger('reset');
        var postId = form.find('input[name="medias_id"]').val();
        var url = '{{ route("media.comment", ":postId") }}';
        url = url.replace(':postId', postId);

        var all_comment_count = $('.'+ postId +'-all-comments-count').html();
        form.find('button[type=submit]').attr('disabled', true);
        $.ajax({
            method: "POST",
            url: url,
            data: values
        })
            .done(function (res) {
                if (form.attr('mod') == 'out') {
                    form.closest('.comments').find('.comments__items').append(res);
                    var comments = form.closest('.comments').find('.comments__items').find('.comment').length;
                    form.closest('.post').find('.post__comment-btn').find('strong').html(comments);
                    form.find('.medias').empty();
                    form.find('textarea').val('');
                    form.find('button[type=submit]').removeAttr('disabled');
                } else {

                    form.closest('.gallery-comment-wrap').find('.post-comment-wrapper').prepend(res);
                    var comments = form.closest('.gallery-comment-wrap').find('.post-comment-wrapper').find('.post-comment-row').length;
                    form.closest('.gallery-comment-wrap').find('.comm-count-info').find('strong').html(comments);

                    $('.'+ postId +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                    $("#" + postId + ".comment-not-fund-info").remove();

                    Comment_Show.reload($('.photo-comment-' + postId));
                    form.trigger('reset');
                }
            });
        e.preventDefault();
    });

    // Event comment, like, share, delete ____ start

    $('body').on('click', '.event_like_button', function (e) {
        obj = $(this);
        event_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{ route(session('place_country') . '.eventlike') }}",
            data: {event_id: event_id}
        })
            .done(function (res) {
                var result = JSON.parse(res);

                if (result.status == 'yes') {

                    obj.find('strong').html(result.count);
                    obj.find('i').toggleClass('red');
                } else if (result.status == 'no') {

                    obj.find('strong').html(result.count);
                    obj.find('i').toggleClass('red');

                }
            });
        e.preventDefault();
    });

    $('body').on('click', '.media_like_button', function (e) {
        obj = $(this);
        media_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{ route(session('place_country') . '.medialike') }}",
            data: {media_id: media_id}
        })
            .done(function (res) {
                var result = JSON.parse(res);

                if (result.status == 'yes') {

                    obj.find('strong').html(result.count);
                    obj.find('i').toggleClass('red');
                } else if (result.status == 'no') {

                    obj.find('strong').html(result.count);
                    obj.find('i').toggleClass('red');

                }
            });
        e.preventDefault();
    });

    $('body').on('click', '.event_share_button', function (e) {
        obj = $(this);
        event_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{ route(session('place_country') . '.eventshare') }}",
            data: {event_id: event_id}
        })
            .done(function (res) {
                var result = JSON.parse(res);

                if (result.status == 'yes') {

                    obj.find('strong').html(result.count);
                    alert(result.msg);
                } else if (result.status == 'no') {
                    alert(result.msg);
                }
            });
        e.preventDefault();
    });

    $('body').on('click', '.eventcomment_delete', function (e) {
        obj = $(this);
        event_id = $(this).attr('data-id');

        e.preventDefault();
            if(!confirm("Are you sure delete this comment?"))
                return;
            $.ajax({
                method: "POST",
                url: "{{ route(session('place_country') . '.eventcommentdelete') }}",
                data: {event_id: event_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if(result.status == 'yes') {
                        var cnt = parseInt(obj.closest('.post').find('.event_post_comments').find('strong').html());
                        obj.closest('.post').find('.event_post_comments').find('strong').html(cnt-1);
                        obj.closest('.post-comment-row').remove();
                    } else {
                        alert(result.msg);
                    }
                });

    });

    $('body').on('click', '.comments__load-more.event', function (e) {
        obj = $(this);
        event_id = $(this).attr('data-id');
        count = obj.parent().find('div[topsort]').length;
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: "{{ route(session('place_country') . '.ajax_event_comment4modal') }}",
            data: {event_id: event_id, count: count}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if(result.data != ''){
                    obj.before(result.data);
                }else{

                }
                var cnt = obj.parent().find("div[topsort]").length;
                if(cnt == result.rows) obj.hide();
            });

    });

    // Event comment, like, share ____ end

    // Media comment sort

    $(document).on('click', '.media-comment-filter-type', function(){
       var filter_type = $(this).data('type');
        var media_id = $(this).data('id');
        mediaCommentSort(filter_type, $(this), $('.photo-comment-' + media_id));
    })

    function mediaCommentSort(type, obj, reload_obj){
        if($(obj).hasClass('active'))
            return;

        $(obj).parent().find('li').removeClass('active');
        $(obj).addClass('active');
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $(reload_obj).find('.post-comment-wrapper');
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if(type == "Top")
                {
                    if ($(list[i]).attr('topsort') < $(list[i + 1]).attr('topsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "New")
                {
                    if ($(list[i]).attr('newsort') < $(list[i + 1]).attr('newsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }
            Comment_Show.reload(reload_obj);

    }

    // Media Comments votes
    $('body').on('click', '.media-comment-vote.up', function (e) {
        var comment_id = $(this).attr('id');
        $.ajax({
            method: "POST",
            url: "{{route('media.comment_updownvote')}}",
            data: {comment_id: comment_id, vote_type:1}
        })
            .done(function (res) {
                var up_result = JSON.parse(res);
                $('.upvote-'+comment_id+' b').html(up_result.count_upvotes);
                $('.downvote-'+comment_id+' b').html(up_result.count_downvotes);
            });
        e.preventDefault()
     });

     $('body').on('click', '.media-comment-vote.down', function (e) {
      var comment_id = $(this).attr('id');
      $.ajax({
          method: "POST",
          url: "{{route('media.comment_updownvote')}}",
          data: {comment_id: comment_id, vote_type:2}
      })
          .done(function (res) {
              var down_result = JSON.parse(res);
              $('.upvote-'+comment_id+' b').html(down_result.count_upvotes);
              $('.downvote-'+comment_id+' b').html(down_result.count_downvotes);
          });
      e.preventDefault()
     });

    //photo commenting end

    // like & unlike photo
    $('body').on('click', '.photo_like_button', function (e) {
        mediaId = $(this).attr('id');
        $.ajax({
            method: "POST",
            url: "{{ route('media.likeunlike') }}",
            data: {media_id: mediaId}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if (result.status == 'yes') {
                    alert("You have just liked this photo.");

                } else if (result.status == 'no') {
                    alert("You have just unliked this photo.");
                }
            });
        e.preventDefault();
    });
</script>

<script>
    $('#modal-share').find('.place_share_btn').click(function(){

        $.ajax({
                method: "POST",
                url: "{{ route(session('place_country') . '.share', $place->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    toastr.success(res.msg);

                    $('body').find("#modal-share").modal('hide');
                });
    });

    function getMediadata4mediamodal(id, obj, key) {
        var comment_body = $("#modal-medias").find(".comments");
        var media_form = $("#modal-medias").find(".mediModalCommentForm");
        if( key == 'init' ) {
            var count = 0;
            $('.comments__load-more.media').show();
            comment_body.find(".post-comment-row").parent().remove();
            if( $(obj).attr('data-video') == "0" ) {
                $("#modal-medias").find(".trip__placeholder").replaceWith(`
                    <div class="trip__placeholder" style="background-size:cover;background-image: url('` + $(obj).attr('data-src') + `')"></div>
                `);
            }else{
                $("#modal-medias").find(".trip__placeholder").replaceWith(`
                    <div class="trip__placeholder">
                        <video class="post-media__img video-object" controls data-id='`+id+`'>
                            <source src="` + $(obj).attr('data-src') + `" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                `);
            }
        } else {
            var count = comment_body.find(".post-comment-row").length;
        }
        comment_body.find(".comments__load-more").attr("data-id", id);
        $.ajax({
                method: "POST",
                url: "{{ route((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) . '.ajax_media_comment4modal') }}",
                data: {media_id: id, count: count}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if(result.author != '') {
                        var profile_picture = '';
                        if(result.author.profile_picture!='') {
                            profile_picture = result.author.profile_picture;
                        }
                        else {
                            if(result.author.gender==1 || result.author.gender == null) {
                                profile_picture = 'https://www.travooo.com/assets2/image/placeholders/male.png';
                            }
                            else if(result.author.gender==2) {
                                profile_picture = 'https://www.travooo.com/assets2/image/placeholders/female.png';
                            }
                        }


                        $("#modal-medias").find('#userInfo').html('<img class="trip__user-avatar" src="'+profile_picture+'" /><div class="trip__user-info"><div class="trip__user-name">By <a href="https://www.travooo.com/profile/'+result.author.id+'">'+result.author.name+'</a></div><div class="trip__posted">'+result.uploaded_at+'</div></div>');
                        if(result.caption) {
                            var caption = result.caption;
                            if (caption.length > 50)
                                    caption = caption.substr(0,50)+'...';

                            $("#modal-medias").find('#caption').html('<span>'+caption+'</span>');
                            $("#modal-medias").find('#caption').css('display', 'block');
                        }
                        else {
                            $("#modal-medias").find('#caption').css('display', 'none');
                        }

                    }else{
                        $("#modal-medias").find('#userInfo').html('');
                        $("#modal-medias").find('#caption').css('display', 'none');
                        $("#modal-medias").find('#caption').html('');
                    }

                    @if(Auth::check())
                        var like_btn = 'media_like_button'
                    @else
                        var like_btn = 'open-login'
                    @endif

                    $("#modal-medias").find('#mediaLikes').html('<button class="post__like-btn '+ like_btn +'" style="outline:none;" type="button" data-id="'+result.media.id+'"><div class="emoji" dir="auto"><i class="fa fa-heart '+result.like_class+'" aria-hidden="true" dir="auto" style="font-size: 14px;padding-right:3px;"></i></div><span><strong>'+result.media_likes+'</strong> Likes</span></button>');
                    if(result.data != ''){
                        comment_body.find(".comments__items").find(".comments__load-more").before(result.data);
                    }else{

                    }
                    var cnt = comment_body.find(".comments__items").find(".post-comment-row").length;
                    comment_body.find(".comments__total").html(result.rows + " Comments");
                    comment_body.find(".comments__number").html( cnt + "/" + result.rows );
                    comment_body.attr('data-total_cnt', result.rows);
                    if(cnt == result.rows) $('.comments__load-more.media').hide();

                    // change form media id
                    media_form.find('input[name="medias_id"]').val(id)
                    media_form.find('input[name="pair"]').attr('id', 'pair'+id)
                    media_form.find('.modal-media-file-input').attr('data-media_id', id)
                });
    }

    $(document).on('click', '.comments__load-more.media', function(e){
        getMediadata4mediamodal($(this).attr("data-id"), this, 'load');
    });

    $(document).on('click', '.comments__load-more.trip', function(e){
        var obj = $(this);
        var comment_body = $("#modal-trips").find(".comments");
        var id = $(this).attr('data-id');
        var count = obj.parent().find('div[topsort]').length;

        $.ajax({
                method: "POST",
                url: "{{ route((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) . '.ajax_trip_comment4modal') }}",
                data: {trip_id: id, count: count}
            })
                .done(function (res) {
                    var result = JSON.parse(res);

                    if(result.data != ''){
                        comment_body.find(".comments__items").find(".comments__load-more").before(result.data);
                    }else{

                    }
                    var cnt = obj.parent().find('div[topsort]').length;

                    comment_body.find(".comments__total").html(result.rows + " Comments");
                    comment_body.find(".comments__number").html( cnt + "/" + result.rows );
                    if(cnt == result.rows) $('.comments__load-more.trip').hide();
                });
    });

    $('body').on('click', '.comments__load-more.report', function (e) {
        obj = $(this);
        report_id = $(this).attr('data-id');
        count = obj.parent().find('div[topsort]').length;
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: "{{ route('report.ajax_report_comment4modal') }}",
            data: {report_id: report_id, count: count}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if(result.data != ''){
                    obj.before(result.data);
                }else{

                }
                var cnt = obj.parent().find("div[topsort]").length;
                if(cnt == result.rows) obj.hide();
            });

    });

    $('body').click(function(evt){
       if(evt.target.className == "lg-inner")
          $('.lg-close.lg-icon').click();

        if($(evt.target).hasClass("custom-modal"))
        {
            $(evt.target).find('.modal__close').click();
        }
    });

    $('body').on('click', '[href="#reports__modal"]', function () {
        if ($('.travelogs__items_reports')[0].slick === undefined) {

            $('.travelogs__items_reports').slick({
                centerMode: !0,
                centerPadding: "30px",
                variableWidth: !0,
                arrows: !1,
                focusOnSelect: !0,
                asNavFor: ".travelog-carousel_reports",
            });
            $('.travelog-carousel_reports').slick({
                centerMode: !0,
                variableWidth: !0,
                focusOnSelect: !0,
                asNavFor: ".travelogs__items_reports",
                prevArrow: '<button class="travelog-carousel__control travelog-carousel__control--prev"><i class="fal fa-chevron-left"></i></button>',
                nextArrow: '<button class="travelog-carousel__control travelog-carousel__control--next"><i class="fal fa-chevron-right"></i></button>'
            });

            $('.travelogs__items_reports').find('.travelogs__item').on('click',function(){
                var slideno = $('.travelogs__items_reports')[0].slick.currentSlide;
                $('.travelog-carousel_reports').slick('slickGoTo', slideno);
            });

            $('.travelog-carousel_reports').on('afterChange',function(event, slick, currentSlide){

                $('.travelogs__items_reports').slick('slickGoTo', currentSlide);
            });
        }
        $("#reports__modal").find("img").each(function(){
            if($(this).attr("src") == undefined) {
                $(this).attr("src", $(this).attr("data-src"));
            }
        });

    });

    $('body').on('click', '[href="#modal-events"]', function (){

        $(".events__items").not('.slick-initialized').slick({
            centerMode: !0,
            centerPadding: "30px",
            variableWidth: !0,
            arrows: !1,
            focusOnSelect: !0
        });

        $("#modal-events").find("img").each(function(){
            if($(this).attr("src") == undefined) {
                $(this).attr("src", $(this).attr("data-src"));
            }
        });
    });

    $('body').on('click', '[href="#modal-trips"]', function (){

        $(".trip__cards-items").not('.slick-initialized').slick({
            centerMode: !0,
            variableWidth: !0,
            focusOnSelect: !0,
            infinite: !1,
            prevArrow: '<button class="trip__cards-control trip__cards-control--prev"><i class="fal fa-chevron-left"></i></button>',
            nextArrow: '<button class="trip__cards-control trip__cards-control--next"><i class="fal fa-chevron-right"></i></button>'
        });

        // $("#modal-trips").find("img").each(function(){
        //     if($(this).attr("src") == undefined) {
        //         $(this).attr("src", $(this).attr("data-src"));
        //     }
        // });
    });

    $('body').on('click', '[href="#modal-medias"]', function (){

        $(".trip__cards-items").not('.slick-initialized').slick({
            centerMode: !0,
            variableWidth: !0,
            focusOnSelect: !0,
            infinite: !1,
            prevArrow: '<button class="trip__cards-control trip__cards-control--prev"><i class="fal fa-chevron-left"></i></button>',
            nextArrow: '<button class="trip__cards-control trip__cards-control--next"><i class="fal fa-chevron-right"></i></button>'
        });

        $('#modal-medias').find('.trip__cards').find('.slick-current').click();
    });

    function project(lat,lng, zoom) {
        var siny = Math.sin(lat * Math.PI / 180);

        // Truncating to 0.9999 effectively limits latitude to 89.189. This is
        // about a third of a tile past the edge of the world tile.
        siny = Math.min(Math.max(siny, -0.9999), 0.9999);

        var worldCoordinate = new google.maps.Point(
            256 * (0.5 + lng / 360),
            256 * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));

        var scale = 1 << zoom;

        return new google.maps.Point(
            Math.floor(worldCoordinate.x * scale),
            Math.floor(worldCoordinate.y * scale));

    }

    function getmap4tripmodal(clat, clng, lat, lng, zoom, obj) {

        let $$ = $(obj).closest('.trip__main').find('.trip__placeholder');
        let w = $$.width();
        let h = $$.height();
        $$.find(".place_image").remove();

        var p_C = project(lat,lng,zoom);
        var p_C_c = project(clat,clng,zoom);

        let width = w/2 + (p_C.x - p_C_c.x) * (w/642) - 17;
        let height = h/2 + (p_C.y - p_C_c.y) * (h/464) - 17;

        let el = $('<div class="place_image" style="width:34px;height:34px;position:absolute;left:'+width+'px;top:'+height+'px;border-radius:100%;border:2px solid white;overflow:hidden;"><img style="width:100%;height:100%" src="'+$(obj).find('img').attr('src')+'"/></div>');
        $$.append(el);
    }

    function getTripdata4tripmodal(id, obj) {
        $.ajax({
            method: "POST",
            url: "{{ route((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) . '.ajaxgettripdata') }}",
            data: {id: id},
            dataType: 'json'
        })
            .done(function (res) {

                if (res.msg == 'ok') {

                    $(obj).closest('#modal-trips').find('.trip__inner').html(res.content);

                    $("#modal-trips").find(".carousel-cards").slick({
                        variableWidth: !0,
                        focusOnSelect: !0,
                        prevArrow: '<button class="carousel-cards__control carousel-cards__control--prev" style="outline: none;left: 0;justify-content: flex-start;display: flex !important;background: none !important;"><svg class="icon icon--angle-left"> <use xlink:href="img/sprite.svg#angle-left"></use> </svg></button>',
                        nextArrow: '<button class="carousel-cards__control carousel-cards__control--next" style="outline: none;"><svg class="icon icon--angle-right"> <use xlink:href="img/sprite.svg#angle-right"></use> </svg></button>'
                    });
                } else {

                }
                //console.log('#post_like_count_' + postId);
            });
    }

    function modal_blog_show(obj, e, type, id) {

        if($(e.target).closest('.dropdown').length > 0) return;
        if($(e.target).closest('button').length > 0) return;
        if($(e.target).closest('.comments').length > 0) return;
        if( $(e.target).hasClass('morelink') ) return;

        switch (type) {
            case 'discussion':
                $(".hero__see-all")[0].click();
                $("#tab-qa").click();
                $("#box-qa").find(".about-modal__questions").attr("travoo-current-page", 0);
                $("#box-qa").find(".question").css("display", "none").removeClass("displayed");
                $("#box-qa").find(".question[data-id='" + id + "']").fadeIn();
                break;
            case 'media':

                var obja = $("#modal-medias").find(".trip__cards").find(".trip__card[data-id='" + id + "']");

//                getMediadata4mediamodal(id, obja, 'init');
                $('[href="#modal-medias"]')[0].click();
                setTimeout(() => {
                    $("#modal-medias").find('.trip__cards').hide();
                }, 300);
                break;
            case 'plan':
                if( $(e.target).closest('.carousel__item').length > 0 ) break;
                var obja = $("#modal-trips").find(".trip__cards").find(".trip__card")[0];
                getTripdata4tripmodal(id, obja);
                $('[href="#modal-trips"]')[0].click();
                setTimeout(() => {
                    $("#modal-trips").find('.trip__cards').hide();
                }, 300);
                break;
            case 'event':
                if( $(e.target).closest('.post-add-comment-block').length > 0 ) break;
                if( $(e.target).closest('.post-comment-wrapper').length > 0 ) break;
                $('[href="#modal-events"]')[0].click();
                $("#modal-events").find('.events__item[data-id="' + id + '"]')[0].click();
                break;
            case 'report':
                $('[href="#reports__modal"]')[0].click();
                $('.reportsTrigger').click();
                $("#reports__modal").find('.travelog-carousel__item[data-id="' + id + '"]')[0].click();
                setTimeout(() => {
                    $("#reports__modal").find('.travelog-carousel_reports').hide();
                }, 300);
                break;
            case 'review':
                $(".hero__see-all")[0].click();
                $("#tab-ratings").click();
                $("#box-ratings").find(".comments__items").attr("travoo-current-page", 0);
                $("#box-ratings").find(".comment").css("display", "none").removeClass("displayed");
                $("#box-ratings").find(".comment[data-id='" + id + "']").fadeIn();
                break;
            default:
                break;
        }

    }

    $(document).on('click', '#modal-trips .trip__cards-control', function(e){
      $(this).closest('#modal-trips').find('.trip__cards').find('.slick-current').click();
    });

    $(document).on('click', '#modal-medias .trip__cards-control', function(e){
      $(this).closest('#modal-medias').find('.trip__cards').find('.slick-current').click();
    });


    //for following of new traveller
    function travel_mate_following(type, id, obj)
    {
        var followurl = "{{  route('profile.follow', ':user_id')}}";
        var unfollowurl = "{{  route('profile.unfolllow', ':user_id')}}";
        if(type == "follow")
        {
            url = followurl.replace(':user_id', id);
        }
        else if(type == "unfollow")
        {
            url = unfollowurl.replace(':user_id', id);
        }
        $.ajax({
            method: "POST",
            url: url,
            data: {name: "test"}
        })
        .done(function (res) {
            if (res.success == true) {
                if(type == "follow")
                {
                    $(obj).replaceWith('<button class="btn btn--md btn--grey" type="button" onclick="travel_mate_following(\'unfollow\', ' + id + ', this)"><svg class="icon icon--add-user"><use xlink:href="{{asset('assets3/img/sprite.svg#user')}}"></use></svg>UnFollow</button>');
                }
                else if(type == "unfollow")
                {
                    $(obj).replaceWith('<button class="btn btn--md btn--main" type="button" onclick="travel_mate_following(\'follow\', ' + id + ', this)"><svg class="icon icon--add-user"><use xlink:href="{{asset('assets3/img/sprite.svg#add-user')}}"></use></svg>Follow</button>');
                }
            } else if (res.success == false) {

            }
        });
    }

    $(document).ready(function(){
        $(".header-search-block").append(`
        <button class="location-identity" type="button">
            <svg class="icon icon--like">
                <use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use>
            </svg>
        </button>`);

        $(".header-search-block").find("#mainSearchInput").attr("placeholder", "Search in {{@$place->trans[0]->title}}...");

        $(".location-identity").tooltip({
            title: "Active to see results about this place.",
            placement: "bottom",
            delay: {show:0, hide:1000}
        })
        $(".location-identity").tooltip('show');
        setTimeout(() => {
            $(".location-identity").tooltip('hide');
        }, 1500);
    });
    $(document).on('click', '.location-identity', function(e) {
        if( $(this).hasClass('active') ) {
            $(this).tooltip('enable');
            // $(this).tooltip('show');
            $(this).removeClass('active');
        } else {
            var divPosition = $('#body4highlight').offset();
            $('html, body').animate({scrollTop: divPosition.top-100}, "slow");
            $(this).tooltip('disable');
            $(this).addClass('active');
            $(this).tooltip('hide');
        }
    });

    $(document).on('click', '.disc-modal', function(e){
        var dis_id = $(this).data('dis_id');

        DISCUSSION_POST.showDiscussionModal(dis_id);
    });

    $(document).on('click', '.video-object', function(e){
        console.log('test');
            var vid_id = $(this).attr('data-id');
            $.ajax({
                method: "POST",
                url: "{{url('medias/')}}/"+vid_id+"/updateviews",
                data: {vid_id: vid_id},
            })
                .done(function (res) {

                });
        });

        $('#about_switch').css('font-weight', 'bold');
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeOut();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeOut();



    $(document).on('click', '#about_switch', function() {
        $('#about_switch').css('font-weight', 'bold');
        $('#tripstips_switch').css('font-weight', 'normal');
        $('#weather_switch').css('font-weight', 'normal');
        $('#etiquette_switch').css('font-weight', 'normal');
        $('#health_switch').css('font-weight', 'normal');
        $('#visa_switch').css('font-weight', 'normal');
        $('#whentogo_switch').css('font-weight', 'normal');
        $('#caution_switch').css('font-weight', 'normal');
        $('#holidays_switch').css('font-weight', 'normal');
        $('#restriction_switch').css('font-weight', 'normal');
        $('#emergency_switch').css('font-weight', 'normal');

        $('#about_content').fadeIn();
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeOut();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeOut();

    });

    $(document).on('click', '#tripstips_switch', function() {


        $('#about_switch').css('font-weight', 'normal');
        $('#tripstips_switch').css('font-weight', 'bold');
        $('#weather_switch').css('font-weight', 'normal');
        $('#etiquette_switch').css('font-weight', 'normal');
        $('#health_switch').css('font-weight', 'normal');
        $('#visa_switch').css('font-weight', 'normal');
        $('#whentogo_switch').css('font-weight', 'normal');
        $('#caution_switch').css('font-weight', 'normal');
        $('#holidays_switch').css('font-weight', 'normal');
        $('#restriction_switch').css('font-weight', 'normal');
        $('#emergency_switch').css('font-weight', 'normal');

        $('#about_content').fadeOut();
        $('#tripstips_content').fadeIn();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeOut();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeOut();

    });

    $(document).on('click', '#weather_switch', function() {


        $('#about_switch').css('font-weight', 'normal');
        $('#tripstips_switch').css('font-weight', 'normal');
        $('#weather_switch').css('font-weight', 'bold');
        $('#etiquette_switch').css('font-weight', 'normal');
        $('#health_switch').css('font-weight', 'normal');
        $('#visa_switch').css('font-weight', 'normal');
        $('#whentogo_switch').css('font-weight', 'normal');
        $('#caution_switch').css('font-weight', 'normal');
        $('#holidays_switch').css('font-weight', 'normal');
        $('#restriction_switch').css('font-weight', 'normal');
        $('#emergency_switch').css('font-weight', 'normal');



        $('#about_content').fadeOut();
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeIn();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeOut();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeOut();

    });

    $(document).on('click', '#etiquette_switch', function() {

        $('#about_switch').css('font-weight', 'normal');
        $('#tripstips_switch').css('font-weight', 'normal');
        $('#weather_switch').css('font-weight', 'normal');
        $('#etiquette_switch').css('font-weight', 'bold');
        $('#health_switch').css('font-weight', 'normal');
        $('#visa_switch').css('font-weight', 'normal');
        $('#whentogo_switch').css('font-weight', 'normal');
        $('#caution_switch').css('font-weight', 'normal');
        $('#holidays_switch').css('font-weight', 'normal');
        $('#restriction_switch').css('font-weight', 'normal');
        $('#emergency_switch').css('font-weight', 'normal');

        $('#about_content').fadeOut();
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeIn();
        $('#health_content').fadeOut();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeOut();

    });

    $(document).on('click', '#health_switch', function() {

        $('#about_switch').css('font-weight', 'normal');
        $('#tripstips_switch').css('font-weight', 'normal');
        $('#weather_switch').css('font-weight', 'normal');
        $('#etiquette_switch').css('font-weight', 'normal');
        $('#health_switch').css('font-weight', 'bold');
        $('#visa_switch').css('font-weight', 'normal');
        $('#whentogo_switch').css('font-weight', 'normal');
        $('#caution_switch').css('font-weight', 'normal');
        $('#holidays_switch').css('font-weight', 'normal');
        $('#restriction_switch').css('font-weight', 'normal');
        $('#emergency_switch').css('font-weight', 'normal');

        $('#about_content').fadeOut();
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeIn();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeOut();

    });

    $(document).on('click', '#visa_switch', function() {

        $('#about_switch').css('font-weight', 'normal');
        $('#tripstips_switch').css('font-weight', 'normal');
        $('#weather_switch').css('font-weight', 'normal');
        $('#etiquette_switch').css('font-weight', 'normal');
        $('#health_switch').css('font-weight', 'normal');
        $('#visa_switch').css('font-weight', 'bold');
        $('#whentogo_switch').css('font-weight', 'normal');
        $('#caution_switch').css('font-weight', 'normal');
        $('#holidays_switch').css('font-weight', 'normal');
        $('#restriction_switch').css('font-weight', 'normal');
        $('#emergency_switch').css('font-weight', 'normal');

        $('#about_content').fadeOut();
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeOut();
        $('#visa_content').fadeIn();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeOut();

    });

    $(document).on('click', '#whentogo_switch', function() {

        $('#about_switch').css('font-weight', 'normal');
        $('#tripstips_switch').css('font-weight', 'normal');
        $('#weather_switch').css('font-weight', 'normal');
        $('#etiquette_switch').css('font-weight', 'normal');
        $('#health_switch').css('font-weight', 'normal');
        $('#visa_switch').css('font-weight', 'normal');
        $('#whentogo_switch').css('font-weight', 'bold');
        $('#caution_switch').css('font-weight', 'normal');
        $('#holidays_switch').css('font-weight', 'normal');
        $('#restriction_switch').css('font-weight', 'normal');
        $('#emergency_switch').css('font-weight', 'normal');

        $('#about_content').fadeOut();
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeOut();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeIn();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeOut();

    });

    $(document).on('click', '#caution_switch', function() {

        $('#about_switch').css('font-weight', 'normal');
        $('#tripstips_switch').css('font-weight', 'normal');
        $('#weather_switch').css('font-weight', 'normal');
        $('#etiquette_switch').css('font-weight', 'normal');
        $('#health_switch').css('font-weight', 'normal');
        $('#visa_switch').css('font-weight', 'normal');
        $('#whentogo_switch').css('font-weight', 'normal');
        $('#caution_switch').css('font-weight', 'bold');
        $('#holidays_switch').css('font-weight', 'normal');
        $('#restriction_switch').css('font-weight', 'normal');
        $('#emergency_switch').css('font-weight', 'normal');

        $('#about_content').fadeOut();
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeOut();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeIn();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeOut();

    });

    $(document).on('click', '#holidays_switch', function() {

        $('#about_switch').css('font-weight', 'normal');
        $('#tripstips_switch').css('font-weight', 'normal');
        $('#weather_switch').css('font-weight', 'normal');
        $('#etiquette_switch').css('font-weight', 'normal');
        $('#health_switch').css('font-weight', 'normal');
        $('#visa_switch').css('font-weight', 'normal');
        $('#whentogo_switch').css('font-weight', 'normal');
        $('#caution_switch').css('font-weight', 'normal');
        $('#holidays_switch').css('font-weight', 'bold');
        $('#restriction_switch').css('font-weight', 'normal');
        $('#emergency_switch').css('font-weight', 'normal');

        $('#about_content').fadeOut();
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeOut();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeIn();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeOut();

    });
    $(document).on('click', '#restriction_switch', function() {

        $('#about_switch').css('font-weight', 'normal');
        $('#tripstips_switch').css('font-weight', 'normal');
        $('#weather_switch').css('font-weight', 'normal');
        $('#etiquette_switch').css('font-weight', 'normal');
        $('#health_switch').css('font-weight', 'normal');
        $('#visa_switch').css('font-weight', 'normal');
        $('#whentogo_switch').css('font-weight', 'normal');
        $('#caution_switch').css('font-weight', 'normal');
        $('#holidays_switch').css('font-weight', 'normal');
        $('#restriction_switch').css('font-weight', 'bold');
        $('#emergency_switch').css('font-weight', 'normal');
        
        $('#about_content').fadeOut();
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeOut();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeIn();
        $('#emergency_content').fadeOut();

    });

    $(document).on('click', '#emergency_switch', function() {

        $('#about_switch').css('font-weight', 'normal');
        $('#tripstips_switch').css('font-weight', 'normal');
        $('#weather_switch').css('font-weight', 'normal');
        $('#etiquette_switch').css('font-weight', 'normal');
        $('#health_switch').css('font-weight', 'normal');
        $('#visa_switch').css('font-weight', 'normal');
        $('#whentogo_switch').css('font-weight', 'normal');
        $('#caution_switch').css('font-weight', 'normal');
        $('#holidays_switch').css('font-weight', 'normal');
        $('#restriction_switch').css('font-weight', 'normal');
        $('#emergency_switch').css('font-weight', 'bold');

        $('#about_content').fadeOut();
        $('#tripstips_content').fadeOut();
        $('#weather_content').fadeOut();
        $('#etiquette_content').fadeOut();
        $('#health_content').fadeOut();
        $('#visa_content').fadeOut();
        $('#whentogo_content').fadeOut();
        $('#caution_content').fadeOut();
        $('#holidays_content').fadeOut();
        $('#restriction_content').fadeOut();
        $('#emergency_content').fadeIn();

    });

    </script>

<script>
    /**
     * Returns the Popup class.
     *
     * Unfortunately, the Popup class can only be defined after
     * google.maps.OverlayView is defined, when the Maps API is loaded.
     * This function should be called by initMap.
     */
    function createPopupClass() {
        /**
         * A customized popup on the map.
         * @param {!google.maps.LatLng} position
         * @param {!Element} content The bubble div.
         * @constructor
         * extends {google.maps.OverlayView}
         */
        function Popup(position, content) {
            this.position = position;

            content.classList.add('popup-bubble');

            // This zero-height div is positioned at the bottom of the bubble.
            var bubbleAnchor = document.createElement('div');
            bubbleAnchor.classList.add('popup-bubble-anchor');
            bubbleAnchor.appendChild(content);

            // This zero-height div is positioned at the bottom of the tip.
            this.containerDiv = document.createElement('div');
            this.containerDiv.classList.add('popup-container');
            this.containerDiv.appendChild(bubbleAnchor);

            // Optionally stop clicks, etc., from bubbling up to the map.
            google.maps.OverlayView.preventMapHitsAndGesturesFrom(this.containerDiv);
        }
        // ES5 magic to extend google.maps.OverlayView.
        Popup.prototype = Object.create(google.maps.OverlayView.prototype);

        /** Called when the popup is added to the map. */
        Popup.prototype.onAdd = function() {
            this.getPanes().floatPane.appendChild(this.containerDiv);
        };

        /** Called when the popup is removed from the map. */
        Popup.prototype.onRemove = function() {
            if (this.containerDiv.parentElement) {
                this.containerDiv.parentElement.removeChild(this.containerDiv);
            }
        };

        /** Called each frame when the popup needs to draw itself. */
        Popup.prototype.draw = function() {
            var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);

            // Hide the popup when it is far out of view.
            var display =
                Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                    'block' :
                    'none';

            if (display === 'block') {
                this.containerDiv.style.left = divPosition.x + 'px';
                this.containerDiv.style.top = divPosition.y + 'px';
            }
            if (this.containerDiv.style.display !== display) {
                this.containerDiv.style.display = display;
            }
        };

        // Set the visibility to 'hidden' or 'visible'.
        Popup.prototype.hide = function() {
            if (this.containerDiv) {
                // The visibility property must be a string enclosed in quotes.
                this.containerDiv.style.visibility = 'hidden';
            }
        };

        Popup.prototype.show = function() {
            if (this.containerDiv) {
                this.containerDiv.style.visibility = 'visible';
            }
        };

        Popup.prototype.isVisible = function() {
            if (this.containerDiv) {
                return this.containerDiv.style.visibility == 'visible';
            }
            return false;
        };

        Popup.prototype.toggle = function() {
            if (this.containerDiv) {
                if (this.containerDiv.style.visibility === 'hidden') {
                    this.show();
                } else {
                    this.hide();
                }
            }
        };

        return Popup;
    }

    function TripPlanInsight(mapIndex, container, avatar_src){
        this._mapIndex = mapIndex;
        this._container = container;
        this._avatar_src = avatar_src;
        this._data = {
            checked_on: '',
            duration: '',
            budget: '',
            upcoming: true,
        };

        this.update();
    }

    TripPlanInsight.prototype.template = function() {
        
        var $text = $('<div class="trip__info-text" dir="auto">'+this._data.checked_on+' </div>');

        // if( !! this._data.upcoming ) {
        //     $text = $('<div class="trip__info-text" dir="auto">Checking on '+this._data.checked_on+' and will stay <strong dir="auto">'+this._data.duration+' min</strong></div>');
        // }

        var $block = $('<div class="trip__info" dir="auto">' +
            '<img class="trip__info-avatar" src="'+this._avatar_src+'" alt="#" title="" dir="auto">' +
            '</div>');

        $block.append($text);

        return $block.get(0);
    };

    TripPlanInsight.prototype.setDataAndUpdate = function(newData){
        $.extend( this._data, newData );
        this.update();
    };

    TripPlanInsight.prototype.update = function(){
        this.remove();
        this.draw();
    };
    TripPlanInsight.prototype.draw = function(){
        this._container.appendChild(this.template());
    };
    TripPlanInsight.prototype.remove = function(){
        this._container.innerHTML = '';
    };



    var DISCUSSION_POST = {

        upvoteReply: function(reply_id){
            if(typeof reply_id === 'undefined' || ! reply_id)
                return;

            var that = this;

            // toggle icons
            this.isActiveUpvoteIcon(reply_id) ? this.deactivateUpvoteReplyIcon(reply_id) : this.activateUpvoteReplyIcon(reply_id);
            this.deactivateDownvoteReplyIcon(reply_id);

            $.ajax({
                    method: "POST",
                    url: "{{ route('discussion.upvote') }}",
                    data: {replies_id: reply_id}
                })
                    .done(function(res){
                        var result = JSON.parse(res);

                        if (result.status == 1) {
                            if(that.getUpvoteNum(reply_id) > result.count_upvotes) {
                                that.deactivateUpvoteReplyIcon(reply_id);
                            }

                            that.setUpvoteNum(reply_id, result.count_upvotes);
                            that.setDownvoteNum(reply_id, result.count_downvotes);
                        }
                    }
            );
        },

        downvoteReply: function(reply_id, el, event){
            if(typeof reply_id === 'undefined' || ! reply_id)
                return;

            var that = this;

            // toggle icons
            this.isActiveDownvoteIcon(reply_id) ? this.deactivateDownvoteReplyIcon(reply_id) : this.activateDownvoteReplyIcon(reply_id);
            this.deactivateUpvoteReplyIcon(reply_id);

            $.ajax({
                method: "POST",
                url: "{{ route('discussion.downvote') }}",
                data: {replies_id: reply_id}
            })
                .done(function(res){
                        var result = JSON.parse(res);

                        if (result.status == 1) {
                            if(that.getDownvoteNum(reply_id) > result.count_downvotes) {
                                that.deactivateDownvoteReplyIcon(reply_id);
                            }

                            that.setUpvoteNum(reply_id, result.count_upvotes);
                            that.setDownvoteNum(reply_id, result.count_downvotes);
                        }
                    }
                );
        },

        getUpvoteNum: function(reply_id){
            return parseInt($(".post__tip-upvotes.vote-btn-"+reply_id+" .num_votes").text());
        },
        setUpvoteNum: function(reply_id, num){
            $(".post__tip-upvotes.vote-btn-"+reply_id+" .num_votes").text( num );
        },
        getDownvoteNum: function(reply_id){
            return parseInt($(".post__tip-downvotes.vote-btn-"+reply_id+" .num_votes").text());
        },
        setDownvoteNum: function(reply_id, num){
            $(".post__tip-downvotes.vote-btn-"+reply_id+" .num_votes").text( num );
        },

        isActiveUpvoteIcon: function(reply_id){
            return $(".post__tip-upvotes.vote-btn-"+reply_id+" .icon-vote").hasClass( "icon-vote-active" );
        },
        activateUpvoteReplyIcon: function(reply_id){
            $(".post__tip-upvotes.vote-btn-"+reply_id+" .icon-vote").addClass( "icon-vote-active" );
        },
        deactivateUpvoteReplyIcon: function(reply_id){
            $(".post__tip-upvotes.vote-btn-"+reply_id+" .icon-vote").removeClass( "icon-vote-active" );
        },
        isActiveDownvoteIcon: function(reply_id){
            return $(".post__tip-downvotes.vote-btn-"+reply_id+" .icon-vote").hasClass( "icon-vote-active" );
        },
        activateDownvoteReplyIcon: function(reply_id){
            $(".post__tip-downvotes.vote-btn-"+reply_id+" .icon-vote").addClass( "icon-vote-active" );
        },
        deactivateDownvoteReplyIcon: function(reply_id){
            $(".post__tip-downvotes.vote-btn-"+reply_id+" .icon-vote").removeClass( "icon-vote-active" );
        },



        showDiscussionModal:function(discussion_id){
            $.ajax({
                method: "POST",
                url: "{{route('discussion.post_view_count')}}",
                data: {discussion_id: discussion_id},
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if(result.status == 1){
                        $('.' + discussion_id + '-view-count').html(result.count_views)
                    }
                });
            $('#Discussion' + discussion_id).modal('show')
        }
    };

    /**
     * AddPostBox
     *
     * */
    function AddPostBox(main, discussion, tripplan,travelmates,reviews,reports){
        this.blocks = {
            'main': main,
            'discussion': discussion,
            'tripplan': tripplan,
            'travelmates':travelmates,
            'reviews':reviews,
            'reports':reports
        };
    }

    AddPostBox.prototype.hideAll = function(){
        for (var prop in this.blocks) {
            this.blocks[prop].hide();
        }
    };

    AddPostBox.prototype.hide = function(block) {
        if(typeof block === 'undefined'){
            this.hideAll();
            return;
        }

        if ( this.blocks[block] ) {
            this.blocks[block].hide();
        }
    };

    AddPostBox.prototype.show = function(block) {
        if(typeof block === 'undefined'){
            this.show('main');
            return;
        }

        if (this.blocks[block]) {
            this.blocks[block].show();

            // hide all others
            for (var prop in this.blocks) {
                if(prop === block)
                    continue;

                this.blocks[prop].hide();
            }
        }
    };

    /**
     * AddPostMain
     * @param el
     * @constructor
     */
    function AddPostMain(el){
        this.el = el;
    }

    AddPostMain.prototype.hide = function() {
        if (this.el) {
            // The visibility property must be a string enclosed in quotes.
            this.el.style.display = 'none';
        }
    };

    AddPostMain.prototype.show = function() {
        if (this.el) {
            this.el.style.display = 'block';
        }
    };

    /**
     * AddPostDiscussionBlock
     * @param el
     * @constructor
     */
    function AddPostDiscussionBlock(el){
        this.el = el;
    }

    AddPostDiscussionBlock.prototype.hide = function() {
        if (this.el) {
            // The visibility property must be a string enclosed in quotes.
            this.el.style.display = 'none';
        }
    };

    AddPostDiscussionBlock.prototype.show = function() {
        if (this.el) {
            this.el.style.display = 'block';
        }
    };


    /**
     * AddPostTripplanBlock
     * @param el
     * @constructor
     */
    function AddPostTripplanBlock(el){
        this.el = el;
    }

    AddPostTripplanBlock.prototype.hide = function() {
        if (this.el) {
            // The visibility property must be a string enclosed in quotes.
            this.el.style.display = 'none';
        }
    };

    AddPostTripplanBlock.prototype.show = function() {
        if (this.el) {
            this.el.style.display = 'block';
        }
    };

    /**
     * AddPostTravelMatesBlock
     * @param el
     * @constructor
     */
     function AddPostTravelMatesBlock(el){
        this.el = el;
    }

    AddPostTravelMatesBlock.prototype.hide = function() {
        if (this.el) {
            // The visibility property must be a string enclosed in quotes.
            this.el.style.display = 'none';
        }
    };

    AddPostTravelMatesBlock.prototype.show = function() {
        if (this.el) {
            this.el.style.display = 'block';
        }
    };
    
    /**
     * AddPostReviewBlock
     * @param el
     * @constructor
     */
     function AddPostReviewBlock(el){
        this.el = el;
    }

    AddPostReviewBlock.prototype.hide = function() {
        if (this.el) {
            // The visibility property must be a string enclosed in quotes.
            this.el.style.display = 'none';
        }
    };

    AddPostReviewBlock.prototype.show = function() {
        if (this.el) {
            this.el.style.display = 'block';
        }
    };
    
    /**
     * AddPostReportBlock
     * @param el
     * @constructor
     */
     function AddPostReportBlock(el){
        this.el = el;
    }

    AddPostReportBlock.prototype.hide = function() {
        if (this.el) {
            // The visibility property must be a string enclosed in quotes.
            this.el.style.display = 'none';
        }
    };

    AddPostReportBlock.prototype.show = function() {
        if (this.el) {
            this.el.style.display = 'block';
        }
    };
</script>
<script>
    
     setTimeout(function () {
            $('#placeAsideAnimation').remove()
            $('#placeAsideBlock').removeClass('d-none')
        }, 2100);
        
       
    $(document).on('click', ".post_like_button a", function (e) {
        e.preventDefault();
        $(this).closest('.post_like_button').toggleClass('liked');
    });
    
    $('body').on('click', '.post_like_button a', function (e) {
            postId = $(this).attr('id');
            type = $(this).data('type');
            if(type == 'trip'){
                postId = $(this).data('id');
                $.ajax({
                    url: "{{route('trip.likeunlike')}}",
                    type: "POST",
                    data: { type: type, id: postId},
                    dataType: "json",
                    success: function(resp, status){
                        if(resp.status ='yes'){
                            $('#trip_like_count_' + postId).html('<a href="#usersWhoLike" data-toggle="modal" class="trip_like_button" id="' + postId + '" data-id="'+postId+'" data-type="trip"><b>' + resp.count + '</b> Likes</a>');
                        }
                    },
                    error: function(){}
                });
                
            }else if(type == 'report'){
               
                $.ajax({
                    method: "POST",
                    url: "{{ route('report.like') }}",
                    data: {post_id: postId}
                })
                .done(function (res) {
                    var resp = JSON.parse(res);
                    $('#report_like_count_' + postId).html('<a href="#usersWhoLike" data-toggle="modal" class="report_like_button" id="' + postId + '" data-id="'+postId+'" data-type="report"><b>' + resp.count + '</b> Likes</a>');
                });
            }else if(type == 'review'){
                value_vote = $(this).data('value');
                $.ajax({
                    method: "POST",
                    url: "{{route('place.revirews_updownvote')}}",
                    data: {review_id: postId, vote_type:value_vote}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if(result.status ='yes')
                            $('#review_like_count_' + postId).html('<a  data-toggle="modal" class="trip_like_button" id="' + postId + '" data-id="'+postId+'" data-value="" data-type="review"><b>' + result.count_upvotes + '</b> Was this helpful?</a>');
                    });
                e.preventDefault();
            }else{
                $.ajax({
                    method: "POST",
                    url: "{{ route('post.likeunlike') }}",
                    data: {post_id: postId}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (res.status == 'yes') {


                        } else if (res == 'no') {

                        }
                        $('#post_like_count_' + postId).html('<a href="#usersWhoLike" data-toggle="modal" class="trip_like_button" id="' + postId + '" data-id="'+postId+'" data-type="post"><b>' + result.count + '</b> Likes</a>');
                    });
                e.preventDefault();
            }
        });
        
    $('#shareablePost').on('show.bs.modal',function (event){
        $('#shared-post-wrap-content').empty();
        id = event.relatedTarget.dataset.id;
        type = event.relatedTarget.dataset.type;
        $('#share-post-id').val(id); 
        $('#share-post-type').val(type); 
        $.ajax({
            method: "GET",
            url: '{{  url("get-shareable-post")}}',
            data: {id: id,'type':type}
        })
        .done(function (res) {
            if(res !=0)
                $('#shared-post-wrap-content').html(res);
        });
    });
    $('.share_post_btn').on('click',function(){
        $.ajax({
            method: "POST",
            url: '{{url("share-post")}}',
            data: {id: $('#share-post-id').val(),'type':$('#share-post-type').val(),'text':$('#message_popup_text_for_share').val()}
        })
        .done(function (res) {  
            if(res !=0){
                var result = JSON.parse(res);
                console.log('res.status', result.status)
                if(result.status == 0){
                    toastr.success(result.messages);
                }else {
                    toastr.success('Post has been shared');
                }

                $('#shareablePost').modal('hide');
                $('#share-post-id').val('');
                $('#share-post-type').val('');
                $('#message_popup_text_for_share').val('');
            }
        });
    });
    // copy post link  
    $(document).on( 'click', '.copy-newsfeed-link', function(){
        var copy_link = $(this).attr('data-link');
        var _temp = $("<input>");
        $("body").append(_temp);
        _temp.val(copy_link).select();
         document.execCommand("copy");
         _temp.remove()

        toastr.success('The link copied!');
    });
    // $(document).on('click', '.drop-meta-item', function() {
    //     var placeId = $(this).data('placeid');
    //     var userId = $(this).data('userid');
    //     var title = $(this).data('title');
    //     var meta = $(this).data('meta');
    //     var country_id = $(this).data('country_id');
    //     var city_id = $(this).data('city_id');
    //     var href = '#';
    //     var idData = '';
    //     var $appendTaggedDestination = '';
    //     if (placeId) { //
    //         $appendTaggedDestination = '<li class="tag">' + title + '<input type="hidden" name="taggedPlaces[]" value="' + placeId + '"><i class="trav-close-icon delete"></i></li>';
    //         href = '/place/' + placeId;
    //         idData = 'data-place_id="' + placeId + '"';
    //     }

    //     if (userId) {
    //         href = '/profile/' + userId;
    //         idData = 'data-user_id="' + userId + '"';
    //     }
    //     if (country_id) {
    //         $appendTaggedDestination = '<li class="tag">' + title + ' <input type="hidden" name="taggedCountries[]" value="' + country_id + '"><i class="trav-close-icon delete"></i></li>';

    //         href = '/country/' + country_id;
    //         idData = 'data-place_id="' + country_id + '"';
    //     }

    //     if (city_id) {
    //         $appendTaggedDestination = '<li class="tag">' + title + ' <input type="hidden" name="taggedCities[]" value="' + city_id + '"><i class="trav-close-icon delete"></i></li>';
    //         href = '/city/' + city_id;
    //         idData = 'data-place_id="' + city_id + '"';
    //     }

    //     var contents_temp = $('#add_post_text').text();
    //     if ($('#add_post_text').length > 0 && userId) {
    //         contents_temp = contents_temp.replace('@' + meta, '<a href="' + href + '" ' + idData + ' class="summernote-selected-items post-text-tag-' + userId + '" data-toggle="popover" data-html="true" data-original-title="" title="">' + title + '</a>&nbsp;');
    //     } else {
    //         contents_temp = contents_temp.replace('@' + meta, '<a href="' + href + '" ' + idData + ' class="summernote-selected-items">' + title + '</a>&nbsp;');
    //     }
    //     $('#add_post_text').text(contents_temp);
    //     $('#add_post_text').placeCursorAtEnd();
    //     $('.post-tags-list').removeClass('d-none');
    //     $('body').find('.tagged-destinations-container').append($appendTaggedDestination);
    //     $('.textcomplete-wrapper').addClass('d-none');
    //     $('.textcomplete-wrapper').html('');

    // });
    // var buildTaggedItems = function(data, tag) {
    //             var appendHTML = '<div class="dropdown-menu text-complate-block"><div class="comment-search-box">' +
    //                 '<ul class="nav nav-tabs search-tabs" role="tablist">' +
    //                 '<li class="nav-item">' +
    //                 '<a class="nav-link active" href="#places" role="tab" data-toggle="tab">Places <span>' + data.places_count + '</span></a>' +
    //                 '</li>' +
    //                 '<li class="nav-item">' +
    //                 '<a class="nav-link" href="#peoples" role="tab" data-toggle="tab">People <span>' + data.users_count + '</span></a>' +
    //                 '</li>' +
    //                 '<li class="nav-item">' +
    //                 '<a class="nav-link" href="#cities_tub" role="tab" data-toggle="tab">Cities <span>' + data.cities_count + '</span></a>' +
    //                 '</li>' +
    //                 '<li class="nav-item">' +
    //                 '<a class="nav-link" href="#countries_tub" role="tab" data-toggle="tab">Countries <span>' + data.countries_count + '</span></a>' +
    //                 '</li>' +
    //                 '</ul>';

    //             appendHTML += '<div class="tab-content">' +
    //                 '<div role="tabpanel" class="tab-pane fade in" id="peoples"  aria-expanded="true">';
    //             if (data.users && data.users.length > 0) {
    //                 data.users.forEach(function(people) {
    //                     appendHTML += `<div class="drop-row drop-items drop-meta-item" data-userid="` + people.id + `" data-title="` + people.name + `" data-meta="` + tag + `">
    //                                                             <div class="img-wrap rounded search-ppl-img">
    //                                                                 <img src="` + people.image + `" >
    //                                                             </div>
    //                                                             <div class="drop-content search-people-content"><p class="search-ppl-name"><span class="item-name">` + markTagMatch(people.name, tag) + `</span></p></div></div>`;
    //                 })
    //             }
    //             appendHTML += '</div>';
    //             appendHTML += '<div role="tabpanel" class="tab-pane fade in" id="cities_tub"  aria-expanded="true">';
    //             if (data.cities && data.cities.length > 0) {
    //                 data.cities.forEach(function(city) {
    //                     appendHTML += `<div class="drop-row drop-items drop-meta-item" data-city_id="` + city.id + `" data-title="` + city.title + `" data-meta="` + tag + `">
    //                                                             <div class="img-wrap rounded search-ppl-img">
    //                                                                 <img src="` + city.image + `">
    //                                                             </div>
    //                                                             <div class="drop-content search-place-content"><p class="search-place-name"><span class="item-name">` + markTagMatch(city.title, tag) + `</span></p></div></div>`;
    //                 })
    //             }
    //             appendHTML += '</div>';
    //             appendHTML += '<div role="tabpanel" class="tab-pane fade in" id="countries_tub"  aria-expanded="true">';
    //             if (data.countries && data.countries.length > 0) {
    //                 data.countries.forEach(function(country) {
    //                     appendHTML += `<div class="drop-row drop-items drop-meta-item" data-country_id="` + country.id + `" data-title="` + country.title + `" data-meta="` + tag + `">
    //                                                             <div class="img-wrap rounded search-ppl-img">
    //                                                                 <img src="` + country.image + `">
    //                                                             </div>
    //                                                             <div class="drop-content search-place-content"><p class="search-place-name"><span class="item-name">` + markTagMatch(country.title, tag) + `</span></p></div></div>`;
    //                 })
    //             }
    //             appendHTML += '</div>' +
    //                 '<div role="tabpanel" class="tab-pane fade in active show" id="places">';
    //             if (data.places && data.places.length > 0) {

    //                 data.places.forEach(function(place) {
    //                     appendHTML += `<div class="drop-row drop-items drop-meta-item" data-placeid="` + place.id + `" data-title="` + place.title + `" data-meta="` + tag + `">
    //                                                             <div class="img-wrap rounded search-ppl-img">
    //                                                                 <img src="` + place.image + `">
    //                                                             </div>
    //                                                             <div class="drop-content search-place-content"><p class="search-place-name"><span class="item-name">` + markTagMatch(place.title, tag) + `</span></p><p class="search-place-desc"><span>` + place.address + `</span></p></div></div>`;
    //                 })
    //             }
    //             appendHTML += '</div>' +
    //                 '</div>';

    //             appendHTML += '</div></div>';

    //             return appendHTML;
    //         }
    //         var markTagMatch = function(text, term) {
    //             var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
    //             var output = text.replace(regEx, "<span class='tag-rendered'>$1</span>");
    //             return output;
    //         }
    // //for following of place
function newsfeed_place_following(type, id, obj)
{
    var followurl = "{{  route('place.follow', ':user_id')}}";
    var unfollowurl = "{{  route('place.unfollow', ':user_id')}}";
    if(type == "follow")
    {
        url = followurl.replace(':user_id', id);
    }
    else if(type == "unfollow")
    {
        url = unfollowurl.replace(':user_id', id);
    }
    $.ajax({
        method: "POST",
        url: url,
        data: {name: "test"}
    })
    .done(function (res) {
        if (res.success == true) {
            $(obj).parent().find("b").html(res.count);
            if(type == "follow")
            {
                $(obj).replaceWith('<button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_place_following(\'unfollow\','+ id +', this)">Unfollow</button>');
            }
            else if(type == "unfollow")
            {
                $(obj).replaceWith('<button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following(\'follow\','+ id +', this)">@lang('home.follow')</button>');
            }
        } else if (res.success == false) {

        }
    });
}
        
</script>
