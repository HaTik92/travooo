<div class="picture-cards">
    <div class="picture-cards__inner">
        <div class="picture-cards__items">
            <div class="picture-cards__item">
                <div class="picture-card">
                    <span class="picture-card__header">
                        <h2 class="picture-card__title">Trending Media</h2>
                        <span class="picture-card__number">{{formatPrefix(count($place->getMedias))}}</span>
                    </span>
                    <img class="picture-card__img " onclick="$('html, body').animate({scrollTop: $('.page-content__row').offset().top}, 1000);" data-type="media" style="width:200px;height:134px;cursor:pointer;" data-src="@if(isset($place->getMedias[0]) && is_object($place->getMedias[0]) && isset($place->getMedias[0]->url)) https://s3.amazonaws.com/travooo-images2/th1100/{{@$place->getMedias[0]->url}} @else {{asset('assets2/image/placeholders/pattern.png')}} @endif" alt="" role="presentation"/>
                    <!--<span class="picture-card__icon js-show-modal" href="#modal-media">-->
                    <span class="picture-card__icon @if(count($place->getMedias) > 0) js-show-modal" href="#modal-medias" @else " @endif style="cursor:pointer" onclick="javascript:$('#modal-medias').find('.trip__cards').show();">
                        <svg class="icon icon--multiple-items"><use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use>
                        </svg>
                    </span>
                </div>
            </div>
            <?php
            
            
                if(count($trip_plans_collection) > 0) {
                    $f_plan = $trip_plans_collection->first();
                } 
            ?>
            <div class="picture-cards__item">
                <div class="picture-card">
                    <span class="picture-card__header">
                        <h2 class="picture-card__title">Trip Plans</h2>
                        <span class="picture-card__number">{{formatPrefix(@count($trip_plans_collection))}}</span>
                    </span>
                    @if(@count($trip_plans_collection) > 0)
                    <img class="picture-card__img " data-type="tripplan" href="#modal-trips" style="cursor:pointer;" data-src="{{get_plan_map(@$f_plan, 185, 115, @$f_plan->active_version)}}" alt="" role="presentation" onclick="$('html, body').animate({scrollTop: $('.page-content__row').offset().top}, 2000);" />
                    <span class="picture-card__icon js-show-modal" href="#modal-trips" style="cursor:pointer" onclick="javascript:$('#modal-trips').find('.trip__cards').show();"><svg class="icon icon--multiple-items"><use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use></svg>
                    </span>
                    @else
                    @lang('place.no_plans')
                    @endif
                </div>
            </div>
            <div class="picture-cards__item js-show-modal" href="#ExpertsModal" data-id="1">
                <div class="picture-card">
                    <span class="picture-card__header" style="margin-bottom:0"><h2 class="picture-card__title">Experts</h2>
                        <span class="picture-card__number">{{formatPrefix(count($all_expert))}}</span>
                    </span>
                    <div class="picture-cards__item__mates">
                    @foreach($all_expert AS $tm)
                    @if($loop->index == 6) @break @endif
                        <div style="width:33%;height:58px;object-fit:cover;overflow:hidden;cursor:grab;background-image:url('{{check_profile_picture(@$tm->user->profile_picture)}}');background-position:center center;background-size: 87px 87px;border:1px solid white;float:left;" alt="" role="presentation"></div>
                    
                    @endforeach
                    </div>
                </div>
            </div>
            <div class="picture-cards__item event">
                <div class="picture-card">
                    <span class="picture-card__header"><h2 class="picture-card__title">Nearby Events</h2>
                    <span class="picture-card__number"></span>
                                        </span>
            
            <?php
            $rand = rand(1, 10);
            $category = array('community', 'concerts', 'conferences', 'expos', 'festivals', 'performing-arts', 'politics', 'school-holidays', 'sports');
            ?>
            <img class="picture-card__img "  data-type="events" onclick="$('html, body').animate({scrollTop: $('.page-content__row').offset().top}, 1000);" href="#modal-events" data-src="{{asset('assets2/image/events/'.$category[$rand%9].'/'.$rand.'.jpg')}}" style="cursor:pointer;width:200px;height:135px;" role="presentation"/>
            <span class="picture-card__icon " href="#modal-events" style="cursor:pointer"><svg class="icon icon--multiple-items"><use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use></svg>
            </span>
            
            <span class="picture-card__icon" style="cursor:pointer"><svg class="icon icon--multiple-items"><use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use></svg>
            </span>
        </div>
    </div>
    <div class="picture-cards__item">
        <div class="picture-card ">
            <span class="picture-card__header">
                <h2 class="picture-card__title">Reports</h2>
                <span class="picture-card__number">@if(count($reportsinfo)) {{formatPrefix(count($reportsinfo))}} @else {{formatPrefix(count($reports))}} @endif</span>
            </span>
            @if(isset($reportsinfo) AND count($reportsinfo)>0)
            @foreach($reportsinfo as $report)
            @if(isset($report->report->cover[0]) && is_object($report->report->cover[0]) && $report->report->cover[0]->val!='')
            <img class="picture-card__img " data-type="reports" onclick="$('html, body').animate({scrollTop: $('.page-content__row').offset().top}, 1000);"  data-id="0" data-src="{{@$report->report->cover[0]->val}}" alt="" style="cursor:pointer;width:200px;height:135px;" role="presentation" />
            @break
            @endif
            @endforeach
            @elseif(isset($reports) AND count($reports)>0)
            @foreach($reports as $report)
            @if(isset($report->cover[0]) && is_object($report->cover[0]) && $report->cover[0]->val!='')
            <img class="picture-card__img " data-type="reports" onclick="$('html, body').animate({scrollTop: $('.page-content__row').offset().top}, 1000);" data-id="0" data-src="{{@$report->cover[0]->val}}" alt="" style="cursor:pointer;width:200px;height:135px;" role="presentation" />
            @break
            @endif
            @endforeach
            @endif
            <span class="picture-card__icon" style="cursor:pointer"><svg class="icon icon--multiple-items"><use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use></svg>
            </span>
        </div>
    </div>
</div>