<style>
  .modal__close {
    position: fixed !important;
  }

</style>

<!-- EXPERTS modal -->
<div class="about-modal custom-modal" id="ExpertsModal">
    <div class="about-modal__inner"><button class="modal__close" type="button"><svg class="icon icon--close">
            <use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use>
            </svg></button>
        <div class="about-modal__links">
            <a class="about-modal__link about-modal__link--active" id="tab-1">Top Experts {{count($experts_top)}}</a>
            <a class="about-modal__link" id="tab-2">Local Experts {{count($experts_local)}}</a>
            <a class="about-modal__link" id="tab-3">My Friends {{count($experts_friends)}}</a>
            <a class="about-modal__link" id="tab-4">Live Check-ins {{count($live_checkins)}}</a>
        </div>
        <div class="about-modal__content" style="padding: 0!important;">

            <div class="about-modal__main">
                <div id="box-1" style='display:none;padding-bottom: 10px;'>
                    <div class="about-modal__item">
                        <div class="about-modal__questions" style='max-height: 500px;overflow: auto;margin-bottom: 0px !important;'>

                            @forelse($experts_top AS $pce)
                            @if(is_object($pce->user))
                            <div class="question" style="padding:10px;">
                                <img class="question__avatar" data-src="{{check_profile_picture($pce->user->profile_picture)}}" alt="{{$pce->user->name}}" title="{{$pce->user->name}}" style="width:46px;height:46px;"/>
                                <div class="question__content" style='width:50%'>
                                    <div class="question__title" ><a target="_blank" href="{{url('profile/'.@$pce->user->id)}}" style="text-decoration: none;color:#1a1a1a">{{$pce->user->name}}</a> @if(is_friend($pce->user->id))<span style='background-color: #e6e6e6;padding: 2px 5px;font-size: 13px;font-weight: 100;-moz-border-radius: 3px;border-radius: 4px;'>FRIEND</span>@endif</div>
                                    <div class="question__total" style="color:darkgrey;font-size:13px;">{{diffForHumans($pce->created_at)}}</div>
                                </div>
                                <div style='width:100%'>
                                    <div id="followContainer" style='float: right;padding-left: 10px;'><a class="btn btn--main button_unfollow" href="#" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Follow</a></div>&nbsp;
                                    @if (Auth::check() && Auth::user()->id != $pce->user->id)
                                    <div id="messageContainer" style='float: right;'><a class="btn btn--main"  data-name="{{$pce->user->name}}" data-toggle="modal" href="#add_message_popup" data-user_id="{{$pce->user->id}}" data-profile_picture="{{check_profile_picture($pce->user->profile_picture)}}" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Message</a></div>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @empty
                            <div class="question" style='padding:10px;'>
                            No Users found
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div id="box-2" style='display:none;padding-bottom: 10px;'>
                    <div class="about-modal__item">
                        <div class="about-modal__questions" style='max-height: 500px;overflow: auto;margin-bottom: 0px !important;'>

                            @forelse($experts_local AS $pce)
                            @if(is_object($pce->user))
                            <div class="question" style="padding:10px;">
                                <img class="question__avatar" data-src="{{check_profile_picture($pce->user->profile_picture)}}" alt="{{$pce->user->name}}" title="{{$pce->user->name}}" style="width:46px;height:46px;"/>
                                <div class="question__content" style='width:50%'>
                                    <div class="question__title"><a target="_blank" href="{{url('profile/'.@$pce->user->id)}}" style="text-decoration: none;color:#1a1a1a">{{$pce->user->name}}</a></div>
                                    <div class="question__total" style="color:darkgrey;font-size:13px;">{{diffForHumans($pce->created_at)}}</div>
                                </div>
                                <div style='width:100%'>
                                    <div id="followContainer" style='float: right;padding-left: 10px;'><a class="btn btn--main button_unfollow" href="#" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Follow</a></div>&nbsp;
                                    @if (Auth::check() && Auth::user()->id != $pce->user->id)
                                    <div id="messageContainer" style='float: right;'><a class="btn btn--main"  data-name="{{$pce->user->name}}" data-toggle="modal" href="#add_message_popup" data-user_id="{{$pce->user->id}}" data-profile_picture="{{check_profile_picture($pce->user->profile_picture)}}" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Message</a></div>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @empty
                            <div class="question" style='padding:10px;'>
                            No Users found
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div id="box-3" style='display:none;padding-bottom: 10px;'>
                    <div class="about-modal__item">
                        <div class="about-modal__questions" style='max-height: 500px;overflow: auto;margin-bottom: 0px !important;'>

                            @forelse($experts_friends AS $pce)
                            @if(is_object($pce->user))
                            <div class="question" style="padding:10px;">
                                <img class="question__avatar" data-src="{{check_profile_picture($pce->user->profile_picture)}}" alt="{{$pce->user->name}}" title="{{$pce->user->name}}" style="width:46px;height:46px;"/>
                                <div class="question__content" style='width:50%'>
                                    <div class="question__title"><a target="_blank" href="{{url('profile/'.@$pce->user->id)}}" style="text-decoration: none;color:#1a1a1a">{{$pce->user->name}}</a></div>
                                    <div class="question__total" style="color:darkgrey;font-size:13px;">{{diffForHumans($pce->created_at)}}</div>
                                </div>
                                <div style='width:100%'>
                                    <div id="followContainer" style='float: right;padding-left: 10px;'><a class="btn btn--main button_unfollow" href="#" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Follow</a></div>&nbsp;
                                    @if (Auth::check() && Auth::user()->id != $pce->user->id)
                                    <div id="messageContainer" style='float: right;'><a class="btn btn--main" data-name="{{$pce->user->name}}" data-toggle="modal" href="#add_message_popup" data-user_id="{{$pce->user->id}}" data-profile_picture="{{check_profile_picture($pce->user->profile_picture)}}" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Message</a></div>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @empty
                            <div class="question" style='padding:10px;'>
                            No Users found
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div id="box-4" style='display:none;padding-bottom: 10px;'>
                    <div class="about-modal__item">
                        <div class="about-modal__questions" style='max-height: 500px;overflow: auto;margin-bottom: 0px !important;'>

                            @forelse($live_checkins AS $key=>$pci)
                            @if(is_object($pci->user))
                            <div class="question" style="padding:10px;">
                                <img class="question__avatar" data-src="{{check_profile_picture($pci->user->profile_picture)}}" alt="{{$pci->user->name}}" title="{{$pci->user->name}}" style="width:46px;height:46px;"/>
                                <div class="question__content" style='width:50%'>
                                    <div class="question__title"><a target="_blank" href="{{url('profile/'.@$pci->user->id)}}" style="text-decoration: none;color:#1a1a1a">{{$pci->user->name}}</a></div>
                                    <div class="question__total" style="color:darkgrey;font-size:13px;">{{diffForHumans($pci->created_at)}}</div>
                                </div>
                                <div style='width:100%'>
                                    <div id="followContainer" style='float: right;padding-left: 10px;'><a class="btn btn--main button_unfollow" href="#" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Follow</a></div>&nbsp;
                                    @if (Auth::check() && Auth::user()->id != $pci->user->id)
                                    <div id="messageContainer" style='float: right;'><a class="btn btn--main" data-name="{{$pci->user->name}}" data-toggle="modal" href="#add_message_popup" data-user_id="{{$pci->user->id}}" data-profile_picture="{{check_profile_picture($pci->user->profile_picture)}}" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Message</a></div>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @empty
                            <div class="question" style='padding:10px;'>
                            No Users found
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- ABOUT modal -->
<div class="about-modal custom-modal" id="modal-about">
    <div class="about-modal__inner"><button class="modal__close" type="button"><svg class="icon icon--close">
            <use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use>
            </svg></button>
        <div class="about-modal__links">
            <a class="about-modal__link about-modal__link--active" id="tab-about">About</a>
            <a class="about-modal__link" id="tab-ratings">Ratings</a>
            {{-- <a class="about-modal__link" id="tab-photos">Photos</a> --}}
            <a class="about-modal__link" id="tab-qa">Q&A</a>
            <a class="about-modal__link" id="tab-people">People</a>
            <a class="about-modal__link" id="tab-today">Today</a>

        </div>
        <div class="about-modal__content" style="padding-top: 0!important;">

            <div class="about-modal__main">
                <div id="box-about">
                    <div class="map map--about">
                        <div class="map__inner" style="background-image: url(https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{$place->lat}},{{$place->lng}}&markers=color:red%7Clabel:C%7C{{$place->lat}},{{$place->lng}}&zoom=14&size=595x360&key={{env('GOOGLE_MAPS_KEY')}})">
                            <div class="map__info">
                                <div class="map__info-item"><svg class="icon icon--user">
                                    <use xlink:href="{{asset('assets3/img/sprite.svg#user')}}"></use>
                                    </svg>
                                    <div class="map__info-wrap">
                                        <div class="map__info-label">#{{$place->id}}</div>
                                        <div class="map__info-value">Popularity</div>
                                    </div>
                                </div>

                            </div>
                            <div class="map__flag"><svg class="icon icon--usa">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#usa')}}"></use>
                                </svg></div>
                        </div>
                    </div>
                    @if(@$place->trans[0]->address!='')
                    <div class="about-modal__meta">
                        <span class="about-modal__meta-label">
                            <svg class="icon icon--location"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use></svg>
                            Address:
                        </span>
                        <span class="about-modal__meta-value">{{@$place->trans[0]->address}}</span>
                    </div>
                    @endif
                    <div class="about-modal__meta">
                        <div class="about-modal__meta-row">
                            @if(@$place->trans[0]->working_days!='')
                            <div class="about-modal__meta-item"><svg class="icon icon--time-2">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#time-2')}}"></use>
                                </svg>
                                <div class="about-modal__meta-content">
                                    <div class="about-modal__meta-label">Working times</div>
                                    <div class="about-modal__meta-value">
                                        {!!@$place->trans[0]->working_days!!}
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(@$place->trans[0]->description!='')
                            <div class="about-modal__meta-item"><svg class="icon icon--chain">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#chain')}}"></use>
                                </svg>
                                <div class="about-modal__meta-content">
                                    @if(session('place_country') == 'place')
                                    <div class="about-modal__meta-label">Website</div>
                                    <a class="about-modal__meta-value" href="{{@$place->trans[0]->description}}" target="_blank">{{@$place->trans[0]->description}}</a>
                                    @elseif(session('place_country') == 'country')
                                    <div class="about-modal__meta-label">Description</div>
                                    <p><?php echo @$place->trans[0]->description; ?></p>
                                    @elseif(session('place_country') == 'city')
                                    <div class="about-modal__meta-label">Description</div>
                                    <p><?php echo @$place->trans[0]->description; ?></p>
                                    @endif
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div id="box-ratings" style="display:none;">
                    <div class="about-modal__meta about-modal__meta--rating" style='padding-top:15px;padding-bottom:15px;'>
                        <div class="about-modal__meta-item">
                            <div class="about-modal__meta-content">
                                <div class="about-modal__meta-label">User Rating</div>
                                <div class="rating">
                                    <div class="star-rating" style="margin-right: 0px;">
                                        <span style="width: {{round($reviews_avg, 1) * 20}}%"></span>

                                    </div>
                                    <div class="rating__total"><strong>{{round($reviews_avg, 1)}}</strong> from <a href="#">{{@count($reviews_places)}} Reviews</a></div>
                                </div>
                            </div>
                            <a class="btn js-show-modal" href="#modal-review">Write a Review</a>
                        </div>
                    </div>
                    <div class="comments">
                        <div class="comments__items sortBody">
                            @foreach($reviews AS $review)
    
                            <div class="comment" sortval="{{ceil(@$review->score + $review->updownvotes()->where('vote_type',1)->count() - $review->updownvotes()->where('vote_type',2)->count())}}" data-id="{{ $review->id }}">
                                <img class="comment__avatar {{@$review->google_profile_photo_url ? '' : 'avatar_wrap'}}" data-src="{{@$review->google_profile_photo_url ? $review->google_profile_photo_url : check_profile_picture(@$review->author->profile_picture)}}" alt="" role="presentation" />
                                <div class="comment__content" style="overflow: hidden;word-break: break-word; width: 87%;">
                                    <div class="comment__header"><a class="comment__username" href="{{@url_with_locale('profile/'.@$review->author->id)}}">{{@$review->google_author_name ? @$review->google_author_name : @$review->author->name}}</a>
                                        <div class="comment__user-id"></div>
                                    </div>
                                    <div class="comment__text">{{$review->text}}</div>
                                    <div class="comment__footer">
                                        <div class="rating"><svg class="icon icon--star @if($review->score>=1) icon--star-active @endif">
                                            <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>
                                            </svg><svg class="icon icon--star @if($review->score>=2) icon--star-active @endif">
                                            <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>
                                            </svg><svg class="icon icon--star @if($review->score>=3) icon--star-active @endif">
                                            <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>
                                            </svg><svg class="icon icon--star @if($review->score>=4) icon--star-active @endif">
                                            <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>
                                            </svg><svg class="icon icon--star @if($review->score>=5) icon--star-active @endif">
                                            <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>
                                            </svg>
                                            <div class="rating__value"><strong>{{$review->score}}</strong> / 5</div>
                                        </div>
                                        <div class="comment__posted">{{$review->google_time ? diffForHumans(date("Y-m-d", $review->google_time)) : diffForHumans($review->created_at)}}</div>
                                    </div>
                                    <div class="mt-2 updownvote" id="rating_updownvote">
                                        <a href="#" class="upvote-link review-vote up disabled" id="{{$review->id}}">
                                            <span class="arrow-icon-wrap"><i class="trav-angle-up"></i></span>
                                        </a>
                                        <span class="upvote-{{$review->id}}"><b>{{count($review->updownvotes()->where('vote_type', 1)->get())}}</b> Upvotes</span>
                                        &nbsp;&nbsp;
                                        <a href="#" class="upvote-link review-vote down disabled" id="{{$review->id}}">
                                            <span class="arrow-icon-wrap"><i class="trav-angle-down"></i></span>
                                        </a>
                                        <span class="downvote-{{$review->id}}"><b>{{count($review->updownvotes()->where('vote_type', 2)->get())}}</b> Downvotes</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div id="box-photos" style="display:none;padding-top:15px;">
                    <div class="about-modal__item">
                        <div class="about-modal__item-header">
                            <div class="about-modal__item-title">Photos of {{@$place->trans[0]->title}} <span class="about-modal__item-total">{{count($place->getMedias)}}</span></div><a href="#" class="about_more_photo" data-page="0">More photos</a>
                        </div>
                        <div class="gallery media-gallery">
                          <div class="gallery__col"></div>
                          <div class="gallery__col"></div>
                          <div class="gallery__col"></div>
                        </div>
                    </div>
                </div>
                <div id="box-qa" style="display:none;padding-top:15px;">
                    <div class="about-modal__item" style="padding-bottom: 10px">
                        <div class="about-modal__item-header" style="margin-bottom: 15px;">
                            <div class="about-modal__item-title">Q&A <span class="about-modal__item-total">{{count($place_discussions)}}</span></div>
                            <div class="user-list"></div>
                            @if(count($place_discussions)>0)
                            <div class="btn">
                                <a href="{{url('discussions?do=place&id='.$place->id)}}">See all {{count($place_discussions)}} Q&A</a></div>
                            @endif
                        </div>
                        <div class="about-modal__questions sortBody" style="display: contents;">
                            @forelse($place_discussions AS $dis)
                            <div class="question" sortval="{{ceil(count($dis->replies) + count($dis->likes) + count($dis->shares))}}" data-id="{{ $dis->id }}">
                                <img class="question__avatar" data-src="{{check_profile_picture($dis->author->profile_picture)}}" alt="#" title="" />
                                <div class="question__content" style="overflow: hidden;word-break: break-word;">
                                    <div class="question__title"><a href="{{url_with_locale('profile/'.$dis->author->id)}}">{{$dis->author->name}}</a> Asked: {{$dis->question}}<span class="question__posted">{{diffForHumans($dis->created_at)}}</span></div>
                                    <div class="question__total"><strong>{{count($dis->replies)}} Answers</strong>
                                        @if(count($dis->replies))
                                        from
                                        @if(count($dis->replies)>3)
                                        @for($i=0;$i<3;$i++)
                                        <a href="{{url_with_locale('profile/'.$dis->replies[$i]->author->id)}}">{{$dis->replies[$i]->author->name}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                        @endfor
                                        and more...
                                        @else
                                        @foreach($dis->replies AS $reply)
                                        <a href="{{url_with_locale('profile/'.$reply->author->id)}}">{{$reply->author->name}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                        @endforeach
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @empty
                            <div class="question">
                            No Q&A related to {{@$place->trans[0]->title}}
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div id="box-people" style="display:none;padding-top:15px;">
                    <div class="about-modal__item">
                        <div class="about-modal__item-header">
                            <div class="about-modal__item-title">People following this place <span class="about-modal__item-total">{{count($place->followers)}}</span></div>
                        </div>
                        <?php
                            $followers = [];
                            if(Auth::check()){
                                $following = \App\Models\UsersFollowers\UsersFollowers::where('followers_id', Auth::user()->id)->get();
                                foreach($following as $f):
                                    $followers[] = $f->users_id;
                                endforeach;
                            }
                        ?>
                        <div class="about-modal__follow-card follow-user-list">
{{--                            @forelse($place->followers AS $fol)--}}
{{--                            <div class="follow-card" data-id="{{$fol->user->id}}">--}}
{{--                                <img class="follow-card__avatar" src="{{check_profile_picture($fol->user->profile_picture)}}" alt="#" title="" />--}}
{{--                                <div class="follow-card__name">{{$fol->user->name}}</div>--}}
{{--                                <div class="follow-card__info">{{$fol->user->display_name}}</div>--}}

{{--                                @if(in_array($fol->user->id, $followers))--}}
{{--                                <div class="btn btn--sm btn-light-grey followbtn4user">UnFollow</div>--}}
{{--                                @elseif($fol->user->id == Auth::user()->id)--}}
{{--                                <div class="btn btn--sm btn-light-grey">Follow</div>--}}
{{--                                @else--}}
{{--                                <div class="btn btn--sm btn-light-primary followbtn4user">Follow</div>--}}
{{--                                @endif--}}

{{--                                <div class="follow-card__followers">{{count($fol->user->get_followers)}} Followers</div>--}}
{{--                            </div>--}}
{{--                            @empty--}}
{{--                            No users are following this place.--}}
{{--                            @endforelse--}}

                        </div>
                    </div>
                </div>
                <div id="box-today" style="display:none;padding-top:15px;">
                    <div class="about-modal__item">
                        <div class="about-modal__item-header" style="margin-bottom: 10px;">
                            <div class="about-modal__item-title">Happening Today in "{{@$place->trans[0]->title}}"</div>
                        </div>
                        <div class="map today-map">
                            <div class="map__inner" id="today_map"></div>
                            <div class="map__users-wrap">
                                <div class="map__users" data-simplebar="data-simplebar">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<!-- REVIEW MODAL -->
<div class="custom-modal add-review" id="modal-review">
    <div class="modal__inner">
        <div class="modal__header">
            <h2 class="modal__title">Reviews for {{@$place->trans[0]->title}}</h2>
            <button class="modal__close" id="review-modal-close" type="button">
                <svg class="icon icon--close">
                  <use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use>
                </svg>
            </button>
        </div>
      <div class="add-review__main">
        <div class="add-review__header"><img class="add-review__img" data-src="@if(isset($place->getMedias[0]->url)) https://s3.amazonaws.com/travooo-images2/th1100/{{$place->getMedias[0]->url}} @else {{asset('assets2/image/placeholders/pattern.png')}} @endif" alt="" role="presentation" width="100%" style="height:294px" />
          <h2 class="add-review__title" style="padding-top: 25px;">{{@$place->trans[0]->title}}</h2>
            <div class="add-review__desc">
                @if(session('place_country') == 'place')
                    {{do_placetype($place->place_type)}} in {{@$place->city->trans[0]->title}}
                @endif
            </div>
            <img class="add-review__rating" data-src="img/rating.svg" alt="" role="presentation" />
        </div>
          @if(Auth::check())
        <form class="add-review__form" action="/place/save-review" method="post" id="writeReviewForm" autocomplete="off">
            {{ csrf_field() }}
            <input type="hidden" name="place_id" value="{{ $place->id }}">
              <div class="add-review__form-header"><img class="add-review__avatar" data-src="{{ check_profile_picture($me->profile_picture) }}" alt="" role="presentation" style="object-fit: cover; object-position: center;" />
                  <textarea class="add-review__input" id="AddReview" name="text" placeholder="Write a review..." type="text"></textarea>
              </div>
              <div class="add-review__form-footer">
                  <!-- Rating -->

                  <div class="rating-wrap">

                      <div class="rating-star-click">
                          <input type="range" min="0" max="5" step="0.1" value="" readonly>

                          <div class="rating-stars">
                              <div class="placehold">
                                  <input type="radio" name="score" id="5-stars" value="5"><label for="5-stars"></label>
                                  <input type="radio" name="score" id="4-stars" value="4"><label for="4-stars"></label>
                                  <input type="radio" name="score" id="3-stars" value="3"><label for="3-stars"></label>
                                  <input type="radio" name="score" id="2-stars" value="2"><label for="2-stars"></label>
                                  <input type="radio" name="score" id="1-star" value="1" checked><label for="1-star"></label>
                              </div>

                              <div class="slider">
                                  <svg class="svg" viewBox="0 0 500 100"><rect height="100" fill="url(#rating)" x="0" y="0"/></svg>
                              </div>
                          </div>
                      </div>

                  </div>

                  <!-- Star svg -->
                  <svg width="0" height="0" viewBox="0 0 100 100">
                      <clipPath id="star">
                          <path d="M 44.675463,5.8046818 32.56075,30.368057 5.4557026,34.319708 c -4.86072673,0.705003 -6.8087272,6.697428 -3.2837723,10.1296 L 21.781811,63.558262 17.14371,90.551989 c -0.834855,4.879315 4.304156,8.534111 8.608312,6.252184 L 49.999998,84.058654 74.247975,96.804173 c 4.304156,2.263397 9.44317,-1.372869 8.608312,-6.252184 L 78.21819,63.558262 97.828066,44.449308 c 3.524964,-3.432172 1.576954,-9.424597 -3.283772,-10.1296 L 67.439248,30.368057 55.324534,5.8046818 c -2.170626,-4.378365 -8.459887,-4.434032 -10.649071,0 z"/>
                      </clipPath>

                      <symbol viewBox="0 0 100 100" id="star-base">
                          <path d="M 44.675463,5.8046818 32.56075,30.368057 5.4557026,34.319708 c -4.86072673,0.705003 -6.8087272,6.697428 -3.2837723,10.1296 L 21.781811,63.558262 17.14371,90.551989 c -0.834855,4.879315 4.304156,8.534111 8.608312,6.252184 L 49.999998,84.058654 74.247975,96.804173 c 4.304156,2.263397 9.44317,-1.372869 8.608312,-6.252184 L 78.21819,63.558262 97.828066,44.449308 c 3.524964,-3.432172 1.576954,-9.424597 -3.283772,-10.1296 L 67.439248,30.368057 55.324534,5.8046818 c -2.170626,-4.378365 -8.459887,-4.434032 -10.649071,0 z"/>
                      </symbol>

                      <pattern id="rating" patternUnits="userSpaceOnUse" width="100" height="100" viewBox="0 0 100 100">
                          <use x="0" y="0" xlink:href="#star-base" width="100" height="100"/>
                      </pattern>

                      <pattern id="empty-star" patternUnits="userSpaceOnUse" width="100" height="100">
                          <use x="0" y="0" xlink:href="#star-base" fill="#ccc" width="100" height="100"/>
                      </pattern>

                      <pattern id="full-star" patternUnits="userSpaceOnUse" width="100" height="100">
                          <use x="0" y="0" xlink:href="#star-base" fill="#00b8d4" width="100" height="100"/>
                      </pattern>
                  </svg>


                  {{--            <div class="rating"><svg class="icon icon--star icon icon--star-active">--}}
    {{--                <use xlink:href="img/sprite.svg#star"></use>--}}
    {{--              </svg><svg class="icon icon--star icon icon--star-active">--}}
    {{--                <use xlink:href="img/sprite.svg#star"></use>--}}
    {{--              </svg><svg class="icon icon--star icon icon--star-active">--}}
    {{--                <use xlink:href="img/sprite.svg#star"></use>--}}
    {{--              </svg><svg class="icon icon--star icon icon--star-active">--}}
    {{--                <use xlink:href="img/sprite.svg#star"></use>--}}
    {{--              </svg><svg class="icon icon--star">--}}
    {{--                <use xlink:href="img/sprite.svg#star"></use>--}}
    {{--              </svg></div>--}}
                <div class="add-review__buttons">
                    <button class="add-review__btn-link" onclick="javascript:document.getElementById('review-modal-close').click();" type="button">Cancel</button>
                    <button class="btn btn--sm btn--main" type="submit">Post</button>
                </div>
              </div>
        </form>
          @endif
      </div>
      <div class="add-review__footer">
        <div class="add-review__comment-btn">

          <div class="user-list">
            <div class="user-list__item">
                @foreach($reviews as $review)
                @if($loop->index == 5) @break @endif
                <div class="user-list__user"><img class="user-list__avatar" data-src="{{isset($review->google_profile_photo_url) ? @$review->google_profile_photo_url : check_profile_picture(@$review->author->profile_picture)}}" alt="" role="presentation" /></div>
                @endforeach
            </div>
          </div><span><strong>{{ @count($reviews) }}</strong> Reviews</span>
        </div>
      </div>
      <div class="comments">
        <div class="comments__header">
          <div class="comments__filter">
            <button class="comments__filter-btn" type="button" onclick="reviewsSort('Top', this)">Top</button>
            <button class="comments__filter-btn comments__filter-btn--active" type="button"  onclick="reviewsSort('New', this)">New</button>
            <button class="comments__filter-btn" type="button"  onclick="reviewsSort('Worse', this)">Worse</button>
          </div>
          <div class="comments__number"><strong>0</strong> / <span>{{ @count($place->reviews) }}</span></div>
        </div>
        <div class="comments__items sortBody">

          @foreach($reviews AS $review)
          <div class="comment review_box_div" id="{{$review->id}}" topsort="{{ 1 * $review->score + $review->updownvotes->where('vote_type',1)->count() -  $review->updownvotes->where('vote_type',2)->count()}}" newsort="{{ $review->google_time ? $review->google_time : strtotime($review->created_at) }}"><img class="comment__avatar" data-src="{{isset($review->google_profile_photo_url) ? $review->google_profile_photo_url : check_profile_picture(@$review->author->profile_picture)}}" alt="" role="presentation" />
            <div class="comment__content" style="width: calc(96% - 44px)">
              <div class="comment__header"><a class="comment__username" href="#">{{isset($review->google_author_name) ? $review->google_author_name : @$review->author->name}}</a>
                <div class="comment__user-id"></div>
              </div>
              <div class="comment__text" style="overflow-wrap: break-word;margin-top:10px">{{$review->text}}</div>
              <div class="comment__footer">
                <div class="rating"><svg class="icon icon--star @if($review->score>=1) icon--star-active @endif">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>
                  </svg><svg class="icon icon--star @if($review->score>=2) icon--star-active @endif">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>
                  </svg><svg class="icon icon--star @if($review->score>=3) icon--star-active @endif">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>
                  </svg><svg class="icon icon--star @if($review->score>=4) icon--star-active @endif">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>
                  </svg><svg class="icon icon--star @if($review->score>=5) icon--star-active @endif">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>
                  </svg>
                  <div class="rating__value"><strong>{{$review->score}}</strong> / 5</div>
                </div>
                <div class="comment__posted">{{isset($review->google_time) ? diffForHumans(date("Y-m-d", $review->google_time)) : diffForHumans($review->created_at)}}</div>
              </div>
              <div class="mt-2 updownvote" id="updownvote">
                  <a href="#" class="upvote-link review-vote up disabled" id="{{$review->id}}">
                      <span class="arrow-icon-wrap"><i class="trav-angle-up"></i></span>
                  </a>
                  <span class="upvote-{{$review->id}}"><b>{{count($review->updownvotes()->where('vote_type', 1)->get())}}</b> Upvotes</span>
                  &nbsp;&nbsp;
                  <a href="#" class="upvote-link review-vote down disabled" id="{{$review->id}}">
                      <span class="arrow-icon-wrap"><i class="trav-angle-down"></i></span>
                  </a>
                  <span class="downvote-{{$review->id}}"><b>{{count($review->updownvotes()->where('vote_type', 2)->get())}}</b> Downvotes</span>
                  @if ($review->author)
                      @if ($review->author && $review->author->id === \Auth::id())
                          <a href="#" class="review-action review-delete"  data-clicked="popup" onclick="post_delete('{{$review->id}}','Review', this, event,true)" review-id="{{$review->id}}">delete</a>
                      @else
                          <div class="dropdown pull-right">
                              <a class="bg-white text-danger" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  report
                              </a>
                              <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Review">
                                  @include('site.home.partials._info-actions', ['post'=>$review])
                              </div>
                          </div>
                      @endif
                  @endif
              </div>
            </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>
  </div>

<!-- FLY TO MODAL -->
<div class="modal bootstrap" id="FlyToModal" tabindex="-1" role="dialog" aria-labelledby="FlyToModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Please select your dates</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

          <label for="idTourDateDetails">From:</label>
          <div class="form-group">
              <div class="input-group">
                  <input type="text" name="date_from" id="date_from" class="form-control datepicker">
                  <span class="input-group-addon"><i id="calIconTourDateDetails" class="glyphicon glyphicon-th"></i></span>
              </div>
          </div>

          <label for="idTourDateDetails">To:</label>
          <div class="form-group">
              <div class="input-group">
                  <input type="text" name="date_to" id="date_to" class="form-control datepicker">
                  <span class="input-group-addon"><i id="calIconTourDateDetails" class="glyphicon glyphicon-th"></i></span>
              </div>
          </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save">Save changes</button>
      </div>
    </div>
  </div>
</div>

@if(Auth::check())
<!-- SHARE MODAL -->
<div class="custom-modal check-in-modal share-modal-helper" id="modal-share">
    <div class="modal__inner">
      <div class="share-modal-header">
        <div>
          <h4>Share on Travooo</h4>
        </div>
        <button class="share-modal__close" type="button">
          <i class="fal fa-times"></i>
        </button>
      </div>
      <div class="share-modal-action">
        <div class='user--info'>
          <img class="ava" src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
          <p>{{Auth::user()->name}}</p>
        </div>
        <div class='share-dropdown-modal'>
          <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="far fa-globe-americas"></i>
                <span>PUBLIC</span>
                <i class="fas fa-sort-down"></i>
            </button>
                <div class="dropdown-menu dropdown-toggle permissoin_show hided" aria-labelledby="dropdownMenuButton">
                    <div class="share-drop-flex">
                        <i class="fal fa-globe-asia"></i>
                        <div>
                            <a class="dropdown-item" href="#">
                                <span>Public</span>
                                <span>Anyone can see this post</span>
                            </a>
                        </div>
                    </div>
                    <div class="share-drop-flex">
                        <i class="fal fa-user-friends"></i>
                        <div>
                            <a class="dropdown-item" href="#">
                                <span>Friends Only</span>
                                <span>Only you and your friends</span>
                            </a>
                        </div>
                    </div>
                    <div class="share-drop-flex">
                        <i class="fal fa-lock"></i>
                        <div>
                            <a class="dropdown-item" href="#">
                                <span>Only Me</span>
                                <span>Only you can see this post</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <textarea name="" style="resize: none;" id="message_popup_text_for_share" cols="30" rows="2" placeholder="Write something..."></textarea>
      <div class="check-in-modal__content">
        <div class="image--wrap">
          <img class="check-in-modal__img" data-src="@if(isset($place->getMedias[0]->url)) https://s3.amazonaws.com/travooo-images2/th1100/{{$place->getMedias[0]->url}} @else {{asset('assets2/image/placeholders/pattern.png')}} @endif" alt="#" title="" />
        </div>
            <div class="share__city">
                <div>
                    <h4>{{$place->transsingle->title}}</h4>
                    <p>
                        @if(session('place_country') == 'place')
                        Place in <a>{{$place->city->transsingle->title}}</a></p>

                        @elseif(session('place_country') == 'country')
                        Country
                        @else
                        City in <a>{{$place->country->transsingle->title}}</a></p>
                        @endif
                </div>
                {{-- <a class='btn'>FOLLOW</a> --}}
            </div>
          <div class="share__city-info-collapse">
              {!!$place->transsingle->description!!}
          </div>
      </div>
       <div class="share-footer-modal">
            <div  class="share-footer-modal__icons">
                <a class="ico--ws"><i class="fab fa-whatsapp"></i></a>
                <a class="ico--face"><i class="fab fa-facebook-f"></i></a>
                <a class="ico--twitter"><i class="fab fa-twitter"></i></a>
                <a class="ico--pin"><i class="fab fa-pinterest-p"></i></a>
                <a class="ico--tumb"><i class="fab fa-tumblr"></i></a>
                <a class="ico--link"><i class="far fa-link"></i></a>
            </div>
          <button class="btn place_cancel_btn share-modal__close" type="button">Cancel</button>
          <button class="btn place_share_btn">Share</button> 
        </div>
    </div>
  </div>


<!-- ADD TO PLAN MODAL -->
<div class="modal bootstrap" id="AddToPlanModal" tabindex="-1" role="dialog" aria-labelledby="AddToPlanModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Please select your plan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row add-trip-block">
              <div class="col-md-6 pr-0 trip-plan-title">Add to a new Plan</div>
              <div class="col-md-3 pl-0">
                  <input type="radio" name="plans_id" id="plans_id" value="0" class="">
              </div>
          </div>
            @foreach($my_plans AS $mp)
                <div class="row add-trip-block">
                    <div class="col-md-6 pr-0 trip-plan-title">{{$mp->title}}</div>
                    <div class="col-md-3 pl-0">
                        <input type="radio" name="plans_id" id="plans_id" value="{{$mp->id}}" class="">
                    </div>
                </div>
            @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary add-plan-btn" data-place_id="{{$place->id}}" id="addTripPlan">Add to selected Plan</button>
      </div>
    </div>
  </div>
</div>
<div class="modal white-style share-post-modal" data-backdrop="false" id="shareablePost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog share-post-new modal-670" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" style="padding-bottom: 40px;" >
                <div class="share-modal-header">
                    <div>
                      <h4>Share this post</h4>
                    </div>
                    <button type="button" data-dismiss="modal" aria-label="Close">
                      <i class="fal fa-times"></i>
                    </button>
                  </div>
                <!-- <div class="shared-post-comment">
                    <div class="shared-post-comment-details">
                        <div class="shared-post-comment-author">
                        <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                            {{Auth::user()->name}}
                        </div>
                        <div class="dropdown privacy-settings">
                            <button id="add_post_permission_buttonz" class="btn btn--sm btn--outline dropdown-toggle " data-toggle="dropdown">
                                <i class="trav-globe-icon"></i> PUBLIC
                            </button>
                            <div class="dropdown-menu dropdown-menu-left permissoin_show hided">
                                <a class="dropdown-item " href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-globe-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Public</b></p>
                                        <p>Anyone can see this post</p>
                                    </div>
                                </a>
                                <a class="dropdown-item permission_drop_down"   href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-users-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Friends Only</b></p>
                                        <p>Only you and your friends can see this post</p>
                                    </div>
                                </a>
                                <a class="dropdown-item permission_drop_down" href="#">
                                    <span class="icon-wrap">
                                        <i class="trav-lock-icon"></i>
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>Only Me</b></p>
                                        <p>Only you can see this post</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="share-modal-action">
                    <div class='user--info'>
                      <img class="ava" src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                      <p>{{Auth::user()->name}}</p>
                    </div>
                    <div class='share-dropdown-modal'>
                      <div class="dropdown privacy-settings">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="far fa-globe-americas"></i>
                            <span>PUBLIC</span>
                            <i class="fas fa-sort-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-toggle permissoin_show hided" aria-labelledby="dropdownMenuButton">
                            <div class="share-drop-flex">
                                <i class="fal fa-globe-asia"></i>
                                <div>
                                    <a class="dropdown-item" href="#">
                                        <span>Public</span>
                                        <span>Anyone can see this post</span>
                                    </a>
                                </div>
                            </div>
                            <div class="share-drop-flex">
                                <i class="fal fa-user-friends"></i>
                                <div>
                                    <a class="dropdown-item" href="#">
                                        <span>Friends Only</span>
                                        <span>Only you and your friends</span>
                                    </a>
                                </div>
                            </div>
                            <div class="share-drop-flex">
                                <i class="fal fa-lock"></i>
                                <div>
                                    <a class="dropdown-item" href="#">
                                        <span>Only Me</span>
                                        <span>Only you can see this post</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <textarea name="" style="resize: none;" id="message_popup_text_for_share" cols="30" rows="2" placeholder="Write something..."></textarea>
                <div class="shared-post-wrap" id="shared-post-wrap-content">
                </div>
                <div class="share-footer-modal">
                    <div  class="share-footer-modal__icons">
                        <a class="ico--ws"><i class="fab fa-whatsapp"></i></a>
                        <a class="ico--face"><i class="fab fa-facebook-f"></i></a>
                        <a class="ico--twitter"><i class="fab fa-twitter"></i></a>
                        <a class="ico--pin"><i class="fab fa-pinterest-p"></i></a>
                        <a class="ico--tumb"><i class="fab fa-tumblr"></i></a>
                        <a class="ico--link"><i class="far fa-link"></i></a>
                    </div>
                    <button class="btn place_cancel_btn share-modal__close" type="button">Cancel</button>
                    <button class="btn place_share_btn share_post_btn">Share</button> 
                    <input type="hidden" name="share-post-id" id="share-post-id">
                    <input type="hidden" name="share-post-type" id="share-post-type">
                </div>

            </div>

        </div>
    </div>
</div>
<div class="modal white-style share-post-modal" data-backdrop="false" id="sendToFriendModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-670" role="document" >
        <div class='friend-share-modal'>
            <div class='modal-content'>
                <div class='friend-share-modal__header'>
                    <div>
                        <h2>Send to a Friend</h2>
                    </div>
                    <button class="modal--close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="shared-post-comment">
                    <div class="shared-post-comment-details">
                        <div class="shared-post-comment-author">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                            {{Auth::user()->name}}
                        </div>
                    </div>
                    <div class="shered-post-comment-text">
                        <textarea name="" id="message_popup_text" cols="30" rows="1" placeholder="Write something..."></textarea>
                    </div>
                    <div class="friend-search">
                        <i class="trav-search-icon" ></i>
                        <input type="text" placeholder="Search in your friends list...">
                    </div>
                    <div class="shared-post-wrap" id="message_popup_content">
                    
                    </div>
                </div>
                <div class="friend-search-results">
                    @php
                    $friends_users = App\Models\User\User::whereIn('id',get_friendlist())->distinct()->get();                    
                    @endphp
                    @if(count($friends_users)>0)
                        @foreach ($friends_users as $f_user)                      
                            <div class="friend-search-results-item">
                                <img class="user-img" src="{{check_profile_picture($f_user->profile_picture)}}" alt="">
                                <div class="text">
                                    <div class="location-title">{{$f_user->name}}</div>
                                </div>
                                <button class="btn btn-light send_message" id="send_message" data-user="{{$f_user->id}}">Send</button>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                            Send to a Friend
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="shared-post-comment">
                    <div class="shared-post-comment-details">
                        <div class="shared-post-comment-author">
                        <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                            {{Auth::user()->name}}
                        </div>
                    </div>
                    <div class="shered-post-comment-text">
                        <textarea name="" id="message_popup_text" cols="30" rows="10" placeholder="Write something..."></textarea>
                    </div>
                </div>
                <div class="shared-post-wrap" id="message_popup_content">
                    
                </div>
                <div class="shared-post-actions share-to-friend ">
                    <button class="extednd-btn ">
                        <span class="extend-ttl">Extend</span> 
                        <span class="shrink-ttl">Shrink</span>
                    </button>
                    <div class="friend-search">
                        <i class="trav-search-icon" ></i>
                        <input type="text" placeholder="Search in your friends list...">
                    </div>
                    <div class="friend-search-results">
                        @php
                            $friends_users = App\Models\User\User::whereIn('id',get_friendlist())->distinct()->get();
                         
                        @endphp
                        @if(count($friends_users)>0)
                            @foreach ($friends_users as $f_user)                      
                                <div class="friend-search-results-item">
                                    <img class="user-img" src="{{check_profile_picture($f_user->profile_picture)}}" alt="">
                                    <div class="text">
                                        <div class="location-title">{{$f_user->name}}</div>
                                    </div>
                                    <button class="btn btn-light send_message" id="send_message" data-user="{{$f_user->id}}">Send</button>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>
<div class="modal opacity--wrap spam-report-modal" data-backdrop="false"                                               id="spamReportDlgNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style report-modal-helper" role="document">
        <div class="post-block">
            <div class="share-modal-header">
                <div>
                  <h4>Report Post</h4>
                </div>
                <button class="share-modal__close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-check mb-2">
                        <i class="fa fa-exclamation-circle report-exclamation" aria-hidden="true"></i>
                        <h5 class="d-inline-block">What is the problem with this post?</h5>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container">
                                    <div class="radio-title-group">
                                        <div class="input-container">
                                            <input id="spam" class="radio-button" type="radio" name="spam_post_type" value="0" checked="checked">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Spam
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="fake-news" class="radio-button" type="radio" name="spam_post_type" value="2">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Fake News
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="harassment" class="radio-button" type="radio" name="spam_post_type" value="3">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Harassment                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="hate-speech" class="radio-button" type="radio" name="spam_post_type" value="4">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Hate Speech                                               </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="nudity" class="radio-button" type="radio" name="spam_post_type" value="5">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Nudity
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="terrorism" class="radio-button" type="radio" name="spam_post_type" value="6">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Terrorism
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="violence" class="radio-button" type="radio" name="spam_post_type" value="7">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Violence
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="other" class="radio-button" type="radio" name="spam_post_type" value="1">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Other
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group spam--input" style='margin-bottom: 0!important;'>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control report-span-input" id="spamText" placeholder="Something Else" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="share-footer-modal modal-footer">
                <div  class="share-footer-modal__icons">
                </div>
                <button style="display: block;" class="btn place_cancel_btn share-modal__close"  type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
                <button class="btn" type="submit" id="spamSend">Send</button> 
            </div>
            <input type="hidden" name="dataid" id="dataid">
            <input type="hidden" name="posttype" id="posttype">
        </div>
    </div>
</div>
@endif

