    <style>
    .fake__card
    {
        position: absolute;
        bottom: 0;
        height: 100%;
        cursor: pointer;
    }
    .checkbox-round {
        width: 1.3em;
        height: 1.3em;
        background-color: white;
        border-radius: 50%;
        vertical-align: middle;
        border: 1px solid #ddd;
        -webkit-appearance: none;
        outline: none;
        cursor: pointer;
    }

    .checkbox-round.active {
        content: '✔';
        background-color: #4080FF;
    }
    #scrollable-trip-plans::-webkit-scrollbar-track {
        border-radius: 10px;
        background-color: #F5F5F5;
    }
    #scrollable-trip-plans::-webkit-scrollbar {
        width: 12px;
        background-color: #F5F5F5;
    }
    </style>
    <div class="hero" data-id="0" style="background-image: url(@if(isset($place->getMedias[0]->url)) @if($place->getMedias[0]->thumbs_done==1)https://s3.amazonaws.com/travooo-images2/th1100/{{$place->getMedias[0]->url}} @else https://s3.amazonaws.com/travooo-images2/{{$place->getMedias[0]->url}} @endif @else {{asset('assets2/image/placeholders/pattern.png')}} @endif);width: 1060px;margin-left: auto;margin-right: auto;height: 525px;">
        <div class="hero__content" style="max-width:500px;">
            <div class='mobile-flex-hero'>
                <div class="hero__meta">
                    <h1 class="hero__title" style="font-size:30px;">{{@$place->trans[0]->title}}</h1>
                    @if(session('place_country') == 'place')
                    <h4 style="color: white">{{ucwords(@explode(',',$place->place_type)[0])}} in &nbsp;<a href="{{ url_with_locale('city/'.@$place->city->id)}}" target="_blank" style="color: white"> {{@$place->city->transsingle->title}}</a></h4>
                    @elseif (session('place_country') == 'country')
                    <h4 style="color: white">Country</h4>
                    @else 
                    <h4 style="color: white">City in &nbsp;<a href="{{ url_with_locale('country/'.@$place->country->id)}}" target="_blank"  style="color: white"> {{@$place->country->transsingle->title}}</a></h4>
                    @endif
                    @if(session('place_country') == 'place') 
                        <div class="hero__rating">
                            <div class="star-rating">
                                <span style="width: {{round($reviews_avg, 1) * 20}}%"></span>

                            </div>
                            <div class="hero__rating-value">{{round($reviews_avg, 1)}}</div>
                            <div class="hero__write-review d-none d-sm-block">
                                <a class="review_clk_btn" data-type="reviews" style="color:white" onclick="$('html, body').animate({scrollTop: $('.page-content__row').offset().top}, 1000);"> <u>Write a Review</u></a>
                            </div>
                        </div>
                    @endif

                        {{--                   <div class="hero__rating">--}}
                        {{--                        <div class="rating"><svg class="icon icon--star">--}}
                        {{--                            <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>--}}
                        {{--                            </svg><svg class="icon icon--star">--}}
                        {{--                            <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>--}}
                        {{--                            </svg><svg class="icon icon--star">--}}
                        {{--                            <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>--}}
                        {{--                            </svg><svg class="icon icon--star">--}}
                        {{--                            <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>--}}
                        {{--                            </svg><svg class="icon icon--star">--}}
                        {{--                            <use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use>--}}
                        {{--                            </svg></div>--}}
                        {{--                        <div class="hero__rating-value">{{round($reviews_avg, 1)}}</div>--}}
                        {{--                        <div class="hero__write-review"><a class="js-show-modal" href="#modal-review">Write a Review</a></div>--}}
                        {{--                    </div>--}}
                        <div class="hero__meta-header">
                        <h2 class="hero__meta-title">About</h2>
                        {{-- <a class="hero__see-all js-show-modal" href="#modal-about"><u>See All</u> --}}
                        </a>
                    </div>
                    <div class="hero__meta-items">
                        @if(session('place_country') == 'place')
                            @if(@$place->trans[0]->phone!='')
                            <div class="hero__meta-item"><svg class="icon icon--phone">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#phone')}}"></use>
                                </svg>
                                <div class="hero__meta-value">{{@$place->trans[0]->phone}}</div>
                            </div>
                            @endif

                            <div class="hero__meta-item"><svg class="icon icon--location">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use>
                                </svg>
                                <div class="hero__meta-value">{{@$place->trans[0]->address}}</div>
                            </div>
                            @if(@$place->trans[0]->description!='')
                            <div class="hero__meta-item"><svg class="icon icon--chain">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#chain')}}"></use>
                                </svg><a class="hero__meta-value" href="{{$place->trans[0]->description}}" target="_blank">Website</a>
                            </div>
                            @endif
                        @elseif(session('place_country') == 'country')
                            <div class="hero__meta-item">
                                <div class="hero__meta-value">{{count($place->followers)}} is following this country</div>
                            </div>
                            <div class="hero__meta-item">
                                <img src="{{asset('assets3/img/clock.png?ver=2')}}" style="width: 24px;margin-right: 6px;margin-top: -3px;">
                                <div class="hero__meta-value">{{count($place->timezones)}} Time zones</div>
                            </div>
                        @elseif(session('place_country') == 'city')
                            <div class="hero__meta-item">
                                <div class="hero__meta-value">{{count($place->followers)}} is following this city</div>
                            </div>
                            @if(session('place_country') == 'place')
                                <div class="hero__meta-item">
                                    <img src="{{asset('assets3/img/clock.png?ver=2')}}" style="width: 24px;margin-right: 6px;margin-top: -3px;">
                                    <div class="hero__meta-value">{{ @$place->timezone->time_zone_name . ' ' . @$place->timezone->dst_offset }}</div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="hero__buttons">
                    <span id="checkinContainer1">
                        @foreach($live_checkins AS $key=>$pci)
                            @if(Auth::check() && $pci->users_id == Auth::user()->id)
                                @php $is_live =1 @endphp
                            @endif
                        @endforeach

                            @if(isset($is_live) && $is_live == 1)
                            <a class="btn btn--secondary" id="checkInBtn"  data-toggle="modal" style='font-family: inherit;background-color:#28A069; opacity: 0.6;'>
                                <svg class="icon icon--location " style="margin-left:-20px"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use>
                                </svg>@lang('place.checked_in')
                            </a>
                            @else
                            @if(Auth::check())
                                <a class="btn btn--secondary" id="checkInBtn" onclick="this.style.color='white';" href="#checkin-modal" data-toggle="modal" style='font-family: inherit;'>
                            @else
                            <a class="btn btn--secondary open-login" href="javascript:;">
                            @endif
                                <svg class="icon icon--location " style="margin-left:-20px"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use>
                                </svg>Check-in
                            </a>
                            @endif
                            @if(Auth::check())
                                <a class="btn btn--secondary" @if(!isset($i_was_here_flag) || $i_was_here_flag==0) id="iWasHereInBtn" disabled="disabled" @endif  data-toggle="modal"  @if(isset($i_was_here_flag) && $i_was_here_flag >0)  style='font-family: inherit;margin-left:-10px;background-color:#28A069;opacity: 0.6;' @else style='font-family: inherit;background-color:#28A069' @endif>
                            @else
                                <a class="btn btn--secondary open-login" href="javascript:;">
                            @endif
                            <img style="margin-left: -15px;
                                width: 18px;
                                height: 16px;
                                margin-right: 3px;
                                padding-right: 3px;" src="{{asset('assets2/image/check.png')}}">I was here
                            </a>
                            <a class='btn btn-details' type="button" rolle='button'>More Details</a>


                    </span>
                    <span id="followContainer1">
                    <?php
                    $follower = [];
                    if(Auth::check()){
                        if(session('place_country') == 'place'):
                        $follower = App\Models\Place\PlaceFollowers::where('places_id', $place->id)
                            ->where('users_id', Auth::guard('user')->user()->id)
                            ->first();
                        elseif(session('place_country') == 'country'):
                        $follower = App\Models\Country\CountriesFollowers::where('countries_id', $place->id)
                            ->where('users_id', Auth::guard('user')->user()->id)
                            ->first();
                        elseif(session('place_country') == 'city'):
                        $follower = App\Models\City\CitiesFollowers::where('cities_id', $place->id)
                            ->where('users_id', Auth::guard('user')->user()->id)
                            ->first();
                        endif;
                    }
                    ?>
                    @if(empty($follower))
                    <a class="btn btn--main button_follow {{Auth::check()?'':'open-login'}}" style="font-family: inherit;" href="#"><svg class="icon icon--follow"><use xlink:href="{{asset('assets3/img/sprite.svg#follow')}}"></use></svg>@lang("place.follow")</a>
                    @else
                    <a class="btn btn--main button_unfollow" style="font-family: inherit;opacity:0.7" href="#"><svg class="icon icon--follow"><use xlink:href="{{asset('assets3/img/sprite.svg#follow')}}"></use></svg>@lang("place.unfollow")</a>
                    @endif
                    </span>

                </div>
            </div>
            <div class="hero__links">
                @if(isset($live_users) && count($live_users)>0)
                <a style="color:white;margin-bottom:10px" href="#live_user_popup" data-toggle="modal">
                    <div class="" style="display: inline-flex;flex-direction: row-reverse;padding-bottom: 10px;padding-left: 6px;">
                        @php $avatar_counters=1 @endphp
                        @foreach ($live_users as $user)
                            @if( $avatar_counters<=3)
                                <img class="avatar" src="{{check_profile_picture($user['users']->profile_picture)}}" alt="Rosita Rodriguez" title="Rosita Rodriguez" role="presentation" dir="auto">
                            @endif
                            @php $avatar_counters++; @endphp
                        @endforeach
                    </div>
                    <div style="display: inline-block;"></div>
                    <span class="hero__link-text " style="font-family:inherit;">
                        @if(count($live_users)>10)
                            +10 checked-in here in the past 24 hours
                        @else
                            {{count($live_users)}}
                                checked-in here in the past 24 hours
                        @endif
                    </span>
                </a>
                @endif
                <a class="hero__link popover-link" href="#"><svg class="icon icon--add-plan">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#add-plan')}}"></use>
                    </svg>
                    <span class="hero__link-text popup-marker top-popup" onclick="this.style.color='white';" data-toggle="popover" data-placement="bottom" data-html="true" data-popover-content="#yourContentHere"><strong style="    font-size: 16px;">Add</strong> <span >to a trip plan</span></span>
                    <div id="yourContentHere" style="display:none;">
                        <div class="popover-body" id="scrollable-trip-plans" style="max-height:300px">
                            @if(Auth::check())
                                <ul class="list-group">
                                @foreach($my_plans AS $mp)
                                    <li style="cursor: pointer;" class="list-group-item plans_id"  data-title="{{$mp->title}}"  name="plans_id" id="plans_id" value="{{$mp->id}}">  <span style="">{{$mp->title}}</span></li>
                                @endforeach
                                </ul>
                            @endif
                            <div class="popover-footer-plan" style="background-color:#F3F3F3;height: 40px;">
                                <div onclick="location.href='{{ route('trip.plan', 0) }}?add={{$place->id}}&add-type={{$place_type}}';">
                                    <i class="trav-trip-plan-loc-icon" style="margin-right:10px;color: #4080FF;"></i>
                                    <span>Create New Trip Plan</span>
                                </div>

                            </div>
                        </div>
                    </div>
                </a>
                @php
                    $place_type = explode(',',$place->place_type);
                @endphp
                @if(in_array('lodging',$place_type))
                    <a class="hero__link" href="#"><svg class="icon icon--tag">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#tag')}}"></use>
                        </svg>
                        <span class="hero__link-text flyToLink" onclick="this.style.color='white';"><strong style="    font-size: 16px;">Check Prices</strong><span > of this {{ session('place_country') }}</span></span>
                    </a>
                @else
                    <a class="hero__link" href="#"><svg class="icon icon--tag">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#tag')}}"></use>
                        </svg>
                        <span class="hero__link-text find--hotels" onclick="this.style.color='white';"><strong style="    font-size: 16px;">Find Hotels </strong> <span>near this {{ session('place_country') }}</span></span>
                    </a>
                @endif
                <a class="hero__link" href="#"><svg class="icon icon--plane">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#plane')}}"></use>
                    </svg>
                    <span class="hero__link-text flyToLink" id="flyToLink" onclick="this.style.color='white';"><strong style="    font-size: 16px;">Fly To</strong> <span >this {{ session('place_country') }}</span></span>
                </a>
                <a class="hero__link" href="#"><svg class="icon icon--share">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
                    </svg>
                    <span class="hero__link-text js-show-modal {{!Auth::check()? 'open-login': ''}}" onclick="this.style.color='white';" href="#modal-share"><strong style="    font-size: 16px;">Share</strong> <span >this {{ session('place_country') }}</span></span>
                </a>
            </div>
        </div>
        <div class="fake__card mediaModalTrigger @if(count($place->getMedias) > 0)  js-show-modal" href="#modal-medias" @else " @endif style="cursor:pointer;" data-src="@if(isset($place->getMedias[0]) && is_object($place->getMedias[0]) && isset($place->getMedias[0]->url)) https://s3.amazonaws.com/travooo-images2/th1100/{{@$place->getMedias[0]->url}} @else {{asset('assets2/image/placeholders/pattern.png')}} @endif" onclick="javascript:$('#modal-medias').find('.trip__cards').show();" data-id="0"></div>
        <div class="hero__cards">



        </div>

        <div class="user-list hero__user-list" style="position:fixed;z-index:1000;-webkit-box-shadow: 0px -2px 6px 0px rgba(0,0,0,0.23);-moz-box-shadow: 0px -2px 6px 0px rgba(0,0,0,0.23);box-shadow: 0px -2px 6px 0px rgba(0,0,0,0.23);">
                <?php $pce0_count = 0; $output_0 = '';?>
                @foreach($experts_top AS $key=>$pce)
                    <?php
                    $output_0 .= '<a class="user-list__user js-show-modal" href="#ExpertsModal" data-id="1"><img class="user-list__avatar" data-src="'.check_profile_picture($pce->user->profile_picture).'" alt title="'.$pce->user->name.'" role="presentation" style="width: 46px;height: 46px;" /></a>';
                    ?>
                        @if($loop->index==4)
                        @break
                        @endif
                    <?php $pce0_count++; ?>
                @endforeach
                @if($pce0_count>5)
                <?php
                $output_0 .= '<a class="user-list__more js-show-modal" href="#ExpertsModal" data-id="1">+'.($pce0_count-5).'</a>';
                ?>
                @endif

                @if($output_0!=='')
                <div class="user-list__item">
                 <h2 class="user-list__title js-show-modal" href="#ExpertsModal" data-id="1" style="cursor:pointer;">Top <br/> Experts</h2>
                {!!$output_0!!}
                </div>
                @endif



                <?php $pce1_count = 0; $output_1 = '';?>

                @foreach($experts_local AS $key=>$pce)
                    <?php
                    $output_1 .= '<a class="user-list__user js-show-modal" href="#ExpertsModal" data-id="2"><img class="user-list__avatar" data-src="'.check_profile_picture($pce->user->profile_picture).'" alt="" title="'.$pce->user->name.'" role="presentation" style="width: 46px;height: 46px;" /></a>';
                    ?>
                        @if($loop->index==2)
                        @break
                        @endif
                    <?php $pce1_count++; ?>
                @endforeach
                @if($pce1_count>3)
                <?php
                $output_1 .= '<a class="user-list__more js-show-modal" href="#ExpertsModal" data-id="2">+'.($pce1_count-3).'</a>';
                ?>
                @endif
                @if($output_1!=='')
                <div class="user-list__item">
                <h2 class="user-list__title js-show-modal" href="#ExpertsModal" data-id="2" style="cursor:pointer;">Local <br/> Experts</h2>
                {!!$output_1!!}
                </div>
                @endif




                <?php $pce2_count = 0; $output_2 = '';?>


                @foreach($experts_friends AS $key=>$pce)
                    <?php
                    $output_2 .= '<a class="user-list__user js-show-modal" href="#ExpertsModal" data-id="3"><img class="user-list__avatar" data-src="'.check_profile_picture($pce->user->profile_picture).'" alt="" title="'.$pce->user->name.'" role="presentation" style="width: 46px;height: 46px;" /></a>';
                    ?>

                        @if($loop->index==2)
                        @break
                        @endif
                        <?php $pce2_count++; ?>
                @endforeach
                @if($pce2_count>3)
                <?php
                    $output_2 .= '<a class="user-list__more js-show-modal" href="#ExpertsModal" data-id="3">+'.($pce2_count-3).'</a>';
                ?>
                @endif
                @if($output_2!=='')
                <div class="user-list__item">
                <h2 class="user-list__title js-show-modal" href="#ExpertsModal" data-id="3" style="cursor:pointer;">My <br/> Friends</h2>
                {!!$output_2!!}
                 </div>
                @endif


                <?php $pce3_count = 0; $output_3 = ''; ?>
                @foreach($live_checkins AS $key=>$pci)
                    @if(is_object($pci->user))
                    <?php
                    $output_3 .= '<a class="user-list__user js-show-modal" href="#ExpertsModal" data-id="4"><img class="user-list__avatar" data-src="'.check_profile_picture($pci->user->profile_picture).'" alt="" title="'.$pci->user->name.'" role="presentation" style="width: 46px;height: 46px;" /></a>';
                    ?>
                        @if($loop->index==3)
                        @break
                        @endif
                        <?php $pce3_count++; ?>
                    @endif
                @endforeach
                    @if(count($live_checkins)>4)
                    <?php
                    $output_3 .= '<a class="user-list__more js-show-modal" href="#ExpertsModal" data-id="4">+'.(count($live_checkins)-4).'</a>';
                    ?>
                    @endif

                    @if($output_3!=='')
                    <div class="user-list__item">
                    <h2 class="user-list__title js-show-modal" href="#ExpertsModal" data-id="4" style="cursor:pointer;">Live <br/> Check-ins</h2>
                    {!!$output_3!!}
                    </div>
                    @endif

        </div>
    </div>

