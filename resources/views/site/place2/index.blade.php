<!DOCTYPE html>
<html class="page" lang="en">
    <head>
        <title>{{@$place->trans[0]->title}} on Travooo</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="date=no">
        <meta name="format-detection" content="address=no">
        <meta name="format-detection" content="email=no">
        <meta content="notranslate" name="google">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <!-- <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css"> -->
        <link rel="stylesheet" href="{{asset('assets2/css/place.css?v='.time()) }}">
        <link rel="stylesheet" href="{{asset('assets2/css/travooo.css?v='.time())}}">
        <link rel="stylesheet" href="{{asset('assets3/css/style-13102019.css?v=0.2')}}">
        <link rel="stylesheet" href="{{asset('assets3/css/datepicker-extended.css')}}">
        <!-- <link rel="stylesheet" type="text/css" href="{{asset('assets3/css/slider-pro.min.css')}}" media="screen"/> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
        <link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
        <link href="{{ asset('assets2/js/summernote/summernote.css') }}" rel="stylesheet">
        <link href="{{ asset('/plugins/summernote-master/tam-emoji/css/emoji.css') }}" rel="stylesheet"/>

        <link rel="stylesheet" href="{{asset('assets3/css/star.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/lightslider.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous">
        <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
        <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
        <script src="https://npmcdn.com/@turf/turf@5.1.6/turf.min.js"></script>
        <style>
            .vertical-center {
                margin-top: 44px !important;
            }
            .modal__close {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                position: absolute;
                top: 20px;
                right: 20px;
                width: 26px;
                height: 26px;
                background-color: rgba(255, 255, 255, .5);
                color: rgba(0, 0, 0, .5);
                border-radius: 50%;
            }
	        .textarea-customize {
                border: 1px solid #dcdcdc;
                border-radius: 3px;
                resize: none;
            }
            .comment {
                margin-bottom: 10px;
            }
            .comment-info{
                display: flex;
                align-items: center;
            }
            .comment-info i{
                margin-right: 9px;
                color: #4080ff;
                font-size: 18px;
            }

            .carousel-cards__control--prev {
                display:flex !important;
                /*
                background: -webkit-gradient(linear,right top,left top,from(rgba(255,255,255,0)),color-stop(70%,#fff));
                background: linear-gradient(to left,rgba(255,255,255,0) 0%,#fff 70%);
                 */
                background: none !important;
            }

            .carousel-cards__item.slick-active > .carousel-cards__card {
                border-color: #4080ff;
                -webkit-box-shadow: 0 1px 6px rgba(0,0,0,.1);
                box-shadow: 0 1px 6px rgba(0,0,0,.1);
            }

            .icon--star {
                color:  #ccc;
            }

            .icon--star-active {
                color: #4080ff!important;
            }
            .user-card.post {
                position: relative;
                padding-bottom: 50px;
            }
            .user-card.post .dropdown {
                position: absolute;
                top: 8px;
                right: 12px;
            }
            .user-card.post .updownvote {
                position: absolute;
                bottom: 15px;
            }
            #lightbox {
                display: flex;
                flex-direction: column-reverse;
            }
            .post__content .event-item::after {
                position: initial;
            }
            .post__tip
            {
                padding-bottom: 15px;
                padding-top: 5px;
            }
            .post__tip-loadmore
            {
                padding: 0;
                border: 0;
                background-color: transparent;
                cursor: pointer;
            }

            .post__tip-upvotes,
            .post__tip-downvotes,
            .post__tip-upvotes:focus,
            .post__tip-downvotes:focus
            {
                outline:none;
            }

            .post__tip-downvotes {
                padding: 0;
                border: 0;
                background-color: transparent;
                cursor: pointer;
                margin-right: 24px;
            }

            .icon-vote {
                width:20px;
                height:20px;

                fill: #AAAAAA;
            }

            .icon-vote-active {
                fill: #4080ff;
            }

            .icon--angle-down-solid{

                transform: rotate(180deg);
            }



            .bs-tooltip-bottom {
                padding: .4em 0;
            }
            .tooltip .arrow {
                top: 0;
                position: absolute;
                display: block;
                width: .8em;
                height: .4em;
                opacity: 0.8;
            }
            .tooltip .tooltip-inner {
                text-align: center;
            }
            .tooltip .arrow::before {
                bottom: 0;
                border-width: 0 .4rem .4rem;
                border-top-color: #000;
                position: absolute;
                content: "";
                border-color: transparent;
                border-style: solid;
                border-bottom-color: #000;
                right: 0;
            }
            .location-identity.active {
                color: #4080ff;
            }
            .location-identity {
                outline: none;
                position: absolute;
                right: 10px;
                top: 11px;
                background: transparent;
                color: #ccc;
            }
            .fa-heart {
                color: #a3a3a3;
            }
            .fa-heart.red {
                color: red;
            }

            .travelmate-post-footer{
                border-left: 1px solid #e6e6e6;
                border-right: 1px solid #e6e6e6;
            }

            .travelmate-post-footer .sub-footer {
                padding: 15px;
                border-bottom: 1px solid #e6e6e6;
            }

            .travelmate-post-footer-info {

            }

            .travelmate-post-footer-info {
                font-size: 1rem;
                font-weight: normal;
            }

            .d-flex {
                display: flex!important;
            }

            .flex-wrap {
                flex-wrap: wrap!important;
            }

            .justify-content-between {
                -ms-flex-pack: justify!important;
                justify-content: space-between!important;
            }

            .w-50 {
                width: 50%!important;
            }
            .w-40 {
                width: 40%!important;
            }

            .mb-3 {
                margin-bottom: 1rem!important;
            }

            /* The popup bubble styling. */
            .popup-bubble {
                /* Position the bubble centred-above its parent. */
                position: absolute;
                top: 0;
                left: 0;
                transform: translate(-50%, -100%);
                /* Style the bubble. */
                background-color: white;
                padding: 5px;
                border-radius: 5px;
                font-family: sans-serif;
                overflow-y: auto;
                max-height: 60px;
                box-shadow: 0px 2px 10px 1px rgba(0,0,0,0.5);
            }
            /* The parent of the bubble. A zero-height div at the top of the tip. */
            .popup-bubble-anchor {
                /* Position the div a fixed distance above the tip. */
                position: absolute;
                width: 100%;
                bottom: /* TIP_HEIGHT= */ 8px;
                left: 0;
            }
            /* This element draws the tip. */
            .popup-bubble-anchor::after {
                content: "";
                position: absolute;
                top: 0;
                left: 0;
                /* Center the tip horizontally. */
                transform: translate(-50%, 0);
                /* The tip is a https://css-tricks.com/snippets/css/css-triangle/ */
                width: 0;
                height: 0;
                /* The tip is 8px high, and 12px wide. */
                border-left: 6px solid transparent;
                border-right: 6px solid transparent;
                border-top: /* TIP_HEIGHT= */ 8px solid white;
            }
            /* JavaScript will position this div at the bottom of the popup tip. */
            .popup-container {
                cursor: auto;
                height: 0;
                position: absolute;
                /* The max width of the info window. */
                width: 200px;
            }
             /* need assist css under task:https://trello.com/c/zIm45cSD/1513-places-notes-about-the-need-assistance-box-on-the-rhs-read-checklist */
             .contributor-title:hover {
                color:#4080ff !important;
            }
            .contributor-title{
                color:black;
            }
            .contributor-footer-img{
                width: 40px;
                margin-right: 5px;
            }
            .expert-info-img-div{
                margin-left: 50px;
                margin-top: 20px;
                margin-bottom: 10px;
            }
            .datepicker{
                z-index: 1650 !important;
            }        
            /* round checkbox for add to tripplance popover     */
            .popover{
                max-width: 300px;
                width: 100%;
            }
            .skyScannerHighlight{
                background-color:#c9c9c9;
                -webkit-transition: all 1s;
                -moz-transition: all 1s;
                -o-transition: all 1s;
                transition:all 1s;
            }
            .skyScannerHighlight{
                background-color:#f2eeee;
                -webkit-transition: all 1s;
                -moz-transition: all 1s;
                -o-transition: all 1s;
                transition:all 1s;
            }
            .skyScannerHighlightoff{
                background-color:#FFFFF;
                -webkit-transition: all 1s;
                -moz-transition: all 1s;
                -o-transition: all 1s;
                transition:all 1s;
            }
            .location-container{
                background: white;
                width: 100%;
                height: 45px;
                border: 1px solid #EDEDED;
                border-radius:3px;
            }
            .trip-plan-places-image
            {
                width: 45px;
                height: 45px;
            }
            .counter-btn{
                width:13px;border-radius:3px;cursor:pointer;color:#B6B6B6
            }
            .counter-input{
                border: none;
                width: 20px;
                margin: 0px;
                padding: 0px;
                padding-bottom: 0;
                padding-left: 0;
                margin-left: 10px;
            }
            .counter-div{
                background:white;height:45px; width:100%;border:1px solid #EDEDED;border-radius:3px;
            }
            .spent-input{
                height: 45px;
                border: 1px solid #EDEDED;
                border-radius: 3px;
                width: 96px;
                padding-left: 15px;
                float: left;
            }
            .ui-timepicker-wrapper{
                z-index: 3500 !important;
            }.btn-places{
                padding: 10px;
                width: 140px;
                height: 36px;
                border-radius: 3px;
                background: #F5F5F5;
                border: 1px solid #E9E9E9;
                margin-right: 12px;
                color: #818181 !important;
                font-family: inherit;
                cursor: pointer;
                text-decoration: none;
            }
            #ck-button {
                margin-left: 8px;
                background-color:#FCFCFC;
                border-radius:4px;
                border:1px solid #D0D0D0;
                overflow:auto;
                float:left;
                background-image: linear-gradient(0deg,rgba(0,0,0,.06) 0,transparent);
            }
            #ck-button:hover {
                cursor: pointer;
            }
            #ck-button label {
                float: left;
                width: 55px;
                height: 31px;
                padding-top: 10px;
                cursor: pointer;
            }
            #ck-button label span {
                text-align:center;
                padding:3px 0px;
                display:block;
            }
            #ck-button label input {
                position:absolute;
                top:-20px;
            }
            #ck-button input:checked + #ck-button {
                background-color:#3F7EFA;
                color:#fff;
            }
            .avatar{
                margin-left: -5px;
                position: relative;
                border: 1px solid #fff;
                border-radius: 50%;
                overflow: hidden;
                width: 50px;
                /* height: 50px; */
                width: 20px;
            }
            .btn-checkin-follow{
                height: 35px;
                float: right;
                margin-top: 11px;
                margin-right: 14px;
                width: 90px;
                font-family: inherit;
                margin-top: 0px;
            }.btn-unfollow{
                color: #000000;
                background: #e0e0e0;
                font-family: inherit;
            }
            .filter-separtor{
                width: 360px;
                border-bottom: 2px solid #dddd;
                display: inline-block;
            }
            .filter-btn{
                outline: none;
                color: #4483FF;
                height: 35px;
                width: 220px;
                font-family: inherit;
                border-radius: 5px;
                background-color: #E3ECFF;
                outline: none;
            }
            .select2-dropdown {
                z-index: 9001;
            }
            div:not[.ask-input-wrap] .select2-container {
                width: 80% !important;
            }
            .post-asking-block .select2-input {
                height: 49px !important;
                padding: 8px !important;
            }
            .summernote-selected-items i {
                color: #b3b3b3;
                font-size: 16px;
            }
            .note-editor.note-frame{
                border: none !important;
            }
            .note-statusbar{
                display: none !important;
            }
            .note-editor{
                width: 80% !important;
            }
            .note-toolbar{
                display: none !important;

            }
        </style>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> 
        {{-- <script src="{{ asset('assets2/js/jquery-3.1.1.min.js') }}"></script> --}}
        <script>
            var map_var = "";
            var map_func = "";
        </script>
    </head>

    <body class="page__body">

    <svg style="position: absolute; width: 0; height: 0; overflow: hidden" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="angle-circle-up-solid" viewBox="0 0 21 21">
                <path d="M10.5.5c5.523 0 10 4.476 10 9.999 0 5.523-4.477 10-10 10s-10-4.477-10-10 4.477-10 10-10zm1.325 7.937L10.539 7.06l-.04.041-.038-.04-1.287 1.377.032.034L6.1 11.758l1.286 1.378L10.5 9.841l3.112 3.295 1.287-1.378-3.105-3.288z"/>
            </symbol>
        </defs>
    </svg>
    @if(Auth::check())
        @include('site/layouts/_header')
    @else
        @include('site/layouts/_non-loggedin-user-header')
    @endif
    
        <div class="page-content page__content {{Auth::check()?'':'pb-90'}}">
            @include('site.place2._hero_block')
            <div class='mobile-assistance'>
                <div class='mobile-assistance_inner'>
                <div>
                    <p>Need Assistance?</p>
                </div>
                <a data-toggle="modal" href="#askingPopup">Ask Expert</a>
                </div>
            </div>
            <div class='cards-mobile-wrap'>
                @include('site.place2._picture_card_block')
            </div>
        </div> 
    </div>
    @php
    if(Auth::check()){
        $nationality = App\Models\Country\CountriesTranslations::where('countries_id',Auth::guard('user')->user()->nationality)->first();
    }else{
        $nationality = App\Models\Country\CountriesTranslations::where('countries_id', $current_country_id)->first();
    }
        
    @endphp
    <div class="page-content__inner mobile-inner">
        <div class='mobile-tabs-desktop mobile-tabs-only'>
            @include('site.place2.partials._posts_filters_block')
        </div>
        <div class="page-content__row">
            <div class="page-content__main">
                <div style="position:relative;" class='post-mobile-helper'>
                    @if(Auth::check())      
                        @include('site.place2.partials._add_post_block')
                        @include('site.place2.partials._add_discussion_block')
                        @include('site.place2.partials._add_tripplan_block')
                        @include('site.place2.partials._add_travel_mates_block')
                        @include('site.place2.partials._add_review_post')
                        @include('site.place2.partials._add_post_report')
                    @endif
                </div>
                <div class="filter-div" dir="auto" style=" margin-bottom: 10px;margin-top: 10px;">
                <span dir="auto" style="font-family: inherit;">
                    Filter by
                </span>
                <div dir="auto" class="filter-separtor" style=""></div>
                <div class="filter-button place__filters" style="display: inline-block;">
                <button class="filter-btn" data-toggle="popover" data-placement="bottom" data-html="true" data-popover-content="#filters-content" people="0" location="0">From anyone / From anywhere</button>
            <div id="filters-content" style="display:none;">
                <div class="popover-body post-filter-popover-body" dir="auto">
                    <span dir="auto" style="font-weight: 500;margin-left: 10px;">
                                            People
                        </span>
                        <div class="" dir="auto" style="margin-top: 10px;margin-left: 10px;width: 96%;height: 1px;border-bottom: 1px solid #ddd;margin-bottom: 10px;">
                    </div>
                    <div class="row mb-1" dir="auto">
                        <div class="col-10" dir="auto">
                            <span style="margin-left: 10px;" dir="auto">From anyone</span>
                        </div>
                        <div class="col-2" dir="auto">
                            <input data-type="people" data-value="anyone" name="filter-people" type="checkbox" value="0" class="checkbox-round filter-people filter-arr" dir="auto" checked>  
                        </div>
                    </div>
                    <div class="row mb-3" dir="auto">
                        <div class="col-10" dir="auto">
                            <span style="margin-left: 10px;font-family: inherit;" dir="auto">People you follow</span>
                        </div>
                        <div class="col-2" dir="auto">
                            <input data-type="people" data-value="follow" name="filter-people" type="checkbox" value="1" class="filter-people checkbox-round filter-arr" dir="auto" > 
                        </div>
                    </div>
                    <span dir="auto" style="font-weight: 500;margin-left: 10px;">
                        Location
                        </span>
                        <div class="" dir="auto" style="margin-top: 10px;margin-left: 10px;width: 96%;height: 1px;border-bottom: 1px solid #ddd;margin-bottom: 10px;">
                    </div>
                    <div class="row mb-1" dir="auto">
                        <div class="col-10" dir="auto">
                            <span style="margin-left: 10px;" dir="auto">Anywhere</span>
                        </div>
                        <div class="col-2" dir="auto">
                            <input data-type="location" data-value="location" name="filter-location" type="checkbox" value="0" class="checkbox-round filter-location filter-arr" dir="auto" checked>  
                        </div>
                    </div>
                    <div class="row mb-1" dir="auto">
                        <div class="col-10" dir="auto">
                        <span style="margin-left: 10px;font-family: inherit;" dir="auto">{{isset($nationality->title)?'From '.$nationality->title:"Near you"}}</span>
                        </div>
                        <div class="col-2" dir="auto">
                            <input data-type="location" data-value="location" name="filter-location" type="checkbox" value="1" class="checkbox-round filter-location filter-arr" dir="auto">  
                        </div>
                    </div>
                    <div class="sorting-filter-div" style="display:none">
                        <span dir="auto" style="font-weight: 500;margin-left: 10px;">
                            Sort by
                            </span>
                            <div class="" dir="auto" style="margin-top: 10px;margin-left: 10px;width: 85%;height: 1px;border-bottom: 1px solid #ddd;margin-bottom: 10px;">
                        </div>
                        <div class="row mb-1" dir="auto">
                            <div class="col-10" dir="auto">
                                <span style="margin-left: 10px;" dir="auto">Oldest</span>
                            </div>
                            <div class="col-2" dir="auto">
                                <input data-type="sort" data-value="sort" name="filter-sort" type="checkbox" value="1" class="checkbox-round filter-sort filter-arr" dir="auto" >  
                            </div>
                        </div>
                        <div class="row mb-1" dir="auto">
                            <div class="col-10" dir="auto">
                                <span style="margin-left: 10px;font-family: inherit;" dir="auto">Newest</span>
                            </div>
                            <div class="col-2" dir="auto">
                                <input data-type="sort" data-value="sort" name="filter-sort" type="checkbox" value="0" class="checkbox-round filter-sort filter-arr" dir="auto" checked  >  
                            </div>
                        </div>
                        <div class="row mb-1" dir="auto">
                            <div class="col-10" dir="auto">
                                <span style="margin-left: 10px;font-family: inherit;" dir="auto">Popular</span>
                            </div>
                            <div class="col-2" dir="auto">
                                <input  data-type="sort" data-value="sort" name="filter-sort" type="checkbox" value="2" class="checkbox-round filter-arr filter-sort" dir="auto"  >  
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
            </div></div>
                <div class="posts mobile-post-helper" id="body4highlight">
                   
                    <div id="newsfeed_content">




                    @if(is_array($pposts))
                    @foreach($pposts AS $ppost)
                        @if($ppost['type']=='regular')
                            <?php
                            $post = App\Models\Posts\Posts::find($ppost['id']);
                            ?>
                            @if($post->text!='' && count($post->medias)>0)
                                @include('site.place2.partials.post_media_with_text')
                            @elseif($post->text=='' && count($post->medias)>0)
                                @include('site.place2.partials.post_media_without_text')
                            @else
                                @include('site.place2.partials.post_text')
                            @endif

                        @elseif($ppost['type']=='discussion')

                        <?php
                        $discussion = App\Models\Discussion\Discussion::find($ppost['id']);
                        ?>
                        @include('site.place2.partials.post_discussion')




                        @elseif($ppost['type']=='plan')
                        <?php
                        $plan = \App\Models\TripPlans\TripPlans::find($ppost['id']);
                        ?>
                        @include('site.place2.partials.post_plan')




                        @elseif($ppost['type']=='report')

                        <?php
                        $report = App\Models\Reports\Reports::find($ppost['id']);
                        if($report){
                        ?>
                        @include('site.place2.partials.post_report')
                        <?php }?>



                        @endif
                    @endforeach
                    @endif

                    </div>
                </div>
                <div id='pagination' style='text-align:center;'>
                    <input type="hidden" id="pageno" value="1">
                    <input type="hidden" id="is_parent" value="0">
                    <div id="feedloader" class="post-animation-content post">
                        <div class="post__header">
                                <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                                <div class="post-animation-info-txt">
                                    <div class="post-top-name animation post-animation-top-name"></div>
                                    <div class="post-info animation post-animation-top-info"></div>
                                </div>
                        </div>
                        <div class="post__content">
                            <div class="post-animation-text animation pw-2"></div>
                            <div class="post-animation-text animation pw-3"></div>
                        <div class="post-animation-footer">
                            <div class="post-animation-text animation pw-4"></div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="page-content__side">
                <div class="card mobile-hide">
                    <div class="card__header">
                        <h2 class="card__title">Need Assistance?
</h2>
                        <a class="card__link card__link--lg"  @if(count($place_top_contributors)>0) style="margin-right:-80px;" @endif data-toggle="modal" href="#askingPopup">Ask Experts</a>  @if(count($place_top_contributors)>0)<a href="#become-expert-modal" data-toggle="modal"> <img src="{{asset('assets2/image/information_places_of_pages.png')}}"> </a> @endif
                    </div>
                    <div class="assistance card__content place-aside-experts-loader">
                        <div class="experts-top-info-layer">
                            <div class="experts-top-info-wrap">
                                <div class="experts-top-avatar-wrap animation"></div>
                                <div class="experts-animation-info-txt">
                                    <div class="experts-animation-top-name animation"></div>
                                    <div class="experts-animation-top-info animation"></div>
                                </div>
                            </div>
                        </div>
                        <div class="experts-top-info-layer">
                            <div class="experts-top-info-wrap">
                                <div class="experts-top-avatar-wrap animation"></div>
                                <div class="experts-animation-info-txt">
                                    <div class="experts-animation-top-name animation"></div>
                                    <div class="experts-animation-top-info animation"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="assistance card__content aside-inner-scroller place-aside-experts-block d-none">
                    @if(count($place_top_contributors)>0)
                         @foreach($place_top_contributors AS $key=>$pcex)

                        <div class="assistance__item">
                            <div class="assistance__avatar-wrap">
                                <a href="{{url_with_locale('profile/'.$pcex->author->id)}}"><img class="assistance__avatar" data-src="{{check_profile_picture($pcex->author->profile_picture)}}" alt="{{$pcex->author->name}}" title="{{$pcex->author->name}}" role="presentation" /></a>
                                @if($loop->index<3)
                                <div class="assistance__icon"><svg class="icon icon--fire">
                                    <use xlink:href="{{asset('assets3/img/sprite.svg#fire')}}"></use>
                                    </svg></div>
                                @endif
                            </div>
                            <div class="assistance__content">
                                <div class="assistance__username">
                                    <a class="contributor-title" href="{{url_with_locale('profile/'.$pcex->author->id)}}">
                                    {{$pcex->author->name}}
                                    </a>
                                </div><a class="assistance__link" href="javascript:void(0)" onclick="update_newsfeed('discussion_replies_24hrs', {{$pcex->author->id}})">View {{$pcex->count}} contributions</a>
                            </div>
                            <div class="assistance__views">
                                <div class="assistance__views-number">{{$pcex->views}}</div>
                                <div class="assistance__views-label">Views</div>
                            </div>
                        </div>
                         @endforeach
                    @else
                    <!--                    <div style="font-weight: 100;display: block;line-height: 1.3em;">
                                            Looks like there is now Experts in this City/Country. <br />
                        Why don't you become an Expert by participating in the <a href="{{url_with_locale('discussions')}}">Discussion Forum</a> now?..
                                        </div>-->
                    <!--Updated left Need assistance box according to task on trello linked: https://trello.com/c/zIm45cSD/1513-places-notes-about-the-need-assistance-box-on-the-rhs-read-checklist-->
                    <div style="font-weight: 100;display: block;line-height: 1.3em;">
                        <img src="{{asset('assets2/image/become-expert-logo.png')}}" alt="Travooo" style="display:inline-block;margin-right: 10px;" dir="auto"><h5 style="display:inline-block" class="card-title">Become a Travooo Expert</h5>
                        <br>
                        <p style="margin-top: 10px;line-height: 20px;">It seems that we don't actively have any advisors for this place. You could become the first one by assisting other people with their questions.
                                                                              </p>
                        <p style="line-height: 20px;">
                            <strong>Become an Expert</strong> by joining our community and participating in the discussion topics related to this place, or by submitting your own recommendations.<br>Click the button below to know more.
                        </p>
                    </div>
                    <div class="text-center">
                        <a href="#become-expert-modal" data-toggle="modal" class="btn  btn--main" style="font-family: inherit; width:100%">Become an Expert</a>
                    </div>
                    @endif

                    </div>
                </div>
                @if(session('place_country') == 'country')
                    <div class="card mobile-hide">
                        <div class="card__header">
                            <h2 class="card__title top_places_btn active">Top Places</h2><h2 class="card__title top_cities_btn">Top Cities</h2><a class="card__link" href="{{url_with_locale('reports/list')}}">See All</a>
                        </div>
                        <div class="card__content tp_places_div" style="max-height: 300px;overflow-y: scroll;">

                            <div class="post-block post-side-block" style="border: none">
                                <div class="post-side-inner top-place-animation-loader" style="padding: 0">
                                    <div class="side-place-block place-animation-side-block">
                                        <div class="place-animation-title">
                                            <div class="place-top-info-layer">
                                                <div class="place-top-info-wrap">
                                                    <div class="place-top-avatar-wrap animation post-animation-avatar"></div>
                                                    <div class="place-animation-info-txt">
                                                        <div class="place-top-name animation"></div>
                                                        <div class="place-info animation"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="place-animation-content">
                                            <div class="animation"></div><div class="animation"></div><div class="animation"></div><div class="animation"></div>
                                        </div>
                                        <div class="place-animation-footer">
                                            <div class="place-animation-count animation"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="post-side-inner top-place-animation-block d-none" style="padding: 0">
                                    @foreach($top_places as $tplace)
                                    <div class="side-place-block">
                                        <div class="side-place-top">
                                            <div class="side-place-avatar-wrap">
                                                <img class="lazy" data-src="https://s3.amazonaws.com/travooo-images2/th180/{{@$tplace->place->medias[0]->url}}"
                                                    alt="" style="width:46px;height:46px;">
                                            </div>
                                            <div class="side-place-txt">
                                                <a class="side-place-name" style="white-space: normal"
                                                href="{{ route('place.index', @$tplace->place->id) }}">{{@$tplace->place->trans[0]->title}}</a>
                                                <div class="side-place-description">
                                                    <b>{{@do_placetype($tplace->place->place_type)}}</b> in <a
                                                        href="{{ route('city.index', @$tplace->place->city->id) }}">{{@$tplace->place->city->trans[0]->title}}</a>,
                                                    <a href="{{ route('country.index', @$tplace->place->country->id)}}">{{@$tplace->place->country->trans[0]->title}}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="side-place-image-list">
                                            @if(isset($tplace->place))
                                            @foreach($tplace->place->medias AS $pm)
                                            @if(!$loop->first AND $loop->iteration<6)
                                            <li>
                                                <img class="lazy" data-src="https://s3.amazonaws.com/travooo-images2/th180/{{@$pm->url}}"
                                                    alt="photo" style="width:79px;height:75px;"></li>
                                            @endif
                                            @endforeach
                                            @endif
                                        </ul>
                                        <div class="side-place-bottom">
                                            <div class="side-follow-info">
                                                <b>{{ @count($tplace->place->followers) }}</b> @lang('home.following_this_place')
                                            </div>
                                            @if(Auth::check())
                                                <?php
                                                    $placefollow = \App\Models\Place\PlaceFollowers::where('users_id', $me->id)->where('places_id', $tplace->places_id)->get();
                                                ?>
                                                @if(count($placefollow) > 0)
                                                <button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_place_following('unfollow', {{$tplace->places_id}}, this)">Unfollow
                                                </button>
                                                @else
                                                <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following('follow', {{$tplace->places_id}}, this)">@lang('home.follow')
                                                </button>
                                                @endif
                                            @else
                                                <button type="button" class="btn btn-light-primary btn-bordered open-login">@lang('home.follow')</button>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="card__content tp_cities_div" style="display: none;cursor: pointer;">

                            <div class="post-block post-side-block" style="border: none">
                                <div class="post-side-inner top-place-animation-loader" style="padding: 0">
                                    <div class="side-place-block place-animation-side-block">
                                        <div class="place-animation-title">
                                            <div class="place-top-info-layer">
                                                <div class="place-top-info-wrap">
                                                    <div class="place-top-avatar-wrap animation post-animation-avatar"></div>
                                                    <div class="place-animation-info-txt">
                                                        <div class="place-top-name animation"></div>
                                                        <div class="place-info animation"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="place-animation-content">
                                            <div class="animation"></div><div class="animation"></div><div class="animation"></div><div class="animation"></div>
                                        </div>
                                        <div class="place-animation-footer">
                                            <div class="place-animation-count animation"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="post-side-inner top-place-animation-block d-none" style="padding: 0">
                                    @foreach($top_cities as $tcities)
                                    
                                        <div class="side-place-block">
                                            <div class="side-place-top">
                                                <div class="side-place-avatar-wrap">
                                                    <img class="lazy" data-src="https://s3.amazonaws.com/travooo-images2/th180/{{@$tcities->getMedias[0]->url}}"
                                                        alt="" style="width:46px;height:46px;">
                                                </div>
                                                <div class="side-place-txt">
                                                    <a class="side-place-name" target="_blank" style="white-space: normal"
                                                    href="{{ route('city.index', @$tcities->id) }}">{{@$tcities->trans[0]->title}}</a>
                                                    <div class="side-place-description">
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="side-place-image-list">
                                                @if(isset($tcities->getMedias))
                                                @foreach($tcities->getMedias AS $pm)
                                                @if(!$loop->first AND $loop->iteration<6)
                                                <li>
                                                    <img class="lazy" data-src="https://s3.amazonaws.com/travooo-images2/th180/{{@$pm->url}}"
                                                        alt="photo" style="width:79px;height:75px;"></li>
                                                @endif
                                                @endforeach
                                                @endif
                                            </ul>
                                            <div class="side-place-bottom">
                                                <div class="side-follow-info">
                                                    <b>{{ @count($tcities->followers) }}</b> @lang('home.following_this_place')
                                                </div>
                                                @if(Auth::check())
                                                    <?php
                                                        $placefollow = \App\Models\City\CitiesFollowers::where('users_id', $me->id)->where('cities_id', $tcities->id)->get();
                                                    ?>
                                                    @if(count($placefollow) > 0)
                                                    <button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_place_following('unfollow', {{$tcities->id}}, this)">Unfollow
                                                    </button>
                                                    @else
                                                    <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following('follow', {{$tcities->id}}, this)">@lang('home.follow')
                                                    </button>
                                                    @endif
                                                @else
                                                    <button type="button" class="btn btn-light-primary btn-bordered open-login">@lang('home.follow')</button>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    </div>
                @else
                    <div class="card mobile-hide">
                        <div class="card__header">
                            <h2 class="card__title">Top Places</h2><a class="card__link" href="{{url_with_locale('reports/list')}}">See All</a>
                        </div>
                        <div class="card__content" style="max-height: 300px;overflow-y: scroll;">

                            <div class="post-block post-side-block" style="border: none">
                                <div class="post-side-inner top-place-animation-loader" style="padding: 0">
                                    <div class="side-place-block place-animation-side-block">
                                        <div class="place-animation-title">
                                            <div class="place-top-info-layer">
                                                <div class="place-top-info-wrap">
                                                    <div class="place-top-avatar-wrap animation post-animation-avatar"></div>
                                                    <div class="place-animation-info-txt">
                                                        <div class="place-top-name animation"></div>
                                                        <div class="place-info animation"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="place-animation-content">
                                            <div class="animation"></div><div class="animation"></div><div class="animation"></div><div class="animation"></div>
                                        </div>
                                        <div class="place-animation-footer">
                                            <div class="place-animation-count animation"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="post-side-inner top-place-animation-block d-none" style="padding: 0">
                                    @foreach($top_places as $tplace)
                                    <div class="side-place-block">
                                        <div class="side-place-top">
                                            <div class="side-place-avatar-wrap">
                                                <img class="lazy" data-src="https://s3.amazonaws.com/travooo-images2/th180/{{@$tplace->place->medias[0]->url}}"
                                                    alt="" style="width:46px;height:46px;">
                                            </div>
                                            <div class="side-place-txt">
                                                <a class="side-place-name" style="white-space: normal"
                                                href="{{ route('place.index', @$tplace->place->id) }}">{{@$tplace->place->trans[0]->title}}</a>
                                                <div class="side-place-description">
                                                    <b>{{@do_placetype($tplace->place->place_type)}}</b> in <a
                                                        href="{{ route('city.index', @$tplace->place->city->id) }}">{{@$tplace->place->city->trans[0]->title}}</a>,
                                                    <a href="{{ route('country.index', @$tplace->place->country->id)}}">{{@$tplace->place->country->trans[0]->title}}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="side-place-image-list">
                                            @if(isset($tplace->place))
                                            @foreach($tplace->place->medias AS $pm)
                                            @if(!$loop->first AND $loop->iteration<6)
                                            <li>
                                                <img class="lazy" data-src="https://s3.amazonaws.com/travooo-images2/th180/{{@$pm->url}}"
                                                    alt="photo" style="width:79px;height:75px;"></li>
                                            @endif
                                            @endforeach
                                            @endif
                                        </ul>
                                        <div class="side-place-bottom">
                                            <div class="side-follow-info">
                                                <b>{{ @count($tplace->place->followers) }}</b> @lang('home.following_this_place')
                                            </div>
                                            @if(Auth::check())
                                                <?php
                                                    $placefollow = \App\Models\Place\PlaceFollowers::where('users_id', $me->id)->where('places_id', $tplace->places_id)->get();
                                                ?>
                                                @if(count($placefollow) > 0)
                                                <button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_place_following('unfollow', {{$tplace->places_id}}, this)">Unfollow
                                                </button>
                                                @else
                                                <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following('follow', {{$tplace->places_id}}, this)">@lang('home.follow')
                                                </button>
                                                @endif
                                            @else
                                                <button type="button" class="btn btn-light-primary btn-bordered open-login">@lang('home.follow')</button>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    </div>
                @endif
                <?php
                $is_hotel=$place->isHotel();
                ?>
                <div class="card" id="skySkannerCard">
                    <ul class="nav nav-tabs skyscanner-navs">
                        <li class="nav-item">
                            <a data-toggle="tab" href="#flights" class="nav-link mobile--flights @if( ! $is_hotel)  active  @endif">Flights</a>
                        </li>
                        <li class="nav-item" style="width: 50%;text-align: center;">
                            <a data-toggle="tab" href="#hotels" class="nav-link mobile--hotels @if($is_hotel)  active  @endif">Hotels</a>
                        </li>
                    </ul>
                    <div class="tab-content skyscanner-block">
                        <div id="flights" class="tab-pane fade @if(!$is_hotel) in active show @endif" style="padding:20px;">
                            <div class="user-list">
                                <div data-skyscanner-widget="SearchWidget"
                                     {{-- data-origin-geo-lookup="true" --}}
                                     data-origin-name = "'{{$myCity}}'"
                                     data-currency="USD"
                                     {{-- data-location-coords="{{$place->long}},{{$place->lat}}" --}}
                                     data-destination-name="
                                     @if(session('place_country') == 'place')
                                        '{{$place->city->transsingle->title}}'
                                     @else
                                        '{{$place->transsingle->title}}'
                                     @endif
                                     "
                                     data-target="_blank"
                                     data-button-colour="#db0000"
                                     data-flight-outbound-date="{{date("Y-n-d", strtotime('+1 day', time()))}}"
                                     data-flight-inbound-date="{{date("Y-n-d", strtotime('+8 day', time()))}}"></div>

                            </div>
                        </div>
                        <div id="hotels" class="tab-pane fade @if($is_hotel) in active show @endif" style="padding:20px;">
                            <div class="user-list">

                                <div data-skyscanner-widget="HotelSearchWidget"
                                     data-currency="USD"
                                     data-destination-phrase ="'{{$place->transsingle->title}}'"
                                     data-target="_blank"
                                     data-button-colour="#db0000"
                                     data-hotel-check-in-date="{{date("Y-n-d", strtotime('+1 day', time()))}}"
                                     data-hotel-check-out-date="{{date("Y-n-d", strtotime('+8 day', time()))}}"
                                     data-responsive="true"
                                     ></div>
                                {{-- <script src="https://widgets.skyscanner.net/widget-server/js/loader.js" async></script> --}}
                                <!-- DOCS https://www.partners.skyscanner.net/affiliates/widgets-documentation/hotel-search-widget -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="side-nav">
                    <a class="side-nav__link" href="{{route('page.privacy_policy')}}">Privacy</a>
                    <a class="side-nav__link" href="{{route('page.terms_of_service')}}">Terms</a>
                    <a class="side-nav__link" href="{{url('/')}}">Advertising</a>
                    <a class="side-nav__link" href="{{url('/')}}">Cookies</a>
                    <a class="side-nav__link" href="{{url('/')}}">More</a>
                </div>
                <div class="page-content__copyright">Travooo © 2017 - {{date('Y')}}</div>
                <button class="page-content__add-post-btn {{Auth::check()?'':'open-login'}} open-create-actions" type="button" style="position: fixed;{{Auth::check()?'bottom: 40px':'bottom: 104px'}};right: 40px;">
                    <svg class="icon icon--add">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#add')}}"></use>
                    </svg>Create
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-sidebar " id="modal-add_place_success" role="dialog" aria-labelledby="Add Place"
        style="overflow:auto;z-index:1600;">
        <div class="modal-dialog modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="height: 45px;">
                    <h5 style="padding-bottom: 10px;" class="modal-title w-100 text-center"><strong style="font-weight:500">Success</strong></h5>
                    <button type="button" class="close" style="color:#A7A7A7" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background: #FAFAFA;">
                    <div class="d-block text-center">
                        <img src="{{asset('assets2/image/i_was_here.png')}}" alt="Travooo" dir="auto">
                    </div>
                    <div style="margin-top: 20px;font-family: inherit;text-align: center;">
                        <strong style="font-weight:500">{{@$place->trans[0]->title}}</strong>
                        <span> has been added to your<br> trip plan </span>
                        <strong><span style="color:#71A0FF;    margin-left: 5px;font-weight:500" class="trip_name"></span></strong>
                    </div>
                    <div class="buttons" style="text-align: center;margin-top: 20px;">
                        <a class="btn-places"  data-dismiss="modal">Keep Discovering 
                        </a>
                        <a id="btnTripToPlace" class="btn-places">Go To Trip Plan</a>
                    </div>
                </div>                
            </div>
        </div>
</div>

<div class="modal modal-sidebar" role="dialog"  id="modal-text-post-popup">
    <button class="modal__close" type="button">
        <svg class="icon icon--close"><use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use></svg>
    </button>
    <div class="modal-dialog modal-lg" role="document">
    
        <div class="post_container">
        </div>
    </div>
</div>

@include('site.place2.partials.modal_like')

@if(Auth::check())
    @include('site.discussions._modals')
    @include('site.place2.partials._asking_information_box',['place_id'=>$place->id])
    @include('site.place2.partials._checkin-modal',['place'=>$place])

    @include('site.place2.partials.__modal_i_was_here_success',['place'=>$place])
    @include('site.place2.partials._modal_add_trip_plan',['place'=>$place])
    @include('site.place2.partials._modal_user_live',['place'=>$place,'live_users'=>$live_users])
    @include('site.home.new.partials._create-options-modal')
@endif

 @include('site.place2.partials._become_expert_modal',['place_id'=>$place->id])



<footer class="page-footer page__footer"></footer>

@if(!Auth::check())
@include('site/layouts/_non-loggedin-user-footer')   
@endif


<script src="{{ asset('assets/js/lightslider.min.js') }}"></script>
<script src="{{ asset('assets3/js/tether.min.js') }}"></script>
<script src="{{ asset('assets2/js/popper.min.js') }}"></script>
<script src="{{ asset('assets2/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets3/js/bootstrap-datepicker-extended.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets2/js/slick.min.js') }}"></script>
<script src="{{ asset('assets3/js/main.js?0.3.2') }}"></script>
<script src="{{ asset('assets2/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/js/lightgallery-all.min.js"></script> -->
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
<script src="{{ asset('assets3/js/jquery.barrating.min.js') }}"></script>
<script src="{{ asset('assets3/js/star.js') }}"></script>

<script src="{{ asset('assets2/js/summernote/summernote.js') }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/config.js') }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/tam-emoji.js?v='.time()) }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/textcomplete.js?v='.time()) }}"></script>
<script src="{{ asset('assets2/js/emoji/jquery.emojiFace.js?v=0.1') }}"></script>
<script src="{{ asset('assets2/js/skyloader.js') }}"></script>
<!-- <script src="{{ asset('assets3/js/jquery.sliderPro.min.js') }}"></script> -->
<script src="{{ asset('assets2/js/jquery-confirm/jquery-confirm.min.js') }}"></script>
<script src="{{ asset('assets2/js/lightbox2/src/js/lightbox.js') }}"></script>
<script data-cfasync="false" src="{{asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('assets2/js/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('assets2/js/posts-script.js?v=0.3') }}"></script>
@if(Auth::check())
    @include('site.home.partials._spam_dialog')
@endif
</body>
<div class='card-popup-helper'>
    @include('site.place2.partials._card_content_modal')
</div>
@include('site.place2._modals')
@include('site.home.partials.modal_comments_like')
<script>
     var flag_parent_post = false;
    mapboxgl.accessToken = '{{config('mapbox.token')}}';
    eval(map_var);
    var today_map, today_lat = {{ $place->lat }}, today_lng = {{ $place->lng }};

    function onClickCarouselCard(map_index, map, markers, lat, lng, obj){

        if(eval('tripPlanInsight'+map_index)){
            eval('tripPlanInsight'+map_index).setDataAndUpdate($(obj).data('insight'));
        }

        // set markers to default
        for (var i=0; i < markers.length; i++) {
            $(markers[i]._element).find('img').attr('src', '/assets2/image/road_marker2.png');
            $(markers[i]._element).height(14).width(14);
        }

        setCenter_usermap(map, markers, lat, lng, obj);
    }

    function setCenter_usermap(map, markers, lat, lng, obj)
    {
        for (var i=0; i < markers.length; i++) {

            if(lng.toFixed(4) === markers[i]._lngLat.lng.toFixed(4) &&
                lat.toFixed(4) === markers[i]._lngLat.lat.toFixed(4))
            {
                $(markers[i]._element).find('img').attr('src', $(obj).find('img').attr('src') + "?1");
                $(markers[i]._element).height(34).width(34);
            }
        }

        $("style").append(`
            img[src="` + $(obj).find('img').attr('src') + `?1"]{
                border-radius:17px;
                border:2px solid #fff !important;
            }
        `);

        map.setCenter([parseFloat(lng), parseFloat(lat)]);
    }

    function initMap() {
        eval(map_func);

        today_map = new mapboxgl.Map({
            container: 'today_map',
            style: 'mapbox://styles/mapbox/satellite-streets-v11'
        });

        today_map.setZoom(4);
        today_map.setCenter([parseFloat(today_lng), parseFloat(today_lat)]);
    }

</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap&language=en">
</script>

<script>

    $("header").css({"width": "100%", "position": "fixed"});
   
    var filter ='';
    var filter_array = [];
    var country_name = '{{isset($nationality->title)?"From ".$nationality->title:"Near you"}}';

    $('body').on('click', '.post_likes_modal', function (e) {
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('post.likes4modal') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);

                    $('#likesModal').find('.modal-body').html(result);
                    $('#likesModal').modal('show');
                });
            e.preventDefault();
        });


    $('body').on('click', '.plan_likes_modal', function (e) {
            planId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('trip_likes4modal') }}",
                data: {plan_id: planId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);

                    $('#likesModal').find('.modal-body').html(result);
                    $('#likesModal').modal('show');
                });
            e.preventDefault();
        });

    function update_newsfeed(what, who,filters=false) {
        var pagenum = parseInt($('#pageno').val());
        var parent_no = parseInt($('#is_parent').val());
        var rv = {};
        rv['user_id'] =who;
        rv['page'] =pagenum;
        if(parent_no == 1){
            rv['is_parent'] =  1;
        }
        for (var key in filters) 
        {
            if (filters[key] !== undefined)
                rv[key] = filters[key]
        }
        if(what=='discussion_replies_24hrs') {

            window._addPostBox.show('discussion');

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/discussion_replies_24hrs")}}',
                    data: rv,
                    success: function(data){
                        render_newsfeed(data, pagenum);
                    }
                });

        }else if(what=='about') {

            window._addPostBox.hide();

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/newsfeed_show_about")}}',
                    data: rv,
                    success: function(data){
                        render_newsfeed_about(data, pagenum);
                        $('#about_switch').click();
                        //render_newsfeed(JSON.stringify('no'), 0);


                        $("#nationalHoliday").lightSlider({
                        autoWidth:true,
                        pager: false,
                        controls: false,
                        slideMargin: 20,
                        loop: true
                      });
                      $("#weatherHourCarousel").lightSlider({
                      pager: false,
                      controls: true,
                      autoWidth:true,
                      slideMargin: 0,
                      onSliderLoad: function(el) {
                        $('.weather-carousel-control>a').on('click', function(e){
                          e.preventDefault();
                          if($(this).hasClass('slide-prev')){
                            el.goToPrevSlide();
                          }
                          if($(this).hasClass('slide-next')){
                            el.goToNextSlide();
                          }
                        })
                      }

                    });
                    }
                });

        }else if(what=='all') {
            window._addPostBox.show('main');

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/newsfeed_show_all")}}',
                    data: rv,
                    success: function(data){
                        render_newsfeed(data, pagenum);
                        var ptype = '{{session("place_country")}}';
                        if(ptype == 'place')
                            getPlaceinfo(pagenum);
                    }
                });

        }else if(what=='discussions') {
            window._addPostBox.show('discussion');

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/newsfeed_show_discussions")}}',
                    data: rv,
                    success: function(data){
                        render_newsfeed(data, pagenum);
                    }
                });

        }else if(what=='top') {

            window._addPostBox.show('main');

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/newsfeed_show_top")}}',
                    data:rv,
                    success: function(data){
                        render_newsfeed(data, pagenum);
                        var ptype = '{{session("place_country")}}';
                        if(ptype == 'place')
                            getPlaceinfo(pagenum);
                    }
                });

        }else if(what=='media') {

            window._addPostBox.show('main');

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/newsfeed_show_media")}}',
                    data: rv,
                    success: function(data){
                        render_newsfeed(data, pagenum);
                    }
                });

        }else if(what=='tripplan') {

            window._addPostBox.show('tripplan');

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/newsfeed_show_tripplan")}}',
                    data: rv,
                    success: function(data){
                        render_newsfeed(data, pagenum);
                    }
                });

        }else if(what=='travelmates') {

            window._addPostBox.show('travelmates');

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/newsfeed_show_travelmates")}}',
                    data: rv,
                    success: function(data){
                        render_newsfeed(data, pagenum);
                    }
                });

        }else if(what=='events') {

            window._addPostBox.hide();

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/newsfeed_show_events")}}',
                    data: rv,
                    success: function(data){
                        render_newsfeed(data, pagenum);
                    }
                });

        }else if(what=='reports') {

            window._addPostBox.show('reports');

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/newsfeed_show_reports")}}',
                    data:rv,
                    success: function(data){
                        render_newsfeed(data, pagenum);
                    }
                });

        }else if(what=='reviews') {

            window._addPostBox.show('reviews');

            $.ajax({
                    type: 'GET',
                    url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/newsfeed_show_reviews")}}',
                    data: rv,
                    success: function(data){
                        render_newsfeed(data, pagenum);
                    }
                });
        }
    }


    function render_newsfeed_about(data, pagenum) {

        data = JSON.parse(data);
                $('#newsfeed_content').html(data);
            $("#pagination").hide();

    }

    function render_newsfeed(data, pagenum) {
        data = JSON.parse(data);
        if(data.html != 'no'){
            if(pagenum == 1)
                $('#newsfeed_content').html(data.html);
            else
                $('#newsfeed_content').append(data.html);

            f_is_parent_post =  data.html.includes('parenting_post_class');
            if(f_is_parent_post){
                $('#is_parent').val(1);
                $('#pageno').val(1);
            }else{
                $('#pageno').val(pagenum + 1);
            }
            $("#pagination").fadeOut().delay(500).fadeIn();
            initMap();
        } else {
            if(pagenum == 1){
                $('.posts').find('.user-card').remove();
                $('#newsfeed_content').html('');
                $('#pageno').val(1);
                pos_ar=0;
                $("#pagination").hide();
                 $('.posts').append('<div class="user-card" search-tab="#searched"><div class="user-card__main"><div class="user-card__content"><div class="user-card__header"><a class="user-card__name" href="#" style="margin-left: calc(50% - 35px)">No result!</a></div></div></div></div>');
            }
            else{
                $("#pagination").hide();
                 $('.posts').append('<div class="user-card" search-tab="#searched"><div class="user-card__main"><div class="user-card__content"><div class="user-card__header"><a class="user-card__name" href="#" style="margin-left: calc(50% - 35px)">No more posts!</a></div></div></div></div>');
         
            }
               
        }

    }
    
    //for following of place
    function newsfeed_place_following(type, id, obj)
    {
        var followurl = "{{  route(session('place_country') . '.follow', ':user_id')}}";
        var unfollowurl = "{{  route(session('place_country') . '.unfollow', ':user_id')}}";
        if(type == "follow")
        {
            url = followurl.replace(':user_id', id);
        }
        else if(type == "unfollow")
        {
            url = unfollowurl.replace(':user_id', id);
        }
        $.ajax({
            method: "POST",
            url: url,
            data: {name: "test"}
        })
        .done(function (res) {
            if (res.success == true) {
                $(obj).parent().find("b").html(res.count);
                if(type == "follow")
                {
                    $(obj).replaceWith('<button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_place_following(\'unfollow\','+ id +', this)">Unfollow</button>');
                }
                else if(type == "unfollow")
                {
                    $(obj).replaceWith('<button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following(\'follow\','+ id +', this)">@lang('home.follow')</button>');
                }
            } else if (res.success == false) {

            }
        });
    }

    $(document).ready(function(){
       //top places and top cities toggle 

        $('.top_cities_btn').on('click',function(){
            $(this).parent().find('h2').removeClass('active');
            $(this).addClass('active');
            $('.tp_places_div').hide();
            $('.tp_cities_div').show();
        });
        $('.top_places_btn').on('click',function(){
            $(this).parent().find('h2').removeClass('active');
            $(this).addClass('active');
            $('.tp_places_div').show();
            $('.tp_cities_div').hide();
        });

        //scroll trip plans

        $('#scrollable-trip-plans').scroll(function() {
            if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                alert('Bottom reached!');
            }
        });

         $(document).on('click','.clickable_titles',function(){
            window.location=$(this).data("href");
            return false;
        });
        $('.country_cities_select').select2({
            width: 'resolve',
        });

        //post filter popover state
        $(document).on('click','.filter-btn',function(){
            let peopleValue = $(this).attr('people');
            let locationValue = $(this).attr('location');
            setTimeout(() => {
                $('.post-filter-popover-body').find(`input[data-type=people][value=${peopleValue}]`).addClass('active');
                $('.post-filter-popover-body').find(`input[data-type=location][value=${locationValue}]`).addClass('active')
            }, 300);
        });

        //post filter scripts
        $(document).on('change','.filter-arr', function() {
            $('#pageno').val(1);
            pos_ar=0;
            // $('.filter-'+$(this).data('type')).not(this).prop('checked', false);
            $('.filter-'+$(this).data('type')).not(this).removeClass('active');
            $(this).addClass('active');
            $('.filter-btn').attr($(this).data('type'), $(this).val());
            var tabattr = $(".posts-filter__link.posts-filter__link--active").attr('href');
            //$("#newsfeed_content").empty();
            
            filter = 'people_'+$(this).val();
                if($(this).val() == 0){
                    if(filter_array.hasOwnProperty($(this).data('type'))){
                        delete filter_array[$(this).data('type')];
                        
                        var temp_text = ''; 
                        for (var key in filter_array) 
                        {   if(key == 'people')
                                temp_text += 'People you follow /';
                            if(key == 'location')
                                temp_text += country_name+ ' /';
                        }

                        if(temp_text == ''){
                        $('.filter-btn').text('From anyone / From Anywhere');
                        }else{
                            $('.filter-btn').text( temp_text.slice(0, -1));
                        }
                    } 
                }else{
                    filter_array[$(this).data('type')] =$(this).val();
                    var temp_text = ''; 
                    for (var key in filter_array) 
                        {   if(key == 'people')
                                temp_text += 'People you follow /';
                            if(key == 'location')
                                temp_text += country_name+ ' /';
                            
                        }
                    if(temp_text == ''){
                        $('.filter-btn').text('From anyone /'+country_name);
                    }else{
                        $('.filter-btn').text( temp_text.slice(0, -1));
                    }
                    
                }
                $('#is_parent').val(0);
            update_newsfeed(tabattr, 0,filter_array);
            //$("#pagination").hide();
        });
        // $(document).on('change','.filter-location', function() {
        //     $('#pageno').val(1);
        //     pos_ar=0;
        //     $('.filter-location').not(this).prop('checked', false);
        //     var tabattr = $(".posts-filter__link.posts-filter__link--active").attr('href');
        //      filter = 'location_'+$(this).val();
        //     update_newsfeed(tabattr, 0,filter);
        // });
        // $(document).on('change','.filter-sort', function() {
        //     $('#pageno').val(1);
        //     pos_ar=0;
        //     $('.filter-sort').not(this).prop('checked', false);
        //     var tabattr = $(".posts-filter__link.posts-filter__link--active").attr('href');
        //      filter = 'sort_'+$(this).val();
        //     update_newsfeed(tabattr, 0,filter);
        // });
        // unique url post viewer 
        $('#modal-text-post-popup').on('show.bs.modal',function(e){
            $(".post_container").empty();
            $.ajax({
                url:"{{url('places/get-post-detail')}}",
                type:'POST',
                data:{'postId':e.relatedTarget.dataset['post_id']},
                success: function(result){
                $(".post_container").html(result);
                if (window.history.replaceState) {
                    //prevents browser from storing history with each change:
                    window.history.replaceState({}, null, "../../post-view/"+e.relatedTarget.dataset['post_id']);
                }

            }});

        });
        $('#modal-text-post-popup').on('hide.bs.modal',function(e){
            if (window.history.replaceState) {
                    window.history.replaceState({}, null, "../../place/{{$place->id}}");
                }
        });
        


        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $('.filter-btn').popover({
            html: true,
            container: '.filter-button',
            content: function () {          
                var content = $(this).attr("data-popover-content");
                return $(content).find(".popover-body, .popover").clone();
            }
        });
        //popoever cover image add trip plan
        $('.top-popup').popover({
            html: true,
            container: '.hero__link.popover-link',
            content: function () {          
                var content = $(this).attr("data-popover-content");
                return $(content).find(".popover-body, .popover").clone();
            }
        });
        var city_for_place_search = 0 ;
        //search place  by country 

        $('.input_for_place_name').select2({
            ajax: {
                url: "{{url('home/searchAllPOIs')}}",
                data: function (params) {
                    var query = {
                        q: params.term,
                    }
                    return query;
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
            }
        });

        $('body').on('click','.plans_id',function() {
            var id = $(this).attr('value');
            var type = '{{$place_type}}';

            window.open('/trip/plan/' + id + '?do=edit&add=' + '{{@$place->id}}&add-type=' + type, '_blank');
        });

        $(document).ready(function() {
            if (window.location.href.includes('#review')) {
                $('a.js-show-modal[href="#modal-review"]:first u').trigger('click');
            }
        })

        $('#checkin_time').datepicker({
            format: 'DD, d MM, yy',
        });
        $('.timepicker').timepicker();
        $(".datepicker_place").datepicker({
            format: 'd MM, yyyy',
        });


        @if(isset($place->id))
        var destSelect = $('#destination_id');

                var option = new Option('{{@$place->trans[0]->title}}', '{{$place->id}},place', true, true);
                destSelect.append(option).trigger('change');
               $('.ask-experts-block').find('.ask-experts-input').removeAttr('disabled')
                about_type = '{{$place->id}},place';
        @endif


        $('#trip_id').select2({
            dropdownParent: $('#askingPopup'),
            placeholder: '@lang('discussion.strings.your_trip_plan')',
            debug: true,
            containerCssClass: "select2-input",
            ajax: {
                url: '{{route('discussion.ajax_search_trips')}}',
                dataType: 'json',

            }
        });

        //fly to this place reworked

        var isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints > 0;

        if(isTouchDevice) {
            $("#skySkannerCard").append('<button class="skyScanner_mobilesearch_close" dir="auto"><i class="trav-close-icon" dir="auto"></i></button>');
            $(".skyScanner_mobilesearch_close").click(function() {
                $("#skySkannerCard").toggleClass('show');
            });
            $(".find--hotels").click(function() {
                $( ".mobile--hotels" ).click();
                $("#skySkannerCard").toggleClass('show');
            });
            $(".flyToLink").click(function() {
                $("#skySkannerCard").toggleClass('show');
                $( ".mobile--flights" ).click();
            });
        } else {
            $(".flyToLink, .find--hotels").click(function() {
                $('html, body').animate({
                    scrollTop: $("#skySkannerCard").offset().top
                }, 2000);
                $( "#skySkannerCard" ).addClass('skyScannerHighlight');
                setTimeout(() => {
                    $('#skySkannerCard').toggleClass('skyScannerHighlight skyScannerHighlightoff');
                }, 4000);
                    
            });
        } 
        //map init for checkin + popup intialization
        
        $('#checkin-modal').on('show.bs.modal',function(e){
           if(typeof e.relatedTarget.dataset.reference !='undefined')
                $('#fromPostBox').val(1);
            var map;
            var today_lat = {{ $place->lat }}, today_lng = {{ $place->lng }};

            map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mapbox/satellite-streets-v11'
            });

            map.on('dataloading', function () {
                map.resize();
            });

            var marker = new mapboxgl.Marker()
                .setLngLat([parseFloat(today_lng), parseFloat(today_lat)])
                .addTo(map);

            map.setZoom(10);
            map.setCenter([parseFloat(today_lng), parseFloat(today_lat)]);
        });
        $('.btn_liveCheckin').on('click',function(){
             var checkin_date ='';
                if ($('#checkin_time').val() == '') {
                toastr.error('Checkin Date is required');
                return false;
            } else {
                var d = new Date($('#checkin_time').val());
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
                if (month.length < 2) 
                    month = '0' + month;
                if (day.length < 2) 
                    day = '0' + day;

                checkin_date = [year, month, day].join('-');
            }
            $.ajax({
                method: "POST",
                url: "{{ route((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) . '.checkin', $place->id) }}",
                data: {name: "test",'checkin_time':checkin_date}
            }).done(function (res) {
                if(res.success){
                    toastr.success(res.message);
                    // $('#success-text').text(res.message);
                    $('#checkin-modal').modal('hide');
                    if($('#fromPostBox').val() ==1){
                        $('#checkinPost').val(res.checkin_id);
                    }
                }
               
            });
        });
        //I was here button 
        $('#iWasHereInBtn').on('click',function(){
             var checkin_date ='';
            
                var d = new Date(new Date().setDate(new Date().getDate()-8));
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
                if (month.length < 2) 
                    month = '0' + month;
                if (day.length < 2) 
                    day = '0' + day;
                checkin_date = [year, month, day].join('-');
            
            $.ajax({
                method: "POST",
                url: "{{ route((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) . '.checkin', $place->id) }}",
                data: {name: "test",'checkin_time':checkin_date}
            }).done(function (res) {
                if(res.success){
                    toastr.success(res.message);
                    $('#checkin-modal').modal('hide');
                }
               
            });
        });
        $('#iWasHere-sucess-modal').on('hidden.bs.modal', function (e) {
            // location.reload();
        });
        $('#modal-add_place_success').on('hidden.bs.modal', function (e) {
            // location.reload();
        });
        

        //minustes and hours calculator
        $('.hours').on('click',function(){
            total = parseInt($('#duration_hours').val())+parseInt($(this).val());
            if(total>=0)
                $('#duration_hours').val(total);
        });
        $('.minutes').on('click',function(){
            total = parseInt($('#duration_minutes').val())+parseInt($(this).val());
            if(total>=0)
                $('#duration_minutes').val(total);
        });
        //add to trip action
        $('body').on('click', '.doAddPlace', function (e) {
                var place_id = $('#places_real_id').val();
                var trip_id = $('#trip_real_id').val();
                var city_id = $('#cities_id').val();
                var version_id =  $('#trip_real_id').val();
                var budget = $("input[name=budget]").val();
                var duration_hours = $("#duration_hours").val();
                var duration_minutes = $("#duration_minutes").val();
                var place_date = $("#actual_place_date").val();
                var place_time = $("#actual_place_time").val();
                var known_for = '';
                if (place_date != '') {
                    var d = new Date(place_date);
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
                    if (month.length < 2) 
                        month = '0' + month;
                    if (day.length < 2) 
                        day = '0' + day;
                        place_date = [year, month, day].join('-');
                }
                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxAddPlaceToTrip')}}",
                    data: {"city_id": city_id, "trip_id": trip_id, "place_id": place_id, "version_id": version_id,
                    "budget": budget, "duration_hours": duration_hours, "duration_minutes": duration_minutes,
                    "date": place_date, "time": place_time, "known_for": known_for }
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            $('#btnTripToPlace').attr('href', '../../trip/plan/'+trip_id+'?do=edit');
                            $('#modal-add_place').modal('hide');                            
                            $('#modal-add_place_success').modal('show');
                        } else {

                        }

                    });
                e.preventDefault();
            });

        $('body').on('change','input[type=radio][name=budget_amount]',function() {
            $('.ck-button').css('background-color','#FCFCFC').css('color', '#000000');
            $(this).closest('div').css('background-color','#407FFD').css('color', '#fff');
            $('#budget').val($(this).val());
        }); 

        $('body').on('click','.actionbtnfollow',function(){
            var type = $(this).data('type');
            var elem = $(this);
            console.log(type);
            if($(this).data('type') == 'follow'){
                var followurl = "{{  route('profile.follow', ':user_id')}}";
                url = followurl.replace(':user_id', $(this).val());
            }else {
                var followurl = "{{  route('profile.unfolllow', ':user_id')}}";
                url = followurl.replace(':user_id', $(this).val());
            }
            $.ajax({
                method: "POST",
                url: url,
                data: {name: "test"},
                success:function(e){
                    console.log(type);
                    if(type == 'follow'){
                        elem.text('Unfollow');
                        elem.attr('data-type','unfollow');
                        elem.addClass('btn-unfollow');
                    }else {
                        elem.text('Follow');
                        elem.attr('data-type','follow');
                        elem.removeClass('btn-unfollow');
                    }
                }
            });
        });
        $('#add_message_popup').on('show.bs.modal',function(e){
            var imgSRc  = '<img style="width: 30px;padding-bottom: 5px;" src="'+e.relatedTarget.dataset.profile_picture+'">';
            $('#live_user_popup').modal('hide');
            $('#message').val('');
            $('#participants').val(e.relatedTarget.dataset.user_id);
            $('.live_user_name').text(e.relatedTarget.dataset.name);
            $('#user_avatar_for_image').empty();
            $('#user_avatar_for_image').html(imgSRc);


        });
        $('#btnSendMsg').on('click',function(){
            if($('#messagee').val() == '')
                return false;
            var message = $('#message').val();
            var participants = $('#participants').val();
            $.ajax({
                method: "POST",
                url: './../../chat/send',
                data: {chat_conversation_id: 0,participants:participants,message:message},
                success:function(e){
                    toastr.success('Your message has been sent');
                    $('#add_message_popup').modal('hide');
                }
            });

        });

    });
    $(window).on('load', function() {
        var place_id_for_media =  '{{$place->id}}';
        var ptype = '{{session("place_country")}}';
        if(ptype == 'place')
            getMediaFor(place_id_for_media);

    });
    function getMediaFor(placeId){
        var is_new = false;
        var param = window.location.search.substr(1).split("&").forEach(function (item) {
          tmp = item.split("=");
          if(tmp[0] == 'is_new' && tmp[1] ==1){
            is_new = true;
          }
        });
        var place_id_for_media =  '{{count($place->getMedias)}}';
        if(is_new || place_id_for_media==0){
            $.ajax({
                method: "POST",
                url: "{{url('places/ajax_get_poi_media')}}",
                data: {
                    place_id: placeId,
                    'is_new':1
                }
            })
                .done(function (res) {
                    if(Object.keys(res).length > 0) {
                        toastr.success('Place Media has been fetched. Please refresh the page');
                    }
            });
        } 
    }
    $(document).ready(function () {
        document.textcompleteUrl = "{{ url('reports/get_search_info') }}";
        // Relative path to emojis
        document.emojiSource = "{{ asset('plugins/summernote-master/tam-emoji/img') }}";
        $("#createPostTxt").summernote({
        airMode: false,
        focus: false,
        disableDragAndDrop: true,
        placeholder: 'Speak your mind, {{Auth::check()?Auth::user()->name:''}}',
        toolbar: [
            ['insert', ['emoji']],
            ['custom', ['textComplate']],
        ],
        callbacks: {
            onFocus: function(e) {
                $(e.target).parent('.note-editing-area').find(".note-placeholder").removeClass('custom-placeholder');
                $(e.target).closest('#createPostTxt').find('.post-create-controls').removeClass('d-none');
            },
            onInit: function(e) {
                e.editingArea.find(".note-placeholder").addClass('custom-placeholder');
            }
        }
    });

        // celsius to fahrenhieght
        $('.celsius_control').on('click',function(){
            var type = $(this).data('type');
            $('.celsius_control').removeClass('font-weight-bold');
            curr_temp = $('.temprature-pointer').data('val');
            $(this).addClass('font-weight-bold');
            if(type == 'c'){
                cToFahr = curr_temp * 9 / 5 + 32;
                $('.temprature-pointer').text(cToFahr);
            }else{
                fToCel = (curr_temp - 32) * 5 / 9;
                $('.temprature-pointer').text(fToCel);
            }
        });

        setTimeout(function () {
            $('.travelmates-user-list-loader').remove()
            $('.travelmates-user-list').removeClass('d-none')

            $('.place-aside-experts-loader').remove()
            $('.place-aside-experts-block').removeClass('d-none')

            $('.top-place-animation-loader').remove()
            $('.top-place-animation-block').removeClass('d-none')
        }, 1000);


        $(document).on('click', 'a.review-edit', function() {
            var reviewId = $(this).attr('review-id');
            var text = $('div#' + reviewId + '.comment div.comment__text').text();

            $('div#' + reviewId + '.comment div.comment__text').replaceWith('<textarea>' + text + '</textarea>');
            $(this).replaceWith('<a href="#" review-id="' + reviewId + '" class="review-action review-save">save</a>')

        });

        $(document).on('click', 'a.review-save', function() {
            var reviewId = $(this).attr('review-id');
            var text = $('div#' + reviewId + '.comment').find('textarea').val().replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
            var self = this;

            $.ajax({
                method: "POST",
                url: '{{route('place.ajax_edit_review')}}/' + reviewId,
                data: {text: text}
            })
            .done(function (result) {
                if (result.success) {
                    $('div#' + reviewId + '.comment').find('textarea').replaceWith('<div class="comment__text" style="overflow-wrap: break-word;margin-top:10px" dir="auto">' + text + '</div>')
                    $(self).replaceWith('<a href="#" review-id="' + reviewId + '" class="review-action review-edit">edit</a>');
                }
});
        });

        $(document).on('click', '.morelink', function(){
            var moretext = "see more";
            var lesstext = "see less";
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });

        $(".about-modal__link").click(function() {
            if( $(this).hasClass('about-modal__link--active') )
                return;

            $(".about-modal__link").removeClass('about-modal__link--active');
            $(this).addClass('about-modal__link--active');

            var attr = $(this).attr('id').split('-');
            $(".about-modal__content").find('[id^=box-]').hide();
            $(".about-modal__content").find( '#box-' + attr[1] ).fadeIn();
        });

        $(".posts-filter__link").click(function(e) {

            // if click on already active tab
            if( $(this).hasClass('posts-filter__link--active') )
                return false;

            $('.posts').find('[search-tab]').remove();
            $('#mainSearchInput').val('');
            $(this).addClass('posts-filter__link--active').siblings().removeClass('posts-filter__link--active');
            var sec = $(this).attr('href');

            if(sec!='about') {
                $('#pagination').fadeIn();
            }
            $('#pageno').val(1);
            $('#is_parent').val(0);

            $('#newsfeed_content').html('');



            e.preventDefault();
        });

        $('.picture-card__img').on('click',function(){

            var type = $(this).data('type');
            $(".posts-filter__link").removeClass("posts-filter__link--active");
            $('a[href^="'+type+'"]').click();
            $('a[href^="'+type+'"]').addClass('posts-filter__link--active');


        })
        
        $('.review_clk_btn').on('click',function(){

            var type = $(this).data('type');
            $(".posts-filter__link").removeClass("posts-filter__link--active");
            $('a[href^="'+type+'"]').click();
            $('a[href^="'+type+'"]').addClass('posts-filter__link--active');


        })

        $('#feedloader').on('inview', function(event, isInView) {
            setTimeout(function(){ 
                if (isInView) {
                    var tabattr = $(".posts-filter__link.posts-filter__link--active").last().attr('href');
                    if(tabattr == 'about')
                        $('.filter-div').hide();
                    else
                    $('.filter-div').show();
                
                    if(tabattr !='top' && tabattr !='all')
                        $('.sorting-filter-div').show();
                    else
                        $('.sorting-filter-div').hide();
                    update_newsfeed(tabattr, 0,filter_array);
                }
            }, 1000);
        
        });
        $('img').on('inview', function(event, isInView) {
            if (isInView && $(this).attr('src') == undefined) {
                $(this).attr('src', $(this).attr('data-src'));
            }
        });

        $.ajax({
                method: "POST",
                url: '{{route(session("place_country") . ".ajax_check_event", $place->id)}}',
                data: {data:'event'}
            })
                .done(function (res) {
                    result = JSON.parse(res);
                    $(".picture-cards__item.event").find(".picture-card__number").html(result.cnt);
                    $("#modal-events").find(".events__items").html(result.data);
                });

    });
</script>
@include('site.place2._scripts')
@include('site.place2.partials._comments_scripts')
@include('site.place2.partials._add_post_script')
@include('site.layouts._footer-scripts')
<script>
    $("*").not("script").not("style").not("link").attr("dir", "auto");
    $("*").not("script").not("style").not("link").each(function(){
        if(/[\u0600-\u06FF]/.test($(this).text()) && !/</.test($(this).html()))
            $(this).attr("dir", "");
    });
    $(".ql-direction-rtl").attr("dir", "rtl");
    $('body').on('DOMNodeInserted', function(e) {
        $(e.target).find("*").attr('dir', "auto");
        $(e.target).find("*").not("script").not("style").not("link").each(function(){
            if(/[\u0600-\u06FF]/.test($(this).text()) && !/</.test($(this).html()))
                $(this).attr("dir", "");
        });
    });


    $(document).on('click', '.tab-item[data-tab]', function (e) {
        $(this).parent('.post-tip-tab').children('.tab-item').removeClass('active-tab');
        $(this).addClass('active-tab');
    });

    $(document).on('click', '.place-about-main-tabs .side-ttl', function() {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
    });

    $(document).on('click', '[data-tab="dangers"]', function (e) {
        $('[data-content=dangers]').css('display', 'block');
        $('[data-content=indexes]').css('display', 'none');
        e.preventDefault();
    });
    $(document).on('click', '[data-tab="indexes"]', function (e) {
        $('[data-content=dangers]').css('display', 'none');
        $('[data-content=indexes]').css('display', 'block');
        e.preventDefault();
    });
    $(document).on('click', '[data-tab="etiquette"]', function (e) {
        $('[data-content=restrictions]').css('display', 'none');
        $('[data-content=etiquette]').css('display', 'block');
        e.preventDefault();
    });
    $(document).on('click', '[data-tab="restrictions"]', function (e) {
        $('[data-content=restrictions]').css('display', 'block');
        $('[data-content=etiquette]').css('display', 'none');
        e.preventDefault();
    });
    $(document).on('click', '[data-tab="packing_tips"]', function (e) {
        $('[data-content=packing_tips]').css('display', 'block');
        $('[data-content=daily_costs]').css('display', 'none');
        e.preventDefault();
    });
    $(document).on('click', '[data-tab="daily_costs"]', function (e) {
        $('[data-content=packing_tips]').css('display', 'none');
        $('[data-content=daily_costs]').css('display', 'block');
        e.preventDefault();
    });

    $(document).on('click', '[data-tab="atmTxt"]', function (e) {
        $('#atmTxt').css('display', 'block');
        $('#wifiTxt').css('display', 'none');
        $('#socketTxt').css('display', 'none');
        $('#hotelTxt').css('display', 'none');
        e.preventDefault();
    });

    $(document).on('click', '[data-tab="wifiTxt"]', function (e) {
        $('#atmTxt').css('display', 'none');
        $('#wifiTxt').css('display', 'block');
        $('#socketTxt').css('display', 'none');
        $('#hotelTxt').css('display', 'none');
        e.preventDefault();
    });

    $(document).on('click', '[data-tab="socketTxt"]', function (e) {
        $('#atmTxt').css('display', 'none');
        $('#wifiTxt').css('display', 'none');
        $('#socketTxt').css('display', 'block');
        $('#hotelTxt').css('display', 'none');
        e.preventDefault();
    });

    $(document).on('click', '[data-tab="hotelTxt"]', function (e) {
        $('#atmTxt').css('display', 'none');
        $('#wifiTxt').css('display', 'none');
        $('#socketTxt').css('display', 'none');
        $('#hotelTxt').css('display', 'block');
        e.preventDefault();
    });

    $(document).on('click', 'a.bpk-button', function(e){
        @if(isset($referrer_is_there) && $referrer_is_there)
            e.stopPropagation();
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "{{url('/ajax_track_ref')}}",
                data: { ref: {{$referrer_is_there}}}
            })
                .done(function( msg ) {
                    var handoffUrl = $(e.currentTarget).attr('href');
                    window.location.href = handoffUrl;
                });
        @endif

    });
    var array_pagenum = [0,1,2,3,4,5,6,7,8];
    shuffle(array_pagenum);
    var pos_ar = 0;
    function getPlaceinfo(page){
        if(pos_ar ==9)
            return;
        page = array_pagenum[pos_ar];
        pos_ar++;
        // $.ajax({
        //     type: 'GET',
        //     url: '{{url((session("place_country") == "place" ? session("place_country")."s" : session("place_country")) ."/".$place->id."/get-place-about")}}',
        //     data: {  pagnum: page },
        //     success: function(data){
        //         console.log(data);
        //         data = JSON.parse(data);
        //         $('#newsfeed_content').append(data.html);
        //     }
        // });
    }
    function shuffle(arra1) {
        var ctr = arra1.length, temp, index;

        // While there are elements in the array
            while (ctr > 0) {
        // Pick a random index
                index = Math.floor(Math.random() * ctr);
        // Decrease ctr by 1
                ctr--;
        // And swap the last element with it
            temp = arra1[ctr];
            arra1[ctr] = arra1[index];
            arra1[index] = temp;
    }
    return arra1;
}

</script>
<script>
var div_top = $('.mobile-tabs-only').offset().top;
$(window).scroll(function() {
    var window_top = $(window).scrollTop() - 0;
    if (window_top > div_top) {
        if (!$('.mobile-tabs-only').is('.sticky')) {
            $('.mobile-tabs-only').addClass('sticky');
        }
    } else {
        $('.mobile-tabs-only').removeClass('sticky');
    }
});
</script>
<script>
// Hide Header on on scroll down
var didScrollTab;
var lastScrollTopTab = 0;
var deltaTab = 5;
var navbarHeightTab = $('.mobile-tabs-only').outerHeight();

$(window).scroll(function(event){
    didScrollTab = true;
});

setInterval(function() {
    if (didScrollTab) {
        hasScrolledTab();
        didScrollTab = false;
    }
});

function hasScrolledTab() {
    var st = $(this).scrollTop();
    
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTopTab - st) <= deltaTab)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st >lastScrollTopTab && st > navbarHeightTab){
        // Scroll Down
        $('.mobile-tabs-only').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('.mobile-tabs-only').removeClass('nav-up').addClass('nav-down');
        }
    }   
    lastScrollTopTab = st;
}


    $(document).ready(function () {
        $(".btn-details").click(function() {
        $('html,body').animate({
            scrollTop: $(".page-content__inner").offset().top -50},
        'slow');
        setTimeout(function() { 
            $(".redirect-scroll").click()
        }, 1000);
    });
});
</script>
</html>
