@php

use App\Models\Place\Place;
use App\Models\Place\PlaceFollowers;
use App\Models\User\UsersFriends;
use App\Models\UsersFollowers\UsersFollowers;use Carbon\Carbon;


$places = [];
$locations = collect([]);

if(Auth::check()){
    $my_following = Auth::user()->followedPlaces->pluck('places_id');
        // get city places 
    $tr_places = Place::where('cities_id',$city)->pluck('id')->toArray();
    if(!empty($tr_places)){
        $places = PlaceFollowers::whereIn('places_id',$places)->whereNotIn('places_id' ,$my_following)->whereDate('created_at', '>', Carbon::today()->subDays(90))->pluck('places_id')->toArray();
    }

    $post_data = [
        'places' => $places,
    ];

    $places = Place::whereIn('id', $places)->get();
    if(count($places->pluck('id')) > 3){
        $locations = $places->map(function($place) {
            $followersCount = count($place->followers);

                return [
                    'id' => $place->id,
                    'title' => @$place->transsingle->title,
                    'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
                    'img' => check_place_photo(@$place, 700),
                    'followers' => $followersCount > 1000 ? floor($followersCount/1000) . 'K' : $followersCount,
                    'type' => 'place',
                    'url' => url('place/'.$place->id),
                    'city' => @$place->city->transsingle->title,
                    'placeType' => @$place->place_type,
                    'followersList' => $place->followers
                ];
            });
    }


}

$id = str_random(5);

@endphp
@if($locations->count())
<div class="post-block">
    <div class="post-side-top">
        <h3 class="side-ttl"> Trending Places <span class="count">{{ $locations->count() }}</span></h3>
        <div class="side-right-control">
            <a href="#" id="places-you-might-like-slider-prev-{{$id}}" class="slide-link places-you-might-like-slider-prev"><i class="trav-angle-left"></i></a>
            <a href="#" id="places-you-might-like-slider-next-{{$id}}" class="slide-link places-you-might-like-slider-next"><i class="trav-angle-right"></i></a>
        </div>
    </div>
    <div class="post-side-inner">
        <div class="post-slide-wrap slide-hide-right-margin">
            <div class="lSSlideOuter ">
                <div class="lSSlideWrapper usingCss" style="transition-duration: 400ms; transition-timing-function: ease;">
                    <ul id="places-you-might-like-slider-{{$id}}" class="post-slider places-you-might-like-slider lightSlider lSSlide lsGrab" style="width: 1480px; height: 337px; padding-bottom: 0%; transform: translate3d(0px, 0px, 0px);">
                        @foreach($locations as $location)
                        <li class="lslide active" style="margin-right: 20px;">
                            <div class="post-popular-inner">
                                <div class="img-wrap">
                                    <a style="text-decoration: none;" href="{{ $location['url'] }}">
                                        <img style="background-image: url({{ $location['img'] }})" src="{{ $location['img'] }}" alt="{{ $location['title'] }}">
                                    </a>
                                </div>
                                <div class="pop-txt">
                                <span class="location-badge"  style="background: #4080ff;">
                                    <i class="far fa-plus-square follow_place" style="cursor: pointer" data-place_id="{{ @$location['id']}}" data-type="follow"></i>
                                </span>
                                    <h5><a style="text-decoration: none;" href="{{ $location['url'] }}">{{ $location['title'] }}</a></h5>
                                    <div class="desc">
                                        <span>{{ do_placetype(@$location['placeType']) }}</span>
                                        {{ $location['city'] ? ' in ' . $location['city'] : '' }}
                                    </div>
                                    <div class="reactions">
                                        <button class="post__comment-btn">
                                            <div class="user-list">
                                                <div class="user-list__item">
                                                    @foreach($location['followersList']->take(2) as $follower)
                                                    <div class="user-list__user">
                                                        <img class="user-list__avatar"
                                                             src="{{check_profile_picture($follower->user->profile_picture)}}"
                                                             alt="{{ $follower->user->name }}"
                                                             title="{{ $follower->user->name }}"
                                                             role="presentation">
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <span><strong>{{ $location['followers'] }}</strong> Talking about this</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<script>
    $(document).ready(function(){
        // Post type - shared place, slider
        var homeRecommendedePlacesSlider = $('#places-you-might-like-slider-{{$id}}').lightSlider({
            autoWidth: true,
            slideMargin: 20,
            pager: false,
            controls: false
        });

        $('#places-you-might-like-slider-prev-{{$id}}').click(function(e){
            e.preventDefault();
            homeRecommendedePlacesSlider.goToPrevSlide();
        });
        $('#places-you-might-like-slider-next-{{$id}}').click(function(e){
            e.preventDefault();
            homeRecommendedePlacesSlider.goToNextSlide();
        });
    })
</script>