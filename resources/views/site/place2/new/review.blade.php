<?php
$review = \App\Models\Reviews\Reviews::find($post['variable']);
$variable = $post['variable'];
$post_type = $post['type'];
$avg = 0;
$total = 0;
if(isset($review->place)){
    $avg = \App\Models\Reviews\Reviews::where('places_id',$review->places_id)->avg('score');
    $total = $review->place->reviews()->count();
}
$followers = [];
$me = Auth::user();
if(Auth::check()){
$following = \App\Models\User\User::find($me->id);
foreach($following->get_followers as $f):
    $followers[] = $f->followers_id;
endforeach;
}
?>
@if(is_object($review) && isset($review->place->id))
    @php
        $uniqueurl = url('review/'. @$review->author->username, @$review->id);
    @endphp

    <div class="post-block post-block-review" >
        <div class="post-top-info-layer" >
            <div class="post-top-info-wrap" >
                <div class="post-top-avatar-wrap" >
                    @if(isset($review))
                        <img src="{{check_profile_picture(@$review->author->profile_picture)}}" alt="" >
                    @else 
                    <img src="{{$review->google_profile_photo_url ? $review->google_profile_photo_url : check_profile_picture('dummy')}} " alt="" >

                    @endif
                </div>
                <div class="post-top-info-txt" >
                    <div class="post-top-name" >
                        <a class="post-name-link" @if(isset($review->author->id)) href="{{url('profile/'.@$review->author->id)}}" @endif >{{$review->google_author_name ? $review->google_author_name : @$review->author->name}}</a>{!! get_exp_icon(@$review->author) !!}
                    </div>
                    <div class="post-info" >
                        <a href="{{$uniqueurl}}" target="_blank" class="post-title-link"> Reviewed </a>
                        <a href="{{ route('place.index', @$review->place->id) }}" class="link-place" >{{@$review->place->transsingle->title}}</a>
                        <a href="{{$uniqueurl}}" target="_blank" class="post-title-link">     
                        <span class="review-rating">{{@ (int) $review->score}}  <i class="trav-star-icon" ></i></span>  {{$review->google_time ? diffForHumans(date("Y-m-d", $review->google_time)) : diffForHumans($review->created_at)}}
                        </a>
                       
                    </div>
                </div>
            </div>
            <div class="post-top-info-action" dir="auto">
                <div class="dropdown" dir="auto">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"      aria-expanded="false" dir="auto">
                        <i class="trav-angle-down" dir="auto"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                        @if(Auth::check() && Auth::user()->id != @$review->author->id)
                            @if(in_array(@$review->author->id,$followers))
                                <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ @$review->author->id}}" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                        <i class="trav-user-plus-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                        <p dir="auto"><b dir="auto">Unfollow {{ @$review->author->name ?? 'User' }}</b></p>
                                        <p dir="auto">Stop seeing posts from {{ @$review->author->name ?? 'User' }}</p>
                                    </div>
                                </a>
                            @endif
                        @endif
                        <a class="dropdown-item" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$variable}}" data-toggle="modal" data-id="{{$variable}}" data-type="review">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-share-icon" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Share</b></p>
                                <p dir="auto">Spread the word</p>
                            </div>
                        </a>
                        <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-link" dir="auto"></i>
                            </span>
                            <div class="drop-txt">
                                <p dir="auto"><b dir="auto">Copy Link</b></p>
                                <p dir="auto">Paste the link anywhere you want</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#sendToFriendModal" data-id="review_{{$review->id}}" data-toggle="modal" data-target="" dir="auto">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-share-on-travo-icon" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                <p dir="auto">Share with your friends</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#spamReportDlgNew" data-toggle="modal" data-target="#" dir="auto">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-flag-icon-o" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Report</b></p>
                                <p dir="auto">Help us understand</p>
                            </div>
                        </a>
                        @if(Auth::check() && Auth::user()->id == @$review->author->id)
                            <a class="dropdown-item" data-toggle="modal" data-type="review" data-element = "review_{{$review->id}}" href="#deletePostNew" data-id="{{$review->id}}">
                                <span class="icon-wrap">
                                <i class="far fa-trash-alt"></i>
                                </span>
                                <div class="drop-txt">
                                    <p><b style="color: red;">Delete</b></p>
                                    <p>Delete this post forever</p>
                                </div>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="post-txt-wrap">
            <div>
                <span class="less-content disc-ml-content">{{substr($review->text,0, 200) }}
                    @if(strlen($review->text)>200)
                        <span class="moreellipses">...</span>
                        <a href="javascript:;" class="read-more-link">More</a>
                    @endif
            </span>
                <span class="more-content disc-ml-content" style="display: none;">{{ $review->text }}<a href="javascript:;" class="read-less-link">Less</a></span>
         
                </div>
        </div>
        <div class="post-image-container post-follow-container wide" >
            <!-- New elements -->
            
           
            <!-- New element END -->
           
        </div>
        @php 
        $media_array = $review->medias;
        // dd($media_array);
        $post_counter= 0;
       @endphp
        <div class="post-image-container post-follow-container wide">
            @if(count($media_array) == 0) 
                {{-- @if(isset($review->place->medias[0]->url))
                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;" >
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;" >
                            <a href="https://s3.amazonaws.com/travooo-images2/{{@$review->place->medias[0]->url}}" data-lightbox="media__post210172" >
                                <img src="https://s3.amazonaws.com/travooo-images2/{{@$review->place->medias[0]->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                            </a>
                        </li>
                    </ul>
                @endif --}}
            @elseif(count($media_array) == 1)
                @php 
                    $post_object = $media_array[0];
                    $file_url = $post_object->url;
                    $file_url_array = explode(".", $file_url);
                    $ext = end($file_url_array);
                    $allowed_video = array('mp4');
                @endphp
                <!-- New element -->
                <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                    @if(in_array($ext, $allowed_video))
                        <li style="padding: 0;position: relative;margin:0 0 0 0;overflow: hidden;">
                            <video width="100%" height="auto" controls="">
                                <source src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <a href="javascript:;" class="v-play-btn play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </li>
                    @else
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;">
                            <a href="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" data-lightbox="media__post210172">
                                <img src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                            </a>
                        </li>
                    @endif
                </ul>

            @elseif(count($media_array) == 2)
                <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                    @foreach ($media_array as $post_object)
                        @php
                            $file_url = $post_object->url;
                            $file_url_array = explode(".", $file_url);
                            $ext = end($file_url_array);
                            $allowed_video = array('mp4');
                        @endphp
                        @if(in_array($ext, $allowed_video))
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                <video width="100%" height="auto" >
                                    <source src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                                <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                            </li>
                        @else
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                <a href="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" data-lightbox="media__post199366">
                                    <img src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                                </a>
                            </li>
                        @endif
                    @endforeach
                   
                    
                </ul>
            @elseif(count($media_array)  >2)
                <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;">
                    @foreach ($media_array as $post_object)
                        @php
                            $file_url = $post_object->url;
                            $file_url_array = explode(".", $file_url);
                            $ext = end($file_url_array);
                            $allowed_video = array('mp4');
                            $post_counter++;
                        @endphp
                        <!-- New element -->
                            @if(in_array($ext, $allowed_video))
                                @if( $post_counter  <=3)
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                        <video width="100%" >
                                            <source src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    </li>
                                @endif
                            @else
                                @if( $post_counter  <=3)
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                            @if(count($media_array)>3 &&  $post_counter ==3)
                                                <a class="more-photos" href="" data-lightbox="media__post199366">{{count($media_array) -3 . ' More Photos'}}</a>
                                            @endif
                                                <a  href="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" data-lightbox="media__post199366">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                                                </a>
                                        </li>
                                @endif
                            @endif
                        <!-- New element END -->
                    @endforeach
                </ul>
            @endif

             <!-- New Elements - Review-bottom -->
             {{-- <div class="review-inner-layer review-inner-content" >
                <div class="review-block" datesort="1600247687" scoresort="5.0" >
                    <div class="review-top" >
                        <div class="top-main" >
                            <div class="location-icon" >
                                <i class="trav-set-location-icon" ></i>
                            </div>
                            <div class="review-txt" >
                                <a style="text-decoration: none;" href="/place/{{@$review->place->id}}" target="_blank"><h3 class="review-ttl" >{{@$review->place->transsingle->title}}</h3></a>
                                <div class="sub-txt" >
                                    <div class="rate-label" >
                                        <b >{{round(@$avg, 1)}}</b>
                                        <i class="trav-star-icon" ></i>
                                    </div>&nbsp;
                                    <span >from <b>{{$total}} reviews</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="btn-wrap" >
                            @if(Auth::check())
                            @php
                                 $placefollow = \App\Models\Place\PlaceFollowers::where('users_id', Auth::user()->id)->where('places_id', @$review->place->id)->get();
                            @endphp
                            @if(count($placefollow) > 0)
                                <button class="btn btn-light-grey btn-bordered place-follow-btn" onclick="newsfeed_place_following('unfollow', {{@$review->place->id}}, this)" data-id="4263691" data-type="follow" >
                                    Unfollow </button>
                            @else
                                <button class="btn btn-light-primary btn-bordered place-follow-btn" onclick="newsfeed_place_following('follow', {{@$review->place->id}}, this)" data-id="4263691" data-type="follow" >
                                Follow </button>
                            @endif
                            @else
                                <button type="button" class="btn btn-light-primary btn-bordered open-login">@lang('home.follow')</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div> --}}
            <!-- New Elements - Review bottom END -->

        </div>

@php
 $flag = false;
 if(Auth::check()){
 $user_like = $review->updownvotes->where('users_id',Auth::user()->id)->where('vote_type',1)->count();
   if( $user_like)
        $flag = true;
 }
       

@endphp
        <div class="post-footer-info" >
            <div class="post-foot-block post-reaction">
                <span class="post_like_button @if($flag) liked @endif ">
                    <a href="#" class="btn-report-like" @if($flag) data-value="0" @else data-value="1" @endif  data-type="review" id="{{$variable}}">
                        <i class="trav-heart-fill-icon"></i>
                    </a>
                </span>
                <span id="review_like_count_{{$variable}}" >
                    <b class="review-lieks-count">{{count($review->updownvotes->where('vote_type',1))}}</b> <a  data-toggle="modal" data-type="review" data-id="{{$variable}}">Was this helpful?</a>
                </span>
            </div>
            <div class="post-foot-block ml-auto" >
                <span class="post_share_button" id="{{$variable }}" >
                    <a  href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$variable}}" data-toggle="modal" data-id="{{$variable}}" data-type="review" >
                        <i class="trav-share-icon" ></i>
                    </a>
                </span>
                <span id="post_share_count_{{$variable }}" >
                    <a href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$variable}}" data-toggle="modal" data-id="{{$variable}}" data-type="review"  ><strong >0</strong> Shares</a>
                </span>
            </div>
        </div>
        <div class="post-comment-wrapper"></div>
    </div>
@endif
<script>
    $(document).ready(function(){
        // Post text tag popover
        var tagProfileIDs = '{{isset($tagProfileIDs) ? $tagProfileIDs : ""}}';
        var profileids = tagProfileIDs.split(',');
        if (profileids.length > 0) {
            $.each(profileids, function (i, v) {
                $(`.post-text-tag-${v}`).popover({
                    html: true,
                    content: $(`#popover-content-${v}`).html(),
                    template: '<div class="popover bottom tagging-popover" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
            })
        }
        $(document).on('click', ".read-more-link", function () {
            $(this).closest('.post-txt-wrap').find('.less-content').hide()
            $(this).closest('.post-txt-wrap').find('.more-content').show()
            $(this).hide()
        });
        $(document).on('click', ".read-less-link", function () {
            $(this).closest('.more-content').hide()
            $(this).closest('.post-txt-wrap').find('.less-content').show()
            $(this).closest('.post-txt-wrap').find('.read-more-link').show()
        });

        // Post type - shared place, slider
        $('.shared-place-slider').lightSlider({
            autoWidth: true,
            slideMargin: 22,
            pager: false,
            controls: false,
        });
        $('.play-btn, .video-play-btn').click(function(){
            $(this).siblings('video').get(0).play();
            $(this).toggleClass('hide');
        });
    })
</script>
