<?php
$post_type = $post['type'];
$variable = $post['variable'];
$discussion = \App\Models\Discussion\Discussion::find($post['variable']);
$flag = false;
if(isset($discussion->experts) && count($discussion->experts)>0){
    foreach ($discussion->experts as $key => $value) {
    if(Auth::check() && $value->expert_id == Auth::user()->id)
            $flag = true;
    }
}

$followers = [];
    if(Auth::check()){
        $following = \App\Models\User\User::find(Auth::user()->id);
        foreach($following->get_followers as $f){
            $followers[] = $f->followers_id;
        }
    }
?>



@if(isset($discussion) AND is_object($discussion))

    @php
        $uniqueurl = url('discussion/'. $discussion->author->username, $discussion->id);
    @endphp

@if(@$is_parent)
    <div class="user-card parenting_post_class" search-tab="#searched">
        <div class="user-card__main">
            <div class="user-card__content">
                <div class="user-card__header"><a class="user-card__name" href="#" style="margin-left: calc(50% - 35px)">
                    {{$text}}</a></div>
            </div>
        </div>
    </div>
@endif

    <div class="post-block discussion mobile--wrap @if($flag)post-block-notification @endif" id="discussion_{{$discussion->id}}">
    <a href="{{$uniqueurl}}" class="unique--link"></a>
        @if($flag)
        <div class="post-top-info-layer">
            <div class="post-info-line">
                <i class="fa fa-question-circle" rel="tooltip" data-toggle="tooltip" data-animation="false" title="" data-original-title="The total number of views your posts have garnered"></i> <strong>Question</strong>
                for you
            </div>
        </div>
        @endif
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap ava-50">
                    <img src="{{check_profile_picture($discussion->author->profile_picture)}}" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="{{ url_with_locale('profile/'.$discussion->author->id)}}">{{$discussion->author->name}}</a>{!! get_exp_icon($discussion->author) !!}
                    </div>
                    <div class="post-info">
                        <a target="_blank" class="post-title-link"> Asked for <strong>tips</strong> about</a>
                        <a href="{{url_with_locale(strtolower($discussion->destination_type)."/".$discussion->destination_id)}}" class="link-place">{{getDiscussionDestination($discussion->destination_type, $discussion->destination_id)}}</a>
                        <a target="_blank" class="post-title-link">on {{diffForHumans($discussion->created_at)}}</a>
                    </div>
                </div>
            </div>
            <div class="post-top-info-action" dir="auto">
                <div class="dropdown" dir="auto">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"      aria-expanded="false" dir="auto">
                        <i class="trav-angle-down" dir="auto"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                        @if(Auth::check() && Auth::user()->id != $discussion->author->id)
                            @if(in_array($discussion->author->id,$followers))
                                <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $discussion->author->id}}" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                        <i class="trav-user-plus-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                        <p dir="auto"><b dir="auto">Unfollow {{ $discussion->author->name ?? 'User' }}</b></p>
                                        <p dir="auto">Stop seeing posts from {{ $discussion->author->name ?? 'User' }}</p>
                                    </div>
                                </a>
                            @endif
                        @endif
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-share-icon" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Share</b></p>
                                <p dir="auto">Spread the word</p>
                            </div>
                        </a>
                        <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-link" dir="auto"></i>
                            </span>
                            <div class="drop-txt">
                                <p dir="auto"><b dir="auto">Copy Link</b></p>
                                <p dir="auto">Paste the link anywhere you want</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#sendToFriendModal" data-id="discussion_{{$discussion->id}}" data-toggle="modal" data-target="" dir="auto">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-share-on-travo-icon" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                <p dir="auto">Share with your friends</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#spamReportDlgNew" data-toggle="modal" data-target="#" dir="auto">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-flag-icon-o" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Report</b></p>
                                <p dir="auto">Help us understand</p>
                            </div>
                        </a>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="topic-inside-panel-wrap mobile--disable" onclick=" window.open('{{route('discussion.index').'#d-'.$discussion->id}}'); " style="cursor: pointer;">
            <div class="topic-inside-panel">
                <div class="panel-txt">
                    <h3 class="panel-ttl">  
                        <a style="text-decoration: none;color:black" href="{{route('discussion.index').'#d-'.$discussion->id}}">
                            {!! str_limit($discussion->question, 100, '') !!}
                        </a>
                    </h3>
                    <div class="post-txt-wrap">
                        <div>
                            <span class="less-content disc-ml-content">{!! str_limit($discussion->description, 125, '') !!}
                                    @if(strlen($discussion->description)>125)<span
                                    class="moreellipses">...</span> <a href="javascript:;" class="read-more-link">More</a>@endif</span>
                            <span class="more-content disc-ml-content" style="display: none;">{{$discussion->description}} <a href="javascript:;"
                                    class="read-less-link">Less</a></span>
                        </div>
                    </div>
                </div>
                <div class="panel-img disc-panel-img">
                    @if(count(@$discussion->media)>0)
                    <img src="{{@$discussion->media[0]->media_url}}" alt="" style="width: 130px; height: 140px;">
                    @endif
                </div>
            </div>
        </div>
        <div class="post-tips-top-layer post-tips-wrapper" dir="auto">
            @if(count($discussion->replies) > 0)
                <div class="post-tips-top" dir="auto">
                <h4 class="post-tips-ttl" dir="auto">Top Tips</h4>
            </div>
            @endif
            <div class="post-tips-main-block" id="discussioReply{{$variable}}">
                @if(count($discussion->replies) > 0)
                    @foreach($discussion->replies()->orderBy('num_upvotes', 'desc')->get() AS $reply)
                        @if ($loop->first)
                            <div class="post-tips-row" dir="auto">
                                <div class="tips-top flex-display-helper" dir="auto">
                                    <div class="tip-avatar" dir="auto">
                                        <img src="{{check_profile_picture($reply->author->profile_picture)}}" alt="" style="width:25px;height:25px;" dir="auto">
                                    </div>
                                    <div class="tip-content ov-wrap" dir="auto">
                                        <div class="top-content-top" dir="auto">
                                            <a href="{{url_with_locale('profile/'.$reply->author->id)}}" class="name-link" dir="auto">{{$reply->author->name}}</a>  {!! get_exp_icon($reply->author) !!}
                                            <span>@lang('discussion.strings.said')</span><span
                                                    class="dot"> · </span>
                                            <span>{{weatherTime($reply->created_at)}}</span>
                                            @if (Auth::check() && $reply->author->id !== Auth::user()->id)
                                                <div class="d-inline-block pull-right" dir="auto">
                                                    <div class="post-top-info-action" dir="auto">
                                                        <div class="dropdown" dir="auto">
                                                            <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" dir="auto">
                                                                <i class="trav-angle-down" dir="auto"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="DiscussionReplies">
                                                                @include('site.home.partials._info-actions', ['post'=>$reply])
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="tip-txt ov-wrap" dir="auto">
                                            <p class="dis_reply_9628 ov-wrap disc-text-block" dir="auto">
                                                {!!$reply->reply!!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
        @php
            
        @endphp
        <div class="post-footer-info">
            <!-- Updated elements -->
            <div class="post-foot-block post-reaction">

                <div class="tips-footer updownvote " id="updown_{{$variable}}"  dir="auto">
                    <a href="#" class="discussion-vote-link up {{(Auth::check() && $discussion->discussion_upvotes()->where('users_id', Auth::user()->id)->first())?'':'disabled'}}  upvote_{{$variable}}" data-id="{{$variable}}" id="upvote_{{$variable}}" data-type="up" dir="auto">
                        <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-up" style="padding-left: 2px;" dir="auto"></i></span>
                    </a>
                    <span><b class="upvote-count  upvote-count_{{$variable}}" dir="auto">{{ optimize_counter(count($discussion->discussion_upvotes))}}</b></span>
                    
                    &nbsp;&nbsp;
                    <a  href="#" class="discussion-vote-link down {{(Auth::check() &&  $discussion->discussion_downvotes()->where('users_id', Auth::user()->id)->first())?'':'disabled'}} downvote_{{$variable}}" data-id="{{$variable}}" id="downvote_{{$variable}}" data-type="down" dir="auto">
                        <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-down" style="padding-left: 2px;" dir="auto"></i></span>
                    </a>
                    <span><b class="upvote-count downvote-count_{{$variable}}" dir="auto">{{ optimize_counter(count($discussion->discussion_downvotes))}}</b></span>
                </div>
            </div>
            @if(count($discussion->replies) == 0)
                <div class="post-foot-block answer-cta">
                    <a href="{{route('discussion.index').'#d-'.$discussion->id}}" target="_blank" class="btn btn-light click-disc">
                        <i class="trav-pencil"></i> Answer
                    </a>
                    Be the first to answer this question 
                </div>
            @else
                @if(count($discussion->replies) > 0)
                    <div class="post-foot-block">
                        <div class="tips-footer">
                            <div class="more-tips">
                                <ul class="avatar-list">
                                    @php $us_list = array(); @endphp
                                    @foreach ($discussion->replies as $reply)
                                        @if(!in_array($reply->author->id, $us_list))
                                            @php $us_list[] = $reply->author->id; @endphp
                                            @if(count($us_list)<4)
                                                <li><img src="{{check_profile_picture($reply->author->profile_picture)}}" class="small-ava"></li>
                                            @else
                                                @php break; @endphp
                                            @endif
                                        @endif
                                    @endforeach
                                </ul>
                                <a href="{{route('discussion.index').'#d-'.$discussion->id}}"><b>{{count($discussion->replies)-1}}</b> more tips</a>
                            </div>
                        </div>
                    </div>
                    @endif
                @endif
            <!-- Updated element END -->
            <div class="post-foot-block ml-auto">
                <span class="post_share_buttons" id="{{$variable}}">
                    <a  href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-id="{{$variable}}" data-type="discussion" data-toggle="modal"  data-toggle="modal" data-target="#sharePostModal">
                        <img class='share--image' src="{{asset('assets2/image/share-mobile.png')}}">
                    </a>
                </span>
                <span id="post_share_count_{{$variable}}">
                    <a href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-id="{{$variable}}" data-type="discussion" data-toggle="modal" ><strong>0</strong>
                        Shares</a>
                </span>
            </div>
        </div>
        <div class="post-add-comment-block d-none" dir="auto" id="reply_disc{{$variable}}">
        </div>
    </div>
@endif
<script>
    $(document).ready(function (){
        $(document).on('click', ".read-more-link", function () {
            $(this).closest('.post-txt-wrap').find('.less-content').hide()
            $(this).closest('.post-txt-wrap').find('.more-content').show()
            $(this).hide()
        });
        $(document).on('click', ".read-less-link", function () {
            $(this).closest('.more-content').hide()
            $(this).closest('.post-txt-wrap').find('.less-content').show()
            $(this).closest('.post-txt-wrap').find('.read-more-link').show()
        });
        $(document).on('click', ".click-disc", function () {
           $('#'+$(this).data('id')).removeClass('d-none');
        //    $(this).parent('.answer-cta').addClass('d-none');
        });
        
    });
    </script>
@include('site.discussions.partials._view-scripts')
