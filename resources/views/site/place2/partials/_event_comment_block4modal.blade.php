<div class="comment">
<img class="comment__avatar" src="{{check_profile_picture($comment->author->profile_picture)}}" alt="" role="presentation">
<div class="comment__content">
    <div class="comment__header"><a class="comment__username" href="{{url_with_locale('profile/'.$comment->author->id)}}" tabindex="0">{{ $comment->author->name }}</a>
    <div class="comment__user-id">{{ '@'.$comment->author->username }}</div>
    </div>
    <div class="comment__text" style="margin-top: 10px;">{{ $comment->text }}</div>
    <div class="comment__footer">
    <div class="rating" posttype="Eventcomment" style="cursor: pointer; color: blue">
        <div class="rating__value" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$comment->id}},this)">Report</div>
    </div>
    <div class="comment__posted">{{ diffForHumans($comment->created_at) }}</div>
    </div>
</div>
</div>