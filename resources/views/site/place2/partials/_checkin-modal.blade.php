<style>
    .img-wrap-newsfeed {
        width: 40px !important;
        height: 40px !important;
        margin: auto 6px !important;
    }
    .media-wrap__new-slider_checkin > div > div {
        width: 40px;
    }
    .img-wrap-newsfeed .close {
        margin-top: -44px;
        margin-right: -5px;
        text-align: center;
    }
    .checkin-footer__boxes_checkin {
        display: flex;
    }
    .checkin-footer__boxes_checkin .add--image {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-right: 0;
    }
    .checkin-footer__boxes_checkin .add--image i {
        font-size: 20px;
        color: #cccccc;
    }
    .checkin-footer__boxes_checkin > div img {
        object-fit: cover;
        width: 100%;
        height: 100%;
    }
    .checkin-footer__boxes_checkin {
        width: 250px;
    }
    .checkin-footer__boxes_checkin > div {
        height: 47px;
    }
    .add--image {
        width: 23px;
        margin-left: 15px;
    }
    .media-wrap__new-slider_checkin {
        min-width: 0;
        display: flex;
        overflow: auto;
        position: relative;
        user-select: none;
        cursor: pointer;
    }
    
    .close {
        font-size: 1rem;
    }
</style>
<div class="modal fade checkin-modal" data-backdrop="false" id="checkin-modal" tabindex="-1" role="dialog" aria-labelledby="likesModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-checkin" role="document">
        <div class="modal-content">
            <div class="checkin-header">
                <div>
                    <svg class="icon icon--location ">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use>
                    </svg>
                    <h4>Check in</h4>
                </div>
                <button type="button" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <textarea class="form-control" id="description" rows="1">I just arrived at</textarea>
            <div class="modal-body">
                <div class='checkin-map'>
                    <div id="map">
                        
                    </div>
                    <div class="checkin-map__details">
                        <label>{{@$place->trans[0]->title}}</label>
                        <div>
                            <label for="checkin_time">Date</label>
                            <input type="hidden" id="fromPostBox" value="0">
                            <input autocomplete="off placeholder="{{date('l, d M, y')}}" value="{{date('l, d M, y')}}" type="text" name="checkin_time" id="checkin_time" class="form-control" required>
                        </div>      
                    </div>
                </div>
            </div>
            <div class='checkin-footer'>
                <div class='flex--wrap'>
                    <div class='checkin-footer__boxes_checkin'>
                        {{-- <div>
                            <img src="https://www.forestryengland.uk/sites/default/files/styles/list_image_large_2x/public/media/West%20Woods.JPG?itok=gUbRuOKA" alt="Italian Trulli">     
                        </div>
                        <div>
                            <img src="https://www.forestryengland.uk/sites/default/files/styles/list_image_large_2x/public/media/West%20Woods.JPG?itok=gUbRuOKA" alt="Italian Trulli">
                            <div class='image--overlay'>
                                <div class="progress">
                                    <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>     
                        </div> --}}
                        <div class="media-wrap__new-slider_checkin items">
                            {{--<div class="add-post__media"></div>--}}                                             
                        </div>
                        <div class="add--image" id="cover">
                            <input type="file" name="file[]" id="file" style="display:none" multiple="" >
                            <i class="trav-camera click-target" data-target="file" ></i>
                            {{-- <ul class="create-link-list">
                                <li class="post-options">
                                    <input type="file" name="file[]" class="report-comment-media-input" data-report_id="12345" data-id="commentfile12345" style="display:none">
                                    <i class="fa fa-camera click-target" aria-hidden="true" data-target="commentfile12345"></i>
                                </li>
                            </ul> --}}
                        </div>
                    </div>
                </div>
                <a class='check-in-button btn_liveCheckin'>
                <svg class="icon icon--location">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use>
                </svg>
                Check in
                </a>
                <i class="far fa-globe-americas"></i>
                <a class='publish-in-button'>
                    Publish
                </a>
            </div>
        </div>      
    </div>
</div>


<script>

$("#_add_post_imagefile , #_add_review_imagefile, #file").change(function(selector){
    var triggered = selector.target.id;
    var fileArray = this.files;
    var mediaBlock = document.getElementsByClassName('img-wrap-newsfeed');
    var limitFileUpload = 0;
    var allowedItems = 10;
    var imgMaxSize = 10000000;
    var videoMaxSize = 250000000;
    if (mediaBlock.length + fileArray.length > 10) {
        allowedItems -= mediaBlock.length;
    }
    $.each(fileArray,function(i,v){
        var file  = v;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if ((file.size > imgMaxSize && ((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) || (file.size > videoMaxSize && (/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type)))) {
            alert("The file can't be uploaded.");
        } else {
            if (limitFileUpload < Math.abs(allowedItems)) {
                if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])) && !/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type)) {
                    $('#cover').css('background-image', 'url(noimage.png)');
                    $("#message").html("<p style='color:red'>Please Select A valid Image File</p>");
                    $("#_add_post_imagefile").val("");
                    $("#_add_review_imagefile").val("");
                    return false;
                } else {
                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {
                            var count = $('.img-wrap-newsfeed').children().length;
                            var fd = new FormData();
                            fd.append('mediafiles', e.target.result);
                            console.log(fd);
                            $.ajax({
                                method: "POST",
                                url: "/place/upload-media-content",
                                data: fd,
                                contentType: false,
                                processData: false,
                                async: true,
                                xhr: function () {
                                    var xhr = $.ajaxSettings.xhr();
                                    xhr.upload.addEventListener("progress", function (evt) {

                                        if (evt.lengthComputable) {
                                            var percentComplete = ((evt.loaded / evt.total) * 100);
                                            if (triggered == '_add_review_imagefile')
                                                $('#' + count + '_media_content_review').width(percentComplete / 2.5 + 'px');
                                            else
                                                $('#' + count + '_media_content').width(percentComplete / 2.5 + 'px');
                                        }
                                    }, false);
                                    return xhr;
                                },
                                beforeSend: function () {
                                    var media = '';
                                    if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                                        media = $('<video style="object-fit:cover;border-radius:5px" width="40" height="40" controls>' +
                                            '<source src=' + e.target.result + ' type="video/mp4">' +
                                            '<source src="movie.ogg" type="video/ogg">' +
                                            '</video>');
                                        // media = $('<img/>').addClass('thumb').attr('src', 'https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_play_arrow_48px-512.png');

                                    } else {
                                        media = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
                                    }
                                    var button = $('<span/>').addClass('close-image').addClass('removeFile').click(function () {
                                        //create close button element
                                        if (triggered == '_add_review_imagefile')
                                            $('#' + count + '_media_attached_review').remove();
                                        else
                                            $('#' + count + '_media_attached').remove();
                                        $(this).parent().remove();
                                    }).append('<span class="close removeFile">&times;</span>');


                                    var imgitem = $('<div>').append(media); //create Image Wrapper element
                                    var progressdivcontainer = '';
                                    if (triggered == '_add_review_imagefile') {
                                        progressdivcontainer = $('<div  id="' + count + '_media_content_review" style="border-radius: 5px;position: absolute;opacity: 1;height: 6px;background: #0275d8;width: 25px;margin-top: 16px;" />').addClass('media-progress-bar');
                                        imgitem = $('<div/>').addClass('img-wrap-newsfeed lslide').append(progressdivcontainer).append(imgitem).append(button);
                                        $(".add-post__media").append(imgitem);
                                    } else {
                                        progressdivcontainer = $('<div  id="' + count + '_media_content" style="border-radius: 5px;position: absolute;opacity: 1;height: 6px;background: #0275d8;width: 25px;margin-top: 16px;" />').addClass('media-progress-bar');
                                        imgitem = $('<div/>').addClass('img-wrap-newsfeed lslide').append(progressdivcontainer).append(imgitem).append(button);
                                        $(".media-wrap__new-slider_checkin").append(imgitem);
                                    }

                                },
                            }).done(function (data) {
                                console.log(data);
                                var inputitem = '';
                                if (triggered == '_add_review_imagefile') {
                                    inputitem = $('<input type="hidden" id="' + count + '_media_attached_review" class="attachmentElm" name="mediafiles[]" />').val(data);
                                    $('#_place_add_review_form').append(inputitem);
                                    $('.add-post__footer-right').show();
                                    $('#' + count + '_media_content_review').width('0px');
                                } else {
                                    inputitem = $('<input type="hidden" id="' + count + '_media_attached" class="attachmentElm" name="mediafiles[]" />').val(data);
                                    $('#_place_add_post_form').append(inputitem);
                                    $('.add-post__footer-right').show();
                                    $('#' + count + '_media_content').width('0px');
                                }

                            });

                        };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.

                }
            }
            limitFileUpload++;
        }
    });
    $("#_add_post_imagefile").val("");
    $("#_add_review_imagefile").val("");
});

$(document).on('click', 'i.fa-camera', function () {
    $("#createPosTxt").click();
    var target = $(this).attr('data-target');
    var target_el = $('#' + target);
    target_el.trigger('click');
})
</script>