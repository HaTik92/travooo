@if(@$is_parent)
<div class="user-card parenting_post_class" search-tab="#searched">
    <div class="user-card__main">
        <div class="user-card__content">
            <div class="user-card__header"><a class="user-card__name" href="#" style="margin-left: calc(50% - 35px)">
                {{$text}}</a></div>
        </div>
    </div>
</div>
@endif