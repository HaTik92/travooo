
<script type="text/javascript">

    $(document).ready(function () {

    // -----START----- Post comment
    $(document).on('click', '.comments__filter-btn', function(){
        var filter_type = $(this).data('type');
        var reload_obj = $(this).closest('.post');

        commentSort(filter_type, $(this), reload_obj);
    })
    
    
    $('body').on('mouseover', '.news-feed-comment', function () {
        $(this).find('.post-com-top-action .dropdown').show()
    });

    $('body').on('mouseout', '.news-feed-comment', function () {
        $(this).find('.post-com-top-action .dropdown').hide()
    });

    $('body').on('mouseover', '.doublecomment', function () {
        $(this).find('.post-com-top-action .dropdown').show()
    });

    $('body').on('mouseout', '.doublecomment', function () {
        $(this).find('.post-com-top-action .dropdown').hide()
    });
        
        
    $(document).on('keydown', '*[data-id="postscommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '*[data-id="postscommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').find('button[type=submit]').click();
        }
    });
    
    $(document).on('change', '.post-comment-media-input', function (e) {
      var post_id = $(this).attr('data-post_id')
          commentMediaUploadFile($(this), $(this).closest(".postscomment"), post_id);

    })

    $(document).on('submit', '.postscomment', function(e){
        var form = $(this);
        var post_id = form.attr('id');
        var comment_text = form.find('textarea').val().trim();
        var all_comment_count = $('.'+ post_id +'-all-comments-count').html();
        var files = form.find('.medias').find('div').length;
        var reload_obj = form.closest('.post')

         if(comment_text == '' && files == 0){
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: '<span class="place-alert-content">Please input text or select files!</span>',
            });
            return false;
        }
        var values = $(this).serialize();
        $.ajax({
            method: "POST",
            url: "{{ route(session('place_country') . '.postcomment') }}",
            data: values
        })
            .done(function (res) {
                form.closest('.comments').find('.comments__items').prepend(res);
                form.find('.medias').find('.removeFile').each(function(){
                               $(this).click();
                           });
                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('textarea').css({"height": "50px", "padding-bottom": "0"});
                form.find('.post-create-controls').css('top', '15px');
                $('.'+ post_id +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                
                Comment_Show.reload(reload_obj);
            });
        e.preventDefault();
    });
    
         $('body').on('click', '.post-comment-delete', function (e) {
            e.preventDefault();
            var _this = $(this);
            var commentId = _this.attr('id');
            var post_id = _this.attr('postid');
            var comments_count = $('.' + post_id + '-all-comments-count').html()
            var reload_obj = _this.closest('.post');
            var type = _this.attr('data-del-type');
            var parent_id = _this.data('parent')
            $.confirm({
                title: 'Delete Comment!',
                content: '<span class="place-alert-content">Are you sure you want to delete this comment?</span> <div class="mb-3"></div>',
                columnClass: 'col-md-5 col-md-offset-5',
                closeIcon: true,
                offsetTop: 0,
                offsetBottom: 500,
                scrollToPreviousElement:false,
                scrollToPreviousElementAnimate:false,
                buttons: {
                    cancel: function () {},
                    confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function () {
                            $.ajax({
                                method: "POST",
                                url: "{{ route('post.commentdelete') }}",
                                data: {comment_id: commentId}
                            })
                            .done(function (res) {
                                $('.commentRow' + commentId).fadeOut();
                                $('.' + post_id + '-all-comments-count').html(parseInt(comments_count) - 1);
                                if (type == 1) {
                                    $('.commentRow' + commentId).parent().remove();
                                    Comment_Show.reload(reload_obj);
                                } else {
                                    if (_this.closest('.post').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                        $('.commentRow' + parent_id).find('.postCommentDelete').removeClass('d-none')
                                        $('.commentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                    }
                                }
                            });
                        }
                    }
                }
            });
        });
        
        $('body').on('click', '.post_comment_like', function (e) {
            var _this = $(this);
            commentId = $(this).attr('id');

            $.ajax({
                method: "POST",
                url: "{{ route('post.commentlikeunlike') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'yes') {
                            _this.find('i').addClass('fill');
                            _this.parent().find('.comment-likes-block').prepend('<span>' + result.name + '</span>')
                        } else if (result.status == 'no') {
                            _this.find('i').removeClass('fill');
                            _this.parent().find('.comment-likes-block span').each(function () {
                                if ($(this).text() == result.name)
                                    $(this).remove();
                            });
                        }
                        _this.find('.comment-like-count').html(result.count);



                    });
            e.preventDefault();
        });
        
        
        $('body').on('click', '.post-edit-comment', function (e) {
            var _this = $(this);

            var commentId = _this.attr('data-id');
            editPostCommentMedia(commentId)
            var postId = _this.attr('data-post');
            $('.commentEditForm' + commentId).removeClass('d-none')
            $('.comment-text-' + commentId + ' p').addClass('d-none')

            comment_textarea_auto_height($('.commentEditForm' + commentId + ' textarea')[0], 'edit')

            e.preventDefault();
        });

        $('body').on('click', '.edit-cancel-link', function (e) {
            var _this = $(this);

            var commentId = _this.attr('data-comment_id');
            $('.commentEditForm' + commentId).addClass('d-none')
            $('.comment-text-' + commentId + ' p').removeClass('d-none')

            e.preventDefault();
        });


        $('body').on('click', '.edit-post-comment-link', function (e) {
            var _this = $(this);
            var commentId = _this.attr('data-comment_id');
            var form = _this.closest('.commentEditForm' + commentId);
            var text = form.find('textarea').val().trim();
            var files = form.find('.medias').find('div').length;
            var comment_type = form.find("input[name='comment_type']").val();

            if (text == "" && files == 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    type: 'orange',
                    offsetTop: 0,
                    offsetBottom: 500,
                    content: 'Please input text or select files!',
                });
                return false;
            }
            var values = form.serialize();
            $.ajax({
                method: "POST",
                url: "{{ route(session('place_country') . '.postcommentedit') }}",
                data: values
            })
                    .done(function (res) {
                        form.find('.medias').find('.removeFile').each(function () {
                            $(this).click();
                        });
                        if (comment_type == 1) {
                            $('.commentRow' + commentId).parent().html(res);
                        } else {
                            $('.commentRow' + commentId).replaceWith(res);
                        }
                    });
            e.preventDefault();
        });
        
        $('body').on('click', '.remove-media-file', function (e) {
            var media_id = $(this).attr('data-media_id');
            $(this).closest('.medias').append('<input type="hidden" name="existing_media[]" value="' + media_id + '">')
            $(this).parent().remove();
        });


         $(document).on('keydown', '.post-reply-text', function(e){
            if(e.keyCode == 13 && !e.shiftKey){
                e.preventDefault();
            }
        });
        
        $(document).on('keyup', '.post-reply-text', function(e){
            if(e.keyCode == 13 && !e.shiftKey){
                var comment_id = $(this).attr('id');
                replyPostComment(comment_id);
                $(this).val('')
            }
        });
      
      
    $(document).on('change', '.post-comment-reply-media-input', function (e) {
      var comment_id = $(this).attr('data-comment-id')
          commentMediaUploadFile($(this), $(this).closest(".commentReplyForm"+comment_id), comment_id);

    })


    $('body').on('click', '.comment-likes-block', function (e) {
        var _this = $(this);
        commentId = $(this).attr('data-id');

        $.ajax({
            method: "POST",
            url: "{{ route('post.commentlikeusers') }}",
            data: {comment_id: commentId}
        })
                .done(function (res) {
                    var result = JSON.parse(res);
                    $('.post-comment-like-users').html(result.text)
                    $('#commentslikePopup').find('.comment-like-count').html(result.count)
                    $('#commentslikePopup').modal('show')
                });
        e.preventDefault();
    });


    $('body').on('click', '.checkinCommentLikes', function (e) {
        obj = $(this);
        commentId = $(this).attr('id');
        $.ajax({
            method: "POST",
            url: "{{ route('post.checkincommentlikeunlike') }}",
            data: {comment_id: commentId}
        })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (res.status == 'yes') {

                    } else if (res == 'no') {

                    }
                    //console.log('#post_like_count_' + postId);
                    obj.html(result.count + " @lang('home.likes')");
                });
        e.preventDefault();
    });

    // follow post comment likes user
    $('body').on('click', '.pcl-follow', function () {
        var _this = $(this);
        var user_id = $(this).attr('data-id');
        var follow_url = "{{  route('profile.follow', ':user_id')}}";
        var url = follow_url.replace(':user_id', user_id);

        $.ajax({
            method: "POST",
            url: url,
            data: {type: "follow"}
        })
                .done(function (res) {
                    if (res.success == true) {
                        _this.html('unfollow');
                        _this.removeClass('pcl-follow');
                        _this.removeClass('btn-light-primary');
                        _this.addClass('pcl-unfollow');
                        _this.addClass('btn-light-grey');
                    }
                });
    });

    // unfollow post comment likes user
    $('body').on('click', '.pcl-unfollow', function () {
        var _this = $(this);
        var user_id = $(this).attr('data-id');
        var unfollow_url = "{{  route('profile.unfolllow', ':user_id')}}";
        var url = unfollow_url.replace(':user_id', user_id);

        $.ajax({
            method: "POST",
            url: url,
            data: {type: "unfollow"}
        })
                .done(function (res) {
                    _this.html('follow');
                    _this.removeClass('pcl-unfollow');
                    _this.removeClass('btn-light-grey');
                    _this.addClass('pcl-follow');
                    _this.addClass('btn-light-primary');
                });
    });

    // -----END----- Post comment
    
    // -----START----- Trip plan comment
    
    $(document).on('click', '.filter-trip-comment', function(){
        var filter_type = $(this).data('type');
        var reload_obj = $(this).closest('.post');

        commentSort(filter_type, $(this), reload_obj);
    })
    
    $(document).on('keydown', '*[data-id="tripscommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '*[data-id="tripscommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').find('button[type=submit]').click();
        }
    });
    
    $(document).on('change', '.trip-comment-media-input', function (e) {
      var plan_id = $(this).attr('data-plan_id')
          commentMediaUploadFile($(this), $(this).closest(".tripscomment"), plan_id);

    })
    
    $(document).on('change', '.trip-comment-modal-input', function (e) {
      var plan_id = $(this).attr('data-plan_id')
          commentMediaUploadFile($(this), $(this).closest(".tripscomment"), plan_id);

    })
    
    $('body').on('submit', '.tripscomment', function (e) {
        var form = $(this);
        var text = form.find('textarea').val().trim();
        var plan_id = form.attr('data-id');
        var files = $('.tripscomment').find('.medias').find('div').length;
        var all_comment_count = $('.'+ plan_id +'-all-comments-count').html();
        var files = form.find('.medias').find('div').length;
        var reload_obj = form.closest('.post')
     
        if(text == '' && files == 0){
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: '<span class="place-alert-content">Please input text or select files!</span>',
            });
            return false;
        }
        
        var values = form.serialize();
    
        $.ajax({
            method: "POST",
            url: "{{ route('trip.comment') }}",
            data: values
        })
            .done(function (res) {
                $('.tripscomment-block-'+plan_id).find('.post-comment-wrapper').prepend(res);
                $('.tripscomment-modal-block-'+plan_id).find('.simplebar-content').prepend(res);
                form.find('.medias').find('.removeFile').each(function(){
                               $(this).click();
                           });
                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('textarea').css({"height": "50px", "padding-bottom": "0"});
                form.find('.post-create-controls').css('top', '15px');
                $('.'+ plan_id +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                
                Comment_Show.reload(reload_obj);
            });
        e.preventDefault();
    });
    
    $('body').on('click', '.plan-comment-delete', function (e) {
            e.preventDefault();
            var _this = $(this);
            var commentId = _this.attr('id');
            var post_id = _this.attr('postid');
            var comments_count = $('.' + post_id + '-all-comments-count').html()
            var reload_obj = _this.closest('.post');
            var type = _this.attr('data-del-type');
            var parent_id = _this.data('parent')
            $.confirm({
                title: 'Delete Comment!',
                content: '<span class="place-alert-content">Are you sure you want to delete this comment?</span> <div class="mb-3"></div>',
                columnClass: 'col-md-5 col-md-offset-5',
                closeIcon: true,
                offsetTop: 0,
                offsetBottom: 500,
                scrollToPreviousElement:false,
                scrollToPreviousElementAnimate:false,
                buttons: {
                    cancel: function () {},
                    confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function () {
                            $.ajax({
                                method: "POST",
                                url: "{{ route('trip.commentdelete') }}",
                                data: {comment_id: commentId}
                            })
                            .done(function (res) {
                                $('.commentRow' + commentId).fadeOut();
                                $('.' + post_id + '-all-comments-count').html(parseInt(comments_count) - 1);
                                if (type == 1) {
                                    $('.commentRow' + commentId).parent().remove();
                                    Comment_Show.reload(reload_obj);
                                } else {
                                    if (_this.closest('.post').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                        $('.commentRow' + parent_id).find('.postCommentDelete').removeClass('d-none')
                                        $('.commentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                    }
                                }
                            });
                        }
                    }
                }
            });
        });
        
        
        
        $('body').on('click', '.plan_comment_like', function (e) {
            var _this = $(this);
            commentId = $(this).attr('id');

            $.ajax({
                method: "POST",
                url: "{{ route('trip.commentlikeunlike') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'yes') {
                            _this.find('i').addClass('fill');
                            _this.parent().find('.comment-likes-block').prepend('<span>' + result.name + '</span>')
                        } else if (result.status == 'no') {
                            _this.find('i').removeClass('fill');
                            _this.parent().find('.comment-likes-block span').each(function () {
                                if ($(this).text() == result.name)
                                    $(this).remove();
                            });
                        }
                        _this.find('.comment-like-count').html(result.count);



                    });
            e.preventDefault();
        });
        
    $(document).on('keydown', '.plan-reply-text', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '.plan-reply-text', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            var comment_id = $(this).attr('data-id');
            replyTripComment($(this), comment_id);
            $(this).val('')
        }
    });
    
    $('body').on('click', '.tripCommentReply', function (e) {
         commentId = $(this).attr('id');
         var plan_modal_content_obj = $(this).closest('.plan-modal-comment-content').find('.tripCommentReplyForm' + commentId);
         
            plan_modal_content_obj.find('.plan-reply-text').addClass('textarea-bg-white')
            plan_modal_content_obj.find('.trip-comment-reply-media-input').addClass('trip-comment-reply-modal-input')
            plan_modal_content_obj.addClass('tripModalReplyForm'+commentId)
            plan_modal_content_obj.find('.trip-comment-reply-media-input').attr('data-id', 'tripcommentmodalreplyfile'+commentId)
            plan_modal_content_obj.find('i.fa-camera').attr('data-target', 'tripcommentmodalreplyfile'+commentId)
            plan_modal_content_obj.find('.trip-comment-reply-media-input').removeClass('trip-comment-reply-media-input')
     })
      
      
    $(document).on('change', '.trip-comment-reply-media-input', function (e) {
      var comment_id = $(this).attr('data-comment-id')
          commentMediaUploadFile($(this), $(this).closest(".tripCommentReplyForm"+comment_id), comment_id);

    })
    
    $(document).on('change', '.trip-comment-reply-modal-input', function (e) {
      var comment_id = $(this).attr('data-comment-id')
          commentMediaUploadFile($(this), $(this).closest(".tripModalReplyForm"+comment_id), comment_id);

    })
    
    $('body').on('click', '.trip-edit-comment', function (e) {
          var _this = $(this);

          var commentId = _this.attr('data-id');
         
          var postId = _this.attr('data-post');
          
        var trip_modal_edit_content_obj = $(this).closest('.plan-modal-comment-content').find('.tripCommentEditForm' + commentId);

        if(trip_modal_edit_content_obj.length > 0){
          _this.closest('.plan-modal-comment-content').find('.tripCommentEditForm' + commentId).removeClass('d-none');
          _this.closest('.plan-modal-comment-content').find('.comment-text-' + commentId + ' p').addClass('d-none')

          trip_modal_edit_content_obj.addClass('tripModalEditForm'+commentId)
          trip_modal_edit_content_obj.find('.commenteditfile').attr('data-id', 'tripcommentmodaleditfile'+commentId)
          trip_modal_edit_content_obj.find('i.fa-camera').attr('data-target', 'tripcommentmodaleditfile'+commentId)

          editTripModalCommentMedia(commentId)
        }else{
         _this.closest('.post-comment-text').find('.tripCommentEditForm' + commentId).removeClass('d-none')
         _this.closest('.post-comment-text').find('.comment-text-' + commentId + ' p').addClass('d-none')

         editTripCommentMedia(commentId)
        }

         comment_textarea_auto_height(_this.closest('.post-comment-text').find('.tripCommentEditForm' + commentId + ' textarea')[0], 'edit')
          

          e.preventDefault();
      });

      $('body').on('click', '.edit-trip-cancel-link', function (e) {
          var _this = $(this);

          var commentId = _this.attr('data-comment_id');
          $('.tripCommentEditForm' + commentId).addClass('d-none')
          $('.comment-text-' + commentId + ' p').removeClass('d-none')

          e.preventDefault();
      });


      $('body').on('click', '.edit-trip-comment-link', function (e) {
          var _this = $(this);
          var commentId = _this.attr('data-comment_id');
          var form = _this.closest('.tripCommentEditForm' + commentId);
          var text = form.find('textarea').val().trim();
          var files = form.find('.medias').find('div').length;
          var comment_type = form.find("input[name='comment_type']").val();

          if (text == "" && files == 0)
          {
              $.alert({
                  icon: 'fa fa-warning',
                  title: '',
                  type: 'orange',
                  offsetTop: 0,
                  offsetBottom: 500,
                  content: 'Please input text or select files!',
              });
              return false;
          }
          var values = form.serialize();
          $.ajax({
              method: "POST",
              url: "{{ route('trip.commentedit') }}",
              data: values
          })
                  .done(function (res) {
                      form.find('.medias').find('.removeFile').each(function () {
                          $(this).click();
                      });
                      if (comment_type == 1) {
                          $('.commentRow' + commentId).parent().html(res);
                      } else {
                          $('.commentRow' + commentId).replaceWith(res);
                      }
                  });
          e.preventDefault();
      });
      
      
    $('body').on('click', '.trip-comment-likes-block', function (e) {
        var _this = $(this);
        commentId = $(this).attr('data-id');
        var like_user_count = _this.parent().find('.comment-like-count').text()
        
        $.ajax({
            method: "POST",
            url: "{{ route('trip.commentlikeusers') }}",
            data: {comment_id: commentId}
        })
                .done(function (res) {
                    $('.post-comment-like-users').html(res)
                    $('#commentslikePopup').find('.comment-like-count').html(like_user_count)
                    $('#commentslikePopup').modal('show')
                });
        e.preventDefault();
    });
    // -----END----- Trip plan comment
 
    // -----START----- events comment
    
    $(document).on('click', '.filter-event-comment', function(){
        var filter_type = $(this).data('type');
        var reload_obj = $(this).closest('.post');

        commentSort(filter_type, $(this), reload_obj);
    })
    
    $(document).on('keydown', '*[data-id="eventscommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '*[data-id="eventscommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').find('button[type=submit]').click();
        }
    });
    
    $(document).on('keydown', '*[data-id="eventsmodalcommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '*[data-id="eventsmodalcommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').find('button[type=submit]').click();
        }
    });
    
    $(document).on('change', '.event-comment-media-input', function (e) {
      var event_id = $(this).attr('data-event_id')
          commentMediaUploadFile($(this), $(this).closest(".eventscomment"), event_id);

    });
    
    
    $(document).on('change', '.event-modal-media-input', function (e) {
      var event_id = $(this).attr('data-event_id')
          commentMediaUploadFile($(this), $(this).closest(".eventscomment"), event_id);

    });
    
    
    $('body').on('submit', '.eventscomment', function (e) {
        var form = $(this);
        var text = form.find('textarea').val().trim();
        var event_id = form.attr('data-id');
        var files = $('.eventscomment').find('.medias').find('div').length;
        var all_comment_count = $('.'+ event_id +'-all-comments-count').html();
        var files = form.find('.medias').find('div').length;
        var reload_obj = form.closest('.post')

        if(text == '' && files == 0){
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: '<span class="place-alert-content">Please input text or select files!</span>',
            });
            return false;
        }
        
        var values = form.serialize();
    
        $.ajax({
            method: "POST",
            url: "{{ route(session('place_country') . '.eventcomment') }}",
            data: values
        })
            .done(function (res) {
                $('.comment-block-' + event_id).find('.post-comment-wrapper').prepend(res);
                form.find('.medias').find('.removeFile').each(function(){
                               $(this).click();
                           });
                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('textarea').css({"height": "50px", "padding-bottom": "0"});
                form.find('.post-create-controls').css('top', '15px');
                $('.'+ event_id +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                
                Comment_Show.reload(reload_obj);
            });
        e.preventDefault();
    });
    
        $('body').on('click', '.event-comment-delete', function (e) {
            e.preventDefault();
            var _this = $(this);
            var commentId = _this.attr('id');
            var event_id = _this.attr('postid');
            var comments_count = $('.' + event_id + '-all-comments-count').html()
            var reload_obj = _this.closest('.post');
            var type = _this.attr('data-del-type');
            var parent_id = _this.data('parent')
            $.confirm({
                title: 'Delete Comment!',
                content: '<span class="place-alert-content">Are you sure you want to delete this comment?</span> <div class="mb-3"></div>',
                columnClass: 'col-md-5 col-md-offset-5',
                closeIcon: true,
                offsetTop: 0,
                offsetBottom: 500,
                scrollToPreviousElement:false,
                scrollToPreviousElementAnimate:false,
                buttons: {
                    cancel: function () {},
                    confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function () {
                            $.ajax({
                                method: "POST",
                                url: "{{ route(session('place_country') . '.eventcommentdelete') }}",
                                data: {comment_id: commentId}
                            })
                            .done(function (res) {
                                $('.commentRow' + commentId).fadeOut();
                                $('.' + event_id + '-all-comments-count').html(parseInt(comments_count) - 1);
                                if (type == 1) {
                                    $('.commentRow' + commentId).parent().remove();
                                    Comment_Show.reload(reload_obj);
                                } else {
                                    if (_this.closest('.post').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                        $('.commentRow' + parent_id).find('.event-comment-delete').removeClass('d-none')
                                        $('.commentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                    }
                                }
                            });
                        }
                    }
                }
            });
        });
        
        
        
        $('body').on('click', '.event_comment_like', function (e) {
            var _this = $(this);
            commentId = $(this).attr('id');

            $.ajax({
                method: "POST",
                url: "{{ route('place.eventcommentlikeunlike') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'yes') {
                            _this.find('i').addClass('fill');
                            _this.parent().find('.comment-likes-block').prepend('<span>' + result.name + '</span>')
                        } else if (result.status == 'no') {
                            _this.find('i').removeClass('fill');
                            _this.parent().find('.comment-likes-block span').each(function () {
                                if ($(this).text() == result.name)
                                    $(this).remove();
                            });
                        }
                        _this.find('.comment-like-count').html(result.count);



                    });
            e.preventDefault();
        });
        
    $(document).on('keydown', '.event-reply-text', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '.event-reply-text', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            var comment_id = $(this).attr('data-id');
            replyEventComment($(this), comment_id);
            $(this).val('')
        }
    });
    
    $('body').on('click', '.eventCommentReply', function (e) {
         commentId = $(this).attr('id');
         var modal_content_obj = $(this).closest('.event-modal-comment-content').find('.eventCommentReplyForm' + commentId);
            modal_content_obj.find('.event-comment-reply-media-input').addClass('event-modal-reply-media-input')
            modal_content_obj.addClass('eventModalReplyForm'+commentId)
            modal_content_obj.find('.event-comment-reply-media-input').attr('data-id', 'commentmodalreplyfile'+commentId)
            modal_content_obj.find('i.fa-camera').attr('data-target', 'commentmodalreplyfile'+commentId)
            modal_content_obj.find('.event-comment-reply-media-input').removeClass('event-comment-reply-media-input')
     })
      
      
    $(document).on('change', '.event-comment-reply-media-input', function (e) {
      var comment_id = $(this).attr('data-comment-id')
          commentMediaUploadFile($(this), $(this).closest(".eventCommentReplyForm"+comment_id), comment_id);

    })
    
    $(document).on('change', '.event-modal-reply-media-input', function (e) {
      var comment_id = $(this).attr('data-comment-id')
          commentMediaUploadFile($(this), $(this).closest(".eventCommentReplyForm"+comment_id), comment_id);

    })
    
    $('body').on('click', '.event-edit-comment', function (e) {
          var _this = $(this);

          var commentId = _this.attr('data-id');
          var postId = _this.attr('data-post');
          var modal_edit_content_obj = $(this).closest('.event-modal-comment-content').find('.eventCommentEditForm' + commentId);

          if(modal_edit_content_obj.length > 0){
            _this.closest('.event-modal-comment-content').find('.eventCommentEditForm' + commentId).removeClass('d-none');
            _this.closest('.event-modal-comment-content').find('.comment-text-' + commentId + ' p').addClass('d-none')
            
            modal_edit_content_obj.addClass('eventModalEditForm'+commentId)
            modal_edit_content_obj.find('.commenteditfile').attr('id', 'commentmodaleditfile'+commentId)
            modal_edit_content_obj.find('i.fa-camera').attr('data-target', 'commentmodaleditfile'+commentId)
            
            editEventModalCommentMedia(commentId)
          }else{
           _this.closest('.post-comment-text').find('.eventCommentEditForm' + commentId).removeClass('d-none')
           _this.closest('.post-comment-text').find('.comment-text-' + commentId + ' p').addClass('d-none')
           
           editEventCommentMedia(commentId)
          }
          
           comment_textarea_auto_height(_this.closest('.post-comment-text').find('.eventCommentEditForm' + commentId + ' textarea')[0], 'edit')
          
          e.preventDefault();
      });

      $('body').on('click', '.edit-event-cancel-link', function (e) {
          var _this = $(this);

          var commentId = _this.attr('data-comment_id');
          $('.eventCommentEditForm' + commentId).addClass('d-none')
          $('.comment-text-' + commentId + ' p').removeClass('d-none')

          e.preventDefault();
      });


      $('body').on('click', '.edit-event-comment-link', function (e) {
          var _this = $(this);
          var commentId = _this.attr('data-comment_id');
          var form = _this.closest('.eventCommentEditForm' + commentId);
          var text = form.find('textarea').val().trim();
          var files = form.find('.medias').find('div').length;
          var comment_type = form.find("input[name='comment_type']").val();

          if (text == "" && files == 0)
          {
              $.alert({
                  icon: 'fa fa-warning',
                  title: '',
                  type: 'orange',
                  offsetTop: 0,
                  offsetBottom: 500,
                  content: 'Please input text or select files!',
              });
              return false;
          }
          var values = form.serialize();
          $.ajax({
              method: "POST",
              url: "{{ route(session('place_country') . '.eventcommentedit') }}",
              data: values
          })
                  .done(function (res) {
                      form.find('.medias').find('.removeFile').each(function () {
                          $(this).click();
                      });
                      if (comment_type == 1) {
                          $('.commentRow' + commentId).parent().html(res);
                      } else {
                          $('.commentRow' + commentId).replaceWith(res);
                      }
                  });
          e.preventDefault();
      });
      
    $('body').on('click', '.event-comment-likes-block', function (e) {
        var _this = $(this);
        commentId = $(this).attr('data-id');
        var like_user_count = _this.parent().find('.comment-like-count').text()
        
        $.ajax({
            method: "POST",
            url: "{{ route(session('place_country') . '.commentlikeusers') }}",
            data: {comment_id: commentId}
        })
                .done(function (res) {
                    $('.post-comment-like-users').html(res)
                    $('#commentslikePopup').find('.comment-like-count').html(like_user_count)
                    $('#commentslikePopup').modal('show')
                });
        e.preventDefault();
    });
    
    // -----END----- events comment

    // -----START----- report comment
    
    $(document).on('click', '.filter-event-comment', function(){
        var filter_type = $(this).data('type');
        var reload_obj = $(this).closest('.post');

        commentSort(filter_type, $(this), reload_obj);
    })
    
    $(document).on('keydown', '*[data-id="reportcommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '*[data-id="reportcommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').find('button[type=submit]').click();
        }
    });
    
    
    $(document).on('keydown', '*[data-id="reportmodalcommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '*[data-id="reportmodalcommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').find('button[type=submit]').click();
        }
    });
    
    $(document).on('change', '.report-comment-media-input', function (e) {
      var report_id = $(this).attr('data-report_id')
          commentMediaUploadFile($(this), $(this).closest(".reportCommentForm"), report_id);

    });
    
    $(document).on('change', '.report-modal-comment-media-input', function (e) {
      var report_id = $(this).attr('data-report_id')
          commentMediaUploadFile($(this), $(this).closest(".reportCommentForm"), report_id);

    });
    
    
    $('body').on('submit', '.reportCommentForm', function (e) {
        var form = $(this);
        var text = form.find('textarea').val().trim();
        var report_id = form.attr('data-id');
        var files = $('.reportCommentForm').find('.medias').find('div').length;
        var all_comment_count = $('.'+ report_id +'-all-comments-count').html();
        var files = form.find('.medias').find('div').length;
        var reload_obj = form.closest('.post')
       
        if(text == '' && files == 0){
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: '<span class="place-alert-content">Please input text or select files!</span>',
            });
            return false;
        }
        
        var values = form.serialize();
    
        $.ajax({
            method: "POST",
            url: "{{url('reports/comment')}}",
            data: values
        })
            .done(function (res) {
                $('.comment-block-'+report_id).find('.post-comment-wrapper').prepend(res);
                form.find('.medias').find('.removeFile').each(function(){
                               $(this).click();
                           });
                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('textarea').css({"height": "50px", "padding-bottom": "0"});
                form.find('.post-create-controls').css('top', '15px');
                $('.'+ report_id +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                
                Comment_Show.reload(reload_obj);
            });
        e.preventDefault();
    });
    
        $('body').on('click', '.report-comment-delete', function (e) {
            e.preventDefault();
            var _this = $(this);
            var commentId = _this.attr('id');
            var report_id = _this.attr('reportid');
            var comments_count = $('.' + report_id + '-all-comments-count').html()
            var reload_obj = _this.closest('.post');
            var type = _this.attr('data-del-type');
            var parent_id = _this.data('parent')
            $.confirm({
                title: 'Delete Comment!',
                content: '<span class="place-alert-content">Are you sure you want to delete this comment?</span> <div class="mb-3"></div>',
                columnClass: 'col-md-5 col-md-offset-5',
                closeIcon: true,
                offsetTop: 0,
                offsetBottom: 500,
                scrollToPreviousElement:false,
                scrollToPreviousElementAnimate:false,
                buttons: {
                    cancel: function () {},
                    confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function () {
                            $.ajax({
                                method: "POST",
                                url: "{{ route('report.commentdelete') }}",
                                data: {comment_id: commentId}
                            })
                            .done(function (res) {
                                $('.commentRow' + commentId).fadeOut();
                                $('.' + report_id + '-all-comments-count').html(parseInt(comments_count) - 1);
                                if (type == 1) {
                                    $('.commentRow' + commentId).parent().remove();
                                    Comment_Show.reload(reload_obj);
                                } else {
                                    if (_this.closest('.post').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                        $('.commentRow' + parent_id).find('.event-comment-delete').removeClass('d-none')
                                        $('.commentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                    }
                                }
                            });
                        }
                    }
                }
            });
        });
        
        
        
        $('body').on('click', '.report_comment_like', function (e) {
            var _this = $(this);
            commentId = $(this).attr('id');
        
            $.ajax({
                method: "POST",
                url: "{{ route('report.commentlike') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'yes') {
                            _this.find('i').addClass('fill');
                        } else if (result.status == 'no') {
                            _this.find('i').removeClass('fill');
                            _this.parent().find('.comment-likes-block span').each(function () {
                                if ($(this).text() == result.name)
                                    $(this).remove();
                            });
                        }
                        $('.'+ commentId +'-comment-like-count').html(result.count);



                    });
            e.preventDefault();
        });
        
        $('body').on('click', '.report-comment-likes-block', function (e) {
            var _this = $(this);
            commentId = $(this).attr('data-id');
            var like_user_count = _this.parent().find('.comment-like-count').text();
            
            $.ajax({
                method: "POST",
                url: "{{ route('report.commentlikeusers') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        $('.post-comment-like-users').html(res)
                        $('#commentslikePopup').find('.comment-like-count').html(like_user_count)
                        $('#commentslikePopup').modal('show')
                    });
            e.preventDefault();
        });
        
    $('body').on('click', '.reportCommentReply', function (e) {
         commentId = $(this).attr('id');
           var report_modal_content_obj = $(this).closest('.reports-modal-comment-block').find('.commentReplyForm' + commentId);
           
            report_modal_content_obj.find('.report-comment-reply-media-input').addClass('report-modal-reply-media-input')
            report_modal_content_obj.addClass('reportModalReplyForm'+commentId)
            report_modal_content_obj.find('.report-comment-reply-media-input').attr('data-id', 'commentmodalreplyfile'+commentId)
            report_modal_content_obj.find('i.fa-camera').attr('data-target', 'commentmodalreplyfile'+commentId)
            report_modal_content_obj.find('.report-comment-reply-media-input').removeClass('report-comment-reply-media-input')
            
         $('.replyForm' + commentId).show();
         e.preventDefault();
     });
      
    $(document).on('change', '.report-modal-reply-media-input', function (e) {
      var comment_id = $(this).attr('data-comment-id')
          commentMediaUploadFile($(this), $(this).closest(".reportModalReplyForm"+comment_id), comment_id);

    })
    
     
    $('body').on('keydown', '.report-reply-text', function(e){
       if(e.keyCode == 13 && !e.shiftKey){
           e.preventDefault();
       }
   });
   
     $(document).on('change', '.report-comment-reply-media-input', function (e) {
      var comment_id = $(this).attr('data-comment-id')
          commentMediaUploadFile($(this), $(this).closest(".commentReplyForm"+comment_id), comment_id);

    })
    
    
    $('body').on('keyup', '.report-reply-text', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            var comment_id = $(this).attr('data-id');
            var form = $(this).closest('.commentReplyForm' + comment_id);
            replyReportComment(form);
            $(this).val('')
        }
    });
    
    $('body').on('click', '.report-edit-comment', function (e) {
        var _this = $(this);

        var commentId = _this.attr('data-id');
        var postId = _this.attr('data-report');        
        var modal_edit_content_obj = $(this).closest('.reports-modal-comment-block').find('.commentEditForm' + commentId);

        if(modal_edit_content_obj.length > 0){
          _this.closest('.reports-modal-comment-block').find('.commentEditForm' + commentId).removeClass('d-none');
          _this.closest('.reports-modal-comment-block').find('.comment-text-' + commentId + ' p').addClass('d-none')

          modal_edit_content_obj.addClass('reportModalEditForm'+commentId)
          modal_edit_content_obj.find('.commenteditfile').attr('data-id', 'commentmodaleditfile'+commentId)
          modal_edit_content_obj.find('i.fa-camera').attr('data-target', 'commentmodaleditfile'+commentId)

          editReportModalCommentMedia(commentId)
        }else{
         _this.closest('.post-comment-text').find('.commentEditForm' + commentId).removeClass('d-none')
         _this.closest('.post-comment-text').find('.comment-text-' + commentId + ' p').addClass('d-none')

         editReportCommentMedia(commentId)
        }
          
           comment_textarea_auto_height(_this.closest('.post-comment-text').find('.commentEditForm' + commentId + ' textarea')[0], 'edit')

        e.preventDefault();
    });

    $('body').on('click', '.edit-cancel-link', function (e) {
        var _this = $(this);

        var commentId = _this.attr('data-comment_id');
        $('.commentEditForm' + commentId).addClass('d-none')
        $('.comment-text-' + commentId + ' p').removeClass('d-none')

        e.preventDefault();
    });

    $('body').on('click', '.edit-report-comment-link', function (e) {
        var _this = $(this);
        var commentId = _this.attr('data-comment_id');
        var form = _this.closest('.commentEditForm' + commentId);
        
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;
        var comment_type = form.find("input[name='comment_type']").val();

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();
        $.ajax({
            method: "POST",
            url: "{{ route('report.commentedit') }}",
            data: values
        })
                .done(function (res) {
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    if (comment_type == 1) {
                        $('.commentRow' + commentId).parent().html(res);
                    } else {
                        $('.commentRow' + commentId).replaceWith(res);
                    }
                });
        e.preventDefault();
    });
    
    // -----END----- report comment
    
    //----START---- media modal comment
    $(document).on('keydown', '#mediacomment4modal', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '#mediacomment4modal', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').submit();
        }
    });
    
    $(document).on('change', '.modal-media-file-input', function (e) {
      var report_id = $(this).attr('data-media_id')
          commentMediaUploadFile($(this), $(this).closest(".mediModalCommentForm"), report_id);

    });

    $(document).on('submit', '.mediModalCommentForm', function(e){
        var form = $(this);
        var text = form.find('#mediacomment4modal').val().trim();
        var files = form.find('.medias').find('div').length;

        if(text == "" && files == 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    type: 'orange',
                    offsetTop: 0,
                    offsetBottom: 500,
                    content: 'Please input text or select files!',
                });
                return false;
            }
        var values = $(this).serialize();
        var comment_body = $("#modal-medias").find(".comments");

        form.trigger('reset');
        var postId = form.find('input[name="medias_id"]').val();
        var media_comments_count = comment_body.find('.post-comment-row').length;
        var media_comments_total_count = comment_body.attr('data-total_cnt');
        var url = '{{ route("media.comment", ":postId") }}';
        url = url.replace(':postId', postId);
       
        $.ajax({
            method: "POST",
            url: url,
            data: values
        })
            .done(function (res) {
                comment_body.find('.comments__items').find(".simplebar-content").prepend(res)
                comment_body.find(".comments__total").html((parseInt(media_comments_total_count)+1) + " Comments");
                comment_body.find(".comments__number").html( (parseInt(media_comments_count)+1) + "/" + (parseInt(media_comments_total_count)+1) );
                comment_body.attr('data-total_cnt', parseInt(media_comments_total_count)+1);
                $('.' +postId+ '-comment-count').html(parseInt(media_comments_total_count)+1);
               
                form.find('.medias').find('.removeFile').each(function () {
                      $(this).click();
                  });                
                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('textarea').css({"height": "0", "padding-bottom": "0"});
                form.find('.post-create-controls').css('top', '15px');
                form.trigger('reset');
            });
        e.preventDefault();
    });
    
    
    $('body').on('click', '.postmediaCommentDelete', function (e) {
        var _this = $(this);
        var commentId = _this.attr('id');
        var comment_body = $("#modal-medias").find(".comments");
        var postId = _this.attr('postid');
        var media_comments_count = comment_body.find('.post-comment-row').length;
        var media_comments_total_count = comment_body.attr('data-total_cnt');
        var type = _this.data('type');
        var parent_id = _this.data('parent')
        

        $.confirm({
            title: 'Delete Comment!',
            content: 'Are you sure you want to delete this comment? <div class="mb-3"></div>',
            columnClass: 'col-md-5 col-md-offset-5',
            closeIcon: true,
            offsetTop: 0,
            offsetBottom: 500,
            scrollToPreviousElement:false,
            scrollToPreviousElementAnimate:false,
            buttons: {
                cancel: function () {},
                confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function () {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('media.commentdelete') }}",
                            data: {comment_id: commentId}
                        })
                        .done(function (res) {

                            $('#mediaCommentRow' + commentId).fadeOut();
                            comment_body.find(".comments__total").html((parseInt(media_comments_total_count)-1) + " Comments");
                            comment_body.find(".comments__number").html( (parseInt(media_comments_count)-1) + "/" + (parseInt(media_comments_total_count)-1) );
                            comment_body.attr('data-total_cnt', parseInt(media_comments_total_count)-1);
                            $('.' +postId+ '-comment-count').html(parseInt(media_comments_total_count)-1);
                            if (type == 1) {
                                $('#mediaCommentRow' + commentId).parent().remove();
                            } else {
                                if (_this.closest('.post-block').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                    $('#mediaCommentRow' + parent_id).find('.postCommentDelete').removeClass('d-none')
                                    $('#mediaCommentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                }
                            }
                        });
                    }
                }
            }
        });
    });
    
    $('body').on('click', '.postmediaCommentLikes', function (e) {
            var _this = $(this);
            commentId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('media.commentlikeunlike') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'yes') {
                            _this.find('i').addClass('fill');
                            _this.parent().find('.media-comment-likes-block').prepend('<span>' + result.name + '</span>')
                        } else if (result.status == 'no') {
                            _this.find('i').removeClass('fill');
                            _this.parent().find('.media-comment-likes-block span').each(function () {
                                if ($(this).text() == result.name)
                                    $(this).remove();
                            });
                        }
                        _this.find('.comment-like-count').html(result.count);



                    });
            e.preventDefault();
        });
        
    $('body').on('click', '.mediaCommentReply', function (e) {
       commentId = $(this).attr('id');
       $('#mediaReplyForm' + commentId).show();
       e.preventDefault();
    });

   $(document).on('change', '.media-file-input', function (e) {
       var comment_id = $(this).attr('data-comment_id')
           commentMediaUploadFile($(this), $(".mediaCommentReplyForm"+comment_id), comment_id);

    })

   $(document).on('keydown', '.media-comment-text', function (e) {
       if (e.keyCode == 13 && !e.shiftKey) {
           e.preventDefault();
       }
    })

    $(document).on('keyup', '.media-comment-text', function (e) {
        if (e.keyCode == 13 && !e.shiftKey) {
            var comment_id = $(this).attr('data-comment_id');
            replyMediaComment(comment_id)
        }
    })
    
    $('body').on('click', '.remove-media-comment-file', function (e) {
        var media_id = $(this).attr('data-media_id');
        $(this).closest('.medias').append('<input type="hidden" name="existing_medias[]" value="' + media_id + '">')
        $(this).parent().remove();
    });
      
      
    $('body').on('click', '.media-edit-comment', function (e) {
        var _this = $(this);

        var commentId = _this.attr('data-id');
        editMediaCommentMedia(commentId)
        var postId = _this.attr('data-media');
        _this.closest('.post-comment-text').find('.mediaCommentEditForm' + commentId).removeClass('d-none')
        _this.closest('.post-comment-text').find('.media-comment-text-' + commentId + ' p').addClass('d-none')
        
        comment_textarea_auto_height(_this.closest('.post-comment-text').find('.mediaCommentEditForm' + commentId + ' textarea')[0], 'edit')

        e.preventDefault();
    });

    $('body').on('click', '.edit-media-cancel-link', function (e) {
        var _this = $(this);

        var commentId = _this.attr('data-comment_id');
        $('.mediaCommentEditForm' + commentId).addClass('d-none')
        $('.media-comment-text-' + commentId + ' p').removeClass('d-none')

        e.preventDefault();
    });

    $('body').on('click', '.edit-media-comment-link', function (e) {
        var _this = $(this);
        var commentId = _this.attr('data-comment_id');
        var form = _this.closest('.mediaCommentEditForm' + commentId);
        
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;
        var comment_type = form.find("input[name='comment_type']").val();

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();
        $.ajax({
            method: "POST",
            url: "{{ route('media.commentedit') }}",
            data: values
        })
                .done(function (res) {
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    if (comment_type == 1) {
                        $('#mediaCommentRow' + commentId).parent().html(res);
                    } else {
                        $('#mediaCommentRow' + commentId).replaceWith(res);
                    }
                });
        e.preventDefault();
    });
    
    $('body').on('click', '.media-comment-likes-block', function (e) {
      var _this = $(this);
      commentId = $(this).attr('data-id');
      var like_user_count = _this.parent().find('.comment-like-count').text()
      $.ajax({
          method: "POST",
          url: "{{ route('media.commentlikeusers') }}",
          data: {comment_id: commentId}
      })
              .done(function (res) {
                  $('.post-comment-like-users').html(res)
                  $('#commentslikePopup').find('.comment-like-count').html(like_user_count)
                  $('#commentslikePopup').modal('show')
              });
      e.preventDefault();
  });
    //----END---- media modal comment



    });

    function comment_textarea_auto_height(element, type) {
        element.style.height = "5px";
        element.style.paddingBottom = '30px';
        if (type == 'reply') {
            jQuery(element).closest('.post-reply-block').find('.post-create-controls').css('top', (element.scrollHeight - 25) + "px")
        } else if (type == 'edit') {
            jQuery(element).closest('.post-edit-block').find('.post-create-controls').css('top', (element.scrollHeight - 24) + "px")
        } else {
            jQuery(element).closest('.post-regular-block').find('.post-create-controls').css('top', (element.scrollHeight - 25) + "px")
        }
        element.style.height = (element.scrollHeight) + "px";
    }
    
      function editPostCommentMedia(id) {
        $('*[data-id="commenteditfile'+id+'"]').on('change', function () {
            commentMediaUploadFile($(this), $(".commentEditForm" + id), id);
        });
    }
    
    function editTripCommentMedia(id) {
        $('*[data-id="commenteditfile'+id+'"]').on('change', function () {
            commentMediaUploadFile($(this), $(".tripCommentEditForm" + id), id);
        });
    }
    
    function editTripModalCommentMedia(id) {
        $('*[data-id="tripcommentmodaleditfile'+id+'"]').on('change', function () {
            commentMediaUploadFile($(this), $(".tripModalEditForm" + id), id);
        });
    }
    
    function editEventModalCommentMedia(id) {
        $('*[data-id="commentmodaleditfile'+id+'"]').on('change', function () {
            commentMediaUploadFile($(this), $(this).closest(".eventModalEditForm" + id), id);
        });
    }
    
    function editEventCommentMedia(id) {
        $('*[data-id="eventcommenteditfile'+id+'"]').on('change', function () {
            commentMediaUploadFile($(this), $(this).closest(".eventCommentEditForm" + id), id);
        });
    }
    
    function editReportCommentMedia(id) {
        $('*[data-id="commenteditfile'+id+'"]').on('change', function () {
            commentMediaUploadFile($(this), $(".commentEditForm" + id), id);
        });
    }
    
    function editReportModalCommentMedia(id) {
        $('*[data-id="commentmodaleditfile'+id+'"]').on('change', function () {
            commentMediaUploadFile($(this), $(".reportModalEditForm" + id), id);
        });
    }
    
    function editMediaCommentMedia(id) {
        $('*[data-id="mediacommenteditfile'+id+'"]').on('change', function () {
            commentMediaUploadFile($(this), $(".mediaCommentEditForm" + id), id);
        });
    }

   

    $(document).on('click', '[data-tab]', function (e) {
        var Item = $(this).attr('data-tab');
        $(this).siblings('[data-content]').css('display', 'none');
        $('[data-content=' + Item + ']').css('display', 'block');
        e.preventDefault();
         Comment_Show.init($(this).closest('.post'));
    });
    $(document).on('click', '[data-tab_reply]', function (e) {
        var Item = $(this).attr('data-tab_reply');
        $('[data-content_reply]').css('display', 'none');
        $('[data-content_reply=' + Item + ']').fadeIn();
        e.preventDefault();
    });


    $(document).on('click', 'i.fa-camera', function () {
        $("#createPosTxt").click();
        var target = $(this).attr('data-target');
        var target_el = $(this).parent().find('*[data-id="'+target+'"]');
        target_el.trigger('click');
    })
    
    function replyMediaComment(id) {
        var form = $('.mediaCommentReplyForm' + id);
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();

        $.ajax({
            method: "POST",
            url: "{{ route('media.commentreply') }}",
            data: values
        })
        .done(function (res) {
            form.parent().parent().before(res);
            form.find('.medias').find('.removeFile').each(function () {
                $(this).click();
            });
            $('#mediaCommentRow' + id).find('.postCommentDelete').addClass('d-none');
            $('#mediaCommentRow' + id).find('.delete-line').addClass('d-none');
            form.find('textarea').val('');
            form.find('textarea').css({"height": "0", "padding-bottom": "0"});
            form.find('.post-create-controls').css('top', '15px');
            form.find('.medias').empty();

        });
    }
    
    
    
    function replyPostComment(id) {
        var form = $('.commentReplyForm' + id);
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();

        $.ajax({
            method: "POST",
            url: "{{ route(session('place_country') . '.postcommentreply') }}",
            data: values
        })
                .done(function (res) {
                    form.parent().parent().before(res);
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    $('#commentRow' + id).find('.postCommentDelete').addClass('d-none');
                    $('#commentRow' + id).find('.delete-line').addClass('d-none');
                    form.find('textarea').val('');
                    form.find('textarea').css({"height": "0", "padding-bottom": "0"});
                    form.find('.post-create-controls').css('top', '15px');
                    form.find('.medias').empty();

                });
    }
    
    function replyReportComment(form) {
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();

        $.ajax({
            method: "POST",
            url: "{{url('reports/comment')}}",
            data: values
        })
                .done(function (res) {
                    form.parent().parent().before(res);
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    form.find('textarea').val('');
                    form.find('textarea').css({"height": "0", "padding-bottom": "0"});
                    form.find('.post-create-controls').css('top', '15px');
                    form.find('.medias').empty();

                });
    }
    
    
    function replyTripComment(obj, id) {
        var form = obj.closest('.tripCommentReplyForm' + id);
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();

        $.ajax({
            method: "POST",
            url: "{{ route('trip.postcommentreply') }}",
            data: values
        })
                .done(function (res) {
                    $('.tripCommentReplyForm' + id).parent().parent().before(res);
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    $('#commentRow' + id).find('.postCommentDelete').addClass('d-none');
                    $('#commentRow' + id).find('.delete-line').addClass('d-none');
                    form.find('textarea').val('');
                    form.find('textarea').css({"height": "0", "padding-bottom": "0"});
                    form.find('.post-create-controls').css('top', '15px');
                    form.find('.medias').empty();

                });
    }
    
    function replyEventComment(obj, id) {
        var form = obj.closest('.eventCommentReplyForm' + id);
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();

        $.ajax({
            method: "POST",
            url: "{{ route(session('place_country') . '.eventcommentreply') }}",
            data: values
        })
                .done(function (res) {
                    $('.eventCommentReplyForm' + id).parent().parent().before(res);
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    $('.commentRow' + id).find('.event-comment-delete').addClass('d-none');
                    $('.commentRow' + id).find('.delete-line').addClass('d-none');
                    form.find('textarea').val('');
                    form.find('textarea').css({"height": "0", "padding-bottom": "0"});
                    form.find('.post-create-controls').css('top', '15px');
                    form.find('.medias').empty();

                });
    }
    
    //Comment filter function
    function commentSort(type, obj, reload_obj){
        if($(obj).hasClass('active'))
            return;
        
        $(obj).parent().find('button').removeClass('active');
        $(obj).addClass('active');
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $(obj).closest('.post').find('.post-comment-wrapper');
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if(type == "Top")
                {
                    if ($(list[i]).attr('topsort') < $(list[i + 1]).attr('topsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "New")
                {
                    if ($(list[i]).attr('newsort') < $(list[i + 1]).attr('newsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }
      
        Comment_Show.reload(reload_obj); 
    }
    
//Comment load more function 
var Comment_Show = (()=>{
    var total_rows = 0;
    var display_unit = 3;
    function init(obj){
        var target = $(obj).find('.sortBody');
        if(target.attr('travooo-comment-rows'))
        {
            return;
        }
        else
        {
           if(target.find(">div").hasClass('comment-not-fund-info')){
               return;
           }else{
            target.find(">div").hide();
            get_totalrows(target, true)
        }   
        }
    }

    function reload(obj)
    {
        var target = $(obj).find('.sortBody');
        target.find(">div").hide();
        target.find(">div").removeClass('displayed');
        
        var check_count = (target.find(">div").length == display_unit+1) ? true : false;
         
        get_totalrows(target, check_count);
    }

    function get_totalrows(obj, check_type)
    {
        this.total_rows = obj.find(">div").length; 
        
        if(this.total_rows>display_unit && check_type && obj.parent().find('a.load-more-comment-link').length == 0){
            var btn = $('<a href="javascript:;"/>').addClass('load-more-comment-link').text('Load more...').click(function(e){
                var l_total_comment = e.parent('.comment').find('.sortBody').attr('travooo-comment-rows');
                var l_length = e.parent('.comment').find('.sortBody').find('.displayed');
                console.log(l_total_comment );
                console.log(l_length);
               Comment_Show.inc(obj)
           });
            obj.after(btn);
       }
       
        obj.attr('travooo-comment-rows', this.total_rows);
        if(!obj.attr('travooo-current-page'))
            obj.attr('travooo-current-page', 1);
        show_rows(obj, obj.attr('travooo-current-page'));
    }

    function show_rows(obj, page)
    {
        this.total_rows = parseInt(obj.attr('travooo-comment-rows'));
        var showing_rows = parseInt(page) * display_unit;
        showing_rows = this.total_rows > showing_rows ? showing_rows : this.total_rows;
        for(var i = 0; i < showing_rows; i++)
        {
            obj.find(">div").eq(i).show().addClass("displayed");
        }

        var disnum = obj.find(".displayed").length;
        obj.parent().find('.comm-count-info').find('strong').html( disnum );
    }

    function increase_show(obj)
    {
        var current_page = parseInt(obj.attr('travooo-current-page'));
        obj.attr('travooo-current-page', (current_page + 1));
        get_totalrows(obj, false);
    }


    return  {
        init: init,
        inc: increase_show,
        reload: reload
    }
})();

// Comment media upload file
function commentMediaUploadFile(_this, form_obj, id) {

    if ((form_obj.find('.medias').children().length + _this[0].files.length) > 10)
    {
        alert("The maximum number of files to upload is 10.");
        _this.val("");
        return;
    }
    if (window.File && window.FileReader && window.FileList && window.Blob) { //check File API supported browser

        var data = _this[0].files;
        $.each(data, function (index, file) { //loop though each file

            var upload = new Upload(file);
            var uid = Math.floor(Math.random() * 1000000);
            if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) {

                var fRead = new FileReader();
                fRead.onload = (function (file) {
                    return function (e) {

                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element

                        var button = $('<span/>').addClass('close').addClass('removeFile').click(function () {     //create close button element
                            var filename = upload.getName();
                            commentFileDelete(filename, this, form_obj.find('*[data-id="pair'+id+'"]').val());
                        }).append('<span>&times;</span>');

                        var imgitem = $('<div>').attr('uid', uid).append(img); //create Image Wrapper element
                        imgitem = $('<div/>').addClass('img-wrap-newsfeed').append(imgitem).append(button);
                        form_obj.find('.medias').append(imgitem);
                        upload.doUpload(uid, form_obj.find('*[data-id="pair'+id+'"]').val());

                    };
                })(file);
                fRead.readAsDataURL(file); //URL representing the file's data.

            } else if (/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type)) {

                var fRead = new FileReader();
                fRead.onload = (function (file) {
                    return function (e) {

                        var video = $('<video style="object-fit:cover;" width="151" height="130" controls>' +
                                '<source src=' + e.target.result + ' type="video/mp4">' +
                                '<source src="movie.ogg" type="video/ogg">' +
                                '</video>');

                        var button = $('<span/>').addClass('close').addClass('removeFile').click(function () {     //create close button element
                            var filename = upload.getName();
                            commentFileDelete(filename, this, form_obj.find('*[data-id="pair'+id+'"]').val());
                        }).append('<span>&times;</span>');

                        var videoitem = $('<div/>').attr('attr', uid).append(video);
                        videoitem = $('<div/>').addClass('img-wrap-newsfeed').append(videoitem).append(button);
                        form_obj.find('.medias').append(videoitem);
                        upload.doUpload(uid, form_obj.find('*[data-id="pair'+id+'"]').val());
                    };
                })(file);
                fRead.readAsDataURL(file); //URL representing the file's data.

            }
        });
        form_obj.find('textarea').focus();
        _this.val("");

    } else {
        alert("Your browser doesn't support File API!"); //if File API is absent
    }
}

$('body').on('click', '.morecommentlink', function () {
    var moretext = "more";
    var lesstext = "less";
    if($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
    } else {
        $(this).addClass("less");
        $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
});

</script>
