<style>
    .gallery__item, .gallery__item img {
        width: 100%;
        height: 100%;
    }
    #modal-trips .comment__text {
      margin-top: 5px;
    }
    #modal-medias .trip__placeholder video {
      right: 0;
      bottom: 0;
      position: absolute;
      min-width: 100%;
      min-height: 100%;
      margin-bottom: 0;
    }
    #modal-medias .comment__content {
      width: calc(100% - 64px);
      overflow-wrap: break-word;
      margin-top: 3px;
    }
    #modal-medias .trip__cards-control {
      outline: none;
    }
    .slick-current .trip-card::after {
      content: none;
    }
    .events__item {
      z-index: unset;
    }
    .event-item:after {
      z-index:0;
    }
</style>
<!-- Report modal start -->
<div class="travelogs modal modal--light custom-modal" id="reports__modal">
    <button class="modal__close" type="button">
        <svg class="icon icon--close"><use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use></svg>
    </button>
    <div class="travelogs__items_reports">
    @if(isset($reportsinfo) AND count($reportsinfo)>0)
    @php $reports = $reportsinfo @endphp
    @endif
    @foreach($reports as $report)
    @if(isset($reportsinfo) AND count($reportsinfo)>0)
    @php $report = $report->report @endphp
    @endif
    <?php
        $cities = App\Models\ExpertsCities\ExpertsCities::with('cities')->get();
        $countries = App\Models\ExpertsCountries\ExpertsCountries::with('countries')->get();
        $expertise = [];
        if(count($cities) > 0 && !empty($report->author)){
            foreach($cities as $city){
                if($city->users_id == $report->author->id){
                    $expertise[] = $city->cities->transsingle->title; 
                }
            }
        }
        
        if(count($countries) > 0 && !empty($report->author)){
            foreach($countries as $country){
                if($country->users_id == $report->author->id){
                    $expertise[] = $country->countries->transsingle->title;
                }
            }
        }
        $expertise = array_unique($expertise);
    ?>
      <div class="travelogs__item">
        <div class="travelog-item">
          <div class="travelog-item__header">
            <div class="travelog-item__header-main">
              <h2 class="travelog-item__title"><a href="{{url_with_locale('reports/'.$report->id)}}">{{str_limit($report->title, 30)}}</a></h2>
              <div class="travelog-item__tags"><a class="travelog-item__tag" href="#"></a><a class="travelog-item__tag" href="#"> </a></div>
            </div>
            <div class="travelog-item__header-right"><button class="travelog-item__header-btn" type="button"><svg class="icon icon--share-2">
                  <use xlink:href="{{asset('assets3/img/sprite.svg#share-2')}}"></use>
                </svg>Share</button>
            </div>
          </div>
          <div class="travelog-item__img" style="background-image: url('{{@$report->cover[0]->val}}')"><svg class="icon icon--multiple-items">
              <use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use>
            </svg>
            <div class="travelog-item__user"><img class="travelog-item__user-avatar" data-src="{{check_profile_picture(@$report->author->profile_picture)}}" alt="{{@$report->author->name}}" title="" />
              <div class="travelog-item__user-content">
                <div class="travelog-item__user-name">By <a href="{{url_with_locale('profile/'.@$report->author->id)}}">{{@$report->author->name}}</a></div>
                <div class="travelog-item__user-desc">Expert in <?php echo '<strong>'.implode('</strong>, <strong>', $expertise).'</strong>';?></div>
              </div>
            </div>
          </div>
          <div class="travelog-item__footer"><button class="travelog-item__like-btn report_like_button" type="button" id="{{$report->id}}"><svg class="icon icon--like">
                <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
              </svg><span><strong>{{count(@$report->likes)}}</strong> Likes</span></button><button class="travelog-item__reaction-btn report_share_button" type="button" id="{{$report->id}}"><span class="emoji">🙂</span><span><strong>{{count(@$report->shares)}}</strong> Reactions</span></button><button class="travelog-item__comment-btn" type="button"><svg class="icon icon--comment">
                <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
              </svg>
              <div class="user-list">
                <div class="user-list__item">
                    @foreach($report->comments as $comment)
                    @if($loop->index == 3) @break @endif
                  <div class="user-list__user"><img class="user-list__avatar" data-src="{{check_profile_picture($comment->author->profile_picture)}}" alt="" role="presentation" /></div>
                    @endforeach
                  
                </div>
              </div><span><strong class="{{$report->id}}-all-comments-count">{{count(@$report->comments)}}</strong> Comments</span>
            </button></div>
          <div class="travelog-item__meta">
            <div class="travelog-item__meta-item">
              <div class="travelog-item__meta-label">Description</div>
              <div class="travelog-item__caption">
                <div class="travelog-item__text">
                    @php
                        $showChar = 200;
                        $ellipsestext = "...";
                        $moretext = "see more";
                        $lesstext = "see less";

                        $question = $report->description;

                        if(strlen($question) > $showChar) {

                            $c = substr($question, 0, $showChar);
                            $h = substr($question, $showChar, strlen($question) - $showChar);

                            $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink" style="color: #4080ff; font-size: 23px;">' . $moretext . '</a></span>';

                            $question = $html;
                        }
                    @endphp
                    {!!$question!!}
                </div>
                <svg class="icon icon--quote">
                  <use xlink:href="{{asset('assets3/img/sprite.svg#quote')}}"></use>
                </svg>
              </div>
            </div>
            <div class="travelog-item__meta-row">
              <div class="travelog-item__meta-item travelog-item__stats">
                <div class="travelog-item__meta-label">Stats</div>
                <div class="travelog-item__meta-value"><svg class="icon icon--eye">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#eye')}}"></use>
                  </svg><span>Over <strong>{{number_format(count(@$report->views) + 100)}}</strong> people read this Travelog</span></div>
              </div>
            </div>
            
          </div>
            <div class="travelog-item__meta">
                <div class="travelog-item__meta-row">
                    <div class="travelog-item__meta-item travelog-item__stats">
                        <div class="travelog-item__meta-label">Comments (<span class="{{$report->id}}-all-comments-count">{{@count($report->comments)}}</span>)</div>
                    </div>
                </div>
                <div class="reports-modal-comment-block comment-block-{{$report->id}}">
                    <div class="comments__items post-comment-wrapper sortBody">
                        @foreach($report->comments->take(5) as $comment)
                             @include('site.reports.partials.report_comment_block2')
                        @endforeach
                        <button class="comments__load-more report" data-id="{{ $report->id }}" type="button" @if(count($report->comments)<=5) style="display:none" @endif tabindex="0">More...</button>
                    </div>
                </div>
                @if(Auth::user())
                <div class="post-add-comment-block">
                    <div class="avatar-wrap">
                        <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt="" width="45px" height="45px">
                    </div>
                    <div class="post-add-com-inputs">
                        <form class="reportCommentForm" method="POST" data-id="{{$report->id}}"  data-type="2" autocomplete="off" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" data-id="pair{{$report->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                            <div class="post-create-block post-comment-create-block post-regular-block" tabindex="0">
                                <div class="post-create-input">
                                    <textarea name="text" data-id="reportmodalcommenttext" class="textarea-customize"
                                              style="display:inline;vertical-align: top;min-height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                                    <div class="medias report-media">
                                    </div>
                                </div>
                                <div class="post-create-controls">
                                    <div class="post-alloptions">
                                        <ul class="create-link-list">
                                            <li class="post-options">
                                                <input type="file" name="file[]" class="report-modal-comment-media-input" data-report_id="{{$report->id}}" data-id="commentmodalfile{{$report->id}}" style="display:none" multiple>
                                                <i class="fa fa-camera click-target" aria-hidden="true" data-target="commentmodalfile{{$report->id}}"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <input type="hidden" name="report_id" value="{{$report->id}}"/>
                            <button type="submit" class="btn btn-primary d-none"></button>
                        </form>

                    </div>
                </div>

                @endif
            </div>
        </div>
      </div>
    @endforeach
    </div>
    <div class="travelog-carousel_reports">
    @if(isset($reportsinfo) AND count($reportsinfo)>0)
    @php $reports = $reportsinfo @endphp
    @endif
    @foreach($reports as $report)
    @if(isset($reportsinfo) AND count($reportsinfo)>0)
    @php $report = $report->report @endphp
    @endif
      <div class="travelog-carousel__item" data-id="{{ $report->id }}">
        <div class="travelog-carousel__img" style="background-image: url('{{@$report->cover[0]->val}}');background-size:cover" alt="#">
          <div class="travelog-carousel__title">{{str_limit($report->title, 30)}}</div>
          <div class="travelog-carousel__author">By <a href="{{url_with_locale('profile/'.@$report->author->id)}}">{{@$report->author->name}}</a></div>
        </div>
      </div>
    @endforeach
    </div>
</div>
<!-- Report modal end -->
<!-- Trip modal start -->
<?php
    // $trip_plans_collection = App\Models\TripPlaces\TripPlaces::where('places_id', $place->id)
    //     ->orderBy('id', 'DESC')
    //     // ->groupBy('trips_id')
    //     ->get();
        
?>
@if(count($trip_plans_collection) > 0)
{{-- @if(false) --}}
  @foreach($trip_plans_collection as $tr_plan)
    @if($loop->index==1) @break @endif
    <?php
        $f_plan = $tr_plan;
        $points = [];
        $polylines = "";
        $center = [];
        $zoom = 8;
        $markers = [];
        $waypoint = [];
        $lats = [];
        $lngs = [];
    ?>
    @if(is_object($f_plan))
    @foreach($f_plan->places as $pl)
      @php
        $markers[] = "markers=anchor:center|icon:".asset("assets2/image/marker_s.png")."|".$pl->lat.",".$pl->lng;
        $points[] = $pl->lat . "," . $pl->lng;
        $waypoint[] = "via:".$pl->lat . "," . $pl->lng;
        $lats[] = $pl->lat;
        $lngs[] = $pl->lng;
      @endphp
    @endforeach
    @endif
      @php
        $newwaypoint = $waypoint;
        $origin = @$points[0];
        $destin = @$points[count($points)-1];
        $url = 'https://maps.googleapis.com/maps/api/directions/json?origin='.$origin.'&destination='.$destin.'&waypoints=optimize:true|'.implode("|", $newwaypoint).'&mode=driving&key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        $googleDirection = json_decode($result, true);
        try {
            $polyline = urlencode($googleDirection['routes'][0]['overview_polyline']['points']);
            $polylines = "enc:".$polyline;
            
            $lat = ($googleDirection['routes'][0]['bounds']['southwest']['lat'] + $googleDirection['routes'][0]['bounds']['northeast']['lat']) / 2;
            $lng = ($googleDirection['routes'][0]['bounds']['southwest']['lng'] + $googleDirection['routes'][0]['bounds']['northeast']['lng']) / 2;
            $center[0] = isset($center[0]) ? ($center[0] + $lat) / 2 : $lat;
            $center[1] = isset($center[1]) ? ($center[1] + $lng) / 2 : $lng;

            $west = $googleDirection['routes'][0]['bounds']['southwest']['lng'];
            $east = $googleDirection['routes'][0]['bounds']['northeast']['lng'];
            
            $angle = $east - $west;
            if ($angle < 0) {
                $angle += 360;
            }
            $zoom = @floor(log(712 * 360 / $angle / 256) / log(2)) - 1;
        } catch (\Throwable $th) {
            if(count($lats)>0) {
                $center[0] = array_sum($lats) / count($lats);
                $center[1] = array_sum($lngs) / count($lngs);

                $angle = max($lngs) - min($lngs);
                if ($angle < 0) {
                    $angle += 360;
                }
                $zoom = @floor(log(712 * 360 / $angle / 256) / log(2)) - 2;
            }
            $polylines = "geodesic:true|".implode('|', $points);
        }
        
      @$center = count($center) > 0 ? $center : [$f_plan->places[0]->lat , $f_plan->places[0]->lng];
      $zoom = $zoom === INF ? 14 : $zoom ;
      $markers = implode("&", $markers);
    @endphp
<div class="trip modal modal--light custom-modal mobile-helper_trips" id="modal-trips">
    <button class="modal__close" type="button">
        <svg class="icon icon--close"><use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use></svg>
    </button>
    <div class="trip__container">
      <div class="trip__inner">
        @include('site.place2.partials._trip_inner4modal', ['plan'=>$f_plan, 'center'=>$center, 'zoom'=>$zoom, 'markers'=>$markers, 'polylines'=>$polylines])
      </div>
    </div>
    <div class="trip__cards" style='margin-top: 15px;height: 215px;'>
      <div class="trip__cards-items">
      @foreach($trip_plans_collection as $tr_plan)
      <?php
          $plan = $tr_plan;
      ?>
      @if(is_object($plan))
        <div class="trip__card" onclick="getTripdata4tripmodal({{@$plan->id}}, this)" style='height: 175px;'>
          <div class="trip-card">
            <div class="trip-card__img-wrap"><img class="trip-card__img" style="height: 140px;" data-src="{{get_plan_map($plan, '268', '140')}}" alt="#" title="" />
              <div class="trip-card__header">
                <div class="trip-card__likes"><svg class="icon icon--like-2">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#like-2')}}"></use>
                  </svg>{{ count($plan->likes) }}</div>
                
                <div class="trip-card__comments"><svg class="icon icon--comment">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
                  </svg>{{ count($plan->comments) }}</div>
              </div>
              <?php
                $budget = $plan->_attribute(\App\Models\TripPlans\TripPlans::ATTRIBUTE_BUDGET);
              ?>
              <div class="trip-card__info">
                <div class="trip-card__info-item">Destinations <strong>{{ $plan->trip_places_by_active_version->count() }}</strong></div>
                <div class="trip-card__info-item">Spent <strong>${{ $budget }}</strong></div>
              </div>
            </div>
            <div class="trip-card__title">{{ $plan->title }}</div>
          </div>
        </div>
        @endif
      @endforeach
      </div>
      <div class="trip__cards-side">
        <button class="trip__cards-btn" style="height:85px" type="button" onclick="javascript:location.href='{{route('trip.plan', 0)}}'">
          <div class="trip__cards-btn-icon"><svg class="icon icon--location-plus">
              <use xlink:href="{{asset('assets3/img/sprite.svg#location-plus')}}"></use>
            </svg></div>
          <div class="trip__cards-btn-text">Add Trip Plan</div>
        </button>
        <button class="trip__cards-btn" style="height:85px" type="button" onclick="javascript:location.href='{{route('profile.plans')}}'">
          <div class="trip__cards-btn-icon"><svg class="icon icon--multiple-items-2">
              <use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items-2')}}"></use>
            </svg></div>
          <div class="trip__cards-btn-text">View All</div>
        </button></div>
    </div>
</div>
@endforeach

@endif
<!-- Trip modal end -->
<!-- Event modal start -->
<div class="modal modal--light custom-modal" id="modal-events">
  <button class="modal__close" type="button">
    <svg class="icon icon--close"><use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use></svg>
  </button>
  <div class="events__items">
    
  </div>
</div>
<!-- Event modal end -->

<!-- Media modal start -->
@foreach($place->getMedias AS $photo)

@if($loop->index==1) @break @endif
@php
    $file_url = "https://s3.amazonaws.com/travooo-images2/" . $photo->url;
    $file_url_array = explode(".", $file_url);
    $ext = end($file_url_array);
    $allowed_video = array('mp4');
@endphp
<div class="trip modal modal--light custom-modal" id="modal-medias">
    <button class="modal__close" type="button">
        <svg class="icon icon--close"><use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use></svg>
    </button>
    <div class="trip__container">
      <div class="trip__inner">
        <div class="trip__main">
          <div class="trip__map" style="margin-bottom: 0;">
          @if(in_array($ext, $allowed_video))
            <div class="trip__placeholder">
                <video class="post-media__img video-object" controls data-id='{{$photo->id}}'>
                    <source src="{{$file_url}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>
          @else
            <div class="trip__placeholder" style="background-size:cover;background-image: url('https://s3.amazonaws.com/travooo-images2/th1100/{{$photo->url}}')"></div>
          @endif
          </div>
          
        </div>
        <div class="trip__side">
          <div class="trip__user" id="userInfo">
            
          </div>
            <div class="trip__subheader" id="caption">
            </div>
          <div class="trip__subheader">
            <div class="trip__reactions" id="mediaLikes">
              
            </div>
            <div class="trip__side-buttons" posttype="Media"><button class="trip__side-btn plan_share_button" type="button" id="{{ $photo->id }}">
                <div class="trip__side-btn-icon {{!Auth::check()?'open-login': ''}}"><svg class="icon icon--share">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
                  </svg></div>
                <div class="trip__side-btn-text">Share</div>
              </button>
              <button class="trip__side-btn {{!Auth::check()?'open-login': ''}}" type="button" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{ $photo->id }},this)">
                <div class="trip__side-btn-icon"><svg class="icon icon--flag">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#flag')}}"></use>
                  </svg></div>
                <div class="trip__side-btn-text">Report</div>
              </button></div>
          </div>
          <div class="comments media-comment-block" data-total_cnt="">
            <div class="comments__header">
              <div class="comments__total">{{ @count(@$photo->comments) }} Comments</div>
              <div class="comments__number">@if(@count(@$photo->comments) > 5) 5 @else {{@count(@$photo->comments)}} @endif / {{ @count(@$photo->comments) }}</div>
            </div>
            <div class="comments__items-wrap">
              <div class="comments__items post-comment-wrapper" data-simplebar="data-simplebar">
                <button class="comments__load-more media" @if(@count(@$photo->comments) <= 5) style="display:none;" @endif type="button" data-id="{{ $photo->id }}">Load more...</button>
              </div>
            </div>
          </div>
          <div class="trip__form media-comment-form-block" action="#" data-id="{{$photo->id}}">
            @if(Auth::check())
                <form class='mediModalCommentForm' method='post' action='#' autocomplete='off'>
                    <div class='post-add-comment-block'>
                        <div class='avatar-wrap'>
                            <img src='{{check_profile_picture(Auth::user()->profile_picture)}}' style='width:45px;height:45px;'>
                        </div>
                            <div class="post-add-com-inputs">
                                {{ csrf_field() }}
                                <input type="hidden" data-id="pair{{$photo->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                                <div class="post-create-block post-comment-create-block post-regular-block" id="createPostBlock" tabindex="0">
                                    <div class="post-create-input">
                                        <textarea name="text" id="mediacomment4modal" class="textarea-customize modal-texarea"
                                            style="display:inline;vertical-align: top;min-height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                                        <div class="medias modal-medias"></div>
                                    </div>
                                    <div class="post-create-controls">
                                        <div class="post-alloptions">
                                            <ul class="create-link-list">
                                                <li class="post-options">
                                                    <input type="file" name="file[]" class="modal-media-file-input" data-media_id="{{$photo->id}}" id="mediacommentfile" style="display:none" multiple>
                                                    <i class="fa fa-camera click-target" data-target="mediacommentfile"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-disabled d-none"></button>
                                    </div>
                                </div>
                            <input type="hidden" name="medias_id" value="{{$photo->id}}"/>
                        </div>
                    </div>
                </form>
            @endif
          </div>
        </div>
      </div>
    </div>
    <div class="trip__cards" style="margin-top: 15px;padding-top:10px;padding-bottom:10px;">
      <div class="trip__cards-items">
      
      @foreach($place->getMedias as $photo)
      
        @php
            $file_url = "https://s3.amazonaws.com/travooo-images2/" . $photo->url;
            $file_url_array = explode(".", $file_url);
            $ext = end($file_url_array);
            $allowed_video = array('mp4');
            $vkey = 1;
            if(!in_array($ext, $allowed_video)) {
              $file_url = "https://s3.amazonaws.com/travooo-images2/th1100/" . $photo->url;
              $vkey = 0;
            }
        @endphp
        <div class="trip__card" data-id="{{ $photo->id }}" data-video="{{ $vkey }}" data-src="{{ $file_url }}" onclick="getMediadata4mediamodal({{$photo->id}}, this, 'init')">
          <div class="trip-card">
            <div class="trip-card__img-wrap">
              @if(in_array($ext, $allowed_video))
                <video class="trip-card__img" style="width:100%;height:100%;">
                    <source src="{{$file_url}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
              @else
              <img class="trip-card__img" style="width:268px;height:150px;object-fit:cover" data-src="https://s3.amazonaws.com/travooo-images2/th180/{{$photo->url}}" alt="#" title="" />
              @endif
              <div class="trip-card__header">
                <div class="trip-card__likes"><i class="fa fa-heart fill" aria-hidden="true" dir="auto" style="color: #ff5a79;font-size: 14px;padding-right:3px;"></i> {{ @count($photo->likes) }}</div>
                
                <div class="trip-card__comments"><svg class="icon icon--comment">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
                    </svg><span class="{{$photo->id}}-comment-count">{{ @count(@$photo->comments) }}</span></div>
              </div>
              @if(in_array($ext, $allowed_video))
              <div class="gallery__icon-play"><svg class="icon icon--play-solid">
                <use xlink:href="{{asset('assets3/img/sprite.svg#play-solid')}}"></use>
              </svg></div>
              @endif
              @if($photo->title!='')
              <div class="trip-card__info">
                <div class="trip-card__info-item">{!! str_limit($photo->title, 32) !!}</div>
              </div>
              @endif
            </div>
            
            <div class="trip-card__title">
            @if(in_array($ext, $allowed_video))
              <?php
                $duration = getDuration($file_url);
              ?>
              Video by <strong>{{ @$photo->mediaUser->user->name }}</strong><br>
              <small>{{ $duration }} | {{intval($photo->views)}} views | {{ diffForHumans($photo->uploaded_at) }}</small></div>
            @else
              @if(is_object($photo->mediaUser)) Photo by <strong>{{ @$photo->mediaUser->user->name }}</strong><br>@endif
              @if($photo->uploaded_at)<small>{{ diffForHumans($photo->uploaded_at) }}</small>@endif
          </div>
            @endif
          </div>
        </div>
      @endforeach
      </div>
      <div class="trip__cards-side">
          @if(Auth::check())
            <button class="trip__cards-btn" type="button" onclick="javascript:$('.modal__close').click();$('.page-content__add-post-btn').click();">
          @else
            <button class="trip__cards-btn open-login" type="button">
          @endif
        
          <div class="trip__cards-btn-icon"><svg class="icon icon--location-plus">
              <use xlink:href="{{asset('assets3/img/sprite.svg#camera')}}"></use>
            </svg></div>
          <div class="trip__cards-btn-text">Add Media</div>
        </button>
        @if(Auth::check())
            <button class="trip__cards-btn" type="button" onclick="javascript:$('.modal__close').click();$('.page-content__add-post-btn').click();$('.posts-filter__link').eq(3).click();">
        @else
            <button class="trip__cards-btn open-login" type="button">
        @endif
            <div class="trip__cards-btn-icon"><svg class="icon icon--multiple-items-2">
              <use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items-2')}}"></use>
            </svg></div>
          <div class="trip__cards-btn-text">All Media</div>
        </button></div>
    </div>
</div>
@endforeach

<!-- Media modal end -->
