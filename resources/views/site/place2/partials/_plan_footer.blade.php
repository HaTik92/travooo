<div class="post__footer">
    <button class="post__like-btn plan_like_button" style="outline:none;margin-right: 0" type="button" id="{{$plan->id}}">
        <svg class="icon icon--like">
        <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
        </svg>
    </button>
    @if(count($plan->likes))<a href='#' class="plan_likes_modal" id='{{$plan->id}}'>@endif
        <span class="like-count-inner" id="plan_like_count_{{$plan->id}}"><strong>{{count($plan->likes()->whereNull('places_id')->get())}}</strong> Likes</span>
    @if(count($plan->likes))</a>@endif
    <button class="post__comment-btn" style="outline:none;" type="button" data-tab="tripscomment{{$plan->id}}">
        <svg class="icon icon--comment">
        <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
        </svg>
        <div class="user-list">
            <div class="user-list__item">
                @foreach($plan->comments()->groupBy('users_id')->get() AS $pco)
                <div class="user-list__user"><img class="user-list__avatar" src="{{check_profile_picture($pco->author->profile_picture)}}" alt="{{$pco->author->name}}" title="{{$pco->author->name}}" role="presentation" /></div>
                @endforeach
            </div>
        </div>
        <span><strong class="{{$plan->id}}-all-comments-count">{{count($plan->comments)}}</strong> Comments</span>
    </button>

    <button class="post__comment-btn" style="outline:none;" type="button" data-tab="post__media_3415" dir="auto">
        <span class="post_share_button" id="191" dir="auto">

            <i class="trav-share-icon" dir="auto"></i>

        </span>
        <span dir="auto">0 Shares</span>
    </button>
</div>