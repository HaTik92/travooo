<div class="posts-filter">
    @if(session('place_country') != 'place')
    <a class="posts-filter__link redirect-scroll" href="about">About</a>
    @endif
    <a class="posts-filter__link posts-filter__link--active" href="top">Top Posts</a>
    <a class="posts-filter__link " href="all">New Posts</a>    
    <a class="posts-filter__link" href="discussions">Discussions</a>
    <a class="posts-filter__link" href="media" >Media</a>
    <a class="posts-filter__link" href="tripplan">Trip Plans</a>
    {{-- <a class="posts-filter__link" href="travelmates">Mates</a> --}}
    <a class="posts-filter__link" href="events">Events</a>
    <a class="posts-filter__link" href="reports">Reports</a>
    @if(session('place_country') == 'place')
        <a class="posts-filter__link" href="reviews">Reviews</a>
    @endif
</div>