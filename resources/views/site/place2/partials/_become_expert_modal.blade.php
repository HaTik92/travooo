<div class="modal fade" id="become-expert-modal" tabindex="-1" role="dialog" aria-labelledby="likesModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header d-block">
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-title text-center" id="exampleModalLabel">Become a <strong>Travooo Expert</strong>
                    <br>
                    Climb up the leaderboards, get featured on destination pages, get your own dashboard, and more!
                </div>
            </div>
            <div class="modal-body">
                <div class="d-block">
                    <img src="{{asset('assets2/image/become-export-background.png')}}" alt="Travooo" dir="auto">
                </div>
                <br>
                <br>
                <div>
                    <label><strong>How I can be an expert?</strong></label>
                    <p style="line-height: 20px;">
                        To become an expert, there is only one main rule: Contribute. Share your experiences with other people by planning trips, creating comprehensive Travelogs, or even by just being social with others. Your actions on Travooo will tailor your pathway of becoming an expert. To get featured as an advisor on this page, engage more with the questions related to this place that are posted in the Discussion Panel. You can visit the Discussion Panel of this place by clicking the button below.
                    </p>
                </div>

            </div>
            <div class="modal-footer">
                <div style="display:inline-block; width: 100%;">
                    <div style=" text-align: center;">
                        <a href="/discussions?do=place&id={{$place_id}}" class="btn  btn--main" style="font-family: inherit;">Go to Discussion Panel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>