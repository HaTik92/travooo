<div class="post__footer">
    <button class="post__like-btn post_like_button" style="outline:none;" type="button" id="{{$post->id}}">
        <svg class="icon icon--like">
        <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
        </svg>
    </button>
    @if(count($post->likes))<a href='#' class="post_likes_modal" id='{{$post->id}}'>@endif
        <span class="like-count-inner" id="post_like_count_{{$post->id}}"><strong>{{count($post->likes)}}</strong> Likes</span>
    @if(count($post->likes))</a>@endif
    <button class="post__comment-btn" style="outline:none;" type="button" data-tab="post__media_{{$post->id}}">
        <svg class="icon icon--comment">
        <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
        </svg>
        <div class="user-list">
            <div class="user-list__item">
                @foreach($post->comments()->groupBy('users_id')->get() AS $pco)
                <div class="user-list__user"><img class="user-list__avatar" src="{{check_profile_picture($pco->author->profile_picture)}}" alt="{{$pco->author->name}}" title="{{$pco->author->name}}" role="presentation" /></div>
                @endforeach
            </div>
        </div>
        <span><strong class="{{$post->id}}-all-comments-count">{{count($post->comments)}}</strong> Comments</span>
    </button>

    <button class="post__like-btn report_share_button" type="button" id="">
        <svg class="icon icon--like" style="color: #4080ff">
            <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
        </svg>
        <span id="report_share_count_"><strong>0</strong> Shares</span>
    </button>
</div>