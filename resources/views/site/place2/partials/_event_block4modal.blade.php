<div class="events__item" data-id="{{ $evt->id }}">
    <div class="event-item">
        <div class="event-item__header">
        <div class="event-item__header-main">
            <div class="event-item__date">
            <div class="post__icon">
                <svg class="icon icon--calendar">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#calendar')}}"></use>
                </svg>
            </div>
            </div>
            <div class="event-item__title-wrap">
            <h2 class="event-item__title"><a href="#">{{$evt->title}}<svg class="icon icon--new-tab">
                    <use xlink:href="img/sprite.svg#new-tab"></use>
                </svg></a></h2>
            <div class="event-item__subtitle"><span class="label">Event</span> in {{ @$evt->address }}</div>
            </div>
        </div>
        <div class="event-item__header-right">
            <div class="event-item__side-buttons" posttype="Event">
            <!-- <button class="event-item__side-btn" type="button" tabindex="0">
                <div class="event-item__side-btn-icon"><svg class="icon icon--share">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#like-2')}}"></use>
                </svg></div>
                <div class="event-item__side-btn-text">Like</div>
            </button> -->

            <button class="event-item__side-btn event_share_button" type="button" tabindex="0" data-id="{{$evt->id}}">
                <div class="event-item__side-btn-icon"><svg class="icon icon--share">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
                </svg></div>
                <div class="event-item__side-btn-text ">Share</div>
            </button>
                
            <button class="event-item__side-btn" type="button" tabindex="0" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$evt->id}},this)">
                <div class="event-item__side-btn-icon"><svg class="icon icon--flag">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#flag')}}"></use>
                </svg></div>
                <div class="event-item__side-btn-text">Report</div>
            </button>
            </div>
        </div>

        </div>
        <div class="event-item__map">
        <img data-src="{{"https://maps.googleapis.com/maps/api/staticmap?center=".$evt->lat.",".$evt->lng."&zoom=6&size=616x287&scale=2&maptype=satellite&markers=color:green%7Clabel:".substr($evt->title,0,1)."%7C".$evt->lat.",".$evt->lng. "&key=".env('GOOGLE_MAPS_KEY')}}"/>
        <!-- <button class="event-item__expand" type="button"><svg class="icon icon--expand">
            <use xlink:href="img/sprite.svg#expand"></use>
            </svg>
        </button> -->
        </div>
        <div class="event-item__footer">
            
            <button class="post__like-btn event_like_button" style="outline:none;" type="button" data-id="{{ $evt->id }}">
                <div class="emoji" dir="auto">
                    <i class="fa fa-heart 
                        @if(i_like('event', $evt->id)) 
                        red 
                        @endif" aria-hidden="true" dir="auto" style="font-size: 14px;padding-right:3px;"></i>
                </div>
                <span>
                    <strong>{{@count($evt->likes)}}</strong> Likes
                </span>
            </button>
            
            <button class="event-item__comment-btn" type="button" tabindex="0" style="outline:none;"><svg class="icon icon--comment">
            <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
            </svg>
            @if(count($evt->comments) > 0)
            <div class="user-list">
            <div class="user-list__item">
                
                    @foreach($evt->comments as $com)
                        @if($loop->index > 3) @break @endif
                            <div class="user-list__user"><img class="user-list__avatar" src="{{check_profile_picture($com->author->profile_picture)}}" alt="" role="presentation"></div>
                    @endforeach
                
            </div>
            </div>
            @endif<span><strong class="{{$evt->id}}-all-comments-count">{{count($evt->comments)}}</strong> Comments</span>
        </button></div>
        <div class="event-item__meta">
        <div class="event-item__meta-row">
            <div class="event-item__meta-item">
            <div class="event-item__meta-label">Short description</div>
            <div class="event-item__meta-value">
            @php
                $showChar = 200;
                $ellipsestext = "...";
                $moretext = "see more";
                $lesstext = "see less";

                $content = $evt->description;

                if(strlen($content) > $showChar) {

                    $c = substr($content, 0, $showChar);
                    $h = substr($content, $showChar, strlen($content) - $showChar);

                    $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                    $content = $html;
                }
            @endphp
            {!!$content!!}  
            </div>
            </div>
            <div class="event-item__meta-item">
            <div class="event-item__meta-label">Address</div>
            <div class="event-item__meta-value">{{ @$evt->address }}</div>
            </div>
            <div class="event-item__meta-item">
            <div class="event-item__meta-label">Duration</div>
            <div class="event-item__meta-value"><strong>{{date('j M ', strtotime($evt->start))}} - {{date('j M ', strtotime($evt->end))}}</strong></div>
            </div>
            
            <div class="event-item__meta-item eventcomment-block comment-block-{{ $evt->id }} event-modal-comment-content" style="flex-basis: 100%;">
                <div class="event-item__meta-label">Comments (<strong class="{{$evt->id}}-all-comments-count">{{count($evt->comments)}}</strong>)</div>
                <div class="comments">
                <div class="comments__items post-comment-wrapper sortBody">
                    @foreach($evt->comments->take(5) as $comment)
                        @include('site.place2.partials.event_comment_block')
                    @endforeach
                    <button class="comments__load-more event" data-id="{{ $evt->id }}" type="button" @if(count($evt->comments)<=5) style="display:none" @endif tabindex="0">More...</button>
                </div>
                @if(Auth::user())
                    <div class="post-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                                 style="width:45px;height:45px;">
                        </div>
                        <div class="post-add-com-inputs">
                            <form class="eventscomment" method="POST" action="{{url("new-comment") }}" data-id="{{$evt->id}}" autocomplete="off" enctype="multipart/form-data">
                                <input type="hidden" data-id="pair{{$evt->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                                <div class="post-create-block post-comment-create-block post-regular-block" id="createPostBlock" tabindex="0">
                                    <div class="post-create-input">
                                        <textarea name="text" data-id="eventsmodalcommenttext" class="textarea-customize"
                                                  style="display:inline;vertical-align: top;height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                                        <div class="medias place-media">
                                        </div>
                                    </div>

                                    <div class="post-create-controls">
                                        <div class="post-alloptions">
                                            <ul class="create-link-list">
                                                <li class="post-options">
                                                    <input type="file" name="file[]" class="event-modal-media-input" data-event_id="{{$evt->id}}" data-id="eventmodalcommentfile{{$evt->id}}" style="display:none" multiple>
                                                    <i class="fa fa-camera click-target" data-target="eventmodalcommentfile{{$evt->id}}"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-disabled d-none"></button>
                                    </div>
                                </div>
                                <input type="hidden" name="event_id" value="{{$evt->id}}"/>
                            </form>
                        </div>
                    </div>
                @endif
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>