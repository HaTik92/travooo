<?php
$comment_childs = \App\Models\Posts\PostsComments::where('parents_id', '=', $ppc->id)->orderby('created_at','DESC')->get();
?>
<div topSort="{{count($ppc->likes) + count($comment_childs)}}" newSort="{{strtotime($ppc->created_at)}}">
<div class="post-comment-row news-feed-comment commentRow{{$ppc->id}}">
    <div class="post-com-avatar-wrap">
        <img src="{{check_profile_picture($ppc->author->profile_picture)}}" alt="">
    </div>
    <div class="post-comment-text">
        <div class="post-com-name-layer">
            <a href="{{url_with_locale('profile/'.$ppc->author->id)}}" class="comment-name">{{$ppc->author->name}}</a>
            <div class="post-com-top-action">
                <div class="dropdown ">
                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                        @if($ppc->author->id==Auth::user()->id)
                        <a href="javascript:;" class="dropdown-item post-edit-comment"  data-id="{{$ppc->id}}" data-post="{{$post->id}}">
                            <span class="icon-wrap">
                               <i class="trav-pencil" aria-hidden="true"></i>
                            </span>
                            <div class="drop-txt comment-edit__drop-text">
                                <p><b>@lang('profile.edit')</b></p>
                            </div>
                        </a>
                        @else
                            <a href="#" class="dropdown-item" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$ppc->id}},this)">
                                <span class="icon-wrap">
                                    <i class="trav-flag-icon-o"></i>
                                </span>
                                <div class="drop-txt comment-report__drop-text">
                                    <p><b>@lang('profile.report')</b></p>
                                </div>
                            </a>
                        @endif
                        @if($ppc->author->id==Auth::user()->id && count($comment_childs) == 0)
                        <a href="javascript:;" class="dropdown-item post-comment-delete" id="{{$ppc->id}}" postid="{{$post->id}}" data-del-type="1">
                            <span class="icon-wrap">
                                <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;">
                            </span>
                            <div class="drop-txt comment-delete__drop-text">
                                <p><b>Delete</b></p>
                            </div>
                        </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="comment-txt comment-text-{{$ppc->id}}">
              @php
                $showChar = 100;
                $ellipsestext = "...";
                $moretext = "see more";
                $lesstext = "see less";

                $content = $ppc->text;

                if(strlen($content) > $showChar) {

                    $c = substr($content, 0, $showChar);
                    $h = substr($content, $showChar, strlen($content) - $showChar);

                    $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                    $content = $html;
                }
            @endphp
            <p>{!!$content!!}</p>
            <form class="commentEditForm{{$ppc->id}} comment-edit-form d-none" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
            {{ csrf_field() }}<input type="hidden" data-id="pair{{$ppc->id}}" name="pair" value="{{uniqid()}}"/>
            <div class="post-create-block post-comment-create-block report-comment-block post-edit-block" tabindex="0">
                <div class="post-create-input">
                    <textarea name="text" id="text{{$ppc->id}}" class="textarea-customize" oninput="comment_textarea_auto_height(this, 'edit')" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="@lang('comment.write_a_comment')">{!!$ppc->text!!}</textarea>
                    <div class="medias place-media">
                         @if(is_object($ppc->medias))
                            @foreach($ppc->medias AS $photo)
                                @php
                                $file_url = $photo->media->url;
                                $file_url_array = explode(".", $file_url);
                                $ext = end($file_url_array);
                                $allowed_video = array('mp4');
                                @endphp
                                <div class="img-wrap-newsfeed">
                                    <div>
                                        @if(in_array($ext, $allowed_video))
                                        <video style="object-fit: cover" class="thumb" controls>
                                            <source src="{{$file_url}}" type="video/mp4">
                                        </video>
                                    @else
                                    <img class="thumb" src="{{$file_url}}" alt="" >
                                    @endif
                                    </div>
                                    <span class="close remove-media-file" data-media_id="{{$photo->media->id}}">
                                        <span>×</span>   
                                    </span></div>
                            @endforeach
                        @endif
                    </div></div>
                <div class="post-create-controls">
                    <div class="post-alloptions">
                        <ul class="create-link-list">
                            <li class="post-options">
                                <input type="file" name="file[]" class="commenteditfile" data-id="commenteditfile{{$ppc->id}}"  style="display:none" multiple>
                                <i class="fa fa-camera click-target" data-target="commenteditfile{{$ppc->id}}"></i>
                            </li>
                        </ul>
                    </div>
                <div class="comment-edit-action">
                    <a href="javascript:;" class="edit-cancel-link"  data-comment_id="{{$ppc->id}}">Cancel</a>
                    <a href="javascript:;" class="edit-post-comment-link" data-comment_id="{{$ppc->id}}">Post</a>
                </div>
                </div>
            </div>
            <input type="hidden" name="post_id" value="{{$post->id}}"/>
            <input type="hidden" name="comment_type" value="1">
            <input type="hidden" name="comment_id" value="{{$ppc->id}}">
            <button type="submit"  class="d-none"></button>
        </form>
        </div>
        <div class="post-image-container">
            @if(is_object($ppc->medias))
                @php
                    $index = 0;
                    
                @endphp
                @foreach($ppc->medias AS $photo)
                    @php
                        $index++;
                        $file_url = $photo->media->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp
                    @if($index % 2 == 1)
                    <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;">
                    @endif
                        <li style="overflow: hidden;margin:1px;">
                            @if(in_array($ext, $allowed_video))
                            <video style="object-fit: cover" width="192" height="210" controls>
                                <source src="{{$file_url}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="{{$file_url}}" data-lightbox="comment__media{{$ppc->id}}">
                                <img src="{{$file_url}}" alt="" style="width:192px;height:210px;object-fit: cover;">
                            </a>
                            @endif
                        </li>
                    @if($index % 2 == 0)
                    </ul>
                    @endif
                @endforeach
            @endif

        </div>


        <div class="comment-bottom-info">
            <div class="comment-info-content">
                <div class="dropdown">
                     <a href="#" class="post_comment_like like-link dropbtn" id="{{$ppc->id}}"><i class="fa fa-heart {{($ppc->likes()->where('users_id', Auth::user()->id)->first())?'fill':''}}" aria-hidden="true"></i> <span class="comment-like-count">{{count($ppc->likes)}}</span></a>
                    @if(count($ppc->likes))
                        <div class="dropdown-content comment-likes-block" data-id="{{$ppc->id}}">
                            @foreach($ppc->likes()->orderBy('created_at', 'DESC')->take(7)->get() as $like)
                                <span>{{$like->author->name}}</span>
                            @endforeach
                             @if(count($ppc->likes)>7)
                            <span>...</span>
                            @endif
                        </div>
                    @endif
                </div>
                <a href="#" class="reportCommentReply reply-link" id="{{$ppc->id}}" data-tab_reply="comment__reply{{$ppc->id}}">Reply</a>
                <span class="com-time"><span class="comment-dot"> · </span>{{diffForHumans($ppc->created_at)}}</span>
            </div>
        </div>
    </div>
</div>
@if(count($comment_childs) > 0)
    @foreach($comment_childs as $child)
        @include('site.place2.partials._post_comment_reply_block')
    @endforeach
@endif
@if(Auth::user())
<div class="post-add-comment-block replyForm{{$ppc->id}}" style="padding-top:0px;padding-left: 59px;display:none;" data-content_reply="comment__reply{{$ppc->id}}">
    <div class="avatar-wrap" style="padding-right:10px">
        <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt="" style="width:35px !important;height:35px !important;">
    </div>
    <div class="post-add-com-inputs">
        <form class="commentReplyForm{{$ppc->id}}" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" data-id="pair{{$ppc->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
            <div class="post-create-block post-comment-create-block report-comment-block post-reply-block" tabindex="0">
                <div class="post-create-input">
                    <textarea name="text" id="{{$ppc->id}}" class="post-reply-text textarea-customize" oninput="comment_textarea_auto_height(this, 'reply')" style="display:inline;vertical-align: top;min-height:50px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                    <div class="medias place-media">
                    </div>
                </div>

                <div class="post-create-controls">
                    <div class="post-alloptions">
                        <ul class="create-link-list">
                            <li class="post-options">
                                <input type="file" name="file[]" class="post-comment-reply-media-input" data-comment-id="{{$ppc->id}}" id="commentreplyfile{{$ppc->id}}" style="display:none" multiple>
                                <i class="fa fa-camera click-target" data-target="commentreplyfile{{$ppc->id}}"></i>
                            </li>
                        </ul>
                    </div>

                    <button type="submit" class="btn btn-primary d-none"></button>
                    
                </div>
                
            </div>
            <input type="hidden" name="post_id" value="{{$post->id}}"/>
            <input type="hidden" name="comment_id" value="{{$ppc->id}}">
        </form>
    </div>
</div>

@endif

</div>
