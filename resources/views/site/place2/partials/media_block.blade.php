<!-- MEDIA WITHOUT TEXT -->
<div class="post" filter-tab="#media" onclick="modal_blog_show(this, event, 'media', {{ $media->id }})">
    <div class="post__header">

        @if(is_object($media->mediaUser))
        <img class="post__avatar" src="{{check_profile_picture(@$media->mediaUser->user->profile_picture)}}" alt="" role="presentation">
        @endif
        <div class="post__title-wrap">
            @if(is_object($media->mediaUser))
            <a class="post__username" href="{{url_with_locale('profile/'.$media->mediaUser->users_id)}}">{{@$media->mediaUser->user->name}}</a>
                {!! get_exp_icon($media->mediaUser->user) !!}
            @else
            <i>Anonymous User</i>
            @endif
            <div class="post__posted">Uploaded a <strong>photo</strong> @if($media->uploaded_at){{diffForHumans($media->uploaded_at)}}@endif</div>
        </div>
        <div class="dropdown">
            <button class="post__menu" type="button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="icon icon--angle-down">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                </svg>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Media">
                @include('site.home.partials._info-actions', ['post'=>$media])

            </div>
        </div>
    </div>
    <div class="post__content">
        <div class="post-media">
            <div class="post-media__row">

                @php
                    $file_url = $media->url;
                    $file_url_array = explode(".", $file_url);
                    $ext = end($file_url_array);
                    $allowed_video = array('mp4');
                @endphp
                @if(in_array($ext, $allowed_video))
                <video class="post-media__img" controls>
                    <source src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
                @else
                <!-- <a href="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" data-lightbox="media"> -->
                    <img class="post-media__img" src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" alt="" role="presentation">
                <!-- </a> -->
                @endif

            </div>
        </div>
    </div>
    <div class="post__footer">
        <button class="post__like-btn post_like_button" style="outline:none;" type="button" id="{{$media->id}}">
            <svg class="icon icon--like">
                <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
            </svg>
            <span id="post_like_count_{{$media->id}}"><strong>{{count($media->likes)}}</strong> Likes</span>
        </button>
        <button class="post__comment-btn" style="outline:none;" type="button" data-tab="_post__media_{{$media->id}}">
            <svg class="icon icon--comment">
                <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
            </svg>
            <div class="user-list">
                <div class="user-list__item">
                    @foreach($media->comments()->groupBy('users_id')->get() AS $pco)
                    <div class="user-list__user"><img class="user-list__avatar" src="{{check_profile_picture(@$pco->user->profile_picture)}}" alt="{{@$pco->user->name}}" title="{{@$pco->user->name}}" role="presentation" /></div>
                    @endforeach
                </div>
            </div>
            <span><strong>{{count($media->comments)}}</strong> Comments</span>
        </button>
    </div>
    <div class="comments" style="display:none" data-content="_post__media_{{$media->id}}">
        <div class="comments__header">
            <div class="comments__filter"><button class="comments__filter-btn comments__filter-btn--active" type="button">Top</button><button class="comments__filter-btn" type="button">New</button></div>
            <div class="comments__number">3 / 20</div>
        </div>
        <div class="comments__items post-comment-wrapper">
            @foreach($media->comments AS $comment)
                @include('site.home.partials.media_comment_block', ['comment' => $comment, 'post_object' => $media])
            @endforeach
            <!-- <button class="comments__load-more" type="button">Load more...</button> -->
        </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-input">

                <form class="media_comment_form" method="POST" mod="out" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                    <div class="post-block post-create-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" id="mediacommenttext" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:40px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias">
                                <!-- <div class="plus-icon" style="display: none;">
                                    <div>
                                        <span class="close" onclick="javascript:$('#commentfile').click();"><span style="font-size:110px;">+</span></span>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <!-- <li class="post-options">
                                        <input type="file" name="file[]" id="commentfile" style="display:none" multiple>
                                        <i class="trav-camera click-target" data-target="commentfile"></i>
                                    </li> -->
                                </ul>
                            </div>

                            <button type="submit" class="btn btn--sm btn--main">@lang('buttons.general.send')</button>

                        </div>

                    </div>
                    <input type="hidden" name="medias_id" value="{{$media->id}}"/>
                    <input type="hidden" name="pair" value="0"/>
                </form>

                </div>
            </div>

        @endif
    </div>
</div>
