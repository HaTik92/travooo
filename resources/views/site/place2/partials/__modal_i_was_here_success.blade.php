<div class="modal fade" id="iWasHere-sucess-modal" tabindex="-1" role="dialog" aria-labelledby="likesModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style=" background-image: url({{asset('assets2/image/i_was_here_popup_background.png')}});background-position: center;    height: 237px;">
            <div class="modal-header d-block" style="    border-bottom: none;">
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span style="color:#A7A7A7" aria-hidden="true">&times;</span>
                </button>
                <div class="modal-title text-center" style="margin-top: -10px;" id="exampleModalLabel">
                    <label ><h4 class="text-success"><strong style="font-weight:500">Success</strong></h4></label>
                </div>
            </div>
            <div class="modal-body">
                <div class="d-block text-center">
                    <img src="{{asset('assets2/image/i_was_here_popup_location.png')}}" alt="Travooo" dir="auto">
                </div>
            </div>
            <br>
            <div class="d-block text-center" style="margin-bottom:20px;">
                <span id="success-text">
                    Thank You! Your action has been registered.
                </span>
            </div>
        </div>
    </div>
</div>
</div>