<div id="add-post-discussion-block"
     class="card w-100"
     style="display: none;position: relative"
>
    <div class="card__content">
        <div class="row">
            <div class="col-sm-2">
                <img class="add-post__avatar" data-src="{{check_profile_picture($me->profile_picture)}}" alt="" role="presentation" />
            </div>
            <div class="col-sm-6 d-flex align-items-center">
                <div style="font-family:inherit;margin-left: -40px;"><span style="font-family:inherit;font-size:16px;">Have a question about <span style="font-weight: 500;">{{@$place->trans[0]->title}}</span>?</span></div>
            </div>
            <div class="col-sm-4">
                <button class="btn btn--main" style="float: right;font-family:inherit" data-toggle="modal" data-target="#askingPopup">Ask A Question</button>
            </div>
        </div>
    </div>
</div>