<?php
$post_object = $checkin;
$post_followers = null;
?>
@if(is_object($post_object))
<div class="post-block"  filter-tab="#event" @if(isset($search_result)) search-tab="#searched" @endif>
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <img src="@if(is_object($post_object->user)){{check_profile_picture($post_object->user->profile_picture)}}@endif" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">@if(is_object($post_object->user))
                    <a class="post-name-link" href="{{url('profile/'.$post_object->user->id)}}">{{$post_object->user->name}}</a>@endif
                    {!! get_exp_icon($post_object->user) !!}
                </div>
                <div class="post-info">
                    @lang('home.checked-in_at')

                    @if(isset($post_object->place) && is_object($post_object->place))
                        <a href="{{route('place.index', $post_object->place->id)}}"
                           class="link-place">{{$post_object->place->transsingle->title}}</a>
                    @elseif(isset($post_object->city) && is_object($post_object->city))
                        <a href="{{route('city.index', $post_object->city->id) }}"
                           class="link-place">{{$post_object->city->transsingle->title}}</a>
                    @elseif(isset($post_object->country) && is_object($post_object->country))
                        <a href="{{route('country.index', $post_object->country->id) }}" class="link-place"><img
                                    src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($post_object->country->iso_code)}}.png"
                                    alt="flag"
                                    style="width:21px;height:13px;"> {{$post_object->country->transsingle->title}}
                        </a>
                    @endif

                    {{diffForHumans($post_object->created_at)}}
                </div>
            </div>
        </div>
        @if(Auth::user()->id && Auth::user()->id==$post_object->users_id)
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Checkin">
                        @include('site.home.partials._info-actions', ['post'=>$post_object])

                        {{--@if(Auth::user()->id && Auth::user()->id==$post_object->users_id)
                            <a class="dropdown-item" href="#" onclick="post_delete('{{$post_object->id}}','{{@$post_type}}', this, event)">
                            <span class="icon-wrap">
                                <img src="{{asset('assets2/image/delete.svg')}}" style="width:20px;">
                            </span>
                                <div class="drop-txt">
                                    <p><b>@lang('buttons.general.crud.delete')</b></p>
                                    <p style="color:red">@lang('home.remove_this_post')</p>
                                </div>
                            </a>
                        @endif--}}
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="post-image-container post-follow-container">


                @if(isset($post_object->place) && is_object($post_object->place) && isset($post_object->place->getMedias[0]))
                <ul class="post-image-list">
                    <li>
                    <img src="https://s3.amazonaws.com/travooo-images2/th700/{{@$post_object->place->getMedias[0]->url}}" alt="">
                    </li>
                    </ul>
                    @elseif(isset($post_object->city) && is_object($post_object->city) && isset($post_object->city->getMedias[0]))
                    <ul class="post-image-list">
                     <li>
                    <img src="https://s3.amazonaws.com/travooo-images2/th700/{{@$post_object->city->getMedias[0]->url}}" alt="">
                    </li>
                    </ul>
                    @elseif(isset($post_object->country) && is_object($post_object->country) && isset($post_object->country->getMedias[0]))
                    <ul class="post-image-list">
                     <li>
                    <img src="https://s3.amazonaws.com/travooo-images2/th700/{{@$post_object->country->getMedias[0]->url}}" alt="">
                    </li>
                    </ul>
                    @endif



        <div class="post-follow-block">
            <div class="follow-txt-wrap">
                <div class="follow-flag-wrap">
                    @if(isset($post_object->country) && is_object($post_object->country))
                    <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($post_object->country->iso_code)}}.png" alt="" style="width:80px;height:60px;">
                    @endif

                </div>
                <div class="follow-txt">
                    <p class="follow-name">
                        @if(isset($post_object->place) && is_object($post_object->place))
                            <a href="{{route('place.index', $post_object->place->id) }}">
                        {{@$post_object->place->transsingle->title}}
                        </a>
                    @elseif(isset($post_object->city) && is_object($post_object->city))
                            <a href="{{ route('city.index', $post_object->city->id) }}">
                        {{@$post_object->city->transsingle->title}}
                    </a>
                    @elseif(isset($post_object->country) && is_object($post_object->country))
                            <a href="{{ route('country.index', $post_object->country->id) }}">
                        {{@$post_object->country->transsingle->title}}
                    </a>
                    @endif
                    </p>
                    <div class="follow-foot-info">
                        <i class="trav-talk-icon icon-grey-comment"></i>
                        <span
                            @if(isset($post_object->place) && is_object($post_object->place))
                                <?php $post_followers = $post_object->place->followers; ?>
                            @elseif(isset($post_object->city) && is_object($post_object->city))
                                <?php $post_followers = $post_object->city->followers; ?>
                            @elseif(isset($post_object->country) && is_object($post_object->country))
                                <?php $post_followers = $post_object->country->followers; ?>
                            @endif
                            @if(count($post_followers) > 0)
                                style="cursor: pointer" data-toggle="modal"
                                data-target="#checkinfollowers_{{$post_object->id}}"
                            @endif
                            >
                            {{count($post_followers)}} @lang('home.following_this')</span>
                    </div>
                </div>
            </div>

                    @if(isset($post_object->place) && is_object($post_object->place))
                    <div class="follow-btn-wrap check-follow-place" id="check_follow_place{{$post_object->place->id}}" data-id='{{$post_object->place->id}}'>
                        </div>
                    @elseif(isset($post_object->city) && is_object($post_object->city))
                    <div class="follow-btn-wrap check-follow-city" data-id='{{$post_object->city->id}}'>
                        </div>
                    @elseif(isset($post_object->country) && is_object($post_object->country))
                    <div class="follow-btn-wrap check-follow-country" data-id='{{$post_object->country->id}}'>
                        </div>
                    @endif



        </div>
    </div>
    <div class="post-footer-info">
        <div class="post-foot-block post-reaction" onclick="checkinlike({{ $post_object->id }}, this)">
            <span>
                <a href="#">
                    <img src="{{asset('assets2/image/like.svg')}}" alt="Like this post" style="width:16px;height:16px;margin:5px;vertical-align:baseline;">
                </a>
            </span>
            <span id="checkin_like_count_{{$post_object->id}}"><a href="#"><b>{{count($post_object->likes)}}</b> Likes</a></span>
        </div>
        <div class="post-foot-block">
            <a href="#" data-tab="checkincomments{{$post_object->id}}">
                <i class="trav-comment-icon"></i>
            </a>

            <ul class="foot-avatar-list">
            </ul>
            <span><a href="#" data-tab="checkincomments{{$post_object->id}}">{{count($post_object->comments)}} Comments</a></span>
        </div>
    </div>
    <div class="post-comment-layer" data-content="checkincomments{{$post_object->id}}" style='display:none;'>

        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this)">@lang('comment.top')</li>
                <li class="active" onclick="commentSort('New', this)">@lang('home.new')</li>
            </ul>
            <div class="comm-count-info">
            </div>
        </div>
        <div class="post-comment-wrapper sortBody" id="checkincomments{{$post_object->id}}">
            @if(count($post_object->comments)>0)
                @foreach($post_object->comments AS $comment)


                    @include('site.home.partials.checkin_do_comment_block')
                @endforeach
            @endif
        </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-input">
                <form class="checkincomments{{$post_object->id}}" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" data-id="pair{{$post_object->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                    <div class="post-block post-create-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" id="checkincommentstext{{$post_object->id}}" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:40px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias">
                                <!-- <div class="plus-icon" style="display: none;">
                                    <div>
                                        <span class="close" onclick="javascript:$('#commentfile').click();"><span style="font-size:110px;">+</span></span>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                </ul>
                            </div>

                            <button type="submit" class="btn btn-primary">@lang('buttons.general.send')</button>

                        </div>

                    </div>
                    <input type="hidden" name="post_id" value="{{$post_object->id}}"/>
                </form>

                </div>
            </div>
            <script>

                $('#checkincommentstext{{$post_object->id}}').keydown(function(e){
                    if(e.keyCode == 13 && !e.shiftKey){
                        e.preventDefault();
                    }
                });
                $('#checkincommentstext{{$post_object->id}}').keyup(function(e){
                    if(e.keyCode == 13 && !e.shiftKey){
                        $('.checkincomments{{$post_object->id}}').find('button[type=submit]').click();
                    }
                });
                $('body').on('submit', '.checkincomments{{$post_object->id}}', function (e) {
                    var form = $(this);
                    var text = form.find('textarea').val().trim();

                    if(text == "")
                    {
                        // alert('Please input text or select files.');
                        return false;
                    }
                    var values = form.serialize();
                    form.find('button[type=submit]').attr('disabled', true);
                    $.ajax({
                        method: "POST",
                        url: "{{ route('post.checkincomment') }}",
                        data: values
                    })
                        .done(function (res) {
                            $('#checkincomments{{$post_object->id}}').append(res);
                            form.find('textarea').val('');
                            form.find('button[type=submit]').removeAttr('disabled');
                        });
                    e.preventDefault();
                });
            </script>
        @endif
    </div>
</div>

@if(count($post_followers)>0)
    <div class="modal fade" tabindex="-1" role="dialog" id="checkinfollowers_{{$post_object->id}}" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">{{count($post_followers)}} @lang('home.following_this')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="post-comment-wrapper">
                    @foreach($post_followers as $follower)
                    <div class="post-comment-row">
                        <div class="post-com-avatar-wrap">
                            <img src="{{check_profile_picture($follower->user->profile_picture)}}" alt="">
                            <a class="comment-name">&nbsp;&nbsp;&nbsp;{{$follower->user->name}}</a>
                            {!! get_exp_icon($follower->user) !!}
                        </div>
                        <div class="post-comment-text">
                            <div class="post-com-name-layer">
                                <!-- <a href="#" class="comment-name"></a> -->
                                <!--<a href="#" class="comment-nickname">@katherin</a>-->
                            </div>
                            <div class="comment-bottom-info">
                                <!-- <div class="com-time">3 minutes ago</div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            </div>
        </div>
    </div>
    @endif
@endif
