<?php
    $check_follow = App\Models\UsersFollowers\UsersFollowers::where('followers_id', Auth::guard('user')->user()->id)->where('users_id', $travelmate_request->author->id)->get()->first();

    $author = $travelmate_request->author;
    $tripPlan = $travelmate_request->plan;
    $tripPlanCountry = $travelmate_request->plan_country->country;

    $tripPlanBudget = $tripPlan->budget();


    $tripPlaces = $tripPlan->trips_places()->orderBy('date', 'asc')
        ->get();
    $places = $tripPlan->places;

    $tripPlanLastCountry = @$places->last()->country;

    $points = [];
    $polylines = "";
    $center = [];
    $zoom = 8;
    $markers = [];
    $waypoint = [];
    $lats = [];
    $lngs = [];

    foreach ($places as $place){
        $markers[] = "markers=anchor:center|icon:".asset("assets2/image/marker_s.png")."|".$place->lat.",".$place->lng;
        $points[] = $place->lat . "," . $place->lng;
        $waypoint[] = "via:".$place->lat . "," . $place->lng;
        $lats[] = $place->lat;
        $lngs[] = $place->lng;
    }

$newwaypoint = $waypoint;
$origin = @$points[0];
$destin = @$points[count($points)-1];
$url = 'https://maps.googleapis.com/maps/api/directions/json?origin='.$origin.'&destination='.$destin.'&waypoints=optimize:true|'.implode("|", $newwaypoint).'&mode=driving&key=AIzaSyAC8_Ma0qQULSkAlfAuwXZpJBWd3OJlA8Q';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, false);
$result = curl_exec($ch);
curl_close($ch);
$googleDirection = json_decode($result, true);

try {
    $polyline = urlencode($googleDirection['routes'][0]['overview_polyline']['points']);
    $polylines = "enc:".$polyline;

    $lat = ($googleDirection['routes'][0]['bounds']['southwest']['lat'] + $googleDirection['routes'][0]['bounds']['northeast']['lat']) / 2;
    $lng = ($googleDirection['routes'][0]['bounds']['southwest']['lng'] + $googleDirection['routes'][0]['bounds']['northeast']['lng']) / 2;
    $center[0] = isset($center[0]) ? ($center[0] + $lat) / 2 : $lat;
    $center[1] = isset($center[1]) ? ($center[1] + $lng) / 2 : $lng;

    $west = $googleDirection['routes'][0]['bounds']['southwest']['lng'];
    $east = $googleDirection['routes'][0]['bounds']['northeast']['lng'];

    $angle = $east - $west;
    if ($angle < 0) {
        $angle += 360;
    }
    $zoom = @floor(log(712 * 360 / $angle / 256) / log(2)) - 1;
} catch (\Throwable $th) {
    if(count($lats)>0) {
        $center[0] = array_sum($lats) / count($lats);
        $center[1] = array_sum($lngs) / count($lngs);

        $angle = max($lngs) - min($lngs);
        if ($angle < 0) {
            $angle += 360;
        }
        $zoom = @floor(log(712 * 360 / $angle / 256) / log(2)) - 2;
    }
    $polylines = "geodesic:true|".implode('|', $points);
}

@$center = count($center) > 0 ? $center : [$f_plan->places[0]->lat , $f_plan->places[0]->lng];
$zoom = $zoom === INF ? 14 : $zoom ;
$markers = implode("&", $markers);
?>
<div class="post" filter-tab="#mate" @if(isset($search_result)) search-tab="#searched" @endif>
    <div class="post__header">
        <img class="post__avatar" src="{{check_profile_picture($author->profile_picture)}}" alt="" role="presentation">
        <div class="post__title-wrap">
            <a class="post__username" href="{{url_with_locale('profile/'.$author->id)}}">{{$author->name}}</a>
            {!! get_exp_icon($author) !!}
            <div class="post__posted">
                Planning a <a href="{{url_with_locale('trip/plan/'.$tripPlan->id)}}"><strong>Trip Plan</strong></a> <!--to {num} Destinations--> in <a href="{{url_with_locale('country/'.$tripPlanCountry->id)}}">{{@$tripPlanCountry->trans[0]->title}}</a>
            </div>
        </div>

        <div class="user-card__btn-wrap">

            @if(is_object($check_follow))
                <button class="btn btn--md btn--grey" type="button" onclick="travel_mate_following('unfollow', {{$travelmate_request->author->id}}, this)"><svg class="icon icon--add-user">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#user')}}"></use>
                    </svg>UnFollow
                </button>
            @else
                <button class="btn btn--md btn--main" type="button" onclick="travel_mate_following('follow', {{$travelmate_request->author->id}}, this)"><svg class="icon icon--add-user">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#add-user')}}"></use>
                    </svg>Follow
                </button>
            @endif
        </div>
    </div>

    <div class="post__content">
        <div class="travelmate-post-map">
            <div style="position:relative; height: 100%;">
                <img src="{{get_plan_map($tripPlan, '594', '303')}}"
                     class="img-responsive" alt="Static map">

                @include('site.place2.partials._trip_map_info', [
                    'user' => $author,
                    'place' => @$places[0],
                    'duration' => @$places[0]->pivot->duration,
                    'budget' => @$places[0]->pivot->budget,
                ])
            </div>
        </div>

        <div class="travelmate-post-footer mb-3">
            <div class="sub-footer d-flex justify-content-between">
                <div class="countries">{{@$tripPlanCountry->trans[0]->title}} - {{@$tripPlanLastCountry->trans[0]->title}}</div>
                <div><b>{{@$tripPlan->users->count()}}</b> {{ __('Used this plan')}}</div>
            </div>
            <div class="sub-footer">
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <div class="d-flex justify-content-between">
                            <div>
                                <i class="trav-map-marker-icon" dir="auto"></i>
                                <span>Destinations:</span>
                            </div>
                            <div><b>{{ $places->count() }}</b></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="d-flex justify-content-between">
                            <div>
                                <i class="trav-budget-icon" dir="auto"></i>
                                <span>Budget:</span>
                            </div>
                            <div><b>${{$tripPlanBudget}}</b></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="d-flex justify-content-between">
                            <div>
                                <i style="margin-left: 4px;" class="trav-flag-icon" dir="auto"></i>
                                <span>Starting:</span>
                            </div>
                            <div><b><?php echo date("D, j M Y", strtotime($tripPlaces->first()->date)) ?></b></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="d-flex justify-content-between">
                            <div>
                                <i class="trav-clock-icon" dir="auto"></i>
                                <span>Duration:</span>
                            </div>
                            <div><b>{{calculate_duration($tripPlan->id, 'all')}}</b></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>