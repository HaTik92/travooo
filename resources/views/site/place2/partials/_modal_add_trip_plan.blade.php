<div class="modal modal-sidebar " id="modal-add_place" role="dialog" aria-labelledby="Edit Place"
    style="overflow:auto;z-index:1600;">
    <div class="modal-dialog modal-lg" role="document" style="width:650px">
        <div class="modal-content">
            <div class="modal-header" style="height: 45px;">
                <h5 style="padding-bottom: 10px;" class="modal-title w-100 text-center">Adding
                    <strong style="font-weight: 500;">{{@$place->trans[0]->title}}</strong> to<strong><span
                            style="color:#71A0FF;    margin-left: 5px;font-weight: 500;" class="trip_name"></span></strong></h5>
                <button type="button" class="close" style="color:#A7A7A7" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="background-color:#FAFAFA">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>Location</label>
                            <div class="location-container">
                                <div class="d-block">
                                    <img class="trip-plan-places-image"
                                        src="@if(isset($place->city->getMedias[0]->url)) @if($place->city->getMedias[0]->thumbs_done==1)https://s3.amazonaws.com/travooo-images2/th1100/{{$place->city->getMedias[0]->url}} @else https://s3.amazonaws.com/travooo-images2/{{$place->city->getMedias[0]->url}} @endif @else {{asset('assets2/image/placeholders/pattern.png')}} @endif">
                                    @if(session('place_country') == 'place')
                                        <label style="margin-left:20px"><strong
                                            style="font-family:inherit;font-weight: 500;    margin-left: -10px;">{{@$place->city->transsingle->title}}</strong>, <span style="font-family:inherit;color:#A7A7A7;">City of {{@$place->city->country->transsingle->title}} </span>
                                    @elseif(session('place_country') == 'country')
                                        <select style="margin-left:20px" class="country_cities_select" name="state">
                                            @foreach($countries_cities as $key=>$city)
                                                <option data-text="{{$city}}" value={{$key}}>{{$city}}</option>
                                            @endforeach
                                        </select>
                                    @elseif(session('place_country') == 'city')
                                        <label style="margin-left:20px"><strong
                                            style="font-family:inherit;font-weight: 500;    margin-left: -10px;">{{@$place->trans[0]->title}}</strong>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:30px">
                            <label>When you will arive?</label>
                            <div class="location-container row ml-0">
                                <div class="col">
                                    <img style="width: 20px;    margin-top: 10px;"
                                        src="{{asset('trip_assets/images/ico-calendar.svg')}} ">
                                </div>
                                <div class="col" style="margin-top:5px;margin-left: -230px;">
                                    <span class="input-name" style="">Date</span><br>
                                    <input type="text" name="date" id="actual_place_date" autocomplete="off"
                                        class=" datepicker_place" required="" placeholder="{{gmdate('d M Y')}}" style="font-family:inherit;border: none;padding-left: 0px;
                                        padding-top: 3px; ">

                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:30px">
                            <label>Planning to stay</label>
                            <div class="row  ml-0 mr-0">
                                <div class="col counter-div" style="margin-right: 7px;">
                                    <div class="row row ml-0">
                                        <div>
                                            <label style="color:#A7A7A7;margin-top: 2px;">Hours</label>
                                            <div>
                                                <div class="row ml-0">
                                                    <div class="col" style="margin-left:-15px"><button value="-1"
                                                            class="counter-btn hours">-</button></div>
                                                    <div class="col"><input type="text" value="0" name="duration_hours"
                                                            id="duration_hours" class="counter-input">
                                                    </div>
                                                    <div class="col" style="right:-5px"><button value="+1"
                                                            class="counter-btn hours">+</button></div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col" dir="auto">
                                        </div>
                                    </div>
                                </div>
                                <div class="col counter-div">
                                    <div class="row row ml-0">
                                        <div>
                                            <label style="color:#A7A7A7;margin-top: 2px;">Minutes</label>
                                            <div>
                                                <div class="row ml-0">
                                                    <div class="col" style="margin-left:-15px"><button value="-1"
                                                            class="counter-btn minutes">-</button></div>
                                                    <div class="col"><input type="text" name="duration_minutes"
                                                            id="duration_minutes" value="0" class="counter-input">
                                                    </div>
                                                    <div class="col" style="right:-5px"><button value="+1"
                                                            class="counter-btn minutes">+</button></div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col" dir="auto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group ">
                            <label>Place Name</label>
                            <div class="location-container">
                                <div class="d-block" style="pt-0">
                                    @if(session('place_country') == 'place')
                                        <img class="trip-plan-places-image"
                                            src="@if(isset($place->getMedias[0]->url)) @if($place->getMedias[0]->thumbs_done==1)https://s3.amazonaws.com/travooo-images2/th1100/{{$place->getMedias[0]->url}} @else https://s3.amazonaws.com/travooo-images2/{{$place->getMedias[0]->url}} @endif @else {{asset('assets2/image/placeholders/pattern.png')}} @endif">
                                        <label style="margin-left:20px"> <span  style="font-family:inherit;color:#A7A7A7;    margin-left: -10px;">
                                                @php 
                                                    if(strlen (@$place->trans[0]->title)<20){
                                                        echo @$place->trans[0]->title;
                                                    }else{
                                                    echo  substr(@$place->trans[0]->title,0,15).'....';
                                                    }
                                                @endphp
                                            </span>
                                        </label>
                                    @else
                                    <img class="trip-plan-places-image" src=" {{asset('assets2/image/placeholders/pattern.png')}}">
                                    <select id="input_for_place_name" class="input_for_place_name" style="margin-left:20px;display:none;"></select>                                        
                                    </span>
                                </label>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:30px">
                            <label> &nbsp;</label>
                            <div class="location-container row ml-0">
                                <div class="col">
                                    <img style="width: 20px;    margin-top: 10px;"
                                        src="{{asset('trip_assets/images/ico-clock.svg')}} ">
                                </div>
                                <div class="col" style="margin-top:5px;margin-left: -230px;">
                                    <span class="input-name" style="">Time</span><br>
                                    <input type="text" name="time" id="actual_place_time" class="timepicker" required=""
                                        placeholder="9:20 PM" style="border: none;padding-left: 0px;
                                        padding-top: 3px;z-index:1650 ">

                                </div>
                            </div>
                        </div>
                        <div class="form-group " style="margin-top:30px">
                            <label>How much you will spend</label>
                            <div class="w-100">
                                <input type="text" placeholder="Amount" id="budget" name="budget" class="spent-input" style="">
                                    
                                <div id="ck-button" class="ck-button">
                                    <label>
                                    <input type="radio"  style="display:none" name="budget_amount" value="50"><span>50$</span>
                                    </label>
                                </div>
                                <div id="ck-button" class="ck-button">
                                    <label>
                                    <input type="radio" style="display:none" name="budget_amount"  value="100"><span>100$</span>
                                    </label>
                                </div>
                                <div id="ck-button" class="ck-button">
                                    <label>
                                    <input type="radio" style="display:none" name="budget_amount" value="200"><span>200$</span>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="height:40px;padding-top: 25px;">
                    <input type='hidden' name='places_real_id' id='places_real_id' value="{{$place->id}}" />
                    <input type='hidden' name='cities_id' id='cities_id' value="{{@$place->city->id}}" />
                    <input type='hidden' name='trip_real_id' id='trip_real_id' />
                    <a class="btn  btn--main " style="font-family: inherit;color:#A7A7A7;height:35px;background:white;margin-right:10px"
                        class="close" data-dismiss="modal">Cancel</a>
                    <a class="btn  btn--main doAddPlace" style="font-family: inherit;color:white;height:35px">Save</a>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>


    