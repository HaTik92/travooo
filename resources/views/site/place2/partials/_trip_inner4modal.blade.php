@if(Auth::check() && $plan)
<div class="trip__main mobile--main">
    <div class="trip__header mobile--header"><a href="{{url_with_locale('trip/plan/'.$plan->id)}}">{{$plan->title}}</a></div>
    <div class='trip-user-mobile'>
        <img class="trip__user-avatar" src="{{check_profile_picture($plan->author->profile_picture)}}" alt="#" title="" />
        <div class="trip__user-name">
            <a href="{{url_with_locale('profile/'.$plan->author->id)}}">{{$plan->author->name}}</a>
            {!! get_exp_icon($plan->author) !!}
            <div class="trip__posted">{{diffForHumans($plan->created_at)}}</div> 
        </div>            
    </div>
    <div class="trip__map">
        <div class="trip__placeholder" style="background-size:100% 100%;background-image: url('{{get_plan_map($plan, '712', '364')}}')"></div>

        @include('site.place2.partials._trip_map_info', [
                'user' => $plan->author,
                'place' => @$plan->places[0],
                'duration' => @$plan->places[0]->pivot->duration,
                'budget' => @$plan->places[0]->pivot->budget,
            ])
    </div><!-- .trip__map -->
    <div class="carousel-cards">
    @foreach($plan->places AS $pplace)
    <div class="carousel-cards__item" onclick="getmap4tripmodal({{@$center[0]}},{{@$center[1]}},{{$pplace->lat}},{{$pplace->lng}},{{$zoom}},this)" style="display: table-cell;cursor:pointer;">
        <span class="carousel-cards__card">
            <img class="carousel-cards__card-img" src="{{check_place_photo($pplace, true)}}" alt="" role="presentation" style="width:68px;height:68px;"/>
            <span class="carousel-cards__card-content">
                <div class="carousel-cards__card-name">{{$pplace->transsingle->title}}</div>
                <div class="carousel-cards__card-label">{{do_placetype($pplace->place_type)}}</div>
            </span>
        </span>
    </div>
    @endforeach

    </div>
</div>
<div class="trip__side">
    <div class="trip__user hidden--xs"><img class="trip__user-avatar" src="{{check_profile_picture($plan->author->profile_picture)}}" alt="#" title="" />
    <div class="trip__user-info">
        <div class="trip__user-name">By <a href="{{url_with_locale('profile/'.$plan->author->id)}}">{{$plan->author->name}}</a>
            {!! get_exp_icon($plan->author) !!}
        </div>
        <div class="trip__posted">{{diffForHumans($plan->created_at)}}</div>
    </div>
    </div>
    <div class="trip__subheader">
    <div class="trip__reactions">
        <div class="emoji">
            <svg class="icon icon--like-2" style="color: #4080ff">
            <use xlink:href="{{asset('assets3/img/sprite.svg#like-2')}}"></use>
            </svg>
        </div><span><strong>{{@count($plan->likes()->whereNull('places_id')->get())}}</strong> Likes</span>
    </div>
    <div class="trip__side-buttons" posttype="Trip"><button class="trip__side-btn plan_share_button" type="button" id="{{ $plan->id }}">
        <div class="trip__side-btn-icon"><svg class="icon icon--share">
            <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
            </svg></div>
        <div class="trip__side-btn-text">Share</div>
        </button>
        <button class="trip__side-btn" type="button" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{ $plan->id }},this)">
        <div class="trip__side-btn-icon"><svg class="icon icon--flag">
            <use xlink:href="{{asset('assets3/img/sprite.svg#flag')}}"></use>
            </svg></div>
        <div class="trip__side-btn-text">Report</div>
        </button></div>
    </div>
    <div class="comments tripscomment-modal-block-{{ $plan->id }} plan-modal-comment-content">
    <div class="comments__header">
        <div class="comments__total"><span class="{{ $plan->id }}-all-comments-count">{{ count(@$plan->comments) }}</span> Comments</div>
        <div class="comments__number">@if(count(@$plan->comments) > 5) 5 @else {{count(@$plan->comments)}} @endif / <span class="{{ $plan->id }}-all-comments-count">{{ count(@$plan->comments) }}</span></div>
    </div>
    <div class="comments__items-wrap">
        <div class="comments__items post-comment-wrapper sortBody" data-simplebar="data-simplebar">
        @foreach($plan->comments->take(5) as $comment)
           @include('site.home.partials.trip_comment_block')
        @endforeach
        <button class="comments__load-more trip" @if(count(@$plan->comments) <= 5) style="display:none;" @endif type="button" data-id="{{ $plan->id }}">Load more...</button>
        </div>
    </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-inputs">
                <form class="tripscomment" method="POST" action="{{url("new-comment") }}" data-id="{{$plan->id}}" autocomplete="off" enctype="multipart/form-data">
                    <input type="hidden" data-id="pair{{$plan->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                    <div class="post-create-block post-comment-create-block post-regular-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" data-id="tripscommenttext" class="textarea-bg-white textarea-customize"
                                style="display:inline;vertical-align: top;height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias place-media">
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <li class="post-options">
                                        <input type="file" name="file[]" class="trip-comment-modal-input" data-plan_id="{{$plan->id}}" id="planmodalcommentfile{{$plan->id}}" style="display:none" multiple>
                                        <i class="fa fa-camera click-target" data-target="planmodalcommentfile{{$plan->id}}"></i>
                                    </li>
                                </ul>
                            </div>
                            <button type="submit" class="btn btn-primary btn-disabled d-none"></button>
                        </div>
                    </div>
                    <input type="hidden" name="plan_id" value="{{$plan->id}}"/>
                </form>
                </div>
            </div>
        @endif
    </div>
</div>
@endif
