<?php
    $mapindex = rand(1, 5000);
    $plan_comments = $plan->comments()->where('parents_id', '=', NULL)->orderBy('created_at', 'DESC')->get();
    $author = $plan->author;
    $places = $plan->places;
?>
<div class="post" filter-tab="#tripplan" @if(isset($search_result)) search-tab="#searched" @endif>
    <div class="post__header" onclick="modal_blog_show(this, event, 'plan', {{ $plan->id }})">
        <img class="post__avatar" src="{{check_profile_picture(@$author->profile_picture)}}" alt="" role="presentation">
        <div class="post__title-wrap">
            <a class="post__username" href="{{url_with_locale('profile/'.@$plan->author->id)}}">{{@$plan->author->name}}</a>
            {!! get_exp_icon($author) !!}
            <div class="post__posted">Planned a <a href="{{url_with_locale('trip/plan/'.$plan->id)}}"><strong>trip</strong></a> to {{$place->transsingle->title}} at {{diffForHumans($plan->created_at)}}
                <?php
                switch($plan->privacy){
                    case(1):
                ?>
                    <svg width="15" height="15" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 22 22">
                    <defs></defs>
                    <path d="M21.60784,10.98858c0,5.84713 -4.75845,10.60385 -10.60791,10.60385c-5.84965,0 -10.60801,-4.75672 -10.60801,-10.60385c0,-5.85061 4.75836,-10.61092 10.60801,-10.61092c5.84946,0 10.60791,4.76031 10.60791,10.61092zM13.87287,2.41001c-0.22097,0.26946 -0.38671,0.52677 -0.3706,0.70146c0.24176,2.42207 1.89228,0.60355 2.21214,0.95684c1.14193,1.26626 -2.1431,1.70726 -1.00136,3.99455c0.55056,1.09709 1.1857,1.40452 2.11134,1.41705c0.92745,0.01606 1.2681,1.59566 1.34182,2.25401c0.0761,0.65802 -0.01135,2.34408 -0.34523,3.07104c-0.3315,0.72459 0.02079,1.4272 0.35229,1.68573c1.17168,-1.5271 1.87378,-3.43334 1.87378,-5.50211c0,-3.98864 -2.58989,-7.3754 -6.17418,-8.57857zM2.55762,7.74605c0.66528,0.06419 2.26307,0.08577 2.46325,0.64698c0.20475,0.57148 -0.70448,1.15427 -0.70448,2.33939c0,1.30661 1.6758,1.29292 1.6758,2.27912c0,0.98543 0.36373,2.13142 0.36373,2.46314c0,0.32785 1.3999,2.1381 1.92003,2.1381c0.51813,0 0.11053,-2.02413 0.13819,-2.55989c0.01631,-0.3443 0.24853,-1.41274 0.31557,-1.66183c0.44909,-1.69158 2.13423,-1.51888 2.51856,-2.54306c1.21307,-3.21714 -2.59438,-3.40005 -3.11241,-3.58291c-0.51804,-0.18413 -1.39732,-0.18529 -1.7313,-0.07677c-0.33379,0.10846 -1.00136,-0.45474 -1.00136,-0.92786c0,-0.47312 1.3217,-1.41975 1.7313,-2.07463c0.40751,-0.65719 0.73223,-0.56204 1.48716,-1.2502c0.3479,-0.31212 0.91867,-0.48918 1.13516,-0.90866c-3.29895,0.45827 -6.03628,2.69854 -7.19919,5.71908zM11.00002,20.03364c1.84174,0 3.55692,-0.55658 4.98877,-1.50635c-0.39845,-0.72221 -1.87645,-0.82289 -2.62442,-0.76333c-0.88892,0.07871 -1.89705,0.64897 -2.46105,0.77238c-0.63534,0.13942 -1.09348,0.33602 -1.65758,0.35677c-0.49276,0.02153 -0.54808,0.39204 -1.09129,0.6764c0.8955,0.29965 1.85089,0.46413 2.84557,0.46413z" fill="#555555" fill-opacity="1"></path>
                    </svg>
                <?php
                        break;
                    case(2):
                ?>
                    <svg id="SVGDoc" width="15" height="15" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 22">
                    <defs></defs>
                    <path d="M10.88388,2.48629c0.80051,-1.35435 2.11983,-2.281 3.84789,-2.19523c0.02252,0.00122 0.04073,0.00235 0.05526,0.00339c0.01453,-0.00104 0.03275,-0.00217 0.05528,-0.00339c4.42467,-0.21934 6.03275,6.19805 3.38593,9.44412c-0.65355,0.7986 -0.75321,1.27211 -1.12991,2.13496c-0.74263,1.03284 1.8177,1.74838 2.48909,1.97164c2.66571,0.88514 3.40519,1.60934 4.13351,3.99378h-5.99444c-0.10183,-0.03813 -0.20553,-0.07649 -0.31116,-0.11513c-0.9573,-0.35054 -4.4734,-1.48483 -3.67632,-2.63512c0.34456,-0.9729 0.39406,-1.51066 1.09892,-2.37152c2.57399,-3.15798 0.16013,-8.98588 -4.18661,-9.79197c0.07184,-0.14935 0.14941,-0.29474 0.23258,-0.43551zM6.41295,16.36657c0.13359,-0.266 0.27903,-0.50166 0.44384,-0.71415c-0.28789,-0.70408 -0.4163,-1.16789 -1.01679,-1.86744c-2.78397,-3.24612 -1.03884,-9.66346 3.38583,-9.44373c0.02252,0.00119 0.04074,0.00229 0.05528,0.00331c0.01453,-0.00102 0.03274,-0.00212 0.05526,-0.00331c4.42476,-0.21973 6.03285,6.19761 3.38602,9.44373c-0.65355,0.79899 -0.75321,1.2725 -1.13001,2.13534c-0.74253,1.0324 1.8177,1.74838 2.48919,1.97125c2.66571,0.88553 3.40519,1.60973 4.13342,3.99416h-17.86776c0.72842,-2.38443 1.4679,-3.10863 4.13361,-3.99416c0.62457,-0.2073 2.88317,-0.84121 2.59807,-1.75954c-0.21275,0.08821 -0.44058,0.16614 -0.66595,0.23455z" fill="#555555" fill-opacity="1"></path>
                    </svg>
                <?php
                        break;
                    case(3):
                ?>
                    <svg id="SVGDoc" width="15" height="15" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 17 23">
                    <defs></defs>
                    <path d="M17.00161,13.68138v7.50659c0,1.00812 -0.78373,1.82592 -1.79214,1.82592h-12.47721c-1.00842,0 -1.85976,-0.81781 -1.85976,-1.82592v-7.50659c0,-0.83227 0.60863,-1.5334 1.31874,-1.75307v-3.57276c0,-2.16161 0.68121,-4.04119 1.94597,-5.4355c1.2413,-1.36848 2.96316,-2.12204 4.82521,-2.12204c1.86205,0 3.56283,0.75356 4.80404,2.12204c1.26486,1.39431 1.9165,3.27388 1.9165,5.4355v3.57276c0.81148,0.21967 1.31865,0.9208 1.31865,1.75307zM10.2473,17.43467c0,-0.74688 -0.60549,-1.3528 -1.3525,-1.3528c-0.74701,0 -1.3525,0.60548 -1.3525,1.3528c0,0.74688 0.60549,1.35242 1.3525,1.35242c0.74701,0 1.3526,-0.60553 1.3525,-1.35242zM13.14685,8.35555c0,-2.95668 -1.73111,-5.02149 -4.2098,-5.02149c-2.47869,0 -4.2098,2.0648 -4.2098,5.02149v3.4999h8.41961z" fill="#555555" fill-opacity="1"></path>
                    </svg>
                <?php
                        break;
                }
                ?>

            </div>
        </div>
        <div class="dropdown">
            <button class="post__menu" type="button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="icon icon--angle-down">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                </svg>
            </button>
            @if(Auth::check() && Auth::user()->id!==$author->id)
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Trip">
                    <a class="dropdown-item plan_share_button" href="#" id="{{$plan->id}}">
                        <span class="icon-wrap">
                            <i class="trav-share-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>@lang('home.share')</b></p>
                            <p>@lang('home.spread_the_word')</p>
                        </div>
                    </a>
                    @include('site.home.partials._info-actions', ['post'=>$plan])
                </div>
            @endif
        </div>
    </div>

    <div class="trip__header"><a href="{{url_with_locale('trip/plan/'.$plan->id)}}">{{$plan->title}}</a></div>

    <div class="carousel">
        <div class="carousel__items">
            <div class="carousel__item">
                <div style="position: relative;">
                    <div id="plan_map{{$mapindex}}" style='width:100%;height:400px;'></div>

                    <div id="plan_map{{$mapindex}}_insight"></div>

                    <a style="position: absolute; bottom:20px; right:60px;" href="/trip/view/{{$plan->id}}" class="btn btn-primary">VIEW PLAN</a>
                </div>

                <div class="carousel-cards cards{{$mapindex}}" style="overflow: hidden">

                    @foreach($places AS $pplace)
                        @php
                            $checkinfo = App\Models\Posts\Checkins::where('users_id', @$author->id)
                                ->where('place_id', $pplace->id ?? null)
                                ->orderBy('created_at', 'desc')
                                ->get()
                                ->first();

                            $now = \Carbon\Carbon::now();
                        @endphp

                    <div class="carousel-cards__item"  style="display: table-cell;cursor:pointer;">
                        <span class="carousel-cards__card"
                              data-insight='{
                                "checked_on": @if(isset($pplace->pivot->date)) @if($now < $pplace->pivot->date)"Will visit this place {{dateDiffernceInDays($pplace->pivot->date)}} later" @elseif ($now > $pplace->pivot->date) "visited this place {{dateDiffernceInDays(@$pplace->pivot->date)}} ago" @else "visit this place today" @endif @endif,
                                "duration": "{{$pplace->pivot->duration}}",
                                "budget": "{{$pplace->pivot->budget}}",
                                "upcoming": @if( ! $checkinfo || $now < $checkinfo->created_at) "true" @else "false" @endif
                              }'
                              onclick="onClickCarouselCard({{$mapindex}}, map{{$mapindex}}, markers_{{$mapindex}}, {{$pplace->lat}}, {{$pplace->lng}}, this)"
                        >
                            <img class="carousel-cards__card-img" src="{{check_place_photo($pplace)}}" alt="" role="presentation" style="width:68px;height:68px;"/>
                            <span class="carousel-cards__card-content">
                                <div class="carousel-cards__card-name">{{$pplace->transsingle->title}}</div>
                                <div class="carousel-cards__card-label">{{do_placetype($pplace->place_type)}}</div>
                            </span>
                        </span>
                    </div>
                    @endforeach

                </div>
            </div>

        </div>
    </div>
    @include('site.place2.partials._plan_footer')
    <div class="post-comment-layer tripscomment-block-{{$plan->id}}" data-content="tripscomment{{ $plan->id }}" style='display:none; padding: 20px 30px 18px;'>
        <div class="post-comment-top-info">
            <div class="comment-filter">
                <button class="filter-trip-comment" data-type="Top">@lang('comment.top')</button>
                <button class="filter-trip-comment active" data-type="New">@lang('home.new')</button>
            </div>
              <div class="comm-count-info">
                        <strong>0</strong> / <span class="{{$plan->id}}-all-comments-count">{{count($plan_comments)}}</span>
            </div>
        </div>
        <div class="post-comment-wrapper sortBody" id="tripscomment">
            @if(@count($plan_comments)>0)
                @foreach($plan_comments AS $comment)
                    @include('site.home.partials.trip_comment_block')
                @endforeach
            @endif
        </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-inputs">
                <form class="tripscomment" method="POST" action="{{url("new-comment") }}" data-id="{{$plan->id}}" autocomplete="off" enctype="multipart/form-data">
                    <input type="hidden" data-id="pair{{$plan->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                    <div class="post-create-block post-comment-create-block post-regular-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" data-id="tripscommenttext" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias place-media">
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <li class="post-options">
                                        <input type="file" name="file[]" class="trip-comment-media-input" data-plan_id="{{$plan->id}}" data-id="plancommentfile{{$plan->id}}" style="display:none" multiple>
                                        <i class="fa fa-camera click-target" data-target="plancommentfile{{$plan->id}}"></i>
                                    </li>
                                </ul>
                            </div>
                            <button type="submit" class="btn btn-primary btn-disabled d-none"></button>
                        </div>
                    </div>
                    <input type="hidden" name="plan_id" value="{{$plan->id}}"/>
                </form>
                </div>
            </div>
        @endif
    </div>
</div>
<script>
    map_var += `
        if($("#plan_map{{$mapindex}}").length > 0) {
            var map{{$mapindex}}, center_lat{{$mapindex}}, center_lng{{$mapindex}},markers_{{$mapindex}} = [];
        }`;

    map_func += `
        if($("#plan_map{{$mapindex}}").length > 0 && typeof(map{{$mapindex}}) != "object" ) {

        window.tripPlanInsight{{$mapindex}} = new TripPlanInsight(
            {{$mapindex}},
            $('#plan_map{{$mapindex}}_insight').get(0),
            '{{check_profile_picture(@$author->profile_picture)}}'
        );

        var insightInitData{{$mapindex}} = $('.cards{{$mapindex}}').find('.carousel-cards__card:first').data('insight');

        window.tripPlanInsight{{$mapindex}}.setDataAndUpdate(insightInitData{{$mapindex}});

        var buildPopup_{{$mapindex}} = function(place_id, place_name){
            var anchor = place_name;

            if (!anchor) {
                anchor = 'Empty';
            }

            return $('<div><a href="/place/' + place_id + '">' + place_name + '</a></div>').html();
        };

        var bounds_{{$mapindex}} = new mapboxgl.LngLatBounds();
        var mapBounds = [
            [-180, -85],
            [180, 85]
        ];

        map{{$mapindex}} = new mapboxgl.Map({
            container: 'plan_map{{$mapindex}}',
            style: 'mapbox://styles/mapbox/satellite-streets-v11',
            maxBounds: mapBounds
        });

        var waypts_{{$mapindex}} = [];
        var latlngs_{{$mapindex}} = [];

        window.markers_{{$mapindex}} = [];
        window.marker_popups_{{$mapindex}} = [];

        @foreach(get_plan_map_data($plan) AS $checking_index => $checkin)
            @if ($checkin['lat'] && $checkin['lng'])
                var lnglat = [{{$checkin['lng']}}, {{$checkin['lat']}}];
                var popup = new mapboxgl.Popup()
                    .setHTML(buildPopup_{{$mapindex}}({{$checkin['id']}}, "{{$checkin['title']}}"));

                var markerEl = $('<div title="{{$checkin['title']}}" style="width: 14px; height: 14px; overflow: hidden; position: absolute; cursor: pointer; touch-action: none;"><img alt="" src="{{asset('assets2/image/road_marker2.png')}}" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 100%; height: 100%; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>').get(0);

                var marker = new mapboxgl.Marker(markerEl)
                        .setLngLat(lnglat)
                        .setPopup(popup)
                        .addTo(map{{$mapindex}});

                markerEl.addEventListener('click',function() {
                    $(".cards{{$mapindex}}").slick('slickGoTo', {{$checking_index}}, true);
                });

                waypts_{{$mapindex}}.push(lnglat);
                bounds_{{$mapindex}}.extend(lnglat);
                markers_{{$mapindex}}.push(marker);
            @endif
        @endforeach

        map{{$mapindex}}.fitBounds(bounds_{{$mapindex}}, {
            padding: {top: 50, bottom:50, left: 50, right: 50},
            maxZoom: 10
        });

        map{{$mapindex}}.on('load', function () {
            map{{$mapindex}}.addSource('route', {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': waypts_{{$mapindex}}
                    }
                }
            });
            map{{$mapindex}}.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#fff',
                    'line-width': 4
                }
            });
        });
    }`;
    try {

        $(".cards{{$mapindex}}").slick({
            variableWidth: true,
            focusOnSelect: true,
            prevArrow: '<button class="carousel-cards__control carousel-cards__control--prev" style="outline: none;left: 0;justify-content: flex-start;"><svg class="icon icon--angle-left"> <use xlink:href="img/sprite.svg#angle-left"></use> </svg></button>',
            nextArrow: '<button class="carousel-cards__control carousel-cards__control--next" style="outline: none;"><svg class="icon icon--angle-right"> <use xlink:href="img/sprite.svg#angle-right"></use> </svg></button>'
        });

        // On after slide change
        $(".cards{{$mapindex}}").on('afterChange', function(event, slick, currentSlide, nextSlide){
            var insightData = $(slick.$slides[currentSlide]).find('.carousel-cards__card').data('insight');
            console.log(insightData);
            window.tripPlanInsight{{$mapindex}}.setDataAndUpdate(insightData);
        });
    } catch (error) {

    }
</script>
