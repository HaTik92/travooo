<style>
.add-post__media::-webkit-scrollbar {
    width: 1px;
}

.add-post__media::-webkit-scrollbar-track {
    border-radius: 10px;
    background: white;
}

.add-post__media::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background: #ddd;
}
.add-post__media
{
    display: flex;
    max-height: 60px;
    max-width: 250px !important;
    overflow-y: hidden;
    overflow-x: scroll; 
    white-space: nowrap;
    position: relative;
    margin-right: 8px;
}
.img-wrap-newsfeed
{
    width: 40px !important;
    height: 30px !important;
    float: left;
    margin: 5px;
}
.img-wrap-newsfeed>div
{

    overflow: hidden;
    justify-content: center;
    display: flex;
}

.img-wrap-newsfeed .close
{
    margin-top: -44px;
    margin-left: 29px;
    margin-right: -5px;
    opacity: 1;
    width: 16px;
    background: #FC225A;
    height: 16px;
    border-radius: 100%;
    position: relative;
}
.img-wrap-newsfeed .close .remove-file-close-btn{ 
    position: absolute;
    font-size: 16px;
    color: white;
    font-weight: 100;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
}
.thumb 
{
    object-fit: cover;
    height: 40px !important;
    width: 40px;
    border-radius: 5px;
}
.hided
{
    display: none;
}
.white_mark
{
    width: 100%;
    height: 100%;
    position: fixed;
    z-index: 8;
    left: 0;
    top: 0;
}
.textcomplete-wrapper{
    top: 100px;
    left: 100px;
}
.spinner-border {
    display: inline-block;
    position: absolute;
    right: 0;
    width: 2rem;
    height: 2rem;
    float: right;
    margin-right: 35px;
    vertical-align: text-bottom;
    border: .25em solid currentColor;
    border-right-color: transparent;
    border-radius: 50%;
    -webkit-animation: spinner-border .75s linear infinite;
    animation: spinner-border .75s linear infinite;
}
@keyframes spinner-border {
  to { transform: rotate(360deg); }
}
</style>


<div class="add-post" id="add-post">
    <h2 class="add-post__title">Add a post to <strong>{{@$place->trans[0]->title}}</strong></h2>
    <div class="add-post__content">
        <form class="add-post__form formPostBox" id="_place_add_post_form">
            <img class="add-post__avatar" data-src="{{check_profile_picture($me->profile_picture)}}" alt="" role="presentation" />
            {{-- <textarea id="add_post_text" class="add-post__textarea" name="text" data-emoji-input="unicode" data-emojiable="true" contentEditable=true placeholder="Speak your mind, {{@explode(" ", $me->name)[0]}}."></textarea> --}}
            <input id="createPostTxt" rows="5" name="text" class="w-100 post-create"></input>

          
            <div class="add-post__form-footer">
                <button class="add-post__emoji" emoji-target="createPostTxt" type="button">🙂</button>
                <a class="add-post__check-in" data-reference="postbox"  href="#checkin-modal" data-toggle="modal">
                    <svg class="icon icon--location">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use>
                    </svg>
                Check in</a>
            </div>
            <input type="file" name="file" id="_add_post_imagefile" style="display:none" multiple>
            <input type="file" name="file" id="_add_post_videofile" style="display:none" multiple>
            <input style="display:none" type="radio" value="0" name="permission" id="public_permission" checked/>
            <input style="display:none" type="radio" value="1" name="permission" id="friend_permission" />
            <input style="display:none" type="radio" value="2" name="permission" id="private_permission" />
            <input type="hidden" name="type" value="" />
            <input type="hidden" id="checkinPost" name="checkinPost" value="" />
        </form>
        <div class="add-post__footer">
            <div class="add-post__footer-left">
                <div class="add-post__media">

                </div>
                <button class="add-post__post-type" data-triggered="post" type="button" data-type="image" style="font-family: inherit;font-size: 15px;color: #555557;width: 143px;border-radius: 25px;background-color: #F5F7FC;">
                    <svg class="icon icon--camera" style="padding-bottom: 2px;margin-right: 5px;margin-left: -1px;">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#camera')}}"></use>
                    </svg>
                     <span style="    font-weight: 500;">Photo / Video</span>
                </button>
            </div>
            <div class="add-post__footer-right">
                <!-- <div class="white_mark hided"></div> -->
                <div class="dropdown" style="position: relative">
                    <button id="add_post_permission_button" class="btn btn--sm btn--outline dropdown-toggle" data-toggle="dropdown" style='font-family: inherit;'>
                        <svg class="icon icon--earth">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#earth')}}"></use>
                        </svg>
                        PUBLIC
                    </button>
                    <ul class="dropdown-menu permissoin_show hided" style="position: absolute; right: 0; z-index: 9;font-size: 12px;min-width:125px;">
                        <li class="add_post_permission dropdown-item">
                            <label for="public_permission" style="margin: 0">
                                <span class="icon-wrap">
                                    <i class="trav-globe-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Public</b></p>
                                    <p dir="auto">Anyone can see this post</p>
                                </div>
                            </label>
                        </li>
                        {{-- <li class="add_post_permission dropdown-item">
                            <label for="follower_permission" style="margin: 0">
                                <span class="icon-wrap">
                                    <i class="trav-users-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Followers Only</b></p>
                                    <p dir="auto">Only you and your friends can see this post</p>
                                </div>
                            </label>
                        </li> --}}
                        <li class="add_post_permission dropdown-item">
                            <label for="friend_permission" style="margin: 0">
                                <span class="icon-wrap">
                                    <i class="trav-users-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Friends Only</b></p>
                                    <p dir="auto">Only you and your friends can see this post</p>
                                </div>
                            </label>
                        </li>
                        <li class="add_post_permission dropdown-item">
                            <label for="private_permission" style="margin: 0">
                            <span class="icon-wrap" dir="auto">
                            <i class="trav-lock-icon" dir="auto"></i>
                            </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Only Me</b></p>
                                    <p dir="auto">Only you can see this post</p>
                                </div>
                            </label>
                        </li>
                    </ul>
                </div>
                <button class="btn btn--sm btn--main" onclick="_place_send_post(this)" style='font-family: inherit;'>Post</button>
            </div>
        </div>
        <div class="add-post__footer" style="height: 70px;display:none;">
            <div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
        </div>
    </div>
    <div class="add-post__resize">
        <svg class="icon icon--resize">
            <use xlink:href="{{asset('assets3/img/sprite.svg#resize')}}"></use>
        </svg>
    </div>
</div>
