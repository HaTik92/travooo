<div class="comment"><img class="comment__avatar" src="{{check_profile_picture($comment->author->profile_picture)}}" alt="" role="presentation" />
    <div class="comment__content">
    <div class="comment__header"><a class="comment__username" href="{{url_with_locale('profile/'.$comment->author->id)}}">{{ $comment->author->name }}</a>
        <div class="comment__posted">{{ diffForHumans($comment->created_at) }}</div>
    </div>
    <div class="comment__text">{!! $comment->text !!}</div>
    <div class="comment__footer" posttype="Tripcomment">
        <button class="comment__flag-btn" type="button" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$comment->id}},this)"><svg class="icon icon--flag">
            <use xlink:href="{{asset('assets3/img/sprite.svg#flag')}}"></use>
        </svg></button>
    </div>
    </div>
</div>