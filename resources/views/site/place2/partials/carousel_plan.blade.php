<?php
use Illuminate\Support\Facades\Auth;
$mapindex = rand(1, 500);
$plan_comments = $plan->comments()->whereNull('places_id')->orderBy('created_at', 'DESC')->get();
?>
<div class="post lg-item">
    <div class="carousel  lg-img-wrap" style="width:66%;">
        <div class="carousel__items">
            <div class="carousel__item">
                <div id="plan_map{{$mapindex}}" style='width:590px;height:400px;'></div>
            </div>            
        </div>
    </div>
    <div class='cover-block' id='{{ @$photo->id }}' style="width:33%;">
        <div class='cover-block-inner comment-block'>
            <ul class='modal-outside-link-list white-bg'>
                <li class='outside-link'>
                    <a href='#'>
                        @if(true)
                        <div class='round-icon'>
                            <i class='trav-share-icon'></i>
                        </div>
                        @endif
                        <span>Share</span>
                    </a>
                </li>
                <li class='outside-link lg-close' posttype="PlaceImage">
                    <a href='#' data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{ @$plan->id }}, this)">
                        <div class='round-icon'>
                            <i class='trav-flag-icon'></i>
                        </div>
                        <span>@lang("place.report")</span>
                    </a>
                </li>
            </ul>
            <div class='gallery-comment-wrap'>
                <div class='gallery-comment-inner mCustomScrollbar'>
                    <div class='top-gallery-content gallery-comment-top'>
                        <div class='top-info-layer pt-3 pb-3'>
                            <div class='top-info-txt'>
                                <div class='preview-txt'>
                                    <p class='dest-place' style="font-size:17px">{{$plan->title}}</p>
                                </div>
                                <div class='preview-txt comment-info'>
                                    <i class="trav-comment-icon" dir="auto"></i>
                                    <span style="color:#1a1a1a"><strong class="{{@$photo->id}}-all-comments-count" >{{count($plan->comments)}}</strong> @lang('comment.comments')</span>     
                                </div>
                            </div>
                        </div>
                        <div class='gal-com-footer-info'>
                            <div class='post-foot-block post-comment-place'>
                                <i class='trav-location'></i>
                                <div class='place-name'>{{$place->trans[0]->address}}</div>
                            </div>
                        </div>
                    </div>                  
                    <div class='post-comment-layer photo-comment-{{@$photo->id}}'>
                        <div class='post-comment-top-info'>
                            <ul class="comment-filter">
                                <li class="media-comment-filter-type" data-type="Top" data-id="{{@$photo->id}}">@lang('other.top')</li>
                                <li class="media-comment-filter-type active" data-type="New" data-id="{{@$photo->id}}">@lang('other.new')</li>
                            </ul>
                            <div class='comm-count-info'>
                                <strong>0</strong> / <span class="{{@$photo->id}}-all-comments-count">{{@count(@$photo->comments)}}</span>
                            </div>
                        </div>
                        <div class='post-comment-wrapper sortBody' data-id="{{@$photo->id}}"></div>
                    </div>
                </div>
                @if(Auth::user())
                <form class='media_comment_form' method='post' action='#' autocomplete='off'>
                    <div class='post-add-comment-block'>
                        <div class='avatar-wrap'>
                            <img src='{{check_profile_picture(Auth::user()->profile_picture)}}' style='width:45px;height:45px;'>
                        </div>
                        <div class='post-add-com-input'>
                            <input type='text' id='mediacommenttext' name='text' placeholder='@lang("comment.write_a_comment")'>
                        </div>
                        <input type='hidden' name='medias_id' value='{{@$photo->id}}' />
                        <input type='hidden' name='users_id' value='{{Auth::user()->id}}' />
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
</div>
<script>
    map_var += `var map{{$mapindex}}, center_lat{{$mapindex}}, center_lng{{$mapindex}};`;
    map_func += `map{{$mapindex}} = new google.maps.Map(document.getElementById('plan_map{{$mapindex}}'), {
            zoom: 4,
            center: new google.maps.LatLng(28.36539800, - 81.52126420),
            mapTypeId: 'satellite'
        });
        @foreach(get_plan_map_data($plan) AS $checkin)
            @if ($checkin['lat'] && $checkin['lng'])
                center_lat{{$mapindex}} = {{$checkin['lat']}};
                center_lng{{$mapindex}} = {{$checkin['lng']}};
                var marker{{$mapindex}}{{$loop->index}} = new google.maps.Marker({
                    position: {lat: {{$checkin['lat']}}, lng: {{$checkin['lng']}}},
                    map: map{{$mapindex}},
                    title: "{{$checkin['title']}}",
                    color: "{{$checkin['color']}}",
                    label: "{{$checkin['label']}}"
                });

                marker{{$mapindex}}{{$loop->index}}.addListener('click', function() {
                    map{{$mapindex}}.setZoom(16);
                    map{{$mapindex}}.setCenter(marker{{$mapindex}}{{$loop->index}}.getPosition());
                });
            @endif
        @endforeach
        map{{$mapindex}}.setCenter(new google.maps.LatLng(center_lat{{$mapindex}}, center_lng{{$mapindex}}));
    `;
</script>