<?php
    // $event_comments = $evt->comments()->whereNull('places_id')->orderBy('created_at', 'DESC')->get();
?>
<div class="post" filter-tab="#event" @if(isset($search_result)) search-tab="#searched" @endif onclick="modal_blog_show(this, event, 'event', '{{ $evt->id }}')">
    <div class="post__header">
        <div class="post__icon">
            <svg class="icon icon--calendar">
                <use xlink:href="{{asset('assets3/img/sprite.svg#calendar')}}"></use>
            </svg>
        </div>
        <div class="post__title-wrap">
            <h2 class="post__title">{{$evt->title}}</h2>
            <div class="post__meta"><span class="label">Event</span> in {{ @$evt->address }}</div>
        </div>
        @if (Auth::check() && Auth::user()->id !== $evt->id)
            <div class="dropdown">
                <button class="post__menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <svg class="icon icon--angle-down">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                    </svg>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Event">
                    @include('site.home.partials._info-actions', ['post'=>$evt])
                </div>
            </div>
        @endif
    </div>
    <div class="post__content">
        <?php
          $rand = rand(1, 10);
        ?>
        <div class="event">
            <img class="event__img" src="{{asset('assets2/image/events/'.$evt->category.'/'.$rand.'.jpg')}}" style="width:594px;height:250px;">
            <div class="event__content">
                <div class="event__date">
                    <div class="event__date-start">
                        <div class="event__date-label">{{date('M', strtotime($evt->start))}}</div>
                        <div class="event__date-value">{{date('j', strtotime($evt->end))}}</div>
                    </div>
                    <svg class="icon icon--angle-right">
                        <use xlink:href="img/sprite.svg#angle-right"></use>
                    </svg>
                    <div class="event__date-end">
                        <div class="event__date-label">{{date('M', strtotime($evt->end))}}</div>
                        <div class="event__date-value">{{date('j', strtotime($evt->end))}}</div>
                    </div>
                </div>
                <div class="event__main">
                    <div class="event__organisator"></div>
                    <div class="event__duration"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="post__footer">
        <button class="post__like-btn event_like_button" style="outline:none;" type="button" data-id="{{ $evt->id }}">
            <svg class="icon icon--like">
                <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
            </svg>
            <span><strong>{{@count($evt->likes)}}</strong> Likes</span>
        </button>
        <button class="post__like-btn event_share_button" style="outline:none;" type="button" data-id="{{$evt->id}}">
            <svg class="icon icon--like" style="color: #4080ff">
            <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
            </svg>
            <span><strong>{{@count($evt->shares)}}</strong> Shares</span>
        </button>
        <button class="post__comment-btn event_post_comments" type="button" data-tab="eventscomment{{ $evt->id }}" style="outline:none;">
            <svg class="icon icon--comment">
                <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
            </svg>
            <div class="user-list">
                <div class="user-list__item">
                    @if(count($evt->comments) > 0)
                        @foreach($evt->comments as $com)
                            @if($loop->index > 3) @break @endif
                             <div class="user-list__user"><img class="user-list__avatar" src="{{check_profile_picture($com->author->profile_picture)}}" alt="" role="presentation"></div>
                        @endforeach
                    @endif
                </div>
            </div><span><strong class="{{$evt->id}}-all-comments-count">{{@count($evt->comments)}}</strong> Comments</span>
        </button>
    </div>
    <div class="post-comment-layer eventcomment-block comment-block-{{ $evt->id }}" data-content="eventscomment{{ $evt->id }}" style='display:none; padding: 20px 30px 18px;'>
        <div class="post-comment-top-info">
            <div class="comment-filter">
                <button class="filter-event-comment" data-type="Top">@lang('comment.top')</button>
                <button class="filter-event-comment active" data-type="New">@lang('home.new')</button>
            </div>
            <div class="comm-count-info">
                        <strong>0</strong> / <span class="{{$evt->id}}-all-comments-count">{{count($evt->comments)}}</span>
            </div>
        </div>
        <div class="post-comment-wrapper sortBody">
            @if(@count($evt->comments)>0)
                @foreach($evt->comments AS $comment)
                    @include('site.place2.partials.event_comment_block')
                @endforeach
            @endif
        </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-inputs">
                <form class="eventscomment" method="POST" action="{{url("new-comment") }}" data-id="{{$evt->id}}" autocomplete="off" enctype="multipart/form-data">
                    <input type="hidden" data-id="pair{{$evt->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                    <div class="post-create-block post-comment-create-block post-regular-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" data-id="eventscommenttext" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias place-media">
                            </div>
                        </div>
                        
                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <li class="post-options">
                                        <input type="file" name="file[]" class="event-comment-media-input" data-event_id="{{$evt->id}}" data-id="eventcommentfile{{$evt->id}}" style="display:none" multiple>
                                        <i class="fa fa-camera click-target" data-target="eventcommentfile{{$evt->id}}"></i>
                                    </li>
                                </ul>
                            </div>
                            <button type="submit" class="btn btn-primary btn-disabled d-none"></button>
                        </div>
                    </div>
                    <input type="hidden" name="event_id" value="{{$evt->id}}"/>
                </form>
                </div>
            </div>
        @endif
    </div>
</div>