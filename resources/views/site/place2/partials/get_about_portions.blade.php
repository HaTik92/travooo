<div style="margin-bottom: 20px;">
    <div class="card">
        <div class="card__header" style="background-color: #FAFAFA">
            <span style="color: #6A6A6A">Suggested Information about <span
                    style="font-weight: 500;color: #666666;">{{$title}}</span></span>
        </div>
        <div class="card-body">

            @foreach($data as $key=>$datum)
            @if($key == 'packing_tips' || $key == 'Restrictions' ||$key == 'Potential Dangers' ||$key == 'Ettiquette')
            <div class="ml-4" style="margin-top: 25px;"><span style="font-size: 16px;font-weight: 500;">{{$key}}</span>
                <hr>
            </div>
            @php
            $i=0;
            @endphp
            @foreach($datum as $row=>$val)
            @php
            $i++;
            @endphp
            <div class="ml-4">
                <div class="row justify-content-center" style="argin-bottom: 15px;">
                    <div class="col-4">
                        <div style="display: inline;padding-top: 3px;margin-right: 5px;"><img
                                src="{{asset('assets2/image/exclamation.png')}}" /></div>
                        <span style="font-weight: 500;">{{$row}}</span>
                    </div>
                    <div class="col-8">
                        <span style="color: #6D6D6D;">{{$val}}</span>
                    </div>
                </div>
                <hr>
            </div>

            @endforeach
            @elseif($key == 'Description')
            <div>
                <div style="margin-bottom: 15px;"><img style="width: 100%;height: 200px; object-fit: cover;"
                        src='https://s3.amazonaws.com/travooo-images2/th180/{{@$datum['image']}}'></div>
                <div class="ml-3 mr-3"><span dir="auto" style="font-weight: 500; ">About this City</span>
                    <hr dir="auto">
                </div>
                <div class="ml-3 mr-3 mb-4" style="line-height: 1.5;">{{@$datum['description']}}</div>
            </div>
            @elseif($key == 'Languages' || $key == 'Religions')
            <div class="ml-4 mr-3">
                <div class="row justify-content-center mb-3">
                    <div class="col-10">
                        @if($key == 'Languages')
                        <div style="    margin-top: 15px;"><span style="font-weight: 500; ">Languages Spoken in</span>
                        </div>
                        @elseif($key == 'Religions')
                        <div style="    margin-top: 15px;"><span style="font-weight: 500; ">Religions in</span>
                        </div>
                        @endif
                        <hr>
                        <div>
                            <img width="20%"
                                src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{@$flag}}.png">
                            <div
                                style="display: inline-block; padding-top: 1px;margin-left: 10px;display: inline-block;">
                                <span style="font-size:20px;font-weight:500">{{$country}}</span><br><span
                                    style="font-size: 12px;">Country in {{$region}}</span></div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-2" style="margin-top: 7px; margin-left: -10px;"><img
                            src="{{asset('assets2/image/revertexclamation.png')}}" /></div>

                </div>

                @foreach($datum as $row=>$val)
                <div class="mr-2">
                    <span>{{$val}}</span><span style="margin-left: 450px">{{$row}}</span>
                    <hr>
                </div>
                @endforeach

                @elseif($key == 'population')
                <div
                    style="height:350px;background-image: url(https://maps.googleapis.com/maps/api/staticmap?maptype=terrain&center={{@$datum['lat']}},{{@$datum['lng']}}&zoom=10&size=750x360&key={{env('GOOGLE_MAPS_KEY')}})">
                </div>
                <div class="map__info" style="bottom: 280px;width: 20%;" dir="auto">
                    <div class="map__info-item" dir="auto"><svg class="icon icon--user" dir="auto">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#user')}}" dir="auto"></use>
                        </svg>
                        <div class="map__info-wrap" dir="auto">
                            <div class="map__info-label" dir="auto">{{@$datum['population']}}</div>
                            <div class="map__info-value" dir="auto">Population</div>
                        </div>
                    </div>

                </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>