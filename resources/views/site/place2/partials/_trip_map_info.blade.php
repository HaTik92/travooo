@php
    $checkinfo = App\Models\Posts\Checkins::where('users_id', $user->id)
        ->where('place_id', $place->id ?? null)
        ->orderBy('created_at', 'desc')
        ->get()
        ->first();

    $now = \Carbon\Carbon::now();
@endphp

<div class="trip__info">
    <img class="trip__info-avatar" src="{{check_profile_picture($user->profile_picture)}}" alt="#" title="" />
    @if( ! $checkinfo || $now < $checkinfo->created_at)
        <div class="trip__info-text">Checking on {{diffForHumans(@$checkinfo->created_at)}} and will stay <strong>{!! @$duration !!} min</strong></div>
    @else
        <div class="trip__info-text">Checked on {{diffForHumans(@$checkinfo->created_at)}}, stayed <strong>{!! @$duration !!} min</strong> and spent <strong>${!! @$budget !!}</strong></div>
    @endif
</div>