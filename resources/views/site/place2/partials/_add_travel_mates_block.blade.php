<style>
    .img_avatar {
        padding-top: 52px;
        height: 70px;
        width: 30px;
        display: inline;
        color: black;
        margin-left: 20px
    }

    .text_div {
        margin-left: 75px;
        margin-top: -36px;
        font-family: inherit;
        line-height: 18px;
        color: #666;
    }

    .btn_div {
        margin-left: 480px;
        margin-top: -40px;
    }
</style>

<div class="w-100" id="add-post-travelmates-block"
    style="display: none;position: relative;border:1px solid #EDEDED;border-radius:7px;border-top: none;margin-top: 0!important;">
    <h2 class="add-post__title">Become a Travel Mate in <strong>{{@$place->trans[0]->title}}</strong></h2>
    <div class="add-post__content" style="background: white;    height: 65px;">
        <div class="img_avatar" style=""><img width="35px" style="    padding-top: 15px;"
                data-src="{{check_profile_picture($me->profile_picture)}}" alt="" role="presentation" />
        </div>

        <div class="text_div"> You can list yourself as a <span style="font-weight:500">Travel Mate</span>
            in this place<br>
            to let other travelers know that you are available</div>
        <div class="btn_div" >
            <a class="btn btn--main" href="/travelmates-newest" style="font-family:inherit;color:white">Get Started</a>
        </div>
    </div>
</div>