<style>
    .side-ttl{
        font-size: 13px !important; 
    }

</style>
<div class="post-block post-country-block" style="background-color:#f7f7f7;margin-bottom:0;border-bottom:0;border-bottom-left-radius:0;border-bottom-right-radius:0;">
        <div class="post-side-top place-about-main-tabs" style="border-bottom: 0;">
            <h3 class="side-ttl" style="margin-top: 0;" id="about_switch">{{@$city->trans[0]->title}}</h3>
            <h3 class="side-ttl" style="margin-top: 0;" id="tripstips_switch">Trips tips</h3>
            <h3 class="side-ttl" style="margin-top: 0;" id="weather_switch">Weather</h3>
            <h3 class="side-ttl" style="margin-top: 0;" id="etiquette_switch">Etiquette</h3>
            <h3 class="side-ttl" style="margin-top: 0;" id="restriction_switch">Restriction</h3>
            {{-- <h3 class="side-ttl" style="margin-top: 0;" id="visa_switch">Visa</h3> --}}
            <h3 class="side-ttl" style="margin-top: 0;" id="whentogo_switch">When to go</h3>
            <h3 class="side-ttl" style="margin-top: 0;" id="caution_switch">Caution</h3>
            <h3 class="side-ttl" style="margin-top: 0;" id="holidays_switch">Holidays</h3>
            <h3 class="side-ttl" style="margin-top: 0;" id="emergency_switch">Emergency Number</h3>
        </div>
    </div> 

<div id="about_content">
    <div class="post-block post-tips-list-block" style="margin-top: 0;">
        
        <div class="post-list-inner">
            <div class="post-tip-row">
                <div style="line-height: 1.5;">{!!@$city->trans[0]->description!!}</div>
            </div>
            {{-- <div class="post-tip-row">
                <div style="height:350px;background-image: url(https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/{{@$city->lat}},{{@$city->lng}}/600x600?access_token=height:350px;background-image: url(https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/24.71355170,46.67529570/600x600?access_token=pk.eyJ1IjoiZHh0cmlhbiIsImEiOiJja2Zta3hoN24wNmN4MnpwbHd0aHRvOWt3In0.XuP_qBknLJLeMMSjsL2Wsg">
                </div>
                @if(isset($city->trans[0]->pollution_index) && $city->trans[0]->pollution_index>0)
                    <div class="map__info" style="    bottom: 829px; width: 20%;left: 50px;" dir="auto">
                        <div class="map__info-item" dir="auto">
                            <svg class="icon icon--user" dir="auto">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#user')}}" dir="auto"></use>
                            </svg>
                            <div class="map__info-wrap" dir="auto">
                                <div class="map__info-label" dir="auto">{{@$city->trans[0]->pollution_index}}</div>
                                <div class="map__info-value" dir="auto">Population</div>
                            </div>
                        </div>
                    </div>
                @endif
            </div> --}}
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.nationality')</b></div>
                <div class="row-txt">
                    <span>{{@($city->trans[0]->nationality!="") ? $city->trans[0]->nationality : $city->country->trans[0]->nationality}}</span>
                </div>
            </div>

            @php
                //languages
                if(isset($city->languages) && count($city->languages)>0){
                    $title_lang = $city->languages[0]->trans->title;
                    $langcount = count($city->languages);
                 
                }elseif(isset($city->country->languages) && count($city->country->languages)> 0){
                    $title_lang = $city->country->languages[0]->trans->title;
                    $langcount = count($city->country->languages);
                }else{
                    $title_lang = '';
                    $langcount =0;
                }
                //time zones
                if(isset($city->country->timezone[0]->time_zone_name) && count($city->country->timezone)>0){
                    $title_timezone = $city->country->timezone[0]->time_zone_name;
                    $tzcount = count($city->country->timezone);
                 
                }else{
                    $title_timezone = '';
                    $tzcount =0;
                }
                //relegions
                if(isset($city->religions) && count($city->religions)>0){
                    $title_releg = $city->religions[0]->trans[0]->title;
                    $relegcount = count($city->religions);
                 
                }elseif(isset($city->country->religions) && count($city->country->religions)> 0){
                    $title_releg = $city->country->religions[0]->trans[0]->title;
                    $relegcount = count($city->country->religions);
                }else{
                    $title_releg = '';
                    $relegcount =0;
                }
            @endphp
            @if($title_lang !='')
                <div class="post-tip-row">
                    <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                        <b>@lang('region.languages_spoken')</b>
                    </div>
                    <div class="row-txt">
                        <span>{{ $title_lang}}</span>
                        @if($langcount>1)
                            <a href="#modal-more-abouts" class="more-link" data-type="Languages" data-toggle="modal">
                                +{{$langcount-1}} @lang('buttons.general.more')
                            </a>
                        @endif
                    </div>
                </div>
            @endif
           
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.currencies')</b></div>
                <div class="row-txt">
                    <span>{{@$city->currencies[0]->trans[0]->title!="" ? $city->currencies[0]->trans[0]->title : $city->country->currencies[0]->trans[0]->title}}</span>
                    <span class="currency"></span>
                </div>
            </div>
        
            @if($title_timezone !='')
                <div class="post-tip-row">
                    <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                        <b>Time Zones</b></div>
                    <div class="row-txt">
                        <span>{{$title_timezone}}</span>
                        @if($tzcount>1)
                        <a href="#modal-more-abouts" class="more-link" data-type="Time Zones" data-toggle="modal">
                            +{{$tzcount-1}} @lang('buttons.general.more')
                        </a>
                        @endif
                    </div>
                </div>
            @endif
            @if($title_releg)
                <div class="post-tip-row">
                    <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                        <b>@lang('region.religions')</b></div>
                    <div class="row-txt">
                        <span>{{$title_releg}}</span>
                        @if($relegcount>1)
                        <a href="#modal-more-abouts" class="more-link" data-type="Relegions" data-toggle="modal" >
                            +{{$relegcount-1}} @lang('buttons.general.more')
                        </a>
                        @endif
                    </div>
                </div>
            @endif
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.phone_code')</b></div>
                <div class="row-txt">
                    <span>{{@$city->code!="" ? $city->code : $city->country->code}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.units_of_measurement')</b>
                </div>
                <div class="row-txt">
                    <span>{{@$city->trans[0]->metrics!="" ? $city->trans[0]->metrics : $city->country->trans[0]->metrics}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.working_days')</b></div>
                <div class="row-txt">
                    <span>{{@$city->trans[0]->working_days!="" ? $city->trans[0]->working_days : $city->country->trans[0]->working_days}}</span>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>Sockets</b></div>
                    {{-- @if($city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_SOCKETS_PLUGS)->first()) --}}
                    <div class="row-txt">
                        <span>{{@$city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_SOCKETS_PLUGS)->first() ? @$city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_SOCKETS_PLUGS)->first()->body : @$city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_SOCKETS_PLUGS)->first()->body}}</span>
                    </div>
                    {{-- @endif --}}
                </div>
            @if(isset($city->country->trans[0]->internet))
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.internet')</b></div>
                <div class="row-txt">
                    <span>{{@$city->country->trans[0]->internet}}</span>
                </div>
            </div>
            @endif
            
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.speed_limit')</b></div>
                <div class="row-txt">
                    <?php
                    if ($city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_SPEED_LIMIT)->get()->toArray() != '') {
                        $speed_limit = $city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_SPEED_LIMIT)->get()->toArray();
                    } else {
                        $speed_limit = $city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_SPEED_LIMIT)->get()->toArray();
                    }
                    ?>
                    @if (count($speed_limit)>0)
                    <span>{{@$speed_limit[0]['title']}}: {{@$speed_limit[0]['body']}}</span>
                    @endif
                    @if(count($speed_limit)>1)
                        <a href="#modal-more-abouts"class="more-link" data-type="Speed Limits" data-toggle="modal" >
                            +{{@count($speed_limit)-1}} @lang('buttons.general.more')
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>
<div id="emergency_content">
    <div class="post-block post-tips-list-block">
        <div class="post-top-layer">
            <div class="top-left">
                <h3 class="post-tip-ttl_lg">@lang('region.emergency_number')
                    &nbsp;<span>{{@count($city->emergency) ? count($city->emergency) : count($city->country->emergency)}}</span>
                </h3>
            </div>
        </div>
        <div class="post-list-inner">
            @if(count($city->emergency))
                @foreach($city->emergency AS $emergency)
                    <div class="post-tip-row">
                        <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                            <b>{{@strip_tags($emergency->trans[0]->description)}}</b></div>
                        <div class="row-txt">
                            <span>{{@$emergency->trans[0]->title}}</span>
                        </div>
                    </div>
                @endforeach
            @else
                @foreach($city->country->emergency AS $emergency)
                    <div class="post-tip-row">
                        <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                            <b>{{@strip_tags($emergency->trans[0]->description)}}</b></div>
                        <div class="row-txt">
                            <span>{{@$emergency->trans[0]->title}}</span>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
    
<div class="post-block" id="holidays_content" style="margin: 0;">
    <div class="post-side-top" style="margin: 0;">
        <h3 class="side-ttl">National holidays <span class="count">{{@count($city->country->holidays)}}</span></h3>
        <div class="side-right-control">
            <a href="#" class="see-more-link" data-toggle="modal" data-target="#nationalHolidayPopup">See all</a>
            <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
            <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
        </div>
    </div>
    <div class="post-side-inner">
        <div class="post-slide-wrap slide-hide-right-margin">
            <ul id="nationalHoliday" class="post-slider">
                @foreach($city->country->holidays AS $holiday)
                    <li class="post-card">
                        <div class="image-wrap">
                            <img src="http://placehold.it/274x234" alt="">
                            <div class="card-cover">
                                <span class="date">4</span>
                                <span class="month">Jul</span>
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">{{@$holiday->trans[0]->title}}</p>
                            <div class="post-footer-info">
                                <div class="post-foot-block">
                                    <!--<ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <span>20 Talking about this</span>
                                                                                            -->
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
    

    
<div id="caution_content" style="margin: 0;">
    <div class="post-block post-tab-block post-tip-tab" style="margin: 0;">
        <div class="post-tab-inner post-tip-tab" id="postTabBlock">
            <div class="tab-item active-tab" data-tab="dangers">
                @lang('region.potential_dangers')
            </div>
            <div class="tab-item" data-tab="indexes">
                @lang('region.indexes') 
            </div>
        </div>
    </div>

    <div class="post-block post-tips-list-block" data-content="dangers" style="margin: 0;">
        <div class="post-list-inner">
            <?php
            $dangers = count($city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_POTENTIAL_DANGERS)->get()) > 0 ? $city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_POTENTIAL_DANGERS)->get() : (count($city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_POTENTIAL_DANGERS)->get()) > 0 ? $city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_POTENTIAL_DANGERS)->get() : []);
            ?>
            @foreach($dangers AS $danger)
                <?php
                $title = str_replace('_', ' ', ucfirst($danger->title));
                ?>
                <div class="post-tip-row tip-txt">
                    <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                        <b>{{@$title}}</b></div>
                    <div class="row-txt"><span>{{@$danger['body']}}</span></div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="post-block post-tips-list-block" data-content="indexes" style="display:none;margin: 0;">
        <div class="post-list-inner">
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.pollution')</b>
                </div>
                <div class="row-txt">
                    <div class="index-slider-wrap">
                        <div class="counter">
                            <span id="current">{{round($city->trans[0]->pollution_index)}}</span>&nbsp;/&nbsp;<span
                                    id="total">200</span>
                        </div>
                        <div id="sliderPollution" class="slider"></div>
                    </div>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.cost_of_living')</b>
                </div>
                <div class="row-txt">
                    <div class="index-slider-wrap">
                        <div class="counter">
                            <span id="currentCost">{{round($city->trans[0]->cost_of_living)}}</span>&nbsp;/&nbsp;<span
                                    id="totalCost">200</span>
                        </div>
                        <div id="costOfLiving" class="slider"></div>
                    </div>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.crime_rate')</b>
                </div>
                <div class="row-txt">
                    <div class="index-slider-wrap">
                        <div class="counter">
                            <span id="currentRate">{{round($city->trans[0]->geo_stats)}}</span>&nbsp;/&nbsp;<span
                                    id="totalRate">200</span>
                        </div>
                        <div id="crimeRate" class="slider"></div>
                    </div>
                </div>
            </div>
            <div class="post-tip-row">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.quality_of_life')</b>
                </div>
                <div class="row-txt">
                    <div class="index-slider-wrap">
                        <div class="counter">
                            <span id="currentQuolity">{{round($city->trans[0]->demographics)}}</span>&nbsp;/&nbsp;<span
                                    id="totalQuolity">200</span>
                        </div>
                        <div id="qualityOfLife" class="slider"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="etiquette_content">
    <div class="post-block post-tips-list-block" data-content='etiquette' style="margin: 0;">

        <div class="post-list-inner">
            <?php
            $etiquettes = count($city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_ETIQUETTE)->get()) > 0? $city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_ETIQUETTE)->get(): (count($city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_ETIQUETTE)->get()) > 0 ?$city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_ETIQUETTE)->get():[]);
            ?>
            @foreach($etiquettes AS $et)
                <div class="post-tip-row tip-txt">
                    <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>{{@$et['title']}}</b>
                    </div>
                    <div class="row-txt"><span>{{@$et['body']}}</span></div>
                </div>
            @endforeach

        </div>
    </div>

</div>

{{-- restriction --}}
<div id="restriction_content">
    <div class="post-block post-tips-list-block" data-content='restrictions' >
        <div class="post-list-inner">
            <?php
            $restrictions = count($city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_RESTRICTIONS)->get()) > 0? $city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_RESTRICTIONS)->get(): (count($city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_RESTRICTIONS)->get()) > 0 ?$city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_RESTRICTIONS)->get():[]);
            ?>
            @foreach($restrictions AS $rest)
                <div class="post-tip-row tip-txt">
                    <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>{{@$rest['title']}}</b>
                    </div>
                    <div class="row-txt"><span>{{@$rest['body']}}</span></div>
                </div>
            @endforeach
        </div>
    </div>
</div>


<div id="health_content" style="margin: 0;">
    <div class="post-block post-tips-list-block" style="margin: 0;">
        <div class="post-top-layer">
            <div class="top-left">
                <h3 class="post-tip-ttl">@lang('region.risk_of_disease')</h3>
            </div>
        </div>
        <div class="post-list-inner">
            <?php
            $city->trans[0]->health_notes != '' ? $risks = $city->trans[0]->health_notes : $city->country->trans[0]->health_notes;
            $risks = @explode("\n", $risks);
            ?>

            @foreach($risks AS $risk)
                @if(trim($risk)!='')
                    <?php
                    $risk = @explode(":", $risk);
                    ?>
                    @if(@$risk[0]!="Number")
                        <div class="post-tip-row tip-txt">
                            <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                                <b>{{@$risk[0]}}</b></div>
                            <div class="row-txt"><span>{{@$risk[1]}}</span></div>
                        </div>
                    @endif
                @endif
            @endforeach
        </div>
    </div>
</div>

<div id="tripstips_content" >
    <div class="post-block post-tab-block post-tip-tab" style="margin: 0;">
        <div class="post-tab-inner post-tip-tab">
            <div class="tab-item active-tab" data-tab='packing_tips'>
                @lang('region.packing_tips')
            </div>
            <div class="tab-item" data-tab='daily_costs'>
                @lang('region.daily_costs')
            </div>
        </div>
    </div>

    <div class="post-block post-tips-list-block" id="daily_costs" data-content="daily_costs" style="display:none;margin: 0;">
        <?php
        $dcosts = count($city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_DAILY_COSTS)->get()) > 0 ? $city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_DAILY_COSTS)->get(): (count($city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_DAILY_COSTS)->get()) > 0 ? $city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_DAILY_COSTS)->get():[]);
        ?>
        <div class="post-list-inner">
            @foreach($dcosts AS $dcost)
            <?php
                $title = str_replace('_', ' ', ucfirst($dcost->title));
            ?>
                <div class="post-tip-row">
                    <div class="row-label"><b>{{$title}}: </b> {{$dcost->body}}</div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="post-block post-tips-list-block" id="packing_tips" data-content="packing_tips" style="margin: 0;">
        <?php
        $ptips = count($city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_PLANNING_TIPS)->get()) > 0 ? $city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_PLANNING_TIPS)->get(): (count($city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_PLANNING_TIPS)->get()) > 0 ? $city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_PLANNING_TIPS)->get():[]);
        ?>
        <div class="post-list-inner">
        @if (isset($ptips))
            @foreach($ptips AS $ptip)
                <div class="post-tip-row tip-txt">
                    <div class="row-txt"><i class="trav-about-icon"></i> {{$ptip['title']}}: &nbsp;</div>
                    <div class="row-txt"><span>{{$ptip['body']}}</span></div>
                </div>
            @endforeach
        @endif
        </div>
    </div>
</div>

<div id="visa_content"  style="margin: 0;">
    <div class="post-block post-tips-list-block" style="margin: 0;">

        <div class="post-list-inner">
            <div class="post-tip-row tip-txt">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.visa')</b></div>
                <div class="row-txt"><span
                            class="red">@lang('region.required_for_moroccan_passport_holders')</span>
                </div>
            </div>
            <div class="post-tip-row tip-txt">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.id')</b></div>
                <div class="row-txt">
                    <span>@lang('region.proin_cursus_erat_at_lorem_placerat_bibendum_tincidunt')</span></div>
            </div>
            <div class="post-tip-row tip-txt">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp; <b>@lang('region.photos')</b></div>
                <div class="row-txt"><span>@lang('region.mauris_laoreet_nibh_nec_odio_porta_consectetur')</span>
                </div>
            </div>
            <div class="post-tip-row tip-txt">
                <div class="row-label"><i class="trav-about-icon"></i>&nbsp;
                    <b>@lang('region.good_behavior_certificate')</b></div>
                <div class="row-txt"><span>@lang('region.aenean_viverra_mi_at_varius_venenatis')</span></div>
            </div>

        </div>
    </div>
</div>

<div class="post-block post-weather-block" id="weather_content" style="margin: 0;">
        <div class="general-weather-view">
            <div class="day-block-wrap">
                <div class="weather-info">
                    <div class="temperature">
                        <span>{{@$current_weather[0]->Temperature->Metric->Value}}</span><sup>o</sup><span
                                class="temp"><b>C</b> / F</span></div>
                    <div class="weather-day">
                        <div class="day-name">{{weatherDate(@$current_weather[0]->LocalObservationDateTime)}}</div>
                    </div>
                </div>
                <div class="icon-wrap">
                    <img src="{{url('assets2/image/weather/'.@$current_weather[0]->WeatherIcon.'-s.png')}}"
                         alt="{{@$current_weather[0]->WeatherText}}"
                         title="{{@$current_weather[0]->WeatherText}}" class="weather-icon">
                </div>
            </div>
            <div class="weather-txt-block">
                <div class="weather-title">
                    {{@$current_weather[0]->WeatherText}} in <span>{{$city->trans[0]->title}}</span>
                </div>
                <p>It's {{@$current_weather[0]->WeatherText}} right now in {{$city->trans[0]->title}}.
                    The forecast today shows a low
                    of {{@$current_weather[0]->Temperature->Metric->Value}}<sup>o</sup></p>
            </div>
        </div>

        <div class="weather-block-wrapper">
            <div class="weather-carousel-control">
                <a href="#" class="slide-prev"><i class="trav-angle-left"></i></a>
                <a href="#" class="slide-next"><i class="trav-angle-right"></i></a>
            </div>
            <div class="weather-hour-carousel" id="weatherHourCarousel">
                @if($country_weather)
                    @foreach($country_weather AS $cw)
                        <div class="hour-block">
                            <div class="time-block">{{weatherTime($cw->DateTime)}}</div>
                            <div class="weather-image">
                                <img src="{{url('assets2/image/weather/'.$cw->WeatherIcon.'-s.png')}}"
                                    alt="{{$cw->IconPhrase}}" title="{{$cw->IconPhrase}}"
                                    class="weather-icon">
                            </div>
                            <div class="temp-block">{{$cw->Temperature->Value}}<sup>o</sup></div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="day-weather-list">
            @if($daily_weather)
                @php
                    $counter_weather=0; 
                @endphp
                @foreach($daily_weather AS $dw)
                    @if($counter_weather != 0)
                        <div class="day-weather-block">
                            <div class="day-block-wrap">
                                <div class="weather-icon">
                                    <img src="{{url('assets2/image/weather/'.$dw->Day->Icon.'-s.png')}}"
                                        alt="{{$dw->Day->IconPhrase}}" title="{{$dw->Day->IconPhrase}}"
                                        class="weather-icon">
                                    <span class="temperature">{{$dw->Temperature->Maximum->Value}}
                                        <sup>o</sup></span>
                                </div>
                                <div class="weather-day">
                                    <div class="day-name">{{date("l", $dw->EpochDate)}}</div>
                                    <div class="weather-desc">{{$dw->Day->IconPhrase}}</div>
                                </div>
                            </div>
                            <div class="btn-wrap">
                                {{-- <button type="button"
                                        class="btn btn-light-grey btn-bordered">@lang('region.buttons.plan_a_trip')
                                </button> --}}
                            </div>
                        </div>
                    @endif
                    @php
                        $counter_weather++;
                    @endphp
                @endforeach
            @endif
        </div>
    </div>

<div class="post-block post-tips-list-block" id="whentogo_content" style="margin: 0;">
        <div class="post-top-layer">
            <div class="top-left">
                <h3 class="post-tip-ttl_lg">@lang('region.best_time_to_go')</h3>
            </div>
            <div class="top-right">
                {{-- <a  class="tip-link "><i
                            class="trav-open-video-in-window "></i>&nbsp; <span class="flyToLink">@lang('region.flight_search')</span></a> --}}
            </div>
        </div>
        <?php
        $best_time = count($city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_BEST_TIME)->get()) > 0 ? $city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_BEST_TIME)->get() : $city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_BEST_TIME)->get();
        $months = array(
            'january',
            'february',
            'march',
            'april',
            'may',
            'june',
            'july',
            'august',
            'september',
            'october',
            'november',
            'december'
        );
        $BT = array();
        foreach ($best_time as $btime) {
            $season = explode(",", $btime->body);
            $title = $btime->title;
        
            if (count($season) > 1){
                foreach ($season as $start_end){
                    $start_end = explode("-", $start_end);
                    if ($start_end[0]<$start_end[1]) {
                        for ($i=$start_end[0]; $i <= $start_end[1]; $i++) { 
                            $BT[$months[$i - 1]] = $title;
                        }
                    } 
                    if ($start_end[0]>$start_end[1]) {
                        for ($i = $start_end[0]; $i <= 12; $i++) { 
                            $BT[$months[$i - 1]] = $title;
                        }
                        for ($i = 1; $i <= $start_end[1]; $i++) { 
                            $BT[$months[$i - 1]] = $title;
                        }
                    }
                }
            } elseif (count($season) == 1) {
                $start_end = explode("-", $btime->body);
                if ($start_end[0]<$start_end[1]) {
                    for ($i=$start_end[0]; $i <= $start_end[1]; $i++) { 
                        $BT[$months[$i - 1]] = $title;
                    }
                } 
                if ($start_end[0]>$start_end[1]) {
                    for ($i = $start_end[0]; $i <= 12; $i++) { 
                        $BT[$months[$i - 1]] = $title;
                    }
                    for ($i = 1; $i <= $start_end[1]; $i++) { 
                        $BT[$months[$i - 1]] = $title;
                    }
                }
            }
        }
        ?>
        <div class="post-flight-content">
            <div class="progress-block">
                <ul class="color-list">
                    <li class="low">
                        @lang('region.low_season')
                    </li>
                    <li class="mid">
                        @lang('region.shoulder')
                    </li>
                    <li class="high">
                        @lang('region.high_season')
                    </li>
                </ul>
                <div class="progress-inner">
                    @foreach($months AS $key => $month)

                        <?php
                        if (@$BT[$month] == 'low_season') {
                            $progress = 'low';
                            $width = 20;
                        } elseif (@$BT[$month] == 'shoulder') {
                            $progress = 'mid';
                            $width = 40;
                        } elseif (@$BT[$month] == 'high_season') {
                            $progress = 'high';
                            $width = 80;
                        } else {
                            $progress = '';
                            $width = '';
                        }
                        ?>


                        <div class="progress-row">
                            <div class="progress-label">{{@$month}}</div>
                            <div class="progress-wrapper">
                                <div class="progress {{@$progress}}">
                                    <div class="progress-bar" style="width:{{@$width}}%"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

<script>
    $('#pagination').fadeOut();
    $('.more-link').on('click',function(){
        var type = $(this).data('type');
       $('.about-title').text(type);
       str = '';
       if(type == 'Relegions'){
           var relegion = {!! json_encode($city->religions->toArray()) !!};
        if(relegion.length == 0)
           var relegion = {!! json_encode($city->country->religions->toArray()) !!};
        if(relegion.length >1){
            $.each(relegion,function(index,val){
                if(index != 0 && typeof val.trans !='undefined' && typeof val.trans[Object.keys(val.trans)[0]].title !='undefined' && val.trans[Object.keys(val.trans)[0]].title !=null){
                    str += '<div class="row" dir="auto">';
                    str += '<div class="w-100" style="border-bottom: 1px solid #EDEDED;padding-bottom: 10px;padding-left: 10px;" dir="auto">';
                    str += '<i class="trav-about-icon"></i> <span style="display:inline-block;font-family:inherit" dir="auto"><strong style="font-weight:500" dir="auto">'+ val.trans[Object.keys(val.trans)[0]].title+ '</strong></span>';
                    str += '</div>'
                    str += '</div>';
                }
            });
        }

       }else if(type == 'Time Zones'){

       }else if(type == 'Languages'){
            var languages =  {!! json_encode($city->languages) !!};
            if(languages ==null || typeof languages =='undefined' || Object.keys(languages).length == 0 )
                var languages =  {!! json_encode($city->country->languages) !!};
            if(languages !=null && typeof languages !='undefined' && Object.keys(languages).length >1){
                $.each(languages,function(index,val){
                    if(index != 0 &&  typeof val.trans !='undefined' && typeof val.trans.title !='undefined' && val.trans.title!=null){
                        str += '<div class="row" dir="auto">';
                        str += '<div class="w-100" style="border-bottom: 1px solid #EDEDED;padding-bottom: 10px;padding-left: 10px;" dir="auto">';
                        str += '<i class="trav-about-icon"></i> <span style="display:inline-block;font-family:inherit" dir="auto"><strong style="font-weight:500" dir="auto">'+ val.trans.title+ '</strong></span>';
                        str += '</div>'
                        str += '</div>';
                    }
                });
            }

       }else{
        var speed_limit =  {!! json_encode($city->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_SPEED_LIMIT)->get()->toArray()) !!};
        console.log(speed_limit);
        if(speed_limit == '')
        var speed_limit =  {!! json_encode($city->country->abouts()->where('type', App\Models\City\CitiesAbouts::TYPE_SPEED_LIMIT)->get()->toArray()) !!};
            if(speed_limit.length >1){
                console.log(speed_limit);
                $.each(speed_limit,function(index,val){
                    console.log(val.title)
                    if(index != 0 && val.title !=null){
                        str += '<div class="row" dir="auto">';
                        str += '<div class="w-100" style="border-bottom: 1px solid #EDEDED;padding-bottom: 10px;padding-left: 10px;" dir="auto">';
                        str += '<i class="trav-about-icon"></i> <span style="display:inline-block;font-family:inherit" dir="auto"><strong style="font-weight:500" dir="auto">'+ val.title + ': ' + val.body + '</strong></span>';
                        str += '</div>'
                        str += '</div>';
                    }
                });
            }
        }
       $('.about-body').html(str);

    });
</script>
<div class="modal fade" id="modal-more-abouts" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="height: 45px;">
                <h5 style="padding-bottom: 10px;" class="modal-title w-100 about-title"></h5>
                <button type="button" class="close" style="color:#A7A7A7" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body about-body" style="overflow: hidden;height: 400px;overflow-y: scroll;">
            </div>
        </div>
    </div>
</div>