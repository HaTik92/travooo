<div class="modal fade" id="asking-expert-information-modal" tabindex="-1" role="dialog"
    aria-labelledby="likesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header d-block">
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-title text-center" id="exampleModalLabel">Get Assistance from <strong>Travooo
                        Experts</strong>
                    <br>
                    Ask experts to help you find the best places to visit, get recomendations and even edit your trip
                    plans
                </div>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="d-block expert-info-img-div">
                            <img src="{{asset('assets2/image/left-expert-info-img.png')}}" alt="Travooo" dir="auto">
                        </div>
                        <div style="height:200px">
                            <label><strong>Ask Experts for Advice</strong></label>
                            <p style="line-height: 20px;">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. .
                            </p>
                            <p style="line-height: 20px;">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            </p>
                        </div>
                        <a href="#">Call to Action </a>
                    </div>
                    <div class="col">
                        <div class="d-block expert-info-img-div">
                            <img src="{{asset('assets2/image/right-expert-info-img.png')}}" alt="Travooo" dir="auto">
                        </div>
                        <div style="height:200px">
                            <label><strong>Invite an Expert to your Trip Plan</strong></label>
                            <p style="line-height: 20px;">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has
                                survived not only five centuries..
                            </p>
                            <p style="line-height: 20px;">
                                Lorem Ipsum has been the industry's.
                            </p>
                        </div>
                        <a href="#">Call to Action </a>
                    </div>
                </div>
                <div class="row mt-1">
                    <div class="col">
                        <label>
                            <strong>
                                Expert can also:
                            </strong>
                        </label>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been
                            the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley
                            of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries,
                            but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>