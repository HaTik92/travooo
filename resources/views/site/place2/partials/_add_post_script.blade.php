<script>
    $(document).ready(function() {
        $('.add-post__emoji').emojiInit({
            fontSize:20,
            success : function(data){

            },
            error : function(data,msg){
            }
        });
    });
    $(".add-post__post-type").click(function(){
        $(".add-post__post-type").removeClass("add-post__post-type--active");
        $(this).addClass("add-post__post-type--active");
    });

    $(document).on('click', '.add-post__post-type', function(){
        var data_type = $(this).attr('data-type');
        if( data_type == "image" )
        {
            if($(this).attr('data-triggered') == 'post')
                $("#_add_post_imagefile").click();
            else 
                $("#_add_review_imagefile").click();
        }
        else if( data_type == "video" )
        {
            $("#_add_post_videofile").click();
        }
    });

    $("#_add_post_imagefile , #_add_review_imagefile").change(function(selector){
        var triggered = selector.target.id;
        var fileArray = this.files;
        $.each(fileArray,function(i,v){
            var file  = v;
            var imagefile = file.type;
            console.log(file);
            var match = ["image/jpeg", "image/png", "image/jpg"];
            if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])) && !/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type))
            {
                $('#cover').css('background-image', 'url(noimage.png)');
                $("#message").html("<p style='color:red'>Please Select A valid Image File</p>");
                $("#_add_post_imagefile").val("");
                $("#_add_review_imagefile").val("");
                return false;
            } 
            else
            {
                var fRead = new FileReader();
                fRead.onload = (function(file){
                    return function(e) {
                        var count = $('.img-wrap-newsfeed').children().length;
                        var fd = new FormData();
                        fd.append('mediafiles',e.target.result);
                        console.log(fd);
                        $.ajax({
                        method: "POST",
                        url: "/place/upload-media-content",
                        data: fd,
                        contentType: false,
                        processData:false,
                        async: true,
                        xhr: function() {
                            var xhr = $.ajaxSettings.xhr() ;
                            xhr.upload.addEventListener("progress", function(evt) {
                        
                                if (evt.lengthComputable) {
                                    var percentComplete = ((evt.loaded / evt.total) * 100);
                                    if(triggered == '_add_review_imagefile')
                                        $('#'+count+'_media_content_review').width(percentComplete/2.5 + 'px');
                                    else 
                                        $('#'+count+'_media_content').width(percentComplete/2.5 + 'px');
                                }
                            }, false);
                            return xhr;
                        },
                        beforeSend: function(){
                        $('.add-post__footer-right').hide();
                            var media ='';
                            if(!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))){
                                media = $('<video style="object-fit:cover;border-radius:5px" width="40" height="40" controls>'+
                                '<source src='+e.target.result+' type="video/mp4">'+
                                '<source src="movie.ogg" type="video/ogg">'+
                                '</video>');
                                // media = $('<img/>').addClass('thumb').attr('src', 'https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_play_arrow_48px-512.png');

                            }else{
                                media = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
                            }
                            var button = $('<span/>').addClass('close').addClass('removeFile').click(function(){   
                                //create close button element
                                if(triggered == '_add_review_imagefile')
                                    $('#'+count+'_media_attached_review').remove();
                                else
                                    $('#'+count+'_media_attached').remove();        
                                $(this).parent().remove();
                            }).append('<span class="remove-file-close-btn">&times;</span>');

                            
                            var imgitem = $('<div>').append(media); //create Image Wrapper element
                                var progressdivcontainer ='';
                                if(triggered == '_add_review_imagefile'){
                                    progressdivcontainer = $('<div  id="'+count+'_media_content_review" style="border-radius: 5px;position: absolute;opacity: 1;height: 10px;background: green;width: 39px;margin-top: 14px;" />').addClass('media-progress-bar');
                                    imgitem = $('<div/>').addClass('img-wrap-newsfeed').append(progressdivcontainer).append(imgitem).append(button);
                                    $(".add-post__media_review").append(imgitem);
                                }else{ 
                                    progressdivcontainer = $('<div  id="'+count+'_media_content" style="border-radius: 5px;position: absolute;opacity: 1;height: 10px;background: green;width: 39px;margin-top: 14px;" />').addClass('media-progress-bar');
                                    imgitem = $('<div/>').addClass('img-wrap-newsfeed').append(progressdivcontainer).append(imgitem).append(button);
                                    $(".add-post__media").append(imgitem);
                                }
                        
                        },
                        }).done(function(data){
                            var inputitem = '';
                            if(triggered == '_add_review_imagefile'){
                                inputitem = $('<input type="hidden" id="'+count+'_media_attached_review" class="attachmentElm" name="mediafiles[]" />').val(data);
                                $('#_place_add_review_form').append(inputitem);
                                $('.add-post__footer-right').show();
                                $('#'+count+'_media_content_review').width('0px');
                            }
                            else {
                                inputitem = $('<input type="hidden" id="'+count+'_media_attached" class="attachmentElm" name="mediafiles[]" />').val(data);
                                $('#_place_add_post_form').append(inputitem);
                                $('.add-post__footer-right').show();
                                $('#'+count+'_media_content').width('0px');
                            }
                            
                        });

                    };
                })(file);
                fRead.readAsDataURL(file); //URL representing the file's data.
               
            }
        });
        $("#_add_post_imagefile").val("");
        $("#_add_review_imagefile").val("");
    });

    $("#_add_post_videofile").change(function(){

        var file = this.files[0];
        
        if ( !/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type) )
        {
            $('#cover').css('background-image', 'url(noimage.png)');
            $("#message").html("<p style='color:red'>Please Select A valid Image File</p>");
            $("#_add_post_videofile").val("");
            return false;
        } 
        else
        {
            var fRead = new FileReader();
            fRead.onload = (function(file){
                return function(e) {

                    var video = $('<video style="object-fit:cover;" width="151" height="130" controls>'+
                    '<source src='+e.target.result+' type="video/mp4">'+
                    '<source src="movie.ogg" type="video/ogg">'+
                    '</video>');
                    
                    var button = $('<span/>').addClass('close').addClass('removeFile').click(function(){     //create close button element
                        $(this).parent().remove();
                    }).append('<span>&times;</span>');

                    var inputitem = $('<input type="hidden" name="mediafiles[]" />').val(e.target.result);

                    var videoitem = $('<div/>').append(video);
                    videoitem = $('<div/>').addClass('img-wrap-newsfeed').append(videoitem).append(button).append(inputitem);

                    $(".add-post__media").append(videoitem);
                };
            })(file);
            fRead.readAsDataURL(file); //URL representing the file's data.
            $("#_add_post_videofile").val("");
        }
    });

    $(document).on('click', '.comment__like-button', function (e) {
        var commentId = $(this).attr('id');
        var obj = $(this);
        $.ajax({
            method: "POST",
            url: "{{ route('post.commentlikeunlike') }}",
            data: {comment_id: commentId}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if (res.status == 'yes') {


                } else if (res == 'no') {

                }
                obj.find('strong').html(result.count);
            });
        e.preventDefault();
    });

    $(document).on('click', '.report_comment__like-button', function (e) {
        var commentId = $(this).attr('id');
        var obj = $(this);
        $.ajax({
            method: "POST",
            url: "{{ route('report.commentlike') }}",
            data: {comment_id: commentId}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if (res.status == 'yes') {


                } else if (res == 'no') {

                }
                obj.find('strong').html(result.count);
            });
        e.preventDefault();
    });

  
    $(document).on('submit', '.reportscomment', function(e){
        var form = $(this);
        var text = form.find('textarea').val().trim();

        if(text == "")
        {
            return false;
        }
        
        var report_id = form.attr('id');
        var all_comment_count = $('.'+ report_id +'-all-comments-count').html(); 
        var values = $(this).serialize();
        form.find('button[type=submit]').attr('disabled', true);
        $.ajax({
            method: "POST",
            url: "{{ route('report.postcomment') }}",
            data: values
        })
            .done(function (res) {
                $("#" + report_id + ".comment-not-fund-info").remove();
                $("#" + report_id + ".comments__header").show();
                $('.'+ report_id +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                
                form.closest('.comments').find('.comments__items').prepend(res);
                var comments = form.closest('.comments').find('.comments__items').find('.comment').length;
                form.closest('.post').find('.post__comment-btn').find('strong').html(comments);
                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('button[type=submit]').removeAttr('disabled');
                
                Comment_Show.reload($('.pp-report-'+report_id)); 
                
            });
        e.preventDefault();
    });

</script>

<script>

    function report_comment_delete(id, obj)
    {
        if(!confirm("Are you sure delete this comment?"))
            return;
        var commentId = id;
        $.ajax({
            method: "POST",
            url: "{{ route('report.commentdelete') }}",
            data: {comment_id: commentId}
        })
            .done(function (res) {

                $(obj).closest('.comment').remove();
            });
    }
    function _place_send_review(obj){


        if ($("#_place_add_review_form").find('textarea').val().trim() == "" ) return;
        var buttons = $(obj).closest(".add-post__footer");
        var loading = buttons.next();
        buttons.hide();
        loading.show();
        var score = 0;
        if(typeof $("input[name='review_score']:checked").val() !='undefined')
            score = $("input[name='review_score']:checked").val();

        $.ajax({
                method: "POST",
                url: "{{url(session('place_country') .'/'.$place->id.'/ajaxPostReview')}}?text="+$('#add_review_text').val()+"&score="+score,
                data:$('#_place_add_review_form').serializeArray(),
            })
                .done(function (res) {
                res = jQuery.parseJSON(res);
                if(res.success)
                {
                    $("#newsfeed_content").prepend(res.posted_review);
                    $("#_place_add_review_form").find('textarea').val('');
                }
                buttons.show();
                loading.hide();

                });
    }
    function _place_send_post(obj)
    {

        // var type = $(".add-post__post-type--active").attr("data-type");
        // if(!type)
        //     return;
            if($("#_place_add_post_form").find('#createPostTxt').val().trim() == "" && $("#_place_add_post_form").find("input[name^=mediafiles]").length == 0)
            return;
            // switch (type) {
        //     case 'text':
        //         if ($("#_place_add_post_form").find('textarea').val().trim() == "" ) return;
        //         break;
        
        //     case 'image':
        //     case 'video':
        //         if ($("#_place_add_post_form").find("input[name^=mediafiles]").length == 0) return;
                
        //         break;

        //     case 'info':
        //         return;
        //         break;
        //     case 'map':
        //         return;
        //         break;
        //     default:
        //         break;
        // }
        if($("#_place_add_post_form").find('#createPostTxt').val().trim() != "") 
            type ='text';
        
        if($("#_place_add_post_form").find("input[name^=mediafiles]").length != 0)
            type ='image';
        
        $("#_place_add_post_form").find("input[name=type]").val(type);

        var buttons = $(obj).closest(".add-post__footer");
        var loading = buttons.next();
        buttons.hide();
        loading.show();
        $.post(
            "{{route(session('place_country') . '.addpost', $place->id)}}",
            {data: $("#_place_add_post_form").serializeArray()},
            function(res)
            {
                if(res != "error")
                {
                    $("#newsfeed_content").prepend(res);
                    $("#_place_add_post_form").find('#createPostTxt').next('.note-editor').find('.note-editable').html('');
                    $(".add-post__media").html('');
                    $("#_place_add_post_form").find("input[name=type]").val('text');
                    $(".attachmentElm").remove();
                    $("#checkinPost").val('');
                    $("#fromPostBox").val('0');
                    $( ".add-post__post-type" ).each(function() {
                        if($(this).attr("data-type")!='text') {
                            $( this ).removeClass( "add-post__post-type--active" );
                        }
                        else {
                             $( this ).addClass( "add-post__post-type--active" );
                        }
                      }); 
                    $('input[name="permission"]').each(function() { 
                        this.checked = false; 
                    }); 
                    $('#public_permission').prop('checked', true);
                    // $("#add_post_permission_button").html($(".add_post_permission:first").find('label').html());
                    
  
                       
                      
                }

                buttons.show();
                loading.hide();
            }
        );
    }

    function post_delete(param, type, obj, e,target=false) 
    {
        if(confirm('Are you sure you want to delete this post?'))
        {
            $.ajax({
                method: "POST",
                url: "{{ route('post.delete') }}",
                data: {post_id: param, post_type: type} 
            })
            .done(function(res) {
                result = JSON.parse(res);
                if(result.status == "yes")
                {
                    if(!target)
                        $(obj).closest('.post').remove();
                    else
                        $(obj).closest('.review_box_div').remove();
                }else{
                    //
                    alert(result.status);
                }
            });
        }

        e.preventDefault();
    }
   

</script>