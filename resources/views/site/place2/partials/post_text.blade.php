<div class="post-block" filter-tab="#top" @if(isset($search_result)) search-tab="#searched" @endif>
    <div class="post-top-info-layer">
        <div class="post-top-avatar-wrap" dir="auto">
            <a class="post-name-link" href="{{url('profile/'.$post->author->id)}}">
                <img class="post__avatar" src="{{check_profile_picture($post->author->profile_picture)}}" alt="{{$post->author->name}}" title="{{$post->author->name}}" role="presentation" />
            </a>
        </div>
        <div class="post-top-info-txt">
            <a class="post__username" href="{{url_with_locale('profile/'.$post->author->id)}}">{{$post->author->name}}</a>
            @if(isset($checkins->checkin_time))

                @if(strtotime($checkins->checkin_time) == strtotime(date('Y-m-d')))
                    <span>Checked on this place at : {{$checkins->checkin_time}}</span>
                @elseif(strtotime($checkins->checkin_time) < strtotime(date('Y-m-d')))
                    <span>Previously checked on this place at : {{$checkins->checkin_time}}</span>
                @elseif(strtotime($checkins->checkin_time) > strtotime(date('Y-m-d')))
                    <span>Will check on this place at : {{$checkins->checkin_time}}</span>
                @endif
            @endif
            {!! get_exp_icon($post->author) !!}
            <div class="post__posted">
                Published a
                <strong>post</strong>
                about
                @if(isset($selected_place))
                    <strong>{{@$selected_place['title']}}</strong>, Place in  <strong>{{@$place->transsingle->title}}</strong>
                @elseif(isset($selected_city))
                    <strong>{{@$selected_city['title']}}</strong>, City in  <strong>{{@$place->transsingle->title}}</strong>
                @else
                    <strong>{{@$place->transsingle->title}}</strong>
                @endif
                <a href="#modal-text-post-popup" data-post_id="{{$post->id}}" data-toggle="modal">{{diffForHumans($post->created_at)}}</a>
                <?php
                switch($post->permission){
                    case(0):
                ?>
                        <svg class="icon icon--earth">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#earth')}}"></use>
                        </svg>
                <?php
                        break;
                    case(1):
                ?>
                        <svg class="icon icon--earth">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#follow')}}"></use>
                        </svg>
                <?php
                        break;
                    case(2):
                ?>
                        <svg class="icon icon--earth">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#user')}}"></use>
                        </svg>
                <?php
                        break;
                }
                ?>

            </div>
        </div>
        <div class="dropdown">
            <button class="post__menu" type="button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="icon icon--angle-down">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                </svg>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post">
                @if(Auth::check() && Auth::user()->id==$post->users_id)
                    <a class="dropdown-item" href="#" onclick="post_delete('{{$post->id}}','Post', this, event)">
                        <span class="icon-wrap">
                            <!--<i class="trav-flag-icon-o"></i>-->
                        </span>
                        <div class="drop-txt">
                            <p><b>@lang('buttons.general.crud.delete')</b></p>
                            <p style="color:red">@lang('home.remove_this_post')</p>
                        </div>
                    </a>
                @else
                    @include('site.home.partials._info-actions', ['post'=>$post])
                @endif
            </div>
        </div>

    </div>
    <div class="post__content">
        <div class="post__text place__post_text">
            @php
               $showChar = 100;
               $ellipsestext = "...";
               $moretext = "see more";
               $lesstext = "see less";

               $content = $post->text;

               if(strlen($content) > $showChar) {

                   $c = substr($content, 0, $showChar);
                   $h = substr($content, $showChar, strlen($content) - $showChar);

                   $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                   $content = $html;
               }
            @endphp
        {!! $content !!}
        </div>
    </div>
    <div class="post__footer">
        <button class="post__like-btn post_like_button" style="outline:none;" type="button" id="{{$post->id}}">
            <svg class="icon icon--like">
            <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
            </svg>
        </button>
        @if(count($post->likes))<a href='#' class="post_likes_modal" id='{{$post->id}}'>@endif
            <span class="like-count-inner" id="post_like_count_{{$post->id}}"><strong>{{count($post->likes)}}</strong> Likes</span>
            @if(count($post->likes))</a>@endif

        <button class="post__comment-btn" style="outline:none;" type="button" data-tab="post__commenttext_{{$post->id}}">
            <svg class="icon icon--comment">
            <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
            </svg>
            <div class="user-list">
                <div class="user-list__item">
                    @foreach($post->comments()->groupBy('users_id')->get() AS $pco)
                    <div class="user-list__user"><img class="user-list__avatar" src="{{check_profile_picture($pco->author->profile_picture)}}" alt="{{$pco->author->name}}" title="{{$pco->author->name}}" role="presentation" /></div>
                    @endforeach
                </div>
            </div><span><strong>{{count($post->comments)}}</strong> Comments</span>
        </button>

        <button class="post__like-btn post_share_button" type="button" id="{{$post->id}}">
            <svg class="icon icon--like" style="color: #4080ff">
            <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
            </svg>
            <span id="post_share_count_{{$post->id}}"><strong>{{count($post->shares)}}</strong> Shares</span>
        </button>
    </div>
    <div class="comments" style="display:none;" data-content="post__commenttext_{{$post->id}}">
        <div class="comments__header">
            <div class="comments__filter">
                <button class="comments__filter-btn" type="button" data-type="Top">Top</button>
                <button class="comments__filter-btn active" type="button" data-type="New">New</button></div>
            <div class="comm-count-info">
                        <strong>0</strong> / <span class="{{$post->id}}-all-comments-count">{{count($post->comments)}}</span>
            </div>
        </div>
        <div class="comments__items post-comment-wrapper place-comment-block sortBody">
            @foreach($post->comments AS $ppc)
                @include('site.place2.partials._post_comment_block')
            @endforeach
        </div>
        @if(Auth::user())
                <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-inputs">
                <form class="postscomment" method="POST" action="{{url("new-comment") }}" id="{{$post->id}}" autocomplete="off" enctype="multipart/form-data">
                    <input type="hidden" data-id="pair{{$post->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                    <div class="post-create-block post-comment-create-block place-comment-block post-regular-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" data-id="postscommenttext" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias place-media">
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <li class="post-options">
                                        <input type="file" name="file[]" class="post-comment-media-input" data-post_id="{{$post->id}}" id="postcommentfile{{$post->id}}" style="display:none" multiple>
                                        <i class="fa fa-camera click-target" data-target="postcommentfile{{$post->id}}"></i>
                                    </li>
                                </ul>
                            </div>

                            <button type="submit" class="btn btn-primary btn-disabled d-none"></button>

                        </div>

                    </div>
                    <input type="hidden" name="post_id" value="{{$post->id}}"/>
                </form>

                </div>
            </div>

        @endif
    </div>
</div>
