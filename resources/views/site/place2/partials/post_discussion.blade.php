<div class="post" filter-tab="#discussion" @if(isset($search_result)) search-tab="#searched" @endif>

    <div class="post__header"><img class="post__avatar" src="{{ check_profile_picture(@$discussion->author->profile_picture) }}" alt="" role="presentation">
        <div class="post__title-wrap"><a class="post__username" href="{{url_with_locale('profile/'.@$discussion->author->id)}}">{{@$discussion->author->name}}</a>
            {!! get_exp_icon(@$discussion->author) !!}
            <div class="post__posted">Asked for <strong>recommendations</strong> in {{$place->transsingle->title}} at {{diffForHumans(@$discussion->created_at)}}</div>
        </div>
        <div class="dropdown">
            <button class="post__menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="icon icon--angle-down">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                </svg>
            </button>
            @if (Auth::check() && Auth::user()->id !== @$discussion->author->id)
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion">
                    @include('site.home.partials._info-actions', ['post'=>$discussion])
                </div>
            @endif
        </div>

    </div>
    <div class="post__content">
        <div class="post-link">
            <div class="post-link__content" style="width:100%;">
                <h3 class="post-link__title disc-modal" data-dis_id="{{$discussion->id}}" style="overflow-wrap: break-word;">

                    {{$discussion->question}}
                </h3>
                <div class="post-link__text dis_description_{{$discussion->id}}" style="overflow-wrap: break-word;">
                    @php
                        $showChar = 100;
                        $ellipsestext = "...";
                        $moretext = "see more";
                        $lesstext = "see less";

                        $description = $discussion->description;

                        if(strlen($description) > $showChar) {

                            $c = substr($description, 0, $showChar);
                            $h = substr($description, $showChar, strlen($description) - $showChar);

                            $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                            $description = $html;
                        }
                    @endphp
                    {!!$description!!}
                </div>
            </div>
        </div>
    </div>


    <div class="post__tips">
    @if(@$latest_tip = $discussion->replies()->orderBy('created_at', 'desc')->first())
        @if($discussion->type==1)
            <h3 class="post__tips-title">@lang('discussion.strings.top_recommendations_by_users')</h3>
        @elseif($discussion->type==2)
            <h3 class="post__tips-title">@lang('discussion.strings.top_tips_by_users')</h3>
        @elseif($discussion->type==3)
            <h3 class="post__tips-title">@lang('discussion.strings.top_planning_tips_by_users')</h3>
        @elseif($discussion->type==4)
            <h3 class="post__tips-title">@lang('discussion.strings.top_answers_by_users')</h3>
        @endif
        <div class="post__tip">
            <div class="post__tip-main">
                <img class="post__tip-avatar" src="{{ check_profile_picture(@$latest_tip->author->profile_picture) }}" alt="" role="presentation">
                <div class="post__tip-content">
                    <div style="margin: 0 0 1em 0;" class="post__tip-header">
                        <a href="#">{{ @$latest_tip->author->name }}</a>{!! get_exp_icon(@$latest_tip->author) !!} said<span class="post__tip-posted">{{ diffForHumans(@$latest_tip->created_at) }}</span>
                    </div>
                    <div class="post__tip-text">{{ $latest_tip->reply }}</div>
                </div>
            </div>
            <div class="post__tip-footer">
                <button class="post__tip-upvotes vote-btn-{{ $latest_tip->id }}" type="button" onclick="DISCUSSION_POST.upvoteReply({{ $latest_tip->id }}, this, event);">
                    <svg class="icon icon-vote icon--angle-up-solid">
                        <use xlink:href="#angle-circle-up-solid"></use>
                    </svg>
                    <span><strong class="num_votes">{{ @count($latest_tip->upvotes) }}</strong>  Upvotes</span>
                </button>
                <button class="post__tip-downvotes vote-btn-{{ $latest_tip->id }}" type="button" onclick="DISCUSSION_POST.downvoteReply({{ $latest_tip->id }}, this, event);">
                    <svg class="icon icon-vote icon--angle-down-solid">
                        <use xlink:href="#angle-circle-up-solid"></use>
                    </svg>
                    <span><strong class="num_votes">{{ @count($latest_tip->downvotes) }}</strong>  Downvotes</span>
                </button>
                @if( @count($discussion->replies) > 1 )
                <button class="post__tip-more" type="button" style="outline:none;" onclick="DISCUSSION_POST.showDiscussionModal({{$discussion->id}})">
                    <div class="user-list">
                        <div class="user-list__item">
                            @foreach( $discussion->replies as $rep )
                            @if( $loop->index == 3 ) @break @endif
                            <div class="user-list__user"><img class="user-list__avatar" src="{{ check_profile_picture(@$rep->author->profile_picture) }}" alt="" role="presentation"></div>
                            @endforeach
                        </div>
                    </div>
                    <span><strong>{{ @(count($discussion->replies) - 1) }} </strong> more tips</span>
                </button>
                @endif
            </div>
        </div>

    @endif

    </div>
    {{--@if( @count($discussion->replies) > 1 ) --}}
    <div style="padding-left: 30px; padding-bottom: 10px; display: none">
        <button class="post__tip-loadmore" type="button" reply-id="{{ $discussion->id }}" page_num="1">
            <svg class="icon icon--angle-up-solid">
                <use xlink:href="{{asset('assets3/img/sprite.svg#add')}}" style="color: rgb(64, 128, 255)"></use>
            </svg>
            <span>load more</span>
        </button>
    </div>
    {{--@endif--}}

    <div class="comments" style="display:none;" data-content="share__commenttext_{{$discussion->id}}">
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-input">
                <form class="dicussioncomment" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data" data-id="{{$discussion->id}}">
                    <div class="post-block post-create-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" id="discussioncommenttext" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:40px;" placeholder="@lang('comment.write_a_reply')"></textarea>
                            <div class="medias">
                                <!-- <div class="plus-icon" style="display: none;">
                                    <div>
                                        <span class="close" onclick="javascript:$('#commentfile').click();"><span style="font-size:110px;">+</span></span>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <!-- <li class="post-options">
                                        <input type="file" name="file[]" id="commentfile" style="display:none" multiple>
                                        <i class="trav-camera click-target" data-target="commentfile"></i>
                                    </li> -->
                                </ul>
                            </div>

                            <button type="submit" class="btn btn--sm btn--main">@lang('buttons.general.send')</button>

                        </div>

                    </div>
                </form>

                </div>
            </div>

        @endif
    </div>
</div>


<!-- MODALS -->
     <?php
     $dis = $discussion;
     ?>
     <div class="modal fade white-style" data-backdrop="false" id="Discussion{{$dis->id}}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-850" role="document">
        <div class="modal-custom-block">

            <div class="post-block post-happen-question">
                <div class="post-top-info-layer">
                    <div class="post-top-info-wrap">
                        <div class="post-top-avatar-wrap">
                            <img src="{{check_profile_picture(@$dis->author->profile_picture)}}" alt=""
                                 style="width:50px;height:50px;">
                        </div>
                        <div class="post-top-info-txt">
                            <div class="post-top-name">
                                <a class="post-name-link" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">{{@$dis->author->name}}</a>
                                {!! get_exp_icon(@$dis->author) !!}
                            </div>
                            <div class="post-info">
                                @lang('discussion.strings.asked_for') {{$dis->getType->type}}s about <a
                                        href="{{url_with_locale($dis->destination_type.'/'.$dis->destination_id)}}"
                                        class="link-place">{{getDiscussionDestination($dis->destination_type, $dis->destination_id)}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown">
                        <div class="post-top-info-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <a class="post-collapse" href="#">
                                <i class="trav-angle-down"></i>
                            </a>
                        </div>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion">

                            @if(Auth::check())
                                @if(Auth::user()->id==@$dis->author->id)
                                    <a class="dropdown-item" href="#" onclick="discussion_delete('{{$dis->id}}', event)">
                                        <span class="icon-wrap">
                                            <img src="{{asset('assets2/image/delete.svg')}}" style="width:20px;">
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>@lang('buttons.general.crud.delete')</b></p>
                                            <p style="color:red">@lang('home.remove_this_post')</p>
                                        </div>
                                    </a>
                                @else
                                    @include('site.home.partials._info-actions', ['post'=>$dis])
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <div class="post-content-inner">
                    <div class="post-main-content">
                        <h3 class="simple dis_question_{{$dis->id}}" style="overflow-wrap: break-word;">
                        @php
                            $showChar = 50;
                            $ellipsestext = "...";
                            $moretext = "see more";
                            $lesstext = "see less";

                            $content = $dis->question;

                            if(strlen($content) > $showChar) {

                                $c = substr($content, 0, $showChar);
                                $h = substr($content, $showChar, strlen($content) - $showChar);

                                $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                                $content = $html;
                            }
                        @endphp
                        {!!$content!!}
                        </h3>
                        <p class="dis_description_{{$dis->id}}" style="overflow-wrap: break-word;">
                        @php
                            $showChar = 100;
                            $ellipsestext = "...";
                            $moretext = "see more";
                            $lesstext = "see less";

                            $content = $dis->description;

                            if(strlen($content) > $showChar) {

                                $c = substr($content, 0, $showChar);
                                $h = substr($content, $showChar, strlen($content) - $showChar);

                                $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                                $content = $html;
                            }
                        @endphp
                        {!!$content!!}</p>

                        <div class="post-modal-content-foot">
                            <div class="post-modal-foot-list">
                                <span>@lang('other.at_date', ['date' => weatherTime(@$dis->created_at)])</span>
                                <span class="dot"> · </span>
                                <span>@lang('other.views')  <b><span class="{{$dis->id}}-view-count">{{count($dis->views)}}</span></b></span>
                                <span class="dot"> · </span>
                                <span>@lang('discussion.strings.follow') <b>{{count($dis->follows)}}</b></span>

                            </div>
                        </div>
                    </div>
                    @if(Auth::check())
                    <div class="post-add-comment-block">

                        <div class="avatar-wrap">
                            <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt=""
                                 style="width:45px;height:45px;">
                        </div>
                        <div class="post-add-com-input">
                            <form method='post' class='discussionReplyForm' id="{{$dis->id}}">
                                <textarea name="text" class="mentionsInput" style="height:40px;padding:8px;display:inline;width:80%;">
                                    @lang('discussion.write_a_tip_dots')
                                </textarea>
                                <input type="hidden" name="discussion_id" value="{{$dis->id}}"/>
                                <input type="submit" name="submit" value="send" style="display:inline;width:100px;vertical-align: top;height:40px;"/>
                            </form>
                        </div>
                    </div>
                    @endif
                    <div class="post-tips-top-layer">
                        <div class="post-tips-top">
                            @if($dis->type==1)
                                <h4 class="post-tips-ttl">@lang('discussion.strings.recommendations_by_users')</h4>
                            @elseif($dis->type==2)
                                <h4 class="post-tips-ttl">@lang('discussion.strings.tips_by_users')</h4>
                            @elseif($dis->type==3)
                                <h4 class="post-tips-ttl">@lang('discussion.strings.planning_tips_by_users')</h4>
                            @elseif($dis->type==4)
                                <h4 class="post-tips-ttl">@lang('discussion.strings.answers_by_users')</h4>
                            @endif
                            @if(count($dis->replies)>0)
                            <div>
                                <a href="javascript:;" class="top-first-link discusson-answer-filter" data-dis_id="{{$dis->id}}">
                                    @lang('discussion.strings.top_first') <i class="trav-caret-down"></i></a>
                                <a href="javascript:;" class="top-first-link discusson-latest-filter latest-link" data-dis_id="{{$dis->id}}">
                                    @lang('discussion.strings.latest') <i class="trav-caret-down"></i></a>
                            </div>
                            @endif
                        </div>
                        <div id="preLoader{{$dis->id}}" class="text-center" style="display: none;">
                           <img src='{{asset('assets2/image/preloader.gif')}}' width='50px'/>
                       </div>
                        <div class="post-tips-main-block reply-block-{{$dis->id}}" id="{{$dis->id}}">
                            @if(count($dis->replies)>0)
                                @foreach($dis->replies()->orderBy('created_at', 'desc')->get() AS $reply)
                                    <div class="post-tips-row">
                                        <div class="tips-top">
                                            <div class="tip-avatar">
                                                <img src="{{check_profile_picture(@$reply->author->profile_picture)}}"
                                                     alt="" style="width:25px;height:25px;">
                                            </div>
                                            <div class="tip-content" style="width: 89%">
                                                <div class="top-content-top">
                                                    <a href="{{url_with_locale('profile/'.@$reply->author->id)}}"
                                                       class="name-link">{{@$reply->author->name}}</a>
                                                    {!! get_exp_icon(@$reply->author) !!}
                                                    <span>said</span><span class="dot"> · </span>
                                                    <span>{{diffForHumans(@$reply->created_at)}}</span>
                                                </div>
                                                <div class="tip-txt">
                                                    <p class="dis_reply_{{$reply->id}}" style="overflow-wrap: break-word;">
                                                    @php
                                                        $showChar = 100;
                                                        $ellipsestext = "...";
                                                        $moretext = "see more";
                                                        $lesstext = "see less";

                                                        $content = $reply->reply;

                                                        if(strlen($content) > $showChar) {

                                                            $c = substr($content, 0, $showChar);
                                                            $h = substr($content, $showChar, strlen($content) - $showChar);

                                                            $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                                                            $content = $html;
                                                        }
                                                    @endphp
                                                    {!!$content!!}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tips-footer updownvote" id="{{$reply->id}}">

                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="post-tips-row empty" id="{{$dis->id}}">
                                    <div class="tips-top">
                                        @lang('discussion.no_tips_for_now_dots')
                                    </div>

                                </div>
                            @endif


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end MODALS -->