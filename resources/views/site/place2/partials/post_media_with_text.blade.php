<!-- MEDIA WITH TEXT -->

<div class="post" filter-tab="#media" @if(isset($search_result)) search-tab="#searched" @endif>
    <div class="post__header"><img class="post__avatar" src="{{check_profile_picture($post->author->profile_picture)}}" alt="" role="presentation">
        <div class="post__title-wrap"><a class="post__username" href="{{url_with_locale('profile/'.$post->author->id)}}">{{$post->author->name}}</a>
            {!! get_exp_icon($post->author) !!}
            <div class="post__posted">Uploaded a <strong>photo</strong> {{diffForHumans($post->created_at)}}
            <?php
                switch($post->permission){
                    case(0):
                ?>
                        <svg class="icon icon--earth">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#earth')}}"></use>
                        </svg>
                <?php
                        break;
                    case(1):
                ?>
                        <svg class="icon icon--earth">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#follow')}}"></use>
                        </svg>
                <?php
                        break;
                    case(2):
                ?>
                        <svg class="icon icon--earth">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#user')}}"></use>
                        </svg>
                <?php
                        break;
                }
                ?>

        </div>
        </div>
        <div class="dropdown">
            <button class="post__menu" type="button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="icon icon--angle-down">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                </svg>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post">
                @if(Auth::user()->id)
                    @if(Auth::user()->id==$post->users_id)
                        <a class="dropdown-item" href="#" onclick="post_delete('{{$post->id}}','Post', this, event)">
                            <span class="icon-wrap">
                                <!--<i class="trav-flag-icon-o"></i>-->
                            </span>
                            <div class="drop-txt">
                                <p><b>@lang('buttons.general.crud.delete')</b></p>
                                <p style="color:red">@lang('home.remove_this_post')</p>
                            </div>
                        </a>
                    @else
                        @include('site.home.partials._info-actions', ['post'=>$post])
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="post__content" onclick="modal_blog_show(this, event, 'media', {{ @$post->medias[0]->media->id }})">
        <div class="post-media__caption post__text">
            @php
                $showChar = 100;
                $ellipsestext = "...";
                $moretext = "see more";
                $lesstext = "see less";

                $content = $post->text;

                if(strlen($content) > $showChar) {

                    $c = substr($content, 0, $showChar);
                    $h = substr($content, $showChar, strlen($content) - $showChar);

                    $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                    $content = $html;
                }
            @endphp
            {!! $content !!}

        </div>
        <div class="post-media">

            <div class="post-media__row" style="width: 100%;">
                @if(is_object($post->medias) && count($post->medias) >= 4)
               <br />
                @foreach($post->medias AS $photo)
                    @php
                        if( is_null($photoMedia = $photo->media))
                            continue;

                        $file_url = $photoMedia->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                        if($loop->index > 3) {
                            echo "<a href='https://s3.amazonaws.com/travooo-images2/".$file_url."' data-lightbox='media__post".$post->id."' style='display:none'></a>";
                            continue;
                        }
                    @endphp
                    @if($loop->index % 3 == 1 || $loop->index == 0)
                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                    @endif
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden; @if($loop->index == 0) height:395px;@else height:200px; @endif ">
                            @if(in_array($ext, $allowed_video))
                            <video width="595" controls>
                                <source src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" type="video/mp4">

                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" data-lightbox="media__post{{$post->id}}">
                                <img src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);max-width: none;">
                            </a>
                            @endif

                            @if($loop->index == 3 && count($post->medias) > 4)
                                <div href="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" data-lightbox="media__post{{$post->id}}" style="position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;background-color: rgba(0,0,0,0.5);color: white;font-weight: bold;display: flex;align-items: center;justify-content: center;">+{{count($post->medias) - 4}} Photos</div>
                            @endif
                        </li>
                    @if($loop->index % 3 == 0)
                    </ul>
                    @endif
                @endforeach



            @elseif(is_object($post->medias) && count($post->medias) == 3)
                @foreach($post->medias AS $photo)
                    @php
                        if( is_null($photoMedia = $photo->media))
                            continue;

                        $file_url = $photoMedia->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp
                    @if($loop->index % 2 == 1 || $loop->index == 0)
                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                    @endif
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden; @if($loop->index == 0) height:395px;@else height:200px; @endif ">
                            @if(in_array($ext, $allowed_video))
                            <video width="595" controls>
                                <source src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" type="video/mp4">

                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" data-lightbox="media__post{{$post->id}}">
                                <img src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);max-width: none;">
                            </a>
                            @endif

                        </li>
                    @if($loop->index % 2 == 0)
                    </ul>
                    @endif
                @endforeach

            @elseif(is_object($post->medias) && count($post->medias) == 2)
                @foreach($post->medias AS $photo)
                    @php
                        if( is_null($photoMedia = $photo->media))
                            continue;

                        $file_url = $photoMedia->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp

                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;">
                            @if(in_array($ext, $allowed_video))
                            <video width="595" controls>
                                <source src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" type="video/mp4">

                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" data-lightbox="media__post{{$post->id}}">
                                <img src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);max-width: none;">
                            </a>
                            @endif

                        </li>
                    </ul>
                @endforeach

            @elseif(is_object($post->medias) && count($post->medias) == 1)
                @foreach($post->medias AS $photo)
                    @php
                        if( is_null($photoMedia = $photo->media))
                            continue;

                        $file_url = $photoMedia->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp

                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:595px;">
                            @if(in_array($ext, $allowed_video))
                            <video width="595" controls>
                                <source src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" type="video/mp4">

                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" data-lightbox="media__post{{$post->id}}">
                                <img src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);max-width: none;">
                            </a>
                            @endif

                        </li>
                    </ul>
                @endforeach


            @endif

            </div>
        </div>
    </div>
    @include('site.place2.partials._post_footer')
    <div class="comments" style="display:none" data-content="post__media_{{$post->id}}">
        <div class="comments__header">
            <div class="comments__filter">
                <button class="comments__filter-btn" type="button" data-type="Top">Top</button>
                <button class="comments__filter-btn active" type="button" data-type="New">New</button></div>
            <div class="comm-count-info">
                        <strong>0</strong> / <span class="{{$post->id}}-all-comments-count">{{count($post->comments)}}</span>
            </div>
        </div>
        <div class="comments__items post-comment-wrapper place-comment-block sortBody">
            @foreach($post->comments AS $ppc)
                @include('site.place2.partials._post_comment_block')
            @endforeach
        </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-inputs">
                <form class="postscomment" method="POST" action="{{url("new-comment") }}" id="{{$post->id}}" autocomplete="off" enctype="multipart/form-data">
                    <input type="hidden" data-id="pair{{$post->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                    <div class="post-create-block post-comment-create-block place-comment-block post-regular-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" data-id="postscommenttext" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias place-media">
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <li class="post-options">
                                        <input type="file" name="file[]" class="post-comment-media-input" data-post_id="{{$post->id}}" id="postcommentfile{{$post->id}}" style="display:none" multiple>
                                        <i class="fa fa-camera click-target" data-target="postcommentfile{{$post->id}}"></i>
                                    </li>
                                </ul>
                            </div>

                            <button type="submit" class="btn btn-primary btn-disabled d-none"></button>

                        </div>

                    </div>
                    <input type="hidden" name="post_id" value="{{$post->id}}"/>
                </form>

                </div>
            </div>

        @endif
    </div>
</div>
