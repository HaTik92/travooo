
{{-- <div class="post" filter-tab="#report" @if(isset($search_result)) search-tab="#searched" @endif onclick="modal_blog_show(this, event, 'report', {{ $report->id }})"> --}}

<div class="post" >
    <div class="post__header">
        <img class="post__avatar" src="{{check_profile_picture(@$report->author->profile_picture)}}" alt="" role="presentation">
        <div class="post__title-wrap">
            <a class="post__username" target="_blank" href="{{url_with_locale('profile/'.@$report->author->id)}}">{{@$report->author->name}}</a>
            {!! get_exp_icon($report->author) !!}
            <div class="post__posted">Wrote a
                <strong>travelog</strong> about
                {{$place->transsingle->title}} at
                {{diffForHumans($report->created_at)}}
            </div>
        </div>
        @if(Auth::check() && Auth::user()->id!=$report->author->id)
            <div class="dropdown">
                <button class="post__menu" type="button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <svg class="icon icon--angle-down">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                    </svg>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Report">
                    @include('site.home.partials._info-actions', ['post'=>$report])
                </div>
            </div>
        @endif
    </div>
    <div class="post__content">
        <div class="post-media">
            <div class="post-media__content" style="width: 96%;">
                <h2 class="post-media__title clickable_titles"  data-href="{{@url_with_locale('reports/'.$report->id)}}" >{{$report->title}}</h2>
                <div class="post-media__text" style="overflow-wrap: break-word;">{{$report->description}}</div>
            </div>
            @if(isset($report->cover[0]) && is_object($report->cover[0]) && $report->cover[0]->val!='')
            <img class="post-media__img" src="{{$report->cover[0]->val}}" alt="{{$report->title}}" title="{{$report->title}}" />
            @endif
        </div>
    </div>
    <div class="post__footer">
        <button class="post__like-btn report_like_button" style="outline:none;" type="button" id="{{$report->id}}">
            <svg class="icon icon--like">
            <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
            </svg>
            <span id="report_like_count_{{$report->id}}"><strong>{{@count($report->likes)}}</strong> Likes</span>
        </button>

        <button class="post__comment-btn place-comment-btn" style="outline:none;" type="button" data-tab="report__commenttext_{{$report->id}}" data-id="{{$report->id}}">
            <svg class="icon icon--comment">
            <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
            </svg>
            <div class="user-list">
                <div class="user-list__item">
                    @foreach(@$report->comments()->groupBy('users_id')->get() AS $pco)
                    <div class="user-list__user"><img class="user-list__avatar" src="{{check_profile_picture($pco->author->profile_picture)}}" alt="{{$pco->author->name}}" title="{{$pco->author->name}}" role="presentation" /></div>
                    @endforeach
                </div>
            </div><span><strong class="{{$report->id}}-all-comments-count">{{@count($report->comments)}}</strong> Comments</span>
        </button>

        <button class="post__like-btn report_share_button" type="button" id="{{$report->id}}">
            <svg class="icon icon--like" style="color: #4080ff">
            <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
            </svg>
            <span id="report_share_count_{{$report->id}}"><strong>{{@count($report->shares)}}</strong> Shares</span>
        </button>
    </div>
    <div class="comments reportcomment-block pp-report-{{$report->id}} comment-block-{{$report->id}}" style="display:none;" data-content="report__commenttext_{{$report->id}}">
        <div class="comments__header" id="{{$report->id}}" {{(count($report->comments)>0)?"":'style=display:none'}}>
            <div class="comments__filter">
                <button class="comments__filter-btn comment-filter-type" type="button" data-type="Top" data-report_id="{{$report->id}}">Top</button>
                <button class="comments__filter-btn comments__filter-btn--active comment-filter-type" type="button" data-type="New" data-report_id="{{$report->id}}">New</button></div>
                <div class="comments__number comm-count-info">
                    <strong>0</strong> / <span class="{{$report->id}}-all-comments-count">{{count($report->comments)}}</span>
                </div>
        </div>
        <div class="comments__items post-comment-wrapper place-comment-block sortBody">
            @if(count($report->comments)>0)
                @foreach($report->comments AS $comment)
                    @include('site.reports.partials.report_comment_block')
                @endforeach
             @else
            @endif
        </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
               <div class="avatar-wrap">
                   <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt="" width="45px" height="45px">
               </div>
               <div class="post-add-com-inputs">
                   <form class="reportCommentForm" method="POST" data-id="{{$report->id}}"  data-type="2" autocomplete="off" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <input type="hidden" data-id="pair{{$report->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                       <div class="post-create-block post-comment-create-block report-comment-block post-regular-block" tabindex="0">
                           <div class="post-create-input">
                               <textarea name="text" data-id="reportcommenttext" class="textarea-customize"
                                         style="display:inline;vertical-align: top;min-height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                               <div class="medias report-media">
                               </div>
                           </div>
                           <div class="post-create-controls">
                               <div class="post-alloptions">
                                   <ul class="create-link-list">
                                       <li class="post-options">
                                           <input type="file" name="file[]" class="report-comment-media-input" data-report_id="{{$report->id}}" id="commentfile{{$report->id}}" style="display:none" multiple>
                                           <i class="fa fa-camera click-target" aria-hidden="true" data-target="commentfile{{$report->id}}"></i>
                                       </li>
                                   </ul>
                               </div>
                           </div>

                       </div>
                       <input type="hidden" name="report_id" value="{{$report->id}}"/>
                       <button type="submit" class="btn btn-primary d-none"></button>
                   </form>

               </div>
           </div>

        @endif
    </div>
</div>
