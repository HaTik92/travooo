<div id="add-post-tripplan-block"
     class="card w-100"
     style="display: none;position: relative;margin-top: 0!important;"
>
    <h2 class="add-post__title">Planning to go to <strong>{{@$place->trans[0]->title}}</strong>?</h2>

    <div class="card__content">
        <div class="d-flex justify-content-center no-text-select">

            <div class="add-post-tripplan-block-item flex-fill text-center">
                <img src="{{asset('assets2/image/memory_trup.png')}}">
                <h4>I have been here</h4>
                <p class="text-muted"><a style="color: #636c72!important;" href="/trip/plan/0?memory=1">Create a memory trip plan</a></p>
            </div>
            <div class="add-post-tripplan-block-item flex-fill text-center">
                <img src="{{asset('assets2/image/create_trip.png')}}">
                <h4>Planning to go here</h4>
                <p class="text-muted"><a style="color: #636c72!important;" href="/trip/plan/0">Create an upcoming trip plan</a></p>
            </div>
            <div class="add-post-tripplan-block-item flex-fill text-center">
                <img src="{{asset('assets2/image/add_trip_icon.png')}}">
                <h4>Add to trip plan</h4>
                <p class="text-muted">
                    <a style="color: #636c72!important;"  data-toggle="popover" data-placement="bottom" data-html="true" data-popover-content="#yourContentHeres" >Add this place to existing trip plan
                    
                        <div id="yourContentHeres" style="display:none;">
                            <div class="popover-body">
                                @foreach($my_plans AS $mp)
                                <div class="row add-trip-block">
                                    <input style="margin-left:25px;margin-right:10px" data-title="{{$mp->title}}" type="checkbox" name="plans_id" id="plans_id" value="{{$mp->id}}" class="checkbox-round">  <span style="">{{$mp->title}}</span>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>