<style>
    .add-post__media
    {
        width: 100%;
    }
    
    .hided
    {
        display: none;
    }
    .white_mark
    {
        width: 100%;
        height: 100%;
        position: fixed;
        z-index: 8;
        left: 0;
        top: 0;
    }
    .spinner-border {
        display: inline-block;
        position: absolute;
        right: 0;
        width: 2rem;
        height: 2rem;
        float: right;
        margin-right: 35px;
        vertical-align: text-bottom;
        border: .25em solid currentColor;
        border-right-color: transparent;
        border-radius: 50%;
        -webkit-animation: spinner-border .75s linear infinite;
        animation: spinner-border .75s linear infinite;
    }.ratings {
        float:left;
        margin-top: 6px;
        margin-right: 15px;
        margin-left: 25px;
    }

    /* :not(:checked) is a filter, so that browsers that don’t support :checked don’t 
      follow these rules. Every browser that supports :checked also supports :not(), so
      it doesn’t make the test unnecessarily selective */
    .ratings:not(:checked) > input {
        position:absolute;
        left:-9999px;
        clip:rect(0,0,0,0);
    }

    .ratings:not(:checked) > label {
        float:right;
        width:1em;
        /* padding:0 .1em; */
        overflow:hidden;
        white-space:nowrap;
        cursor:pointer;
        font-size:30px;
        /* line-height:1.2; */
        color:#ddd;
    }

    .ratings:not(:checked) > label:before {
        content: '★ ';
    }

    .ratings > input:checked ~ label {
        color: #4080FF;
        
    }

    .ratings:not(:checked) > label:hover,
    .ratings:not(:checked) > label:hover ~ label {
        color: #4080FF;
        
    }

    .ratings > input:checked + label:hover,
    .ratings > input:checked + label:hover ~ label,
    .ratings > input:checked ~ label:hover,
    .ratings > input:checked ~ label:hover ~ label,
    .ratings > label:hover ~ input:checked ~ label {
        color: #4080FF;
        
    }

    .ratings > label:active {
        position:relative;
        top:2px;
        left:2px;
    }
    @keyframes spinner-border {
      to { transform: rotate(360deg); }
    }
    </style>
    
    
    <div class="w-100" id="add-post-review-block" style="display: none;position: relative;margin-top: 0!important; border: 1px solid #EDEDED;border-radius: 7px;">
        <h2 class="add-post__title">Write a review about <strong>{{@$place->trans[0]->title}}</strong></h2>
        <div class="add-post__content" style="background:white">
            <form class="add-post__form formPostBox" id="_place_add_review_form">
                <img class="add-post__avatar" data-src="{{check_profile_picture($me->profile_picture)}}" alt="" role="presentation" />
                <textarea id="add_review_text" class="add-post__textarea" name="text" data-emoji-input="unicode" data-emojiable="true" placeholder="What is your review for this place?"></textarea>
                <div class="add-post__form-footer">
                    <button class="add-post__emoji" emoji-target="add_post_text" type="button">🙂</button>
                </div>
                <input type="file" name="file" id="_add_review_imagefile" style="display:none" multiple>

                <input type="hidden" name="type" value="" />
            </form>
            <div class="add-post__footer">
                <div class="add-post__footer-left">
                    <div class="add-post__media_review">
    
                    </div>
                    <button class="add-post__post-type" data-triggered="review" type="button" data-type="image" style="font-family: inherit;font-size: 15px;color: #555557;width: 143px;border-radius: 25px;background-color: #F5F7FC;">
                        <svg class="icon icon--camera" style="padding-bottom: 2px;margin-right: 5px;margin-left: -1px;">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#camera')}}"></use>
                        </svg>
                         <span style="    font-weight: 500;">Photo / Video</span>
                    </button>
                </div>
                <div class="add-post__footer-right">
                    <div class="rating-wrap" dir="auto" style="border: 1px solid #ededed;display: flex; margin-top: -5px;width: 267px;border-radius: 30px;background: #F5F7FC;">
                        <span class="text-muted" style="padding-left: 15px;font-size: 17px; padding-top: 14px">Rating</span>
                        <div class="row">
                            <div class="ratings">
                            <input type="radio" id="star1" name="review_score" value="5" /><label for="star1" title="">1 star</label>
                            <input type="radio" id="star2" name="review_score" value="4" /><label for="star2" title="">2 stars</label>
                            <input type="radio" id="star3" name="review_score" value="3" /><label for="star3" title="">3 stars</label>
                            <input type="radio" id="star4" name="review_score" value="2" /><label for="star4" title="">4 stars</label>
                            <input type="radio" id="star5" name="review_score" value="1" /><label for="star5" title="">5 stars</label>
                            </div>
                        </div>
                      <span style="padding-left: 5px; font-size: 17px; padding-top: 14px;" >{{round($reviews_avg, 1)}}</span>
                  </div>                
              

                    <button class="btn btn--sm btn--main" onclick="_place_send_review(this)" style='font-family: inherit;height: 40px;'>Publish</button>
                </div>
            </div>
            <div class="add-post__footer" style="height: 70px;display:none;">
                <div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
            </div> 
        </div>
        <div class="add-post__resize">
            <svg class="icon icon--resize">
                <use xlink:href="{{asset('assets3/img/sprite.svg#resize')}}"></use>
            </svg>
        </div>
    </div>
    