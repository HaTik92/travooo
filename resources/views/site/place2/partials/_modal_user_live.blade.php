<div class="modal fade" id="live_user_popup" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="height: 45px;">
                <h5 style="padding-bottom: 10px;" class="modal-title w-100">People who checked-in here in the past 24
                    hours</h5>
                <button type="button" class="close" style="color:#A7A7A7" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="overflow: hidden;height: 400px;overflow-y: scroll;">
                @if(count($live_users)>0)
                <div class="row">
                    @foreach ($live_users as $user)
                    <div class="w-100" style="border-bottom: 1px solid #EDEDED;padding-bottom: 10px;padding-top: 10px">
                        <img style="margin-left:10px;margin-right:6px" width="40px"
                            src="{{check_profile_picture($user['users']->profile_picture)}}"> <span
                            style="display:inline-block;font-family:inherit"><strong style="font-weight:500">{{$user['users']->name}}</strong></span>
                            @if(Auth::user()->id !=$user['users']->id)
                                <button data-toggle="modal" data-target="#add_message_popup" data-user_id="{{$user['users']->id}}" data-name="{{$user['users']->name}}" data-profile_picture="{{$user['users']->profile_picture}}" class="btn btn--main btn-checkin-follow">Message</button> 
                                @if(!$user['is_followed'])
                                    <button class="btn btn--main btn-checkin-follow actionbtnfollow" data-type='follow' style="font-family: inherit;" value="{{$user['users']->id}}">Follow</button>
                                @else 
                                    <button class="btn btn--main btn-checkin-follow btn-unfollow actionbtnfollow"  data-type='unfollow' style="font-family: inherit;" value="{{$user['users']->id}}">Unfollow</button>
                                @endif
                            @endif
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_message_popup" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="height: 45px;">
                <h5 class="modal-title w-100">Message <span id="user_avatar_for_image" style="margin: 0 5px;"></span> <span
                        class="live_user_name" style="font-weight: 400;"></span></h5>
                <button type="button" class="close" style="color:#A7A7A7" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="participants" name="participants"/>
                <textarea rows="5" cols="30" style="width: 100%;margin-top: 0px;margin-bottom: 0px;height: 94px;resize: none;padding: 0;"
                    placeholder="Type a message..." id="message" name="message"></textarea>
            </div>

            <div class="modal-footer" style=" height: 55px;">
                <div class="w-100">
                    <div class="add-post__footer" dir="auto" style="padding-left:0px;padding-right: 0;">
                        <button class="add-post__emoji" emoji-target="message" type="button" dir="auto"onclick="showFaceBlock()" id="faceEnter">🙂</button>
                        <button id="btnSendMsg" class="btn btn--main float-right" style="font-family:inherit;height: 35px;">Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>